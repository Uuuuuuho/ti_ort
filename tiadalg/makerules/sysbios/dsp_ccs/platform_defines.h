#ifndef __PLATFORM_DEFINES_H__
#define __PLATFORM_DEFINES_H__

#include "ti_file_io.h"

#define EXTRA_MEM_FOR_ALIGN (0)
#define L4_MEM_SIZE  (668 * 1024 * 1024)

#endif /*__PLATFORM_DEFINES_H__*/
