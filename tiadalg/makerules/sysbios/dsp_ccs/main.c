#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#ifndef HOST_EMULATION
#include <xdc/runtime/Memory.h>
#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Task.h>
#include <ti/sysbios/family/c7x/Hwi.h>
#include <ti/sysbios/family/c7x/Cache.h>

#include <ti/csl/soc.h>
#include <ti/csl/csl_clec.h>
#include <ti/drv/sciclient/sciclient.h>
#endif

#include "memmap.h"

int32_t algoMain(int32_t argc, char *argv[]);

static uint8_t algoTskStackMain[64*1024]
__attribute__ ((section(".bss:taskStackSection")))
__attribute__ ((aligned(8192)))
    ;

#ifndef HOST_EMULATION
void algoMainWrapper(UArg arg0, UArg arg1)
{
	int argc = 0;
	char **argv = NULL;

	Sciclient_ConfigPrms_t sciClientCfg;

	Sciclient_configPrmsInit(&sciClientCfg);
	Sciclient_init(&sciClientCfg);

	if(!argc) {
      argc = 1;
      argv = malloc(sizeof(char *));
      argv[0] = "application";
	}

	/* add a trailing NULL */
	argv = (char **)realloc(argv, (argc + 1) * sizeof(char *));
	argv[argc] = NULL;

    ti_sysbios_family_c7x_Cache_Size  cacheSize;
    Cache_getSize(&cacheSize);

    printf("L1p cache size is %d \n", cacheSize.l1pSize);
    printf("L1d cache size is %d \n", cacheSize.l1dSize);
    printf("L2 cache size is %d \n", cacheSize.l2Size);

    algoMain(argc,argv);
}

/* To set C71 timer interrupts */
void algoTimerInterruptInit(void)
{
    CSL_ClecEventConfig   cfgClec;
    CSL_CLEC_EVTRegs     *clecBaseAddr = (CSL_CLEC_EVTRegs*)CSL_COMPUTE_CLUSTER0_CLEC_REGS_BASE;

    uint32_t input         = 1249; /* Used for Timer Interrupt */
    uint32_t corepackEvent = 15;

    /* Configure CLEC */
    cfgClec.secureClaimEnable = FALSE;
    cfgClec.evtSendEnable     = TRUE;
    cfgClec.rtMap             = CSL_CLEC_RTMAP_CPU_ALL;
    cfgClec.extEvtNum         = 0;
    cfgClec.c7xEvtNum         = corepackEvent;
    CSL_clecConfigEvent(clecBaseAddr, input, &cfgClec);
    CSL_clecConfigEventLevel(clecBaseAddr, input, 0); /* configure interrupt as pulse */
    Hwi_setPriority(corepackEvent, 1);
}

void algoC7xClecInitForNonSecAccess(void)
{
    CSL_ClecEventConfig   cfgClec;
    CSL_CLEC_EVTRegs     *clecBaseAddr = (CSL_CLEC_EVTRegs*)CSL_COMPUTE_CLUSTER0_CLEC_REGS_BASE;

    uint32_t max_inputs      = 2048;
    uint32_t dru_input_start = 192;
    uint32_t dru_input_num   = 16;
    uint32_t i;

    /* make secure claim bit to FALSE so that after we switch to non-secure mode
     * we can program the CLEC MMRs
     */
    for(i=0; i<max_inputs; i++)
    {
        cfgClec.secureClaimEnable = FALSE;
        cfgClec.evtSendEnable     = FALSE;
        cfgClec.rtMap             = CSL_CLEC_RTMAP_DISABLE;
        cfgClec.extEvtNum         = 0;
        cfgClec.c7xEvtNum         = 0;
        CSL_clecConfigEvent(clecBaseAddr, i, &cfgClec);
    }
    /* program CLEC events from DRU used for polling by TIDL
     * to map to required events in C7x
     */
    for(i=dru_input_start; i<(dru_input_start+dru_input_num); i++)
    {
        /* Configure CLEC */
        cfgClec.secureClaimEnable = FALSE;
        cfgClec.evtSendEnable     = TRUE;
        cfgClec.rtMap             = CSL_CLEC_RTMAP_CPU_ALL;
        cfgClec.extEvtNum         = 0;
        cfgClec.c7xEvtNum         = (i-dru_input_start)+32;
        CSL_clecConfigEvent(clecBaseAddr, i, &cfgClec);
    }
}
void algoCacheInit()
{
    ti_sysbios_family_c7x_Cache_Size  cacheSize;

    /* init cache size here, since this needs to be done in secure mode */
    cacheSize.l1pSize = ti_sysbios_family_c7x_Cache_L1Size_32K;
    cacheSize.l1dSize = ti_sysbios_family_c7x_Cache_L1Size_32K;
    cacheSize.l2Size  = ti_sysbios_family_c7x_Cache_L2Size_64K;
    Cache_setSize(&cacheSize);
}



void algoMmuInit(void)
{
    algoC7xClecInitForNonSecAccess();

    algoMmuMap(false);
    algoMmuMap(true);
    algoCacheInit();
}

void algoIdleLoop(void)
{
   __asm(" IDLE");
}

void algoPerfStatsBiosLoadUpdate(void)
{
}

int32_t debug_printf(const char *format, ...)
{
	va_list ap;
	int32_t ret;

	va_start(ap, format);
	ret = vprintf(format, ap);
	va_end(ap);

	return ret;
}
#endif

int main(int argc, char **argv)
{
#ifndef HOST_EMULATION
    Task_Params tskParams;
    Task_Handle task;

    algoTimerInterruptInit();

    Task_Params_init(&tskParams);

    tskParams.arg0 = (UArg)NULL;
    tskParams.arg1 = (UArg)NULL;
    tskParams.priority = 8u;
    tskParams.stack = algoTskStackMain;
    tskParams.stackSize = sizeof (algoTskStackMain);
    task = Task_create(algoMainWrapper, &tskParams, NULL);
    if(NULL == task)
    {
        BIOS_exit(0);
    }
    BIOS_start();
#else
    algoMain(argc,argv);
#endif
    return 0;
}
