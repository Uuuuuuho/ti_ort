#ifeq ($(TARGET_PLATFORM), J7_CCS) || ifeq ($(TARGET_PLATFORM), PC)

#ifeq ($(TARGET_PLATFORM),$(filter $(TARGET_PLATFORM),J7_CCS PC))
ifeq ($(TARGET_PLATFORM),J7_CCS)
include $(PRELUDE)

ifneq ($(TARGET_PLATFORM), PC)
TARGET      := TI_DEVICE_tiadalg_algo
else
TARGET      := PC_tiadalg_algo
endif

TARGETTYPE  := exe
CSOURCES    := $(call all-c-files)

# DSP build needs extenal getopt() support


# DSP build needs XDC support
XDC_BLD_FILE = $($(_MODULE)_SDIR)/config_c71.bld
XDC_IDIRS    = $($(_MODULE)_SDIR)
XDC_CFG_FILE = $($(_MODULE)_SDIR)/c7x_1.cfg
XDC_PLATFORM = "ti.platforms.tms320C7x:J7ES"

# DSP build needs linker command files for memory maps
LINKER_CMD_FILES +=  $($(_MODULE)_SDIR)/linker_mem_map.cmd
LINKER_CMD_FILES +=  $($(_MODULE)_SDIR)/linker.cmd

# DSP build needs CGT, BIOS, and XDC include files
ifeq ($(TARGET_PLATFORM), PC)
IDIRS += $(CGT7X_ROOT)/host_emulation/include/C7100
else
IDIRS += $(CGT7X_ROOT)/include
IDIRS += $(BIOS_PATH)/packages
IDIRS += $(PDK_PATH)/packages
IDIRS += $(XDCTOOLS_PATH)/packages
endif

IDIRS += $(HOST_ROOT)/


# path to algorithm library
ifeq ($(TARGET_PLATFORM), PC)
LDIRS += $(CGT7X_ROOT)/host_emulation
LDIRS += $(PDK_PATH)/packages/ti/csl/lib/j721e/c7x-hostemu/$(TARGET_BUILD)
LDIRS += $(PDK_PATH)/packages/ti/osal/lib/nonos/j721e/c7x-hostemu/$(TARGET_BUILD)
LDIRS += $(PDK_PATH)/packages/ti/drv/sciclient/lib/j721e_hostemu/c7x-hostemu/$(TARGET_BUILD)
LDIRS += $(PDK_PATH)/packages/ti/drv/udma/lib/j721e_hostemu/c7x-hostemu/$(TARGET_BUILD)
else
LDIRS += $(CGT7X_ROOT)/lib
LDIRS += $(PDK_PATH)/packages/ti/csl/lib/j721e/c7x/$(TARGET_BUILD)
LDIRS += $(PDK_PATH)/packages/ti/osal/lib/tirtos/j721e/c7x/$(TARGET_BUILD)
LDIRS += $(PDK_PATH)/packages/ti/drv/sciclient/lib/j721e/c7x_1/$(TARGET_BUILD)
LDIRS += $(PDK_PATH)/packages/ti/drv/udma/lib/j721e/c7x_1/$(TARGET_BUILD)
LDIRS += $(HOST_ROOT)/../../lib/$(TARGET_CPU)/$(TARGET_BUILD)
endif

# External libraries
ifeq ($(TARGET_PLATFORM), PC)
ADDITIONAL_STATIC_LIBS += dmautils.lib
ADDITIONAL_STATIC_LIBS += udma.lib
ADDITIONAL_STATIC_LIBS += sciclient.lib
ADDITIONAL_STATIC_LIBS += ti.csl.lib
ADDITIONAL_STATIC_LIBS += ti.osal.lib
else
ADDITIONAL_STATIC_LIBS += ti.csl.ae71
ADDITIONAL_STATIC_LIBS += ti.osal.ae71
ADDITIONAL_STATIC_LIBS += sciclient.ae71
ADDITIONAL_STATIC_LIBS += udma.ae71
ADDITIONAL_STATIC_LIBS += dmautils.ae71
endif

ifeq ($(TARGET_PLATFORM), PC)
ADDITIONAL_STATIC_LIBS += libC7100-host-emulation.a
endif

ADDITIONAL_STATIC_LIBS += libtiadalg_structure_from_motion.a

# Suppress this warning, 10063-D: entry-point symbol other than "_c_int00" specified
# c7x boots in secure mode and to switch to non-secure mode we need to start at a special entry point '_c_int00_secure'
# and later after switching to non-secure mode, sysbios jumps to usual entry point of _c_int00
# Hence we need to suppress this warning
ifneq ($(TARGET_PLATFORM), PC)
CFLAGS+=--diag_suppress=10063
endif

# CCS build needs Host IO
#DEFS += USE_HOST_FILE_IO
#DEFS += DDR_BW_STATS
ifeq ($(TARGET_PLATFORM), PC)
DEFS  += HOST_EMULATION
DEFS  += _HOST_BUILD
override CC := g++-5
else
DEFS  += SOC_J721E
endif

DEFS  += __C7100__
DEFS  += TIADALG_BIOS_BUILD

# get the common make flags from test/src/<plat>/../concerto_common.mak
include $(HOST_ROOT)/concerto_common.mak

include $(FINALE)

endif
