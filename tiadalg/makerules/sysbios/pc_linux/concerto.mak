ifeq ($(TARGET_PLATFORM), PC)
ifeq ($(TARGET_OS), LINUX)

include $(PRELUDE)

TARGET      := PC_tiadalg_algo.out
TARGETTYPE  := exe
CSOURCES    := $(call all-c-files)

# host emulation needs HE includes
IDIRS += $(CGT7X_ROOT)/host_emulation/include/C7100
IDIRS += $(HOST_ROOT)

# library search dirs are always platform specific
LDIRS += $(CGT7X_ROOT)/host_emulation
#LDIRS += $(PDK_PATH)/packages/ti/csl/lib/j721e/c7x-hostemu/$(TARGET_BUILD)
#LDIRS += $(PDK_PATH)/packages/ti/osal/lib/nonos/j721e/c7x-hostemu/$(TARGET_BUILD)
#LDIRS += $(PDK_PATH)/packages/ti/drv/sciclient/lib/j721e_hostemu/c7x-hostemu/$(TARGET_BUILD)
#LDIRS += $(PDK_PATH)/packages/ti/drv/udma/lib/j721e_hostemu/c7x-hostemu/$(TARGET_BUILD)

LDIRS += $(PDK_PATH)/packages/ti/csl/lib/j721e/c7x-hostemu/release
LDIRS += $(PDK_PATH)/packages/ti/osal/lib/nonos/j721e/c7x-hostemu/release
LDIRS += $(PDK_PATH)/packages/ti/drv/sciclient/lib/j721e_hostemu/c7x-hostemu/release
LDIRS += $(PDK_PATH)/packages/ti/drv/udma/lib/j721e_hostemu/c7x-hostemu/release

# path to tidl_algo and tidl_priv_algo
LDIRS += $($(_MODULE)_SDIR)/../../../lib/PC/dsp/algo/$(TARGET_BUILD)
LDIRS += $(HOST_ROOT)/../../lib/$(TARGET_CPU)/$(TARGET_BUILD)


# External libraries: The order in which they are defined ins important
ADDITIONAL_STATIC_LIBS += dmautils.lib
ADDITIONAL_STATIC_LIBS += udma.lib
ADDITIONAL_STATIC_LIBS += sciclient.lib
ADDITIONAL_STATIC_LIBS += ti.csl.lib
ADDITIONAL_STATIC_LIBS += ti.osal.lib

# Custom Library
#STATIC_LIBS += tidl_custom

# internal libraries
#STATIC_LIBS += tidl_algo
#STATIC_LIBS += tidl_obj_algo
#STATIC_LIBS += tidl_priv_algo
STATIC_LIBS += tiadalg_structure_from_motion


# External libraries: The order in which they are defined ins important
# Also, must be defined after the internal libraries
#STATIC_LIBS += mmalib_cn_x86_64
#STATIC_LIBS += mmalib_x86_64
#STATIC_LIBS += common_x86_64
STATIC_LIBS += C7100-host-emulation

# CUDA
# TODO may have LDIRS, IDIRS, {STATIC, SHARED}_LIBS
ifeq ($(BUILD_WITH_CUDA), 1)
DEFS += BUILD_WITH_CUDA
endif

# OPENCV
ifeq ($(BUILD_WITH_OPENCV), 1)
DEFS += BUILD_WITH_OPENCV

DEFS += __C7100__
# search path for opencv includes
IDIRS += $(TIDL_OPENCV_PATH)/modules/core/include
IDIRS += $(TIDL_OPENCV_PATH)/modules/highgui/include
IDIRS += $(TIDL_OPENCV_PATH)/modules/imgcodecs/include
IDIRS += $(TIDL_OPENCV_PATH)/modules/videoio/include
IDIRS += $(TIDL_OPENCV_PATH)/modules/imgproc/include

# search path for opencv library
LDIRS += $(TIDL_OPENCV_PATH)/cmake/lib
LDIRS += $(TIDL_OPENCV_PATH)/cmake/3rdparty/lib

# opencv libraries
STATIC_LIBS += opencv_imgproc
STATIC_LIBS += opencv_imgcodecs
STATIC_LIBS += opencv_core
STATIC_LIBS += libtiff
STATIC_LIBS += libwebp
STATIC_LIBS += libpng
STATIC_LIBS += libjpeg
STATIC_LIBS += IlmImf
STATIC_LIBS += zlib
STATIC_LIBS += libjasper

# opencv needs libdl.so
SHARED_LIBS += dl
endif

# AVX
# TODO may have LDIRS, IDIRS, {STATIC, SHARED}_LIBS
ifeq ($(TIDL_BUILD_WITH_AVX), 1)
DEFS += TIDL_BUILD_WITH_AVX
endif

# defines for host emulation
DEFS += HOST_EMULATION
DEFS += _HOST_BUILD
DEFS += TIADALG_BIOS_BUILD

# get the common make flags from test/src/<plat>/../concerto_common.mak
#include $($(_MODULE)_SDIR)/../concerto_common.mak
include $(HOST_ROOT)/concerto_common.mak

# override CC so that build uses g++-5
override CC := g++-5

include $(FINALE)

endif
endif
