#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>
#include <malloc.h>

//#include "platform_common.h"

#if 0
uint8_t *L1Scratch;
uint8_t *L2Scratch;
uint8_t *L3Scratch;
uint8_t *L4Scratch;
#endif

int32_t algoMain(int32_t argc, char **argv);

#if 0
int32_t debug_printf(const char *format, ...)
{
	va_list ap;
	int32_t ret;

	va_start(ap, format);
	ret = vprintf(format, ap);
	va_end(ap);

	return ret;
}
#endif

uint64_t tidltb_virtToPhyAddrConversion(const void *virtAddr,
                                      uint32_t chNum,
                                      void *appData)
{
	return (uint64_t)virtAddr;
}

int main(int argc, char **argv)
{


	algoMain(argc, argv);


	return 0;
}
