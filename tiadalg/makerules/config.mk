#
# Copyright (c) {2015 - 2018} Texas Instruments Incorporated
#
# All rights reserved not granted herein.
#
# Limited License.
#
# Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
# license under copyrights and patents it now or hereafter owns or controls to make,
# have made, use, import, offer to sell and sell ("Utilize") this software subject to the
# terms herein.  With respect to the foregoing patent license, such license is granted
# solely to the extent that any such patent is necessary to Utilize the software alone.
# The patent license shall not apply to any combinations which include this software,
# other than combinations with devices manufactured by or for TI ("TI Devices").
# No hardware patent is licensed hereunder.
#
# Redistributions must preserve existing copyright notices and reproduce this license
# (including the above copyright notice and the disclaimer and (if applicable) source
# code license limitations below) in the documentation and/or other materials provided
# with the distribution
#
# Redistribution and use in binary form, without modification, are permitted provided
# that the following conditions are met:
#
# *       No reverse engineering, decompilation, or disassembly of this software is
# permitted with respect to any software provided in binary form.
#
# *       any redistribution and use are licensed by TI for use only with TI Devices.
#
# *       Nothing shall obligate TI to provide you with source code for the software
# licensed and provided to you in object code.
#
# If software source code is provided to you, modification and redistribution of the
# source code are permitted provided that the following conditions are met:
#
# *       any redistribution and use of the source code, including any resulting derivative
# works, are licensed by TI for use only with TI Devices.
#
# *       any redistribution and use of any object code compiled from the source code
# and any resulting derivative works, are licensed by TI for use only with TI Devices.
#
# Neither the name of Texas Instruments Incorporated nor the names of its suppliers
#
# may be used to endorse or promote products derived from this software without
# specific prior written permission.
#
# DISCLAIMER.
#
# THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
# OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
# OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
# IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
# OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
# OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
# OF THE POSSIBILITY OF SUCH DAMAGE.
#
#

ifdef SystemRoot
PSDK_PATH ?= C:\ti
else
PSDK_PATH ?= $(abspath ..)
##PSDK_PATH ?= /ti/j7/workarea
endif

DSP_TOOLS_C6X       ?=$(PSDK_PATH)/ti-cgt-c6000_8.3.7
DSP_TOOLS_C7X       ?=$(PSDK_PATH)/ti-cgt-c7000_1.4.2.LTS
SHOW_COMMANDS       ?= 0
UTILS_PATH          ?= C:\\ti\\ccs930\\ccs\\utils\\cygwin

ifdef SystemRoot

BIOS_PATH           ?="$(PSDK_PATH)\bios_6_83_02_07"
XDCTOOLS_PATH       ?="$(PSDK_PATH)\xdctools_3_61_04_40_core"
# windows build, change the C7x compiler path and CCS installation path appropriately
PDK_INSTALL_PATH    ?=$(PSDK_PATH)\pdk_jacinto_08_00_00_37\
CONCERTO_ROOT       ?=$(PSDK_PATH)\vision_apps\concerto
IVISION_PATH        ?="$(PSDK_PATH)\ivision"

else

BIOS_PATH           ?="$(PSDK_PATH)/bios_6_83_02_07"
XDCTOOLS_PATH       ?="$(PSDK_PATH)/xdctools_3_61_04_40_core"
PDK_INSTALL_PATH    ?="$(PSDK_PATH)/pdk_jacinto_08_00_00_37/"
CONCERTO_ROOT       ?=$(PSDK_PATH)/vision_apps/concerto
IVISION_PATH        ?="$(PSDK_PATH)/ivision"

endif

TARGET_SOC                     ?= J721E
CORE                           ?= dsp
# Default C66
# Supported C64T, C64P, C64, C66, C674, C67, C67P, m4
TARGET_CPU                     ?= C66

#C7x Version (C7120/C7100)
#Compiles code for a particular C7x mode
TARGET_C7X_VERSION             ?= C7100

TARGET_BUILD                   ?= release
TARGET_PLATFORM                ?= TI_DEVICE
TIDL_BIOS_BUILD                ?= 1
TIDL_HOST_CCS                  ?= 1


