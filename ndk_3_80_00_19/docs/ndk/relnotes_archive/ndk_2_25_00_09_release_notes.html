<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head><!-- 
 *  Copyright (c) 2010-2016, Texas Instruments Incorporated
 *  All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions
 *  are met:
 *
 *  *  Redistributions of source code must retain the above copyright
 *     notice, this list of conditions and the following disclaimer.
 *
 *  *  Redistributions in binary form must reproduce the above copyright
 *     notice, this list of conditions and the following disclaimer in the
 *     documentation and/or other materials provided with the distribution.
 *
 *  *  Neither the name of Texas Instruments Incorporated nor the names of
 *     its contributors may be used to endorse or promote products derived
 *     from this software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 *  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 *  THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 *  PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 *  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 *  PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 *  OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 *  OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 *  EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
-->

    
    
    
  

  <title>Network Development Kit (NDK) 2.25.00.09 GA Release Notes</title><!-- For now, we use the doxygen style sheet --><link type="text/css" rel="stylesheet" href="docs/html/doxygen.css"><!-- doxygen's css .memproto's all have tables for a little extra pad...
         don't like tables, so give 'em a little extra pad ourselves
    --->

    
    
  <style type="text/css">
    .memproto {
        padding: 3;
    }
    </style></head>
<body>

<table width="100%">

  <tbody>

    <tr>

      <td bgcolor="black" width="1">
        <a href="http://www.ti.com">
          <img src="docs/html/tilogo.gif" alt="Texas Instruments" border="0">
        </a>
      </td>

      <td bgcolor="red">
        <img src="docs/html/titagline.gif" alt="Technology for Innovators(tm)">
      </td>

    </tr>

  
  </tbody>
</table>


<h1 align="center">NDK 2.25.00.09 GA Release Notes</h1>

<h2 align="center">February 05, 2016</h2>


<p>
This GA release of the NDK contains documentation updates, support
for graphical configuration pages, and major bug fixes.
&nbsp;This release is intended to support
SYS/BIOS 6.45.00.20 or greater. </p>

<p align="center">
<a href="#Introduction">Introduction</a>,
<a href="#Documentation">Documentation</a>,
<a href="#Whats_New">What's New</a>,
<a href="#Upgrade_Info">Upgrade Info</a>,
<a href="#Compatibility">Compatibility Information</a>,
<a href="#Device_Support">Device Support</a>,
<a href="#Validation">Validation Info</a>,
<a href="#Known_Issues">Known Issues</a>,
<a href="#Benchmarks">Benchmarks</a>,
<a href="#Examples">Examples</a>,
<a href="#Rebuilding_The_Core">Rebuilding The NDK Core Libraries</a>,
<a href="#Version">Version Information</a>,
<a href="#Support">Technical Support</a>.
</p>


<hr>

<h2><a name="Introduction">Introduction</a></h2>

<p>
The Network Developer's Kit (NDK) is a platform for development
and demonstration of network enabled applications on TI embedded
processors,
currently limited to the TMS320C6000 family and ARM processors.
The code included in this NDK release is generic C code which runs
on any C66, C674, ARM9, Cortex-A8, Cortex-A9, Cortex-A15, Cortex-M3,
or Cortex-M4 device (support for COFF targets is no longer shipped).
Users can obtain an appropriate NDK Support Package (NSP) for the various
supported
platforms separately. The NDK Support Packages include demonstration
software
showcasing capabilities across a range of network enabled
applications. In addition, the stack serves as a rapid prototype
platform
for the development of network and packet processing applications, or
to add
network connectivity to existing applications for communications,
configuration, and control. Using the components provided in the NDK,
developers can quickly move from development concepts to working
implementations attached to the network. Please check the
<a href="http://software-dl.ti.com/dsps/dsps_public_sw/sdo_sb/targetcontent/ndk/index.html">
release website</a>
for previous release notes and NDK Support Packages.
</p>


<hr>

<h2><a name="Documentation">Documentation</a></h2>

<p>
The following documents provide an overview of the NDK, the networking
programming API, and how to port the software to other platforms:
</p>

<ul>

  <li>
    <a href="docs/spru523j.pdf">NDK User's
    Guide</a>: Documents the NDK basics, and describes how to develop or
    port a network-enabled application. It also describes how to customize
    the network environment to fit your embedded environment. 
  </li>

  <li><a href="docs/spru524j.pdf">NDK Programmer's
    Reference Guide</a>: Describes the NDK library API calls in detail.
    It also includes a description of the stack's internal object based
    API functions. 
  </li>

  <li><a href="packages/ti/ndk/docs/stack/doxygen/index.html">NDK Microsoft HTML
    .chm API Guide</a>: Doxygen generated API documentation
    which includes IPv6 APIs.
  </li>

  <li><a href="docs/sprufp2b.pdf">NDK Support
    Package Ethernet Driver Design Guide</a>: 
    Describes the NIMU based architecture of the Ethernet and serial drivers
    packaged in the NSPs.</li>

  <li><a href="http://www.ti.com/lit/an/spraax4/spraax4.pdf" target="_blank">
    C6000 NDK Getting Started Guide</a>: Note that this older document contains
    an outdated set of lab exercises, relative to this latest NDK release.
    However, the document does provide an overview of networking and it is
    still a good reference guide in relation to gaining a better understanding
    of how the NDK works.  It highly recommended that the user use the guide for
    informational purposes only and not to attempt the lab steps.</li>

  <li><a href="docs/doxygen/html/index.html">SNTP API
    Documentation</a>: API guide describing the SNTP module and
    how it can be used.</li>

  <li><a href="packages/ti/ndk/reports/report.html" target="_blank">
    Static Analysis Reports</a></li>
</ul>


<p>
In addition, users are encouraged to monitor (and contribute to!) the
<a href="http://tiexpressdsp.com">TI Embedded Processors Wiki</a>.
</p>


<p>
Release notes from previous releases are also available in the <a href="docs/relnotes_archive">release notes archive</a> directory and the <a href="http://software-dl.ti.com/dsps/dsps_public_sw/sdo_sb/targetcontent/ndk/index.html">
release website.</a>
</p>


<hr>
<h2><a name="Whats_New">What's New</a></h2>


<h3>New Features</h3>

<ul>
  <font color="red">
  <li>The family member of all socket address structs has been changed to be type short (16-bit) instead of the previous type of char (8-bit). This is consistent with the BSD sockets implementation.</li>
  <ul>
    <li>This major change breaks binary compatibility with previous versions of the NDK!</li>
    <li>See the <a href="#Upgrade_Info">Upgrade Info</a> section for further details.</li>
  </ul>
  </font>
  <li>The SNTP client is being deprecated. See the <a href="#Upgrade_Info">Upgrade Info</a> section for further details.</li>
  <li>The MYTIME module has been removed. See the <a href="#Upgrade_Info">Upgrade Info</a> section for further details.</li>
  <li>NDK now built against a new version of SYS/BIOS and XDC tools (constitutes a major version dependency change).</li>
  <li>Removed build support for COFF target libraries.</li>
  <li>Removed empty header file unistd.h, as it is now provided by SYS/BIOS.  See the <a href="#Upgrade_Info">Upgrade Info</a> section for further details.</li>
  <li>TaskCreate now specifies an Error Block.</li>
  <li>Fixed a bug in the SNTP Client, in which another Task's socket could be closed by SNTP.</li>
  <li>Added additional APIs to the BSD layer which were previously missing.</li>
  <li>getaddrinfo now supports passing a NULL argument for the hints parameter.</li>
  <li>getaddrinfo now properly handles arguments of zero for the ai_protocol and ai_socktype hints (for TCP and UDP)</li>
  <li>Updated NDK User's Guide, API Guide, and Ethernet Driver Design Guide</li>
</ul>

<h3>Bug Fixes in NDK 2.25.00.09</h3>
  <ul>
  <li>SDOCM00119981 getaddrinfo should handle 0 values for ai_socktype and ai_protocol hints for TCP/UDP</li>
  <li>SDOCM00119825 add support for a NULL hints arg in getaddrinfo</li>
  <li>SDOCM00119781 NDK header files do not have C++ guards</li>
  <li>SDOCM00119587 in.h defines type but doesn't include proper header</li>
  <li>SDOCM00119582 unistd.h should move to BIOS</li>
  <li>SDOCM00118868 The Task_create example in SPRU523 should note the priority restriction</li>
  <li>SDOCM00118691 TaskCreate() should specify an Error_Block when calling Task_create()</li>
  <li>SDOCM00118042 SNTP client doesn't reset its socket handle, results in it closing another task's socket!</li>
  <li>SDOCM00117982 need to define standard macro INET6_ADDRSTRLEN and INET_ADDRSTRLEN</li>
  <li>SDOCM00117747 struct sockaddr has wrong type in definition</li>
  <li>SDOCM00115879 BSD layer missing a few socket APIs</li>
  <li>SDOCM00115023 IGMPJoinHostGroup and IGMPLeaveHostGroup APIs should be removed from NDK docs</li>
  <li>SDOCM00113941 NDK 2.24 documentation items</li>
  <li>SDOCM00113026 NDK API Guide Bugs (spru524i.pdf)</li>
  <li>SDOCM00112646 NDK docs missing ACD hook info</li>
  <li>SDOCM00093562 NDK DNS sample code in documentation is incorrect</li>
  </ul>

<h2><a name="Upgrade_Info">Upgrade Information</a></h2>

<p>
All the libraries in this release support either IPv4 or IPv6 (or both)
and NIMU. The low-level (LL) packet driver architecture has been
deprecated since NDK v2.0.0 release.
</p>

<p>The NDK stack in this release supports SYS/BIOS&nbsp;6.45.00.20 (or
greater) and is meant to be used with CCSv6.1 and up.
</p>


<p>
Network Support Packages (NSPs), which contains Ethernet drivers,  are released
independently of this product and are usually shipped as part of a sofware
development kit (SDK).  The NSPs for evmOMAPL138 and evm6748 platforms can be
obtained from the 
<a href="http://software-dl.ti.com/dsps/dsps_public_sw/sdo_sb/targetcontent/ndk/index.html">NDK website</a>.
Ethernet drivers for MCU devices can be found in the TI-RTOS product (see the 
<a href="http://software-dl.ti.com/dsps/dsps_public_sw/sdo_sb/targetcontent/tirtos/index.html">TI-RTOS download website</a>).
  For other devices, please contact your FAE for where to obtain the appropriate
SDK.
</p>

<font color="red">
<h3>Type change for "family" members of the socket address structures</h3>
<p>Starting in NDK 2.25, the socket address structures have been redefined to use a 16-bit type for the family members:</p>
<code>
  <ul>
    <li>sa_family</li>
    <li>sin_family</li>
    <li>sin6_family</li>
  </ul>
</code>
<p> Applications and/or libraries which contain references to these fields must be rebuilt due to this change!
</p>
<p>This was done in order to make the NDK conform to industry standards, in which the 16-bit type 'sa_family_t' is used for these family fields.</p>
<p>The NDK's socket address structures are now defined as follows:
<pre>
<code>
/* ******** "legacy" NDK layer: ******** */

struct sockaddr {
    <b>UINT16 sa_family; /* address family */</b>
    char sa_data[14]; /* socket data */
};

struct sockaddr_in {
    <b>UINT16  sin_family;         /* address family */</b>
    UINT16  sin_port;           /* port */
    struct  in_addr sin_addr;
    INT8    sin_zero[8];        /* fixed length address value */
};

struct sockaddr_in6 {
    <b>UINT16  sin6_family;            /* address family */</b>
    UINT16  sin6_port;              /* port */
    UINT32  sin6_flowinfo;          /* IPv6 flow information */
    struct  in6_addr sin6_addr;     /* IPv6 address */
    UINT32  sin6_scope_id;          /* scope id */
};

/* ******** BSD compatibility layer: ******** */

struct sockaddr {
    <b>sa_family_t sa_family; /* address family */</b>
    char sa_data[14]; /* socket data */
};

struct sockaddr_in {
    <b>sa_family_t    sin_family;       /* address family */</b>
    unsigned short sin_port;         /* port */
    struct         in_addr sin_addr;
    char           sin_zero[8];      /* fixed length address value */
};

struct sockaddr_in6 {
    <b>sa_family_t     sin6_family;    /* address family */</b>
    unsigned short  sin6_port;      /* port */
    unsigned int    sin6_flowinfo;  /* IPv6 flow information */
    struct in6_addr sin6_addr;      /* IPv6 address */
    unsigned int    sin6_scope_id;  /* scope id */
};
</code>
</pre>
</p>
</font>

<h3>The SNTP Client Is Now Contained In Its Own Library</h3>
<p>As of NDK 2.25, the SNTP client is still available in the NDK, however it is no longer part of the nettools library.  It has now been factored out of nettools and into its own library.  It can still be found in the nettools directory of the NDK:
  <ul>
    <li>ti/ndk/nettools/lib/sntp.lib (dual IPv4/IPv6 support)</li>
    <li>ti/ndk/nettools/lib/sntp_ipv4.lib (pure IPv4)</li>
  </ul>
(Note: the library extenstion ".lib" is used here, the actual file names will have an extension that matches the target type.)
Due to this change, it is necessary for users to manually update their project to link in the sntp library, as it will no longer be linked automatically.
</p>

<h3>The MYTIME module has been removed</h3>
<p>Starting in NDK 2.25, the MYTIME module is no longer supported.  User's must update their application code to use the SYS/BIOS Seconds module, as this is the replacement for MYTIME.</p>
<p>Please refer to the SNTP documentation for further details on using the Seconds module with the SNTP client, and the SYS/BIOS API guide for details on the Seconds module.</p>

<h3>unistd.h has been removed from the NDK BSD layer</h3>
<p>Starting in NDK 2.25, unistd.h is no longer provided in the NDK.  This file is now shipped in the SYS/BIOS product.  If your application includes unistd.h, you must update your compiler options to add the proper path location for this file.  It should typically be something like "&#60;SYS/BIOS install dir&#62/packages/ti/sysbios/posix".  It may also be necessary to update your *.cfg file to use the posix package's Settings.xdc module [i.e. xdc.useModule('ti.sysbios.posix.Settings');] in order to link in the posix libraries into your application.
</p>

<hr>
<h2><a name="Compatibility">Compatibility Information</a></h2>
<p>
This release must be used with&nbsp;following component versions (or higher):
</p>

<ul>
  <li>SYS/BIOS<span style="font-weight: bold;">&nbsp;6.45.00.20</span></li>
  <li>CCS&nbsp;<span style="font-weight: bold;">6.1</span></li>
  <li>XDCtools <b>3.32.00.06_core</b></li>
  <li>TI Code Generation Tools Versions</li>
  <ul type="square">

    <li>66xx: Code Generation Tools <b>7.2.0</b>
    </li>

    <li>674x+: Code Generation Tools&nbsp;<span style="font-weight: bold;">7.2.0</span>
    </li>

    <li>Arm9: Code Generation Tools&nbsp;<span style="font-weight: bold;">5.2.4</span>
    </li>

    <li>Cortex-A8: Code Generation Tools&nbsp;<b>5.2.4</b>
    </li>
  </ul>
  <li>GNU Code Generation Tools Versions (for ARM only)</li>
  <ul type="square">
    <li>Arm version &nbsp;<span style="font-weight: bold;">gcc-arm-none-eabi-4_8-2014q3</span> or later
    </li>
  </ul>
  <li>IAR Code Generation Tools Versions (for ARM Cortex-M only)</li>
  <ul type="square">
    <li>Arm version &nbsp;<span style="font-weight: bold;">6.70.1</span>
    </li>
  </ul>
</ul>




<hr>
<h2><a name="Device_Support">Device Support</a></h2>


<p>
This release supports the following devices:
</p>
<ul>
  <li>66xx: ELF format, big and little endian</li>
  <li>674x+: ELF format, little endian</li>
  <li>Arm9: ELF format</li>
  <li>Cortex-A8: ELF format and GCC</li>
  <li>Cortex-A9: GCC only</li>
  <li>Cortex-A15: GCC only</li>
  <li>Cortex-M3: ELF format, GCC and IAR</li>
  <li>Cortex-M4: ELF format, GCC and IAR</li>
</ul>

<hr>
<h2><a name="Validation">Validation</a></h2>

<p>
The following component versions were used to build and run test applications
to validiate this release:
</p>

<ul>
  <li>SYS/BIOS <span style="font-weight: bold;">6.45.00.20</span></li>
  <li>NSP <span style="font-weight: bold;">1.10.03.15</span> (evm6748)</li>
  <li>CCS <span style="font-weight: bold;">6.1</span></li>
  <li>TI-RTOS <span style="font-weight: bold;">2.15.00.17</span> (EK-TM4C1294XL)</li>

  <li>XDCtools&nbsp;<b>3.32.00.06_core</b></li>

  <li>Code Generation Tools <b>5.1.5 </b>(evm6748 ARM9 Application Build)</li>
  <li>Code Generation Tools <b>7.2.0 </b>(evm6748 C674X Application Build)</li>
  <li>Code Generation Tools <b>5.2.2 </b>(EK-TM4C1294XL Application Build)</li>
  <li>GNU Code Generation Tools <b> gcc-arm-none-eabi-4_8-2014q3 </b>(EK-TM4C1294XL Application Build)</li>
  <li>IAR Code Generation Tools <b>6.70.1 </b>(EK-TM4C1294XL Application Build)</li>
</ul>

<p>
This release was validated using the following hardware platforms:
</p>

<ul>
  <li>EVM OMAPL138 (ARM9 and C674)</li>
  <li>EK-TM4C1294XL</li>
</ul>

<hr>
<h2><a name="Known_Issues">Known Issues</a></h2>

<span style="text-decoration: underline;">GNU Applications Must Define _POSIX_SOURCE:</span>
<br><br>
Both the NDK and GNU define the fd_set structure.  Since SYS/BIOS ships GNU
header files, this conflict may arise when building NDK applications using GCC.

In order to avoid such conflicts, GCC apps need to throw -D_POSIX_SOURCE in
the application's compiler options in order to take the NDK's definition of
fd_set.
<br><br>
<span style="text-decoration: underline;">Terminated Tasks Must Be Automatically Deleted:</span>
<br><br>
The NDK has been updated to take advantage of the new SYS/BIOS feature
which automatically deletes dynamically created Task objects which have
reached the terminated state. &nbsp;If your application configuration
is loading the package 'ti.ndk.config' and/or using the
'ti.ndk.config.Global.xdc' module, then Task clean up will be set up
correctly for you.<br><br>However, if your application configuration is
not loading the 'ti.ndk.config' package or the
'ti.ndk.config.Global.xdc' module, then you must add the following
lines of code to your application configuration (*.cfg) file:<br><br><div style="margin-left: 40px;">var Task = xdc.useModule('ti.sysbios.knl.Task');<br>Task.deleteTerminatedTasks = true;<br></div><br>If not, you may experience out of memory issues due to improper Task clean up. &nbsp;Please refer to section&nbsp;<span style="font-weight: bold;">5.2.2 TaskCreate(), TaskExit(), and TaskDestroy()</span> of the <a href="docs/spru523j.pdf">NDK User's
    Guide</a> for more information.<br><br><span style="text-decoration: underline;">Known Bugs</span>:<ul>
<li>SDOCM00115751 - PPP does not work with Linux modem manager</li>
<li>SDOCM00115654 - HDLC and PPP shut down the stack if a bad packet received</li>
<li>SDOCM00098788 - ARM9 IPv6 telnet console echo application shows negative number for send rate</li>
<li>SDOCM00075040 - running ping command within a telnet session on the ndk causes the terminal and telnet session to hang</li>

  <li>SDSCM00025064 - DSP crash on Reboot with a PPPoE server on NDK.
      Workaround: Ensure that the PPPoE server of the NDK stack is closed
      before rebooting the stack.
  </li>

  <li>SDSCM00024506 - PPPoE client connection handle is not removed after
  PPPoE session timeout. Workaround: Ensure that the session timeout on the
  PPPoE server is configured to be a large value to avoid this issue.
  </li>
  
</ul>


<hr>
<h2><a name="Benchmarks">Benchmarks</a></h2>


<p>NDK sizing benchmarks were obtained from applications that,&nbsp;although basic,
demonstrate "real world" usage of the TCP/IP stack. &nbsp;The following
summarizes the size benchmark applications:</p>
<ul>

  <li><span style="font-weight: bold;">basic</span> - The base application; a
  bare minimum, ping-able NDK application.&nbsp; Contains no sockets, uses
  DHCP to obtain IP address.</li>

  <li><span style="font-weight: bold;">tcpSocket</span>
  - &nbsp;Minimized configuration for the TI-RTOS tcpEcho example that
  contains a single TCP socket running in a single task.&nbsp; The TCP
  send/receive buffers have been lowered to a value of 512 bytes. Uses
  DHCP to obtain IP address.</li>

  <li><span style="font-weight: bold;">tcpEchoServer</span>
  - Minimal configuration for application that creates a TCP echo daemon
  for the NDK library function "dtask_tcp_echo" (from
  ti/ndk/tools/servers).&nbsp; The program echoes data received back to
  its sender.&nbsp; Uses DHCP to obtain IP address.</li>

  <li><span style="font-weight: bold;">udpSocket</span>
  - &nbsp;Minimized configuration of the TI-RTOS udpEcho example that
  contains a single UDP socket running in a single task.&nbsp; The UDP
  receive buffer has been lowered to a value of 512 bytes. Uses
  DHCP to obtain IP address.</li>

  <li><span style="font-weight: bold;">udpEchoServer</span>
  - Minimal configuration for application that creates a UDP echo daemon
  for the NDK library function "dtask_udp_echo" (from
  ti/ndk/tools/servers).&nbsp; The program echoes data received back to
  its sender.&nbsp; Uses DHCP to obtain IP address.</li>

  <li><span style="font-weight: bold;">httpServer</span> - &nbsp;Minimal
  configuration for an HTTP server with a very basic web page and CGI
  script. Uses DHCP to obtain IP address.<br><br></li>
</ul>

<p>Please find the bench mark results here:</p>
<li><a href="packages/ti/ndk/benchmarks/sizing/doc-files/Results.html">NDK Size Benchmarks</a></li>
<p></p>


<hr>
<h2><a name="Examples">Examples</a></h2>


<p>NDK examples are no longer included as part of the NDK core release.  All
   examples are now located in a seperate Network Support Package (NSP).
</p>
<p>
   This NDK release was validated using the latest NSP 1.10.03.15 product for the
   evmOMAPL138 and evm6748 and so works best with the examples found in that
   product. Users who have the evmOMAPL138 or evm6748 hardware platforms should
   make sure to use the latest NSP product with this NDK release.
</p>
<p>
   The latest NSP product may be downloaded from the
   <a href="http://software-dl.ti.com/dsps/dsps_public_sw/sdo_sb/targetcontent/ndk/index.html">NDK website</a>
</p>
<p>
   For all other hardware platforms, the corresponding NSP products and
   examples may be found in the previous NDK 2.00 and NDK 2.01.00 products,
   which are also found at the 
   <a href="http://software-dl.ti.com/dsps/dsps_public_sw/sdo_sb/targetcontent/ndk/index.html">NDK website</a>.
</p>


<hr>
<h2><a name="Rebuilding_The_Core">Rebuilding The NDK Core Libraries</a></h2>

<p>The NDK product includes source files and build scripts that allow the user to modify its sources and rebuild its libraries.
   You can do this in order to modify, update, or add functionality. If you<br>edit
the NDK source code and/or corresponding build scripts, you must also
rebuild the NDK in order to create new libraries containing these
modifications.</p><p>The NDK ships with a make file which may be used to rebuild the NDK libraries after sources have been modified.
</p>

<p>Please refer to the following web page for instructions on how to rebuild
   the NDK: <a href="http://processors.wiki.ti.com/index.php/Rebuilding_The_NDK_Core_Using_Gmake">Rebuilding The NDK Core With Gmake</a>
</p>
<hr>
<h2><a name="Version">Version Information</a></h2>

<p>
This product's version follows a version format, <b>M.mm.pp.bb</b>,
where <b>M</b> is a single digit Major number, <b>mm</b> is 2 digit
minor number, <b>pp</b> is a 2 digit patch number, and <b>b</b> is an
unrestricted set of digits used as an incrementing build counter.
</p>


<p>
To support multiple side-by-side installations of the product, the
product version is encoded in the top level directory,
ex. <b>ndk_2_25_00_09</b>.
</p>


<p>
Subsequent releases of patch upgrades will be identified by the patch
number, ex. NDK 2.20.01 with directory <b>ndk_2_20_01</b>.
Typically, these patches only include critical bug fixes.
</p>


<hr>
<h2><a name="Support">Technical Support</a></h2>


<ul>

  <li>
  <a href="http://support.ti.com">Technical support resources</a>
  </li>

  <li>
  Forum-based technical support is available through the TI E2E Community in
  the <a href="http://e2e.ti.com/support/embedded/f/355.aspx">TI-RTOS forum</a>
  </li>

  <li>
  <a href="http://processors.wiki.ti.com/index.php/Category:NDK">Wiki-based documentation</a>
  </li>
  <li>
  <a href="http://focus.ti.com/general/docs/techdocsabstract.tsp?abstractName=spraax4">Getting
  Started with the C6000 Network Developer's Kit (NDK)</a>
  </li>

</ul>

<p></p>


<p>
Check
the <a href="http://software-dl.ti.com/dsps/dsps_public_sw/sdo_sb/targetcontent/ndk/index.html">NDK
    website</a> for updates.
</p>


<hr>
<p>
Last updated: February 05, 2016 Build Ver: ndk_2_25_00_09 Rev: f09
</p>

</body></html>
</body>
</html>
