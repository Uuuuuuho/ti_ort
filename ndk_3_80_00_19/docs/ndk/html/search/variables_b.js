var searchData=
[
  ['s_5faddr',['s_addr',['../structin__addr.html#a5abe94a260a50619a60ce111a59f00b5',1,'in_addr']]],
  ['sa_5fdata',['sa_data',['../structsockaddr.html#afd5d95d56d0f8959f5b56458b3b3c714',1,'sockaddr']]],
  ['sa_5ffamily',['sa_family',['../structsockaddr.html#a24dbd3e20c62f767f18ddaec18ed3b63',1,'sockaddr']]],
  ['sin6_5faddr',['sin6_addr',['../structsockaddr__in6.html#a219e7f3ecd6d7dcf8fc2465475be490f',1,'sockaddr_in6']]],
  ['sin6_5ffamily',['sin6_family',['../structsockaddr__in6.html#ac529d8e83c6d57c56d1cb7ddf61b0e8b',1,'sockaddr_in6']]],
  ['sin6_5fflowinfo',['sin6_flowinfo',['../structsockaddr__in6.html#a4faf8571dc08f1ed214acfe06941954e',1,'sockaddr_in6']]],
  ['sin6_5fport',['sin6_port',['../structsockaddr__in6.html#a4fc2b7a478d258e9e778772701096022',1,'sockaddr_in6']]],
  ['sin6_5fscope_5fid',['sin6_scope_id',['../structsockaddr__in6.html#a90915190af45ba4f62e57ea662840f39',1,'sockaddr_in6']]],
  ['sin_5faddr',['sin_addr',['../structsockaddr__in.html#a4ea5f2f1138e5c8597097db255a9ec6c',1,'sockaddr_in']]],
  ['sin_5ffamily',['sin_family',['../structsockaddr__in.html#a1b187e5b6abc36bcf03efe678448ed75',1,'sockaddr_in']]],
  ['sin_5fport',['sin_port',['../structsockaddr__in.html#a3cf9239fdd8bd32855d66a4b86349fbb',1,'sockaddr_in']]],
  ['sin_5fzero',['sin_zero',['../structsockaddr__in.html#a0254bfac1c18e922cb6638c3f6139e09',1,'sockaddr_in']]],
  ['status',['Status',['../struct__ci__srvargs.html#a079e7f87aa732663a64c21505477475a',1,'_ci_srvargs::Status()'],['../struct__ci__client.html#ac23e50702627359714f889432282fc52',1,'_ci_client::Status()']]]
];
