var searchData=
[
  ['cis_5fflg_5fcallbyip',['CIS_FLG_CALLBYIP',['../netcfg_8h.html#a832cfff97c21bf4e48376e324bcc68e0',1,'netcfg.h']]],
  ['cis_5fflg_5fifidxvalid',['CIS_FLG_IFIDXVALID',['../netcfg_8h.html#a83adce088b7fa79f4537d7c5692ef4b0',1,'netcfg.h']]],
  ['cis_5fflg_5fresolveip',['CIS_FLG_RESOLVEIP',['../netcfg_8h.html#a4f06a4e39f2aa7fd6d74ebf62ed94b9b',1,'netcfg.h']]],
  ['cis_5fflg_5frestartipterm',['CIS_FLG_RESTARTIPTERM',['../netcfg_8h.html#a21fee40edf19b5c4fb7195b21fd0671a',1,'netcfg.h']]],
  ['cis_5fsrv_5fstatus_5fdisabled',['CIS_SRV_STATUS_DISABLED',['../netcfg_8h.html#abce40cd166813846ebe41c944e91637b',1,'netcfg.h']]],
  ['cis_5fsrv_5fstatus_5fenabled',['CIS_SRV_STATUS_ENABLED',['../netcfg_8h.html#aed131b75fed39c937a1f7dfbdd015f24',1,'netcfg.h']]],
  ['cis_5fsrv_5fstatus_5ffailed',['CIS_SRV_STATUS_FAILED',['../netcfg_8h.html#a4353ca7664d569ac463f57fb1ece39f9',1,'netcfg.h']]],
  ['cis_5fsrv_5fstatus_5fipterm',['CIS_SRV_STATUS_IPTERM',['../netcfg_8h.html#aafcf480fea86aabc7f1c26329d77f5cf',1,'netcfg.h']]],
  ['cis_5fsrv_5fstatus_5fwait',['CIS_SRV_STATUS_WAIT',['../netcfg_8h.html#a58e12048e431ca9497ed26bd6a1d7cf4',1,'netcfg.h']]]
];
