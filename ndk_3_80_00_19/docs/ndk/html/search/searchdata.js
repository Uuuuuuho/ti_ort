var indexSectionsWithContent =
{
  0: "_acdefhilmnprstu",
  1: "_ilns",
  2: "cdnst",
  3: "cfmnt",
  4: "cdefhilmnprstu",
  5: "cfnst",
  6: "_acfimnps",
  7: "cdnt",
  8: "n"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "defines",
  7: "groups",
  8: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Data Structures",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Macros",
  7: "Modules",
  8: "Pages"
};

