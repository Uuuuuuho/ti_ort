/*
 * Copyright (c) 2020, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * */
/*
 * ======== ndkhookslib.c ========
 *
 * This file handles the management and creation of NDK hook functions
 */
#include <stkmain.h>

static NDK_HOOK_NODE* ndkHooks[NDK_MAX_HOOKS];
static void *hooksSem = 0;

int NDK_registerHook(int32_t type, NDK_HookFxn fxn)
{
    NDK_HOOK_NODE* ptrHookNode;

    if((type < 0) || (type >= NDK_MAX_HOOKS) || (fxn == NULL))
    {
        return (-1);
    }

    ptrHookNode = mmAlloc(sizeof(NDK_HOOK_NODE));
    if (ptrHookNode == NULL)
    {
        return (-1);
    }
    ptrHookNode->fxn = fxn;

    NDK_hooksPend();

    /*
     * This type of hook can only have one hook fxn, so overwite this hook if
     * it exists
     */
    if((type == NDK_CREATE_SKT_CTX_HOOK) && ndkHooks[type])
    {
        mmFree(ndkHooks[type]);
        ndkHooks[type] = NULL;
    }

    list_add((NDK_LIST_NODE**)(&(ndkHooks[type])), (NDK_LIST_NODE*)ptrHookNode);

    NDK_hooksPost(hooksSem);

    return (0);
}

int NDK_unregisterHook(int32_t type, NDK_HookFxn fxn)
{
    NDK_HOOK_NODE* ptrHookNode;

    if((type < 0) || (type >= NDK_MAX_HOOKS) || (fxn == NULL))
    {
        return (-1);
    }

    NDK_hooksPend();

    ptrHookNode = (NDK_HOOK_NODE *)list_get_head(
                                        (NDK_LIST_NODE**)(&(ndkHooks[type])));
    while(ptrHookNode != NULL)
    {
        if(ptrHookNode->fxn == fxn)
        {
            list_remove_node((NDK_LIST_NODE**)(&(ndkHooks[type])),
                        (NDK_LIST_NODE*)ptrHookNode);
            mmFree(ptrHookNode);

            NDK_hooksPost(hooksSem);

            return (0);
        }

        ptrHookNode = (NDK_HOOK_NODE *)list_get_next(
                                            (NDK_LIST_NODE*)ptrHookNode);
    }

    NDK_hooksPost();

    /* If we got here we couldn't find the fxn in the linked list */
    return (-1);
}

NDK_HOOK_NODE* NDK_getHook(int32_t type)
{
    if((type < 0) || (type >= NDK_MAX_HOOKS))
    {
        return (NULL);
    }
    else
    {
        return (ndkHooks[type]);
    }
}

void NDK_hooksPend()
{
    SemPendBinary(hooksSem, SEM_FOREVER);
}

void NDK_hooksPost()
{
    SemPostBinary(hooksSem);
}

void NDK_initHooksArray()
{
    memset(ndkHooks, 0, sizeof(ndkHooks));
}

void NDK_initHooksSem()
{
    if(hooksSem == 0)
    {
        hooksSem = SemCreateBinary(1);
    }
}

void NDK_deinitHooks()
{
    SemDeleteBinary(hooksSem);
    hooksSem = 0;
}
