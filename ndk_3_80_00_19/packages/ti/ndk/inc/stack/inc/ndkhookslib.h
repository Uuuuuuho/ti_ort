/*
 * Copyright (c) 2020, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * */
/*
 * ======== ndkhookslib.h ========
 *
 *  Contains structures and exported function that are used by the NDK Hooks
 *  library
 *
 */

#include "listlib.h"

#ifndef __NDKHOOKSLIB_H__
#define __NDKHOOKSLIB_H__

#ifdef __cplusplus
extern "C" {
#endif

#define NDK_MAX_HOOKS 5

/* Hook types */
#define NDK_CREATE_SKT_CTX_HOOK 0
#define NDK_CLOSE_SKT_CTX_HOOK 1
#define NDK_NETSTART_ERROR_HOOK 2
#define NDK_IPADDR_HOOK 3
#define NDK_LINK_STATUS_HOOK 4

/* Function pointer definition for ndk hook functions */
typedef int (*NDK_HookFxn) (uintptr_t);

/* Struct defintion used as node in a linked list of ndk hook functions */
typedef struct NDK_HOOK_NODE
{
    NDK_LIST_NODE links;
    NDK_HookFxn fxn;
} NDK_HOOK_NODE;

/* Error struct to be used with the NETSTART_ERROR_HOOK */
typedef struct _NetStartError {
    int error;
} NetStartError_Obj;

/* Arg struct to be used with the IPADDR_HOOK */
typedef struct _IPAddrHookArgs {
    uint32_t IPAddr;
    uint32_t IfIdx;
    uint32_t fAdd;
} IPAddrHookArgs_Obj;

/* Arg struct to be used with the LINK_STATUS_HOOK */
typedef struct LinkStatusHookArgs {
    uint32_t linkUp;
} LinkStatusHookArgs_Obj;

/**
 *  @b Description
 *  @n
 *      Registers a NDK_HookFxn of a given type. For instance a
 *      NDK_CREATE_SKT_CTX_HOOK type will register the function inside SockNew.
 *      Most hook types will allow you to register multiple hooks by calling
 *      this function multiple times. Each new hook of the same type is placed
 *      on a linked list of hooks. The NDK_CREATE_SKT_CTX_HOOK type only allows
 *      for one hook to be registered. This hook will be overwritten if another
 *      fxn of the same type is added.
 *
 *  @param[in]  type
 *      NDK_HookFxn function type.
 *
 *  @param[in]  fxn
 *      The NDK_HookFxn to register.
 *
 *  @retval
 *      Success - 0
 *  @retval
 *      Error - -1
 */
extern int NDK_registerHook(int32_t type, NDK_HookFxn fxn);

/**
 *  @b Description
 *  @n
 *      Unregisters a NDK_HookFxn of a given type. If the NDK_HookFxn is not
 *      registered this function will return an error
 *
 *  @param[in]  type
 *      NDK_HookFxn function type.
 *
 *  @param[in]  fxn
 *      The NDK_HookFxn to register.
 *
 *  @retval
 *      Success - 0
 *  @retval
 *      Error - -1
 */
extern int NDK_unregisterHook(int32_t type, NDK_HookFxn fxn);

/**
 *  @b Description
 *  @n
 *      Return a pointer to the NDK_HOOK_NODE associated with the passed in
 *      hook type. This NDK_HOOK_NODE can be traversed as a linked list by
 *      utilizing the `NDK_LIST_NODE` API.
 *
 *  @param[in]  type
 *      NDK_HookFxn function type.
 *
 *  @param[in]  fxn
 *      The NDK_HookFxn to register.
 *
 *  @retval
 *      NDK_HOOK_NODE*
 */
extern NDK_HOOK_NODE* NDK_getHook(int32_t type);

/**
 *  @b Description
 *  @n
 *      Access the internal NDK Hooks semaphore and pend on it.
 */
extern void NDK_hooksPend();

/**
 *  @b Description
 *  @n
 *      Access the internal NDK Hooks semaphore and post to it.
 */
extern void NDK_hooksPost();

/**
 *  @b Description
 *  @n
 *      Initialize the array containing NDK hook nodes
 */
extern void NDK_initHooksArray();

/**
 *  @b Description
 *  @n
 *      Initialize the hooksSem semaphore used by register hook APIs
 */
extern void NDK_initHooksSem();

/**
 *  @b Description
 *  @n
 *      Resets the hooksSem semaphore to its initial value.
 */
extern void NDK_deinitHooks();

#ifdef __cplusplus
}
#endif /* extern "C" */

#endif /* __NDKHOOKSLIB_H__ */
