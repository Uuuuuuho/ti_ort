#
#  ======== ndk.mak ========
#

#
# Where to install/stage the packages
# Typically this would point to the devkit location
#
DESTDIR ?= <UNDEFINED>

prefix ?= /
docdir ?= /docs/ndk
packagesdir ?= /packages

# USER STEP: user must define below path to xdc installation
# Set up dependencies
XDC_INSTALL_DIR ?= C:/ti/xdctools_version
SYSBIOS_INSTALL_DIR ?= C:/ti/sysbios_version
NS_INSTALL_DIR ?= C:/ti/ns_version

# Optional: Set this to enable MBEDTLS-enabled SlNetIf libraries
# (not all devices support this yet)
MBEDTLS_INSTALL_DIR ?=

#
# Set location of various cgtools
# These variables can be set here or on the command line.
#
# USER STEP: user must define below paths to compilers
ti.targets.elf.C66 ?=
ti.targets.elf.C66_big_endian ?=
ti.targets.elf.C674 ?=

ti.targets.arm.elf.Arm9 ?=
ti.targets.arm.elf.M4 ?=
ti.targets.arm.elf.M4F ?=
ti.targets.arm.elf.R5F ?=
ti.targets.arm.elf.R5Ft ?=
ti.targets.arm.clang.M4F ?=
ti.targets.arm.clang.M4 ?=
ti.targets.arm.clang.M33F ?=
ti.targets.arm.clang.M33 ?=

gnu.targets.arm.M4 ?=
gnu.targets.arm.M4F ?=
gnu.targets.arm.A8F ?=
gnu.targets.arm.A9F ?=
gnu.targets.arm.A15F ?=
gnu.targets.arm.A53F ?=

iar.targets.arm.M4F ?=

# Set BUILDMINSTACK to 1 to only build base stack libraries (stk.a* and stk6.a*)
BUILDMINSTACK ?= 0

# Specify additional compiler options (can be set via cmd-line)
# ENABLE_JUMBO : If enabled, packet sizes larger than 1500 bytes are supported
# ENABLE_STRONG_CHECKING : If enabled, error checking is performed on all handles
ENABLE_JUMBO ?= 0
ENABLE_STRONG_CHECKING ?= 0

# Derive POSIX location from SYSBIOS
TIPOSIX_REPO ?= $(SYSBIOS_INSTALL_DIR)/packages

#
# Set XDCARGS to some of the variables above.  XDCARGS are passed
# to the XDC build engine... which will load ndk.bld... which will
# extract these variables and use them to determine what to build and which
# toolchains to use.
#
# Note that not all of these variables need to be set to something valid.
# Unfortunately, since these vars are unconditionally assigned, your build line
# will be longer and more noisy than necessary
#
# Some background is here:
#     http://rtsc.eclipse.org/docs-tip/Command_-_xdc#Environment_Variables
#
XDCARGS= \
    ti.targets.arm.elf.Arm9=\"$(ti.targets.arm.elf.Arm9)\" \
    ti.targets.arm.elf.M4=\"$(ti.targets.arm.elf.M4)\" \
    ti.targets.arm.elf.M4F=\"$(ti.targets.arm.elf.M4F)\" \
    ti.targets.arm.elf.R5F=\"$(ti.targets.arm.elf.R5F)\" \
    ti.targets.arm.elf.R5Ft=\"$(ti.targets.arm.elf.R5Ft)\" \
    ti.targets.arm.clang.M4F=\"$(ti.targets.arm.clang.M4F)\" \
    ti.targets.arm.clang.M4=\"$(ti.targets.arm.clang.M4)\" \
    ti.targets.arm.clang.M33F=\"$(ti.targets.arm.clang.M33F)\" \
    ti.targets.arm.clang.M33=\"$(ti.targets.arm.clang.M33)\" \
    ti.targets.elf.C66=\"$(ti.targets.elf.C66)\" \
    ti.targets.elf.C66_big_endian=\"$(ti.targets.elf.C66_big_endian)\" \
    ti.targets.elf.C674=\"$(ti.targets.elf.C674)\" \
    gnu.targets.arm.M4=\"$(gnu.targets.arm.M4)\" \
    gnu.targets.arm.M4F=\"$(gnu.targets.arm.M4F)\" \
    gnu.targets.arm.A8F=\"$(gnu.targets.arm.A8F)\" \
    gnu.targets.arm.A9F=\"$(gnu.targets.arm.A9F)\" \
    gnu.targets.arm.A15F=\"$(gnu.targets.arm.A15F)\" \
    gnu.targets.arm.A53F=\"$(gnu.targets.arm.A53F)\" \
    iar.targets.arm.M4F=\"$(iar.targets.arm.M4F)\" \
    BUILDMINSTACK=\"$(BUILDMINSTACK)\" \
    TIPOSIX_REPO=\"$(TIPOSIX_REPO)\" \
    MBEDTLS_INSTALL_DIR=$(MBEDTLS_INSTALL_DIR) \
    NS_INSTALL_DIR=\"$(NS_INSTALL_DIR)\" \
    ENABLE_JUMBO=\"$(ENABLE_JUMBO)\" \
    ENABLE_STRONG_CHECKING=\"$(ENABLE_STRONG_CHECKING)\"

#
# Set XDCPATH to contain necessary repositories.
#
XDCPATH = $(SYSBIOS_INSTALL_DIR)/packages;
export XDCPATH

#
# Set XDCOPTIONS.  Use -v for a verbose build.
#
#XDCOPTIONS=v
export XDCOPTIONS

j = 8
#
# Set XDC executable command
# Note that XDCBUILDCFG points to the ndk.bld file which uses
# the arguments specified by XDCARGS
#
XDC = $(XDC_INSTALL_DIR)/xdc -j $(j) XDCBUILDCFG=./ndk.bld

######################################################
## Shouldnt have to modify anything below this line ##
######################################################

all:
	@ echo building ndk packages ...
	@ $(XDC) XDCARGS="$(XDCARGS)" -Pr ./packages

debug:
	@ echo building ndk debug packages ...
	@ $(XDC) XDCARGS="profile=debug $(XDCARGS)" -Pr ./packages

clean:
	@ echo cleaning ndk packages ...
	@ $(XDC) XDCARGS="$(XDCARGS)" clean -Pr ./packages

install-packages:
	@ echo installing ndk packages to $(DESTDIR) ...
	@ mkdir -p $(DESTDIR)/$(prefix)/$(docdir)
	@ cp -rf $(wildcard ndk_*_ReleaseNotes.html) docs/* $(DESTDIR)/$(prefix)/$(docdir)
	@ mkdir -p $(DESTDIR)/$(prefix)/$(packagesdir)
	@ cp -rf packages/* $(DESTDIR)/$(prefix)/$(packagesdir)

help:
	@ echo "Make Goals"
	@ echo "    all     # build all libraries in release profile"
	@ echo ""
	@ echo "    debug   # build all libraries in debug profile"
	@ echo ""
	@ echo "    clean   # remove all generated content"
