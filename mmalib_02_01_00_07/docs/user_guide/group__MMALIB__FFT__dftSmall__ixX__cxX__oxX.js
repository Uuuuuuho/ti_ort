var group__MMALIB__FFT__dftSmall__ixX__cxX__oxX =
[
    [ "MMALIB_FFT_dftSmall_ixX_cxX_oxX_InitArgs", "structMMALIB__FFT__dftSmall__ixX__cxX__oxX__InitArgs.html", [
      [ "funcStyle", "structMMALIB__FFT__dftSmall__ixX__cxX__oxX__InitArgs.html#abefbdefdcc304c44d917bf78beb7dfe5", null ],
      [ "fftSize", "structMMALIB__FFT__dftSmall__ixX__cxX__oxX__InitArgs.html#a9a88c0451ef61c0372bb8c40bc11bf08", null ],
      [ "batchSize", "structMMALIB__FFT__dftSmall__ixX__cxX__oxX__InitArgs.html#aed4ffb358fa77fc68f7761b05cdc89fe", null ],
      [ "interleave", "structMMALIB__FFT__dftSmall__ixX__cxX__oxX__InitArgs.html#a90a8bf4a617cf52ef98ea3c83a5b449f", null ],
      [ "shiftVector", "structMMALIB__FFT__dftSmall__ixX__cxX__oxX__InitArgs.html#a99e0c664792f99cbee24619f0cdd3798", null ]
    ] ],
    [ "MMALIB_FFT_DFTSMALL_IXX_CXX_OXX_NUMSHIFTS", "group__MMALIB__FFT__dftSmall__ixX__cxX__oxX.html#ga5ed070b4e132a1d5000028ffde26d6a7", null ],
    [ "MMALIB_FFT_dftSmall_ixX_cxX_oxX_getHandleSize", "group__MMALIB__FFT__dftSmall__ixX__cxX__oxX.html#gaa942a5c7aa13031c67f1e995b4f77c08", null ],
    [ "MMALIB_FFT_dftSmall_ixX_cxX_oxX_getSizes", "group__MMALIB__FFT__dftSmall__ixX__cxX__oxX.html#gaee538079aa4516e705027bab631ea80f", null ],
    [ "MMALIB_FFT_dftSmall_ixX_cxX_oxX_twGen", "group__MMALIB__FFT__dftSmall__ixX__cxX__oxX.html#ga992fccb5f3d7904f968f1baff12ed54c", null ],
    [ "MMALIB_FFT_dftSmall_ixX_cxX_oxX_init", "group__MMALIB__FFT__dftSmall__ixX__cxX__oxX.html#gaff7b10ad10ee2e724cfd4f7554544f94", null ],
    [ "MMALIB_FFT_dftSmall_ixX_cxX_oxX_init_checkParams", "group__MMALIB__FFT__dftSmall__ixX__cxX__oxX.html#ga6332b588e2a40e44ee8b4eaed133a83d", null ],
    [ "MMALIB_FFT_dftSmall_ixX_cxX_oxX_exec", "group__MMALIB__FFT__dftSmall__ixX__cxX__oxX.html#gae20d9f5d295562dfd6c882f110cc5fe0", null ],
    [ "MMALIB_FFT_dftSmall_ixX_cxX_oxX_exec_checkParams", "group__MMALIB__FFT__dftSmall__ixX__cxX__oxX.html#ga54c2ea662eb83349cc76a8fd16a7f510", null ],
    [ "MMALIB_FFT_dftSmall_ixX_cxX_oxX_perfEst", "group__MMALIB__FFT__dftSmall__ixX__cxX__oxX.html#ga3b3aee1f4944cd624d169ab1ff9ab2f6", null ]
];