var group__MMALIB__CNN__fullyConnected__ixX__ixX__oxX =
[
    [ "MMALIB_CNN_fullyConnected_ixX_ixX_oxX_InitArgs", "structMMALIB__CNN__fullyConnected__ixX__ixX__oxX__InitArgs.html", [
      [ "funcStyle", "structMMALIB__CNN__fullyConnected__ixX__ixX__oxX__InitArgs.html#a3a4efdc6d8d44365b0c815adcdb28c52", null ],
      [ "shift", "structMMALIB__CNN__fullyConnected__ixX__ixX__oxX__InitArgs.html#a0da00afd11466e5fc2e27ae361ba235c", null ],
      [ "bias", "structMMALIB__CNN__fullyConnected__ixX__ixX__oxX__InitArgs.html#a6d6750a4594f44e697de73cef7e9cf2e", null ],
      [ "activationType", "structMMALIB__CNN__fullyConnected__ixX__ixX__oxX__InitArgs.html#a6ab6b1efbfa53520c9cccd70c48b2391", null ],
      [ "Ni", "structMMALIB__CNN__fullyConnected__ixX__ixX__oxX__InitArgs.html#a742fd86270ee80b779e4e61fb2918911", null ],
      [ "kDimRows", "structMMALIB__CNN__fullyConnected__ixX__ixX__oxX__InitArgs.html#a6e414d86e72b82b32ccf0af825161766", null ],
      [ "multiPassX", "structMMALIB__CNN__fullyConnected__ixX__ixX__oxX__InitArgs.html#ab1831b6d16b348d750b88165318bcb0f", null ],
      [ "multiPassH", "structMMALIB__CNN__fullyConnected__ixX__ixX__oxX__InitArgs.html#ab62b1dd20762c501aa5c92ee42a69196", null ],
      [ "bufCAccum_cn", "structMMALIB__CNN__fullyConnected__ixX__ixX__oxX__InitArgs.html#ac363ec12c6bb66185c872e0a6b25c6dc", null ]
    ] ],
    [ "MMALIB_CNN_fullyConnected_ixX_ixX_oxX_ExecArgs", "structMMALIB__CNN__fullyConnected__ixX__ixX__oxX__ExecArgs.html", [
      [ "multiPassX", "structMMALIB__CNN__fullyConnected__ixX__ixX__oxX__ExecArgs.html#a946f266c1c6f8436874fdea079369258", null ],
      [ "multiPassH", "structMMALIB__CNN__fullyConnected__ixX__ixX__oxX__ExecArgs.html#a3626463a6e8df02c20759088223dd2bd", null ],
      [ "writeCBuffer", "structMMALIB__CNN__fullyConnected__ixX__ixX__oxX__ExecArgs.html#ad40c58a71bde76aab985104c2521ce08", null ],
      [ "firstCall", "structMMALIB__CNN__fullyConnected__ixX__ixX__oxX__ExecArgs.html#a724cf215c5209dd7a9b4ee452bde39ac", null ],
      [ "lastCall", "structMMALIB__CNN__fullyConnected__ixX__ixX__oxX__ExecArgs.html#a2cf3da6d4f101e262b361a5014738fbd", null ],
      [ "blockOffsetA", "structMMALIB__CNN__fullyConnected__ixX__ixX__oxX__ExecArgs.html#a64fb67ca10746b16550becfba2ab64f6", null ]
    ] ],
    [ "MMALIB_CNN_FULLYCONNECTED_IXX_IXX_OXX_STATUS_NAME", "group__MMALIB__CNN__fullyConnected__ixX__ixX__oxX.html#ga66378d8a66a1266a4466a9bffbdabc5e", [
      [ "MMALIB_CNN_FULLYCONNECTED_IXX_IXX_OXX_ERR_SMALL_K", "group__MMALIB__CNN__fullyConnected__ixX__ixX__oxX.html#gga66378d8a66a1266a4466a9bffbdabc5eae800229dc161e59473d3cb4e3ca2da13", null ],
      [ "MMALIB_CNN_FULLYCONNECTED_IXX_IXX_OXX_ERR_MAX", "group__MMALIB__CNN__fullyConnected__ixX__ixX__oxX.html#gga66378d8a66a1266a4466a9bffbdabc5eac0b95ada26f0a286ec0bb53b78eb3815", null ]
    ] ],
    [ "MMALIB_CNN_fullyConnected_ixX_ixX_oxX_getHandleSize", "group__MMALIB__CNN__fullyConnected__ixX__ixX__oxX.html#ga7294efcad2502b8db02ec719c60e4b9c", null ],
    [ "MMALIB_CNN_fullyConnected_ixX_ixX_oxX_init", "group__MMALIB__CNN__fullyConnected__ixX__ixX__oxX.html#ga98668aef03cb5e2faca13ed817d21867", null ],
    [ "MMALIB_CNN_fullyConnected_ixX_ixX_oxX_init_checkParams", "group__MMALIB__CNN__fullyConnected__ixX__ixX__oxX.html#ga72be64c691e7292726d8b3447742162b", null ],
    [ "MMALIB_CNN_fullyConnected_ixX_ixX_oxX_exec", "group__MMALIB__CNN__fullyConnected__ixX__ixX__oxX.html#ga0bbb44bc01afee3f996157b8b63bc1d9", null ],
    [ "MMALIB_CNN_fullyConnected_ixX_ixX_oxX_exec_checkParams", "group__MMALIB__CNN__fullyConnected__ixX__ixX__oxX.html#ga9befb2d44a3ac094a296d8b0f6bc84b1", null ],
    [ "MMALIB_CNN_generateFillBiasPredicateRegisters", "group__MMALIB__CNN__fullyConnected__ixX__ixX__oxX.html#gabe981ca8a8bbcb4abd6df0cc0114cb7c", null ],
    [ "MMALIB_CNN_fullyConnected_ixX_ixX_oxX_reorderWeights", "group__MMALIB__CNN__fullyConnected__ixX__ixX__oxX.html#ga1e6c56bf229c8ce0627bc37c4347a1a2", null ],
    [ "MMALIB_CNN_fullyConnected_ixX_ixX_oxX_perfEst", "group__MMALIB__CNN__fullyConnected__ixX__ixX__oxX.html#ga354bab677faec1ff6a78d5c20bb4a7ea", null ]
];