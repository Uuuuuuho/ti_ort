var group__MMALIB__FFT__fft__ixX__cxX__oxX =
[
    [ "MMALIB_FFT_fft_ixX_cxX_oxX_InitArgs", "structMMALIB__FFT__fft__ixX__cxX__oxX__InitArgs.html", [
      [ "funcStyle", "structMMALIB__FFT__fft__ixX__cxX__oxX__InitArgs.html#a9759cadb0af8ff1f08db7d1a4e9ab520", null ],
      [ "fftSize", "structMMALIB__FFT__fft__ixX__cxX__oxX__InitArgs.html#a0bc7f04ede61bc3647b026b25cf3bdef", null ],
      [ "batchSize", "structMMALIB__FFT__fft__ixX__cxX__oxX__InitArgs.html#aef79836e8ae0dcc2e7a1e11faa15da64", null ],
      [ "interleave", "structMMALIB__FFT__fft__ixX__cxX__oxX__InitArgs.html#a63665f9b3174bd56aae76c9ca781554d", null ],
      [ "shiftVector", "structMMALIB__FFT__fft__ixX__cxX__oxX__InitArgs.html#a7840f9c87cf272c4b056ac79b1ac4e04", null ]
    ] ],
    [ "MMALIB_FFT_FFT_IXX_CXX_OXX_NUMSHIFTS", "group__MMALIB__FFT__fft__ixX__cxX__oxX.html#ga8fb0c8ce13774838e9391b93fb79a5b8", null ],
    [ "MMALIB_FFT_fft_ixX_cxX_oxX_getHandleSize", "group__MMALIB__FFT__fft__ixX__cxX__oxX.html#gac76440edc88680168678dbc965002614", null ],
    [ "MMALIB_FFT_fft_ixX_cxX_oxX_getSizes", "group__MMALIB__FFT__fft__ixX__cxX__oxX.html#gafe5b05fe5e5baac95eb0a5576116f7a0", null ],
    [ "MMALIB_FFT_fft_ixX_cxX_oxX_twGen", "group__MMALIB__FFT__fft__ixX__cxX__oxX.html#ga004d8cd9f8c809e14775c16ac25c2f10", null ],
    [ "MMALIB_FFT_fft_ixX_cxX_oxX_init", "group__MMALIB__FFT__fft__ixX__cxX__oxX.html#ga6af60d9c75519ac9b594f4c8d0a71699", null ],
    [ "MMALIB_FFT_fft_ixX_cxX_oxX_init_checkParams", "group__MMALIB__FFT__fft__ixX__cxX__oxX.html#gaf864fb5aafaf6adb8efc5cd390bf3c0b", null ],
    [ "MMALIB_FFT_fft_ixX_cxX_oxX_exec", "group__MMALIB__FFT__fft__ixX__cxX__oxX.html#gac19dc51fc0b28083275e1432e2afbea3", null ],
    [ "MMALIB_FFT_fft_ixX_cxX_oxX_exec_checkParams", "group__MMALIB__FFT__fft__ixX__cxX__oxX.html#gacb937bae6fbab772746342fec1361482", null ]
];