var group__MMALIB__CNN__pixelShuffle__row__ixX__ixX__oxX =
[
    [ "MMALIB_CNN_pixelShuffle_row_ixX_ixX_oxX_InitArgs", "structMMALIB__CNN__pixelShuffle__row__ixX__ixX__oxX__InitArgs.html", [
      [ "funcStyle", "structMMALIB__CNN__pixelShuffle__row__ixX__ixX__oxX__InitArgs.html#a0d373c2d2f377de6826e85c690e426cd", null ],
      [ "No", "structMMALIB__CNN__pixelShuffle__row__ixX__ixX__oxX__InitArgs.html#acef121b8713682950d03586d57078088", null ],
      [ "inChOffset", "structMMALIB__CNN__pixelShuffle__row__ixX__ixX__oxX__InitArgs.html#aace7f7c48c83a67f4420829a1f7a7736", null ],
      [ "validColsIn", "structMMALIB__CNN__pixelShuffle__row__ixX__ixX__oxX__InitArgs.html#afa41362f6bb793a547cd8a50770fa539", null ],
      [ "validColsPerRowIn", "structMMALIB__CNN__pixelShuffle__row__ixX__ixX__oxX__InitArgs.html#a3d30b98d8b8bd4ae8b0913ec2a543a9f", null ],
      [ "validRowsIn", "structMMALIB__CNN__pixelShuffle__row__ixX__ixX__oxX__InitArgs.html#ab939b3ba6cf16282a05b3b3c9287091a", null ],
      [ "inputPitchPerRow", "structMMALIB__CNN__pixelShuffle__row__ixX__ixX__oxX__InitArgs.html#a91827e34622579647cb4f68a71510855", null ],
      [ "outputPitchPerRow", "structMMALIB__CNN__pixelShuffle__row__ixX__ixX__oxX__InitArgs.html#ab89278c3d2c8f8bd0dc5e2e374a1ea1c", null ],
      [ "inWidth", "structMMALIB__CNN__pixelShuffle__row__ixX__ixX__oxX__InitArgs.html#a7db6a1896d5f271b3272810983be97ce", null ],
      [ "pad", "structMMALIB__CNN__pixelShuffle__row__ixX__ixX__oxX__InitArgs.html#aa25b63bb7e85a4aaf8c35fb8fba895da", null ],
      [ "maxHeight", "structMMALIB__CNN__pixelShuffle__row__ixX__ixX__oxX__InitArgs.html#a21e4a5b704f71e781a23c78f51132141", null ],
      [ "subMChannels", "structMMALIB__CNN__pixelShuffle__row__ixX__ixX__oxX__InitArgs.html#aa5f2045b15bbeb8f648926c2346b856b", null ],
      [ "shift", "structMMALIB__CNN__pixelShuffle__row__ixX__ixX__oxX__InitArgs.html#a5586a38375fe809a488d562c4c48e3ec", null ],
      [ "Fr", "structMMALIB__CNN__pixelShuffle__row__ixX__ixX__oxX__InitArgs.html#adc743f292cee6ae5eb722239c8e359a4", null ],
      [ "Fc", "structMMALIB__CNN__pixelShuffle__row__ixX__ixX__oxX__InitArgs.html#adbe622fc898c6e4f530179f037134346", null ],
      [ "strideX", "structMMALIB__CNN__pixelShuffle__row__ixX__ixX__oxX__InitArgs.html#ac13f54caa832f7802f8259f54251c617", null ],
      [ "strideY", "structMMALIB__CNN__pixelShuffle__row__ixX__ixX__oxX__InitArgs.html#ac62797170f05d9b65fd39ec3aa4af35c", null ],
      [ "dilationX", "structMMALIB__CNN__pixelShuffle__row__ixX__ixX__oxX__InitArgs.html#a039c449f658f461fd58ba0d4b61bc027", null ],
      [ "dilationY", "structMMALIB__CNN__pixelShuffle__row__ixX__ixX__oxX__InitArgs.html#aa5c0961b8e747d3008bf94e7181c26db", null ],
      [ "upscaleFactor", "structMMALIB__CNN__pixelShuffle__row__ixX__ixX__oxX__InitArgs.html#ad649d802a6281c5d57defc88395d8814", null ],
      [ "bias", "structMMALIB__CNN__pixelShuffle__row__ixX__ixX__oxX__InitArgs.html#a0e6fb6f7f1ef2ba6da076fa63e3ab80f", null ],
      [ "activationType", "structMMALIB__CNN__pixelShuffle__row__ixX__ixX__oxX__InitArgs.html#a5c3decba3293c59d03a2c8b1d6fafc94", null ],
      [ "mode", "structMMALIB__CNN__pixelShuffle__row__ixX__ixX__oxX__InitArgs.html#a435738bbaceff213289b80395cc143fd", null ]
    ] ],
    [ "MMALIB_CNN_pixelShuffle_row_ixX_ixX_oxX_ExecInArgs", "structMMALIB__CNN__pixelShuffle__row__ixX__ixX__oxX__ExecInArgs.html", [
      [ "validColsIn", "structMMALIB__CNN__pixelShuffle__row__ixX__ixX__oxX__ExecInArgs.html#ae61c6121d78b9d3ffee20940fe0cc4b9", null ],
      [ "validColsPerRowIn", "structMMALIB__CNN__pixelShuffle__row__ixX__ixX__oxX__ExecInArgs.html#a47bd7e7e30e4b577a8a529af6d3c7712", null ],
      [ "validRowsIn", "structMMALIB__CNN__pixelShuffle__row__ixX__ixX__oxX__ExecInArgs.html#a31bd9ffa652766d4d99d5550c875e5f3", null ],
      [ "pad", "structMMALIB__CNN__pixelShuffle__row__ixX__ixX__oxX__ExecInArgs.html#af41359d5b241ee3a6a4c5e6dfd0cfbfa", null ],
      [ "subMChannels", "structMMALIB__CNN__pixelShuffle__row__ixX__ixX__oxX__ExecInArgs.html#adae89f47734e3ecaddd68037cd5a7221", null ]
    ] ],
    [ "MMALIB_CNN_pixelShuffle_row_ixX_ixX_oxX_ExecOutArgs", "structMMALIB__CNN__pixelShuffle__row__ixX__ixX__oxX__ExecOutArgs.html", [
      [ "validColsOut", "structMMALIB__CNN__pixelShuffle__row__ixX__ixX__oxX__ExecOutArgs.html#ad9d5df7a04adcb45996831eeeba107bd", null ],
      [ "validColsPerRowOut", "structMMALIB__CNN__pixelShuffle__row__ixX__ixX__oxX__ExecOutArgs.html#a017d9ffda3663985ab125dc4ca4136a2", null ],
      [ "validRowsOut", "structMMALIB__CNN__pixelShuffle__row__ixX__ixX__oxX__ExecOutArgs.html#ae601aa7c428ba6e7a85d211020bd21ca", null ]
    ] ],
    [ "MMALIB_CNN_PIXELSHUFFLE_ROW_IXX_IXX_OXX_STATUS_NAME", "group__MMALIB__CNN__pixelShuffle__row__ixX__ixX__oxX.html#ga18b9024a3cfe252a8aa6b07959d2f2e8", [
      [ "MMALIB_CNN_PIXELSHUFFLE_ROW_IXX_IXX_OXX_ERR_SMALL_K", "group__MMALIB__CNN__pixelShuffle__row__ixX__ixX__oxX.html#gga18b9024a3cfe252a8aa6b07959d2f2e8ae66f03d97e465b706c78e5140cd57ffb", null ],
      [ "MMALIB_CNN_PIXELSHUFFLE_ROW_IXX_IXX_OXX_ERR_MAX", "group__MMALIB__CNN__pixelShuffle__row__ixX__ixX__oxX.html#gga18b9024a3cfe252a8aa6b07959d2f2e8adc2c448177653e7d920a9b851ea14f50", null ]
    ] ],
    [ "MMALIB_CNN_pixelShuffle_row_ixX_ixX_oxX_getHandleSize", "group__MMALIB__CNN__pixelShuffle__row__ixX__ixX__oxX.html#ga815d56f27575399e092ed9e465f0d4e4", null ],
    [ "MMALIB_CNN_pixelShuffle_row_ixX_ixX_oxX_init", "group__MMALIB__CNN__pixelShuffle__row__ixX__ixX__oxX.html#ga02d6f9b573db493c907a18c835ca654a", null ],
    [ "MMALIB_CNN_pixelShuffle_row_ixX_ixX_oxX_init_checkParams", "group__MMALIB__CNN__pixelShuffle__row__ixX__ixX__oxX.html#ga605dc245c565c23649373f1ca3566c3b", null ],
    [ "MMALIB_CNN_pixelShuffle_row_ixX_ixX_oxX_exec", "group__MMALIB__CNN__pixelShuffle__row__ixX__ixX__oxX.html#ga97f9a27f4910ca66da91cb8e5329eaec", null ],
    [ "MMALIB_CNN_pixelShuffle_row_ixX_ixX_oxX_exec_checkParams", "group__MMALIB__CNN__pixelShuffle__row__ixX__ixX__oxX.html#ga4964c7c848709d8e5865ee42ea919fe7", null ],
    [ "MMALIB_CNN_generateFillSeamPredicateRegistersPixelShuffle", "group__MMALIB__CNN__pixelShuffle__row__ixX__ixX__oxX.html#ga3955c18a9e8325baf8946804d9135777", null ],
    [ "MMALIB_CNN_seamPredicateRegistersSizePixelShuffle", "group__MMALIB__CNN__pixelShuffle__row__ixX__ixX__oxX.html#ga79b664e0e83cb2d2db1dba094c63ebbe", null ],
    [ "MMALIB_CNN_seamPredicateRegistersSizeDefaultPixelShuffle", "group__MMALIB__CNN__pixelShuffle__row__ixX__ixX__oxX.html#ga4731838f563d6fa2f72e62db0b81fd8e", null ],
    [ "MMA_CNNLIB_pixelShuffle_rowBiasReLUCompute_ixX_ixX_oxX_perfEst", "group__MMALIB__CNN__pixelShuffle__row__ixX__ixX__oxX.html#gaf4619e4235f0cb0d903dac3f0cde4f3c", null ]
];