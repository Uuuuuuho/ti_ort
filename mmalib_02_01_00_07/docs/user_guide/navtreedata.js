var NAVTREE =
[
  [ "MMALIB User Guide", "index.html", [
    [ "MMALIB", "index.html", null ],
    [ "Modules", "modules.html", "modules" ],
    [ "MMALIB Build Instructions", "BUILD_INSTRUCTIONS.html", [
      [ "Build instructions for MMALIB", "BUILD_INSTRUCTIONS.html#build_instructions", null ]
    ] ],
    [ "TI Disclaimer", "TI_DISCLAIMER.html", null ]
  ] ]
];

var NAVTREEINDEX =
[
"BUILD_INSTRUCTIONS.html",
"group__MMALIB__FFT__dftLarge__ixX__cxX__oxX.html#gaa916d444da69f8f28508c94c17740399",
"structMMALIB__DSP__firSmall__ixX__ixX__oxX__InitArgs.html#a858f37563bd459aa2b081842ccfbd681"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';