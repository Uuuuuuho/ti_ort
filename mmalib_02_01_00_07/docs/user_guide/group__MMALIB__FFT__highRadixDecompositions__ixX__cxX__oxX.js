var group__MMALIB__FFT__highRadixDecompositions__ixX__cxX__oxX =
[
    [ "MMALIB_FFT_highRadixDecompositions_ixX_cxX_oxX_InitArgs", "structMMALIB__FFT__highRadixDecompositions__ixX__cxX__oxX__InitArgs.html", [
      [ "funcStyle", "structMMALIB__FFT__highRadixDecompositions__ixX__cxX__oxX__InitArgs.html#af588b757e03a66d0473f2c8e11001cd6", null ],
      [ "fftSize", "structMMALIB__FFT__highRadixDecompositions__ixX__cxX__oxX__InitArgs.html#aec0ce4a4e81deff11f2c327ab05090f7", null ],
      [ "batchSize", "structMMALIB__FFT__highRadixDecompositions__ixX__cxX__oxX__InitArgs.html#a803414550d2b12b6bbe972f6c2fd9ab1", null ],
      [ "interleave", "structMMALIB__FFT__highRadixDecompositions__ixX__cxX__oxX__InitArgs.html#a528665940d1b570418860bc7c08083c3", null ],
      [ "shiftVector", "structMMALIB__FFT__highRadixDecompositions__ixX__cxX__oxX__InitArgs.html#a1463c9c9b3d6c0dc5c78588a9dec6572", null ]
    ] ],
    [ "MMALIB_FFT_HIGHRADIXDECOMPOSITIONS_IXX_CXX_OXX_NUMSHIFTS", "group__MMALIB__FFT__highRadixDecompositions__ixX__cxX__oxX.html#ga17484e6180ee1202477a2fc53b346a83", null ],
    [ "MMALIB_FFT_highRadixDecompositions_ixX_cxX_oxX_getHandleSize", "group__MMALIB__FFT__highRadixDecompositions__ixX__cxX__oxX.html#ga14d3ce02f3354cfea8fadb1d8e350bfa", null ],
    [ "MMALIB_FFT_highRadixDecompositions_ixX_cxX_oxX_getSizes", "group__MMALIB__FFT__highRadixDecompositions__ixX__cxX__oxX.html#ga444749fa0e724855c2c1a8ceb919135d", null ],
    [ "MMALIB_FFT_highRadixDecompositions_ixX_cxX_oxX_twGen", "group__MMALIB__FFT__highRadixDecompositions__ixX__cxX__oxX.html#ga4bd07575f2beef0b0052679ae974a5d3", null ],
    [ "MMALIB_FFT_highRadixDecompositions_ixX_cxX_oxX_init", "group__MMALIB__FFT__highRadixDecompositions__ixX__cxX__oxX.html#gab6e45c9bed8a0c4b0f55c09671e0f024", null ],
    [ "MMALIB_FFT_highRadixDecompositions_ixX_cxX_oxX_init_checkParams", "group__MMALIB__FFT__highRadixDecompositions__ixX__cxX__oxX.html#ga7ba8d18dd97ccf9170102acbf1e34ea2", null ],
    [ "MMALIB_FFT_highRadixDecompositions_ixX_cxX_oxX_exec", "group__MMALIB__FFT__highRadixDecompositions__ixX__cxX__oxX.html#ga27c5ec49a82a86c15b0e571619289409", null ],
    [ "MMALIB_FFT_highRadixDecompositions_ixX_cxX_oxX_exec_checkParams", "group__MMALIB__FFT__highRadixDecompositions__ixX__cxX__oxX.html#ga01720d177c4f14795b7033dc9073fab1", null ],
    [ "MMALIB_FFT_highRadixDecompositions_ixX_cxX_oxX_perfEst", "group__MMALIB__FFT__highRadixDecompositions__ixX__cxX__oxX.html#gae79f1674ddc7f36aa364fd9054f91aee", null ],
    [ "MMALIB_FFT_twData", "group__MMALIB__FFT__highRadixDecompositions__ixX__cxX__oxX.html#ga491a4daad53c208c0d6bca651e2fdf76", null ]
];