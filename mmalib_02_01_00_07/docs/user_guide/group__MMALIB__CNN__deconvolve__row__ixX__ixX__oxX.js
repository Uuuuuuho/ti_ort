var group__MMALIB__CNN__deconvolve__row__ixX__ixX__oxX =
[
    [ "MMALIB_CNN_deconvolve_row_ixX_ixX_oxX_InitArgs", "structMMALIB__CNN__deconvolve__row__ixX__ixX__oxX__InitArgs.html", [
      [ "funcStyle", "structMMALIB__CNN__deconvolve__row__ixX__ixX__oxX__InitArgs.html#a23079e4ea2d480a548756c16873d4917", null ],
      [ "No", "structMMALIB__CNN__deconvolve__row__ixX__ixX__oxX__InitArgs.html#ae6ad807966cb3aaa83654b50872bb79f", null ],
      [ "numGroups", "structMMALIB__CNN__deconvolve__row__ixX__ixX__oxX__InitArgs.html#a7977c2de7ba1a5c543a6619b953aa766", null ],
      [ "inChOffset", "structMMALIB__CNN__deconvolve__row__ixX__ixX__oxX__InitArgs.html#ad596ce64bd1eaaf2ea342780d45124a9", null ],
      [ "validColsIn", "structMMALIB__CNN__deconvolve__row__ixX__ixX__oxX__InitArgs.html#a8f01f799dcbcf1f432e9f851ec2293c6", null ],
      [ "validColsPerRowIn", "structMMALIB__CNN__deconvolve__row__ixX__ixX__oxX__InitArgs.html#a7ca78148edf06a3648c91fd4d6ef63ff", null ],
      [ "validRowsIn", "structMMALIB__CNN__deconvolve__row__ixX__ixX__oxX__InitArgs.html#ae6d64968790556d9c013dfdff905a42e", null ],
      [ "inputPitchPerRow", "structMMALIB__CNN__deconvolve__row__ixX__ixX__oxX__InitArgs.html#a13b923706a9220c2ad42f6aaa8efb795", null ],
      [ "outputPitchPerRow", "structMMALIB__CNN__deconvolve__row__ixX__ixX__oxX__InitArgs.html#a0c372e2daaf8b8d9800e9224704f0dca", null ],
      [ "inWidth", "structMMALIB__CNN__deconvolve__row__ixX__ixX__oxX__InitArgs.html#a2bfd20c61ebf7d73b46cf0b91c426279", null ],
      [ "pad", "structMMALIB__CNN__deconvolve__row__ixX__ixX__oxX__InitArgs.html#a1c4688ae2116d0c9b45ca462c31b41e5", null ],
      [ "maxHeight", "structMMALIB__CNN__deconvolve__row__ixX__ixX__oxX__InitArgs.html#a565bfd0688529509e9d5c8cc23cd2b35", null ],
      [ "subMChannels", "structMMALIB__CNN__deconvolve__row__ixX__ixX__oxX__InitArgs.html#a15b8d16b2bb80c3a0b2931ee0a589367", null ],
      [ "shift", "structMMALIB__CNN__deconvolve__row__ixX__ixX__oxX__InitArgs.html#a814b749d7a3000252b670d42a0d146b5", null ],
      [ "Fr", "structMMALIB__CNN__deconvolve__row__ixX__ixX__oxX__InitArgs.html#a330bd281f03e918c65612c99897d3878", null ],
      [ "Fc", "structMMALIB__CNN__deconvolve__row__ixX__ixX__oxX__InitArgs.html#a3e14dab9155200c60cc75858e3283b37", null ],
      [ "strideX", "structMMALIB__CNN__deconvolve__row__ixX__ixX__oxX__InitArgs.html#ae80bb57de35f754b9f6dc921e3f32441", null ],
      [ "strideY", "structMMALIB__CNN__deconvolve__row__ixX__ixX__oxX__InitArgs.html#a34014e46e017ff2315fc7584cd669c01", null ],
      [ "dilationX", "structMMALIB__CNN__deconvolve__row__ixX__ixX__oxX__InitArgs.html#aaa613f688a6e478da92c6b9e36082af1", null ],
      [ "dilationY", "structMMALIB__CNN__deconvolve__row__ixX__ixX__oxX__InitArgs.html#a98d0a36ee46e7f581158089abc542fd6", null ],
      [ "bias", "structMMALIB__CNN__deconvolve__row__ixX__ixX__oxX__InitArgs.html#ae8b2be34c8667698a145cd4630d790ca", null ],
      [ "activationType", "structMMALIB__CNN__deconvolve__row__ixX__ixX__oxX__InitArgs.html#ae76585fbf10833da6413869340877d2e", null ],
      [ "mode", "structMMALIB__CNN__deconvolve__row__ixX__ixX__oxX__InitArgs.html#a88db3a6c12417484fcb4d838fb081aa4", null ]
    ] ],
    [ "MMALIB_CNN_deconvolve_row_ixX_ixX_oxX_ExecInArgs", "structMMALIB__CNN__deconvolve__row__ixX__ixX__oxX__ExecInArgs.html", [
      [ "validColsIn", "structMMALIB__CNN__deconvolve__row__ixX__ixX__oxX__ExecInArgs.html#aafc5820d1c8e0c65acb6ae7c53981e11", null ],
      [ "subMChannels", "structMMALIB__CNN__deconvolve__row__ixX__ixX__oxX__ExecInArgs.html#a66ef773ea8f55a1dce4f43cb1c81af45", null ]
    ] ],
    [ "MMALIB_CNN_deconvolve_row_ixX_ixX_oxX_ExecOutArgs", "structMMALIB__CNN__deconvolve__row__ixX__ixX__oxX__ExecOutArgs.html", [
      [ "validColsOut", "structMMALIB__CNN__deconvolve__row__ixX__ixX__oxX__ExecOutArgs.html#ac5f9d679c1870a0242134e5ef275bdf9", null ]
    ] ],
    [ "MMALIB_CNN_DECONVOLVE_ROW_IXX_IXX_OXX_STATUS_NAME", "group__MMALIB__CNN__deconvolve__row__ixX__ixX__oxX.html#gafb5bd3de0ebf4fb9597ee663394d1642", [
      [ "MMALIB_CNN_DECONVOLVE_ROW_IXX_IXX_OXX_ERR_SMALL_K", "group__MMALIB__CNN__deconvolve__row__ixX__ixX__oxX.html#ggafb5bd3de0ebf4fb9597ee663394d1642a1f24cb44515bbd6304cdaaf079a55ec3", null ],
      [ "MMALIB_CNN_DECONVOLVE_ROW_IXX_IXX_OXX_ERR_MAX", "group__MMALIB__CNN__deconvolve__row__ixX__ixX__oxX.html#ggafb5bd3de0ebf4fb9597ee663394d1642a74a5c078b36a58fbac31da978836b0d5", null ]
    ] ],
    [ "MMALIB_CNN_deconvolve_row_ixX_ixX_oxX_getHandleSize", "group__MMALIB__CNN__deconvolve__row__ixX__ixX__oxX.html#ga098fc05d6392abd753040abe6f57d0c6", null ],
    [ "MMALIB_CNN_deconvolve_row_ixX_ixX_oxX_init", "group__MMALIB__CNN__deconvolve__row__ixX__ixX__oxX.html#ga0236c6dab5bc7b457c7315d81632bd5a", null ],
    [ "MMALIB_CNN_deconvolve_row_ixX_ixX_oxX_init_checkParams", "group__MMALIB__CNN__deconvolve__row__ixX__ixX__oxX.html#ga5006eadab7fe3b81073562b87b9c42c3", null ],
    [ "MMALIB_CNN_deconvolve_row_ixX_ixX_oxX_exec", "group__MMALIB__CNN__deconvolve__row__ixX__ixX__oxX.html#ga03358851bb59d15bfd2b294a1745bc82", null ],
    [ "MMALIB_CNN_deconvolve_row_ixX_ixX_oxX_exec_checkParams", "group__MMALIB__CNN__deconvolve__row__ixX__ixX__oxX.html#ga688446fee4da89e8c13691be90721e8f", null ],
    [ "MMALIB_CNN_deconvolve_row_4x4Stride2PreProcessParameters", "group__MMALIB__CNN__deconvolve__row__ixX__ixX__oxX.html#ga256c8fcb6169cc120f9e9463bdb50b0b", null ],
    [ "MMALIB_CNN_deconvolve_row_2x2Stride2PreProcessParameters", "group__MMALIB__CNN__deconvolve__row__ixX__ixX__oxX.html#gac1fd4d0e6fdaa278f75af2c8967b0866", null ],
    [ "MMALIB_CNN_deconvolve_row_8x8Stride2PreProcessParameters", "group__MMALIB__CNN__deconvolve__row__ixX__ixX__oxX.html#gad5805ab7421b587ce26e66269c33042f", null ],
    [ "MMALIB_CNN_deconvolveBiasReLUCompute_ixX_ixX_oxX_perfEst", "group__MMALIB__CNN__deconvolve__row__ixX__ixX__oxX.html#ga1486725b6ed8b6b03331f99087a6f8c8", null ]
];