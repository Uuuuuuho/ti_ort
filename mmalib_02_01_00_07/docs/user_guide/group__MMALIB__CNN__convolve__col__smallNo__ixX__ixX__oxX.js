var group__MMALIB__CNN__convolve__col__smallNo__ixX__ixX__oxX =
[
    [ "MMALIB_CNN_convolve_col_smallNo_ixX_ixX_oxX_reorderWeights", "group__MMALIB__CNN__convolve__col__smallNo__ixX__ixX__oxX__reorderWeights.html", "group__MMALIB__CNN__convolve__col__smallNo__ixX__ixX__oxX__reorderWeights" ],
    [ "MMALIB_CNN_convolve_col_smallNo_ixX_ixX_oxX_InitArgs", "structMMALIB__CNN__convolve__col__smallNo__ixX__ixX__oxX__InitArgs.html", [
      [ "funcStyle", "structMMALIB__CNN__convolve__col__smallNo__ixX__ixX__oxX__InitArgs.html#a48dd411d606bbb47f5470af6e03fe2b3", null ],
      [ "Ni", "structMMALIB__CNN__convolve__col__smallNo__ixX__ixX__oxX__InitArgs.html#a59ae530db454d6dbc22d30750de07427", null ],
      [ "No", "structMMALIB__CNN__convolve__col__smallNo__ixX__ixX__oxX__InitArgs.html#ae9d8b2116a3481cffb88328000fc5efe", null ],
      [ "Fr", "structMMALIB__CNN__convolve__col__smallNo__ixX__ixX__oxX__InitArgs.html#aa81bbbe3b0f89aa09a9d4dbb580eca55", null ],
      [ "Fc", "structMMALIB__CNN__convolve__col__smallNo__ixX__ixX__oxX__InitArgs.html#ab4fddd54cd03a1878829de1e634eb95a", null ],
      [ "shift", "structMMALIB__CNN__convolve__col__smallNo__ixX__ixX__oxX__InitArgs.html#ae3b957954b330fcd5e1411f2e29c97be", null ],
      [ "shiftMethod", "structMMALIB__CNN__convolve__col__smallNo__ixX__ixX__oxX__InitArgs.html#a88466b6a55ef0079ed3ee2059c345f5f", null ],
      [ "strideX", "structMMALIB__CNN__convolve__col__smallNo__ixX__ixX__oxX__InitArgs.html#a312c583c41caf2b40cc0eec77f4a5e9c", null ],
      [ "strideY", "structMMALIB__CNN__convolve__col__smallNo__ixX__ixX__oxX__InitArgs.html#aa902a4a6b0069faae1a44f5fccb2f646", null ],
      [ "dilationX", "structMMALIB__CNN__convolve__col__smallNo__ixX__ixX__oxX__InitArgs.html#a52c0b050faba62079fdc9c28fa786ad8", null ],
      [ "dilationY", "structMMALIB__CNN__convolve__col__smallNo__ixX__ixX__oxX__InitArgs.html#acf670e0e0376b3585f62db926999008e", null ],
      [ "bias", "structMMALIB__CNN__convolve__col__smallNo__ixX__ixX__oxX__InitArgs.html#a2b68e3c10cda6587f335d9072c139356", null ],
      [ "biasDataType", "structMMALIB__CNN__convolve__col__smallNo__ixX__ixX__oxX__InitArgs.html#ac9fef8db6bbed79109a02e93c0e52b40", null ],
      [ "numBiasVals", "structMMALIB__CNN__convolve__col__smallNo__ixX__ixX__oxX__InitArgs.html#a5844280db037992a0798878f6264a252", null ],
      [ "activationType", "structMMALIB__CNN__convolve__col__smallNo__ixX__ixX__oxX__InitArgs.html#a883b94e1b3466bab4b7ff0ebea697511", null ],
      [ "featureWidth", "structMMALIB__CNN__convolve__col__smallNo__ixX__ixX__oxX__InitArgs.html#a7486cc053fbe39d377db101ef6e56a08", null ],
      [ "blockFeatureHeight", "structMMALIB__CNN__convolve__col__smallNo__ixX__ixX__oxX__InitArgs.html#ab84b623dd1ee054b5edc6a339eb86a33", null ],
      [ "blockFeaturePitch", "structMMALIB__CNN__convolve__col__smallNo__ixX__ixX__oxX__InitArgs.html#a325d1dc622e30bdfef98fd4e6989a6a0", null ],
      [ "columnOffset", "structMMALIB__CNN__convolve__col__smallNo__ixX__ixX__oxX__InitArgs.html#ab9edbc308bb6380bd22fa777b7959713", null ],
      [ "inPairOffset", "structMMALIB__CNN__convolve__col__smallNo__ixX__ixX__oxX__InitArgs.html#a77f96c34783852453f286d0817836d02", null ],
      [ "inChOffset", "structMMALIB__CNN__convolve__col__smallNo__ixX__ixX__oxX__InitArgs.html#ac3bc4c44a35204a4e4bac963d10f1c70", null ],
      [ "outPairOffset", "structMMALIB__CNN__convolve__col__smallNo__ixX__ixX__oxX__InitArgs.html#ada470fe2aff16036595ad97088752f4a", null ],
      [ "numGroupsPerKernel", "structMMALIB__CNN__convolve__col__smallNo__ixX__ixX__oxX__InitArgs.html#a3e3c0ae3644c5addf30bc04f1e5a5d51", null ]
    ] ],
    [ "MMALIB_CNN_convolve_col_smallNo_ixX_ixX_oxX_ExecInArgs", "structMMALIB__CNN__convolve__col__smallNo__ixX__ixX__oxX__ExecInArgs.html", [
      [ "blockFeatureWidth", "structMMALIB__CNN__convolve__col__smallNo__ixX__ixX__oxX__ExecInArgs.html#ada4b48f2e0920008e4f4531a4a769746", null ]
    ] ],
    [ "MMALIB_CNN_convolve_col_smallNo_ixX_ixX_oxX_ExecOutArgs", "structMMALIB__CNN__convolve__col__smallNo__ixX__ixX__oxX__ExecOutArgs.html", [
      [ "validColsOut", "structMMALIB__CNN__convolve__col__smallNo__ixX__ixX__oxX__ExecOutArgs.html#ac2d48881ae4de765285f89b43a38da43", null ]
    ] ],
    [ "MMALIB_CONVOLVE_COL_SHIFT_SINGLE", "group__MMALIB__CNN__convolve__col__smallNo__ixX__ixX__oxX.html#ga90e3349e8ed49a4a4c1e986e7a76af93", null ],
    [ "MMALIB_CONVOLVE_COL_SHIFT_PER_GROUP", "group__MMALIB__CNN__convolve__col__smallNo__ixX__ixX__oxX.html#ga2147043f20dea48433f160813138f485", null ],
    [ "MMALIB_CNN_CONVOLVE_COL_SMALLNO_IXX_IXX_OXX_STATUS_NAME", "group__MMALIB__CNN__convolve__col__smallNo__ixX__ixX__oxX.html#ga3b7c70f02dfbcdbbdb6266da5bb9fa5b", [
      [ "MMALIB_CNN_CONVOLVE_COL_SMALLNO_IXX_IXX_OXX_ERR_SMALL_K", "group__MMALIB__CNN__convolve__col__smallNo__ixX__ixX__oxX.html#gga3b7c70f02dfbcdbbdb6266da5bb9fa5bac2803354a33f11ce5ed98ba992b2a81f", null ],
      [ "MMALIB_CNN_CONVOLVE_COL_SMALLNO_IXX_IXX_OXX_ERR_MAX", "group__MMALIB__CNN__convolve__col__smallNo__ixX__ixX__oxX.html#gga3b7c70f02dfbcdbbdb6266da5bb9fa5ba64cf242cdfa4823b02aee76c7991f69f", null ]
    ] ],
    [ "MMALIB_CNN_convolve_col_smallNo_ixX_ixX_oxX_getHandleSize", "group__MMALIB__CNN__convolve__col__smallNo__ixX__ixX__oxX.html#ga7242664cfec4698f1f4e95ff8d18672e", null ],
    [ "MMALIB_CNN_convolve_col_smallNo_ixX_ixX_oxX_init", "group__MMALIB__CNN__convolve__col__smallNo__ixX__ixX__oxX.html#ga8cf731b81577f64eed01c47c4aafb36c", null ],
    [ "MMALIB_CNN_convolve_col_smallNo_ixX_ixX_oxX_init_checkParams", "group__MMALIB__CNN__convolve__col__smallNo__ixX__ixX__oxX.html#ga63c1afc25bdd63a1bca165311b1d99cf", null ],
    [ "MMALIB_CNN_convolve_col_smallNo_ixX_ixX_oxX_exec", "group__MMALIB__CNN__convolve__col__smallNo__ixX__ixX__oxX.html#gad80523e0a06eb11aa0d309e361fd3f5e", null ],
    [ "MMALIB_CNN_convolve_col_smallNo_ixX_ixX_oxX_exec_checkParams", "group__MMALIB__CNN__convolve__col__smallNo__ixX__ixX__oxX.html#gae51620b608fab46308672b88d2a7076d", null ],
    [ "MMALIB_CNN_convolve_col_smallNo_ixX_ixX_oxX_perfEst", "group__MMALIB__CNN__convolve__col__smallNo__ixX__ixX__oxX.html#ga12582a493735fc88d7a15741e7badc13", null ]
];