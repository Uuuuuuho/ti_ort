var group__MMALIB__DSP__fir__ixX__ixX__oxX =
[
    [ "MMALIB_DSP_fir_ixX_ixX_oxX_InitArgs", "structMMALIB__DSP__fir__ixX__ixX__oxX__InitArgs.html", [
      [ "funcStyle", "structMMALIB__DSP__fir__ixX__ixX__oxX__InitArgs.html#a3796e4466315d24557308e9d4d8f4c85", null ],
      [ "dataSize", "structMMALIB__DSP__fir__ixX__ixX__oxX__InitArgs.html#a3f53055e279e0289dd5f12a9e12a4e04", null ],
      [ "batchSize", "structMMALIB__DSP__fir__ixX__ixX__oxX__InitArgs.html#a4984e58dd60a7bc7bf1bbdf38ab89079", null ],
      [ "filterSize", "structMMALIB__DSP__fir__ixX__ixX__oxX__InitArgs.html#a3ed2ae2277b6e53348fc115ad8a30332", null ],
      [ "shift", "structMMALIB__DSP__fir__ixX__ixX__oxX__InitArgs.html#a076597f0a97afc08d2a81a1aed5093a2", null ]
    ] ],
    [ "MMALIB_DSP_fir_ixX_ixX_oxX_getHandleSize", "group__MMALIB__DSP__fir__ixX__ixX__oxX.html#gace6d8053365f4197328c3c64fdc3d838", null ],
    [ "MMALIB_DSP_fir_ixX_ixX_oxX_getSizes", "group__MMALIB__DSP__fir__ixX__ixX__oxX.html#gabe33763c6667f5ad1f7d32bd4e582d52", null ],
    [ "MMALIB_DSP_fir_ixX_ixX_oxX_init", "group__MMALIB__DSP__fir__ixX__ixX__oxX.html#ga055702847a8c5df5f36e5e3566896344", null ],
    [ "MMALIB_DSP_fir_ixX_ixX_oxX_init_checkParams", "group__MMALIB__DSP__fir__ixX__ixX__oxX.html#ga9b18cb62841af10bb0235a4319151ad8", null ],
    [ "MMALIB_DSP_fir_ixX_ixX_oxX_exec", "group__MMALIB__DSP__fir__ixX__ixX__oxX.html#gae300f8de6a768c4c294549db207cb57a", null ],
    [ "MMALIB_DSP_fir_ixX_ixX_oxX_exec_checkParams", "group__MMALIB__DSP__fir__ixX__ixX__oxX.html#gaa37127fcfc21bd3ee03ca4618d41ada5", null ],
    [ "MMALIB_DSP_fir_ixX_ixX_oxX_perfEst", "group__MMALIB__DSP__fir__ixX__ixX__oxX.html#ga471f4a66b0ff693ddc757ca8eb2d0895", null ]
];