var group__MMALIB__FFT__dftLarge__ixX__cxX__oxX =
[
    [ "MMALIB_FFT_dftLarge_ixX_cxX_oxX_InitArgs", "structMMALIB__FFT__dftLarge__ixX__cxX__oxX__InitArgs.html", [
      [ "funcStyle", "structMMALIB__FFT__dftLarge__ixX__cxX__oxX__InitArgs.html#ad0118e5f4d3745d190b7b83e9c18d39a", null ],
      [ "fftSize", "structMMALIB__FFT__dftLarge__ixX__cxX__oxX__InitArgs.html#aa2f3a070c1ab467d8b19abdaae9292d5", null ],
      [ "batchSize", "structMMALIB__FFT__dftLarge__ixX__cxX__oxX__InitArgs.html#aba33c39a74a929abf56d70b30fbee2f8", null ],
      [ "interleave", "structMMALIB__FFT__dftLarge__ixX__cxX__oxX__InitArgs.html#ad8290674edab6adde0bdcc0d05cf6adc", null ],
      [ "shiftVector", "structMMALIB__FFT__dftLarge__ixX__cxX__oxX__InitArgs.html#a85f017968b42a97cb0c8e822b7141f0f", null ]
    ] ],
    [ "MMALIB_FFT_DFTLARGE_IXX_CXX_OXX_NUMSHIFTS", "group__MMALIB__FFT__dftLarge__ixX__cxX__oxX.html#gabf0fe0cfd63921e9831b700479fdd783", null ],
    [ "MMALIB_FFT_dftLarge_ixX_cxX_oxX_getHandleSize", "group__MMALIB__FFT__dftLarge__ixX__cxX__oxX.html#gac15cda2a2ac152f38fbf13249434fc39", null ],
    [ "MMALIB_FFT_dftLarge_ixX_cxX_oxX_getSizes", "group__MMALIB__FFT__dftLarge__ixX__cxX__oxX.html#ga3fb044739bcd6f36ce6c8bc0b9c296c9", null ],
    [ "MMALIB_FFT_dftLarge_ixX_cxX_oxX_twGen", "group__MMALIB__FFT__dftLarge__ixX__cxX__oxX.html#gaa916d444da69f8f28508c94c17740399", null ],
    [ "MMALIB_FFT_dftLarge_ixX_cxX_oxX_init", "group__MMALIB__FFT__dftLarge__ixX__cxX__oxX.html#gacc6dae03592a72b8efcc9cda864fab3b", null ],
    [ "MMALIB_FFT_dftLarge_ixX_cxX_oxX_init_checkParams", "group__MMALIB__FFT__dftLarge__ixX__cxX__oxX.html#gaca351579127d52b86b88311836c69296", null ],
    [ "MMALIB_FFT_dftLarge_ixX_cxX_oxX_exec", "group__MMALIB__FFT__dftLarge__ixX__cxX__oxX.html#ga14940686748ef8038b80b38e5ecc5225", null ],
    [ "MMALIB_FFT_dftLarge_ixX_cxX_oxX_exec_checkParams", "group__MMALIB__FFT__dftLarge__ixX__cxX__oxX.html#ga0a6299176a0b21221ea472b970870364", null ],
    [ "MMALIB_FFT_dftLarge_ixX_cxX_oxX_perfEst", "group__MMALIB__FFT__dftLarge__ixX__cxX__oxX.html#ga4a68a6466c7a7157ea24644a492b4923", null ]
];