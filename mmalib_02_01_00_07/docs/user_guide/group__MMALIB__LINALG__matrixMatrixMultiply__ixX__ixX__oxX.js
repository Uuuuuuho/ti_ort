var group__MMALIB__LINALG__matrixMatrixMultiply__ixX__ixX__oxX =
[
    [ "MMALIB_LINALG_matrixMatrixMultiply_ixX_ixX_oxX_InitArgs", "structMMALIB__LINALG__matrixMatrixMultiply__ixX__ixX__oxX__InitArgs.html", [
      [ "funcStyle", "structMMALIB__LINALG__matrixMatrixMultiply__ixX__ixX__oxX__InitArgs.html#a0471e85350779e50e1b010306c35d84d", null ],
      [ "shift", "structMMALIB__LINALG__matrixMatrixMultiply__ixX__ixX__oxX__InitArgs.html#a9ab44e61b553c50cd3681047cd8f9872", null ]
    ] ],
    [ "MMALIB_LINALG_MATRIXMATRIXMULTIPLY_IXX_IXX_OXX_STATUS_NAME", "group__MMALIB__LINALG__matrixMatrixMultiply__ixX__ixX__oxX.html#ga2322b7df9a38410df7ae5a405f1b2fe8", [
      [ "MMALIB_LINALG_MATRIXMATRIXMULTIPLY_IXX_IXX_OXX_ERR_SMALL_K", "group__MMALIB__LINALG__matrixMatrixMultiply__ixX__ixX__oxX.html#gga2322b7df9a38410df7ae5a405f1b2fe8a271cfe8986dbb4e37a42060516e06b62", null ],
      [ "MMALIB_LINALG_MATRIXMATRIXMULTIPLY_IXX_IXX_OXX_ERR_MAX", "group__MMALIB__LINALG__matrixMatrixMultiply__ixX__ixX__oxX.html#gga2322b7df9a38410df7ae5a405f1b2fe8a0c92f4e52fdf84229412cea0af05f3da", null ]
    ] ],
    [ "MMALIB_LINALG_matrixMatrixMultiply_ixX_ixX_oxX_getHandleSize", "group__MMALIB__LINALG__matrixMatrixMultiply__ixX__ixX__oxX.html#gaf4fc309546b14ebba6b2dcce6b114d25", null ],
    [ "MMALIB_LINALG_matrixMatrixMultiply_ixX_ixX_oxX_init", "group__MMALIB__LINALG__matrixMatrixMultiply__ixX__ixX__oxX.html#ga51d75dbee5819297b85ea8165a478424", null ],
    [ "MMALIB_LINALG_matrixMatrixMultiply_ixX_ixX_oxX_init_checkParams", "group__MMALIB__LINALG__matrixMatrixMultiply__ixX__ixX__oxX.html#gaeacbaa27286de4c83f41293be0d79a5c", null ],
    [ "MMALIB_LINALG_matrixMatrixMultiply_ixX_ixX_oxX_exec", "group__MMALIB__LINALG__matrixMatrixMultiply__ixX__ixX__oxX.html#ga66d08efd1bdd8b2ed82e1f1da663edac", null ],
    [ "MMALIB_LINALG_matrixMatrixMultiply_ixX_ixX_oxX_exec_checkParams", "group__MMALIB__LINALG__matrixMatrixMultiply__ixX__ixX__oxX.html#ga433acf215c3f75a8cbfd695b87cf7cfd", null ],
    [ "MMALIB_LINALG_matrixMatrixMultiply_ixX_ixX_oxX_perfEst", "group__MMALIB__LINALG__matrixMatrixMultiply__ixX__ixX__oxX.html#gae56ab21c132d5756477236a6b51f30ca", null ]
];