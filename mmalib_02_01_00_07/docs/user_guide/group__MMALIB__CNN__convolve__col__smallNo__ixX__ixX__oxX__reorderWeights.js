var group__MMALIB__CNN__convolve__col__smallNo__ixX__ixX__oxX__reorderWeights =
[
    [ "MMALIB_CNN_convolve_col_smallNo_ixX_ixX_oxX_reorderWeights_Args", "structMMALIB__CNN__convolve__col__smallNo__ixX__ixX__oxX__reorderWeights__Args.html", [
      [ "dataType", "structMMALIB__CNN__convolve__col__smallNo__ixX__ixX__oxX__reorderWeights__Args.html#acc0a7b9fa3c0e0347f7243c03b792bb0", null ],
      [ "Ni", "structMMALIB__CNN__convolve__col__smallNo__ixX__ixX__oxX__reorderWeights__Args.html#a062b6a9d6e88256c1c0534decce4b9ed", null ],
      [ "No", "structMMALIB__CNN__convolve__col__smallNo__ixX__ixX__oxX__reorderWeights__Args.html#a25a5a2978efd30d4fd292d126ab77b29", null ],
      [ "Fr", "structMMALIB__CNN__convolve__col__smallNo__ixX__ixX__oxX__reorderWeights__Args.html#acedf21825292d15328f4a59b1bb4fd40", null ],
      [ "Fc", "structMMALIB__CNN__convolve__col__smallNo__ixX__ixX__oxX__reorderWeights__Args.html#a09a533138397cdcb9ab33f532e0f5948", null ],
      [ "strideX", "structMMALIB__CNN__convolve__col__smallNo__ixX__ixX__oxX__reorderWeights__Args.html#a1fc74275b1f616ce2b7cf45cb7c340aa", null ],
      [ "strideY", "structMMALIB__CNN__convolve__col__smallNo__ixX__ixX__oxX__reorderWeights__Args.html#a3df5ec653b95f49fcfa2abe3dc39a4ec", null ],
      [ "dilationX", "structMMALIB__CNN__convolve__col__smallNo__ixX__ixX__oxX__reorderWeights__Args.html#a74975ce0e9b1b5ede9bec847bd6a1a2e", null ],
      [ "featureWidth", "structMMALIB__CNN__convolve__col__smallNo__ixX__ixX__oxX__reorderWeights__Args.html#a4b5acadb219c178eaa98af960687fd1c", null ],
      [ "blockFeatureHeight", "structMMALIB__CNN__convolve__col__smallNo__ixX__ixX__oxX__reorderWeights__Args.html#acd61e01aa612a1f55a81f3bdb202c8a8", null ],
      [ "numBiasVals", "structMMALIB__CNN__convolve__col__smallNo__ixX__ixX__oxX__reorderWeights__Args.html#a20e6f57321b8068fb4bf5c004464daa1", null ],
      [ "numGroupsPerKernel", "structMMALIB__CNN__convolve__col__smallNo__ixX__ixX__oxX__reorderWeights__Args.html#ae3cf0495dc374551441cde34704de5f4", null ]
    ] ],
    [ "MMALIB_reorder_weights_operation_e", "group__MMALIB__CNN__convolve__col__smallNo__ixX__ixX__oxX__reorderWeights.html#ga706969959f02635b5b2b3db6bf12d59e", [
      [ "REORDER_WEIGHTS_AND_BIAS", "group__MMALIB__CNN__convolve__col__smallNo__ixX__ixX__oxX__reorderWeights.html#gga706969959f02635b5b2b3db6bf12d59eaccb4d93f8fac77af7623365e7f497af4", null ],
      [ "REORDER_WEIGHTS", "group__MMALIB__CNN__convolve__col__smallNo__ixX__ixX__oxX__reorderWeights.html#gga706969959f02635b5b2b3db6bf12d59ea880c0b3a814ca2853e318df238a8ff77", null ],
      [ "REORDER_BIAS", "group__MMALIB__CNN__convolve__col__smallNo__ixX__ixX__oxX__reorderWeights.html#gga706969959f02635b5b2b3db6bf12d59eab7d49143344645fd44269574e5c4cbb1", null ]
    ] ],
    [ "MMALIB_CNN_convolve_col_smallNo_ixX_ixX_oxX_reorderWeights_getMemorySize", "group__MMALIB__CNN__convolve__col__smallNo__ixX__ixX__oxX__reorderWeights.html#gaa3bc5a48fe750a7315b531c42184d2c6", null ],
    [ "MMALIB_CNN_convolve_col_smallNo_ixX_ixX_oxX_reorderWeights_fillBufParams", "group__MMALIB__CNN__convolve__col__smallNo__ixX__ixX__oxX__reorderWeights.html#gad2869cab98d0482aaeac5b089996a4d2", null ],
    [ "MMALIB_CNN_convolve_col_smallNo_ixX_ixX_oxX_reorderWeights_exec", "group__MMALIB__CNN__convolve__col__smallNo__ixX__ixX__oxX__reorderWeights.html#gad6c1a75231ecd2c80f06da26df74745c", null ]
];