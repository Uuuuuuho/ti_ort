var group__MMALIB__LINALG__pointwiseMatrixMatrixMultiply__ixX__ixX__oxX =
[
    [ "MMALIB_LINALG_pointwiseMatrixMatrixMultiply_ixX_ixX_oxX_InitArgs", "structMMALIB__LINALG__pointwiseMatrixMatrixMultiply__ixX__ixX__oxX__InitArgs.html", [
      [ "funcStyle", "structMMALIB__LINALG__pointwiseMatrixMatrixMultiply__ixX__ixX__oxX__InitArgs.html#a93b99e5de4cf3a102a8b9cd6658ef86e", null ],
      [ "shift", "structMMALIB__LINALG__pointwiseMatrixMatrixMultiply__ixX__ixX__oxX__InitArgs.html#a413e0319d9314fe7902a7a460484b09d", null ]
    ] ],
    [ "MMALIB_LINALG_POINTWISEMATRIXMATRIXMULTIPY_IXX_IXX_OXX_STATUS_NAME", "group__MMALIB__LINALG__pointwiseMatrixMatrixMultiply__ixX__ixX__oxX.html#gaab6465f856408024befb5717ea5ba789", [
      [ "MMALIB_LINALG_POINTWISEMATRIXMATRIXMULTIPY_IXX_IXX_OXX_ERR_SMALL_K", "group__MMALIB__LINALG__pointwiseMatrixMatrixMultiply__ixX__ixX__oxX.html#ggaab6465f856408024befb5717ea5ba789a9a09fd7e414879b1fdf7ff1d27566d7e", null ],
      [ "MMALIB_LINALG_POINTWISEMATRIXMATRIXMULTIPY_IXX_IXX_OXX_ERR_MAX", "group__MMALIB__LINALG__pointwiseMatrixMatrixMultiply__ixX__ixX__oxX.html#ggaab6465f856408024befb5717ea5ba789a211bcaf679d6a2f632dcc88f7cd047e7", null ]
    ] ],
    [ "MMALIB_LINALG_pointwiseMatrixMatrixMultiply_ixX_ixX_oxX_getHandleSize", "group__MMALIB__LINALG__pointwiseMatrixMatrixMultiply__ixX__ixX__oxX.html#ga1d4bf19ef2f3bdab7a17819d580c7a8c", null ],
    [ "MMALIB_LINALG_pointwiseMatrixMatrixMultiply_ixX_ixX_oxX_init", "group__MMALIB__LINALG__pointwiseMatrixMatrixMultiply__ixX__ixX__oxX.html#ga11f931fb568c998a3fc8973d1f61dfa6", null ],
    [ "MMALIB_LINALG_pointwiseMatrixMatrixMultiply_ixX_ixX_oxX_init_checkParams", "group__MMALIB__LINALG__pointwiseMatrixMatrixMultiply__ixX__ixX__oxX.html#ga75f308da2fd75c714609c1950aa19817", null ],
    [ "MMALIB_LINALG_pointwiseMatrixMatrixMultiply_ixX_ixX_oxX_exec", "group__MMALIB__LINALG__pointwiseMatrixMatrixMultiply__ixX__ixX__oxX.html#ga47329d6e77f5469b389883a38ea41e47", null ],
    [ "MMALIB_LINALG_pointwiseMatrixMatrixMultiply_ixX_ixX_oxX_exec_checkParams", "group__MMALIB__LINALG__pointwiseMatrixMatrixMultiply__ixX__ixX__oxX.html#ga2dba553d042d31f8feb5373ae312bcf6", null ],
    [ "MMALIB_LINALG_pointwiseMatrixMatrixMultiply_ixX_ixX_oxX_perfEst", "group__MMALIB__LINALG__pointwiseMatrixMatrixMultiply__ixX__ixX__oxX.html#gac1710d9c443064851ed63a7ff47787b4", null ]
];