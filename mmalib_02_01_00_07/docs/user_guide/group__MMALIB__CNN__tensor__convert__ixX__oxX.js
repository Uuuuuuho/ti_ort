var group__MMALIB__CNN__tensor__convert__ixX__oxX =
[
    [ "MMALIB_CNN_tensor_convert_ixX_oxX_InitArgs", "structMMALIB__CNN__tensor__convert__ixX__oxX__InitArgs.html", [
      [ "funcStyle", "structMMALIB__CNN__tensor__convert__ixX__oxX__InitArgs.html#a698933d3f5f71e029683245c61963d7d", null ],
      [ "inPad", "structMMALIB__CNN__tensor__convert__ixX__oxX__InitArgs.html#a6ee95c37eb3ebffab7240ecbf2c78cbe", null ],
      [ "outPad", "structMMALIB__CNN__tensor__convert__ixX__oxX__InitArgs.html#a7be2da0976a3597e9dc11909d69e9d20", null ],
      [ "inZf", "structMMALIB__CNN__tensor__convert__ixX__oxX__InitArgs.html#ab27998c0d8e4a7485eccb83c5eb0890c", null ],
      [ "outZf", "structMMALIB__CNN__tensor__convert__ixX__oxX__InitArgs.html#a823e0550042b31e6aa523e2f8fd29500", null ],
      [ "tensorFormatIn", "structMMALIB__CNN__tensor__convert__ixX__oxX__InitArgs.html#a8589f1e04be4d1dd2aa8899e793547f3", null ],
      [ "tensorFormatOut", "structMMALIB__CNN__tensor__convert__ixX__oxX__InitArgs.html#a8239a2d14ff99119e05f01dfc3c9fa59", null ]
    ] ],
    [ "MMALIB_CNN_tensor_convert_ixX_oxX_ExecInArgs", "structMMALIB__CNN__tensor__convert__ixX__oxX__ExecInArgs.html", [
      [ "src0DimZ", "structMMALIB__CNN__tensor__convert__ixX__oxX__ExecInArgs.html#ad5ed871a7a08506343d62cf4ebcdce21", null ],
      [ "src0DimY", "structMMALIB__CNN__tensor__convert__ixX__oxX__ExecInArgs.html#a0df56d60e230e10d0839b0cba0972e28", null ]
    ] ],
    [ "MMALIB_CNN_tensor_convert_ixX_oxX_ExecOutArgs", "structMMALIB__CNN__tensor__convert__ixX__oxX__ExecOutArgs.html", [
      [ "validColsOut", "structMMALIB__CNN__tensor__convert__ixX__oxX__ExecOutArgs.html#accc0359999c53df3a6f325e8308582ff", null ]
    ] ],
    [ "MMALIB_CNN_tensor_convert_ixX_oxX_getHandleSize", "group__MMALIB__CNN__tensor__convert__ixX__oxX.html#ga285e94d67cd812c06da5a8d08bb5fe55", null ],
    [ "MMALIB_CNN_tensor_convert_ixX_oxX_init", "group__MMALIB__CNN__tensor__convert__ixX__oxX.html#ga1dcbeb7d6353f19ab4181ccc41c71761", null ],
    [ "MMALIB_CNN_tensor_convert_ixX_oxX_init_checkParams", "group__MMALIB__CNN__tensor__convert__ixX__oxX.html#gabfcc38ff8fb495c473edc06b9b84a605", null ],
    [ "MMALIB_CNN_tensor_convert_ixX_oxX_exec", "group__MMALIB__CNN__tensor__convert__ixX__oxX.html#gadae38c5fad8740cb5111ca3dfe893760", null ],
    [ "MMALIB_CNN_tensor_convert_ixX_oxX_exec_checkParams", "group__MMALIB__CNN__tensor__convert__ixX__oxX.html#ga2931d4eb7c52455ee0dbf8924e3ab9b5", null ]
];