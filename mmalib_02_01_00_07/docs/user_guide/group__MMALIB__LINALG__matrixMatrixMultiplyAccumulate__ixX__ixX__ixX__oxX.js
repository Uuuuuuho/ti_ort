var group__MMALIB__LINALG__matrixMatrixMultiplyAccumulate__ixX__ixX__ixX__oxX =
[
    [ "MMALIB_LINALG_matrixMatrixMultiplyAccumulate_ixX_ixX_ixX_oxX_InitArgs", "structMMALIB__LINALG__matrixMatrixMultiplyAccumulate__ixX__ixX__ixX__oxX__InitArgs.html", [
      [ "funcStyle", "structMMALIB__LINALG__matrixMatrixMultiplyAccumulate__ixX__ixX__ixX__oxX__InitArgs.html#a76689e8dca062581f511e28213641fdd", null ],
      [ "shift", "structMMALIB__LINALG__matrixMatrixMultiplyAccumulate__ixX__ixX__ixX__oxX__InitArgs.html#a7a2100dce2db74a2a2f7b721d9dd347e", null ]
    ] ],
    [ "MMALIB_LINALG_MATRIXMATRIXMULTIPLYACCUMULATE_IXX_IXX_IXX_OXX_STATUS_NAME", "group__MMALIB__LINALG__matrixMatrixMultiplyAccumulate__ixX__ixX__ixX__oxX.html#gae114aa6f78ad81939a20764c14ab114f", [
      [ "MMALIB_LINALG_MATRIXMATRIXMULTIPLYACCUMULATE_IXX_IXX_IXX_OXX_ERR_SMALL_K", "group__MMALIB__LINALG__matrixMatrixMultiplyAccumulate__ixX__ixX__ixX__oxX.html#ggae114aa6f78ad81939a20764c14ab114fa5380b69ca87431ff4676bb18769fff9b", null ],
      [ "MMALIB_LINALG_MATRIXMATRIXMULTIPLYACCUMULATE_IXX_IXX_IXX_OXX_ERR_MAX", "group__MMALIB__LINALG__matrixMatrixMultiplyAccumulate__ixX__ixX__ixX__oxX.html#ggae114aa6f78ad81939a20764c14ab114fa900f327a5e24c3bf4c0a6dbfcc5e42b6", null ]
    ] ],
    [ "MMALIB_LINALG_matrixMatrixMultiplyAccumulate_ixX_ixX_ixX_oxX_getHandleSize", "group__MMALIB__LINALG__matrixMatrixMultiplyAccumulate__ixX__ixX__ixX__oxX.html#gac932d3feaab05fe16b4cf172a1d6d167", null ],
    [ "MMALIB_LINALG_matrixMatrixMultiplyAccumulate_ixX_ixX_ixX_oxX_init", "group__MMALIB__LINALG__matrixMatrixMultiplyAccumulate__ixX__ixX__ixX__oxX.html#ga461e089a2e5543a62db983c8f4163665", null ],
    [ "MMALIB_LINALG_matrixMatrixMultiplyAccumulate_ixX_ixX_ixX_oxX_init_checkParams", "group__MMALIB__LINALG__matrixMatrixMultiplyAccumulate__ixX__ixX__ixX__oxX.html#ga2c3eac9732884e34e889ac4f70547d6f", null ],
    [ "MMALIB_LINALG_matrixMatrixMultiplyAccumulate_ixX_ixX_ixX_oxX_exec", "group__MMALIB__LINALG__matrixMatrixMultiplyAccumulate__ixX__ixX__ixX__oxX.html#ga1f2940eae168e337e8a1e7d1a4f332fa", null ],
    [ "MMALIB_LINALG_matrixMatrixMultiplyAccumulate_ixX_ixX_ixX_oxX_exec_checkParams", "group__MMALIB__LINALG__matrixMatrixMultiplyAccumulate__ixX__ixX__ixX__oxX.html#ga7cd027330d0cf2f12631aa9a2070d2c1", null ],
    [ "MMALIB_LINALG_matrixMatrixMultiplyAccumulate_ixX_ixX_ixX_oxX_perfEst", "group__MMALIB__LINALG__matrixMatrixMultiplyAccumulate__ixX__ixX__ixX__oxX.html#ga4c90279dfe4b8c77f02148e0568760e5", null ]
];