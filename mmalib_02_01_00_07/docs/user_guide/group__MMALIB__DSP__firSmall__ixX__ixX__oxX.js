var group__MMALIB__DSP__firSmall__ixX__ixX__oxX =
[
    [ "MMALIB_DSP_firSmall_ixX_ixX_oxX_InitArgs", "structMMALIB__DSP__firSmall__ixX__ixX__oxX__InitArgs.html", [
      [ "funcStyle", "structMMALIB__DSP__firSmall__ixX__ixX__oxX__InitArgs.html#a87919c097aa954b9d1505128f32c52d6", null ],
      [ "dataSize", "structMMALIB__DSP__firSmall__ixX__ixX__oxX__InitArgs.html#a3b16faef5b24c76824e845dc6cba2fd2", null ],
      [ "batchSize", "structMMALIB__DSP__firSmall__ixX__ixX__oxX__InitArgs.html#a48b3efcbaedf49e3c5a5599494bbf64a", null ],
      [ "filterSize", "structMMALIB__DSP__firSmall__ixX__ixX__oxX__InitArgs.html#a858f37563bd459aa2b081842ccfbd681", null ],
      [ "shift", "structMMALIB__DSP__firSmall__ixX__ixX__oxX__InitArgs.html#a179156cbdeff844986d283e5ee2873c7", null ]
    ] ],
    [ "MMALIB_DSP_firSmall_ixX_ixX_oxX_getHandleSize", "group__MMALIB__DSP__firSmall__ixX__ixX__oxX.html#gab2b3eb480bdf8de8a01479821ceed8dc", null ],
    [ "MMALIB_DSP_firSmall_ixX_ixX_oxX_getSizes", "group__MMALIB__DSP__firSmall__ixX__ixX__oxX.html#ga6f39b0e3f1fe4db9f748d5f6d7fbc110", null ],
    [ "MMALIB_DSP_firSmall_ixX_ixX_oxX_init", "group__MMALIB__DSP__firSmall__ixX__ixX__oxX.html#ga09555f7135f8960b2935dc776c94c2f6", null ],
    [ "MMALIB_DSP_firSmall_ixX_ixX_oxX_init_checkParams", "group__MMALIB__DSP__firSmall__ixX__ixX__oxX.html#ga25af9c2f34f13bfad3f40e22993a0ddb", null ],
    [ "MMALIB_DSP_firSmall_ixX_ixX_oxX_exec", "group__MMALIB__DSP__firSmall__ixX__ixX__oxX.html#ga9b2236d34e242d0d814649980f223806", null ],
    [ "MMALIB_DSP_firSmall_ixX_ixX_oxX_exec_checkParams", "group__MMALIB__DSP__firSmall__ixX__ixX__oxX.html#gad706224f362b18ffdeafc6289286bbd9", null ],
    [ "MMALIB_DSP_firSmall_ixX_ixX_oxX_perfEst", "group__MMALIB__DSP__firSmall__ixX__ixX__oxX.html#ga55fa4912abf346ab7043ec488824a98e", null ]
];