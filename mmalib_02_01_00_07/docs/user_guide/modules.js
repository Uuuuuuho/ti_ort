var modules =
[
    [ "Common definitions", "group__MMALIB__COMMON.html", "group__MMALIB__COMMON" ],
    [ "Convolutional Neural Networks (CNN) kernels", "group__MMALIB__CNN.html", "group__MMALIB__CNN" ],
    [ "Digital Signal Processing (DSP) kernels", "group__MMALIB__DSP.html", "group__MMALIB__DSP" ],
    [ "Fast Fourier Transform (FFT) kernels", "group__MMALIB__FFT.html", "group__MMALIB__FFT" ],
    [ "Linear Algebra (LINALG) kernels", "group__MMALIB__LINALG.html", "group__MMALIB__LINALG" ]
];