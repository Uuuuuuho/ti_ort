var group__MMALIB__LINALG__matrixTranspose__ixX__oxX =
[
    [ "MMALIB_LINALG_matrixTranspose_ixX_oxX_InitArgs", "structMMALIB__LINALG__matrixTranspose__ixX__oxX__InitArgs.html", [
      [ "funcStyle", "structMMALIB__LINALG__matrixTranspose__ixX__oxX__InitArgs.html#a8cfbb311f05e37eee73bfc8b0e9cd580", null ]
    ] ],
    [ "MMALIB_LINALG_matrixTranspose_ixX_oxX_getHandleSize", "group__MMALIB__LINALG__matrixTranspose__ixX__oxX.html#ga15e6ab89fe3a75f101be1031a02be799", null ],
    [ "MMALIB_LINALG_matrixTranspose_ixX_oxX_init", "group__MMALIB__LINALG__matrixTranspose__ixX__oxX.html#gaa9dfd1133fc5e77c941d060d4b4b4b22", null ],
    [ "MMALIB_LINALG_matrixTranspose_ixX_oxX_init_checkParams", "group__MMALIB__LINALG__matrixTranspose__ixX__oxX.html#ga5c2177ea97b09ebe472a0a09167ef418", null ],
    [ "MMALIB_LINALG_matrixTranspose_ixX_oxX_exec", "group__MMALIB__LINALG__matrixTranspose__ixX__oxX.html#ga4b70703e7219cda6ad9dba163ad210eb", null ],
    [ "MMALIB_LINALG_matrixTranspose_ixX_oxX_exec_checkParams", "group__MMALIB__LINALG__matrixTranspose__ixX__oxX.html#ga2c21d8e6c36f0587aa824dae3799b250", null ],
    [ "MMALIB_LINALG_matrixTranspose_ixX_oxX_perfEst", "group__MMALIB__LINALG__matrixTranspose__ixX__oxX.html#gaf203c37a6a02c7a5fbed0d56ed207a83", null ]
];