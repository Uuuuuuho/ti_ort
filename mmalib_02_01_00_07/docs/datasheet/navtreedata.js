var NAVTREE =
[
  [ "MMALIB Datasheet", "index.html", [
    [ "MMALIB", "index.html", null ],
    [ "Data Sheet", "data_sheet.html", "data_sheet" ]
  ] ]
];

var NAVTREEINDEX =
[
"data_sheet.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';