/******************************************************************************
*                                                                             *
* module name       :MMALIB                                                   *
*                                                                             *
* module descripton :Matrix Multiply Accelerator Library module for C7x+MMA   *
*                                                                             *
* Copyright (C) 2017-2018 Texas Instruments Incorporated - http://www.ti.com/ *
* ALL RIGHTS RESERVED                                                         *
*                                                                             *
******************************************************************************/

/**
  ******************************************************************************
  *  @file     MMALIB_bufParams.h
  *
  *  @brief    File to hold buffer parameter related info for MMALIB
  *
  *  @version  0.1 - Jan 2018 : Initial Version with as a template [PKS] 
  *            
 *******************************************************************************
*/
#ifndef _MMALIB_H_
#define _MMALIB_H_

#include "common/MMALIB_bufParams.h"
#include "common/MMALIB_types.h"

#include "cnn_c7xmma/mmalib_cnn.h"
#include "dsp_c7xmma/mmalib_dsp.h"
#include "linalg_c7xmma/mmalib_linalg.h"
#include "fft_c7xmma/mmalib_fft.h"

#if !defined(_HOST_BUILD)
#include "fft_c7x/c7x_fft.h"
#endif /* _HOST_BUILD */


#endif /*_MMALIB_H_*/
