/******************************************************************************
 *                                                                             *
 * module name       :MMALIB                                                   *
 *                                                                             *
 * module descripton :Matrix Multiply Accelerator Library module for C7x+MMA   *
 *                                                                             *
 * Copyright (C) 2017-2018 Texas Instruments Incorporated - http://www.ti.com/ *
 * ALL RIGHTS RESERVED                                                         *
 *                                                                             *
 ******************************************************************************/
/**
******************************************************************************
*  @file     MMALIB_CNN_tensor_convert_ixX_oxX.h
*
*  @brief    Public header file for MMALIB_CNN_tensor_convert_ixX_oxX
*            function
*
*  //version  0.1 - June 2020 : Initial Version - [Barath Ramesh]
*
*******************************************************************************
*/

#ifndef MMALIB_CNN_TENSOR_CONVERT_IXX_IXX_OXX_H_
#define MMALIB_CNN_TENSOR_CONVERT_IXX_IXX_OXX_H_

#include "../../common/MMALIB_types.h"

#ifdef __cplusplus
extern "C" {
#endif

/*!
 * @defgroup MMALIB_CNN_tensor_convert_ixX_oxX
 * MMALIB_CNN_tensor_convert_ixX_oxX
 * @brief Kernel for MMALIB_CNN_tensor_converting tensors of various datatypes
 * @ingroup  MMALIB_CNN
 */
/* @{ */

/*!
 * @brief Structure containing the parameters
 * initialization of tensor convert function
 */
typedef struct {
   /*! @brief Variant of the function refer to @ref MMALIB_FUNCTION_STYLE     */
   int8_t  funcStyle;
   int32_t inPad;
   /*! @brief output Pad of each row of output feature map, specify pad only on
    * one side
    */
   int32_t outPad;
   /*! @brief zero point conversion
    */
   float inZf;
   float outZf;
   /*! @brief input tensor format: CHW or HWC
    */
   MMALIB_tensor_format tensorFormatIn;
   /*! @brief output tensor format: CHW or HWC
    */
   MMALIB_tensor_format tensorFormatOut;

} MMALIB_CNN_tensor_convert_ixX_oxX_InitArgs;

typedef struct {

   /* bool                    lastCall; */
   uint32_t src0DimZ; // update if this is different from init src0_addr.dimz
   uint32_t src0DimY; // update if this is different from init src0_addr.dimy

} MMALIB_CNN_tensor_convert_ixX_oxX_ExecInArgs;

typedef struct {
   // dummy placeholder for now
   int32_t validColsOut;
} MMALIB_CNN_tensor_convert_ixX_oxX_ExecOutArgs;

/*!
 *  @brief        This is a query function to calculate the size of internal
 *                handle
 *  @param [in]   pKerInitArgs  : Pointer to structure holding init parameters
 *  @return       Size of the buffer in bytes
 *  @remarks      Application is expected to allocate buffer of the requested
 *                size and provide it as input to other functions requiring it.
 */
int32_t MMALIB_CNN_tensor_convert_ixX_oxX_getHandleSize (
    MMALIB_CNN_tensor_convert_ixX_oxX_InitArgs *pKerInitArgs);

/*!
*  @brief       This function call is required to initialize the handle. In
*               this function most of the one time operation are performed
*               and results are stored in handle
*
*  @param [in]  handle       :  Active handle to the kernel
*  @param [in]  src0_addr    :  Pointer to structure containing dimensional
*                               information of src
*  @param [in]  src1_addr    :  Pointer to structure containing dimensional
*                               information of scale
*  @param [out] dst_addr     :  Pointer to structure containing dimensional
*                               information of dst feature maps
*  @param [in]  pKerInitArgs : Pointer to structure holding init parameters

*  @return      Status of success or Error with Error Codes
*
*  @remarks     Application is expected to do provide valid handle
*/

MMALIB_STATUS MMALIB_CNN_tensor_convert_ixX_oxX_init (
    MMALIB_kernelHandle                               handle,
    const MMALIB_bufParams3D_t *                      src0_addr,
    const MMALIB_bufParams1D_t *                      src1_addr,
    const MMALIB_bufParams3D_t *                      dst_addr,
    const MMALIB_CNN_tensor_convert_ixX_oxX_InitArgs *pKerInitArgs);

/*!
*  @brief       This function call is required to initialize the handle. In
*               this function most of the one time operation are performed
*               and results are stored in handle
*
*  @param [in]  handle       :  Active handle to the kernel
*  @param [in]  src0_addr    :  Pointer to structure containing dimensional
*                               information of src
*  @param [out] dst_addr     :  Pointer to structure containing dimensional
*                               information of dst output feature maps
*  @param [in]  pKerInitArgs : Pointer to structure holding init parameters

*  @return      Status of success or Error with Error Codes
*
*  @remarks     Application is expected to do provide valid handle
*/

MMALIB_STATUS MMALIB_CNN_tensor_convert_ixX_oxX_init_checkParams (
    MMALIB_kernelHandle                               handle,
    const MMALIB_bufParams3D_t *                      src0_addr,
    const MMALIB_bufParams1D_t *                      src1_addr,
    const MMALIB_bufParams3D_t *                      dst_addr,
    const MMALIB_CNN_tensor_convert_ixX_oxX_InitArgs *pKerInitArgs);

/*!
*  @brief       This function is the main compute function, and performs
*               the tensor datatype conversion.
*
*  @details     TBD
*
*  @param [in]  handle      : Active handle to the kernel
*  @param [in]  src0[]      : Pointer to buffer holding input tensor/feature map
*  @param [in]  src1[]      : Pointer to buffer holding scale vector/scalar
*  @param [in]  src2[]      : Pointer to buffer holding shift vector/scalar
*  @param [out] dst[]       : Pointer to buffer holding output feature map

*  @return      Status of success or Error with Error Codes
*
*  @par Assumptions:
*    - I/O buffer pointers are assumed to be not aliased.
*
*  @par Performance Considerations:
*    - TBD
*
*  @remarks     Application is expected to do call of checkParams function prior
*               to this function as it avoids check of paramaters for each
*               invocation for optimization
*/

MMALIB_STATUS MMALIB_CNN_tensor_convert_ixX_oxX_exec (
    MMALIB_kernelHandle                                 handle,
    const void *                                        src0,
    const void *                                        src1,
    const uint8_t *                                     src2,
    void *                                              dst,
    const MMALIB_CNN_tensor_convert_ixX_oxX_ExecInArgs *pKerInArgs,
    MMALIB_CNN_tensor_convert_ixX_oxX_ExecOutArgs *     pKerOutArgs);

/*!
 *  @brief       This function checks the parameters and should be called
 *               before kernel executuon. It can be called once
 *
 *  @param [in]  handle      : Active handle to the kernel
 *  @param [in]  src[]       : Pointer to buffer holding input feature map
 *  @param [out] dst[]       : Pointer to buffer holding output feature map
 *
 *  @return      Status of success or Error with Error Codes
 *
 *  @remarks     None
 */

MMALIB_STATUS
MMALIB_CNN_tensor_convert_ixX_oxX_exec_checkParams (
    MMALIB_kernelHandle                                 handle,
    const void *                                        src0,
    const void *                                        src1,
    const uint8_t *                                     src2,
    void *                                              dst,
    const MMALIB_CNN_tensor_convert_ixX_oxX_ExecInArgs *pKerInArgs,
    MMALIB_CNN_tensor_convert_ixX_oxX_ExecOutArgs *     pKerOutArgs);

/** @} */
#ifdef __cplusplus
}
#endif
#endif /* _MMALIB_CNN_TENSOR_CONVERT_IXX_IXX_OXX_H_ */

/* ======================================================================== */
/*  End of file:  MMALIB_CNN_tensor_convert_ixX_oxX.h                     */
/* ======================================================================== */
