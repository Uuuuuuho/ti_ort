/******************************************************************************
*                                                                             *
* module name       :MMALIB                                                   *
*                                                                             *
* module descripton :Matrix Multiply Accelerator Library module for C7x+MMA   *
*                                                                             *
* Copyright (C) 2017-2018 Texas Instruments Incorporated - http://www.ti.com/ *
* ALL RIGHTS RESERVED                                                         *
*                                                                             *
******************************************************************************/

/**
  ******************************************************************************
  *  @file     mmalib_dsp.h
  *
  *  @brief    File to hold buffer parameter related info for MMALIB
  *
  *  @version  0.1 - Jan 2018 : Initial Version with as a template [PKS]
  *
 *******************************************************************************
*/
#ifndef _MMALIB_DSP_H_
#define _MMALIB_DSP_H_

#include "MMALIB_DSP_fir_ixX_ixX_oxX/MMALIB_DSP_fir_ixX_ixX_oxX.h"
#include "MMALIB_DSP_firSmall_ixX_ixX_oxX/MMALIB_DSP_firSmall_ixX_ixX_oxX.h"
#endif /*_MMALIB_DSP_H_*/

/*!
 * \defgroup MMALIB_DSP Digital Signal Processing (DSP) kernels
 * \brief This module consists of kernels that implement DSP
 *        algorithms.
 *
 */
