/*******************************************************************************
**+--------------------------------------------------------------------------+**
**|                            ****                                          |**
**|                            ****                                          |**
**|                            ******o***                                    |**
**|                      ********_///_****                                   |**
**|                      ***** /_//_/ ****                                   |**
**|                       ** ** (__/ ****                                    |**
**|                           *********                                      |**
**|                            ****                                          |**
**|                            ***                                           |**
**|                                                                          |**
**|         Copyright (c) 2017 Texas Instruments Incorporated                |**
**|                        ALL RIGHTS RESERVED                               |**
**|                                                                          |**
**| Permission to use, copy, modify, or distribute this software,            |**
**| whether in part or in whole, for any purpose is forbidden without        |**
**| a signed licensing agreement and NDA from Texas Instruments              |**
**| Incorporated (TI).                                                       |**
**|                                                                          |**
**| TI makes no representation or warranties with respect to the             |**
**| performance of this computer program, and specifically disclaims         |**
**| any responsibility for any damages, special or consequential,            |**
**| connected with the use of this program.                                  |**
**|                                                                          |**
**+--------------------------------------------------------------------------+**
*******************************************************************************/

#include <math.h>
#include <mmalib.h>

// include test infrastructure provided by MMALIB
#include "../../test/MMALIB_test.h"

#include "MMALIB_fft1dBatched_i16sc_c16sc_o16sc.h"
#include "MMALIB_fft1dBatched_i16sc_c16sc_o16sc_cn.h"
#include "MMALIB_fft1dBatched_i16sc_c16sc_o16sc_idat.h"

__attribute__ ((section (".msmcData"), aligned (64)))
int8_t msmcBuffer[6 * 1024 * 1024];
__attribute__ ((section (".ddrData"), aligned (64)))
int8_t ddrBuffer[2048 * 1024];

#pragma DATA_SECTION(MMALIB_fft1dBatched_i16sc_c16sc_o16sc_pBlock, ".pinit")
uint8_t MMALIB_fft1dBatched_i16sc_c16sc_o16sc_pBlock
    [MMALIB_FFT1DBATCHED_I16SC_C16SC_O16SC_PBLOCK_SIZE];

int16_t volatile volatileSum = 0;

static int16_t float2short (MMALIB_D64 x)
{
   x = floor (0.5 + x); // Explicit rounding to integer //
   if (x >= 32767.0)
      return 32767;
   if (x <= -32768.0)
      return -32768;
   return (int16_t) x;
}

void tw_gen (int16_t *pW, uint32_t numPoints)
{
   int32_t          i, j, k, t;
   const MMALIB_D64 PI   = 3.141592654;
   MMALIB_D64 twF2sScale = 32767.5; /* Scale twiddle factors (max abs value of
                                     * 1) to use full capacity of int16_t */

   t = numPoints >> 2;
   for (j = 1, k = 0; j <= numPoints >> 2; j = j << 2) {
      for (i = 0; i < numPoints >> 2; i += j) {
         /* TODO: Big endian requires different format of Twiddle factors? */
         pW[k]     = float2short (twF2sScale * cos (2 * PI * i / numPoints));
         pW[k + 1] = float2short (twF2sScale * (-sin (2 * PI * i / numPoints)));
         pW[k + 2 * t] =
             float2short (twF2sScale * cos (4 * PI * i / numPoints));
         pW[k + 2 * t + 1] =
             float2short (twF2sScale * (-sin (4 * PI * i / numPoints)));
         pW[k + 4 * t] =
             float2short (twF2sScale * cos (6 * PI * i / numPoints));
         pW[k + 4 * t + 1] =
             float2short (twF2sScale * (-sin (6 * PI * i / numPoints)));
         k += 2;
      }
      k += (t) *4;
      t = t >> 2;
   }
}

int MMALIB_fft1dBatched_i16sc_c16sc_o16sc_d (uint32_t *pProfile,
                                             uint8_t   LevelOfFeedback)
{
   int32_t                                      tpi;
   fft1dBatched_i16sc_c16sc_o16sc_testParams_t *prm;

   uint32_t testNum = 0;     // test case id
   int32_t  currentTestFail; // status of current test case
   int32_t  fail = 0;        // fail flag

   fft1dBatched_i16sc_c16sc_o16sc_getTestParams (&prm, &test_cases);

   TI_profile_init ("MMALIB_fft1dBatched_i16sc_c16sc_o16sc");

   for (tpi = 0; tpi < test_cases; tpi++) {

      int32_t       status_nat_vs_opt = TI_TEST_KERNEL_PASS;
      int32_t       status_nat_vs_ref = TI_TEST_KERNEL_PASS;
      MMALIB_STATUS status_opt        = MMALIB_SUCCESS;
      MMALIB_STATUS status_nat        = MMALIB_SUCCESS;
      uint32_t      numPoints;
      uint32_t      numChannels;
      uint32_t      numShifts;
      uint32_t      dataMemSize;

      int16_t * pX;
      int16_t * pY;
      int16_t * pXCn;
      int16_t * pYCn;
      int16_t * pW;
      uint32_t *pShift;

      testNum     = prm[tpi].testID;
      numPoints   = prm[tpi].numPoints;
      numChannels = prm[tpi].numChannels;
      numShifts   = prm[tpi].numShifts;

      dataMemSize = numChannels * numPoints < 128
                        ? /* Kernel requires input/output */
                        128 * 2
                        : numChannels * numPoints * 2; /* buffers to be atleast
                                                        * 128 elements long */

      pX = (int16_t *) TI_memalign (
          128,                             /* pX is required to be*/
          dataMemSize * sizeof (int16_t)); /* 16-byte aligned for
                                            * streaming engine use
                                            * in kernel           */
      pY     = (int16_t *) TI_memalign (128, dataMemSize * sizeof (int16_t));
      pW     = (int16_t *) TI_memalign (128, numPoints * 2 * sizeof (int16_t));
      pShift = (uint32_t *) TI_memalign (128, numShifts * sizeof (uint32_t));
      /* pXCn   = (int16_t *) malloc (dataMemSize * sizeof (int16_t)); */
      /* pYCn   = (int16_t *) malloc (dataMemSize * sizeof (int16_t)); */
      /* pShift = (uint32_t *) TI_malloc (numShifts * sizeof (uint32_t)); */
      /* pY = (int16_t *) msmcBuffer; */
      pXCn = (int16_t *) msmcBuffer;
      pYCn = pXCn + dataMemSize;
      /* pY   = pYCn + dataMemSize; */

      if (pX && pY && pW && pXCn && pYCn && pShift) {
         int32_t              fail;
         MMALIB_bufParams1D_t bufParamsData;
         MMALIB_bufParams1D_t bufParamsShift;
         MMALIB_bufParams1D_t bufParamsTw;
         int32_t              bufWidth, bufHeight;

         /* Utilities to copy buffers assume 16-bit types for buffer width,
          * height and stride. Hence, set width so that
          * stride = width*elementSize occupies less than 16 bits. */
         bufWidth  = numChannels * numPoints * 2;
         bufWidth  = bufWidth > 0x0800 ? 0x0800 : bufWidth;
         bufHeight = numChannels * numPoints * 2 / bufWidth;

         TI_fillBuffer (prm[tpi].testPattern,
                        0,
                        pX,
                        prm[tpi].staticIn0,
                        bufWidth,
                        bufHeight,
                        bufWidth * sizeof (int16_t),
                        sizeof (int16_t),
                        testPatternString);
         bufParamsData.dim_x     = dataMemSize;
         bufParamsData.data_type = MMALIB_INT16;

         /* Save data before calling kernel function because kernel
          * writes into input buffer                                */
         TI_fillBuffer (STATIC,
                        0,
                        pXCn,
                        pX,
                        bufWidth,
                        bufHeight,
                        bufWidth * sizeof (int16_t),
                        sizeof (int16_t),
                        testPatternString);

         bufParamsShift.dim_x     = numShifts;
         bufParamsShift.data_type = MMALIB_UINT32;
         TI_fillBuffer (STATIC,
                        0, /* Shift vector is always STATIC */
                        pShift,
                        prm[tpi].shiftVector,
                        numShifts,
                        1,
                        1,
                        sizeof (uint32_t),
                        testPatternString);

         tw_gen (pW, numPoints);
         bufParamsTw.dim_x     = numPoints * 2;
         bufParamsTw.data_type = MMALIB_INT16;

         TI_profile_start (TI_PROFILE_KERNEL_INIT);
         status_opt = MMALIB_fft1dBatched_i16sc_c16sc_o16sc_init (
             (int16_t *) pX,
             &bufParamsData,
             (int16_t *) pW,
             &bufParamsTw,
             (int16_t *) pY,
             &bufParamsData,
             (uint32_t *) pShift,
             &bufParamsShift,
             numPoints,
             numChannels,
             MMALIB_fft1dBatched_i16sc_c16sc_o16sc_pBlock);
         TI_profile_stop ();

         TI_profile_start (TI_PROFILE_KERNEL_OPT);
         status_opt = MMALIB_fft1dBatched_i16sc_c16sc_o16sc_kernel (
             (int16_t *) pX,
             &bufParamsData,
             (int16_t *) pW,
             &bufParamsTw,
             (int16_t *) pY,
             &bufParamsData,
             (uint32_t *) pShift,
             &bufParamsShift,
             numPoints,
             numChannels,
             MMALIB_fft1dBatched_i16sc_c16sc_o16sc_pBlock);
         TI_profile_stop ();

         /* Re-copy data before calling kernel function because kernel
          * writes into input buffer                                */
         TI_fillBuffer (STATIC,
                        0,
                        pX,
                        pXCn,
                        bufWidth,
                        bufHeight,
                        bufWidth * sizeof (int16_t),
                        sizeof (int16_t),
                        testPatternString);

         TI_profile_start (TI_PROFILE_KERNEL_OPT_WARM);
         status_opt = MMALIB_fft1dBatched_i16sc_c16sc_o16sc_kernel (
             (int16_t *) pX,
             &bufParamsData,
             (int16_t *) pW,
             &bufParamsTw,
             (int16_t *) pY,
             &bufParamsData,
             (uint32_t *) pShift,
             &bufParamsShift,
             numPoints,
             numChannels,
             MMALIB_fft1dBatched_i16sc_c16sc_o16sc_pBlock);
         TI_profile_stop ();

         // get output to L1D
         uint32_t row    = 0;
         int32_t  col    = 0;
         int16_t  outSum = 0;
         // treat output as bytes to be data type agnostic
         int16_t *pDst = (int16_t *) pY;
         for (row = 0; row < numChannels; row++) {
            for (col = 0; col < numPoints; col++) {
               outSum += *pDst;
               pDst++;
            }
         }

         /* dummy store of outSum to insure that the compiler does
not remove
          * it. */
         volatileSum = outSum;

         /* Re-copy data before calling kernel function because kernel
          * writes into input buffer                                */
         TI_fillBuffer (STATIC,
                        0,
                        pX,
                        pXCn,
                        bufWidth,
                        bufHeight,
                        bufWidth * sizeof (int16_t),
                        sizeof (int16_t),
                        testPatternString);

         TI_profile_start (TI_PROFILE_KERNEL_OPT_WARMWRB);
         status_opt = MMALIB_fft1dBatched_i16sc_c16sc_o16sc_kernel (
             (int16_t *) pX,
             &bufParamsData,
             (int16_t *) pW,
             &bufParamsTw,
             (int16_t *) pY,
             &bufParamsData,
             (uint32_t *) pShift,
             &bufParamsShift,
             numPoints,
             numChannels,
             MMALIB_fft1dBatched_i16sc_c16sc_o16sc_pBlock);
         TI_profile_stop ();

         TI_profile_start (TI_PROFILE_KERNEL_CN);
         status_nat =
             MMALIB_fft1dBatched_i16sc_c16sc_o16sc_cn ((int16_t *) pXCn,
                                                       &bufParamsData,
                                                       (int16_t *) pW,
                                                       &bufParamsTw,
                                                       (int16_t *) pYCn,
                                                       &bufParamsData,
                                                       (uint32_t *) pShift,
                                                       &bufParamsShift,
                                                       numPoints,
                                                       numChannels);
         TI_profile_stop ();

         status_nat_vs_opt =
             TI_compare_mem ((void *) pY,
                             (void *) pYCn,
                             numChannels * numPoints * 2 * sizeof (int16_t));
         if (prm[tpi].staticOut != NULL) {
            status_nat_vs_ref =
                TI_compare_mem ((void *) prm[tpi].staticOut,
                                (void *) pYCn,
                                numChannels * numPoints * 2 * sizeof (int16_t));
         }

         currentTestFail =
             ((status_nat_vs_opt == TI_TEST_KERNEL_FAIL) ||
              (status_nat_vs_ref == TI_TEST_KERNEL_FAIL) ||
              (status_opt != MMALIB_SUCCESS) || (status_nat != MMALIB_SUCCESS))
                 ? 1
                 : 0;

         fail = ((fail == 1) || (currentTestFail == 1)) ? 1 : 0;

         pProfile[3 * tpi] =
             (int32_t) TI_profile_get_cycles (TI_PROFILE_KERNEL_OPT);
         pProfile[3 * tpi + 1] =
             (int32_t) TI_profile_get_cycles (TI_PROFILE_KERNEL_OPT_WARM);
         pProfile[3 * tpi + 2] =
             (int32_t) TI_profile_get_cycles (TI_PROFILE_KERNEL_OPT_WARMWRB);
         /* est_test = 1; */
         sprintf (desc,
                  "%s generated input | numPoints=%d, numChannels=%d",
                  testPatternString,
                  numPoints,
                  numChannels);

         uint64_t archCycles = 0, estCycles = 0;
         TI_profile_add_test (testNum++,
                              numPoints * numChannels,
                              archCycles,
                              estCycles,
                              currentTestFail,
                              desc);
      }
      else {
         sprintf (desc, "numPoints=%d, numChannels=%d", numPoints, numChannels);
         TI_profile_skip_test (desc);
         TI_profile_clear_run_stats ();
      }

      /* Free buffers for each test vector */
      TI_align_free (pX);
      TI_align_free (pY);
      TI_align_free (pW);
      /* free (pXCn); */
      /* free (pYCn); */
   }

   return fail;
}

int test_main (uint32_t *pProfile)
{
#if !defined(_HOST_BUILD)
   if (TI_cache_init ()) {
      TI_memError ("MMALIB_fft1dBatched_i16sc_c16sc_o16sc");
      return 1;
   }
   else
#else
   printf ("_HOST_BUILD is defined.\n");
#endif //_HOST_BUILD
   {
      return MMALIB_fft1dBatched_i16sc_c16sc_o16sc_d (&pProfile[0], 0);
   }
}

/* Main call for individual test projects */
int main ()
{
   int fail = 1;

   uint32_t profile[256 * 3];

   MMALIB_TEST_init ();

   fail = test_main (&profile[0]);

#if !defined(NO_PRINTF)
   if (fail == 0)
      printf ("Test Pass!\n");
   else
      printf ("Test Fail!\n");

   int i;
   for (i = 0; i < test_cases; i++) {
      printf ("Test %4d: Cold Cycles = %8d, Warm Cycles = %8d, Warm Cycles WRB "
              "= %8d\n",
              i,
              profile[3 * i],
              profile[3 * i + 1],
              profile[3 * i + 2]);
   }
#endif

   return fail;
}

/* ======================================================================== */
/*  End of file:  MMALIB_fft1dBatched_i16sc_c16sc_o16sc_d.c */
/* ======================================================================== */
