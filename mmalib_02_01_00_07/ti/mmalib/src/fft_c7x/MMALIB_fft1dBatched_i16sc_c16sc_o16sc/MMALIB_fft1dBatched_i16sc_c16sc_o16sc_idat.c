/*******************************************************************************
 **+--------------------------------------------------------------------------+**
 **|                            **** |**
 **|                            **** |**
 **|                            ******o*** |**
 **|                      ********_///_**** |**
 **|                      ***** /_//_/ **** |**
 **|                       ** ** (__/ **** |**
 **|                           ********* |**
 **|                            **** |**
 **|                            *** |**
 **| |**
 **|         Copyright (c) 2017 Texas Instruments Incorporated |**
 **|                        ALL RIGHTS RESERVED |**
 **| |**
 **| Permission to use, copy, modify, or distribute this software, |**
 **| whether in part or in whole, for any purpose is forbidden without |**
 **| a signed licensing agreement and NDA from Texas Instruments |**
 **| Incorporated (TI). |**
 **| |**
 **| TI makes no representation or warranties with respect to the |**
 **| performance of this computer program, and specifically disclaims |**
 **| any responsibility for any damages, special or consequential, |**
 **| connected with the use of this program. |**
 **| |**
 **+--------------------------------------------------------------------------+**
 *******************************************************************************/
#include "MMALIB_fft1dBatched_i16sc_c16sc_o16sc_idat.h"
/* #include "../../common/TI_memory.h" */

__attribute__ ((section (".staticData"))) static int16_t staticRefInput4[] = {
    -108, -126, 109,  86,   56,  62,   -46, 8,    -88, -101, -78, -127, 67,   -33, -12, 97,  117, 109,  32,   91,  -44, -118, 117,  72,  20,  -43,
    -66,  -77,  82,   19,   31,  -84,  -22, -115, -46, -114, -30, -73,  -115, 25,  118, 25,  -18, 92,   61,   90,  67,  -39,  -21,  88,  11,  85,
    -72,  86,   34,   -107, -11, -113, -54, 125,  -37, 3,    86,  38,   -124, 89,  12,  90,  9,   -13,  -12,  22,  14,  17,   -109, 84,  22,  -16,
    2,    -79,  128,  76,   52,  -53,  -38, -105, 113, 106,  63,  -110, -85,  -68, -50, -82, 71,  -124, 93,   -16, 42,  -122, 78,   -70, -64, -45,
    -118, 57,   -127, 124,  -17, -65,  117, -40,  12,  68,   -70, -76,  6,    -18, -96, -3,  -70, -113, -111, 51,  92,  -8,   104,  -108};
__attribute__ ((section (".staticData"))) static uint32_t shiftVector4[]     = {0, 0, 0};
__attribute__ ((section (".staticData"))) static int16_t  staticRefOutput4[] = {
    79,    -619, 373,  -719, -638,  213,  520,  -711, 370,   -615,  -225, 264,   452,   1007,  5,     -521, 1068, 180,  1451,  600,  150,   303,
    539,   -566, -343, -890, 284,   -710, -261, -789, -278,  -296,  270,  1036,  -1357, -143,  169,   -136, -35,  -922, -617,  -426, 197,   8,
    97,    866,  894,  -56,  -491,  567,  -484, -383, -1229, -1178, -268, -37,   -622,  419,   138,   -358, -85,  -217, 242,   -98,  297,   -479,
    139,   -185, -830, -421, 202,   585,  42,   -661, -359,  634,   -464, -275,  133,   -651,  -1088, 1438, -153, 106,  380,   87,   -725,  -156,
    -1361, -738, 98,   -272, -1301, 595,  -14,  -104, -634,  -770,  -347, -125,  -733,  -1228, 157,   -276, 125,  -30,  -1433, -466, -1001, 218,
    312,   -540, 1099, 31,   138,   369,  539,  1080, 706,   -381,  -770, -1131, 212,   -220,  -93,   1379, -550, -550};

__attribute__ ((section (".staticData"))) static uint32_t shiftVector27[] = {0, 1, 1, 0, 1};
__attribute__ ((section (".staticData"))) static uint32_t shiftVector28[] = {1, 1, 1, 1};
__attribute__ ((section (".staticData"))) static uint32_t shiftVector29[] = {1, 1, 0};
__attribute__ ((section (".staticData"))) static uint32_t shiftVector30[] = {1, 1};
__attribute__ ((section (".staticData"))) static uint32_t shiftVector31[] = {1, 0};
__attribute__ ((section (".staticData"))) static uint32_t shiftVector32[] = {0, 0};
__attribute__ ((section (".staticData"))) static uint32_t shiftVector33[] = {0, 1};

__attribute__ ((section (".staticData"))) static uint32_t shiftVector16Point[]   = {0, 0};
__attribute__ ((section (".staticData"))) static uint32_t shiftVector32Point[]   = {0, 0};
__attribute__ ((section (".staticData"))) static uint32_t shiftVector64Point[]   = {0, 0, 0};
__attribute__ ((section (".staticData"))) static uint32_t shiftVector128Point[]  = {0, 0, 0};
__attribute__ ((section (".staticData"))) static uint32_t shiftVector256Point[]  = {0, 0, 0, 0};
__attribute__ ((section (".staticData"))) static uint32_t shiftVector512Point[]  = {0, 0, 0, 0};
__attribute__ ((section (".staticData"))) static uint32_t shiftVector1024Point[] = {0, 0, 0, 0, 0};
__attribute__ ((section (".staticData"))) static uint32_t shiftVector2048Point[] = {0, 0, 0, 0, 0};
__attribute__ ((section (".staticData"))) static uint32_t shiftVector4096Point[] = {0, 0, 0, 0, 0, 0};
__attribute__ ((section (".staticData"))) static uint32_t shiftVector8192Point[] = {0, 0, 0, 0, 0, 0};

__attribute__ ((section (".staticData"))) static fft1dBatched_i16sc_c16sc_o16sc_testParams_t testParams[] = {
/********************************************
    {
       testPattern,
       *staticIn0, staticIn1, staticOut
       width, height, stride
    },
*********************************************/

    {STATIC, staticRefInput4, shiftVector4, staticRefOutput4, 3, 1, 64, 0},

    {
        RANDOM,
        NULL,
        shiftVector512Point,
        NULL,
        4,
        64,
        512,
        66,
    },

    {
        RANDOM,
        NULL,
        shiftVector32Point,
        NULL,
        2,
        256,
        32,
        95,
    },
};

/*
 *  Sends the test parameter structure and number of tests
 */
void fft1dBatched_i16sc_c16sc_o16sc_getTestParams (fft1dBatched_i16sc_c16sc_o16sc_testParams_t **params, int32_t *numTests)
{
   *params   = testParams;
   *numTests = sizeof (testParams) / sizeof (fft1dBatched_i16sc_c16sc_o16sc_testParams_t);
}

/* ======================================================================== */
/*                       End of file                                        */
/* ------------------------------------------------------------------------ */
/*            Copyright (c) 2017 Texas Instruments, Incorporated.           */
/*                           All Rights Reserved.                           */
/* ======================================================================== */
