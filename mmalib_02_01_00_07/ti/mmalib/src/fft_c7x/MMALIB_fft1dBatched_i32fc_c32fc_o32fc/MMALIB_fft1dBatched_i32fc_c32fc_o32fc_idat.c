#include "MMALIB_fft1dBatched_i32fc_c32fc_o32fc_idat.h"

static fft1d_testParams_t testParams[] = {
    {
        RANDOM, // test pattern
        NULL,   // staticIn0
        NULL,   // staticIn1
        256,    // numChannels
        32,     // numPoints
        27,     // test ID
    },

    {
        RANDOM, // test pattern
        NULL,   // staticIn0
        NULL,   // staticIn1
        64,     // numChannels
        64,     // numPoints
        48,     // test ID
    },

    {
        RANDOM, // test pattern
        NULL,   // staticIn0
        NULL,   // staticIn1
        64,     // numChannels
        128,    // numPoints
        55,     // test ID
    },

/* #if (defined(ALL_TEST_CASES) || (TEST_CATEGORY == 300) ||                      \ */
/*      (TEST_CATEGORY == 100) || (EXTENDED_TEST_CASE == 76)) */
/*     { */
/*         RANDOM_SIGNED, // test pattern */
/*         NULL,   // staticIn0 */
/*         NULL,   // staticIn1 */
/*         64,     // numChannels */
/*         1024,   // numPoints */
/*         76,     // test ID */
/*     }, */
/* #endif */

/* #if (defined(ALL_TEST_CASES) || (TEST_CATEGORY == 300) ||                      \ */
/*      (TEST_CATEGORY == 100) || (EXTENDED_TEST_CASE == 82)) */
/*     { */
/*         RANDOM, // test pattern */
/*         NULL,   // staticIn0 */
/*         NULL,   // staticIn1 */
/*         32,     // numChannels */
/*         2048,   // numPoints */
/*         82,     // test ID */
/*     }, */
/* #endif */

/* #if (defined(ALL_TEST_CASES) || (TEST_CATEGORY == 300) ||                      \ */
/*      (TEST_CATEGORY == 100) || (EXTENDED_TEST_CASE == 83)) */
/*     { */
/*         RANDOM, // test pattern */
/*         NULL,   // staticIn0 */
/*         NULL,   // staticIn1 */
/*         64,     // numChannels */
/*         2048,   // numPoints */
/*         83,     // test ID */
/*     }, */
/* #endif */

/* #if (defined(ALL_TEST_CASES) || (TEST_CATEGORY == 300) ||                      \ */
/*      (TEST_CATEGORY == 100) || (EXTENDED_TEST_CASE == 88)) */
/*     { */
/*         RANDOM, // test pattern */
/*         NULL,   // staticIn0 */
/*         NULL,   // staticIn1 */
/*         16,     // numChannels */
/*         4096,   // numPoints */
/*         88,     // test ID */
/*     }, */
/* #endif */

/* #if (defined(ALL_TEST_CASES) || (TEST_CATEGORY == 300) ||                      \ */
/*      (TEST_CATEGORY == 100) || (EXTENDED_TEST_CASE == 89)) */
/*     { */
/*         RANDOM, // test pattern */
/*         NULL,   // staticIn0 */
/*         NULL,   // staticIn1 */
/*         32,     // numChannels */
/*         4096,   // numPoints */
/*         89,     // test ID */
/*     }, */
/* #endif */

/* #if (defined(ALL_TEST_CASES) || (TEST_CATEGORY == 300) ||                      \ */
/*      (TEST_CATEGORY == 100) || (EXTENDED_TEST_CASE == 90)) */
/*     { */
/*         RANDOM, // test pattern */
/*         NULL,   // staticIn0 */
/*         NULL,   // staticIn1 */
/*         64,     // numChannels */
/*         4096,   // numPoints */
/*         90,     // test ID */
/*     }, */
/* #endif */

    /* #if (defined(ALL_TEST_CASES) || (TEST_CATEGORY == 200) ||                      \ */
    /*      (TEST_CATEGORY == 100) || (EXTENDED_TEST_CASE == 97)) */
    /*     { */
    /*         RANDOM, // test pattern */
    /*         NULL,   // staticIn0 */
    /*         NULL,   // staticIn1 */
    /*         64,     // numChannels */
    /*         8192,   // numPoints */
    /*         97,     // test ID */
    /*     }, */
    /* #endif */

};

/*
 *  Sends the test parameter structure and number of tests
 */
void fft1d_getTestParams (fft1d_testParams_t **params, int32_t *numTests)
{
   *params   = testParams;
   *numTests = sizeof (testParams) / sizeof (fft1d_testParams_t);
}
