/******************************************************************************
*                                                                             *
* module name       :MMALIB                                                   *
*                                                                             *
* module descripton :Matrix Multiply Accelerator Library module for C7x+MMA   *
*                                                                             *
* Copyright (C) 2017-2018 Texas Instruments Incorporated - http://www.ti.com/ *
* ALL RIGHTS RESERVED                                                         *
*                                                                             *
******************************************************************************/

/**
  ******************************************************************************
  *  @file     c7x_fft.h
  *
  *  @brief    File to hold buffer parameter related info for MMALIB
  *
  *  @version  0.1 - May 2021 : Initial Version with as a template [PKS]
  *
 *******************************************************************************
*/

#ifndef _C7X_FFT_H_
#define _C7X_FFT_H_
#include "MMALIB_fft1dBatched_i32fc_c32fc_o32fc/MMALIB_fft1dBatched_i32fc_c32fc_o32fc.h"
#include "MMALIB_fft1dBatched_i16sc_c16sc_o16sc/MMALIB_fft1dBatched_i16sc_c16sc_o16sc.h"
#include "MMALIB_fft1d_i32fc_c32fc_o32fc/MMALIB_fft1d_i32fc_c32fc_o32fc.h"
#include "MMALIB_fft1d_i16sc_c16sc_o16sc/MMALIB_fft1d_i16sc_c16sc_o16sc.h"
#endif /*_C7X_FFT_H_*/
