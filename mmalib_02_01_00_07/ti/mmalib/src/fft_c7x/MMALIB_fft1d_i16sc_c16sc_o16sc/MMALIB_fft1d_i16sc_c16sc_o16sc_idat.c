/*******************************************************************************
 **+--------------------------------------------------------------------------+**
 **|                            **** |**
 **|                            **** |**
 **|                            ******o*** |**
 **|                      ********_///_**** |**
 **|                      ***** /_//_/ **** |**
 **|                       ** ** (__/ **** |**
 **|                           ********* |**
 **|                            **** |**
 **|                            *** |**
 **| |**
 **|         Copyright (c) 2017 Texas Instruments Incorporated |**
 **|                        ALL RIGHTS RESERVED |**
 **| |**
 **| Permission to use, copy, modify, or distribute this software, |**
 **| whether in part or in whole, for any purpose is forbidden without |**
 **| a signed licensing agreement and NDA from Texas Instruments |**
 **| Incorporated (TI). |**
 **| |**
 **| TI makes no representation or warranties with respect to the |**
 **| performance of this computer program, and specifically disclaims |**
 **| any responsibility for any damages, special or consequential, |**
 **| connected with the use of this program. |**
 **| |**
 **+--------------------------------------------------------------------------+**
 *******************************************************************************/
#include "MMALIB_fft1d_i16sc_c16sc_o16sc_idat.h"
/* #include "../../common/TI_memory.h" */

static int16_t staticRefInput4[] = {
    -108, -126, 109,  86,   56,  62,   -46, 8,    -88, -101, -78, -127, 67,   -33, -12, 97,  117, 109,  32,   91,  -44, -118, 117,  72,  20,  -43,
    -66,  -77,  82,   19,   31,  -84,  -22, -115, -46, -114, -30, -73,  -115, 25,  118, 25,  -18, 92,   61,   90,  67,  -39,  -21,  88,  11,  85,
    -72,  86,   34,   -107, -11, -113, -54, 125,  -37, 3,    86,  38,   -124, 89,  12,  90,  9,   -13,  -12,  22,  14,  17,   -109, 84,  22,  -16,
    2,    -79,  128,  76,   52,  -53,  -38, -105, 113, 106,  63,  -110, -85,  -68, -50, -82, 71,  -124, 93,   -16, 42,  -122, 78,   -70, -64, -45,
    -118, 57,   -127, 124,  -17, -65,  117, -40,  12,  68,   -70, -76,  6,    -18, -96, -3,  -70, -113, -111, 51,  92,  -8,   104,  -108};
/* static uint32_t shiftVector4[]     = {0, 0, 0}; */
static int16_t staticRefOutput4[] = {
    79,    -619, 373,  -719, -638,  213,  520,  -711, 370,   -615,  -225, 264,   452,   1007,  5,     -521, 1068, 180,  1451,  600,  150,   303,
    539,   -566, -343, -890, 284,   -710, -261, -789, -278,  -296,  270,  1036,  -1357, -143,  169,   -136, -35,  -922, -617,  -426, 197,   8,
    97,    866,  894,  -56,  -491,  567,  -484, -383, -1229, -1178, -268, -37,   -622,  419,   138,   -358, -85,  -217, 242,   -98,  297,   -479,
    139,   -185, -830, -421, 202,   585,  42,   -661, -359,  634,   -464, -275,  133,   -651,  -1088, 1438, -153, 106,  380,   87,   -725,  -156,
    -1361, -738, 98,   -272, -1301, 595,  -14,  -104, -634,  -770,  -347, -125,  -733,  -1228, 157,   -276, 125,  -30,  -1433, -466, -1001, 218,
    312,   -540, 1099, 31,   138,   369,  539,  1080, 706,   -381,  -770, -1131, 212,   -220,  -93,   1379, -550, -550};

static int16_t staticRefInput6[] = {
    -34,  -25,  122,  15,   77,   -89,  -89,  -123, -60,  -90,  13,   59,   7,    -66,  -10,  35,   -77,  114,  95,   114,  114,  -41,  57,   40,
    -67,  -67,  29,   -105, 120,  112,  19,   105,  -36,  -122, 126,  40,   -118, 77,   87,   -62,  -77,  -4,   123,  -33,  -41,  -49,  123,  -89,
    -4,   57,   -105, -26,  49,   85,   -104, -84,  122,  12,   3,    17,   -82,  -91,  -3,   -53,  112,  -126, -17,  -97,  -22,  15,   -21,  -101,
    127,  86,   -61,  -99,  -22,  -119, -106, 107,  -39,  32,   107,  -52,  120,  62,   -103, 89,   -47,  59,   -39,  12,   39,   -78,  117,  63,
    37,   49,   92,   -17,  -81,  -58,  108,  -40,  10,   125,  84,   103,  -50,  67,   -22,  -51,  55,   107,  -91,  -4,   61,   -78,  106,  27,
    -123, 55,   108,  44,   -96,  82,   32,   26,   -15,  -17,  124,  -118, -95,  41,   -39,  27,   -108, 65,   100,  -6,   -122, 85,   27,   95,
    -71,  -69,  -30,  -67,  -18,  -33,  58,   51,   28,   66,   -113, 122,  -125, 120,  -18,  -88,  96,   19,   -121, -92,  61,   28,   92,   -40,
    90,   34,   -57,  50,   -112, -105, -51,  -38,  -24,  93,   71,   91,   -92,  119,  -18,  -58,  92,   12,   62,   -78,  -110, -118, 126,  4,
    -97,  -34,  102,  -62,  -9,   121,  102,  7,    -62,  -4,   100,  99,   60,   37,   112,  84,   -86,  118,  -127, -10,  42,   69,   21,   68,
    8,    -74,  111,  19,   58,   120,  126,  -51,  -117, -65,  2,    -107, -105, 10,   -120, 42,   60,   63,   126,  -107, -126, 37,   74,   48,
    -106, -14,  124,  -28,  -73,  82,   106,  -112, 7,    11,   120,  65,   58,   -106, 102,  10,   8,    -33,  33,   35,   -82,  -15,  126,  8,
    -5,   -24,  95,   113,  115,  60,   -42,  67,   65,   -83,  -34,  104,  -110, -91,  -74,  -93,  3,    43,   -52,  -71,  -120, -109, -77,  26,
    46,   46,   100,  -123, 73,   -74,  -46,  83,   80,   81,   35,   -34,  -100, 91,   -91,  -3,   72,   55,   -18,  -8,   -77,  -87,  63,   -85,
    66,   -10,  25,   -43,  -54,  121,  -118, 4,    -23,  -39,  -98,  71,   36,   -125, -21,  41,   -43,  31,   -120, -100, -96,  -118, -115, 66,
    -90,  55,   -18,  17,   104,  72,   -12,  20,   126,  -55,  -70,  121,  -37,  25,   -111, -40,  -107, -97,  91,   -21,  54,   9,    101,  68,
    96,   -74,  68,   -65,  50,   31,   63,   7,    -94,  -22,  -90,  -57,  -8,   -39,  33,   -121, 117,  -11,  60,   127,  49,   -10,  -26,  -66,
    42,   86,   -98,  -109, 32,   -30,  -14,  106,  -122, -67,  84,   -25,  128,  2,    93,   126,  -87,  115,  66,   5,    -27,  -24,  -47,  -114,
    -88,  122,  40,   -84,  108,  -102, -23,  -76,  39,   -39,  20,   -67,  107,  115,  -63,  -121, 32,   -63,  -65,  -70,  -126, -93,  -97,  78,
    -112, -37,  116,  -46,  -123, 117,  -91,  57,   -40,  39,   -67,  -65,  117,  85,   -31,  -17,  -124, 110,  -113, -31,  -45,  -13,  -56,  105,
    -62,  -26,  -122, 105,  -32,  -100, -90,  -119, 50,   15,   -108, 25,   5,    49,   118,  -60,  76,   69,   115,  112,  31,   0,    79,   -15,
    -38,  62,   82,   -38,  109,  -127, 3,    -114, 101,  30,   -88,  -30,  45,   87,   100,  -83,  -82,  -55,  -14,  31,   -14,  19,   22,   71,
    11,   -120, -3,   -36,  -125, 39,   29,   70,   -10,  -111, 79,   99,   -28,  120,  79,   38,   2,    68,   59,   -4,   -63,  46,   -78,  -96,
    -44,  -92,  112,  71,   82,   21,   -38,  -32,  2,    -40,  -28,  -33,  -104, 110,  115,  -17,  -44,  -24,  7,    -19,  -109, 54,   -28,  -33,
    -63,  98,   -8,   -117, -12,  72,   99,   -114, 32,   -57,  -32,  121,  115,  -123, -95,  18,   19,   -70,  -46,  -99,  68,   -71,  60,   -76,
    59,   19,   105,  90,   39,   -76,  -121, -106, -74,  -15,  -42,  -97,  102,  13,   118,  14,   -27,  -69,  -61,  -68,  99,   82,   -81,  -30,
    107,  22,   -48,  63,   13,   109,  97,   -120, -104, 1,    -101, 57,   -25,  -115, -47,  -26,  -110, 9,    -120, -22,  84,   15,   -124, -83,
    8,    85,   -113, -120, 94,   -4,   109,  -39,  -27,  -94,  -46,  41,   105,  -89,  33,   -70,  112,  108,  26,   70,   64,   -20,  -13,  -111,
    114,  64,   -127, -66,  53,   67,   -4,   103,  98,   110,  8,    -101, -54,  -70,  101,  -22,  -20,  -1,   123,  -121, 0,    -40,  -111, 16,
    -25,  19,   -44,  -11,  -53,  55,   9,    -29,  -86,  -89,  -37,  -60,  -67,  -69,  112,  92,   -72,  33,   43,   1,    76,   -3,   -69,  -16,
    70,   -103, 8,    116,  -95,  -12,  92,   -69,  98,   16,   -40,  -85,  -93,  -47,  -86,  -101, 100,  24,   -119, -85,  114,  115,  -44,  -84,
    119,  -28,  -76,  -114, 6,    125,  -125, 69,   89,   -74,  42,   48,   64,   -117, 28,   119,  -26,  127,  107,  -108, 123,  -56,  -30,  -49,
    12,   -25,  69,   89,   -26,  30,   120,  -69,  5,    -112, 66,   -30,  -103, -126, -86,  79,   34,   73,   -67,  -61,  -10,  -110, 64,   49,
    -61,  35,   127,  0,    116,  110,  -83,  -25,  115,  88,   0,    88,   -17,  -46,  34,   -21,  128,  95,   -89,  118,  -91,  85,   95,   -45,
    16,   -26,  -26,  -34,  1,    61,   11,   -8,   -120, 70,   37,   89,   -76,  119,  16,   -19,  -119, 95,   74,   84,   -31,  103,  73,   84,
    -65,  86,   113,  17,   119,  5,    116,  89,   47,   -12,  -31,  50,   22,   -114, 52,   0,    -98,  -63,  -63,  -103, -127, -92,  7,    3,
    -94,  25,   -9,   -30,  0,    35,   -58,  94,   116,  -27,  -87,  -12,  -14,  -11,  123,  92,   7,    -103, 108,  -8,   -40,  53,   6,    -1,
    113,  -104, -7,   -82,  -43,  -123, 74,   38,   -75,  40,   25,   -64,  -122, 2,    -20,  30,   40,   103,  -79,  -59,  -102, 126,  -50,  -54,
    126,  -82,  -69,  52,   17,   -113, -10,  -44,  63,   58,   96,   59,   44,   29,   11,   -75,  -29,  21,   -82,  70,   -124, 98,   31,   71,
    -113, 23,   -115, 34,   73,   -39,  -116, -115, 83,   69,   127,  -53,  -12,  -73,  116,  23,   -103, 32,   59,   110,  -20,  2,    56,   6,
    75,   79,   124,  -1,   -30,  -107, 12,   7,    50,   22,   -114, 17,   56,   -51,  32,   -127, -67,  -7,   33,   115,  -107, 110,  -62,  121,
    6,    41,   -87,  9,    -81,  98,   16,   -122, -121, -94,  -25,  -103, -4,   68,   -4,   -88,  78,   -102, 47,   7,    -116, -68,  50,   -10,
    51,   -123, 81,   -53,  -100, 122,  30,   -98,  55,   -23,  -90,  82,   79,   -39,  16,   102,  -87,  27,   68,   22,   113,  -78,  61,   9,
    60,   -26,  30,   -37,  45,   -105, 116,  97,   82,   -123, 95,   -101, 77,   65,   92,   -77,  80,   92,   54,   49,   74,   -2,   -96,  126,
    -21,  82,   52,   65,   67,   -23,  -25,  -32,  -40,  -26,  82,   -67,  66,   -38,  73,   103,  112,  -10,  -8,   27,   9,    -56,  49,   -125,
    -47,  -96,  11,   -79,  -55,  -3,   79,   -64,  34,   -11,  104,  115,  -21,  -106, -32,  -18,  72,   100,  17,   -12,  104,  17,   48,   28,
    52,   61,   -42,  1,    -48,  28,   23,   -110, -3,   -9,   -8,   -54,  70,   42,   -39,  46,   7,    -108, -108, -72,  -84,  -39,  -115, -86,
    -23,  0,    25,   -4,   -49,  79,   84,   -38,  -53,  9,    -30,  -30,  -100, 101,  70,   43,   -71,  69,   89,   112,  -77,  9,    -79,  -93,
    -19,  42,   78,   103,  15,   100,  75,   43,   -3,   -95,  92,   -71,  -120, 91,   8,    -87,  -76,  -21,  -21,  20,   2,    -117, 105,  -25,
    -16,  -92,  -64,  74,   -20,  -87,  93,   -107, -77,  28,   -102, -120, -125, -57,  -5,   -90,  107,  -33,  17,   -22,  32,   48,   113,  -73,
    37,   -32,  54,   -42,  118,  41,   105,  62,   39,   -75,  -8,   -118, -13,  57,   -58,  -78,  112,  55,   -36,  -116, 99,   125,  10,   82,
    118,  77,   -91,  -125, 49,   -74,  79,   35,   -2,   -5,   86,   46,   62,   -78,  90,   42,   -81,  -110, -20,  -1,   -55,  -48,  -102, -96,
    80,   93,   -41,  -26,  107,  127,  85,   -49,  53,   -86,  -106, 100,  -36,  124,  -20,  -9,   -13,  -83,  99,   106,  109,  -103, -98,  50,
    84,   -14,  42,   -85,  110,  84,   99,   5,    -94,  121,  -105, 64,   -115, 58,   2,    -91,  -52,  14,   33,   19,   -58,  48,   -67,  -113,
    12,   -101, 56,   87,   73,   -98,  -125, -46,  103,  94,   -110, -76,  94,   49,   -62,  -9,   -106, 117,  111,  46,   -53,  124,  -91,  7,
    41,   35,   48,   36,   15,   110,  -56,  52,   18,   -54,  33,   -46,  -26,  51,   118,  -106, -38,  -74,  74,   108,  -68,  -117, -8,   29,
    40,   -14,  99,   -38,  61,   -35,  91,   -2,   -17,  -78,  112,  -120, -30,  -60,  -15,  -108, 8,    54,   117,  -79,  -120, 66,   -53,  112,
    -111, -87,  -82,  -87,  119,  93,   61,   17,   19,   89,   26,   81,   6,    -97,  -86,  -106, 2,    22,   -116, 11,   35,   -97,  61,   69,
    -115, -49,  80,   -45,  119,  44,   116,  49,   75,   -30,  18,   -33,  99,   -115, -94,  31,   -26,  28,   112,  80,   66,   -77,  -5,   78,
    -59,  -16,  114,  106,  61,   -113, -79,  -48,  -70,  106,  112,  -20,  79,   -21,  123,  -3,   99,   113,  78,   25,   93,   59,   -72,  -54,
    -126, -35,  -82,  -118, 106,  -60,  -57,  61,   -99,  -57,  -65,  107,  117,  42,   65,   -103, -68,  12,   114,  -71,  80,   55,   -28,  23,
    81,   -60,  8,    62,   3,    13,   -116, -117, -5,   53,   -104, 75,   48,   6,    -46,  -83,  -113, -21,  10,   72,   60,   62,   -105, -7,
    81,   6,    52,   -46,  -18,  98,   122,  -31,  104,  -25,  -27,  -41,  113,  -82,  11,   59,   22,   -26,  115,  44,   -57,  -58,  28,   -115,
    76,   -101, 60,   -58,  -75,  29,   65,   -34,  -72,  -64,  -99,  -127, -16,  -18,  69,   110,  -118, 34,   0,    -94,  -114, 127,  -24,  -13,
    -69,  13,   17,   -70,  -44,  5,    60,   -39,  49,   95,   65,   -2,   112,  92,   -47,  59,   122,  117,  2,    -94,  3,    7,    -123, -90,
    41,   70,   41,   123,  50,   63,   71,   -106, -120, 53,   -89,  18,   -125, 67,   77,   33,   -89,  -102, 77,   76,   -11,  -114, 74,   27,
    -77,  9,    -128, -77,  -45,  88,   26,   -84,  7,    44,   25,   -28,  48,   15,   -42,  115,  113,  -13,  40,   -101, -109, 69,   -56,  94,
    120,  55,   -38,  29,   101,  -110, 71,   44,   -84,  0,    73,   -2,   -114, -22,  -79,  106,  -114, 7,    21,   5,    -105, 35,   -122, 10,
    -75,  -89,  7,    -88,  32,   -42,  50,   -106, -62,  -71,  48,   72,   13,   113,  92,   -101, 39,   121,  -55,  75,   71,   92,   24,   -93,
    -28,  60,   114,  -42,  -52,  -22,  -59,  -32,  90,   -75,  -9,   101,  -75,  41,   -27,  -90,  -127, 37,   -73,  90,   102,  -25,  117,  -8,
    -126, 7,    -9,   58,   -127, 46,   16,   72,   -34,  -101, 98,   20,   -105, 67,   16,   -74,  -35,  71,   117,  6,    84,   -100, 1,    100,
    2,    -90,  72,   85,   59,   52,   -127, 96,   -117, -33,  -76,  76,   -120, -80,  -123, -66,  -21,  15,   52,   18,   113,  43,   37,   88,
    -26,  72,   -125, 97,   -35,  -24,  3,    122,  -1,   -107, -11,  99,   -113, 87,   -47,  -2,   -105, -61,  78,   40,   -84,  41,   -84,  0,
    123,  97,   -120, -22,  40,   79,   92,   2,    61,   -40,  -125, 5,    21,   -111, -37,  -112, -70,  -104, -46,  107,  10,   31,   -105, -49,
    -122, -123, 95,   22,   -20,  123,  80,   24,   -77,  -23,  -96,  41,   -62,  -36,  114,  -50,  -105, -25,  26,   -68,  -51,  -58,  59,   -106,
    -10,  -45,  -64,  7,    120,  -127, -62,  44,   88,   61,   65,   -38,  -75,  9,    110,  75,   -18,  53,   -119, -9,   33,   110,  -99,  -26,
    69,   -67,  -26,  65,   -84,  20,   -10,  114,  34,   7,    75,   -86,  100,  -60,  102,  -28,  118,  -23,  115,  84,   -119, 84,   -63,  95,
    25,   50,   69,   -3,   -120, 88,   18,   -78,  10,   44,   -29,  -40,  -58,  -22,  15,   106,  -80,  54,   -103, 125,  22,   -69,  -92,  -63,
    -42,  -66,  56,   -56,  61,   97,   -38,  27,   50,   -37,  -126, -102, 79,   -114, -71,  34,   -8,   -10,  -38,  -124, -46,  -56,  98,   11,
    94,   -91,  -97,  95,   -82,  -31,  7,    -70,  20,   62,   57,   -118, -50,  -32,  8,    -2,   -16,  52,   -33,  126,  -35,  75,   -22,  -105,
    -103, 44,   -51,  -72,  -45,  39,   -68,  -31,  118,  81,   53,   -13,  59,   -69,  -58,  -69,  -127, -9,   -101, -67,  -17,  32,   -38,  85,
    113,  98,   -62,  73,   42,   -73,  -54,  49,   62,   85,   17,   117,  53,   63,   14,   108,  -44,  -106, 90,   -72,  4,    20,   74,   -108,
    110,  79,   53,   101,  -38,  -95,  -39,  -25,  51,   -60,  95,   66,   -103, 46,   -100, 101,  -83,  -60,  88,   38,   122,  39,   15,   -89,
    -104, 96,   109,  -109, 15,   30,   -92,  113,  17,   74,   -108, -87,  7,    -83,  -69,  -100, -119, -122, -74,  29,   -41,  -51,  -21,  -50,
    59,   22,   -102, -54,  -124, -120, 102,  25,   -83,  26,   5,    28,   -109, 19,   -26,  -18,  102,  17,   41,   -1,   75,   -50,  -92,  -35,
    73,   -5,   -107, 86,   -8,   -60,  -95,  -108, 40,   5,    -58,  -18,  -119, -92,  -12,  -122, -7,   -10,  4,    -124, -112, -30,  96,   -54,
    67,   -108, 42,   87,   63,   79,   55,   27};
static uint32_t shiftVector6[]     = {0, 0, 0, 0, 0};
static int16_t  staticRefOutput6[] = {
    1794,  -1822, -4471, 427,   885,   -2984, 5082,  -2440, 581,   -2683, -668,  972,   1239,  -3937, -2258, -2686, 2220,  -409,  -471,  -4526, 3265,
    379,   -2146, -1724, 1046,  -3642, -4583, 1740,  662,   -6793, 862,   2069,  6651,  2442,  2105,  -78,   20,    2141,  200,   1201,  -515,  4033,
    2596,  -1338, 4044,  -1531, 104,   -2685, 3121,  1625,  1517,  -2096, 1737,  365,   -4018, -785,  586,   2686,  472,   -2443, -3443, -535,  1270,
    509,   -982,  -280,  1997,  -4507, 1458,  -1188, 4362,  -3197, -4080, -317,  -2416, -403,  1724,  3582,  1925,  -1424, 2195,  1845,  -3393, 3233,
    1951,  -5203, 2761,  -3460, 1511,  322,   -1997, 408,   1907,  -176,  -770,  3834,  7696,  2232,  -2876, 1556,  -98,   1813,  -995,  -807,  1371,
    1638,  21,    -1995, 2812,  3493,  -871,  2901,  51,    355,   -943,  1176,  -1177, -633,  -1440, -1059, -775,  905,   3958,  821,   2943,  2152,
    98,    -475,  2173,  4081,  -3268, -2505, -1771, -2515, -1822, -300,  -3698, 5487,  -1201, -4690, 269,   1219,  2765,  1005,  -469,  2536,  -55,
    1740,  -3528, -639,  -625,  -1805, -1655, -2667, 4440,  3841,  727,   3338,  -2087, 2385,  2478,  -169,  -6321, -159,  3000,  4493,  827,   3420,
    2109,  1028,  1547,  -2096, -465,  1416,  -1342, -2458, -2129, 360,   -1384, -2206, -1835, 4970,  -4436, 2744,  653,   585,   -860,  -5842, -2141,
    -2425, -1265, 768,   1410,  1639,  -1153, -492,  1969,  -1418, -391,  -2865, 3903,  -1968, -2936, 2085,  -3214, 214,   2218,  914,   1088,  -2626,
    3184,  -1738, -3047, 4312,  -2642, 2046,  682,   222,   -2337, 811,   -2087, -2005, -4360, -2727, 1170,  106,   200,   3432,  2917,  -2488, -782,
    -1725, -2841, 910,   -2067, 3964,  136,   5853,  -5143, 4579,  2193,  -1346, -2443, 176,   -178,  1188,  241,   -2213, 5656,  -149,  3794,  1329,
    -197,  -1431, -2124, -2899, -410,  647,   -404,  1440,  -3457, 2229,  581,   2703,  1176,  -3353, -749,  -1063, -1134, -510,  2215,  4887,  -1733,
    623,   -551,  3794,  -1083, 2194,  4781,  -2197, 1053,  -3670, 1075,  907,   -2057, 3709,  -3006, -1196, -1235, -932,  -6350, -475,  106,   1105,
    2607,  -396,  -4498, 2368,  621,   -2112, 2508,  724,   2035,  -1489, 1610,  -2209, 683,   -2690, 1906,  -1178, -1125, -2936, 238,   641,   737,
    33,    68,    -1320, -2955, 3874,  912,   -866,  352,   -323,  -1649, 1017,  -499,  -2443, -1224, -2410, 372,   -1804, -2867, -1720, 1150,  1619,
    -2479, 1413,  -1153, -1017, 4596,  3206,  -2288, -394,  -290,  -1304, 2688,  1747,  -100,  782,   6128,  -1646, 4871,  5116,  1136,  2006,  -2091,
    2785,  -2432, -284,  -2240, -1855, -3920, -1812, -870,  -4814, -463,  -1388, -2831, 3405,  803,   -330,  -1805, -3492, -1427, -2936, 716,   2261,
    564,   3911,  1073,  -1375, 2451,  -155,  -1425, -1893, -855,  -2340, 0,     790,   -615,  1060,  5575,  -358,  -1533, 2368,  2324,  -360,  -3748,
    423,   -5006, -1863, 35,    1151,  3204,  -1163, -748,  -526,  -3237, -3623, -4101, -1970, -2512, 1460,  964,   114,   2360,  -2521, 1612,  351,
    -2268, -893,  -3988, -831,  3305,  2895,  -2678, -4725, -927,  1268,  -1771, 1593,  3633,  -387,  4040,  5139,  2568,  -1592, 4160,  3804,  1352,
    1704,  4535,  2194,  2695,  -1219, -1687, -1827, 809,   512,   -2191, -2782, -2825, 649,   -2920, -2092, -2623, -5588, -4565, -538,  998,   -228,
    2009,  -2257, 370,   1792,  -1063, -2117, -3410, -1197, 3116,  -3279, -765,  -4275, -185,  -2690, -891,  -1845, 1437,  -1509, 2209,  -65,   2604,
    -761,  -943,  286,   5476,  2331,  106,   -786,  1719,  -5631, -1837, 1365,  1839,  1741,  1029,  -3199, -299,  51,    -2187, -2990, 1205,  -984,
    -141,  1385,  -1506, 654,   4474,  -478,  -1843, -7,    2218,  -842,  2465,  -767,  1241,  2812,  -916,  622,   394,   -1654, -431,  -817,  -610,
    580,   2539,  871,   2160,  -1397, -1453, -374,  -3895, -297,  3618,  216,   -2935, 3177,  -131,  954,   2853,  48,    -1672, 97,    -1219, 212,
    -3680, -1405, -811,  -352,  -3267, -2036, -2227, 1091,  -298,  5538,  -3669, 4312,  3725,  570,   -1625, -4321, 1769,  3110,  -386,  3332,  2436,
    -2441, -653,  -1741, 626,   1757,  -3842, 2426,  -854,  2407,  -1789, 2697,  114,   602,   325,   -407,  2553,  -1250, -452,  -575,  346,   3591,
    2933,  1281,  3213,  1818,  726,   -704,  1961,  277,   -1711, -977,  -403,  -1558, 6312,  -2051, 975,   -476,  -2495, -954,  -165,  -1887, 284,
    820,   -6872, -36,   2181,  -926,  -2863, 2303,  1615,  -446,  -874,  -208,  -1833, -280,  -224,  -4582, -1141, -2427, 2805,  1470,  4582,  -918,
    -1070, 1989,  2235,  -1485, 1656,  -4669, -726,  -3053, -1090, -1347, 2101,  -541,  3165,  -2186, 2109,  193,   -542,  -388,  -1310, -3133, -3023,
    -4268, 1400,  1016,  2455,  -2323, -3566, 789,   -3010, -389,  508,   -3555, -200,  1426,  3533,  -1847, -487,  -2200, -4994, 5271,  -1926, -2972,
    -2836, 397,   3219,  2073,  2155,  -1090, -4659, -1404, -699,  84,    -1564, 501,   -6322, 4013,  -1476, 6624,  334,   975,   -1701, 2925,  -4534,
    -945,  1072,  -4072, 6275,  265,   -1110, 770,   -3928, -302,  2107,  -3028, 2844,  1861,  -205,  -1548, 155,   196,   -2486, 1850,  -242,  -1735,
    437,   3872,  4572,  418,   -316,  -2116, -2788, 112,   -4374, -94,   2371,  1532,  -302,  3946,  -1199, 1429,  2967,  1037,  -2937, 1067,  1244,
    2889,  2847,  -1939, 2012,  -178,  1101,  -3676, 2400,  529,   2321,  -2116, -1176, 2239,  462,   -565,  1088,  620,   986,   5325,  -1038, -317,
    -1196, -946,  -5251, -343,  270,   -2785, -3058, -1253, 633,   21,    -2719, -3206, -2165, 1770,  -850,  1618,  -1548, 602,   -473,  -425,  1514,
    719,   -1214, -3774, 1041,  4023,  1386,  -4338, -652,  -1174, -5391, -464,  -1186, 665,   -5699, 147,   5917,  2028,  3380,  -2356, 4907,  5511,
    1807,  -995,  -1991, 344,   1139,  -356,  -349,  2254,  -166,  -1577, -1896, -4028, 5600,  -345,  14,    820,   -1211, -3021, -2023, -1166, -4605,
    356,   -1987, 2253,  -1333, -970,  -670,  623,   -932,  -2884, 425,   3565,  2003,  3124,  -3553, 1053,  -1158, 2755,  -4300, -1462, -5245, 3297,
    -469,  4021,  -1295, -259,  603,   -636,  2570,  555,   2738,  66,    -1665, -3823, -2456, -320,  2151,  -953,  1282,  -1472, 440,   2727,  3931,
    559,   -5707, 1845,  -610,  501,   -630,  2096,  4493,  -971,  3118,  3752,  -146,  419,   -93,   -829,  2240,  3058,  -173,  -3308, 801,   -1235,
    269,   -4344, 373,   2592,  -645,  -6082, 2137,  -6400, -712,  -3065, 319,   -1061, 6388,  -1890, -3153, 1345,  -46,   1519,  -750,  -5240, -1630,
    -2143, 2826,  4567,  1133,  234,   5780,  1960,  -4028, -1713, -2866, 1107,  2374,  5860,  80,    935,   2415,  -3201, -2767, 3137,  -404,  3693,
    -1154, 2070,  -1116, -22,   2466,  -1391, -2005, -1270, -1916, 957,   80,    153,   -2457, 1255,  -332,  1873,  -4212, 2601,  -1781, -2099, 2545,
    3657,  3675,  1400,  -2719, 7659,  -2704, -1018, 843,   -7979, -182,  119,   1923,  145,   1226,  -2508, 4896,  1293,  2337,  -752,  1739,  3205,
    1330,  4176,  -70,   -779,  1039,  978,   621,   3965,  2506,  -4660, 1899,  -2253, -1952, 2126,  -613,  -8024, -270,  -2475, 29,    -2193, -1638,
    -1275, -1501, -4183, 2210,  -2670, -2998, -968,  -2965, 1774,  1747,  1942,  1934,  -3305, 394,   -298,  -2780, -366,  625,   455,   -1239, 1605,
    -1363, -1347, -1512, 3312,  2287,  -3452, 1075,  -2904, -1859, -719,  2921,  -2723, -3830, -2706, -2572, -346,  -4306, 4018,  -31,   -1245, -239,
    920,   2074,  6440,  1323,  3053,  -1772, 1170,  3533,  -3621, 3558,  -5192, -2480, 59,    -4007, -430,  1835,  -651,  -1430, 3032,  -174,  -4276,
    2671,  4018,  3066,  -1255, -4426, -123,  -2715, 540,   -1879, -1568, -2516, -1197, 802,   3645,  -2755, 3529,  866,   1176,  -1308, 103,   3138,
    -1837, 1875,  599,   -3173, 2318,  3055,  3005,  -2728, -1163, 924,   2686,  848,   -2687, -591,  -4389, -1378, -167,  1936,  -3716, -7369, -247,
    534,   5074,  2506,  2397,  -1992, 3543,  -526,  -1961, 1506,  -1346, 1147,  -3634, -461,  -3389, -2679, -489,  -3123, -2241, 515,   -1034, -2845,
    -904,  -1341, -2376, -1661, -452,  646,   -1320, -452,  732,   -2444, 1844,  -196,  -5255, 1975,  -1825, 121,   920,   -85,   1317,  1534,  -159,
    57,    -2161, 427,   -437,  -1241, 1042,  -2177, -1359, 334,   -1913, -795,  1045,  -1270, -3335, 2205,  -928,  226,   1337,  1057,  701,   -148,
    1873,  -1357, -1025, 5522,  -632,  -1920, 697,   5033,  -592,  739,   419,   723,   -3001, 4241,  4128,  -3175, -1458, 712,   993,   553,   3667,
    4133,  -491,  -1896, 147,   -2957, 2964,  2469,  -857,  4646,  1119,  -4451, -3221, 2350,  477,   1537,  826,   -819,  -3032, 1033,  1792,  -3211,
    772,   -774,  518,   181,   2656,  3626,  898,   -1633, -6286, 216,   2208,  3763,  -629,  336,   582,   -195,  -1405, -2309, -1084, 2078,  443,
    3311,  240,   361,   -3622, -1909, -525,  -3217, -1114, 1470,  -1879, 722,   4406,  -2320, 372,   -4138, -1172, -160,  2162,  119,   390,   2364,
    -2236, -1608, 418,   2281,  -923,  -2109, 2185,  -2604, 2217,  2822,  944,   734,   310,   -2555, -1172, 752,   -99,   1453,  132,   -109,  -1204,
    -1592, 5513,  4289,  -1499, -1995, -1848, 669,   -306,  480,   3906,  4239,  4885,  -5502, 1337,  -974,  477,   2551,  1693,  -3598, -2703, -54,
    -6051, 2614,  682,   2923,  -577,  -239,  -1733, -2644, 895,   -1511, 737,   242,   1052,  -1279, -4165, -4817, 2521,  -3467, -710,  3273,  -1668,
    -2171, -3245, -949,  916,   1477,  191,   -815,  1679,  3034,  -1190, -1187, 448,   2016,  -675,  2232,  49,    1755,  7022,  -3022, 496,   -147,
    3694,  -3988, 36,    4721,  2269,  2376,  715,   -3441, 574,   3304,  -206,  -1361, 2420,  -1070, 3649,  -2637, 373,   -1642, 5028,  -2417, -2108,
    1196,  -2256, 1738,  -389,  -2645, 1031,  -2655, -4639, -1068, 844,   122,   -3484, -97,   1658,  1576,  1797,  1417,  -3937, 1455,  1371,  -698,
    1240,  4046,  -4254, 2096,  494,   -2858, -151,  -5486, 3634,  -1854, 2882,  173,   1996,  3692,  -426,  499,   -2321, 872,   2056,  -3562, 2087,
    1688,  -2270, -2852, 76,    -2547, 2564,  -87,   -4975, -2763, 1436,  2845,  -818,  -165,  5306,  -2950, -3215, -242,  -1559, -1965, -3229, 2249,
    -1799, -1371, 205,   3307,  3204,  -3010, -1824, -743,  -1050, 93,    -1482, -2563, 934,   -2356, -2932, 1530,  -375,  -398,  -731,  1993,  -1141,
    -1076, 781,   -2278, 782,   127,   -5191, -3003, 614,   -4690, -42,   1146,  -1484, -2284, -1905, -1290, 51,    604,   131,   -230,  135,   -455,
    1527,  -526,  1571,  37,    -142,  971,   -4789, -2171, -1887, -3272, -3195, -2498, 476,   -1736, -1724, -2280, -1042, 995,   4056,  1383,  -163,
    -309,  -2991, 3035,  -312,  -1159, 1094,  -1505, 1185,  418,   2614,  519,   92,    -1777, -2894, 726,   -2070, -813,  1119,  1160,  -272,  -2419,
    -3587, -1000, 4475,  -1254, 23,    -3269, -1459, 4849,  -1098, -31,   -2149, 1393,  257,   -5061, 3587,  -1152, -395,  973,   -898,  72,    -597,
    2706,  -4774, -677,  -2961, -4425, -395,  387,   -819,  -305,  -213,  -889,  -57,   -2405, 3972,  503,   -2106, 801,   -189,  -2288, 4446,  2000,
    -24,   1261,  3791,  -1678, 330,   829,   1,     4165,  1332,  80,    -74,   -5734, -460,  -1365, 2755,  -3074, 2478,  -1295, -5461, 3392,  -1689,
    1795,  558,   -2669, -1491, -2162, 112,   3503,  -447,  -7913, 416,   1291,  712,   408,   -267,  -569,  922,   -866,  -3957, -2061, 4532,  -375,
    3630,  -5207, 1295,  -1644, -1056, 2013,  3604,  -51,   768,   -1519, 2297,  -1129, 596,   3906,  1370,  1938,  4333,  651,   1861,  -1842, -2951,
    -3052, -3562, 3230,  1595,  -2937, 1455,  474,   -1536, 2155,  -559,  1035,  1726,  2080,  -331,  3276,  -791,  2121,  -4425, -1441, 700,   -88,
    2728,  -4921, -1329, -2229, -823,  4083,  840,   4774,  3321,  -169,  -2100, 1897,  2610,  -1315, 1577,  2072,  1120,  -988,  -1824, 245,   3112,
    427,   4401,  769,   -452,  1102,  -970,  539,   -42,   -2882, 1574,  2599,  4373,  863,   -3360, 1096,  -214,  104,   2927,  1843,  587,   648,
    4159,  1886,  881,   -462,  -4467, 2933,  663,   1707,  -1422, -3041, -1797, -14,   60,    1192,  -1143, -3117, -1938, -788,  -1006, -371,  2871,
    -1766, -661,  -1232, -5041, 1548,  -1275, 200,   3012,  -387,  -39,   -2977, -984,  1272,  235,   3604,  -2796, -2908, 3209,  3489,  -1165, 555,
    -1352, 1383,  -2690, -227,  720,   -460,  -577,  4478,  3055,  2100,  1564,  -1934, -2047, -5981, 3639,  2646,  -4215, -2612, -3420, -1739, 1763,
    -3138, -406,  348,   -2180, -421,  1452,  596,   81,    1287,  -472,  -273,  2816,  1914,  -504,  -604,  3389,  141,   -1634, -3884, 2830,  -2514,
    -1126, -2134, 1686,  -4050, 474,   2693,  -1942, 1436,  1060,  2537,  -1361, 1953,  671,   3865,  153,   4456,  229,   245,   1469,  778,   2992,
    3069,  -3696, -1526, 4067,  583,   1662,  596,   2449,  -1034, -2045, -46,   4532,  -4928, 197,   -2400, 2767,  -1702, 336,   -133,  -569,  -500,
    2705,  1882,  -2477, 2981,  -2571, -299,  728,   809,   1062,  -1560, 1860,  -2754, -598,  3747,  -1037, -1002, 387,   -2050, 140,   -845,  -393,
    3094,  -212,  662,   -1742, 217,   -848,  -108,  -1435, -1535, 347,   -1261, 2170,  1792,  6682,  2613,  1069,  4049,  -2483, 6025,  1592,  1459,
    -2374, -2285, -20,   852,   3335,  -1296, -40,   1350,  -237,  674,   -3492, -1125, 1009,  -1999, -292,  6083,  2496,  4489,  2371,  -361,  -1442,
    1434,  -877,  2424,  594,   2693,  -277,  661,   -1626, 777,   1721,  -1860, -2595, 2826,  998,   -417,  1821,  1157,  2313,  353,   -217,  1413,
    -178,  -724,  -2223, 704,   -1214, 2083,  -911,  -1962, -1050, 4199,  -1139, 352,   -102,  2158,  21,    -1,    1237,  -4227, -1341, -482,  -2957,
    -522,  1904,  1191,  955,   2634,  -84,   -360,  841,   -1559, 207,   3638,  -5340, 719,   -320,  -1099, -723,  2159,  -1706, 1335,  6106,  -427,
    1320,  1317,  670,   1382,  -1743, 585,   -115,  3484,  1428,  517,   -5209, -342,  721,   -2724, -3454, 180,   -2487, 1026,  -275,  2567,  -3800,
    1280,  -964,  1562,  -1759, -3322, 2207,  6046,  -2474, -784,  -5965, 3635,  6165,  -1227, -2655, 1860,  4371,  3148,  168,   -3246, 1544,  -2194,
    4893,  4075,  388,   -3744, -1299, 546,   -2609, 531,   -1877, 244,   1429,  -1554, 4537,  -2511, 1015,  2845,  -559,  4093,  -2032, 557,   1165,
    -5632, -4018, 2717,  1965,  880,   2319,  -13,   -4427, -694,  -694,  -2108, -1677, -99,   3560,  -2725, -699,  -1628, -2702, -994,  -1833, -3465,
    1834,  -1813, -1079, 3782,  -1166, -3825, 341,   -3814, 934,   4839,  190,   360,   -2397, 2257,  691,   -394,  -13,   1059,  5761,  2304,  1642,
    1838,  3386,  -1619, -1894, -1301, -1058, -4262, 3959,  1454,  1366,  -1706, 838,   1993,  -1197, -279,  217,   2239,  367,   1032,  -2770, 725,
    -1742, -3815, 778,   75,    -1921, -867,  -797,  2284,  2272,  -2394, 1386};

static uint32_t shiftVector4[]  = {0, 0, 0};
static uint32_t shiftVector5[]  = {0, 0, 0, 0};
static uint32_t shiftVector11[] = {0, 0, 0, 0, 0, 0};
static uint32_t shiftVector12[] = {0, 0, 0, 0, 0, 1};
static uint32_t shiftVector13[] = {0, 0, 0, 0, 1, 0};
static uint32_t shiftVector14[] = {0, 0, 0, 1, 0, 0};
static uint32_t shiftVector15[] = {0, 0, 0, 1, 0, 0, 0};
static uint32_t shiftVector16[] = {1, 1, 1, 1, 1, 1, 1};

static fft1d_i16sc_c16sc_o16sc_testParams_t testParams[] = {
/********************************************
    {
       testPattern,
       *staticIn0, staticIn1, staticOut
       width, height, stride
    },
*********************************************/
    {STATIC, staticRefInput4, shiftVector4, staticRefOutput4, 3, 64, 0},

    {STATIC, staticRefInput6, shiftVector6, staticRefOutput6, 5, 1024, 2},

};

/*
 *  Sends the test parameter structure and number of tests
 */
void fft1d_i16sc_c16sc_o16sc_getTestParams (fft1d_i16sc_c16sc_o16sc_testParams_t **params, int32_t *numTests)
{
   *params   = testParams;
   *numTests = sizeof (testParams) / sizeof (fft1d_i16sc_c16sc_o16sc_testParams_t);
}

/* ======================================================================== */
/*                       End of file                                        */
/* ------------------------------------------------------------------------ */
/*            Copyright (c) 2017 Texas Instruments, Incorporated.           */
/*                           All Rights Reserved.                           */
/* ======================================================================== */
