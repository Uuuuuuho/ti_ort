Dir or File            Contains
-----------            --------
XML_TI_MAP.pm          Perl module that converts XML to data structures
dump_map.pl            Script that dumps Perl data structure built from
                          the XML file
lib_footprint.pl       Script that finds all the libs in an application and
                          reports how much memory they use
gen_shm_cmd.pl         generates cmd file shared MEMORY sections (useful
                          in eg OMAP or DM320 hetero apps)
classes.txt            Configuration file optionally used by lib_footprint.pl
find_dup_syms.pl       Script that finds/reports duplicate symbol
                       declarations in an application.
