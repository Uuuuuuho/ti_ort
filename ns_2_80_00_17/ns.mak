#
#   Copyright (c) 2015-2020, Texas Instruments Incorporated
#
#   Redistribution and use in source and binary forms, with or without
#   modification, are permitted provided that the following conditions
#   are met:
#
#   *  Redistributions of source code must retain the above copyright
#      notice, this list of conditions and the following disclaimer.
#
#   *  Redistributions in binary form must reproduce the above copyright
#      notice, this list of conditions and the following disclaimer in the
#      documentation and/or other materials provided with the distribution.
#
#   *  Neither the name of Texas Instruments Incorporated nor the names of
#      its contributors may be used to endorse or promote products derived
#      from this software without specific prior written permission.
#
#   THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
#   AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
#   THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
#   PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
#   CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
#   EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
#   PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
#   OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
#   WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
#   OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
#   EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#

#
# ==== Standard Variables ====
#

# USER STEP: user must define below paths
XDC_INSTALL_DIR ?= C:/ti/xdctools_version
SDK_INSTALL_DIR ?= C:/ti/simplelink_msp432e4_sdk_version

TIPOSIX_INSTALL_DIR=$(SDK_INSTALL_DIR)

#
# Set XDCOPTIONS.  Use -v for a verbose build.
#
#XDCOPTIONS=v
export XDCOPTIONS

#
# Set location of various code generation tools
# These variables can be set here or on the command line.
#
# USER STEP: user must define path for each compiler they wish to use
ti.targets.elf.C66 ?=
ti.targets.elf.C674 ?=

ti.targets.arm.elf.M4 ?=
ti.targets.arm.elf.M4F ?=
ti.targets.arm.elf.Arm9 ?=
ti.targets.arm.elf.R5F ?=
ti.targets.arm.elf.R5Ft ?=

ti.targets.arm.clang.M4 ?=
ti.targets.arm.clang.M4F ?=

iar.targets.arm.M4 ?=
iar.targets.arm.M4F ?=

gnu.targets.arm.M4 ?=
gnu.targets.arm.M4F ?=
gnu.targets.arm.A8F ?=
gnu.targets.arm.A9F ?=
gnu.targets.arm.A15F ?=
gnu.targets.arm.A53F ?=
gnu.targets.arm.GCArmv7AF ?=

# Set XDCARGS to some of the variables above.  XDCARGS are passed
# to the XDC build engine... which will load ns.bld... which will
# extract these variables and use them to determine what to build and which
# toolchains to use.
#
# Note that not all of these variables need to be set to something valid.
# Unfortunately, since these vars are unconditionally assigned, your build line
# will be longer and more noisy than necessary (e.g., it will include C66
# assignment even if you're just building for C64P).
#
# Some background is here:
#     http://rtsc.eclipse.org/docs-tip/Command_-_xdc#Environment_Variables
#
XDCARGS= \
    TIPOSIX_INSTALL_DIR=\"$(TIPOSIX_INSTALL_DIR)\" \
    TARGETFS_v7AF=\"$(TARGETFS_v7AF)\" \
    ti.targets.elf.C66=\"$(ti.targets.elf.C66)\" \
    ti.targets.elf.C674=\"$(ti.targets.elf.C674)\" \
    ti.targets.arm.elf.M4=\"$(ti.targets.arm.elf.M4)\" \
    ti.targets.arm.elf.M4F=\"$(ti.targets.arm.elf.M4F)\" \
    ti.targets.arm.elf.Arm9=\"$(ti.targets.arm.elf.Arm9)\" \
    ti.targets.arm.elf.R5F=\"$(ti.targets.arm.elf.R5F)\" \
    ti.targets.arm.elf.R5Ft=\"$(ti.targets.arm.elf.R5Ft)\" \
    ti.targets.arm.clang.M4=\"$(ti.targets.arm.clang.M4)\" \
    ti.targets.arm.clang.M4F=\"$(ti.targets.arm.clang.M4F)\" \
    iar.targets.arm.M4=\"$(iar.targets.arm.M4)\" \
    iar.targets.arm.M4F=\"$(iar.targets.arm.M4F)\" \
    gnu.targets.arm.M4=\"$(gnu.targets.arm.M4)\" \
    gnu.targets.arm.M4F=\"$(gnu.targets.arm.M4F)\" \
    gnu.targets.arm.A8F=\"$(gnu.targets.arm.A8F)\" \
    gnu.targets.arm.A9F=\"$(gnu.targets.arm.A9F)\" \
    gnu.targets.arm.A15F=\"$(gnu.targets.arm.A15F)\" \
    gnu.targets.arm.A53F=\"$(gnu.targets.arm.A53F)\" \
    gnu.targets.arm.GCArmv7AF=\"$(gnu.targets.arm.GCArmv7AF)\"

# Set XDCPATH to contain necessary repositories.  Although the NS libraries
# have no binding to TI-RTOS, the XDC targets we use to build are found in
# TI-RTOS.  So TI-RTOS must be included on the XDCPATH.
XDCPATH = $(SDK_INSTALL_DIR)/kernel/tirtos/packages
export XDCPATH

# Default number of jobs is 1, but this can be overridden via cmd line
j = 1

#
# Set XDC executable command
# Note that XDCBUILDCFG points to the ns.bld file which uses
# the arguments specified by XDCARGS
#
XDC = $(XDC_INSTALL_DIR)/xdc -j$(j) XDCARGS="$(XDCARGS)" XDCBUILDCFG=./ns.bld
XS = $(XDC_INSTALL_DIR)/xs

######################################################
## Shouldnt have to modify anything below this line ##
######################################################

all:
	@echo building packages...
	@$(XDC) -Pr source

clean:
	@echo cleaning packages ...
	@echo $(XDC) clean -Pr ./source
	@$(XDC) clean -Pr ./source
