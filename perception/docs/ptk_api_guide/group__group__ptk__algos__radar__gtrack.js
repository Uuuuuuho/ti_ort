var group__group__ptk__algos__radar__gtrack =
[
    [ "PTK_Alg_RadarGTrackAdvParams", "structPTK__Alg__RadarGTrackAdvParams.html", [
      [ "gatingParams", "structPTK__Alg__RadarGTrackAdvParams.html#a87ec55c1a0462c1001b1b7e43444c24f", null ],
      [ "allocationParams", "structPTK__Alg__RadarGTrackAdvParams.html#ac3f2108e46fba883de207683ab57678d", null ],
      [ "unrollingParams", "structPTK__Alg__RadarGTrackAdvParams.html#a41ab26d9543e6058e6632763cc1c7933", null ],
      [ "stateParams", "structPTK__Alg__RadarGTrackAdvParams.html#ad4becc81ae5d3a200bb9455ce1a00d3f", null ],
      [ "variationParams", "structPTK__Alg__RadarGTrackAdvParams.html#a7617cbefb6a13070aafc981f15c0ed36", null ],
      [ "sceneryParams", "structPTK__Alg__RadarGTrackAdvParams.html#a9697f1842a85f732280f810351f3cca9", null ]
    ] ],
    [ "PTK_Alg_RadarGTrackParams", "structPTK__Alg__RadarGTrackParams.html", [
      [ "maxNumPoints", "structPTK__Alg__RadarGTrackParams.html#a296b2e42fc161dac48433f809d9e942c", null ],
      [ "maxNumTracks", "structPTK__Alg__RadarGTrackParams.html#a06f6f8306891e94daf55505ec1a23b5e", null ],
      [ "initialRadialVelocity", "structPTK__Alg__RadarGTrackParams.html#a0cbb1bfa12a7dfe0f0bd5a05c19165c3", null ],
      [ "maxRadialVelocity", "structPTK__Alg__RadarGTrackParams.html#abf9c21292917028db8922e9f80225960", null ],
      [ "radialVelocityResolution", "structPTK__Alg__RadarGTrackParams.html#a680fb3812027a1320df07f9ae888a4f1", null ],
      [ "maxAcceleration", "structPTK__Alg__RadarGTrackParams.html#accb93068da7f82198699b73faf37f7ab", null ],
      [ "deltaT", "structPTK__Alg__RadarGTrackParams.html#a30c25aade37d703023ae4a148982dd2f", null ],
      [ "advParams", "structPTK__Alg__RadarGTrackParams.html#a751bdd25aa99bcb040ed003e87d7e7c1", null ]
    ] ],
    [ "PTK_Alg_RadarGTrackTargetInfo", "structPTK__Alg__RadarGTrackTargetInfo.html", [
      [ "numDesc", "structPTK__Alg__RadarGTrackTargetInfo.html#a0798a077eead9327e710541965070f2b", null ],
      [ "desc", "structPTK__Alg__RadarGTrackTargetInfo.html#a18e16553a1bc6413bac1c89a89e2d8cf", null ],
      [ "egoLocation", "structPTK__Alg__RadarGTrackTargetInfo.html#ad8567d88bf93ce45c9916d2a180a3831", null ]
    ] ],
    [ "GTRACK_measurement_vector", "structGTRACK__measurement__vector.html", [
      [ "range", "structGTRACK__measurement__vector.html#abc5980beb73bb944188b93c4a0d9fb1f", null ],
      [ "angle", "structGTRACK__measurement__vector.html#a6c40d58327c1bb4a028d53037765dab6", null ],
      [ "doppler", "structGTRACK__measurement__vector.html#aa03c2db5db1870b277f1eff2c59700a1", null ]
    ] ],
    [ "GTRACK_cartesian_position", "structGTRACK__cartesian__position.html", [
      [ "posX", "structGTRACK__cartesian__position.html#a2bdf81e949b4b5a5cb2e420b9cfa3b7c", null ],
      [ "posY", "structGTRACK__cartesian__position.html#a9d2fa15fb8fa1d6f6e8b926565a525a1", null ]
    ] ],
    [ "GTRACK_state_vector_pos_vel", "structGTRACK__state__vector__pos__vel.html", [
      [ "posX", "structGTRACK__state__vector__pos__vel.html#adda9bcf76eee6365a1020d5c90c90f75", null ],
      [ "posY", "structGTRACK__state__vector__pos__vel.html#a4311b35345c96884140c417a53202576", null ],
      [ "velX", "structGTRACK__state__vector__pos__vel.html#afa78874e14979e89a8ebd281ed635194", null ],
      [ "velY", "structGTRACK__state__vector__pos__vel.html#a4853e6ff5ca90faac36178433fc68357", null ]
    ] ],
    [ "GTRACK_state_vector_pos_vel_acc", "structGTRACK__state__vector__pos__vel__acc.html", [
      [ "posX", "structGTRACK__state__vector__pos__vel__acc.html#aaa627b73bf9a363671834c850755ac6e", null ],
      [ "posY", "structGTRACK__state__vector__pos__vel__acc.html#a907c82c1e263866cc52ec9e568cd1556", null ],
      [ "velX", "structGTRACK__state__vector__pos__vel__acc.html#a9b6bef8df2ceecf2937276eebc772b96", null ],
      [ "velY", "structGTRACK__state__vector__pos__vel__acc.html#a616ef8b1b2d3fa254f8758bbee9c6f2f", null ],
      [ "accX", "structGTRACK__state__vector__pos__vel__acc.html#a9b0e497fe4f12a5a7599a654f45759d0", null ],
      [ "accY", "structGTRACK__state__vector__pos__vel__acc.html#a18f5ffa055512befec8db3918e45679e", null ]
    ] ],
    [ "GTRACK_boundaryBox", "structGTRACK__boundaryBox.html", [
      [ "x1", "structGTRACK__boundaryBox.html#a53417977b782d7f9f77733174d83e4a3", null ],
      [ "x2", "structGTRACK__boundaryBox.html#a5bbabd724411eb2ac1983657bd3ce5a4", null ],
      [ "y1", "structGTRACK__boundaryBox.html#a5704190849fd50ba1751fd56076ce284", null ],
      [ "y2", "structGTRACK__boundaryBox.html#a558a5ab78593c34be4fa719d7791bfbd", null ],
      [ "z1", "structGTRACK__boundaryBox.html#a0d7b5e797c25125626d16d0855eaa60f", null ],
      [ "z2", "structGTRACK__boundaryBox.html#a74cfc610a60a9ca4121b3730e10ae0e4", null ]
    ] ],
    [ "GTRACK_gateLimits", "structGTRACK__gateLimits.html", [
      [ "width", "structGTRACK__gateLimits.html#a4cf62ec49af426bcbd968c0864a7c7f7", null ],
      [ "depth", "structGTRACK__gateLimits.html#a11ac4937013f2ec1abd9bcb0be1b1767", null ],
      [ "height", "structGTRACK__gateLimits.html#a4225af44fe4619d8b7c09ebdc18c80c4", null ],
      [ "vel", "structGTRACK__gateLimits.html#a8522dafc886a680e21e361d199753a63", null ]
    ] ],
    [ "GTRACK_varParams", "structGTRACK__varParams.html", [
      [ "widthStd", "structGTRACK__varParams.html#a48711a8ab63bfefbd488898270f75224", null ],
      [ "depthStd", "structGTRACK__varParams.html#aefc51155d0ae13b8e2856ca60ce0a2ec", null ],
      [ "heightStd", "structGTRACK__varParams.html#af5cffcb1f55ca6baa7bdb648a73f7e60", null ],
      [ "dopplerStd", "structGTRACK__varParams.html#a7aeba0f1c975488c5f7eb4513ec98dfa", null ]
    ] ],
    [ "GTRACK_sceneryParams", "structGTRACK__sceneryParams.html", [
      [ "numBoundaryBoxes", "structGTRACK__sceneryParams.html#ac8019b7993c4c7f4c17ad019ce3fd70e", null ],
      [ "boundaryBox", "structGTRACK__sceneryParams.html#a68b11996ca509ba9d990df6f6296b3d2", null ],
      [ "numStaticBoxes", "structGTRACK__sceneryParams.html#a6b4dcd0eaf07ff281913e23c5e9ac6b4", null ],
      [ "staticBox", "structGTRACK__sceneryParams.html#ad0a454d550d906c1b664d5b8dd0104a2", null ]
    ] ],
    [ "GTRACK_gatingParams", "structGTRACK__gatingParams.html", [
      [ "volume", "structGTRACK__gatingParams.html#a9a30918b98928822cc9d68250520ce96", null ],
      [ "limits", "structGTRACK__gatingParams.html#a0f7a539d8d66836c3120d6b2e50b7e61", null ],
      [ "limitsArray", "structGTRACK__gatingParams.html#a9188cf4d32295314f437febb2807041b", null ]
    ] ],
    [ "GTRACK_stateParams", "structGTRACK__stateParams.html", [
      [ "det2actThre", "structGTRACK__stateParams.html#a317fe69bf909910fe7f5ea8e59c9a2c3", null ],
      [ "det2freeThre", "structGTRACK__stateParams.html#abd1e522e7ef9401ad446c536bb4d338a", null ],
      [ "active2freeThre", "structGTRACK__stateParams.html#aacec65e4924cf5d59e05e01c67013d1a", null ],
      [ "static2freeThre", "structGTRACK__stateParams.html#afe9047b0055edeb91f1a36d81c6e862c", null ],
      [ "exit2freeThre", "structGTRACK__stateParams.html#aed8c13cef3ea7506b3a1f52c4fa7208c", null ]
    ] ],
    [ "GTRACK_allocationParams", "structGTRACK__allocationParams.html", [
      [ "snrThre", "structGTRACK__allocationParams.html#a423e3f9de3f479549a4f5523bdca5614", null ],
      [ "snrThreObscured", "structGTRACK__allocationParams.html#ae5b0bda05bdaa66f8b83172bde75975d", null ],
      [ "velocityThre", "structGTRACK__allocationParams.html#a4ba60b55db1cc95b27e5a8302aaa3e9f", null ],
      [ "pointsThre", "structGTRACK__allocationParams.html#af4c9c19cca9fbc0158dffe99d01a1a6e", null ],
      [ "maxDistanceThre", "structGTRACK__allocationParams.html#a57fa341dbd7f3c462b108f1583333433", null ],
      [ "maxVelThre", "structGTRACK__allocationParams.html#a73a101d896947d0007aa5731420963c4", null ]
    ] ],
    [ "GTRACK_unrollingParams", "structGTRACK__unrollingParams.html", [
      [ "alpha", "structGTRACK__unrollingParams.html#ab154de447f84c1a97b2eb8005e7fd4ef", null ],
      [ "confidence", "structGTRACK__unrollingParams.html#a5ce05a6809b66e4a36114d7621654e22", null ]
    ] ],
    [ "GTRACK_measurementPoint", "structGTRACK__measurementPoint.html", [
      [ "vector", "structGTRACK__measurementPoint.html#ab9580647f1e51a144814d7030de818d2", null ],
      [ "array", "structGTRACK__measurementPoint.html#a0806cb96bd8559c605fe8bbcbb706bd3", null ],
      [ "snr", "structGTRACK__measurementPoint.html#a97b7a1f28707847cfcaaf8350480fcf2", null ]
    ] ],
    [ "GTRACK_targetDesc", "structGTRACK__targetDesc.html", [
      [ "uid", "structGTRACK__targetDesc.html#aab35201051ef32018abdb0c9b88501e5", null ],
      [ "tid", "structGTRACK__targetDesc.html#af96c892ba3fb8bebb590a650ce12ac24", null ],
      [ "S", "structGTRACK__targetDesc.html#a30b0e7ada07905116498a11b3e7211e9", null ],
      [ "EC", "structGTRACK__targetDesc.html#aca4db3a704119e932f64a955570cb54d", null ],
      [ "G", "structGTRACK__targetDesc.html#a6549466ebd30da39ba12bd45470cea53", null ],
      [ "dim", "structGTRACK__targetDesc.html#ae05b11351725767a81368842a09826ba", null ]
    ] ],
    [ "GTRACK_NUM_POINTS_MAX", "group__group__ptk__algos__radar__gtrack.html#ga622b9062312e3832a45e8d78f307bf32", null ],
    [ "GTRACK_NUM_TRACKS_MAX", "group__group__ptk__algos__radar__gtrack.html#ga0cc093499dcb27b25d93db1721c46b73", null ],
    [ "GTRACK_ID_POINT_TOO_WEAK", "group__group__ptk__algos__radar__gtrack.html#gad5cda721a38a3cb6c84b289f388d5796", null ],
    [ "GTRACK_ID_POINT_BEHIND_THE_WALL", "group__group__ptk__algos__radar__gtrack.html#gac15e912ce87065507395f47e3fba665d", null ],
    [ "GTRACK_ID_POINT_NOT_ASSOCIATED", "group__group__ptk__algos__radar__gtrack.html#ga9a522224d04fcc4aec672fcb7d6560e4", null ],
    [ "GTRACK_BENCHMARK_SETUP", "group__group__ptk__algos__radar__gtrack.html#gaabddb80eb950d627750bba2aa371d131", null ],
    [ "GTRACK_BENCHMARK_PREDICT", "group__group__ptk__algos__radar__gtrack.html#gaf98b37f96aac697eee16e566680da540", null ],
    [ "GTRACK_BENCHMARK_ASSOCIATE", "group__group__ptk__algos__radar__gtrack.html#ga221904397d2500dbc3b6f9a26f89f87d", null ],
    [ "GTRACK_BENCHMARK_ALLOCATE", "group__group__ptk__algos__radar__gtrack.html#ga3e206bc4ea2d255a230db00075bd1bba", null ],
    [ "GTRACK_BENCHMARK_UPDATE", "group__group__ptk__algos__radar__gtrack.html#gab18a34d6995e78353e12b120d6aa897c", null ],
    [ "GTRACK_BENCHMARK_REPORT", "group__group__ptk__algos__radar__gtrack.html#gaf349132994321744ac1ea4d3c3f1f1d7", null ],
    [ "GTRACK_BENCHMARK_SIZE", "group__group__ptk__algos__radar__gtrack.html#ga395da7089c9f0bc8e946ea762e205a59", null ],
    [ "GTRACK_MAX_SAVE_FILE_NAME_LENGTH", "group__group__ptk__algos__radar__gtrack.html#ga55d06937963498b3aa6198a89c80e7a5", null ],
    [ "GTRACK_MAX_BOUNDARY_BOXES", "group__group__ptk__algos__radar__gtrack.html#ga27e76ea0967be28831d56b8af6e5a5ff", null ],
    [ "GTRACK_MAX_STATIC_BOXES", "group__group__ptk__algos__radar__gtrack.html#gaf42bdc2aac8f7665adc3f4670e0598a0", null ],
    [ "GTRACK_STATE_VECTOR_SIZE", "group__group__ptk__algos__radar__gtrack.html#ga23b309eec224793841e6005583fb10a0", null ],
    [ "GTRACK_MEASUREMENT_VECTOR_SIZE", "group__group__ptk__algos__radar__gtrack.html#ga4dcefbaf9ba12da78df1313eee8938c5", null ],
    [ "GTRACK_VERBOSE_TYPE", "group__group__ptk__algos__radar__gtrack.html#ga56ae787b8feff2425013007288693b14", [
      [ "GTRACK_VERBOSE_NONE", "group__group__ptk__algos__radar__gtrack.html#gga56ae787b8feff2425013007288693b14a26b48e71b421f25012fe889c608abf8c", null ],
      [ "GTRACK_VERBOSE_ERROR", "group__group__ptk__algos__radar__gtrack.html#gga56ae787b8feff2425013007288693b14a3301b46face212a8040f7f7bf9e017b2", null ],
      [ "GTRACK_VERBOSE_WARNING", "group__group__ptk__algos__radar__gtrack.html#gga56ae787b8feff2425013007288693b14aa3294c409fec3c0bbc8e0e7533269760", null ],
      [ "GTRACK_VERBOSE_DEBUG", "group__group__ptk__algos__radar__gtrack.html#gga56ae787b8feff2425013007288693b14a8f3fdb8b17e383badaf665e9ea3e9efe", null ],
      [ "GTRACK_VERBOSE_MATRIX", "group__group__ptk__algos__radar__gtrack.html#gga56ae787b8feff2425013007288693b14a9119458d8800df9f9792b02d82c462af", null ],
      [ "GTRACK_VERBOSE_MAXIMUM", "group__group__ptk__algos__radar__gtrack.html#gga56ae787b8feff2425013007288693b14afcc380c9f4678ad5fd507bbbf5309b85", null ]
    ] ],
    [ "PTK_Alg_RadarGTrackInit", "group__group__ptk__algos__radar__gtrack.html#gac5d9d38b7d0a281477b0da725c92660a", null ],
    [ "PTK_Alg_RadarGTrackProcess", "group__group__ptk__algos__radar__gtrack.html#gab98a0091bafb7d8a5a007c4700e12c46", null ],
    [ "PTK_Alg_RadarGTrackDeInit", "group__group__ptk__algos__radar__gtrack.html#ga365f0215af378e7f4e7757c25651ac87", null ],
    [ "gtrack_alloc", "group__group__ptk__algos__radar__gtrack.html#ga641be2019eeac7c0641338cba10afabd", null ],
    [ "gtrack_free", "group__group__ptk__algos__radar__gtrack.html#gaeb9c8366b3901ca951bc9aa820d9ee45", null ],
    [ "gtrack_log", "group__group__ptk__algos__radar__gtrack.html#ga790801a4a071595d1d9df025d7291e52", null ]
];