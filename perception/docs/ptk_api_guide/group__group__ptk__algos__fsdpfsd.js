var group__group__ptk__algos__fsdpfsd =
[
    [ "PTK_Alg_FsdPfsdBoxParams", "structPTK__Alg__FsdPfsdBoxParams.html", [
      [ "orientation", "structPTK__Alg__FsdPfsdBoxParams.html#a1a8584b6acf6d5b65934fc5952d5bf73", null ],
      [ "x_offset", "structPTK__Alg__FsdPfsdBoxParams.html#ae1460b93cae4ca468b96adca382d9b63", null ],
      [ "y_offset", "structPTK__Alg__FsdPfsdBoxParams.html#a35abc77f9e73d92c647437f0676877e0", null ],
      [ "x_length", "structPTK__Alg__FsdPfsdBoxParams.html#aac8fbe0ac21985769543e6db50ca2c99", null ],
      [ "y_length", "structPTK__Alg__FsdPfsdBoxParams.html#a155475ec46080065ed0012aa7bd94e9d", null ],
      [ "pfsd_enable", "structPTK__Alg__FsdPfsdBoxParams.html#a2ed7224680502dc5bafa2a5ac79e0d40", null ],
      [ "pfsd_x_length", "structPTK__Alg__FsdPfsdBoxParams.html#a234574021663e800653496914d30f6d6", null ],
      [ "pfsd_x_start", "structPTK__Alg__FsdPfsdBoxParams.html#a86a9315f4a8911769a615f336f23f3d1", null ],
      [ "pfsd_x_end", "structPTK__Alg__FsdPfsdBoxParams.html#a8a89b55d87dbe8379a708bfc80c42cf9", null ],
      [ "pfsd_y_length", "structPTK__Alg__FsdPfsdBoxParams.html#aabe530d0b8f11855f5d1508d9b87a24c", null ]
    ] ],
    [ "PTK_Alg_FsdPfsdParams", "structPTK__Alg__FsdPfsdParams.html", [
      [ "checkFreeFlag", "structPTK__Alg__FsdPfsdParams.html#ad4ac2620247779ebc3335a8a20cace21", null ],
      [ "numBoxes", "structPTK__Alg__FsdPfsdParams.html#a7764d221745ce609f74e024b50e92d78", null ],
      [ "numBoxes_ogMap_1", "structPTK__Alg__FsdPfsdParams.html#a680e7b811e9a92259b816c1bcce3aaf6", null ],
      [ "numBoxes_ogMap_2", "structPTK__Alg__FsdPfsdParams.html#a027592bb655239305a62bda2ac140c6f", null ],
      [ "numBoxes_ogMap_3", "structPTK__Alg__FsdPfsdParams.html#a5b4225c689802879fc45b82f521f5f3c", null ],
      [ "numBoxes_ogMap_4", "structPTK__Alg__FsdPfsdParams.html#a8ee005ccff85e90059f0c06f31a199df", null ],
      [ "boxParams", "structPTK__Alg__FsdPfsdParams.html#a97dc9a8acc495755fe4d3030d3dff35e", null ],
      [ "searchRadius", "structPTK__Alg__FsdPfsdParams.html#a40085f7c6f5800564fccd677eb54f3b5", null ],
      [ "occGridId", "structPTK__Alg__FsdPfsdParams.html#a275a01fbdc88d2fb6514d4385735bd21", null ],
      [ "yawRateThresh", "structPTK__Alg__FsdPfsdParams.html#a4a4028303544d2219be1d9d5eca4c225", null ],
      [ "gridConfig", "structPTK__Alg__FsdPfsdParams.html#ab5b11f52485d6f4e28da2d95ccb45bff", null ],
      [ "ogFlagOccupied", "structPTK__Alg__FsdPfsdParams.html#ac7713ea9f62eccd0e6f6273795413972", null ],
      [ "ogFlagFree", "structPTK__Alg__FsdPfsdParams.html#a859ba31c7a004779cb7962a11a29ce6d", null ],
      [ "ogFlagFst", "structPTK__Alg__FsdPfsdParams.html#aa6ec3125c65777e3c4bdb8884a4d157b", null ],
      [ "ogFlagFsd", "structPTK__Alg__FsdPfsdParams.html#a73a4e8c6607241df148a6d74d0d21ddb", null ],
      [ "ogFlagPfsd", "structPTK__Alg__FsdPfsdParams.html#ad59476f42d93e4d88c917fbfffbcb1bb", null ],
      [ "minEgoDisp4Fsd", "structPTK__Alg__FsdPfsdParams.html#abc9b4014076386d8a85cdb3ea85bb4a9", null ],
      [ "prevInsX4FSD", "structPTK__Alg__FsdPfsdParams.html#a2106f6a7427115bb709384ab806abd72", null ],
      [ "prevInsY4FSD", "structPTK__Alg__FsdPfsdParams.html#a4aec296e29505e6aa40ef7baa9ef0b2f", null ],
      [ "newFSDCycle", "structPTK__Alg__FsdPfsdParams.html#a2fbfb86d438c00e2a2f917a2444998fc", null ],
      [ "reCheckPFSD", "structPTK__Alg__FsdPfsdParams.html#a572839b214ec0ddd05670cab30904899", null ]
    ] ],
    [ "PTK_Alg_FsdPfsdPSCoords", "structPTK__Alg__FsdPfsdPSCoords.html", [
      [ "x_1", "structPTK__Alg__FsdPfsdPSCoords.html#a64faf6aca3500a36816b3253b077694c", null ],
      [ "x_2", "structPTK__Alg__FsdPfsdPSCoords.html#a62ab9ea10f73ca17d8dbe5213b749d5c", null ],
      [ "y_1", "structPTK__Alg__FsdPfsdPSCoords.html#a3603b6a40cdfe244818b5ad0dde45e64", null ],
      [ "y_2", "structPTK__Alg__FsdPfsdPSCoords.html#a89e4dcbe1401a823562764a40669bd35", null ],
      [ "fsdBoxNum", "structPTK__Alg__FsdPfsdPSCoords.html#aa517d6f52ff57e7bcfb7ae8b4abb61b9", null ]
    ] ],
    [ "PTK_Alg_FsdPfsdPSDesc", "structPTK__Alg__FsdPfsdPSDesc.html", [
      [ "numSpaces", "structPTK__Alg__FsdPfsdPSDesc.html#a6955bbe255e11325ef5f4ed7c04478d0", null ],
      [ "psCoords", "structPTK__Alg__FsdPfsdPSDesc.html#a16e8e6f303c70608078da08343bfbe91", null ],
      [ "curInsRec", "structPTK__Alg__FsdPfsdPSDesc.html#af616b7a1af92f5696992dece9675e8c1", null ]
    ] ],
    [ "FSD_PFSD_MAX_NUM_BOXES", "group__group__ptk__algos__fsdpfsd.html#gaaeaffb2a34142c0ee4d0353f6617a96d", null ],
    [ "FSD_PFSD_MAX_PARKABLE_SPACES", "group__group__ptk__algos__fsdpfsd.html#ga1268dbd367bad2fd1e9876e716da72bf", null ],
    [ "PTK_Alg_FsdPfsdConfig", "group__group__ptk__algos__fsdpfsd.html#ga6b08ea18433d81f6379e99a65a9c520e", null ],
    [ "PTK_Alg_FsdPfsdInit", "group__group__ptk__algos__fsdpfsd.html#ga8b538c48a6fbd4bc9dbc9784d9787938", null ],
    [ "PTK_Alg_FsdPfsdProcess", "group__group__ptk__algos__fsdpfsd.html#ga1d454aee53a1ccf621a3528364cdbdd3", null ]
];