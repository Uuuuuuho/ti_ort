var group__group__ptk__algos__radar__common =
[
    [ "PTK_Alg_RadarDetObjOutput", "structPTK__Alg__RadarDetObjOutput.html", [
      [ "range", "structPTK__Alg__RadarDetObjOutput.html#a916a08de3c186b9ab10eced5c03901ac", null ],
      [ "azimuthAngle", "structPTK__Alg__RadarDetObjOutput.html#a46edd5f0820b456c8b6785fdfd16439c", null ],
      [ "velocity", "structPTK__Alg__RadarDetObjOutput.html#ae9b3a47fa8befb2cc3dc02f1b4ee438e", null ],
      [ "doaVarEst", "structPTK__Alg__RadarDetObjOutput.html#a101ee46680fa36d054d06124f8ac276b", null ],
      [ "snr", "structPTK__Alg__RadarDetObjOutput.html#a2fad1496e0c3382ad179067d9f0417f4", null ]
    ] ],
    [ "PTK_Alg_RadarDetOutput", "structPTK__Alg__RadarDetOutput.html", [
      [ "numObj", "structPTK__Alg__RadarDetOutput.html#a92b6ce0fd8eb3f944f8793e0a195bed7", null ],
      [ "objInfo", "structPTK__Alg__RadarDetOutput.html#adc887f366d8ed8dd3b412b577e8cced5", null ]
    ] ],
    [ "PTK_Alg_RadarSensorConfig", "structPTK__Alg__RadarSensorConfig.html", [
      [ "valid", "structPTK__Alg__RadarSensorConfig.html#a4cab16be3569f742c6a8ac018be59fd7", null ],
      [ "gatingAbsMaxAngle", "structPTK__Alg__RadarSensorConfig.html#ae911efc9bfb67f47145a910e02a7e450", null ],
      [ "gatingMinRange", "structPTK__Alg__RadarSensorConfig.html#a7f935533e1740cf0de0fd21bb7a754c7", null ],
      [ "gatingMaxRange", "structPTK__Alg__RadarSensorConfig.html#ae7eedfcb784c4ec5748b17ebebd6de9f", null ],
      [ "gatingMinSnr", "structPTK__Alg__RadarSensorConfig.html#a7cb0f03044620c38c1ddeab250b401d3", null ],
      [ "sensorOrientation", "structPTK__Alg__RadarSensorConfig.html#aebe1fc7dbb28a8d912a0601e112df6ff", null ],
      [ "sensorPosition_x", "structPTK__Alg__RadarSensorConfig.html#abdb648d3b981b8c176156301571f714e", null ],
      [ "sensorPosition_y", "structPTK__Alg__RadarSensorConfig.html#a6da16073121a14c0c626cb990e9ae808", null ]
    ] ],
    [ "PTK_ALG_RADAR_MAX_OBJ_PER_FRAME", "group__group__ptk__algos__radar__common.html#gac97ff5eed4ead36591669d09a728b3b3", null ],
    [ "PTK_ALG_RADAR_NUM_SENSORS", "group__group__ptk__algos__radar__common.html#gaf90cf51cbe0123421277f5842266d96f", null ]
];