var group__group__ptk__lidar =
[
    [ "PTK_LidarGatingParams", "structPTK__LidarGatingParams.html", [
      [ "valid", "structPTK__LidarGatingParams.html#aa9bcf40d7a099c2ef26d910fdda81626", null ],
      [ "gatingAbsMaxAngle", "structPTK__LidarGatingParams.html#ae890df2998e980f6470289639f1e42d6", null ],
      [ "gatingMinRange", "structPTK__LidarGatingParams.html#a8cab5c38ff071710eb3e6f017b8bc7d1", null ],
      [ "gatingMaxRange", "structPTK__LidarGatingParams.html#a720ce9c8244c5f89da8c66d5871f0f9f", null ],
      [ "laserEnableMask", "structPTK__LidarGatingParams.html#ae25bcf2190b9c273819d6bd845350642", null ]
    ] ],
    [ "PTK_LidarMetaConfig", "structPTK__LidarMetaConfig.html", [
      [ "lasers", "structPTK__LidarMetaConfig.html#a3fcf4649562c1573a665129a8a687cc5", null ],
      [ "slices", "structPTK__LidarMetaConfig.html#ad7fa903dfb5b8f041b67256d33fda3d1", null ],
      [ "sinThetaTable", "structPTK__LidarMetaConfig.html#ab38c471fc5c94a2d24c737b8d8c2b84e", null ],
      [ "cosThetaTable", "structPTK__LidarMetaConfig.html#a34eda693bd66b4f853e22c7dcd11d8cf", null ],
      [ "sinAlphaTable", "structPTK__LidarMetaConfig.html#a55573abb5b94fc8e2b9b6a50c49fee81", null ],
      [ "cosAlphaTable", "structPTK__LidarMetaConfig.html#a231633e420ebe93fa78a4c0d0bf179db", null ],
      [ "distance", "structPTK__LidarMetaConfig.html#a863005279d00143eaa97d5a6cb0588c7", null ],
      [ "scale", "structPTK__LidarMetaConfig.html#a5a10311f3dae6bda14777e4bdfa15bf4", null ],
      [ "angle", "structPTK__LidarMetaConfig.html#af38c5000443ff9af89e4eb79e0c99fd9", null ],
      [ "height", "structPTK__LidarMetaConfig.html#a02315f79074ea73711530a0a31212339", null ],
      [ "gatingParams", "structPTK__LidarMetaConfig.html#ae628826b127b23da415df7d389bef71e", null ]
    ] ],
    [ "PTK_LidarMeta", "structPTK__LidarMeta.html", [
      [ "config", "structPTK__LidarMeta.html#a4b269a4a67d87f72cc62f443a258c2b1", null ],
      [ "pointCount", "structPTK__LidarMeta.html#a371c5172e33fc5a4a9892073f025a9d1", null ],
      [ "slicesFound", "structPTK__LidarMeta.html#acbed3ac7b5a5beec8ae08bad4f8f7e00", null ],
      [ "timeOffset", "structPTK__LidarMeta.html#a17ae9a5a817fb0062158abbe17acb441", null ],
      [ "pointOffset", "structPTK__LidarMeta.html#a73b5c6c6c119495036cc128689b4b3e2", null ]
    ] ],
    [ "PTK_Lidar_VelodyneRecord", "structPTK__Lidar__VelodyneRecord.html", [
      [ "startIdentifier", "structPTK__Lidar__VelodyneRecord.html#a53ab2b40b3134791e52a65cf0352cea0", null ],
      [ "theta", "structPTK__Lidar__VelodyneRecord.html#abc699b5a0d87fbe763188624d4dffb20", null ],
      [ "raw", "structPTK__Lidar__VelodyneRecord.html#a8d7f9bb3e7313570c13fbca05435f6ea", null ]
    ] ],
    [ "PTK_Lidar_VelodynePacket", "structPTK__Lidar__VelodynePacket.html", [
      [ "records", "structPTK__Lidar__VelodynePacket.html#a3f9b02b92f68ab03db1c82f8443f4ffb", null ],
      [ "gpsTimestamp", "structPTK__Lidar__VelodynePacket.html#a2554459a292ffcdfcd4acdf038fc2c10", null ],
      [ "reserved", "structPTK__Lidar__VelodynePacket.html#a5d23cad2b550c9362420d8065a221e81", null ],
      [ "systemTimestamp", "structPTK__Lidar__VelodynePacket.html#ac2fbdd1ae71c35e6cc7ecdbfaddcb70f", null ]
    ] ],
    [ "PTK_Lidar_VelodyneFrame", "structPTK__Lidar__VelodyneFrame.html", [
      [ "packets", "structPTK__Lidar__VelodyneFrame.html#a9f6f3e5ae74de6c7ebf53009c31bdc29", null ]
    ] ],
    [ "PTK_LIDAR_DEFAULT_LASER_ENABLE_MASK", "group__group__ptk__lidar.html#gac069813cf1654e37aa4e60fe4436a0aa", null ],
    [ "PTK_LIDAR_MAX_GATING_ANGLE", "group__group__ptk__lidar.html#ga929eb7c04308ced8cf8294bc9993114c", null ],
    [ "PTK_LidarMeta_getPointsData", "group__group__ptk__lidar.html#ga04a9897f75f6dfa4266a9356ea0fb51f", null ],
    [ "PTK_LidarMeta_getTimestampData", "group__group__ptk__lidar.html#ga365dbfa372b14bc5ff0713e8da1e7848", null ],
    [ "PTK_LIDAR_HDL32E_LASERS", "group__group__ptk__lidar.html#ga1c37e9fabb616d4c6948aebb9bf31d6d", null ],
    [ "PTK_LIDAR_HDL32E_RECORDS_PER_PACKET", "group__group__ptk__lidar.html#ga0f6dabc6b8644e0894fef6e3a9523678", null ],
    [ "PTK_LIDAR_HDL32E_MAX_PACKETS", "group__group__ptk__lidar.html#gaeb129eefa0ad3e88a8de9d18f15fffb4", null ],
    [ "PTK_LIDAR_HDL32E_SLICES_PER_ROTATION", "group__group__ptk__lidar.html#ga0532a68103e936375692fd63a7b89801", null ],
    [ "PTK_LIDAR_HDL32E_POINTS_PER_ROTATION", "group__group__ptk__lidar.html#gaf582046883370907b0e84a0013bdcb3a", null ],
    [ "PTK_LidarMeta_getSize", "group__group__ptk__lidar.html#ga9fd0658835745de2fab90b4d3a233664", null ],
    [ "PTK_LidarMeta_init", "group__group__ptk__lidar.html#gaa3d77a1ee960409affd5e93944189dcf", null ],
    [ "PTK_LidarMeta_clear", "group__group__ptk__lidar.html#ga16decce248405a150cb8461be0e20dbd", null ],
    [ "PTK_LidarMeta_getSlices", "group__group__ptk__lidar.html#gab092332f638f3c35e0313fd690ec538c", null ],
    [ "PTK_LidarMeta_getLasers", "group__group__ptk__lidar.html#gab0bf6d971d5ee9e42702055f65b8e67d", null ],
    [ "PTK_LidarMeta_getTimestamp", "group__group__ptk__lidar.html#ga5a6c14adb8e5a63ff76e876a5c1ee203", null ],
    [ "PTK_LidarMeta_getPointIndex", "group__group__ptk__lidar.html#ga17d495d379f6001b3881d6cbd7a5c844", null ],
    [ "PTK_LidarMeta_setPointIndex", "group__group__ptk__lidar.html#ga6e83080e63c43aeeaee00e82ab542191", null ],
    [ "PTK_Lidar_Velodyne_initParser", "group__group__ptk__lidar.html#ga2ceb4f77ac5895a5ffab1a9783b09c29", null ],
    [ "PTK_Lidar_Velodyne_setHDL32ETrigTables", "group__group__ptk__lidar.html#gae9d3084c37fa2ef2dc8a51780af8c344", null ],
    [ "PTK_Lidar_Velodyne_parse", "group__group__ptk__lidar.html#ga3b962e5be44af4b6021a0833891fa2c7", null ],
    [ "PTK_Lidar_Velodyne_mapPhysicalToLogicalLaser", "group__group__ptk__lidar.html#gab7b8a55de4d8035503bab12c70774416", null ]
];