var group__group__ptk__algos__lidar__gpc =
[
    [ "PTK_Lidar_GpcConfig", "structPTK__Lidar__GpcConfig.html", [
      [ "z_threshold", "structPTK__Lidar__GpcConfig.html#ab14e09503eebb317a6b162907015d72c", null ],
      [ "ground_dtol", "structPTK__Lidar__GpcConfig.html#a539ace67e4202ab59c15d34c32c96311", null ],
      [ "ransac_iter", "structPTK__Lidar__GpcConfig.html#a20825e6a769f3f8523d8c02915a95463", null ],
      [ "removed_tag", "structPTK__Lidar__GpcConfig.html#ad5192613d550ddbee426e5ea2643e4b3", null ],
      [ "ground_tag", "structPTK__Lidar__GpcConfig.html#a1c1216d1ea7b0fb13eef0a74ce567f49", null ]
    ] ],
    [ "PTK_Lidar_getGpcScratchMemSize", "group__group__ptk__algos__lidar__gpc.html#ga503579916a3a9bc68207056ea075796c", null ],
    [ "PTK_Lidar_getEstNormScratchMemSize", "group__group__ptk__algos__lidar__gpc.html#gac2b769e01172ac591b768baf23c4ace7", null ],
    [ "PTK_Lidar_performGPC", "group__group__ptk__algos__lidar__gpc.html#gada5f3ff576f47b524f234b4f635eb694", null ],
    [ "PTK_Lidar_estimatePointNormals", "group__group__ptk__algos__lidar__gpc.html#ga4a61cecf10623ab4d478cbab506e3708", null ],
    [ "PTK_Lidar_shiftToRootENU", "group__group__ptk__algos__lidar__gpc.html#ga11570ba9e5dd3c3b4a4cdc4bab5e158e", null ]
];