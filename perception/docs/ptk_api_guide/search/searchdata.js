var indexSectionsWithContent =
{
  0: "_abcdefghiklmnopqrstuvwxyz~",
  1: "_abcdfghiklmpqrstuvz",
  2: "pu",
  3: "_abcdefghilmnopqrstuvwz",
  4: "_abcdefghilmnopqrstuvwz~",
  5: "_abcdefghiklmnopqrstuvwxyz",
  6: "bdefgiklmprsuvw",
  7: "acdfgloprs",
  8: "abcdfghilmnprstuvwxyz",
  9: "_acdfgimnpqrstv",
  10: "dpsv",
  11: "pt"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "typedefs",
  7: "enums",
  8: "enumvalues",
  9: "defines",
  10: "groups",
  11: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Data Structures",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Typedefs",
  7: "Enumerations",
  8: "Enumerator",
  9: "Macros",
  10: "Modules",
  11: "Pages"
};

