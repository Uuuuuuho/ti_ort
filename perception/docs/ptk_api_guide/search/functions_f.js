var searchData=
[
  ['quadrenderable',['QuadRenderable',['../classptk_1_1QuadRenderable.html#a4a425f4510a25063ca7b4578773de0db',1,'ptk::QuadRenderable::QuadRenderable(const QuadRenderable &amp;)=delete'],['../classptk_1_1QuadRenderable.html#ac49119a2120db32253decdaefd648f47',1,'ptk::QuadRenderable::QuadRenderable(uint16_t quads)']]],
  ['quadrenderer',['QuadRenderer',['../classptk_1_1QuadRenderer.html#ab63a2ec5f13913eaeaa3bb4e30b537c2',1,'ptk::QuadRenderer::QuadRenderer()'],['../classptk_1_1QuadRenderer.html#ad59fdfb6e4d1d0ccef90b96ff78207aa',1,'ptk::QuadRenderer::QuadRenderer(const QuadRenderer &amp;)=delete']]],
  ['quadrendererbase',['QuadRendererBase',['../classptk_1_1QuadRendererBase.html#ab6d3e1162e63ee17c8e1d04078bf58b0',1,'ptk::QuadRendererBase::QuadRendererBase()'],['../classptk_1_1QuadRendererBase.html#a72f2097b40f747015933422242b14fb5',1,'ptk::QuadRendererBase::QuadRendererBase(const QuadRendererBase &amp;)=delete']]]
];
