var searchData=
[
  ['validate_5fprogram',['validate_program',['../classptk_1_1Shader.html#ac5b8c1de25b0d5a1ce25d8930e780c53',1,'ptk::Shader']]],
  ['virtualsensorcreator',['VirtualSensorCreator',['../classptk_1_1VirtualSensorCreator.html#a732a6d907a7cba6d39c158dbfcd012ff',1,'ptk::VirtualSensorCreator::VirtualSensorCreator()'],['../classptk_1_1VirtualSensorCreator.html#aafdc115f7d3fbb127a1df6d8460a42f5',1,'ptk::VirtualSensorCreator::VirtualSensorCreator(const std::string &amp;databaseDir, int seqId, const std::string &amp;parentFolderName, const std::string &amp;outFolderName, const std::string &amp;descTxtFiles, enum data_type_e dataType, bool overwriteExistingData)']]],
  ['virtualsensorcreator_5fadd_5frecord',['VirtualSensorCreator_add_record',['../group__group__ptk__virtual__sensor.html#ga43895f09b6fe60c743918c5f878b52a6',1,'virtual_sensor_creator.h']]],
  ['virtualsensorcreator_5fcreate',['VirtualSensorCreator_create',['../group__group__ptk__virtual__sensor.html#gaa38bd6e04a1ac9d17136e866ae12ac74',1,'virtual_sensor_creator.h']]],
  ['virtualsensorcreator_5fcreate_5fby_5fdbconfig',['VirtualSensorCreator_create_by_dbconfig',['../c_2virtual__sensor__creator_8h.html#abcae61e564186179654130a34f859d34',1,'virtual_sensor_creator.h']]],
  ['virtualsensorcreator_5fdelete',['VirtualSensorCreator_delete',['../group__group__ptk__virtual__sensor.html#gac27bd6bf1fb9e920ea1fc506bbf43927',1,'virtual_sensor_creator.h']]],
  ['virtualsensorcreator_5fis_5fdisabled',['VirtualSensorCreator_is_disabled',['../group__group__ptk__virtual__sensor.html#ga268cfc764ec18cc4eeee1a505630b699',1,'virtual_sensor_creator.h']]],
  ['visible',['Visible',['../classptk_1_1Visible.html#a12fbd40c9335e21965d90e2b93b92dfd',1,'ptk::Visible::Visible(void)'],['../classptk_1_1Visible.html#acf088dcd29367073af102ee810de03e1',1,'ptk::Visible::Visible(bool visible)']]]
];
