var group__group__ptk__net__utils =
[
    [ "PTK_GenDataType", "unionPTK__GenDataType.html", [
      [ "u32", "unionPTK__GenDataType.html#ad6f0d01dbcbaa4bdf798a303c3800545", null ],
      [ "u64", "unionPTK__GenDataType.html#a1584d3dac4b099e4d4e21b5fc66f85be", null ],
      [ "ptr", "unionPTK__GenDataType.html#ab0d33c9e116f1454cd5ca7615d28b873", null ]
    ] ],
    [ "PTK_NetAddr", "unionPTK__NetAddr.html", [
      [ "inet", "unionPTK__NetAddr.html#a4019ac0cc3583be057218b1dc54c7460", null ],
      [ "unet", "unionPTK__NetAddr.html#a46a98992fa71310f5eab531efb2751cf", null ],
      [ "addr", "unionPTK__NetAddr.html#ade8f7cc9bafa3767ecb38fb20ab1c552", null ]
    ] ],
    [ "PTK_NetConnCntxt", "structPTK__NetConnCntxt.html", [
      [ "state", "structPTK__NetConnCntxt.html#aa21ef11330d89214dbbd39e342343429", null ],
      [ "myId", "structPTK__NetConnCntxt.html#acb7ca016c4b1ed6099d33bf3597c98f8", null ],
      [ "sockId", "structPTK__NetConnCntxt.html#acb4ef65935d2d85cce6d25568cdde187", null ],
      [ "rdWrSize", "structPTK__NetConnCntxt.html#a5285773383aec15c307e47e0a936ff07", null ],
      [ "rdWrOffset", "structPTK__NetConnCntxt.html#a90b65947fcac4b9b220a7a2da0ed6e17", null ],
      [ "rspPayload", "structPTK__NetConnCntxt.html#ae670f6a0870e1dabb107b5269d1282f7", null ],
      [ "auxData", "structPTK__NetConnCntxt.html#aa2be455d5ba2969e6676499c62be85dc", null ]
    ] ],
    [ "PTK_NetSrvCntxt", "structPTK__NetSrvCntxt.html", [
      [ "state", "structPTK__NetSrvCntxt.html#a3728f244e835a6dcb7bad249571a1b51", null ],
      [ "svrSock", "structPTK__NetSrvCntxt.html#aae1d6f68923be39b89eb7b5e3adcdc3d", null ],
      [ "netParam", "structPTK__NetSrvCntxt.html#a0218553db5c31cd9d6b817be0cafbc6b", null ],
      [ "sockType", "structPTK__NetSrvCntxt.html#ab2932fb4a79b28bfd06c6510be44e32e", null ],
      [ "svrType", "structPTK__NetSrvCntxt.html#aa6a59cf76de291510b6c72233181cb20", null ],
      [ "events", "structPTK__NetSrvCntxt.html#a5f43a3d462e8ada8e5d543cd9e03f3da", null ],
      [ "cliHdlr", "structPTK__NetSrvCntxt.html#a48c554f96a0e21f2fe95351bf8126d59", null ],
      [ "listenCnt", "structPTK__NetSrvCntxt.html#a9c41804ee01f785abc40641e91b9ba43", null ],
      [ "evtMask", "structPTK__NetSrvCntxt.html#ade18e312b4876b4211f5ffeb5ecfe8b4", null ],
      [ "epollFd", "structPTK__NetSrvCntxt.html#acb5825ffd10a617e22bb9fe7d4837faa", null ],
      [ "memAllocator", "structPTK__NetSrvCntxt.html#a541e0fe476d75df8564c72993012a69c", null ],
      [ "connCntxt", "structPTK__NetSrvCntxt.html#a7233c170241250a006faf5dd34f0332a", null ],
      [ "appData", "structPTK__NetSrvCntxt.html#a2e0d94c7a2e8a1f127551fced120ae82", null ]
    ] ],
    [ "PTK_MAX_NET_LISTEN_CNT", "group__group__ptk__net__utils.html#ga139b749a0bcbcd46b5a06daf076a9f42", null ],
    [ "PTK_NET_MAX_XFER_SIZE", "group__group__ptk__net__utils.html#gad99e39f565556840c535f03780e8dbe5", null ],
    [ "PTK_NET_RET_FAILURE", "group__group__ptk__net__utils.html#ga77b4651efb71c28aaa2a2d572c6752cd", null ],
    [ "PTK_NET_RET_SUCCESS", "group__group__ptk__net__utils.html#gae556ee684b3f1425c534c2159fa560d0", null ],
    [ "PTK_NET_RET_PENDING", "group__group__ptk__net__utils.html#ga91c76736aa2e59392f85920600d80903", null ],
    [ "PTK_NET_SOCK_CREATE_ERROR", "group__group__ptk__net__utils.html#ga43a69ac58bda38f918a0cf85a91cbfad", null ],
    [ "PTK_NET_SOCK_BIND_ERROR", "group__group__ptk__net__utils.html#ga38e6d80094c3ba5da34566a663308d74", null ],
    [ "PTK_NET_SOCK_LISTEN_ERROR", "group__group__ptk__net__utils.html#gaaee6f80632522830620b454463b76fc2", null ],
    [ "PTK_NET_SOCK_CONNECT_ERROR", "group__group__ptk__net__utils.html#gaac2852ed5a767fe1ae83fd99df93e435", null ],
    [ "PTK_NET_SOCK_RCV_ERROR", "group__group__ptk__net__utils.html#ga8a5fb3dd77261ceb04b6f8d1bfce8f46", null ],
    [ "PTK_NET_SOCK_SEND_ERROR", "group__group__ptk__net__utils.html#ga35470fa81b0321462f6489e8b9ca8a94", null ],
    [ "PTK_NET_SOCK_FCNTL_ERROR", "group__group__ptk__net__utils.html#ga1fae835635ec630f3a885e4c49d82d11", null ],
    [ "PTK_NET_SOCK_PEER_SHUTDOWN", "group__group__ptk__net__utils.html#ga24fcdd2cdc71153c1edf1b2d5772abe2", null ],
    [ "PTK_NET_SOCK_STATE_INVALID", "group__group__ptk__net__utils.html#gaca66c77f16011409f3d7be62865b738d", null ],
    [ "PTK_netCliHdlr", "group__group__ptk__net__utils.html#ga6fbc6d2094fae771d8e5e0ca3e37a416", null ],
    [ "PTK_IoCtl", "group__group__ptk__net__utils.html#gaac0f528a169141fba61d10b418049b74", [
      [ "PTK_IoCtl_BLOCKING", "group__group__ptk__net__utils.html#ggaac0f528a169141fba61d10b418049b74add8385b6b2854595a535ccf715215557", null ],
      [ "PTK_IoCtl_NON_BLOCKING", "group__group__ptk__net__utils.html#ggaac0f528a169141fba61d10b418049b74a11efe7e469d51cfb38ef9a2a94f5b0eb", null ]
    ] ],
    [ "PTK_NetSvrType", "group__group__ptk__net__utils.html#ga6a8f856b7ad70cb3cfced9aee6d78b0a", [
      [ "PTK_NetSvrType_ITER", "group__group__ptk__net__utils.html#gga6a8f856b7ad70cb3cfced9aee6d78b0aacedbf34dc4095c34601974bffc9ef985", null ],
      [ "PTK_NetSvrType_CONCUR", "group__group__ptk__net__utils.html#gga6a8f856b7ad70cb3cfced9aee6d78b0aa35364862bdbae0acc4a7bd9490e3b759", null ]
    ] ],
    [ "PTK_NetSockType", "group__group__ptk__net__utils.html#gabeaacf67c44ce22686acdce8131cd164", [
      [ "PTK_NetSockType_TCP", "group__group__ptk__net__utils.html#ggabeaacf67c44ce22686acdce8131cd164afd3832c70ae30abd6142fc8231a4be8e", null ],
      [ "PTK_NetSockType_UDP", "group__group__ptk__net__utils.html#ggabeaacf67c44ce22686acdce8131cd164a056952217c8ad510dcb446e435a3c0b0", null ],
      [ "PTK_NetSockType_UNIX_STRM", "group__group__ptk__net__utils.html#ggabeaacf67c44ce22686acdce8131cd164a70c582748fbfb5165a51a0b055304cc6", null ],
      [ "PTK_NetSockType_LAST", "group__group__ptk__net__utils.html#ggabeaacf67c44ce22686acdce8131cd164a56f48f5cfec1172bf74ec1b3ee46a366", null ]
    ] ],
    [ "PTK_netTcpSockOpen", "group__group__ptk__net__utils.html#ga7cc2ad1900faeb62dfe90251da3ff459", null ],
    [ "PTK_netTcpConnect", "group__group__ptk__net__utils.html#ga9637810fdb0a58fc86568678f26875b4", null ],
    [ "PTK_netUnixSockOpen", "group__group__ptk__net__utils.html#ga50c5f228f7fe0d6e010c3fde11c6b91b", null ],
    [ "PTK_netUnixConnect", "group__group__ptk__net__utils.html#ga46d65e58a12e0658e411cdf22227b001", null ],
    [ "PTK_netUdpSockOpen", "group__group__ptk__net__utils.html#ga3e8f134cc8f5e666ecf74636c956e4db", null ],
    [ "PTK_netReadSock", "group__group__ptk__net__utils.html#gabd8f1242e8dfa04b1a11e4aeca4fe547", null ],
    [ "PTK_netWriteSock", "group__group__ptk__net__utils.html#ga728dc5c92baf7c0c5afa9ab0d9d40c94", null ],
    [ "PTK_netReadMsg", "group__group__ptk__net__utils.html#ga3282d6aed10a586c1867cba1d02438b3", null ],
    [ "PTK_netSendMsg", "group__group__ptk__net__utils.html#gad9d83d7b07d8ac75b2c6afe4a30b19ef", null ],
    [ "PTK_netSockNonBlockRead", "group__group__ptk__net__utils.html#gaefa37ed05ff5244ad9e38a0954f13ce0", null ],
    [ "PTK_netSockNonBlockWrite", "group__group__ptk__net__utils.html#ga97fcf6292b1e8b3f904360c1e7d1fd32", null ],
    [ "PTK_netSetSockBlockingMode", "group__group__ptk__net__utils.html#ga44dcb108242825755d610fb0ed59ca59", null ],
    [ "PTK_netCreateSvrSocket", "group__group__ptk__net__utils.html#ga4bd5e611d25ade7df1c4523a2eefbc0f", null ],
    [ "PTK_netThread", "group__group__ptk__net__utils.html#gaf4a1ef591550928d7662d194efdde1a7", null ]
];