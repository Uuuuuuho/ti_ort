var group__group__ptk__api =
[
    [ "PTK Algorithms", "group__group__ptk__algos.html", "group__group__ptk__algos" ],
    [ "PTK Base", "group__group__ptk__base.html", "group__group__ptk__base" ],
    [ "PTK Database Tools", "group__group__ptk__dbtools.html", "group__group__ptk__dbtools" ],
    [ "PTK Driver Component", "group__group__ptk__drv.html", "group__group__ptk__drv" ],
    [ "PTK External API support", "group__group__ptk__ext__dependencies.html", "group__group__ptk__ext__dependencies" ]
];