var group__group__ptk__db__config =
[
    [ "PTK_DBConfig_Sensor", "structPTK__DBConfig__Sensor.html", [
      [ "folder", "structPTK__DBConfig__Sensor.html#a07ae127a7721badc04ce3a80921c4f17", null ],
      [ "type", "structPTK__DBConfig__Sensor.html#a63591af85f7cc87f563af697783fcf10", null ],
      [ "appTag", "structPTK__DBConfig__Sensor.html#a5c15358810322de86e894a775d286641", null ]
    ] ],
    [ "PTK_DBConfig_VirtualSensor", "structPTK__DBConfig__VirtualSensor.html", [
      [ "folder", "structPTK__DBConfig__VirtualSensor.html#a63d6c4e3a6bb603e8fb88ec2201e2d16", null ],
      [ "appTag", "structPTK__DBConfig__VirtualSensor.html#a100667ae1405f3b89d05c461de075083", null ],
      [ "parentSensorSpecified", "structPTK__DBConfig__VirtualSensor.html#ac15308b96aa3c0f76e31cd588498bbaf", null ],
      [ "parentSensorFolder", "structPTK__DBConfig__VirtualSensor.html#aab98ab4fde0c932104e6ee8f070e4623", null ],
      [ "dataType", "structPTK__DBConfig__VirtualSensor.html#aabafcd80eee41003939cb8fee25f11e4", null ],
      [ "numDescFiles", "structPTK__DBConfig__VirtualSensor.html#ae4d7b8ffad6aa964a0d30544cdf29a90", null ],
      [ "descFilesPaths", "structPTK__DBConfig__VirtualSensor.html#a78fb6d9d06ce716d353e57eacc588500", null ]
    ] ],
    [ "PTK_DBConfig", "structPTK__DBConfig.html", [
      [ "databasePath", "structPTK__DBConfig.html#a366642a04024f96ce21e0fef2b09f02a", null ],
      [ "dataSeqId", "structPTK__DBConfig.html#ae6a766490e5d5107c046d8b7c0d3f3f2", null ],
      [ "refSensor", "structPTK__DBConfig.html#aae166edb23199e40bce8a23a46fb1cea", null ],
      [ "startFrame", "structPTK__DBConfig.html#a7f1aed9281fb6a511e3130b1af4d67fb", null ],
      [ "endFrame", "structPTK__DBConfig.html#aac2c643db765af75477345954cd3b5c5", null ],
      [ "numFrames", "structPTK__DBConfig.html#ad74a6dc326176b80040736a3cd112093", null ],
      [ "numSensors", "structPTK__DBConfig.html#a43091b8de62186eb151ce8261bf76db1", null ],
      [ "sensors", "structPTK__DBConfig.html#aa778810a87b0edae7b9478f20993fab5", null ],
      [ "numSensorsPerType", "structPTK__DBConfig.html#ac1b6bbcf7a868c4f3d930c40e1ed9fc6", null ],
      [ "numOutputs", "structPTK__DBConfig.html#ae94146fee8f5e429ea9fc7d99b1838ef", null ],
      [ "outputs", "structPTK__DBConfig.html#a8afeccdc761b9caa7a066565d6a0a5af", null ],
      [ "overwriteExistingData", "structPTK__DBConfig.html#a5d37d8d8c80261d117db1498e8de34f7", null ]
    ] ],
    [ "DBCONFIG_MAX_NUM_SENSORS", "group__group__ptk__db__config.html#gac8f1857087efb548d791264bbacd00c3", null ],
    [ "DBCONFIG_MAX_NUM_OUTPUTS", "group__group__ptk__db__config.html#ga425ac88e92fa9267d95187911c00dae8", null ],
    [ "DBCONFIG_MAX_DESC_FILES", "group__group__ptk__db__config.html#gaae129324cba6a2029677289a4938c1fc", null ],
    [ "PTK_DBConfig_SensorType", "group__group__ptk__db__config.html#ga169f4be3abd86d279dc1bf17a72c901f", [
      [ "dbconfig_sensor_type_CAMERA", "group__group__ptk__db__config.html#gga169f4be3abd86d279dc1bf17a72c901fab3be9ed8f6b0b96b2a7a84e422bbff73", null ],
      [ "dbconfig_sensor_type_RADAR", "group__group__ptk__db__config.html#gga169f4be3abd86d279dc1bf17a72c901fa9accd5d1fc732b79e2ec4eae7a90b16f", null ],
      [ "dbconfig_sensor_type_LIDAR", "group__group__ptk__db__config.html#gga169f4be3abd86d279dc1bf17a72c901fa047dbf2e8800d5d42047b485a0c4104a", null ],
      [ "dbconfig_sensor_type_INS", "group__group__ptk__db__config.html#gga169f4be3abd86d279dc1bf17a72c901fa3eb6ad1f82e85b4768f59e31aac3ad6e", null ],
      [ "dbconfig_sensor_type_IMU", "group__group__ptk__db__config.html#gga169f4be3abd86d279dc1bf17a72c901faa303a9b902637f25f108968d9e317145", null ],
      [ "dbconfig_sensor_type_GPS", "group__group__ptk__db__config.html#gga169f4be3abd86d279dc1bf17a72c901fa1fb3c6ad3aee25586a48f2edee53423e", null ],
      [ "dbconfig_sensor_type_TIME", "group__group__ptk__db__config.html#gga169f4be3abd86d279dc1bf17a72c901faa307b63b0e1112b1e842222a07213a4c", null ],
      [ "dbconfig_sensor_type_OTHER", "group__group__ptk__db__config.html#gga169f4be3abd86d279dc1bf17a72c901fa77618489b084a17b1da6d0850edb5ae7", null ],
      [ "dbconfig_sensor_type_MAX", "group__group__ptk__db__config.html#gga169f4be3abd86d279dc1bf17a72c901facbbee2d4428cf22b0d99b7f8f8ea955a", null ]
    ] ],
    [ "PTK_DBConfig_parse", "group__group__ptk__db__config.html#ga31497e163aec1f0ca79b61a0fab2543b", null ],
    [ "PTK_DBConfig_print", "group__group__ptk__db__config.html#ga0a96da64e0fa7082c6c010f22ef18f32", null ],
    [ "PTK_DBConfig_exist_sensor_apptag", "group__group__ptk__db__config.html#gae9fe65b1ec9a7afce2a38c5c00d2ebfd", null ],
    [ "PTK_DBConfig_exist_output_apptag", "group__group__ptk__db__config.html#ga623bd4c8a7729d5ff20f3ccec69331bb", null ]
];