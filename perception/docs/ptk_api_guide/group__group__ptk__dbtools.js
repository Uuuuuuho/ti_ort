var group__group__ptk__dbtools =
[
    [ "Database Configuration", "group__group__ptk__db__config.html", "group__group__ptk__db__config" ],
    [ "Sensor Data Player", "group__group__ptk__sensor__data__player.html", "group__group__ptk__sensor__data__player" ],
    [ "Sensor Stream Handling", "group__group__ptk__sensor__stream.html", "group__group__ptk__sensor__stream" ],
    [ "Virtual Sensor Creator", "group__group__ptk__virtual__sensor.html", "group__group__ptk__virtual__sensor" ]
];