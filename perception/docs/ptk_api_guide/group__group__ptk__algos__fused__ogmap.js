var group__group__ptk__algos__fused__ogmap =
[
    [ "PTK_Alg_FusedOgmapTime", "structPTK__Alg__FusedOgmapTime.html", [
      [ "cameraFramsTs", "structPTK__Alg__FusedOgmapTime.html#a829cc8a66601f705380b1618708e7617", null ],
      [ "radarFramsTs", "structPTK__Alg__FusedOgmapTime.html#a3034685e28d071acc3dee35a9eafe068", null ],
      [ "lidarFramsTs", "structPTK__Alg__FusedOgmapTime.html#a07c64d6c0b39ab674b1487cc923eca36", null ],
      [ "psFrameTs", "structPTK__Alg__FusedOgmapTime.html#a018b6153cc40c9912f06a720a73f23db", null ],
      [ "psImgFrameTs", "structPTK__Alg__FusedOgmapTime.html#a2cc9e6a7b53e8f18bd26a4fa6217c826", null ]
    ] ],
    [ "PTK_Alg_FusedOgmapParams", "structPTK__Alg__FusedOgmapParams.html", [
      [ "sensorEnableMask", "structPTK__Alg__FusedOgmapParams.html#aa2eb03e9f624d20915c2a35b02ed8b9a", null ],
      [ "sensorDataValidMask", "structPTK__Alg__FusedOgmapParams.html#a722e295ba1ab41aa6d6810ac2c18bddd", null ],
      [ "ogFlagOccupied", "structPTK__Alg__FusedOgmapParams.html#ab0f7ad3621057afa3cb59d5454c82c2c", null ],
      [ "ogFlagFree", "structPTK__Alg__FusedOgmapParams.html#a62b294686c411abb8a3981f4c1f04979", null ],
      [ "ogFlagChanged", "structPTK__Alg__FusedOgmapParams.html#a2b6564fe4aa83d53cc9c023f36f49244", null ],
      [ "radarGridId", "structPTK__Alg__FusedOgmapParams.html#ac8c14daa48bcacb8d34b4274b07f3615", null ],
      [ "lidarGridId", "structPTK__Alg__FusedOgmapParams.html#a25882117a5796fc5e9b24e4cdb932947", null ],
      [ "cameraGridId", "structPTK__Alg__FusedOgmapParams.html#afff6e9d7418f6fd67be011b0ae5da1a0", null ],
      [ "outGridId", "structPTK__Alg__FusedOgmapParams.html#a038cdb099b9d7264dffaffca18e14dd3", null ],
      [ "fusedGridId", "structPTK__Alg__FusedOgmapParams.html#a16a1638813cbff7c1f0b55b31a7dc048", null ],
      [ "gridConfig", "structPTK__Alg__FusedOgmapParams.html#a3d655718538e84adee69d42fac1850bb", null ],
      [ "roiParams", "structPTK__Alg__FusedOgmapParams.html#a9f3594f18b2a6f591783f46808a01253", null ],
      [ "fusionMethod", "structPTK__Alg__FusedOgmapParams.html#a52da1e0f54d88508e01286635376aa98", null ],
      [ "fusionTable", "structPTK__Alg__FusedOgmapParams.html#ad9284f8d5f59020abff038716dd5e8f0", null ],
      [ "dsWeights", "structPTK__Alg__FusedOgmapParams.html#afc7324545867751ad84dd4356ba3f399", null ],
      [ "dsThresh", "structPTK__Alg__FusedOgmapParams.html#a6d8d8dbd28c2b722b32f85b0ec772b0e", null ]
    ] ],
    [ "PTK_ALG_FUSED_OGMAP_METHOD_VOTING", "group__group__ptk__algos__fused__ogmap.html#ga98057df44eee9fbfc5ce3026181a1bde", null ],
    [ "PTK_ALG_FUSED_OGMAP_METHOD_DS", "group__group__ptk__algos__fused__ogmap.html#ga81256536dcc76010c1f410cb7d782a95", null ],
    [ "PTK_ALG_FUSED_OGMAP_FUSIONTABLE_SIZE", "group__group__ptk__algos__fused__ogmap.html#gacf665de03d14828bf226d7a84fa1af00", null ],
    [ "PTK_ALG_FUSED_OGMAP_DSWEIGHTS_SIZE", "group__group__ptk__algos__fused__ogmap.html#ga0c752e15ebb596cf3b303c3718f7f4bc", null ],
    [ "PTK_ALG_FUSED_OGMAP_DSTHRESH_SIZE", "group__group__ptk__algos__fused__ogmap.html#ga48c30fb8ee4463f20a37bc5e80fc6c18", null ],
    [ "PTK_ALG_FUSED_OGMAP_NUM_MEM_REQ_BLKS", "group__group__ptk__algos__fused__ogmap.html#ga2a76ba9c99cb48567077d98b8d152811", null ],
    [ "PTK_ALG_FUSED_OGMAP_NUM_MEM_RSP_BLKS", "group__group__ptk__algos__fused__ogmap.html#ga08878d7efc0e47b8c1e84d08a594e5ea", null ],
    [ "PTK_Alg_FusedOgmapGetMapConfig", "group__group__ptk__algos__fused__ogmap.html#ga5514ae390b933af4aada082992cc5fca", null ],
    [ "PTK_Alg_FusedOgmapConfig", "group__group__ptk__algos__fused__ogmap.html#ga801c5e98271e537d6d03c9748f4d0547", null ],
    [ "PTK_Alg_FusedOgmapInit", "group__group__ptk__algos__fused__ogmap.html#ga769b8e4afb9f7219f616dc8aed633c40", null ],
    [ "PTK_Alg_FusedOgmapProcess", "group__group__ptk__algos__fused__ogmap.html#ga44f8a51d6010c7879f8b60e6403470bc", null ],
    [ "PTK_Alg_FusedOgmapDeInit", "group__group__ptk__algos__fused__ogmap.html#ga9e86f47163643c94d54acaf3e256cbb1", null ]
];