var group__group__ptk__algos__ds =
[
    [ "PTK_Alg_DsData", "structPTK__Alg__DsData.html", [
      [ "sd", "structPTK__Alg__DsData.html#a34c4b7f26e8d56866677ecf4c47d477d", null ],
      [ "d", "structPTK__Alg__DsData.html#a303c0733dcfa24baaf28c31973771508", null ],
      [ "f", "structPTK__Alg__DsData.html#aa11ec2428e206dd4d6af5f7327c9a48c", null ]
    ] ],
    [ "PTK_Alg_DsRadarData", "structPTK__Alg__DsRadarData.html", [
      [ "sd", "structPTK__Alg__DsRadarData.html#a00204622d3ba89622f66e8f22b42d227", null ],
      [ "d", "structPTK__Alg__DsRadarData.html#aa13a1d30db8fbda8fd52164d4011645d", null ],
      [ "f", "structPTK__Alg__DsRadarData.html#aeac2b18f174db0640d4a54af77c174b3", null ],
      [ "instOcc", "structPTK__Alg__DsRadarData.html#a645451cc78bcd5340dd89f21a75e6942", null ],
      [ "instVelocity", "structPTK__Alg__DsRadarData.html#a569bca3908aec6816fdb3800c133f0b3", null ]
    ] ],
    [ "PTK_Alg_DsInstRadarData", "structPTK__Alg__DsInstRadarData.html", [
      [ "inst_SD", "structPTK__Alg__DsInstRadarData.html#aef5f1f68ff5f0d22bbb39bbd360cbe6a", null ],
      [ "inst_D", "structPTK__Alg__DsInstRadarData.html#a20c7e87f31814beed4cd5868df7853c6", null ],
      [ "inst_F", "structPTK__Alg__DsInstRadarData.html#ad5fa30d12790e7932510a44e22b2982a", null ]
    ] ],
    [ "PTK_Alg_DsInstLidarData", "structPTK__Alg__DsInstLidarData.html", [
      [ "instOccCnt", "structPTK__Alg__DsInstLidarData.html#a6074df2be880ff982516f0ede76c263a", null ],
      [ "instFreeCnt", "structPTK__Alg__DsInstLidarData.html#a736d003f59be0b03757bb22b52910615", null ]
    ] ],
    [ "PTK_Alg_DsInstSfmData", "structPTK__Alg__DsInstSfmData.html", [
      [ "instOccCnt", "structPTK__Alg__DsInstSfmData.html#ac955b3bfcc481c0a320c7c574b45f7f3", null ],
      [ "instFreeCnt", "structPTK__Alg__DsInstSfmData.html#a40ad1046550cee1f0703c85ea718a7bb", null ]
    ] ],
    [ "PTK_Alg_DsOgmapUpdate", "group__group__ptk__algos__ds.html#ga063149d71cc9c3143449e78442d4c919", null ]
];