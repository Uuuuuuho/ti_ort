var group__group__ptk__algos__lidar__ogmap =
[
    [ "PTK_Alg_LidarOgmapParams", "structPTK__Alg__LidarOgmapParams.html", [
      [ "gridConfig", "structPTK__Alg__LidarOgmapParams.html#a622659b08c8ccf2ae92ff4fbe3f9bd0b", null ],
      [ "accGridId", "structPTK__Alg__LidarOgmapParams.html#a8cdd81eaf24cd782fb8751cb4025d0af", null ],
      [ "instOccGridId", "structPTK__Alg__LidarOgmapParams.html#a8ed47efc8d26e81f07780b14b4f5f473", null ],
      [ "instDsGridId", "structPTK__Alg__LidarOgmapParams.html#a358239ad2469234881b17dd9a7093cba", null ],
      [ "tagPcRemoved", "structPTK__Alg__LidarOgmapParams.html#ab71c40272fd1a84c8c74c44c071a3402", null ],
      [ "tagPcGround", "structPTK__Alg__LidarOgmapParams.html#a970c85df11b22dfbff53c0c85459aab4", null ],
      [ "tagOgGround", "structPTK__Alg__LidarOgmapParams.html#acaa6b292c0265aec477836e99365bab4", null ],
      [ "tagOgObstacle", "structPTK__Alg__LidarOgmapParams.html#aeeb2a5b93a53dc3a4005c210747e08e7", null ],
      [ "ogFlagChanged", "structPTK__Alg__LidarOgmapParams.html#acbd14fedf29970436d8a26855a667394", null ],
      [ "mappingMethod", "structPTK__Alg__LidarOgmapParams.html#ae77fc372d29b6f65b29120f711fce6be", null ],
      [ "dsBinarizeThresh", "structPTK__Alg__LidarOgmapParams.html#ad2c46e8eb7bf5c85a70c75107f46d0d6", null ],
      [ "dsSDWeight", "structPTK__Alg__LidarOgmapParams.html#a16dfcb6293420e07bafd7ddad09c6c2e", null ],
      [ "dsFWeight", "structPTK__Alg__LidarOgmapParams.html#af83765214e84716b98222ff79ac6748a", null ],
      [ "dsDWeight", "structPTK__Alg__LidarOgmapParams.html#a0b8d6e15022f6416713133071fef7e52", null ],
      [ "obstCntThresh", "structPTK__Alg__LidarOgmapParams.html#ad69feaa980062033adedeb0c58425d28", null ],
      [ "gndCntThresh", "structPTK__Alg__LidarOgmapParams.html#a7a2563b68be8d2105b23eab58f9b47c1", null ],
      [ "gndToObstRatioThresh", "structPTK__Alg__LidarOgmapParams.html#a113c95613a29858efad78a68b7b37f3a", null ]
    ] ],
    [ "PTK_Alg_LidarOgmapCntData", "structPTK__Alg__LidarOgmapCntData.html", [
      [ "groundCnt", "structPTK__Alg__LidarOgmapCntData.html#a13786307aa900b5e5ed43552082e43d2", null ],
      [ "obstCnt", "structPTK__Alg__LidarOgmapCntData.html#a39a152156ec45abe35b07bcdee3020fa", null ]
    ] ],
    [ "PTK_ALG_LIDAR_OGMAP_MAPPING_METHOD_CNT_ONLY", "group__group__ptk__algos__lidar__ogmap.html#ga7e3da375147eee419bb2db3449cce335", null ],
    [ "PTK_ALG_LIDAR_OGMAP_MAPPING_METHOD_CNT_AND_DS", "group__group__ptk__algos__lidar__ogmap.html#ga5091cbf754fbf3aec9ac44c77b9c7dd3", null ],
    [ "PTK_ALG_LIDAR_OGMAP_MAPPING_METHOD_MAX", "group__group__ptk__algos__lidar__ogmap.html#gac8038279e604a14151bdbadaec6fe0e1", null ],
    [ "PTK_ALG_LIDAR_OGMAP_OUT_MAP_ACC_OCC", "group__group__ptk__algos__lidar__ogmap.html#ga1889b91e97b4a24680880c5b4f4891d4", null ],
    [ "PTK_ALG_LIDAR_OGMAP_OUT_MAP_INST_DS", "group__group__ptk__algos__lidar__ogmap.html#gae77bdae6bfb889d0dfab74a6713fb443", null ],
    [ "PTK_ALG_LIDAR_OGMAP_NUM_OUT_MAPS", "group__group__ptk__algos__lidar__ogmap.html#ga1a92bb948929454dbc6c853d2d5e5f54", null ],
    [ "PTK_ALG_LIDAR_OGMAP_MEM_BUFF_MAP_ACC_OCC", "group__group__ptk__algos__lidar__ogmap.html#ga905a9297fb2a5106eb837e8d28ffabf3", null ],
    [ "PTK_ALG_LIDAR_OGMAP_MEM_BUFF_MAP_INST_DS", "group__group__ptk__algos__lidar__ogmap.html#ga5d8ccbdf132e6a107177be04641ff037", null ],
    [ "PTK_ALG_LIDAR_OGMAP_NUM_MEM_REQ_BLKS", "group__group__ptk__algos__lidar__ogmap.html#ga704307d0fb8d3771e7c87e4739874f6a", null ],
    [ "PTK_ALG_LIDAR_OGMAP_NUM_MEM_RSP_BLKS", "group__group__ptk__algos__lidar__ogmap.html#gad6c9c61070125fb74968ee4db90e7500", null ],
    [ "PTK_Alg_LidarOgmapGetMapConfig", "group__group__ptk__algos__lidar__ogmap.html#ga9d836a832e5855cbd3ac3507e450c73d", null ],
    [ "PTK_Alg_LidarOgmapConfig", "group__group__ptk__algos__lidar__ogmap.html#ga607e485df98ba64104e11e8e97908ef1", null ],
    [ "PTK_Alg_LidarOgmapInit", "group__group__ptk__algos__lidar__ogmap.html#gaa98a9617aa597961d54384e11fc435d9", null ],
    [ "PTK_Alg_LidarOgmapProcess", "group__group__ptk__algos__lidar__ogmap.html#gaa2f56f6a70863933787b18dac3956fad", null ],
    [ "PTK_Alg_LidarOgmapReset", "group__group__ptk__algos__lidar__ogmap.html#gaae513f93ea9b2d004ff8ebc63f0c467d", null ],
    [ "PTK_Alg_LidarOgmapDeInit", "group__group__ptk__algos__lidar__ogmap.html#ga5210408db3b9409d0e71fb2f87fe5d8c", null ]
];