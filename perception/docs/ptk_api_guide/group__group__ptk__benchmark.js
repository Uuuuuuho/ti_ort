var group__group__ptk__benchmark =
[
    [ "PTK_Benchmark_init", "group__group__ptk__benchmark.html#ga631892809c0ef3b28ef9388f1db3a4e7", null ],
    [ "PTK_Benchmark_startTimer", "group__group__ptk__benchmark.html#ga6f38d585d373833e3b7b9c81076d7d71", null ],
    [ "PTK_Benchmark_stopTimer", "group__group__ptk__benchmark.html#gafd6e606eb406856d6271296c576018d3", null ],
    [ "PTK_Benchmark_startCounter", "group__group__ptk__benchmark.html#ga97abb32ed8026aeb672bfa68a5d65019", null ],
    [ "PTK_Benchmark_incrementCounter", "group__group__ptk__benchmark.html#ga813a0cf78d766ebbf988ade636e518b1", null ],
    [ "PTK_Benchmark_addN", "group__group__ptk__benchmark.html#ga9f77c40c6c638d94af5933ed21387f6b", null ],
    [ "PTK_Benchmark_stopCounter", "group__group__ptk__benchmark.html#gace5f0436a22b632c1c70b3e12eb43b60", null ],
    [ "PTK_Benchmark_show", "group__group__ptk__benchmark.html#ga32151e37fe4cab11720464dc3d915639", null ],
    [ "PTK_Benchmark_showAll", "group__group__ptk__benchmark.html#gaa3965145688e3929fc7ca16de04d1cae", null ]
];