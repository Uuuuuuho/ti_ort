var group__group__ptk__positioning =
[
    [ "PTK Position", "group__group__ptk__position.html", "group__group__ptk__position" ],
    [ "PTK_INS_Velocity", "structPTK__INS__Velocity.html", [
      [ "east", "structPTK__INS__Velocity.html#a00b990e82147849313da5f876c80ebd2", null ],
      [ "north", "structPTK__INS__Velocity.html#a69222aa568cb64d797eaf2f672ec3f9f", null ],
      [ "up", "structPTK__INS__Velocity.html#a7fc371fc1ec184af113cbd619a2015ae", null ]
    ] ],
    [ "PTK_INS_Attitude", "structPTK__INS__Attitude.html", [
      [ "pitch", "structPTK__INS__Attitude.html#af63c5457197b265e39324d180da63ea7", null ],
      [ "roll", "structPTK__INS__Attitude.html#a9727f8494575fb6d13a47b34df43d95f", null ],
      [ "azimuth", "structPTK__INS__Attitude.html#ae204e09cc9525b4fa7ff66801c8921aa", null ]
    ] ],
    [ "PTK_INS_InsPva", "structPTK__INS__InsPva.html", [
      [ "position", "structPTK__INS__InsPva.html#a8b441abb5779b1fe5bb2594371062d16", null ],
      [ "velocity", "structPTK__INS__InsPva.html#a69fb47f55dfcaad123afab332a9670cd", null ],
      [ "attitude", "structPTK__INS__InsPva.html#a9635b6738247c93d2736e09a14e02155", null ],
      [ "status", "structPTK__INS__InsPva.html#ae4c874e4e9dab20120eccde3aaf50cea", null ],
      [ "gpsWeek", "structPTK__INS__InsPva.html#af011e5f8a071da107c3a88cabe89c6d5", null ],
      [ "gpsSeconds", "structPTK__INS__InsPva.html#a7afefe8b51bb03cd6dc09aede8c95eb7", null ]
    ] ],
    [ "PTK_INS_MsgInscov", "structPTK__INS__MsgInscov.html", [
      [ "position", "structPTK__INS__MsgInscov.html#a805ef46217ded4f9d39a4e08449d7832", null ],
      [ "attitude", "structPTK__INS__MsgInscov.html#ade67fc579911c6f175dd320c6d6b2fa5", null ],
      [ "velocity", "structPTK__INS__MsgInscov.html#ac1b4234832f094b5866e01310357c8a6", null ]
    ] ],
    [ "PTK_INS_RecordData", "unionPTK__INS__RecordData.html", [
      [ "inspva", "unionPTK__INS__RecordData.html#a49d736b6d9536ee796803ed240a2ccb1", null ],
      [ "inscov", "unionPTK__INS__RecordData.html#a3786385b8c1337884f98d11075c4cdf4", null ]
    ] ],
    [ "PTK_INS_Record", "structPTK__INS__Record.html", [
      [ "timestamp", "structPTK__INS__Record.html#a57410e92a04fd2f1762423cb2982ec24", null ],
      [ "type", "structPTK__INS__Record.html#a43510786e37fe903efae5465f7c00acc", null ],
      [ "data", "structPTK__INS__Record.html#a97cef099cd9538844204730db614be03", null ]
    ] ],
    [ "PTK_INS_RecordType", "group__group__ptk__positioning.html#ga3ddb5258ab008ef5c30b389038e678af", [
      [ "PTK_INS_RECORD_TYPE_INSPVA", "group__group__ptk__positioning.html#gga3ddb5258ab008ef5c30b389038e678afab5b9acc003e11644a471266f2e53d791", null ],
      [ "PTK_INS_RECORD_TYPE_INSCOV", "group__group__ptk__positioning.html#gga3ddb5258ab008ef5c30b389038e678afad55405a0ee7a536ed0d35f975fbbb185", null ],
      [ "PTK_INS_RECORD_TYPE_MAX", "group__group__ptk__positioning.html#gga3ddb5258ab008ef5c30b389038e678afad3c3337eaa256a9cb5f5b8d158ddc98c", null ]
    ] ],
    [ "PTK_INS_RetCode", "group__group__ptk__positioning.html#gaaa6588c96d393d059a7b583bb641b11f", [
      [ "PTK_INS_RETURN_CODE_OK", "group__group__ptk__positioning.html#ggaaa6588c96d393d059a7b583bb641b11fa302b123691967071ce66451861bcee38", null ],
      [ "PTK_INS_RETURN_CODE_RECORD_PARTIALLY_AVAIL", "group__group__ptk__positioning.html#ggaaa6588c96d393d059a7b583bb641b11fa8cdb0f7d56e07be9d302cc41ee144f9b", null ],
      [ "PTK_INS_RETURN_CODE_RECORD_UNAVAIL", "group__group__ptk__positioning.html#ggaaa6588c96d393d059a7b583bb641b11fa0b439b8f01bcaa2d1b82048e211c1d37", null ],
      [ "PTK_INS_RETURN_CODE_ILLEGAL_ARGIN", "group__group__ptk__positioning.html#ggaaa6588c96d393d059a7b583bb641b11fac4ef8d9d1a75e8caf9c59ed16e2e9d23", null ],
      [ "PTK_INS_RETURN_CODE_INVALID_RECORD_TYPE", "group__group__ptk__positioning.html#ggaaa6588c96d393d059a7b583bb641b11face81d78f0ff389a530765460fbe0ef48", null ],
      [ "PTK_INS_RETURN_CODE_UNEXPECTED_BEHAVIOR", "group__group__ptk__positioning.html#ggaaa6588c96d393d059a7b583bb641b11fa6877335839ea40286820fd8e5a4416c0", null ]
    ] ],
    [ "PTK_INS_SolStatus", "group__group__ptk__positioning.html#gacfe61f9ac507c53d217c9aa5a80bb424", [
      [ "PTK_INS_SOL_STATUS_GOOD", "group__group__ptk__positioning.html#ggacfe61f9ac507c53d217c9aa5a80bb424af8cfd51c546e4b5ab99c20b96e57a7ad", null ],
      [ "PTK_INS_SOL_STATUS_NOT_GOOD", "group__group__ptk__positioning.html#ggacfe61f9ac507c53d217c9aa5a80bb424ac99a5255b45ecdcf956829a43c0305aa", null ],
      [ "PTK_INS_SOL_STATUS_UNAVAILABLE", "group__group__ptk__positioning.html#ggacfe61f9ac507c53d217c9aa5a80bb424ac5863dcfe53a335e3bddd71fabad89cd", null ]
    ] ],
    [ "PTK_INS_initializeBuffers", "group__group__ptk__positioning.html#ga19d97d98c5ac604c934b4e90cc6241f3", null ],
    [ "PTK_INS_resetBuffers", "group__group__ptk__positioning.html#gac5500f56e94c0d8eff0c0af858b6b4d0", null ],
    [ "PTK_INS_getCurrentRecord", "group__group__ptk__positioning.html#gac7aefb8998205087b800ccc962416321", null ],
    [ "PTK_INS_getRecordBefore", "group__group__ptk__positioning.html#ga0edc40d226c1ffd964c87f2cc6c07852", null ],
    [ "PTK_INS_getRecordAfter", "group__group__ptk__positioning.html#gafa5e5a79ead7ac0263216bfe62de070e", null ],
    [ "PTK_INS_getRecordLinearInterp", "group__group__ptk__positioning.html#gacdfcbbe636c33b91710a6b8dd71dfdd1", null ],
    [ "PTK_INS_getIMUtoENU", "group__group__ptk__positioning.html#ga338b1fec3895c3180dc4c3f91d1e2a47", null ],
    [ "PTK_INS_getReferenceFrame", "group__group__ptk__positioning.html#ga1c9f62d8f7c1cde9466216cc40be8f5a", null ],
    [ "PTK_INS_clearReferenceFrame", "group__group__ptk__positioning.html#ga76555778d56b4e30c7af58a553f98811", null ],
    [ "PTK_INS_getNextRecord", "group__group__ptk__positioning.html#ga474f245fd5f3da7c5eb07039c4fe2819", null ],
    [ "PTK_INS_publishRecord", "group__group__ptk__positioning.html#gac661375a40d3ae70e72f915eb6d939ff", null ],
    [ "PTK_INS_Novatel_checkArchitecture", "group__group__ptk__positioning.html#ga20c2f72c1501d3e2d7c2c723f63ca6ce", null ],
    [ "PTK_INS_Novatel_parseINSPVA", "group__group__ptk__positioning.html#ga9e8e4d27e3528865d9db94b769309e51", null ],
    [ "PTK_INS_Novatel_processMessage", "group__group__ptk__positioning.html#gac3dfee04ec577b1f15dc88d60ba77715", null ]
];