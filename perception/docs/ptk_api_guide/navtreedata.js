/*
@ @licstart  The following is the entire license notice for the
JavaScript code in this file.

Copyright (C) 1997-2017 by Dimitri van Heesch

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

@licend  The above is the entire license notice
for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "Perception Tool Kit (PTK) API Guide", "index.html", [
    [ "Perception Toolkit", "index.html", null ],
    [ "TI Disclaimer", "TI_DISCLAIMER.html", null ],
    [ "Modules", "modules.html", "modules" ]
  ] ]
];

var NAVTREEINDEX =
[
"TI_DISCLAIMER.html",
"group__group__ptk__db__config.html#ga425ac88e92fa9267d95187911c00dae8",
"group__group__ptk__point__cloud.html#gaba5c0cf9190c0fd5fba5df7add77b804",
"structPTK__Alg__FusedOgmapParams.html#a722e295ba1ab41aa6d6810ac2c18bddd",
"structPTK__Alg__StereoAlgo__groundParams.html#adf6d5fb8a88de5b3cd43376e700cdec6",
"structPTK__DBConfig.html#a7f1aed9281fb6a511e3130b1af4d67fb"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';