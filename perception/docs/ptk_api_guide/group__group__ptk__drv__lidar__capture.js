var group__group__ptk__drv__lidar__capture =
[
    [ "PTK_Drv_LidarDrvParams", "structPTK__Drv__LidarDrvParams.html", [
      [ "drvType", "structPTK__Drv__LidarDrvParams.html#ac63cbf64361440b3d539514670042626", null ]
    ] ],
    [ "LIDAR_CAPTURE_DRV_VELODYNE_DATA_PORT", "group__group__ptk__drv__lidar__capture.html#ga1fa64f719a8634aaa11649db47dbe8c2", null ],
    [ "LIDAR_CAPTURE_DRV_VELODYNE_GPS_PORT", "group__group__ptk__drv__lidar__capture.html#gafd94d11f394639a87518a858ea36b4d1", null ],
    [ "LIDAR_CAPTURE_DRV_BUFFER_SIZE", "group__group__ptk__drv__lidar__capture.html#gacf538cf650f8a5a2bc1c53ce5ddc23ee", null ],
    [ "LIDAR_CAPTURE_DRV_INVALID_SOCK_ID", "group__group__ptk__drv__lidar__capture.html#ga9937a8e9c1363e10f23a40455016fd29", null ],
    [ "PTK_LidarCapHandle", "group__group__ptk__drv__lidar__capture.html#ga0917a2f212623ad59b0faad58834fdad", null ],
    [ "PTK_Drv_LidarDrvType", "group__group__ptk__drv__lidar__capture.html#ga6671d1a377facf877a560ec0c6939e82", [
      [ "PTK_Drv_LidarDrvType_NETWORK", "group__group__ptk__drv__lidar__capture.html#gga6671d1a377facf877a560ec0c6939e82a2736068b01c389624011a4f3274a61db", null ]
    ] ],
    [ "PTK_LidarCapCreate", "group__group__ptk__drv__lidar__capture.html#ga709c1b45cbdaa22ca0bdbe187a939d69", null ],
    [ "PTK_LidarCapRegisterAppCb", "group__group__ptk__drv__lidar__capture.html#ga505ff70208548ef3b6d69171ab8d8eea", null ],
    [ "PTK_LidarCapEnqueDesc", "group__group__ptk__drv__lidar__capture.html#ga3a35f3a502f8ec98c481692e69264641", null ],
    [ "PTK_LidarCapSpawnThreads", "group__group__ptk__drv__lidar__capture.html#gaad07a0cefae257b4a81bec272fae3ad1", null ],
    [ "PTK_LidarCapDelete", "group__group__ptk__drv__lidar__capture.html#gaef82f1e33657486e9a30d0e5c2884553", null ],
    [ "PTK_Drv_LidarCaptureConfig", "group__group__ptk__drv__lidar__capture.html#ga844d01a9535f76cff565bce47b2a5407", null ],
    [ "PTK_Drv_LidarCaptureInit", "group__group__ptk__drv__lidar__capture.html#ga99eebe70dcf1f8bff9a7d9be8bd4bcc0", null ],
    [ "PTK_Drv_LidarCaptureProcess", "group__group__ptk__drv__lidar__capture.html#ga2c6e192c555a8ee18b25edebdfc6374e", null ],
    [ "PTK_Drv_LidarCaptureDeInit", "group__group__ptk__drv__lidar__capture.html#ga6ecff76f286debfba4f312a690aacc8d", null ]
];