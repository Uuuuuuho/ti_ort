/*
@ @licstart  The following is the entire license notice for the
JavaScript code in this file.

Copyright (C) 1997-2017 by Dimitri van Heesch

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

@licend  The above is the entire license notice
for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "Perception Tool Kit (PTK)", "index.html", [
    [ "Fusion Occupancy Grid (OG) Mapping", "md_internal_docs_algo_guide_content_pages_fusion_og_mapping.html", null ],
    [ "LiDAR Ground Point Computation", "md_internal_docs_algo_guide_content_pages_lidar_gpc.html", null ],
    [ "LiDAR Motion Distortion Correction", "md_internal_docs_algo_guide_content_pages_lidar_mdc.html", null ],
    [ "LiDAR Occupancy Grid (OG) Mapping", "md_internal_docs_algo_guide_content_pages_lidar_og_mapping.html", null ],
    [ "Free Space Detection in an Occupancy Grid Map", "md_internal_docs_algo_guide_content_pages_ogmap_fsd.html", null ],
    [ "Radar Occupancy Grid (OG) Mapping", "md_internal_docs_algo_guide_content_pages_radar_og_mapping.html", null ],
    [ "SFM Occupancy Grid (OG) Mapping", "md_internal_docs_algo_guide_content_pages_sfm_og_mapping.html", null ],
    [ "Stereo Ground Estimation", "md_internal_docs_algo_guide_content_pages_stereo_ground_estimation.html", null ],
    [ "Stereo Obstacle / Freespace Detection", "md_internal_docs_algo_guide_content_pages_stereo_obstacle_freespace_detection.html", null ],
    [ "Stereo Post-Processing", "md_internal_docs_algo_guide_content_pages_stereo_post_processing.html", null ]
  ] ]
];

var NAVTREEINDEX =
[
"index.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';