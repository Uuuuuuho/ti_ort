/*
 *
 * Copyright (c) 2019 Texas Instruments Incorporated
 *
 * All rights reserved not granted herein.
 *
 * Limited License.
 *
 * Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
 * license under copyrights and patents it now or hereafter owns or controls to make,
 * have made, use, import, offer to sell and sell ("Utilize") this software subject to the
 * terms herein.  With respect to the foregoing patent license, such license is granted
 * solely to the extent that any such patent is necessary to Utilize the software alone.
 * The patent license shall not apply to any combinations which include this software,
 * other than combinations with devices manufactured by or for TI ("TI Devices").
 * No hardware patent is licensed hereunder.
 *
 * Redistributions must preserve existing copyright notices and reproduce this license
 * (including the above copyright notice and the disclaimer and (if applicable) source
 * code license limitations below) in the documentation and/or other materials provided
 * with the distribution
 *
 * Redistribution and use in binary form, without modification, are permitted provided
 * that the following conditions are met:
 *
 * *       No reverse engineering, decompilation, or disassembly of this software is
 * permitted with respect to any software provided in binary form.
 *
 * *       any redistribution and use are licensed by TI for use only with TI Devices.
 *
 * *       Nothing shall obligate TI to provide you with source code for the software
 * licensed and provided to you in object code.
 *
 * If software source code is provided to you, modification and redistribution of the
 * source code are permitted provided that the following conditions are met:
 *
 * *       any redistribution and use of the source code, including any resulting derivative
 * works, are licensed by TI for use only with TI Devices.
 *
 * *       any redistribution and use of any object code compiled from the source code
 * and any resulting derivative works, are licensed by TI for use only with TI Devices.
 *
 * Neither the name of Texas Instruments Incorporated nor the names of its suppliers
 *
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * DISCLAIMER.
 *
 * THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
#ifndef RADAR_GTRACK_COMMON_H
#define RADAR_GTRACK_COMMON_H

#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>

#include <perception/algos/radar_gtrack_2d.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * \name Maximum supported configurations.
 * \{ */
 /**
 * \brief Defines maximum possible number of measurments point the algorithm will
 *        accept at configuration time.
 *
 * \ingroup group_ptk_algos_radar_gtrack
 */

#define GTRACK_NUM_POINTS_MAX            (1000U)
 /**
 * \brief Defines maximum possible number of tracking target the algorithm will
 *        accept at configuration time.
 *
 * \ingroup group_ptk_algos_radar_gtrack
 */

#define GTRACK_NUM_TRACKS_MAX            (250U)
/** \} */

/**
 * \name Target ID definitions.
 * \{
 * \details
 *  Target IDs are uint8_t, with valid IDs ranging from 0 to 249. Values 250 to 252
 *  are reserved for future use, other values as defined below.
 */
 /**
 * \brief Point is not associated, is too weak.
 *
 * \ingroup group_ptk_algos_radar_gtrack
 */
#define GTRACK_ID_POINT_TOO_WEAK            (253U)

 /**
 * \brief Point is not associated, is behind the wall.
 *
 * \ingroup group_ptk_algos_radar_gtrack
 */
#define GTRACK_ID_POINT_BEHIND_THE_WALL     (254U)

 /**
 * \brief Point is not associated, noise.
 *
 * \ingroup group_ptk_algos_radar_gtrack
 */
#define GTRACK_ID_POINT_NOT_ASSOCIATED      (255U)
/** \} */

/**
 * \name Benchmarking results.
 * \{
 * \details During runtime execution, tracking step function can optionally return
 *          cycle counts for the sub-functions defined below. Each count is 32bit
 *          unsigned integer value representing a timestamp of free runing clock.
 */
 /**
 * \brief Cycle count at step setup.
 *
 * \ingroup group_ptk_algos_radar_gtrack
 */
#define GTRACK_BENCHMARK_SETUP              (0U)

 /**
 * \brief Cycle count after predict function.
 *
 * \ingroup group_ptk_algos_radar_gtrack
 */
#define GTRACK_BENCHMARK_PREDICT            (1U)
 /**
 * \brief Cycle count after associate function.
 *
 * \ingroup group_ptk_algos_radar_gtrack
 */
#define GTRACK_BENCHMARK_ASSOCIATE          (2U)

 /**
 * \brief Cycle count after allocate function.
 *
 * \ingroup group_ptk_algos_radar_gtrack
 */
#define GTRACK_BENCHMARK_ALLOCATE           (3U)

 /**
 * \brief Cycle count after update function.
 *
 * \ingroup group_ptk_algos_radar_gtrack
 */
#define GTRACK_BENCHMARK_UPDATE             (4U)

 /**
 * \brief Cycle count after report function.
 *
 * \ingroup group_ptk_algos_radar_gtrack
 */
#define GTRACK_BENCHMARK_REPORT             (5U)

 /**
 * \brief  Size of benchmarking array.
 *
 * \ingroup group_ptk_algos_radar_gtrack
 */
#define GTRACK_BENCHMARK_SIZE               (GTRACK_BENCHMARK_REPORT)
/** \} */

/**
* \brief Size of name of output file.
*
* \ingroup group_ptk_algos_radar_gtrack
*/
#define GTRACK_MAX_SAVE_FILE_NAME_LENGTH     (200U)

/**
 * \name Boundary boxes.
 * \{
 * \details Application can configure tracker with scene boundries. Boundaries are defined
 *          as a boxes.
 */

 /**
  * \brief Maximum number of boundary boxes. Points outside of boundary boxes are
  *        ignored.
  *
  * \ingroup group_ptk_algos_radar_gtrack
  */
#define GTRACK_MAX_BOUNDARY_BOXES           (2U)

/**
 * \brief Maximum number of static boxes. Targets inside the static box can persist
 *         longer
 *
 * \ingroup group_ptk_algos_radar_gtrack
 */
#define GTRACK_MAX_STATIC_BOXES             (2U)
/** \} */

/**
 * \brief Size of state vector.
 *
 * \ingroup group_ptk_algos_radar_gtrack
 */
#define GTRACK_STATE_VECTOR_SIZE sizeof(GTRACK_state_vector_pos_vel_acc)/sizeof(float)

/**
 * \brief Size of measurements vector.
 *
 * \ingroup group_ptk_algos_radar_gtrack
 */
#define GTRACK_MEASUREMENT_VECTOR_SIZE sizeof(GTRACK_measurement_vector)/sizeof(float)

/**
 * \brief GTRACK Verbose Level.
 *
 * \details Defines Algorithm verboseness level.
 *
 * \ingroup group_ptk_algos_radar_gtrack
 */
typedef enum
{
    /**  \brief NONE. */
    GTRACK_VERBOSE_NONE = 0,

    /**  \brief ERROR Level, only errors are reported. */
    GTRACK_VERBOSE_ERROR,

    /**  \brief WARNING Level, errors and warnings are reported. */
    GTRACK_VERBOSE_WARNING,

    /**  \brief DEBUG Level, errors, warnings, and state transitions are reported. */
    GTRACK_VERBOSE_DEBUG,

    /**  \brief MATRIX Level, previous level plus all intermediate computation
                  results are reported. */
    GTRACK_VERBOSE_MATRIX,

    /**  \brief MAXIMUM Level, maximum amount of details are reported. */
    GTRACK_VERBOSE_MAXIMUM

} GTRACK_VERBOSE_TYPE;


/**
 * \brief GTRACK Box Structure.
 *
 * \details The structure defines the box element used to describe the scenery.
 *
 * \ingroup group_ptk_algos_radar_gtrack
 */
typedef struct {
    /**  \brief Left boundary, m. \n
      * Range: (-inf, inf)
      */
    float x1;

    /**  \brief Right boundary, m. \n
      * Range: (-inf, inf)
      */
    float x2;

    /**  \brief Near boundary, m. \n
      * Range: (-inf, inf)
      */
    float y1;

    /**  \brief Far boundary, m. \n
      * Range: (-inf, inf)
      */
    float y2;

    /**  \brief Bottom boundary, m. \n
      * Range: (-inf, inf)
      */
    float z1;

    /**  \brief Top boundary, m. \n
      * Range: (-inf, inf)
      */
    float z2;

} GTRACK_boundaryBox;

/**
 * \brief GTRACK Gate Limits.
 *
 * \details The structure describes the limits the gating function will expand.
 *
 * \ingroup group_ptk_algos_radar_gtrack
 */
typedef struct {
    /**  \brief   Width limit, m. \n
      * Range: [0, inf)
      */
    float width;

    /**  \brief   Depth limit, m. \n
      * Range: [0, inf)
      */
    float depth;

    /**  \brief   Heigth limit, m. \n
      * Range: [0, inf)
      */
    float height;

    /**  \brief   Radial velocity limit, m/s. \n
      * Range: (-inf, inf)
      */
    float vel;
} GTRACK_gateLimits;

/**
 * \brief GTRACK Update Function Parameters.
 *
 * \details The structure describes default standard deviation values applied when
 *          no variance information provided in the point Cloud.
 *
 * \ingroup group_ptk_algos_radar_gtrack
 */
typedef struct {
    /**  \brief Expected standard deviation of measurements in target length dimension. \n
      * Range: (0, inf)
      */
    float widthStd;

    /**  \brief Expected standard deviation of measurements in target length dimension. \n
      * Range: (0, inf)
      */
    float depthStd;

    /**  \brief Expected standard deviation of measurements in target width dimension. \n
      * Range: (0, inf)
      */
    float heightStd;

    /**  \brief Expected standard deviation of measurements of target radial velocity. \n
      * Range: (0, inf)
      */
    float dopplerStd;

} GTRACK_varParams;

/**
 * \brief GTRACK Scene Parameters/
 *
 * \details This set of parameters describes the scenery. <br>
 *          It allows user to configure the tracker with expected boundaries,
 *          and areas of  static behavior. <br>
 *  User can define up to \ref GTRACK_MAX_BOUNDARY_BOXES boundary boxes, and up to
 *  \ref GTRACK_MAX_STATIC_BOXES static boxes. <br>
 *  Boxes coordinates are in meters, sensor is assumed at (0, 0) of Cartesian (X, Y)
 *  space.
 *
 * \ingroup group_ptk_algos_radar_gtrack
 */
typedef struct {
    /**  \brief Number of scene boundary boxes. If defined (numBoundaryBoxes > 0),
                only points within the boundary box(s) can be associated with tracks.. \n
      * Allowed: {0, 1, GTRACK_MAX_BOUNDARY_BOXES}
      */
    uint8_t             numBoundaryBoxes;

    /**  \brief Scene boundary boxes */
    GTRACK_boundaryBox  boundaryBox[GTRACK_MAX_BOUNDARY_BOXES];

    /**  \brief Number of scene static boxes. If defined (numStaticBoxes > 0), only
                targets within the static box(s) can persist as static. \n
      * Allowed: {0, 1, GTRACK_MAX_STATIC_BOXES}
      */
    uint8_t             numStaticBoxes;

    /**  \brief Scene static boxes. */
    GTRACK_boundaryBox  staticBox[GTRACK_MAX_STATIC_BOXES];

} GTRACK_sceneryParams;


/**
 * \brief GTRACK Gating Function Parameters.
 *
 * \details The structure describes gating function parameters.
 *
 * \ingroup group_ptk_algos_radar_gtrack
 */
typedef struct {
    /**  \brief   Volume of the gating function.  \n
      * Range: (0, inf)
      */
    float        volume;

    /**  \brief   Gating function limits. */
    union {
        GTRACK_gateLimits    limits;
        float limitsArray[4];
    };

} GTRACK_gatingParams;

/**
 * \brief GTRACK Tracking Management Function Parameters.
 *
 * \details The structure describes the thresholds for state changing counters.
 *
 * \ingroup group_ptk_algos_radar_gtrack
 */
typedef struct {
    /**  \brief  DETECTION => ACTIVE threshold. \n
      * Allowed: {0, 1, 2, ..., UINT32_MAX)
      */
    uint16_t det2actThre;

    /**  \brief  DETECTION => FREE threshold. \n
      * Allowed: {0, 1, 2, ..., UINT32_MAX)
      */
    uint16_t det2freeThre;

    /**  \brief  ACTIVE => FREE threshold. \n
      * Allowed: {0, 1, 2, ..., UINT32_MAX)
      */
    uint16_t active2freeThre;

    /**  \brief  STATIC => FREE threshold. \n
      * Allowed: {0, 1, 2, ..., UINT32_MAX)
      */
    uint16_t static2freeThre;

    /**  \brief  EXIT ZONE => FREE threshold. \n
      * Allowed: {0, 1, 2, ..., UINT32_MAX)
      */
    uint16_t exit2freeThre;

} GTRACK_stateParams;


/**
 * \brief GTRACK Allocation Function Parameters.
 *
 * \details The structure describes the thresholds used in Allocation function.
 *
 * \ingroup group_ptk_algos_radar_gtrack
 */
typedef struct {
    /**  \brief  Minimum total SNR. \n
      * Range: [0, inf)
      */
    float snrThre;

    /**  \brief  Minimum total SNR when behind another target. \n
      * Range: [0, inf)
      */
    float snrThreObscured;

    /**  \brief  Minimum initial velocity, m/s. \n
      * Range: [0, inf)
      */
    float velocityThre;

    /**  \brief  Minimum number of points in a set. \n
      * Allowed: {0, 1, 2, ..., UINT16_MAX}
      */

    uint16_t pointsThre;

    /**  \brief  Maximum squared distance between points in a set. \n
      * Range: (-inf, inf)
      */
    float    maxDistanceThre;

    /**  \brief  Maximum velocity delta between points in a set.\n
      * Range: (-inf, inf)
      */
    float    maxVelThre;

} GTRACK_allocationParams;

/**
 * \brief GTRACK Unrolling Parameters.
 *
 * \details The structure describes the filtering parameters used to switch unrolling
 *          states.
 *
 * \ingroup group_ptk_algos_radar_gtrack
 */
typedef struct {
    /**  \brief  Range rate filtering alpha. \n
      * Range: [0, inf)
      */
    float alpha;

    /**  \brief  Range rate filtering confidence. \n
      * Range: [0, inf)
      */
    float confidence;

} GTRACK_unrollingParams;

/**
 * \brief GTRACK Measurement point.
 *
 * \details The structure describes measurement point format.
 *
 * \ingroup group_ptk_algos_radar_gtrack
 */
typedef struct
{
    union {
        /**  \brief Measurement vector. */
        GTRACK_measurement_vector vector;

        /**  \brief Array to store Measurement vector. \n
          * Range: (-inf, inf)
          */
        float array[GTRACK_MEASUREMENT_VECTOR_SIZE];
    };

    /**  \brief Range detection SNR, linear.\n
      * Range: [0, inf)
      */
    float snr;

} GTRACK_measurementPoint;

/**
 * \brief GTRACK target descriptor.
 *
 * \details The structure describes target descriptorformat.
 *
 * \ingroup group_ptk_algos_radar_gtrack
 */
typedef struct
{
    /**  \brief Tracking Unit Identifier. \n
      * Allowed: {0, 1, 2, ..., UINT8_MAX}
      */
    uint8_t uid;

    /**  \brief Target Identifier. \n
      * Allowed: {0, 1, 2, ..., UINT8_MAX}
      */
    uint32_t tid;

    /**  \brief State vector. \n
      * Range: (-inf, inf)
      */
    float S[GTRACK_STATE_VECTOR_SIZE];

    /**  \brief Group covariance matrix. \n
      * Range: (-inf, inf)
      */
    float EC[GTRACK_MEASUREMENT_VECTOR_SIZE*GTRACK_MEASUREMENT_VECTOR_SIZE];

    /**  \brief Gain factor. \n
      * Range: [0, inf)
      */
    float G;

    /**  \brief Estimated target dimensions: depth, width, [height], doppler.\n
      * Range: (-inf, inf)
      */
    float dim[GTRACK_MEASUREMENT_VECTOR_SIZE];

} GTRACK_targetDesc;

#ifdef __cplusplus
}
#endif

#endif /* GTRACK_H */
