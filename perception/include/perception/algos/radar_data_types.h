/*
 *
 * Copyright (c) 2018 Texas Instruments Incorporated
 *
 * All rights reserved not granted herein.
 *
 * Limited License.
 *
 * Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
 * license under copyrights and patents it now or hereafter owns or controls to make,
 * have made, use, import, offer to sell and sell ("Utilize") this software subject to the
 * terms herein.  With respect to the foregoing patent license, such license is granted
 * solely to the extent that any such patent is necessary to Utilize the software alone.
 * The patent license shall not apply to any combinations which include this software,
 * other than combinations with devices manufactured by or for TI ("TI Devices").
 * No hardware patent is licensed hereunder.
 *
 * Redistributions must preserve existing copyright notices and reproduce this license
 * (including the above copyright notice and the disclaimer and (if applicable) source
 * code license limitations below) in the documentation and/or other materials provided
 * with the distribution
 *
 * Redistribution and use in binary form, without modification, are permitted provided
 * that the following conditions are met:
 *
 * *       No reverse engineering, decompilation, or disassembly of this software is
 * permitted with respect to any software provided in binary form.
 *
 * *       any redistribution and use are licensed by TI for use only with TI Devices.
 *
 * *       Nothing shall obligate TI to provide you with source code for the software
 * licensed and provided to you in object code.
 *
 * If software source code is provided to you, modification and redistribution of the
 * source code are permitted provided that the following conditions are met:
 *
 * *       any redistribution and use of the source code, including any resulting derivative
 * works, are licensed by TI for use only with TI Devices.
 *
 * *       any redistribution and use of any object code compiled from the source code
 * and any resulting derivative works, are licensed by TI for use only with TI Devices.
 *
 * Neither the name of Texas Instruments Incorporated nor the names of its suppliers
 *
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * DISCLAIMER.
 *
 * THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
#ifndef _RADAR_DATA_TYPES_H_
#define _RADAR_DATA_TYPES_H_

#include <math.h>

#include <perception/perception.h>
#include <perception/mathmapping.h>
#include <perception/algos/alg_common.h>
#include <perception/utils/api_memory_if.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * \defgroup group_ptk_algos_radar_common PTK Common Data Structures shared by Radar Algorithms
 * \ingroup group_ptk_algos
 *
 */

/**
 * \brief Number of objects per frame.
 * \ingroup group_ptk_algos_radar_common
 */
#define PTK_ALG_RADAR_MAX_OBJ_PER_FRAME             (400U)

/**
 * \brief Number of radar sensors.
 * \ingroup group_ptk_algos_radar_common
 */
#define PTK_ALG_RADAR_NUM_SENSORS                   (4U)

/**
 * \brief Per object detecttion output.
 *
 * \ingroup group_ptk_algos_radar_common
 */
typedef struct
{
    /** Distance of the object in meter. \n
      * Range: [0.0, inf)
      */
    float       range;

    /** (Azimuth) angle of the object in degrees. \n
      * Range: [-180, 180]
      */
    float       azimuthAngle;

    /** Speed of the object in m/s. \n
      * Range: [0.0, inf)
      */
    float       velocity;

    /** Variance from DOA stage. \n
      * Range: [0.0, inf)
      */
    float       doaVarEst;

    /** SNR. \n
      * Range: [0.0, inf)
      */
    float       snr;

} PTK_Alg_RadarDetObjOutput;

/**
 * \brief Per frame detection output.
 *
 * \ingroup group_ptk_algos_radar_common
 */
typedef struct
{
    /** Number of objects (points). \n
      * Allowed: {0, 1, 2, ..., \ref PTK_ALG_RADAR_MAX_OBJ_PER_FRAME}
      */
    int32_t                     numObj;

    /** Object information. */
    PTK_Alg_RadarDetObjOutput   objInfo[PTK_ALG_RADAR_MAX_OBJ_PER_FRAME];

} PTK_Alg_RadarDetOutput;

/**
 * \brief Radar sensor configuration.
 *
 * \ingroup group_ptk_algos_radar_common
 */
typedef struct
{
    /** Flag indicating if the configuration is valid. \n
      * Allowed: {0, 1}
      */
    uint32_t        valid;

    /** Absolute angle to be gated for OG map processing in degrees. \n
      * Range: [0.0, 180]
      */
    float           gatingAbsMaxAngle;

    /** Min range to be gated for OG map processing in meters. \n
      * Range: [0.0, inf)
      */
    float           gatingMinRange;

    /** Max range to be gated for OG map processing in meters. \n
      * Range: [0.0, inf)
      */
    float           gatingMaxRange;

    /** SNR to be gated for OG map processing. \n
      * Range: [0.0, inf)
      */
    float           gatingMinSnr;

    /** Sensor orientation wrt heading in degrees.  \n
      * Range: [-180, 180)
      */
    float           sensorOrientation;

    /** Sensor x position wrt vehicle center in meters.  \n
      * Range: (-inf, inf)
      */
    float           sensorPosition_x;

    /** Sensor y position wrt vehicle center in meters.  \n
      * Range: (-inf, inf)
      */
    float           sensorPosition_y;

} PTK_Alg_RadarSensorConfig;

#ifdef __cplusplus
}
#endif

#endif
