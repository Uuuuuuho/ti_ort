/*
 *
 * Copyright (c) 2019 Texas Instruments Incorporated
 *
 * All rights reserved not granted herein.
 *
 * Limited License.
 *
 * Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
 * license under copyrights and patents it now or hereafter owns or controls to make,
 * have made, use, import, offer to sell and sell ("Utilize") this software subject to the
 * terms herein.  With respect to the foregoing patent license, such license is granted
 * solely to the extent that any such patent is necessary to Utilize the software alone.
 * The patent license shall not apply to any combinations which include this software,
 * other than combinations with devices manufactured by or for TI ("TI Devices").
 * No hardware patent is licensed hereunder.
 *
 * Redistributions must preserve existing copyright notices and reproduce this license
 * (including the above copyright notice and the disclaimer and (if applicable) source
 * code license limitations below) in the documentation and/or other materials provided
 * with the distribution
 *
 * Redistribution and use in binary form, without modification, are permitted provided
 * that the following conditions are met:
 *
 * *       No reverse engineering, decompilation, or disassembly of this software is
 * permitted with respect to any software provided in binary form.
 *
 * *       any redistribution and use are licensed by TI for use only with TI Devices.
 *
 * *       Nothing shall obligate TI to provide you with source code for the software
 * licensed and provided to you in object code.
 *
 * If software source code is provided to you, modification and redistribution of the
 * source code are permitted provided that the following conditions are met:
 *
 * *       any redistribution and use of the source code, including any resulting derivative
 * works, are licensed by TI for use only with TI Devices.
 *
 * *       any redistribution and use of any object code compiled from the source code
 * and any resulting derivative works, are licensed by TI for use only with TI Devices.
 *
 * Neither the name of Texas Instruments Incorporated nor the names of its suppliers
 *
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * DISCLAIMER.
 *
 * THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
#ifndef _FUSED_OGMAP_H_
#define _FUSED_OGMAP_H_

#include <perception/perception.h>
#include <perception/algos/alg_common.h>
#include <perception/utils/api_memory_if.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * \defgroup group_ptk_algos_fused_ogmap PTK Multi-sensor Fused Occupancy Grid Map
 * \ingroup group_ptk_algos
 *
 */

/**
 * \brief Voting method based fusion.
 *
 * \ingroup group_ptk_algos_fused_ogmap
 */
#define PTK_ALG_FUSED_OGMAP_METHOD_VOTING       (0U)

/**
 * \brief Dempster-Shafer method based fusion.
 *
 * \ingroup group_ptk_algos_fused_ogmap
 */
#define PTK_ALG_FUSED_OGMAP_METHOD_DS           (1U)

/**
 * \brief Voting based fusion table size
 *
 * \ingroup group_ptk_algos_fused_ogmap
 */
#define PTK_ALG_FUSED_OGMAP_FUSIONTABLE_SIZE    (26U)

/**
 * \brief Dempster-Shafer weight table size
 *
 * \ingroup group_ptk_algos_fused_ogmap
 */
#define PTK_ALG_FUSED_OGMAP_DSWEIGHTS_SIZE      (9U)

/**
 * \brief Dempster-Shafer threshold table size
 *
 * \ingroup group_ptk_algos_fused_ogmap
 */
#define PTK_ALG_FUSED_OGMAP_DSTHRESH_SIZE       (9U)

/**
 * \brief Number of entries that would be set in the 'memReq' object in the
 *        PTK_Alg_FusedOgmapConfig() API.
 * \ingroup group_ptk_algos_fused_ogmap
 */
#define PTK_ALG_FUSED_OGMAP_NUM_MEM_REQ_BLKS    (3U)

/**
 * \brief Number of entries that should be set in the 'memRsp' object in the
 *        PTK_Alg_FusedOgmapInit() API.
 * \ingroup group_ptk_algos_fused_ogmap
 */
#define PTK_ALG_FUSED_OGMAP_NUM_MEM_RSP_BLKS    (2U)

/**
 * \brief Containes for holding time stamps for different sensors.
 * \ingroup group_ptk_algos_fused_ogmap
 */
typedef struct
{
    /** Time stamp corresponding to the SfM based map.\n
      * Allowed: {0, 1, ... UINT64_MAX}
      */
    uint64_t    cameraFramsTs;

    /** Time stamp corresponding to the Radar based map.\n
      * Allowed: {0, 1, ... UINT64_MAX}
      */
    uint64_t    radarFramsTs;

    /** Time stamp corresponding to the Lidar based map.\n
      * Allowed: {0, 1, ... UINT64_MAX}
      */
    uint64_t    lidarFramsTs;

    /** Time stamp corresponding to the parking spot record.\n
      * Allowed: {0, 1, ... UINT64_MAX}
      */
    uint64_t    psFrameTs;

    /** Time stamp corresponding to the parking spot Image.\n
      * Allowed: {0, 1, ... UINT64_MAX}
      */
    uint64_t    psImgFrameTs;

} PTK_Alg_FusedOgmapTime;

/**
 * \brief Configuration parameters.
 * \ingroup group_ptk_algos_fused_ogmap
 */
typedef struct
{
    /** Sensor Enable Mask.\n
      * Allowed: {0, 1, ... UINT32_MAX}
      */
    uint32_t        sensorEnableMask;

    /** Mask indicating which sensor data is valid.\n
      * Allowed: {0, 1, ... UINT32_MAX}
      */
    uint32_t        sensorDataValidMask;

    /** Flag for an occupied grid cell. \n
      * Allowed: {0x00000001, 0x00000002, 0x00000004, 0x00000008,
                 0x00000010, 0x00000020, 0x00000080, 0x00000100}
      */
    uint32_t        ogFlagOccupied;

    /** Flag for a free grid cell. \n
      * Allowed: {0x00000001, 0x00000002, 0x00000004, 0x00000008,
      *           0x00000010, 0x00000020, 0x00000080, 0x00000100}
      */
    uint32_t        ogFlagFree;

    /** Flag to indicate cell was updated. \n
      * Allowed: {0x00000001, 0x00000002, 0x00000004, 0x00000008,
      *           0x00000010, 0x00000020, 0x00000080, 0x00000100}
      */    
    uint32_t        ogFlagChanged;

    /** Instantaneous Radar Grid ID to be used in Fusion. \n
      * Allowed: {0x00000001, 0x00000002, 0x00000004, 0x00000008,
      *           0x00000010, 0x00000020, 0x00000080, 0x00000100}
      */
    uint32_t        radarGridId;

    /** Instantaneous Lidar Grid ID to be used in Fusion. \n
      * Allowed: {0x00000001, 0x00000002, 0x00000004, 0x00000008,
      *           0x00000010, 0x00000020, 0x00000080, 0x00000100}
      */
    uint32_t        lidarGridId;

    /** Instantaneous Camera Grid ID to be used in Fusion. \n
      * Allowed: {0x00000001, 0x00000002, 0x00000004, 0x00000008,
      *           0x00000010, 0x00000020, 0x00000080, 0x00000100}
      */
    uint32_t        cameraGridId;

    /** Output grid Id.
     * Allowed: {0, 1, 2, ..., \ref PTK_MAP_MAX_NUM_GRIDS_PER_MAP - 1}\n
     */
    uint32_t        outGridId;

    /** DS-Fusion Grid ID. \n
      * Allowed: {0x00000001, 0x00000002, 0x00000004, 0x00000008,
      *           0x00000010, 0x00000020, 0x00000080, 0x00000100}
      */
    uint32_t        fusedGridId;

    /** Grid configuration. */
    PTK_GridConfig  gridConfig;

    /** Region of interest offset. */
    PTK_GridRoi     roiParams;

    /** Fusion method. \n
      * \verbatim
      * Allowed: {0, 1}
      * if fusionMethod is '0'
      *   --> radar_ogmap
      *      - mappingMethod should be either '0'
      *      - binarizeThresh can be any float > 0.0
      *   --> lidar_ogmap
      *      - mappingMethod should be either '0'
      *
      * if fusionMethod is '1'
      *   --> radar_ogmap
      *      - mappingMethod should be either '1'
      *      - binarizeThresh should be float between 0.0 and 1.0
      *   --> lidar_ogmap
      *      - mappingMethod should be either '1'
      *      - dsBinarizeThresh should be float between 0.0 and 1.0 \endverbatim
      */
    uint32_t        fusionMethod;

    /** Parameters for Voting based fusion (i.e. fusionMethod = 0) \n
      * \verbatim
      * Abbreviations:
      *  Sensors:
      *    Lidar
      *    Radar
      *    Camera
      *  State:
      *    U - Unknown
      *    O - Occupied
      *    F - Free
      *
      * Allowed: {0,1}
      *
      * Table entry correspondence to sensor states
      * ==================================================
      * fusionTable[0] : Lidar: U | Radar: U | Camera: O |
      * fusionTable[1] : Lidar: U | Radar: U | Camera: F |
      * fusionTable[2] : Lidar: U | Radar: O | Camera: U |
      * fusionTable[3] : Lidar: U | Radar: O | Camera: O |
      * fusionTable[4] : Lidar: U | Radar: O | Camera: F |
      * fusionTable[5] : Lidar: U | Radar: F | Camera: U |
      * fusionTable[6] : Lidar: U | Radar: F | Camera: O |
      * fusionTable[7] : Lidar: U | Radar: F | Camera: F |
      * fusionTable[8] : Lidar: O | Radar: U | Camera: U |
      * fusionTable[9] : Lidar: O | Radar: U | Camera: O |
      * fusionTable[10]: Lidar: O | Radar: U | Camera: F |
      * fusionTable[11]: Lidar: O | Radar: O | Camera: U |
      * fusionTable[12]: Lidar: O | Radar: O | Camera: O |
      * fusionTable[13]: Lidar: O | Radar: O | Camera: F |
      * fusionTable[14]: Lidar: O | Radar: F | Camera: U |
      * fusionTable[15]: Lidar: O | Radar: F | Camera: O |
      * fusionTable[16]: Lidar: O | Radar: F | Camera: F |
      * fusionTable[18]: Lidar: F | Radar: U | Camera: O |
      * fusionTable[17]: Lidar: F | Radar: U | Camera: U |
      * fusionTable[19]: Lidar: F | Radar: U | Camera: F |
      * fusionTable[20]: Lidar: F | Radar: O | Camera: U |
      * fusionTable[21]: Lidar: F | Radar: O | Camera: O |
      * fusionTable[22]: Lidar: F | Radar: O | Camera: F |
      * fusionTable[23]: Lidar: F | Radar: F | Camera: U |
      * fusionTable[24]: Lidar: F | Radar: F | Camera: O |
      * fusionTable[25]: Lidar: F | Radar: F | Camera: F |\endverbatim
      */
    uint32_t         fusionTable[PTK_ALG_FUSED_OGMAP_FUSIONTABLE_SIZE];

    /** DS fusion weights for different modalities (i.e. fusionMethod = 1)
      * \verbatim
      * Table entry correspondence to sensor states
      * ===========================================
      * dsWeights[0] : Radar  SD weight
      * dsWeights[1] : Radar  D  weight
      * dsWeights[2] : Radar  F  weight
      * dsWeights[3] : Lidar  SD weight
      * dsWeights[4] : Lidar  D  weight
      * dsWeights[5] : Lidar  F  weight
      * dsWeights[6] : Camera SD weight
      * dsWeights[7] : Camera D  weight
      * dsWeights[8] : Camera F  weight \endverbatim
      */
    float           dsWeights[PTK_ALG_FUSED_OGMAP_DSWEIGHTS_SIZE];

    /** DS fusion weights for different modalities (i.e. fusionMethod = 1) \n
      * \verbatim
      * Table entry correspondence to sensor states
      * ===========================================
      * dsThresh[0] : Radar  SD threshold
      * dsThresh[1] : Radar  D  threshold
      * dsThresh[2] : Radar  F  threshold
      * dsThresh[3] : Lidar  SD threshold
      * dsThresh[4] : Lidar  D  threshold
      * dsThresh[5] : Lidar  F  threshold
      * dsThresh[6] : Camera SD threshold
      * dsThresh[7] : Camera D  threshold
      * dsThresh[8] : Camera F  threshold \endverbatim
      */
    float           dsThresh[PTK_ALG_FUSED_OGMAP_DSTHRESH_SIZE];

} PTK_Alg_FusedOgmapParams;

/* PUBLIC APIS  */
/**
 * \brief Initializes the map configuration based on the given algorithm
 *        configuration parameters.
 *
 * \param [in] cfgParams Algorithm configuration parameters.
 *
 * \param [out] config Map configuration
 *
 * \return
 *        - PTK_ALG_RET_SUCCESS, if successful
 *        - < error code, otherwise
 *
 * \ingroup group_ptk_algos_fused_ogmap
 */
int32_t
PTK_Alg_FusedOgmapGetMapConfig(const PTK_Alg_FusedOgmapParams * cfgParams,
                               PTK_MapConfig                  * config);

/**
 * \brief Computes the memory requirements based on the given algorithm
 *        configuration parameters.
 *
 * \param [in] cfgParams Algorithm configuration parameters.
 *
 * \param [out] memReq Memory requirements as specified below
 *             - entry [0]: Memory for internal context. Best allocated from
 *                          the fastest memory.
 *             - entry [1]: Memory for internal point cloud. Best allocated from
 *                          the fastest memory.
 *             - entry [2]: Memory for the output buffer. The output buffer is
 *                          allocated by the Application so this entry is just
 *                          to communicate the output buffer size. No need to
 *                          pass the memory to the library in the init call
 *                          PTK_Alg_FusedOgmapInit().
 *
 * \return
 *        - PTK_ALG_RET_SUCCESS, if successful
 *        - < error code, otherwise
 *
 * \ingroup group_ptk_algos_fused_ogmap
 */
int32_t PTK_Alg_FusedOgmapConfig(const PTK_Alg_FusedOgmapParams   * cfgParams,
                                 PTK_Api_MemoryReq                * memReq);

/**
 * \brief Initializes the library based on the given algorithm configuration
 *        parameters and allocated memory blocks.
 *
 * \param [in] cfgParams Algorithm configuration parameters. It is ecpected
 *                       that the same configuration parameters passed in the
 *                       PTK_Alg_FusedOgmapConfig() API are given. No check can
 *                       be made to make sure these are the same. Failure to
 *                       provide the same parameter set might result in an
 *                       unexpected behavior (ex:- memory requirements could
 *                       be different for different parameter set).
 *
 * \param [in] memRsp Memory blocks as specified below
 *             - entry [0]: Memory for internal context.
 *             - entry [1]: Memory for internal point cloud.
 *
 * \return
 *        - valid handle, if successful
 *        - NULL, otherwise
 *
 * \ingroup group_ptk_algos_fused_ogmap
 */
PTK_AlgHandle
PTK_Alg_FusedOgmapInit(const PTK_Alg_FusedOgmapParams * cfgParams,
                       const PTK_Api_MemoryRsp        * memRsp);

/**
 * \brief Processes the given point cloud and performs the Occupancy grid
 *        mapping.
 *
 * \param [inout] algHandle Algorithm handle.
 *
 * \param [in] cfgParams Configuration parameters.
 *
 * \param [inout] cameraMap OGMAP from camera processing.
 *
 * \param [inout] radarMap OGMAP from radar processing.
 *
 * \param [inout] lidarMap OGMAP from lidar processing.
 *
 * \param [in] insRec INS record corresponding to the sensor maps.
 *
 * \param [in] Md_ecef_w Transform from world to ECEF.
 *
 * \param [out] outMap Fused output OGMAP.
 *
 * \return
 *        - PTK_ALG_RET_SUCCESS, if successful
 *        - < error code, otherwise
 *
 * \ingroup group_ptk_algos_fused_ogmap
 */
int32_t PTK_Alg_FusedOgmapProcess(PTK_AlgHandle                 algHandle,
                                  PTK_Alg_FusedOgmapParams     *cfgParams,
                                  PTK_Map                      *cameraMap,
                                  PTK_Map                      *radarMap,
                                  PTK_Map                      *lidarMap,
                                  PTK_INS_Record               *insRec,
                                  const PTK_RigidTransform_d   *Md_ecef_w,
                                  PTK_Map                      *outMap);

/**
 * \brief De-initializes the algorithm context.
 *
 * \param [inout] algHandle Algorithm handle.
 *
 * \ingroup group_ptk_algos_fused_ogmap
 */
void PTK_Alg_FusedOgmapDeInit(PTK_AlgHandle algHandle);

/* PRIVATE APIS (not visible to OVX node) */

#ifdef __cplusplus
}
#endif

#endif
