/*
 *
 * Copyright (c) 2018 Texas Instruments Incorporated
 *
 * All rights reserved not granted herein.
 *
 * Limited License.
 *
 * Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
 * license under copyrights and patents it now or hereafter owns or controls to make,
 * have made, use, import, offer to sell and sell ("Utilize") this software subject to the
 * terms herein.  With respect to the foregoing patent license, such license is granted
 * solely to the extent that any such patent is necessary to Utilize the software alone.
 * The patent license shall not apply to any combinations which include this software,
 * other than combinations with devices manufactured by or for TI ("TI Devices").
 * No hardware patent is licensed hereunder.
 *
 * Redistributions must preserve existing copyright notices and reproduce this license
 * (including the above copyright notice and the disclaimer and (if applicable) source
 * code license limitations below) in the documentation and/or other materials provided
 * with the distribution
 *
 * Redistribution and use in binary form, without modification, are permitted provided
 * that the following conditions are met:
 *
 * *       No reverse engineering, decompilation, or disassembly of this software is
 * permitted with respect to any software provided in binary form.
 *
 * *       any redistribution and use are licensed by TI for use only with TI Devices.
 *
 * *       Nothing shall obligate TI to provide you with source code for the software
 * licensed and provided to you in object code.
 *
 * If software source code is provided to you, modification and redistribution of the
 * source code are permitted provided that the following conditions are met:
 *
 * *       any redistribution and use of the source code, including any resulting derivative
 * works, are licensed by TI for use only with TI Devices.
 *
 * *       any redistribution and use of any object code compiled from the source code
 * and any resulting derivative works, are licensed by TI for use only with TI Devices.
 *
 * Neither the name of Texas Instruments Incorporated nor the names of its suppliers
 *
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * DISCLAIMER.
 *
 * THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
#ifndef PTK_LIDAR_MDC_H
#define PTK_LIDAR_MDC_H

/**
 * @file lidar_algorithms.h
 * @brief Definition of lidar algorithm data structures and API
 */
#include <perception/perception.h>
#include <perception/algos/alg_common.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * \defgroup group_ptk_algos_lidar_mdc PTK Lidar Motion Distortion Correction
 * \ingroup group_ptk_algos
 *
 */

/**
 * \brief This undoes distortion in the collected LiDAR data caused by the
 *        sensor's movement as it completes a single rotation. The results
 *        are overwritten in the original cloud given. The algorithm used is to
 *        transform each point from LiDAR coordinates to the reference ENU
 *        coordinate system and then back to the LiDAR coordinate system
 *        corresponding to the first slice in the point cloud.
 *
 * \param [inout] src The point cloud to correct.
 *
 * \param [in] meta The input LiDAR metadata.
 *
 * \param [in] M_ego_lidar The calibrated transformation matrix from LiDAR
 *                         coordinates to ego coordinates.
 *
 * \param [out] referenceRoot The reference point that defines the ENU
 *                            coordinate system where we do local processing
 *
 * \return
 *        - PTK_ALG_RET_SUCCESS, if successful.
 *        - < error code, otherwise.
 *
 * \ingroup group_ptk_algos_lidar_mdc
 */
int32_t PTK_Lidar_correctMotionDistortion_HDL32E(PTK_PointCloud *src,
                                                 const PTK_LidarMeta *meta,
                                                 const PTK_RigidTransform *M_ego_lidar,
                                                 PTK_Position *referenceRoot);

#ifdef __cplusplus
}
#endif

#endif

