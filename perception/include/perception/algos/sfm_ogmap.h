/*
 *
 * Copyright (c) 2018 Texas Instruments Incorporated
 *
 * All rights reserved not granted herein.
 *
 * Limited License.
 *
 * Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
 * license under copyrights and patents it now or hereafter owns or controls to make,
 * have made, use, import, offer to sell and sell ("Utilize") this software subject to the
 * terms herein.  With respect to the foregoing patent license, such license is granted
 * solely to the extent that any such patent is necessary to Utilize the software alone.
 * The patent license shall not apply to any combinations which include this software,
 * other than combinations with devices manufactured by or for TI ("TI Devices").
 * No hardware patent is licensed hereunder.
 *
 * Redistributions must preserve existing copyright notices and reproduce this license
 * (including the above copyright notice and the disclaimer and (if applicable) source
 * code license limitations below) in the documentation and/or other materials provided
 * with the distribution
 *
 * Redistribution and use in binary form, without modification, are permitted provided
 * that the following conditions are met:
 *
 * *       No reverse engineering, decompilation, or disassembly of this software is
 * permitted with respect to any software provided in binary form.
 *
 * *       any redistribution and use are licensed by TI for use only with TI Devices.
 *
 * *       Nothing shall obligate TI to provide you with source code for the software
 * licensed and provided to you in object code.
 *
 * If software source code is provided to you, modification and redistribution of the
 * source code are permitted provided that the following conditions are met:
 *
 * *       any redistribution and use of the source code, including any resulting derivative
 * works, are licensed by TI for use only with TI Devices.
 *
 * *       any redistribution and use of any object code compiled from the source code
 * and any resulting derivative works, are licensed by TI for use only with TI Devices.
 *
 * Neither the name of Texas Instruments Incorporated nor the names of its suppliers
 *
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * DISCLAIMER.
 *
 * THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
#ifndef _SFM_OGMAP_H_
#define _SFM_OGMAP_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <perception/perception.h>
#include <perception/algos/alg_common.h>
#include <perception/utils/api_memory_if.h>

/**
 * \defgroup group_ptk_algos_sfm_ogmap PTK Structure From Motion Occupancy Grid Map
 * \ingroup group_ptk_algos
 *
 */

/**
 * \brief Mapping method : count OG map only.
 * \ingroup group_ptk_algos_sfm_ogmap
 */
#define PTK_ALG_SFM_OGMAP_MAPPING_METHOD_CNT_ONLY   (0U)

/**
 * \brief Mapping method : count and DS OG maps.
 * \ingroup group_ptk_algos_sfm_ogmap
 */
#define PTK_ALG_SFM_OGMAP_MAPPING_METHOD_CNT_AND_DS (PTK_ALG_SFM_OGMAP_MAPPING_METHOD_CNT_ONLY+1U)

/**
 * \brief Mapping method : Max value for error check.
 * \ingroup group_ptk_algos_sfm_ogmap
 */
#define PTK_ALG_SFM_OGMAP_MAPPING_METHOD_MAX        (PTK_ALG_SFM_OGMAP_MAPPING_METHOD_CNT_AND_DS+1)

/**
 * \brief Index of the accumulated occupancy map information to be used
 *        in the mapconfig array.
 * \ingroup group_ptk_algos_sfm_ogmap
 */
#define PTK_ALG_SFM_OGMAP_OUT_MAP_ACC_OCC           (0U)

/**
 * \brief Index of the instantaneous DS map information to be used
 *        in the mapconfig array.
 * \ingroup group_ptk_algos_sfm_ogmap
 */
#define PTK_ALG_SFM_OGMAP_OUT_MAP_INST_DS           (PTK_ALG_SFM_OGMAP_OUT_MAP_ACC_OCC+1U)

/**
 * \brief Maximum allowed sfm output maps.
 * \ingroup group_ptk_algos_sfm_ogmap
 */
#define PTK_ALG_SFM_OGMAP_NUM_OUT_MAPS              (PTK_ALG_SFM_OGMAP_OUT_MAP_INST_DS+1U)

/**
 * \brief Index of the accumulated occupancy map information to be used
 *        in the memReq/memRsp arrays.
 * \ingroup group_ptk_algos_sfm_ogmap
 */
#define PTK_ALG_SFM_OGMAP_MEM_BUFF_MAP_ACC_OCC      (4U)

/**
 * \brief Index of the instantaneous occupancy map information to be used
 *        in the memReq/memRsp arrays.
 * \ingroup group_ptk_algos_sfm_ogmap
 */
#define PTK_ALG_SFM_OGMAP_MEM_BUFF_MAP_INST_DS      (5U)

/**
 * \brief Number of entries that would be set in the 'memReq' object in the
 *        PTK_Alg_SfmOgmapConfig() API.
 * \ingroup group_ptk_algos_sfm_ogmap
 */
#define PTK_ALG_SFM_OGMAP_NUM_MEM_REQ_BLKS          (6U)

/**
 * \brief Number of entries that should be set in the 'memRsp' object in the
 *        PTK_Alg_SfmOgmapInit() API.
 * \ingroup group_ptk_algos_sfm_ogmap
 */
#define PTK_ALG_SFM_OGMAP_NUM_MEM_RSP_BLKS          (4U)

typedef struct
{
    float       pheight;
    uint32_t    ppointCount;
    uint32_t    pgroundCount;
    uint32_t    pobstacleCount;

}PTK_Alg_SfmOgmapCntData;

/**
 * \brief SFM Ogmap algorithm configuration parameters.
 *
 * \ingroup group_ptk_algos_sfm_ogmap
 */
typedef struct
{
    /** [STATIC]0-average height map + ground plane from height map +
     *  ground/obstacle point counter.
     */
    uint32_t            method;

    /** Grid configuration. */
    PTK_GridConfig      gridConfig;

    /** Grid ID of the accumulated DS Grid.  \n
      * Allowed: {0, 1, 2, ..., UINT32_MAX}
      */
    uint32_t            accGridId;

    /** Grid ID of the instantaneous Occupancy Grid.  \n
      * Allowed: {0, 1, 2, ..., UINT32_MAX}
      */
    uint32_t            instOccGridId;

    /** Grid ID of the instantaneous DS Grid.  \n
      * Allowed: {0, 1, 2, ..., UINT32_MAX}
      */
    uint32_t            instDsGridId;

    /** [STATIC] bit flag used in "occupancy" grid to indicate free. \n
      * Allowed: {0x00000001, 0x00000002, 0x00000004, 0x00000008,
      *           0x00000010, 0x00000020, 0x00000080, 0x00000100}
      */
    uint32_t            ogFlagFree;

    /** [STATIC] bit flag used in "occupancy" grid to indicate occupied. \n
      * Allowed: {0x00000001, 0x00000002, 0x00000004, 0x00000008,
      *           0x00000010, 0x00000020, 0x00000080, 0x00000100}
      */
    uint32_t            ogFlagOccupied;

    /** Flag to indicate cell was updated. \n
      * Allowed: {0x00000001, 0x00000002, 0x00000004, 0x00000008,
      *           0x00000010, 0x00000020, 0x00000080, 0x00000100}
      */   
    uint32_t             ogFlagChanged;

    /** Mapping method
      *  \verbatim
      *  0 - Count OG map
      *  1 - Dempster-Shafer OG map
      *  2 - Binarized count OG map and Dempster-Shafer OG map.
      * Allowed: {0, 1, 2}
      * \endverbatim
      */
    uint32_t            mappingMethod;

    /** Threshold to binarise DS OG map.\n
      * Range: [0, 1]
      */
    float               dsBinarizeThresh;

    /**  Weight for State SD.\n
      * Range: [0, 1]
      */
    float               dsSDWeight;

    /**  Weight for State F. \n
      * Range: [0, 1]
      */
    float               dsFWeight;

    /**  Weight for State D. \n
      * Range: [0, 1]
      */
    float               dsDWeight;


    /* Mapping algorithm parameters */
    /** [STATIC]initial (static) estimate for ground to ego (vehicle)
      *  transform.
      */
    PTK_RigidTransform  M_e_g;

    /** [DYNAMIC]point is "obstacle" if its height > "heightThreshBase +
      *  heightThreshSlope * distance to car". \n
      * Range: [0.0, inf)
      */
    float               heightThreshBase;

    /** [DYNAMIC]point is "obstacle" if its height > "heightThreshBase +
      *  heightThreshSlope * distance to car".\n
      * Range: [0.0, inf)
      */
    float               heightThreshSlope;

    /** [DYNAMIC]minimum number of observed points in a cell to be considered
      *  for free/oocupied classification. \n
      * Allowed: {0, 1, 2, ..., UINT32_MAX}
      */
    uint32_t            minPointCount;

    /** [DYNAMIC]maximum longitudinal distance from car for a point to be
      *  considered for mapping. \n
      * Range: [0.0, inf)
      */
    float               maxObjectDistanceLong;

    /** [DYNAMIC]minimum longitudinal distance from car for a point to be
     *  considered for mapping. \n
     * Range: [0.0, inf)
     */
    float               minObjectDistanceLong;

    /** [DYNAMIC]maximum lateral distance from car for a point to be considered
     *  for mapping. \n
     * Range: [0.0, inf)
     */
    float               maxObjectDistanceLat;

    /** [DYNAMIC]maximum height from ground for a point to be considered for
     *  mapping. \n
     * Range: [0.0, inf)
     */
    float               maxObjectDistanceUp;

    /** [DYNAMIC]maximum distance from car for a cell to be considered for
     *  ground plane RANSAC.\n
     * Range: [0.0, inf)
     */
    float               ransacMaxDistance;

    /** [DYNAMIC]number of iterations for ground plane RANSAC. \n
      * Allowed: {0, 1, 2, ..., UINT32_MAX}
      */
    uint32_t            ransacIters;

    /** [DYNAMIC]tolerance (inlier threshold) for ground plane RANSAC. \n
      * Range: [0.0, inf)
      */
    float               ransacTol;

} PTK_Alg_SfmOgmapParams;

/* PUBLIC APIS  */
/**
 * \brief Initializes the map configuration based on the given algorithm
 *        configuration parameters.
 *
 * \param [in] cfgParams Algorithm configuration parameters.
 *
 * \param [out] config Map configuration
 *
 * \ingroup group_ptk_algos_sfm_ogmap
 */
 int32_t PTK_Alg_SfmOgmapGetMapConfig(
     const PTK_Alg_SfmOgmapParams    * cfgParams,
     PTK_MapConfig                     config[PTK_ALG_SFM_OGMAP_NUM_OUT_MAPS]);

/**
 * \brief Computes the memory requirements based on the given algorithm
 *        configuration parameters.
 *
 * \param [in] cfgParams Algorithm configuration parameters.
 *
 * \param [out] memReq Memory requirements as specified below
 *             - entry [0]: Memory for internal context. Best allocated from
 *                          the fastest memory.
 *             - entry [1]: Memory for internal data. Best allocated from
 *                          the fastest memory.
 *             - entry [2]: Memory for internal data. Best allocated from
 *                          the fastest memory.
 *             - entry [3]: Memory for internal data. Best allocated from
 *                          the fastest memory.
 *             - entry [4]: Memory for the accumulated map. The output buffer is
 *                          allocated by the Application so this entry is just
 *                          to communicate the output buffer size. No need to
 *                          pass the memory to the library in the init call
 *                          PTK_Alg_RadarOgmapInit().
 *             - entry [5]: Memory for the intsntaneous map. The output buffer is
 *                          allocated by the Application so this entry is just
 *                          to communicate the output buffer size. No need to
 *                          pass the memory to the library in the init call
 *                          PTK_Alg_RadarOgmapInit().
 *
 * \return
 *        - PTK_ALG_RET_SUCCESS, if successful
 *        - < error code, otherwise
 *
 * \ingroup group_ptk_algos_sfm_ogmap
 */
int32_t PTK_Alg_SfmOgmapConfig(
    const PTK_Alg_SfmOgmapParams   * cfgParams,
    PTK_Api_MemoryReq              * memReq);

/**
 * \brief Initializes the library based on the given algorithm configuration
 *        parameters and allocated memory blocks.
 *
 * \param [in] cfgParams Algorithm configuration parameters. It is ecpected
 *                       that the same configuration parameters passed in the
 *                       PTK_Alg_SfmOgmapConfig() API are given. No check can
 *                       be made to make sure these are the same. Failure to
 *                       provide the same parameter set might result in an
 *                       unexpected behavior (ex:- memory requirements could
 *                       be different for different parameter set).
 *
 * \param [in] memRsp Memory blocks as specified below
 *             - entry [0]: Memory for internal context.
 *             - entry [1]: Memory for internal data
 *             - entry [2]: Memory for internal data
 *             - entry [3]: Memory for internal data
 *
 * \return
 *        - valid handle, if successful
 *        - NULL, otherwise
 *
 * \ingroup group_ptk_algos_sfm_ogmap
 */
PTK_AlgHandle PTK_Alg_SfmOgmapInit(
    const PTK_Alg_SfmOgmapParams   * cfgParams,
    const PTK_Api_MemoryRsp        * memRsp);

/**
 * \brief Processes the given point cloud and performs the Occupancy grid
 *        mapping.
 *
 * \param [inout] algHandle Algorithm handle.
 *
 * \param [in] numPoints Number of valid points in the point array xyz_w.
 *
 * \param [in] xyz_w Detected points to be transformed and mapped.
 *
 * \param [in] M_w_e_in Ego to world coordinate transformation matrix.
 *
 * \param [in] M_e_g Current ground to ego estimate.
 *
 * \param [out] outAccMap Accumulated OGMAP
 *
 * \param [out] outInstMap Instantaneous OGMAP
 *
 * \return
 *        - PTK_ALG_RET_SUCCESS, if successful
 *        - < error code, otherwise
 *
 * \ingroup group_ptk_algos_sfm_ogmap
 */
int32_t PTK_Alg_SfmOgmapProcess(
    PTK_AlgHandle               algHandle,
    uint32_t                    numPoints,
    const PTK_Point           * xyz_w,
    const PTK_RigidTransform  * M_w_e_in,
    const PTK_RigidTransform  * M_e_g,
    PTK_Map                   * outAccMap,
    PTK_Map                   * outInstMap);

/**
 * \brief Resets the internal data buffers. This is needed if the application
 *        needs control to clear the current OGMAP.
 *
 * \param [inout] algHandle Algorithm handle.
 *
 * \return
 *        - PTK_ALG_RET_SUCCESS, if successful.
 *        - < error code, otherwise.
 *
 * \ingroup group_ptk_algos_sfm_ogmap
 */
int32_t PTK_Alg_SfmOgmapReset(PTK_AlgHandle algHandle);

/**
 * \brief De-initializes the algorithm context.
 *
 * \param [inout] algHandle Algorithm handle.
 *
 * \ingroup group_ptk_algos_sfm_ogmap
 */
void PTK_Alg_SfmOgmapDeInit(PTK_AlgHandle   algHandle);

/* PRIVATE APIS (not visible to OVX node) */

#ifdef __cplusplus
}
#endif

#endif
