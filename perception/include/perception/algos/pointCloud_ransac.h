/*
 *
 * Copyright (c) 2018 Texas Instruments Incorporated
 *
 * All rights reserved not granted herein.
 *
 * Limited License.
 *
 * Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
 * license under copyrights and patents it now or hereafter owns or controls to make,
 * have made, use, import, offer to sell and sell ("Utilize") this software subject to the
 * terms herein.  With respect to the foregoing patent license, such license is granted
 * solely to the extent that any such patent is necessary to Utilize the software alone.
 * The patent license shall not apply to any combinations which include this software,
 * other than combinations with devices manufactured by or for TI ("TI Devices").
 * No hardware patent is licensed hereunder.
 *
 * Redistributions must preserve existing copyright notices and reproduce this license
 * (including the above copyright notice and the disclaimer and (if applicable) source
 * code license limitations below) in the documentation and/or other materials provided
 * with the distribution
 *
 * Redistribution and use in binary form, without modification, are permitted provided
 * that the following conditions are met:
 *
 * *       No reverse engineering, decompilation, or disassembly of this software is
 * permitted with respect to any software provided in binary form.
 *
 * *       any redistribution and use are licensed by TI for use only with TI Devices.
 *
 * *       Nothing shall obligate TI to provide you with source code for the software
 * licensed and provided to you in object code.
 *
 * If software source code is provided to you, modification and redistribution of the
 * source code are permitted provided that the following conditions are met:
 *
 * *       any redistribution and use of the source code, including any resulting derivative
 * works, are licensed by TI for use only with TI Devices.
 *
 * *       any redistribution and use of any object code compiled from the source code
 * and any resulting derivative works, are licensed by TI for use only with TI Devices.
 *
 * Neither the name of Texas Instruments Incorporated nor the names of its suppliers
 *
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * DISCLAIMER.
 *
 * THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
#pragma once
#ifndef POINT_CLOUD_RANSAC_H
#define POINT_CLOUD_RANSAC_H

/**
 * @file pointCloud.h
 * @brief Declaration of structures and functions used to manipulate point clouds
 */

#include <stdint.h>
#include <perception/base/pointCloud.h>
#include <perception/base/well.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * \defgroup group_ptk_algos_pc_ransac PTK Point Cloud based RANSAC
 * \ingroup group_ptk_algos
 *
 */

/**
 * \brief
 *
 * \param [in] pc Input point cloud.
 *
 * \param [in] ids array of length N. Indices of the points in the point cloud
 *                 pc that should be considered for ransac fit.
 *
 * \param [in] N Length of ids array.
 *
 * \param [in] tol RANSAC tolerance (if error < tol ? inlier, otherwise ? outlier).
 *                 Error is computed as Euclidean distance from the proposed plane.
 *
 * \param [in] iters Number of RANSAC iterations to perform.
 *
 * \param [out] nd Array of length 4. First 3 elements are unit normal vector
 *                 of the plane, 4th element is the plane distance from the
 *                 origin. 
 *
 *                 A point (x,y,z) on the plane satisfies
 *                 nd[0]*x + nd[1]*y + nd[2]*z - nd[3]==0.
 *
 * \param [out] foundInliers Array of length "return value" with indices in
 *                           point cloud pc found to be inliers
 *
 * \return
 *
 * \ingroup group_ptk_algos_pc_ransac
 */
uint32_t PTK_PointCloud_ransacPlane(const PTK_PointCloud   *pc,
                                    const uint32_t         *__restrict ids,
                                    uint32_t                N,
                                    float                   tol,
                                    uint32_t                iters,
                                    float                  *nd,
                                    uint32_t                *__restrict foundInliers);

#ifdef __cplusplus
}
#endif

#endif
