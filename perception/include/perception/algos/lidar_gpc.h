/*
 *
 * Copyright (c) 2018 Texas Instruments Incorporated
 *
 * All rights reserved not granted herein.
 *
 * Limited License.
 *
 * Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
 * license under copyrights and patents it now or hereafter owns or controls to make,
 * have made, use, import, offer to sell and sell ("Utilize") this software subject to the
 * terms herein.  With respect to the foregoing patent license, such license is granted
 * solely to the extent that any such patent is necessary to Utilize the software alone.
 * The patent license shall not apply to any combinations which include this software,
 * other than combinations with devices manufactured by or for TI ("TI Devices").
 * No hardware patent is licensed hereunder.
 *
 * Redistributions must preserve existing copyright notices and reproduce this license
 * (including the above copyright notice and the disclaimer and (if applicable) source
 * code license limitations below) in the documentation and/or other materials provided
 * with the distribution
 *
 * Redistribution and use in binary form, without modification, are permitted provided
 * that the following conditions are met:
 *
 * *       No reverse engineering, decompilation, or disassembly of this software is
 * permitted with respect to any software provided in binary form.
 *
 * *       any redistribution and use are licensed by TI for use only with TI Devices.
 *
 * *       Nothing shall obligate TI to provide you with source code for the software
 * licensed and provided to you in object code.
 *
 * If software source code is provided to you, modification and redistribution of the
 * source code are permitted provided that the following conditions are met:
 *
 * *       any redistribution and use of the source code, including any resulting derivative
 * works, are licensed by TI for use only with TI Devices.
 *
 * *       any redistribution and use of any object code compiled from the source code
 * and any resulting derivative works, are licensed by TI for use only with TI Devices.
 *
 * Neither the name of Texas Instruments Incorporated nor the names of its suppliers
 *
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * DISCLAIMER.
 *
 * THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
#ifndef PTK_LIDAR_GPC_H
#define PTK_LIDAR_GPC_H

#include <perception/perception.h>
#include <perception/algos/alg_common.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * \defgroup group_ptk_algos_lidar_gpc PTK Lidar Ground Point Classification
 * \ingroup group_ptk_algos
 *
 */

/**
 * \brief Lidar GPC configuration parameters.
 *
 * \ingroup group_ptk_algos_lidar_gpc
 */
typedef struct
{
    /** Z component threshold for inlier set. \n
      * Range: [0.0, inf)
      */
    float       z_threshold;

    /** Tolerance for inliers in the ground plane estimation.\n
      * Range: [0.0, inf)
      */
    float       ground_dtol;

    /** Number of ransac iterations, 0 for default. \n
      * Allowed: {0, 1, ..., UINT32_MAX}
      */
    uint32_t    ransac_iter;

    /** Tag for points that are removed from the point cloud and should not be considered. \n
      * Allowed: {0, 1}
      */
    uint32_t    removed_tag;

    /** Tag to apply to ground points after detection.\n
      * Allowed: {0, 1}
      */
    uint32_t    ground_tag;

} PTK_Lidar_GpcConfig;

/**
 * \brief Get the size of the scratch memory needed to be used in
 *        PTK_Lidar_performGPC() API.
 *
 * \return Size in bytes for the scratch memory.
 *
 * \ingroup group_ptk_algos_lidar_gpc
 */
uint32_t PTK_Lidar_getGpcScratchMemSize();

/**
 * \brief Get the size of the scratch memory needed to be used in
 *        PTK_Lidar_estimatePointNormals() API.
 *
 * \return Size in bytes for the scratch memory.
 *
 * \ingroup group_ptk_algos_lidar_gpc
 */
uint32_t PTK_Lidar_getEstNormScratchMemSize();

/**
 * \brief Process the input point cloud and classify the points as ground
 *        or not.
 *
 * \param [in] pc The input LiDAR point cloud.
 *
 * \param [in] meta The input LiDAR metadata.
 *
 * \param [in] config Algorithm configuration.
 *
 * \param [in] scratchMem Scratch memory buffer. The size of this memory
 *             should match the value returned by
 *             PTK_Lidar_getGpcScratchMemSize() API.
 *
 * \param [out] normals Normal vectors written as points here.
 *
 * \return
 *        - PTK_ALG_RET_SUCCESS, if successful.
 *        - < error code, otherwise.
 *
 * \ingroup group_ptk_algos_lidar_gpc
 */
int32_t PTK_Lidar_performGPC(PTK_PointCloud            * pc,
                             const PTK_LidarMeta       * meta,
                             const PTK_Lidar_GpcConfig * config,
                             uint8_t                   * scratchMem,
                             PTK_PointCloud            * normals);

/**
 * \brief Estimate the normal vectors for each point of the given cloud and
 *        write them to the normals cloud at matching indices. This requires
 *        normals to have been initialized as a point cloud with the same
 *        configuration as cloud.
 *
 * \param [in] src The input LiDAR point cloud.
 *
 * \param [in] meta The input LiDAR metadata.
 *
 * \param [in] scratchMem Scratch memory buffer. The size of this memory
 *             should match the value returned by
 *             PTK_Lidar_getEstNormScratchMemSize() API.
 *
 * \param [out] normals Normal vectors written as points here.
 *
 * \return
 *        - PTK_ALG_RET_SUCCESS, if successful.
 *        - < error code, otherwise.
 *
 * \ingroup group_ptk_algos_lidar_gpc
 */
int32_t PTK_Lidar_estimatePointNormals(PTK_PointCloud *__restrict src,
                                       const PTK_LidarMeta *meta,
                                       uint8_t             *scratchMem,
                                       PTK_PointCloud *__restrict normals);

/**
 * \brief This transforms the given point cloud (which is assumed to have been
 *        motion distortion corrected) from its current LiDAR coordinate system
 *        to the reference ENU coordinate system defined by the position given.
 *        It also writes the matching translation vector out to be reused by
 *        the caller if necessary.
 *
 * \param [inout] src The point cloud to shift.
 *
 * \param [in] meta The LiDAR metadata.
 *
 * \param [in] M_ego_lidar The calibrated transformation matrix from LiDAR
 *                         coordinates to ego coordinates.
 *
 * \param [out] referenceRoot The reference point that defines the ENU
 *                            coordinate system where we do local processing.
 *
 * \param [out] translateENU The translation vector from the lidar's ENU frame
 *                           to the reference ENU frame is stored here for later
 *                           reuse.
 *
 * \ingroup group_ptk_algos_lidar_gpc
 */
void PTK_Lidar_shiftToRootENU(PTK_PointCloud *src,
                              const PTK_LidarMeta *meta,
                              const PTK_RigidTransform *M_ego_lidar,
                              PTK_Position *referenceRoot,
                              PTK_Point *translateENU);

#ifdef __cplusplus
}
#endif

#endif
