/*
 *
 * Copyright (c) 2019 Texas Instruments Incorporated
 *
 * All rights reserved not granted herein.
 *
 * Limited License.
 *
 * Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
 * license under copyrights and patents it now or hereafter owns or controls to make,
 * have made, use, import, offer to sell and sell ("Utilize") this software subject to the
 * terms herein.  With respect to the foregoing patent license, such license is granted
 * solely to the extent that any such patent is necessary to Utilize the software alone.
 * The patent license shall not apply to any combinations which include this software,
 * other than combinations with devices manufactured by or for TI ("TI Devices").
 * No hardware patent is licensed hereunder.
 *
 * Redistributions must preserve existing copyright notices and reproduce this license
 * (including the above copyright notice and the disclaimer and (if applicable) source
 * code license limitations below) in the documentation and/or other materials provided
 * with the distribution
 *
 * Redistribution and use in binary form, without modification, are permitted provided
 * that the following conditions are met:
 *
 * *       No reverse engineering, decompilation, or disassembly of this software is
 * permitted with respect to any software provided in binary form.
 *
 * *       any redistribution and use are licensed by TI for use only with TI Devices.
 *
 * *       Nothing shall obligate TI to provide you with source code for the software
 * licensed and provided to you in object code.
 *
 * If software source code is provided to you, modification and redistribution of the
 * source code are permitted provided that the following conditions are met:
 *
 * *       any redistribution and use of the source code, including any resulting derivative
 * works, are licensed by TI for use only with TI Devices.
 *
 * *       any redistribution and use of any object code compiled from the source code
 * and any resulting derivative works, are licensed by TI for use only with TI Devices.
 *
 * Neither the name of Texas Instruments Incorporated nor the names of its suppliers
 *
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * DISCLAIMER.
 *
 * THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
#ifndef _RADAR_OGMAP_H_
#define _RADAR_OGMAP_H_

#include <math.h>

#include <perception/perception.h>
#include <perception/common/common_types.h>
#include <perception/algos/alg_common.h>
#include <perception/algos/radar_data_types.h>
#include <perception/utils/api_memory_if.h>

/**
 * \defgroup group_ptk_algos_radar_ogmap PTK Radar Occupancy Grid Map
 * \ingroup group_ptk_algos
 *
 */

#ifdef __cplusplus
extern "C" {
#endif

/**
 * \brief Mapping method : probability OG map only.
 * \ingroup group_ptk_algos_radar_ogmap
 */
#define PTK_ALG_RADAR_OGMAP_MAPPING_METHOD_PROB_ONLY    (0U)

/**
 * \brief Mapping method : DS OG map only.
 * \ingroup group_ptk_algos_radar_ogmap
 */
#define PTK_ALG_RADAR_OGMAP_MAPPING_METHOD_DS_ONLY      (1U)

/**
 * \brief Mapping method : Max value for error checks.
 * \ingroup group_ptk_algos_radar_ogmap
 */
#define PTK_ALG_RADAR_OGMAP_MAPPING_METHOD_MAX          (PTK_ALG_RADAR_OGMAP_MAPPING_METHOD_DS_ONLY+1)

/**
 * \brief Maximum allowed radar measurements.
 * \ingroup group_ptk_algos_radar_ogmap
 */
#define PTK_ALG_RADAR_OGMAP_MAX_MEASUREMENTS            (400U)

/**
 * \brief Maximum allowed radar measurements.
 * \ingroup group_ptk_algos_radar_ogmap
 */
#define PTK_ALG_RADAR_OGMAP_MAX_GRID_SIZE               (1000000U)

/**
 * \brief Radar sensor model 1.
 * \ingroup group_ptk_algos_radar_ogmap
 */
#define PTK_ALG_RADAR_OGMAP_SENSOR_MODEL1               (1U)

/**
 * \brief Radar sensor model 2.
 * \ingroup group_ptk_algos_radar_ogmap
 */
#define PTK_ALG_RADAR_OGMAP_SENSOR_MODEL3               (3U)

/**
 * \brief Index of the accumulated occupancy map information to be used
 *        in the mapconfig array.
 * \ingroup group_ptk_algos_radar_ogmap
 */
#define PTK_ALG_RADAR_OGMAP_OUT_MAP_ACC_OCC             (0U)

/**
 * \brief Index of the instantaneous DS map information to be used
 *        in the mapconfig array.
 * \ingroup group_ptk_algos_radar_ogmap
 */
#define PTK_ALG_RADAR_OGMAP_OUT_MAP_INST_DS             (PTK_ALG_RADAR_OGMAP_OUT_MAP_ACC_OCC+1U)

/**
 * \brief Maximum allowed radar output maps.
 * \ingroup group_ptk_algos_radar_ogmap
 */
#define PTK_ALG_RADAR_OGMAP_NUM_OUT_MAPS                (PTK_ALG_RADAR_OGMAP_OUT_MAP_INST_DS+1U)

/**
 * \brief Index of the accumulated occupancy map information to be used
 *        in the memReq/memRsp arrays.
 * \ingroup group_ptk_algos_radar_ogmap
 */
#define PTK_ALG_RADAR_OGMAP_MEM_BUFF_MAP_ACC_OCC        (3U)

/**
 * \brief Index of the instantaneous occupancy map information to be used
 *        in the memReq/memRsp arrays.
 * \ingroup group_ptk_algos_radar_ogmap
 */
#define PTK_ALG_RADAR_OGMAP_MEM_BUFF_MAP_INST_DS        (4U)

/**
 * \brief Number of entries that would be set in the 'memReq' object in the
 *        PTK_Alg_RadarOgmapConfig() API.
 * \ingroup group_ptk_algos_radar_ogmap
 */
#define PTK_ALG_RADAR_OGMAP_NUM_MEM_REQ_BLKS            (5U)

/**
 * \brief Number of entries that should be set in the 'memRsp' object in the
 *        PTK_Alg_RadarOgmapInit() API.
 * \ingroup group_ptk_algos_radar_ogmap
 */
#define PTK_ALG_RADAR_OGMAP_NUM_MEM_RSP_BLKS            (3U)

/**
 * \brief Radar Ogmap algorithm configuration parameters.
 *
 * \ingroup group_ptk_algos_radar_ogmap
 */
typedef struct
{
    /** Sensor Model to be used. \n
      * Allowed: {1, 3}
      */
    uint32_t                    sensorModel;

    /** x range over which the probablilites are computed. \n
      * Range: [0.0, inf)
      */
    int32_t                     range_x;

    /** y range over which the probablilites are computed. \n
      * Range: [0.0, inf)
      */
    int32_t                     range_y;

    /** Grid configuration. */
    PTK_GridConfig              gridConfig;

    /** Smoothing factor for grid probability calculations. \n
      * Range: [0.0, 1.0]
      */
    float                       beta;

    /** Minimum occupany probability. \n
      * Range: [0.0, 1.0]
      */
    float                       minOccupProb;

    /** Mask indicating which sensors are enabled. It can be a combination
      * of the following
      * \verbatim
      * 0x01 Radar 1
      * 0x01 Radar 2
      * 0x01 Radar 3
      * 0x01 Radar 4
      * \endverbatim
      */
    uint32_t                    sensorMask;

    /** Grid ID of the accumulated DS Grid.  \n
      * Allowed: {0, 1, 2, ..., UINT32_MAX}
      */
    uint32_t                    accGridId;

    /** Grid ID of the instantaneous Occupancy Grid.  \n
      * Allowed: {0, 1, 2, ..., UINT32_MAX}
      */
    uint32_t                    instOccGridId;

    /** Grid ID of the instantaneous DS Grid.  \n
      * Allowed: {0, 1, 2, ..., UINT32_MAX}
      */
    uint32_t                    instDsGridId;


    /** Flag for an occupied grid cell.  \n
      * Allowed: {0x00000001, 0x00000002, 0x00000004, 0x00000008,
      *           0x00000010, 0x00000020, 0x00000080, 0x00000100}
      */
    uint32_t                    ogFlagOccupied;

    /** Flag for an occupied grid cell.  \n
      * Allowed: {0x00000001, 0x00000002, 0x00000004, 0x00000008,
      *           0x00000010, 0x00000020, 0x00000080, 0x00000100}
      */
    uint32_t                    ogFlagFree;

    /** Flag to indicate cell was updated. \n
      * Allowed: {0x00000001, 0x00000002, 0x00000004, 0x00000008,
      *           0x00000010, 0x00000020, 0x00000080, 0x00000100}
      */    
    uint32_t                    ogFlagChanged;

    /** Binarization enabled or not. 0-OFF 1-ON.  \n
      * Allowed: {0, 1}
      */
    uint32_t                    binarize;

    /** Threshold for binarization. \n
      * Range: [0.0, inf) if mappingMethod == 0
      * Range: [0.0, 1.0] if mappingMethod == 1
      */
    float                       binarizeThresh;

    /** Configuration information. */
    PTK_Alg_RadarSensorConfig   cfg[PTK_ALG_RADAR_NUM_SENSORS];

    /** Maximum range to gate radar measurements \n
      * Range: [0.0, inf)
      */
    float                       max_range;

    /** Range after which the weighting on range measurements
        will decay exponentially. \n
      * Range: [0.0, inf)
      */
    float                       decayT_F_range;

    /** Rate at which range measurements will decay after
        decayT_F_range. \n
      * Range: [0.0, inf)
      */
    float                       expWeight_F_range;

    /** Range after which weighting factor is set to 0
        to reduce computations (i.e. when weights ~0).\n
      * Range: [0.0, inf)
      */
    float                       cutOffT_F_range;

    /** Maximum angle to gate radar measurements. \n
      * Range: [0.0, 2*pi]
      */
    float                       max_angle;

    /** Noise floor for radar measurements. \n
      * Range: [0.0, inf)
      */
    float                       noiseFloor;

    /** Maximum value allowed for instantaneous probability
        of Free. \n
      * Range: [0.0, 1.0]
      */
    float                       maxProbOcc_F;

    /** Maximum value allowed for instantaneous probability
        of Occupied by dynamic object. \n
      * Range: [0.0, 1.0]
      */
    float                       maxProbOcc_D;

    /** Maximum value allowed for instantaneous probability
        of Occupied by dynamic or stationary object. \n
      * Range: [0.0, 1.0]
      */
    float                       maxProbOcc_SD;

    /** Used as a threshold on Velocity to
        distinguish between SD and D. \n
      * Range: [0.0, inf)
      */
    float                       velEps;

    /** When 0, the code assumes Doppler is unavailable, when
        1 code assumes Doppler is available. \n
      * Allowed: {0, 1}
      */
    uint8_t                     velAvailable;

    /** Mapping method:
        0 - Proabability OG map.
        1 - Dempster-Shafer OG map.
        2 - Proabability OG map and Dempster-Shafer OG map.\n
      * Allowed: {0, 1, 2}
      */
    uint32_t                    mappingMethod;

} PTK_Alg_RadarOgmapParams;

static inline float PTK_Alg_fmax(
	float f_1,
	float f_2)
{
    return (f_1 > f_2) ? f_1 : f_2;
}

static inline float PTK_Alg_fmin(
	float f_1,
	float f_2)
{
    return (f_1 < f_2) ? f_1 : f_2;
}

/* PUBLIC APIS  */
/**
 * \brief Initializes the map configuration based on the given algorithm
 *        configuration parameters.
 *
 * \param [in] cfgParams Algorithm configuration parameters.
 *
 * \param [out] config Map configuration.
 *                     - config[0] gives the accumulated map configuration
 *                     - config[1] gives the instantaneous map configuration
 *
 * \ingroup group_ptk_algos_radar_ogmap
 */
int32_t
PTK_Alg_RadarOgmapGetMapConfig(
    const PTK_Alg_RadarOgmapParams * cfgParams,
    PTK_MapConfig                    config[PTK_ALG_RADAR_OGMAP_NUM_OUT_MAPS]);

/**
 * \brief Computes the memory requirements based on the given algorithm
 *        configuration parameters.
 *
 * \param [in] cfgParams Algorithm configuration parameters.
 *
 * \param [out] memReq Memory requirements as specified below
 *             - entry [0]: Memory for internal context. Best allocated from
 *                          the fastest memory.
 *             - entry [1]: Memory for internal data. Best allocated from
 *                          the fastest memory.
 *             - entry [2]: Memory for internal data. Best allocated from
 *                          the fastest memory.
 *             - entry [3]: Memory for the accumulated map. The output buffer is
 *                          allocated by the Application so this entry is just
 *                          to communicate the output buffer size. No need to
 *                          pass the memory to the library in the init call
 *                          PTK_Alg_RadarOgmapInit().
 *             - entry [4]: Memory for the intsntaneous map. The output buffer is
 *                          allocated by the Application so this entry is just
 *                          to communicate the output buffer size. No need to
 *                          pass the memory to the library in the init call
 *                          PTK_Alg_RadarOgmapInit().
 *
 * \return
 *        - PTK_ALG_RET_SUCCESS, if successful.
 *        - < error code, otherwise.
 *
 * \ingroup group_ptk_algos_radar_ogmap
 */
int32_t PTK_Alg_RadarOgmapConfig(
    const PTK_Alg_RadarOgmapParams   * cfgParams,
    PTK_Api_MemoryReq                * memReq);

/**
 * \brief Initializes the library based on the given algorithm configuration
 *        parameters and allocated memory blocks.
 *
 * \param [in] cfgParams Algorithm configuration parameters. It is ecpected
 *                       that the same configuration parameters passed in the
 *                       PTK_Alg_RadarOgmapConfig() API are given. No check can
 *                       be made to make sure these are the same. Failure to
 *                       provide the same parameter set might result in an
 *                       unexpected behavior (ex:- memory requirements could
 *                       be different for different parameter set).
 *
 * \param [in] memRsp Memory blocks as specified below
 *             - entry [0]: Memory for internal context.
 *             - entry [1]: Memory for internal data
 *             - entry [2]: Memory for internal data
 *
 * \return
 *        - valid handle, if successful.
 *        - NULL, otherwise.
 *
 * \ingroup group_ptk_algos_radar_ogmap
 */
PTK_AlgHandle
PTK_Alg_RadarOgmapInit(
    const PTK_Alg_RadarOgmapParams * cfgParams,
    const PTK_Api_MemoryRsp        * memRsp);

/**
 * \brief Processes the given point cloud and performs the Occupancy grid
 *        mapping.
 *
 * \param [inout] algHandle Algorithm handle.
 *
 * \param [in] sensorCfg Radar sensor configuration.
 *
 * \param [in] doaData Radar object data.
 *
 * \param [in] posAndRef Current and previous INS records along with the
 *                       transform for ecef to world.
 *
 * \param [inout] outAccMap Accumulated OGMAP
 *
 * \param [out] outInstMap Instantaneous OGMAP
 *
 * \return
 *        - PTK_ALG_RET_SUCCESS, if successful.
 *        - < error code, otherwise.
 *
 * \ingroup group_ptk_algos_radar_ogmap
 */
int32_t PTK_Alg_RadarOgmapProcess(
    PTK_AlgHandle                     algHandle,
    const PTK_Alg_RadarSensorConfig * sensorCfg,
    const PTK_Alg_RadarDetOutput    * doaData,
    const PTK_InsPoseAndRef         * posAndRef,
    PTK_Map                         * outAccMap,
    PTK_Map                         * outInstMap);

/**
 * \brief Resets the internal data buffers. This is needed if the application
 *        needs control to clear the current OGMAP.
 *
 * \param [inout] algHandle Algorithm handle.
 *
 * \return
 *        - PTK_ALG_RET_SUCCESS, if successful.
 *        - < error code, otherwise.
 *
 * \ingroup group_ptk_algos_radar_ogmap
 */
int32_t PTK_Alg_RadarOgmapReset(PTK_AlgHandle algHandle);

/**
 * \brief Unpacks the data coming from the sensor into a detection object
 *        structure.
 *
 * \param [in] packedData Packed data buffer with detection information.
 *
 * \param [out] detOutput Unpacked detection output.
 *
 * \return
 *        - PTK_ALG_RET_SUCCESS, if successful.
 *        - < error code, otherwise.
 *
 * \ingroup group_ptk_algos_radar_ogmap
 */
int32_t PTK_Alg_RadarOgmapParseObjData(
    const uint8_t          * packedData,
    PTK_Alg_RadarDetOutput * detOutput);

/**
 * \brief Applies the range gating on the input data and geneates the output.
 *
 * \param [in] sensorCfg Packed data buffer with detection information.
 *
 * \param [in] dataIn Input data to be range gated.
 *
 * \param [out] dataOut Range gated output data.
 *
 * \ingroup group_ptk_algos_radar_ogmap
 */
void
PTK_Alg_RadarOgmapGateObjData(
    const PTK_Alg_RadarSensorConfig * sensorCfg,
    const PTK_Alg_RadarDetOutput    * dataIn,
    PTK_Alg_RadarDetOutput          * dataOut);

/**
 * \brief De-initializes the algorithm context.
 *
 * \param [inout] algHandle Algorithm handle.
 *
 * \ingroup group_ptk_algos_radar_ogmap
 */
void PTK_Alg_RadarOgmapDeInit(PTK_AlgHandle algHandle);

/* PRIVATE APIS (not visible to OVX node) */

#ifdef __cplusplus
}
#endif

#endif
