/*
*
* Copyright (c) 2019 Texas Instruments Incorporated
*
* All rights reserved not granted herein.
*
* Limited License.
*
* Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
* license under copyrights and patents it now or hereafter owns or controls to make,
* have made, use, import, offer to sell and sell ("Utilize") this software subject to the
* terms herein.  With respect to the foregoing patent license, such license is granted
* solely to the extent that any such patent is necessary to Utilize the software alone.
* The patent license shall not apply to any combinations which include this software,
* other than combinations with devices manufactured by or for TI ("TI Devices").
* No hardware patent is licensed hereunder.
*
* Redistributions must preserve existing copyright notices and reproduce this license
* (including the above copyright notice and the disclaimer and (if applicable) source
* code license limitations below) in the documentation and/or other materials provided
* with the distribution
*
* Redistribution and use in binary form, without modification, are permitted provided
* that the following conditions are met:
*
* *       No reverse engineering, decompilation, or disassembly of this software is
* permitted with respect to any software provided in binary form.
*
* *       any redistribution and use are licensed by TI for use only with TI Devices.
*
* *       Nothing shall obligate TI to provide you with source code for the software
* licensed and provided to you in object code.
*
* If software source code is provided to you, modification and redistribution of the
* source code are permitted provided that the following conditions are met:
*
* *       any redistribution and use of the source code, including any resulting derivative
* works, are licensed by TI for use only with TI Devices.
*
* *       any redistribution and use of any object code compiled from the source code
* and any resulting derivative works, are licensed by TI for use only with TI Devices.
*
* Neither the name of Texas Instruments Incorporated nor the names of its suppliers
*
* may be used to endorse or promote products derived from this software without
* specific prior written permission.
*
* DISCLAIMER.
*
* THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
* OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
* OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
* IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
* BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
* OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
* OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
* OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

#ifndef _FSD_PFSD_H_
#define _FSD_PFSD_H_

#include <perception/perception.h>
#include <perception/mathmapping.h>
#include <perception/common/common_types.h>
#include <perception/algos/alg_common.h>
#include <perception/utils/api_memory_if.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * \defgroup group_ptk_algos_fsdpfsd PTK Free and Park-able Free Space Detection
 * \ingroup group_ptk_algos
 *
 */

 /**
  * \brief Maximum allowed PFSD boxes.
  * \ingroup group_ptk_algos_fsdpfsd
  */
#define FSD_PFSD_MAX_NUM_BOXES          (4U)

/**
 * \brief Maximum allowed Park-parkable spaces,
 * \ingroup group_ptk_algos_fsdpfsd
 */
#define FSD_PFSD_MAX_PARKABLE_SPACES    (50U)

/**
 * \brief FSD-PFSD algorithm box parameters.
 *
 * \ingroup group_ptk_algos_fsdpfsd
 */
typedef struct
{
    /** Orientation of the box with respect to the EGO position.
      * the angle in the counter-clock wise direction is positive.
      * This angle must be specified with respect to the X-axis. \n
      * Range: [0.0, 2*pi]
      */
    float        orientation;

    /** The X coordinate of the lower left corner with respect to the EGO potision. \n
      * Range: (-inf, inf)
      */
    float        x_offset;

    /** The Y coordinate of the lower left corner with respect to the EGO potision. \n
      * Range: (-inf, inf)
      */
    float        y_offset;

    /** Length of the box along X-direction. \n
      * Range: [0.0, inf)
      */
    float        x_length;

    /** Length of the box along Y-direction. \n
      * Range: [0.0, inf)
      */
    float        y_length;

    /** pfsd feature enabled = 1. \n
      * Allowed: {0,1}
      */
    uint32_t     pfsd_enable;

    /** PFSD search length in X. Must be smaller than x_length. \n
      * Range: [0.0, x_length]
      */
    float       pfsd_x_length;

    /** PFSD start relative to FSD box edge (specified by x_offset). \n
      * Note: x_offset is the X coordinates of the closest lower left corner with 
      *       respect to the EGO position. \n
      * Must be smaller than (x_length - pfsd_x_length). \n
      *    Range: [0.0, (x_length - pfsd_x_length)]
      */
    float       pfsd_x_start;

    /** Variable to hold (pfsd_x_start+pfsd_x_length) for efficiency
      *    Range: [0.0, inf]
      */
    float       pfsd_x_end;

    /** PFSD search length in Y. Must be smaller than y_length. \n
      * Range: [0.0, y_length]
      */
    float       pfsd_y_length;

} PTK_Alg_FsdPfsdBoxParams;


/**
 * \brief FSD-PFSD algorithm configuration parameters.
 *
 * \ingroup group_ptk_algos_fsdpfsd
 */
typedef struct
{
  /** Flag to look for to determine FSD. \n
    * 0: Free if for Occupied Flag set to 0.
    * 1: Free if for Free Flag set to 1.
    * Allowed: {0, 1}
    */
    uint32_t                    checkFreeFlag;

    /** Number of FSD regions \n
      * Allowed: {0, 1, ..., \ref FSD_PFSD_MAX_NUM_BOXES}
      */
    uint32_t                    numBoxes;

    /** Number of FSD regions for OG map 1. \n
      * Allowed: {0, 1, ..., \ref FSD_PFSD_MAX_NUM_BOXES}
      */
    uint32_t                    numBoxes_ogMap_1;

    /** Number of FSD regions for OG map 2. \n
      * Allowed: {0, 1, ..., \ref FSD_PFSD_MAX_NUM_BOXES}
      */
    uint32_t                    numBoxes_ogMap_2;

    /** Number of FSD regions for OG map 3. \n
      * Allowed: {0, 1, ..., \ref FSD_PFSD_MAX_NUM_BOXES}
      */
    uint32_t                    numBoxes_ogMap_3;

    /** Number of FSD regions for OG map 4. \n
      * Allowed: {0, 1, ..., \ref FSD_PFSD_MAX_NUM_BOXES}
      */
    uint32_t                    numBoxes_ogMap_4;

    /** Box configurations. */
    PTK_Alg_FsdPfsdBoxParams    boxParams[FSD_PFSD_MAX_NUM_BOXES];

    /** Search radius to be used by free space detection logic. \n
      * Range: [0.0, inf)
      */
    float                       searchRadius;

    /** ID of grid on which FSD is to be performed.  \n
      * Allowed: {0, 1, 2, ..., \ref PTK_MAP_MAX_NUM_GRIDS_PER_MAP - 1}
      */
    uint32_t                    occGridId;

    /** Minimum yaw rate qualify as a curve. \n
      * Range: [0.0, 2*pi]
      */
    float                       yawRateThresh;

    /** Grid configuration. */
    PTK_GridConfig              gridConfig;

    /** Occupied flag in OG bitmap. \n
      * Allowed: {0x00000001, 0x00000002, 0x00000004, 0x00000008,
      *           0x00000010, 0x00000020, 0x00000080, 0x00000100}
      */
    uint32_t                    ogFlagOccupied;

    /** Free flag in OG bitmap. \n
      * Allowed: {0x00000001, 0x00000002, 0x00000004, 0x00000008,
      *           0x00000010, 0x00000020, 0x00000080, 0x00000100}
      */
    uint32_t                    ogFlagFree;

    /** Free space tested (FST) in OG bitmap. \n
      * Allowed: {0x00000001, 0x00000002, 0x00000004, 0x00000008,
      *           0x00000010, 0x00000020, 0x00000080, 0x00000100}
      */
    uint32_t                    ogFlagFst;

    /** Free space detected (FSD) flag in OG bitmap. \n
      * Allowed: {0x00000001, 0x00000002, 0x00000004, 0x00000008,
      *           0x00000010, 0x00000020, 0x00000080, 0x00000100}
      */
    uint32_t                    ogFlagFsd;

    /** Parking free space detected (PFSD) flag in OG bitmap. \n
      * Allowed: {0x00000001, 0x00000002, 0x00000004, 0x00000008,
      *           0x00000010, 0x00000020, 0x00000080, 0x00000100}
      */
    uint32_t                    ogFlagPfsd;

    /** Minimum ego displacement required to activate FSD. \n
      * Range: [0.0, inf)
      */
    float                       minEgoDisp4Fsd;

    /** X location the frame after last FSD performed. \n
      * Range: (-inf, inf)
      */
    float                       prevInsX4FSD;

    /** Y location the frame after last FSD performed. \n
      * Range: (-inf, inf)
      */
    float                       prevInsY4FSD;

    /** Indicator for new FSD cycle. \n
      * Allowed: {0,1}
      */
    uint32_t                    newFSDCycle;

    /** Flag indicating whether to clear (1) or not to clear (0)
      *  PFSD flag when revisiting a cell. \n
      * Allowed: {0,1}
      */
    uint32_t                    reCheckPFSD;

} PTK_Alg_FsdPfsdParams;

/**
 * \brief Park-able space coordinates (part of output).
 *        [park-able space: contiguous space larger than PFSD dimensions]  \n
 *
 * \ingroup group_ptk_algos_fsdpfsd
 */
typedef struct
{
    /** First x-coordinate. \n
      * Range: (-inf, inf)
      */
    float      x_1;

    /** Second x-coordinate. \n
      * Range: (-inf, inf)
      */
    float      x_2;

    /** First y-coordinate. \n
      * Range: (-inf, inf)
      */
    float      y_1;

    /** Second y-coordinate. \n
      * Range: (-inf, inf)
      */
    float      y_2;

    /** FSD box number. \n
      * Allowed: {0, 1, ..., \ref FSD_PFSD_MAX_PARKABLE_SPACES}
      */
    uint32_t   fsdBoxNum;

}PTK_Alg_FsdPfsdPSCoords;

/**
 * \brief Output structure that holds park-able free space coordinates.
 *
 * \ingroup group_ptk_algos_fsdpfsd
 */
typedef struct
{
    /** Number of park-able spaces. \n
      * Allowed: {0, 1, ..., \ref FSD_PFSD_MAX_PARKABLE_SPACES}
      */
    uint32_t                    numSpaces;

    /** Park-able space coordinates. */
    PTK_Alg_FsdPfsdPSCoords     psCoords[FSD_PFSD_MAX_PARKABLE_SPACES];

    /** Current INS record. */
    PTK_INS_Record              curInsRec;

}PTK_Alg_FsdPfsdPSDesc;

/**
* \brief Computes the memory requirements based on the given algorithm
*        configuration parameters.
*
* \param [in] cfgParams Algorithm configuration parameters.
*
* \param [out] memReq   Memory requirements as specified below
*                       - entry [0]: Memory for internal context. Best
*                         allocated from the fastest memory.
*
* \return
*        - PTK_ALG_RET_SUCCESS, if successful.
*        - < error code, otherwise.
*
* \ingroup group_ptk_algos_fsdpfsd
*/
int32_t PTK_Alg_FsdPfsdConfig(
    const PTK_Alg_FsdPfsdParams   * cfgParams,
    PTK_Api_MemoryReq             * memReq);

/**
* \brief Initializes the library based on the given algorithm configuration
*        parameters and allocated memory blocks.
*
* \param [in] cfgParams Algorithm configuration parameters. .
*
* \param [in] memRsp    Memory blocks as specified below
*                       - entry [0]: Memory for internal context.
*
* \return
*        - valid handle, if successful.
*        - NULL, otherwise.
*
* \ingroup group_ptk_algos_fsdpfsd
*/
PTK_AlgHandle PTK_Alg_FsdPfsdInit(
    const PTK_Alg_FsdPfsdParams   * cfgParams,
    const PTK_Api_MemoryRsp       * memRsp);

/**
 * \brief Processes the given occupancy map and performs the Free Space and
 *        Parking Free Space detection.
 *
 * \param [inout] algHandle  Algorithm handle.
 *
 * \param [in] posAndRef Current and previous INS records along with the
 *                       transform for ecef to world.
 *
 * \param [out] outMap   Output OG Map.
 *
 * \param [out] psDesc   Data structure that stores, number of parkable
 *                       space dimentions of spaces, and current INS record.
 *
 * \return
 *        - PTK_ALG_RET_SUCCESS, if successful.
 *        - < error code, otherwise.
 *
 * \ingroup group_ptk_algos_fsdpfsd
 */
int32_t PTK_Alg_FsdPfsdProcess(
    PTK_AlgHandle                   algHandle,
    const PTK_InsPoseAndRef       * posAndRef,
    PTK_Map                       * outMap,
    PTK_Alg_FsdPfsdPSDesc         * psDesc);

void PTK_Alg_FsdPfsdDeInit(PTK_AlgHandle   algHandle);

#ifdef __cplusplus
}
#endif

#endif
