/*
 *
 * Copyright (c) 2019 Texas Instruments Incorporated
 *
 * All rights reserved not granted herein.
 *
 * Limited License.
 *
 * Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
 * license under copyrights and patents it now or hereafter owns or controls to make,
 * have made, use, import, offer to sell and sell ("Utilize") this software subject to the
 * terms herein.  With respect to the foregoing patent license, such license is granted
 * solely to the extent that any such patent is necessary to Utilize the software alone.
 * The patent license shall not apply to any combinations which include this software,
 * other than combinations with devices manufactured by or for TI ("TI Devices").
 * No hardware patent is licensed hereunder.
 *
 * Redistributions must preserve existing copyright notices and reproduce this license
 * (including the above copyright notice and the disclaimer and (if applicable) source
 * code license limitations below) in the documentation and/or other materials provided
 * with the distribution
 *
 * Redistribution and use in binary form, without modification, are permitted provided
 * that the following conditions are met:
 *
 * *       No reverse engineering, decompilation, or disassembly of this software is
 * permitted with respect to any software provided in binary form.
 *
 * *       any redistribution and use are licensed by TI for use only with TI Devices.
 *
 * *       Nothing shall obligate TI to provide you with source code for the software
 * licensed and provided to you in object code.
 *
 * If software source code is provided to you, modification and redistribution of the
 * source code are permitted provided that the following conditions are met:
 *
 * *       any redistribution and use of the source code, including any resulting derivative
 * works, are licensed by TI for use only with TI Devices.
 *
 * *       any redistribution and use of any object code compiled from the source code
 * and any resulting derivative works, are licensed by TI for use only with TI Devices.
 *
 * Neither the name of Texas Instruments Incorporated nor the names of its suppliers
 *
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * DISCLAIMER.
 *
 * THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
#ifndef RADAR_GTRACK_H
#define RADAR_GTRACK_H

#include <perception/perception.h>
#include <perception/common/common_types.h>
#include <perception/algos/radar_data_types.h>
#include <perception/algos/radar_gtrack_common.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * \defgroup group_ptk_algos_radar_gtrack PTK RADAR Group Tracker
 * \ingroup group_ptk_algos
 *
 */

/**
 * \brief GTRACK Advanced Parameters
 *
 * \details The structure describes advanced configuration parameters.
 *
 * \ingroup group_ptk_algos_radar_gtrack
 */
typedef struct
{
    /**  \brief  Pointer to gating parameters. */
    GTRACK_gatingParams     gatingParams;

    /**  \brief  Pointer to allocation parameters. */
    GTRACK_allocationParams allocationParams;

    /**  \brief  Pointer to unrolling parameters. */
    GTRACK_unrollingParams  unrollingParams;

    /**  \brief  Pointer to tracking state parameters. */
    GTRACK_stateParams      stateParams;

    /**  \brief  Pointer to measurements variation parameters. */
    GTRACK_varParams        variationParams;

    /**  \brief  Pointer to scenery parameters. */
    GTRACK_sceneryParams    sceneryParams;

} PTK_Alg_RadarGTrackAdvParams;

/**
 * \brief GTRACK Configuration
 *
 * \details The structure describes the GTRACK algorithm configuration options.
 *
 * \ingroup group_ptk_algos_radar_gtrack
 */
typedef struct
{
    /**  \brief   Maximum Number of Measurement Points per frame. Up to
                  \ref GTRACK_NUM_POINTS_MAX supported.\n
      * Allowed: {0, 1, 2, ..., GTRACK_NUM_POINTS_MAX}
      */
    uint16_t                    maxNumPoints;

    /**  \brief   Maximum Number of Tracking Objects. Up to \ref
                  GTRACK_NUM_TRACKS_MAX supported. \n
      * Allowed: {0, 1, 2, ..., GTRACK_NUM_TRACKS_MAX}
      */
    uint16_t                    maxNumTracks;

    /**  \brief   Expected target radial velocity at the moment of detection, m/s. \n
      * Range: (-inf, inf)
      */
    float                       initialRadialVelocity;

    /**  \brief   Maximum radial velocity reported by sensor +/- m/s. \n
      * Range: (-inf, inf)
      */
    float                       maxRadialVelocity;

    /**  \brief   Radial Velocity resolution, m/s.\n
      * Range: (-inf, inf)
      */
    float                       radialVelocityResolution;

    /**  \brief   Maximum expected target acceleration in lateral (X), longitudinal (Y),
                  and vertical (Z) directions, m/s2. \n
      * Range: (-inf, inf)
      */
    float                       maxAcceleration[3];

    /**  \brief   Frame rate, ms \n
      * Range: [0, inf)
      */
    float                       deltaT;

    /**  \brief   Advanced parameters, set to NULL for defaults. */
    PTK_Alg_RadarGTrackAdvParams advParams;

} PTK_Alg_RadarGTrackParams;

/**
* \brief Data structure which holds tracks and ego-location for frame.
*
* \details The structure describes the GTRACK algorithm configuration options.
*
* \ingroup group_ptk_algos_radar_gtrack
*/
typedef struct
{
    /** Number of tracks. \n
      * Allowed: {0, 1, 2, ..., UINT16_MAX}
      */
    uint16_t            numDesc;

    /** Information about tracks i.e. State Vector, covariance etc. . */
    GTRACK_targetDesc   desc[GTRACK_NUM_TRACKS_MAX];

    /** Locatiton of ego-car. */
    PTK_Vector          egoLocation;

} PTK_Alg_RadarGTrackTargetInfo;

/**
 * \brief Initializes the library based on the given algorithm configuration
 *        parameters and allocated memory blocks.
 *
 * \param [in] cfgParams Algorithm configuration parameters. It is ecpected
 *                       that the same configuration parameters passed in the
 *                       PTK_Alg_RadarOgmapConfig() API are given. No check can
 *                       be made to make sure these are the same. Failure to
 *                       provide the same parameter set might result in an
 *                       unexpected behavior (ex:- memory requirements could
 *                       be different for different parameter set).
 *
 * \return
 *        - valid handle, if successful.
 *        - NULL, otherwise.
 *
 * \ingroup group_ptk_algos_radar_gtrack
 */
PTK_AlgHandle
PTK_Alg_RadarGTrackInit(const PTK_Alg_RadarGTrackParams * cfgParams);

/**
 * \brief Processes the given point cloud and performs Group Tracking.
 *
 * \param [inout] algHandle Algorithm handle.
 *
 * \param [in] sensorCfg Radar sensor configuration.
 *
 * \param [in] doaData Radar object data.
 *
 * \param [in] posAndRef Current and previous INS records along with the
 *                       transform for ecef to world.
 *
 * \param [out] targetInfo Data structure which holds tracks and
 *                         ego-location for frame.
 *
 * \return
 *        - PTK_ALG_RET_SUCCESS, if successful.
 *        - < error code, otherwise.
 *
 * \ingroup group_ptk_algos_radar_gtrack
 */
int PTK_Alg_RadarGTrackProcess(
    PTK_AlgHandle                     algHandle,
    const PTK_Alg_RadarSensorConfig * sensorCfg,
    const PTK_Alg_RadarDetOutput    * doaData,
    const PTK_InsPoseAndRef         * posAndRef,
    PTK_Alg_RadarGTrackTargetInfo   * targetInfo);


/**
 * \brief De-initializes the algorithm context.
 *
 * \param [inout] algHandle Algorithm handle.
 *
 * \ingroup group_ptk_algos_radar_gtrack
 */
void PTK_Alg_RadarGTrackDeInit(PTK_AlgHandle algHandle);

/* Implementations to be provided by the Application. */
/**
 *  \brief GTRACK calls this function to allocate memory. Expects the void pointer if
 *         allocation is sucessful, and NULL otherwise.
 *
 *  \param[in] numElements Number of elements to allocate.
 *
 *  \param[in] sizeInBytes Size of each element in bytes to allocate.
 *
 * \ingroup group_ptk_algos_radar_gtrack
 */
extern void *gtrack_alloc(uint32_t numElements, uint32_t sizeInBytes);

/**
 * \brief GTRACK calls this function to free memory
 *
 *  \param[in] pFree Pointer to a memmory to free.
 *
 *  \param[in] sizeInBytes Size of memory in bytes to free.
 *
 * \ingroup group_ptk_algos_radar_gtrack
 */
extern void gtrack_free(void *pFree, uint32_t sizeInBytes);

/**
 *  \brief GTRACK calls this function to log the events.
 *
 *  \param[in] level Level is the event importance.
 *
 *  \param[in] format Format is the variable size formated output.
 *
 * \ingroup group_ptk_algos_radar_gtrack
 */
extern void gtrack_log(GTRACK_VERBOSE_TYPE level, const char *format, ...);

#ifdef __cplusplus
}
#endif

#endif /* RADAR_GTRACK_H */
