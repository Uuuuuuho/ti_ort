/*
 *
 * Copyright (c) 2019 Texas Instruments Incorporated
 *
 * All rights reserved not granted herein.
 *
 * Limited License.
 *
 * Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
 * license under copyrights and patents it now or hereafter owns or controls to make,
 * have made, use, import, offer to sell and sell ("Utilize") this software subject to the
 * terms herein.  With respect to the foregoing patent license, such license is granted
 * solely to the extent that any such patent is necessary to Utilize the software alone.
 * The patent license shall not apply to any combinations which include this software,
 * other than combinations with devices manufactured by or for TI ("TI Devices").
 * No hardware patent is licensed hereunder.
 *
 * Redistributions must preserve existing copyright notices and reproduce this license
 * (including the above copyright notice and the disclaimer and (if applicable) source
 * code license limitations below) in the documentation and/or other materials provided
 * with the distribution
 *
 * Redistribution and use in binary form, without modification, are permitted provided
 * that the following conditions are met:
 *
 * *       No reverse engineering, decompilation, or disassembly of this software is
 * permitted with respect to any software provided in binary form.
 *
 * *       any redistribution and use are licensed by TI for use only with TI Devices.
 *
 * *       Nothing shall obligate TI to provide you with source code for the software
 * licensed and provided to you in object code.
 *
 * If software source code is provided to you, modification and redistribution of the
 * source code are permitted provided that the following conditions are met:
 *
 * *       any redistribution and use of the source code, including any resulting derivative
 * works, are licensed by TI for use only with TI Devices.
 *
 * *       any redistribution and use of any object code compiled from the source code
 * and any resulting derivative works, are licensed by TI for use only with TI Devices.
 *
 * Neither the name of Texas Instruments Incorporated nor the names of its suppliers
 *
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * DISCLAIMER.
 *
 * THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
#if !defined(_PTK_MEM_BITMAP_ALLOCATOR_H_)
#define _PTK_MEM_BITMAP_ALLOCATOR_H_

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

#define PTK_BITMAP_ALLOC_RET_SUCCESS                    0
#define PTK_BITMAP_ALLOC_RET_FAILURE                    (-1)
#define PTK_BITMAP_ALLOC_RET_INVALID_ARGS               (PTK_BITMAP_ALLOC_RET_FAILURE-2)
#define PTK_BITMAP_ALLOC_RET_CNTXT_MEM_ALLOC_FAILURE    (PTK_BITMAP_ALLOC_RET_FAILURE-3)
#define PTK_BITMAP_ALLOC_RET_MEM_ALLOC_FAILURE          (PTK_BITMAP_ALLOC_RET_FAILURE-4)
#define PTK_BITMAP_ALLOC_RET_ID_INVALID                 (PTK_BITMAP_ALLOC_RET_FAILURE-5)
#define PTK_BITMAP_ALLOC_RET_ID_DUPLICATE               (PTK_BITMAP_ALLOC_RET_FAILURE-6)

typedef struct
{
    /** Memory for tracking allocation. */
    uint32_t          * mem;

    /** Maximum number of bins valid. This is used to validate the
     *  ids during de-allocation.
     */
    uint32_t            maxNumBits;

    /** Maximum number of words allocated to accomodate "maxNumBits".
     *  This will be (maxNumBits + 31)/32.
     */
    uint32_t            numWords;

    /** Number of free bits left. This will be used for a quick check
     *  during allocation.
     */
    uint32_t            numFreeBits;

} PTK_MemBitmapAllocator;

/* Function prototypes. */
extern int32_t
PTK_memBitmapAllocatorInit(PTK_MemBitmapAllocator   * allocator,
                           uint32_t                   maxNumBits);

extern int32_t
PTK_memBitmapAllocatorAlloc(PTK_MemBitmapAllocator  * allocator);

extern int32_t
PTK_memBitmapAllocatorFree(PTK_MemBitmapAllocator   * allocator,
                           uint32_t                   id);

extern int32_t
PTK_memBitmapAllocatorDeInit(PTK_MemBitmapAllocator   * allocator);

#ifdef __cplusplus
}
#endif

#endif /* !defined(_PTK_MEM_BITMAP_ALLOCATOR_H_) */

