/*
 *
 * Copyright (c) 2019 Texas Instruments Incorporated
 *
 * All rights reserved not granted herein.
 *
 * Limited License.
 *
 * Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
 * license under copyrights and patents it now or hereafter owns or controls to make,
 * have made, use, import, offer to sell and sell ("Utilize") this software subject to the
 * terms herein.  With respect to the foregoing patent license, such license is granted
 * solely to the extent that any such patent is necessary to Utilize the software alone.
 * The patent license shall not apply to any combinations which include this software,
 * other than combinations with devices manufactured by or for TI ("TI Devices").
 * No hardware patent is licensed hereunder.
 *
 * Redistributions must preserve existing copyright notices and reproduce this license
 * (including the above copyright notice and the disclaimer and (if applicable) source
 * code license limitations below) in the documentation and/or other materials provided
 * with the distribution
 *
 * Redistribution and use in binary form, without modification, are permitted provided
 * that the following conditions are met:
 *
 * *       No reverse engineering, decompilation, or disassembly of this software is
 * permitted with respect to any software provided in binary form.
 *
 * *       any redistribution and use are licensed by TI for use only with TI Devices.
 *
 * *       Nothing shall obligate TI to provide you with source code for the software
 * licensed and provided to you in object code.
 *
 * If software source code is provided to you, modification and redistribution of the
 * source code are permitted provided that the following conditions are met:
 *
 * *       any redistribution and use of the source code, including any resulting derivative
 * works, are licensed by TI for use only with TI Devices.
 *
 * *       any redistribution and use of any object code compiled from the source code
 * and any resulting derivative works, are licensed by TI for use only with TI Devices.
 *
 * Neither the name of Texas Instruments Incorporated nor the names of its suppliers
 *
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * DISCLAIMER.
 *
 * THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
#if !defined(_PTK_IPC_CHAN_H_)
#define _PTK_IPC_CHAN_H_

#include <stdint.h>
#include <perception/utils/comm_chan.h>

#define PTK_IPC_BUFF_DESC_MAX_PAYLOAD_BUFFS (8U)

using namespace UTILS;

/* Forward declaration. */
struct PTK_IPC_BuffDesc;

using PTK_IpcChan = Channel<PTK_IPC_BuffDesc*>;
typedef void (*PTK_IPC_Cb)(PTK_IPC_BuffDesc *desc, void *appData);

namespace UTILS
{
    template<>
    class Channel<PTK_IPC_BuffDesc*>
    {
        private:
            // Mutex for protecting the operations on the free queue
            std::mutex      m_freeQMutex;

            // Mutex for protecting the operations on the destination queue
            std::mutex      m_dstQMutex;

            // Semaphore to indicate to the receiver that data is available
            Semaphore       m_dataAvailSem{0};

            // Semaphore to indicate to the sender that a free buffer is available
            Semaphore       m_freeBuffAvailSem{0};

            // Queue for holding the free buffers. The expected usage is as follows
            // SENDER   : Dequeues
            // REVEIVER : Enqueues
            std::queue<PTK_IPC_BuffDesc*>   m_freeQ;

            // Queue for holding the buffers for the receiver
            // SENDER   : Enqueus
            // REVEIVER : Dequeues
            std::queue<PTK_IPC_BuffDesc*>   m_dstQ;

        private:
            // Avoid copying
            Channel(const Channel &q) = delete;
            Channel& operator=(const Channel &q) = delete;

        public:
            Channel(){}

            // Non-blocking function for sending a buffer
            int32_t send(PTK_IPC_BuffDesc*  buff);

            // Blocking call for receiving a buffer
            PTK_IPC_BuffDesc* receive();

            // Non-Blocking call for receiving a buffer
            PTK_IPC_BuffDesc* nb_receive();

            // Allocates a buffer from the free queue
            PTK_IPC_BuffDesc* alloc();

            // Non-blockting free buffer allocator
            PTK_IPC_BuffDesc* nb_alloc();

            // Releases the buffer back to the free queue
            int32_t free(PTK_IPC_BuffDesc*  desc);

            void flush();

            ~Channel()
            {
                flush();

            } // ~Channel()

    }; // class Channel

} // namespace UTILS

/**
 * \brief PTK Inter Process Communication descriptor.
 *
 * \ingroup group_ptk_utils
 */
typedef struct PTK_IPC_BuffDesc
{
    /** Channel identifier. Not used by the library. To be used by the
     *  Application.
     */
    uint8_t         chanId;

    /** Reserved for future use. */
    uint8_t         rsvd0;

    /** Reserved for future use. */
    uint8_t         rsvd1;

    /** Number of valid payload pointers. */
    uint8_t         numBuffs;

    /** Total size of the payload. This should not exceed the sum of the sizes
     *  in the payloadSize[] array.
     */
    uint32_t        totalPayloadSize;

    /** Payload size information. */
    uint32_t        payloadSize[PTK_IPC_BUFF_DESC_MAX_PAYLOAD_BUFFS];

    /** Payload pointer. */
    uint8_t       * payload[PTK_IPC_BUFF_DESC_MAX_PAYLOAD_BUFFS];

    /** Application specific data. This will not be looked at by the
     *  library. The Application could use it attach any object for
     *  its tracking purposes.
     */
    void          * appData;

    /** Return channel for freeing the descriptor. */
    PTK_IpcChan   * retChan;

} PTK_IPC_BuffDesc;

#ifdef __cplusplus
extern "C" {
#endif

/**
 * \brief Releases the descriptor back to the origination i.e. the free queue
 *        where the descriptor was allocated from.
 *
 *        The following fields need will be looked at. An error will be
 *        returned if this field is not set.
 *        retChan
 *
 * \param [in] desc Descriptor to hold capture data.
 *
 * \return
 *        - 0, if successful.
 *        - < 0, otherwise.
 *
 * \ingroup group_ptk_utils
 */
int32_t PTK_IPC_BuffDescRelease(PTK_IPC_BuffDesc  * desc);

#ifdef __cplusplus
}
#endif

#endif //!defined(_PTK_IPC_CHAN_H_)

