/*
 *
 * Copyright (c) 2019 Texas Instruments Incorporated
 *
 * All rights reserved not granted herein.
 *
 * Limited License.
 *
 * Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
 * license under copyrights and patents it now or hereafter owns or controls to make,
 * have made, use, import, offer to sell and sell ("Utilize") this software subject to the
 * terms herein.  With respect to the foregoing patent license, such license is granted
 * solely to the extent that any such patent is necessary to Utilize the software alone.
 * The patent license shall not apply to any combinations which include this software,
 * other than combinations with devices manufactured by or for TI ("TI Devices").
 * No hardware patent is licensed hereunder.
 *
 * Redistributions must preserve existing copyright notices and reproduce this license
 * (including the above copyright notice and the disclaimer and (if applicable) source
 * code license limitations below) in the documentation and/or other materials provided
 * with the distribution
 *
 * Redistribution and use in binary form, without modification, are permitted provided
 * that the following conditions are met:
 *
 * *       No reverse engineering, decompilation, or disassembly of this software is
 * permitted with respect to any software provided in binary form.
 *
 * *       any redistribution and use are licensed by TI for use only with TI Devices.
 *
 * *       Nothing shall obligate TI to provide you with source code for the software
 * licensed and provided to you in object code.
 *
 * If software source code is provided to you, modification and redistribution of the
 * source code are permitted provided that the following conditions are met:
 *
 * *       any redistribution and use of the source code, including any resulting derivative
 * works, are licensed by TI for use only with TI Devices.
 *
 * *       any redistribution and use of any object code compiled from the source code
 * and any resulting derivative works, are licensed by TI for use only with TI Devices.
 *
 * Neither the name of Texas Instruments Incorporated nor the names of its suppliers
 *
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * DISCLAIMER.
 *
 * THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
#if !defined(_PTK_COMMUNICATION_CHANNEL_H_)
#define _PTK_COMMUNICATION_CHANNEL_H_

#include <iostream>
#include <queue>
#include <cassert>
#include <stdint.h>

#include <perception/utils/ptk_semaphore.h>

namespace UTILS
{
    template<typename T>
    class Channel
    {
        private:
            // Mutex for protecting the operations on the free queue
            std::mutex      m_freeQMutex;

            // Mutex for protecting the operations on the destination queue
            std::mutex      m_dstQMutex;

            // Semaphore to indicate to the receiver that data is available
            Semaphore       m_dataAvailSem{0};

            // Semaphore to indicate to the sender that a free buffer is available
            Semaphore       m_freeBuffAvailSem{0};

            // Queue for holding the free buffers. The expected usage is as follows
            // SENDER   : Dequeues
            // REVEIVER : Enqueues
            std::queue<T>   m_freeQ;

            // Queue for holding the buffers for the receiver
            // SENDER   : Enqueus
            // REVEIVER : Dequeues
            std::queue<T>   m_dstQ;

        private:
            // Avoid copying
            Channel(const Channel &q) = delete;
            Channel& operator=(const Channel &q) = delete;

        public:
            Channel()
            {
            } // Channel()

            // Non-blocking function for sending a buffer
            int32_t send(T  buff)
            {
                // Acquire Mutex
                std::unique_lock<std::mutex> lock(m_dstQMutex);

                // Enqueue the buffer to the destination queue
                m_dstQ.push(buff);

                // Notify the receiver
                m_dataAvailSem.notify();

                return ( 0 );

            } // send()

            // Blocking call for receiving a buffer
            T receive()
            {
                T   buff{nullptr};

                // Block on the message availability
                m_dataAvailSem.wait();

                // Acquire Mutex
                std::unique_lock<std::mutex> lock(m_dstQMutex);

                // It is possible that the receiver could consume multiple
                // buffers per single event in which case the subsequent calls
                // would result in a successful Semaphore get() but the queue
                // could be empty.
                if ( !m_dstQ.empty() )
                {
                    // Dequeue the destination queue
                    buff = m_dstQ.front();

                    m_dstQ.pop();
                }

                return ( buff );

            } // receive()

            // Non-Blocking call for receiving a buffer
            T nb_receive()
            {
                T   buff{nullptr};

                // Acquire Mutex
                std::unique_lock<std::mutex> lock(m_dstQMutex);

                // It is possible that the receiver could consume multiple
                // buffers per single event in which case the subsequent calls
                // would result in a successful Semaphore get() but the queue
                // could be empty.
                if ( !m_dstQ.empty() )
                {
                    // Dequeue the destination queue
                    buff = m_dstQ.front();

                    m_dstQ.pop();
                }

                return ( buff );

            } // receive()

            // Allocates a buffer from the free queue
            T alloc()
            {
                T   buff{nullptr};

                // Block on the message free buffer availability
                m_freeBuffAvailSem.wait();

                // Acquire Mutex
                std::unique_lock<std::mutex> lock(m_freeQMutex);

                if ( !m_freeQ.empty() )
                {
                    buff = m_freeQ.front();

                    m_freeQ.pop();
                }

                return ( buff );

            } // alloc()

            // Non-blockting free buffer allocator
            T nb_alloc()
            {
                T   buff{nullptr};

                // Acquire Mutex
                std::unique_lock<std::mutex> lock(m_freeQMutex);

                if ( !m_freeQ.empty() )
                {
                    buff = m_freeQ.front();

                    m_freeQ.pop();
                }

                return ( buff );

            } // alloc()

            // Releases the buffer back to the free queue
            int32_t free(T  buff)
            {
                // Acquire Mutex
                std::unique_lock<std::mutex> lock(m_freeQMutex);

                m_freeQ.push(buff);

                // Notify the sender
                m_freeBuffAvailSem.notify();

                return ( 0 );

            } // free()

            void flush()
            {
                // Release the allocated buffers
                std::unique_lock<std::mutex> lock1(m_freeQMutex);

                while ( !m_freeQ.empty() )
                {
                    m_freeQ.pop();
                }

                std::unique_lock<std::mutex> lock2(m_dstQMutex);

                while ( !m_dstQ.empty() )
                {
                    m_dstQ.pop();
                }
            }

            ~Channel()
            {
                flush();

            } // ~Channel()

    }; // class Channel

} // namespace UTILS

#endif // !defined(_PTK_COMMUNICATION_CHANNEL_H_)

