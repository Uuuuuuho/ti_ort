/*
*
* Copyright (c) 2018 Texas Instruments Incorporated
*
* All rights reserved not granted herein.
*
* Limited License.
*
* Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
* license under copyrights and patents it now or hereafter owns or controls to make,
* have made, use, import, offer to sell and sell ("Utilize") this software subject to the
* terms herein.  With respect to the foregoing patent license, such license is granted
* solely to the extent that any such patent is necessary to Utilize the software alone.
* The patent license shall not apply to any combinations which include this software,
* other than combinations with devices manufactured by or for TI ("TI Devices").
* No hardware patent is licensed hereunder.
*
* Redistributions must preserve existing copyright notices and reproduce this license
* (including the above copyright notice and the disclaimer and (if applicable) source
* code license limitations below) in the documentation and/or other materials provided
* with the distribution
*
* Redistribution and use in binary form, without modification, are permitted provided
* that the following conditions are met:
*
* *       No reverse engineering, decompilation, or disassembly of this software is
* permitted with respect to any software provided in binary form.
*
* *       any redistribution and use are licensed by TI for use only with TI Devices.
*
* *       Nothing shall obligate TI to provide you with source code for the software
* licensed and provided to you in object code.
*
* If software source code is provided to you, modification and redistribution of the
* source code are permitted provided that the following conditions are met:
*
* *       any redistribution and use of the source code, including any resulting derivative
* works, are licensed by TI for use only with TI Devices.
*
* *       any redistribution and use of any object code compiled from the source code
* and any resulting derivative works, are licensed by TI for use only with TI Devices.
*
* Neither the name of Texas Instruments Incorporated nor the names of its suppliers
*
* may be used to endorse or promote products derived from this software without
* specific prior written permission.
*
* DISCLAIMER.
*
* THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
* OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
* OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
* IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
* BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
* OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
* OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
* OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/
#ifndef _CALMAT_TOOLS_H_
#define _CALMAT_TOOLS_H_

#include <stdint.h>
#include <perception/base/rigidTransform.h>

#ifdef __cplusplus
extern "C" {
#endif

/*maximum number of cameras supported by calmat*/
#define CALMAT_MAX_NUM_CAMS (4)

// Calibration table precision
//(format is int32 with 1 bit sign)
#define CALMAT_R_SHIFT 30 // Rotation Matrix in [R|T] Q1.30
#define CALMAT_T_SHIFT 10 // Translation Matrix in [R|T] Q21.10
#define CALMAT_H_SHIFT 20 // Perspective matrix precision H Q11.20

/**
 * extract transform from ground (chart reference frame) to one specified camera
 * @param[in]  calmat       pointer to calmat array
 * @param[in]  camId        camera id for which to extract the transform
 * @param[out] M_cam_ground pointer to rigid transform where output is written (translation in [mm])
 * */
void PTK_Util_GroundToCamera(int32_t              * calmat,
                             uint32_t               camId,
                             PTK_RigidTransform   * M_cam_ground);

/**
 * extract transform from one camera 1 to another camera 2
 * @param[in]  calmat     pointer to calmat array
 * @param[in]  camId1     camera id for camera 1
 * @param[in]  camId2     camera id for camera 2
 * @param[out] M_ca2_cam1 pointer to rigid transform where output is written (translation in [mm])
 * */
void PTK_Util_CameraToCamera(int32_t              * calmat,
                             uint32_t               camId1,
                             uint32_t               camId2,
                             PTK_RigidTransform   * M_cam2_cam1);

/**
 * Extract transforms from the "xy-centroid" reference frame to each camera.
 * The "xy-centroid" reference frame is defined as the ground (chart reference frame) shifted
 * in x and y by the 2-dimensional x-y-centroid of all camera centers. z-coordinates are not
 * considered in the centroid calculation.
 * @param[in]  calmat       pointer to calmat array
 * @param[in]  camId        camera id for which to extract the transform
 * @param[out] M_cam_ground pointer to array of rigid transform where output is written (translations in [mm])
 * */
void PTK_Util_CamerasToCentroidXY(int32_t             * calmat,
                                  uint32_t              numCams,
                                  PTK_RigidTransform    M_center_cam[CALMAT_MAX_NUM_CAMS]);

#ifdef __cplusplus
}
#endif

#endif
