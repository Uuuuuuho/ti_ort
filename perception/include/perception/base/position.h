/*
 *
 * Copyright (c) 2018 Texas Instruments Incorporated
 *
 * All rights reserved not granted herein.
 *
 * Limited License.
 *
 * Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
 * license under copyrights and patents it now or hereafter owns or controls to make,
 * have made, use, import, offer to sell and sell ("Utilize") this software subject to the
 * terms herein.  With respect to the foregoing patent license, such license is granted
 * solely to the extent that any such patent is necessary to Utilize the software alone.
 * The patent license shall not apply to any combinations which include this software,
 * other than combinations with devices manufactured by or for TI ("TI Devices").
 * No hardware patent is licensed hereunder.
 *
 * Redistributions must preserve existing copyright notices and reproduce this license
 * (including the above copyright notice and the disclaimer and (if applicable) source
 * code license limitations below) in the documentation and/or other materials provided
 * with the distribution
 *
 * Redistribution and use in binary form, without modification, are permitted provided
 * that the following conditions are met:
 *
 * *       No reverse engineering, decompilation, or disassembly of this software is
 * permitted with respect to any software provided in binary form.
 *
 * *       any redistribution and use are licensed by TI for use only with TI Devices.
 *
 * *       Nothing shall obligate TI to provide you with source code for the software
 * licensed and provided to you in object code.
 *
 * If software source code is provided to you, modification and redistribution of the
 * source code are permitted provided that the following conditions are met:
 *
 * *       any redistribution and use of the source code, including any resulting derivative
 * works, are licensed by TI for use only with TI Devices.
 *
 * *       any redistribution and use of any object code compiled from the source code
 * and any resulting derivative works, are licensed by TI for use only with TI Devices.
 *
 * Neither the name of Texas Instruments Incorporated nor the names of its suppliers
 *
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * DISCLAIMER.
 *
 * THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
#pragma once
#ifndef PTK_POSITION_H
#define PTK_POSITION_H

#include <stdint.h>

#include <perception/base/point.h>
#include <perception/base/rigidTransform.h>

/**
 * \defgroup group_ptk_position PTK Position
 * \ingroup group_ptk_positioning
 *
 * All positions are represented by a single struct, PTK_Position, which has a
 * fixed memory layout and size and can be initialized from latitude, longitude,
 * altitude (LLA) but not Earth-centered, Earth-fixed (ECEF) position data. From
 * a PTK_Position structure it is possible to compute the transformation matrix
 * between the local East-North-Up (ENU) coordinate system and the global ECEF
 * coordinate system and vice versa. It is also possible to compute the
 * transformation between any two PTK_Position structs in any ENU frame or in
 * ECEF.
 */

#ifdef __cplusplus
extern "C" {
#endif

/**
 * \brief WGS84 Ellipsoid constant
 *
 * \ingroup group_ptk_position
 */
#define WGS84_ELLIPSOID_A 6378137.0

/**
 * \brief WGS84 Ellipsoid constant
 *
 * \ingroup group_ptk_position
 */
#define WGS84_ELLIPSOID_F (1.0/298.258223563)

typedef struct
{
    /* Actual gps coordinates in WGS84 geodetic datum. */

    /** Latitude (WGS84) in radians. */
    double lat;

    /** Longitude (WGS84) in radians. */
    double lon;

    /** Ellipsoidal Height (WGS84) in meters. */
    double alt;

    /* Memoization/acceleration for sin/cos reuse. */

    /** Flag for tracking the sin/cos initialization. */
    uint32_t scInit;

    /** sine of the latitude. */
    double slat;

    /** cosine of the latitude. */
    double clat;

    /** sine of the longitude. */
    double slon;

    /** cosine of the longitude. */
    double clon;

    /* Memoization/acceleration for ecef calculation. */

    /** Flag for tracking the ECEF initialization. */
    uint32_t ecefInit;

    /** X-coordinate in ECEF coordinate frame in meters. */
    double x;

    /** Y-coordinate in ECEF coordinate frame in meters. */
    double y;

    /** Z-coordinate in ECEF coordinate frame in meters. */
    double z;

} PTK_Position;

/**
 * \brief Initializes the lat, lon, and alt fields of the position object with
 *        lat, lon, and alt values respectively. It also resets the sin/cos and
 *        ECEF field initialization status flags within the position object.
 *
 * \param [out] position object to update the lat, lon, and alt fields within.
 *
 * \param [in] lat Latitude (WGS84) in radians.
 *
 * \param [in] lon Longitude (WGS84) in radians.
 *
 * \param [in] alt Height (WGS84) in meters.
 *
 * \ingroup group_ptk_position
 */
void PTK_Position_initLLA(PTK_Position *position, double lat, double lon, double alt);

/**
 * \brief Reads the latitude, longitude, and height information from the
 *        position object.
 *
 * \param [in] position Position object to read the information from.
 *
 * \param [out] lat Latitude (WGS84) in radians.
 *
 * \param [out] lon Longitude (WGS84) in radians.
 *
 * \param [out] alt Height (WGS84) in meters.
 *
 * \ingroup group_ptk_position
 */
void PTK_Position_getLLA(const PTK_Position *position, double *lat, double *lon, double *alt);

/**
 * \brief Reads the ECEF 3D position coordinates (i.e. X, Y, and Z) from the
 *        position object. Based on the current state of the position object
 *        the state will be updated as follows and the resulting ECEF
 *        coordinates will returned via the point object.
 *        - If the position object has been initialized with the LLA
 *          information only then this call will compute the ECEF coordinates
 *          from the LLA information and the ECEF Initialization flag will be
 *          set.
 *        - If the ECEF initialization flag is already set, then the ECEF
 *          computation from the LLA information is skipped.
 *
 * \param [inout] position Position object to read the information from.
 *
 * \param [out] pt Point object the ECEF 3D coordinates are copied to.
 *
 * \ingroup group_ptk_position
 */
void PTK_Position_getECEF_d(PTK_Position *position, PTK_Point_d *pt);

/**
 * \brief Reads the ECEF 3D position coordinates (i.e. X, Y, and Z) from the
 *        position object. Based on the current state of the position object
 *        the state will be updated as follows and the resulting ECEF
 *        coordinates will returned via the point object.
 *        - If the position object has been initialized with the LLA
 *          information only then this call will compute the ECEF coordinates
 *          from the LLA information and the ECEF Initialization flag will be
 *          set.
 *        - If the ECEF initialization flag is already set, then the ECEF
 *          computation from the LLA information is skipped.
 *
 * \param [inout] position Position object to read the information from.
 *
 * \param [out] pt Point object the ECEF 3D coordinates are copied to.
 *
 * \ingroup group_ptk_position
 */
void PTK_Position_getECEF(PTK_Position *position, PTK_Point *pt);

/**
 * \brief Returns the matrix for transformation from the ENU coordinate system
 *        to the ECEF coordinate system. Based on the current state of the
 *        position object the state will be updated as follows and the
 *        resulting transformation matrix is returned.
 *        - If the position object has been initialized with the LLA
 *          information only then this call will compute the ECEF coordinates
 *          from the LLA information and the ECEF Initialization flag will be
 *          set.
 *        - If the ECEF initialization flag is already set, then the ECEF
 *          computation from the LLA information is skipped.
 *        - The rotation and translation information is computed and copied
 *          to the matrix object.
 *
 * \param [inout] position Position object to read the information from.
 *
 * \param [out] M_ecef_enu Resulting ENU to ECEF coordinate transformation
 *              matrix.
 *
 * \ingroup group_ptk_position
 */
void PTK_Position_getENUtoECEF_d(PTK_Position *position, PTK_RigidTransform_d *M_ecef_enu);

/**
 * \brief Returns the matrix for transformation from the ENU coordinate system
 *        to the ECEF coordinate system. Based on the current state of the
 *        position object the state will be updated as follows and the
 *        resulting transformation matrix is returned.
 *        - If the position object has been initialized with the LLA
 *          information only then this call will compute the ECEF coordinates
 *          from the LLA information and the ECEF Initialization flag will be
 *          set.
 *        - If the ECEF initialization flag is already set, then the ECEF
 *          computation from the LLA information is skipped.
 *        - The rotation and translation information is computed and copied
 *          to the matrix object.
 *
 * \param [inout] position Position object to read the information from.
 *
 * \param [out] M_ecef_enu Resulting ENU to ECEF coordinate transformation
 *              matrix.
 *
 * \ingroup group_ptk_position
 */
void PTK_Position_getENUtoECEF(PTK_Position *position, PTK_RigidTransform *M_ecef_enu);

/**
 * \brief Returns the matrix for transformation from the ECEF coordinate system
 *        to the ENU coordinate system. Based on the current state of the
 *        position object the state will be updated as follows and the
 *        resulting transformation matrix is returned.
 *        - If the position object has been initialized with the LLA
 *          information only then this call will compute the ECEF coordinates
 *          from the LLA information and the ECEF Initialization flag will be
 *          set.
 *        - If the ECEF initialization flag is already set, then the ECEF
 *          computation from the LLA information is skipped.
 *        - The rotation and translation information is computed and copied
 *          to the matrix object.
 *
 * \param [inout] position Position object to read the information from.
 *
 * \param [out] M_enu_ecef Resulting ECEF to ENU coordinate transformation
 *              matrix.
 *
 * \ingroup group_ptk_position
 */
void PTK_Position_getECEFtoENU_d(PTK_Position *position, PTK_RigidTransform_d *M_enu_ecef);

/**
 * \brief Returns the matrix for transformation from the ECEF coordinate system
 *        to the ENU coordinate system. Based on the current state of the
 *        position object the state will be updated as follows and the
 *        resulting transformation matrix is returned.
 *        - If the position object has been initialized with the LLA
 *          information only then this call will compute the ECEF coordinates
 *          from the LLA information and the ECEF Initialization flag will be
 *          set.
 *        - If the ECEF initialization flag is already set, then the ECEF
 *          computation from the LLA information is skipped.
 *        - The rotation and translation information is computed and copied
 *          to the matrix object.
 *
 * \param [inout] position Position object to read the information from.
 *
 * \param [out] M_enu_ecef Resulting ECEF to ENU coordinate transformation
 *              matrix.
 *
 * \ingroup group_ptk_position
 */
void PTK_Position_getECEFtoENU(PTK_Position *position, PTK_RigidTransform *M_enu_ecef);

/**
 * \brief Returns a matrix to transform from the coordinate frame defined by the
 *        start position to the coordinate frame defined by finish position.
 *
 * \param [out] start Position defining the source coordinate frame.
 *
 * \param [out] finish Position defining the destination frame.
 *
 * \param [out] M_finish_start Matrix to transform from the source coordinate
 *                             frame to the finish coordinate frame. 
 *
 * \ingroup group_ptk_position
 */
void PTK_Position_getENUtoENU(PTK_Position *start, PTK_Position *finish, PTK_RigidTransform *M_finish_start);

#ifdef __cplusplus
}
#endif

#endif
