/*
 *
 * Copyright (c) 2018 Texas Instruments Incorporated
 *
 * All rights reserved not granted herein.
 *
 * Limited License.
 *
 * Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
 * license under copyrights and patents it now or hereafter owns or controls to make,
 * have made, use, import, offer to sell and sell ("Utilize") this software subject to the
 * terms herein.  With respect to the foregoing patent license, such license is granted
 * solely to the extent that any such patent is necessary to Utilize the software alone.
 * The patent license shall not apply to any combinations which include this software,
 * other than combinations with devices manufactured by or for TI ("TI Devices").
 * No hardware patent is licensed hereunder.
 *
 * Redistributions must preserve existing copyright notices and reproduce this license
 * (including the above copyright notice and the disclaimer and (if applicable) source
 * code license limitations below) in the documentation and/or other materials provided
 * with the distribution
 *
 * Redistribution and use in binary form, without modification, are permitted provided
 * that the following conditions are met:
 *
 * *       No reverse engineering, decompilation, or disassembly of this software is
 * permitted with respect to any software provided in binary form.
 *
 * *       any redistribution and use are licensed by TI for use only with TI Devices.
 *
 * *       Nothing shall obligate TI to provide you with source code for the software
 * licensed and provided to you in object code.
 *
 * If software source code is provided to you, modification and redistribution of the
 * source code are permitted provided that the following conditions are met:
 *
 * *       any redistribution and use of the source code, including any resulting derivative
 * works, are licensed by TI for use only with TI Devices.
 *
 * *       any redistribution and use of any object code compiled from the source code
 * and any resulting derivative works, are licensed by TI for use only with TI Devices.
 *
 * Neither the name of Texas Instruments Incorporated nor the names of its suppliers
 *
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * DISCLAIMER.
 *
 * THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
#pragma once
#ifndef PTK_BENCHMARK_H
#define PTK_BENCHMARK_H

/**
 * \defgroup group_ptk_benchmark PTK Benchmark APIs
 * \ingroup group_ptk_base
 *
 * A simple benchmarking system is provided to facilitate measuring how
 * long things take to run in real time (timer mode) and measuring how many
 * times things are done (counter mode), both done as an average over some
 * pre-specified number of trials. Timer mode and counter mode benchmarks
 * should not be mixed in the same benchmark data structure. The benchmark
 * data structure is a complete opaque type of fixed size, so
 * sizeof(struct ptk benchmark t) should be used if necessary but no member
 * fields should be accessed directly. There is no configuration structure
 * associated with a struct ptk benchmark t; all configuration is supplied
 * directly to the initializer or already set during PTK initialization.
 */

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

#define PTK_BENCHMARK_MAX_RUNS 64

typedef struct ptk_benchmark_t
{
    /** Identification information. */
    const char                * name;

    /** */
    uint64_t                    runs[PTK_BENCHMARK_MAX_RUNS];

    /** */
    uint64_t                    start[PTK_BENCHMARK_MAX_RUNS];

    /** */
    uint64_t                    stop[PTK_BENCHMARK_MAX_RUNS];

    /** */
    uint64_t                    min;

    /** */
    uint64_t                    max;

    /** */
    uint64_t                    average;

    // Bookkeeping information

    /** */
    uint32_t                    nextRun;

    /** */
    uint32_t                    totalRuns;

    // Internal linked list for readout, do not modify

    /** */
    struct ptk_benchmark_t    * nextBenchmark;

} PTK_Benchmark;

/**
 * \brief Populate and initialize a benchmark structure for use with the given
 *        name and number of trials.
 *
 * \param [inout] b Benchmark context to initialize.
 *
 * \param [in] name Name of the benchmark for display.
 *
 * \param [in] runs Number of trial runs to be tracked and averaged.
 *
 * \ingroup group_ptk_benchmark
 */
void PTK_Benchmark_init(PTK_Benchmark *b, const char *name, uint32_t runs);

/**
 * \brief Start a timer-mode benchmark in the given structure.
 *
 * \param [in] b Benchmark context.
 *
 * \ingroup group_ptk_benchmark
 */
void PTK_Benchmark_startTimer(PTK_Benchmark *b);

/**
 * \brief Stop a currently running timer-mode benchmark and save how long it
 *        took. Do not use this with counter-mode benchmarks.
 *
 * \param [in] b Benchmark context.
 *
 * \ingroup group_ptk_benchmark
 */
void PTK_Benchmark_stopTimer(PTK_Benchmark *b);

/**
 * \brief This starts the given benchmark in counter mode.
 *
 * \param [in] b Benchmark context.
 *
 * \ingroup group_ptk_benchmark
 */
void PTK_Benchmark_startCounter(PTK_Benchmark *b);

/**
 * \brief This increments the counter associated with the given benchmark.
 *        Bench must have been started in counter mode, or the results are
 *        undefined.
 *
 * \param [in] b Benchmark context.
 *
 * \ingroup group_ptk_benchmark
 */
void PTK_Benchmark_incrementCounter(PTK_Benchmark *b);

/**
 * \brief This increments the counter associated with the given benchmark by
 *        the given number. Useful for aggregating multiple individual calls
 *        to increment if operations are done a known multiple number of times.
 *
 * \param [in] b Benchmark context.
 *
 * \param [in] n Amount to increment.
 *
 * \ingroup group_ptk_benchmark
 */
void PTK_Benchmark_addN(PTK_Benchmark *b, uint32_t n);

/**
 * \brief This stops the given, previously-started, counter-mode benchmark and
 *        records the accumulated count as a trial.
 *
 * \param [in] b Benchmark context.
 *
 * \ingroup group_ptk_benchmark
 */
void PTK_Benchmark_stopCounter(PTK_Benchmark *b);

/**
 * \brief This prints the information for a benchmark using the general printf
 *        function provided to PTK during initialization. This will include the
 *        min/avg/max time among all trials.
 *
 * \param [in] b Benchmark context.
 *
 * \ingroup group_ptk_benchmark
 */
void PTK_Benchmark_show(PTK_Benchmark *b);

/**
 * \brief This prints all benchmarks that were initialized, in the current
 *        process, as if PTK Benchmark show() were called on each individually.
 *
 * \ingroup group_ptk_benchmark
 */
void PTK_Benchmark_showAll();

#ifdef __cplusplus
}
#endif

#endif
