/*
 *
 * Copyright (c) 2018 Texas Instruments Incorporated
 *
 * All rights reserved not granted herein.
 *
 * Limited License.
 *
 * Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
 * license under copyrights and patents it now or hereafter owns or controls to make,
 * have made, use, import, offer to sell and sell ("Utilize") this software subject to the
 * terms herein.  With respect to the foregoing patent license, such license is granted
 * solely to the extent that any such patent is necessary to Utilize the software alone.
 * The patent license shall not apply to any combinations which include this software,
 * other than combinations with devices manufactured by or for TI ("TI Devices").
 * No hardware patent is licensed hereunder.
 *
 * Redistributions must preserve existing copyright notices and reproduce this license
 * (including the above copyright notice and the disclaimer and (if applicable) source
 * code license limitations below) in the documentation and/or other materials provided
 * with the distribution
 *
 * Redistribution and use in binary form, without modification, are permitted provided
 * that the following conditions are met:
 *
 * *       No reverse engineering, decompilation, or disassembly of this software is
 * permitted with respect to any software provided in binary form.
 *
 * *       any redistribution and use are licensed by TI for use only with TI Devices.
 *
 * *       Nothing shall obligate TI to provide you with source code for the software
 * licensed and provided to you in object code.
 *
 * If software source code is provided to you, modification and redistribution of the
 * source code are permitted provided that the following conditions are met:
 *
 * *       any redistribution and use of the source code, including any resulting derivative
 * works, are licensed by TI for use only with TI Devices.
 *
 * *       any redistribution and use of any object code compiled from the source code
 * and any resulting derivative works, are licensed by TI for use only with TI Devices.
 *
 * Neither the name of Texas Instruments Incorporated nor the names of its suppliers
 *
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * DISCLAIMER.
 *
 * THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
#pragma once
#ifndef PTK_RENDERER_H
#define PTK_RENDERER_H

/**
 * @file Renderer.h
 * @brief This provides the interface for a rendering engine, which handles drawing things onscreen
 */

#include <string>
#include <functional>
#include <map>
#include <list>
#include <vector>
#include <chrono>
#include <thread>

#include <perception/gui/GLlibs.h>

#if defined(PLATFORM_SDL)
#ifdef LINUX
#	include <SDL2/SDL.h>
#	include <SDL2/SDL_image.h>
#else
#	include <SDL.h>
#	include <SDL_image.h>
#endif
#define RENDERER_PLATFORM_WINDOW_TYPE  SDL_Window*
#define RENDERER_PLATFORM_CONTEXT_TYPE SDL_GLContext
#define RENDERER_PLATFORM_EVENT_TYPE   SDL_EventType
#elif defined(PLATFORM_EGL)
#include <perception/utils/opengl_utils.h>
#define RENDERER_PLATFORM_WINDOW_TYPE EGLSurface
#define RENDERER_PLATFORM_CONTEXT_TYPE uint32_t
#define RENDERER_PLATFORM_EVENT_TYPE   uint32_t
#elif defined(PLATFORM_X11)
#include "X11/Xlib.h"
#include "X11/Xutil.h"
#define RENDERER_PLATFORM_WINDOW_TYPE EGLSurface
#define RENDERER_PLATFORM_CONTEXT_TYPE uint32_t
#define RENDERER_PLATFORM_EVENT_TYPE   uint32_t
#elif defined(PLATFORM_WAYLAND)
#include <wayland-client.h>
#include <wayland-server.h>
#include <wayland-egl.h>
#include <linux/input.h>
#define RENDERER_PLATFORM_WINDOW_TYPE EGLSurface
#define RENDERER_PLATFORM_CONTEXT_TYPE uint32_t
#define RENDERER_PLATFORM_EVENT_TYPE   uint32_t
#elif defined(PLATFORM_GLFW)
#include <GLFW/glfw3.h>
#define RENDERER_PLATFORM_WINDOW_TYPE  GLFWwindow*
#define RENDERER_PLATFORM_CONTEXT_TYPE uint32_t
#define RENDERER_PLATFORM_EVENT_TYPE   uint32_t
#else
#error "Must define PLATFORM_SDL, PLATFORM_GLFW, PLATFORM_X11 or PLATFORM_WAYLAND"
#endif

#if !defined(PLATFORM_SDL)
/* TODO: Ideally, we should use DevIL for all platforms
         need to check Windows compatibility though
*/
#include <IL/il.h>
#endif

#include <glm/glm.hpp>

#include <ft2build.h>
#include FT_FREETYPE_H

#include <perception/gui/Shader.h>
#include <perception/gui/Dimensions.h>

namespace ptk {

class Renderable;
class Layout;
class Font;
class String;
class QuadRenderer;
class TexturedQuadRenderer;
class UntexturedQuadRenderable;
class TexturedQuadRenderable;

#ifdef DEBUG
#	define GL_DEBUG(x) do { GLenum errorCode; \
	while ((errorCode = glGetError()) != GL_NO_ERROR) \
	{ \
	printf("Error in " #x "; gl error code: %d\n", errorCode); \
	} } while (0)
#else
#	define GL_DEBUG(x)
#endif

#ifndef GUI_HISTORY_LENGTH
#	define GUI_HISTORY_LENGTH 60
#endif

struct KeyHandlerTable
{
#if defined(PLATFORM_SDL)
	std::function<void(SDL_Event*)> cb[290];
#else
	std::function<void(uint32_t *)> cb[290];
#endif
};

class Renderer
{
public:
#if defined(PLATFORM_SDL)
	typedef std::function<void(SDL_Event*)> EventCallback;
#else
	typedef std::function<void(uint32_t *)> EventCallback;
#endif
	typedef std::list<EventCallback> EventCallbackList;
	typedef std::map<RENDERER_PLATFORM_EVENT_TYPE, EventCallbackList *> EventCallbackMap;
	typedef std::pair<int, std::string> FontName;

public:
	Renderer();
	virtual ~Renderer();

    // Disable copy constructor and assignment operator.
    Renderer(const Renderer&) = delete;
    Renderer& operator=(const Renderer&) = delete;

	PixelDimensions getCanvasDimensions() const;

	void run();
	void step();
	void render(uint64_t delta);
	void handleEvents();
	void createWindow(const std::string& name, uint32_t width, uint32_t height);
	void createConsole();
	FT_Library *getFreeTypeLibrary() { return &_ft; }

	void add(Renderable *r);
	void add(Renderable *r, Dimensions d);
	void add(UntexturedQuadRenderable *r);
	void add(TexturedQuadRenderable *r);

	void remove(Renderable *r);
	void remove(UntexturedQuadRenderable *r);
	void remove(TexturedQuadRenderable *r);

	void setFPSTarget(uint32_t target);
	void setWindowDimensions(uint32_t width, uint32_t height);
	void setFovYZ(float fovY, float nearClip, float farClip);

	void setKeyHandlerTable(KeyHandlerTable *table) { _keyHandlers = table; }

	uint32_t getWidth() const { return _width; }
	uint32_t getHeight() const { return _height; }
	float getAspectRatio() const { return _aspectRatio; }
	float getPixelWidth() const { return 1.0f / _width; }
	float getPixelHeight() const { return 1.0f / _height; }

	void computeProjectionMatrix();
	glm::mat4x4 getProjectionMatrix() const;

	int loadFont(const std::string& path, int size);
	int getFontId(const std::string& path, int size) const;
	Font *getFont(int fontId) const;
	void setFont(int fontId);
	void setColor(glm::vec4 color) { _curColor = color; }

	// String management interface
	ptk::String *createString(const std::string& source);
	ptk::String *createString(int fontId, const std::string& source);
	ptk::String *createString(const std::string& source, float normX, float normY);
	ptk::String *createString(int fontId, const std::string& source, float normX, float normY);
	void deleteString(ptk::String *s);

	const Shader& getTextShader() const { return _textShader; }

	// Statistics
	float getFPS() const { return _lastFps; }
	GLint getGLPrimitivesGenerated() const { return _glPrimitives; }
	GLint getGLSamplesPassed() const { return _glSamples; }
	GLint getGLTimeElapsed() const { return _glTime; }

	// Texture/image loading features
	GLuint loadImage(const std::string& path);
	GLuint loadImage(void *mem, size_t size);
	GLuint loadImageYUV(const std::string& path);
	GLuint loadImageYUV(void *mem, size_t size);
	// @todo reference counting for things that use textures

	GLuint createTexture();
	void destroyTexture(GLuint texture);

	void platformSetupGraphicsContext(const std::string& title, uint32_t width, uint32_t height);
	void platformDestroyGraphicsContext();
	void platformAddEventHandler(RENDERER_PLATFORM_EVENT_TYPE type, EventCallback callback);
	void platformRegisterEvents();
	void platformHandleEvents();
	void platformSwapBuffers();
	void platformLoadImageTo(const std::string& path, GLuint tex);
	void platformLoadImageTo(void *mem, size_t size, GLuint tex);
	void platformGetWindowDimensions(uint32_t *width, uint32_t *height);

#ifdef PLATFORM_EGL
	void setEglFdandPitch(uint32_t fd, uint32_t fd_off, int32_t pitch);// {_eglDisplayFd=fd; _eglDisplayFdOff=fd_off;}
#endif

protected:
	uint32_t						_fpsTarget;
	bool							_run;
	uint32_t						_width;
	uint32_t						_height;
	float							_aspectRatio;
	float							_fovY;
	float							_nearClip;
	float							_farClip;
	glm::mat4x4						_projection;

	std::chrono::high_resolution_clock::time_point			_previousFrame;
	std::chrono::nanoseconds		_tickGoal;
	std::chrono::nanoseconds		_sleepGoal;
	std::chrono::nanoseconds		_renderHistory[GUI_HISTORY_LENGTH];
	uint32_t	 					_historyIndex;

	EventCallbackMap				_callbackMap;
	KeyHandlerTable				*	_keyHandlers;

	Layout						*	_rootLayout;

	RENDERER_PLATFORM_WINDOW_TYPE				_mainWindow;
	RENDERER_PLATFORM_CONTEXT_TYPE				_glContext;

	FT_Library						_ft;

	// Font and string management
	std::map<FontName, int>						_fontIds;
	std::map<int, Font *>						_fonts;
	int											_nextFontId;
	int											_curFont;
	glm::vec4									_curColor;
	float										_curZ;
	std::map<int, std::list<ptk::String *>*>	_strings;
	Shader										_textShader;

	QuadRenderer				*	_quadRenderer;
	TexturedQuadRenderer		*	_texturedQuadRenderer;

	GLuint							_statsQueries[3];
	GLint							_glTime;
	GLint							_glSamples;
	GLint							_glPrimitives;
	float							_lastFps;
#ifdef PLATFORM_EGL
    uint32_t                        _eglDisplayFd;
    uint32_t                        _eglDisplayFdOff;
    int32_t                         _eglDisplayPitch;
#endif
};

};

#endif
