/*
 *
 * Copyright (c) 2018 Texas Instruments Incorporated
 *
 * All rights reserved not granted herein.
 *
 * Limited License.
 *
 * Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
 * license under copyrights and patents it now or hereafter owns or controls to make,
 * have made, use, import, offer to sell and sell ("Utilize") this software subject to the
 * terms herein.  With respect to the foregoing patent license, such license is granted
 * solely to the extent that any such patent is necessary to Utilize the software alone.
 * The patent license shall not apply to any combinations which include this software,
 * other than combinations with devices manufactured by or for TI ("TI Devices").
 * No hardware patent is licensed hereunder.
 *
 * Redistributions must preserve existing copyright notices and reproduce this license
 * (including the above copyright notice and the disclaimer and (if applicable) source
 * code license limitations below) in the documentation and/or other materials provided
 * with the distribution
 *
 * Redistribution and use in binary form, without modification, are permitted provided
 * that the following conditions are met:
 *
 * *       No reverse engineering, decompilation, or disassembly of this software is
 * permitted with respect to any software provided in binary form.
 *
 * *       any redistribution and use are licensed by TI for use only with TI Devices.
 *
 * *       Nothing shall obligate TI to provide you with source code for the software
 * licensed and provided to you in object code.
 *
 * If software source code is provided to you, modification and redistribution of the
 * source code are permitted provided that the following conditions are met:
 *
 * *       any redistribution and use of the source code, including any resulting derivative
 * works, are licensed by TI for use only with TI Devices.
 *
 * *       any redistribution and use of any object code compiled from the source code
 * and any resulting derivative works, are licensed by TI for use only with TI Devices.
 *
 * Neither the name of Texas Instruments Incorporated nor the names of its suppliers
 *
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * DISCLAIMER.
 *
 * THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
#pragma once
#ifndef PTK_GUI_DEMODASHBOARD_H
#define PTK_GUI_DEMODASHBOARD_H

/**
 * @file DemoDashboard.h
 * @brief A dashboard for the demo with a place to track some vehicle stats and stuff
 */

#include <cstdint>

#include <string>

#include <perception/sensors/ins/ins.h>
#include <perception/gui/SharedStatic.h>
#include <perception/gui/Shader.h>
#include <perception/gui/Renderable.h>
#include <perception/gui/VirtualCamera.h>
#include <perception/gui/Visible.h>

namespace ptk
{

class String;
class Image;

class DemoDashboard :
	public Visible<DemoDashboard>,
	public Renderable,
	public SharedStatic<DemoDashboard>
{
public:
    //DemoDashboard(Renderer *r, const std::string& bgPath, int fontId);
    DemoDashboard(Renderer *r, const std::string& datasetPath, const std::string& splashBGpath, const std::string& dashBGpath, int fontId, int smFontId, uint64_t endTime);
	virtual ~DemoDashboard();

    // Disable copy constructor and assignment operator.
    DemoDashboard(const DemoDashboard &) = delete;
    DemoDashboard& operator=(const DemoDashboard &) = delete;

	virtual DemoDashboard *show();
	virtual DemoDashboard *hide();

	void setPositionInfo(PTK_INS_Record *record);
	void setBkgrdImageTextureID(GLuint camera_tex) { _bkgdImgTex = camera_tex; }
	void resetPositionInfo();
	void renderBkgrndImage();

	virtual void render(uint64_t time, Dimensions d);
	virtual void windowResize() { _modified = true; }
	void update();

public:
	static void initStatic();
	static void deinitStatic();


private:
	static Shader			_shader;			//!< Shader objects for each rendering mode
	static Shader           _bkgdImgProgram;	//!< GLSL shader program for background image rendering
	static GLint            _v_corner;          //!< Location of image square corner input
	static GLint            _v_imgTex;          //!< Location of the texture coordinates for image
	static GLint            _u_camera_sampler;  //!< Location of camera texture sampler
	static GLuint			_imgVAO;			//!< Vertex array object for image shader
	static GLuint           _imgVBOs[2];        //!< Vertex buffer objects for image shaders

	GLuint                  _bkgdImgTex;        //!< Texture containing background image to render

	int					_font;
	int					_smFont;
	int					_winWidth;
	int					_winHeight;

	Image			*	_bg{nullptr};
	String			*	_heading{nullptr};
	String			*	_position{nullptr};
	String			*	_speed{nullptr};
	String			*	_distance{nullptr};

	uint64_t			_endTime;

	// Added by DJM
	String			*	_datasetPath;
	String			*	_timeDuration;
	String			*	_timeElapsed;

	String			*	_radarPCrangeAxisLabel[5];
	String			*	_radarPCangleAxisLabel[5];

	// Sensor Labels
	String			*   _srvIndicator;
	String			*   _srrIndicator;
	String			*   _ldrIndicator;
	String			*   _imuIndicator;
	String			*   _gpsIndicator;

	uint16_t			_frameNum{0};
	PTK_INS_Record		_1strecord;
	PTK_INS_Record		_record{0};
	PTK_INS_Record		_lastRecord{0};
	float				_totalDistance{0.0f};

	Dimensions			_lastDimensions{0, 0, 0, 0};
	bool				_modified{true};
};

};

#endif
