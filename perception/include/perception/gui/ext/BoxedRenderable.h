/*
 *
 * Copyright (c) 2018 Texas Instruments Incorporated
 *
 * All rights reserved not granted herein.
 *
 * Limited License.
 *
 * Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
 * license under copyrights and patents it now or hereafter owns or controls to make,
 * have made, use, import, offer to sell and sell ("Utilize") this software subject to the
 * terms herein.  With respect to the foregoing patent license, such license is granted
 * solely to the extent that any such patent is necessary to Utilize the software alone.
 * The patent license shall not apply to any combinations which include this software,
 * other than combinations with devices manufactured by or for TI ("TI Devices").
 * No hardware patent is licensed hereunder.
 *
 * Redistributions must preserve existing copyright notices and reproduce this license
 * (including the above copyright notice and the disclaimer and (if applicable) source
 * code license limitations below) in the documentation and/or other materials provided
 * with the distribution
 *
 * Redistribution and use in binary form, without modification, are permitted provided
 * that the following conditions are met:
 *
 * *       No reverse engineering, decompilation, or disassembly of this software is
 * permitted with respect to any software provided in binary form.
 *
 * *       any redistribution and use are licensed by TI for use only with TI Devices.
 *
 * *       Nothing shall obligate TI to provide you with source code for the software
 * licensed and provided to you in object code.
 *
 * If software source code is provided to you, modification and redistribution of the
 * source code are permitted provided that the following conditions are met:
 *
 * *       any redistribution and use of the source code, including any resulting derivative
 * works, are licensed by TI for use only with TI Devices.
 *
 * *       any redistribution and use of any object code compiled from the source code
 * and any resulting derivative works, are licensed by TI for use only with TI Devices.
 *
 * Neither the name of Texas Instruments Incorporated nor the names of its suppliers
 *
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * DISCLAIMER.
 *
 * THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
#pragma once
#ifndef PTK_GUI_EXT_BOXEDRENDERABLE_H
#define PTK_GUI_EXT_BOXEDRENDAPPBOX_H

/**
 * @file ext/BoxedRenderable.h
 * @brief This extension class renders a box around a 3D renderable object, which includes some
 * title information overlaid on the box
 */

#include <perception/gui/GLlibs.h>

#include <glm/glm.hpp>
#include <glm/gtc/type_precision.hpp>

#include <perception/gui/Visible.h>
#include <perception/gui/ZOrderable.h>
#include <perception/gui/UntexturedQuadRenderable.h>
#include <perception/gui/Renderable.h>
#include <perception/gui/Dimensions.h>
#include <perception/gui/Padded.h>

namespace ptk {

class Renderer;
class String;
class Label;

class BoxedRenderable :
	public UntexturedQuadRenderable,
	public Visible<BoxedRenderable>,
	public ZOrderable<BoxedRenderable>,
	public Padded<BoxedRenderable>,
	public Renderable
{
public:
	BoxedRenderable(Renderer *r);
	virtual ~BoxedRenderable();

	virtual BoxedRenderable *show();
	virtual BoxedRenderable *hide();

	virtual BoxedRenderable *setZ(float z);
	virtual BoxedRenderable *setZ(GLushort z);

	BoxedRenderable *setContents(Renderable *r) { _inner = r; return this; }
	BoxedRenderable *setTitle(Label *lbl) { _title = lbl; return this; }
	BoxedRenderable *setBackgroundColor(const glm::vec4& color);
	BoxedRenderable *setBorderColor(const glm::vec4& color);

	void update();
	void render(uint64_t delta, Dimensions d);
	virtual void windowResize() { _modified = true; }

private:
	static const int QUAD_BG = 0;
	static const int QUAD_BORDER_LEFT = 1;
	static const int QUAD_BORDER_RIGHT = 2;
	static const int QUAD_BORDER_BOTTOM = 3;
	static const int QUAD_BORDER_TOP = 4;

private:
	Renderable		*	_inner;
	bool				_modified;
	Dimensions			_lastDimensions;

	glm::vec4			_backgroundColor;
	glm::vec4			_borderColor;

	Label			*	_title;
	Dimensions			_titleDimensions;
	Dimensions			_innerDimensions;

};

};

#endif
