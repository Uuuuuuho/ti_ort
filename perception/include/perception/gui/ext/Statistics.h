/*
 *
 * Copyright (c) 2018 Texas Instruments Incorporated
 *
 * All rights reserved not granted herein.
 *
 * Limited License.
 *
 * Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
 * license under copyrights and patents it now or hereafter owns or controls to make,
 * have made, use, import, offer to sell and sell ("Utilize") this software subject to the
 * terms herein.  With respect to the foregoing patent license, such license is granted
 * solely to the extent that any such patent is necessary to Utilize the software alone.
 * The patent license shall not apply to any combinations which include this software,
 * other than combinations with devices manufactured by or for TI ("TI Devices").
 * No hardware patent is licensed hereunder.
 *
 * Redistributions must preserve existing copyright notices and reproduce this license
 * (including the above copyright notice and the disclaimer and (if applicable) source
 * code license limitations below) in the documentation and/or other materials provided
 * with the distribution
 *
 * Redistribution and use in binary form, without modification, are permitted provided
 * that the following conditions are met:
 *
 * *       No reverse engineering, decompilation, or disassembly of this software is
 * permitted with respect to any software provided in binary form.
 *
 * *       any redistribution and use are licensed by TI for use only with TI Devices.
 *
 * *       Nothing shall obligate TI to provide you with source code for the software
 * licensed and provided to you in object code.
 *
 * If software source code is provided to you, modification and redistribution of the
 * source code are permitted provided that the following conditions are met:
 *
 * *       any redistribution and use of the source code, including any resulting derivative
 * works, are licensed by TI for use only with TI Devices.
 *
 * *       any redistribution and use of any object code compiled from the source code
 * and any resulting derivative works, are licensed by TI for use only with TI Devices.
 *
 * Neither the name of Texas Instruments Incorporated nor the names of its suppliers
 *
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * DISCLAIMER.
 *
 * THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
#pragma once
#ifndef PTK_GUI_STATISTICS_H
#define PTK_GUI_STATISTICS_H
/**
 * @class Statistics
 * This handles displaying visualization statistics about OpenGL performance
 */

#include <string>

#include <perception/gui/GLlibs.h>

#include <glm/glm.hpp>
#include <glm/gtc/type_precision.hpp>

#include <perception/gui/Renderable.h>
#include <perception/gui/Visible.h>
#include <perception/gui/ZOrderable.h>
#include <perception/gui/UntexturedQuadRenderable.h>

namespace ptk {

class Renderer;

class Statistics :
	public UntexturedQuadRenderable,
	public Visible<Statistics>,
	public ZOrderable<Statistics>,
	public Renderable
{
private:
	// Quad numbers to simplify management
	static const int QUAD_BG = 0;			///< Quad number for the background
	static const int QUAD_LBORDER = 1;		///< Quad number for the left border
	static const int QUAD_RBORDER = 2;		///< Quad number for the right border
	static const int QUAD_TBORDER = 3;		///< Quad number for the top border
	static const int QUAD_BBORDER = 4;		///< Quad number for the bottom border

	// Drawn on screen strings
	int				_font;
	String		*	_fpsDisplay;
	String		*	_glPrimitives;
	String		*	_glTime;
	String		*	_glSamples;
	String		*	_dummy;

	// Visibility status for either just fps display, or full display
	bool			_extended;

	// Dimensions/position information to simplify border recalculations
	// Note these positions apply to the bottom left corner of the text,
	// the box (including border) is drawn two pixels wider in each direction
	float			_x;
	float 			_y;
	float 			_w;
	float 			_h;

public:
	Statistics(Renderer *r, int fontId);
	virtual ~Statistics(void);

    // Disable copy constructor and assignment operator.
    Statistics(const Statistics&) = delete;
    Statistics& operator=(const Statistics&) = delete;

	virtual Statistics *show(void);
	virtual Statistics *hide(void);
	virtual Statistics *setZ(float z);
	virtual Statistics *setZ(GLushort z);

	void showExtended(void);
	void hideExtended(void);

	virtual void render(uint64_t time, Dimensions d);
	virtual void windowResize();
	void update(void);

	/**
	 * Returns extended status visibility information
	 */
	bool isExtendedVisible(void) const { return _extended; }
};

};

#endif
