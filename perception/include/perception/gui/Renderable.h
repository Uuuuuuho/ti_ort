/*
 *
 * Copyright (c) 2018 Texas Instruments Incorporated
 *
 * All rights reserved not granted herein.
 *
 * Limited License.
 *
 * Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
 * license under copyrights and patents it now or hereafter owns or controls to make,
 * have made, use, import, offer to sell and sell ("Utilize") this software subject to the
 * terms herein.  With respect to the foregoing patent license, such license is granted
 * solely to the extent that any such patent is necessary to Utilize the software alone.
 * The patent license shall not apply to any combinations which include this software,
 * other than combinations with devices manufactured by or for TI ("TI Devices").
 * No hardware patent is licensed hereunder.
 *
 * Redistributions must preserve existing copyright notices and reproduce this license
 * (including the above copyright notice and the disclaimer and (if applicable) source
 * code license limitations below) in the documentation and/or other materials provided
 * with the distribution
 *
 * Redistribution and use in binary form, without modification, are permitted provided
 * that the following conditions are met:
 *
 * *       No reverse engineering, decompilation, or disassembly of this software is
 * permitted with respect to any software provided in binary form.
 *
 * *       any redistribution and use are licensed by TI for use only with TI Devices.
 *
 * *       Nothing shall obligate TI to provide you with source code for the software
 * licensed and provided to you in object code.
 *
 * If software source code is provided to you, modification and redistribution of the
 * source code are permitted provided that the following conditions are met:
 *
 * *       any redistribution and use of the source code, including any resulting derivative
 * works, are licensed by TI for use only with TI Devices.
 *
 * *       any redistribution and use of any object code compiled from the source code
 * and any resulting derivative works, are licensed by TI for use only with TI Devices.
 *
 * Neither the name of Texas Instruments Incorporated nor the names of its suppliers
 *
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * DISCLAIMER.
 *
 * THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
#pragma once
#ifndef PTK_RENDERABLE_H
#define PTK_RENDERABLE_H

#include <inttypes.h>

#include <perception/gui/VirtualCamera.h>
#include <perception/gui/Dimensions.h>

namespace ptk {

class Renderer;
class Container;

class Renderable
{
public:
	Renderable(Renderer *r);
	virtual ~Renderable() {}

	/**
	 * Render this object, including a time delta in microseconds since it was last rendered and the
	 * size of the region being renderered to. For 3D objects, glViewport will hand dimensions and it is
	 * informative. For 2D objects with a separate aggregate pipeline, the dimensions should be used to
	 * check and update position information, so that all positions can be specified relative to the
	 * "container" and allow boxes to be nested and repositioned easily
	 * @param[in] tick The number of microseconds that have passed since this object was last rendered
	 * @param[in] dimensions The dimensions of the region this renders to of the entire screen
	 */
	virtual void render(uint64_t tick, Dimensions dimensions) = 0;

	/**
	 * Save a pointer to the renderer this is attached to, which may be needed later for some global
	 * state access
	 * @param[in] r The renderer that is being used to visualize this renderable
	 */
	void setRenderer(Renderer *r) { _r = r; }

	/**
	 * Save a pointer to the given virtual camera to use when visualizing this object
	 * @param[in] camera Pointer to virtual camera to use
	 */
	void setVirtualCamera(PTK_VirtualCamera *camera) { _viewCamera = camera; }
	PTK_VirtualCamera *getVirtualCamera() const { return _viewCamera; }

protected:
	Renderer			*	_r;
	PTK_VirtualCamera	*	_viewCamera;

protected:
	static PTK_VirtualCamera	*	_defaultCamera;
};

};

#endif
