/*
 *
 * Copyright (c) 2018 Texas Instruments Incorporated
 *
 * All rights reserved not granted herein.
 *
 * Limited License.
 *
 * Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
 * license under copyrights and patents it now or hereafter owns or controls to make,
 * have made, use, import, offer to sell and sell ("Utilize") this software subject to the
 * terms herein.  With respect to the foregoing patent license, such license is granted
 * solely to the extent that any such patent is necessary to Utilize the software alone.
 * The patent license shall not apply to any combinations which include this software,
 * other than combinations with devices manufactured by or for TI ("TI Devices").
 * No hardware patent is licensed hereunder.
 *
 * Redistributions must preserve existing copyright notices and reproduce this license
 * (including the above copyright notice and the disclaimer and (if applicable) source
 * code license limitations below) in the documentation and/or other materials provided
 * with the distribution
 *
 * Redistribution and use in binary form, without modification, are permitted provided
 * that the following conditions are met:
 *
 * *       No reverse engineering, decompilation, or disassembly of this software is
 * permitted with respect to any software provided in binary form.
 *
 * *       any redistribution and use are licensed by TI for use only with TI Devices.
 *
 * *       Nothing shall obligate TI to provide you with source code for the software
 * licensed and provided to you in object code.
 *
 * If software source code is provided to you, modification and redistribution of the
 * source code are permitted provided that the following conditions are met:
 *
 * *       any redistribution and use of the source code, including any resulting derivative
 * works, are licensed by TI for use only with TI Devices.
 *
 * *       any redistribution and use of any object code compiled from the source code
 * and any resulting derivative works, are licensed by TI for use only with TI Devices.
 *
 * Neither the name of Texas Instruments Incorporated nor the names of its suppliers
 *
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * DISCLAIMER.
 *
 * THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
#pragma once
#ifndef PTK_GUI_ZORDERABLE_H
#define PTK_GUI_ZORDERABLE_H
/**
 * @class ZOrderable
 * Mixin for classes that support changing their z-order as a single value
 */

#include <perception/gui/GLlibs.h>

namespace ptk {

template<class T>
class ZOrderable {
protected:
	GLushort _z;

	/**
	 * Behind the scenes concrete implementation, this method adjusts stored z values
	 * properly
	 * @param z The new z value, as an unnormalized unsigned short
	 */
	void _setZ(GLushort z) {
		_z = z;
	}

public:
	/**
	 * Default constructor does nothing
	 */
	ZOrderable(void) : _z(0) {}
	
	/**
	 * Optional constructor with starting/default z-index
	 * @param z Starting Z index to use
	 */
	ZOrderable(float z) : ZOrderable(static_cast<GLushort>(z)) {}

	/**
	 * Optional constructor with starting/default z-index
	 * @param z Starting Z index to use
	 */
	ZOrderable(GLushort z) : _z(z) {}

	/**
	 * Virtual destructor because this is a virtual class, but this one doesn't do anything
	 */
	virtual ~ZOrderable(void) {}

	/**
	 * The actual interface: adjust Z value
	 * @param z The new z to use, this is an unnormalized float
	 */
	virtual T *setZ(float z) {
		setZ(static_cast<GLushort>(z));
		return static_cast<T*>(this);
	}

	/**
	 * The actual interface, adjust Z value
	 * @param z The new z to use, this is an unnormalized short
	 */
	virtual T *setZ(GLushort z) {
		_setZ(z);
		return static_cast<T*>(this);
	}

};

};

#endif
