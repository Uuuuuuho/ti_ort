/*
 *
 * Copyright (c) 2018 Texas Instruments Incorporated
 *
 * All rights reserved not granted herein.
 *
 * Limited License.
 *
 * Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
 * license under copyrights and patents it now or hereafter owns or controls to make,
 * have made, use, import, offer to sell and sell ("Utilize") this software subject to the
 * terms herein.  With respect to the foregoing patent license, such license is granted
 * solely to the extent that any such patent is necessary to Utilize the software alone.
 * The patent license shall not apply to any combinations which include this software,
 * other than combinations with devices manufactured by or for TI ("TI Devices").
 * No hardware patent is licensed hereunder.
 *
 * Redistributions must preserve existing copyright notices and reproduce this license
 * (including the above copyright notice and the disclaimer and (if applicable) source
 * code license limitations below) in the documentation and/or other materials provided
 * with the distribution
 *
 * Redistribution and use in binary form, without modification, are permitted provided
 * that the following conditions are met:
 *
 * *       No reverse engineering, decompilation, or disassembly of this software is
 * permitted with respect to any software provided in binary form.
 *
 * *       any redistribution and use are licensed by TI for use only with TI Devices.
 *
 * *       Nothing shall obligate TI to provide you with source code for the software
 * licensed and provided to you in object code.
 *
 * If software source code is provided to you, modification and redistribution of the
 * source code are permitted provided that the following conditions are met:
 *
 * *       any redistribution and use of the source code, including any resulting derivative
 * works, are licensed by TI for use only with TI Devices.
 *
 * *       any redistribution and use of any object code compiled from the source code
 * and any resulting derivative works, are licensed by TI for use only with TI Devices.
 *
 * Neither the name of Texas Instruments Incorporated nor the names of its suppliers
 *
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * DISCLAIMER.
 *
 * THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
#pragma once
#ifndef PTK_GRID_FLAG_VIEW
#define PTK_GRID_FLAG_VIEW

/**
 * @file GridFlagView.h
 * @brief This provides a view for rendering bitfield/flag-based Grid structures and highlighting
 * only a single flag combination in a given color
 */

#include <glm/glm.hpp>

#include <perception/gui/GridView.h>

namespace ptk {

class GridFlagView : public GridView
{
public:
	GridFlagView() :
		_color(0.0f, 0.0f, 0.0f, 0.0f),
		_flag(0)
	{}
	virtual ~GridFlagView() {}

	GridFlagView *setFlag(uint32_t flag)
	{
		_flag = flag;
		return this;
	}

	GridFlagView *setColor(const glm::vec4& color)
	{
		_color.r = static_cast<uint8_t>(color.r * 255);
		_color.g = static_cast<uint8_t>(color.g * 255);
		_color.b = static_cast<uint8_t>(color.b * 255);
		_color.a = static_cast<uint8_t>(color.a * 255);
		return this;
	}

	GridFlagView *setColor(const glm::u8vec4& color)
	{
		_color = color;
		return this;
	}

	virtual void setColormapable(HasColormap *hc)
	{
		hc->getColormap().setPixel(_flag, _color);
		hc->getColormap().setFiltering(GL_NEAREST);
	}

	virtual bool isFixed() const { return true; }

protected:
	glm::u8vec4		_color;
	uint32_t		_flag;
};

};

#endif
