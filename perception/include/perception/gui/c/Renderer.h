/*
 *
 * Copyright (c) 2018 Texas Instruments Incorporated
 *
 * All rights reserved not granted herein.
 *
 * Limited License.
 *
 * Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
 * license under copyrights and patents it now or hereafter owns or controls to make,
 * have made, use, import, offer to sell and sell ("Utilize") this software subject to the
 * terms herein.  With respect to the foregoing patent license, such license is granted
 * solely to the extent that any such patent is necessary to Utilize the software alone.
 * The patent license shall not apply to any combinations which include this software,
 * other than combinations with devices manufactured by or for TI ("TI Devices").
 * No hardware patent is licensed hereunder.
 *
 * Redistributions must preserve existing copyright notices and reproduce this license
 * (including the above copyright notice and the disclaimer and (if applicable) source
 * code license limitations below) in the documentation and/or other materials provided
 * with the distribution
 *
 * Redistribution and use in binary form, without modification, are permitted provided
 * that the following conditions are met:
 *
 * *       No reverse engineering, decompilation, or disassembly of this software is
 * permitted with respect to any software provided in binary form.
 *
 * *       any redistribution and use are licensed by TI for use only with TI Devices.
 *
 * *       Nothing shall obligate TI to provide you with source code for the software
 * licensed and provided to you in object code.
 *
 * If software source code is provided to you, modification and redistribution of the
 * source code are permitted provided that the following conditions are met:
 *
 * *       any redistribution and use of the source code, including any resulting derivative
 * works, are licensed by TI for use only with TI Devices.
 *
 * *       any redistribution and use of any object code compiled from the source code
 * and any resulting derivative works, are licensed by TI for use only with TI Devices.
 *
 * Neither the name of Texas Instruments Incorporated nor the names of its suppliers
 *
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * DISCLAIMER.
 *
 * THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
#ifndef C_RENDERER_H
#define C_RENDERER_H

#include <stdint.h>

#include <perception/gui/c/Renderable.h>
#include <perception/gui/c/Font.h>
#include <perception/gui/c/Dimensions.h>

#ifdef __cplusplus
namespace ptk {
extern "C" {
#endif

struct String;
typedef struct String String;
struct Renderer;
struct KeyHandlerTable;

typedef struct Renderer Renderer;
typedef struct KeyHandlerTable KeyHandlerTable;

typedef struct KeyHandlerTable * pKeyHandlerTable;
typedef struct Renderer * pRenderer;


Renderer *Renderer_createRenderer();
void Renderer_delete(Renderer *obj);
void Renderer_createConsole(Renderer *obj);
void Renderer_createWindow(Renderer *obj, char *name, uint32_t width, uint32_t height);
void Renderer_run(Renderer *obj);
void Renderer_step(Renderer *obj);
void Renderer_setFovYZ(Renderer *obj, float fovY, float nearClip, float farClip);
void Renderer_addRenderable(Renderer *obj, pRenderable r, PTK_Dimensions dims);
//void Renderer_addRenderable_2(Renderer *obj, pRenderable r);

pKeyHandlerTable Renderer_createKeyHandlerTable();
void Renderer_deleteKeyHandlerTable();
void Renderer_setKeyHandlerTable(Renderer *obj, pKeyHandlerTable k);

void Renderer_addCameraControls(Renderer *obj, pKeyHandlerTable k, struct ptk_virtual_camera_t* vc);

// Font management interface
int   Renderer_loadFont(Renderer *obj, const char *path, int size);
int   Renderer_getFontId(Renderer *obj, const char *path, int size);
ptk_Font *Renderer_getFont(Renderer *obj, int fontId);
void  Renderer_setFont(Renderer *obj, int fontId);
void  Renderer_setColor(Renderer *obj, float r, float g, float b, float a);

// String management interface
String   *Renderer_createString(Renderer *obj, int fontId, const char *source);
void      Renderer_deleteString(Renderer *obj, String *s);
uint32_t  Renderer_loadImageFromPath(Renderer *obj, const char *path);
void      Renderer_loadImageFromPathWithTex(Renderer *obj, const char *path, unsigned int texture);
uint32_t  Renderer_createTexture(Renderer *obj);
uint32_t  Renderer_loadImage(Renderer *obj, void *mem, size_t size);
uint32_t  Renderer_loadImageYUVFromPath(Renderer *obj, const char *path);
uint32_t  Renderer_loadImageYUV(Renderer *obj, void *mem, size_t size);

uint32_t Renderer_getWidth(Renderer *obj);
uint32_t Renderer_getHeight(Renderer *obj);
void     Renderer_getCanvasDimensions(Renderer *obj, uint32_t *x, uint32_t *y, uint32_t *w, uint32_t *h);

#ifdef __cplusplus
}
} // namespace ptk
#endif

#endif
