#pragma once
#ifndef PTK_GUI_COLORMAP_H
#define PTK_GUI_COLORMAP_H
/**
 * Copyright (C) 2018 Texas Instruments Incorporated - http://www.ti.com/
 * All Rights Reserved.
 * The recipient shall have no right to copy or distribute or make any
 * use of this code, except as expressly provided in a separate written
 * license, executed between recipient and Texas Instruments.

 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * @file Colormap.h
 * @brief Defines a colormap in a single useful, reusable way
 */

#include <cinttypes>
#include <memory>

#include <perception/gui/GLlibs.h>
#include <glm/glm.hpp>

namespace ptk {

class Colormap
{
public:
	Colormap();
	Colormap(const glm::u8vec4 *data, uint32_t length);
	~Colormap();

	void setFiltering(GLint filter);
	void setPixel(uint32_t pixel, const glm::u8vec4& color);
	void bindTexture();

private:
	/**
	 * The Texture class is used to store and track the actual data, as well as
	 * ensure it is loaded to the GPU, while the Colormap wrapper is used as a
	 * shared copy-on-write pointer to a Texture that allows us to have built-in
	 * Colormaps (such as HEAT) which the user can "modify" without disturbing
	 * other users of that colormap. But, if the user isn't modifying it, this
	 * allows us to only have one copy in memory instead of dozens.
	 */
	class Texture
	{
		friend Colormap;
	public:
		Texture();
		~Texture();

        // Disable copy constructor and assignment operator.
        Texture(const Texture&) = delete;
        Texture& operator=(const Texture&) = delete;

		void bind();
		void setPixel(uint32_t pixel, const glm::u8vec4& color);
		void setFiltering(GLint filter);

	private:
		glm::u8vec4			*	_data;
		uint32_t				_length;
		bool					_loaded;
		GLuint					_tex;
	};

public:
	static Colormap 		RED;
	static Colormap			BLUE;
	static Colormap			GREEN;
	static Colormap			WHITE;
	static Colormap			HEAT;

private:
	void copy();

private:
	std::shared_ptr<Texture>	_holder;
};

};

#endif
