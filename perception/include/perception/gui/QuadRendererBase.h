/*
 *
 * Copyright (c) 2018 Texas Instruments Incorporated
 *
 * All rights reserved not granted herein.
 *
 * Limited License.
 *
 * Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
 * license under copyrights and patents it now or hereafter owns or controls to make,
 * have made, use, import, offer to sell and sell ("Utilize") this software subject to the
 * terms herein.  With respect to the foregoing patent license, such license is granted
 * solely to the extent that any such patent is necessary to Utilize the software alone.
 * The patent license shall not apply to any combinations which include this software,
 * other than combinations with devices manufactured by or for TI ("TI Devices").
 * No hardware patent is licensed hereunder.
 *
 * Redistributions must preserve existing copyright notices and reproduce this license
 * (including the above copyright notice and the disclaimer and (if applicable) source
 * code license limitations below) in the documentation and/or other materials provided
 * with the distribution
 *
 * Redistribution and use in binary form, without modification, are permitted provided
 * that the following conditions are met:
 *
 * *       No reverse engineering, decompilation, or disassembly of this software is
 * permitted with respect to any software provided in binary form.
 *
 * *       any redistribution and use are licensed by TI for use only with TI Devices.
 *
 * *       Nothing shall obligate TI to provide you with source code for the software
 * licensed and provided to you in object code.
 *
 * If software source code is provided to you, modification and redistribution of the
 * source code are permitted provided that the following conditions are met:
 *
 * *       any redistribution and use of the source code, including any resulting derivative
 * works, are licensed by TI for use only with TI Devices.
 *
 * *       any redistribution and use of any object code compiled from the source code
 * and any resulting derivative works, are licensed by TI for use only with TI Devices.
 *
 * Neither the name of Texas Instruments Incorporated nor the names of its suppliers
 *
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * DISCLAIMER.
 *
 * THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
#pragma once
#ifndef PTK_GUI_QUADRENDERERBASE_H
#define PTK_GUI_QUADRENDERERBASE_H

/**
 * @class QuadRendererBase
 * This is a templated base class for rendering subclasses of iQuadRenderable that
 * attempts to unify as much as possible and use CRTP-style programming to expose
 * hooks to allow specific quad renderer types to make small adjustments to the
 * overall rendering algorithm
 */

#include <cstdint>
#include <set>

#include <perception/gui/GLlibs.h>

#include <glm/glm.hpp>
#include <glm/gtc/type_precision.hpp>

#include <perception/gui/Shader.h>

namespace ptk {

/**
 * @tparam T The derived class that extends this base class and contains implementations
 * @tparam U The QuadRenderable-descendent to be used for storage
 */
template <typename T, typename U>
class QuadRendererBase {
public:
	typedef std::set<U*> RenderableSet;							//! Set of quad renderable instances to draw
	typedef typename RenderableSet::iterator RenderableIter;	//! Iterator for the set of renderables

protected:
	uint16_t			_bufferSize;	//! Size of arrays as allocated in memory
	uint16_t			_count;			//! Number of quads that we are going to draw
	bool				_updateIndex;	//! Flag indicating whether or not to re-push the index buffer

	glm::u16vec3	*	_vCoords;		//! Coordinates are stored as x, y, z, but z is constant for a quad
	GLushort		*	_index;			//! Index buffers are used to reduce the memory requirements

	GLuint 				_vao;			//! OpenGL Vertex Array Object that is used for all quads
	GLuint 				_vbo[2];		//! OpenGL Vertex Buffer Objects used to store index and vertex data
	Shader 				_shader;		//! Shader that is used to draw

	RenderableSet 		_drawItems;		// List of quad renderables that we should draw each frame

public:
	/**
	 * Initializes as much of the shared state as possible. Derived classes will need
	 * to initialize additional VBOs and bind all VBOs to shader locations
	 */
	QuadRendererBase() :
		_bufferSize(0),
		_count(0),
		_updateIndex(false),
		_vCoords(nullptr),
		_index(nullptr),
		_vao(0),
		_vbo{0, 0},
		_shader(),
		_drawItems()
	{
		// Create buffers
		glGenVertexArrays(1, &_vao);
		glBindVertexArray(_vao);
		glGenBuffers(2, _vbo);

		// Bind VBOs to VAO so we don't have to later
		glBindBuffer(GL_ARRAY_BUFFER, _vbo[0]);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _vbo[1]);
		glBindVertexArray(0);
	}

	/**
	 * Frees top level opengl resources and buffers that we control
	 */
	virtual ~QuadRendererBase(void)
	{
		glBindVertexArray(_vao);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
		glDeleteBuffers(2, _vbo);
		glBindVertexArray(0);
		glDeleteVertexArrays(1, &_vao);

		if (_vCoords)
        {
			free(_vCoords);
		}

		if (_index)
        {
			free(_index);
		}
	}

    // Disable copy constructor and assignment operator.
    QuadRendererBase(const QuadRendererBase&) = delete;
    QuadRendererBase& operator=(const QuadRendererBase&) = delete;

	/**
	 * Interface methods to implement by subclasses
	 */
	virtual void resizeBuffers(uint16_t quads) = 0;
	virtual bool renderItem(RenderableIter& iter, uint16_t offset) = 0;
	virtual void updateBuffers() = 0;
	virtual void drawElements() = 0;

	/**
	 * Trigger resize event for all tracked renderables
	 */
	void windowResize()
	{
		for (auto item : _drawItems)
		{
			item->windowResize();
		}
	}

	/**
	 * Adds the quad renderable to the drawing set
	 * @param r The Renderable to start drawing
	 */
	void add(U *r)
	{
		std::pair<RenderableIter, bool> result = _drawItems.insert(r);
		if (result.second) {
			_count += r->getQuadCount();
			ensureCapacity(_count);
		}
	}

	/**
	 * Removes the quad renderable from the drawing set.
	 * @param r The Renderable to remove
	 */
	void remove(U *r)
	{
		size_t count = _drawItems.erase(r);
		if (count == 1)
			_count -= r->getQuadCount();
	}

	/**
	 * Ensure that the coordinate arrays are large enough to handle a given number of
	 * entries all at once, to limit the number of memory operations that must be
	 * performed.
	 * @param quads The number of quads we need to be able to store
	 */
	void ensureCapacity(uint16_t quads)
	{
		if (quads > _bufferSize) {
			// Reallocate memory
			_vCoords = static_cast<glm::u16vec3 *>(realloc(_vCoords, quads*4*sizeof(glm::u16vec3)));
			_index = static_cast<GLushort *>(realloc(_index, quads*6*sizeof(GLushort)));
			resizeBuffers(quads);

			// Update indices for new values
			for (uint16_t i = _bufferSize; i < quads; ++i) {
				_index[6*i] = 4*i;
				_index[6*i+1] = 4*i + 2;
				_index[6*i+2] = 4*i + 3;
				_index[6*i+3] = 4*i;
				_index[6*i+4] = 4*i + 1;
				_index[6*i+5] = 4*i + 2;
			}

			_bufferSize = quads;
			_updateIndex = true;
		}
	}

	/**
	 * To render, we iterate over the visible quads, update their information in our
	 * buffer if necessary, and then draw them.
	 */
	void render(void) {
		bool updateVBO = false;
		uint16_t offset = 0;
		RenderableIter iter;

		_shader.use();
		glBindVertexArray(_vao);

		// Iterate over our tracked renderables and ask them for state updates
		for (iter = _drawItems.begin(); iter != _drawItems.end(); ++iter) {
			updateVBO = renderItem(iter, offset) || updateVBO;
			offset += (*iter)->getQuadCount();
		}

		// Update GPU memory as appropriate
		if (updateVBO) {
			glBindBuffer(GL_ARRAY_BUFFER, _vbo[0]);
			glBufferData(GL_ARRAY_BUFFER, _count*sizeof(glm::u16vec3)*4, _vCoords, GL_DYNAMIC_DRAW);
			updateBuffers();
		}

		// Index buffers are reloaded separately
		if (_updateIndex) {
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, _vbo[1]);
			glBufferData(GL_ELEMENT_ARRAY_BUFFER, _count*sizeof(GLushort)*6, _index, GL_DYNAMIC_DRAW);
			_updateIndex = false;
		}

		// Draw the elements indexed if we have any visible
		if (_count > 0) {
			drawElements();
		}

		glBindVertexArray(0);
		glUseProgram(0);
	}

};

};

#endif
