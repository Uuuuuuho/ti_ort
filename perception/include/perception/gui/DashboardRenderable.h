#pragma once
#ifndef PTK_DASHBOARD_RENDERABLE_H
#define PTK_DASHBOARD_RENDERABLE_H
/**
 * Copyright (C) 2018 Texas Instruments Incorporated - http://www.ti.com/
 * All Rights Reserved.
 * The recipient shall have no right to copy or distribute or make any
 * use of this code, except as expressly provided in a separate written
 * license, executed between recipient and Texas Instruments.

 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/**
 * @file DashboardRenderable.h
 * @brief Animate a progress bar and heading arrow within the dashboard
 */

#include <perception/gui/GLlibs.h>

#include <perception/sensors/ins/ins.h>
#include <perception/gui/Shader.h>
#include <perception/gui/Renderable.h>
#include <perception/gui/SharedStatic.h>


#define DASHBOARD_COL_RATIO             0.500f
#define DASHBOARD_ROW_RATIO             0.250f
// 
#define DASHBOARD_IMG_WIDTH             2164.0f
#define DASHBOARD_IMG_HEIGHT            811.0f
#define DASHBOARD_IMG_HALF_WIDTH        (DASHBOARD_IMG_WIDTH / 2)
#define DASHBOARD_IMG_HALF_HEIGHT       (DASHBOARD_IMG_HEIGHT / 2)
#define DASHBOARD_TIME_ELAPSED_LEFT     1690.0f
#define DASHBOARD_TIME_DURATION_TOP       16.0f
#define DASHBOARD_TIME_DURATION_LEFT    2164.0f
#define DASHBOARD_PATH_LEFT              560.0f
#define DASHBOARD_VEHICLE_METRICS_BOTTOM 585.0f
#define DASHBOARD_PROGRESSBAR_LEFT      (550.0f - DASHBOARD_IMG_HALF_WIDTH)
#define DASHBOARD_PROGRESSBAR_BOTTOM    (184.0f - DASHBOARD_IMG_HALF_HEIGHT)
#define DASHBOARD_PROGRESSBAR_LENGTH    1600.f
#define DASHBOARD_PROGRESSBAR_NUMROWS   11
#define DASHBOARD_HEADINGARROW_VERTICES 4
#define DASHBOARD_HEADINGARROW_LEFT     (600.0f - DASHBOARD_IMG_HALF_WIDTH)
#define DASHBOARD_HEADINGARROW_BOTTOM   (448.0f - DASHBOARD_IMG_HALF_HEIGHT)

namespace ptk {

class DashboardRenderable : public Renderable, public SharedStatic<DashboardRenderable>
{
public:
	DashboardRenderable(Renderer *, uint16_t windowCol, uint16_t winHeight);
	~DashboardRenderable();
	void update(uint16_t windowCols, uint16_t windowRows, uint64_t _endTime, uint64_t _startTime);
	void setCurTime(uint64_t curTime);
	virtual void render(uint64_t delta, Dimensions d);
	void setPositionInfo(PTK_INS_Record *record);


public:
    static void initStatic();
    static void deinitStatic();

private:
	GLuint				_vao[2];        // vertex array object 
	GLuint				_vbo[3];        // vertex buffer object for progress bar
	GLuint				_vboHA[2];      // vertex buffer object for heading arrow

	uint64_t			_startTime;		// time stamp (ns) from the first record in the sequence
	uint64_t			_endTime;		// time stamp (ns) from the last record in the sequence
	uint64_t			_seqTime;		// duration (ns) of the sequence
	uint64_t			_curTime;
	uint16_t			_windowCols; 
	uint16_t			_windowRows;
	double				_rowDelta;
	glm::vec2			_endPoints[DASHBOARD_PROGRESSBAR_NUMROWS * 2];		// elements for 2D
	
private:
	static Shader		_shaderProgressBar;
	static GLint		_v_coordsPB;
	static GLint		_u_colorPB;

	static Shader		_shaderHeadingArrow;
	static GLint		_v_coordsHA;
	static GLint		_u_colorHA;

	PTK_INS_Record		_record;
};

};

#endif
