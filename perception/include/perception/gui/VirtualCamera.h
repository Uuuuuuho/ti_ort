/*
 *
 * Copyright (c) 2018 Texas Instruments Incorporated
 *
 * All rights reserved not granted herein.
 *
 * Limited License.
 *
 * Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
 * license under copyrights and patents it now or hereafter owns or controls to make,
 * have made, use, import, offer to sell and sell ("Utilize") this software subject to the
 * terms herein.  With respect to the foregoing patent license, such license is granted
 * solely to the extent that any such patent is necessary to Utilize the software alone.
 * The patent license shall not apply to any combinations which include this software,
 * other than combinations with devices manufactured by or for TI ("TI Devices").
 * No hardware patent is licensed hereunder.
 *
 * Redistributions must preserve existing copyright notices and reproduce this license
 * (including the above copyright notice and the disclaimer and (if applicable) source
 * code license limitations below) in the documentation and/or other materials provided
 * with the distribution
 *
 * Redistribution and use in binary form, without modification, are permitted provided
 * that the following conditions are met:
 *
 * *       No reverse engineering, decompilation, or disassembly of this software is
 * permitted with respect to any software provided in binary form.
 *
 * *       any redistribution and use are licensed by TI for use only with TI Devices.
 *
 * *       Nothing shall obligate TI to provide you with source code for the software
 * licensed and provided to you in object code.
 *
 * If software source code is provided to you, modification and redistribution of the
 * source code are permitted provided that the following conditions are met:
 *
 * *       any redistribution and use of the source code, including any resulting derivative
 * works, are licensed by TI for use only with TI Devices.
 *
 * *       any redistribution and use of any object code compiled from the source code
 * and any resulting derivative works, are licensed by TI for use only with TI Devices.
 *
 * Neither the name of Texas Instruments Incorporated nor the names of its suppliers
 *
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * DISCLAIMER.
 *
 * THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
#pragma once
#ifndef PTK_VIRTUAL_CAMERA_H
#define PTK_VIRTUAL_CAMERA_H

/**
 * @file virtualCamera.h
 * @brief Defines a virtual camera which can be used with modules that require one for
 *        visualization or identifying a new perspective
 */

#include <stdint.h>

#ifdef __cplusplus
#include <glm/glm.hpp>

extern "C" {
#endif

// In C we use an incomplete type to name the pointers and provide basic type safety
struct ptk_virtual_camera_t;
typedef struct ptk_virtual_camera_t PTK_VirtualCamera;

typedef enum ptk_virtual_camera_basis_e
{
    PTK_VIRTUAL_CAMERA_BASIS_FORWARD,
    PTK_VIRTUAL_CAMERA_BASIS_LEFT,
    PTK_VIRTUAL_CAMERA_BASIS_UP
} PTK_VirtualCameraBasis;

typedef enum ptk_virtual_camera_direction_e
{
    PTK_VIRTUAL_CAMERA_DIRECTION_FORWARD,
    PTK_VIRTUAL_CAMERA_DIRECTION_LEFT,
    PTK_VIRTUAL_CAMERA_DIRECTION_RIGHT,
    PTK_VIRTUAL_CAMERA_DIRECTION_BACK,
    PTK_VIRTUAL_CAMERA_DIRECTION_UP,
    PTK_VIRTUAL_CAMERA_DIRECTION_DOWN
} PTK_VirtualCameraDirection;

uint32_t PTK_VirtualCamera_getSize();
void PTK_VirtualCamera_print(const PTK_VirtualCamera *);

PTK_VirtualCamera *PTK_VirtualCamera_alloc();
void PTK_VirtualCamera_free(PTK_VirtualCamera *);

void PTK_VirtualCamera_copy(PTK_VirtualCamera *__restrict dst,
                            const PTK_VirtualCamera *__restrict src);

void PTK_VirtualCamera_getViewMatrix(const PTK_VirtualCamera *, float *);
void PTK_VirtualCamera_getProjMatrix(const PTK_VirtualCamera *, float *);
void PTK_VirtualCamera_getVPMatrix(const PTK_VirtualCamera *, float *);

void PTK_VirtualCamera_move(PTK_VirtualCamera *, PTK_VirtualCameraDirection, float);
void PTK_VirtualCamera_rotate(PTK_VirtualCamera *, PTK_VirtualCameraBasis, float);

void PTK_VirtualCamera_set(PTK_VirtualCamera *, PTK_VirtualCameraBasis, float *);
void PTK_VirtualCamera_setTranslate(PTK_VirtualCamera *, float *);

void PTK_VirtualCamera_setFovY(PTK_VirtualCamera *vc, float fovY);
void PTK_VirtualCamera_setAspectRatio(PTK_VirtualCamera *vc, float aspectRatio);
void PTK_VirtualCamera_setClippingPlanes(PTK_VirtualCamera *vc, float nearZ, float farZ);

#ifdef __cplusplus
}

/*Note on coordinate systems:
 * The virtual camera coordinate system is
 *   x-axis pointing to the right of the virtual image
 *   y-axis pointing to the top of the virtual image image
 *   z-axis pointing into the virtual camera
 * This should be in accordance with OpenGL conventions, so that subsequent perspective
 * matrix can be applied as usual.
 * In particular, note that points with negative z-value (in virtual camera coordinates) are in
 * front of the virtual camera.
 */
struct ptk_virtual_camera_t
{
    //!< Defining View
    glm::vec3 forward; //camera forward (negative z-axis) in world coordinates
    glm::vec3 left;    //camera left (negative x-axis) in world coordinates
    glm::vec3 up;      //camera up (y-axis) in world coordinates
    glm::vec3 translate; //vector "camera origin to world origin" in world coordinates

    //!< Defining Projection
    float fovY; //!< vertical field of view
    float aspectRatio; //!< aspect ratio (width/height) of viewport
    float nearZ; //!< clip data closer than this
    float farZ;  //!< clip data further than this

    ptk_virtual_camera_t() :
        forward(0.f, 0.f, -1.f),
        left(-1.f, 0.f, 0.f),
        up(0.f, 1.f, 0.f),
        translate(0.f, 0.f, 0.f),
        fovY(33.75),
        aspectRatio(1.0f),
        nearZ(0.1f),
        farZ(5000.0f)
    {}

    ~ptk_virtual_camera_t() {}
};

// Only in C++ builds we add some more functions
glm::mat4x4 PTK_VirtualCamera_getViewMatrix4x4(const PTK_VirtualCamera *);
glm::mat4x4 PTK_VirtualCamera_getProjMatrix4x4(const PTK_VirtualCamera *);
glm::mat4x4 PTK_VirtualCamera_getVPMatrix4x4(const PTK_VirtualCamera *);
void PTK_VirtualCamera_freeRotate(PTK_VirtualCamera *vc, const glm::vec3& about, float angle);

void PTK_VirtualCamera_setVec3(PTK_VirtualCamera *, PTK_VirtualCameraBasis, const glm::vec3 &);
#endif


#endif
