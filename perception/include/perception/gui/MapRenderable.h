/*
 *
 * Copyright (c) 2018 Texas Instruments Incorporated
 *
 * All rights reserved not granted herein.
 *
 * Limited License.
 *
 * Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
 * license under copyrights and patents it now or hereafter owns or controls to make,
 * have made, use, import, offer to sell and sell ("Utilize") this software subject to the
 * terms herein.  With respect to the foregoing patent license, such license is granted
 * solely to the extent that any such patent is necessary to Utilize the software alone.
 * The patent license shall not apply to any combinations which include this software,
 * other than combinations with devices manufactured by or for TI ("TI Devices").
 * No hardware patent is licensed hereunder.
 *
 * Redistributions must preserve existing copyright notices and reproduce this license
 * (including the above copyright notice and the disclaimer and (if applicable) source
 * code license limitations below) in the documentation and/or other materials provided
 * with the distribution
 *
 * Redistribution and use in binary form, without modification, are permitted provided
 * that the following conditions are met:
 *
 * *       No reverse engineering, decompilation, or disassembly of this software is
 * permitted with respect to any software provided in binary form.
 *
 * *       any redistribution and use are licensed by TI for use only with TI Devices.
 *
 * *       Nothing shall obligate TI to provide you with source code for the software
 * licensed and provided to you in object code.
 *
 * If software source code is provided to you, modification and redistribution of the
 * source code are permitted provided that the following conditions are met:
 *
 * *       any redistribution and use of the source code, including any resulting derivative
 * works, are licensed by TI for use only with TI Devices.
 *
 * *       any redistribution and use of any object code compiled from the source code
 * and any resulting derivative works, are licensed by TI for use only with TI Devices.
 *
 * Neither the name of Texas Instruments Incorporated nor the names of its suppliers
 *
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * DISCLAIMER.
 *
 * THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
#pragma once
#ifndef PTK_MAP_RENDERABLE_H
#define PTK_MAP_RENDERABLE_H

/**
 * @file MapRenderable.h
 * @brief Used to visualize Map structs
 */

#include <map>
#include <vector>

#include <perception/base/map.h>

#include <perception/gui/Shader.h>
#include <perception/gui/Renderable.h>
#include <perception/gui/VirtualCamera.h>
#include <perception/gui/SharedStatic.h>
#include <perception/gui/GridView.h>
#include <perception/gui/HasColormap.h>

#include <perception/base/map.h>

namespace ptk {

class Renderer;
class GridView;
class GridIntensityView;
class GridFlagView;

class MapRenderable :
	public Renderable,
	public HasColormap,
	public SharedStatic<MapRenderable>
{
public:
	MapRenderable(Renderer *r);
	virtual ~MapRenderable();

    /**
    * Render this object, including a time delta in microseconds since it was last rendered
    * @param[in] tick The number of microseconds that have passed since this object was last rendered
    */
    virtual void setMap(PTK_Map *map);

	/**
	 * Add the given grid view to be used when rendering this MapRenderable
	 * @param[in] view The view to use to assist in drawing
	 * @param[in] src The identifier of the source grid to give to the view
	 */
	virtual void add(GridView *view, uint32_t src);

	virtual GridIntensityView *createGridIntensityView() const;
	virtual GridFlagView *createGridFlagView() const;

	virtual void render(uint64_t delta, Dimensions d);

	void useFixedBounds(bool fixed) { _useFixedNormalization = fixed; }
	void setNormalizationBounds(float zmin, float zmax) { _zmin = zmin; zmax = _zmax; }

public:
    static void initStatic();
    static void deinitStatic();

private:
	void computeBounds(const PTK_Grid *g, float& zmin, float& zmax, float& znorm) const;

private:
    uint32_t        		_x;
    uint32_t        		_y;
    GLuint          		_texture[2];
    GLuint          		_buffers[2];
	GLuint					_vao;
	float					_pCorners[8];
	float					_zmin;
	float					_zmax;
	bool					_useFixedBMP;
	bool					_useFixedNormalization;
	std::map<uint32_t, std::vector<GridView *>*> _views;
    PTK_Map				*	_map;

private:
    static Shader      		_shader;
    static GLint       		_v_coords;
    static GLint       		_v_texCoords;
	static GLint       		_u_texGrid;
	static GLint       		_u_colorLUT;
	static GLint       		_u_mvp;
	static GLint       		_u_useFixed;
	static GLint       		_u_intTexGrid;
	static GLint       		_u_norm;
};

};

#endif
