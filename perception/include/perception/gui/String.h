/*
 *
 * Copyright (c) 2018 Texas Instruments Incorporated
 *
 * All rights reserved not granted herein.
 *
 * Limited License.
 *
 * Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
 * license under copyrights and patents it now or hereafter owns or controls to make,
 * have made, use, import, offer to sell and sell ("Utilize") this software subject to the
 * terms herein.  With respect to the foregoing patent license, such license is granted
 * solely to the extent that any such patent is necessary to Utilize the software alone.
 * The patent license shall not apply to any combinations which include this software,
 * other than combinations with devices manufactured by or for TI ("TI Devices").
 * No hardware patent is licensed hereunder.
 *
 * Redistributions must preserve existing copyright notices and reproduce this license
 * (including the above copyright notice and the disclaimer and (if applicable) source
 * code license limitations below) in the documentation and/or other materials provided
 * with the distribution
 *
 * Redistribution and use in binary form, without modification, are permitted provided
 * that the following conditions are met:
 *
 * *       No reverse engineering, decompilation, or disassembly of this software is
 * permitted with respect to any software provided in binary form.
 *
 * *       any redistribution and use are licensed by TI for use only with TI Devices.
 *
 * *       Nothing shall obligate TI to provide you with source code for the software
 * licensed and provided to you in object code.
 *
 * If software source code is provided to you, modification and redistribution of the
 * source code are permitted provided that the following conditions are met:
 *
 * *       any redistribution and use of the source code, including any resulting derivative
 * works, are licensed by TI for use only with TI Devices.
 *
 * *       any redistribution and use of any object code compiled from the source code
 * and any resulting derivative works, are licensed by TI for use only with TI Devices.
 *
 * Neither the name of Texas Instruments Incorporated nor the names of its suppliers
 *
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * DISCLAIMER.
 *
 * THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
#pragma once
#ifndef PTK_GUI_STRING_H
#define PTK_GUI_STRING_H

/*
 * @todo: Unify the buffers in the renderer so that strings of a certain type can be rendered
 *        with one call; use a uniform buffer object with color vectors and add a single id per char
 *        indicating which color to use
 * @todo: Take another look at the use of short/long. The use of shorts caused problems with stuff
 *        that went off screen but it wasn't fixed fully, five years ago
 */

/**
 * @file String.h
 * @brief A string that is rendered in 2D over the screen
 */

#include <string>

#include <perception/gui/GLlibs.h>

#include <glm/glm.hpp>
#include <glm/gtc/type_precision.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <perception/gui/Font.h>
#include <perception/gui/ZOrderable.h>
#include <perception/gui/Visible.h>

namespace ptk {

class Renderer;
class Font;

/**
 * todo refactor this to be a Renderable and support receiving dimensions at render time which are
 * used to update position information
 */
class String : public ZOrderable<String>, public Visible<String> {
private:
	//
	std::string			_source;
	bool				_init;
	bool				_gInit;
	bool				_modified;
	int					_fontId;
	Font 			*	_font;
	Renderer		*	_renderer;

	// Start of the string
	float				_x;
	float				_y;

	// Start of string, normalized
	GLushort 			_startX;
	GLushort			_startY;

	// End of string, normalized
	GLushort			_curX;
	GLushort			_curY;

	// Bounds for where we are allowed to draw our string (should not emit vertices greater than these)
	GLint 				_bMinX;
	GLint 				_bMaxX;
	GLint 				_bMinY;
	GLint 				_bMaxY;

	// Buffers to copy to GPU
	glm::u16vec2	*	_vertcoords;
	glm::u16vec2	*	_texcoords;
	GLushort		*	_index;
	glm::vec4			_color;

	// Array size counters
	size_t				_strLen;
	size_t				_maxCount;
	int					_vertexCount;
	int					_indexCount;

	// opengl buffer identifiers
	GLuint				_vao;
	GLuint				_vbo[2];
	GLuint				_ibo;

	// Drawing and management helper methods
	void drawChar(const Font::char_info& ci,
	              GLshort curX,
				  GLshort curY,
				  int indexOffset,
				  int vertexOffset);
	bool hasCapacity(size_t count);
	void increaseCapacity(size_t minCapacity, size_t copyCount);
	void findPen(const std::string& source);
	void findPenDraw(const std::string& source);

public:
	String(Renderer *r, Font* font);
	~String(void);

    // Disable copy constructor and assignment operator.
    String(const String&) = delete;
    String& operator=(const String&) = delete;

	// Accessors
	size_t length(void) const { return _source.length(); }
	int getFontId(void) const { return _fontId; }
	Font* getFont(void) const { return _font; }
	int getVertexCount(void) const { return _vertexCount; }
	int getIndexCount(void) const { return _indexCount; }
	const std::string& getText(void) const { return _source; }

	// Text adjustment
	void drawText(const std::string& source, float normX, float normY);
	void drawText(const std::string& source);
	String *translate(float normX, float normY);
	String *setPosition(float normX, float normY);
	String *setColor(const glm::vec4& color) { _color = color; return this; }
	void recompute() { drawText(_source, _x, _y); }

	// Update the opacity
	void setOpacity(float alpha);
	void setOpacity(GLubyte alpha);

	// Bounding box adjustment and reading
	void setMinX(float minX);
	void setMinY(float minY);
	void setMaxX(float maxX);
	void setMaxY(float maxY);
	float getWidthf(void) const { return _font->getStringWidthf(_source); }
	float getHeightf(void) const { return _font->getHeight(); }
	float getX(void) const { return _x; }
	float getY(void) const { return _y; }

	// Text modification
	String *append(const std::string& source);
	String *remove(size_t start);
	String *remove(size_t start, size_t length);
	String *insert(const std::string& source, int start);

	// Rendering
	void graphicsInit(void);
	void render(void);
};

};

#endif
