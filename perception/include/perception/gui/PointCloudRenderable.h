/*
 *
 * Copyright (c) 2018 Texas Instruments Incorporated
 *
 * All rights reserved not granted herein.
 *
 * Limited License.
 *
 * Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
 * license under copyrights and patents it now or hereafter owns or controls to make,
 * have made, use, import, offer to sell and sell ("Utilize") this software subject to the
 * terms herein.  With respect to the foregoing patent license, such license is granted
 * solely to the extent that any such patent is necessary to Utilize the software alone.
 * The patent license shall not apply to any combinations which include this software,
 * other than combinations with devices manufactured by or for TI ("TI Devices").
 * No hardware patent is licensed hereunder.
 *
 * Redistributions must preserve existing copyright notices and reproduce this license
 * (including the above copyright notice and the disclaimer and (if applicable) source
 * code license limitations below) in the documentation and/or other materials provided
 * with the distribution
 *
 * Redistribution and use in binary form, without modification, are permitted provided
 * that the following conditions are met:
 *
 * *       No reverse engineering, decompilation, or disassembly of this software is
 * permitted with respect to any software provided in binary form.
 *
 * *       any redistribution and use are licensed by TI for use only with TI Devices.
 *
 * *       Nothing shall obligate TI to provide you with source code for the software
 * licensed and provided to you in object code.
 *
 * If software source code is provided to you, modification and redistribution of the
 * source code are permitted provided that the following conditions are met:
 *
 * *       any redistribution and use of the source code, including any resulting derivative
 * works, are licensed by TI for use only with TI Devices.
 *
 * *       any redistribution and use of any object code compiled from the source code
 * and any resulting derivative works, are licensed by TI for use only with TI Devices.
 *
 * Neither the name of Texas Instruments Incorporated nor the names of its suppliers
 *
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * DISCLAIMER.
 *
 * THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
#pragma once
#ifndef PTK_POINTCLOUD_RENDERABLE_H
#define PTK_POINTCLOUD_RENDERABLE_H

/**
 * @file PointCloudRenderable.h
 * @brief API for visualizing point clouds
 */

#include <cstdint>
#include <glm/glm.hpp>

#include <perception/base/point.h>
#include <perception/base/pointCloud.h>

#include <perception/gui/SharedStatic.h>
#include <perception/gui/Shader.h>
#include <perception/gui/Shader.h>
#include <perception/gui/Renderable.h>
#include <perception/gui/VirtualCamera.h>
#include <perception/gui/HasColormap.h>

namespace ptk {

class PointCloudRenderable :
	public Renderable,
	public HasColormap,
	public SharedStatic<PointCloudRenderable>
{
public:
    // Configuration
    enum Colormap {
        WHITE = 0,     //!< all white
        RED,           //!< all red
        GREEN,         //!< all green
        BLUE,          //!< all blue
        HEAT,          //!< linear spectrum from blue to red
        NUM_COLORMAPS
    };

    enum ColormapInput {
        X = 0,
        Y,
        Z,
        W,
        NUM_COLORMAPINPUTS
    };

    enum RenderMode {
        MODE_3D = 0,            //!< Point cloud is shown in 3D with virtual camear
        MODE_2D_PROJECTED,      //!< Point cloud is projected on GPU with camera model
        MODE_2D_PRECOMPUTED,    //!< Point cloud is projected beforehand and is already in NDC on GPU
        NUM_RENDERMODES
    };

    /*For MODE_2D_PROJECTED only*/
    struct CameraParams {
    //Note: Allowing only camera model with parameters f, px, py, plus radial lens distortion LUT
            uint32_t       numPixelsX;
            uint32_t       numPixelsY;
            float          focalLength; //!< focal length of camera in pixels
            float          principalPointX; //!< x-coordinate of principal point of camera in pixels
            float          principalPointY; //!< y-coordinate of principal point of camera in pixels
            //float *     u2d_table; //!< LDC look-up table (no distortion correction of null pointer is passed)
            //int32_t     u2d_length; //!< if _u2d_table is passed, length of the LUT
            //float       u2d_step;   //!< if _u2d_table is passed, step size of the LUT
            float          P[12]; //!< 12th degree polynomial coefficients in terms of angle theta [deg] for lens distortion:
                                  //!< r_distorted = Pol(theta)*r_undistorted where Pol(th) = P[0] + P[1] * th+...
    };

public:
	PointCloudRenderable(Renderer *r);
    virtual ~PointCloudRenderable();

	virtual void render(uint64_t time, Dimensions d);

    void setPointCloud(PTK_PointCloud *pc);
    void setVideoData(GLuint camera_tex) { _camera_tex = camera_tex; }
    void setRigidTransform(const PTK_RigidTransform *M_pre);

    void setRenderMode(RenderMode renderMode) { _renderMode = renderMode; }
    void setColormapInputVector(const glm::vec4& cmap) { _colormapInputVec = cmap; }
    void setPointSize(float pointSize) { _pointSize = pointSize; }
    void setColormapInput(ColormapInput colormapInput);
    void setColormapAutoRange(bool autoRange);
    void setColormapRange(float rangeMin, float rangeMax)
	{
		_rangeMin = rangeMin;
		_rangeMax = rangeMax;
	}

    void setCameraParams(struct CameraParams *prms);
    void setCameraResolution(uint32_t numPixelsX, uint32_t numPixelsY)
	{
		_realCam.numPixelsX = numPixelsX;
		_realCam.numPixelsY = numPixelsY;
	}

    void setCameraFocalLength(float focalLength) { _realCam.focalLength = focalLength; }

    void setCameraPrincipalPoint(float principalPointX, float principalPointY)
	{
		_realCam.principalPointX = principalPointX;
		_realCam.principalPointY = principalPointY;
	}
    void setCameraLensDistortion(float P[12]);
    void setMaxTheta(float maxTheta) {_maxTheta = maxTheta;};

public:
    static void initStatic();
	static void deinitStatic();

private:
    void renderPointCloud();
    void renderVideo();
    void loadPointCloud();
    void computeMvp();
    void computeAutoRange();

private:
	static Shader			_shader[3];			//!< Shader objects for each rendering mode
    static GLint            _v_point[3];       //!< Location of input laser data attribute
    static GLint            _u_mvp[3];              //!< Location of MVP matrix in vertex shader
    static GLint            _u_colorMap[3];    //!< Location of color atlas texture in vertex shader
    static GLint            _u_colorMapInput[3]; //!< Location of color map input in vertex shader
    static GLint            _u_norm[3];        //!< Location of height normalization input
    static GLint            _u_pointSize[3];       //!< Location of point size in vertex shader
    static GLint            _u_f;        //!< Location of focal length in vertex shader
    static GLint            _u_px;       //!< Location of principal point x in vertex shader
    static GLint            _u_py;       //!< Location of principal point y in vertex shader
    static GLint            _u_nx;       //!< Location of # pixels in x in vertex shader
    static GLint            _u_ny;       //!< Location of # pixels in y in vertex shader
    static GLint            _u_poly4;
    static GLint            _u_poly8;
    static GLint            _u_poly12;
    static GLint            _u_maxTheta;       //!< Location of maxTheta in vertex shader

    static Shader           _videoProgram;      //!< GLSL shader program for video rendering
    static GLint            _v_corner;           //!< Location of video square corner input
    static GLint            _v_vidTex;           //!< Location of the texture coordinates for video
    static GLint            _u_camera_sampler;   //!< Location of camera texture sampler
	static GLuint			_videoVAO;			//!< Vertex array object for video shader
    static GLuint           _videoVBOs[2];      //!< Vertex buffer objects for video shaders

    //Point Cloud data
    uint32_t                _numPoints;         //!< number of points currently loaded/to be rendered
    PTK_PointCloud     *    _pointCloud;        //!< pointer to current point data buffer
	GLuint					_cloudVAO;			//!< Vertex array object for cloud data
    GLuint                  _cloudVBOs[1];      //!< VBOs for cloud data
    glm::mat4               _mvp;                //!< Actual model-view-projection matrix
    glm::mat4               _m_pre;             //!< pre-rendering transform applied to point cloud data

    //Video data
    GLuint                  _camera_tex;        //!< Texture containing camera video frames to render

    //Render Modes
    RenderMode              _renderMode;

    //Configurations for MODE_3D
    //Note: 3D mode: _mvp = virtualCamProjection * virtualCamPose * _m_pre

    //Configurations for MODE_2D_PROJECTED
    //Note: 2D projected mode: _mvp = _m_pre, then custom projection, then custom lens LUT
    struct CameraParams     _realCam;
    float                   _maxTheta; //!< maximum angle theta (degrees) for a point to be rendered
    //pinholeCamera          _pinholeCamera; //TODO only needed if projection done outside of shader

    //Configuration for appearance of points
    enum ColormapInput      _colormapInput;
    glm::vec4               _colormapInputVec;
    float                   _rangeMin;
    float                   _rangeMax;
    bool                    _autoRange;
    bool                    _computeAutoRangeNeeded;
    float                   _pointSize;
};

};

#endif
