/*
 *
 * Copyright (c) 2018 Texas Instruments Incorporated
 *
 * All rights reserved not granted herein.
 *
 * Limited License.
 *
 * Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
 * license under copyrights and patents it now or hereafter owns or controls to make,
 * have made, use, import, offer to sell and sell ("Utilize") this software subject to the
 * terms herein.  With respect to the foregoing patent license, such license is granted
 * solely to the extent that any such patent is necessary to Utilize the software alone.
 * The patent license shall not apply to any combinations which include this software,
 * other than combinations with devices manufactured by or for TI ("TI Devices").
 * No hardware patent is licensed hereunder.
 *
 * Redistributions must preserve existing copyright notices and reproduce this license
 * (including the above copyright notice and the disclaimer and (if applicable) source
 * code license limitations below) in the documentation and/or other materials provided
 * with the distribution
 *
 * Redistribution and use in binary form, without modification, are permitted provided
 * that the following conditions are met:
 *
 * *       No reverse engineering, decompilation, or disassembly of this software is
 * permitted with respect to any software provided in binary form.
 *
 * *       any redistribution and use are licensed by TI for use only with TI Devices.
 *
 * *       Nothing shall obligate TI to provide you with source code for the software
 * licensed and provided to you in object code.
 *
 * If software source code is provided to you, modification and redistribution of the
 * source code are permitted provided that the following conditions are met:
 *
 * *       any redistribution and use of the source code, including any resulting derivative
 * works, are licensed by TI for use only with TI Devices.
 *
 * *       any redistribution and use of any object code compiled from the source code
 * and any resulting derivative works, are licensed by TI for use only with TI Devices.
 *
 * Neither the name of Texas Instruments Incorporated nor the names of its suppliers
 *
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * DISCLAIMER.
 *
 * THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
#pragma once
#ifndef PTK_GUI_QUADRENDERABLE_H
#define PTK_GUI_QUADRENDERABLE_H
/**
 * @class iQuadRenderable
 * The interface QuadRenderable describes an object that allocates and handles
 * at least some of its rendering through quads. This handles the position tracking
 * for the quads but does not dictate any of their other properties.
 */

#include <cstdint>

#include <perception/gui/GLlibs.h>

#include <glm/glm.hpp>
#include <glm/gtc/type_precision.hpp>

namespace ptk {

class QuadRenderable {
protected:
	uint16_t			_count;			//! Count of quads rendered
	uint16_t			_prevOffset;	//! Offset that we previously rendered to
	bool				_modified;		//! Have the vertex or color data been modified?
	glm::u16vec3	*	_vCoords;		//! Local copy of vertex coordinates

public:
    // Disable copy constructor and assignment operator.
    QuadRenderable(const QuadRenderable&) = delete;
    QuadRenderable& operator=(const QuadRenderable&) = delete;

	QuadRenderable(uint16_t quads);
	virtual ~QuadRenderable(void);

	// Interface for subclasses to draw the quad position information
	void setQuadsZ(GLshort z);
	void setQuadXY(uint16_t quad, float x, float y, float w, float h);
	void setQuadXY(uint16_t quad, GLshort x, GLshort y, GLshort w, GLshort h);
	void setQuadZ(uint16_t quad, GLshort z);
	void setQuadPosition(uint16_t quad, const glm::u16vec3& coords, GLshort w, GLshort h);

	/**
	 * Called when the window is resized by the Renderer
	 */
	virtual void windowResize() = 0;

	bool render(glm::u16vec3 *vCoords, uint16_t offset, bool force);

	/**
	 * @return Number of quads this Renderable expects
	 */
	uint16_t getQuadCount(void) const { return _count; }
};

};

#endif
