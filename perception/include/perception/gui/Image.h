/*
 *
 * Copyright (c) 2018 Texas Instruments Incorporated
 *
 * All rights reserved not granted herein.
 *
 * Limited License.
 *
 * Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
 * license under copyrights and patents it now or hereafter owns or controls to make,
 * have made, use, import, offer to sell and sell ("Utilize") this software subject to the
 * terms herein.  With respect to the foregoing patent license, such license is granted
 * solely to the extent that any such patent is necessary to Utilize the software alone.
 * The patent license shall not apply to any combinations which include this software,
 * other than combinations with devices manufactured by or for TI ("TI Devices").
 * No hardware patent is licensed hereunder.
 *
 * Redistributions must preserve existing copyright notices and reproduce this license
 * (including the above copyright notice and the disclaimer and (if applicable) source
 * code license limitations below) in the documentation and/or other materials provided
 * with the distribution
 *
 * Redistribution and use in binary form, without modification, are permitted provided
 * that the following conditions are met:
 *
 * *       No reverse engineering, decompilation, or disassembly of this software is
 * permitted with respect to any software provided in binary form.
 *
 * *       any redistribution and use are licensed by TI for use only with TI Devices.
 *
 * *       Nothing shall obligate TI to provide you with source code for the software
 * licensed and provided to you in object code.
 *
 * If software source code is provided to you, modification and redistribution of the
 * source code are permitted provided that the following conditions are met:
 *
 * *       any redistribution and use of the source code, including any resulting derivative
 * works, are licensed by TI for use only with TI Devices.
 *
 * *       any redistribution and use of any object code compiled from the source code
 * and any resulting derivative works, are licensed by TI for use only with TI Devices.
 *
 * Neither the name of Texas Instruments Incorporated nor the names of its suppliers
 *
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * DISCLAIMER.
 *
 * THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
#pragma once
#ifndef PTK_GUI_IMAGE_H
#define PTK_GUI_IMAGE_H

/**
 * @file Image.h
 * @brief Show an image on screen with inherited dimensions
 * @todo add scaling/stretching/sizing options to images
 */

#include <perception/gui/Renderable.h>
#include <perception/gui/TexturedQuadRenderable.h>
#include <perception/gui/Visible.h>

namespace ptk {

class Renderer;

class Image :
	public Renderable,
	public TexturedQuadRenderable,
	public Visible<Image>
{
public:
	enum FitType
	{
		STRETCH,		//!< Reshape to full dimensions given
		FIT_X,			//!< Keep aspect ratio, match x (may overflow y)
		FIT_Y,			//!< Keep aspect ratio, match y (may overflow x)
		FIT,			//!< Keep aspect ratio, match largest dimension (no overflows)
		NATURAL			//!< Do not resize, may overflow (not recommended)
	};

public:
	Image(Renderer *r, GLuint texture, bool invert, FitType fit);
	Image(Renderer *r, GLuint texture, bool invert);
	Image(Renderer *r, GLuint texture, FitType fit);
	Image(Renderer *r, GLuint texture);
	virtual ~Image();

	virtual Image *show();
	virtual Image *hide();

	Image *setTexture(GLuint tex);

	virtual void render(uint64_t delta, Dimensions d);
	virtual void windowResize() { _modified = true; }
	void update();

	float getScale() const { return _scale; }
	float getXOffset() const { return _xOffset; }
	float getYOffset() const { return _yOffset; }

private:
	Dimensions		_lastDimensions;
	bool			_modified;
	bool			_invert;
	FitType			_fit;
	int				_texWidth;
	int				_texHeight;

	// Compute parameters that people might want to query
	float			_scale;
	float			_xOffset;
	float			_yOffset;
};

};

#endif
