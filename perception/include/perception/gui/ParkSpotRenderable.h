#pragma once
#ifndef PTK_GUI_PARKSPOTRENDERABLE_H
#define PTK_GUI_PARKSPOTRENDERABLE_H
/**
 * Copyright (C) 2018 Texas Instruments Incorporated - http://www.ti.com/
 * All Rights Reserved.
 * The recipient shall have no right to copy or distribute or make any
 * use of this code, except as expressly provided in a separate written
 * license, executed between recipient and Texas Instruments.

 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 * OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 * SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 * THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <vector>

#include <perception/gui/Renderable.h>
#include <perception/gui/SharedStatic.h>
#include <perception/gui/Shader.h>
#include <perception/gui/String.h>

#define	PARKING_SPOT_MIN_CNN_DETECT_PROB	0.9f
#define PARKING_SPOT_COMPACT_MIN_WIDTH		2.4384f	// 8 FT
#define PARKING_SPOT_COMPACT_MIN_DEPTH		4.8768f	// 16 FT
#define PARKING_SPOT_STANDARD_MIN_WIDTH		2.7432f	// 9 FT
#define PARKING_SPOT_STANDARD_MIN_DEPTH		5.4864f	// 18 FT

/*  Parking spot determined by four vertice (either in Image Coordinate System (ICS) 
	World Coordinate System (WCS). The former assumes a 512x512 input image while the 
	latter assumes the map space  */
typedef struct {
	uint16_t	parkingSpotAvailability; /* 1 == available parking spot (free), 2 == occupied parking spot */
	float		detectionProbability;
	float		x[4];   /*! x-coordinates for the vertices */
	float		y[4];   /*! y-coordinates for the vertices */
} alg_parking_CNN_psd_desc_t;

typedef struct {
	alg_parking_CNN_psd_desc_t *cnnSpot;
	float		width;	/*! minimum length of the quad */
	float		depth;	/*! minimum length between the sides of the quad */
	float		len[4];	/*! length of each side of the quad */
} alg_validParkingSpot_t;

enum class alg_parking_coord_Space
{
	ICS_512x512 = 0,
	WCS_MAP
};

namespace ptk {

class ParkSpotRenderable : public Renderable, public SharedStatic<ParkSpotRenderable>
{
public:
	ParkSpotRenderable(Renderer *r);
	virtual ~ParkSpotRenderable();

	virtual void render(uint64_t time, Dimensions d);

	float getLineLength(float x1, float y1, float x2, float y2);
	void setCNNPSDDetections(alg_parking_CNN_psd_desc_t *spots, uint32_t numSpots, alg_parking_coord_Space cs, uint32_t camRef);
	void setCameraRatio(uint32_t camRef);
	void setLabel(alg_validParkingSpot_t *validSpots, uint32_t n);
	void pruneLabels(uint32_t n);

	alg_parking_CNN_psd_desc_t*		getSpots(void) { return	_spots; }

public:
	static void initStatic();
	static void deinitStatic();

private:
	static Shader			_shader;
	static GLint			_v_coords;
	static GLint			_u_color;
	static GLint			_u_mvp;
	static GLint			_u_z;

private:
	GLuint					_vao[1]{0};
	GLuint					_vbo[2]{0};

	uint32_t				_numSpots{0};
	uint32_t				_numValidSpots{0};
	uint32_t				_numCameras{0};
	std::vector<String *>	_labels;

	float					_ICS_scaleX[4]{1};	// Support for up to four camera sensors
	float					_ICS_scaleY[4]{1};
	float					_WCS_scaleX{0.01f};
	float					_WCS_scaleY{0.01f};
	float					_offsetX{0.5f};
	float					_offsetY{0.5f};

	alg_parking_CNN_psd_desc_t		*_spots{nullptr};
	alg_validParkingSpot_t			*_validSpots{nullptr};

private:
	bool chkValidSpot(alg_parking_CNN_psd_desc_t *spot);
};

};

#endif
