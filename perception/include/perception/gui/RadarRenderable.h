/*
 *
 * Copyright (c) 2018 Texas Instruments Incorporated
 *
 * All rights reserved not granted herein.
 *
 * Limited License.
 *
 * Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
 * license under copyrights and patents it now or hereafter owns or controls to make,
 * have made, use, import, offer to sell and sell ("Utilize") this software subject to the
 * terms herein.  With respect to the foregoing patent license, such license is granted
 * solely to the extent that any such patent is necessary to Utilize the software alone.
 * The patent license shall not apply to any combinations which include this software,
 * other than combinations with devices manufactured by or for TI ("TI Devices").
 * No hardware patent is licensed hereunder.
 *
 * Redistributions must preserve existing copyright notices and reproduce this license
 * (including the above copyright notice and the disclaimer and (if applicable) source
 * code license limitations below) in the documentation and/or other materials provided
 * with the distribution
 *
 * Redistribution and use in binary form, without modification, are permitted provided
 * that the following conditions are met:
 *
 * *       No reverse engineering, decompilation, or disassembly of this software is
 * permitted with respect to any software provided in binary form.
 *
 * *       any redistribution and use are licensed by TI for use only with TI Devices.
 *
 * *       Nothing shall obligate TI to provide you with source code for the software
 * licensed and provided to you in object code.
 *
 * If software source code is provided to you, modification and redistribution of the
 * source code are permitted provided that the following conditions are met:
 *
 * *       any redistribution and use of the source code, including any resulting derivative
 * works, are licensed by TI for use only with TI Devices.
 *
 * *       any redistribution and use of any object code compiled from the source code
 * and any resulting derivative works, are licensed by TI for use only with TI Devices.
 *
 * Neither the name of Texas Instruments Incorporated nor the names of its suppliers
 *
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * DISCLAIMER.
 *
 * THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
#pragma once
#ifndef PTK_RADAR_RENDERABLE_H
#define PTK_RADAR_RENDERABLE_H

/**
 * @file RadarRenderable.h
 * @brief Show a radar target display on screen
 */

#include <perception/gui/GLlibs.h>

#include <perception/gui/Shader.h>
#include <perception/gui/Renderable.h>
#include <perception/gui/SharedStatic.h>

namespace ptk {

// Temporarily define radar stuff here because we have nowhere else to put it
#define PTK_MAX_RADAR_DETECTIONS 400

struct ptk_radar_detection_t
{
	float range;
	float azimuthAngle;
	float velocity;
	float doaVarEst;
	float snr;
};

struct ptk_radar_detection_list_t
{
	int32_t							numObj;
	struct ptk_radar_detection_t	objInfo[PTK_MAX_RADAR_DETECTIONS];
};

typedef struct ptk_radar_detection_t PTK_RadarDetection;
typedef struct ptk_radar_detection_list_t PTK_RadarDetectionList;
// End of radar stuff to be relocated

class RadarRenderable : public Renderable, public SharedStatic<RadarRenderable>
{
public:
	RadarRenderable(Renderer *);
	~RadarRenderable();

    // Disable copy constructor and assignment operator.
    RadarRenderable(const RadarRenderable&) = delete;
    RadarRenderable& operator=(const RadarRenderable&) = delete;

	virtual void render(uint64_t delta, Dimensions d);

	void loadBGMesh();
	void loadMesh();
	void loadVertexData();
	void setDetectionList(PTK_RadarDetectionList *list);

public:
    static void initStatic();
    static void deinitStatic();

private:
	GLuint						_vao[2];        // vertex array object 
	GLuint						_bgVBO[3];      // vertex buffer object for background
	GLuint						_targetVBO[3];  // vertex buffer object for foreground

	float						_Rmax;
	float						_thetaMax;

	PTK_RadarDetectionList		_data;
	glm::vec3				*	_targets;			// elements for x,y,z so that polar coordinates can be mapped to Cartesian for display. 
													// The z will be held for velocity; TODO move the z element to _targetMetaData -- DJM
	glm::vec3				*	_targetMetaData;	// elements hold values for range, doaVarEst & snr  -- DJM

private:
	static Shader		_shader;
	static GLint		_v_coords;
	static GLint		_u_origin;
	static GLint		_u_color;
	static GLint		_u_scale;

	static Shader		_bgShader;
	static GLint		_bg_v_coords;
	static GLint		_bg_u_color;
};

};

#endif
