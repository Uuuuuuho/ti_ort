/*
 *
 * Copyright (c) 2018 Texas Instruments Incorporated
 *
 * All rights reserved not granted herein.
 *
 * Limited License.
 *
 * Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
 * license under copyrights and patents it now or hereafter owns or controls to make,
 * have made, use, import, offer to sell and sell ("Utilize") this software subject to the
 * terms herein.  With respect to the foregoing patent license, such license is granted
 * solely to the extent that any such patent is necessary to Utilize the software alone.
 * The patent license shall not apply to any combinations which include this software,
 * other than combinations with devices manufactured by or for TI ("TI Devices").
 * No hardware patent is licensed hereunder.
 *
 * Redistributions must preserve existing copyright notices and reproduce this license
 * (including the above copyright notice and the disclaimer and (if applicable) source
 * code license limitations below) in the documentation and/or other materials provided
 * with the distribution
 *
 * Redistribution and use in binary form, without modification, are permitted provided
 * that the following conditions are met:
 *
 * *       No reverse engineering, decompilation, or disassembly of this software is
 * permitted with respect to any software provided in binary form.
 *
 * *       any redistribution and use are licensed by TI for use only with TI Devices.
 *
 * *       Nothing shall obligate TI to provide you with source code for the software
 * licensed and provided to you in object code.
 *
 * If software source code is provided to you, modification and redistribution of the
 * source code are permitted provided that the following conditions are met:
 *
 * *       any redistribution and use of the source code, including any resulting derivative
 * works, are licensed by TI for use only with TI Devices.
 *
 * *       any redistribution and use of any object code compiled from the source code
 * and any resulting derivative works, are licensed by TI for use only with TI Devices.
 *
 * Neither the name of Texas Instruments Incorporated nor the names of its suppliers
 *
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * DISCLAIMER.
 *
 * THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
#pragma once
#ifndef PTK_GUI_DIMENSIONS_H
#define PTK_GUI_DIMENSIONS_H

#include <cstdint>
#include <limits>
#include <cmath>
#include <algorithm>

/**
 * Represents dimensions on screen as a number of integer pixels
 */
struct PixelDimensions
{
	uint32_t x;
	uint32_t y;
	uint32_t w;
	uint32_t h;
	PixelDimensions() : x(0), y(0), w(0), h(0) {}
	PixelDimensions(uint32_t px, uint32_t py, uint32_t pw, uint32_t ph) :
		x(px),
		y(py),
		w(pw),
		h(ph)
	{}
};

/**
 * Represents dimensions on screen as a % of an element (between 0 and 1)
 */
struct Dimensions
{
	float x;
	float y;
	float w;
	float h;
	Dimensions() :
		Dimensions(0.0f, 0.0f, 1.0f, 1.0f)
	{}
	Dimensions(float px, float py, float pw, float ph) :
		x(px),
		y(py),
		w(pw),
		h(ph)
	{}

	bool operator!=(const Dimensions& rhs)
	{
		return !(almost_equal(x, rhs.x) && almost_equal(y, rhs.y)
				&& almost_equal(w, rhs.w) && almost_equal(h, rhs.h));
	}

	PixelDimensions selectRegionOf(PixelDimensions pd)
	{
		return PixelDimensions(
			pd.x + static_cast<uint32_t>(x * pd.w),
			pd.y + static_cast<uint32_t>(y * pd.h),
			static_cast<uint32_t>(pd.w * w),
			static_cast<uint32_t>(pd.h * h)
		);
	}

	Dimensions selectRegionOf(Dimensions d)
	{
		return Dimensions(
			d.x + x*d.w,
			d.y + y*d.h,
			d.w * w,
			d.h * h
		);
	}

private:
	bool almost_equal(float a, float b)
	{
		return (std::abs(a-b) <= std::numeric_limits<float>::epsilon() * std::abs(a+b));
	}

};

#endif
