/*
 *
 * Copyright (c) 2018 Texas Instruments Incorporated
 *
 * All rights reserved not granted herein.
 *
 * Limited License.
 *
 * Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
 * license under copyrights and patents it now or hereafter owns or controls to make,
 * have made, use, import, offer to sell and sell ("Utilize") this software subject to the
 * terms herein.  With respect to the foregoing patent license, such license is granted
 * solely to the extent that any such patent is necessary to Utilize the software alone.
 * The patent license shall not apply to any combinations which include this software,
 * other than combinations with devices manufactured by or for TI ("TI Devices").
 * No hardware patent is licensed hereunder.
 *
 * Redistributions must preserve existing copyright notices and reproduce this license
 * (including the above copyright notice and the disclaimer and (if applicable) source
 * code license limitations below) in the documentation and/or other materials provided
 * with the distribution
 *
 * Redistribution and use in binary form, without modification, are permitted provided
 * that the following conditions are met:
 *
 * *       No reverse engineering, decompilation, or disassembly of this software is
 * permitted with respect to any software provided in binary form.
 *
 * *       any redistribution and use are licensed by TI for use only with TI Devices.
 *
 * *       Nothing shall obligate TI to provide you with source code for the software
 * licensed and provided to you in object code.
 *
 * If software source code is provided to you, modification and redistribution of the
 * source code are permitted provided that the following conditions are met:
 *
 * *       any redistribution and use of the source code, including any resulting derivative
 * works, are licensed by TI for use only with TI Devices.
 *
 * *       any redistribution and use of any object code compiled from the source code
 * and any resulting derivative works, are licensed by TI for use only with TI Devices.
 *
 * Neither the name of Texas Instruments Incorporated nor the names of its suppliers
 *
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * DISCLAIMER.
 *
 * THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
#ifndef PTK_GUI_FONT_H
#define PTK_GUI_FONT_H

#include <perception/gui/GLlibs.h>

#include <glm/glm.hpp>
#include <glm/gtc/type_precision.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <ft2build.h>
#include FT_FREETYPE_H

#include <iostream>
#include <string>

namespace ptk {

class Renderer;

class Font {
public:
	/**
	 * char_info is used to store the necessary information to draw a quad representing
	 * a specific character using this Font
	 */
	struct char_info {
		// Screen normalized
		GLushort ax;	//!< advance.x

		// Screen normalized coordinates for positioning
		GLushort sbw;	//!< bitmap.width;
		GLushort bl;	//!< bitmap_left;

		// Texture normalized which get copied verbatim
		GLushort tx;	//!< x offset of glyph within the texture
		GLushort txEnd;	//!< end x offset of glyph within texture
	};

private:
	int					_id;
	bool				_init;
	float				_height;

	Renderer		*	_renderer;
	std::string			_path;
	int					_pixelSize;

	GLuint				_texture;

	GLshort				_texHeight;			// Normalized to screen coordinates
	GLshort				_maxDescender;		// Absolute value of the max descent value
	GLushort			_maxWidth;			// Maximum character width using advance.x

	FT_Bool				_kerning;
	FT_Face				_face;
	char_info			_info[256];

public:
	Font(int id, Renderer *r, const std::string& path, int size) :
		_id(id),
		_init(false),
		_height(0.0f),
		_renderer(r),
		_path(path),
		_pixelSize(size),
		_texture(0),
		_texHeight(0),
		_maxDescender(0),
		_maxWidth(0)
	{}
	~Font(void);

	GLuint getMaxStringWidth(int count);
	GLuint getStringWidth(const std::string& text);
	float getStringWidthf(const std::string& text);
	GLshort getKerning(FT_UInt left, FT_UInt right);

	const std::string& getFontPath() const { return _path; }
	int getId(void) const { return _id; }
	int getSize(void) const { return _pixelSize; }

	/**
	 * Return the texture id that serves as a handle to the opengl texture
	 * @return OpenGL Texture ID for this font atlas
	 */
	GLuint getTextureId(void) const { return _texture; }

	/**
	 * Get the char_info struct for a specific character
	 * @param c The character whose info we need to look up
	 * @return pointer to a character info struct for the character
	 */
	const char_info *getCharInfo(unsigned char c) const { return &_info[c]; }

	/**
	 * Get the height of this font when drawn on screen, in normalized coordinates
	 * @return Height of this font as a float, in normalized coordinates
	 */
	float getHeight(void) const { return _height; }

	/**
	 * Get the height of this texture in normalized coordinates. I'm not sure
	 * that this is different from getHeight(), aside from already being a short
	 * @return The texture height in normalized coordinates, already as a short
	 */
	GLshort getTexHeight(void) const { return _texHeight; }

	/**
	 * Get the absolute value of the maximum descent below the baseline. Alternatively,
	 * this could be interpreted as the offset of the baseline above the bottom of
	 * the texture.
	 * @return The absolute value of the maximum descender below the font baseline, normalized
	 */
	GLshort getMaxDescender(void) const { return _maxDescender; }

public:
	static Font *loadFont(int id, Renderer *r, const std::string& path, int size);
};

};

#endif
