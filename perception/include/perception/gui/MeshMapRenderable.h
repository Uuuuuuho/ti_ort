/*
 *
 * Copyright (c) 2018 Texas Instruments Incorporated
 *
 * All rights reserved not granted herein.
 *
 * Limited License.
 *
 * Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
 * license under copyrights and patents it now or hereafter owns or controls to make,
 * have made, use, import, offer to sell and sell ("Utilize") this software subject to the
 * terms herein.  With respect to the foregoing patent license, such license is granted
 * solely to the extent that any such patent is necessary to Utilize the software alone.
 * The patent license shall not apply to any combinations which include this software,
 * other than combinations with devices manufactured by or for TI ("TI Devices").
 * No hardware patent is licensed hereunder.
 *
 * Redistributions must preserve existing copyright notices and reproduce this license
 * (including the above copyright notice and the disclaimer and (if applicable) source
 * code license limitations below) in the documentation and/or other materials provided
 * with the distribution
 *
 * Redistribution and use in binary form, without modification, are permitted provided
 * that the following conditions are met:
 *
 * *       No reverse engineering, decompilation, or disassembly of this software is
 * permitted with respect to any software provided in binary form.
 *
 * *       any redistribution and use are licensed by TI for use only with TI Devices.
 *
 * *       Nothing shall obligate TI to provide you with source code for the software
 * licensed and provided to you in object code.
 *
 * If software source code is provided to you, modification and redistribution of the
 * source code are permitted provided that the following conditions are met:
 *
 * *       any redistribution and use of the source code, including any resulting derivative
 * works, are licensed by TI for use only with TI Devices.
 *
 * *       any redistribution and use of any object code compiled from the source code
 * and any resulting derivative works, are licensed by TI for use only with TI Devices.
 *
 * Neither the name of Texas Instruments Incorporated nor the names of its suppliers
 *
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * DISCLAIMER.
 *
 * THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
#pragma once
#ifndef PTK_GUI_MESHMAPRENDERABLE_H
#define PTK_GUI_MESHMAPRENDERABLE_H

/**
 * @file MeshMapRenderable.h
 * @brief The Mesh Map draws a 3D representation of a map with information drawn from one grid and
 *        color drawn from another (or also the height) according to a GridView
 */

#include <perception/gui/GLlibs.h>

#include <perception/gui/Shader.h>
#include <perception/gui/SharedStatic.h>
#include <perception/gui/Renderable.h>
#include <perception/gui/BMPGridView.h>
#include <perception/gui/HasColormap.h>

namespace ptk {

class Colormap;

class MeshMapRenderable :
	public Renderable,
	public HasColormap,
	public SharedStatic<MeshMapRenderable>
{
public:
	MeshMapRenderable(Renderer *r);
	virtual ~MeshMapRenderable();

	virtual void setMap(PTK_Map *map);
	virtual void render(uint64_t time, Dimensions d);

	void setROI(PTK_GridRoi *roi);
	void setHeightID(uint32_t height) { _heightID = height; }
	void loadROICoordinates();
	void loadXYCoordinates();
	void loadZCoordinates();

public:
	static void initStatic();
	static void deinitStatic();

private:
	static Shader		_shader;
	static GLint		_v_coords;
	static GLint		_u_z;
	static GLint		_u_start;
	static GLint		_u_size;
	static GLint		_u_mvp;
	static GLint		_u_color;
	static GLint		_u_norm;

private:
	GLuint				_vao;
	GLuint				_vbo[4];
	GLuint				_texture[3];
	PTK_Map			*	_map;
	uint32_t			_heightID;

	uint32_t			_xCells;
	uint32_t			_yCells;
	float				_xStart;
	float				_yStart;
	float				_xCellSize;
	float				_yCellSize;

	PTK_GridRoi			_roi;
	uint32_t			_lineElements;
	uint32_t			_quadElements;

	bool				_zLoaded;
	bool				_xyLoaded;
	bool				_roiLoaded;
};

};

#endif
