/*
 *
 * Copyright (c) 2019 Texas Instruments Incorporated
 *
 * All rights reserved not granted herein.
 *
 * Limited License.
 *
 * Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
 * license under copyrights and patents it now or hereafter owns or controls to make,
 * have made, use, import, offer to sell and sell ("Utilize") this software subject to the
 * terms herein.  With respect to the foregoing patent license, such license is granted
 * solely to the extent that any such patent is necessary to Utilize the software alone.
 * The patent license shall not apply to any combinations which include this software,
 * other than combinations with devices manufactured by or for TI ("TI Devices").
 * No hardware patent is licensed hereunder.
 *
 * Redistributions must preserve existing copyright notices and reproduce this license
 * (including the above copyright notice and the disclaimer and (if applicable) source
 * code license limitations below) in the documentation and/or other materials provided
 * with the distribution
 *
 * Redistribution and use in binary form, without modification, are permitted provided
 * that the following conditions are met:
 *
 * *       No reverse engineering, decompilation, or disassembly of this software is
 * permitted with respect to any software provided in binary form.
 *
 * *       any redistribution and use are licensed by TI for use only with TI Devices.
 *
 * *       Nothing shall obligate TI to provide you with source code for the software
 * licensed and provided to you in object code.
 *
 * If software source code is provided to you, modification and redistribution of the
 * source code are permitted provided that the following conditions are met:
 *
 * *       any redistribution and use of the source code, including any resulting derivative
 * works, are licensed by TI for use only with TI Devices.
 *
 * *       any redistribution and use of any object code compiled from the source code
 * and any resulting derivative works, are licensed by TI for use only with TI Devices.
 *
 * Neither the name of Texas Instruments Incorporated nor the names of its suppliers
 *
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * DISCLAIMER.
 *
 * THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
#if !defined(_PTK_NET_UTILS_H_ )
#define _PTK_NET_UTILS_H_

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <fcntl.h>
#include <netdb.h>
#include <sys/epoll.h>
#include <errno.h>
#include <strings.h>

#include <perception/utils/bitmap_mem_allocator.h>

/**
 * \defgroup group_ptk_net_utils PTK Network Utilities
 * \ingroup group_ptk_algos
 *
 */

#ifdef __cplusplus
extern "C" {
#endif

/**
 * \brief Maximum Simultaneous client connections.
 * \ingroup group_ptk_net_utils
 */
#define PTK_MAX_NET_LISTEN_CNT            5

/**
 * \brief Maximum transfer size per read/write request - 256 KB
 * \ingroup group_ptk_net_utils
 */
#define PTK_NET_MAX_XFER_SIZE             (256 * 1024)

/**
 * \brief Failure return code used by the network utility component.
 * \ingroup group_ptk_net_utils
 */
#define PTK_NET_RET_FAILURE               (-1)

/**
 * \brief Success return code used by the network utility component.
 * \ingroup group_ptk_net_utils
 */
#define PTK_NET_RET_SUCCESS               0

/**
 * \brief Value indicating that the read/write operation in case of
 *        non-blocking socket is not complete.
 * \ingroup group_ptk_net_utils
 */
#define PTK_NET_RET_PENDING               1

/**
 * \brief Error creating a socket.
 * \ingroup group_ptk_net_utils
 */
#define PTK_NET_SOCK_CREATE_ERROR         (PTK_NET_RET_FAILURE-2)

/**
 * \brief Error during the server bind() call.
 * \ingroup group_ptk_net_utils
 */
#define PTK_NET_SOCK_BIND_ERROR           (PTK_NET_RET_FAILURE-3)

/**
 * \brief Error during the server listen() call.
 * \ingroup group_ptk_net_utils
 */
#define PTK_NET_SOCK_LISTEN_ERROR         (PTK_NET_RET_FAILURE-4)

/**
 * \brief Error during the connect() call.
 * \ingroup group_ptk_net_utils
 */
#define PTK_NET_SOCK_CONNECT_ERROR        (PTK_NET_RET_FAILURE-5)

/**
 * \brief Error reading a socket.
 * \ingroup group_ptk_net_utils
 */
#define PTK_NET_SOCK_RCV_ERROR            (PTK_NET_RET_FAILURE-6)

/**
 * \brief Error writing to a socket.
 * \ingroup group_ptk_net_utils
 */
#define PTK_NET_SOCK_SEND_ERROR           (PTK_NET_RET_FAILURE-7)

/**
 * \brief Error occured during setting the blocking mode on a socket.
 * \ingroup group_ptk_net_utils
 */
#define PTK_NET_SOCK_FCNTL_ERROR          (PTK_NET_RET_FAILURE-8)

/**
 * \brief Error indicating that the peer entiry has closed the socket.
 * \ingroup group_ptk_net_utils
 */
#define PTK_NET_SOCK_PEER_SHUTDOWN        (PTK_NET_RET_FAILURE-9)

/**
 * \brief Initial set for the client connection for concurrent server
 *        connections.
 * \ingroup group_ptk_net_utils
 */
#define PTK_NET_SOCK_STATE_INVALID        0x00

/**
 * \brief Enumeration for specifying the socket blocking property.
 *
 * \ingroup group_ptk_net_utils
 */
typedef enum
{
    /** Set the blocking mode. */
    PTK_IoCtl_BLOCKING     = 0,

    /** Reset the blocking mode. */
    PTK_IoCtl_NON_BLOCKING = 1

} PTK_IoCtl;

/**
 * \brief Network server type.
 *
 * \ingroup group_ptk_net_utils
 */
typedef enum
{
    /** Iterative server. Will handle only one client connection at a time. */
    PTK_NetSvrType_ITER   = 0,

    /** Concurrent server. Can handle multiple client connections. */
    PTK_NetSvrType_CONCUR = 1

} PTK_NetSvrType;

/**
 * \brief Network socket type.
 *
 * \ingroup group_ptk_net_utils
 */
typedef enum
{
    /** TCP socket. */
    PTK_NetSockType_TCP       = 0,

    /** UDP socket type. */
    PTK_NetSockType_UDP       = 1,

    /** UNIX socket type. */
    PTK_NetSockType_UNIX_STRM = 2,

    /** Marker for error checks. */
    PTK_NetSockType_LAST

} PTK_NetSockType;

/**
 * \brief generic data type container.
 *
 * \ingroup group_ptk_net_utils
 */
typedef union
{
    /** Holds a 32-bit value. */
    uint32_t    u32;

    /** Holds a 64-bit value. */
    uint64_t    u64;

    /** Holds a pointer. */
    void      * ptr;

} PTK_GenDataType;

/**
 * \brief Network address type.
 *
 * \ingroup group_ptk_net_utils
 */
typedef union
{
    /** For TCP/UDP sockets. */
    struct sockaddr_in  inet;

    /** For Unix Domain sockets. */
    struct sockaddr_un  unet;

    /** Generic address structure. */
    struct sockaddr     addr;

} PTK_NetAddr;

/**
 * \brief Network server client cal back signature.
 *
 * \ingroup group_ptk_net_utils
 */
typedef void
(*PTK_netCliHdlr)(void     * cntxt,
                  void     * auxData);

/**
 * \brief Network connection context.
 *
 * \ingroup group_ptk_net_utils
 */
typedef struct
{
    /** Connection state. */
    uint32_t                    state;

    /** Connection Id. */
    uint32_t                    myId;

    /** Client socket Id. */
    int32_t                     sockId;

    /** Number of bytes to send/receive. */
    uint32_t                    rdWrSize;

    /** Next read/write location. */
    uint32_t                    rdWrOffset;

    /** Response payload pointer. */
    void                      * rspPayload;

    /** Application specific auxiliary data. */
    void                      * auxData;

} PTK_NetConnCntxt;

/**
 * \brief Network server context.
 *
 * \ingroup group_ptk_net_utils
 */
typedef struct
{
    /** Server state. */
    uint32_t                    state;

    /** Server socket Id. */
    int32_t                     svrSock;

    /** Generic data type to carry port/pathname. */
    PTK_GenDataType             netParam;

    /** Socket type. */
    PTK_NetSockType             sockType;

    /** Server type - Iterative/Concurrent. */
    PTK_NetSvrType              svrType;

    /** Event context. */
    struct epoll_event          events[PTK_MAX_NET_LISTEN_CNT];

    /** Server Processing function. */
    PTK_netCliHdlr              cliHdlr;

    /** Number of concurrent clients to handle. */
    uint32_t                    listenCnt;

    /** Event mask. */
    uint32_t                    evtMask;

    /** Polling descriptor. This is used only for the concurrent server. */
    int32_t                     epollFd;

    /** Bitmap based allocation for connections. This is used only for the
     *  concurrent server.
     */
    PTK_MemBitmapAllocator      memAllocator;

    /** Connection context used by the concurrent server. */
    PTK_NetConnCntxt          * connCntxt;

    /** Application specific auxiliary data. Useful for iterative server
     *  since 'connCntxt' does not exist.
     */
    void                      * appData;

} PTK_NetSrvCntxt;

/* Function prototypes. */
/**
 * \brief Opens a TCP socket and returns the socket id, if successful.
 *
 * \return
 *        - socket Id, if successful.
 *        - error code, otherwise.
 *
 * \ingroup group_ptk_net_utils
 */
int32_t
PTK_netTcpSockOpen();

/**
 * \brief Initiates a connection on a TCP socket.
 *
 * \param [in] sockId TCP socket id.
 *
 * \param [in] ipAddr IP address of the TCP server to connect to.
 *
 * \param [in] port TCP port number to connect to.
 *
 * \return
 *        - PTK_NET_RET_SUCCESS, if successful.
 *        - error code, otherwise.
 *
 * \ingroup group_ptk_net_utils
 */
int32_t
PTK_netTcpConnect(int32_t       sockId,
                  const char  * ipAddr,
                  uint16_t      port);

/**
 * \brief Opens a UNIX domain socket and returns the socket id, if successful.
 *
 * \return
 *        - socket Id, if successful.
 *        - error code, otherwise.
 *
 * \ingroup group_ptk_net_utils
 */
int32_t
PTK_netUnixSockOpen();

/**
 * \brief Initiates a connection on a UNIX domain socket.
 *
 * \param [in] sockId UNIX domain socket id.
 *
 * \param [in] pathName Unique file path.
 *
 * \return
 *        - PTK_NET_RET_SUCCESS, if successful.
 *        - error code, otherwise.
 *
 * \ingroup group_ptk_net_utils
 */
int32_t
PTK_netUnixConnect(int32_t      sockId,
                   char       * pathName);

/**
 * \brief Opens a UDP socket and returns the socket id, if successful.
 *
 * \return
 *        - socket Id, if successful.
 *        - error code, otherwise.
 *
 * \ingroup group_ptk_net_utils
 */
int32_t
PTK_netUdpSockOpen();

/**
 * \brief Performs a socket read for 'size' number of bytes.
 *
 * \param [in] sockFd Socket descriptor to perform read operation on.
 *
 * \param [out] buff Buffer to read the data into. The buffer should be sized
 *                   to hold atleast 'size' bytes.
 *
 * \param [in] size Size of data in bytes to read.
 *
 * \param [in] addr Address field to be used only when performing a read on
 *                  the socket. This should be set to NULL if not used.
 *
 * \param [in] addrLen Length of the address field . This should be set to NULL
 *                     if 'addr' is set to NULL. Otherwise it should be set to
 *                     the size of the 'addr' data type, in which case the
 *                     value will be modified to set to the exact size of the
 *                     address received.
 *
 * \return
 *        - PTK_NET_RET_SUCCESS, if successful.
 *        - < 0, otherwise.
 *
 * \ingroup group_ptk_net_utils
 */
int32_t
PTK_netReadSock(int32_t                sockFd,
                void                 * buff,
                uint32_t               size,
                struct sockaddr_in   * addr,
                uint32_t             * addrLen);

/**
 * \brief Performs a socket write for 'size' number of bytes.
 *
 * \param [in] sockFd Socket descriptor to perform write operation on.
 *
 * \param [out] buff Buffer to read the data from and send data on the socket.
 *
 * \param [in] size Size of data in bytes to write.
 *
 * \param [in] addr Address field to be used only when performing a write on
 *                  the socket. This should be set to NULL if not used.
 *
 * \param [in] addrLen Length of the address field. This should be set to 0 if
 *                     'addr' is NULL.
 *
 * \return
 *        - PTK_NET_RET_SUCCESS, if successful.
 *        - error code, otherwise.
 *
 * \ingroup group_ptk_net_utils
 */
int32_t
PTK_netWriteSock(int32_t                sockFd,
                 void const           * buff,
                 int32_t                size,
                 struct sockaddr_in   * addr,
                 uint32_t               addrLen);

/**
 * \brief Performs a socket read for 'numBytesToRead' number of bytes.
 *
 * \param [in] sockFd Socket descriptor to perform read operation on.
 *
 * \param [out] dataBuff Buffer to read the data into. The buffer should be
 *                       sized to hold atleast 'size' bytes.
 *
 * \param [in] maxXfrSize Maximum chunk size to issue per read() on the socket.
 *
 * \param [in] numBytesToRead Size of data in bytes to read.
 *
 * \return
 *        - PTK_NET_RET_SUCCESS, if successful.
 *        - error code, otherwise.
 *
 * \ingroup group_ptk_net_utils
 */
int32_t
PTK_netReadMsg(int32_t      sockFd,
               void       * dataBuff,
               uint32_t     maxXfrSize,
               uint32_t     numBytesToRead);

/**
 * \brief Performs a socket write for 'numBytesToWrite' number of bytes.
 *
 * \param [in] sockFd Socket descriptor to perform write operation on.
 *
 * \param [out] dataBuff Buffer to read the data from and send data on the
 *              socket.
 *
 * \param [in] maxXfrSize Maximum chunk size to issue per send() on the socket.
 *
 * \param [in] numBytesToWrite numBytesToWrite of data in bytes to write.
 *
 * \return
 *        - PTK_NET_RET_SUCCESS, if successful.
 *        - error code, otherwise.
 *
 * \ingroup group_ptk_net_utils
 */
int32_t
PTK_netSendMsg(int32_t      sockFd,
               void const * dataBuff,
               uint32_t     maxXfrSize,
               uint32_t     numBytesToWrite);

/**
 * \brief Performs a non-blocking read on the connection identified by
 *        'netConnPtr'. The information on the client socket Id, total data
 *        size to read and the current state of the read is stored in this
 *        context.
 *
 * \param [inout] svrCntxt Server context.
 *
 * \param [inout] netConnPtr Client connection context.
 *
 * \param [out] buff Buffer to read data into.
 *
 * \return
 *        - PTK_NET_RET_SUCCESS, if read operation is complete
 *        - PTK_NET_RET_PENDING, if read operation incomplete and there is more
 *          data to read. Since the read is of type non-blocking type, this is a
 *          normal case.
 *        - error code, otherwise.
 *
 * \ingroup group_ptk_net_utils
 */
int32_t
PTK_netSockNonBlockRead(PTK_NetSrvCntxt     * svrCntxt,
                        PTK_NetConnCntxt    * netConnPtr,
                        char                * buff);

/**
 * \brief Performs a non-blocking write on the connection identified by
 *        'netConnPtr'. The information on the client socket Id, total data
 *        size to write and the current state of the write is stored in this
 *        context.
 *
 * \param [inout] svrCntxt Server context.
 *
 * \param [inout] netConnPtr Client connection context.
 *
 * \param [out] buff Buffer to read data from and write on the socket.
 *
 * \return
 *        - PTK_NET_RET_SUCCESS, if write operation is complete
 *        - PTK_NET_RET_PENDING, if write operation incomplete and there is more
 *          data to write. Since the read is of type non-blocking type, this is
 *          a normal case.
 *        - error code, otherwise.
 *
 * \ingroup group_ptk_net_utils
 */
int32_t
PTK_netSockNonBlockWrite(PTK_NetSrvCntxt    * svrCntxt,
                         PTK_NetConnCntxt   * netConnPtr,
                         char               * buff);

/**
 * \brief Sets the blocking mode on the given socket.
 *
 * \param [in] sockId Socket Id.
 *
 * \param [in] ioCtl Option specified for blocking/non-blocking.
 *
 * \return
 *        - PTK_NET_RET_SUCCESS, if successful.
 *        - error code, otherwise.
 *
 * \ingroup group_ptk_net_utils
 */
int32_t
PTK_netSetSockBlockingMode(int32_t    sockId,
                           PTK_IoCtl  ioCtl);

/**
 * \brief Creates a server of a given type (ITER/CONCURRENT) and of given
 *        protocol (TCP/UDP/UNIX) and returns the socket created. If the
 *        server type is ITER then it sets the socket as BLOCKING, otherwise
 *        it sets the socket as NON-BLOCKING. The caller of this function is
 *        responsible for setting up the parameters correctly and need to
 *        handle the incoming connections using accept() call. The API
 *        'PTK_netThread()' does this.
 *
 * \param [in] svrType Operation type of the server.
 *
 * \param [in] sockType Protocol type of the server.
 *
 * \param [in] netParam Network parameters for server creation.
 *
 * \param [in] listenCnt Size of the listen queue for the network connection.
 *
 * \return
 *        - socket Id, if successful.
 *        - error code, otherwise.
 *
 * \ingroup group_ptk_net_utils
 */
int32_t
PTK_netCreateSvrSocket(PTK_NetSvrType   svrType,
                       PTK_NetSockType  sockType,
                       PTK_GenDataType  netParam,
                       uint32_t         listenCnt);

/**
 * \brief Creates a server based on the parameters set in 'svrCntxt'. This is a
 *        blocking call and this function is expected to be used with a thread,
 *        as a thread body.
 *
 * \param [inout] svrCntxt Net server context.
 *
 * \return
 *        - NULL, if error.
 *
 * \ingroup group_ptk_net_utils
 */
void *
PTK_netThread(PTK_NetSrvCntxt  * svrCntxt);

#ifdef __cplusplus
}
#endif

#endif /* !defined(_PTK_NET_UTILS_H_) */

