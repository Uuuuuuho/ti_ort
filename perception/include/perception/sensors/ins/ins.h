/*
 *
 * Copyright (c) 2018 Texas Instruments Incorporated
 *
 * All rights reserved not granted herein.
 *
 * Limited License.
 *
 * Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
 * license under copyrights and patents it now or hereafter owns or controls to make,
 * have made, use, import, offer to sell and sell ("Utilize") this software subject to the
 * terms herein.  With respect to the foregoing patent license, such license is granted
 * solely to the extent that any such patent is necessary to Utilize the software alone.
 * The patent license shall not apply to any combinations which include this software,
 * other than combinations with devices manufactured by or for TI ("TI Devices").
 * No hardware patent is licensed hereunder.
 *
 * Redistributions must preserve existing copyright notices and reproduce this license
 * (including the above copyright notice and the disclaimer and (if applicable) source
 * code license limitations below) in the documentation and/or other materials provided
 * with the distribution
 *
 * Redistribution and use in binary form, without modification, are permitted provided
 * that the following conditions are met:
 *
 * *       No reverse engineering, decompilation, or disassembly of this software is
 * permitted with respect to any software provided in binary form.
 *
 * *       any redistribution and use are licensed by TI for use only with TI Devices.
 *
 * *       Nothing shall obligate TI to provide you with source code for the software
 * licensed and provided to you in object code.
 *
 * If software source code is provided to you, modification and redistribution of the
 * source code are permitted provided that the following conditions are met:
 *
 * *       any redistribution and use of the source code, including any resulting derivative
 * works, are licensed by TI for use only with TI Devices.
 *
 * *       any redistribution and use of any object code compiled from the source code
 * and any resulting derivative works, are licensed by TI for use only with TI Devices.
 *
 * Neither the name of Texas Instruments Incorporated nor the names of its suppliers
 *
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * DISCLAIMER.
 *
 * THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
#pragma once
#ifndef PTK_INS_H
#define PTK_INS_H

/**
 * @file ins.h
 * @brief definitions and declarations of generic ins/imu API
 */

#include <stdint.h>
#include <perception/base/position.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * \brief INS record type enumeration.
 *
 * \ingroup group_ptk_positioning
 */
typedef enum
{
    /** INS-computed position, velocity, and attitude information. */
    PTK_INS_RECORD_TYPE_INSPVA = 0,

    /** covariance information for position, velocity, and attitude records. */
    PTK_INS_RECORD_TYPE_INSCOV,

    /** Tag to detect maximum allowed enumeration. */
    PTK_INS_RECORD_TYPE_MAX

} PTK_INS_RecordType;

/**
 * \brief Error code enumeration.
 *
 * \ingroup group_ptk_positioning
 */
typedef enum
{
    /** The requested record was retrieved without issue. */
    PTK_INS_RETURN_CODE_OK = 0,

    /** The requested record was not completely available but a similar record
     *  was provided instead.
     */
    PTK_INS_RETURN_CODE_RECORD_PARTIALLY_AVAIL,

    /** The requested record was not available and nothing was retrieved. */
    PTK_INS_RETURN_CODE_RECORD_UNAVAIL,

    /** Invalid arguments were provided as part of the request for an IMU
     *  record.
     */
    PTK_INS_RETURN_CODE_ILLEGAL_ARGIN,

    /** Invalid record type requested.  */
    PTK_INS_RETURN_CODE_INVALID_RECORD_TYPE,

    /** An undefined failure happened. This should not be observed but may
     *  indicate hardware or other problems.
     */
    PTK_INS_RETURN_CODE_UNEXPECTED_BEHAVIOR

} PTK_INS_RetCode;

/**
 * \brief INS reported status code enumeration.
 *
 * \ingroup group_ptk_positioning
 */
typedef enum
{
    /** INS solution was computed accurately, according to the sensor. */
    PTK_INS_SOL_STATUS_GOOD = 0,

    /** INS solution may be inaccurate, according to sensor, for any reason. */
    PTK_INS_SOL_STATUS_NOT_GOOD,

    /** INS solution is incorrect or may simply be dummy data reported by the
     *  driver, according to the sensor.
     */
    PTK_INS_SOL_STATUS_UNAVAILABLE

} PTK_INS_SolStatus;

/**
 * \brief Velocity
 *
 * \ingroup group_ptk_positioning
 */
typedef struct
{
    /** Velovity in the East direction (in ENU) in m/s. */
    float               east;

    /** Velovity in the North direction (in ENU) in m/s. */
    float               north;

    /** Velovity in the Up direction (in ENU) in m/s. */
    float               up;

} PTK_INS_Velocity;

/**
 * \brief Attitude
 *
 * \ingroup group_ptk_positioning
 */
typedef struct
{
    /** Right-handed rotation around East in radians. */
    float               pitch;

    /** Right-handed rotation around North in radians. */
    float               roll;

    /** Right-handed rotation around Up in radians. */
    float               azimuth;

} PTK_INS_Attitude;

/**
 * \brief Ins Position, Velocity, and Attitude information.
 *
 * \ingroup group_ptk_positioning
 */
typedef struct
{
    /** Position information. */
    PTK_Position            position;

    /** Velocity information. */
    PTK_INS_Velocity        velocity;

    /** Attiture information. */
    PTK_INS_Attitude        attitude;

    /** INS status for this record. */
    PTK_INS_SolStatus       status;

    /** Week information reported by GPS. */
    uint32_t                gpsWeek;

    /** Seconds information reported by GPS. */
    double                  gpsSeconds;

} PTK_INS_InsPva;

/**
 * \brief Covariance record definition.
 *
 * \ingroup group_ptk_positioning
 */
typedef struct
{
    /** 3x3 covariance matrix for position data with diagonal = [x y z]. */
    float       position[9];

    /** 3x3 covariance matrix for attitude data with
     *  diagonal = [pitch, roll, azimuth].
     */
    float       attitude[9];

    /** 3x3 covariance matrix for velocity data with
     *  diagonal = [east north up].
     */
    float       velocity[9];

} PTK_INS_MsgInscov;

/**
 * \brief Data type indicator.
 *
 * \ingroup group_ptk_positioning
 */
typedef union
{
    /** Position information variant. */
    PTK_INS_InsPva      inspva;

    /** Position covariance variant. */
    PTK_INS_MsgInscov   inscov;

} PTK_INS_RecordData;

/**
 * \brief INS record
 *
 * \ingroup group_ptk_positioning
 */
typedef struct
{
    /** Timestamp when the data was collected. */
    uint64_t            timestamp;

    /** Indicates which type of data was populated in the union. */
    PTK_INS_RecordType  type;

    /** Actual data written to one fo the union members. */
    PTK_INS_RecordData  data;

} PTK_INS_Record;

/**
 * \brief Initialize internal storage structures. This must be called once
 *        during startup to configure the INS software subsystem for use.
 *
 *        This is independent from the driver initialization or the general
 *        PTK initialization.
 *
 * \ingroup group_ptk_positioning
 */
void PTK_INS_initializeBuffers();

/**
 * \brief Reset the internal storage structures. This can be called anytime
 *        after the initial initialization.
 *
 *        This resets the internal read/write pointers.
 *
 * \ingroup group_ptk_positioning
 */
void PTK_INS_resetBuffers();

/**
 * \brief Copy the most recently written record of the specified type into the 
 *        record struct.
 *
 * \param [in] type The type of record to retrieve.
 *
 * \param [out] dst The record is copied to this struct.
 *
 * \return
 *        - PTK_INS_RETURN_CODE_OK, if record found
 *        - PTK_INS_RETURN_CODE_RECORD_UNAVAIL, no data has been written yet
 *
 * \ingroup group_ptk_positioning
 */
PTK_INS_RetCode PTK_INS_getCurrentRecord(PTK_INS_RecordType   type,
                                         PTK_INS_Record      *dst);

/**
 * \brief Copy the first record before the given timestamp into the given struct.
 *
 * \param [in] type The type of record to retrieve.
 *
 * \param [in] timestamp The timestamp to look for.
 *
 * \param [out] dst The record is copied to this struct.
 *
 * \return
 *        - PTK_INS_RETURN_CODE_OK, if record found
 *        - PTK_INS_RETURN_CODE_RECORD_UNAVAIL, otherwise
 *
 * \ingroup group_ptk_positioning
 */
PTK_INS_RetCode PTK_INS_getRecordBefore(PTK_INS_RecordType    type,
                                        uint64_t              timestamp,
                                        PTK_INS_Record       *dst);
/**
 * \brief Copy the first record after or equal to the given timestamp into the
 *        given struct.
 *
 * \param [in] type The type of record to retrieve.
 *
 * \param [in] timestamp The timestamp to look for.
 *
 * \param [out] dst The record is copied to this struct.
 *
 * \return
 *        - PTK_INS_RETURN_CODE_OK, if record found
 *        - PTK_INS_RETURN_CODE_RECORD_UNAVAIL, otherwise
 *
 * \ingroup group_ptk_positioning
 */
PTK_INS_RetCode PTK_INS_getRecordAfter(PTK_INS_RecordType type,
                                       uint64_t           timestamp,
                                       PTK_INS_Record    *dst);
/**
 * \brief Interpolate between the two records surrounding the given time stamp
 *        and store the computed INS information in the given struct.
 *
 *        If only one of the two records surrounding the given timestamp is
 *        available, it will be copied exactly and
 *        PTK_INS_RETURN_CODE_RECORD_PARTIALLY_AVAIL will be returned.
 *
 *        If both the records are available PTK_INS_RETURN_CODE_OK will be
 *        returned.
 *
 *        If no records are avilable then PTK_INS_RETURN_CODE_RECORD_UNAVAIL
 *        will be returned.
 *
 * \param [in] type The type of record to retrieve.
 *
 * \param [in] timestamp The timestamp to look for
 *
 * \param [out] dst The record is copied to this struct.
 *
 * \return
 *        - PTK_INS_RETURN_CODE_OK
 *        - PTK_INS_RETURN_CODE_RECORD_PARTIALLY_AVAIL
 *        - PTK_INS_RETURN_CODE_RECORD_UNAVAIL
 *
 * \ingroup group_ptk_positioning
 */
PTK_INS_RetCode PTK_INS_getRecordLinearInterp(PTK_INS_RecordType  type,
                                              uint64_t            timestamp,
                                              PTK_INS_Record     *dst);

/**
 * \brief Compute the transformation between the IMU-based coordinate system
 *        (sometimes called ego coordinates) and the ENU frame defined at the
 *        specified position. The resulting transformation is only a rotation.
 *
 * \param [in] imu INS record defining the ENU coordinate system. The type of
 *                 the record must be PTK_INS_RECORD_TYPE_INSPVA.
 *
 * \param [out] M_enu_imu The IMU to ENU coordinate transformation is written here.
 *
 * \return
 *        - PTK_INS_RETURN_CODE_OK
 *        - PTK_INS_RETURN_CODE_INVALID_RECORD_TYPE
 *
 * \ingroup group_ptk_positioning
 */
PTK_INS_RetCode PTK_INS_getIMUtoENU(const PTK_INS_Record   *imu,
                                    PTK_RigidTransform     *M_enu_imu);

/**
 * \brief Get the current reference frame (coordinate system) w.r.t ECEF. The
 *        first call to this function sets the reference frame to the current
 *        INS position oriented as ENU. Subsequent calls simply return this
 *        reference frame.
 *
 *        The reference frame can be cleared using PTK_INS_clearReferenceFrame()
 *
 * \param [out] M_ecef_ref Reference frame.
 *
 * \param [out] ref Position information from the reference record.
 *
 * \return
 *        - PTK_INS_RETURN_CODE_OK if reference frame is available
 *        - PTK_INS_RETURN_CODE_RECORD_UNAVAIL, otherwise
 *
 * \ingroup group_ptk_positioning
 */
PTK_INS_RetCode PTK_INS_getReferenceFrame(PTK_RigidTransform_d *M_ecef_ref,
                                          PTK_Position         *ref);

/**
 * \brief Clears the pointer to the frame previously marked as reference.
 *
 * \ingroup group_ptk_positioning
 */
void PTK_INS_clearReferenceFrame();

/**
 * \brief This gets a writeable pointer to the next data record in our buffer.
 *
 * \param [in] type
 *
 * \return
 *        - Pointer to data structure to write to, if successful.
 *        - NULL, otherwise
 *
 * \ingroup group_ptk_positioning
 */
PTK_INS_Record *PTK_INS_getNextRecord(PTK_INS_RecordType    type);

/**
 * \brief This "publishes" the current data record to the database, advancing
 *        the write pointer and allowing read access to the record it was
 *        just on.
 *
 * \param [in] type
 *
 * \ingroup group_ptk_positioning
 */
void PTK_INS_publishRecord(PTK_INS_RecordType   type);

#ifdef __cplusplus
}
#endif

#endif
