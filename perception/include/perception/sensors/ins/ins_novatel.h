/*
 *
 * Copyright (c) 2018 Texas Instruments Incorporated
 *
 * All rights reserved not granted herein.
 *
 * Limited License.
 *
 * Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
 * license under copyrights and patents it now or hereafter owns or controls to make,
 * have made, use, import, offer to sell and sell ("Utilize") this software subject to the
 * terms herein.  With respect to the foregoing patent license, such license is granted
 * solely to the extent that any such patent is necessary to Utilize the software alone.
 * The patent license shall not apply to any combinations which include this software,
 * other than combinations with devices manufactured by or for TI ("TI Devices").
 * No hardware patent is licensed hereunder.
 *
 * Redistributions must preserve existing copyright notices and reproduce this license
 * (including the above copyright notice and the disclaimer and (if applicable) source
 * code license limitations below) in the documentation and/or other materials provided
 * with the distribution
 *
 * Redistribution and use in binary form, without modification, are permitted provided
 * that the following conditions are met:
 *
 * *       No reverse engineering, decompilation, or disassembly of this software is
 * permitted with respect to any software provided in binary form.
 *
 * *       any redistribution and use are licensed by TI for use only with TI Devices.
 *
 * *       Nothing shall obligate TI to provide you with source code for the software
 * licensed and provided to you in object code.
 *
 * If software source code is provided to you, modification and redistribution of the
 * source code are permitted provided that the following conditions are met:
 *
 * *       any redistribution and use of the source code, including any resulting derivative
 * works, are licensed by TI for use only with TI Devices.
 *
 * *       any redistribution and use of any object code compiled from the source code
 * and any resulting derivative works, are licensed by TI for use only with TI Devices.
 *
 * Neither the name of Texas Instruments Incorporated nor the names of its suppliers
 *
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * DISCLAIMER.
 *
 * THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
#pragma once
#ifndef PTK_INS_NOVATEL_H
#define PTK_INS_NOVATEL_H

/**
 * @file ins_novatel.h
 * @brief Public declarations related to the novatel driver for the ins component of PTK
 *        Internal definitions are elsewhere
 */

#include <stdint.h>

#include <perception/sensors/ins/ins.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * \brief Perform architecture and alignment checks that we assume are true for
 *        the rest of the IMU parsing library and return a status code
 *        indicating whether those checks were passed. This should be called at
 *        startup during development to detect any problems caused by compiler
 *        or achitecture that may affect correct behavior.
 *
 * \return
 *        - PTK_INS_RETURN_CODE_OK, if the check is successful
 *        - PTK_INS_RETURN_CODE_UNEXPECTED_BEHAVIOR, otherwise
 *
 * \ingroup group_ptk_positioning
 */
PTK_INS_RetCode PTK_INS_Novatel_checkArchitecture();

/**
 * \brief Read data out of a INSPVA message, which contains all of our position
 *        and velocity information. This assumes that the packet given is an
 *        INSPVA packet, otherwise the behavior is undefined.
 *
 * \param [in] pRawMessage Pointer to message data received from the INS.
 *
 * \param [in] timestamp Timestamp to tag the data with internally.
 *
 * \param [out] insRec Decoded INSPVA record.
 *
 * \ingroup group_ptk_positioning
 */
void PTK_INS_Novatel_parseINSPVA(uint8_t *pRawMessage, uint64_t timestamp, PTK_INS_Record *insRec);

/**
 * \brief Process the given message and write it to the internal storage
 *        structure where it will be accessible to the generic INS interface
 *        API.
 *
 * \param [in] pRawMessage Pointer to message data received from the INS.
 *
 * \param [in] timestamp Timestamp to tag the data with internally.
 *
 * \return
 *        - PTK_INS_RETURN_CODE_OK, if successful
 *        - PTK_INS_RETURN_CODE_ILLEGAL_ARGIN, otherwise
 *
 * \ingroup group_ptk_positioning
 */
PTK_INS_RetCode PTK_INS_Novatel_processMessage(uint8_t *pRawMessage, uint64_t timestamp);

#ifdef __cplusplus
}
#endif

#endif
