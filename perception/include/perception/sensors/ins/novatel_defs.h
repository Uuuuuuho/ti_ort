/*
 *
 * Copyright (c) 2018 Texas Instruments Incorporated
 *
 * All rights reserved not granted herein.
 *
 * Limited License.
 *
 * Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
 * license under copyrights and patents it now or hereafter owns or controls to make,
 * have made, use, import, offer to sell and sell ("Utilize") this software subject to the
 * terms herein.  With respect to the foregoing patent license, such license is granted
 * solely to the extent that any such patent is necessary to Utilize the software alone.
 * The patent license shall not apply to any combinations which include this software,
 * other than combinations with devices manufactured by or for TI ("TI Devices").
 * No hardware patent is licensed hereunder.
 *
 * Redistributions must preserve existing copyright notices and reproduce this license
 * (including the above copyright notice and the disclaimer and (if applicable) source
 * code license limitations below) in the documentation and/or other materials provided
 * with the distribution
 *
 * Redistribution and use in binary form, without modification, are permitted provided
 * that the following conditions are met:
 *
 * *       No reverse engineering, decompilation, or disassembly of this software is
 * permitted with respect to any software provided in binary form.
 *
 * *       any redistribution and use are licensed by TI for use only with TI Devices.
 *
 * *       Nothing shall obligate TI to provide you with source code for the software
 * licensed and provided to you in object code.
 *
 * If software source code is provided to you, modification and redistribution of the
 * source code are permitted provided that the following conditions are met:
 *
 * *       any redistribution and use of the source code, including any resulting derivative
 * works, are licensed by TI for use only with TI Devices.
 *
 * *       any redistribution and use of any object code compiled from the source code
 * and any resulting derivative works, are licensed by TI for use only with TI Devices.
 *
 * Neither the name of Texas Instruments Incorporated nor the names of its suppliers
 *
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * DISCLAIMER.
 *
 * THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
#ifndef IMU_NOVATEL_DEFS_H
#define IMU_NOVATEL_DEFS_H

/**
 * @file novatel_defs.h
 * @brief Definitions for the Novatel SPAN SE system.
 *
 *        Note: Some packed structures appear to be misaligned.
 *        However, when accounting for the 28 byte header, double accesses are
 *        aligned to 8 byte boundaries, assuming the original network buffer is similarly aligned.
 *        This is considered to be a limitation of the NovaTel IMU. A more robust decoder that does
 *        not assume alignment could be created, at a significant performance cost (individually
 *        realigning and reinterpreting each byte), but that has not shown itself to be necessary.
 */

#include <stdint.h>

#ifdef SYSBIOS
#   define PA_PACK( __Declaration__ ) __Declaration__ 
#elif !defined(LINUX)
#   define PA_PACK( __Declaration__ ) __pragma( pack(push, 1) ) __Declaration__ __pragma( pack(pop) )
#else
#   define PA_PACK( __Declaration__ ) __Declaration__ __attribute__((__packed__))
#endif

#ifdef __cplusplus
extern "C" {
#endif

#define NOVATEL_MSG_SYNC_WORD0              (0xAA)
#define NOVATEL_MSG_SYNC_WORD1              (0x44)
#define NOVATEL_MSG_SYNC_WORD2_LONG_HDR     (0x12)
#define NOVATEL_MSG_SYNC_WORD2_SHORT_HDR    (0x13)

//! Common header to decide on long versus short binary header
PA_PACK(
struct PTK_INS_NovatelBinHdrPrefix
{
    uint8_t     sync0;              //!< Sync byte, expected 0xAA
    uint8_t     sync1;              //!< Sync byte, expected 0x44
    uint8_t     sync2;              //!< Sync byte, expected 0x12 for long, 0x13 for short
});

typedef struct PTK_INS_NovatelBinHdrPrefix PTK_INS_NovatelBinHdrPrefix;

//! Long header definition for binary messages, to match the binary format exactly
PA_PACK(
struct PRK_INS_NovatelBinLongHdrPrefix
{
    uint8_t     sync0;              //!< Sync byte, expected 0xAA
    uint8_t     sync1;              //!< Sync byte, expected 0x44
    uint8_t     sync2;              //!< Sync byte, expected 0x12
    uint8_t     headerLength;       //!< Length of header in bytes

    uint16_t    messageID;          //!< Message ID corresponding to the log received
    uint8_t     messageType;        //!< Type of message received (ASCII, binary, etc.)
    uint8_t     port;               //!< Internal port the message was sent to

    uint16_t    messageLength;      //!< Length of message body, not including CRC
    uint16_t    sequence;           //!< Sequence number of message, for multi-message logs

    uint8_t     idle;               //!< Time processor spent idle, 0-200 maps over 0-100%
    uint8_t     gpsTimeStatus;      //!< GPS Time status enum value (Page 221, Table 43)
    uint16_t    gpsWeek;            //!< GPS week number

    uint32_t    gpsMs;              //!< Milliseconds since start of current gps week

    uint32_t    receiverStatus;     //!< Receiver status words

    uint16_t    reserved;           //!< Reserved, unused field
    uint16_t    receiverSWVersion;  //!< Version number of receiver software
});

typedef struct PRK_INS_NovatelBinLongHdrPrefix PRK_INS_NovatelBinLongHdrPrefix;

//! Short header definition for binary messages, to match the binary format exactly
PA_PACK(
struct PTK_INS_NovatelBinShortHdr
{
    uint8_t     sync0;              //!< Sync byte, expected 0xAA
    uint8_t     sync1;              //!< Sync byte, expected 0x44
    uint8_t     sync2;              //!< Sync byte, expected 0x13
    uint8_t     messageLength;      //!< Length of message body, not including CRC

    uint16_t    messageID;          //!< Message ID corresponding to the log received
    uint16_t    gpsWeek;            //!< GPS week number

    uint32_t    gpsMs;              //!< Milliseconds since start of current gps week
});

typedef struct PTK_INS_NovatelBinShortHdr PTK_INS_NovatelBinShortHdr;

//! Message layout in memory for an INSPVA and INSPVAS binary message
PA_PACK(
struct PTK_INS_NovatelMsgInspva
{
    uint32_t    gpsWeek;
    double      gpsSecond;
    double      latitude;           //!< wgs84 latitude, in degrees
    double      longitude;          //!< wgs84 longitude, in degrees
    double      height;             //!< wgs84 height, in m
    double      velocityNorth;      //!< velocity in north direction, in m/s
    double      velocityEast;       //!< velocity in east direction, in m/s
    double      velocityUp;         //!< velocity in up direction, in m/s
    double      roll;               //!< roll angle, in degrees
    double      pitch;              //!< pitch angle, in degrees
    double      azimuth;            //!< azimuth angle, in degrees
    uint32_t    status;             //!< INS solution status word
    uint32_t    crc32;              //!< 32-bit CRC
});

typedef struct PTK_INS_NovatelMsgInspva PTK_INS_NovatelMsgInspva;

//! Message layout in memory for an INSCOV and INSCOVS binary message
PA_PACK(
struct PTK_INS_NovatelMsgInscov
{
    uint32_t    gpsWeek;
    double      gpsSecond;
    double      position[9];
    double      attitude[9];
    double      velocity[9];
    uint32_t    crc32;              //!< 32-bit CRC
});

typedef struct PTK_INS_NovatelMsgInscov PTK_INS_NovatelMsgInscov;

//! Message layout in memory for an BESTPOS and BESTGPSPOS binary message
PA_PACK(
struct PTK_INS_NovatelMsgBestGpsPos
{
    uint32_t    solStatus;          //!< Solution status (userguide Table 45, p.231)
    uint32_t    posType;            //!< Position type (userguide Table 44, p.229)
    double      lat;                //!< Latitude
    double      lon;                //!< Longitude
    double      hgt;                //!< Height above mean sea level
    float       undulation;         //!< Undulation
    uint32_t    datumID;            //!< Datum ID (see DATUM command in OEMV Family Firmware Reference Manual)
    float       latSigma;           //!< Latitude standard deviation
    float       lonSigma;           //!< Longitude standard deviation
    float       hgtSigma;           //!< Height standard deviation
    uint8_t     stnID[4];           //!< Base station ID
    float       diff_age;           //!< Differential age
    float       sol_age;            //!< Solution age in secs
    uint8_t     numObs;             //!< Number of observations tracked
    uint8_t     numSolnSVs;         //!< Number of satellite solutions used in solution
    uint8_t     numL1;              //!< Number of GPS and GLONASS L1 ranges above the RTK mask angle
    uint8_t     numL2;              //!< Number of GPS and GLONASS L2 ranges above the RTK mask angle
    uint8_t     reserved1;          //!< reserved
    uint8_t     extSolStat;         //!< reserved for BESTGPSPOS; extended solution status for BESTPOS (userguide Table 47, p.233)
    uint8_t     reserved2;          //!< reserved
    uint8_t     sigMask;            //!< reserved for BESTGPSPOS; signals used mask for BESTPOS (userguide Table 46, p.233)
    uint32_t    crc32;              //!< 32-bit CRC
});

typedef struct PTK_INS_NovatelMsgBestGpsPos PTK_INS_NovatelMsgBestGpsPos;

//! Message layout in memory for an RAWIMU and RAWIMUS binary message
PA_PACK(
struct PTK_INS_NovatelMsgRawImu
{
    uint32_t    gpsWeek;
    double      gpsSecond;
    int32_t     imuStatus;          //< The status of the IMU. See userguide Table 72 p.325 (IMU-CPT status)
    int32_t     accelZ;             //< Change in velocity count along Z-axis (see foot notes in userguide p.320)
    int32_t     negAccelY;          //! Negative change in velocity count along Y-axis (see foot notes in userguide p.320)
    int32_t     accelX;             //< Change in velocity count along X-axis (see foot notes in userguide p.320)
    int32_t     gyroZ;              //< Change in angle count along Z-axis right-handed (see foot notes in userguide p.320)
    int32_t     negGyroY;           //! Negative change in angle count along Y-axis right-handed (see foot notes in userguide p.320)
    int32_t     gyroX;              //< Change in angle count along X-axis right-handed (see foot notes in userguide p.320)
    uint32_t    crc32;              //!< 32-bit CRC
});

typedef struct PTK_INS_NovatelMsgRawImu PTK_INS_NovatelMsgRawImu;

//! Message layout in memory for an CORRIMUDATA and CORRIMUDATAS binary message (user guide p.241)
PA_PACK(
struct PTK_INS_NovatelMsgCorrImuData
{
    uint32_t    gpsWeek;
    double      gpsSecond;
    double      pitchRate;          //!< about X-axis rotation
    double      rollRate;           //!< about Y-axis rotation
    double      yawRate;            //!< about Z-axis rotation (right-handed)
    double      lateralAcc;         //!< INS lateral acceleration (along X-axis)
    double      longitudinalAcc;    //!< INS longitudinal acceleration (along Y-axis)
    double      verticalAcc;        //!< INS vertical acceleration (along Z-axis)
    uint32_t    crc32;              //!< 32-bit CRC
});

typedef struct PTK_INS_NovatelMsgCorrImuData PTK_INS_NovatelMsgCorrImuData;

//! Message layout in memory for a TIME binary message (user guide p.341)
PA_PACK(
struct PTK_INS_NovatelMsgTime
{
    uint32_t    clockModelStatus;   //!< Tab. 62, p.307
    double      offset;             //!< Receiver clock offset in seconds from GPS time. GPS time = receiver time - offset
    double      offsetStd;          //!< Receiver clock offset standard deviation
    double      utcOffset;          //!< The offset of GPS time from UTC time. UTC time = GPS time + offset + UTC offset
    uint32_t    utcYear;            //!< UTC year
    uint8_t     utcMonth;           //!< UTC month 0-12 (0 = unknown)
    uint8_t     utcDay;             //!< UTC day 0-31 (0 = unknown)
    uint8_t     utcHour;            //!< UTC hour 0-23
    uint8_t     utcMin;             //!< UTC minutes 0-59
    uint32_t    utcMs;              //!< UTC milliseconds 0 - 60999 (60999 = leap second)
    uint32_t    utcStatus;          //!< UTC status: 0=invalid, 1=valid
    uint32_t    crc32;              //!< 32-bit CRC
});

typedef struct PTK_INS_NovatelMsgTime PTK_INS_NovatelMsgTime;

//! Message layout in memory for an RXSTATUS binary message (user guide p.331)
//WARNING: RXSTATUS message is dynamic in length (depending on # of status code), so can't
//         completely statically define the message.
PA_PACK(
struct PTK_INS_NovatelMsgRxStatusCode
{
    uint32_t    rxStat;             //!< Receiver status word (user guide Table 76 p.334)
    uint32_t    rxStatPri;          //!< Receiver status priority mask
    uint32_t    rxStatSet;          //!< Receiver status event set mask
    uint32_t    rxStatClear;        //!< Receiver status event clear mask
    uint32_t    aux1Stat;           //!< Auxiliary 1 status word (user guide Table 77 p.335)
    uint32_t    aux1StatPri;        //!< Auxiliary 1 status priority mask
    uint32_t    aux1StatSet;        //!< Auxiliary 1 status event set mask
    uint32_t    aux1StatClear;      //!< Auxiliary 1 status event clear mask
    uint32_t    v3Stat;             //!< OEMV-3 status word (user guide Table 78 p.335)
    uint32_t    v3StatPri;          //!< OEMV-3 status priority mask
    uint32_t    v3StatSet;          //!< OEMV-3 status event set mask
    uint32_t    v3StatClear;        //!< OEMV-3 status event clear mask
    uint32_t    v2Stat;             //!< OEMV-2 status word (user guide Table 79 p.335)
    uint32_t    v2StatPri;          //!< OEMV-2 status priority mask
    uint32_t    v2StatSet;          //!< OEMV-2 status event set mask
    uint32_t    v2StatClear;        //!< OEMV-2 status event clear mask
});

typedef struct PTK_INS_NovatelMsgRxStatusCode PTK_INS_NovatelMsgRxStatusCode;

PA_PACK(
struct PTK_INS_NovatelMsgRxStatus
{
    uint32_t    error;              //!< Receiver error (user guide Table 75 p.332)
    uint32_t    numStats;           //!< Number of status codes (including receiver status)
    //after this, message is dynamic (repeat "struct rxstatus_code" "numStats" times), then uint32_t crc32).
});

typedef struct PTK_INS_NovatelMsgRxStatus PTK_INS_NovatelMsgRxStatus;

//! Message layout in memory for an IMUTOANTOFFSET binary message (user guide p.273)
//WARNING: IMUTOANTOFFSET message is dynamic in length (depending on # of stored lever arms),
//         # of stored lever arms should be limited by 2, so we can estimate max length as 124 bytes
#define NOVATEL_IMUTOANTOFFSETS_MAX_NUM_LEVERARMS (2)
PA_PACK(
struct PTK_INS_NovatelMsgImu2AntOffsetsLeverArm
{
    uint32_t    leverArmType;       //!< type of lever arm (user guide Table 53 p.274, PTK_INS_NovatelLeverArmType)
    double      offsetX;            //!< IMU Enclosure Frame
    double      offsetY;            //!< IMU Enclosure Frame
    double      offsetZ;            //!< IMU Enclosure Frame
    double      uncertaintyX;       //!< IMU Enclosure Frame
    double      uncertaintyY;       //!< IMU Enclosure Frame
    double      uncertaintyZ;       //!< IMU Enclosure Frame
    uint32_t    leverArmSource;     //!< source of the lever arm (user guide Table 54 p.275)
});

typedef struct PTK_INS_NovatelMsgImu2AntOffsetsLeverArm PTK_INS_NovatelMsgImu2AntOffsetsLeverArm;

PA_PACK(
struct PTK_INS_NovatelMsgImu2AntOffsetsHead
{
    uint32_t    imuOrientation;     //!< IMU orientation (user guide Table 34 p.183)
    uint32_t    numEntries;         //!< Number of stored lever arms
    //after this, message is dynamic (repeat "struct leverarm" "numEntries" times), then uint32_t crc32).
});

typedef struct PTK_INS_NovatelMsgImu2AntOffsetsHead PTK_INS_NovatelMsgImu2AntOffsetsHead;

//! Message layout in memory for a VEHICLEBODYROTATION binary message (user guide p.345)
PA_PACK(
struct PTK_INS_NovatelMsgVehBodyRot
{
    double      angleX;             //!< Right hand rotation about SPAN computation frame X-axis in degrees
    double      angleY;             //!< Right hand rotation about SPAN computation frame Y-axis in degrees
    double      angleZ;             //!< Right hand rotation about SPAN computation frame Z-axis in degrees
    double      uncertaintyX;       //!< Uncertainty of X rotation in degrees.
    double      uncertaintyY;       //!< Uncertainty of Y rotation in degrees.
    double      uncertaintyZ;       //!< Uncertainty of Z rotation in degrees.
    uint32_t    crc32;              //!< 32-bit CRC
});

typedef struct PTK_INS_NovatelMsgVehBodyRot PTK_INS_NovatelMsgVehBodyRot;

//!< List of lever arm types (user guide Table 53 p.274)
typedef enum
{
    NOVATEL_LEVER_ARM_TYPE_INVALID = 0, //!< An invalid lever arm
    NOVATEL_LEVER_ARM_TYPE_PRIMARY,     //!< Primary lever arm entered for all SPAN systems
    NOVATEL_LEVER_ARM_TYPE_SECONDARY    //!< Secondary lever arm entered for dual antenna SPAN systems.
} PTK_INS_NovatelLeverArmType;

//!< List of lever arm sources (user guide Table 54 p.275)
typedef enum
{
    NOVATEL_LEVER_ARM_SRC_NONE = 0,     //!< No lever arm exists
    NOVATEL_LEVER_ARM_SRC_FROM_NVM,     //!< Lever arm restored from NVM
    NOVATEL_LEVER_ARM_SRC_CALIBRATING,  //!< Lever arm currently calibrating via LEVERARMCALIBRATE command
    NOVATEL_LEVER_ARM_SRC_CALIBRATED,   //!< Lever arm computed from calibration routine via LEVERARMCALIBRATE command
    NOVATEL_LEVER_ARM_SRC_FROM_COMMAND, //!< Lever arm entered via command
    NOVATEL_LEVER_ARM_SRC_RESET         //!< If the current IMU orientation does not match the value restored from NVM then the lever arm will be reset to zero with this status.
} PTK_INS_NovatelLeverArmSrc;

//! List of binary header types
typedef enum
{
    NOVATEL_HEADER_TYPE_LONG = 0,
    NOVATEL_HEADER_TYPE_SHORT,
    NOVATEL_HEADER_TYPE_UNKNOWN
} PTK_INS_NovatelHdrType;

//! Lists of message ID types
typedef enum
{
    NOVATEL_MESSAGE_ID_UNKNOWN              = 0,
    NOVATEL_MESSAGE_ID_BESTPOS              = 42,
    NOVATEL_MESSAGE_ID_RXSTATUS             = 93,
    NOVATEL_MESSAGE_ID_TIME                 = 101,
    NOVATEL_MESSAGE_ID_INSCOV               = 264,
    NOVATEL_MESSAGE_ID_RAWIMU               = 268,
    NOVATEL_MESSAGE_ID_INSCOVS              = 320,
    NOVATEL_MESSAGE_ID_RAWIMUS              = 325,
    NOVATEL_MESSAGE_ID_BESTGPSPOS           = 423,
    NOVATEL_MESSAGE_ID_INSPVA               = 507,
    NOVATEL_MESSAGE_ID_INSPVAS              = 508,
    NOVATEL_MESSAGE_ID_VEHICLEBODYROTATION  = 642,
    NOVATEL_MESSAGE_ID_CORRIMUDATA          = 812,
    NOVATEL_MESSAGE_ID_CORRIMUDATAS         = 813,
    NOVATEL_MESSAGE_ID_IMUTOANTOFFSETS      = 1270
} PTK_INS_NovatelMsgId;

//! Values for GPS time status we may see
typedef enum
{
    NOVATEL_GPSTIMESTATUS_UNKNOWN = 20,           //!< Time status is unknown
    NOVATEL_GPSTIMESTATUS_APPROXIMATE = 60,       //!< Time is set approximately
    NOVATEL_GPSTIMESTATUS_COARSE = 100,           //!< Time is valid to coarse precision (+/- 10 ms)
    NOVATEL_GPSTIMESTATUS_COARSESTEERING = 120,   //!< Time is set to coarse precision and is being steered
    NOVATEL_GPSTIMESTATUS_FREEWHEELING = 130,     //!< Position tracking was lost, hurting time tracking performance
    NOVATEL_GPSTIMESTATUS_FINEADJUSTING = 140,    //!< Time is adjusting to fine precision
    NOVATEL_GPSTIMESTATUS_FINE = 160,             //!< Time is valid to fine precision (+/- 1 microsecond)
    NOVATEL_GPSTIMESTATUS_FINESTEERING = 180,     //!< Time is set to fine precision and is being steered
    NOVATEL_GPSTIMESTATUS_SATTIME = 200           //!< Time on the satellite, for an associated log (like almanacs)
} PTK_INS_NovatelGpsTimeStatus;

//! INS Solution status types
typedef enum
{
    NOVATEL_SOLUTIONSTATUS_SOL_COMPUTED = 0,       //!< Solution computed successfully
    NOVATEL_SOLUTIONSTATUS_INSUFFICIENT_OBS = 1,   //!< Insufficient observations to compute a solution
    NOVATEL_SOLUTIONSTATUS_NO_CONVERGENCE = 2,     //!< Solution did not converge
    NOVATEL_SOLUTIONSTATUS_SINGULARITY = 3,        //!< Singularity in parameters matrix
    NOVATEL_SOLUTIONSTATUS_COV_TRACE = 4,          //!< Covariance trace exceeds maximum (trace > 1000 m)
    NOVATEL_SOLUTIONSTATUS_TEST_DIST = 5,          //!< Test distance exceeded--maximum of 3 rejections if distance > 10km
    NOVATEL_SOLUTIONSTATUS_COLD_START = 6,         //!< Not yet converged from cold start
    NOVATEL_SOLUTIONSTATUS_V_H_LIMIT = 7,          //!< Height or velocity limits exceeded (export law compliance)
    NOVATEL_SOLUTIONSTATUS_VARIANCE = 8,           //!< Variance exceeds limits
    NOVATEL_SOLUTIONSTATUS_RESIDUALS = 9,          //!< Residuals are too large
    NOVATEL_SOLUTIONSTATUS_DELTA_POS = 10,         //!< Delta position is too large,
    NOVATEL_SOLUTIONSTATUS_NEGATIVE_VAR = 11,      //!< Negative variance
    // 12-17 are reserved and unused
    NOVATEL_SOLUTIONSTATUS_PENDING = 18,           //!< Results pending after FIX POSITION command
    NOVATEL_SOLUTIONSTATUS_INVALID_FIX = 19        //!< The fixed position from FIX POSITION is invalid
} PTK_INS_NovatelInsSolStatus;

//! INS status values
typedef enum
{
    NOVATEL_INSSTATUS_INS_INACTIVE = 0,
    NOVATEL_INSSTATUS_INS_ALIGNING = 1,
    NOVATEL_INSSTATUS_INS_SOLUTION_NOT_GOOD = 2,
    NOVATEL_INSSTATUS_INS_SOLUTION_GOOD = 3,
    NOVATEL_INSSTATUS_INS_BAD_GPS_AGREEMENT = 6,
    NOVATEL_INSSTATUS_INS_ALIGNMENT_COMPLETE = 7
} PTK_INS_NovatelInsStatus;

//! message log on options
typedef enum
{
    NOVATEL_LOG_ON_NEVER = 0,
    NOVATEL_LOG_ON_TIME,
    NOVATEL_LOG_ON_CHANGED,
    NOVATEL_LOG_ON_NEW,
    NOVATEL_LOG_ON_ONCE
} PTK_INS_NovatelLogOnCtrl;

// Expected data lengths for verification
//NOTE: When adding a new log here, ensure that the maximum package length in
//      IMU link driver is large enough
#define NOVATEL_LONG_HEADER_LENGTH      28  //should match PRK_INS_NovatelBinLongHdrPrefix
#define NOVATEL_SHORT_HEADER_LENGTH     12  //should match PTK_INS_NovatelBinShortHdr
#define NOVATEL_INSCOV_LENGTH           232 //should match PTK_INS_NovatelMsgInscov
#define NOVATEL_INSCOVS_LENGTH          232 //should match PTK_INS_NovatelMsgInscov
#define NOVATEL_INSPVA_LENGTH           92  //should match PTK_INS_NovatelMsgInspva
#define NOVATEL_INSPVAS_LENGTH          92  //should match PTK_INS_NovatelMsgInspva
#define NOVATEL_BESTGPSPOS_LENGTH       76  //should match PTK_INS_NovatelMsgBestGpsPos
#define NOVATEL_BESTPOS_LENGTH          76  //should match PTK_INS_NovatelMsgBestGpsPos
#define NOVATEL_RAWIMU_LENGTH           44  //should match PTK_INS_NovatelMsgRawImu
#define NOVATEL_RAWIMUS_LENGTH          44  //should match PTK_INS_NovatelMsgRawImu
#define NOVATEL_CORRIMUDATA_LENGTH      64  //should match PTK_INS_NovatelMsgCorrImuData
#define NOVATEL_CORRIMUDATAS_LENGTH     64  //should match PTK_INS_NovatelMsgCorrImuData
#define NOVATEL_TIME_LENGTH             48  //should match PTK_INS_NovatelMsgTime
#define NOVATEL_VEHICLEBODYROTATION_LENGTH 52 //should match PTK_INS_NovatelMsgVehBodyRot
#define NOVATEL_IMUTOANTOFFSETS_MAX_LENGTH 124 //not used, just to check max package length


// The port part of a packet only contains the lower byte of the port number on the device
#define IMU_DRIVER_PORT_ICOM1   0xA0

// Mounting parameters
// as of 2016/12/16
#define NOVATEL_IMUTOANTOFFSET_X      (-0.1758)
#define NOVATEL_IMUTOANTOFFSET_Y      (-0.1341)
#define NOVATEL_IMUTOANTOFFSET_Z      ( 0.0631)
#define NOVATEL_IMUTOANTUNCERTAINTY_X ( 0.001)
#define NOVATEL_IMUTOANTUNCERTAINTY_Y ( 0.001)
#define NOVATEL_IMUTOANTUNCERTAINTY_Z ( 0.001)
#define NOVATEL_VEHICLEBODYROTATION_X (-2.6)
#define NOVATEL_VEHICLEBODYROTATION_Y ( 0.0)
#define NOVATEL_VEHICLEBODYROTATION_Z ( 0.0)

#undef PA_PACK

#ifdef __cplusplus
}
#endif

#endif
