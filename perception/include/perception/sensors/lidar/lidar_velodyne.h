/*
 *
 * Copyright (c) 2018 Texas Instruments Incorporated
 *
 * All rights reserved not granted herein.
 *
 * Limited License.
 *
 * Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
 * license under copyrights and patents it now or hereafter owns or controls to make,
 * have made, use, import, offer to sell and sell ("Utilize") this software subject to the
 * terms herein.  With respect to the foregoing patent license, such license is granted
 * solely to the extent that any such patent is necessary to Utilize the software alone.
 * The patent license shall not apply to any combinations which include this software,
 * other than combinations with devices manufactured by or for TI ("TI Devices").
 * No hardware patent is licensed hereunder.
 *
 * Redistributions must preserve existing copyright notices and reproduce this license
 * (including the above copyright notice and the disclaimer and (if applicable) source
 * code license limitations below) in the documentation and/or other materials provided
 * with the distribution
 *
 * Redistribution and use in binary form, without modification, are permitted provided
 * that the following conditions are met:
 *
 * *       No reverse engineering, decompilation, or disassembly of this software is
 * permitted with respect to any software provided in binary form.
 *
 * *       any redistribution and use are licensed by TI for use only with TI Devices.
 *
 * *       Nothing shall obligate TI to provide you with source code for the software
 * licensed and provided to you in object code.
 *
 * If software source code is provided to you, modification and redistribution of the
 * source code are permitted provided that the following conditions are met:
 *
 * *       any redistribution and use of the source code, including any resulting derivative
 * works, are licensed by TI for use only with TI Devices.
 *
 * *       any redistribution and use of any object code compiled from the source code
 * and any resulting derivative works, are licensed by TI for use only with TI Devices.
 *
 * Neither the name of Texas Instruments Incorporated nor the names of its suppliers
 *
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * DISCLAIMER.
 *
 * THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
#pragma once
#ifndef PTK_LIDAR_VELODYNE_H
#define PTK_LIDAR_VELODYNE_H

/**
 * @file lidar_velodyne.h
 * @brief Definition of Velodyne-specific lidar extension structures and API
 */

#include <stdint.h>
#include <perception/base/pointCloud.h>
#include <perception/sensors/lidar/lidar.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * \brief Maximum number of sensors for Velodyne HDL-32E module.
 *
 * \ingroup group_ptk_lidar
 */
#define PTK_LIDAR_HDL32E_LASERS             (32U)

/**
 * \brief Number of records per packet
 *
 * \ingroup group_ptk_lidar
 */
#define PTK_LIDAR_HDL32E_RECORDS_PER_PACKET (12U)

/**
 * \brief Constant for marking invalid result.
 *
 * \ingroup group_ptk_lidar
 */
#define PTK_LIDAR_HDL32E_MAX_PACKETS        (181U)

/**
 * \brief Maximum number of slices per rotation, per laser.
 *
 * \ingroup group_ptk_lidar
 */
#define PTK_LIDAR_HDL32E_SLICES_PER_ROTATION (PTK_LIDAR_HDL32E_MAX_PACKETS *\
                                              PTK_LIDAR_HDL32E_RECORDS_PER_PACKET)

/**
 * \brief Maximum number of points per rotation, across all lasers.
 *
 * \ingroup group_ptk_lidar
 */
#define PTK_LIDAR_HDL32E_POINTS_PER_ROTATION (PTK_LIDAR_HDL32E_SLICES_PER_ROTATION *\
                                              PTK_LIDAR_HDL32E_LASERS)

#define PTK_LIDAR_HDL32E_DISTANCE_SCALE     (0.002f)

/**
 * \brief
 *
 * A single record within a UDP packet is 100 bytes long
 *
 * \ingroup group_ptk_lidar
 */
typedef struct
{
    /** Start identifier. */
    uint16_t        startIdentifier;

    /** Rotational value. */
    uint16_t        theta;

    /** 32 * 3 byte combinations reported as follows:
     *  - bytes 1 and 2: report distance to the nearest 0.2 cm
     *  - byte 3       : reports intensity ona scale of 0-255
     */
    uint8_t         raw[96];

} PTK_Lidar_VelodyneRecord;

/**
 * \brief Format of the packet reveived from Velodyne HDL-32E unit.
 *
 * An entire packet has 12 records+metadata for a total of 1206 bytes, followed
 * by 4 bytes of GPS stamp data.
 *
 * The following table provides the details on the angle corresponding
 * to each of the 32 lasers. 
 *
 *  Laser#| Angle  | Enable Mask
 * ------:|-------:|-----------:
 *  1     | -30.67 | 0x00000001
 *  2     |  -9.33 | 0x00000002
 *  3     | -29.33 | 0x00000004
 *  4     |  -8.00 | 0x00000008
 *  5     | -28.00 | 0x00000010
 *  6     |  -6.67 | 0x00000020
 *  7     | -26.67 | 0x00000040
 *  8     |  -5.33 | 0x00000080
 *  9     | -25.33 | 0x00000100
 * 10     |  -4.00 | 0x00000200
 * 11     | -24.00 | 0x00000400
 * 12     |  -2.67 | 0x00000800
 * 13     | -22.67 | 0x00001000
 * 14     |  -1.33 | 0x00002000
 * 15     | -21.33 | 0x00004000
 * 16     |   0.00 | 0x00008000
 * 17     | -20.00 | 0x00010000
 * 18     |   1.33 | 0x00020000
 * 19     | -18.67 | 0x00040000
 * 20     |   2.67 | 0x00080000
 * 21     | -17.33 | 0x00100000
 * 22     |   4.00 | 0x00200000
 * 23     | -16.00 | 0x00400000
 * 24     |   5.33 | 0x00800000
 * 25     | -14.67 | 0x01000000
 * 26     |   6.67 | 0x02000000
 * 27     | -13.33 | 0x04000000
 * 28     |   8.00 | 0x08000000
 * 29     | -12.00 | 0x10000000
 * 30     |   9.33 | 0x20000000
 * 31     | -10.67 | 0x40000000
 * 32     |  10.67 | 0x80000000
 * 
 * \ingroup group_ptk_lidar
 */
typedef struct
{
    /** Data records. */
    PTK_Lidar_VelodyneRecord    records[PTK_LIDAR_HDL32E_RECORDS_PER_PACKET];

    /** GPS timestamp logged by the unit. */
    uint32_t                    gpsTimestamp;

    /** Reserved field. */
    uint16_t                    reserved;

    /** System timestamp recorded by the driver on the receiving device
     *  (ex:- TDA2x/TDA3x).
     */
    uint64_t                    systemTimestamp;

} PTK_Lidar_VelodynePacket;

/**
 * \brief Format of the Lidar frame.
 *
 * An entire frame has PTK_LIDAR_HDL32E_MAX_PACKETS packets.
 *
 * \ingroup group_ptk_lidar
 */
typedef struct
{
    /** Data records. */
    PTK_Lidar_VelodynePacket    packets[PTK_LIDAR_HDL32E_MAX_PACKETS];

} PTK_Lidar_VelodyneFrame;

/**
 * \brief This must be called once at startup to initialize the internal
 *        structures used to parse LiDAR data from a Velodyne sensor.
 *        This is independent of the general PTK initialization routine.
 *
 * \ingroup group_ptk_lidar
 */
void PTK_Lidar_Velodyne_initParser();

/**
 * \brief This adds pointers to internally managed trig tables suitable for use
 *        with a Velodyne HDL32E to the given LiDAR metadata structure
 *
 * \param [in] cfg  The config to attach table pointers to.
 *
 * \ingroup group_ptk_lidar
 */
void PTK_Lidar_Velodyne_setHDL32ETrigTables(PTK_LidarMetaConfig *cfg);

/**
 * \brief This does incremental parsing of an additional packet of data from
 *        the Velodyne sensor, appending information to the given
 *        PTK_PointCloud and PTK_Lidar meta structures. If a packet does not
 *        fit, none of it is inserted to the given point cloud or metadata
 *        structure, and a 1 is returned indicating that the packet should be
 *        parsed with an empty cloud and metadata structure.
 *
 * \param [in] packet The raw packet as received from the sensor.
 *
 * \param [in] dst The point cloud to write decoded points to.
 *
 * \param [in] meta The metadata structure to record additional information to.
 *
 * \return
 *        - 0 if the packet was parsed completely
 *        - 1 if the packet would not fit in the given cloud or metadata
 *            structure
 *
 * \ingroup group_ptk_lidar
 */
uint32_t PTK_Lidar_Velodyne_parse(PTK_Lidar_VelodynePacket *packet,
                                  PTK_PointCloud *dst,
                                  PTK_LidarMeta *meta);

/**
 * \brief This converts from the logical laser ordering in a Velodyne data
 *        packet to the physical laser order used for determining neighbors.
 *
 * \param [in] laser The laser number within a packet.
 *
 * \return The physical location of that laser in the scanner.
 *
 * \ingroup group_ptk_lidar
 */
uint32_t PTK_Lidar_Velodyne_mapPhysicalToLogicalLaser(uint32_t laser);

#ifdef __cplusplus
}
#endif

#endif
