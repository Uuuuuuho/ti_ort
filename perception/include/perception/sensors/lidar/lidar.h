/*
 *
 * Copyright (c) 2018 Texas Instruments Incorporated
 *
 * All rights reserved not granted herein.
 *
 * Limited License.
 *
 * Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
 * license under copyrights and patents it now or hereafter owns or controls to make,
 * have made, use, import, offer to sell and sell ("Utilize") this software subject to the
 * terms herein.  With respect to the foregoing patent license, such license is granted
 * solely to the extent that any such patent is necessary to Utilize the software alone.
 * The patent license shall not apply to any combinations which include this software,
 * other than combinations with devices manufactured by or for TI ("TI Devices").
 * No hardware patent is licensed hereunder.
 *
 * Redistributions must preserve existing copyright notices and reproduce this license
 * (including the above copyright notice and the disclaimer and (if applicable) source
 * code license limitations below) in the documentation and/or other materials provided
 * with the distribution
 *
 * Redistribution and use in binary form, without modification, are permitted provided
 * that the following conditions are met:
 *
 * *       No reverse engineering, decompilation, or disassembly of this software is
 * permitted with respect to any software provided in binary form.
 *
 * *       any redistribution and use are licensed by TI for use only with TI Devices.
 *
 * *       Nothing shall obligate TI to provide you with source code for the software
 * licensed and provided to you in object code.
 *
 * If software source code is provided to you, modification and redistribution of the
 * source code are permitted provided that the following conditions are met:
 *
 * *       any redistribution and use of the source code, including any resulting derivative
 * works, are licensed by TI for use only with TI Devices.
 *
 * *       any redistribution and use of any object code compiled from the source code
 * and any resulting derivative works, are licensed by TI for use only with TI Devices.
 *
 * Neither the name of Texas Instruments Incorporated nor the names of its suppliers
 *
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * DISCLAIMER.
 *
 * THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
#pragma once
#ifndef PTK_LIDAR_H
#define PTK_LIDAR_H

/**
 * @file lidar.h
 * @brief Definition of generic lidar data structures and API
 */

#include <stdint.h>

/**
 * \defgroup group_ptk_lidar PTK Lidar
 * \ingroup group_ptk_base
 *
 * The lidar component provides data structures, drivers for decoding data from
 * Velodyne sensors, and algorithms for performing some basic LiDAR-related
 * processing tasks. LiDAR measurements are stored in a normal PTK_PointCloud
 * as defined in section 3, but an additional metadata structure is used to
 * index this point cloud in scan order, in order to provide information that
 * may accelerate some algorithms which depend on the physical arrangement of
 * the laser scan.
 *
 */

#ifdef __cplusplus
extern "C" {
#endif

/**
 * \brief Macro for default laser enable status. By default, all 32 lasers
 *        are enabled.
 *
 * \ingroup group_ptk_lidar
 */
#define PTK_LIDAR_DEFAULT_LASER_ENABLE_MASK (0xFFFFFFFFU)

/**
 * \brief Macro for Maximum gating angle in degrees.
 *
 * \ingroup group_ptk_lidar
 */
#define PTK_LIDAR_MAX_GATING_ANGLE      (180.0)

/**
 * \brief Macro for getting a pointer to the point data.
 *
 * \ingroup group_ptk_lidar
 */
#define PTK_LidarMeta_getPointsData(m) (uint32_t*)((uint8_t*)m + m->pointOffset)

/**
 * \brief Macro for getting a pointer to the timestamp data.
 *
 * \ingroup group_ptk_lidar
 */
#define PTK_LidarMeta_getTimestampData(m) (uint64_t*)((uint8_t*)m + m->timeOffset)

/**
 * \brief Lidar sensor range and angle gating configuration.
 *
 * \ingroup group_ptk_lidar
 */
typedef struct
{
    /** Flag indicating if the configuration is valid. \n
      * Allowed: {0, 1}
      */
    uint32_t        valid;

    /** Absolute angle to be gated for in degrees. \n
      * Range: [0.0, 180]
      * 0.0 means no angle gating.
      * If the specified angle is x (non-zero), then points within -x:+x will
      * be considered and everything ele will be discarded.
      */
    float           gatingAbsMaxAngle;

    /** Min range to be gated in meters. \n
      * Range: [0.0, inf)
      */
    float           gatingMinRange;

    /** Max range to be gated in meters. \n
      * Range: [0.0, inf)
      */
    float           gatingMaxRange;

    /** Laser enable mask. Each bit captures the enable status. The mask
     *  should be interpreted as follows. A '1' means that the specified laser
     *  data will be included in the point cloud generation and a '0' means
     *  that it will not be included.
     *  - bit  0 - laser  1
     *  - bit  1 - laser  2
     *  - ...
     */
    uint32_t        laserEnableMask;

} PTK_LidarGatingParams;

/**
 * \brief
 *
 *  Metadata structure is used to index lidar point cloud in scan order, in
 *  order to provide information that may accelerate some algorithms which
 *  depend on the physical arrangement of the laser scan.
 *
 * \ingroup group_ptk_lidar
 */
typedef struct
{
    /** The number of vertically stacked lasers available.  In an HDL-32E this
     *  is 32.
     */
    uint32_t                lasers;

    /** The number of horizontal slices in a complete rotation.  In an HDL-32E,
     *  this is 2172.
     */
    uint32_t                slices;

    /** Pointer to table of sin() values to use in decoding for laser rotation
     *  angles. Must be 36000 entries long in increments of 0.01 degrees.
     */
    float                 * sinThetaTable;

    /** Pointer to table of cos() values to use in decoding for laser rotation
     *  angles. Must be 36000 entries long in increments of 0.01 degrees.
     */
    float                 * cosThetaTable;

    /** Pointer to table of sin() values to use in decoding for laser vertical
     *  angles, must be the same length as number of lasers.
     */
    float                 * sinAlphaTable;

    /** Pointer to table of cos() values to use in decoding for laser vertical
     *  angles, must be the same length as number of lasers.
     */
    float                 * cosAlphaTable;

    /** Array of distance offsets to use in decoding, must be the same length
     *  as the number of lasers.
     */
    float                 * distance;

    /** Array of range scale values to use in decoding, must be the same length
     *  as the number of lasers.
     */
    float                 * scale;

    /** Array of angle offsets, in 1/3600 degrees, to use in decoding, must be
     *  the same length as the number of lasers.
     */
    int16_t               * angle;

    /** Array of height offsets to use in decoding, must be the same length as
     *  the number of lasers.
     */
    float                 * height;

    /** Angle and range gating information. */
    PTK_LidarGatingParams   gatingParams;

} PTK_LidarMetaConfig;

/**
 * \brief
 *
 *  This is an entirely opaque structure which is populated by the LiDAR driver
 *  appropriate to the sensor being used. A convenience method exists for
 *  HDL32E sensors to provide sin/cos table for both theta and alpha values
 *  (see the Velodyne driver section below).
 *
 *  There are several constants pre-defined by the PercepTIon Toolkit
 *  specifically for the Velodyne HDL32E, to simplify populating the
 *  metadata configuration struct.
 *
 * \ingroup group_ptk_lidar
 */
typedef struct
{
    /** Lidar Meta configuration*/
    PTK_LidarMetaConfig     config;

    /** Number of points in the scan. */
    uint32_t                pointCount;

    /** Number of slices found in the scan. */
    uint32_t                slicesFound;

    /** Offset into the packet where the slice (time) data is found. */
    uint32_t                timeOffset;

    /** Offset into the packet where the point data is found. */
    uint32_t                pointOffset;

} PTK_LidarMeta;

/**
 * \brief Compute the size of the PTK_LidarMeta struct for the given
 *        configuration, in bytes.
 *
 * \param [in] cfg  LiDAR metadata configuration parameters.
 *
 * \return Size required in bytes.
 *
 * \ingroup group_ptk_lidar
 */
uint32_t PTK_LidarMeta_getSize(const PTK_LidarMetaConfig *cfg);

/**
 * \brief Initialize the given piece of user-allocated memory as a
 *        PTK_LidarMeta struct and convert the pointer type.
 *
 * \param [out] mem Pointer to externally allocated memory to initialize.
 *
 * \param [in] cfg Configuration to use when initializing this memory.
 *
 * \return Initialized meta data object.
 *
 * \ingroup group_ptk_lidar
 */
PTK_LidarMeta *PTK_LidarMeta_init(uint8_t *mem, const PTK_LidarMetaConfig *cfg);

/**
 * \brief This clears the given LiDAR metadata struct (but not its
 *        configuration) so that it can be reused to record new data received
 *        from the LiDAR driver.
 *
 * \param [out] meta The metadata to clear.
 *
 * \ingroup group_ptk_lidar
 */
void PTK_LidarMeta_clear(PTK_LidarMeta *meta);

/**
 * \brief Get the number of slices contained within a given LiDAR metadata
 *        structure.
 *
 * \param [in] meta The LiDAR metadata to read.
 *
 * \return The number of slices.
 *
 * \ingroup group_ptk_lidar
 */
uint32_t PTK_LidarMeta_getSlices(const PTK_LidarMeta *meta);

/**
 * \brief Get the number of lasers contained within a given LiDAR metadata
 *        structure.
 *
 * \param [in] meta The LiDAR metadata to read.
 *
 * \return The number of lasers.
 *
 * \ingroup group_ptk_lidar
 */
uint32_t PTK_LidarMeta_getLasers(const PTK_LidarMeta *meta);

/**
 * \brief This reads the timestamp matching when a given slice was captured. If
 *        a slice was not captured for the slice number given, the timestamp
 *        produced is 0.
 *
 * \param [in] meta The LiDAR metadata to read.
 *
 * \param [in] slice The slice number to look up.
 *
 * \return Timestamp when the requested slice was recorded. If a slice was not
 *        captured for the slice number given, 0 is returned.
 *
 * \ingroup group_ptk_lidar
 */
uint64_t PTK_LidarMeta_getTimestamp(const PTK_LidarMeta *meta, uint32_t slice);

/**
 * \brief This looks up the index in the matching PTK_PointCloud for the point
 *        identified by the given (slice, laser) combination. If no data was
 *        recorded for that coordinate, a value of PTK_POINTCLOUD_INVALID_POINT
 *        is returned.
 *
 * \param [in] meta The LiDAR metadata to read.
 *
 * \param [in] slice The slice number to look up.
 *
 * \param [in] laser The laser number to look up.
 *
 * \return
 *        - The matching point cloud index, is found
 *        - PTK_POINTCLOUD_INVALID_POINT, otherwise
 *
 * \ingroup group_ptk_lidar
 */
uint32_t PTK_LidarMeta_getPointIndex(const PTK_LidarMeta *meta, uint32_t slice, uint32_t laser);

/**
 * \brief This stores the index in the matching PTK_PointCloud for the point
 *        identified by the given (slice, laser) combination. If the (slice,
 *        laser) combination is invalid then no updates are made.
 *
 * \param [inout] meta The LiDAR metadata to write to.
 *
 * \param [in] slice The slice number to look up.
 *
 * \param [in] laser The laser number to look up.
 *
 * \param [in] index Index value to write.
 *
 * \ingroup group_ptk_lidar
 */
void PTK_LidarMeta_setPointIndex(PTK_LidarMeta *meta,
                                 uint32_t slice,
                                 uint32_t laser,
                                 uint32_t index);

#ifdef __cplusplus
}
#endif

#endif
