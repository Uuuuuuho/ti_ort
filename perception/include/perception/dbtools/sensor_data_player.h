/*
 *
 * Copyright (c) 2018 Texas Instruments Incorporated
 *
 * All rights reserved not granted herein.
 *
 * Limited License.
 *
 * Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
 * license under copyrights and patents it now or hereafter owns or controls to make,
 * have made, use, import, offer to sell and sell ("Utilize") this software subject to the
 * terms herein.  With respect to the foregoing patent license, such license is granted
 * solely to the extent that any such patent is necessary to Utilize the software alone.
 * The patent license shall not apply to any combinations which include this software,
 * other than combinations with devices manufactured by or for TI ("TI Devices").
 * No hardware patent is licensed hereunder.
 *
 * Redistributions must preserve existing copyright notices and reproduce this license
 * (including the above copyright notice and the disclaimer and (if applicable) source
 * code license limitations below) in the documentation and/or other materials provided
 * with the distribution
 *
 * Redistribution and use in binary form, without modification, are permitted provided
 * that the following conditions are met:
 *
 * *       No reverse engineering, decompilation, or disassembly of this software is
 * permitted with respect to any software provided in binary form.
 *
 * *       any redistribution and use are licensed by TI for use only with TI Devices.
 *
 * *       Nothing shall obligate TI to provide you with source code for the software
 * licensed and provided to you in object code.
 *
 * If software source code is provided to you, modification and redistribution of the
 * source code are permitted provided that the following conditions are met:
 *
 * *       any redistribution and use of the source code, including any resulting derivative
 * works, are licensed by TI for use only with TI Devices.
 *
 * *       any redistribution and use of any object code compiled from the source code
 * and any resulting derivative works, are licensed by TI for use only with TI Devices.
 *
 * Neither the name of Texas Instruments Incorporated nor the names of its suppliers
 *
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * DISCLAIMER.
 *
 * THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
#pragma once
#ifndef PTK_SENSOR_DATA_PLAYER_H
#define PTK_SENSOR_DATA_PLAYER_H

/**
 * @file SensorDataPlayer.h
 * @brief class to handle a set of sensorstreams from one data sequence
 *        providing data records in a time-ordered manner
 */

#include <cstdint>
#include <string>
#include <iostream>

#include <perception/dbtools/sensorstream.h>
#include <perception/dbtools/dbconfig.h>

namespace ptk {

class SensorDataPlayer
{
public:

    /* Constructor
      @param [in] cfg            database configuration file specifying available sensors
      @param [in] appTags        vector of strings. Sensors in @cfg with appTag field matching an @appTags vector entry
                                 will be used for sensor data replay.
                                 Ordering of @appTags establishes a sensor enumeration for application to
                                 reference a certain sensor,
                                 i.e., sensor with app tag @appTags[i] has sensor app id i.
      @param [in] intraFrameWait Flag to indicate if the player should implement the wait
                                 operation between successive frames based on two successive
                                 timestamps.
    */
    SensorDataPlayer(PTK_DBConfig *cfg,
                     const std::vector<std::string> &appTags,
                     bool intraFrameWait=false);

    /* Destructor */
    virtual ~SensorDataPlayer();

    /*
     * Get the the record from the sensorstream whose current timestamp is smallest (next).
     *
     *  @param [out]  dataPtr         pointer to data pointer to the returned record, by reference
     *  @param [out]  dataSize        size of the returned record, by reference
     *  @param [out]  timestamp       timestamp for the returned record, by reference
     *
     *  Returns sensor app id of sensor returning this record.
     *  Returns MAX UINT32_T if no more records are available.
     */
    virtual std::uint32_t get_next(void         *&dataPtr,
                                   std::size_t   &dataSize,
                                   std::uint64_t &timestamp);

    /*
     * Set time to given @timestamp. This rewinds all sensorstreams to the first
     * record after given @timestamp.
     *
     *  @param [in]  timestamp   timestamp to set all sensorstreams
     *
     */
    virtual void set_time(std::uint64_t timestamp);

    /*
     * Get access to the sensorstream for given sensor app id
     *
     *  @param [in]  sensorAppId      sensor's app id (refer to Constructor brief for details)
     *
     *  Returns sensorstream pointer; nullptr if sensorstream is inactive
     */
    sensorstream * get_sensorstream(std::uint32_t sensorAppId);

    /*
     *  Returns number of active streams
     */
    std::uint32_t get_active_streams_count(){return _numActvStreams;};

    /*
     *  Returns current timestamp
     */
    std::uint64_t get_current_time(){return _curTime;};

    void check_sensorstreams(uint64_t* time);

protected:
    std::uint64_t                _curTime{0}; //time from when last record provided
    std::uint32_t                _numActvStreams{0}; //number of active streams
    std::vector<sensorstream *>  _sStreams; //sensorstreams (length numActvStreams)
    std::vector<std::uint32_t>   _actvStreamId2appId; //map from internal sensor ordering (numActvStreams) to sensor app id
    std::vector<std::uint32_t>   _appId2actvStreamId; //map from sensor app id to internal sensor ordering (numActvStreams)
    std::uint64_t                _endTime{0};
    bool                         _firstRec{true};
    bool                         _intraFrameWait{false};

    void inter_frame_delay(std::uint64_t  newTs);

private:



};

};

#endif
