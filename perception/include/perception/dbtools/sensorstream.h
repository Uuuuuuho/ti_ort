/*
 *
 * Copyright (c) 2018 Texas Instruments Incorporated
 *
 * All rights reserved not granted herein.
 *
 * Limited License.
 *
 * Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
 * license under copyrights and patents it now or hereafter owns or controls to make,
 * have made, use, import, offer to sell and sell ("Utilize") this software subject to the
 * terms herein.  With respect to the foregoing patent license, such license is granted
 * solely to the extent that any such patent is necessary to Utilize the software alone.
 * The patent license shall not apply to any combinations which include this software,
 * other than combinations with devices manufactured by or for TI ("TI Devices").
 * No hardware patent is licensed hereunder.
 *
 * Redistributions must preserve existing copyright notices and reproduce this license
 * (including the above copyright notice and the disclaimer and (if applicable) source
 * code license limitations below) in the documentation and/or other materials provided
 * with the distribution
 *
 * Redistribution and use in binary form, without modification, are permitted provided
 * that the following conditions are met:
 *
 * *       No reverse engineering, decompilation, or disassembly of this software is
 * permitted with respect to any software provided in binary form.
 *
 * *       any redistribution and use are licensed by TI for use only with TI Devices.
 *
 * *       Nothing shall obligate TI to provide you with source code for the software
 * licensed and provided to you in object code.
 *
 * If software source code is provided to you, modification and redistribution of the
 * source code are permitted provided that the following conditions are met:
 *
 * *       any redistribution and use of the source code, including any resulting derivative
 * works, are licensed by TI for use only with TI Devices.
 *
 * *       any redistribution and use of any object code compiled from the source code
 * and any resulting derivative works, are licensed by TI for use only with TI Devices.
 *
 * Neither the name of Texas Instruments Incorporated nor the names of its suppliers
 *
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * DISCLAIMER.
 *
 * THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
#pragma once
#ifndef PTK_SENSORSTREAM_H
#define PTK_SENSORSTREAM_H

/**
 * @file sensorstream.h
 * @brief utility that provides data from a data sequence to a C++ user application.
 *        One sensorstream object is associated with all data from one sensor
 *        in a data sequence. It may be used to retrieve this data in a stream fashion
 *        by requesting one record after another, or in a single-access fashion by requesting
 *        a record at a certain time or position.
 *
 *        The "get" functions provide a void pointer to raw data in DDR memory.
 *        Once another "get" function is called, previous pointers must be assumed invalid.
 *        The void pointer is either pointing to binary or to text (sequence of chars) data.
 *        API "is_binary_data()" can be used to determine/ensure the format if necessary.
 *        In general, .png and .bin files are read as binary, whereas .txt and .csv are read as text.
 *
 */

#include <cstdint>
#include <string>
#include <vector>

namespace ptk {

class sensorstream
{
public:

    /*Default constructor*/
    sensorstream();

    /*Constructor and destructor*/
    //! Constructor
    //!@param [in] databaseDir      base directory of database: WARNING: Use double backslashes for paths!
    //!@param [in] seqId            data seqence id for this sensor
    //!@param [in] sensorFolderName subfolder name of the sensor, e.g. "camera002" or "lidar001"
    sensorstream(const std::string& databaseDir, std::uint32_t seqId, const std::string& sensorFolderName);
    ~sensorstream();

    // Disable copy constructor and assignment operator.
    sensorstream(const sensorstream&) = delete;
    sensorstream& operator=(const sensorstream&) = delete;

    /*stream related functions*/
    //! set next record in stream to be the one before timestampIn
    void set_stream_before(uint64_t timestampIn);
    //! set next record in stream to be the one after timestampIn
    void set_stream_after(uint64_t timestampIn);
    //! set next record in stream to be the one at file position posIn
    void set_stream_pos(int posIn);
    //! get info (position and timestamp) about next record in stream
    void check_stream(int &nextPosOut, uint64_t &nextTimestampOut);

    //! get next record in stream. Note: data pointer valid only until another get_ method is called.
    //! Return record position read (>=0) on success, -1 on fail
    int get_stream_next(void *&pDataOut, size_t &dataSizeOut, uint64_t &timestampOut);

    /*single access-related functions (return status: >0=pos where record was found, -1==fail)*/
    //! get record before timestampIn. Note: data pointer valid only until another get_ method is called.
    int get_before(uint64_t timestampIn, void *&pDataOut, size_t &dataSizeOut, uint64_t &timestampOut);
    //! get record after timestampIn. Note: data pointer valid only until another get_ method is called.
    int get_after(uint64_t timestampIn, void *&pDataOut, size_t &dataSizeOut, uint64_t &timestampOut);
    //! get record at file position posIn. Note: data pointer valid only until another get_ method is called.
    int get_pos(int posIn, void *&pDataOut, size_t &dataSizeOut, uint64_t &timestampOut);

    //! return true if pointers returned by "get" should be interpreted as "binary"; false otherwise.
    bool is_binary_data();

	/**
	 * Get the total number of records available in this sensor
	 * @return Total number of records
	 */
	int records() const { return _numRecords; };

	/**
	 * Get the current timestamp of the sensor stream, that is, the timestamp of the record most
	 * recently returned. If no record has been returned yet, this produces 0.
	 * @return timestamp of the record most recently returned by get_stream_next()
	 */
	uint64_t current_timestamp() const;

    /**
     * read extrinsic calibration into float array of size 12 (Rxx, Rxy, Rxz, Ryx, Ryy, Ryz, Rzx, Rzy, Rzz, tx, ty, tz)
     * return status 0==OK, -1==fail
     */
    int get_extrinsic_calibration(float M_c_e[12]);

    /**
     * get path of sensor directory, e.g. "/home/user/database_dir/sequence0003/camera002"
     */
    std::string get_full_dir_path() { return _sensorDir; };

    uint64_t getStartTimeInNanoSecs() { return _startTime; }
    uint64_t getEndTimeInNanoSecs() { return _endTime; }


    private:

        enum sensorCategory
        {
            CAMERA = 0,
            RADAR = 1,
            LIDAR = 2,
            GPS = 3,
            IMU = 4,
            INS = 5,
            TIME = 6,
            VIRTUAL = 7,
        };

        enum dataFileExtension
        {
            BIN = 0,
            TXT = 1,
            CSV = 2,
            PNG = 3,
            BMP = 4,
            UNDEFINED = 5 
        };

    std::string                 _sensorFolderName; /*e.g. "camera001*/
    std::string                 _sensorDir; /*database base dir plus seqeunce dir plus sensorFolderName*/
    enum sensorCategory         _sensorCat; /*sensor category*/
    int                         _sensorDeviceId{0}; /*currently only needed for lidar, gps, imu, ins*/
    int                         _sensorLogId{0}; /*currently only needed for Novatel SPAN-SE (gps, imu, ins)*/
    enum dataFileExtension      _dataFileExt; /*extension for data file*/
    int                         _numRecords{0}; /*number of records in this sequence*/
    std::vector<uint64_t>       _timestamps; /*will be _numRecords long*/
    uint64_t                    _startTime{0}; /*first timestamp for the sequence*/
    uint64_t                    _endTime{0}; /*last timestamp for the sequence*/
    int                         _nextPos{0}; /*position (in timestamps.txt file) for next record to be produced by stream */
    void *                      _pData{nullptr}; /*holds the current data record. Using malloc to allocate*/
    size_t                      _dataSizeAllocated{0}; //!< data size allocated for _pData

    /*return pos*/
    int findPosAfter(uint64_t timestampIn);
    int findPosBefore(uint64_t timestampIn);
    /*return status 0==OK, -1==fail*/
    void set_stream(int posIn); //!< set stream to output pos next
    size_t getRecord(int posIn); //!< write record into _pData, manages allocation, returns size of data written
    size_t getRecord_readFile(const std::string& dataFilePath); //!< used by getRecord to read data from file

    std::string getFileExtension(); //!< returns file extension string (e.g., ".png") based on _dataFileExt member

    /*memory management*/
    void freeMemory(); //!< free _pData
    int32_t allocateMemory(size_t dataSize); //!< allocate memory for _pData, input: number of bytes to be allocated
};

};

#endif
