/*
 *
 * Copyright (c) 2018 Texas Instruments Incorporated
 *
 * All rights reserved not granted herein.
 *
 * Limited License.
 *
 * Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
 * license under copyrights and patents it now or hereafter owns or controls to make,
 * have made, use, import, offer to sell and sell ("Utilize") this software subject to the
 * terms herein.  With respect to the foregoing patent license, such license is granted
 * solely to the extent that any such patent is necessary to Utilize the software alone.
 * The patent license shall not apply to any combinations which include this software,
 * other than combinations with devices manufactured by or for TI ("TI Devices").
 * No hardware patent is licensed hereunder.
 *
 * Redistributions must preserve existing copyright notices and reproduce this license
 * (including the above copyright notice and the disclaimer and (if applicable) source
 * code license limitations below) in the documentation and/or other materials provided
 * with the distribution
 *
 * Redistribution and use in binary form, without modification, are permitted provided
 * that the following conditions are met:
 *
 * *       No reverse engineering, decompilation, or disassembly of this software is
 * permitted with respect to any software provided in binary form.
 *
 * *       any redistribution and use are licensed by TI for use only with TI Devices.
 *
 * *       Nothing shall obligate TI to provide you with source code for the software
 * licensed and provided to you in object code.
 *
 * If software source code is provided to you, modification and redistribution of the
 * source code are permitted provided that the following conditions are met:
 *
 * *       any redistribution and use of the source code, including any resulting derivative
 * works, are licensed by TI for use only with TI Devices.
 *
 * *       any redistribution and use of any object code compiled from the source code
 * and any resulting derivative works, are licensed by TI for use only with TI Devices.
 *
 * Neither the name of Texas Instruments Incorporated nor the names of its suppliers
 *
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * DISCLAIMER.
 *
 * THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
#pragma once
#ifndef PTK_VIRTUAL_SENSOR_CREATOR_H
#define PTK_VIRTUAL_SENSOR_CREATOR_H

/**
 * @file virtual_sensor_creator.h
 * @brief class to save a sequence of data into a virtual sensor database entry.
 *        The class takes care of conforming to the virtual sensor data format
 *        specification.
 */

#include <cstdint>
#include <string>
#include <iostream>

#include <perception/dbtools/sensorstream.h>

namespace ptk {

class VirtualSensorCreator
{
public:
    enum data_type_e
    {
        BIN = 0,
        PNG = 3,
        BMP = 4
    };

    /* Default Constructor: disabled object, whose method have no effect*/
    VirtualSensorCreator();

    /* Constructor
      @param [in] databaseDir            base directory of database: WARNING: Use double backslashes for paths!
      @param [in] seqId                  data seqence id for this sensor
      @param [in] parentFolderName       subfolder name of the sensor, e.g. "camera002" or "lidar001", from which
                                         this virtual sensor is derived.
                                         Extrinsic calibration and sensor processor delay info  will be
                                         copied from this sensor. Intrinsic calibration will have parent sensor.
                                         If "none" is passed, these files are created (if they don't exist already)
                                         with default values. User has to change these manually if desired. If they
                                         already exist, they are not changed, i.e., @descTextFiles has no effect.
      @param [in] outFolderName          snake-style output folder name for the virtual sensor
      @param [in] descTextFiles          space separated list of text files that will be appended to the virtual
                                         sensor's intrinsic calibration file (e.g., parameters used to produce virtual sensor)
      @param [in] dataType               output data type, raw (binary data) or png (images)
      @param [in] overwriteExistingData  bool, erase existing data in destination folder, if destination folder already exists?
    */
    VirtualSensorCreator(const std::string&   databaseDir,
                         int                  seqId,
                         const std::string&   parentFolderName,
                         const std::string&   outFolderName,
                         const std::string&   descTxtFiles,
                         enum data_type_e     dataType,
                         bool                 overwriteExistingData);
    /* Destructor */
    ~VirtualSensorCreator();

    /*
     * Add record to virtual sensor (append timestamp in timestamps.txt).
     * Depending on data type being "raw" or "png", the method behaves differently.
     *
     * For raw data:
     * Data pointed by dataPtr is written to bin file and
     * path to bin file is returned.
     *
     * For image data:
     * Does not write the data file, just returns the path for image to be written to.
     * !!! Application is expected to write its image data to that file !!!
     *
     *  @param [out] dstFilePath     path to file written (BIN data) or to be written by user (PNG data)
     *  @param [in]  timestamp       timestamp for the recorded to be written
     *  @param [in]  dataPtr         pointer to data to be written (only BIN data), otherwise NULL
     *  @param [in]  dataSize        size of data to be written (only BIN data), otherwise 0
     */
    int32_t add_record(std::string  &dstFilePath,
                       std::uint64_t timestamp,
                       void         *dataPtr,
                       std::size_t   dataSize);

    bool is_disabled(){return _disabled;};

private:
    bool               _disabled; /* true, if object is disabled, i.e., its public methods have no effect */
    bool               _hasParentSensor; /* true if constructor received a valid parent sensor, false otherwise */

    std::string        _databaseDir;
    int                _seqId;
    std::string        _vFolderName;
    std::string        _sensorDir; /*database base dir plus seqeunce dir plus sensorFolderName*/
    std::string        _dataDir; /*sensorDir plus /data/ */
    std::string        _timestampsTxt; /*path to timestamps txt file */
    enum data_type_e   _dataType;

    /* Clear data (./data/ folder and timestamps content) */
    int32_t clear();

    int32_t copyFile(const std::string& dstPath, const std::string& srcPath, const std::string& fileName); //copy a file to a folder
    bool makeDirectory(const std::string& dirPath); //make directory, returns true on success
    void removeAllFiles(const std::string& dirPath); //remove all files from given folder
    void appendFiles(const std::string& dstFile, const std::string& srcFiles); //append contents of new-line separated list of text files (srcFiles) to dstFile

};

};

#endif
