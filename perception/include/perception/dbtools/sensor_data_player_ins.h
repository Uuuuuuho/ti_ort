/*
 *
 * Copyright (c) 2018 Texas Instruments Incorporated
 *
 * All rights reserved not granted herein.
 *
 * Limited License.
 *
 * Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
 * license under copyrights and patents it now or hereafter owns or controls to make,
 * have made, use, import, offer to sell and sell ("Utilize") this software subject to the
 * terms herein.  With respect to the foregoing patent license, such license is granted
 * solely to the extent that any such patent is necessary to Utilize the software alone.
 * The patent license shall not apply to any combinations which include this software,
 * other than combinations with devices manufactured by or for TI ("TI Devices").
 * No hardware patent is licensed hereunder.
 *
 * Redistributions must preserve existing copyright notices and reproduce this license
 * (including the above copyright notice and the disclaimer and (if applicable) source
 * code license limitations below) in the documentation and/or other materials provided
 * with the distribution
 *
 * Redistribution and use in binary form, without modification, are permitted provided
 * that the following conditions are met:
 *
 * *       No reverse engineering, decompilation, or disassembly of this software is
 * permitted with respect to any software provided in binary form.
 *
 * *       any redistribution and use are licensed by TI for use only with TI Devices.
 *
 * *       Nothing shall obligate TI to provide you with source code for the software
 * licensed and provided to you in object code.
 *
 * If software source code is provided to you, modification and redistribution of the
 * source code are permitted provided that the following conditions are met:
 *
 * *       any redistribution and use of the source code, including any resulting derivative
 * works, are licensed by TI for use only with TI Devices.
 *
 * *       any redistribution and use of any object code compiled from the source code
 * and any resulting derivative works, are licensed by TI for use only with TI Devices.
 *
 * Neither the name of Texas Instruments Incorporated nor the names of its suppliers
 *
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * DISCLAIMER.
 *
 * THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
#pragma once
#ifndef PTK_SENSOR_DATA_PLAYER_INS_H
#define PTK_SENSOR_DATA_PLAYER_INS_H

/**
 * @file SensorDataPlayerINS.h
 * @brief class using SensorDataPlayer to implement a special type of
 *        sensor processing with INS. The assumption is that among the provided
 *        sensors, there is one INS sensor, which is supposed to be fed to
 *        the PTK routines for handling INS data. In other words,
 *        this class behaves just like a SensorDataPlayer except that the
 *        INS sensor is treated special by re-routing its data to PTK
 *        INS framework.
 *        The class takes care of initializing PTK INS buffers as well.
 *        Currently supports only Novatel INSPVA record as "INS sensor".
 *        This data player also initializes PTK_INS_getReferenceFrame based
 *        on the first INS record in the data sequence.
 */

#include <cstdint>
#include <string>
#include <iostream>

#include <perception/dbtools/sensor_data_player.h>
#include <perception/perception.h>

namespace ptk {

class SensorDataPlayerINS : public SensorDataPlayer
{
public:

    /* Constructor
      @param [in] cfg            database configuration file specifying available sensors
      @param [in] appTags        vector of strings. Sensors in @cfg with appTag field matching an @appTags vector entry
                                 will be used for sensor data replay.
                                 Ordering of @appTags establishes a sensor enumeration for application to
                                 reference a certain sensor,
                                 i.e., sensor with app tag @appTags[i] has sensor app id i.
      @param [in] insSensorId    sensor id (i.e., index in @appTags vector) for the INS sensor
      @param [in] intraFrameWait Flag to indicate if the player should implement the wait
                                 operation between successive frames based on two successive
                                 timestamps.
    */
    SensorDataPlayerINS(PTK_DBConfig *cfg,
                        const std::vector<std::string> &appTags,
                        std::uint32_t insSensorId,
                        bool intraFrameWait=false);

    /* Destructor */
    ~SensorDataPlayerINS();

    /*
     * Get the the record from the sensorstream whose current timestamp is smallest (next).
     *
     *  @param [out]  dataPtr         data pointer to the returned record, by reference
     *  @param [out]  dataSize        size of the returned record, by reference
     *  @param [out]  timestamp       timestamp for the returned record, by reference
     *
     *  Returns sensor app id of sensor returning this record.
     *  Returns MAX UINT32_T if no more records are available.
     */
    std::uint32_t get_next(void         *&dataPtr,
                           std::size_t   &dataSize,
                           std::uint64_t &timestamp);

    /*
     * Set time to given @timestamp. This rewinds all sensorstreams to the first
     * record after given @timestamp.
     * Also, re-initializes the INS buffer correctly.
     *
     *  @param [in]  timestamp   timestamp to set all sensorstreams
     *
     */
    void set_time(std::uint64_t timestamp);

private:
    std::uint32_t                _insSensorAppId; //INS sensor id passed by app
    std::uint32_t                _insSensorStrmId; //INS sensor id in stream vector
    void reset_ptk_ins_buffer(); //reset INS buffer to the state at time _curTime
    int32_t load_ins_data(std::uint64_t timestamp); //load missing INS data up to first record with timestamp >= given @timestamp
};

};

#endif
