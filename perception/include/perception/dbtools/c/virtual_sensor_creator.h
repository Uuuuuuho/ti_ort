/*
 *
 * Copyright (c) 2018 Texas Instruments Incorporated
 *
 * All rights reserved not granted herein.
 *
 * Limited License.
 *
 * Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
 * license under copyrights and patents it now or hereafter owns or controls to make,
 * have made, use, import, offer to sell and sell ("Utilize") this software subject to the
 * terms herein.  With respect to the foregoing patent license, such license is granted
 * solely to the extent that any such patent is necessary to Utilize the software alone.
 * The patent license shall not apply to any combinations which include this software,
 * other than combinations with devices manufactured by or for TI ("TI Devices").
 * No hardware patent is licensed hereunder.
 *
 * Redistributions must preserve existing copyright notices and reproduce this license
 * (including the above copyright notice and the disclaimer and (if applicable) source
 * code license limitations below) in the documentation and/or other materials provided
 * with the distribution
 *
 * Redistribution and use in binary form, without modification, are permitted provided
 * that the following conditions are met:
 *
 * *       No reverse engineering, decompilation, or disassembly of this software is
 * permitted with respect to any software provided in binary form.
 *
 * *       any redistribution and use are licensed by TI for use only with TI Devices.
 *
 * *       Nothing shall obligate TI to provide you with source code for the software
 * licensed and provided to you in object code.
 *
 * If software source code is provided to you, modification and redistribution of the
 * source code are permitted provided that the following conditions are met:
 *
 * *       any redistribution and use of the source code, including any resulting derivative
 * works, are licensed by TI for use only with TI Devices.
 *
 * *       any redistribution and use of any object code compiled from the source code
 * and any resulting derivative works, are licensed by TI for use only with TI Devices.
 *
 * Neither the name of Texas Instruments Incorporated nor the names of its suppliers
 *
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * DISCLAIMER.
 *
 * THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
#ifndef PTK_C_VIRTUAL_SENSOR_CREATOR_H
#define PTK_C_VIRTUAL_SENSOR_CREATOR_H

/**
 * \defgroup group_ptk_virtual_sensor Virtual Sensor Creator
 * \ingroup group_ptk_dbtools
 *
 */

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stdlib.h>

#include <perception/dbtools/c/dbconfig.h>

typedef void * vscHandle;

/**
 * \brief Constructor
 *
 * \param [in] databaseDir            base directory of database: WARNING: Use double backslashes for paths!
 * \param [in] seqId                  data seqence id for this sensor
 * \param [in] parentFolderName       subfolder name of the sensor, e.g. "camera002" or "lidar001", from which
 *                                    this virtual sensor is derived.
 *                                    Determines virtual sensor's folder name.
 *                                    Extrinsic calibration and sensor processor delay info  will be
 *                                    copied from this sensor.
 * \param [in] outFolderName          snake-style output folder name for the virtual sensor
 * \param [in] descTxtFiles           space separated list of text files that will be appended to the virtual
 *                                    sensor's intrinsic calibration file (e.g., parameters used to produce virtual sensor)
 * \param [in] dataTypechar           output data type, 0=raw (binary data) or 3=png (images)
 * \param [in] overwriteExistingData  erase existing data in destination folder, if destination folder already exists (1=yes, 0=no)?
 *
 * \return A handle to the virctaul sensor context.
 *
 * \ingroup group_ptk_virtual_sensor
 */
vscHandle VirtualSensorCreator_create(char      databaseDir[],
                                      int       seqId,
                                      char      parentFolderName[],
                                      char      outFolderName[],
                                      char      descTxtFiles[],
                                      uint32_t  dataTypechar,
                                      uint32_t  overwriteExistingData);


/*
 * \brief Constructor by DBConfig object and appTag
 * Searches in cfg's outputs for the appTag and if exists, creates the VirtualSensorCreator object.
 * If the appTag does not exist, creates a disabled VirtualSensorCreator object, whose methods have no effect.
 *
 * \param [in] cfg       pointer to PTK_DBConfig
 * \param [in] appTag    const char *
 *
 * \return A handle to the virctaul sensor context.
 *
 * \ingroup group_ptk_virtual_sensor
 */
vscHandle VirtualSensorCreator_create_by_dbconfig(PTK_DBConfig *cfg, const char * appTag);


/**
 * \brief Deletes a previously created context
 *
 * \param [in]   ss pointer to Virtual Sensor context.
 *
 * \ingroup group_ptk_virtual_sensor
 */
void VirtualSensorCreator_delete(vscHandle ss);

/**
 * \brief Checks to see if the virtual sensor has been disabled.
 *
 * \param [in]   ss Handle to the virtual sensor context.
 *
 * \return
 *        - 1 if disabled
 *        - 0, otherwise
 *
 * \ingroup group_ptk_virtual_sensor
 */
uint32_t VirtualSensorCreator_is_disabled(vscHandle ss);

/**
 * \brief Add record to virtual sensor (append timestamp in timestamps.txt).
 *
 * Depending on data type being "raw" or "png", the method behaves differently.
 *
 * For raw data:
 * Data pointed by dataPtr is written to bin file and
 * path to bin file is returned.
 *
 * For image data:
 * Does not write the data file, just returns the path for image to be written to.
 * !!! Application is expected to write its image data to that file !!!
 *
 * \param [in]  ss              Handle to the virtual sensor context
 * \param [out] dstFilePath     path to file written (BIN data) or to be written by user (PNG data)
 * \param [in]  timestamp       timestamp for the recorded to be written
 * \param [in]  dataPtr         pointer to data to be written (only BIN data), otherwise NULL
 * \param [in]  dataSize        size of data to be written (only BIN data), otherwise 0
 *
 * \ingroup group_ptk_virtual_sensor
 */
void VirtualSensorCreator_add_record(vscHandle ss,
                char   **dstFilePath,
                uint64_t timestamp,
                void    *dataPtr,
                size_t   dataSize);

#ifdef __cplusplus
}
#endif

#endif
