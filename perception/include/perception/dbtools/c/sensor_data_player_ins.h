/*
 *
 * Copyright (c) 2018 Texas Instruments Incorporated
 *
 * All rights reserved not granted herein.
 *
 * Limited License.
 *
 * Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
 * license under copyrights and patents it now or hereafter owns or controls to make,
 * have made, use, import, offer to sell and sell ("Utilize") this software subject to the
 * terms herein.  With respect to the foregoing patent license, such license is granted
 * solely to the extent that any such patent is necessary to Utilize the software alone.
 * The patent license shall not apply to any combinations which include this software,
 * other than combinations with devices manufactured by or for TI ("TI Devices").
 * No hardware patent is licensed hereunder.
 *
 * Redistributions must preserve existing copyright notices and reproduce this license
 * (including the above copyright notice and the disclaimer and (if applicable) source
 * code license limitations below) in the documentation and/or other materials provided
 * with the distribution
 *
 * Redistribution and use in binary form, without modification, are permitted provided
 * that the following conditions are met:
 *
 * *       No reverse engineering, decompilation, or disassembly of this software is
 * permitted with respect to any software provided in binary form.
 *
 * *       any redistribution and use are licensed by TI for use only with TI Devices.
 *
 * *       Nothing shall obligate TI to provide you with source code for the software
 * licensed and provided to you in object code.
 *
 * If software source code is provided to you, modification and redistribution of the
 * source code are permitted provided that the following conditions are met:
 *
 * *       any redistribution and use of the source code, including any resulting derivative
 * works, are licensed by TI for use only with TI Devices.
 *
 * *       any redistribution and use of any object code compiled from the source code
 * and any resulting derivative works, are licensed by TI for use only with TI Devices.
 *
 * Neither the name of Texas Instruments Incorporated nor the names of its suppliers
 *
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * DISCLAIMER.
 *
 * THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
#ifndef PTK_C_SENSOR_DATA_PLAYER_H
#define PTK_C_SENSOR_DATA_PLAYER_H

/**
 * \file sensor_data_player_ins.h
 *
 * \brief C-wrapper for SensorDataPlayerINS class
 *
 */

/**
 * \defgroup group_ptk_sensor_data_player Sensor Data Player
 * \ingroup group_ptk_dbtools
 *
 */

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stdlib.h>

#include <perception/dbtools/c/dbconfig.h>
#include <perception/dbtools/c/sensorstream.h>

typedef void * sensorDataPlayerInsHandle;

/**
 * \brief Creates a sensor data player context.
 *
 * \param [in] cfg         database configuration file specifying available sensors
 * \param [in] appTags     string (char array) array. Sensors in cfg with appTag field matching an appTags vector entry
 *                         will be used for sensor data replay.
 *                         Ordering of appTags establishes a sensor enumeration for application to
 *                         reference a certain sensor,
 *                         i.e., sensor with app tag appTags[i] has sensor app id i.
 * \param [in] numAppTags  number of app tags (second dimension of appTags)
 * \param [in] insSensorId sensor id (i.e., index in appTags vector) for the INS sensor
 *
 * \return
 *        - Handle to SensorDataPlayerINS object, if successful
 *        - NULL, otherwise
 *
 * \ingroup group_ptk_sensor_data_player
 */
sensorDataPlayerInsHandle SensorDataPlayerINS_create(PTK_DBConfig *cfg,
                           char appTags[][DBCONFIG_MAX_WORD_LEN],
                           uint32_t numAppTags,
                           uint32_t insSensorId,
                           uint8_t intraFrameWait);

/**
 * \brief Deletes a previously created context
 *
 * \param [in]   hdl pointer to SensorDataPlayerINS object
 *
 * \ingroup group_ptk_sensor_data_player
 */
void SensorDataPlayerINS_delete(sensorDataPlayerInsHandle hdl);

/**
 * \brief Get the the record from the sensorstream whose current timestamp is
 *        smallest (next).
 *
 * \param [in]   hdl       pointer to SensorDataPlayerINS object
 * \param [out]  dataPtr   void pointer to data pointer to the returned record
 * \param [out]  dataSize  size_t pointer to size of the returned record
 * \param [out]  timestamp uint64_t pointer to timestamp for the returned record
 *
 * \return
 *        - sensor app id of sensor returning this record.
 *        - MAX UINT32_T if no more records are available.
 *
 * \ingroup group_ptk_sensor_data_player
 */
uint32_t SensorDataPlayerINS_get_next(sensorDataPlayerInsHandle hdl,
                                      void         **dataPtr,
                                      size_t        *dataSize,
                                      uint64_t      *timestamp);

/**
 * \brief Set time to given timestamp. This rewinds all sensorstreams to the
 *        first record after given timestamp. Also, re-initializes the INS
 *        buffer correctly.
 *
 * \param [in]   hdl      pointer to SensorDataPlayerINS object
 * \param [in]  timestamp uint64_t timestamp to set all sensorstreams
 *
 * \ingroup group_ptk_sensor_data_player
 *
 */
void SensorDataPlayerINS_set_time(sensorDataPlayerInsHandle hdl,
                                  uint64_t timestamp);

/**
 * \brief Get access to the sensorstream for given sensor app id
 *
 * \param [in]  hdl         pointer to SensorDataPlayerINS object
 * \param [in]  sensorAppId sensor's app id (refer to Constructor brief for details)
 *
 * \return
 *        - sensorstream handle, if stream is active
 *        - nullptr if sensorstream is inactive
 *
 * \ingroup group_ptk_sensor_data_player
 */
ssHandle SensorDataPlayerINS_get_sensorstream(sensorDataPlayerInsHandle hdl,
                                              uint32_t sensorAppId);


void SensorDataPlayerINS_check_sensorstreams(sensorDataPlayerInsHandle hdl, uint64_t *time);

void SensorDataPlayerINS_getEndTime(sensorDataPlayerInsHandle hdl, uint32_t sensorAppId, uint64_t *time);

#ifdef __cplusplus
}
#endif

#endif
