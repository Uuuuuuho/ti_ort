/*
 *
 * Copyright (c) 2018 Texas Instruments Incorporated
 *
 * All rights reserved not granted herein.
 *
 * Limited License.
 *
 * Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
 * license under copyrights and patents it now or hereafter owns or controls to make,
 * have made, use, import, offer to sell and sell ("Utilize") this software subject to the
 * terms herein.  With respect to the foregoing patent license, such license is granted
 * solely to the extent that any such patent is necessary to Utilize the software alone.
 * The patent license shall not apply to any combinations which include this software,
 * other than combinations with devices manufactured by or for TI ("TI Devices").
 * No hardware patent is licensed hereunder.
 *
 * Redistributions must preserve existing copyright notices and reproduce this license
 * (including the above copyright notice and the disclaimer and (if applicable) source
 * code license limitations below) in the documentation and/or other materials provided
 * with the distribution
 *
 * Redistribution and use in binary form, without modification, are permitted provided
 * that the following conditions are met:
 *
 * *       No reverse engineering, decompilation, or disassembly of this software is
 * permitted with respect to any software provided in binary form.
 *
 * *       any redistribution and use are licensed by TI for use only with TI Devices.
 *
 * *       Nothing shall obligate TI to provide you with source code for the software
 * licensed and provided to you in object code.
 *
 * If software source code is provided to you, modification and redistribution of the
 * source code are permitted provided that the following conditions are met:
 *
 * *       any redistribution and use of the source code, including any resulting derivative
 * works, are licensed by TI for use only with TI Devices.
 *
 * *       any redistribution and use of any object code compiled from the source code
 * and any resulting derivative works, are licensed by TI for use only with TI Devices.
 *
 * Neither the name of Texas Instruments Incorporated nor the names of its suppliers
 *
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * DISCLAIMER.
 *
 * THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
#ifndef PTK_C_SENSORSTREAM_H
#define PTK_C_SENSORSTREAM_H

/**
 * @file sensorstream.h
 *
 * @brief C-wrapper for senstream.h C++ class
 *
 */

/**
 * \defgroup group_ptk_sensor_stream Sensor Stream Handling
 * \ingroup group_ptk_dbtools
 *
 */

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stddef.h>

typedef void * ssHandle;

/**
 * \brief 
 *
 * \param [in] databaseDir      base directory of database: WARNING: Use double backslashes for paths! \\
 * \param [in] seqId            data seqence id for this sensor
 * \param [in] sensorFolderName subfolder name of the sensor, e.g. "camera002" or "lidar001"
 *
 * \return Handle to the creates sensor stream.
 *
 * \ingroup group_ptk_sensor_stream
 */
ssHandle sensorstream_create(char databaseDir[], uint32_t seqId, char sensorFolderName[]);

/**
 * \brief Deletes the previously created sensor stream context.
 *
 * \param [in] ss  Sensor stream handle
 *
 * \ingroup group_ptk_sensor_stream
 */
void sensorstream_delete(ssHandle ss);

/**
 * \brief Set next record in stream to be the one before the time indicated by
 *        timestampIn.
 *
 * \param [in] ss  Sensor stream handle
 *
 * \param [in] timestampIn Reference time stamp.
 *
 * \ingroup group_ptk_sensor_stream
 */
void sensorstream_set_stream_before(ssHandle ss, uint64_t timestampIn);

/**
 * \brief Set next record in stream to be the one after the time indicated by
 *        timestampIn.
 *
 * \param [in] ss  Sensor stream handle
 *
 * \param [in] timestampIn Reference time stamp.
 *
 * \ingroup group_ptk_sensor_stream
 */
void sensorstream_set_stream_after(ssHandle ss, uint64_t timestampIn);

/**
 * \brief Set next record in stream to be the one at file position 'posIn'.
 *
 * \param [in] ss  Sensor stream handle
 *
 * \param [in] posIn File position.
 *
 * \ingroup group_ptk_sensor_stream
 */
void sensorstream_set_stream_pos(ssHandle ss, int posIn);

/**
 * \brief Extracts the position and timestamp information of the next record in
 *        the stream.
 *
 * \param [in] ss  Sensor stream handle
 *
 * \param [out] nextPosOut Position of the record. If there are no more records
 *                         in the stream then this will be set to -1.
 *
 * \param [out] nextTimestampOut Timestamp of the record. This is valid only if
 *                               nextPosOut != -1.
 *
 * \ingroup group_ptk_sensor_stream
 */
void sensorstream_check_stream(ssHandle ss, int *nextPosOut, uint64_t *nextTimestampOut);

/**
 * \brief Gets the next record in the stream.
 *
 * \param [in] ss  Sensor stream handle
 *
 * \param [out] pDataOut Pointer to the record.
 *
 * \param [out] dataSizeOut Size of the record.
 *
 * \param [out] timestampOut Timestamp associated with the record.
 *
 * \return
 *        - >=0, if successful
 *        - -1, otherwise
 *
 * \ingroup group_ptk_sensor_stream
 */
int sensorstream_get_stream_next(ssHandle ss, void **pDataOut, size_t *dataSizeOut, uint64_t *timestampOut);

/**
 * \brief Gets the data record before the provided timestamp.
 *
 * \param [in] ss  Sensor stream handle
 *
 * \param [in] timestampIn reference timestamp
 *
 * \param [out] pDataOut Pointer to the record.
 *
 * \param [out] dataSizeOut Size of the record.
 *
 * \param [out] timestampOut Timestamp associated with the record.
 *
 * \return
 *        - >=0, if successful
 *        - -1, otherwise
 *
 * \ingroup group_ptk_sensor_stream
 */
int sensorstream_get_before(ssHandle ss, uint64_t timestampIn, void **pDataOut, size_t *dataSizeOut, uint64_t *timestampOut);

/**
 * \brief Gets the data record after the provided timestamp.
 *
 * \param [in] ss  Sensor stream handle
 *
 * \param [in] timestampIn reference timestamp
 *
 * \param [out] pDataOut Pointer to the record.
 *
 * \param [out] dataSizeOut Size of the record.
 *
 * \param [out] timestampOut Timestamp associated with the record.
 *
 * \return
 *        - >=0, if successful
 *        - -1, otherwise
 *
 * \ingroup group_ptk_sensor_stream
 */
int sensorstream_get_after(ssHandle ss, uint64_t timestampIn, void **pDataOut, size_t *dataSizeOut, uint64_t *timestampOut);

/**
 * \brief Gets the record at file position indicated by 'posIn'.
 *
 * \param [in] ss  Sensor stream handle
 *
 * \param [in] posIn Position to look for.
 *
 * \param [out] pDataOut Pointer to the record.
 *
 * \param [out] dataSizeOut Size of the record.
 *
 * \param [out] timestampOut Timestamp associated with the record.
 *
 * \return Size required in bytes.
 *
 * \ingroup group_ptk_sensor_stream
 */
int sensorstream_get_pos(ssHandle ss, int posIn, void **pDataOut, size_t *dataSizeOut, uint64_t *timestampOut);

/**
 * \brief Checks if the data handled by the stream is of type binary.
 *
 * \param [in] ss  Sensor stream handle
 *
 * \return
 *        - 0, if the data is non-binary
 *        - >0, otherwise
 *
 * \ingroup group_ptk_sensor_stream
 */
unsigned int sensorstream_is_binary_data(ssHandle ss);

/**
 * \brief Get the total number of records available in this sensor.
 *
 * \param [in] ss  Sensor stream handle
 *
 * \return Total number of records
 *
 * \ingroup group_ptk_sensor_stream
 */
int sensorstream_records(ssHandle ss);

/**
 * \brief Get the current timestamp of the sensor stream i.e., the timestamp of
 *        the most recent record.
 *
 * \param [in] ss  Sensor stream handle
 *
 * \return
 *        - timestamp of the record most recent record
 *        - -1, if no records exist
 *
 * \ingroup group_ptk_sensor_stream
 */
uint64_t sensorstream_current_timestamp(ssHandle ss);

/**
 * \brief Reads the extrinsic calibration data for the sensor.
 *
 * \param [in] ss  Sensor stream handle
 *
 * \param [out] M_c_e Extrinsic calibration data will be written here. The
 *              data is of size 12 elements of type float. The fields are as
 *              shown below:
 *              (Rxx, Rxy, Rxz, Ryx, Ryy, Ryz, Rzx, Rzy, Rzz, tx, ty, tz)
 *
 * \return
 *        - 0, if successful
 *        - -1, otherwise
 *
 * \ingroup group_ptk_sensor_stream
 */
int sensorstream_get_extrinsic_calibration(ssHandle ss, float M_c_e[12]);

/**
 * \brief Get path of sensor directory,
 *        e.g. "/home/user/database_dir/sequence0003/camera002"
 *
 * \param [in] ss  Sensor stream handle
 *
 * \param [out] path Absolute path to the sensor stream directory.
 *
 * \ingroup group_ptk_sensor_stream
 */
void sensorstream_get_full_dir_path(ssHandle ss, char *path);

#ifdef __cplusplus
}
#endif

#endif
