/*
 *
 * Copyright (c) 2018 Texas Instruments Incorporated
 *
 * All rights reserved not granted herein.
 *
 * Limited License.
 *
 * Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
 * license under copyrights and patents it now or hereafter owns or controls to make,
 * have made, use, import, offer to sell and sell ("Utilize") this software subject to the
 * terms herein.  With respect to the foregoing patent license, such license is granted
 * solely to the extent that any such patent is necessary to Utilize the software alone.
 * The patent license shall not apply to any combinations which include this software,
 * other than combinations with devices manufactured by or for TI ("TI Devices").
 * No hardware patent is licensed hereunder.
 *
 * Redistributions must preserve existing copyright notices and reproduce this license
 * (including the above copyright notice and the disclaimer and (if applicable) source
 * code license limitations below) in the documentation and/or other materials provided
 * with the distribution
 *
 * Redistribution and use in binary form, without modification, are permitted provided
 * that the following conditions are met:
 *
 * *       No reverse engineering, decompilation, or disassembly of this software is
 * permitted with respect to any software provided in binary form.
 *
 * *       any redistribution and use are licensed by TI for use only with TI Devices.
 *
 * *       Nothing shall obligate TI to provide you with source code for the software
 * licensed and provided to you in object code.
 *
 * If software source code is provided to you, modification and redistribution of the
 * source code are permitted provided that the following conditions are met:
 *
 * *       any redistribution and use of the source code, including any resulting derivative
 * works, are licensed by TI for use only with TI Devices.
 *
 * *       any redistribution and use of any object code compiled from the source code
 * and any resulting derivative works, are licensed by TI for use only with TI Devices.
 *
 * Neither the name of Texas Instruments Incorporated nor the names of its suppliers
 *
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * DISCLAIMER.
 *
 * THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
#ifndef PTK_C_DBCONFIG_H
#define PTK_C_DBCONFIG_H

/**
 * @file dbconfig.h
 *
 * @brief C-wrapper for parsing database config files
 *
 */

/**
 * \defgroup group_ptk_db_config Database Configuration
 * \ingroup group_ptk_dbtools
 *
 */

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include <stdlib.h>

#include <perception/base/sensor_types.h>

#define DBCONFIG_MAX_PATH_LEN (4096U)

/*
 * \brief Maximum word length. Words are tags, folder names, file names, etc.
 *
 * \ingroup group_ptk_db_config
 */
#define DBCONFIG_MAX_WORD_LEN (256U)

/**
 * \brief Maximum number of sensors per data sequence.
 *
 * \ingroup group_ptk_db_config
 */
#define DBCONFIG_MAX_NUM_SENSORS (20U)

/**
 * \brief Maximum number of virtual sensor outputs.
 *
 * \ingroup group_ptk_db_config
 */
#define DBCONFIG_MAX_NUM_OUTPUTS (10U)

/**
 * \brief Maximum number of description text files that can be passed to create
 *        a virtual sensor.
 *
 * \ingroup group_ptk_db_config
 */
#define DBCONFIG_MAX_DESC_FILES (10U)

/**
 * \brief Sensor type enumeration.
 *
 * \ingroup group_ptk_db_config
 */
typedef enum dbconfig_sensor_type_e
{
    dbconfig_sensor_type_CAMERA = PTK_SensorType_CAMERA,
    dbconfig_sensor_type_RADAR  = PTK_SensorType_RADAR,
    dbconfig_sensor_type_LIDAR  = PTK_SensorType_LIDAR,
    dbconfig_sensor_type_INS    = PTK_SensorType_INS,
    dbconfig_sensor_type_IMU    = PTK_SensorType_IMU,
    dbconfig_sensor_type_GPS    = PTK_SensorType_GPS,
    dbconfig_sensor_type_TIME   = PTK_SensorType_TIME,
    dbconfig_sensor_type_OTHER  = PTK_SensorType_OTHER,
    dbconfig_sensor_type_MAX    = PTK_SensorType_MAX

} PTK_DBConfig_SensorType;

/**
 * \brief Sensor Input
 *
 * \ingroup group_ptk_db_config
 */
typedef struct dbconfig_sensor_t
{
    /** Sub-folder name for the sensor */
    char                        folder[DBCONFIG_MAX_WORD_LEN];

    /** Sensor-type */
    PTK_DBConfig_SensorType     type;

    /** Application-specific tag. */
    char                        appTag[DBCONFIG_MAX_WORD_LEN];

} PTK_DBConfig_Sensor;

/**
 * \brief Virtual sensor output.
 *
 * \ingroup group_ptk_db_config
 */
typedef struct dbconfig_virtual_sensor_t
{
    /** Sub-folder name for the virtual sensor. */
    char        folder[DBCONFIG_MAX_WORD_LEN];

    /** Application-specific tag. */
    char        appTag[DBCONFIG_MAX_WORD_LEN];

    /** 1 if parent sensor is specified, 0 otherwise */
    uint32_t    parentSensorSpecified;

    /** Sub-folder name for the parent-sensor */
    char        parentSensorFolder[DBCONFIG_MAX_WORD_LEN];

    /** Data type (allowed: 0=BIN, 3=PNG, 4=BMP) */
    uint32_t    dataType;

    /** Number of desc txt files */
    uint32_t    numDescFiles;

    /** Paths to the desc text files */
    char        descFilesPaths[DBCONFIG_MAX_DESC_FILES][DBCONFIG_MAX_PATH_LEN];

} PTK_DBConfig_VirtualSensor;

/**
 * \brief Database configuration.
 *
 * \ingroup group_ptk_db_config
 */
typedef struct dbconfig_t
{
    /** Database path. */
    char                        databasePath[DBCONFIG_MAX_PATH_LEN];

    /** Data sequence id. */
    uint32_t                    dataSeqId;

    /** Reference sensor for which startFrame and endFrame are defined */
    PTK_DBConfig_Sensor         refSensor;

    /** First frame to be processed. */
    uint32_t                    startFrame;

    /** Last frame to be processed. */
    uint32_t                    endFrame;

    /** Number of frames to be processed. */
    uint32_t                    numFrames;

    /* INPUT SENSORS */

    /** Number of sensors in the system. */
    uint32_t                    numSensors;

    /** The sensors in the system */
    PTK_DBConfig_Sensor         sensors[DBCONFIG_MAX_NUM_SENSORS];

    /** Number of sensors per type. */
    uint32_t                    numSensorsPerType[dbconfig_sensor_type_MAX];

    /** OUTPUT SENSORS */

    /** Number of outputs (virtual sensors) */
    uint32_t                    numOutputs;

    /** The outputs (virtual sensors). */
    PTK_DBConfig_VirtualSensor  outputs[DBCONFIG_MAX_NUM_OUTPUTS];

    /** Enable automatic erasing of existing data (if output folder already exists) */
    uint32_t                    overwriteExistingData;

} PTK_DBConfig;

/**
 * \brief Parse a config file to initialize the DBConfig struct
 *
 * \param [out] config          pointer to DBConfig struct
 * \param [in]  configFilePath  string to configuration file
 *
 * \return
 *        - 0 on success
 *        - -1 on failure
 *
 * \ingroup group_ptk_db_config
*/
int32_t PTK_DBConfig_parse(PTK_DBConfig *config, const char *configFilePath);

/**
 * \brief Print a config file via PTK_print()
 *
 * \param [in] config pointer to DBConfig struct
 *
 * \ingroup group_ptk_db_config
*/
void PTK_DBConfig_print(PTK_DBConfig *config);

/**
 * \brief Check if a sensor with this apptag exists
 *
 * \param [in] config pointer to DBConfig struct
 * \param [in] appTag string with apptag to be checked
 *
 * \return
 *        - index in sensors array where apptag was found
 *        - -1 if not found
 *
 * \ingroup group_ptk_db_config
*/
int32_t PTK_DBConfig_exist_sensor_apptag(PTK_DBConfig *config, const char *appTag);

/**
 * \brief Check if an output with this apptag exists
 *
 * \param [in] config pointer to DBConfig struct
 * \param [in] appTag string with apptag to be checked
 *
 * \return
 *        - index in outputs array where apptag was found
 *        - -1 if not found
 *
 * \ingroup group_ptk_db_config
*/
int32_t PTK_DBConfig_exist_output_apptag(PTK_DBConfig *config, const char *appTag);

#ifdef __cplusplus
}
#endif

#endif
