/*
 *
 * Copyright (c) 2019 Texas Instruments Incorporated
 *
 * All rights reserved not granted herein.
 *
 * Limited License.
 *
 * Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
 * license under copyrights and patents it now or hereafter owns or controls to make,
 * have made, use, import, offer to sell and sell ("Utilize") this software subject to the
 * terms herein.  With respect to the foregoing patent license, such license is granted
 * solely to the extent that any such patent is necessary to Utilize the software alone.
 * The patent license shall not apply to any combinations which include this software,
 * other than combinations with devices manufactured by or for TI ("TI Devices").
 * No hardware patent is licensed hereunder.
 *
 * Redistributions must preserve existing copyright notices and reproduce this license
 * (including the above copyright notice and the disclaimer and (if applicable) source
 * code license limitations below) in the documentation and/or other materials provided
 * with the distribution
 *
 * Redistribution and use in binary form, without modification, are permitted provided
 * that the following conditions are met:
 *
 * *       No reverse engineering, decompilation, or disassembly of this software is
 * permitted with respect to any software provided in binary form.
 *
 * *       any redistribution and use are licensed by TI for use only with TI Devices.
 *
 * *       Nothing shall obligate TI to provide you with source code for the software
 * licensed and provided to you in object code.
 *
 * If software source code is provided to you, modification and redistribution of the
 * source code are permitted provided that the following conditions are met:
 *
 * *       any redistribution and use of the source code, including any resulting derivative
 * works, are licensed by TI for use only with TI Devices.
 *
 * *       any redistribution and use of any object code compiled from the source code
 * and any resulting derivative works, are licensed by TI for use only with TI Devices.
 *
 * Neither the name of Texas Instruments Incorporated nor the names of its suppliers
 *
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * DISCLAIMER.
 *
 * THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
#if !defined(_LIDAR_CAPTURE_H_)
#define _LIDAR_CAPTURE_H_

#include <perception/drv/lidar_capture_drv.h>
#include <perception/utils/ipc_chan.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * \brief LIDAR capture handle.
 *
 * \ingroup group_ptk_drv_lidar_capture
 */
typedef void * PTK_LidarCapHandle;

/**
 * \brief Initializes the LIDAR capture module with the configuration
 *        parameters provided.
 *
 * \param [in] cfgParams Capture configuration parameters.
 *
 * \return
 *        - A valid capture handle, if successful.
 *        - NULL, otherwise.
 *
 * \ingroup group_ptk_drv_lidar_capture
 */
PTK_LidarCapHandle PTK_LidarCapCreate(const PTK_Drv_LidarDrvParams * cfgParams);

/**
 * \brief Registers the provided callback. This callback is invoked everytime
 *        there is a new data capture available form the LIDAR driver. If the
 *        callback is NULL then the capture module skips the callback
 *        invocation.
 *
 *        When the callback is invoked it will be called with the registered
 *        'appData' and a data descriptor. The Appliation needs to release
 *        the descriptor back to the capture block by invoking the folloiwng
 *        API. Failure to do so will starve the capture block and eventually
 *        it will stall. 
 *        - PTK_IPC_BuffDescRelease(desc)
 *
 * \param [in] handle Capture handle.
 *
 * \param [in] cb Callback function.
 *
 * \param [in] appData Optional application specific pointer to be returned
 *             with the callback invocation.
 *
 * \ingroup group_ptk_drv_lidar_capture
 */
void PTK_LidarCapRegisterAppCb(PTK_LidarCapHandle   handle,
                               PTK_IPC_Cb           cb,
                               void               * appData);

/**
 * \brief Enqueues a descriptor to the capture queue. The application would use
 *        this API to provide multiple buffers to the capture block. These
 *        descriptors are returned with data when a capture is complete.
 *
 *        The following fields need to be set for each descriptor:
 *        numBuffs       --> This should be 1;
 *        payloadSize[0] --> This should be based on the maximum LIDAR data
 *                           packet size expected.
 *        payload[0]     --> Pointer to the buffer memory to hold the LIDAR data
 *
 * \param [in] handle Capture handle.
 *
 * \param [in] desc Descriptor to hold capture data.
 *
 * \return
 *        - PTK_DRV_RET_SUCCESS, if successful.
 *        - < error code, otherwise.
 *
 * \ingroup group_ptk_drv_lidar_capture
 */
int32_t PTK_LidarCapEnqueDesc(PTK_LidarCapHandle    handle,
                              PTK_IPC_BuffDesc    * desc);

/**
 * \brief Spawns two threads internally, one for capturing the data using the
 *        LIDAR driver and the other one for processing the captured packet
 *        (ex:- invoking the application registeerd callbakc function.
 *
 * \param [in] handle Capture handle.
 *
 * \ingroup group_ptk_drv_lidar_capture
 */
void PTK_LidarCapSpawnThreads(PTK_LidarCapHandle handle);

/**
 * \brief De-initializes the capture context.
 *
 * \param [in] handle Capture handle.
 *
 * \ingroup group_ptk_drv_lidar_capture
 */
void PTK_LidarCapDelete(PTK_LidarCapHandle handle);

#ifdef __cplusplus
}
#endif

#endif // !defined(_LIDAR_CAPTURE_H_)

