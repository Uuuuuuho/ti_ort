/*
 *
 * Copyright (c) 2019 Texas Instruments Incorporated
 *
 * All rights reserved not granted herein.
 *
 * Limited License.
 *
 * Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
 * license under copyrights and patents it now or hereafter owns or controls to make,
 * have made, use, import, offer to sell and sell ("Utilize") this software subject to the
 * terms herein.  With respect to the foregoing patent license, such license is granted
 * solely to the extent that any such patent is necessary to Utilize the software alone.
 * The patent license shall not apply to any combinations which include this software,
 * other than combinations with devices manufactured by or for TI ("TI Devices").
 * No hardware patent is licensed hereunder.
 *
 * Redistributions must preserve existing copyright notices and reproduce this license
 * (including the above copyright notice and the disclaimer and (if applicable) source
 * code license limitations below) in the documentation and/or other materials provided
 * with the distribution
 *
 * Redistribution and use in binary form, without modification, are permitted provided
 * that the following conditions are met:
 *
 * *       No reverse engineering, decompilation, or disassembly of this software is
 * permitted with respect to any software provided in binary form.
 *
 * *       any redistribution and use are licensed by TI for use only with TI Devices.
 *
 * *       Nothing shall obligate TI to provide you with source code for the software
 * licensed and provided to you in object code.
 *
 * If software source code is provided to you, modification and redistribution of the
 * source code are permitted provided that the following conditions are met:
 *
 * *       any redistribution and use of the source code, including any resulting derivative
 * works, are licensed by TI for use only with TI Devices.
 *
 * *       any redistribution and use of any object code compiled from the source code
 * and any resulting derivative works, are licensed by TI for use only with TI Devices.
 *
 * Neither the name of Texas Instruments Incorporated nor the names of its suppliers
 *
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * DISCLAIMER.
 *
 * THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
#if !defined(_INS_CAPTURE_DRV_H_)
#define _INS_CAPTURE_DRV_H_

#include <stdint.h>

/**
 * \defgroup group_ptk_drv_ins_capture PTK Ins capture driver
 * \ingroup group_ptk_drv
 *
 * Provides API for the TCP socket based INS driver.
 */

#ifdef __cplusplus
extern "C" {
#endif

#include <perception/perception.h>
#include <perception/drv/drv_common.h>
#include <perception/utils/api_memory_if.h>
#include <perception/net/net_utils.h>
#include <perception/sensors/ins/novatel_defs.h>

/**
 * \brief INS capture TCP port number.
 * \ingroup group_ptk_drv_ins_capture
 */
#define INS_CAPTURE_DRV_IMU_PORT            (3001U)

/**
 * \brief Value to indicate invalid socket descriptor.
 * \ingroup group_ptk_drv_ins_capture
 */
#define INS_CAPTURE_DRV_INVALID_SOCK_ID     (-1)

/**
 * \brief Maximum number of configurable log commands.
 * \ingroup group_ptk_drv_ins_capture
 */
#define INS_CAPTURE_DRV_MAX_NUM_LOG_CMDS    (8U)

/**
 * \brief Timestamp field size.
 * \ingroup group_ptk_drv_ins_capture
 */
#define INS_CAPTURE_DRV_TS_FIELD_LENGTH     (sizeof(uint64_t))

/**
 * \brief Maximum length of an INS message.
 * \ingroup group_ptk_drv_ins_capture
 */
#define INS_CAPTURE_DRV_MAX_MSG_LENGTH      (1500U +\
                                             INS_CAPTURE_DRV_TS_FIELD_LENGTH)

/**
 * \brief Length of the IP address string
 *
 * IP v4 address will have a max of 15 characters
 * aaa.bbb.ccc.ddd
 * The last character of the string should be a null character.
 *
 * \ingroup group_ptk_drv_ins_capture
 */
#define INS_CAPTURE_DRV_IP_ADDR_LENGTH      (16)

/**
 * \brief Ins capture driver type.
 *        Currently only network driver supported.
 *
 * \ingroup group_ptk_drv_ins_capture
 */
typedef enum
{
    /** Network (TCP) based driver. */
    PTK_Drv_InsDrvType_NETWORK = 0

} PTK_Drv_InsDrvType;

/**
 * \brief Ins capture driver type.
 *        Currently only network driver supported.
 *
 * \ingroup group_ptk_drv_ins_capture
 */
typedef struct
{
    /** Message to be logged. */
    PTK_INS_NovatelMsgId        msgId;

    /** Message log on condition (on time, on change). */
    PTK_INS_NovatelLogOnCtrl    logCtrl;

    /** Interval in sec. Only valid when log is ON_TIME. */
    float                       interval;

} PTK_Drv_InsLogCmd;

/**
 * \brief Ins capture driver configuration parameters.
 *
 * \ingroup group_ptk_drv_ins_capture
 */
typedef struct
{
    /** Driver type. */
    PTK_Drv_InsDrvType  drvType;

    /** Server IP address to connect to. */
    char                ipAddr[INS_CAPTURE_DRV_IP_ADDR_LENGTH];

    /** Server port to connect to. */
    int16_t             portNum;

    /** Number of messages to pack into one output packet. */
    uint8_t             numMsgsPerBuffer;

    /** Number of valid log commands. */
    uint8_t             numLogCmds;

    /** Log commands to configure. */
    PTK_Drv_InsLogCmd   logCmds[INS_CAPTURE_DRV_MAX_NUM_LOG_CMDS];

} PTK_Drv_InsDrvParams;

/**
 * \brief Computes the memory requirements for the driver.
 *
 * \param [in] cfgParams Driver configuration parameters.
 *
 * \param [out] memReq Memory requirements as specified below
 *             - entry [0]: Memory for internal context. Best allocated from
 *                          the fastest memory.
 *             - entry [1]: Memory for the output buffer. The output buffer is
 *                          allocated by the Application so this entry is just
 *                          to communicate the output buffer size. No need to
 *                          pass the memory to the library in the init call
 *                          PTK_Drv_InsCaptureInit().
 *
 * \return
 *        - PTK_DRV_RET_SUCCESS, if successful.
 *        - < error code, otherwise.
 *
 * \ingroup group_ptk_drv_ins_capture
 */
int32_t PTK_Drv_InsCaptureConfig(const PTK_Drv_InsDrvParams   * cfgParams,
                                 PTK_Api_MemoryReq            * memReq);

/**
 * \brief Initializes the driver. A socket will be created to communicate with
 *        the INS server. No attempt will be made to connect to the server. It
 *        must be done later using PTK_Drv_InsCaptureConnect() API.
 *
 * \param [in] cfgParams Driver configuration parameters.
 *
 * \param [in] memRsp Memory blocks as specified below
 *             - entry [0]: Memory for internal context.
 *
 * \return
 *        - valid handle, if successful.
 *        - NULL, otherwise.
 *
 * \ingroup group_ptk_drv_ins_capture
 */
PTK_DrvHandle
PTK_Drv_InsCaptureInit(const PTK_Drv_InsDrvParams * cfgParams,
                       const PTK_Api_MemoryRsp    * memRsp);

/**
 * \brief Tries to connect to the INS server over a TCP socket created during
 *        the initialization phase (i.e. using PTK_Drv_InsCaptureInit()) API.
 *
 * \param [inout] drvHandle Driver handle.
 *
 * \return
 *        - PTK_DRV_RET_SUCCESS, if successful.
 *        - < error code, otherwise.
 *
 * \ingroup group_ptk_drv_ins_capture
 */
int32_t
PTK_Drv_InsCaptureConnect(PTK_DrvHandle     drvHandle);

/**
 * \brief Configures the sensor with user defined messages.
 *
 * \param [inout] drvHandle Driver handle.
 *
 * \return
 *        - PTK_DRV_RET_SUCCESS, if successful.
 *        - < error code, otherwise.
 *
 * \ingroup group_ptk_drv_ins_capture
 */
int32_t
PTK_Drv_InsConfigLogs(PTK_DrvHandle     drvHandle);

/**
 * \brief Captures the Ins scan data into the output buffer. The buffer
 *        contains one fill Velodyne Packet.
 *
 * \param [inout] drvHandle Driver handle.
 *
 * \param [out] outBuff Output Buffer.
 *
 * \return
 *        - PTK_DRV_RET_SUCCESS, if successful.
 *        - < error code, otherwise.
 *
 * \ingroup group_ptk_drv_ins_capture
 */
int32_t PTK_Drv_InsCaptureProcess(PTK_DrvHandle     drvHandle,
                                  uint8_t         * outBuff);

/**
 * \brief De-initializes the driver context.
 *
 * \param [inout] drvHandle Driver handle.
 *
 * \ingroup group_ptk_drv_ins_capture
 */
void PTK_Drv_InsCaptureDeInit(PTK_DrvHandle drvHandle);

#ifdef __cplusplus
}
#endif

#endif // !defined(_INS_CAPTURE_DRV_H_)

