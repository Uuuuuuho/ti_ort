/*
 *
 * Copyright (c) 2019 Texas Instruments Incorporated
 *
 * All rights reserved not granted herein.
 *
 * Limited License.
 *
 * Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
 * license under copyrights and patents it now or hereafter owns or controls to make,
 * have made, use, import, offer to sell and sell ("Utilize") this software subject to the
 * terms herein.  With respect to the foregoing patent license, such license is granted
 * solely to the extent that any such patent is necessary to Utilize the software alone.
 * The patent license shall not apply to any combinations which include this software,
 * other than combinations with devices manufactured by or for TI ("TI Devices").
 * No hardware patent is licensed hereunder.
 *
 * Redistributions must preserve existing copyright notices and reproduce this license
 * (including the above copyright notice and the disclaimer and (if applicable) source
 * code license limitations below) in the documentation and/or other materials provided
 * with the distribution
 *
 * Redistribution and use in binary form, without modification, are permitted provided
 * that the following conditions are met:
 *
 * *       No reverse engineering, decompilation, or disassembly of this software is
 * permitted with respect to any software provided in binary form.
 *
 * *       any redistribution and use are licensed by TI for use only with TI Devices.
 *
 * *       Nothing shall obligate TI to provide you with source code for the software
 * licensed and provided to you in object code.
 *
 * If software source code is provided to you, modification and redistribution of the
 * source code are permitted provided that the following conditions are met:
 *
 * *       any redistribution and use of the source code, including any resulting derivative
 * works, are licensed by TI for use only with TI Devices.
 *
 * *       any redistribution and use of any object code compiled from the source code
 * and any resulting derivative works, are licensed by TI for use only with TI Devices.
 *
 * Neither the name of Texas Instruments Incorporated nor the names of its suppliers
 *
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * DISCLAIMER.
 *
 * THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */
#if !defined(_LIDAR_CAPTURE_DRV_H_)
#define _LIDAR_CAPTURE_DRV_H_

#include <stdint.h>

/**
 * \defgroup group_ptk_drv_lidar_capture PTK Lidar capture driver
 * \ingroup group_ptk_drv
 *
 * Provides API for the UDP socket based Lidar driver.
 */

#ifdef __cplusplus
extern "C" {
#endif

#include <perception/perception.h>
#include <perception/drv/drv_common.h>
#include <perception/utils/api_memory_if.h>
#include <perception/sensors/lidar/lidar_velodyne.h>
#include <perception/net/net_utils.h>

/**
 * \brief Lidar data port number.
 * \ingroup group_ptk_drv_lidar_capture
 */
#define LIDAR_CAPTURE_DRV_VELODYNE_DATA_PORT    (2368U)

/**
 * \brief Lidar GPS data port number. Currently unused.
 * \ingroup group_ptk_drv_lidar_capture
 */
#define LIDAR_CAPTURE_DRV_VELODYNE_GPS_PORT     (8308U)

/**
 * \brief Maximum length of a lidar sensor message.
 * \ingroup group_ptk_drv_lidar_capture
 */
#define LIDAR_CAPTURE_DRV_BUFFER_SIZE           (1206U)

/**
 * \brief Value to indicate invalid socket descriptor.
 * \ingroup group_ptk_drv_lidar_capture
 */
#define LIDAR_CAPTURE_DRV_INVALID_SOCK_ID       (-1)

/**
 * \brief Lidar capture driver type.
 *        Currently only network driver supported.
 *
 * \ingroup group_ptk_drv_lidar_capture
 */
typedef enum
{
    /** Network (UDP/TCP) based driver. */
    PTK_Drv_LidarDrvType_NETWORK = 0

} PTK_Drv_LidarDrvType;

/**
 * \brief Lidar capture driver configuration parameters.
 *
 * \ingroup group_ptk_drv_lidar_capture
 */
typedef struct
{
    /** Driver type. */
    PTK_Drv_LidarDrvType    drvType;

} PTK_Drv_LidarDrvParams;

/**
 * \brief Computes the memory requirements for the driver.
 *
 * \param [in] cfgParams Driver configuration parameters.
 *
 * \param [out] memReq Memory requirements as specified below
 *             - entry [0]: Memory for internal context. Best allocated from
 *                          the fastest memory.
 *             - entry [1]: Memory for the output buffer. The output buffer is
 *                          allocated by the Application so this entry is just
 *                          to communicate the output buffer size. No need to
 *                          pass the memory to the library in the init call
 *                          PTK_Drv_LidarCaptureInit().
 *
 * \return
 *        - PTK_DRV_RET_SUCCESS, if successful.
 *        - < error code, otherwise.
 *
 * \ingroup group_ptk_drv_lidar_capture
 */
int32_t PTK_Drv_LidarCaptureConfig(const PTK_Drv_LidarDrvParams   * cfgParams,
                                   PTK_Api_MemoryReq              * memReq);

/**
 * \brief Initializes the driver.
 *
 * \param [in] cfgParams Driver configuration parameters.
 *
 * \param [in] memRsp Memory blocks as specified below
 *             - entry [0]: Memory for internal context.
 *
 * \return
 *        - valid handle, if successful.
 *        - NULL, otherwise.
 *
 * \ingroup group_ptk_drv_lidar_capture
 */
PTK_DrvHandle
PTK_Drv_LidarCaptureInit(const PTK_Drv_LidarDrvParams * cfgParams,
                         const PTK_Api_MemoryRsp      * memRsp);


/**
 * \brief Captures the Lidar scan data into the output buffer. The buffer
 *        contains one fill Velodyne Packet.
 *
 * \param [inout] drvHandle Driver handle.
 *
 * \param [out] frameBuff Output Buffer.
 *
 * \return
 *        - PTK_DRV_RET_SUCCESS, if successful.
 *        - < error code, otherwise.
 *
 * \ingroup group_ptk_drv_lidar_capture
 */
int32_t PTK_Drv_LidarCaptureProcess(PTK_DrvHandle               drvHandle,
                                    PTK_Lidar_VelodyneFrame   * frameBuff);

/**
 * \brief De-initializes the driver context.
 *
 * \param [inout] drvHandle Driver handle.
 *
 * \ingroup group_ptk_drv_lidar_capture
 */
void PTK_Drv_LidarCaptureDeInit(PTK_DrvHandle drvHandle);

#ifdef __cplusplus
}
#endif

#endif // !defined(_LIDAR_CAPTURE_DRV_H_)

