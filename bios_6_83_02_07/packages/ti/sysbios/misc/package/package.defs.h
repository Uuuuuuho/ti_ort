/*
 *  Do not modify this file; it is automatically 
 *  generated and any modifications will be overwritten.
 *
 * @(#) xdc-J06
 */

#ifndef ti_sysbios_misc__
#define ti_sysbios_misc__


/*
 * ======== module ti.sysbios.misc.Stats ========
 */

typedef struct ti_sysbios_misc_Stats_Params ti_sysbios_misc_Stats_Params;
typedef struct ti_sysbios_misc_Stats_Object ti_sysbios_misc_Stats_Object;
typedef struct ti_sysbios_misc_Stats_Struct ti_sysbios_misc_Stats_Struct;
typedef ti_sysbios_misc_Stats_Object* ti_sysbios_misc_Stats_Handle;
typedef ti_sysbios_misc_Stats_Object* ti_sysbios_misc_Stats_Instance;


#endif /* ti_sysbios_misc__ */ 
