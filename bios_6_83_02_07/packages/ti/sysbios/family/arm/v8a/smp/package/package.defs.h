/*
 *  Do not modify this file; it is automatically 
 *  generated and any modifications will be overwritten.
 *
 * @(#) xdc-J06
 */

#ifndef ti_sysbios_family_arm_v8a_smp__
#define ti_sysbios_family_arm_v8a_smp__


/*
 * ======== module ti.sysbios.family.arm.v8a.smp.Core ========
 */

typedef struct ti_sysbios_family_arm_v8a_smp_Core_IpcMsg ti_sysbios_family_arm_v8a_smp_Core_IpcMsg;
typedef struct ti_sysbios_family_arm_v8a_smp_Core_Module_State ti_sysbios_family_arm_v8a_smp_Core_Module_State;
typedef struct ti_sysbios_family_arm_v8a_smp_Core_Fxns__ ti_sysbios_family_arm_v8a_smp_Core_Fxns__;
typedef const struct ti_sysbios_family_arm_v8a_smp_Core_Fxns__* ti_sysbios_family_arm_v8a_smp_Core_Module;

/*
 * ======== module ti.sysbios.family.arm.v8a.smp.GateSmp ========
 */

typedef struct ti_sysbios_family_arm_v8a_smp_GateSmp_Fxns__ ti_sysbios_family_arm_v8a_smp_GateSmp_Fxns__;
typedef const struct ti_sysbios_family_arm_v8a_smp_GateSmp_Fxns__* ti_sysbios_family_arm_v8a_smp_GateSmp_Module;
typedef struct ti_sysbios_family_arm_v8a_smp_GateSmp_Params ti_sysbios_family_arm_v8a_smp_GateSmp_Params;
typedef struct ti_sysbios_family_arm_v8a_smp_GateSmp_Object ti_sysbios_family_arm_v8a_smp_GateSmp_Object;
typedef struct ti_sysbios_family_arm_v8a_smp_GateSmp_Struct ti_sysbios_family_arm_v8a_smp_GateSmp_Struct;
typedef ti_sysbios_family_arm_v8a_smp_GateSmp_Object* ti_sysbios_family_arm_v8a_smp_GateSmp_Handle;
typedef ti_sysbios_family_arm_v8a_smp_GateSmp_Object* ti_sysbios_family_arm_v8a_smp_GateSmp_Instance;


#endif /* ti_sysbios_family_arm_v8a_smp__ */ 
