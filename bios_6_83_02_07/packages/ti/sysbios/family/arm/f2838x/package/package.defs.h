/*
 *  Do not modify this file; it is automatically 
 *  generated and any modifications will be overwritten.
 *
 * @(#) xdc-J06
 */

#ifndef ti_sysbios_family_arm_f2838x__
#define ti_sysbios_family_arm_f2838x__


/*
 * ======== module ti.sysbios.family.arm.f2838x.TimestampProvider ========
 */

typedef struct ti_sysbios_family_arm_f2838x_TimestampProvider_Fxns__ ti_sysbios_family_arm_f2838x_TimestampProvider_Fxns__;
typedef const struct ti_sysbios_family_arm_f2838x_TimestampProvider_Fxns__* ti_sysbios_family_arm_f2838x_TimestampProvider_Module;

/*
 * ======== module ti.sysbios.family.arm.f2838x.Timer ========
 */

typedef struct ti_sysbios_family_arm_f2838x_Timer_EmulationMode ti_sysbios_family_arm_f2838x_Timer_EmulationMode;
typedef struct ti_sysbios_family_arm_f2838x_Timer_Module_State ti_sysbios_family_arm_f2838x_Timer_Module_State;
typedef struct ti_sysbios_family_arm_f2838x_Timer_Fxns__ ti_sysbios_family_arm_f2838x_Timer_Fxns__;
typedef const struct ti_sysbios_family_arm_f2838x_Timer_Fxns__* ti_sysbios_family_arm_f2838x_Timer_Module;
typedef struct ti_sysbios_family_arm_f2838x_Timer_Params ti_sysbios_family_arm_f2838x_Timer_Params;
typedef struct ti_sysbios_family_arm_f2838x_Timer_Object ti_sysbios_family_arm_f2838x_Timer_Object;
typedef struct ti_sysbios_family_arm_f2838x_Timer_Struct ti_sysbios_family_arm_f2838x_Timer_Struct;
typedef ti_sysbios_family_arm_f2838x_Timer_Object* ti_sysbios_family_arm_f2838x_Timer_Handle;
typedef ti_sysbios_family_arm_f2838x_Timer_Object* ti_sysbios_family_arm_f2838x_Timer_Instance;


#endif /* ti_sysbios_family_arm_f2838x__ */ 
