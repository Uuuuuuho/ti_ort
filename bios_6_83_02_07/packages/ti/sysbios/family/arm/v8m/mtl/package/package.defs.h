/*
 *  Do not modify this file; it is automatically 
 *  generated and any modifications will be overwritten.
 *
 * @(#) xdc-J06
 */

#ifndef ti_sysbios_family_arm_v8m_mtl__
#define ti_sysbios_family_arm_v8m_mtl__


/*
 * ======== module ti.sysbios.family.arm.v8m.mtl.Core ========
 */

typedef struct ti_sysbios_family_arm_v8m_mtl_Core_Fxns__ ti_sysbios_family_arm_v8m_mtl_Core_Fxns__;
typedef const struct ti_sysbios_family_arm_v8m_mtl_Core_Fxns__* ti_sysbios_family_arm_v8m_mtl_Core_Module;

/*
 * ======== module ti.sysbios.family.arm.v8m.mtl.EvtMan ========
 */


/*
 * ======== module ti.sysbios.family.arm.v8m.mtl.Timer ========
 */

typedef struct ti_sysbios_family_arm_v8m_mtl_Timer_TimerDevice ti_sysbios_family_arm_v8m_mtl_Timer_TimerDevice;
typedef struct ti_sysbios_family_arm_v8m_mtl_Timer_Module_State ti_sysbios_family_arm_v8m_mtl_Timer_Module_State;
typedef struct ti_sysbios_family_arm_v8m_mtl_Timer_Fxns__ ti_sysbios_family_arm_v8m_mtl_Timer_Fxns__;
typedef const struct ti_sysbios_family_arm_v8m_mtl_Timer_Fxns__* ti_sysbios_family_arm_v8m_mtl_Timer_Module;
typedef struct ti_sysbios_family_arm_v8m_mtl_Timer_Params ti_sysbios_family_arm_v8m_mtl_Timer_Params;
typedef struct ti_sysbios_family_arm_v8m_mtl_Timer_Object ti_sysbios_family_arm_v8m_mtl_Timer_Object;
typedef struct ti_sysbios_family_arm_v8m_mtl_Timer_Struct ti_sysbios_family_arm_v8m_mtl_Timer_Struct;
typedef ti_sysbios_family_arm_v8m_mtl_Timer_Object* ti_sysbios_family_arm_v8m_mtl_Timer_Handle;
typedef ti_sysbios_family_arm_v8m_mtl_Timer_Object* ti_sysbios_family_arm_v8m_mtl_Timer_Instance;

/*
 * ======== module ti.sysbios.family.arm.v8m.mtl.TimestampProvider ========
 */

typedef struct ti_sysbios_family_arm_v8m_mtl_TimestampProvider_Fxns__ ti_sysbios_family_arm_v8m_mtl_TimestampProvider_Fxns__;
typedef const struct ti_sysbios_family_arm_v8m_mtl_TimestampProvider_Fxns__* ti_sysbios_family_arm_v8m_mtl_TimestampProvider_Module;


#endif /* ti_sysbios_family_arm_v8m_mtl__ */ 
