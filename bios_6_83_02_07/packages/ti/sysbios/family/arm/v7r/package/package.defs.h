/*
 *  Do not modify this file; it is automatically 
 *  generated and any modifications will be overwritten.
 *
 * @(#) xdc-J06
 */

#ifndef ti_sysbios_family_arm_v7r__
#define ti_sysbios_family_arm_v7r__


/*
 * ======== module ti.sysbios.family.arm.v7r.Cache ========
 */

typedef struct ti_sysbios_family_arm_v7r_Cache_Module_State ti_sysbios_family_arm_v7r_Cache_Module_State;
typedef struct ti_sysbios_family_arm_v7r_Cache_Fxns__ ti_sysbios_family_arm_v7r_Cache_Fxns__;
typedef const struct ti_sysbios_family_arm_v7r_Cache_Fxns__* ti_sysbios_family_arm_v7r_Cache_Module;

/*
 * ======== module ti.sysbios.family.arm.v7r.MemProtect ========
 */


/*
 * ======== module ti.sysbios.family.arm.v7r.SysCall ========
 */

typedef struct ti_sysbios_family_arm_v7r_SysCall_Module_State ti_sysbios_family_arm_v7r_SysCall_Module_State;
typedef struct ti_sysbios_family_arm_v7r_SysCall_Fxns__ ti_sysbios_family_arm_v7r_SysCall_Fxns__;
typedef const struct ti_sysbios_family_arm_v7r_SysCall_Fxns__* ti_sysbios_family_arm_v7r_SysCall_Module;


#endif /* ti_sysbios_family_arm_v7r__ */ 
