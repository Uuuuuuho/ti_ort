/*
 * Copyright (c) 2014-2020 Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
/*
 *  ======== Exception.c ========
 */

#include <xdc/std.h>

#include <xdc/runtime/Error.h>
#include <xdc/runtime/Startup.h>
#include <xdc/runtime/System.h>

#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/family/c7x/Hwi.h>

#include "package/internal/Exception.xdc.h"
#pragma FUNC_EXT_CALLED(Exception_handler);

#include <c7x.h>


/*
 *  ======== Exception_Module_startup ========
 *  Exception_Module_startup must be called to enable events to cause
 *  an exception.
 */
Int Exception_Module_startup(Int phase)
{
    /*
     *  Exception uses Hwi, needs to wait for its startup.
     */
    if (!Hwi_Module_startupDone()) {
        return (Startup_NOTDONE);
    }

    if (Exception_useInternalBuffer) {
        Exception_module->excPtr = (Char *)
            (((UInt64)Exception_module->contextBuf +
                Exception_sizeContextBuf - 8) & ~0x7);
    }
    else {
        Exception_module->excPtr = ti_sysbios_family_c7x_Hwi_getIsrStackAddress();
    }

    return Startup_DONE;
}

#pragma FUNC_CANNOT_INLINE(printTwoVectors)
static Void printTwoVectors(String name1, Ptr vectorReg1[],
                            String name2, Ptr vectorReg2[])
{
    Int i;

    System_printf("\n%s=0x%016lx [0]    %s=0x%016lx [0]\n    ",
                  name1, vectorReg1[0], name2, vectorReg2[0]);
    for (i = 1; i < 8; i++) {
        System_printf("0x%016lx [%d]        0x%016lx [%d]\n    ",
                      vectorReg1[i], i, vectorReg2[i], i);
    }
}

/*
 *  ======== Exception_handler ========
 *  This handler is called by the dispatch function.  It calls the various
 *  exception hook functions.
 */
Void Exception_handler(Bool abortFlag, Int vectorType)
{
    UInt64 *contextStack;
    UInt64 nrp;
    UInt64 savedTSR;
    Exception_Context *excp;
    UInt ncnt;

    /* set exception context */
    excp = Exception_module->excContext;

    if (Exception_enablePrint) {
        /* Force MAIN threadtype So we can safely call System_printf */
        BIOS_setThreadType(BIOS_ThreadType_Main);

        System_printf("A0 =0x%016lx A1 =0x%016lx\n", excp->A0, excp->A1);
        System_printf("A2 =0x%016lx A3 =0x%016lx\n", excp->A2, excp->A3);
        System_printf("A4 =0x%016lx A5 =0x%016lx\n", excp->A4, excp->A5);
        System_printf("A6 =0x%016lx A7 =0x%016lx\n", excp->A6, excp->A7);
        System_printf("A8 =0x%016lx A9 =0x%016lx\n", excp->A8, excp->A9);
        System_printf("A10=0x%016lx A11=0x%016lx\n", excp->A10, excp->A11);
        System_printf("A12=0x%016lx A13=0x%016lx\n", excp->A12, excp->A13);
        System_printf("A14=0x%016lx A15=0x%016lx\n", excp->A14, excp->A15);
        System_printf("D0 =0x%016lx D1 =0x%016lx\n", excp->D0, excp->D1);
        System_printf("D2 =0x%016lx D3 =0x%016lx\n", excp->D2, excp->D3);
        System_printf("D4 =0x%016lx D5 =0x%016lx\n", excp->D4, excp->D5);
        System_printf("D6 =0x%016lx D7 =0x%016lx\n", excp->D6, excp->D7);
        System_printf("D8 =0x%016lx D9 =0x%016lx\n", excp->D8, excp->D9);
        System_printf("D10=0x%016lx D11=0x%016lx\n", excp->D10, excp->D11);
        System_printf("D12=0x%016lx D13=0x%016lx\n", excp->D12, excp->D13);
        System_printf("D14=0x%016lx D15=0x%016lx\n", excp->D14, excp->D15);
        System_printf("AM0=0x%016lx AM1=0x%016lx\n", excp->AM0, excp->AM1);
        System_printf("AM2=0x%016lx AM3=0x%016lx\n", excp->AM2, excp->AM3);
        System_printf("AM4=0x%016lx AM5=0x%016lx\n", excp->AM4, excp->AM5);
        System_printf("AM6=0x%016lx AM7=0x%016lx\n", excp->AM6, excp->AM7);
        System_printf("AL0=0x%016lx AL1=0x%016lx\n", excp->AL0, excp->AL1);
        System_printf("AL2=0x%016lx AL3=0x%016lx\n", excp->AL2, excp->AL3);
        System_printf("AL4=0x%016lx AL5=0x%016lx\n", excp->AL4, excp->AL5);
        System_printf("AL6=0x%016lx AL7=0x%016lx\n", excp->AL6, excp->AL7);
        System_printf("P0=0x%016lx P1=0x%016lx\n", excp->P0, excp->P1);
        System_printf("P2=0x%016lx P3=0x%016lx\n", excp->P2, excp->P3);
        System_printf("P4=0x%016lx P5=0x%016lx\n", excp->P4, excp->P5);
        System_printf("P6=0x%016lx P7=0x%016lx\n", excp->P6, excp->P7);
        System_printf("FPCR=0x%016lx FSR=0x%016lx\n", excp->FPCR, excp->FSR);
        System_printf("GFPGFR=0x%016lx GPLY=0x%016lx\n",
                      excp->GFPGFR, excp->GPLY);
        printTwoVectors("VBM0", excp->VBM0, "VBM1", excp->VBM1);
        printTwoVectors("VBM2", excp->VBM2, "VBM3", excp->VBM3);
        printTwoVectors("VBM4", excp->VBM4, "VBM5", excp->VBM5);
        printTwoVectors("VBM6", excp->VBM6, "VBM7", excp->VBM7);
        printTwoVectors("VBL0", excp->VBL0, "VBL1", excp->VBL1);
        printTwoVectors("VBL2", excp->VBL2, "VBL3", excp->VBL3);
        printTwoVectors("VBL4", excp->VBL4, "VBL5", excp->VBL5);
        printTwoVectors("VBL6", excp->VBL6, "VBL7", excp->VBL7);
        printTwoVectors("VB0", excp->VB0, "VB1", excp->VB1);
        printTwoVectors("VB2", excp->VB2, "VB3", excp->VB3);
        printTwoVectors("VB4", excp->VB4, "VB5", excp->VB5);
        printTwoVectors("VB6", excp->VB6, "VB7", excp->VB7);
        printTwoVectors("VB8", excp->VB8, "VB9", excp->VB9);
        printTwoVectors("VB10", excp->VB10, "VB11", excp->VB11);
        printTwoVectors("VB12", excp->VB12, "VB13", excp->VB13);
        printTwoVectors("VB14", excp->VB14, "VB15", excp->VB15);
        printTwoVectors("CUCR0", excp->CUCR0, "CUCR1", excp->CUCR1);
        printTwoVectors("CUCR2", excp->CUCR2, "CUCR3", excp->CUCR3);
        printTwoVectors("SE0_0", excp->SE0_0, "SE0_1", excp->SE0_1);
        printTwoVectors("SE0_2", excp->SE0_2, "SE0_3", excp->SE0_3);
        printTwoVectors("SE1_0", excp->SE1_0, "SE1_1", excp->SE1_1);
        printTwoVectors("SE1_2", excp->SE1_2, "SE1_3", excp->SE1_3);
        printTwoVectors("SA0CR", excp->SA0CR, "SA1CR", excp->SA1CR);
        printTwoVectors("SA2CR", excp->SA2CR, "SA3CR", excp->SA3CR);
        printTwoVectors("SA0CNTR0", excp->SA0CNTR0, "SA1CNTR0", excp->SA1CNTR0);
        printTwoVectors("SA2CNTR0", excp->SA2CNTR0, "SA3CNTR0", excp->SA3CNTR0);

        System_printf("\n");
    }

    ncnt = (__ECSP_S & 0xe000) >> 13;
    if (ncnt) {
        contextStack = (UInt64 *)(((UInt64)__ECSP_S) + ((ncnt - 1) * 0x2000));
    }
    else  {
        contextStack = (UInt64 *)__TCSP;
    }

    nrp = contextStack[0];
    savedTSR = contextStack[1];

    Exception_module->nrp = nrp;
    Exception_module->ntsr = savedTSR;

    /* print general exception info */
    if (Exception_enablePrint) {
        System_printf("Exception at 0x%016lx\n", (IArg)Exception_module->nrp);
        System_printf("TSR at time of exception: 0x%016lx\n",
                      (IArg)Exception_module->ntsr);
    }

    if (*Exception_exceptionHook != NULL) {
        (*Exception_exceptionHook)();
    }

    /* process all possible causes of exception */
    if (vectorType == 0) {
        /* internal exception */
        System_printf("Internal exception:\n");
        Exception_internalHandler();
    }
    if (vectorType == 1) {
        /* page fault */
        System_printf("Page fault:\n");
        Exception_internalHandler();
    }

    if (abortFlag) {
        if (Exception_enablePrint) {
            Error_raise(0, Exception_E_exceptionMax, nrp, excp->D15);
        }
        else {
            Error_raise(0, Exception_E_exceptionMin, nrp, excp->D15);
        }
    }
}

/*
 *  ======== Exception_internalHandler ========
 *  This function is called only if an internal exception has occurred.
 *  It checks the EFR register to determine what type of exceptions occurred.
 */
Void Exception_internalHandler(Void)
{
    UInt ierr;
    UInt iesr19_16;
    UInt iesr15_0;

    /* record IERR in Exc_module field */
    Exception_module->ierr = __IERR;
    Exception_module->iear = __IEAR;
    Exception_module->iesr = __IESR;

    if (Exception_enablePrint) {
        System_printf("  IERR=0x%016lx\n"
                      "  IEAR=0x%016lx\n"
                      "  IESR=0x%016lx\n",
                      Exception_module->ierr,
                      Exception_module->iear,
                      Exception_module->iesr);

        iesr19_16 = (Exception_module->iesr & 0xf0000) >> 16;
        iesr15_0 = Exception_module->iesr & 0xffff;
        ierr = Exception_module->ierr;

        if (ierr & Exception_IERRPFX) {
            System_printf("  Page fault exception:\n");
            switch (iesr19_16) {
              case 0:
                System_printf("    uTLB Fault, cpu_pmc_rstatus[10:0]=0x%x\n",
                              iesr15_0 & 0x7ff);
                break;
              case 1:
                System_printf("    .D1 or .D2 uTLB lookup fault, Non-speculative load\n");
                System_printf("    utlb_rstatus=0x%x\n", iesr15_0);
                break;
              case 2:
                System_printf("    .D1 or .D2 uTLB lookup fault, Speculative load\n");
                System_printf("    utlb_rstatus=0x%x\n", iesr15_0);
                break;
              case 3:
                System_printf("    uTLB Fault, cpu_se_N_rstatus[10:0]=0x%x\n",
                              iesr15_0 & 0x7ff);
                break;
              default:
                System_printf("    unknown sub-type, iesr[19:16]=0x%x\n",
                              iesr19_16);
                break;
            }
        }
        if (ierr & Exception_IERRIFX) {
            System_printf("  Instruction fetch exception, cpu_pmc_rstatus[2:0]=0x%x\n",
                          iesr15_0 & 0x7);
        }
        if (ierr & Exception_IERRFPX) {
            System_printf("  Fetch packet exception\n");
        }
        if (ierr & Exception_IERREPX) {
            System_printf("  Execute packet exception\n");
        }
        if (ierr & Exception_IERROPX) {
            System_printf("  Illegal opcode exception\n");
        }
        if (ierr & Exception_IERRRCX) {
            System_printf("  Resource conflict exception\n");
        }
        if (ierr & Exception_IERRRAX) {
            System_printf("  Resource access exception\n");
            switch (iesr19_16) {
              case 0:
                System_printf("    *SE access when stream is not open\n");
                break;
              case 1:
                System_printf("    SESAVE or SERSTR while stream is open\n");
                break;
              case 2:
                System_printf("    LUT ,HIST when the access bit = 0\n");
                System_printf("    Histogram Base CR #: %d\n", iesr15_0);
                break;
              case 3:
                System_printf("    ECR Non-zero Rstatus except for rstatus == 2 (permission violation)\n");
                System_printf("    cpu_to_ecr_status=0x%x\n", iesr15_0);
                break;
              case 4:
                System_printf("    DSWBP when SWBP en == 0\n");
                break;
              default:
                System_printf("    unknown sub-type, iesr[19:16]=0x%x\n",
                              iesr19_16);
                break;
            }
        }
        if (ierr & Exception_IERRPRX) {
            System_printf("  Privilege exception\n");
            switch (iesr19_16) {
              case 0:
                System_printf("    Internal MVC permission violation\n");
                break;
              case 1:
                System_printf("    External MVC permission violation\n");
                System_printf("    cpu_to_ecr_status=0x%x\n", iesr15_0);
                break;
              case 2:
                System_printf("    Invalid Permissions for given opcode\n");
                break;
              case 3:
                System_printf("    Illegal RETE/RETS execution\n");
                break;
              case 4:
                System_printf("    ECSP addressing mode attempted as a user\n");
                break;
              default:
                System_printf("    unknown sub-type, iesr[19:16]=0x%x\n",
                              iesr19_16);
                break;
            }
        }
        if (ierr & Exception_IERRLBX) {
            System_printf("  Loop buffer exception\n");
        }
        if (ierr & Exception_IERRMSX) {
            System_printf("  Missed stall exception\n");
        }
        if (ierr & Exception_IERRDFX) {
            System_printf("  Data fetch exception\n");
	    System_printf("  cpu_dmc_X_rstatus[2:0]=0x%x\n", iesr15_0 & 0x7);
        }
        if (ierr & Exception_IERRSEX) {
            System_printf("  Streaming engine exception\n");
            switch (iesr19_16) {
              case 0:
                System_printf("    L2 Return Error, cpu_se_N_rstatus[2:0]=0x%x\n",
                              iesr15_0 & 0x7);
                break;
              case 1:
                System_printf("    SE Internal Error, cpu_se_N_rstatus[2:0]=0x%x\n",
                              iesr15_0 & 0x7);
                break;
              default:
                System_printf("    unknown sub-type, iesr[19:16]=0x%x\n",
                              iesr19_16);
                break;
            }
        }
        if (ierr & Exception_IERREXX) {
            System_printf("  Execution exception\n");
            switch (iesr19_16) {
              case 0:
                System_printf("    Executing PROT-mode only instructions in UNPROT mode\n");
                break;
              case 1:
                System_printf("    Division by zero\n");
                break;
              default:
                System_printf("    unknown sub-type, iesr[19:16]=0x%x\n",
                              iesr19_16);
                break;
            }
        }
        if (ierr & Exception_IERRADX) {
            System_printf("  Address exception\n");
            switch (iesr19_16) {
              case 0:
                System_printf("    Out-of-range address on DMC memory address\n");
                break;
              case 1:
                System_printf("    Out-of-range address on CR memory address\n");
                break;
              default:
                System_printf("    unknown sub-type, iesr[19:16]=0x%x\n",
                              iesr19_16);
                break;
            }
        }
        if (ierr & Exception_IERRMMX) {
            System_printf("  MMA exception\n");
            switch (iesr19_16) {
              case 0:
                System_printf("    MMA instruction when MMA isn't present\n");
                break;
              case 1:
                System_printf("    MMA instruction when MMA isn't active\n");
                break;
              case 2:
                System_printf("    MMA status error\n");
	        System_printf("    mma_rstatus=0x%x\n", iesr15_0);
                break;
              default:
                System_printf("    unknown sub-type, iesr[19:16]=0x%x\n",
                              iesr19_16);
                break;
            }
        }
    }

    if (*Exception_internalHook != NULL) {
        (*Exception_internalHook)();
    }

    /* clear internal exceptions to allow them to be recognized again */
    __IERR = 0;
}

/*
 *  ======== Exception_getLastStatus ========
 *  This function returns the last exception status through the status ptr.
 */
Void Exception_getLastStatus(Exception_Status *status)
{
    status->nrp = Exception_module->nrp;
    status->ntsr = Exception_module->ntsr;
    status->ierr = Exception_module->ierr;
    status->iear = Exception_module->iear;
    status->iesr = Exception_module->iesr;
    status->excContext = Exception_module->excContext;
}

/*
 *  ======== Exception_clearLastStatus ========
 *  This function clears the last exception status.
 */
Void Exception_clearLastStatus(Void)
{
    Exception_module->nrp = 0;
    Exception_module->ntsr = 0;
    Exception_module->ierr = 0;
    Exception_module->iear = 0;
    Exception_module->iesr = 0;
    Exception_module->excContext = 0;
}

/*
 *  ======== Exception_setReturnPtr ========
 *  This function sets the new return pointer and returns the old pointer.
 */
Exception_FuncPtr Exception_setReturnPtr(Exception_FuncPtr ptr)
{
    Exception_FuncPtr curPtr;

    curPtr = Exception_module->returnHook;
    Exception_module->returnHook = ptr;

    return curPtr;
}
