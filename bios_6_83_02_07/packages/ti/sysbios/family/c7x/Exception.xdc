/*
 * Copyright (c) 2015-2020, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
/*
 *  ======== Exception.xdc ========
 *
 */

package ti.sysbios.family.c7x;

import xdc.runtime.Error;

/*!
 *  ======== Exception ========
 *  Exception Module
 *
 *  The Exception module is a basic C7x exception handler.  It is generally
 *  considered to be a program endpoint, since an exception usually
 *  indicates something fatal to the system.
 *
 *  Function hooks are provided to the user for hooking in their own functions
 *  at different points of an exception. The hook functions are called in the
 *  following order:
 *  (1) exceptionHook - called whenever an exception occurs.
 *  (2) internalHook - called only when an internal exception or page fault
 *      occurs.
 *  (5) returnHook - called whenever an exception occurs.
 *
 *  @p(html)
 *  <h3> Calling Context </h3>
 *  <table border="1" cellpadding="3">
 *    <colgroup span="1"></colgroup> <colgroup span="5" align="center"></colgroup>
 *
 *    <tr><th> Function                 </th><th>  Hwi   </th><th>  Swi   </th><th>  Task  </th><th>  Main  </th><th>  Startup  </th></tr>
 *    <!--                                                                                                                 -->
 *    <tr><td> {@link #clearLastStatus}         </td><td>   Y    </td><td>   Y    </td><td>   Y    </td><td>   Y    </td><td>   Y    </td></tr>
 *    <tr><td> {@link #getLastStatus}           </td><td>   Y    </td><td>   Y    </td><td>   Y    </td><td>   Y    </td><td>   Y    </td></tr>
 *    <tr><td> {@link #setReturnPtr}            </td><td>   Y    </td><td>   Y    </td><td>   Y    </td><td>   Y    </td><td>   Y    </td></tr>
 *    <tr><td colspan="6"> Definitions: <br />
 *       <ul>
 *         <li> <b>Hwi</b>: API is callable from a Hwi thread. </li>
 *         <li> <b>Swi</b>: API is callable from a Swi thread. </li>
 *         <li> <b>Task</b>: API is callable from a Task thread. </li>
 *         <li> <b>Main</b>: API is callable during any of these phases: </li>
 *           <ul>
 *             <li> In your module startup after this module is started (e.g. Mod_Module_startupDone() returns TRUE). </li>
 *             <li> During xdc.runtime.Startup.lastFxns. </li>
 *             <li> During main().</li>
 *             <li> During BIOS.startupFxns.</li>
 *           </ul>
 *         <li> <b>Startup</b>: API is callable during any of these phases:</li>
 *           <ul>
 *             <li> During xdc.runtime.Startup.firstFxns.</li>
 *             <li> In your module startup before this module is started (e.g. Mod_Module_startupDone() returns FALSE).</li>
 *           </ul>
 *       </ul>
 *    </td></tr>
 *
 *  </table>
 *  @p
 */

@ModuleStartup         /* generate a call to Exception at startup */
@DirectCall
module Exception
{
    // -------- Module Types --------

    /*! FuncPtr - Hook function type definition. */
    typedef Void (*FuncPtr)(void);

    /*! @_nodoc
     *  Context - Register contents at the time of the exception.
     *  dispatch() creates a Context structure on the Hwi ISR stack or
     *  context buffer and fills it before calling handler.  A pointer
     *  to this Context structure is returned by getLastStatus().
     */
    struct Context {
        Ptr IERR;
        Ptr IEAR;
        Ptr IESR;
        Ptr RP;
        Ptr FPCR;
        Ptr FSR;
        Ptr GFPGFR;
        Ptr GPLY;
        Ptr P7;
        Ptr P6;
        Ptr P5;
        Ptr P4;
        Ptr P3;
        Ptr P2;
        Ptr P1;
        Ptr P0;
        Ptr SE0_0[8];
        Ptr SE0_1[8];
        Ptr SE0_2[8];
        Ptr SE0_3[8];
        Ptr SE1_0[8];
        Ptr SE1_1[8];
        Ptr SE1_2[8];
        Ptr SE1_3[8];
        Ptr SA3CR[8];
        Ptr SA2CR[8];
        Ptr SA1CR[8];
        Ptr SA0CR[8];
        Ptr SA3CNTR0[8];
        Ptr SA2CNTR0[8];
        Ptr SA1CNTR0[8];
        Ptr SA0CNTR0[8];
        Ptr CUCR3[8];
        Ptr CUCR2[8];
        Ptr CUCR1[8];
        Ptr CUCR0[8];
        Ptr VBL7[8];
        Ptr VBL6[8];
        Ptr VBL5[8];
        Ptr VBL4[8];
        Ptr VBL3[8];
        Ptr VBL2[8];
        Ptr VBL1[8];
        Ptr VBL0[8];
        Ptr VBM7[8];
        Ptr VBM6[8];
        Ptr VBM5[8];
        Ptr VBM4[8];
        Ptr VBM3[8];
        Ptr VBM2[8];
        Ptr VBM1[8];
        Ptr VBM0[8];
        Ptr VB15[8];
        Ptr VB14[8];
        Ptr VB13[8];
        Ptr VB12[8];
        Ptr VB11[8];
        Ptr VB10[8];
        Ptr VB9[8];
        Ptr VB8[8];
        Ptr VB7[8];
        Ptr VB6[8];
        Ptr VB5[8];
        Ptr VB4[8];
        Ptr VB3[8];
        Ptr VB2[8];
        Ptr VB1[8];
        Ptr VB0[8];
        Ptr AL7;
        Ptr AL6;
        Ptr AL5;
        Ptr AL4;
        Ptr AL3;
        Ptr AL2;
        Ptr AL1;
        Ptr AL0;
        Ptr AM7;
        Ptr AM6;
        Ptr AM5;
        Ptr AM4;
        Ptr AM3;
        Ptr AM2;
        Ptr AM1;
        Ptr AM0;
        Ptr D15;
        Ptr D14;
        Ptr D13;
        Ptr D12;
        Ptr D11;
        Ptr D10;
        Ptr D9;
        Ptr D8;
        Ptr D7;
        Ptr D6;
        Ptr D5;
        Ptr D4;
        Ptr D3;
        Ptr D2;
        Ptr D1;
        Ptr D0;
        Ptr A15;
        Ptr A14;
        Ptr A13;
        Ptr A12;
        Ptr A11;
        Ptr A10;
        Ptr A9;
        Ptr A8;
        Ptr A7;
        Ptr A6;
        Ptr A5;
        Ptr A4;
        Ptr A3;
        Ptr A2;
        Ptr A1;
        Ptr A0;
    };

    /*! Status - structure filled by getLastStatus(). */
    struct Status {
        Bits64  nrp;            //! NMI return pointer register
        Bits64  ntsr;           //! NMI/Exception task state register
        Bits64  ierr;           //! Internal Exception report register
        Bits64  iear;           //! Internal Exception report register
        Bits64  iesr;           //! Internal Exception report register
        Context *excContext;//! Context structure filled by last exception
    };

    // -------- Module Constants --------

    /*
     *  Bitmasks for C7x Internal Exception Report Register (IERR).
     */
    const Bits32 IERRPFX = 0x00000001;  //! Page fault exception
    const Bits32 IERRIFX = 0x00000002;  //! Instruction fetch exception
    const Bits32 IERRFPX = 0x00000004;  //! Fetch packet exception
    const Bits32 IERREPX = 0x00000008;  //! Execute packet exception
    const Bits32 IERROPX = 0x00000010;  //! Illegal opcode exception
    const Bits32 IERRRCX = 0x00000020;  //! Resource conflict exception
    const Bits32 IERRRAX = 0x00000040;  //! Resource access exeption
    const Bits32 IERRPRX = 0x00000080;  //! Priviledge exception
    const Bits32 IERRLBX = 0x00000100;  //! Loop buffer exception
    const Bits32 IERRMSX = 0x00000200;  //! Missed stall exception
    const Bits32 IERRDFX = 0x00000400;  //! Data fetch exception
    const Bits32 IERRSEX = 0x00000800;  //! Streaming engine exception
    const Bits32 IERREXX = 0x00001000;  //! Execution exception
    const Bits32 IERRADX = 0x00002000;  //! Address exception
    const Bits32 IERRMMX = 0x00004000;  //! MMA exception

    /* size of buffer (in bytes) to hold exception context */
    const UInt32 sizeContextBuf = 0x1000;

    /*! Error raised when {@link #enablePrint Exception.enablePrint} is false */
    config Error.Id E_exceptionMin = {
        msg: "E_exceptionMin: pc = 0x%08x, sp = 0x%08x.\nTo see more exception detail, use ROV or set 'ti.sysbios.family.c64p.Exception.enablePrint = true;'"
    };

    /*! Error raised when {@link #enablePrint Exception.enablePrint} is true */
    config Error.Id E_exceptionMax = {
        msg: "E_exceptionMax: pc = 0x%08x, sp = 0x%08x."
    };

    // -------- Module Parameters --------

    /*!
     *  If true, the exception context is saved to an internal buffer.
     *  If false, the exception context is saved to the bottom of the isr stack
     *  and no memory for the internal buffer is allocated.
     */
    config Bool useInternalBuffer = false;

    /*!
     *  enablePrint - Enable print of exception details and Register values
     */
    config Bool enablePrint = true;

    /*!
     *  exceptionHook - Function hook called by handler
     *  This is called anytime an exception occurs.
     */
    config FuncPtr exceptionHook = null;

    /*!
     *  internalHook - Function hook called by internalHandler
     *  Function is only called when an internal exception has occurred.
     */
    config FuncPtr internalHook = null;

    /*! returnHook - Function hook called at the end of Exception_dispatch */
    config FuncPtr returnHook = null;

    /*!
     *  getLastStatus - Fills passed status structure with the Status
     *  fields that were recorded by the last invocation of
     *  dispatch(), handler() and internalHandler().
     *  The 'excContext' is valid only in the scope of sub-handler
     *  "Hook" functions.
     */
    Void getLastStatus(Status *status);

    /*!
     *  clearLastStatus - Clears internal Status structure.
     */
    Void clearLastStatus();

    /*!
     *  setReturnPtr - Configures dispatch() to "return" (branch) to the
     *  passed ptr.
     */
    FuncPtr setReturnPtr(FuncPtr ptr);

    /*! @_nodoc
     *  dispatch - The default low-level dispatcher, plugged into the
     *  C7x internal exception vector and page fault vector.
     */
    Void dispatch();

internal:

    /*!
     *  handler - The high-level dispatcher, called by dispatch().
     *  Performs the following steps in order:
     *    a. records EFR/NRP/NTSR in a Status structure
     *    b. logs EFR/NRP/NTSR.CXM with System_printf()
     *    c. calls exceptionHook
     *    d. clears EFR
     *    e. calls into subhandlers
     *    f. aborts system
     */
    Void handler(Bool abortFlag, Int vectorType);

    /*!
     *  internalHandler - Internal exception handler called by
     *  handler().  Performs the following steps in order:
     *    a. records IERR in a Status structure
     *    b. logs IERR with System_printf()
     *    c. calls internalHook
     *    d. clears IERR
     */
    Void internalHandler();

    struct Module_State {
        Bits64  nrp;
        Bits64  ntsr;
        Bits64  ierr;
        Bits64  iear;
        Bits64  iesr;
        FuncPtr returnHook;
        Context *excContext;
        Char    *excPtr;        // points to isrStack or contextBuf
        Char    contextBuf[];   // Need at least 74 words for context buf
    };
}
