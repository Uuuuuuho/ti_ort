/*
 * Copyright (c) 2015-2020, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 *  ======== Exception.xs ========
 */

/*
 * ======== getAsmFiles ========
 * return the array of assembly language files associated
 * with targetName (ie Program.build.target.$name)
 */
function getAsmFiles(targetName)
{
    switch(targetName) {
        case "ti.targets.elf.C71":
            return (["Exception_asm.s71"]);
            break;

        default:
            return (null);
            break;
    }
}

var Hwi = null;
var Exception = null;
var BIOS = null;
var Build = null;

/*
 *  ======== module$meta$init ========
 */
function module$meta$init()
{
    /* Only process during "cfg" phase */
    if (xdc.om.$name != "cfg") {
        return;
    }

    /* provide getAsmFiles() for Build.getAsmFiles() */
    this.$private.getAsmFiles = getAsmFiles;
}

/*
 *  ======== module$use ========
 */
function module$use()
{
    Hwi = xdc.useModule("ti.sysbios.family.c7x.Hwi");
    var Diags = xdc.module("xdc.runtime.Diags");
    BIOS = xdc.module("ti.sysbios.BIOS");
    Build = xdc.module("ti.sysbios.Build");

    this.common$.diags_USER1 = Diags.ALWAYS_ON;

    Exception = this;

    if (BIOS.buildingAppLib == true) {
        /* add -D to compile line to optimize exception code */
        Build.ccArgs.$add("-Dti_sysbios_family_c7x_Exception_enablePrint__D=" +
            (Exception.enablePrint ? "TRUE" : "FALSE"));
    }
}

/*
 *  ======== module$static$init ========
 */
function module$static$init(mod, params)
{
    mod.nrp = null;
    mod.ntsr = null;
    mod.ierr = null;
    mod.iear = null;
    mod.iesr = null;
    mod.returnHook = params.returnHook;
    mod.excContext = null;
    mod.excPtr = null;

    if (params.useInternalBuffer) {
        /*
         *  The size of context buffer must be at least 74 words (296 bytes)
         *  to hold the exception context.  Add some padding.
         */
        mod.contextBuf.length = Exception.sizeContextBuf;

        /* init the context buffer to 0 */
        for (var i=0; i < mod.contextBuf.length; i++) {
            mod.contextBuf[i] = 0;
        }
    }
    else {
        mod.contextBuf.length = 0;
    }

    Build.ccArgs.$add("-Dti_sysbios_family_c7x_Exception_vectors__D");
}
