/*
 *  Do not modify this file; it is automatically 
 *  generated and any modifications will be overwritten.
 *
 * @(#) xdc-J06
 */

#ifndef ti_sysbios_family_c28_f2838x_init_Boot__INTERNAL__
#define ti_sysbios_family_c28_f2838x_init_Boot__INTERNAL__

#ifndef ti_sysbios_family_c28_f2838x_init_Boot__internalaccess
#define ti_sysbios_family_c28_f2838x_init_Boot__internalaccess
#endif

#include <ti/sysbios/family/c28/f2838x/init/Boot.h>

#undef xdc_FILE__
#ifndef xdc_FILE
#define xdc_FILE__ NULL
#else
#define xdc_FILE__ xdc_FILE
#endif

/* getFrequency */
#undef ti_sysbios_family_c28_f2838x_init_Boot_getFrequency
#define ti_sysbios_family_c28_f2838x_init_Boot_getFrequency ti_sysbios_family_c28_f2838x_init_Boot_getFrequency__E


#endif /* ti_sysbios_family_c28_f2838x_init_Boot__INTERNAL____ */
