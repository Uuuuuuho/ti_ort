/*
 *  Do not modify this file; it is automatically 
 *  generated and any modifications will be overwritten.
 *
 * @(#) xdc-J06
 */
import java.util.*;
import org.mozilla.javascript.*;
import xdc.services.intern.xsr.*;
import xdc.services.spec.Session;

public class ti_sysbios_family_c28_f2838x_init
{
    static final String VERS = "@(#) xdc-J06\n";

    static final Proto.Elm $$T_Bool = Proto.Elm.newBool();
    static final Proto.Elm $$T_Num = Proto.Elm.newNum();
    static final Proto.Elm $$T_Str = Proto.Elm.newStr();
    static final Proto.Elm $$T_Obj = Proto.Elm.newObj();

    static final Proto.Fxn $$T_Met = new Proto.Fxn(null, null, 0, -1, false);
    static final Proto.Map $$T_Map = new Proto.Map($$T_Obj);
    static final Proto.Arr $$T_Vec = new Proto.Arr($$T_Obj);

    static final XScriptO $$DEFAULT = Value.DEFAULT;
    static final Object $$UNDEF = Undefined.instance;

    static final Proto.Obj $$Package = (Proto.Obj)Global.get("$$Package");
    static final Proto.Obj $$Module = (Proto.Obj)Global.get("$$Module");
    static final Proto.Obj $$Instance = (Proto.Obj)Global.get("$$Instance");
    static final Proto.Obj $$Params = (Proto.Obj)Global.get("$$Params");

    static final Object $$objFldGet = Global.get("$$objFldGet");
    static final Object $$objFldSet = Global.get("$$objFldSet");
    static final Object $$proxyGet = Global.get("$$proxyGet");
    static final Object $$proxySet = Global.get("$$proxySet");
    static final Object $$delegGet = Global.get("$$delegGet");
    static final Object $$delegSet = Global.get("$$delegSet");

    Scriptable xdcO;
    Session ses;
    Value.Obj om;

    boolean isROV;
    boolean isCFG;

    Proto.Obj pkgP;
    Value.Obj pkgV;

    ArrayList<Object> imports = new ArrayList<Object>();
    ArrayList<Object> loggables = new ArrayList<Object>();
    ArrayList<Object> mcfgs = new ArrayList<Object>();
    ArrayList<Object> icfgs = new ArrayList<Object>();
    ArrayList<String> inherits = new ArrayList<String>();
    ArrayList<Object> proxies = new ArrayList<Object>();
    ArrayList<Object> sizes = new ArrayList<Object>();
    ArrayList<Object> tdefs = new ArrayList<Object>();

    void $$IMPORTS()
    {
        Global.callFxn("loadPackage", xdcO, "xdc");
        Global.callFxn("loadPackage", xdcO, "xdc.corevers");
        Global.callFxn("loadPackage", xdcO, "xdc.rov");
        Global.callFxn("loadPackage", xdcO, "xdc.runtime");
    }

    void $$OBJECTS()
    {
        pkgP = (Proto.Obj)om.bind("ti.sysbios.family.c28.f2838x.init.Package", new Proto.Obj());
        pkgV = (Value.Obj)om.bind("ti.sysbios.family.c28.f2838x.init", new Value.Obj("ti.sysbios.family.c28.f2838x.init", pkgP));
    }

    void Boot$$OBJECTS()
    {
        Proto.Obj po, spo;
        Value.Obj vo;

        po = (Proto.Obj)om.bind("ti.sysbios.family.c28.f2838x.init.Boot.Module", new Proto.Obj());
        vo = (Value.Obj)om.bind("ti.sysbios.family.c28.f2838x.init.Boot", new Value.Obj("ti.sysbios.family.c28.f2838x.init.Boot", po));
        pkgV.bind("Boot", vo);
        // decls 
        om.bind("ti.sysbios.family.c28.f2838x.init.Boot.OscClk", new Proto.Enm("ti.sysbios.family.c28.f2838x.init.Boot.OscClk"));
        om.bind("ti.sysbios.family.c28.f2838x.init.Boot.SrcCM", new Proto.Enm("ti.sysbios.family.c28.f2838x.init.Boot.SrcCM"));
        om.bind("ti.sysbios.family.c28.f2838x.init.Boot.Div", new Proto.Enm("ti.sysbios.family.c28.f2838x.init.Boot.Div"));
        spo = (Proto.Obj)om.bind("ti.sysbios.family.c28.f2838x.init.Boot$$ModuleView", new Proto.Obj());
        om.bind("ti.sysbios.family.c28.f2838x.init.Boot.ModuleView", new Proto.Str(spo, false));
    }

    void Boot$$CONSTS()
    {
        // module Boot
        om.bind("ti.sysbios.family.c28.f2838x.init.Boot.OscClk_INTOSC2", xdc.services.intern.xsr.Enum.make((Proto.Enm)om.findStrict("ti.sysbios.family.c28.f2838x.init.Boot.OscClk", "ti.sysbios.family.c28.f2838x.init"), "ti.sysbios.family.c28.f2838x.init.Boot.OscClk_INTOSC2", xdc.services.intern.xsr.Enum.intValue(0x0L)+0));
        om.bind("ti.sysbios.family.c28.f2838x.init.Boot.OscClk_XTAL", xdc.services.intern.xsr.Enum.make((Proto.Enm)om.findStrict("ti.sysbios.family.c28.f2838x.init.Boot.OscClk", "ti.sysbios.family.c28.f2838x.init"), "ti.sysbios.family.c28.f2838x.init.Boot.OscClk_XTAL", xdc.services.intern.xsr.Enum.intValue(0x1L)+0));
        om.bind("ti.sysbios.family.c28.f2838x.init.Boot.OscClk_INTOSC1", xdc.services.intern.xsr.Enum.make((Proto.Enm)om.findStrict("ti.sysbios.family.c28.f2838x.init.Boot.OscClk", "ti.sysbios.family.c28.f2838x.init"), "ti.sysbios.family.c28.f2838x.init.Boot.OscClk_INTOSC1", xdc.services.intern.xsr.Enum.intValue(0x2L)+0));
        om.bind("ti.sysbios.family.c28.f2838x.init.Boot.OscClk_RESERVED", xdc.services.intern.xsr.Enum.make((Proto.Enm)om.findStrict("ti.sysbios.family.c28.f2838x.init.Boot.OscClk", "ti.sysbios.family.c28.f2838x.init"), "ti.sysbios.family.c28.f2838x.init.Boot.OscClk_RESERVED", xdc.services.intern.xsr.Enum.intValue(0x3L)+0));
        om.bind("ti.sysbios.family.c28.f2838x.init.Boot.SrcCM_AuxPLL", xdc.services.intern.xsr.Enum.make((Proto.Enm)om.findStrict("ti.sysbios.family.c28.f2838x.init.Boot.SrcCM", "ti.sysbios.family.c28.f2838x.init"), "ti.sysbios.family.c28.f2838x.init.Boot.SrcCM_AuxPLL", xdc.services.intern.xsr.Enum.intValue(0L)+0));
        om.bind("ti.sysbios.family.c28.f2838x.init.Boot.SrcCM_SystemPLL", xdc.services.intern.xsr.Enum.make((Proto.Enm)om.findStrict("ti.sysbios.family.c28.f2838x.init.Boot.SrcCM", "ti.sysbios.family.c28.f2838x.init"), "ti.sysbios.family.c28.f2838x.init.Boot.SrcCM_SystemPLL", xdc.services.intern.xsr.Enum.intValue(1L)+0));
        om.bind("ti.sysbios.family.c28.f2838x.init.Boot.Div_1", xdc.services.intern.xsr.Enum.make((Proto.Enm)om.findStrict("ti.sysbios.family.c28.f2838x.init.Boot.Div", "ti.sysbios.family.c28.f2838x.init"), "ti.sysbios.family.c28.f2838x.init.Boot.Div_1", xdc.services.intern.xsr.Enum.intValue(0L)+0));
        om.bind("ti.sysbios.family.c28.f2838x.init.Boot.Div_2", xdc.services.intern.xsr.Enum.make((Proto.Enm)om.findStrict("ti.sysbios.family.c28.f2838x.init.Boot.Div", "ti.sysbios.family.c28.f2838x.init"), "ti.sysbios.family.c28.f2838x.init.Boot.Div_2", xdc.services.intern.xsr.Enum.intValue(1L)+0));
        om.bind("ti.sysbios.family.c28.f2838x.init.Boot.Div_3", xdc.services.intern.xsr.Enum.make((Proto.Enm)om.findStrict("ti.sysbios.family.c28.f2838x.init.Boot.Div", "ti.sysbios.family.c28.f2838x.init"), "ti.sysbios.family.c28.f2838x.init.Boot.Div_3", xdc.services.intern.xsr.Enum.intValue(2L)+0));
        om.bind("ti.sysbios.family.c28.f2838x.init.Boot.Div_4", xdc.services.intern.xsr.Enum.make((Proto.Enm)om.findStrict("ti.sysbios.family.c28.f2838x.init.Boot.Div", "ti.sysbios.family.c28.f2838x.init"), "ti.sysbios.family.c28.f2838x.init.Boot.Div_4", xdc.services.intern.xsr.Enum.intValue(3L)+0));
        om.bind("ti.sysbios.family.c28.f2838x.init.Boot.Div_5", xdc.services.intern.xsr.Enum.make((Proto.Enm)om.findStrict("ti.sysbios.family.c28.f2838x.init.Boot.Div", "ti.sysbios.family.c28.f2838x.init"), "ti.sysbios.family.c28.f2838x.init.Boot.Div_5", xdc.services.intern.xsr.Enum.intValue(4L)+0));
        om.bind("ti.sysbios.family.c28.f2838x.init.Boot.Div_6", xdc.services.intern.xsr.Enum.make((Proto.Enm)om.findStrict("ti.sysbios.family.c28.f2838x.init.Boot.Div", "ti.sysbios.family.c28.f2838x.init"), "ti.sysbios.family.c28.f2838x.init.Boot.Div_6", xdc.services.intern.xsr.Enum.intValue(5L)+0));
        om.bind("ti.sysbios.family.c28.f2838x.init.Boot.Div_7", xdc.services.intern.xsr.Enum.make((Proto.Enm)om.findStrict("ti.sysbios.family.c28.f2838x.init.Boot.Div", "ti.sysbios.family.c28.f2838x.init"), "ti.sysbios.family.c28.f2838x.init.Boot.Div_7", xdc.services.intern.xsr.Enum.intValue(6L)+0));
        om.bind("ti.sysbios.family.c28.f2838x.init.Boot.Div_8", xdc.services.intern.xsr.Enum.make((Proto.Enm)om.findStrict("ti.sysbios.family.c28.f2838x.init.Boot.Div", "ti.sysbios.family.c28.f2838x.init"), "ti.sysbios.family.c28.f2838x.init.Boot.Div_8", xdc.services.intern.xsr.Enum.intValue(7L)+0));
        om.bind("ti.sysbios.family.c28.f2838x.init.Boot.getFrequency", new Extern("ti_sysbios_family_c28_f2838x_init_Boot_getFrequency__E", "xdc_UInt32(*)(xdc_Void)", true, false));
    }

    void Boot$$CREATES()
    {
        Proto.Fxn fxn;
        StringBuilder sb;

    }

    void Boot$$FUNCTIONS()
    {
        Proto.Fxn fxn;

        // fxn Boot.registerFreqListener
        fxn = (Proto.Fxn)om.bind("ti.sysbios.family.c28.f2838x.init.Boot$$registerFreqListener", new Proto.Fxn(om.findStrict("ti.sysbios.family.c28.f2838x.init.Boot.Module", "ti.sysbios.family.c28.f2838x.init"), null, 0, -1, false));
    }

    void Boot$$SIZES()
    {
        Proto.Str so;
        Object fxn;

    }

    void Boot$$TYPES()
    {
        Scriptable cap;
        Proto.Obj po;
        Proto.Str ps;
        Proto.Typedef pt;
        Object fxn;

        cap = (Scriptable)Global.callFxn("loadCapsule", xdcO, "ti/sysbios/family/c28/f2838x/init/Boot.xs");
        om.bind("ti.sysbios.family.c28.f2838x.init.Boot$$capsule", cap);
        po = (Proto.Obj)om.findStrict("ti.sysbios.family.c28.f2838x.init.Boot.Module", "ti.sysbios.family.c28.f2838x.init");
        po.init("ti.sysbios.family.c28.f2838x.init.Boot.Module", om.findStrict("xdc.runtime.IModule.Module", "ti.sysbios.family.c28.f2838x.init"));
                po.addFld("$hostonly", $$T_Num, 0, "r");
        if (isCFG) {
            po.addFld("rovViewInfo", (Proto)om.findStrict("xdc.rov.ViewInfo.Instance", "ti.sysbios.family.c28.f2838x.init"), $$UNDEF, "wh");
            po.addFld("disableWatchdog", $$T_Bool, false, "wh");
            po.addFld("configureClocks", $$T_Bool, false, "w");
            po.addFld("OSCCLKSRCSEL", (Proto)om.findStrict("ti.sysbios.family.c28.f2838x.init.Boot.OscClk", "ti.sysbios.family.c28.f2838x.init"), om.find("ti.sysbios.family.c28.f2838x.init.Boot.OscClk_INTOSC2"), "wh");
            po.addFld("OSCCLK", Proto.Elm.newCNum("(xdc_UInt)"), 10L, "wh");
            po.addFld("SPLLIMULT", Proto.Elm.newCNum("(xdc_UInt)"), 38L, "wh");
            po.addFld("SPLLREFDIV", Proto.Elm.newCNum("(xdc_UInt)"), 0L, "wh");
            po.addFld("SPLLODIV", Proto.Elm.newCNum("(xdc_UInt)"), 1L, "wh");
            po.addFld("SYSCLKDIVSEL", Proto.Elm.newCNum("(xdc_UInt)"), 0L, "wh");
            po.addFld("CMDIVSRCSEL", (Proto)om.findStrict("ti.sysbios.family.c28.f2838x.init.Boot.SrcCM", "ti.sysbios.family.c28.f2838x.init"), om.find("ti.sysbios.family.c28.f2838x.init.Boot.SrcCM_SystemPLL"), "wh");
            po.addFld("CMCLKDIV", (Proto)om.findStrict("ti.sysbios.family.c28.f2838x.init.Boot.Div", "ti.sysbios.family.c28.f2838x.init"), om.find("ti.sysbios.family.c28.f2838x.init.Boot.Div_2"), "wh");
            po.addFld("limpAbortFunction", new Proto.Adr("xdc_Fxn", "Pf"), $$UNDEF, "wh");
            po.addFld("configureFlashController", $$T_Bool, true, "wh");
            po.addFld("configureFlashWaitStates", $$T_Bool, true, "wh");
            po.addFld("enableFlashProgramCache", $$T_Bool, true, "wh");
            po.addFld("enableFlashDataCache", $$T_Bool, true, "wh");
            po.addFld("bootFromFlash", $$T_Bool, true, "wh");
            po.addFld("configureSharedRAMs", $$T_Bool, false, "wh");
            po.addFld("sharedMemoryOwnerMask", Proto.Elm.newCNum("(xdc_Bits32)"), 0L, "wh");
            po.addFld("loadSegment", $$T_Str, $$UNDEF, "wh");
            po.addFld("runSegment", $$T_Str, $$UNDEF, "wh");
            po.addFld("timestampFreq", Proto.Elm.newCNum("(xdc_UInt)"), $$UNDEF, "wh");
            po.addFld("displayFrequency", $$T_Str, $$UNDEF, "wh");
            po.addFld("flashWaitStates", Proto.Elm.newCNum("(xdc_UInt)"), $$UNDEF, "wh");
        }//isCFG
        fxn = Global.get(cap, "module$use");
        if (fxn != null) om.bind("ti.sysbios.family.c28.f2838x.init.Boot$$module$use", true);
        if (fxn != null) po.addFxn("module$use", $$T_Met, fxn);
        fxn = Global.get(cap, "module$meta$init");
        if (fxn != null) om.bind("ti.sysbios.family.c28.f2838x.init.Boot$$module$meta$init", true);
        if (fxn != null) po.addFxn("module$meta$init", $$T_Met, fxn);
        fxn = Global.get(cap, "module$static$init");
        if (fxn != null) om.bind("ti.sysbios.family.c28.f2838x.init.Boot$$module$static$init", true);
        if (fxn != null) po.addFxn("module$static$init", $$T_Met, fxn);
        fxn = Global.get(cap, "module$validate");
        if (fxn != null) om.bind("ti.sysbios.family.c28.f2838x.init.Boot$$module$validate", true);
        if (fxn != null) po.addFxn("module$validate", $$T_Met, fxn);
                po.addFxn("registerFreqListener", (Proto.Fxn)om.findStrict("ti.sysbios.family.c28.f2838x.init.Boot$$registerFreqListener", "ti.sysbios.family.c28.f2838x.init"), Global.get(cap, "registerFreqListener"));
        // struct Boot.ModuleView
        po = (Proto.Obj)om.findStrict("ti.sysbios.family.c28.f2838x.init.Boot$$ModuleView", "ti.sysbios.family.c28.f2838x.init");
        po.init("ti.sysbios.family.c28.f2838x.init.Boot.ModuleView", null);
                po.addFld("$hostonly", $$T_Num, 1, "r");
                po.addFld("disableWatchdog", $$T_Bool, $$UNDEF, "w");
                po.addFld("configureClocks", $$T_Bool, $$UNDEF, "w");
                po.addFld("configureFlashController", $$T_Bool, $$UNDEF, "w");
                po.addFld("configureFlashWaitStates", $$T_Bool, $$UNDEF, "w");
                po.addFld("enableFlashProgramCache", $$T_Bool, $$UNDEF, "w");
                po.addFld("enableFlashDataCache", $$T_Bool, $$UNDEF, "w");
                po.addFld("configureSharedRAMs", $$T_Bool, $$UNDEF, "w");
                po.addFld("bootFromFlash", $$T_Bool, $$UNDEF, "w");
    }

    void Boot$$ROV()
    {
        Proto.Obj po;
        Value.Obj vo;

        vo = (Value.Obj)om.findStrict("ti.sysbios.family.c28.f2838x.init.Boot", "ti.sysbios.family.c28.f2838x.init");
    }

    void $$SINGLETONS()
    {
        pkgP.init("ti.sysbios.family.c28.f2838x.init.Package", (Proto.Obj)om.findStrict("xdc.IPackage.Module", "ti.sysbios.family.c28.f2838x.init"));
        Scriptable cap = (Scriptable)Global.callFxn("loadCapsule", xdcO, "ti/sysbios/family/c28/f2838x/init/package.xs");
        om.bind("xdc.IPackage$$capsule", cap);
        Object fxn;
                fxn = Global.get(cap, "init");
                if (fxn != null) pkgP.addFxn("init", (Proto.Fxn)om.findStrict("xdc.IPackage$$init", "ti.sysbios.family.c28.f2838x.init"), fxn);
                fxn = Global.get(cap, "close");
                if (fxn != null) pkgP.addFxn("close", (Proto.Fxn)om.findStrict("xdc.IPackage$$close", "ti.sysbios.family.c28.f2838x.init"), fxn);
                fxn = Global.get(cap, "validate");
                if (fxn != null) pkgP.addFxn("validate", (Proto.Fxn)om.findStrict("xdc.IPackage$$validate", "ti.sysbios.family.c28.f2838x.init"), fxn);
                fxn = Global.get(cap, "exit");
                if (fxn != null) pkgP.addFxn("exit", (Proto.Fxn)om.findStrict("xdc.IPackage$$exit", "ti.sysbios.family.c28.f2838x.init"), fxn);
                fxn = Global.get(cap, "getLibs");
                if (fxn != null) pkgP.addFxn("getLibs", (Proto.Fxn)om.findStrict("xdc.IPackage$$getLibs", "ti.sysbios.family.c28.f2838x.init"), fxn);
                fxn = Global.get(cap, "getSects");
                if (fxn != null) pkgP.addFxn("getSects", (Proto.Fxn)om.findStrict("xdc.IPackage$$getSects", "ti.sysbios.family.c28.f2838x.init"), fxn);
        pkgP.bind("$capsule", cap);
        pkgV.init2(pkgP, "ti.sysbios.family.c28.f2838x.init", Value.DEFAULT, false);
        pkgV.bind("$name", "ti.sysbios.family.c28.f2838x.init");
        pkgV.bind("$category", "Package");
        pkgV.bind("$$qn", "ti.sysbios.family.c28.f2838x.init.");
        pkgV.bind("$vers", Global.newArray(1, 0, 0));
        Value.Map atmap = (Value.Map)pkgV.getv("$attr");
        atmap.seal("length");
        imports.clear();
        pkgV.bind("$imports", imports);
        StringBuilder sb = new StringBuilder();
        sb.append("var pkg = xdc.om['ti.sysbios.family.c28.f2838x.init'];\n");
        sb.append("if (pkg.$vers.length >= 3) {\n");
            sb.append("pkg.$vers.push(Packages.xdc.services.global.Vers.getDate(xdc.csd() + '/..'));\n");
        sb.append("}\n");
        sb.append("if ('ti.sysbios.family.c28.f2838x.init$$stat$base' in xdc.om) {\n");
            sb.append("pkg.packageBase = xdc.om['ti.sysbios.family.c28.f2838x.init$$stat$base'];\n");
            sb.append("pkg.packageRepository = xdc.om['ti.sysbios.family.c28.f2838x.init$$stat$root'];\n");
        sb.append("}\n");
        sb.append("pkg.build.libraries = [\n");
            sb.append("'lib/Boot.ae28FP',\n");
            sb.append("'lib/Boot.ae28FP64',\n");
            sb.append("'lib/Boot.a28FP',\n");
        sb.append("];\n");
        sb.append("pkg.build.libDesc = [\n");
            sb.append("['lib/Boot.ae28FP', {target: 'ti.targets.elf.C28_float', suffix: 'e28FP'}],\n");
            sb.append("['lib/Boot.ae28FP64', {target: 'ti.targets.elf.C28_float64', suffix: 'e28FP64'}],\n");
            sb.append("['lib/Boot.a28FP', {target: 'ti.targets.C28_float', suffix: '28FP'}],\n");
        sb.append("];\n");
        Global.eval(sb.toString());
    }

    void Boot$$SINGLETONS()
    {
        Proto.Obj po;
        Value.Obj vo;

        vo = (Value.Obj)om.findStrict("ti.sysbios.family.c28.f2838x.init.Boot", "ti.sysbios.family.c28.f2838x.init");
        po = (Proto.Obj)om.findStrict("ti.sysbios.family.c28.f2838x.init.Boot.Module", "ti.sysbios.family.c28.f2838x.init");
        vo.init2(po, "ti.sysbios.family.c28.f2838x.init.Boot", $$DEFAULT, false);
        vo.bind("Module", po);
        vo.bind("$category", "Module");
        vo.bind("$capsule", om.findStrict("ti.sysbios.family.c28.f2838x.init.Boot$$capsule", "ti.sysbios.family.c28.f2838x.init"));
        vo.bind("$package", om.findStrict("ti.sysbios.family.c28.f2838x.init", "ti.sysbios.family.c28.f2838x.init"));
        tdefs.clear();
        proxies.clear();
        mcfgs.clear();
        icfgs.clear();
        inherits.clear();
        mcfgs.add("Module__diagsEnabled");
        icfgs.add("Module__diagsEnabled");
        mcfgs.add("Module__diagsIncluded");
        icfgs.add("Module__diagsIncluded");
        mcfgs.add("Module__diagsMask");
        icfgs.add("Module__diagsMask");
        mcfgs.add("Module__gateObj");
        icfgs.add("Module__gateObj");
        mcfgs.add("Module__gatePrms");
        icfgs.add("Module__gatePrms");
        mcfgs.add("Module__id");
        icfgs.add("Module__id");
        mcfgs.add("Module__loggerDefined");
        icfgs.add("Module__loggerDefined");
        mcfgs.add("Module__loggerObj");
        icfgs.add("Module__loggerObj");
        mcfgs.add("Module__loggerFxn0");
        icfgs.add("Module__loggerFxn0");
        mcfgs.add("Module__loggerFxn1");
        icfgs.add("Module__loggerFxn1");
        mcfgs.add("Module__loggerFxn2");
        icfgs.add("Module__loggerFxn2");
        mcfgs.add("Module__loggerFxn4");
        icfgs.add("Module__loggerFxn4");
        mcfgs.add("Module__loggerFxn8");
        icfgs.add("Module__loggerFxn8");
        mcfgs.add("Object__count");
        icfgs.add("Object__count");
        mcfgs.add("Object__heap");
        icfgs.add("Object__heap");
        mcfgs.add("Object__sizeof");
        icfgs.add("Object__sizeof");
        mcfgs.add("Object__table");
        icfgs.add("Object__table");
        vo.bind("OscClk", om.findStrict("ti.sysbios.family.c28.f2838x.init.Boot.OscClk", "ti.sysbios.family.c28.f2838x.init"));
        vo.bind("SrcCM", om.findStrict("ti.sysbios.family.c28.f2838x.init.Boot.SrcCM", "ti.sysbios.family.c28.f2838x.init"));
        vo.bind("Div", om.findStrict("ti.sysbios.family.c28.f2838x.init.Boot.Div", "ti.sysbios.family.c28.f2838x.init"));
        vo.bind("ModuleView", om.findStrict("ti.sysbios.family.c28.f2838x.init.Boot.ModuleView", "ti.sysbios.family.c28.f2838x.init"));
        tdefs.add(om.findStrict("ti.sysbios.family.c28.f2838x.init.Boot.ModuleView", "ti.sysbios.family.c28.f2838x.init"));
        mcfgs.add("configureClocks");
        icfgs.add("timestampFreq");
        icfgs.add("displayFrequency");
        icfgs.add("flashWaitStates");
        vo.bind("OscClk_INTOSC2", om.findStrict("ti.sysbios.family.c28.f2838x.init.Boot.OscClk_INTOSC2", "ti.sysbios.family.c28.f2838x.init"));
        vo.bind("OscClk_XTAL", om.findStrict("ti.sysbios.family.c28.f2838x.init.Boot.OscClk_XTAL", "ti.sysbios.family.c28.f2838x.init"));
        vo.bind("OscClk_INTOSC1", om.findStrict("ti.sysbios.family.c28.f2838x.init.Boot.OscClk_INTOSC1", "ti.sysbios.family.c28.f2838x.init"));
        vo.bind("OscClk_RESERVED", om.findStrict("ti.sysbios.family.c28.f2838x.init.Boot.OscClk_RESERVED", "ti.sysbios.family.c28.f2838x.init"));
        vo.bind("SrcCM_AuxPLL", om.findStrict("ti.sysbios.family.c28.f2838x.init.Boot.SrcCM_AuxPLL", "ti.sysbios.family.c28.f2838x.init"));
        vo.bind("SrcCM_SystemPLL", om.findStrict("ti.sysbios.family.c28.f2838x.init.Boot.SrcCM_SystemPLL", "ti.sysbios.family.c28.f2838x.init"));
        vo.bind("Div_1", om.findStrict("ti.sysbios.family.c28.f2838x.init.Boot.Div_1", "ti.sysbios.family.c28.f2838x.init"));
        vo.bind("Div_2", om.findStrict("ti.sysbios.family.c28.f2838x.init.Boot.Div_2", "ti.sysbios.family.c28.f2838x.init"));
        vo.bind("Div_3", om.findStrict("ti.sysbios.family.c28.f2838x.init.Boot.Div_3", "ti.sysbios.family.c28.f2838x.init"));
        vo.bind("Div_4", om.findStrict("ti.sysbios.family.c28.f2838x.init.Boot.Div_4", "ti.sysbios.family.c28.f2838x.init"));
        vo.bind("Div_5", om.findStrict("ti.sysbios.family.c28.f2838x.init.Boot.Div_5", "ti.sysbios.family.c28.f2838x.init"));
        vo.bind("Div_6", om.findStrict("ti.sysbios.family.c28.f2838x.init.Boot.Div_6", "ti.sysbios.family.c28.f2838x.init"));
        vo.bind("Div_7", om.findStrict("ti.sysbios.family.c28.f2838x.init.Boot.Div_7", "ti.sysbios.family.c28.f2838x.init"));
        vo.bind("Div_8", om.findStrict("ti.sysbios.family.c28.f2838x.init.Boot.Div_8", "ti.sysbios.family.c28.f2838x.init"));
        vo.bind("$$tdefs", Global.newArray(tdefs.toArray()));
        vo.bind("$$proxies", Global.newArray(proxies.toArray()));
        vo.bind("$$mcfgs", Global.newArray(mcfgs.toArray()));
        vo.bind("$$icfgs", Global.newArray(icfgs.toArray()));
        inherits.add("xdc.runtime");
        vo.bind("$$inherits", Global.newArray(inherits.toArray()));
        ((Value.Arr)pkgV.getv("$modules")).add(vo);
        ((Value.Arr)om.findStrict("$modules", "ti.sysbios.family.c28.f2838x.init")).add(vo);
        vo.bind("$$instflag", 0);
        vo.bind("$$iobjflag", 0);
        vo.bind("$$sizeflag", 1);
        vo.bind("$$dlgflag", 0);
        vo.bind("$$iflag", 0);
        vo.bind("$$romcfgs", "|");
        vo.bind("$$nortsflag", 1);
        if (isCFG) {
            Proto.Str ps = (Proto.Str)vo.find("Module_State");
            if (ps != null) vo.bind("$object", ps.newInstance());
            vo.bind("$$meta_iobj", 1);
        }//isCFG
        vo.bind("getFrequency", om.findStrict("ti.sysbios.family.c28.f2838x.init.Boot.getFrequency", "ti.sysbios.family.c28.f2838x.init"));
        vo.bind("$$fxntab", Global.newArray("ti_sysbios_family_c28_f2838x_init_Boot_Module__startupDone__E", "ti_sysbios_family_c28_f2838x_init_Boot_getFrequency__E"));
        vo.bind("$$logEvtCfgs", Global.newArray());
        vo.bind("$$errorDescCfgs", Global.newArray());
        vo.bind("$$assertDescCfgs", Global.newArray());
        Value.Map atmap = (Value.Map)vo.getv("$attr");
        atmap.setElem("", true);
        atmap.setElem("", "./Boot.xdt");
        atmap.seal("length");
        if (isCFG) {
            vo.put("common$", vo, Global.get((Proto.Obj)om.find("xdc.runtime.Defaults.Module"), "noRuntimeCommon$"));
            ((Value.Obj)vo.geto("common$")).seal(null);
        }//isCFG
        vo.bind("MODULE_STARTUP$", 0);
        vo.bind("PROXY$", 0);
        loggables.clear();
        vo.bind("$$loggables", loggables.toArray());
        vo.bind("TEMPLATE$", "./Boot.xdt");
        pkgV.bind("Boot", vo);
        ((Value.Arr)pkgV.getv("$unitNames")).add("Boot");
    }

    void $$INITIALIZATION()
    {
        Value.Obj vo;

        if (isCFG) {
        }//isCFG
        Global.callFxn("module$meta$init", (Scriptable)om.findStrict("ti.sysbios.family.c28.f2838x.init.Boot", "ti.sysbios.family.c28.f2838x.init"));
        if (isCFG) {
            vo = (Value.Obj)om.findStrict("ti.sysbios.family.c28.f2838x.init.Boot", "ti.sysbios.family.c28.f2838x.init");
            Global.put(vo, "rovViewInfo", Global.callFxn("create", (Scriptable)om.find("xdc.rov.ViewInfo"), Global.newObject("viewMap", Global.newArray(new Object[]{Global.newArray(new Object[]{"Module", Global.newObject("type", om.find("xdc.rov.ViewInfo.MODULE"), "viewInitFxn", "viewInitModule", "structName", "ModuleView")})}))));
        }//isCFG
        Global.callFxn("init", pkgV);
        ((Value.Obj)om.getv("ti.sysbios.family.c28.f2838x.init.Boot")).bless();
        ((Value.Arr)om.findStrict("$packages", "ti.sysbios.family.c28.f2838x.init")).add(pkgV);
    }

    public void exec( Scriptable xdcO, Session ses )
    {
        this.xdcO = xdcO;
        this.ses = ses;
        om = (Value.Obj)xdcO.get("om", null);

        Object o = om.geto("$name");
        String s = o instanceof String ? (String)o : null;
        isCFG = s != null && s.equals("cfg");
        isROV = s != null && s.equals("rov");

        $$IMPORTS();
        $$OBJECTS();
        Boot$$OBJECTS();
        Boot$$CONSTS();
        Boot$$CREATES();
        Boot$$FUNCTIONS();
        Boot$$SIZES();
        Boot$$TYPES();
        if (isROV) {
            Boot$$ROV();
        }//isROV
        $$SINGLETONS();
        Boot$$SINGLETONS();
        $$INITIALIZATION();
    }
}
