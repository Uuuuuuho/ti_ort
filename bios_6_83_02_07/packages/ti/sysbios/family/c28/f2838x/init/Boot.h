/*
 *  Do not modify this file; it is automatically 
 *  generated and any modifications will be overwritten.
 *
 * @(#) xdc-J06
 */

/*
 * ======== GENERATED SECTIONS ========
 *
 *     PROLOGUE
 *     INCLUDES
 *
 *     FUNCTION DECLARATIONS
 *
 *     EPILOGUE
 *     STATE STRUCTURES
 *     PREFIX ALIASES
 */


/*
 * ======== PROLOGUE ========
 */

#ifndef ti_sysbios_family_c28_f2838x_init_Boot__include
#define ti_sysbios_family_c28_f2838x_init_Boot__include

#ifndef __nested__
#define __nested__
#define ti_sysbios_family_c28_f2838x_init_Boot__top__
#endif

#ifndef __extern
#ifdef __cplusplus
#define __extern extern "C"
#else
#define __extern extern
#endif
#endif

#define ti_sysbios_family_c28_f2838x_init_Boot___VERS 200


/*
 * ======== INCLUDES ========
 */

#include <xdc/std.h>

#include <ti/sysbios/family/c28/f2838x/init/package/package.defs.h>


/*
 * ======== AUXILIARY DEFINITIONS ========
 */


/*
 * ======== FUNCTION DECLARATIONS ========
 */

/* getFrequency__E */
#define ti_sysbios_family_c28_f2838x_init_Boot_getFrequency ti_sysbios_family_c28_f2838x_init_Boot_getFrequency__E
xdc__CODESECT(ti_sysbios_family_c28_f2838x_init_Boot_getFrequency__E, "ti_sysbios_family_c28_f2838x_init_Boot_getFrequency")
__extern xdc_UInt32 ti_sysbios_family_c28_f2838x_init_Boot_getFrequency__E( void);


/*
 * ======== EPILOGUE ========
 */

#ifdef ti_sysbios_family_c28_f2838x_init_Boot__top__
#undef __nested__
#endif

#endif /* ti_sysbios_family_c28_f2838x_init_Boot__include */


/*
 * ======== STATE STRUCTURES ========
 */

#if defined(__config__) || (!defined(__nested__) && defined(ti_sysbios_family_c28_f2838x_init_Boot__internalaccess))

#ifndef ti_sysbios_family_c28_f2838x_init_Boot__include_state
#define ti_sysbios_family_c28_f2838x_init_Boot__include_state


#endif /* ti_sysbios_family_c28_f2838x_init_Boot__include_state */

#endif

/*
 * ======== PREFIX ALIASES ========
 */

#if !defined(__nested__) && !defined(ti_sysbios_family_c28_f2838x_init_Boot__nolocalnames)

#ifndef ti_sysbios_family_c28_f2838x_init_Boot__localnames__done
#define ti_sysbios_family_c28_f2838x_init_Boot__localnames__done

/* module prefix */
#define Boot_configureClocks ti_sysbios_family_c28_f2838x_init_Boot_configureClocks
#define Boot_getFrequency ti_sysbios_family_c28_f2838x_init_Boot_getFrequency

#endif /* ti_sysbios_family_c28_f2838x_init_Boot__localnames__done */
#endif
