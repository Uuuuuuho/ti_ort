/*
 *  Do not modify this file; it is automatically 
 *  generated and any modifications will be overwritten.
 *
 * @(#) xdc-J06
 */

#ifndef ti_sysbios_family_c28_f2838x__
#define ti_sysbios_family_c28_f2838x__


/*
 * ======== module ti.sysbios.family.c28.f2838x.TimestampProvider ========
 */

typedef struct ti_sysbios_family_c28_f2838x_TimestampProvider_Fxns__ ti_sysbios_family_c28_f2838x_TimestampProvider_Fxns__;
typedef const struct ti_sysbios_family_c28_f2838x_TimestampProvider_Fxns__* ti_sysbios_family_c28_f2838x_TimestampProvider_Module;


#endif /* ti_sysbios_family_c28_f2838x__ */ 
