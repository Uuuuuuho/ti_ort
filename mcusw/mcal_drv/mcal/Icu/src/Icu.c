/*
*
* Copyright (c) 2021 Texas Instruments Incorporated
*
* All rights reserved not granted herein.
*
* Limited License.
*
* Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
* license under copyrights and patents it now or hereafter owns or controls to make,
* have made, use, import, offer to sell and sell ("Utilize") this software subject to the
* terms herein.  With respect to the foregoing patent license, such license is granted
* solely to the extent that any such patent is necessary to Utilize the software alone.
* The patent license shall not apply to any combinations which include this software,
* other than combinations with devices manufactured by or for TI ("TI Devices").
* No hardware patent is licensed hereunder.
*
* Redistributions must preserve existing copyright notices and reproduce this license
* (including the above copyright notice and the disclaimer and (if applicable) source
* code license limitations below) in the documentation and/or other materials provided
* with the distribution
*
* Redistribution and use in binary form, without modification, are permitted provided
* that the following conditions are met:
*
* *       No reverse engineering, decompilation, or disassembly of this software is
* permitted with respect to any software provided in binary form.
*
* *       any redistribution and use are licensed by TI for use only with TI Devices.
*
* *       Nothing shall obligate TI to provide you with source code for the software
* licensed and provided to you in object code.
*
* If software source code is provided to you, modification and redistribution of the
* source code are permitted provided that the following conditions are met:
*
* *       any redistribution and use of the source code, including any resulting derivative
* works, are licensed by TI for use only with TI Devices.
*
* *       any redistribution and use of any object code compiled from the source code
* and any resulting derivative works, are licensed by TI for use only with TI Devices.
*
* Neither the name of Texas Instruments Incorporated nor the names of its suppliers
*
* may be used to endorse or promote products derived from this software without
* specific prior written permission.
*
* DISCLAIMER.
*
* THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
* OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
* OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
* IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
* BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
* OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
* OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
* OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/**
  *  \file      Icu.c
  *
  *  \brief    This file contains ICU MCAL driver
  *
  */

/* ========================================================================== */
/*                             Include Files                                  */
/* ========================================================================== */
#include "string.h"
#include "Icu.h"
#include "SchM_Icu.h"
#if (STD_ON == ICU_DEV_ERROR_DETECT)
#include "Det.h"
#endif
#include "Icu_Irq.h"
#include "Icu_Priv.h"

/* There are static inline functions in hw_types.h file. Map them as well */
#define ICU_START_SEC_CODE
#include "Icu_MemMap.h"
#define ICU_STOP_SEC_CODE
#include "Icu_MemMap.h"

/* ========================================================================== */
/*                           Macros & Typedefs                                */
/* ========================================================================== */

/* AUTOSAR version information check has to match definition in header file */
#if ((ICU_AR_RELEASE_MAJOR_VERSION != (4U)) || \
    (ICU_AR_RELEASE_MINOR_VERSION != (3U)) ||  \
    (ICU_AR_RELEASE_REVISION_VERSION != (1U)))
    #error "Icu: AUTOSAR Version Numbers of Icu are different!!"
#endif

/* AUTOSAR version information check has to match definition in header file */
#if ((ICU_SW_MAJOR_VERSION != (1U)) || \
    (ICU_SW_MINOR_VERSION != (3U)) ||  \
    (ICU_SW_PATCH_VERSION != (2U)))
    #error "Icu: Software Version Numbers are inconsistent!!"
#endif

/* ========================================================================== */
/*                         Structures and Enums                               */
/* ========================================================================== */

/* ========================================================================== */
/*                 Internal Function Declarations                             */
/* ========================================================================== */


/* ========================================================================== */
/*                            Global Variables                                */
/* ========================================================================== */

#define ICU_START_SEC_VAR_INIT_32
#include "Icu_MemMap.h"
/** \brief Icu driver init status */
ICU_VAR_DATA_INIT_8_SECTION volatile VAR(uint8, ICU_VAR_INIT)
    Icu_DrvStatus = ICU_STATUS_UNINIT;

#define ICU_STOP_SEC_VAR_INIT_32
#include "Icu_MemMap.h"

#define ICU_START_SEC_VAR_NO_INIT_UNSPECIFIED
#include "Icu_MemMap.h"
/** \brief ICU driver object. */
ICU_VAR_DATA_NO_INIT_UNSPECIFIED_SECTION
    VAR(Icu_ChObjType, ICU_VAR_NO_INIT) Icu_ChObj[ICU_MAX_NUM_CHANNELS];
/* Icu_ChObj is not static storage class, as this requires to be accessed by
    debug interface provided.  */
#define ICU_STOP_SEC_VAR_NO_INIT_UNSPECIFIED
#include "Icu_MemMap.h"


/* ========================================================================== */
/*                          Function Definitions                              */
/* ========================================================================== */

#define ICU_START_SEC_CODE
#include "Icu_MemMap.h"

/* Design : DES_ICU_006 */
/* Requirements : MCAL-4610, MCAL-4611, MCAL-4612, MCAL-4613, MCAL-4614,
                MCAL-4616, MCAL-4617, MCAL-4618, MCAL-4619, MCAL-4620 */
ICU_FUNC_TEXT_SECTION FUNC(void, ICU_CODE) Icu_Init(
                           P2CONST(Icu_ConfigType, AUTOMATIC, ICU_PBCFG) ConfigPtr)
{
    uint32 chIdx, chNum;
    uint32 maxCh;
    const Icu_ConfigType *IcuConfigPtr = ConfigPtr;
#if (STD_ON == ICU_PRE_COMPILE_VARIANT)
    IcuConfigPtr = &ICU_INIT_CONFIG_PC;
#endif

#if (STD_ON == ICU_DEV_ERROR_DETECT)
    if (NULL == IcuConfigPtr)
    {
        (void)Icu_reportDetError(ICU_INIT_ID, ICU_E_PARAM_POINTER);
    }
    else if (ICU_STATUS_INIT == Icu_DrvStatus)
    {
        (void)Icu_reportDetError(ICU_INIT_ID, ICU_E_ALREADY_INITIALIZED);
    }
    else
#endif /* (STD_ON == ICU_DEV_ERROR_DETECT) */
    {

        maxCh = (uint32) IcuConfigPtr->icuMaxChannel;

        for (chNum = 0U; chNum < maxCh; chNum++)
        {
#if (STD_ON == ICU_DEV_ERROR_DETECT)
            if(((&IcuConfigSet_PC)->chCfg[chNum].channelId) >=
            (uint32)ICU_MAX_NUM_CHANNELS)
            {
                /*Invalid channel */
                (void)Icu_reportDetError(ICU_INIT_ID, ICU_E_PARAM_CHANNEL);
            }
            else
#endif /* (STD_ON == ICU_DEV_ERROR_DETECT) */
            {
                for (chIdx = 0; chIdx < ICU_MAX_NUM_CHANNELS; chIdx++)
                {
                    if(chIdx == ((&IcuConfigSet_PC)->chCfg[chNum].channelId))
                    {
                        /* Reset Channel object */
                        Icu_ResetChObj(&Icu_ChObj[chIdx]);
                        /* Copy the configuration */
                        Icu_CopyConfig(&Icu_ChObj[chIdx],
                        &IcuConfigPtr->chCfg[chNum],&IcuConfigSet_PC.chCfg[chNum]);
                        /* HW Channel Init */
                        Icu_HwUnitInit(&Icu_ChObj[chIdx]);

#if (STD_ON == ICU_TIMESTAMP_API)
                        Icu_ChObj[chIdx].IsActive = FALSE;
#endif
                        break;
                    }
                }
            }
        }

#if (STD_ON == ICU_DEV_ERROR_DETECT)
        Icu_DrvStatus = ICU_STATUS_INIT;
#endif

    }

    return;
}

#if (STD_ON == ICU_DE_INIT_API)
/* Design : DES_ICU_007 */
/* Requirements : MCAL-4621, MCAL-4622, MCAL-4623, MCAL-4624, MCAL-4625,
                    MCAL-4626, MCAL-4627, MCAL-4628, MCAL-4629, MCAL-4630 */
ICU_FUNC_TEXT_SECTION FUNC(void, ICU_CODE) Icu_DeInit(void)
{
    uint32          chIdx;
#if (STD_ON == ICU_DEV_ERROR_DETECT)
    if (ICU_STATUS_UNINIT == Icu_DrvStatus)
    {
        (void)Icu_reportDetError(ICU_DEINIT_ID, ICU_E_UNINIT);
    }
    else
#endif /* (STD_ON == ICU_DEV_ERROR_DETECT) */
    {
        Icu_DrvStatus = ICU_STATUS_UNINIT;

        for (chIdx = 0; chIdx < ICU_MAX_NUM_CHANNELS; chIdx++)
        {
            /*Check if any channel is running. If yes, cannot call Deinit*/
            if(FALSE == Icu_ChObj[chIdx].IsRunning)
            {
                /*Disable and Clear Interrupts*/
                ECAPIntDisable(Icu_ChObj[chIdx].baseAddr, ECAP_INT_ALL);
                ECAPIntStatusClear(Icu_ChObj[chIdx].baseAddr, ECAP_INT_ALL);

                /* Disable CAP1-CAP4 register loads */
                ECAPCaptureLoadingDisable(Icu_ChObj[chIdx].baseAddr);

                Icu_ResetChObj(&Icu_ChObj[chIdx]);

            }
        }
    }
    return;
}
#endif /*ICU_DE_INIT_API*/

/* Design : DES_ICU_009 */
/* Requirements : MCAL-4665, MCAL-4666, MCAL-4667, MCAL-4668 */
ICU_FUNC_TEXT_SECTION FUNC(void, ICU_CODE) Icu_DisableNotification( Icu_ChannelType Channel)
{
#if (STD_ON == ICU_DEV_ERROR_DETECT)
    if (ICU_STATUS_UNINIT == Icu_DrvStatus)
    {
        (void)Icu_reportDetError(ICU_DISABLENOTIFICATION_ID, ICU_E_UNINIT);
    }
    else if (ICU_MAX_NUM_CHANNELS <= Channel)
    {
        (void)Icu_reportDetError(ICU_DISABLENOTIFICATION_ID, ICU_E_PARAM_CHANNEL);
    }
    else
#endif /* (STD_ON == ICU_DEV_ERROR_DETECT) */
    {
        Icu_ChObj[Channel].NotificationEnabled = FALSE;
        /* Disable the notification on the given channel */
    }
}

/* Design : DES_ICU_010 */
/* Requirements : MCAL-4669, MCAL-4670, MCAL-4671, MCAL-4672 */
ICU_FUNC_TEXT_SECTION FUNC(void, ICU_CODE) Icu_EnableNotification( Icu_ChannelType Channel)
{
#if (STD_ON == ICU_DEV_ERROR_DETECT)
    if (ICU_STATUS_UNINIT == Icu_DrvStatus)
    {
        (void)Icu_reportDetError(ICU_ENABLENOTIFICATION_ID, ICU_E_UNINIT);
    }
    else if (ICU_MAX_NUM_CHANNELS <= Channel)
    {
        (void)Icu_reportDetError(ICU_ENABLENOTIFICATION_ID, ICU_E_PARAM_CHANNEL);
    }
    else
#endif /* (STD_ON == ICU_DEV_ERROR_DETECT) */
    {
        Icu_ChObj[Channel].NotificationEnabled = TRUE;
        /* Enables the notification on the given channel */
    }
    return;
}

/* Design : DES_ICU_008 */
/* Requirements : MCAL-4659, MCAL-4660, MCAL-4661, MCAL-4662, MCAL-4663, MCAL-4664 */
ICU_FUNC_TEXT_SECTION FUNC(void, ICU_CODE) Icu_SetActivationCondition(
    Icu_ChannelType Channel, Icu_ActivationType Activation )
{
#if (STD_ON == ICU_DEV_ERROR_DETECT)
    if (ICU_STATUS_UNINIT == Icu_DrvStatus)
    {
        (void)Icu_reportDetError(ICU_SETACTIVATIONCONDITION_ID, ICU_E_UNINIT);
    }
    else if (ICU_MAX_NUM_CHANNELS <= Channel)
    {
        (void)Icu_reportDetError(ICU_SETACTIVATIONCONDITION_ID, ICU_E_PARAM_CHANNEL);
    }
    else if (ICU_MODE_SIGNAL_MEASUREMENT == Icu_ChObj[Channel].chCfg.measurementMode)
    {
        (void)Icu_reportDetError(ICU_SETACTIVATIONCONDITION_ID, ICU_E_PARAM_CHANNEL);
    }
    else if ((Activation != ICU_RISING_EDGE) && (Activation != ICU_FALLING_EDGE) && (Activation != ICU_BOTH_EDGES))
    {
        (void)Icu_reportDetError(ICU_SETACTIVATIONCONDITION_ID, ICU_E_PARAM_ACTIVATION);
    }
    else
#endif /* (STD_ON == ICU_DEV_ERROR_DETECT) */
    {

        Icu_ChObj[Channel].activation_edge = Activation;

#if ((ICU_EDGE_DETECT_API == STD_ON) || (ICU_SIGNAL_MEASUREMENT_API == STD_ON))
        Icu_ChObj[Channel].InputState = ICU_IDLE;
#endif
    }

    return;
}

#if (STD_ON == ICU_GET_INPUT_STATE_API)
/* Design : DES_ICU_011 */
/* Requirements : MCAL-4673, MCAL-4674, MCAL-4675, MCAL-4676, MCAL-4677,
                MCAL-4678, MCAL-4679, MCAL-4680, MCAL-4681, MCAL-4682, MCAL-4683 */
ICU_FUNC_TEXT_SECTION FUNC(Icu_InputStateType, ICU_CODE) Icu_GetInputState(
    Icu_ChannelType Channel)
{
    Icu_InputStateType retState = ICU_IDLE;
    
#if (STD_ON == ICU_DEV_ERROR_DETECT)
    if (ICU_STATUS_UNINIT == Icu_DrvStatus)
    {
        (void)Icu_reportDetError(ICU_GETINPUTSTATE_ID, ICU_E_UNINIT);
    }
    else if (ICU_MAX_NUM_CHANNELS <= Channel)
    {
        (void)Icu_reportDetError(ICU_GETINPUTSTATE_ID, ICU_E_PARAM_CHANNEL);
    }
    else if ((ICU_MODE_SIGNAL_EDGE_DETECT != Icu_ChObj[Channel].chCfg.measurementMode) &&
             (ICU_MODE_SIGNAL_MEASUREMENT != Icu_ChObj[Channel].chCfg.measurementMode))
    {
        (void)Icu_reportDetError(ICU_GETINPUTSTATE_ID, ICU_E_PARAM_CHANNEL);
    }
    else
#endif /* (STD_ON == ICU_DEV_ERROR_DETECT) */
    {
        /* Return the current input status for the channel */
        Icu_InputStateType CurrentState = Icu_ChObj[Channel].InputState;

        if (CurrentState == ICU_ACTIVE)
        {
            Icu_ChObj[Channel].InputState = ICU_IDLE;
        }

        if (Icu_ChObj[Channel].chCfg.measurementMode == ICU_MODE_SIGNAL_MEASUREMENT)
        {
            /*Check if measurement is completed before returning ICU_ACTIVE*/
            if (Icu_ChObj[Channel].IsRunning)
            {
                Icu_ChObj[Channel].InputState = CurrentState;
                retState = ICU_IDLE;
            }
            else
            {
                retState = CurrentState;
            }
        }
        else
        {
            retState = CurrentState;
        }

    }

    return retState;

}
#endif /* ICU_GET_INPUT_STATE_API*/


#if (STD_ON == ICU_EDGE_DETECT_API)
/* Design : DES_ICU_017 */
/* Requirements : MCAL-4731, MCAL-4732, MCAL-4733, MCAL-4734, MCAL-4735,
                    MCAL-4736, MCAL-4737, MCAL-4738 */
ICU_FUNC_TEXT_SECTION FUNC(void, ICU_CODE) Icu_EnableEdgeDetection(
    Icu_ChannelType Channel)
{
    uint32 baseAddr;

#if (STD_ON == ICU_DEV_ERROR_DETECT)
    if (ICU_STATUS_UNINIT == Icu_DrvStatus)
    {
        (void)Icu_reportDetError(ICU_ENABLEEDGEDETECTION_ID, ICU_E_UNINIT);
    }
    else if (ICU_MAX_NUM_CHANNELS <= Channel)
    {
        (void)Icu_reportDetError(ICU_ENABLEEDGEDETECTION_ID, ICU_E_PARAM_CHANNEL);
    }
    else if (ICU_MODE_SIGNAL_EDGE_DETECT != Icu_ChObj[Channel].chCfg.measurementMode)
    {
        (void)Icu_reportDetError(ICU_ENABLEEDGEDETECTION_ID, ICU_E_PARAM_CHANNEL);
    }
    else
#endif /* (STD_ON == ICU_DEV_ERROR_DETECT) */
    {
        baseAddr = Icu_ChObj[Channel].baseAddr;

        Icu_ConfigEcap(baseAddr, Icu_ChObj[Channel].activation_edge, ICU_DELTA_MODE, TRUE);
        Icu_ChObj[Channel].IsRunning = TRUE;

    }
}

/* Design : DES_ICU_018 */
/* Requirements : MCAL-4739, MCAL-4740, MCAL-4741, MCAL-4742, MCAL-4743, MCAL-4744 */
ICU_FUNC_TEXT_SECTION FUNC(void, ICU_CODE) Icu_DisableEdgeDetection(
    Icu_ChannelType Channel)
{
    uint32 baseAddr;

#if (STD_ON == ICU_DEV_ERROR_DETECT)
    if (ICU_STATUS_UNINIT == Icu_DrvStatus)
    {
        (void)Icu_reportDetError(ICU_DISABLEEDGEDETECTION_ID, ICU_E_UNINIT);
    }
    else if (ICU_MAX_NUM_CHANNELS <= Channel)
    {
        (void)Icu_reportDetError(ICU_DISABLEEDGEDETECTION_ID, ICU_E_PARAM_CHANNEL);
    }
    else if (ICU_MODE_SIGNAL_EDGE_DETECT != Icu_ChObj[Channel].chCfg.measurementMode)
    {
        (void)Icu_reportDetError(ICU_DISABLEEDGEDETECTION_ID, ICU_E_PARAM_CHANNEL);
    }
    else
#endif /* (STD_ON == ICU_DEV_ERROR_DETECT) */
    {
        baseAddr = Icu_ChObj[Channel].baseAddr;

        /*Disable Interrupts*/
        ECAPIntDisable(baseAddr, ECAP_INT_ALL);
        ECAPIntStatusClear(baseAddr, ECAP_INT_ALL);

        /* Disable CAP1-CAP4 register loads */
        ECAPCaptureLoadingDisable(baseAddr);
        Icu_ChObj[Channel].IsRunning = FALSE;
    }
}

#endif /*ICU_EDGE_DETECT_API*/

#if (STD_ON == ICU_TIMESTAMP_API)
/* Design : DES_ICU_012 */
/* Requirements : MCAL-4684, MCAL-4685, MCAL-4686, MCAL-4687, MCAL-4688,
                MCAL-4689, MCAL-4690, MCAL-4691, MCAL-4692, MCAL-4693, MCAL-4694,
                MCAL-4695, MCAL-4696, MCAL-4697, MCAL-4198, MCAL-4199 */
ICU_FUNC_TEXT_SECTION FUNC(void, ICU_CODE) Icu_StartTimestamp(
    Icu_ChannelType Channel, Icu_ValueType* BufferPtr, uint16 BufferSize, uint16 NotifyInterval)
{
    uint32 baseAddr;

#if (STD_ON == ICU_DEV_ERROR_DETECT)
    if (ICU_STATUS_UNINIT == Icu_DrvStatus)
    {
        (void)Icu_reportDetError(ICU_STARTTIMESTAMP_ID, ICU_E_UNINIT);
    }
    else if (ICU_MAX_NUM_CHANNELS <= Channel)
    {
        (void)Icu_reportDetError(ICU_STARTTIMESTAMP_ID, ICU_E_PARAM_CHANNEL);
    }
    else if (ICU_MODE_TIMESTAMP != Icu_ChObj[Channel].chCfg.measurementMode)
    {
        (void)Icu_reportDetError(ICU_STARTTIMESTAMP_ID, ICU_E_PARAM_CHANNEL);
    }
    else if (NULL_PTR == BufferPtr)
    {
        (void)Icu_reportDetError(ICU_STARTTIMESTAMP_ID, ICU_E_PARAM_POINTER);
    }
    else if (0U == BufferSize)
    {
        (void)Icu_reportDetError(ICU_STARTTIMESTAMP_ID, ICU_E_PARAM_BUFFER_SIZE);
    }
    else
#endif /* (STD_ON == ICU_DEV_ERROR_DETECT) */
    {
        baseAddr = Icu_ChObj[Channel].baseAddr;

        Icu_TimeStamp_Init(Channel, BufferPtr, BufferSize, NotifyInterval);

        Icu_ConfigEcap(baseAddr, Icu_ChObj[Channel].activation_edge, ICU_ABSOLUTE_MODE, TRUE);

        Icu_ChObj[Channel].IsRunning = TRUE;
        Icu_ChObj[Channel].IsActive = TRUE;
    }

    return;
}

/* Design : DES_ICU_013 */
/* Requirements : MCAL-4700, MCAL-4701, MCAL-4702, MCAL-4703,
                MCAL-4704, MCAL-4705, MCAL-4706, MCAL-4707 */
ICU_FUNC_TEXT_SECTION FUNC(void, ICU_CODE) Icu_StopTimestamp(
    Icu_ChannelType Channel)
{
    uint32 baseAddr;

#if (STD_ON == ICU_DEV_ERROR_DETECT)
    if (ICU_STATUS_UNINIT == Icu_DrvStatus)
    {
        (void)Icu_reportDetError(ICU_STOPTIMESTAMP_ID, ICU_E_UNINIT);
    }
    else if (ICU_MAX_NUM_CHANNELS <= Channel)
    {
        (void)Icu_reportDetError(ICU_STOPTIMESTAMP_ID, ICU_E_PARAM_CHANNEL);
    }
    else if (ICU_MODE_TIMESTAMP != Icu_ChObj[Channel].chCfg.measurementMode)
    {
        (void)Icu_reportDetError(ICU_STOPTIMESTAMP_ID, ICU_E_PARAM_CHANNEL);
    }
    else
#endif /* (STD_ON == ICU_DEV_ERROR_DETECT) */
    {
        if (FALSE == Icu_ChObj[Channel].IsActive && FALSE == Icu_ChObj[Channel].IsRunning)
        {
            (void)Icu_reportDetRuntimeError(ICU_STOPTIMESTAMP_ID, ICU_E_NOT_STARTED);
        }
        else
        {
            baseAddr = Icu_ChObj[Channel].baseAddr;

            ECAPIntDisable(baseAddr, ECAP_INT_ALL);
            ECAPIntStatusClear(baseAddr, ECAP_INT_ALL);

            /* Disable CAP1-CAP4 register loads */
            ECAPCaptureLoadingDisable(baseAddr);

            Icu_TimeStamp_Clear(Channel);

            Icu_ChObj[Channel].IsRunning = FALSE;
            Icu_ChObj[Channel].IsActive = FALSE;
        }
    }
    return;
}

/* Design : DES_ICU_014 */
/* Requirements : MCAL-4708, MCAL-4709, MCAL-4710, MCAL-4710, MCAL-4711,
                MCAL-4712, MCAL-4713, MCAL-47104, MCAL-4715, MCAL-4716 */
ICU_FUNC_TEXT_SECTION FUNC(Icu_IndexType, ICU_CODE) Icu_GetTimeStampIndex(
    Icu_ChannelType Channel)
{
    Icu_IndexType index = 0;

#if (STD_ON == ICU_DEV_ERROR_DETECT)
    if (ICU_STATUS_UNINIT == Icu_DrvStatus)
    {
        (void)Icu_reportDetError(ICU_STOPTIMESTAMP_ID, ICU_E_UNINIT);
    }
    else if (ICU_MAX_NUM_CHANNELS <= Channel)
    {
        (void)Icu_reportDetError(ICU_STOPTIMESTAMP_ID, ICU_E_PARAM_CHANNEL);
    }
    else if (ICU_MODE_TIMESTAMP != Icu_ChObj[Channel].chCfg.measurementMode)
    {
        (void)Icu_reportDetError(ICU_STOPTIMESTAMP_ID, ICU_E_PARAM_CHANNEL);
    }
    else
#endif
    {
        if ((Icu_ChObj[Channel].IsActive))
        {
            index = Icu_ChObj[Channel].NextTimeStampIndex;
        }
        else
        {
            /*return 0*/
        }
    }

    return index;
}


#endif /* ICU_TIMESTAMP_API*/

#if (STD_ON == ICU_EDGE_COUNT_API)
/* Design : DES_ICU_015 */
/* Requirements : MCAL-4717, MCAL-4719, MCAL-4720, MCAL-4718, MCAL-4721, MCAL-4722 */
ICU_FUNC_TEXT_SECTION FUNC(void, ICU_CODE) Icu_ResetEdgeCount(
    Icu_ChannelType Channel)
{
#if (STD_ON == ICU_DEV_ERROR_DETECT)
    if (ICU_STATUS_UNINIT == Icu_DrvStatus)
    {
        (void)Icu_reportDetError(ICU_RESETEDGECOUNT_ID, ICU_E_UNINIT);
    }
    else if (ICU_MAX_NUM_CHANNELS <= Channel)
    {
        (void)Icu_reportDetError(ICU_RESETEDGECOUNT_ID, ICU_E_PARAM_CHANNEL);
    }
    else if (ICU_MODE_EDGE_COUNTER != Icu_ChObj[Channel].chCfg.measurementMode)
    {
        (void)Icu_reportDetError(ICU_RESETEDGECOUNT_ID, ICU_E_PARAM_CHANNEL);
    }
    else
#endif /* (STD_ON == ICU_DEV_ERROR_DETECT) */
    {
        Icu_ChObj[Channel].EdgeCounter = 0;
    }
}

/* Design : DES_ICU_016 */
/* Requirements : MCAL-4723, MCAL-4724, MCAL-4725, MCAL-4726, MCAL-4727,
                MCAL-4728, MCAL-4729, MCAL-4730 */
ICU_FUNC_TEXT_SECTION FUNC(void, ICU_CODE) Icu_EnableEdgeCount(
    Icu_ChannelType Channel)
{
    uint32 baseAddr;

#if (STD_ON == ICU_DEV_ERROR_DETECT)
    if (ICU_STATUS_UNINIT == Icu_DrvStatus)
    {
        (void)Icu_reportDetError(ICU_ENABLEEDGECOUNT_ID, ICU_E_UNINIT);
    }
    else if (ICU_MAX_NUM_CHANNELS <= Channel)
    {
        (void)Icu_reportDetError(ICU_ENABLEEDGECOUNT_ID, ICU_E_PARAM_CHANNEL);
    }
    else if (ICU_MODE_EDGE_COUNTER != Icu_ChObj[Channel].chCfg.measurementMode)
    {
        (void)Icu_reportDetError(ICU_ENABLEEDGECOUNT_ID, ICU_E_PARAM_CHANNEL);
    }
    else
#endif /* (STD_ON == ICU_DEV_ERROR_DETECT) */
    {
        baseAddr = Icu_ChObj[Channel].baseAddr;

        Icu_ConfigEcap(baseAddr, Icu_ChObj[Channel].activation_edge, ICU_DELTA_MODE, TRUE);

        Icu_ChObj[Channel].IsRunning = TRUE;

    }
}

/* Design : DES_ICU_019 */
/* Requirements : MCAL-4745, MCAL-4746, MCAL-4747, MCAL-4748, MCAL-4749, MCAL-4750 */
ICU_FUNC_TEXT_SECTION FUNC(void, ICU_CODE) Icu_DisableEdgeCount(
    Icu_ChannelType Channel)
{
    uint32 baseAddr;

#if (STD_ON == ICU_DEV_ERROR_DETECT)
    if (ICU_STATUS_UNINIT == Icu_DrvStatus)
    {
        (void)Icu_reportDetError(ICU_DISABLEEDGECOUNT_ID, ICU_E_UNINIT);
    }
    else if (ICU_MAX_NUM_CHANNELS <= Channel)
    {
        (void)Icu_reportDetError(ICU_DISABLEEDGECOUNT_ID, ICU_E_PARAM_CHANNEL);
    }
    else if (ICU_MODE_EDGE_COUNTER != Icu_ChObj[Channel].chCfg.measurementMode)
    {
        (void)Icu_reportDetError(ICU_DISABLEEDGECOUNT_ID, ICU_E_PARAM_CHANNEL);
    }
    else
#endif /* (STD_ON == ICU_DEV_ERROR_DETECT) */
    {
        baseAddr = Icu_ChObj[Channel].baseAddr;

        ECAPIntDisable(baseAddr, ECAP_INT_ALL);
        ECAPIntStatusClear(baseAddr, ECAP_INT_ALL);

        /* Disable CAP1-CAP4 register loads */
        ECAPCaptureLoadingDisable(baseAddr);

        Icu_ChObj[Channel].IsRunning = FALSE;
    }
}

/* Design : DES_ICU_020 */
/* Requirements : MCAL-4751, MCAL-4752, MCAL-4753, MCAL-4754,
                MCAL-4755, MCAL-4756, MCAL-4757 */
ICU_FUNC_TEXT_SECTION FUNC(Icu_EdgeNumberType, ICU_CODE) Icu_GetEdgeNumbers(
    Icu_ChannelType Channel)
{
    Icu_EdgeNumberType edgecount = 0;

#if (STD_ON == ICU_DEV_ERROR_DETECT)
    if (ICU_STATUS_UNINIT == Icu_DrvStatus)
    {
        (void)Icu_reportDetError(ICU_GETEDGENUMBERS_ID, ICU_E_UNINIT);
    }
    else if (ICU_MAX_NUM_CHANNELS <= Channel)
    {
        (void)Icu_reportDetError(ICU_GETEDGENUMBERS_ID, ICU_E_PARAM_CHANNEL);
    }
    else if (ICU_MODE_EDGE_COUNTER != Icu_ChObj[Channel].chCfg.measurementMode)
    {
        (void)Icu_reportDetError(ICU_GETEDGENUMBERS_ID, ICU_E_PARAM_CHANNEL);
    }
    else
#endif /* (STD_ON == ICU_DEV_ERROR_DETECT) */
    {
        edgecount= Icu_ChObj[Channel].EdgeCounter;
    }

    return edgecount;
}

#endif /*ICU_EDGE_COUNT_API*/


#if (STD_ON == ICU_SIGNAL_MEASUREMENT_API)
/* Design : DES_ICU_021 */
/* Requirements : MCAL-4758, MCAL-4759, MCAL-4760, MCAL-4761, MCAL-4762,
                MCAL-4763, MCAL-4764, MCAL-4765 */
ICU_FUNC_TEXT_SECTION FUNC(void, ICU_CODE) Icu_StartSignalMeasurement(
    Icu_ChannelType Channel)
{
    uint32 baseAddr;

#if (STD_ON == ICU_DEV_ERROR_DETECT)
    if (ICU_STATUS_UNINIT == Icu_DrvStatus)
    {
        (void)Icu_reportDetError(ICU_STARTSIGNALMEASUREMENT_ID, ICU_E_UNINIT);
    }
    else if (ICU_MAX_NUM_CHANNELS <= Channel)
    {
        (void)Icu_reportDetError(ICU_STARTSIGNALMEASUREMENT_ID, ICU_E_PARAM_CHANNEL);
    }
    else if (ICU_MODE_SIGNAL_MEASUREMENT != Icu_ChObj[Channel].chCfg.measurementMode)
    {
        (void)Icu_reportDetError(ICU_STARTSIGNALMEASUREMENT_ID, ICU_E_PARAM_CHANNEL);
    }
    else
#endif /* (STD_ON == ICU_DEV_ERROR_DETECT) */
    {
        baseAddr = Icu_ChObj[Channel].baseAddr;

        Icu_ChObj[Channel].InputState = ICU_IDLE;

        Icu_SignalMeasurement_Init(Channel);

        Icu_ConfigEcap(baseAddr, ICU_BOTH_EDGES, ICU_DELTA_MODE, FALSE);

        Icu_ChObj[Channel].IsRunning = TRUE;

    }

    return;

}

/* Design : DES_ICU_022 */
/* Requirements : MCAL-4766, MCAL-4767, MCAL-4768, MCAL-4769,
                    MCAL-4770, MCAL-4771, MCAL-4772 */
ICU_FUNC_TEXT_SECTION FUNC(void, ICU_CODE) Icu_StopSignalMeasurement(
    Icu_ChannelType Channel)
{
    uint32 baseAddr;
    Icu_ValueType highTime, lowTime, period;

#if (STD_ON == ICU_DEV_ERROR_DETECT)
    if (ICU_STATUS_UNINIT == Icu_DrvStatus)
    {
        (void)Icu_reportDetError(ICU_STOPSIGNALMEASUREMENT_ID, ICU_E_UNINIT);
    }
    else if (ICU_MAX_NUM_CHANNELS <= Channel)
    {
        (void)Icu_reportDetError(ICU_STOPSIGNALMEASUREMENT_ID, ICU_E_PARAM_CHANNEL);
    }
    else if (ICU_MODE_SIGNAL_MEASUREMENT != Icu_ChObj[Channel].chCfg.measurementMode)
    {
        (void)Icu_reportDetError(ICU_STOPSIGNALMEASUREMENT_ID, ICU_E_PARAM_CHANNEL);
    }
    else
#endif /* (STD_ON == ICU_DEV_ERROR_DETECT) */
    {
        baseAddr = Icu_ChObj[Channel].baseAddr;

        ECAPIntDisable(baseAddr, ECAP_INT_ALL);
        ECAPIntStatusClear(baseAddr, ECAP_INT_ALL);

        /* Disable CAP1-CAP4 register loads */
        ECAPCaptureLoadingDisable(baseAddr);

        Icu_ChObj[Channel].cap1 = ECAPTimeStampRead(baseAddr, ECAP_CAPTURE_EVENT_1);
        Icu_ChObj[Channel].cap2 = ECAPTimeStampRead(baseAddr, ECAP_CAPTURE_EVENT_2);
        Icu_ChObj[Channel].cap3 = ECAPTimeStampRead(baseAddr, ECAP_CAPTURE_EVENT_3);
        Icu_ChObj[Channel].cap4 = ECAPTimeStampRead(baseAddr, ECAP_CAPTURE_EVENT_4);


        if ((Icu_ChObj[Channel].cap1 == 0U) ||
            (Icu_ChObj[Channel].cap2 == 0U) ||
            (Icu_ChObj[Channel].cap3 == 0U) ||
            (Icu_ChObj[Channel].cap4 == 0U))
        {
            Icu_ChObj[Channel].DutyAcquired = FALSE;
            Icu_ChObj[Channel].PeriodAcquired = FALSE;
        }
        else
        {
            uint32 clkFreq_Mhz = Icu_ChObj[Channel].chCfg.instanceClkMHz;

            highTime = Icu_ChObj[Channel].cap2 / clkFreq_Mhz;
            lowTime = Icu_ChObj[Channel].cap3 / clkFreq_Mhz;
            period = highTime + lowTime;

            Icu_ChObj[Channel].HighTime = highTime;
            Icu_ChObj[Channel].LowTime = lowTime;
            Icu_ChObj[Channel].Period = period;
            Icu_ChObj[Channel].DutyCycle.PeriodTime = period;
            Icu_ChObj[Channel].DutyCycle.ActiveTime = highTime;
            Icu_ChObj[Channel].DutyAcquired = TRUE;
            Icu_ChObj[Channel].PeriodAcquired = TRUE;
        }

        Icu_ChObj[Channel].IsRunning = FALSE;
    }

    return;
}

#endif /* ICU_SIGNAL_MEASUREMENT_API*/


#if (STD_ON == ICU_GET_TIME_ELAPSED_API)
/* Design : DES_ICU_023 */
/* Requirements : MCAL-4773, MCAL-4774, MCAL-4775, MCAL-4776, MCAL-4777,
                    MCAL-4778, MCAL-4779, MCAL-4780, MCAL-4781, MCAL-4782,
                    MCAL-4783, MCAL-4784 */
ICU_FUNC_TEXT_SECTION FUNC(Icu_ValueType, ICU_CODE) Icu_GetTimeElapsed (
        Icu_ChannelType Channel)
{
    Icu_ValueType elapsedTime = 0U;

#if (STD_ON == ICU_DEV_ERROR_DETECT)
    if (ICU_STATUS_UNINIT == Icu_DrvStatus)
    {
        (void)Icu_reportDetError(ICU_GETTIMEELAPSED_ID, ICU_E_UNINIT);
    }
    else if (ICU_MAX_NUM_CHANNELS <= Channel)
    {
        (void)Icu_reportDetError(ICU_GETTIMEELAPSED_ID, ICU_E_PARAM_CHANNEL);
    }
    else if (ICU_MODE_SIGNAL_MEASUREMENT != Icu_ChObj[Channel].chCfg.measurementMode)
    {
        (void)Icu_reportDetError(ICU_GETTIMEELAPSED_ID, ICU_E_PARAM_CHANNEL);
    }
    else
#endif /* (STD_ON == ICU_DEV_ERROR_DETECT) */
    {
        if (Icu_ChObj[Channel].PeriodAcquired == TRUE)
        {
            switch (Icu_ChObj[Channel].chCfg.signalMeasurementProperty)
            {
                case ICU_DUTY_CYCLE:
                    elapsedTime = Icu_ChObj[Channel].Period;
                    break;
                case ICU_PERIOD_TIME:
                    elapsedTime = Icu_ChObj[Channel].Period;
                    break;
                case ICU_HIGH_TIME:
                    elapsedTime = Icu_ChObj[Channel].HighTime;
                    break;
                case ICU_LOW_TIME:
                    elapsedTime = Icu_ChObj[Channel].LowTime;
                    break;
                default:
                    elapsedTime = 0U;
                    break;
            }
            Icu_ChObj[Channel].PeriodAcquired = FALSE;
        }
        else
        {
            elapsedTime = 0U;
        }

    }
    return elapsedTime;
}

#endif

#if (STD_ON == ICU_GET_DUTY_CYCLE_VALUES_API)
/* Design : DES_ICU_024 */
/* Requirements : MCAL-4785, MCAL-4786, MCAL-4787, MCAL-4788, MCAL-4789, MCAL-4790,
                MCAL-4791, MCAL-4792, MCAL-4793, MCAL-4794 */
FUNC (void, ICU_CODE) Icu_GetDutyCycleValues( Icu_ChannelType Channel,
                    Icu_DutyCycleType* DutyCycleValues )
{
#if (STD_ON == ICU_DEV_ERROR_DETECT)
    if (ICU_STATUS_UNINIT == Icu_DrvStatus)
    {
        (void)Icu_reportDetError(ICU_GETDUTYCYCLEVALUES_ID, ICU_E_UNINIT);
    }
    else if (ICU_MAX_NUM_CHANNELS <= Channel)
    {
        (void)Icu_reportDetError(ICU_GETDUTYCYCLEVALUES_ID, ICU_E_PARAM_CHANNEL);
    }
    else if (ICU_MODE_SIGNAL_MEASUREMENT != Icu_ChObj[Channel].chCfg.measurementMode)
    {
        (void)Icu_reportDetError(ICU_GETDUTYCYCLEVALUES_ID, ICU_E_PARAM_CHANNEL);
    }
    else if (NULL_PTR == DutyCycleValues)
    {
        (void)Icu_reportDetError(ICU_GETDUTYCYCLEVALUES_ID, ICU_E_PARAM_POINTER);
    }
    else
#endif /* (STD_ON == ICU_DEV_ERROR_DETECT) */
    {
        if (Icu_ChObj[Channel].DutyAcquired == TRUE)
        {
            DutyCycleValues->PeriodTime = Icu_ChObj[Channel].DutyCycle.PeriodTime;
            DutyCycleValues->ActiveTime = Icu_ChObj[Channel].DutyCycle.ActiveTime;

            Icu_ChObj[Channel].DutyAcquired = FALSE;
        }
        else
        {
            DutyCycleValues->PeriodTime = 0U;
            DutyCycleValues->ActiveTime = 0U;
        }
    }
}

#endif


#if (STD_ON == ICU_GET_VERSION_INFO_API)
/* Design : DES_ICU_025 */
/* Requirements : MCAL-4795, MCAL-4796 */
ICU_FUNC_TEXT_SECTION FUNC(void, ICU_CODE) Icu_GetVersionInfo(
            P2VAR(Std_VersionInfoType, AUTOMATIC, ICU_APPL_DATA) VersionInfoPtr)
{
#if (STD_ON == ICU_DEV_ERROR_DETECT)
    if (NULL_PTR == VersionInfoPtr)
    {
        Icu_reportDetError(ICU_GETVERSIONINFO_ID, ICU_E_PARAM_VINFO);
    }
    else
#endif
    {
        VersionInfoPtr->vendorID         = ICU_VENDOR_ID;
        VersionInfoPtr->moduleID         = ICU_MODULE_ID;
        VersionInfoPtr->sw_major_version = (uint8) ICU_SW_MAJOR_VERSION;
        VersionInfoPtr->sw_minor_version = (uint8) ICU_SW_MINOR_VERSION;
        VersionInfoPtr->sw_patch_version = (uint8) ICU_SW_PATCH_VERSION;
    }

    return;
}
#endif /* ICU_GET_VERSION_INFO_API*/
