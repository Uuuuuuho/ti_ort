/*
*
* Copyright (c) 2020 Texas Instruments Incorporated
*
* All rights reserved not granted herein.
*
* Limited License.
*
* Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
* license under copyrights and patents it now or hereafter owns or controls to make,
* have made, use, import, offer to sell and sell ("Utilize") this software subject to the
* terms herein.  With respect to the foregoing patent license, such license is granted
* solely to the extent that any such patent is necessary to Utilize the software alone.
* The patent license shall not apply to any combinations which include this software,
* other than combinations with devices manufactured by or for TI ("TI Devices").
* No hardware patent is licensed hereunder.
*
* Redistributions must preserve existing copyright notices and reproduce this license
* (including the above copyright notice and the disclaimer and (if applicable) source
* code license limitations below) in the documentation and/or other materials provided
* with the distribution
*
* Redistribution and use in binary form, without modification, are permitted provided
* that the following conditions are met:
*
* *       No reverse engineering, decompilation, or disassembly of this software is
* permitted with respect to any software provided in binary form.
*
* *       any redistribution and use are licensed by TI for use only with TI Devices.
*
* *       Nothing shall obligate TI to provide you with source code for the software
* licensed and provided to you in object code.
*
* If software source code is provided to you, modification and redistribution of the
* source code are permitted provided that the following conditions are met:
*
* *       any redistribution and use of the source code, including any resulting derivative
* works, are licensed by TI for use only with TI Devices.
*
* *       any redistribution and use of any object code compiled from the source code
* and any resulting derivative works, are licensed by TI for use only with TI Devices.
*
* Neither the name of Texas Instruments Incorporated nor the names of its suppliers
*
* may be used to endorse or promote products derived from this software without
* specific prior written permission.
*
* DISCLAIMER.
*
* THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
* OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
* OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
* IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
* BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
* OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
* OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
* OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/**
 *  \file     Icu_Irq.c
 *
 *  \brief    This file contains the ISR implementation of ICU MCAL driver
 *
 */

/* ========================================================================== */
/*                             Include Files                                  */
/* ========================================================================== */
#include "Icu_Priv.h"
#include "Icu_Irq.h"

/* ========================================================================== */
/*                           Macros & Typedefs                                */
/* ========================================================================== */

/* None */

/* ========================================================================== */
/*                         Structures and Enums                               */
/* ========================================================================== */

/* None */

/* ========================================================================== */
/*                 Internal Function Declarations                             */
/* ========================================================================== */

/* None */

/* ========================================================================== */
/*                            Global Variables                                */
/* ========================================================================== */

/* None */

/* ========================================================================== */
/*                          Function Definitions                              */
/* ========================================================================== */

#define ICU_START_SEC_ISR_CODE
#include "Icu_MemMap.h"

ICU_ISR_TEXT_SECTION void Icu_ch0Notify(void)
{

#if (STD_ON == ICU_DEV_ERROR_DETECT)
    uint32 baseAddr = Icu_GetBaseAddr(ICU_CHANNEL0);
    if (ICU_STATUS_INIT != Icu_DrvStatus)
    {
        /* Disable and Clear Interrupt */
        ECAPIntDisable(baseAddr, ECAP_INT_ALL);
        ECAPIntStatusClear(baseAddr, ECAP_INT_ALL);
    }
    else
#endif
    {
        Icu_ChannelISR(ICU_CHANNEL0);
    }
}

ICU_ISR_TEXT_SECTION void Icu_ch1Notify(void)
{

#if (STD_ON == ICU_DEV_ERROR_DETECT)
    uint32 baseAddr = Icu_GetBaseAddr(ICU_CHANNEL1);
    if (ICU_STATUS_INIT != Icu_DrvStatus)
    {
        /* Disable and Clear Interrupt */
        ECAPIntDisable(baseAddr, ECAP_INT_ALL);
        ECAPIntStatusClear(baseAddr, ECAP_INT_ALL);
    }
    else
#endif
    {
        Icu_ChannelISR(ICU_CHANNEL1);
    }
}

ICU_ISR_TEXT_SECTION void Icu_ch2Notify(void)
{

#if (STD_ON == ICU_DEV_ERROR_DETECT)
    uint32 baseAddr = Icu_GetBaseAddr(ICU_CHANNEL2);
    if (ICU_STATUS_INIT != Icu_DrvStatus)
    {
        /* Disable and Clear Interrupt */
        ECAPIntDisable(baseAddr, ECAP_INT_ALL);
        ECAPIntStatusClear(baseAddr, ECAP_INT_ALL);
    }
    else
#endif
    {
        Icu_ChannelISR(ICU_CHANNEL2);
    }
}


#define PWM_STOP_SEC_ISR_CODE
#include "Pwm_MemMap.h"
