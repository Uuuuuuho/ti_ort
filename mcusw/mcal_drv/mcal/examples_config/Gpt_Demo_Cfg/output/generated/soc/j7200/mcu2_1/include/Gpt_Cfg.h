/*
*
* Copyright (c) 2019 Texas Instruments Incorporated
*
* All rights reserved not granted herein.
*
* Limited License.
*
* Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
* license under copyrights and patents it now or hereafter owns or controls to make,
* have made, use, import, offer to sell and sell ("Utilize") this software subject to the
* terms herein.  With respect to the foregoing patent license, such license is granted
* solely to the extent that any such patent is necessary to Utilize the software alone.
* The patent license shall not apply to any combinations which include this software,
* other than combinations with devices manufactured by or for TI ("TI Devices").
* No hardware patent is licensed hereunder.
*
* Redistributions must preserve existing copyright notices and reproduce this license
* (including the above copyright notice and the disclaimer and (if applicable) source
* code license limitations below) in the documentation and/or other materials provided
* with the distribution
*
* Redistribution and use in binary form, without modification, are permitted provided
* that the following conditions are met:
*
* *       No reverse engineering, decompilation, or disassembly of this software is
* permitted with respect to any software provided in binary form.
*
* *       any redistribution and use are licensed by TI for use only with TI Devices.
*
* *       Nothing shall obligate TI to provide you with source code for the software
* licensed and provided to you in object code.
*
* If software source code is provided to you, modification and redistribution of the
* source code are permitted provided that the following conditions are met:
*
* *       any redistribution and use of the source code, including any resulting derivative
* works, are licensed by TI for use only with TI Devices.
*
* *       any redistribution and use of any object code compiled from the source code
* and any resulting derivative works, are licensed by TI for use only with TI Devices.
*
* Neither the name of Texas Instruments Incorporated nor the names of its suppliers
*
* may be used to endorse or promote products derived from this software without
* specific prior written permission.
*
* DISCLAIMER.
*
* THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
* OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
* OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
* IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
* BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
* OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
* OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
* OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/**
 *  \file     Gpt_Cfg.h
 *
 *  \brief    This file contains generated pre compile configurations
 *            for GPT MCAL driver
 *
 */
 
/******************************************************************************
    Project         : Bug_Fix_MCAL_5019
    Date            : 2020-09-16 20:26:53
    SW Ver          : 1.2.1
    Module Rele Ver : AUTOSAR 4.3.1 0

    This file is generated by EB Tresos
    Do not modify this file,otherwise the software may behave in unexpected way.
******************************************************************************/

/**
 *  \defgroup MCAL_GPT_CFG GPT Configuration
 *
 *  This files defines GPT MCAL configuration structures
 *  @{
 */

#ifndef GPT_CFG_H_
#define GPT_CFG_H_

/* Generic requirements covered mapping */
/*
 * Requirements : MCAL-1945
 */
/*
 * Design : DES_GPT_012
 */

/* ========================================================================== */
/*                             Include Files                                  */
/* ========================================================================== */

#include "Dem.h"
#include "Os.h"


#ifdef __cplusplus
extern "C" {
#endif

/* ========================================================================== */
/*                           Macros & Typedefs                                */
/* ========================================================================== */
/*
 * Requirements : MCAL-2120, MCAL-1945
 */
/*
 * Design : DES_GPT_014
 */
/**
*  \brief GPT Pre-Compile Build Variant flag.
*   STD_ON for VariantPreCompile / STD_OFF for VariantPostBuild
*/
#define GPT_PRE_COMPILE_VARIANT                (STD_OFF)

/*
 * Requirements : MCAL-1946
 */
/*
 * Design : DES_GPT_012
 */
/** \brief Enable/disable GPT dev detect error */
#define GPT_DEV_ERROR_DETECT                   (STD_ON)
/** \brief ISR type */
#define GPT_ISR_TYPE                           (GPT_ISR_CAT1)

/** \brief Enable/disable wakeup source in wakeup related APIs */
#define GPT_REPORT_WAKEUP_SOURCE               (STD_ON)
/**
 *  \name Pre-Compile Switches for API Services
 *  @{
 */
/** \brief Enable/disable GPT get version info API */
#define GPT_VERSION_INFO_API                   (STD_ON)
/*
 * Requirements : MCAL-1964
 */
/*
 * Design : DES_GPT_022
 */
/** \brief Enable/disable GPT deinit API */
#define GPT_DEINIT_API                         (STD_ON)
/** \brief Enable/disable GPT get time elapsed API */
/*
 * Requirements : MCAL-1966
 */
/*
 * Design : DES_GPT_023
 */
#define GPT_TIME_ELAPSED_API                   (STD_ON)
/** \brief Enable/disable GPT time remaining API */
/*
 * Requirements : MCAL-1967
 */
/*
 * Design : DES_GPT_024
 */
#define GPT_TIME_REMAINING_API                 (STD_ON)
/*
 * Requirements : MCAL-1965
 */
/*
 * Design : DES_GPT_028
 */
/** \brief Enable/disable GPT enable/disable GPT API */
#define GPT_ENABLE_DISABLE_NOTIFICATION_API    (STD_ON)
/*
 * Requirements : MCAL-1968, MCAL-1969,
 */
/*
 * Design : DES_GPT_030, DES_GPT_031, DES_GPT_032,
 *          DES_GPT_033
 */
/** \brief Enable/disable GPT wakeup functionality API */
#define GPT_WAKEUP_FUNCTIONALITY_API           (STD_ON)
/* @} */

/*
 * Requirements : MCAL-1952, MCAL-1953
 */
/*
 * Design : DES_GPT_012, DES_GPT_018
 */
/** \brief No. of channels configured for GPT driver */
#define GPT_MAX_CHANNELS                        (5U)

/** \brief Macro for enabling predefined timers
 *        This is in case to disable GPT Predef Timers if timers can not be
 *        supported by hardware reasons.
 */
#define GPT_PREDEF_TIMER_TYPE                  (GPT_PREDEF_TIMER_DISABLED)

/** \brief: Specifies the grade of enabling the GPT Predef Timers with 1us
 *         tick duration */
#define GPT_PREDEF_TIMER_1US_ENABLING_GRADE    (GPT_PREDEF_TIMER_1US_DISABLED)


/** \brief Counter ID for counter used to count wait ticks */
#define GPT_OS_COUNTER_ID                      ((CounterType)OsCounter_0)

/**
 *  \brief ETH timeout.
 *   Each tick is 31.25us (for 32K Counter). Wait for 5s which comes to
 *   below value
 */
#define GPT_TIMEOUT_DURATION                   (32000U)

/**
 *  \name GPT DEM Error codes to report
 *
 *  Pre-compile switches for enabling/disabling DEM events
 *  @{
 */
#define DemConf_DemEventParameter_GPT_DEM_NO_EVENT (0xFFFFU)
#define GPT_DEM_NO_EVENT DemConf_DemEventParameter_GPT_DEM_NO_EVENT

#ifndef GPT_E_HARDWARE_ERROR
/** \brief Hardware failed */
#define GPT_E_HARDWARE_ERROR                   (DemConf_DemEventParameter_GPT_E_HARDWARE_ERROR)
#endif

/* @} */

/** \brief Enable/disable GPT register read back API */
#define GPT_REGISTER_READBACK_API              (STD_ON)

/**
*  \brief Channel ID
*   Configured channel ID(s)
*/
#define GptConf_GptChannelConfiguration_TIMER1          (11U)
/**< Channel identifiers */
#define GptConf_GptChannelConfiguration_MCU_TIMER6          (6U)
/**< Channel identifiers */
#define GptConf_GptChannelConfiguration_MCU_TIMER9          (9U)
/**< Channel identifiers */
#define GptConf_GptChannelConfiguration_TIMER5          (15U)
/**< Channel identifiers */
#define GptConf_GptChannelConfiguration_TIMER7          (17U)
/**< Channel identifiers */

/* ========================================================================== */
/*                         Structures and Enums                               */
/* ========================================================================== */
/**
* \brief GPT Configuration
*
*/
extern const struct Gpt_ConfigType_s GptChannelConfigSet;

/** \brief GPT PC Configuration struct declaration */
extern const struct Gpt_ChannelConfigType_PC_s Gpt_ChannelConfig_PC[GPT_MAX_CHANNELS];

/* ========================================================================== */
/*                          Function Declarations                             */
/* ========================================================================== */

/* None */

#ifdef __cplusplus
}
#endif


        
        
        
        
        

#endif  /* #ifndef GPT_CFG_H_ */

/* @} */

