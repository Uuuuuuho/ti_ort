/*
*
* Copyright (c) 2020 Texas Instruments Incorporated
*
* All rights reserved not granted herein.
*
* Limited License.
*
* Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
* license under copyrights and patents it now or hereafter owns or controls to make,
* have made, use, import, offer to sell and sell ("Utilize") this software subject to the
* terms herein.  With respect to the foregoing patent license, such license is granted
* solely to the extent that any such patent is necessary to Utilize the software alone.
* The patent license shall not apply to any combinations which include this software,
* other than combinations with devices manufactured by or for TI ("TI Devices").
* No hardware patent is licensed hereunder.
*
* Redistributions must preserve existing copyright notices and reproduce this license
* (including the above copyright notice and the disclaimer and (if applicable) source
* code license limitations below) in the documentation and/or other materials provided
* with the distribution
*
* Redistribution and use in binary form, without modification, are permitted provided
* that the following conditions are met:
*
* *       No reverse engineering, decompilation, or disassembly of this software is
* permitted with respect to any software provided in binary form.
*
* *       any redistribution and use are licensed by TI for use only with TI Devices.
*
* *       Nothing shall obligate TI to provide you with source code for the software
* licensed and provided to you in object code.
*
* If software source code is provided to you, modification and redistribution of the
* source code are permitted provided that the following conditions are met:
*
* *       any redistribution and use of the source code, including any resulting derivative
* works, are licensed by TI for use only with TI Devices.
*
* *       any redistribution and use of any object code compiled from the source code
* and any resulting derivative works, are licensed by TI for use only with TI Devices.
*
* Neither the name of Texas Instruments Incorporated nor the names of its suppliers
*
* may be used to endorse or promote products derived from this software without
* specific prior written permission.
*
* DISCLAIMER.
*
* THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
* OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
* OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
* IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
* BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
* OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
* OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
* OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/**
 *  \file     Fls_Cfg.c
 *
 *  \brief    This file contains generated pre compile configuration file
 *            for FLS MCAL driver
 */

  /*****************************************************************************
    Project: FLS_1
    Date   : 2020-09-15 13:57:01
    This file is generated by EB Tresos
    Do not modify this file, otherwise the software may behave in unexpected way
 ******************************************************************************/

/*******************************************************************************
 *  INCLUDES
 ******************************************************************************/
#include "Fls.h"


/*******************************************************************************
 *  VERSION CHECK
 ******************************************************************************/
#if ((FLS_SW_MAJOR_VERSION != (1U)) || (FLS_SW_MINOR_VERSION != (3U)))
  #error "Version numbers of Fls_Cfg.c and Fls.h are inconsistent!"
#endif
/*******************************************************************************
 *  LOCAL CONSTANT MACROS
 ******************************************************************************/

/*******************************************************************************
 *  LOCAL FUNCTION MACROS
 ******************************************************************************/

/*******************************************************************************
 *  LOCAL DATA TYPES AND STRUCTURES
 ******************************************************************************/

/*******************************************************************************
 *  LOCAL DATA PROTOTYPES
 ******************************************************************************/

/*******************************************************************************
 *  GLOBAL DATA
 ******************************************************************************/
#define  FLS_START_SEC_CONFIG_DATA
#include "Fls_MemMap.h"
/* generation of runtime configuration       */

#ifdef __cplusplus
extern "C" {
#endif




extern void Fee_JobEndNotification(void);

extern void Fee_JobErrorNotification(void);

CONST(struct Fls_ConfigType_s, FLS_CONFIG_DATA) FlsConfigSet =
{
    .Fls_JobEndNotification = Fee_JobEndNotification,
    .Fls_JobErrorNotification = Fee_JobErrorNotification,
    .maxReadNormalMode = 768U,
    .maxWriteNormalMode = 768U,
    .sectorList =
    {
            [0] =
            {
                .numberOfSectors = 16384U,
                .sectorPageSize = 256U,
                .sectorSize = 4096U,
                .sectorStartaddress = 1342177280U,
            },
        },
    .dacEnable = FALSE,
    .xipEnable = FALSE,
    .ospiClkSpeed = 166666666U,
    .dtrEnable = TRUE,
    .phyEnable = FALSE,
};

#ifdef __cplusplus
}
#endif

#define  FLS_STOP_SEC_CONFIG_DATA
#include "Fls_MemMap.h"

/*******************************************************************************
 *  END OF FILE: Fls_Cfg.c
 ******************************************************************************/
