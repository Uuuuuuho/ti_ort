/*
*
* Copyright (c) 2019 Texas Instruments Incorporated
*
* All rights reserved not granted herein.
*
* Limited License.
*
* Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
* license under copyrights and patents it now or hereafter owns or controls to make,
* have made, use, import, offer to sell and sell ("Utilize") this software subject to the
* terms herein.  With respect to the foregoing patent license, such license is granted
* solely to the extent that any such patent is necessary to Utilize the software alone.
* The patent license shall not apply to any combinations which include this software,
* other than combinations with devices manufactured by or for TI ("TI Devices").
* No hardware patent is licensed hereunder.
*
* Redistributions must preserve existing copyright notices and reproduce this license
* (including the above copyright notice and the disclaimer and (if applicable) source
* code license limitations below) in the documentation and/or other materials provided
* with the distribution
*
* Redistribution and use in binary form, without modification, are permitted provided
* that the following conditions are met:
*
* *       No reverse engineering, decompilation, or disassembly of this software is
* permitted with respect to any software provided in binary form.
*
* *       any redistribution and use are licensed by TI for use only with TI Devices.
*
* *       Nothing shall obligate TI to provide you with source code for the software
* licensed and provided to you in object code.
*
* If software source code is provided to you, modification and redistribution of the
* source code are permitted provided that the following conditions are met:
*
* *       any redistribution and use of the source code, including any resulting derivative
* works, are licensed by TI for use only with TI Devices.
*
* *       any redistribution and use of any object code compiled from the source code
* and any resulting derivative works, are licensed by TI for use only with TI Devices.
*
* Neither the name of Texas Instruments Incorporated nor the names of its suppliers
*
* may be used to endorse or promote products derived from this software without
* specific prior written permission.
*
* DISCLAIMER.
*
* THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
* OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
* OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
* IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
* BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
* OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
* OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
* OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/**
 *  \file     Eth.h
 *
 *  \brief    This file contains the interface of the Ethernet driver.
 */

/**
 *  \defgroup MCAL_ETH_API Ethernet Driver API
 *
 *  The Ethernet Driver provides low-level access to the two-port Gigabit
 *  Ethernet Switch (MCU_CPSW0) hardware in the Jacinto 7 family.  The
 *  first port (Port 0) is called host port and has a CPPI interface
 *  that interconnects the CPSW peripheral with the rest of the SoC.  The
 *  second port (Port 1) is a MAC port which can be configured in RGMII
 *  or RMII mode.
 *
 *  The Ethernet Driver implements the standardized interface described in
 *  the AUTOSAR Release 4.2.1 Specification of the Ethernet Driver
 *  (AUTOSAR_SWS_EthDriver,  Document ID 430).
 *
 *  The Ethernet Driver is part of the Communication Stack in the AUTOSAR
 *  Basic Software (BSW).
 *
 *  \sa MCAL_ETH_CFG
 *  \sa MCAL_ETH_IRQ
 *  @{
 */

/*
 * Below are the generic requirements met by this Eth driver which can't be
 * mapped to a specific piece of code
 */
/*
 * Design:       ETH_DesignId_001
 * Requirements: MCAL-1519, MCAL-1520, MCAL-1522, MCAL-1523,
 *               MCAL-1525, MCAL-1526, MCAL-1531, MCAL-1614,
 *               MCAL-1635, MCAL-1636, MCAL-1703, MCAL-981
 */

#ifndef ETH_H_
#define ETH_H_

/* ========================================================================== */
/*                             Include Files                                  */
/* ========================================================================== */

/*
 * Design:       ETH_DesignId_001
 * Requirements: MCAL-1635
 *
 * Note: As per "Header file structure" in Ethernet Driver spec:
 * Eth.h --> Eth_Types.h --> Eth_GeneralTypes.h --> ComStack_Types.h
 */
#include <Eth_Types.h>
#include <Eth_Packet.h>

#ifdef __cplusplus
extern "C"
{
#endif

/* ========================================================================== */
/*                           Macros & Typedefs                                */
/* ========================================================================== */

/**
 *  \name Eth Driver Module SW Version Info
 *
 *  Definitions for the Eth Driver version used for compatibility checks
 *  @{
 */
/*
 * Design:       ETH_DesignId_001
 * Requirements: MCAL-1522
 */
/** \brief Driver Implementation Major Version */
#define ETH_SW_MAJOR_VERSION            (1U)
/** \brief Driver Implementation Minor Version */
#define ETH_SW_MINOR_VERSION            (3U)
/** \brief Driver Implementation Patch Version */
#define ETH_SW_PATCH_VERSION            (2U)
/* @} */

/**
 *  \name Eth Driver Module AUTOSAR Version Info
 *
 *  Definitions for the Etj Driver AUTOSAR version used for compatibility checks
 *  @{
 */
/** \brief AUTOSAR Major version specification implemented by Eth Driver */
#define ETH_AR_RELEASE_MAJOR_VERSION    (4U)
/** \brief AUTOSAR Minor  version specification implemented by Eth Driver */
#define ETH_AR_RELEASE_MINOR_VERSION    (3U)
/** \brief AUTOSAR Patch version specification implemented by Eth Driver */
#define ETH_AR_RELEASE_REVISION_VERSION (1U)
/* @} */

/**
 *  \name Eth Driver ID Info
 *  @{
 */
/** \brief Texas Instruments Vendor ID */
#define ETH_VENDOR_ID                   ((uint16) 44U)
/** \brief Eth Driver Module ID */
#define ETH_MODULE_ID                   ((uint16) 88U)
/** \brief Eth Driver Instance ID */
#define ETH_INSTANCE_ID                 ((uint8) 0U)
/* @} */

/**
 *  \name Eth Controller names
 *
 *  Symbolic names for the EthControllerId.
 *  @{
 */
/*
 * Design:       ETH_DesignId_001
 * Requirements: MCAL-1519
 */
/** \brief Eth controller ID 0  */
#define ETH_CONTROLLER_ID_0             (0U)

/* @} */

/**
 *  \anchor Eth_ErrorCodes
 *  \name Eth Error Codes
 *
 *  Error codes returned by Eth functions
 *  @{
 */
/*
 * Design:       ETH_DesignId_003
 * Requirements: MCAL-1530
 */
#ifndef ETH_E_INV_CTRL_IDX
/** \brief Invalid controller index */
#define ETH_E_INV_CTRL_IDX              ((uint8) 0x01U)
#endif
#ifndef ETH_E_UNINIT
/** \brief Eth module was not initialized */
#define ETH_E_UNINIT                    ((uint8) 0x02U)
#endif
#ifndef ETH_E_PARAM_POINTER
/** \brief Invalid pointer in parameter list */
#define ETH_E_PARAM_POINTER             ((uint8) 0x03U)
#endif
#ifndef ETH_E_INV_PARAM
/** \brief Invalid parameter */
#define ETH_E_INV_PARAM                 ((uint8) 0x04U)
#endif
#ifndef ETH_E_INV_MODE
/** \brief Invalid mode */
#define ETH_E_INV_MODE                  ((uint8) 0x05U)
#endif
#ifndef ETH_E_VIRTMAC_APIMISMATCH
/** \brief Mismatch in API version between Eth Driver and ethernet firmware  */
#define ETH_E_VIRTMAC_APIMISMATCH       ((uint8) 0x07U)
#endif
#ifndef ETH_E_VIRTMAC_RPCCMDFAILED
/** \brief Virtual MAC RPC command failed */
#define ETH_E_VIRTMAC_RPCCMDFAILED      ((uint8) 0x08U)
#endif
#ifndef ETH_E_VIRTMAC_UNSUPPORTECLIENTNOTIFY
/** \brief Ethernet MCAL RPC client received unhandled notify from ethernet firmware server */
#define ETH_E_VIRTMAC_UNSUPPORTECLIENTNOTIFY ((uint8) 0x09U)
#endif
#ifndef ETH_E_VIRTMAC_UNSUPPORTEDSRVCMD
/** \brief Rpc Command sent by client not supported by server */
#define ETH_E_VIRTMAC_UNSUPPORTEDSRVCMD       ((uint8) 0x0AU)
#endif
#ifndef ETH_E_BUSY
/** \brief Device or resource is busy */
#define ETH_E_BUSY                      ((uint8) 0x0BU)
#endif

/* @} */

/**
 *  \addtogroup MCAL_ETH_CFG Eth Configuration
 *  @{
 */

/**
 *  \anchor Eth_ServiceIds
 *  \name Eth Service Ids
 *
 *  The Service Id is used to identify the source of an error when
 *  reported through the Det_ReportError() function.
 *  @{
 */
/** \brief Eth_Init() API Service ID */
#define ETH_SID_INIT                    ((uint8) 0x01U)

/** \brief Eth_SetControllerMode() API Service ID */
#define ETH_SID_SET_CONTROLLER_MODE     ((uint8) 0x03U)

/** \brief Eth_GetControllerMode() API Service ID */
#define ETH_SID_GET_CONTROLLER_MODE     ((uint8) 0x04U)

/** \brief Eth_WriteMii() API Service ID */
#define ETH_SID_WRITE_MII               ((uint8) 0x05U)

/** \brief Eth_ReadMii() API Service ID */
#define ETH_SID_READ_MII                ((uint8) 0x06U)

/** \brief Eth_GetPhysAddr() API Service ID */
#define ETH_SID_GET_PHYS_ADDR           ((uint8) 0x08U)

/** \brief Eth_ProvideTxBuffer() API Service ID */
#define ETH_SID_PROVIDE_TX_BUFFER       ((uint8) 0x09U)

/** \brief Eth_MainFunction() API Service ID */
/* REVISIT: Same as below? */
#define ETH_SID_MAIN_FUNCTION           ((uint8) 0x0AU)

/** \brief Eth_Transmit() API Service ID */
#define ETH_SID_TRANSMIT                ((uint8) 0x0BU)

/** \brief Eth_Receive() API Service ID */
#define ETH_SID_RECEIVE                 ((uint8) 0x0CU)

/** \brief Eth_TxConfirmation() API Service ID */
#define ETH_SID_TX_CONFIRMATION         ((uint8) 0x0EU)

/** \brief Eth_GetVersionInfo() API Service ID */
#define ETH_SID_GET_VERSION_INFO        ((uint8) 0x0FU)

/** \brief Eth_RxIrqHdlr_<CtrlIdx>() API Service ID */
#define ETH_SID_RX_IRQ_HDLR             ((uint8) 0x10U)

/** \brief Eth_TxIrqHdlr_<CtrlIdx>() API Service ID */
#define ETH_SID_TX_IRQ_HDLR             ((uint8) 0x11U)

/** \brief Eth_UpdatePhysAddrFilter() API Service ID */
#define ETH_SID_UPDATE_PHYS_ADDR_FILTER ((uint8) 0x12U)

/** \brief Eth_SetPhysAddr() API Service ID */
#define ETH_SID_SET_PHYS_ADDR           ((uint8) 0x13U)

/** \brief Eth_GetCounterValues() API Service ID */
#define ETH_SID_GET_COUNTER_VALUES       ((uint8) 0x14U)

/** \brief Eth_GetRxStats() API Service ID */
#define ETH_SID_GET_RX_STATS         ((uint8) 0x15U)

/** \brief Eth_GetTxStats() API Service ID */
#define ETH_SID_GET_TX_STATS         ((uint8) 0x1CU)

/** \brief Eth_GetTxErrorCounterValues() API Service ID */
#define ETH_SID_GET_TXERROR_COUNTERVALUES     ((uint8) 0x1DU)

/** \brief Eth_GetCurrentTime() API Service ID */
#define ETH_SID_GET_CURRENT_TIME        ((uint8) 0x16U)

/** \brief Eth_EnableEgressTimeStamp() API Service ID */
#define ETH_SID_ENABLE_EGRESS_TIMESTAMP ((uint8) 0x17U)

/** \brief Eth_GetEgressTimeStamp() API Service ID */
#define ETH_SID_GET_EGRESS_TIMESTAMP    ((uint8) 0x18U)

/** \brief Eth_GetIngressTimeStamp() API Service ID */
#define ETH_SID_GET_INGRESS_TIMESTAMP   ((uint8) 0x19U)

/** \brief Eth_SetCorrectionTime() API Service ID */
#define ETH_SID_SET_CORRECTION_TIME     ((uint8) 0x1AU)

/** \brief Eth_SetGlobalTime() API Service ID */
#define ETH_SID_SET_GLOBAL_TIME         ((uint8) 0x1BU)

/** \brief Eth_DispatchVirtmacInit() API Service ID */
#define ETH_SID_DISPATCH_VIRTMAC_INIT  ((uint8) 0x1CU)

/** \brief Eth_DispatchVirtmacDeinit() API Service ID */
#define ETH_SID_DISPATCH_VIRTMAC_DEINIT  ((uint8) 0x1DU)

/** \brief Eth_NotifyVirtmacMsgRecv() API Service ID */
#define ETH_SID_NOTIFY_VIRTMAC_MSGRECV ((uint8) 0x1EU)

/** \brief Eth_DispatchVirtmacSubscribeAllTraffic() API Service ID */
#define ETH_SID_DISPATCH_VIRTMAC_SUBSCRIBE_ALLTRAFFIC   ((uint8) 0x1FU)

/** \brief Eth_DispatchVirtmacUnsubscribeAllTraffic() API Service ID */
#define ETH_SID_DISPATCH_VIRTMAC_UNSUBSCRIBE_ALLTRAFFIC ((uint8) 0x20U)

/** \brief Eth_DispatchVirtmacSubscribeDstMac() API Service ID */
#define ETH_SID_DISPATCH_VIRTMAC_SUBSCRIBE_DSTMAC       ((uint8) 0x21U)

/** \brief Eth_DispatchVirtmacUnsubscribeDstMac() API Service ID */
#define ETH_SID_DISPATCH_VIRTMAC_UNSUBSCRIBE_DSTMAC     ((uint8) 0x22U)

/** \brief Eth_DispatchVirtmacIPv4MacAddrAssociate() API Service ID */
#define ETH_SID_DISPATCH_VIRTMAC_IPV4_MACADDR_ASSOCIATE    ((uint8) 0x23U)

/** \brief Eth_DispatchVirtmacIPv4MacAddrDisassociate() API Service ID */
#define ETH_SID_DISPATCH_VIRTMAC_IPV4_MACADDR_DISASSOCIATE ((uint8) 0x24U)

/** \brief Eth_DispatchVirtmacAddUnicastMacAddr() API Service ID */
#define ETH_SID_DISPATCH_VIRTMAC_ADD_UNICAST_MACADDR ((uint8) 0x25U)

/** \brief Eth_DispatchVirtmacAddMcastMacAddr() API Service ID */
#define ETH_SID_DISPATCH_VIRTMAC_ADD_MCAST_MACADDR ((uint8) 0x26U)

/** \brief Eth_DispatchVirtmacDelMacAddr() API Service ID */
#define ETH_SID_DISPATCH_VIRTMAC_DEL_MACADDR ((uint8) 0x27U)

/** \brief Eth_DispatchVirtmacAddVlan() API Service ID */
#define ETH_SID_DISPATCH_VIRTMAC_ADD_VLAN ((uint8) 0x28U)

/** \brief Eth_DispatchVirtmacDelVlan() API Service ID */
#define ETH_SID_DISPATCH_VIRTMAC_DEL_VLAN ((uint8) 0x29U)

/** \brief Eth_SendCustomNotify() API Service ID */
#define ETH_SID_SEND_CUSTOM_NOTIFY ((uint8) 0x2AU)

/** \brief Eth_TransmitNoCpy() API Service ID */
#define ETH_SID_TRANSMIT_NO_COPY        ((uint8) 0x2BU)

/** \brief Eth_RetrieveRxReadyQ() API Service ID */
#define ETH_SID_RETRIEVE_RX_READY_QUEUE ((uint8) 0x2CU)

/** \brief Eth_SubmitRxFreeQ() API Service ID */
#define ETH_SID_SUBMIT_RX_FREE_QUEUE    ((uint8) 0x2DU)

/** \brief Eth_SubmitTxReadyQ() API Service ID */
#define ETH_SID_SUBMIT_TX_READY_QUEUE   ((uint8) 0x2EU)

/** \brief Eth_RetrieveTxDoneQ() API Service ID */
#define ETH_SID_RETRIEVE_TX_DONE_QUEUE  ((uint8) 0x2FU)

/** \brief Eth_VirtMacRpcInit() API Service ID */
#define ETH_SID_VIRTMAC_RPC_INIT   ((uint8) 0x2BU)

/* @} */
/* @} */

/* ========================================================================== */
/*                         Structures and Enums                               */
/* ========================================================================== */

/* None */

/* ========================================================================== */
/*                          Function Declarations                             */
/* ========================================================================== */

/**
 *  \brief This function initializes the driver.
 *
 *  \verbatim
 *  Service name      : Eth_Init
 *  Syntax            : void Eth_Init(
 *                          const Eth_ConfigType* CfgPtr
 *                      )
 *  Mode              : Supervisor Mode (Privileged Mode)
 *  Service ID[hex]   : 0x01
 *  Sync/Async        : Synchronous
 *  Reentrancy        : Non Reentrant
 *  Parameters (in)   : CfPtr. Points to the implementation specific structure
 *  Parameters (inout): None
 *  Parameters (out)  : None
 *  Return value      : None
 *  Description       : Initializes the Ethernet Driver.
 *  \endverbatim
 */
FUNC(void, ETH_CODE)
Eth_Init(P2CONST(Eth_ConfigType, AUTOMATIC, ETH_PBCFG) CfgPtr);

/**
 *  \brief This function enables / disables the indexed controller.
 *
 *  \verbatim
 *  Service name      : Eth_SetControllerMode
 *  Syntax            : Std_ReturnType Eth_SetControllerMode(
 *                          uint8 CtrlIdx,
 *                          Eth_ModeType CtrlMode
 *                      )
 *  Mode              : Supervisor Mode (Privileged Mode)
 *  Service ID[hex]   : 0x03
 *  Sync/Async        : Asynchronous
 *  Reentrancy        : Non Reentrant
 *  Parameters (in)   : CtrlIdx. Index of the controller within the context of the
 *                               Ethernet Driver
 *                      CtrlMode.
 *                        ETH_MODE_DOWN: disable the controller
 *                        ETH_MODE_ACTIVE: enable the controller
 *  Parameters (inout): None
 *  Parameters (out)  : None
 *  Return value      : Std_ReturnType
 *                        E_OK: success
 *                        E_NOT_OK: controller mode could not be changed
 *  Description       : Enables / disables the indexed controller.
 *  \endverbatim
 */
FUNC(Std_ReturnType, ETH_CODE)
Eth_SetControllerMode(uint8 CtrlIdx,
                      Eth_ModeType CtrlMode);

/**
 *  \brief This function obtains the state of the indexed controller.
 *
 *  \verbatim
 *  Service name      : Eth_GetControllerMode
 *  Syntax            : Std_ReturnType Eth_GetControllerMode(
 *                          uint8 CtrlIdx,
 *                          Eth_ModeType *CtrlModePtr
 *                      )
 *  Mode              : User Mode (Non-Privileged Mode)
 *  Service ID[hex]   : 0x04
 *  Sync/Async        : Synchronous
 *  Reentrancy        : Non Reentrant
 *  Parameters (in)   : CtrlIdx. Index of the controller within the context of the
 *                               Ethernet Driver
 *  Parameters (inout): CtrlModePtr
 *                        ETH_MODE_DOWN: the controller is disabled
 *                        ETH_MODE_ACTIVE: the controller is enabled
 *  Parameters (out)  : None
 *  Return value      : Std_ReturnType
 *                        E_OK: success
 *                        E_NOT_OK: controller mode could not be obtained
 *  Description       : Obtains the state of the indexed controller.
 *  \endverbatim
 */
FUNC(Std_ReturnType, ETH_CODE)
Eth_GetControllerMode(uint8 CtrlIdx,
                      P2VAR(Eth_ModeType, AUTOMATIC, ETH_APPL_DATA) CtrlModePtr);

/**
 *  \brief This function obtains the physical source address used by the indexed
 *         controller
 *
 *  \verbatim
 *  Service name      : Eth_GetPhysAddr
 *  Syntax            : void Eth_GetPhysAddr(
 *                          uint8 CtrlIdx,
 *                          uint8 *PhysAddrPtr
 *                      )
 *  Mode              : User Mode (Non-Privileged Mode)
 *  Service ID[hex]   : 0x08
 *  Sync/Async        : Synchronous
 *  Reentrancy        : Non Reentrant
 *  Parameters (in)   : CtrlIdx. Index of the controller within the context of the
 *                               Ethernet Driver
 *  Parameters (inout):
 *  Parameters (out)  : PhysAddrPtr. Physical source address (MAC address) in network
 *                                   byte order
 *  Return value      : None
 *  Description       : Obtains the physical source address used by the indexed
 *                      controller.
 *  \endverbatim
 */
FUNC(void, ETH_CODE)
Eth_GetPhysAddr(uint8 CtrlIdx,
                P2VAR(uint8, AUTOMATIC, ETH_APPL_DATA) PhysAddrPtr);

/**
 *  \brief This function sets the physical source address used by the indexed
 *         controller.
 *
 *  \verbatim
 *  Service name      : Eth_SetPhysAddr
 *  Syntax            : void Eth_SetPhysAddr(
 *                          uint8 CtrlIdx,
 *                          const uint8* PhysAddrPtr
 *                      )
 *  Mode              : Supervisor Mode (Privileged Mode)
 *  Service ID[hex]   : 0x13
 *  Sync/Async        : Synchronous
 *  Reentrancy        : Non Reentrant for the same CtrlIdx, reentrant for different
 *  Parameters (in)   : CtrlIdx. Index of the controller within the context of the
 *                               Ethernet Driver
 *                      PhysAddrPtr. Pointer to memory containing the physical source
 *                                   address (MAC address) in network byte order
 *  Parameters (inout): None
 *  Parameters (out)  : None
 *  Return value      : None
 *  Description       : Sets the physical source address used by the indexed
 *                      controller.
 *  \endverbatim
 */
FUNC(void, ETH_CODE)
Eth_SetPhysAddr(uint8 CtrlIdx,
                P2CONST(uint8, AUTOMATIC, ETH_APPL_DATA) PhysAddrPtr);

#if (STD_ON == ETH_UPDATE_PHYS_ADDR_FILTER_API)
/**
 *  \brief This function updates the physical source address to / from the indexed
 *         controller filter.
 *
 *  \verbatim
 *  Service name      : Eth_UpdatePhysAddrFilter
 *  Syntax            : Std_ReturnType Eth_UpdatePhysAddrFilter(
 *                          uint8 CtrlIdx,
 *                          uint8* PhysAddrPtr,
 *                          Eth_FilterActionType Action
 *                      )
 *  Mode              : Supervisor Mode (Privileged Mode)
 *  Service ID[hex]   : 0x12
 *  Sync/Async        : Synchronous
 *  Reentrancy        : Non Reentrant for the same CtrlIdx, reentrant for different
 *  Parameters (in)   : CtrlIdx. Index of the controller within the context of the
 *                               Ethernet Driver
 *                      PhysAddrPtr. Pointer to memory containing the physical source
 *                                   address (MAC address) in network byte order
 *                      Action. Add or remove the address from the Ethernet controllers
 *                              filter
 *  Parameters (inout): None
 *  Parameters (out)  : None
 *  Return value      : Std_ReturnType
 *                        E_OK: filter was successfully changed
 *                        E_NOT_OK: filter could not be changed
 *  Description       : Updates the physical source address to/from the indexed ontroller
 *                      filter. If the Ethernet Controller is not capable to do the
 *                      filtering, the software has to do this.
 *  \endverbatim
 */
FUNC(Std_ReturnType, ETH_CODE)
Eth_UpdatePhysAddrFilter(uint8 CtrlIdx,
                         P2VAR(uint8, AUTOMATIC, ETH_APPL_DATA) PhysAddrPtr,
                         Eth_FilterActionType Action);
#endif /* (STD_ON == ETH_UPDATE_PHYS_ADDR_FILTER_API) */

#if (STD_ON == ETH_ENABLE_MII_API)
/**
 *  \brief This function configures a transceiver register or triggers a
 *         function offered by the receiver Service.
 *
 *  \verbatim
 *  Service name      : Eth_WriteMii
 *  Syntax            : Std_ReturnType Eth_WriteMii(
 *                          uint8 CtrlIdx,
 *                          uint8 TrcvIdx,
 *                          uint8 RegIdx,
 *                          uint16 RegVal
 *                      )
 *  Mode              : Supervisor Mode (Privileged Mode)
 *  Service ID[hex]   : 0x05
 *  Sync/Async        : Asynchronous
 *  Reentrancy        : Non Reentrant
 *  Parameters (in)   : CtrlIdx. Index of the controller within the context of the
 *                               Ethernet Driver
 *                      TrcvIdx. Index of the transceiver on the MII
 *                      RegIdx.Index of the transceiver register on the MII
 *                      RegVal. Value to be written into the indexed registerNone
 *  Parameters (inout): None
 *  Parameters (out)  : None
 *  Return value      : Std_ReturnType
 *                        E_OK: Service accepted
 *                        E_NOT_OK: Service denied
 *                        ETH_E_NO_ACCESS: Ethernet transceiver access failure
 *  Description       : Configures a transceiver register or triggers a function
 *                      offered by the receiver.
 *  \endverbatim
 */
FUNC(Std_ReturnType, ETH_CODE)
Eth_WriteMii(uint8 CtrlIdx,
             uint8 TrcvIdx,
             uint8 RegIdx,
             uint16 RegVal);

/**
 *  \brief This function reads a transceiver register.
 *
 *  \verbatim
 *  Service name      : Eth_ReadMii
 *  Syntax            : Std_ReturnType Eth_ReadMii(
 *                          uint8 CtrlIdx,
 *                          uint8 TrcvIdx,
 *                          uint8 RegIdx,
 *                          uint16* RegValPtr
 *                      )
 *  Mode              : Supervisor Mode (Privileged Mode)
 *  Service ID[hex]   : 0x06
 *  Sync/Async        : Asynchronous
 *  Reentrancy        : Non Reentrant
 *  Parameters (in)   : CtrlIdx. Index of the controller within the context of the
 *                               Ethernet Driver
 *                      TrcvIdx. Index of the transceiver on the MII
 *                      RegIdx. Index of the transceiver register on the MII
 *                      RegVal. Value to be written into the indexed register
 *  Parameters (inout): None
 *  Parameters (out)  : RegValPtr. Filled with the register content of the indexed
 *                                 register
 *  Return value      : Std_ReturnType
 *                        E_OK: Service accepted
 *                        E_NOT_OK: Service denied
 *                        ETH_E_NO_ACCESS: Ethernet transceiver access failure
 *  Description       : Reads a transceiver register.
 *  \endverbatim
 */
FUNC(Std_ReturnType, ETH_CODE)
Eth_ReadMii(uint8 CtrlIdx,
            uint8 TrcvIdx,
            uint8 RegIdx,
            P2VAR(uint16, AUTOMATIC, ETH_APPL_DATA) RegValPtr);
#endif /* (STD_ON == ETH_ENABLE_MII_API) */

#if (STD_ON == ETH_GET_COUNTER_VALUES_API)
/**
 *  \brief This function reads a list with counter values of the
 *         corresponding controller.
 *
 *  \verbatim
 *  Service name      : Eth_GetCounterValues
 *  Syntax            : Std_ReturnType Eth_GetCounterValues(uint8 CtrlIdx,
 *                                                          Eth_CounterType* CounterPtr);
 *  Mode              : Supervisor Mode (Privileged Mode)
 *  Service ID[hex]   : 0x14
 *  Sync/Async        : Synchronous
 *  Reentrancy        : Non Reentrant
 *  Parameters (in)   : CtrlIdx. Index of the controller within the context of the
 *                               Ethernet Driver
 *  Parameters (out)  : CounterPtr. counter values according to IETF RFC 1757, RFC 1643 and
 *                      RFC 2233. Refer to the \ref Eth_CounterType.
 *  Return value      : Std_ReturnType
 *                        E_OK: success
 *                        E_NOT_OK: drop counter could not be obtained
 *  Description       : Reads a list with drop counter values of the corresponding controller.
 *                      The meaning of these values is described at Eth_CounterType
 *  \endverbatim
 */
FUNC(Std_ReturnType, ETH_CODE)
Eth_GetCounterValues(uint8 CtrlIdx,
                     Eth_CounterType* CounterPtr);
#endif /* (STD_ON == ETH_GET_COUNTER_VALUES_API) */

#if (STD_ON == ETH_GET_RX_STATS_API)
/**
 *  \brief This function reads a list with RX statistics values of the
 *          corresponding controller.
 *
 *  \verbatim
 *  Service name      : Eth_GetRxStats
 *  Syntax            : Std_ReturnType Eth_GetRxStats(
 *                          uint8 CtrlIdx,
 *                          Eth_RxStatsType* RxStats
 *                      )
 *  Mode              : Supervisor Mode (Privileged Mode)
 *  Service ID[hex]   : 0x15
 *  Sync/Async        : Synchronous
 *  Reentrancy        : Non Reentrant
 *  Parameters (in)   : CtrlIdx. Index of the controller within the context of the
 *                               Ethernet Driver
 *  Parameters (inout): None
 *  Parameters (out)  : RxStats. List of values according to IETF RFC 2819 (Remote Network
 *                      Monitoring Management Information Base). Refer to the \ref Eth_RxStatsType.
 *  Return value      : Std_ReturnType
 *                        E_OK: success
 *                        E_NOT_OK: drop counter could not be obtained
 *  Description       :
 *  \endverbatim
 */
FUNC(Std_ReturnType, ETH_CODE)
Eth_GetRxStats(uint8 CtrlIdx,
               P2VAR(Eth_RxStatsType, AUTOMATIC, ETH_APPL_DATA) RxStats);
#endif /* (STD_ON == ETH_GET_RX_STATS_API) */

#if (STD_ON == ETH_GET_TX_STATS_API)
/**
 *  \brief This function reads a list with TX statistics values of the
 *          corresponding controller.
 *
 *  \verbatim
 *  Service name      : Eth_GetTxStats
 *  Syntax            : Std_ReturnType Eth_GetTxStats(
 *                          uint8 CtrlIdx,
 *                          Eth_TxStatsType* TxStats
 *                      )
 *  Mode              : Supervisor Mode (Privileged Mode)
 *  Service ID[hex]   : 0x15
 *  Sync/Async        : Synchronous
 *  Reentrancy        : Non Reentrant
 *  Parameters (in)   : CtrlIdx. Index of the controller within the context of the
 *                               Ethernet Driver
 *  Parameters (inout): None
 *  Parameters (out)  : TxStats. List of values according to IETF RFC 2819 (Remote Network
 *                      Monitoring Management Information Base). Refer to the \ref Eth_TxStatsType.
 *  Return value      : Std_ReturnType
 *                        E_OK: success
 *                        E_NOT_OK: drop counter could not be obtained
 *  Description       :
 *  \endverbatim
 */
FUNC(Std_ReturnType, ETH_CODE)
Eth_GetTxStats(uint8 CtrlIdx,
               P2VAR(Eth_TxStatsType, AUTOMATIC, ETH_APPL_DATA) TxStats);
#endif /* (STD_ON == ETH_GET_TX_STATS_API) */

#if (STD_ON == ETH_GET_TX_ERROR_COUNTERSVALUES_API)
/**
 *  \brief This function reads a list of values to read statistic error counter values
 *          for transmission for corresponding controller.
 *
 *  \verbatim
 *  Service name      : Eth_GetTxErrorCounterValues
 *  Syntax            : Eth_GetTxErrorCounterValues(uint8 CtrlIdx,
 *                            Eth_TxErrorCounterValuesType *TxErrorCounterValues)
 *  Mode              : Supervisor Mode (Privileged Mode)
 *  Service ID[hex]   : 0x15
 *  Sync/Async        : Synchronous
 *  Reentrancy        : Non Reentrant
 *  Parameters (in)   : CtrlIdx. Index of the controller within the context of the
 *                               Ethernet Driver
 *  Parameters (inout): None
 *  Parameters (out)  : TxErrorCounterValues. List of values to read statistic error counter values
 *                      for transmission. Refer to the \ref Eth_TxErrorCounterValuesType.
 *  Return value      : Std_ReturnType
 *                        E_OK: success
 *                        E_NOT_OK: drop counter could not be obtained
 *  Description       :
 *  \endverbatim
 */
FUNC(Std_ReturnType, ETH_CODE)
Eth_GetTxErrorCounterValues(uint8 CtrlIdx,
               P2VAR(Eth_TxErrorCounterValuesType, AUTOMATIC, ETH_APPL_DATA) TxErrorCounterValues);
#endif

#if (STD_ON == ETH_GLOBALTIMESUPPORT_API)
/**
 *  \brief This function returns a time value out of the HW registers.
 *
 *  \verbatim
 *  Service name      : Eth_GetCurrentTime
 *  Syntax            : Std_ReturnType Eth_GetCurrentTime(
 *                          uint8 CtrlIdx,
 *                          Eth_TimeStampQualType *timeQualPtr,
 *                          Eth_TimeStampType *timeStampPtr
 *                      )
 *  Mode              : Supervisor Mode (Privileged Mode)
 *  Service ID[hex]   : 0x16
 *  Sync/Async        : Synchronous
 *  Reentrancy        : Non Reentrant
 *  Parameters (in)   : CtrlIdx. Index of the controller within the context of the
 *                               Ethernet Driver
 *  Parameters (inout): None
 *  Parameters (out)  : timeQualPtr. Quality of HW time stamp, e.g. based on current
 *                                   drift
 *  Return value      : Std_ReturnType
 *                        E_OK: success
 *                        E_NOT_OK: failed
 *  Description       : Returns a time value out of the HW registers according
 *                      to the capability of the HW. Is the HW resolution is lower than
 *                      the Eth_TimeStampType resolution resp. range, than an the
 *                      remaining bits will be filled with 0.
 *  \endverbatim
 */
FUNC(Std_ReturnType, ETH_CODE)
Eth_GetCurrentTime(uint8 CtrlIdx,
                   P2VAR(Eth_TimeStampQualType, AUTOMATIC, ETH_APPL_DATA) timeQualPtr,
                   P2VAR(Eth_TimeStampType, AUTOMATIC, ETH_APPL_DATA) timeStampPtr);

/**
 *  \brief This function activates egress time stamping on a dedicated message
 *         object.
 *
 *  \verbatim
 *  Service name      : Eth_EnableEgressTimeStamp
 *  Syntax            : void Eth_EnableEgressTimeStamp(
 *                          uint8 CtrlIdx,
 *                          uint8 BufIdx
 *                      )
 *  Mode              : Supervisor Mode (Privileged Mode)
 *  Service ID[hex]   : 0x17
 *  Sync/Async        : Synchronous
 *  Reentrancy        : Non Reentrant
 *  Parameters (in)   : CtrlIdx. Index of the controller within the context of the
 *                               Ethernet Driver
 *                      BufIdx. Index of the message buffer, where Application
 *                              expects egress time stamping
 *  Parameters (inout): None
 *  Parameters (out)  : None
 *  Return value      : None
 *  Description       : Activates egress time stamping on a dedicated message
 *                      object.
 *                      Some HW does store once the egress time stamp marker and
 *                      some HW needs it always before transmission. There will be
 *                      no disable functionality, due to the fact, that the message
 *                      type is always "time stamped" by network design.
 *  \endverbatim
 */
FUNC(void, ETH_CODE)
Eth_EnableEgressTimeStamp(uint8 CtrlIdx, uint8 BufIdx);

/**
 *  \brief This function reads back the egress time stamp on a dedicated message
 *         object.
 *
 *  \verbatim
 *  Service name      : Eth_GetEgressTimeStamp
 *  Syntax            : void Eth_GetEgressTimeStamp(
 *                          uint8 CtrlIdx,
 *                          uint8 BufIdx,
 *                          Eth_TimeStampQualType* timeQualPtr,
 *                          Eth_TimeStampType *timeStampPtr
 *                          )
 *  Mode              : Supervisor Mode (Privileged Mode)
 *  Service ID[hex]   : 0x18
 *  Sync/Async        : Synchronous
 *  Reentrancy        : Non Reentrant
 *  Parameters (in)   : CtrlIdx. Index of the controller within the context of the
 *                               Ethernet Driver
 *                      BufIdx. Index of the message buffer, where Application
 *                              expects egress time stamping
 *  Parameters (inout): timeQualPtr. Quality of HW time stamp, e.g. based on current
 *                                   drift
 *                      timeStampPtr. Current time stamp
 *  Parameters (out)  : None
 *  Return value      : None
 *  Description       : Reads back the egress time stamp on a dedicated message
 *                      object.
 *                      It must be called within the TxConfirmation() function.
 *  \endverbatim
 */
FUNC(void, ETH_CODE)
Eth_GetEgressTimeStamp(uint8 CtrlIdx,
                       uint8 BufIdx,
                       P2VAR(Eth_TimeStampQualType, AUTOMATIC, ETH_APPL_DATA) timeQualPtr,
                       P2VAR(Eth_TimeStampType, AUTOMATIC, ETH_APPL_DATA) timeStampPtr);

/**
 *  \brief This function reads back the ingress time stamp on a dedicated
 *         message object.
 *
 *  \verbatim
 *  Service name      : Eth_GetIngressTimeStamp
 *  Syntax            : void Eth_GetIngressTimeStamp(
 *                          uint8 CtrlIdx,
 *                          Eth_DataType* DataPtr,
 *                          Eth_TimeStampQualType* timeQualPtr,
 *                          Eth_TimeStampType *timeStampPtr
 *                      )
 *  Mode              : Supervisor Mode (Privileged Mode)
 *  Service ID[hex]   : 0x19
 *  Sync/Async        : Synchronous
 *  Reentrancy        : Non Reentrant
 *  Parameters (in)   : CtrlIdx. Index of the controller within the context of the
 *                               Ethernet Driver
 *                      DataPtr. Pointer to the message buffer, where Application
 *                               expects ingress time stamping
 *  Parameters (inout): timeQualPtr. Quality of HW time stamp, e.g. based on current
 *                                   drift
 *                      timeStampPtr. Current time stamp
 *  Parameters (out)  : None
 *  Return value      : None
 *  Description       : Reads back the ingress time stamp on a dedicated message
 *                      object.
 *                      It must be called within the RxIndication() function.
 *  \endverbatim
 */
FUNC(void, ETH_CODE)
Eth_GetIngressTimeStamp(uint8 CtrlIdx,
                        P2VAR(Eth_DataType, AUTOMATIC, ETH_APPL_DATA) DataPtr,
                        P2VAR(Eth_TimeStampQualType, AUTOMATIC, ETH_APPL_DATA) timeQualPtr,
                        P2VAR(Eth_TimeStampType, AUTOMATIC, ETH_APPL_DATA) timeStampPtr);

/**
 *  \brief This function allows the Time Slave to adjust the local ETH Reference
 *         clock in HW.
 *
 *  \verbatim
 *  Service name      : Eth_SetCorrectionTime
 *  Syntax            : void Eth_SetCorrectionTime(
 *                          uint8 CtrlIdx,
 *                          Eth_TimeIntDiffType* timeOffsetPtr,
 *                          Eth_RateRatioType *rateRatioPtr
 *                      )
 *  Mode              : Supervisor Mode (Privileged Mode)
 *  Service ID[hex]   : 0x1A
 *  Sync/Async        : Synchronous
 *  Reentrancy        : Non Reentrant
 *  Parameters (in)   : CtrlIdx. Index of the controller within the context of the
 *                               Ethernet Driver
 *  Parameters (inout): timeOffsetPtr. Offset between time stamp grandmaster and
 *                                    time stamp by local clock:
 *                        (OriginTimeStampSync[FUP] - IngressTimeStampSync) +
 *                         Pdelay
 *                      rateRatioPtr. Time elements to calculate and to modify the
 *                                    ratio of the frequency of the grandmaster in
 *                                    relation to the frequency of the Local Clock
 *                                    with:
 *                        ratio = OriginTimeStampDelta / IngressTimeStampDelta
 *  Parameters (out)  : None
 *  Return value      : None
 *  Description       : Allows the Time Slave to adjust the local ETH Reference
 *                      clock in HW.
 *  \endverbatim
 */
FUNC(void, ETH_CODE)
Eth_SetCorrectionTime(uint8 CtrlIdx,
                      P2VAR(Eth_TimeIntDiffType, AUTOMATIC, ETH_APPL_DAT) timeOffsetPtr,
                      P2VAR(Eth_RateRatioType, AUTOMATIC, ETH_APPL_DAT) rateRatioPtr);

/**
 *  \brief This function allows the Time Master to adjust the global ETH
 *         Reference clock in HW.
 *
 *  \verbatim
 *  Service name      : Eth_SetGlobalTime
 *  Syntax            : Std_ReturnType Eth_SetGlobalTime(
 *                          uint8 CtrlIdx,
 *                          Eth_TimeStampType* timeStampPtr
 *                      )
 *  Mode              : Supervisor Mode (Privileged Mode)
 *  Service ID[hex]   : 0x1B
 *  Sync/Async        : Synchronous
 *  Reentrancy        : Non Reentrant
 *  Parameters (in)   : CtrlIdx. Index of the controller within the context of the
 *                               Ethernet Driver
 *                      timeStampPtr. New time stamp
 *  Parameters (inout): None
 *  Parameters (out)  : None
 *  Return value      : Std_ReturnType
 *                        E_OK: success
 *                        E_NOT_OK: failed
 *  Description       : Allows the Time Master to adjust the global ETH Reference
 *                      clock in HW. We can use this method to set a global time base
 *                      on ETH in general or to synchronize the global ETH time base
 *                      with another time base, e.g. FlexRay.
 *  \endverbatim
 */
FUNC(Std_ReturnType, ETH_CODE)
Eth_SetGlobalTime(uint8 CtrlIdx,
                  P2VAR(Eth_TimeStampType, AUTOMATIC, ETH_APPL_DAT) timeStampPtr);
#endif /* (STD_ON == ETH_GLOBALTIMESUPPORT_API) */

#if (STD_OFF == ETH_USE_Q_APIS)
/**
 *  \brief This function provides access to a transmit buffer of the specified
 *         controller.
 *
 *  \verbatim
 *  Service name      : Eth_ProvideTxBuffer
 *  Syntax            : BufReq_ReturnType Eth_ProvideTxBuffer(
 *                          uint8 CtrlIdx,
 *                          uint8 Priority,
 *                          uint8* BufIdxPtr,
 *                          uint8** BufPtr,
 *                          uint16* LenBytePtr
 *                      )
 *  Mode              : User Mode (Non-Privileged Mode)
 *  Service ID[hex]   : 0x09
 *  Sync/Async        : Synchronous
 *  Reentrancy        : Non Reentrant
 *  Parameters (in)   : CtrlIdx. Index of the controller within the context of the
 *                               Ethernet Driver
 *  Parameters (in)   : Priority. Frame priority for transmit buffer FIFO selection
 *  Parameters (inout): LenBytePtr
 *                        In: desired length in bytes, out: granted length in bytes
 *  Parameters (out)  : BufIdxPtr. Index to the granted buffer resource. To be used
 *                                 for subsequent requests
 *                      BufPtr. Pointer to the granted buffer
 *  Return value      : BufReq_ReturnType
 *                        BUFREQ_OK: success
 *                        BUFREQ_E_NOT_OK: development error detected
 *                        BUFREQ_E_BUSY: all buffers in use
 *                        BUFREQ_E_OVFL: requested buffer too large
 *  Description       : Provides access to a transmit buffer of the specified
 *                      controller.
 *  \endverbatim
 */
FUNC(BufReq_ReturnType, ETH_CODE)
Eth_ProvideTxBuffer(uint8 CtrlIdx,
                    uint8 Priority,
                    P2VAR(Eth_BufIdxType, AUTOMATIC, ETH_APPL_DAT) BufIdxPtr,
                    P2VAR(uint8, AUTOMATIC, ETH_APPL_DAT) *BufPtr,
                    P2VAR(uint16, AUTOMATIC, ETH_APPL_DAT) LenBytePtr);
#endif /* (STD_OFF == ETH_USE_Q_APIS) */

#if (STD_OFF == ETH_USE_Q_APIS)
/**
 *  \brief This function triggers transmission of a previously filled transmit
 *         buffer.
 *
 *  \verbatim
 *  Service name      : Eth_Transmit
 *  Syntax            : Std_ReturnType Eth_Transmit(
 *                          uint8 CtrlIdx,
 *                          uint8 BufIdx,
 *                          Eth_FrameType FrameType,
 *                          boolean TxConfirmation,
 *                          uint16 LenByte,
 *                          uint8 *PhysAddrPtr)
 *  Mode              : Supervisor Mode (Privileged Mode)
 *  Service ID[hex]   : 0xA
 *  Sync/Async        : Synchronous
 *  Reentrancy        : Non Reentrant
 *  Parameters (in)   : CtrlIdx. Index of the controller within the context of the
 *                               Ethernet Driver
 *                      BufIdx. Index of the buffer resource
 *                      FrameType. Ethernet frame type
 *                      TxConfirmation. Activates transmission confirmation
 *                      LenByte. Data length in byte
 *                      PhysAddrPtr. Physical target address (MAC address) in
 *                                   network byte order
 *  Parameters (inout): None
 *  Parameters (out)  : None
 *  Return value      : Std_ReturnType
 *                        E_OK: success
 *                        E_NOT_OK: transmission failed
 *  Description       : Triggers transmission of a previously filled transmit
 *                      buffer.
 *  \endverbatim
 */
FUNC(Std_ReturnType, ETH_CODE)
Eth_Transmit(uint8 CtrlIdx,
             Eth_BufIdxType BufIdx,
             Eth_FrameType FrameType,
             boolean TxConfirmation,
             uint16 LenByte,
             P2VAR(uint8, AUTOMATIC, ETH_APPL_DAT) PhysAddrPtr);
#endif /* (STD_ON == ETH_USE_Q_APIS) */

#if ((STD_ON == ETH_ZERO_COPY_TX) && (STD_OFF == ETH_USE_Q_APIS))
/**
 *  \brief This function triggers transmission of a packet without buffer copy.
 *
 *  \verbatim
 *  Service name      : Eth_TransmitNoCpy
 *  Syntax            : Std_ReturnType Eth_TransmitNoCpy(
 *                          uint8 CtrlIdx,
 *                          uint8 *BufPtr,
 *                          Eth_FrameType FrameType,
 *                          uint16 LenByte,
 *                          uint8 *PhysAddrPtr)
 *  Mode              : Supervisor Mode (Privileged Mode)
 *  Service ID[hex]   : 0x2B
 *  Sync/Async        : Synchronous
 *  Reentrancy        : Non Reentrant
 *  Parameters (in)   : CtrlIdx. Index of the controller within the context of the
 *                               Ethernet Driver
 *                      BufPtr. Pointer to the packet buffer
 *                      FrameType. Ethernet frame type
 *                      LenByte. Data length in byte
 *                      PhysAddrPtr. Physical target address (MAC address) in
 *                                   network byte order
 *  Parameters (inout): None
 *  Parameters (out)  : None
 *  Return value      : Std_ReturnType
 *                        E_OK: success
 *                        E_NOT_OK: transmission failed
 *                        ETH_E_BUSY: hardware is busy and can't take new buffer
 *  Description       : Triggers transmission of a previously filled packet
 *                      buffer without performing additional buffer copy.
 *  \endverbatim
 */
FUNC(Std_ReturnType, ETH_CODE)
Eth_TransmitNoCpy(uint8 CtrlIdx,
                  P2VAR(uint8, AUTOMATIC, ETH_APPL_DAT) BufPtr,
                  Eth_FrameType FrameType,
                  uint16 LenByte,
                  P2VAR(uint8, AUTOMATIC, ETH_APPL_DAT) PhysAddrPtr);
#endif /* ((STD_ON == ETH_ZERO_COPY_TX) && (STD_OFF == ETH_USE_Q_APIS)) */

#if (STD_OFF == ETH_USE_Q_APIS)
/**
 *  \brief This function triggers frame reception.
 *
 *  \verbatim
 *  Service name      : Eth_Receive
 *  Syntax            : void Eth_Receive(
 *                          uint8 CtrlIdx,
 *                          uint8 FifoIdx,
 *                          Eth_RxStatusType* RxStatusPtr
 *                      )
 *  Mode              : Supervisor Mode (Privileged Mode)
 *  Service ID[hex]   : 0xB
 *  Sync/Async        : Synchronous
 *  Reentrancy        : Non Reentrant
 *  Parameters (in)   : CtrlIdx. Index of the controller within the context of the
 *                               Ethernet Driver
 *  Parameters (in)   : FifoIdx. Specifies the related fifo
 *  Parameters (inout): None
 *  Parameters (out)  : RxStatusPtr. Indicates whether a frame has been received and
 *                                   if so, whether more frames are available or
 *                                   frames got lost
 *  Return value      : None
 *  Description       : Triggers frame reception.
 *  \endverbatim
 */
FUNC(void, ETH_CODE)
Eth_Receive(uint8 CtrlIdx,
            uint8 FifoIdx,
            P2VAR(Eth_RxStatusType, AUTOMATIC, ETH_APPL_DAT) RxStatusPtr);
#endif /* (STD_OFF == ETH_USE_Q_APIS) */

/**
 *  \brief This function triggers frame transmission confirmation.
 *
 *  \verbatim
 *  Service name      : Eth_TxConfirmation
 *  Syntax            : void Eth_TxConfirmation(
 *                          uint8 CtrlIdx
 *                      )
 *  Mode              : Supervisor Mode (Privileged Mode)
 *  Service ID[hex]   : 0xC
 *  Sync/Async        : Synchronous
 *  Reentrancy        : Non Reentrant
 *  Parameters (in)   : CtrlIdx. Index of the controller within the context of the
 *                                Ethernet Driver
 *  Parameters (inout): None
 *  Parameters (out)  : None
 *  Return value      : None
 *  Description       : Triggers frame transmission confirmation.
 *  \endverbatim
 */
FUNC(void, ETH_CODE)
Eth_TxConfirmation(uint8 CtrlIdx);

#if (STD_ON == ETH_VERSION_INFO_API)
/**
 *  \brief Function returns the version information of this module.
 *
 *  \verbatim
 *  Service name      : Eth_GetVersionInfo
 *  Syntax            : void Eth_GetVersionInfo(
 *                          Std_VersionInfoType* versioninfo
 *                      )
 *  Mode              : User Mode (Non-Privileged Mode)
 *  Service ID[hex]   : 0xD
 *  Sync/Async        : Synchronous
 *  Reentrancy        : Reentrant
 *  Parameters (in)   : None
 *  Parameters (inout): None
 *  Parameters (out)  : VersionInfoPtr. Pointer to where to store the version
 *                      information of this module
 *  Return value      : None
 *  Description       : Returns the version information of this module.
 *  \endverbatim
 */
FUNC(void, ETH_CODE)
Eth_GetVersionInfo(P2VAR(Std_VersionInfoType, AUTOMATIC, ETH_APPL_DATA) VersionInfo);
#endif  /* (STD_ON == ETH_VERSION_INFO_API) */

/**
 *  \brief The function checks for controller errors and lost frames. Used for
 *         polling state changes. Calls EthIf_CtrlModeIndication when the
 *         controller mode changed.
 *
 *  \verbatim
 *  Service name      : Eth_MainFunction
 *  Syntax            : void Eth_MainFunction(
 *                          void
 *                      )
 *  Mode              : Supervisor Mode (Privileged Mode)
 *  Service ID[hex]   : 0x0A
 *  Description       : The function checks for controller errors and lost frames.
 *                      Used for polling state changes. Calls EthIf_CtrlModeIndication
 *                      when the controller mode changed.
 *  \endverbatim
 */
FUNC(void, ETH_CODE)
Eth_MainFunction(void);

#if (STD_ON == ETH_VIRTUALMAC_SUPPORT)
/**
 *  \brief This function initializes Eth Virtual MAC driver's Remote Procedure Call
 *
 *  \verbatim
 *  Service name      : Eth_VirtMacRpcInit
 *  Syntax            : Std_ReturnType Eth_VirtMacRpcInit(Eth_ConfigType *CfgPtr)
 *  Mode              : Supervisor Mode (Privileged Mode)
 *  Service ID[hex]   : 0x2BU
 *  Sync/Async        : Asynchronous
 *  Reentrancy        : Non-Reentrant
 *  Parameters (in)   : CfgPtr. Pointer to virt mac config struct. Refer to /ref Eth_ConfigType
 *  Parameters (inout): None
 *  Parameters (out)  : None
 *  Return value      :  Std_ReturnType
 *                        E_OK: success
 *                        E_NOT_OK: transmission failed
 *
 *  Description       :  The function initializes the RPC configuration required to invoke remote procedure
 *                       calls to ethernet firmware to enable data path setup to AUTOSAR Eth MCAL
 *                       driver. On initialization of AUTOSAR MCAL driver in virtual mac mode, the driver
 *                       announces autosar ethernet mcal driver RpMsg service to the ethernet
 *                       firmware core. The ethernet firmware server service will block  waiting for
 *                       the announcment from MCAL driver and then attach to the Ethernet MCAL driver
 *                       by sending it a EthFwInfo msg
 *                       If configured in virtual mac mode application should
 *                       invoke this API as follows:
 *                       1. Eth_VirtMacRpcInit()
 *                       2. Wait for fwRegisteredCb() callback
 *                       3. Eth_DispatchVirtmacInit
 *                       4. Wait for rpcCmdComplete() callback with status E_OK
 *                       5. Eth_Init()
 *                       The callbacks fwRegisteredCb() and rpcCmdComplete() are
 *                       populated by the application as part of the Eth_ConfigType
 *                       structure
 *  \endverbatim
 */
FUNC(Std_ReturnType, ETH_CODE)
Eth_VirtMacRpcInit(P2CONST(Eth_ConfigType, AUTOMATIC, ETH_PBCFG) CfgPtr);
#endif  /* (STD_ON == ETH_VIRTUALMAC_SUPPORT) */

#if (STD_ON == ETH_VIRTUALMAC_SUPPORT)
/**
 *  \brief Dispatch virtual mac initialization request to ethernet firmware
 *
 *  \verbatim
 *  Service name      : Eth_DispatchVirtmacInit
 *  Syntax            : Std_ReturnType Eth_DispatchVirtmacInit(CtrlIdx)
 *  Mode              : Supervisor Mode (Privileged Mode)
 *  Service ID[hex]   : 0x1CU
 *  Sync/Async        : Asynchronous
 *  Reentrancy        : Non-Reentrant
 *  Parameters (in)   : CtrlIdx. Index of the controller within the context of the
 *                                Ethernet Driver
 *  Parameters (inout): None
 *  Parameters (out)  : None
 *  Return value      :  Std_ReturnType
 *                        E_OK: success
 *                        E_NOT_OK: transmission failed
 *
 *  Description       :  The function dispatches RPC command to ethernet
 *                       firmware core to allocate data path resources.
 *                       The allocated Tx, Rx DMA channels are then setup in the
 *                       Eth_Init() API. The function only send the
 *                       RPC msg to ethernet firmware core. Completion of RPC msg
 *                       processing is indicated by invokcation of
 *                       rpcCmdComplete part of Eth_ConfigType sturcture.
 *                       If configured in virtual mac mode application should
 *                       invoke this API as follows:
 *                       1. Eth_VirtMacRpcInit()
 *                       2. Wait for fwRegisteredCb() callback
 *                       3. Eth_DispatchVirtmacInit
 *                       4. Wait for rpcCmdComplete() callback with status E_OK
 *                       5. Eth_Init()
 *                       The callbacks fwRegisteredCb() and rpcCmdComplete() are
 *                       populated by the application as part of the Eth_ConfigType
 *                       structure
 *  \endverbatim
 */
FUNC(Std_ReturnType, ETH_CODE)
Eth_DispatchVirtmacInit(uint8 CtrlIdx);
#endif  /* (STD_ON == ETH_VIRTUALMAC_SUPPORT) */

#if (STD_ON == ETH_VIRTUALMAC_SUPPORT)
/**
 *  \brief Dispatch virtual mac deinit request to ethernet firmware
 *
 *  \verbatim
 *  Service name      : Eth_DispatchVirtmacDeinit
 *  Syntax            : Std_ReturnType Eth_DispatchVirtmacDeinit(CtrlIdx)
 *  Mode              : Supervisor Mode (Privileged Mode)
 *  Service ID[hex]   : 0x1DU
 *  Sync/Async        : Asynchronous
 *  Reentrancy        : Non-Reentrant
 *  Parameters (in)   : CtrlIdx. Index of the controller within the context of the
 *                                Ethernet Driver
 *  Parameters (inout): None
 *  Parameters (out)  : None
 *  Return value      :  Std_ReturnType
 *                        E_OK: success
 *                        E_NOT_OK: transmission failed
 *
 *  Description       :  The function dispatches RPC command to ethernet
 *                       firmware core to free data path resources.
 *                       This function dispatches RPC msg to ethernet firmware core.
 *                       Completion of RPC msg processing is indicated by invokcation
 *                       of rpcCmdComplete part of Eth_ConfigType sturcture.
 *                       Once complete the data path to Ethernet virtual MAC
 *                       has been torn down and driver can no loner send or receive packets
 *  \endverbatim
 */
FUNC(Std_ReturnType, ETH_CODE)
Eth_DispatchVirtmacDeinit(uint8 CtrlIdx);
#endif  /* (STD_ON == ETH_VIRTUALMAC_SUPPORT) */

#if ((STD_ON == ETH_VIRTUALMAC_SUPPORT) && (STD_ON == ETH_VIRTUALMAC_NOTIFYMSGRECEIVED_API))
/**
 *  \brief Notify Ethernet driver that a RPC msg has been received from ethernet firmware
 *
 *  \verbatim
 *  Service name      : Eth_NotifyVirtmacMsgReceived
 *  Syntax            : Std_ReturnType Eth_NotifyVirtmacMsgReceived(CtrlIdx)
 *  Mode              : Supervisor Mode (Privileged Mode)
 *  Service ID[hex]   : 0x1EU
 *  Sync/Async        : Asynchronous
 *  Reentrancy        : Non-Reentrant
 *  Parameters (in)   : CtrlIdx. Index of the controller within the context of the
 *                                Ethernet Driver
 *  Parameters (inout): None
 *  Parameters (out)  : None
 *  Return value      :  Std_ReturnType
 *                        E_OK: success
 *                        E_NOT_OK: transmission failed
 *
 *  Description       :  Invocation of this function will cause the Ethernet
 *                       MCAL driver to process any queued messages received
 *                       from ethernet firmware. Depending on type of msg received
 *                       ethernet firmware will invoke rpcCmdComplete/fwRegisteredCb
 *                       application callbacks. This function is typically
 *                       called from the Cdd_IpcNewMessageNotify if the channel
 *                       id matches the Ethernet MCAL IPC channel id
 *                       (ethfwRpcComChId member of Eth_ConfigType structure)
 *                       Application can also trigger recv msg processing by
 *                       setting pollRecvMsgInEthMain in Eth_ConfigType to TRUE
 *                       Invoking Eth_MainFunction with this config set will
 *                       trigger recv msg processing. In triggering recv msg
 *                       processing from Eth_MainFunction, this API should be
 *                       disabled
 *
 *  \endverbatim
 */
FUNC(Std_ReturnType, ETH_CODE)
Eth_NotifyVirtmacMsgReceived(uint8 CtrlIdx);
#endif  /* ((STD_ON == ETH_VIRTUALMAC_SUPPORT) && (STD_ON == ETH_VIRTUALMAC_NOTIFYMSGRECEIVED_API)) */

#if ((STD_ON == ETH_VIRTUALMAC_SUPPORT) && (STD_ON == ETH_VIRTUALMAC_SUBSCRIBEALLTRAFFIC_API))
/**
 *  \brief Dispatch virtual mac all traffic subscription request to ethernet firmware
 *
 *  \verbatim
 *  Service name      : Eth_DispatchVirtmacSubscribeAllTraffic
 *  Syntax            : Std_ReturnType Eth_DispatchVirtmacSubscribeAllTraffic(CtrlIdx)
 *  Mode              : Supervisor Mode (Privileged Mode)
 *  Service ID[hex]   : 0x1FU
 *  Sync/Async        : Asynchronous
 *  Reentrancy        : Non-Reentrant
 *  Parameters (in)   : CtrlIdx. Index of the controller within the context of the
 *                                Ethernet Driver
 *  Parameters (inout): None
 *  Parameters (out)  : None
 *  Return value      :  Std_ReturnType
 *                        E_OK: success
 *                        E_NOT_OK: transmission failed
 *
 *  Description       :  The function dispatches RPC command to ethernet
 *                       firmware core to subscribe to all default flow traffic
 *                       received on host port.The default flow contains all
 *                       traffic that is not explicitly categorized into a specific
 *                       flow using a classifier entry in the switch .
 *                       Note that only one core can subscribe to default flow
 *                       traffic in a system and this is by default the ethernet
 *                       firmware core. This API requires a compatible ethernet
 *                       firmware that _does_ _not_ subscribe to default flow
 *                       and allows Eth Driver to subscribe to default flow
 *                       The function only send the RPC msg to ethernet firmware core.
 *                       Completion of RPC msg processing is indicated by invokcation of
 *                       rpcCmdComplete part of Eth_ConfigType sturcture.
 *                       A failure status indicates ethernet firmware does not allow
 *                       AUTOSAR Eth Driver to subscribe to default flow
 *                       If configured in virtual mac mode application should
 *                       invoke this API as follows:
 *                       1. Eth_VirtMacRpcInit()
 *                       2. Wait for fwRegisteredCb() callback
 *                       3. Eth_DispatchVirtmacInit
 *                       4. Wait for rpcCmdComplete() callback with status E_OK
 *                       5. Eth_Init()
 *                       6. Eth_DispatchVirtmacSubscribeAllTraffic()
 *                       7. Wait for rpcCmdComplete() callback with status E_OK
 *                       The callbacks fwRegisteredCb() and rpcCmdComplete() are
 *                       populated by the application as part of the Eth_ConfigType
 *                       structure
 *  \endverbatim
 */
FUNC(Std_ReturnType, ETH_CODE)
Eth_DispatchVirtmacSubscribeAllTraffic(uint8 CtrlIdx);
#endif  /* ((STD_ON == ETH_VIRTUALMAC_SUPPORT) && (STD_ON == ETH_VIRTUALMAC_SUBSCRIBEALLTRAFFIC_API)) */

#if ((STD_ON == ETH_VIRTUALMAC_SUPPORT) && (STD_ON == ETH_VIRTUALMAC_UNSUBSCRIBEALLTRAFFIC_API))
/**
 *  \brief Dispatch virtual mac all traffic unsubscription request to ethernet firmware
 *
 *  \verbatim
 *  Service name      : Eth_DispatchVirtmacUnsubscribeAllTraffic
 *  Syntax            : Std_ReturnType Eth_DispatchVirtmacUnsubscribeAllTraffic(CtrlIdx)
 *  Mode              : Supervisor Mode (Privileged Mode)
 *  Service ID[hex]   : 0x20U
 *  Sync/Async        : Asynchronous
 *  Reentrancy        : Non-Reentrant
 *  Parameters (in)   : CtrlIdx. Index of the controller within the context of the
 *                                Ethernet Driver
 *  Parameters (inout): None
 *  Parameters (out)  : None
 *  Return value      :  Std_ReturnType
 *                        E_OK: success
 *                        E_NOT_OK: transmission failed
 *
 *  Description       :  The function dispatches RPC command to ethernet
 *                       firmware core to unsubscribe to all default flow traffic
 *                       received on host port.The driver should have subscribed
 *                       to all default flow traffic previously by invoking
 *                       Eth_DispatchVirtmacSubscribeAllTraffic().
 *                       On unsubscribing to default flow all traffic with
 *                       default flow will be dropped.
 *                       The function only send the RPC msg to ethernet firmware core.
 *                       Completion of RPC msg processing is indicated by invokcation of
 *                       rpcCmdComplete part of Eth_ConfigType sturcture.
 *                       The callbacks fwRegisteredCb() and rpcCmdComplete() are
 *                       populated by the application as part of the Eth_ConfigType
 *                       structure
 *                       If configured in virtual mac mode application should
 *                       invoke this API as follows:
 *                       1. Eth_VirtMacRpcInit()
 *                       2. Wait for fwRegisteredCb() callback
 *                       3. Eth_DispatchVirtmacInit
 *                       4. Wait for rpcCmdComplete() callback with status E_OK
 *                       5. Eth_Init()
 *                       6. Eth_DispatchVirtmacSubscribeAllTraffic()
 *                       7. Wait for rpcCmdComplete() callback with status E_OK
 *                       8. Eth_SetControllerMode(ETH_MODE_ACTIVE)
 *                       9. Packet processing using Eth_Receive/Eth_Trasmit
 *                       10. Eth_DispatchVirtmacUnsubscribeAllTraffic
 *                       11. Wait for rpcCmdComplete() callback with status E_OK
 *                       The callbacks fwRegisteredCb() and rpcCmdComplete() are
 *                       populated by the application as part of the Eth_ConfigType
 *                       structure
 *
 *  \endverbatim
 */
FUNC(Std_ReturnType, ETH_CODE)
Eth_DispatchVirtmacUnsubscribeAllTraffic(uint8 CtrlIdx);
#endif  /* ((STD_ON == ETH_VIRTUALMAC_SUPPORT) && (STD_ON == ETH_VIRTUALMAC_UNSUBSCRIBEALLTRAFFIC_API)) */

#if ((STD_ON == ETH_VIRTUALMAC_SUPPORT) && (STD_ON == ETH_VIRTUALMAC_SUBSCRIBEDSTMAC_API))
/**
 *  \brief Dispatch virtual mac destination mac traffic subscription request to ethernet firmware
 *
 *  \verbatim
 *  Service name      : Eth_DispatchVirtmacSubscribeDstMac
 *  Syntax            : Std_ReturnType Eth_DispatchVirtmacSubscribeDstMac(CtrlIdx, macAddress)
 *  Mode              : Supervisor Mode (Privileged Mode)
 *  Service ID[hex]   : 0x21
 *  Sync/Async        : Asynchronous
 *  Reentrancy        : Non-Reentrant
 *  Parameters (in)   : CtrlIdx: Index of the controller within the context of the
 *                                Ethernet Driver
 *                      macAddress: Destination Mac address
 *  Parameters (inout): None
 *  Parameters (out)  : None
 *  Return value      :  Std_ReturnType
 *                        E_OK: success
 *                        E_NOT_OK: transmission failed
 *
 *  Description       :  The function dispatches RPC command to ethernet
 *                       firmware core to subscribe to traffic with the given
 *                       destination macAddress to be routed to the rx flow
 *                       of the Ethernet MCAL driver so that driver can receive
 *                       the packets.The macAddres can be a unicast address
 *                       allocated to the EthDriver (returned by Eth_GetPhysAddr())
 *                       or a multicast address.
 *                       The switch address resolution table should have an entry to
 *                       route the destination mac address to the host port.
 *                       For unicast address , the switch address resolution table
 *                       can be updated using Eth_DispatchVirtmacAddUnicastAddr()
 *                       For multicast address , the switch address resolution table
 *                       can be updated using Eth_DispatchVirtmacAddMcastAddr()
 *                       Note that broadcast address is usually subscribed to by
 *                       ethernet firmware so that it can respond to ARP requests.
 *                       If ARP response handling by ethernet firmware is required,
 *                       Eth driver should not be subscribed to broadcast address.
 *                       The function only send the RPC msg to ethernet firmware core.
 *                       Completion of RPC msg processing is indicated by invokcation of
 *                       rpcCmdComplete part of Eth_ConfigType sturcture.
 *                       A failure status indicates ethernet firmware does not allow
 *                       AUTOSAR Eth Driver to subscribe to default flow
 *                       If configured in virtual mac mode application should
 *                       invoke this API as follows:
 *                       1. Eth_VirtMacRpcInit()
 *                       2. Wait for fwRegisteredCb() callback
 *                       3. Eth_DispatchVirtmacInit
 *                       4. Wait for rpcCmdComplete() callback with status E_OK
 *                       5. Eth_Init()
 *                       6. Eth_DispatchVirtmacSubscribeDstMac()
 *                       7. Wait for rpcCmdComplete() callback with status E_OK
 *                       The callbacks fwRegisteredCb() and rpcCmdComplete() are
 *                       populated by the application as part of the Eth_ConfigType
 *                       structure
 *  \endverbatim
 */
FUNC(Std_ReturnType, ETH_CODE)
Eth_DispatchVirtmacSubscribeDstMac(uint8 CtrlIdx, uint8 *macAddress);
#endif  /* ((STD_ON == ETH_VIRTUALMAC_SUPPORT) && (STD_ON == ETH_VIRTUALMAC_SUBSCRIBEDSTMAC_API)) */

#if ((STD_ON == ETH_VIRTUALMAC_SUPPORT) && (STD_ON == ETH_VIRTUALMAC_UNSUBSCRIBEDSTMAC_API))
/**
 *  \brief Dispatch virtual mac destination traffic unsubscription request to ethernet firmware
 *
 *  \verbatim
 *  Service name      : Eth_DispatchVirtmacUnsubscribeDstMac
 *  Syntax            : Std_ReturnType Eth_DispatchVirtmacUnsubscribeDstMac(CtrlIdx)
 *  Mode              : Supervisor Mode (Privileged Mode)
 *  Service ID[hex]   : 0x22
 *  Sync/Async        : Asynchronous
 *  Reentrancy        : Non-Reentrant
 *  Parameters (in)   : CtrlIdx. Index of the controller within the context of the
 *                                Ethernet Driver
 *                      macAddress: Destination Mac address
 *  Parameters (inout): None
 *  Parameters (out)  : None
 *  Return value      :  Std_ReturnType
 *                        E_OK: success
 *                        E_NOT_OK: transmission failed
 *
 *  Description       :  The function dispatches RPC command to ethernet
 *                       firmware core to unsubscribe the specified destination
 *                       mac address flow traffic received on host port.
 *                       The driver should have subscribed to the destination mac
 *                       address previously by invoking
 *                       Eth_DispatchVirtmacSubscribeDstMac().
 *                       On unsubscribing to destination mac flow , traffic with
 *                       given destination mac address flow will be directed to
 *                       default flow.
 *                       The function only send the RPC msg to ethernet firmware core.
 *                       Completion of RPC msg processing is indicated by invokcation of
 *                       rpcCmdComplete part of Eth_ConfigType sturcture.
 *                       The callbacks fwRegisteredCb() and rpcCmdComplete() are
 *                       populated by the application as part of the Eth_ConfigType
 *                       structure
 *                       If configured in virtual mac mode application should
 *                       invoke this API as follows:
 *                       1. Eth_VirtMacRpcInit()
 *                       2. Wait for fwRegisteredCb() callback
 *                       3. Eth_DispatchVirtmacInit
 *                       4. Wait for rpcCmdComplete() callback with status E_OK
 *                       5. Eth_Init()
 *                       6. Eth_DispatchVirtmacSubscribeDstMac()
 *                       7. Wait for rpcCmdComplete() callback with status E_OK
 *                       8. Eth_SetControllerMode(ETH_MODE_ACTIVE)
 *                       9. Packet processing using Eth_Receive/Eth_Trasmit
 *                       10. Eth_DispatchVirtmacUnsubscribeDstMac()
 *                       11. Wait for rpcCmdComplete() callback with status E_OK
 *                       12. Eth_SetControllerMode(ETH_MODE_DOWN)
 *                       Note that all subscribed MAC address must be unsubscribed
 *                       before Eth_SetControllerMode(ETH_MODE_DOWN) is invoked
 *                       The callbacks fwRegisteredCb() and rpcCmdComplete() are
 *                       populated by the application as part of the Eth_ConfigType
 *                       structure
 *  \endverbatim
 */
FUNC(Std_ReturnType, ETH_CODE)
Eth_DispatchVirtmacUnsubscribeDstMac(uint8 CtrlIdx, uint8 *macAddress);
#endif  /* ((STD_ON == ETH_VIRTUALMAC_SUPPORT) && (STD_ON == ETH_VIRTUALMAC_UNSUBSCRIBEDSTMAC_API)) */

#if ((STD_ON == ETH_VIRTUALMAC_SUPPORT) && (STD_ON == ETH_VIRTUALMAC_ASSOCIATEIPV4MACADDR_API))
/**
 *  \brief Dispatch virtual mac IPV4 address:destination mac association request to ethernet firmware
 *
 *  \verbatim
 *  Service name      : Eth_DispatchVirtmacAssociateIPv4Macaddr
 *  Syntax            : Std_ReturnType Eth_DispatchVirtmacAssociateIPv4Macaddr(CtrlIdx, ipv4Address, macAddress)
 *  Mode              : Supervisor Mode (Privileged Mode)
 *  Service ID[hex]   : 0x23
 *  Sync/Async        : Asynchronous
 *  Reentrancy        : Non-Reentrant
 *  Parameters (in)   : CtrlIdx: Index of the controller within the context of the
 *                                Ethernet Driver
 *                      ipv4Address: IPv4 address
 *                      macAddress: Destination Mac address
 *  Parameters (inout): None
 *  Parameters (out)  : None
 *  Return value      :  Std_ReturnType
 *                        E_OK: success
 *                        E_NOT_OK: transmission failed
 *
 *  Description       :  The function dispatches RPC command to ethernet
 *                       firmware core to associate the given unicast macaddress
 *                       with the given IP address.
 *                       The ethernet firmware maintains the ARP database for
 *                       the ethernet driver and ethfw will respond to any ARP request.
 *                       To correctly respond to ARP queries for IP address assigned to
 *                       the AUTOSAR network stack , the AUTOSAR stack must invoke
 *                       this API so that the ARP database maintained by ethernet
 *                       firmware is updated.The macAddres must be a unicast address
 *                       allocated to the EthDriver (returned by Eth_GetPhysAddr())
 *                       If configured in virtual mac mode application should
 *                       invoke this API as follows:
 *                       1. Eth_VirtMacRpcInit()
 *                       2. Wait for fwRegisteredCb() callback
 *                       3. Eth_DispatchVirtmacInit
 *                       4. Wait for rpcCmdComplete() callback with status E_OK
 *                       5. Eth_Init()
 *                       6. Eth_DispatchVirtmacSubscribeDstMac()
 *                       7. Wait for rpcCmdComplete() callback with status E_OK
 *                       8. Eth_SetControllerMode(ETH_MODE_ACTIVE)
 *                       9. Send out DHCP request to acquire IP address for the
 *                          AUTOSAR network stack
 *                       10. On getting DHCP response with IP address, invoke
 *                           Eth_DispatchVirtmacAssociateIPv4Macaddr
 *                       11. Wait for rpcCmdComplete() callback with status E_OK
 *                       The callbacks fwRegisteredCb() and rpcCmdComplete() are
 *                       populated by the application as part of the Eth_ConfigType
 *                       structure
 *  \endverbatim
 */
FUNC(Std_ReturnType, ETH_CODE)
Eth_DispatchVirtmacAssociateIPv4Macaddr(uint8 CtrlIdx, uint8 *ipv4Address, uint8 *macAddress);
#endif  /* ((STD_ON == ETH_VIRTUALMAC_SUPPORT) && (STD_ON == ETH_VIRTUALMAC_ASSOCIATEIPV4MACADDR_API)) */


#if ((STD_ON == ETH_VIRTUALMAC_SUPPORT) && (STD_ON == ETH_VIRTUALMAC_DISASSOCIATEIPV4MACADDR_API))
/**
 *  \brief Dispatch virtual mac IPV4 address:destination mac disassociation request to ethernet firmware
 *
 *  \verbatim
 *  Service name      : Eth_DispatchVirtmacDisassociateIPv4Macaddr
 *  Syntax            : Std_ReturnType Eth_DispatchVirtmacDisassociateIPv4Macaddr(CtrlIdx, ipv4Address)
 *  Mode              : Supervisor Mode (Privileged Mode)
 *  Service ID[hex]   : 0x24
 *  Sync/Async        : Asynchronous
 *  Reentrancy        : Non-Reentrant
 *  Parameters (in)   : CtrlIdx: Index of the controller within the context of the
 *                                Ethernet Driver
 *                      ipv4Address: IPv4 address
 *  Parameters (inout): None
 *  Parameters (out)  : None
 *  Return value      :  Std_ReturnType
 *                        E_OK: success
 *                        E_NOT_OK: transmission failed
 *
 *  Description       :  The function dispatches RPC command to ethernet
 *                       firmware core to remove the given IP address from the
 *                       ARP database maintained by ethernet firmware.
 *                       On deletion the ethfw will no longer respond to any ARP
 *                       queries for the given IP address.
 *                       If configured in virtual mac mode application should
 *                       invoke this API as follows:
 *                       1. Eth_VirtMacRpcInit()
 *                       2. Wait for fwRegisteredCb() callback
 *                       3. Eth_DispatchVirtmacInit
 *                       4. Wait for rpcCmdComplete() callback with status E_OK
 *                       5. Eth_Init()
 *                       6. Eth_DispatchVirtmacSubscribeDstMac()
 *                       7. Wait for rpcCmdComplete() callback with status E_OK
 *                       8. Eth_SetControllerMode(ETH_MODE_ACTIVE)
 *                       9. Send out DHCP request to acquire IP address for the
 *                          AUTOSAR network stack
 *                       10. On getting DHCP response with IP address, invoke
 *                           Eth_DispatchVirtmacAssociateIPv4Macaddr
 *                       11. Wait for rpcCmdComplete() callback with status E_OK
 *                       12. Network Packet processing
 *                       13. Eth_DispatchVirtmacDisassociateIPv4Macaddr
 *                       14. Wait for rpcCmdComplete() callback with status E_OK
 *                       The callbacks fwRegisteredCb() and rpcCmdComplete() are
 *                       populated by the application as part of the Eth_ConfigType
 *                       structure
 *  \endverbatim
 */
FUNC(Std_ReturnType, ETH_CODE)
Eth_DispatchVirtmacDisassociateIPv4Macaddr(uint8 CtrlIdx, uint8 *ipv4Address);
#endif  /* ((STD_ON == ETH_VIRTUALMAC_SUPPORT) && (STD_ON == ETH_VIRTUALMAC_DISASSOCIATEIPV4MACADDR_API)) */

#if ((STD_ON == ETH_VIRTUALMAC_SUPPORT) && (STD_ON == ETH_VIRTUALMAC_ADD_UNICAST_MACADDR_API))
/**
 *  \brief Dispatch virtual mac addition of given unicast address to switch address resolution table request to ethernet firmware
 *
 *  \verbatim
 *  Service name      : Eth_DispatchVirtmacAddUnicastAddr
 *  Syntax            : Std_ReturnType Eth_DispatchVirtmacAddUnicastAddr(CtrlIdx, macAddress, port, vlanId)
 *  Mode              : Supervisor Mode (Privileged Mode)
 *  Service ID[hex]   : 0x25
 *  Sync/Async        : Asynchronous
 *  Reentrancy        : Non-Reentrant
 *  Parameters (in)   : CtrlIdx: Index of the controller within the context of the
 *                                Ethernet Driver
 *                      macAddress: Mac address to be added to the switch address resolution table
 *                      port: Port to which macAddress should be routed.
 *                      vlanId: Vlan Id associated with the mac Address. If vlan id is not needed, 0 should be passed
 *  Parameters (inout): None
 *  Parameters (out)  : None
 *  Return value      :  Std_ReturnType
 *                        E_OK: success
 *                        E_NOT_OK: transmission failed
 *
 *  Description       :  The function dispatches RPC command to ethernet
 *                       firmware core to add the given unicast macaddress
 *                       to the switch address resolution table with the given
 *                       port and vlan id.
 *                       These are static entries and not learned entries.
 *                       On addition of entry switch would switch frames
 *                       received with the given destination macAddress:vlan id
 *                       combo to the given port.
 *                       If configured in virtual mac mode application should
 *                       invoke this API as follows:
 *                       1. Eth_VirtMacRpcInit()
 *                       2. Wait for fwRegisteredCb() callback
 *                       3. Eth_DispatchVirtmacInit
 *                       4. Wait for rpcCmdComplete() callback with status E_OK
 *                       5. Eth_Init()
 *                       6. Eth_DispatchVirtmacSubscribeDstMac()
 *                       7. Wait for rpcCmdComplete() callback with status E_OK
 *                       8. Eth_DispatchVirtmacAddUnicastAddr()
 *                       9. Wait for rpcCmdComplete() callback with status E_OK
 *                       10. Eth_SetControllerMode(ETH_MODE_ACTIVE)
 *                       The callbacks fwRegisteredCb() and rpcCmdComplete() are
 *                       populated by the application as part of the Eth_ConfigType
 *                       structure
 *  \endverbatim
 */
FUNC(Std_ReturnType, ETH_CODE)
Eth_DispatchVirtmacAddUnicastAddr(uint8 CtrlIdx, uint8 *macAddress, Eth_PortType port , uint32 vlanId);
#endif  /* ((STD_ON == ETH_VIRTUALMAC_SUPPORT) && (STD_ON == ETH_VIRTUALMAC_ADD_UNICAST_MACADDR_API)) */

#if ((STD_ON == ETH_VIRTUALMAC_SUPPORT) && (STD_ON == ETH_VIRTUALMAC_ADD_MCAST_MACADDR_API))
/**
 *  \brief Dispatch virtual mac addition of given multicast address to switch address resolution table request to ethernet firmware
 *
 *  \verbatim
 *  Service name      : Eth_DispatchVirtmacAddMcastAddr
 *  Syntax            : Std_ReturnType Eth_DispatchVirtmacAddMcastAddr(CtrlIdx, macAddress, numLsbToIgnore, vlanId, portList)
 *  Mode              : Supervisor Mode (Privileged Mode)
 *  Service ID[hex]   : 0x26
 *  Sync/Async        : Asynchronous
 *  Reentrancy        : Non-Reentrant
 *  Parameters (in)   : CtrlIdx: Index of the controller within the context of the
 *                                Ethernet Driver
 *                      macAddress: Mac address to be added to the switch address resolution table
 *                      numLsbToIgnore: Number of least significant bits in the multicast address to ignore.
 *                                      This allows a range of multicast address to be handled by a single entry
 *                      vlanId: Vlan Id associated with the mac Address. If vlan id is not needed, 0 should be passed
 *                      portList: List of ports in the switch added to membership of the given multicast address
 *  Parameters (inout): None
 *  Parameters (out)  : None
 *  Return value      :  Std_ReturnType
 *                        E_OK: success
 *                        E_NOT_OK: transmission failed
 *
 *  Description       :  The function dispatches RPC command to ethernet
 *                       firmware core to add the given multicast macaddress
 *                       to the switch address resolution table with the given
 *                       portlist as members and vlan id.
 *                       On addition of entry switch would switch frames
 *                       received with the given destination multicast macAddress:
 *                       vlan id  combo to all the ports in the port list.
 *                       If configured in virtual mac mode application should
 *                       invoke this API as follows:
 *                       1. Eth_VirtMacRpcInit()
 *                       2. Wait for fwRegisteredCb() callback
 *                       3. Eth_DispatchVirtmacInit
 *                       4. Wait for rpcCmdComplete() callback with status E_OK
 *                       5. Eth_Init()
 *                       6. Eth_DispatchVirtmacSubscribeDstMac()
 *                       7. Wait for rpcCmdComplete() callback with status E_OK
 *                       8. Eth_DispatchVirtmacAddMcastAddr()
 *                       9. Wait for rpcCmdComplete() callback with status E_OK
 *                       10. Eth_SetControllerMode(ETH_MODE_ACTIVE)
 *                       The callbacks fwRegisteredCb() and rpcCmdComplete() are
 *                       populated by the application as part of the Eth_ConfigType
 *                       structure
 *  \endverbatim
 */
FUNC(Std_ReturnType, ETH_CODE)
Eth_DispatchVirtmacAddMcastAddr(uint8 CtrlIdx, uint8 *macAddress, uint32 numLsbToIgnore, uint32 vlanId, Eth_PortListType *portList);
#endif  /* ((STD_ON == ETH_VIRTUALMAC_SUPPORT) && (STD_ON == ETH_VIRTUALMAC_ADD_MCAST_MACADDR_API)) */

#if ((STD_ON == ETH_VIRTUALMAC_SUPPORT) && (STD_ON == ETH_VIRTUALMAC_DEL_MACADDR_API))
/**
 *  \brief Dispatch virtual mac removal of given mac address from switch address resolution table request to ethernet firmware
 *
 *  \verbatim
 *  Service name      : Eth_DispatchVirtmacDelAddr
 *  Syntax            : Std_ReturnType Eth_DispatchVirtmacDelAddr(uint8 CtrlIdx, uint8 *macAddress, uint32 vlanId)
 *  Mode              : Supervisor Mode (Privileged Mode)
 *  Service ID[hex]   : 0x27
 *  Sync/Async        : Asynchronous
 *  Reentrancy        : Non-Reentrant
 *  Parameters (in)   : CtrlIdx: Index of the controller within the context of the
 *                                Ethernet Driver
 *                      macAddress: Mac address to be deleted from the switch address resolution table
 *                      vlanId: Vlan Id associated with the mac Address. If vlan id is not needed, 0 should be passed
 *  Parameters (inout): None
 *  Parameters (out)  : None
 *  Return value      :  Std_ReturnType
 *                        E_OK: success
 *                        E_NOT_OK: transmission failed
 *
 *  Description       :  The function dispatches RPC command to ethernet
 *                       firmware core to remove the given multicast macaddress
 *                       and vlan id from the switch address resolution table
 *                       On removal of entry switch would switch frames
 *                       received with the given destination macAddress:
 *                       vlan id  combo by flooding to all the ports in vlan membership.
 *                       Address should have been added previously using Eth_DispatchVirtmacAddMcastAddr()/
 *                       Eth_DispatchVirtmacAddUnicastAddr()
 *                       If configured in virtual mac mode application should
 *                       invoke this API as follows:
 *                       1. Eth_VirtMacRpcInit()
 *                       2. Wait for fwRegisteredCb() callback
 *                       3. Eth_DispatchVirtmacInit
 *                       4. Wait for rpcCmdComplete() callback with status E_OK
 *                       5. Eth_Init()
 *                       6. Eth_DispatchVirtmacSubscribeDstMac()
 *                       7. Wait for rpcCmdComplete() callback with status E_OK
 *                       8. Eth_DispatchVirtmacAddMcastAddr()/Eth_DispatchVirtmacAddUnicastAddr()
 *                       9. Wait for rpcCmdComplete() callback with status E_OK
 *                       10. Eth_DispatchVirtmacDelAddr()
 *                       11. Wait for rpcCmdComplete() callback with status E_OK
 *                       The callbacks fwRegisteredCb() and rpcCmdComplete() are
 *                       populated by the application as part of the Eth_ConfigType
 *                       structure
 *  \endverbatim
 */
FUNC(Std_ReturnType, ETH_CODE)
Eth_DispatchVirtmacDelAddr(uint8 CtrlIdx, uint8 *macAddress, uint32 vlanId);
#endif  /* ((STD_ON == ETH_VIRTUALMAC_SUPPORT) && (STD_ON == ETH_VIRTUALMAC_DEL_MACADDR_API)) */

#if ((STD_ON == ETH_VIRTUALMAC_SUPPORT) && (STD_ON == ETH_VIRTUALMAC_ADD_VLAN_API))
/**
 *  \brief Dispatch virtual mac addition of given vlan id to switch address resolution table request to ethernet firmware
 *
 *  \verbatim
 *  Service name      : Eth_DispatchVirtmacAddVlan
 *  Syntax            : Std_ReturnType Eth_DispatchVirtmacAddVlan(CtrlIdx, vlanId, portList)
 *  Mode              : Supervisor Mode (Privileged Mode)
 *  Service ID[hex]   : 0x28
 *  Sync/Async        : Asynchronous
 *  Reentrancy        : Non-Reentrant
 *  Parameters (in)   : CtrlIdx: Index of the controller within the context of the
 *                                Ethernet Driver
 *                      vlanId:  Vlan Id for which port membership is to be added to switch VLAN membership table
 *                      portList: List of ports in the switch added to membership of the given vlan id
 *  Parameters (inout): None
 *  Parameters (out)  : None
 *  Return value      :  Std_ReturnType
 *                        E_OK: success
 *                        E_NOT_OK: transmission failed
 *
 *  Description       :  The function dispatches RPC command to ethernet
 *                       firmware core to add the given vlan id
 *                       to the switch address resolution table with the given
 *                       portlist as members of the vlan.
 *                       On addition of entry switch would switch frames
 *                       with the given vlan only within the vlan membership
 *                       specified by the port list. This applies to both
 *                       unicast and multicast address
 *                       If configured in virtual mac mode application should
 *                       invoke this API as follows:
 *                       1. Eth_VirtMacRpcInit()
 *                       2. Wait for fwRegisteredCb() callback
 *                       3. Eth_DispatchVirtmacInit
 *                       4. Wait for rpcCmdComplete() callback with status E_OK
 *                       5. Eth_Init()
 *                       6. Eth_DispatchVirtmacSubscribeDstMac()
 *                       7. Wait for rpcCmdComplete() callback with status E_OK
 *                       8. Eth_DispatchVirtmacAddVlan()
 *                       9. Wait for rpcCmdComplete() callback with status E_OK
 *                       10. Eth_SetControllerMode(ETH_MODE_ACTIVE)
 *                       The callbacks fwRegisteredCb() and rpcCmdComplete() are
 *                       populated by the application as part of the Eth_ConfigType
 *                       structure
 *                       Note: The API adds the given portList also as the
 *                       unregisterMcastFloodMask and registeredMulticastFloodMask
 *                       The VLAN entry in the switch in added as inner vlan entry
 *                       Further the following are hardcoded for the VLAN entry
 *                           forceUntaggedEgressMask  = 0
 *                           noLearnMask              = 0
 *                           vidIngressCheck          = FALSE
 *                           limitIPNxtHdr            = FALSE
 *                           disallowIPFrag           = FALSE
 *                       Refer to CPSW documentation for details on these params
 *  \endverbatim
 */
FUNC(Std_ReturnType, ETH_CODE)
Eth_DispatchVirtmacAddVlan(uint8 CtrlIdx, uint32 vlanId, Eth_PortListType *portList);
#endif  /* ((STD_ON == ETH_VIRTUALMAC_SUPPORT) && (STD_ON == ETH_VIRTUALMAC_ADD_VLAN_API)) */

#if ((STD_ON == ETH_VIRTUALMAC_SUPPORT) && (STD_ON == ETH_VIRTUALMAC_DEL_VLAN_API))
/**
 *  \brief Dispatch virtual mac removal of given vlanid from switch address resolution table request to ethernet firmware
 *
 *  \verbatim
 *  Service name      : Eth_DispatchVirtmacDelVlan
 *  Syntax            : Std_ReturnType Eth_DispatchVirtmacDelVlan(uint8 CtrlIdx, uint32 vlanId)
 *  Mode              : Supervisor Mode (Privileged Mode)
 *  Service ID[hex]   : 0x29
 *  Sync/Async        : Asynchronous
 *  Reentrancy        : Non-Reentrant
 *  Parameters (in)   : CtrlIdx: Index of the controller within the context of the
 *                                Ethernet Driver
 *                      vlanId: Vlan Id which is to be removed from switch VLAN membership table
 *  Parameters (inout): None
 *  Parameters (out)  : None
 *  Return value      :  Std_ReturnType
 *                        E_OK: success
 *                        E_NOT_OK: transmission failed
 *
 *  Description       :  The function dispatches RPC command to ethernet
 *                       firmware core to remove the given vlan id
 *                       from the switch vlan membership table
 *                       On removal of vlan membership entry switch would treat any
 *                       frame received with the given vlan as per unknownVlanMembership mask configured in
 *                       switch.Vlan id should have been added previously using Eth_DispatchVirtmacAddVlan()
 *                       If configured in virtual mac mode application should
 *                       invoke this API as follows:
 *                       1. Eth_VirtMacRpcInit()
 *                       2. Wait for fwRegisteredCb() callback
 *                       3. Eth_DispatchVirtmacInit
 *                       4. Wait for rpcCmdComplete() callback with status E_OK
 *                       5. Eth_Init()
 *                       6. Eth_DispatchVirtmacSubscribeDstMac()
 *                       7. Wait for rpcCmdComplete() callback with status E_OK
 *                       8. Eth_DispatchVirtmacAddVlan()
 *                       9. Wait for rpcCmdComplete() callback with status E_OK
 *                       10. Eth_DispatchVirtmacDelVlan()
 *                       11. Wait for rpcCmdComplete() callback with status E_OK
 *                       The callbacks fwRegisteredCb() and rpcCmdComplete() are
 *                       populated by the application as part of the Eth_ConfigType
 *                       structure
 *  \endverbatim
 */
FUNC(Std_ReturnType, ETH_CODE)
Eth_DispatchVirtmacDelVlan(uint8 CtrlIdx, uint32 vlanId);
#endif  /* ((STD_ON == ETH_VIRTUALMAC_SUPPORT) && (STD_ON == ETH_VIRTUALMAC_DEL_VLAN_API)) */


#if ((STD_ON == ETH_VIRTUALMAC_SUPPORT) && (STD_ON == ETH_VIRTUALMAC_SEND_CUSTOM_NOTIFY_API))
/**
 *  \brief Send a custom notification to the ethernet firmware with given notify info and notify len
 *
 *  \verbatim
 *  Service name      : Eth_SendCustomNotify
 *  Syntax            : Std_ReturnType Eth_SendCustomNotify(uint8 CtrlIdx, void *notifyInfo, uint32 notifyLen)
 *  Mode              : Supervisor Mode (Privileged Mode)
 *  Service ID[hex]   : 0x28
 *  Sync/Async        : Asynchronous
 *  Reentrancy        : Non-Reentrant
 *  Parameters (in)   : CtrlIdx: Index of the controller within the context of the
 *                                Ethernet Driver
 *                      notifyInfo: Notify info to be sent
 *                      notifyLen: Notify length
 *  Parameters (inout): None
 *  Parameters (out)  : None
 *  Return value      :  Std_ReturnType
 *                        E_OK: success
 *                        E_NOT_OK: transmission failed
 *
 *  Description       :  The function send a custom notify msg to the ethernet firmware.
 *                       Notify msgs are msgs which do not require response from ethernet firmware.
 *                       The notify info format should mstch the format expected by ethernet firmware application.
 *                       The driver does not interpret the notify info.This API
 *                       is typically used to send some debug info to the ethernet firmware like CPU load, request to dump switch statistics on UART console etc.
 *                       If configured in virtual mac mode application should
 *                       invoke this API as follows:
 *                       1. Eth_VirtMacRpcInit()
 *                       2. Wait for fwRegisteredCb() callback
 *                       3. Eth_DispatchVirtmacInit
 *                       4. Wait for rpcCmdComplete() callback with status E_OK
 *                       5. Eth_SendCustomNotify()
 *                       The callbacks fwRegisteredCb() and rpcCmdComplete() are
 *                       populated by the application as part of the Eth_ConfigType
 *                       structure
 *  \endverbatim
 */
FUNC(Std_ReturnType, ETH_CODE)
Eth_SendCustomNotify(uint8 CtrlIdx, void *notifyInfo, uint32 notifyLen);
#endif  /* ((STD_ON == ETH_VIRTUALMAC_SUPPORT) && (STD_ON == ETH_VIRTUALMAC_SEND_CUSTOM_NOTIFY_API)) */

#if (STD_ON == ETH_USE_Q_APIS)
/**
 *  \brief This function retrieves a queue of received Ethernet frames.
 *
 *  \verbatim
 *  Service name      : Eth_RetrieveRxReadyQ
 *  Syntax            : Std_ReturnType Eth_RetrieveRxReadyQ(
 *                          uint8 CtrlIdx,
 *                          Eth_PktQ *retrievePktQ)
 *  Mode              : Supervisor Mode (Privileged Mode)
 *  Service ID[hex]   : 0x2C
 *  Sync/Async        : Synchronous
 *  Reentrancy        : Non Reentrant
 *  Parameters (in)   : CtrlIdx. Index of the controller within the context of
 *                               the Ethernet Driver
 *  Parameters (inout): retrievePktQ. Packet queue to hold the received packets
 *  Parameters (out)  : None
 *  Return value      : Std_ReturnType
 *                        E_OK: success
 *                        E_NOT_OK: transmission failed
 *  Description       : Retrieves received Ethernet frames in a queue. The queue
 *                      based approach allows retrieval of multiple frames at
 *                      once, which isn't possible with single frame oriented
 *                      Eth_Receive() function.
 *  \endverbatim
 */
FUNC(Std_ReturnType, ETH_CODE)
Eth_RetrieveRxReadyQ(uint8 CtrlIdx,
                     Eth_PktQ *retrievePktQ);
#endif /* (STD_ON == ETH_USE_Q_APIS) */

#if (STD_ON == ETH_USE_Q_APIS)
/**
 *  \brief This function submits a queue of free Ethernet frames for future
 *         reception.
 *
 *  \verbatim
 *  Service name      : Eth_SubmitRxFreeQ
 *  Syntax            : Std_ReturnType Eth_SubmitRxFreeQ(
 *                          uint8 CtrlIdx,
 *                          Eth_PktQ *submitPktQ)
 *  Mode              : Supervisor Mode (Privileged Mode)
 *  Service ID[hex]   : 0x2D
 *  Sync/Async        : Synchronous
 *  Reentrancy        : Non Reentrant
 *  Parameters (in)   : CtrlIdx. Index of the controller within the context of
 *                               the Ethernet Driver
 *  Parameters (inout): submitPktQ. Packet queue that holds free packets
 *  Parameters (out)  : None
 *  Return value      : Std_ReturnType
 *                        E_OK: success
 *                        E_NOT_OK: transmission failed
 *  Description       : Submits free Ethernet frames in a queue for future frame
 *                      reception. This function should be called after the
 *                      application has consumed the frames received via
 *                      Eth_RetrieveRxReadyQ() and is ready to recycle them back
 *                      to the driver.
 *  \endverbatim
 */
FUNC(Std_ReturnType, ETH_CODE)
Eth_SubmitRxFreeQ(uint8 CtrlIdx,
                  Eth_PktQ *submitPktQ);
#endif /* (STD_ON == ETH_USE_Q_APIS) */

#if (STD_ON == ETH_USE_Q_APIS)
/**
 *  \brief This function submits a queue of Ethernet frames for transmission.
 *
 *  \verbatim
 *  Service name      : Eth_SubmitTxReadyQ
 *  Syntax            : Std_ReturnType Eth_SubmitTxReadyQ(
 *                          uint8 CtrlIdx,
 *                          Eth_PktQ *submitPktQ)
 *  Mode              : Supervisor Mode (Privileged Mode)
 *  Service ID[hex]   : 0x2E
 *  Sync/Async        : Synchronous
 *  Reentrancy        : Non Reentrant
 *  Parameters (in)   : CtrlIdx. Index of the controller within the context of
 *                               the Ethernet Driver
 *  Parameters (inout): submitPktQ. Packet queue that holds packets ready for
 *                                  transmission
 *  Parameters (out)  : None
 *  Return value      : Std_ReturnType
 *                        E_OK: success
 *                        E_NOT_OK: transmission failed
 *  Description       : Submits a queue of Ethernet frames to be transmitted.
 *                      The queue based approach allows submission of multiple
 *                      frames at once, which isn't possible with single frame
 *                      oriented Eth_Transmit() function.
 *  \endverbatim
 */
FUNC(Std_ReturnType, ETH_CODE)
Eth_SubmitTxReadyQ(uint8 CtrlIdx,
                   Eth_PktQ *submitPktQ);
#endif /* (STD_ON == ETH_USE_Q_APIS) */

#if (STD_ON == ETH_USE_Q_APIS)
/**
 *  \brief This function retrieves a queue of Ethernet frames that have already
 *         being used for transmission.
 *
 *  \verbatim
 *  Service name      : Eth_RetrieveTxDoneQ
 *  Syntax            : Std_ReturnType Eth_RetrieveTxDoneQ(
 *                          uint8 CtrlIdx,
 *                          Eth_PktQ *retrievePktQ)
 *  Mode              : Supervisor Mode (Privileged Mode)
 *  Service ID[hex]   : 0x2F
 *  Sync/Async        : Synchronous
 *  Reentrancy        : Non Reentrant
 *  Parameters (in)   : CtrlIdx. Index of the controller within the context of
 *                               the Ethernet Driver
 *  Parameters (inout): retrievePktQ. Packet queue to hold packets that have
 *                                    already been used for transmission
 *  Parameters (out)  : None
 *  Return value      : Std_ReturnType
 *                        E_OK: success
 *                        E_NOT_OK: transmission failed
 *  Description       : Retrieves a queue of Ethernet frames that have been used
 *                      for transmission and can be recycled by the application
 *                      for future transmission. This function should be called
 *                      to recycle packets which were previously submitted for
 *                      transmission via Eth_SubmitTxReadyQ().
 *  \endverbatim
 */
FUNC(Std_ReturnType, ETH_CODE)
Eth_RetrieveTxDoneQ(uint8 CtrlIdx,
                    Eth_PktQ *retrievePktQ);
#endif /* (STD_ON == ETH_USE_Q_APIS) */

#ifdef __cplusplus
}
#endif

#endif  /* ETH_H_ */

/* @} */
