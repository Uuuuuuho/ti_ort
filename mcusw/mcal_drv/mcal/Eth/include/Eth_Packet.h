/*
*
* Copyright (c) 2019 Texas Instruments Incorporated
*
* All rights reserved not granted herein.
*
* Limited License.
*
* Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
* license under copyrights and patents it now or hereafter owns or controls to make,
* have made, use, import, offer to sell and sell ("Utilize") this software subject to the
* terms herein.  With respect to the foregoing patent license, such license is granted
* solely to the extent that any such patent is necessary to Utilize the software alone.
* The patent license shall not apply to any combinations which include this software,
* other than combinations with devices manufactured by or for TI ("TI Devices").
* No hardware patent is licensed hereunder.
*
* Redistributions must preserve existing copyright notices and reproduce this license
* (including the above copyright notice and the disclaimer and (if applicable) source
* code license limitations below) in the documentation and/or other materials provided
* with the distribution
*
* Redistribution and use in binary form, without modification, are permitted provided
* that the following conditions are met:
*
* *       No reverse engineering, decompilation, or disassembly of this software is
* permitted with respect to any software provided in binary form.
*
* *       any redistribution and use are licensed by TI for use only with TI Devices.
*
* *       Nothing shall obligate TI to provide you with source code for the software
* licensed and provided to you in object code.
*
* If software source code is provided to you, modification and redistribution of the
* source code are permitted provided that the following conditions are met:
*
* *       any redistribution and use of the source code, including any resulting derivative
* works, are licensed by TI for use only with TI Devices.
*
* *       any redistribution and use of any object code compiled from the source code
* and any resulting derivative works, are licensed by TI for use only with TI Devices.
*
* Neither the name of Texas Instruments Incorporated nor the names of its suppliers
*
* may be used to endorse or promote products derived from this software without
* specific prior written permission.
*
* DISCLAIMER.
*
* THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
* OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
* OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
* IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
* BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
* OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
* OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
* OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/**
 *  \file     Eth_Packet.h
 *
 *  \brief    This file contains the structure definitions and function
 *            prototypes of the Ethernet packet and packet queue abstactions.
 */

#ifndef ETH_PACKET_H_
#define ETH_PACKET_H_

/* ========================================================================== */
/*                             Include Files                                  */
/* ========================================================================== */

#include <Eth_Types.h>

#ifdef __cplusplus
extern "C" {
#endif

/* ========================================================================== */
/*                                 Macros                                     */
/* ========================================================================== */

/* None */

/* ========================================================================== */
/*                         Structures and Enums                               */
/* ========================================================================== */

/**
 *  \brief Ethernet frame header
 *
 *  Ethernet frame header without VLAN tag
 */
typedef struct
{
    uint8 dstMac[ETH_MAC_ADDR_LEN];
    /**< Destination MAC address */
    uint8 srcMac[ETH_MAC_ADDR_LEN];
    /**< Source MAC address */
    Eth_FrameType etherType;
    /**< EtherType field */
} Eth_FrameHeader;

/**
 *  \brief Ethernet frame
 *
 *  Ethernet frame without VLAN tag
 */
typedef struct
{
    Eth_FrameHeader hdr;
    /**< Ethernet frame header */
    uint8 payload[0U];
    /**< Pointer to the frame's payload */
} __attribute__((packed)) Eth_Frame;

/**
 *  \brief VLAN tag
 *
 *  VLAN tag (802.1Q)
 */
typedef struct
{
    uint16 tpid;
    /**< Tag protocol identifier (TPID) */
    uint16 tci;
    /**< Tag control information (TCI) */
} Eth_VlanTag;

/**
 *  \brief VLAN-tagged Ethernet frame header
 *
 *  Ethernet frame header with VLAN tag
 */
typedef struct
{
    uint8 dstAddr[ETH_MAC_ADDR_LEN];
    /**< Destination MAC address */
    uint8 srcAddr[ETH_MAC_ADDR_LEN];
    /**< Source MAC address */
    Eth_VlanTag vlanTag;
    /**< VLAN tag */
    Eth_FrameType etherType;
    /**< EtherType field */
} Eth_VlanFrameHeader;

/**
 *  \brief VLAN-tagged Ethernet frame
 *
 *  Ethernet frame with VLAN tag
 */
typedef struct
{
    Eth_VlanFrameHeader header;
    /**< Ethernet frame header with VLAN tag */
    uint8 payload[0U];
    /**< Pointer to the frame's payload */
} Eth_VlanFrame;

/**
 *  \brief Ethernet buffer state
 *
 *  The state of the Ethernet buffer.
 */
typedef enum
{
    ETH_BUF_STATE_FREE   = 0U,
    /**< Buffer is free for allocation */
    ETH_BUF_STATE_IN_USE = 1U,
    /**< Buffer is in use */
    ETH_BUF_STATE_QUEUED = 2U,
    /**< Buffer has been queued for transmit */
} Eth_BufState;

/**
 *  \brief Ethernet Packet
 *
 *  Ethernet packet abstraction that represents the Ethernet buffer passed
 *  to the Eth driver along with misc information associated with it, such
 *  as the buffer index, whether txConfirmation is needed, buffer state, etc.
 */
typedef struct Eth_Pkt_s
{
    struct Eth_Pkt_s *next;
    /**< Pointer to the next Eth packet */
    void *buf;
    /**< Data buffer pointer */
    uint32 len;
    /**< Original data buffer length at allocation time */
    uint32 userLen;
    /**< User buffer length.  In transmission, it gets set with the Ethernet
     *   frame size.  In reception, it gets set with the received frame size */
    Eth_BufIdxType idx;
    /**< Buffer index, applicable only to tranmission buffers.  This index is
     *   used as an unique identifier of the packet for tranmission */
    Eth_BufState state;
    /**< Buffer state */
    boolean txConfirmation;
    /**< EthIf_TxConfirmation() call needed for this buffer.  Applicable only
     *   to transmission buffers */
#if (STD_ON == ETH_ZERO_COPY_TX)
    boolean txNoCpy;
    /**< Whether packet is being transmitted via Eth_TransmitNoCpy() */
#endif
} Eth_Pkt;

/**
 *  \brief Ethernet Packet Queue
 *
 *  Queue of Ethernet packets.
 */
typedef struct
{
    uint32 count;
    /**< Number of packets in the queue */
    Eth_Pkt *head;
    /**< Pointer to the head of the queue */
    Eth_Pkt *tail;
    /**< Pointer to the tail of  the queue */
} Eth_PktQ;

/* ========================================================================== */
/*                         Global Variables Declarations                      */
/* ========================================================================== */

/* None */

/* ========================================================================== */
/*                          Function Declarations                             */
/* ========================================================================== */

/**
 *  \brief Init Eth packet
 *
 *  Initializes an Eth packet.
 *
 *  \param pkt      Eth packet handle
 */
void EthPkt_init(Eth_Pkt *pkt);

/**
 *  \brief Init Eth packet queue
 *
 *  Initializes a Eth packet queue.
 *
 *  \param queue    Eth packet queue handle
 */
void EthPktQ_init(Eth_PktQ *queue);

/**
 *  \brief Enqueue a packet into the Eth packet queue
 *
 *  Enqueues a packet into the Eth packet queue.
 *
 *  \param queue    Eth packet queue handle
 *  \param pkt      Eth packet handle
 */
void EthPktQ_queue(Eth_PktQ *queue,
                   Eth_Pkt *pkt);

/**
 *  \brief Dequeue a packet from the Eth packet queue
 *
 *  Dequeues a packet from the Eth packet queue.  A null pointer is
 *  returned if the queue was already empty.
 *
 *  \param queue    Eth packet queue handle
 *
 *  \return         Eth packet handle or NULL if queue was empty
 */
Eth_Pkt *EthPktQ_dequeue(Eth_PktQ *queue);

/**
 *  \brief Get an indexed packet from the Eth packet queue
 *
 *  Gets a packet from the Eth packet queue that matches a given index.
 *  A null pointer is returned if the queue was already empty or the
 *  indexed packet was not found.
 *
 *  \param queue    Eth packet queue handle
 *
 *  \return         Eth packet handle or NULL if queue was empty or
 *                  index was not found
 */
Eth_Pkt *EthPktQ_get(Eth_PktQ *queue,
                     Eth_BufIdxType idx);

/**
 *  \brief Peek into the Eth packet queue
 *
 *  Shows the first packet in the packet but doesn't dequeue it.  A null
 *  pointer is returned if the queue is empty.
 *
 *  \param queue    Eth packet queue handle
 *
 *  \return         Eth packet handle or NULL if queue was empty
 */
Eth_Pkt *EthPktQ_peek(Eth_PktQ *queue);

/**
 *  \brief Copy Eth packet queues
 *
 *  Copies packet queues.  The copied queue will have the same head/tail
 *  pointers and count as the source queue.
 *
 *  \param dst      Destination Eth packet handle
 *  \param src      Source Eth packet queue handle
 */
void EthPktQ_copy(Eth_PktQ *dst,
                  Eth_PktQ *src);

/**
 *  \brief Append Eth packet queue
 *
 *  Appends a packet queue into another queue.  The packets in the source
 *  queue are queued to the tail of the destination queue.
 *
 *  \param dst      Destination Eth packet handle
 *  \param src      Source Eth packet queue handle
 */
void EthPktQ_append(Eth_PktQ *dst,
                    Eth_PktQ *src);

/**
 *  \brief Get first packet of the Eth packet queue
 *
 *  Gets the first packet in the packet but doesn't dequeue it.  It is
 *  effectively the same operation as EthPktQ_peek().
 *
 *  \param queue    Eth packet queue handle
 *
 *  \return         Eth packet handle or NULL if queue was empty
 */
Eth_Pkt *EthPktQ_getFirstPkt(Eth_PktQ *queue);

/**
 *  \brief Get last packet of the Eth packet queue
 *
 *  Gets the last packet in the packet but doesn't dequeue it.
 *
 *  \param queue    Eth packet queue handle
 *
 *  \return         Eth packet handle or NULL if queue was empty
 */
Eth_Pkt *EthPktQ_getLastPkt(Eth_PktQ *queue);

/**
 *  \brief Get Eth packet queue count
 *
 *  Gets the number of packets in the queue.
 *
 *  \param queue    Eth packet queue handle
 *
 *  \return         Number of packets in the queue
 */
uint32 EthPktQ_getCount(Eth_PktQ *queue);

/* ========================================================================== */
/*                        Deprecated Function Declarations                    */
/* ========================================================================== */

/* None */

#ifdef __cplusplus
}
#endif

#endif /* ETH_PACKET_H_ */
