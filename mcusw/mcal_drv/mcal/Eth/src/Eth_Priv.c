/*
*
* Copyright (c) 2019 Texas Instruments Incorporated
*
* All rights reserved not granted herein.
*
* Limited License.
*
* Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
* license under copyrights and patents it now or hereafter owns or controls to make,
* have made, use, import, offer to sell and sell ("Utilize") this software subject to the
* terms herein.  With respect to the foregoing patent license, such license is granted
* solely to the extent that any such patent is necessary to Utilize the software alone.
* The patent license shall not apply to any combinations which include this software,
* other than combinations with devices manufactured by or for TI ("TI Devices").
* No hardware patent is licensed hereunder.
*
* Redistributions must preserve existing copyright notices and reproduce this license
* (including the above copyright notice and the disclaimer and (if applicable) source
* code license limitations below) in the documentation and/or other materials provided
* with the distribution
*
* Redistribution and use in binary form, without modification, are permitted provided
* that the following conditions are met:
*
* *       No reverse engineering, decompilation, or disassembly of this software is
* permitted with respect to any software provided in binary form.
*
* *       any redistribution and use are licensed by TI for use only with TI Devices.
*
* *       Nothing shall obligate TI to provide you with source code for the software
* licensed and provided to you in object code.
*
* If software source code is provided to you, modification and redistribution of the
* source code are permitted provided that the following conditions are met:
*
* *       any redistribution and use of the source code, including any resulting derivative
* works, are licensed by TI for use only with TI Devices.
*
* *       any redistribution and use of any object code compiled from the source code
* and any resulting derivative works, are licensed by TI for use only with TI Devices.
*
* Neither the name of Texas Instruments Incorporated nor the names of its suppliers
*
* may be used to endorse or promote products derived from this software without
* specific prior written permission.
*
* DISCLAIMER.
*
* THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
* OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
* OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
* IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
* BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
* OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
* OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
* OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/**
 *  \file     Eth_Priv.c
 *
 *  \brief    This file contains the implementation of internal helper
 *            functions of the Ethernet driver.
 */

/* ========================================================================== */
/*                             Include Files                                  */
/* ========================================================================== */

#include <EthIf_Cbk.h>
#include <Dem.h>
#include <Os.h>

#include "Eth_Priv.h"
#include <ti/drv/enet/enet.h>
#include "cpsw/include/cpsw/Cpsw_Soc.h"

/* ========================================================================== */
/*                           Macros & Typedefs                                */
/* ========================================================================== */

/**
 *  \brief Ethernet frame length
 *
 *  Maximum frame length in bytes.
 */
#if (ETH_TX_BUF_LEN_BYTE > ETH_RX_BUF_LEN_BYTE)
#define ETH_FRAME_LEN                   (ETH_TX_BUF_LEN_BYTE)
#else
#define ETH_FRAME_LEN                   (ETH_RX_BUF_LEN_BYTE)
#endif

/**
 *  \brief Number of Ethernet TX packets
 *
 *  Number of packets used by the Eth driver for frame transmission.  Each
 *  packet is represented by an Eth_Pkt structure and its corresponding data
 *  buffer.
 */
#define ETH_TX_PKT_NUM                  (ETH_NUM_TX_BUFFERS)

/**
 * \brief Number of Ethernet RX packets
 *
 *  Number of packets used by the Eth driver for frame reception.  Each
 *  packet is represented by an Eth_Pkt structure and its corresponding data
 *  buffer.
 *
 *  A 2x factor is used in a ping-pong fashion to ensure that CPSW always
 *  have packets ready to be filled with incoming frames
 */
#define ETH_RX_PKT_NUM                  (2 * ETH_NUM_RX_BUFFERS)

/**
 *  \brief Number of CPSW TX packets
 *
 *  Number of packets used by the CPSW driver for frame transmission.  It's
 *  the maximum amount of packets that can be submitted to the CPSW at one
 *  given time.
 */
#define CPSW_TX_PKT_NUM                 (ETH_NUM_RX_BUFFERS)

/**
 *  \brief Number of CPSW RX packets
 *
 *  Number of packets used by the CPSW driver for frame reception.  It's
 *  the maximum amount of packets that can be filled with incoming packets
 *  at one given time.  It must be large enough to ensure the CPSW always
 *  has packets ready to be filled.
 */
#define CPSW_RX_PKT_NUM                 (ETH_NUM_RX_BUFFERS)

/** \brief Number of entries in the TX Ring Acc's */
#define CPSW_TX_RING_ENTRIES            (CPSW_TX_PKT_NUM)

/** \brief Number of entries in the RX Ring Acc's */
#define CPSW_RX_RING_ENTRIES            (CPSW_RX_PKT_NUM)

/** \brief VLAN Tag Protocol Identifier (TPID) */
#define ETH_VLAN_TPID                   (0x8100U)

/** \brief Macro to convert CPSW error codes to Std_ReturnType */
#define TO_STDRET(ret)                  ((CPSW_SOK == (ret)) ? E_OK : E_NOT_OK)

/** \brief Macro to configure default MDIO bus frequencu */
#define CPSW_MDIO_BUS_FREQ_DEFAULT      (2200000U)

/* ========================================================================== */
/*                         Structures and Enums                               */
/* ========================================================================== */

/* None */

/* ========================================================================== */
/*                 Internal Function Declarations                             */
/* ========================================================================== */

static Enet_Type Eth_getEnetType(const EthDrv *drv);
static CpswPort_Num Eth_getCpswPortNum(Eth_PortType port);

/* ========================================================================== */
/*                            Global Variables                                */
/* ========================================================================== */

/* Packet buffer memories */
#define ETH_START_SEC_TX_DATA
#include <Eth_MemMap.h>
static VAR(uint8, ETH_VAR_NO_INIT)
    gEthTxBufMem[ETH_TX_PKT_NUM][ETH_ALIGN(ETH_FRAME_LEN)] __attribute__((aligned(CPSW_MEM_ALIGNMENT)));
#define ETH_STOP_SEC_TX_DATA
#include <Eth_MemMap.h>

#define ETH_START_SEC_RX_DATA
#include <Eth_MemMap.h>
static VAR(uint8, ETH_VAR_NO_INIT)
    gEthRxBufMem[ETH_RX_PKT_NUM][ETH_ALIGN(ETH_FRAME_LEN)] __attribute__((aligned(CPSW_MEM_ALIGNMENT)));
#define ETH_STOP_SEC_RX_DATA
#include <Eth_MemMap.h>

/* Eth Packet memories */
static Eth_Pkt gEthTxPkt[ETH_TX_PKT_NUM];
static Eth_Pkt gEthRxPkt[ETH_RX_PKT_NUM];
#if (STD_ON == ETH_ZERO_COPY_TX)
static Eth_Pkt gEthTxPktNoCpy[ETH_TX_PKT_NUM];
#endif

/* CPSW Packet memories */
#define ETH_START_SEC_UDMA_DESC
#include <Eth_MemMap.h>
static VAR(CpswPkt, ETH_VAR_NO_INIT_UDMA)
    gEthCpswTxPkt[CPSW_TX_PKT_NUM] __attribute__((aligned(CPSW_MEM_ALIGNMENT)));
static VAR(CpswPkt, ETH_VAR_NO_INIT_UDMA)
    gEthCpswRxPkt[CPSW_RX_PKT_NUM] __attribute__((aligned(CPSW_MEM_ALIGNMENT)));
#define ETH_STOP_SEC_UDMA_DESC
#include <Eth_MemMap.h>

/* RingAcc memories */
#define ETH_START_SEC_UDMA_RING
#include <Eth_MemMap.h>

/* TX RingAcc memories */
static VAR(uint64_t, ETH_VAR_NO_INIT_UDMA)
    gEthTxFqRingMem[CPSW_TX_RING_ENTRIES] __attribute__((aligned(CPSW_MEM_ALIGNMENT)));
static VAR(uint64_t, ETH_VAR_NO_INIT_UDMA)
    gEthTxCqRingMem[CPSW_TX_RING_ENTRIES] __attribute__((aligned(CPSW_MEM_ALIGNMENT)));
static VAR(uint64_t, ETH_VAR_NO_INIT_UDMA)
    gEthTxTdCqRingMem[CPSW_TX_RING_ENTRIES] __attribute__((aligned(CPSW_MEM_ALIGNMENT)));

/* RX RingAcc memories */
static VAR(uint64_t, ETH_VAR_NO_INIT_UDMA)
    gEthRxFqRingMem[CPSW_RX_RING_ENTRIES] __attribute__((aligned(CPSW_MEM_ALIGNMENT)));
static VAR(uint64_t, ETH_VAR_NO_INIT_UDMA)
    gEthRxCqRingMem[CPSW_RX_RING_ENTRIES] __attribute__((aligned(CPSW_MEM_ALIGNMENT)));
static VAR(uint64_t, ETH_VAR_NO_INIT_UDMA)
    gEthRxTdCqRingMem[CPSW_RX_RING_ENTRIES] __attribute__((aligned(CPSW_MEM_ALIGNMENT)));

#define ETH_STOP_SEC_UDMA_RING
#include <Eth_MemMap.h>

static CpswMcal_Config cpswCfg = {
    /* ALE configuration */
    .aleCfg = {
        .flags = (ALE_FLAG_ENABLE |
                  ALE_FLAG_VLANAWARE),
        .portCfg = {
            { /* Port 0 */
                .state = CPSWMCAL_ALE_PORTSTATE_FORWARD,
            },
            { /* Port 1 */
                .state = CPSWMCAL_ALE_PORTSTATE_FORWARD,
            },
        },
    },

    /* DMA configuration */
    .dmaCfg = {
        .cacheOps = {
            .cacheWbInv = NULL,
            .cacheWb = NULL,
            .cacheInv = NULL,
        },
    },

    /* DMA: TX channel configuration */
    .dmaTxChCfg = {
        .pktMem       = gEthCpswTxPkt,
        .pktNum       = ARRAY_SIZE(gEthCpswTxPkt),
        .fqRingMem    = gEthTxFqRingMem,
        .cqRingMem    = gEthTxCqRingMem,
        .tdCqRingMem  = gEthTxTdCqRingMem,
        .ringEntryNum = ARRAY_SIZE(gEthTxFqRingMem),
        .intrNum      = CPSW_DMA_NO_INTR,
    },

    /* DMA: TX channel configuration */
    .dmaRxChCfg = {
        .pktMem       = gEthCpswRxPkt,
        .pktNum       = ARRAY_SIZE(gEthCpswRxPkt),
        .fqRingMem    = gEthRxFqRingMem,
        .cqRingMem    = gEthRxCqRingMem,
        .tdCqRingMem  = gEthRxTdCqRingMem,
        .ringEntryNum = ARRAY_SIZE(gEthRxFqRingMem),
        .intrNum      = CPSW_DMA_NO_INTR,
    },

    /* MAC configuration */
    .macCfg = {
        {
            .flags = MAC_FLAG_PASSCONTROL,
        },
    },

    /* MDIO configuration */
    .mdioCfg = {
        .busFreq = CPSW_MDIO_BUS_FREQ_DEFAULT,
    },

    /* Port configuration */
    .portCfg = {
        { /* Port 0 */
            .flags = (PORT_FLAG_TXCRCREMOVE |
                      PORT_FLAG_P0RXPAD),
        },
        { /* Port 1 */
            .macAddr = { 0 },
        }
    },
};

/* ========================================================================== */
/*                  Internal Function Definitions                             */
/* ========================================================================== */

/* None */

/* ========================================================================== */
/*                          Function Definitions                              */
/* ========================================================================== */

void Eth_initEthPktQs(EthDrv *drv)
{
    Eth_Pkt *pkt;
    uint32 i;

    /* Initialize all queues */
    CpswPktQ_init(&drv->txFreeDmaQ);
    EthPktQ_init(&drv->txFreeQ);
    EthPktQ_init(&drv->txGrantedQ);
    EthPktQ_init(&drv->txConfQ);
    EthPktQ_init(&drv->rxFreeQ);
    EthPktQ_init(&drv->rxReadyQ);

#if (STD_ON == ETH_ZERO_COPY_TX)
    /* Initialize TX Eth_Pkts for no-copy API and queue them to txFreeQ */
    EthPktQ_init(&drv->txFreeQNoCpy);

    for (i = 0U; i < ARRAY_SIZE(gEthTxPktNoCpy); i++)
    {
        pkt = &gEthTxPktNoCpy[i];

        EthPkt_init(pkt);
        pkt->buf = NULL;
        pkt->idx = i;
        pkt->state = ETH_BUF_STATE_FREE;

        EthPktQ_queue(&drv->txFreeQNoCpy, pkt);
    }
#endif

    /* Initialize TX Eth_Pkts and queue them to txFreeQ */
    for (i = 0U; i < ARRAY_SIZE(gEthTxPkt); i++)
    {
        pkt = &gEthTxPkt[i];

        EthPkt_init(pkt);
        pkt->buf = (void *)&gEthTxBufMem[i][0U];
        pkt->len = ETH_FRAME_LEN;
        pkt->idx = i;
        pkt->state = ETH_BUF_STATE_FREE;

        EthPktQ_queue(&drv->txFreeQ, pkt);
    }

    /* Initialize RX Eth_Pkts and queue them to rxFreeQ */
    for (i = 0U; i < ARRAY_SIZE(gEthRxPkt); i++)
    {
        pkt = &gEthRxPkt[i];

        EthPkt_init(pkt);
        pkt->buf = (void *)&gEthRxBufMem[i][0U];
        pkt->len = ETH_FRAME_LEN;
        pkt->idx = i;
        pkt->state = ETH_BUF_STATE_FREE;

        EthPktQ_queue(&drv->rxFreeQ, pkt);
    }
}

static void Eth_initVirtualMacConfig (CpswMcal_Config *cpswCfg, EthDrv *drv, Eth_ConfigType *ethCfg)
{
    cpswCfg->virtualMacMode = TRUE;
    cpswCfg->dmaTxChCfg.txChPeerThreadId =  drv->virtualMacInfo.txPSILThreadId;
    cpswCfg->dmaRxChCfg.virtualMacConfig.startIdx = drv->virtualMacInfo.rxFlowStartIdx;
    cpswCfg->dmaRxChCfg.virtualMacConfig.flowIdx  = drv->virtualMacInfo.rxFlowIdx;
}

Std_ReturnType Eth_openCpsw(EthDrv *drv,
                            Eth_ConfigType *cfg)
{
    Cpsw *cpsw = &drv->cpsw;
    CpswAle *ale = Cpsw_getAle(cpsw);
    CpswMdio *mdio = Cpsw_getMdio(cpsw);
    CpswDma *dma = Cpsw_getDma(cpsw);
#if (STD_ON == ETH_ENABLE_TX_INTERRUPT)
    CpswDmaCh *txCh = CpswDma_getCh(dma, CPSW_DIR_TX);
#endif
    CpswDmaCh *rxCh = CpswDma_getCh(dma, CPSW_DIR_RX);
    uint32 chNum;
    sint32 ret;
    uint32 i;
    Enet_Type enetType;

    /* Initialize MII operation state */
    drv->miiAccess.isActive = FALSE;

    /* Initialize CPSW params with configurable params */
    cpswCfg.dmaCfg.cacheOps.cacheWbInv = (CpswDma_CacheWbInv)drv->cacheWbInv;
    cpswCfg.dmaCfg.cacheOps.cacheWb = (CpswDma_CacheWb)drv->cacheWb;
    cpswCfg.dmaCfg.cacheOps.cacheInv = (CpswDma_CacheInv)drv->cacheInv;

#if (STD_ON == ETH_ENABLE_TX_INTERRUPT)
    cpswCfg.dmaTxChCfg.intrNum = cfg->dmaTxChIntrNum;
#endif
#if (STD_ON == ETH_ENABLE_RX_INTERRUPT)
    cpswCfg.dmaRxChCfg.intrNum = cfg->dmaRxChIntrNum;
#endif
    cpswCfg.mdioCfg.busFreq = cfg->mdioBusFreq;
    for (i = 0U; i < CPSW_PORT_MAX; i++)
    {
        cpswCfg.portCfg[i].pktMTU = ETH_FRAME_LEN;
    }
    for (i = 0U; i < CPSW_MAC_MAX; i++)
    {
        cpswCfg.macCfg[i].pktMTU = ETH_FRAME_LEN;
        cpswCfg.macCfg[i].connType = (CpswMac_ConnType)cfg->connType;
        if (cfg->loopback)
        {
            cpswCfg.macCfg[i].flags |= MAC_FLAG_MACLOOPBACK;
        }
    }

    enetType = Eth_getEnetType(drv);
    cpswCfg.udmaInstId = cfg->udmaInstId;
    if (cfg->enableVirtualMac)
    {
        Eth_initVirtualMacConfig(&cpswCfg, drv, cfg);
    }
    else
    {
        cpswCfg.dmaTxChCfg.txChPeerThreadId = CpswSoc_getTxChPeerId (enetType,
                                                                     0U /* instId */,
                                                                     0U /* chNum*/);
    }

#if (STD_ON == ETH_RING_IN_CACHED_MEMORY)
    /* Write back and invalidate ring memories (which could be placed in .cinit
     * section) in advance in order to avoid later cache evictions which could
     * corrupt rings filled via proxy */
    Eth_wbInvCache(drv, (uint8 *)gEthTxFqRingMem, sizeof(gEthTxFqRingMem));
    Eth_wbInvCache(drv, (uint8 *)gEthTxCqRingMem, sizeof(gEthTxCqRingMem));
    Eth_wbInvCache(drv, (uint8 *)gEthTxTdCqRingMem, sizeof(gEthTxTdCqRingMem));
    Eth_wbInvCache(drv, (uint8 *)gEthRxFqRingMem, sizeof(gEthRxFqRingMem));
    Eth_wbInvCache(drv, (uint8 *)gEthRxCqRingMem, sizeof(gEthRxCqRingMem));
    Eth_wbInvCache(drv, (uint8 *)gEthRxTdCqRingMem, sizeof(gEthRxTdCqRingMem));
#endif

    /* Open the CPSW driver */
    ret = CpswMcal_open(cpsw, &cpswCfg, enetType);

    /* Set ALE policer default thread and clear interrupts */
    if (CPSW_SOK == ret)
    {
        if (cfg->enableVirtualMac == FALSE)
        {
            chNum = CpswDma_getDefaultFlowNum(dma, rxCh);
            CpswAle_setDefThread(ale, chNum);
        }

#if (STD_ON == ETH_ENABLE_TX_INTERRUPT)
        CpswDma_clearIntr(dma, txCh);
#endif
#if (STD_ON == ETH_ENABLE_RX_INTERRUPT)
        CpswDma_clearIntr(dma, rxCh);
#endif
        if (cfg->enableVirtualMac == FALSE)
        {
            CpswMdio_clearUserInt(mdio);
        }
    }

    return TO_STDRET(ret);
}

void Eth_closeCpsw(EthDrv *drv)
{
    Cpsw *cpsw = &drv->cpsw;
    CpswMdio *mdio = Cpsw_getMdio(cpsw);
    CpswDma *dma = Cpsw_getDma(cpsw);
#if (STD_ON == ETH_ENABLE_TX_INTERRUPT)
    CpswDmaCh *txCh = CpswDma_getCh(dma, CPSW_DIR_TX);
#endif
#if (STD_ON == ETH_ENABLE_RX_INTERRUPT)
    CpswDmaCh *rxCh = CpswDma_getCh(dma, CPSW_DIR_RX);
#endif

    /* Clear pending interrupts on the way out */
#if (STD_ON == ETH_ENABLE_TX_INTERRUPT)
    CpswDma_clearIntr(dma, txCh);
#endif
#if (STD_ON == ETH_ENABLE_RX_INTERRUPT)
    CpswDma_clearIntr(dma, rxCh);
#endif
    CpswMdio_clearUserInt(mdio);

    drv->miiAccess.isActive = FALSE;

    CpswMcal_close(cpsw, drv->ethConfig.enableVirtualMac);
}

Std_ReturnType Eth_enableCpsw(EthDrv *drv)
{
    Cpsw *cpsw = &drv->cpsw;
    CpswDma *dma = Cpsw_getDma(cpsw);
    CpswPort *p0 = Cpsw_getPort(cpsw, CPSW_PORT0);
    CpswDmaCh *txCh = CpswDma_getCh(dma, CPSW_DIR_TX);
    CpswDmaCh *rxCh = CpswDma_getCh(dma, CPSW_DIR_RX);
    CpswPktQ tempPQ;
    CpswPkt *cpswPkt;
    Eth_Pkt *pkt;
    sint32 ret =  CPSW_SOK;
    Enet_Type enetType;

    enetType = Eth_getEnetType(drv);
    /* Open TX channel if needed (i.e. controller being reactivated) */
    if (FALSE == CpswDma_isInitCh(dma, txCh))
    {
        ret = CpswDma_openCh(dma, txCh, &cpswCfg.dmaTxChCfg, enetType, drv->ethConfig.enableVirtualMac);
    }

    /* Open RX channel if needed (i.e. controller being reactivated) */
    if ((CPSW_SOK == ret) && (FALSE == CpswDma_isInitCh(dma, rxCh)))
    {
        ret = CpswDma_openCh(dma, rxCh, &cpswCfg.dmaRxChCfg, enetType, drv->ethConfig.enableVirtualMac);
    }

    /* Retrieve all TX and RX packets */
    if (CPSW_SOK == ret)
    {
        CpswDma_reclaimPktQ(dma, txCh, &drv->txFreeDmaQ);
        CpswDma_reclaimPktQ(dma, rxCh, &tempPQ);

        /* Check if there are enough free RX packets */
        if (CpswPktQ_getCount(&tempPQ) > EthPktQ_getCount(&drv->rxFreeQ))
        {
            ret = CPSW_EUNEXPECTED;
        }
    }

    /* Set the RX buffer address and length */
    if (CPSW_SOK == ret)
    {
        cpswPkt = CpswPktQ_peek(&tempPQ);
        while (NULL != cpswPkt)
        {
            pkt = EthPktQ_dequeue(&drv->rxFreeQ);

            CpswPkt_setBuf(cpswPkt, pkt->buf, pkt->len);
            CpswPkt_setPriv(cpswPkt, pkt);
            Eth_invCache(drv, (uint8 *)pkt->buf, pkt->len);

            cpswPkt = cpswPkt->next;
        }
    }

    /* Resubmit RX packets */
    if (CPSW_SOK == ret)
    {
        ret = CpswDma_submitPktQ(dma, rxCh, &tempPQ);
    }

    /* Enable TX channel */
    if (CPSW_SOK == ret)
    {
        ret = CpswDma_enableCh(dma, txCh);
    }

    /* Enable RX channel */
    if (CPSW_SOK == ret)
    {
        if (drv->ethConfig.enableVirtualMac == FALSE)
        {
            ret = CpswDma_enableCh(dma, rxCh);
        }
    }

    /* Enable the host port */
    if (CPSW_SOK == ret)
    {
        if (drv->ethConfig.enableVirtualMac == FALSE)
        {
            CpswPort_enable(p0);
        }
    }
    else
    {
        Eth_disableCpsw(drv);
    }
    return TO_STDRET(ret);
}

void Eth_disableCpsw(EthDrv *drv)
{
    Cpsw *cpsw = &drv->cpsw;
    CpswDma *dma = Cpsw_getDma(cpsw);
    CpswPort *p0 = Cpsw_getPort(cpsw, CPSW_PORT0);
    CpswDmaCh *txCh = CpswDma_getCh(dma, CPSW_DIR_TX);
    CpswDmaCh *rxCh = CpswDma_getCh(dma, CPSW_DIR_RX);
    CpswPktQ tempQ;
    CpswPkt *cpswPkt;
    Eth_Pkt *pkt;


    if (drv->ethConfig.enableVirtualMac == FALSE)
    {
        /* Disable the host port */
        CpswPort_disable(p0);
    }

    /* Disable RX channel */
    CpswDma_disableCh(dma, rxCh);

    /* Close RX channel */
    CpswDma_closeCh(dma, rxCh);

    /* Disable TX channel */
    CpswDma_disableCh(dma, txCh);

    /* Close TX channel */
    CpswDma_closeCh(dma, txCh);

    /* Reclaim RX packets owned by CPSW */
    /* Requirements: MCAL-1545 */
    CpswPktQ_init(&tempQ);
    CpswDma_reclaimPktQ(dma, rxCh, &tempQ);

    cpswPkt = CpswPktQ_dequeue(&tempQ);
    while (NULL != cpswPkt)
    {
        pkt = (Eth_Pkt *)CpswPkt_getPriv(cpswPkt);
        if (NULL != pkt)
        {
            EthPktQ_queue(&drv->rxFreeQ, pkt);
        }

        cpswPkt = CpswPktQ_dequeue(&tempQ);
    }

    /* Reclaim TX packets owned by CPSW */
    /* Requirements: MCAL-1545, MCAL-1626 */
    CpswPktQ_init(&tempQ);
    CpswDma_reclaimPktQ(dma, txCh, &tempQ);

    cpswPkt = CpswPktQ_dequeue(&tempQ);
    while (NULL != cpswPkt)
    {
        pkt = (Eth_Pkt *)CpswPkt_getPriv(cpswPkt);
        if (NULL != pkt)
        {
            EthPktQ_queue(&drv->txFreeQ, pkt);
        }

        cpswPkt = CpswPktQ_dequeue(&tempQ);
    }
}

BufReq_ReturnType Eth_requestTxPkt(EthDrv *drv,
                                   uint8 ctrlIdx,
                                   Eth_Pkt **pkt,
                                   uint16 *len)
{
    Eth_Pkt *found;
    BufReq_ReturnType retVal = BUFREQ_OK;

    /* Check if free packets are available */
    /* Requirements: MCAL-1577 */
    if ((0U == EthPktQ_getCount(&drv->txFreeQ)) ||
        (0U == CpswPktQ_getCount(&drv->txFreeDmaQ)))
    {
        retVal = BUFREQ_E_BUSY;
    }

    /* Check if the requested buffer length is too large */
    if (BUFREQ_OK == retVal)
    {
        /* Requirements: MCAL-1576 */
        found = EthPktQ_peek(&drv->txFreeQ);
        if (found->len < *len)
        {
            *len = (uint16)found->len;
            retVal = BUFREQ_E_OVFL;
        }
    }

    /* Move packet to txGrantedQ and give the buffer to the caller */
    if (BUFREQ_OK == retVal)
    {
        /* Requirements: MCAL-1575 */
        found = EthPktQ_dequeue(&drv->txFreeQ);
        found->state = ETH_BUF_STATE_IN_USE;
        found->userLen = (uint32)*len;
        EthPktQ_queue(&drv->txGrantedQ, found);
        *pkt = found;
    }
    else
    {
        *pkt = NULL;
    }

    return retVal;
}

Std_ReturnType Eth_getGrantedTxPkt(EthDrv *drv,
                                   uint8 ctrlIdx,
                                   Eth_BufIdxType idx,
                                   Eth_Pkt **pkt)
{
    Std_ReturnType retVal = E_NOT_OK;

    *pkt = EthPktQ_get(&drv->txGrantedQ, idx);
    if (NULL != *pkt)
    {
        retVal = E_OK;
    }

    return retVal;
}

void Eth_releaseTxPkts(EthDrv *drv,
                       uint8 ctrlIdx)
{
    Eth_Pkt *pkt;

    pkt = EthPktQ_dequeue(&drv->txGrantedQ);
    while (NULL != pkt)
    {
        pkt->state = ETH_BUF_STATE_FREE;
        EthPktQ_queue(&drv->txFreeQ, pkt);
        pkt = EthPktQ_dequeue(&drv->txGrantedQ);
    }
}

Std_ReturnType Eth_transmitPktQ(EthDrv *drv,
                                uint8 ctrlIdx,
                                Eth_PktQ *submitPktQ)
{
    Cpsw *cpsw = &drv->cpsw;
    CpswDma *dma = Cpsw_getDma(cpsw);
    CpswDmaCh *txCh = CpswDma_getCh(dma, CPSW_DIR_TX);
    CpswPkt *cpswPkt;
    Eth_Pkt *ethPkt;
    sint32 ret = E_OK;
    Std_ReturnType retVal;
    CpswPktQ tempQ;

    CpswPktQ_init(&tempQ);

    if (NULL == submitPktQ)
    {
        retVal = E_NOT_OK;
    }
    else if (EthPktQ_getCount(submitPktQ) > CpswPktQ_getCount(&drv->txFreeDmaQ))
    {
        /* TODO: Handle underflow by keeping driver CPSW packet free Q */
        (void)Dem_SetEventStatus(ETH_E_TX_INTERNAL,
                                 DEM_EVENT_STATUS_PREFAILED);
        retVal = E_NOT_OK;
    }
    else
    {
        /* Get a CPSW packet, fill the buffer info and submit it */
        ethPkt = EthPktQ_peek(submitPktQ);
        while (NULL != ethPkt)
        {
            ethPkt = EthPktQ_dequeue(submitPktQ);
            cpswPkt = CpswPktQ_dequeue(&drv->txFreeDmaQ);
            if (NULL == cpswPkt)
            {
                /* Serious condition */
                (void)Dem_SetEventStatus(ETH_E_TX_INTERNAL,
                                      DEM_EVENT_STATUS_PREFAILED);
                ret = CPSW_EFAIL;
                break;
            }

            /* Fill the CPSW packet */
            CpswPkt_setBuf(cpswPkt, ethPkt->buf, ethPkt->userLen);
            CpswPkt_setPriv(cpswPkt, ethPkt);
            Eth_wbCache(drv, (uint8 *)ethPkt->buf, ethPkt->userLen);

            ethPkt->state = ETH_BUF_STATE_QUEUED;

            /* Submit the CPSW packet for transmission */
            CpswPktQ_queue(&tempQ, cpswPkt);
            ethPkt = EthPktQ_peek(submitPktQ);
        }
    }

    if (CPSW_SOK == ret)
    {
        /* Submit the CPSW packet for transmission */
        ret = CpswDma_submitPktQ(dma, txCh, &tempQ);
    }

    retVal = TO_STDRET(ret);

    return retVal;
}

Std_ReturnType Eth_transmitPkt(EthDrv *drv,
                               uint8 ctrlIdx,
                               Eth_Pkt *pkt)
{
    Cpsw *cpsw = &drv->cpsw;
    CpswDma *dma = Cpsw_getDma(cpsw);
    CpswDmaCh *txCh = CpswDma_getCh(dma, CPSW_DIR_TX);
    CpswPkt *cpswPkt;
    sint32 ret = CPSW_EBADARGS;
    Std_ReturnType retVal;

    if (NULL != pkt)
    {
        /* Get a CPSW packet, fill the buffer info and submit it */
        cpswPkt = CpswPktQ_dequeue(&drv->txFreeDmaQ);
        if (NULL != cpswPkt)
        {
            /* Fill the CPSW packet */
            CpswPkt_setBuf(cpswPkt, pkt->buf, pkt->userLen);
            CpswPkt_setPriv(cpswPkt, pkt);
            Eth_wbCache(drv, (uint8 *)pkt->buf, pkt->userLen);

            pkt->state = ETH_BUF_STATE_QUEUED;

            /* Submit the CPSW packet for transmission */
            ret = CpswDma_submitPkt(dma, txCh, cpswPkt);
        }
        else
        {
            ret = CPSW_EFAIL;
        }
    }

    retVal = TO_STDRET(ret);

    return retVal;
}

Std_ReturnType Eth_retrieveFreeTxPktQ(EthDrv *drv,
                                      uint8 ctrlIdx,
                                      Eth_PktQ *retrievePktQ)
{
    Cpsw *cpsw = &drv->cpsw;
    CpswDma *dma = Cpsw_getDma(cpsw);
    CpswDmaCh *txCh = CpswDma_getCh(dma, CPSW_DIR_TX);
    CpswPktQ tempQ;
    CpswPkt *cpswPkt;
    Eth_Pkt *pkt;
    boolean needsTxConf;
    sint32 ret;

    /* Retrieve any CPSW packets that may be free now */
    CpswPktQ_init(&tempQ);
    ret = CpswDma_retrievePktQ(dma, txCh, &tempQ);

    /* Issue a TX confirmation for the packets that requested it */
    if (CPSW_SOK == ret)
    {
        cpswPkt = CpswPktQ_dequeue(&tempQ);
        while (NULL != cpswPkt)
        {
            pkt = (Eth_Pkt *)CpswPkt_getPriv(cpswPkt);
            if (NULL != pkt)
            {
                if (NULL == retrievePktQ)
                {
                    needsTxConf = (TRUE == pkt->txConfirmation);
#if (STD_ON == ETH_ZERO_COPY_TX)
                    needsTxConf = needsTxConf || pkt->txNoCpy;
#endif
                    if (needsTxConf)
                    {
                        /* Requirements: MCAL-1598 */
                        EthPktQ_queue(&gEthDrv.txConfQ, pkt);
                    }
                    else
                    {
                        /* Requirements: MCAL-1586 */
                        pkt->state = ETH_BUF_STATE_FREE;
                        EthPktQ_queue(&drv->txFreeQ, pkt);
                    }
                }
                else
                {
                    EthPktQ_queue(retrievePktQ, pkt);
                }

                CpswPktQ_queue(&gEthDrv.txFreeDmaQ, cpswPkt);
            }
            else
            {
                /* Serious condition, an Eth packet was lost */
                (void)Dem_SetEventStatus(ETH_E_TX_INTERNAL,
                                      DEM_EVENT_STATUS_PREFAILED);
            }

            cpswPkt = CpswPktQ_dequeue(&tempQ);
        }
    }

    return ret;
}

Std_ReturnType Eth_receivePktQ(EthDrv *drv,
                               uint8 ctrlIdx,
                               Eth_PktQ *recvPktQ)
{
    sint32 ret = CPSW_EFAIL;
#if (STD_OFF == ETH_ENABLE_RX_INTERRUPT)
    Cpsw *cpsw = &drv->cpsw;
    CpswDma *dma = Cpsw_getDma(cpsw);
    CpswDmaCh *rxCh = CpswDma_getCh(dma, CPSW_DIR_RX);
    CpswPktQ tempQ;
    CpswPkt *cpswPkt;
    Eth_Pkt *pkt;

    /* Retrieve any CPSW packets which are ready */
    CpswPktQ_init(&tempQ);
    ret = CpswDma_retrievePktQ(dma, rxCh, &tempQ);

    /* Queue the received packet to recvPktQ and pass new ones from rxFreeQ */
    if (CPSW_SOK == ret)
    {
        cpswPkt = CpswPktQ_peek(&tempQ);
        while (NULL != cpswPkt)
        {
            /* Queue received packets to rxReady */
            pkt = (Eth_Pkt *)CpswPkt_getPriv(cpswPkt);
            if (NULL != pkt)
            {
                pkt->userLen = CpswPkt_getLen(cpswPkt);
                EthPktQ_queue(recvPktQ, pkt);
            }
            else
            {
                /* Serious condition, an Eth packet was lost */
                (void)Dem_SetEventStatus(ETH_E_RX_FRAMES_LOST,
                                      DEM_EVENT_STATUS_PREFAILED);
            }

            /* Fill new packets from rxFreeQ */
            pkt = EthPktQ_dequeue(&drv->rxFreeQ);
            if (NULL != pkt)
            {
                CpswPkt_setBuf(cpswPkt, pkt->buf, pkt->len);
                CpswPkt_setPriv(cpswPkt, pkt);
                Eth_invCache(drv, (uint8 *)pkt->buf, pkt->len);
            }
            else
            {
                /* If this happens, then we have CPSW packets with no buffer */
                /* TODO: Handle underflow by keeping driver CPSW packet free Q */
                (void)Dem_SetEventStatus(ETH_E_RX_FRAMES_LOST,
                                         DEM_EVENT_STATUS_PREFAILED);
            }

            cpswPkt = cpswPkt->next;
        }

        /* Submit CPSW packets with new free buffers */
        if (CpswPktQ_getCount(&tempQ) > 0)
        {
            ret = CpswDma_submitPktQ(dma, rxCh, &tempQ);
        }
    }
#endif /* (STD_OFF == ETH_ENABLE_RX_INTERRUPT) */

    return TO_STDRET(ret);
}

Eth_RxStatusType Eth_processRxPkt(EthDrv *drv,
                                  uint8 ctrlIdx)
{
    Eth_Pkt *pkt;
    Eth_Frame *frame;
    Eth_FrameType etherType;
    void *payload;
    boolean isBroadcast;
    uint16 len;
    Eth_RxStatusType status;

    pkt = EthPktQ_dequeue(&drv->rxReadyQ);
    if (NULL != pkt)
    {
        frame = (Eth_Frame *)pkt->buf;
        etherType = ntohs(frame->hdr.etherType);
        len = pkt->userLen - (uint16)sizeof(Eth_FrameHeader);
        payload = frame->payload;

        /* Requirements: MCAL-1640 */
        isBroadcast = Eth_isBcastMacAddr(frame->hdr.dstMac);

        /* Requirements: MCAL-1593, MCAL-4826 */
        EthIf_RxIndication(ctrlIdx,
                           etherType,
                           isBroadcast,
                           frame->hdr.srcMac,
                           (Eth_DataType *)payload,
                           len);

        EthPktQ_queue(&drv->rxFreeQ, pkt);

        pkt = EthPktQ_peek(&drv->rxReadyQ);
        if (NULL == pkt)
        {
            status = ETH_RECEIVED;
        }
        else
        {
            status = ETH_RECEIVED_MORE_DATA_AVAILABLE;
        }
    }
    else
    {
        status = ETH_NOT_RECEIVED;
    }

    return status;
}

Std_ReturnType Eth_checkControllerAccess(EthDrv *drv)
{
    Std_ReturnType retVal;
    Cpsw *cpsw = &drv->cpsw;
    CpswMcal_Version version;

    Cpsw_getVersion(cpsw, &version, Eth_getEnetType(drv));

    /* Requirements: SWS_Eth_00173 */
    if (version.major >= 1)
    {
        (void)Dem_SetEventStatus(ETH_E_ACCESS,
                                 DEM_EVENT_STATUS_PREPASSED);
        retVal = E_OK;
    }
    else
    {
        (void)Dem_SetEventStatus(ETH_E_ACCESS,
                                 DEM_EVENT_STATUS_PREFAILED);
        retVal = E_NOT_OK;
    }

    return retVal;
}

void Eth_checkControllerErrors(EthDrv *drv)
{
    Cpsw *cpsw = &drv->cpsw;
    CpswStats *stats = Cpsw_getStats(cpsw);
    CpswStats_Stats st;
    Eth_CtrlErrors *errors = &drv->errors;
    CpswPort_Num port;

    port = Eth_getCpswPortNum(drv->ethConfig.macPort);
    CpswStats_getStats(stats, port, &st);

    if (st.RxCRCErrors > errors->rxCRC)
    {
        (void)Dem_SetEventStatus(ETH_E_CRC,
                              DEM_EVENT_STATUS_PREFAILED);
    }

    if (st.RxDropBottom > errors->rxOverrun)
    {
        (void)Dem_SetEventStatus(ETH_E_RX_FRAMES_LOST,
                              DEM_EVENT_STATUS_PREFAILED);
    }

    if (st.RxUndersized > errors->rxUndersized)
    {
        (void)Dem_SetEventStatus(ETH_E_UNDERSIZEFRAME,
                              DEM_EVENT_STATUS_PREFAILED);
    }

    if (st.RxOversized > errors->rxOversized)
    {
        (void)Dem_SetEventStatus(ETH_E_OVERSIZEFRAME,
                              DEM_EVENT_STATUS_PREFAILED);
    }

    if (st.RxAlignCodeErrors > errors->rxAlignCode)
    {
        (void)Dem_SetEventStatus(ETH_E_ALIGNMENT,
                              DEM_EVENT_STATUS_PREFAILED);
    }

    if (st.TxSingleColl > errors->txSingleColl)
    {
        (void)Dem_SetEventStatus(ETH_E_SINGLECOLLISION,
                              DEM_EVENT_STATUS_PREFAILED);
    }

    if (st.TxMultiColl > errors->txMultiColl)
    {
        (void)Dem_SetEventStatus(ETH_E_MULTIPLECOLLISION,
                              DEM_EVENT_STATUS_PREFAILED);
    }

    if (st.TxLateColl > errors->txLateColl)
    {
        (void)Dem_SetEventStatus(ETH_E_LATECOLLISION,
                              DEM_EVENT_STATUS_PREFAILED);
    }

    errors->rxCRC        = st.RxCRCErrors;
    errors->rxOverrun    = st.RxDropBottom;
    errors->rxUndersized = st.RxUndersized;
    errors->rxOversized  = st.RxOversized;
    errors->rxAlignCode  = st.RxAlignCodeErrors;
    errors->txSingleColl = st.TxSingleColl;
    errors->txMultiColl  = st.TxMultiColl;
    errors->txLateColl   = st.TxLateColl;
}

void Eth_getRxStatsRFC2819(EthDrv *drv,
                           Eth_RxStatsType *RxStats)
{
    Cpsw *cpsw = &drv->cpsw;
    CpswStats *stats = Cpsw_getStats(cpsw);
    CpswStats_Stats st;
    CpswPort_Num port;

    port = Eth_getCpswPortNum(drv->ethConfig.macPort);

    CpswStats_getStats(stats, port, &st);

    RxStats->RxStatsDropEvents           = st.RxAleDrop +
                                           st.RxAleOverrunDrop +
                                           st.RxDropBottom +
                                           st.PortmaskDrop +
                                           st.RxDropTop +
                                           st.AleRateLimitDrop +
                                           st.AleVidDrop +
                                           st.AleAddrEqDrop;
    RxStats->RxStatsOctets               = st.RxOctets;
    RxStats->RxStatsPkts                 = st.RxGoodFrames +
                                           st.RxBCastFrames +
                                           st.RxMCastFrames;
    RxStats->RxStatsBroadcastPkts        = st.RxBCastFrames;
    RxStats->RxStatsMulticastPkts        = st.RxMCastFrames;
    RxStats->RxStatsCrcAlignErrors       = st.RxCRCErrors +
                                           st.RxAlignCodeErrors;
    RxStats->RxStatsUndersizePkts        = st.RxUndersized;
    RxStats->RxStatsOversizePkts         = st.RxOversized;
    RxStats->RxStatsFragments            = st.RxFragments;
    RxStats->RxStatsJabbers              = st.RxJabber;
    RxStats->RxStatsCollisions           = ETH_STATCNT_INVALID;
    RxStats->RxStatsPkts64Octets         = ETH_STATCNT_INVALID;
    RxStats->RxStatsPkts65to127Octets    = ETH_STATCNT_INVALID;
    RxStats->RxStatsPkts128to255Octets   = ETH_STATCNT_INVALID;
    RxStats->RxStatsPkts256to511Octets   = ETH_STATCNT_INVALID;
    RxStats->RxStatsPkts512to1023Octets  = ETH_STATCNT_INVALID;
    RxStats->RxStatsPkts1024to1518Octets = ETH_STATCNT_INVALID;
    RxStats->RxUnicastFrames             = st.RxGoodFrames -
                                           st.RxBCastFrames -
                                           st.RxMCastFrames;
}

void Eth_getTxStatsRFC1213(EthDrv *drv,
                           Eth_TxStatsType *TxStats)
{
    Cpsw *cpsw = &drv->cpsw;
    CpswStats *stats = Cpsw_getStats(cpsw);
    CpswStats_Stats st;
    CpswPort_Num port;

    port = Eth_getCpswPortNum(drv->ethConfig.macPort);

    CpswStats_getStats(stats, port, &st);

    TxStats->TxNumberOfOctets = st.TxOctets;
    TxStats->TxNUcastPkts     = st.TxBCastFrames +
                                st.TxMCastFrames;
    TxStats->TxUniCastPkts    = st.TxGoodFrames -
                                st.TxBCastFrames -
                                st.TxMCastFrames;
}

void Eth_getTxErrorCounterValues(EthDrv *drv,
                                 Eth_TxErrorCounterValuesType *TxErrorCounterValues)
{
    Cpsw *cpsw = &drv->cpsw;
    CpswStats *stats = Cpsw_getStats(cpsw);
    CpswStats_Stats st;
    CpswPort_Num port;
    uint32 i;

    port = Eth_getCpswPortNum(drv->ethConfig.macPort);

    CpswStats_getStats(stats, port, &st);

    /* Check drop count for all 8 priorities levels */
    for (i = 0U; i < 8U; i++)
    {
        TxErrorCounterValues->TxDroppedErrorPkts = st.TxPriDropPktCnt[i];
    }

    TxErrorCounterValues->TxDeferredTrans      = st.TxDeferred;
    TxErrorCounterValues->TxSingleCollision    = st.TxSingleColl;
    TxErrorCounterValues->TxMultipleCollision  = st.TxMultiColl;
    TxErrorCounterValues->TxLateCollision      = st.TxLateColl;
    TxErrorCounterValues->TxExcessiveCollison  = st.TxExcessiveColl;
    TxErrorCounterValues->TxDroppedNoErrorPkts = ETH_STATCNT_INVALID;
}

void Eth_getCounterValues(EthDrv *drv,
                          Eth_CounterType* CounterPtr)
{
    Cpsw *cpsw = &drv->cpsw;
    CpswStats *stats = Cpsw_getStats(cpsw);
    CpswStats_Stats st;
    CpswPort_Num port;

    port = Eth_getCpswPortNum(drv->ethConfig.macPort);

    CpswStats_getStats(stats, port, &st);

    CounterPtr->DropPktBufOverrun = st.RxDropBottom;
    CounterPtr->DropPktCrc        = st.RxCRCErrors;
    CounterPtr->UndersizePkt      = st.RxUndersized;
    CounterPtr->OversizePkt       = st.RxOversized;
    CounterPtr->AlgnmtErr         = st.RxAlignCodeErrors;
    CounterPtr->SqeTestErr        = ETH_STATCNT_INVALID;
    CounterPtr->DiscInbdPkt       = ETH_STATCNT_INVALID;
    CounterPtr->ErrInbdPkt        = st.RxAlignCodeErrors +
                                    st.RxOversized +
                                    st.RxUndersized +
                                    st.RxCRCErrors;
    CounterPtr->DiscOtbdPkt       = ETH_STATCNT_INVALID;
    CounterPtr->ErrOtbdPkt        = st.TxDeferred +
                                    st.TxCollision +
                                    st.TxCarrierSLoss;
    CounterPtr->SnglCollPkt       = st.TxSingleColl;
    CounterPtr->MultCollPkt       = st.TxMultiColl;
    CounterPtr->DfrdPkt           = st.TxDeferred;
    CounterPtr->LatCollPkt        = st.TxLateColl;
    CounterPtr->HwDepCtr0         = st.TxExcessiveColl;
    CounterPtr->HwDepCtr1         = st.TxCarrierSLoss;
    CounterPtr->HwDepCtr2         = ETH_STATCNT_INVALID;
    CounterPtr->HwDepCtr3         = ETH_STATCNT_INVALID;
}

boolean Eth_isBcastMacAddr(const uint8 *addr)
{
    uint32 i;
    boolean retVal = TRUE;

    for (i = 0U; i < ETH_MAC_ADDR_LEN; i++)
    {
        if (addr[i] != 0xFFU)
        {
            retVal = FALSE;
            break;
        }
    }

    return retVal;
}

boolean Eth_isMcastMacAddr(const uint8 *addr)
{
    boolean retVal;

    /* MSbit (right most bit) of the MSB (first byte) is 1, then
     * the address is multicast, otherwise it's unicast */
    if (0U != (addr[0U] & 1))
    {
        retVal = TRUE;
    }
    else
    {
        retVal = FALSE;
    }

    return retVal;
}

boolean Eth_isNullMacAddr(const uint8 *addr)
{
    uint32 i;
    boolean retVal = TRUE;

    for (i = 0U; i < ETH_MAC_ADDR_LEN; i++)
    {
        if (addr[i] != 0x00U)
        {
            retVal = FALSE;
            break;
        }
    }

    return retVal;
}

Std_ReturnType Eth_setMacAddr(EthDrv *drv,
                              const uint8 *addr)
{
    sint32 ret = CPSW_SOK;

    if (drv->ethConfig.enableVirtualMac == FALSE)
    {
        Cpsw *cpsw = &drv->cpsw;
        CpswPort_Num pn = Eth_getCpswPortNum(drv->ethConfig.macPort);
        CpswPort *port = Cpsw_getPort(cpsw, pn);

        /* Set the address to the MAC port */
        ret = CpswPort_setMacAddr(port, addr);
    }
    return TO_STDRET(ret);
}

Std_ReturnType Eth_getMacAddr(EthDrv *drv,
                              uint8 *addr)
{
    memcpy(addr, drv->macAddr, sizeof(drv->macAddr));

    return TO_STDRET(CPSW_SOK);
}

Std_ReturnType Eth_setPromiscuousMode(EthDrv *drv)
{
    Cpsw *cpsw = &drv->cpsw;
    CpswAle *ale = Cpsw_getAle(cpsw);

    /* Clear ALE table and enable bypass */
    CpswAle_clearTable(ale);
    CpswAle_setBypass(ale, TRUE);

    return E_OK;
}

Std_ReturnType Eth_setNonPromiscuousMode(EthDrv *drv,
                                         uint8 *addr)
{
    sint32 ret = CPSW_SOK;

    if (drv->ethConfig.enableVirtualMac == FALSE)
    {
        Cpsw *cpsw = &drv->cpsw;
        CpswAle *ale = Cpsw_getAle(cpsw);
        CpswPort_Num pn = Eth_getCpswPortNum(drv->ethConfig.macPort);
        CpswPort *port = Cpsw_getPort(cpsw, pn);
        uint32 portMask = ALE_PORT_MASK_BIT(CPSW_PORT0) |
                          ALE_PORT_MASK_BIT(pn);
        uint32 vlanId = 0U;
        const uint8 bcastAddr[ETH_MAC_ADDR_LEN] = {
            0xFFU, 0xFFU, 0xFFU, 0xFFU, 0xFFU, 0xFFU
        };

        /* Clear ALE table and disable bypass */
        CpswAle_clearTable(ale);
        CpswAle_setBypass(ale, FALSE);

        /* Add broadcast address entry in ALE for ports 0 and n */
        ret = CpswAle_addMcastAddr(ale, bcastAddr, vlanId, portMask,
                                   FALSE,
                                   CPSWMCAL_ALE_PORTSTATE_FORWARD);

        /* Add unicast entry in ALE for Port 0 */
        if (CPSW_SOK == ret)
        {
            ret = CpswAle_addUcastAddr(ale, CPSW_PORT0, addr, vlanId,
                                       FALSE, FALSE);
        }

        /* Set VLAN list and flood mask */
        if (CPSW_SOK == ret)
        {
            CpswAle_setUnknownVlan(ale, portMask);
            CpswAle_setUnknownRegMcastFlood(ale, portMask);
        }

        if (CPSW_SOK != ret)
        {
            CpswAle_clearTable(ale);
        }
    }
    return TO_STDRET(ret);
}

Std_ReturnType Eth_replaceUcastAddr(EthDrv *drv,
                                    const uint8 *oldAddr,
                                    const uint8 *newAddr)
{
    Cpsw *cpsw = &drv->cpsw;
    CpswAle *ale = Cpsw_getAle(cpsw);
    uint32 vlanId = 0U;
    sint32 ret = E_OK;

    if (drv->ethConfig.enableVirtualMac == FALSE)
    {
        /* Delete ALE entries for the old unicast address */
        CpswAle_removeUcastAddr(ale, oldAddr, vlanId);

        /* Add unicast entry in ALE for Port 0 */
        ret = CpswAle_addUcastAddr(ale, CPSW_PORT0, newAddr, vlanId,
                                   FALSE, FALSE);
    }
    return TO_STDRET(ret);
}

Std_ReturnType Eth_addFilterAddr(EthDrv *drv,
                                 const uint8 *addr)
{
    Cpsw *cpsw = &drv->cpsw;
    CpswAle *ale = Cpsw_getAle(cpsw);
    CpswPort_Num pn = Eth_getCpswPortNum(drv->ethConfig.macPort);
    uint32 vlanId = 0U;
    sint32 ret;

    if (TRUE == Eth_isMcastMacAddr(addr))
    {
        ret = CpswAle_addMcastAddr(ale, addr, vlanId,
                                   ALE_PORT_MASK_BIT(CPSW_PORT0) |
                                   ALE_PORT_MASK_BIT(pn),
                                   FALSE,
                                   CPSWMCAL_ALE_PORTSTATE_FORWARD);
    }
    else
    {
        ret = CpswAle_addUcastAddr(ale, CPSW_PORT0, addr, vlanId,
                                   FALSE, FALSE);
    }

    return TO_STDRET(ret);
}

Std_ReturnType Eth_delFilterAddr(EthDrv *drv,
                                 const uint8 *addr)
{
    Cpsw *cpsw = &drv->cpsw;
    CpswAle *ale = Cpsw_getAle(cpsw);
    CpswPort_Num pn = Eth_getCpswPortNum(drv->ethConfig.macPort);
    uint32 vlanId = 0U;
    sint32 ret;

    if (TRUE == Eth_isMcastMacAddr(addr))
    {
        ret = CpswAle_removeMcastAddr(ale, addr, vlanId,
                                      ALE_PORT_MASK_BIT(CPSW_PORT0) |
                                      ALE_PORT_MASK_BIT(pn));
    }
    else
    {
        ret = CpswAle_removeUcastAddr(ale, addr, vlanId);
    }

    return TO_STDRET(ret);
}

Std_ReturnType Eth_triggerMiiRead(EthDrv *drv,
                                  uint8 ctrlIdx,
                                  uint8 trcvIdx,
                                  uint8 regIdx)
{
    Cpsw *cpsw = &drv->cpsw;
    CpswMdio *mdio = Cpsw_getMdio(cpsw);
    TickType start = 0U;
    TickType elapsed = 0U;
    StatusType status;
    boolean alive;
    sint32 ret = CPSW_EFAIL;

    /* Initiate MDIO register read operation if the PHY is alive */
    alive = CpswMdio_isPhyAlive(mdio, trcvIdx);
    if (TRUE == alive)
    {
        ret = CpswMdio_initiatePhyRegRead(mdio, trcvIdx, regIdx);
        if (CPSW_EBUSY == ret)
        {
            status = GetCounterValue(ETH_OS_COUNTER_ID, &start);
            if (E_OK != status)
            {
                ret = CPSW_EFAIL;
            }

            while (CPSW_EBUSY == ret)
            {
                status = GetElapsedValue(ETH_OS_COUNTER_ID,
                                         &start,
                                         &elapsed);
                if (E_OK != status)
                {
                    ret = CPSW_EFAIL;
                    break;
                }

                if (elapsed >= ETH_TIMEOUT_DURATION)
                {
                    ret = CPSW_ETIMEOUT;
                    break;
                }

                ret = CpswMdio_initiatePhyRegRead(mdio, trcvIdx, regIdx);
            }
        }

        if (CPSW_SOK == ret)
        {
            drv->miiAccess.isActive = TRUE;
            drv->miiAccess.isRead = TRUE;
            drv->miiAccess.ctrlIdx = ctrlIdx;
            drv->miiAccess.trcvIdx = trcvIdx;
            drv->miiAccess.regIdx = regIdx;
        }
    }

    return TO_STDRET(ret);
}

Std_ReturnType Eth_triggerMiiWrite(EthDrv *drv,
                                   uint8 ctrlIdx,
                                   uint8 trcvIdx,
                                   uint8 regIdx,
                                   uint16 regVal)
{
    Cpsw *cpsw = &drv->cpsw;
    CpswMdio *mdio = Cpsw_getMdio(cpsw);
    TickType start = 0U;
    TickType elapsed = 0U;
    StatusType status;
    boolean alive;
    sint32 ret = CPSW_EFAIL;

    /* Initiate MDIO register write operation if the PHY is alive */
    alive = CpswMdio_isPhyAlive(mdio, trcvIdx);
    if (TRUE == alive)
    {
        ret = CpswMdio_initiatePhyRegWrite(mdio, trcvIdx, regIdx, regVal);
        if (CPSW_EBUSY == ret)
        {
            status = GetCounterValue(ETH_OS_COUNTER_ID, &start);
            if (E_OK != status)
            {
                ret = CPSW_EFAIL;
            }

            while (CPSW_EBUSY == ret)
            {
                status = GetElapsedValue(ETH_OS_COUNTER_ID,
                                         &start,
                                         &elapsed);
                if (E_OK != status)
                {
                    ret = CPSW_EFAIL;
                    break;
                }

                if (elapsed >= ETH_TIMEOUT_DURATION)
                {
                    ret = CPSW_ETIMEOUT;
                    break;
                }

                ret = CpswMdio_initiatePhyRegWrite(mdio, trcvIdx, regIdx, regVal);
            }
        }

        if (CPSW_SOK == ret)
        {
            drv->miiAccess.isActive = TRUE;
            drv->miiAccess.isRead = FALSE;
            drv->miiAccess.ctrlIdx = ctrlIdx;
            drv->miiAccess.trcvIdx = trcvIdx;
            drv->miiAccess.regIdx = regIdx;
        }
    }

    return TO_STDRET(ret);
}

Std_ReturnType Eth_getMiiReadVal(EthDrv *drv,
                                 uint8 ctrlIdx,
                                 uint16 *regVal)
{
    Cpsw *cpsw = &drv->cpsw;
    CpswMdio *mdio = Cpsw_getMdio(cpsw);
    sint32 ret;

    /* Get value read from PHY register */
    ret = CpswMdio_getPhyRegVal(mdio, regVal);

    return TO_STDRET(ret);
}

void Eth_clearMdioInt(EthDrv *drv,
                      uint8 ctrlIdx)
{
    Cpsw *cpsw = &drv->cpsw;
    CpswMdio *mdio = Cpsw_getMdio(cpsw);

    CpswMdio_clearUserInt(mdio);
}

void Eth_readMacAddr(EthDrv *drv, uint8 *addr)
{
    uint32 val;

    if (drv->ethConfig.enableVirtualMac == FALSE)
    {
        val = CSL_REG32_RD(&drv->mmrRegs->MAC_ID0);
        addr[5] = (uint8)((val & 0x000000FFU) >>  0U);
        addr[4] = (uint8)((val & 0x0000FF00U) >>  8U);
        addr[3] = (uint8)((val & 0x00FF0000U) >> 16U);
        addr[2] = (uint8)((val & 0xFF000000U) >> 24U);

        val = CSL_REG32_RD(&drv->mmrRegs->MAC_ID1);
        addr[1] = (uint8)((val & 0x000000FFU) >> 0U);
        addr[0] = (uint8)((val & 0x0000FF00U) >> 8U);
    }
    else
    {
        memcpy(addr, drv->virtualMacInfo.macAddress, sizeof(drv->virtualMacInfo.macAddress));
    }
}

static Enet_Type Eth_getEnetType(const EthDrv *drv)
{
    Enet_Type enetType;

    switch (drv->ethConfig.enetType)
    {
        case ETH_ENETTYPE_CPSW2G:
            enetType = ENET_CPSW_2G;
            break;
        case ETH_ENETTYPE_CPSW5G:
            enetType = ENET_CPSW_5G;
            break;
        case ETH_ENETTYPE_CPSW9G:
            enetType = ENET_CPSW_9G;
            break;
    }
    return enetType;
}


static CpswPort_Num Eth_getCpswPortNum(Eth_PortType port)
{
    CpswPort_Num portNum;

    switch (port)
    {
        case ETH_PORT_HOST_PORT:
             portNum = CPSW_PORT0;
             break;
        case ETH_PORT_MAC_PORT_1:
            portNum = CPSW_PORT1;
            break;
        /* TODO: Add additional ports when CPSW9G is supported in native MAC mode */
        default:
            portNum = CPSW_PORT0;
            break;
    }
    return portNum;
}
