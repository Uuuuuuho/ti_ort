/*
*
* Copyright (c) 2019 Texas Instruments Incorporated
*
* All rights reserved not granted herein.
*
* Limited License.
*
* Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
* license under copyrights and patents it now or hereafter owns or controls to make,
* have made, use, import, offer to sell and sell ("Utilize") this software subject to the
* terms herein.  With respect to the foregoing patent license, such license is granted
* solely to the extent that any such patent is necessary to Utilize the software alone.
* The patent license shall not apply to any combinations which include this software,
* other than combinations with devices manufactured by or for TI ("TI Devices").
* No hardware patent is licensed hereunder.
*
* Redistributions must preserve existing copyright notices and reproduce this license
* (including the above copyright notice and the disclaimer and (if applicable) source
* code license limitations below) in the documentation and/or other materials provided
* with the distribution
*
* Redistribution and use in binary form, without modification, are permitted provided
* that the following conditions are met:
*
* *       No reverse engineering, decompilation, or disassembly of this software is
* permitted with respect to any software provided in binary form.
*
* *       any redistribution and use are licensed by TI for use only with TI Devices.
*
* *       Nothing shall obligate TI to provide you with source code for the software
* licensed and provided to you in object code.
*
* If software source code is provided to you, modification and redistribution of the
* source code are permitted provided that the following conditions are met:
*
* *       any redistribution and use of the source code, including any resulting derivative
* works, are licensed by TI for use only with TI Devices.
*
* *       any redistribution and use of any object code compiled from the source code
* and any resulting derivative works, are licensed by TI for use only with TI Devices.
*
* Neither the name of Texas Instruments Incorporated nor the names of its suppliers
*
* may be used to endorse or promote products derived from this software without
* specific prior written permission.
*
* DISCLAIMER.
*
* THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
* OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
* OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
* IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
* BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
* OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
* OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
* OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/**
 *  \file     Eth_Irq.c
 *
 *  \brief    This file contains the ISR implementation of the Ethernet
 *            driver.
 */

/* ========================================================================== */
/*                             Include Files                                  */
/* ========================================================================== */

#include <Dem.h>
#if (STD_ON == ETH_DEV_ERROR_DETECT)
#include <Det.h>
#endif
#include <Eth_Cfg.h>
#include <Eth.h>
#include <EthIf_Cbk.h>
#include <Eth_Irq.h>
#if (STD_ON == ETH_ENABLE_MII_API)
#include <EthTrcv.h>
#endif

#include "Eth_Priv.h"

/* ========================================================================== */
/*                           Macros & Typedefs                                */
/* ========================================================================== */

/* None */

/* ========================================================================== */
/*                         Structures and Enums                               */
/* ========================================================================== */

/* None */

/* ========================================================================== */
/*                 Internal Function Declarations                             */
/* ========================================================================== */

/* None */

/* ========================================================================== */
/*                            Global Variables                                */
/* ========================================================================== */

/* None */

/* ========================================================================== */
/*                  Internal Function Definitions                             */
/* ========================================================================== */

/* None */

/* ========================================================================== */
/*                          Function Definitions                              */
/* ========================================================================== */

#define ETH_START_SEC_ISR_CODE
#include <Eth_MemMap.h>

/* Design : ETH_DesignId_017 */
/*
 * Requirements : MCAL-1604, MCAL-1605, MCAL-1606, MCAL-1607,
 *                MCAL-1608
 */
#if (ETH_ISR_TYPE == ETH_ISR_CAT1 || ETH_ISR_TYPE == ETH_ISR_VOID)
ETH_ISR_TEXT_SECTION FUNC(void, ETH_CODE_FAST) Eth_RxIrqHdlr_0(void)
#else
ETH_ISR_TEXT_SECTION ISR(Eth_RxIrqHdlr_0)
#endif
{
    Cpsw *cpsw;
    CpswDma *dma;
    CpswDmaCh *ch;
    CpswPktQ tempQ;
    CpswPkt *cpswPkt;
    Eth_Pkt *pkt;
    Eth_RxStatusType status;
    sint32 ret = CPSW_SOK;

#if (STD_ON == ETH_DEV_ERROR_DETECT)
    if (ETH_STATE_INIT != gEthDrvStatus)
    {
        /* Requirements: MCAL-1606 */
        Eth_reportDetError(ETH_SID_RX_IRQ_HDLR, ETH_E_UNINIT);
        ret = CPSW_EFAIL;
    }
#endif

    /* CPSW and DMA handles are no longer valid if the controller is down */
    if (ETH_MODE_ACTIVE != gEthDrv.ctrlMode)
    {
        ret = CPSW_EFAIL;
    }

    /*
     * Important Note: There are two ways for the completion interrupt to be
     * cleared:
     *  1. Explicitly as previously done in the ISR. If following this approach
     *     one must ensure that:
     *       a) Ring occupancy becomes 0, otherwise no new interrupt will be
     *          fired.
     *       b) Ring occupancy may need to be read multiple times to ensure
     *          occupancy actually reached 0.  New descs could arrive while
     *          we were emptying the previous occupancy, so one must ensure
     *          those are dequeued from ring before leaving the ISR.
     *  2. Interrupt self-clears when ring is emptied (ring occupancy becomes
     *     0).
     *       a) Completion interrupt is not explicitly (forcefully) cleared.
     *          IOW, this approach shouldn't be used along with (1).
     *       b) Leaving descs in the ring when leaving the ISR is not fatal.
     *          The completion interrupt will not be cleared and ISR will run
     *          again.  This condition could happen if new descs land in the
     *          ring while we were clearing the ring.
     */

    /* Retrieve any CPSW packets which are ready */
    if (CPSW_SOK == ret)
    {
        cpsw = &gEthDrv.cpsw;
        dma = Cpsw_getDma(cpsw);
        ch = CpswDma_getCh(dma, CPSW_DIR_RX);

        CpswPktQ_init(&tempQ);
        ret = CpswDma_retrievePktQ(dma, ch, &tempQ);
    }

    /* Queue the received packet to rxReadyQ and pass new ones from rxFreeQ */
    if (CPSW_SOK == ret)
    {
        cpswPkt = CpswPktQ_peek(&tempQ);
        while (NULL != cpswPkt)
        {
            /* Queue received packets to rxReady */
            pkt = (Eth_Pkt *)CpswPkt_getPriv(cpswPkt);
            if (NULL != pkt)
            {
                pkt->userLen = CpswPkt_getLen(cpswPkt);
                EthPktQ_queue(&gEthDrv.rxReadyQ, pkt);
            }
            else
            {
                /* Serious condition, an Eth packet was lost */
                (void)Dem_SetEventStatus(ETH_E_RX_FRAMES_LOST,
                                      DEM_EVENT_STATUS_PREFAILED);
            }

            /* Fill new packets from rxFreeQ */
            pkt = EthPktQ_dequeue(&gEthDrv.rxFreeQ);
            if (NULL != pkt)
            {
                CpswPkt_setBuf(cpswPkt, pkt->buf, pkt->len);
                CpswPkt_setPriv(cpswPkt, pkt);
                Eth_invCache(&gEthDrv, (uint8 *)pkt->buf, pkt->len);
            }
            else
            {
                /* If this happens, then we have CPSW packets with no buffer */
            }

            cpswPkt = cpswPkt->next;
        }

        /* Submit CPSW packets with new free buffers */
        if (CpswPktQ_getCount(&tempQ) > 0U)
        {
            ret = CpswDma_submitPktQ(dma, ch, &tempQ);
        }
    }

    /* Dequeue one received packet from rxReadyQ and pass it to EthIf */
    if (CPSW_SOK == ret)
    {
        do
        {
            status = Eth_processRxPkt(&gEthDrv, gEthDrv.ethConfig.ctrlIdx);
        }
        while (ETH_RECEIVED_MORE_DATA_AVAILABLE == status);
    }
}

/* Design : ETH_DesignId_018 */
/*
 * Requirements : MCAL-1609, MCAL-1610, MCAL-1611, MCAL-1612,
 *                MCAL-1613
 */
#if (ETH_ISR_TYPE == ETH_ISR_CAT1 || ETH_ISR_TYPE == ETH_ISR_VOID)
ETH_ISR_TEXT_SECTION FUNC(void, ETH_CODE_FAST) Eth_TxIrqHdlr_0(void)
#else
ETH_ISR_TEXT_SECTION ISR(Eth_TxIrqHdlr_0)
#endif
{
    Eth_Pkt *pkt;
    Std_ReturnType retVal;
    sint32 ret = CPSW_SOK;

#if (STD_ON == ETH_DEV_ERROR_DETECT)
    if (ETH_STATE_INIT != gEthDrvStatus)
    {
        /* Requirements: MCAL-1611 */
        Eth_reportDetError(ETH_SID_TX_IRQ_HDLR, ETH_E_UNINIT);
        ret = CPSW_EFAIL;
    }
#endif

    /* CPSW and DMA handles are no longer valid if the controller is down */
    if (ETH_MODE_ACTIVE != gEthDrv.ctrlMode)
    {
        ret = CPSW_EFAIL;
    }

    /*
     * Important Note: There are two ways for the completion interrupt to be
     * cleared:
     *  1. Explicitly as previously done in the ISR. If following this approach
     *     one must ensure that:
     *       a) Ring occupancy becomes 0, otherwise no new interrupt will be
     *          fired.
     *       b) Ring occupancy may need to be read multiple times to ensure
     *          occupancy actually reached 0.  New descs could arrive while
     *          we were emptying the previous occupancy, so one must ensure
     *          those are dequeued from ring before leaving the ISR.
     *  2. Interrupt self-clears when ring is emptied (ring occupancy becomes
     *     0).
     *       a) Completion interrupt is not explicitly (forcefully) cleared.
     *          IOW, this approach shouldn't be used along with (1).
     *       b) Leaving descs in the ring when leaving the ISR is not fatal.
     *          The completion interrupt will not be cleared and ISR will run
     *          again.  This condition could happen if new descs land in the
     *          ring while we were clearing the ring.
     */

    /* Retrieve any CPSW packets that may be free now */
    if (CPSW_SOK == ret)
    {
        retVal = Eth_retrieveFreeTxPktQ(&gEthDrv,
                                        gEthDrv.ethConfig.ctrlIdx,
                                        NULL);
        ret = (E_OK == retVal) ? CPSW_SOK : CPSW_EFAIL;
    }

    if (CPSW_SOK == ret)
    {
        /* Packets that need confirmation are already in txConfQ */
        pkt = EthPktQ_dequeue(&gEthDrv.txConfQ);
        while (NULL != pkt)
        {
            if (pkt->txConfirmation)
            {
                /* Requirements: MCAL-4825 */
                EthIf_TxConfirmation(gEthDrv.ethConfig.ctrlIdx, pkt->idx, E_OK);

                /* Requirements: MCAL-1599 */
                pkt->state = ETH_BUF_STATE_FREE;

                EthPktQ_queue(&gEthDrv.txFreeQ, pkt);
            }
#if ((STD_ON == ETH_ZERO_COPY_TX) && (STD_OFF == ETH_USE_Q_APIS))
            else if (pkt->txNoCpy)
            {
                EthIf_TxConfirmationNoCpy(gEthDrv.ethConfig.ctrlIdx,
                                          pkt->buf,
                                          E_OK);
                EthPktQ_queue(&gEthDrv.txFreeQNoCpy, pkt);
            }
#endif
            else
            {
                /* nothing */
            }

            pkt = EthPktQ_dequeue(&gEthDrv.txConfQ);
        }
    }
}

#if (STD_ON == ETH_ENABLE_MII_API)

#if (ETH_ISR_TYPE == ETH_ISR_CAT1 || ETH_ISR_TYPE == ETH_ISR_VOID)
ETH_ISR_TEXT_SECTION FUNC(void, ETH_CODE_FAST) Eth_MdioIrqHdlr_0(void)
#else
ETH_ISR_TEXT_SECTION ISR(Eth_MdioIrqHdlr_0)
#endif
{
    EthMiiAccess *access = &(gEthDrv.miiAccess);
    uint16 val;
    Std_ReturnType retVal;

    if (TRUE == access->isActive)
    {
        if (TRUE == access->isRead)
        {
            retVal = Eth_getMiiReadVal(&gEthDrv, access->ctrlIdx, &val);
            if (E_OK == retVal)
            {
                EthTrcv_ReadMiiIndication(access->ctrlIdx,
                                          access->trcvIdx,
                                          access->regIdx,
                                          val);
                access->isActive = FALSE;
            }
        }
        else
        {
            EthTrcv_WriteMiiIndication(access->ctrlIdx,
                                       access->trcvIdx,
                                       access->regIdx);
            access->isActive = FALSE;
        }

        Eth_clearMdioInt(&gEthDrv, access->ctrlIdx);
    }
}

#endif /* (STD_ON == ETH_ENABLE_MII_API) */

#define ETH_STOP_SEC_ISR_CODE
#include <Eth_MemMap.h>
