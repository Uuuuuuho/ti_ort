/*
*
* Copyright (c) 2019 Texas Instruments Incorporated
*
* All rights reserved not granted herein.
*
* Limited License.
*
* Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
* license under copyrights and patents it now or hereafter owns or controls to make,
* have made, use, import, offer to sell and sell ("Utilize") this software subject to the
* terms herein.  With respect to the foregoing patent license, such license is granted
* solely to the extent that any such patent is necessary to Utilize the software alone.
* The patent license shall not apply to any combinations which include this software,
* other than combinations with devices manufactured by or for TI ("TI Devices").
* No hardware patent is licensed hereunder.
*
* Redistributions must preserve existing copyright notices and reproduce this license
* (including the above copyright notice and the disclaimer and (if applicable) source
* code license limitations below) in the documentation and/or other materials provided
* with the distribution
*
* Redistribution and use in binary form, without modification, are permitted provided
* that the following conditions are met:
*
* *       No reverse engineering, decompilation, or disassembly of this software is
* permitted with respect to any software provided in binary form.
*
* *       any redistribution and use are licensed by TI for use only with TI Devices.
*
* *       Nothing shall obligate TI to provide you with source code for the software
* licensed and provided to you in object code.
*
* If software source code is provided to you, modification and redistribution of the
* source code are permitted provided that the following conditions are met:
*
* *       any redistribution and use of the source code, including any resulting derivative
* works, are licensed by TI for use only with TI Devices.
*
* *       any redistribution and use of any object code compiled from the source code
* and any resulting derivative works, are licensed by TI for use only with TI Devices.
*
* Neither the name of Texas Instruments Incorporated nor the names of its suppliers
*
* may be used to endorse or promote products derived from this software without
* specific prior written permission.
*
* DISCLAIMER.
*
* THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
* OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
* OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
* IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
* BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
* OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
* OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
* OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/**
 *  \file     Cpsw_Ale.c
 *
 *  \brief    This file contains Address Lookup Engine (ALE) related
 *            functionality.
 *
 */

/* ========================================================================== */
/*                             Include Files                                  */
/* ========================================================================== */

#include <string.h>
#include <ti/csl/csl_cpswitch.h>
#include <cpsw/Cpsw_Types.h>
#include <cpsw/Cpsw_Ale.h>
#include <cpsw/Cpsw_Soc.h>

/* ========================================================================== */
/*                           Macros & Typedefs                                */
/* ========================================================================== */

/* None */

/* ========================================================================== */
/*                         Structures and Enums                               */
/* ========================================================================== */

/**
 *  \brief ALE entry type
 *
 *  The type of an ALE table entry.
 */
typedef union
{
    CSL_CPSW_ALE_MCASTADDR_ENTRY mcast;
    /**< Multicast address entry */
    CSL_CPSW_ALE_VLANMCASTADDR_ENTRY vlanMcast;
    /**< VLAN/Multicast address entry */
    CSL_CPSW_ALE_UNICASTADDR_ENTRY ucast;
    /**< Unicast address entry */
    CSL_CPSW_ALE_OUIADDR_ENTRY oui;
    /**< OUI unicast address entry */
    CSL_CPSW_ALE_VLANUNICASTADDR_ENTRY vlanUcast;
    /**< VLAN/Unicast address entry */
    CSL_CPSW_ALE_VLAN_ENTRY vlan;
    /**< VLAN entry */
} CpswAle_Entry;

/* ========================================================================== */
/*                 Internal Function Declarations                             */
/* ========================================================================== */

/**
 *  \brief Find a free entry
 *
 *  Finds a free entry in the ALE table
 *
 *  \return         Entry index if found, otherwise CPSW_ALE_TABLE_SIZE
 */
uint32 CpswAle_findFreeEntry(CpswAle *ale);

/**
 *  \brief Find an entry
 *
 *  Finds a entry in the ALE table matching the given address and VLAN
 *
 *  \param addr     MAC address
 *  \param vlanId   VLAN id
 *
 *  \return         Entry index if found, otherwise CPSW_ALE_TABLE_SIZE
 */
uint32 CpswAle_findEntry(CpswAle *ale, const uint8 *addr, uint32 vlanId);


/* ========================================================================== */
/*                            Global Variables                                */
/* ========================================================================== */

/* None */

/* ========================================================================== */
/*                  Internal Function Definitions                             */
/* ========================================================================== */

uint32 CpswAle_findFreeEntry(CpswAle *ale)
{
    CSL_CPSW_ALE_ENTRYTYPE type;
    uint32 idx = CPSW_ALE_TABLE_SIZE;
    uint32 i;

    for (i = 0U; i < CPSW_ALE_TABLE_SIZE; i++)
    {
        type = CSL_CPSW_getALEEntryType(ale->regs, i,
                                        CSL_CPSW_ALETABLE_TYPE_4PORT);
        if (CSL_ALE_ENTRYTYPE_FREE == type)
        {
            idx = i;
            break;
        }
    }

    return idx;
}

uint32 CpswAle_findEntry(CpswAle *ale, const uint8 *addr, uint32 vlanId)
{
    CSL_CPSW_ALE_ENTRYTYPE type;
    uint32 w0Addr, w1Addr;
    uint32 w2, w1, w0;
    uint32 idx = CPSW_ALE_TABLE_SIZE;
    uint32 i;

    w1Addr = (addr[0U] <<  8U) | addr[1U];
    w0Addr = (addr[2U] << 24U) | (addr[3U] << 16U) |
             (addr[4U] <<  8U) | addr[5U];

    for (i = 0U; i < CPSW_ALE_TABLE_SIZE; i++)
    {
        CSL_CPSW_getAleTableEntry(ale->regs, i, &w0, &w1, &w2);

        type = (CSL_CPSW_ALE_ENTRYTYPE)((w1 >> 28U) & 0x3U);
        if ((CSL_ALE_ENTRYTYPE_ADDRESS == type) ||
            (CSL_ALE_ENTRYTYPE_VLANADDRESS == type))
        {
            /* Address and VLAN fields must match */
            if ((w1Addr == (w1 & 0x0000FFFFU)) &&
                (w0Addr == w0) &&
                (vlanId == ((w1 >> 16U) & 0xFFFU)))
            {
                idx = i;
                break;
            }
        }
    }

    return idx;
}

/* ========================================================================== */
/*                          Function Definitions                              */
/* ========================================================================== */

sint32 CpswMcalAle_open(CpswAle *ale, CpswMcalAle_Config *cfg, Enet_Type enetType)
{
    uint32 i;
    uint32 val;
    uint32 baseAddr = NULL;

    baseAddr = CpswSoc_getBaseAddr(enetType, 0U /*instId*/);
    ale->regs = (CSL_AleRegs *)(baseAddr + CPSW_ALE_OFFSET);

    /* Set port states */
    for (i = 0U; i < CPSW_PORT_MAX; i++)
    {
        CpswAle_setPortState(ale, (CpswPort_Num)i, cfg->portCfg[i].state);
    }

    /* Clear ALE table */
    CSL_CPSW_clearAleTable(ale->regs);

    val = CSL_CPSW_getAleControlReg(ale->regs);
    val &= ~(CSL_ALE_ALE_CONTROL_ENABLE_BYPASS_MASK |
             CSL_ALE_ALE_CONTROL_ALE_VLAN_AWARE_MASK |
             CSL_ALE_ALE_CONTROL_ENABLE_ALE_MASK);

    /* ALE bypass (all packets sent to the host port) */
    if (0 != (cfg->flags & ALE_FLAG_BYPASS))
    {
        val |= CSL_ALE_ALE_CONTROL_ENABLE_BYPASS_MASK;
    }

    /* ALE enable (enable ALE processing or drop all packets) */
    if (0 != (cfg->flags & ALE_FLAG_ENABLE))
    {
        val |= CSL_ALE_ALE_CONTROL_ENABLE_ALE_MASK;
    }

    /* VLAN aware (flood or drop packet if VLAN not found) */
    if (0 != (cfg->flags & ALE_FLAG_VLANAWARE))
    {
        val |= CSL_ALE_ALE_CONTROL_ALE_VLAN_AWARE_MASK;
    }

    CSL_CPSW_setAleControlReg(ale->regs, val);

    memcpy(&ale->cfg, cfg, sizeof(ale->cfg));
    ale->isInit = TRUE;

    return CPSW_SOK;
}

void CpswMcalAle_close(CpswAle *ale)
{
    if (TRUE == ale->isInit)
    {
        /* Clear table and disable ALE */
        CSL_CPSW_clearAleTable(ale->regs);
        CSL_CPSW_disableAle(ale->regs);

        memset(&ale->cfg, 0, sizeof(ale->cfg));
        ale->isInit = FALSE;
        ale->regs = NULL;
    }
}

void CpswAle_enable(CpswAle *ale)
{
    CSL_CPSW_enableAle(ale->regs);
    ale->enable = TRUE;
}

void CpswAle_disable(CpswAle *ale)
{
    CSL_CPSW_disableAle(ale->regs);
    ale->enable = FALSE;
}

void CpswAle_setBypass(CpswAle *ale, boolean enable)
{
    if (enable)
    {
        CSL_CPSW_enableAleBypass(ale->regs);
    }
    else
    {
        CSL_CPSW_disableAleBypass(ale->regs);
    }

    ale->bypass = enable;
}

void CpswAle_clearTable(CpswAle *ale)
{
    CSL_CPSW_clearAleTable(ale->regs);
}

void CpswAle_setPortState(CpswAle *ale,
                          CpswPort_Num port,
                          CpswMcalAle_PortState state)
{
    CSL_CPSW_ALE_PORTCONTROL control;
    uint32 portNum = (uint32)port;

    CSL_CPSW_getAlePortControlReg(ale->regs, portNum, &control);
    control.portState = (CSL_CPSW_ALE_PORTSTATE)state;
    CSL_CPSW_setAlePortControlReg(ale->regs, portNum, &control);
}

void CpswAle_setDefThread(CpswAle *ale, uint32 thread)
{
    CSL_CPSW_ALE_POLICER_GLOB_CONFIG cfg;

    memset(&cfg, 0, sizeof(cfg));
    cfg.defThread = thread;
    cfg.defThreadEnable = 1U;
    CSL_CPSW_setAlePolicerGlobConfig(ale->regs, &cfg);
}

sint32 CpswAle_addUcastAddr(CpswAle *ale,
                             CpswPort_Num port,
                             const uint8 *addr,
                             uint32 vlanId,
                             uint32 block,
                             uint32 secure)
{
    CpswAle_Entry entry;
    uint32 idx;
    sint32 retVal = CPSW_SOK;

    /* Use existing entry or find a new free one */
    idx = CpswAle_findEntry(ale, addr, vlanId);
    if (CPSW_ALE_TABLE_SIZE == idx)
    {
        idx = CpswAle_findFreeEntry(ale);
    }

    /* Add the unicast entry */
    if (CPSW_ALE_TABLE_SIZE > idx)
    {
        if (0U == vlanId)
        {
            memcpy(&entry.ucast.macAddress, addr, CPSW_MAC_ADDR_LEN);
            entry.ucast.ageable = 0U;
            entry.ucast.secureEnable = secure;
            entry.ucast.blockEnable = block;
            entry.ucast.portNumber = (uint32)port;
            entry.ucast.trunkFlag = 0U;
            CSL_CPSW_setAleUnicastAddrEntry(ale->regs, idx, &entry.ucast,
                                            CSL_CPSW_ALETABLE_TYPE_4PORT);
        }
        else
        {
            memcpy(&entry.vlanUcast.macAddress, addr, CPSW_MAC_ADDR_LEN);
            entry.vlanUcast.vlanId = vlanId;
            entry.vlanUcast.ageable = 0U;
            entry.vlanUcast.secureEnable = secure;
            entry.vlanUcast.blockEnable = block;
            entry.vlanUcast.portNumber = (uint32)port;
            entry.vlanUcast.trunkFlag = 0U;
            CSL_CPSW_setAleVlanUnicastAddrEntry(ale->regs, idx, &entry.vlanUcast,
                                                CSL_CPSW_ALETABLE_TYPE_4PORT);
        }
    }
    else
    {
        retVal = CPSW_EFAIL;
    }

    return retVal;
}

sint32 CpswAle_removeUcastAddr(CpswAle *ale,
                                const uint8 *addr,
                                uint32 vlanId)
{
    return CpswAle_removeAddr(ale, addr, vlanId);
}

sint32 CpswAle_addMcastAddr(CpswAle *ale,
                             const uint8 *addr,
                             uint32 vlanId,
                             uint32 portMask,
                             uint32 super,
                             CpswMcalAle_PortState state)
{
    CpswAle_Entry entry;
    boolean isVlanAware;
    uint32 idx;
    uint32 mask = 0U;
    sint32 retVal = CPSW_SOK;

    isVlanAware = CSL_CPSW_isAleVlanAwareEnabled(ale->regs);
    if (FALSE == isVlanAware)
    {
        vlanId = 0U;
    }

    /* Use existing entry or find a new free one */
    idx = CpswAle_findEntry(ale, addr, vlanId);
    if (CPSW_ALE_TABLE_SIZE == idx)
    {
        idx = CpswAle_findFreeEntry(ale);
        memcpy(&entry.mcast.macAddress, addr, CPSW_MAC_ADDR_LEN);
        mask = portMask;
    }
    else
    {
        CSL_CPSW_getAleVlanMcastAddrEntry(ale->regs, idx, &entry.vlanMcast,
                                          CSL_CPSW_ALETABLE_TYPE_4PORT);
        mask = entry.mcast.portMask | portMask;
    }

    /* Add the multicast entry */
    if (CPSW_ALE_TABLE_SIZE > idx)
    {
        entry.mcast.mcastFwdState = (uint32)state;
        entry.mcast.superEnable = super;
        entry.mcast.portMask = mask;

        if (0U == vlanId)
        {
            CSL_CPSW_setAleMcastAddrEntry(ale->regs, idx, &entry.mcast,
                                          CSL_CPSW_ALETABLE_TYPE_4PORT);
        }
        else
        {
            memcpy(&entry.vlanMcast.macAddress, addr, CPSW_MAC_ADDR_LEN);
            entry.vlanMcast.vlanId = vlanId;
            CSL_CPSW_setAleVlanMcastAddrEntry(ale->regs, idx, &entry.vlanMcast,
                                              CSL_CPSW_ALETABLE_TYPE_4PORT);
        }
    }
    else
    {
        retVal = CPSW_EFAIL;
    }

    return retVal;
}

sint32 CpswAle_removeMcastAddr(CpswAle *ale,
                                const uint8 *addr,
                                uint32 vlanId,
                                uint32 portMask)
{
    CpswAle_Entry entry;
    uint32 idx;
    sint32 retVal = CPSW_SOK;

    /* Clear the port bit from the mask if an ALE entry is found */
    idx = CpswAle_findEntry(ale, addr, vlanId);
    if (CPSW_ALE_TABLE_SIZE != idx)
    {
        if (0U == vlanId)
        {
            CSL_CPSW_getAleMcastAddrEntry(ale->regs, idx, &entry.mcast,
                                          CSL_CPSW_ALETABLE_TYPE_4PORT);
            entry.mcast.portMask &= ~portMask;
            if (0U == entry.mcast.portMask)
            {
                CSL_CPSW_clearAleEntry(ale->regs, idx);
            }
            else
            {
                CSL_CPSW_setAleMcastAddrEntry(ale->regs, idx, &entry.mcast,
                                              CSL_CPSW_ALETABLE_TYPE_4PORT);
            }
        }
        else
        {
            CSL_CPSW_getAleVlanMcastAddrEntry(ale->regs, idx, &entry.vlanMcast,
                                              CSL_CPSW_ALETABLE_TYPE_4PORT);
            entry.vlanMcast.portMask &= ~portMask;
            if (0U == entry.vlanMcast.portMask)
            {
                CSL_CPSW_clearAleEntry(ale->regs, idx);
            }
            else
            {
                CSL_CPSW_setAleVlanMcastAddrEntry(ale->regs, idx, &entry.vlanMcast,
                                              CSL_CPSW_ALETABLE_TYPE_4PORT);
            }
        }
    }
    else
    {
        retVal = CPSW_EFAIL;
    }

    return retVal;
}

sint32 CpswAle_removeAddr(CpswAle *ale,
                           const uint8 *addr,
                           uint32 vlanId)
{
    uint32 idx;
    sint32 retVal = CPSW_SOK;

    /* Clear the ALE entry if a matching one was found */
    idx = CpswAle_findEntry(ale, addr, vlanId);
    if (CPSW_ALE_TABLE_SIZE != idx)
    {
        CSL_CPSW_clearAleEntry(ale->regs, idx);
    }
    else
    {
        retVal = CPSW_EFAIL;
    }

    return retVal;
}

void CpswAle_setUnknownVlan(CpswAle *ale,
                            uint32 portMask)
{
    uint32 vlanList;
    uint32 mcastMask;
    uint32 regMcastMask;
    uint32 forceUntagged;

    CSL_CPSW_getAleUnkownVlanReg(ale->regs,
                                 &vlanList,
                                 &mcastMask,
                                 &regMcastMask,
                                 &forceUntagged);

    vlanList = portMask;

    CSL_CPSW_setAleUnkownVlanReg(ale->regs,
                                 vlanList,
                                 mcastMask,
                                 regMcastMask,
                                 forceUntagged);
}

void CpswAle_setUnknownMcastFlood(CpswAle *ale,
                                  uint32 portMask)
{
    uint32 vlanList;
    uint32 mcastMask;
    uint32 regMcastMask;
    uint32 forceUntagged;

    CSL_CPSW_getAleUnkownVlanReg(ale->regs,
                                 &vlanList,
                                 &mcastMask,
                                 &regMcastMask,
                                 &forceUntagged);

    mcastMask = portMask;

    CSL_CPSW_setAleUnkownVlanReg(ale->regs,
                                 vlanList,
                                 mcastMask,
                                 regMcastMask,
                                 forceUntagged);
}

void CpswAle_setUnknownRegMcastFlood(CpswAle *ale,
                                     uint32 portMask)
{
    uint32 vlanList;
    uint32 mcastMask;
    uint32 regMcastMask;
    uint32 forceUntagged;

    CSL_CPSW_getAleUnkownVlanReg(ale->regs,
                                 &vlanList,
                                 &mcastMask,
                                 &regMcastMask,
                                 &forceUntagged);

    regMcastMask = portMask;

    CSL_CPSW_setAleUnkownVlanReg(ale->regs,
                                 vlanList,
                                 mcastMask,
                                 regMcastMask,
                                 forceUntagged);
}

