/*
*
* Copyright (c) 2019 Texas Instruments Incorporated
*
* All rights reserved not granted herein.
*
* Limited License.
*
* Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
* license under copyrights and patents it now or hereafter owns or controls to make,
* have made, use, import, offer to sell and sell ("Utilize") this software subject to the
* terms herein.  With respect to the foregoing patent license, such license is granted
* solely to the extent that any such patent is necessary to Utilize the software alone.
* The patent license shall not apply to any combinations which include this software,
* other than combinations with devices manufactured by or for TI ("TI Devices").
* No hardware patent is licensed hereunder.
*
* Redistributions must preserve existing copyright notices and reproduce this license
* (including the above copyright notice and the disclaimer and (if applicable) source
* code license limitations below) in the documentation and/or other materials provided
* with the distribution
*
* Redistribution and use in binary form, without modification, are permitted provided
* that the following conditions are met:
*
* *       No reverse engineering, decompilation, or disassembly of this software is
* permitted with respect to any software provided in binary form.
*
* *       any redistribution and use are licensed by TI for use only with TI Devices.
*
* *       Nothing shall obligate TI to provide you with source code for the software
* licensed and provided to you in object code.
*
* If software source code is provided to you, modification and redistribution of the
* source code are permitted provided that the following conditions are met:
*
* *       any redistribution and use of the source code, including any resulting derivative
* works, are licensed by TI for use only with TI Devices.
*
* *       any redistribution and use of any object code compiled from the source code
* and any resulting derivative works, are licensed by TI for use only with TI Devices.
*
* Neither the name of Texas Instruments Incorporated nor the names of its suppliers
*
* may be used to endorse or promote products derived from this software without
* specific prior written permission.
*
* DISCLAIMER.
*
* THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
* OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
* OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
* IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
* BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
* OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
* OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
* OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/**
 *  \file     Cpsw_Port.c
 *
 *  \brief    This file contains the Port module related functionality.
 */

/* ========================================================================== */
/*                             Include Files                                  */
/* ========================================================================== */

#include <string.h>
#include <ti/csl/csl_cpswitch.h>
#include <cpsw/Cpsw_Types.h>
#include <cpsw/Cpsw_Soc.h>
#include <cpsw/Cpsw_Port.h>

/* ========================================================================== */
/*                           Macros & Typedefs                                */
/* ========================================================================== */

/* None */

/* ========================================================================== */
/*                         Structures and Enums                               */
/* ========================================================================== */

/* None */

/* ========================================================================== */
/*                 Internal Function Declarations                             */
/* ========================================================================== */

/* None */

/* ========================================================================== */
/*                            Global Variables                                */
/* ========================================================================== */

/* None */

/* ========================================================================== */
/*                  Internal Function Definitions                             */
/* ========================================================================== */

/* ========================================================================== */
/*                          Function Definitions                              */
/* ========================================================================== */

sint32 CpswPort_open(CpswPort *port, CpswPort_Num num, CpswPort_Config *cfg, Enet_Type enetType)
{
    CSL_CPSW_CONTROL control;
    uint32 baseAddr = NULL;

    baseAddr = CpswSoc_getBaseAddr(enetType, 0U /*instId*/);
    port->regs = (CSL_Xge_cpswRegs *)(baseAddr + CPSW_NU_OFFSET);

    CSL_CPSW_getCpswControlReg(port->regs, &control);

    if (CPSW_PORT0 == num)
    {
        /* VLAN Aware Mode */
        if (0U != (cfg->flags & PORT_FLAG_VLANAWARE))
        {
            control.vlanAware = TRUE;
        }
        else
        {
            control.vlanAware = FALSE;
        }

        /* Port 0 Pass Priority Tagged */
        if (0U != (cfg->flags & PORT_FLAG_P0PASSPRITAGGED))
        {
            control.p0PassPriTag = TRUE;
        }
        else
        {
            control.p0PassPriTag = FALSE;
        }

        /* Remove TX CRC (packet reception from host point of view) */
        if (0U != (cfg->flags & PORT_FLAG_TXCRCREMOVE))
        {
            control.p0TxCrcRemove = TRUE;
        }
        else
        {
            control.p0TxCrcRemove = FALSE;
        }

        /* Port 0 Receive Short Packet Pad */
        if (0U != (cfg->flags & PORT_FLAG_P0RXPAD))
        {
            control.p0RxPad = TRUE;
        }
        else
        {
            control.p0RxPad = FALSE;
        }

        /* Port 0 Pass Received CRC Errors */
        if (0U != (cfg->flags & PORT_FLAG_P0RXPASSCRCERR))
        {
            control.p0RxPassCrcErr = TRUE;
        }
        else
        {
            control.p0RxPassCrcErr = FALSE;
        }
    }
    else if (CPSW_PORT1 == num)
    {
        /* Port 1 Pass Priority Tagged */
        if (0U != (cfg->flags & PORT_FLAG_P1PASSPRITAGGED))
        {
            control.p1PassPriTag = TRUE;
        }
        else
        {
            control.p1PassPriTag = FALSE;
        }
    }

    CSL_CPSW_setCpswControlReg(port->regs, &control);

    /* Set port's MAC address (not applicable for host port) */
    port->num = (uint32)num;
    if ((0U < port->num) && (CPSW_PORT_MAX > port->num))
    {
        CSL_CPSW_setPortMACAddress(port->regs, port->num, cfg->macAddr);
    }

    /* Set packet MTU */
    CpswPort_setMaxLen(port, cfg->pktMTU);

    memcpy(&port->cfg, cfg, sizeof(port->cfg));
    port->isInit = TRUE;

    return CPSW_SOK;
}

void CpswPort_close(CpswPort *port)
{
    if (TRUE == port->isInit)
    {
        memset(&port->cfg, 0, sizeof(port->cfg));
        port->regs = NULL;
        port->isInit = FALSE;
    }
}

void CpswPort_enable(CpswPort *port)
{
    /* Only port 0 can be enabled/disabled, NOP for others */
    if (0U == port->num)
    {
        CSL_CPSW_enablePort0(port->regs);
    }
}

void CpswPort_disable(CpswPort *port)
{
    /* Only port 0 can be enabled/disabled, NOP for others */
    if (0U == port->num)
    {
        CSL_CPSW_disablePort0(port->regs);
    }
}

sint32 CpswPort_setMacAddr(CpswPort *port, const uint8 *addr)
{
    sint32 retVal = CPSW_SOK;

    /* Only MAC ports have a MAC address */
    if ((0U < port->num) && (CPSW_PORT_MAX > port->num))
    {
        CSL_CPSW_setPortMACAddress(port->regs, port->num, (uint8 *)addr);
        memcpy(port->cfg.macAddr, addr, CPSW_MAC_ADDR_LEN);
    }
    else
    {
        retVal = CPSW_EINVALID_PARAMS;
    }

    return retVal;
}

sint32 CpswPort_getMacAddr(CpswPort *port, uint8 *addr)
{
    sint32 retVal = CPSW_SOK;

    /* Only MAC ports have a MAC address */
    if ((0U < port->num) && (CPSW_PORT_MAX > port->num))
    {
        CSL_CPSW_getPortMACAddress(port->regs, port->num, addr);
    }
    else
    {
        retVal = CPSW_EINVALID_PARAMS;
    }

    return retVal;
}

void CpswPort_setMaxLen(CpswPort *port, uint32 len)
{
    /* For MAC ports, this is equivalent to CpswMac_setMaxLen() */
    if (CPSW_PORT_MAX > port->num)
    {
        CSL_CPSW_setPortRxMaxLen(port->regs, port->num, len);
        port->cfg.pktMTU = len;
    }
}

uint32 CpswPort_getMaxLen(CpswPort *port)
{
    uint32 len = 0;

    /* For MAC ports, this is equivalent to CpswMac_getMaxLen() */
    if (CPSW_PORT_MAX > port->num)
    {
        len = CSL_CPSW_getPortRxMaxLen(port->regs, port->num);
    }

    return len;
}
