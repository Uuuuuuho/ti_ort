/*
*
* Copyright (c) 2019 Texas Instruments Incorporated
*
* All rights reserved not granted herein.
*
* Limited License.
*
* Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
* license under copyrights and patents it now or hereafter owns or controls to make,
* have made, use, import, offer to sell and sell ("Utilize") this software subject to the
* terms herein.  With respect to the foregoing patent license, such license is granted
* solely to the extent that any such patent is necessary to Utilize the software alone.
* The patent license shall not apply to any combinations which include this software,
* other than combinations with devices manufactured by or for TI ("TI Devices").
* No hardware patent is licensed hereunder.
*
* Redistributions must preserve existing copyright notices and reproduce this license
* (including the above copyright notice and the disclaimer and (if applicable) source
* code license limitations below) in the documentation and/or other materials provided
* with the distribution
*
* Redistribution and use in binary form, without modification, are permitted provided
* that the following conditions are met:
*
* *       No reverse engineering, decompilation, or disassembly of this software is
* permitted with respect to any software provided in binary form.
*
* *       any redistribution and use are licensed by TI for use only with TI Devices.
*
* *       Nothing shall obligate TI to provide you with source code for the software
* licensed and provided to you in object code.
*
* If software source code is provided to you, modification and redistribution of the
* source code are permitted provided that the following conditions are met:
*
* *       any redistribution and use of the source code, including any resulting derivative
* works, are licensed by TI for use only with TI Devices.
*
* *       any redistribution and use of any object code compiled from the source code
* and any resulting derivative works, are licensed by TI for use only with TI Devices.
*
* Neither the name of Texas Instruments Incorporated nor the names of its suppliers
*
* may be used to endorse or promote products derived from this software without
* specific prior written permission.
*
* DISCLAIMER.
*
* THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
* OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
* OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
* IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
* BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
* OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
* OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
* OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/**
 *  \file     Cpsw_Mac.c
 *
 *  \brief    This file contains MAC configuration related functionality.
 */

/* ========================================================================== */
/*                             Include Files                                  */
/* ========================================================================== */

#include <string.h>
#include <ti/csl/csl_cpswitch.h>
#include <cpsw/Cpsw_Types.h>
#include <cpsw/Cpsw_Soc.h>
#include <cpsw/Cpsw_Mac.h>

/* ========================================================================== */
/*                           Macros & Typedefs                                */
/* ========================================================================== */

/* None */

/* ========================================================================== */
/*                         Structures and Enums                               */
/* ========================================================================== */

/* None */

/* ========================================================================== */
/*                 Internal Function Declarations                             */
/* ========================================================================== */

/**
 *  \brief Set flags to MACCONTROL register value
 *
 *  Set the value to be written to the MACCONTROL register according to the
 *  flags passed.
 *
 *  \param val      MACCONTROL value
 *  \param flags    MAC configuration flags \ref MAC_ConfigFlags
 */
void CpswMac_setControl(uint32 *val, uint32 flags);

/**
 *  \brief Set connection type to MACCONTROL register value
 *
 *  Set the value to be written to the MACCONTROL register according to the
 *  connection type.
 *
 *  \param val      MACCONTROL value
 *  \param type     Connection type
 */
void CpswMac_updateConnType(uint32 *val, CpswMac_ConnType type);

/* ========================================================================== */
/*                            Global Variables                                */
/* ========================================================================== */

/* None */

/* ========================================================================== */
/*                  Internal Function Definitions                             */
/* ========================================================================== */

void CpswMac_setControl(uint32 *val, uint32 flags)
{
    *val &= ~(CSL_CPGMAC_SL_MACCONTROL_RX_CSF_EN |
              CSL_CPGMAC_SL_MACCONTROL_RX_CEF_EN |
              CSL_CPGMAC_SL_MACCONTROL_RX_CMF_EN |
              CSL_CPGMAC_SL_MACCONTROL_TX_SHORT_GAP_EN |
              CSL_CPGMAC_SL_MACCONTROL_EXT_EN_TX_FLOW |
              CSL_CPGMAC_SL_MACCONTROL_EXT_EN_RX_FLOW |
              CSL_CPGMAC_SL_MACCONTROL_CMD_IDLE_EN |
              CSL_CPGMAC_SL_MACCONTROL_CASTAGNOLI_CRC |
              CSL_CPGMAC_SL_MACCONTROL_TX_PACE_EN |
              CSL_CPGMAC_SL_MACCONTROL_TX_FLOW_EN |
              CSL_CPGMAC_SL_MACCONTROL_RX_FLOW_EN |
              CSL_CPGMAC_SL_MACCONTROL_LOOPBACK_EN);

    /* RX Copy MAC Control Frames */
    if (0U != (flags & MAC_FLAG_PASSCONTROL))
    {
        *val |= CSL_CPGMAC_SL_MACCONTROL_RX_CSF_EN;
    }

    /* RX Copy Short and Error Frames */
    if (0U != (flags & MAC_FLAG_PASSERROR))
    {
        *val |= (CSL_CPGMAC_SL_MACCONTROL_RX_CEF_EN |
                 CSL_CPGMAC_SL_MACCONTROL_RX_CMF_EN);
    }

    /* Transmit Short Gap Enable */
    if (0U != (flags & MAC_FLAG_TXSHORTGAPEN))
    {
        *val |= CSL_CPGMAC_SL_MACCONTROL_TX_SHORT_GAP_EN;
    }

    /* External Transmit Flow Control */
    if (0U != (flags & MAC_FLAG_EXTTXFLOWCNTL))
    {
        *val |= CSL_CPGMAC_SL_MACCONTROL_EXT_EN_TX_FLOW;
    }

    /* External Receive Flow Control */
    if (0U != (flags & MAC_FLAG_EXTRXFLOWCNTL))
    {
        *val |= CSL_CPGMAC_SL_MACCONTROL_EXT_EN_RX_FLOW;
    }

    /* CRC type */
    if (0U != (flags & MAC_FLAG_CASTAGNOLI_CRC))
    {
        *val |= CSL_CPGMAC_SL_MACCONTROL_CASTAGNOLI_CRC;
    }

    /* Command Idle */
    if (0U != (flags & MAC_FLAG_CMDIDLE))
    {
        *val |= CSL_CPGMAC_SL_MACCONTROL_CMD_IDLE_EN;
    }

    /* Transmit Pacing */
    if (0U != (flags & MAC_FLAG_TXPACE))
    {
        *val |= CSL_CPGMAC_SL_MACCONTROL_TX_PACE_EN;
    }

    /* Transmit Flow Control */
    if (0U != (flags & MAC_FLAG_TXFLOWCNTL))
    {
        *val |= CSL_CPGMAC_SL_MACCONTROL_TX_FLOW_EN;
    }

    /* Receive Flow Control */
    if (0U != (flags & MAC_FLAG_RXFLOWCNTL))
    {
        *val |= CSL_CPGMAC_SL_MACCONTROL_RX_FLOW_EN;
    }

    /* Loop Back */
    if (0U != (flags & MAC_FLAG_MACLOOPBACK))
    {
        *val |= CSL_CPGMAC_SL_MACCONTROL_LOOPBACK_EN;
    }
}

void CpswMac_updateConnType(uint32 *val, CpswMac_ConnType type)
{
    *val &= ~(CSL_CPGMAC_SL_MACCONTROL_EXT_EN |
              CSL_CPGMAC_SL_MACCONTROL_GIG_FORCE_EN |
              CSL_CPGMAC_SL_MACCONTROL_IFCTL_A_EN |
              CSL_CPGMAC_SL_MACCONTROL_GIG_EN |
              CSL_CPGMAC_SL_MACCONTROL_FULLDUPLEX_EN);

    switch (type)
    {
    case CPSWMCAL_MAC_CONN_TYPE_RMII_10:
        *val |= CSL_CPGMAC_SL_MACCONTROL_FULLDUPLEX_EN;
        break;
    case CPSWMCAL_MAC_CONN_TYPE_RMII_100:
        *val |= (CSL_CPGMAC_SL_MACCONTROL_FULLDUPLEX_EN |
                 CSL_CPGMAC_SL_MACCONTROL_IFCTL_A_EN);
        break;
    case CPSWMCAL_MAC_CONN_TYPE_RGMII_FORCE_100_HALF:
        *val |= CSL_CPGMAC_SL_MACCONTROL_GIG_FORCE_EN;
        break;
    case CPSWMCAL_MAC_CONN_TYPE_RGMII_FORCE_100_FULL:
        *val |= (CSL_CPGMAC_SL_MACCONTROL_FULLDUPLEX_EN |
                 CSL_CPGMAC_SL_MACCONTROL_GIG_FORCE_EN);
        break;
    case CPSWMCAL_MAC_CONN_TYPE_RGMII_FORCE_1000_FULL:
        *val |= (CSL_CPGMAC_SL_MACCONTROL_FULLDUPLEX_EN |
                 CSL_CPGMAC_SL_MACCONTROL_GIG_EN |
                 CSL_CPGMAC_SL_MACCONTROL_GIG_FORCE_EN);
        break;
    case CPSWMCAL_MAC_CONN_TYPE_RGMII_DETECT_INBAND:
        *val |= CSL_CPGMAC_SL_MACCONTROL_EXT_EN;
        break;
    default:
        break;
    }
}

/* ========================================================================== */
/*                          Function Definitions                              */
/* ========================================================================== */

sint32 CpswMcalMac_open(CpswMac *mac, CpswMac_Num num, CpswMac_Config *cfg, Enet_Type enetType)
{
    boolean done;
    uint32 val;
    uint32 baseAddr = NULL;

    baseAddr = CpswSoc_getBaseAddr(enetType, 0U /*instId*/);
    mac->regs = (CSL_Xge_cpswRegs *)(baseAddr + CPSW_NU_OFFSET);
    mac->num = (uint32)num;
    memcpy(&mac->cfg, cfg, sizeof(mac->cfg));

    /* Soft reset the MAC */
    CSL_CPGMAC_SL_resetMac(mac->regs, num);
    do
    {
        done = CSL_CPGMAC_SL_isMACResetDone(mac->regs, num);
    }
    while (FALSE == done);

    /* Set packet MTU */
    CpswMac_setMaxLen(mac, cfg->pktMTU);

    /* Set MAC control register with configuration flags and selected MII mode */
    val = CSL_CPGMAC_SL_getMacControlReg(mac->regs, mac->num);
    CpswMac_setControl(&val, cfg->flags);
    CpswMac_updateConnType(&val, cfg->connType);
    CSL_CPGMAC_SL_setMacControlReg(mac->regs, mac->num, val);

    CSL_CPGMAC_SL_enableGMII(mac->regs, mac->num);

    return CPSW_SOK;
}

void CpswMcalMac_close(CpswMac *mac)
{
    boolean done;

    if (TRUE == mac->isInit)
    {
        /* Soft reset the MAC */
        CSL_CPGMAC_SL_resetMac(mac->regs, mac->num);
        do
        {
            done = CSL_CPGMAC_SL_isMACResetDone(mac->regs, mac->num);
        }
        while (FALSE == done);

        memset(&mac->cfg, 0, sizeof(mac->cfg));
        mac->regs = NULL;
        mac->isInit = FALSE;
    }
}

void CpswMac_setConnType(CpswMac *mac, CpswMac_ConnType type)
{
    uint32 val;

    CSL_CPGMAC_SL_disableGMII(mac->regs, mac->num);

    val = CSL_CPGMAC_SL_getMacControlReg(mac->regs, mac->num);
    CpswMac_updateConnType(&val, type);
    CSL_CPGMAC_SL_setMacControlReg(mac->regs, mac->num, val);

    CSL_CPGMAC_SL_enableGMII(mac->regs, mac->num);

    mac->cfg.connType = type;
}

void CpswMac_setMaxLen(CpswMac *mac, uint32 len)
{
    CSL_CPGMAC_SL_setRxMaxLen(mac->regs, mac->num, len);
    mac->cfg.pktMTU = len;
}

uint32 CpswMac_getMaxLen(CpswMac *mac)
{
    return CSL_CPGMAC_SL_getRxMaxLen(mac->regs, mac->num);
}
