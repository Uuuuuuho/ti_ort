/*
*
* Copyright (c) 2019 Texas Instruments Incorporated
*
* All rights reserved not granted herein.
*
* Limited License.
*
* Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
* license under copyrights and patents it now or hereafter owns or controls to make,
* have made, use, import, offer to sell and sell ("Utilize") this software subject to the
* terms herein.  With respect to the foregoing patent license, such license is granted
* solely to the extent that any such patent is necessary to Utilize the software alone.
* The patent license shall not apply to any combinations which include this software,
* other than combinations with devices manufactured by or for TI ("TI Devices").
* No hardware patent is licensed hereunder.
*
* Redistributions must preserve existing copyright notices and reproduce this license
* (including the above copyright notice and the disclaimer and (if applicable) source
* code license limitations below) in the documentation and/or other materials provided
* with the distribution
*
* Redistribution and use in binary form, without modification, are permitted provided
* that the following conditions are met:
*
* *       No reverse engineering, decompilation, or disassembly of this software is
* permitted with respect to any software provided in binary form.
*
* *       any redistribution and use are licensed by TI for use only with TI Devices.
*
* *       Nothing shall obligate TI to provide you with source code for the software
* licensed and provided to you in object code.
*
* If software source code is provided to you, modification and redistribution of the
* source code are permitted provided that the following conditions are met:
*
* *       any redistribution and use of the source code, including any resulting derivative
* works, are licensed by TI for use only with TI Devices.
*
* *       any redistribution and use of any object code compiled from the source code
* and any resulting derivative works, are licensed by TI for use only with TI Devices.
*
* Neither the name of Texas Instruments Incorporated nor the names of its suppliers
*
* may be used to endorse or promote products derived from this software without
* specific prior written permission.
*
* DISCLAIMER.
*
* THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
* OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
* OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
* IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
* BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
* OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
* OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
* OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/**
 *  \file     Cpsw_Mdio.c
 *
 *  \brief    This file contains MDIO related functionality.
 */

/* ========================================================================== */
/*                             Include Files                                  */
/* ========================================================================== */

#include <string.h>
#include <ti/csl/csl_cpswitch.h>
#include <cpsw/Cpsw_Types.h>
#include <cpsw/Cpsw_Soc.h>
#include <cpsw/Cpsw_Mdio.h>

/* ========================================================================== */
/*                           Macros & Typedefs                                */
/* ========================================================================== */

/* None */

/* ========================================================================== */
/*                         Structures and Enums                               */
/* ========================================================================== */

/* None */

/* ========================================================================== */
/*                 Internal Function Declarations                             */
/* ========================================================================== */

/* None */

/* ========================================================================== */
/*                            Global Variables                                */
/* ========================================================================== */

/* None */

/* ========================================================================== */
/*                  Internal Function Definitions                             */
/* ========================================================================== */

/* ========================================================================== */
/*                          Function Definitions                              */
/* ========================================================================== */

sint32 CpswMcalMdio_open(CpswMdio *mdio, CpswMcalMdio_Config *cfg, Enet_Type enetType)
{
    uint32 clkdiv;
    sint32 retVal = CPSW_SOK;
    uint32 baseAddr = NULL;
    uint64_t cppiClkFreqHz;

    baseAddr = CpswSoc_getBaseAddr(enetType, 0U /*instId*/);
    mdio->regs = (CSL_mdioHandle)(baseAddr + CPSW_MDIO_OFFSET);

    if (ENET_CPSW_2G == enetType)
    {
        cppiClkFreqHz = 333333333LLU;
    }
    else
    {
        retVal = CPSW_EBADARGS;
    }

    if (CPSW_SOK == retVal)
    {
        if (cfg->busFreq != 0U)
        {
            clkdiv = (cppiClkFreqHz / cfg->busFreq) - 1U;
            if (clkdiv > 65535U)
            {
                retVal = CPSW_EINVALID_PARAMS;
            }
        }
        else
        {
            retVal = CPSW_EBADARGS;
        }
    }

    if (CPSW_SOK == retVal)
    {
        /* Clear the User Command Complete interrupt and the USER_ACCESS_REG */
        CSL_REG32_FINS(&mdio->regs->USER_INT_MASK_SET_REG,
                       MDIO_USER_INT_MASK_SET_REG_USERINTMASKSET, 1U);
        CSL_REG32_WR(&mdio->regs->USER_GROUP[0U].USER_ACCESS_REG, 0U);

        /* Set the MDIO clock divider */
        CSL_MDIO_setClkDivVal(mdio->regs, (uint16)clkdiv);

        /* Enable the state machine */
        CSL_MDIO_enableStateMachine(mdio->regs);

        memcpy(&mdio->cfg, cfg, sizeof(mdio->cfg));
        mdio->isInit = TRUE;
    }
    else
    {
        mdio->isInit = FALSE;
    }

    return retVal;
}

void CpswMcalMdio_close(CpswMdio *mdio)
{
    if (FALSE == mdio->isInit)
    {
        /* Disable the state machine */
        CSL_MDIO_disableStateMachine(mdio->regs);
        memset(&mdio->cfg, 0, sizeof(mdio->cfg));
        mdio->regs = NULL;
        mdio->isInit = FALSE;
    }
}

boolean CpswMdio_isPhyAlive(CpswMdio *mdio, uint32 phyAddr)
{
    return CSL_MDIO_isPhyAlive(mdio->regs, phyAddr);
}

boolean CpswMdio_isPhyLinked(CpswMdio *mdio, uint32 phyAddr)
{
    return CSL_MDIO_isPhyLinked(mdio->regs, phyAddr);
}

void CpswMdio_writePhyReg(CpswMdio *mdio,
                          uint32 phyAddr,
                          uint32 reg,
                          uint16 val)
{
    CSL_MDIO_phyRegWrite2(mdio->regs, 0U, phyAddr, reg, val);
    CpswMdio_clearUserInt(mdio);
}

sint32 CpswMdio_readPhyReg(CpswMdio *mdio,
                            uint32 phyAddr,
                            uint32 reg,
                            uint16 *val)
{
    sint32 retVal = CPSW_SOK;
    boolean ret;

    ret = CSL_MDIO_phyRegRead2(mdio->regs, 0U, phyAddr, reg, val);
    if (FALSE == ret)
    {
        retVal = CPSW_EFAIL;
    }

    CpswMdio_clearUserInt(mdio);

    return retVal;
}

sint32 CpswMdio_initiatePhyRegWrite(CpswMdio *mdio,
                                     uint32 phyAddr,
                                     uint32 reg,
                                     uint16 val)
{
    uint32 userAccess = 0U;
    sint32 retVal = CPSW_SOK;

    if (0U == CSL_REG32_FEXT(&mdio->regs->USER_GROUP[0U].USER_ACCESS_REG,
                             MDIO_USER_GROUP_USER_ACCESS_REG_GO))
    {
        CSL_FINS(userAccess,
                 MDIO_USER_GROUP_USER_ACCESS_REG_GO,
                 CSL_MDIO_USER_GROUP_USER_ACCESS_REG_GO_EN_0x1);

        CSL_FINS(userAccess,
                 MDIO_USER_GROUP_USER_ACCESS_REG_WRITE,
                 CSL_MDIO_USER_GROUP_USER_ACCESS_REG_WRITE);

        CSL_FINS(userAccess,
                 MDIO_USER_GROUP_USER_ACCESS_REG_PHYADR,
                 phyAddr);

        CSL_FINS(userAccess,
                 MDIO_USER_GROUP_USER_ACCESS_REG_REGADR,
                 reg);

        CSL_FINS(userAccess,
                 MDIO_USER_GROUP_USER_ACCESS_REG_DATA,
                 val);

        CSL_REG32_WR(&mdio->regs->USER_GROUP[0U].USER_ACCESS_REG, userAccess);
    }
    else
    {
        retVal = CPSW_EBUSY;
    }

    return retVal;
}

sint32 CpswMdio_initiatePhyRegRead(CpswMdio *mdio,
                                    uint32 phyAddr,
                                    uint32 reg)
{
    uint32 userAccess = 0U;
    sint32 retVal = CPSW_SOK;

    if (0U == CSL_REG32_FEXT(&mdio->regs->USER_GROUP[0U].USER_ACCESS_REG,
                             MDIO_USER_GROUP_USER_ACCESS_REG_GO))
    {
        CSL_FINS(userAccess,
                 MDIO_USER_GROUP_USER_ACCESS_REG_GO,
                 CSL_MDIO_USER_GROUP_USER_ACCESS_REG_GO_EN_0x1);

        CSL_FINS(userAccess,
                 MDIO_USER_GROUP_USER_ACCESS_REG_WRITE,
                 CSL_MDIO_USER_GROUP_USER_ACCESS_REG_READ);

        CSL_FINS(userAccess,
                 MDIO_USER_GROUP_USER_ACCESS_REG_PHYADR,
                 phyAddr);

        CSL_FINS(userAccess,
                MDIO_USER_GROUP_USER_ACCESS_REG_REGADR,
                reg);

        CSL_REG32_WR(&mdio->regs->USER_GROUP[0U].USER_ACCESS_REG, userAccess);
    }
    else
    {
        retVal = CPSW_EBUSY;
    }

    return retVal;
}

sint32 CpswMdio_getPhyRegVal(CpswMdio *mdio, uint16 *val)
{
    sint32 retVal = CPSW_SOK;

    if (CSL_MDIO_USER_GROUP_USER_ACCESS_REG_ACK_PASS ==
        CSL_REG32_FEXT(&mdio->regs->USER_GROUP[0U].USER_ACCESS_REG,
                       MDIO_USER_GROUP_USER_ACCESS_REG_ACK))
    {
        *val = (uint16)CSL_REG32_FEXT(&mdio->regs->USER_GROUP[0].USER_ACCESS_REG,
                                        MDIO_USER_GROUP_USER_ACCESS_REG_DATA);
    }
    else
    {
        retVal = CPSW_EBUSY;
    }

    return retVal;
}

void CpswMdio_clearUserInt(CpswMdio *mdio)
{
    /* Clear the User Command Complete interrupt */
    CSL_REG32_FINS(&mdio->regs->USER_INT_MASKED_REG,
                   MDIO_USER_INT_MASKED_REG_USERINTMASKED,
                   1U);
}
