/*
*
* Copyright (c) 2019 Texas Instruments Incorporated
*
* All rights reserved not granted herein.
*
* Limited License.
*
* Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
* license under copyrights and patents it now or hereafter owns or controls to make,
* have made, use, import, offer to sell and sell ("Utilize") this software subject to the
* terms herein.  With respect to the foregoing patent license, such license is granted
* solely to the extent that any such patent is necessary to Utilize the software alone.
* The patent license shall not apply to any combinations which include this software,
* other than combinations with devices manufactured by or for TI ("TI Devices").
* No hardware patent is licensed hereunder.
*
* Redistributions must preserve existing copyright notices and reproduce this license
* (including the above copyright notice and the disclaimer and (if applicable) source
* code license limitations below) in the documentation and/or other materials provided
* with the distribution
*
* Redistribution and use in binary form, without modification, are permitted provided
* that the following conditions are met:
*
* *       No reverse engineering, decompilation, or disassembly of this software is
* permitted with respect to any software provided in binary form.
*
* *       any redistribution and use are licensed by TI for use only with TI Devices.
*
* *       Nothing shall obligate TI to provide you with source code for the software
* licensed and provided to you in object code.
*
* If software source code is provided to you, modification and redistribution of the
* source code are permitted provided that the following conditions are met:
*
* *       any redistribution and use of the source code, including any resulting derivative
* works, are licensed by TI for use only with TI Devices.
*
* *       any redistribution and use of any object code compiled from the source code
* and any resulting derivative works, are licensed by TI for use only with TI Devices.
*
* Neither the name of Texas Instruments Incorporated nor the names of its suppliers
*
* may be used to endorse or promote products derived from this software without
* specific prior written permission.
*
* DISCLAIMER.
*
* THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
* OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
* OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
* IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
* BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
* OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
* OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
* OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/**
 *  \file     Cpsw_Dma.c
 *
 *  \brief    This file contains CPPI-related functionality.
 */

/* ========================================================================== */
/*                             Include Files                                  */
/* ========================================================================== */

#include <string.h>
#include <ti/csl/arch/csl_arch.h>
#include <ti/drv/enet/enet.h>
#include <ti/drv/enet/include/core/enet_dma.h>
#include <ti/drv/enet/include/core/enet_rm.h>
#include <cpsw/Cpsw_Types.h>
#include <cpsw/Cpsw_Packet.h>
#include <cpsw/Cpsw_Dma.h>
#include <cpsw/Cpsw_Soc.h>

/* ========================================================================== */
/*                           Macros & Typedefs                                */
/* ========================================================================== */

/** \brief UDMA max teardown time (in msecs) */
#define CPSW_UDMA_TEARDOWN_TIME_MS      (100U)

/* ========================================================================== */
/*                         Structures and Enums                               */
/* ========================================================================== */

/* None */

/* ========================================================================== */
/*                 Internal Function Declarations                             */
/* ========================================================================== */

/**
 *  \brief Cache write-back invalidate
 *
 *  Performs a cache write-back invalidate operation using driver's registered
 *  write-back invalidate function.
 *
 *  \param dma      CpswDma handle
 *  \param buf      Buffer address
 *  \param len      Length (in bytes)
 */
inline void CpswDma_wbInvCache(CpswDma *dma,
                               uint8 *buf,
                               uint32 len)
{
    dma->cacheOps.cacheWbInv(buf, len);
}

/**
 *  \brief Cache write-back
 *
 *  Performs a cache write-back operation using driver's registered write-back
 *  function.
 *
 *  \param dma      CpswDma handle
 *  \param buf      Buffer address
 *  \param len      Length (in bytes)
 */
inline void CpswDma_wbCache(CpswDma *dma,
                            uint8 *buf,
                            uint32 len)
{
    dma->cacheOps.cacheWb(buf, len);
}

/**
 *  \brief Cache invalidate
 *
 *  Performs a cache invalidate operation using driver's registered invalidate
 *  function.
 *
 *  \param dma      CpswDma handle
 *  \param buf      Buffer address
 *  \param len      Length (in bytes)
 */
inline void CpswDma_invCache(CpswDma *dma,
                             uint8 *buf,
                             uint32 len)
{
    dma->cacheOps.cacheInv(buf, len);
}

/**
 *  \brief Initialize the DMA TX channel
 *
 *  Sets the UDMA TX channel params and opens the channel.
 *
 *  \param dma      CpswDma handle
 *  \param ch       TX CpswDmaCh handle
 *  \param config   Configuration information
 *  \param enetType CPSW Type config
 *
 *  \return \ref Cpsw_ErrorCodes
 */
sint32 CpswDma_initTxCh(CpswDma *dma, CpswDmaCh *ch, CpswDmaCh_Config *config, Enet_Type enetType);

/**
 *  \brief Initialize the DMA RX channel
 *
 *  Sets the UDMA RX channel params and opens the channel.
 *
 *  \param dma      CpswDma handle
 *  \param ch       RX CpswDmaCh handle
 *  \param config   Configuration information
 *  \param enetType CPSW Type config
 *
 *  \return \ref Cpsw_ErrorCodes
 */
sint32 CpswDma_initRxCh(CpswDma *dma, CpswDmaCh *ch, CpswDmaCh_Config *config, Enet_Type enetType);

/**
 *  \brief Initialize channel interrupt
 *
 *  Registers a DMA completion event with the UDMA driver.  The event is
 *  configured for the interrupt number passed to the function, but no actual
 *  interrupt registration takes place.
 *
 *  \param dma      CpswDma handle
 *  \param ch       CpswDmaCh handle
 *  \param intrNum  Interrupt number
 *
 *  \return \ref Cpsw_ErrorCodes
 */
sint32 CpswDma_initChIntr(CpswDma *dma, CpswDmaCh *ch, uint32 intrNum);

/**
 *  \brief Uninitialize channel interrupt
 *
 *  Unregisters the event with the UDMA driver.
 *
 *  \param dma      CpswDma handle
 *  \param ch       CpswDmaCh handle
 *
 *  \return \ref Cpsw_ErrorCodes
 */
sint32 CpswDma_uninitChIntr(CpswDma *dma, CpswDmaCh *ch);

/**
 *  \brief Initialize Rx Flow instance
 *
 *  \param dma          CpswDma handle
 *  \param pRxFlow      CpswDmaRxFlow *pRxFlow
 *  \param pRxFlowPrms  CpswDmaRxFlow_Config *pRxFlowPrms
 *
 *  \return \ref Cpsw_ErrorCodes
 */
static sint32 CpswDma_initRxFlow(CpswDma *dma,
                                  CpswDmaRxFlow *pRxFlow,
                                  CpswDmaRxFlow_Config *pRxFlowPrms);

/**
 *  \brief De initialize Rx Flow instance
 *
 *  \param pRxFlow      CpswDmaRxFlow *pRxFlow
 *
 *  \return \ref Cpsw_ErrorCodes
 */
static sint32 CpswDma_deInitRxFlow(CpswDmaRxFlow * pRxFlow);

/**
 *  \brief Allocate proxy for FQ ring
 *
 *  Allocates a dedicated proxy for FQ ring.
 *
 *  \param dma      CpswDma handle
 *  \param ch       CpswDmaCh handle
 *
 *  \return \ref Cpsw_ErrorCodes
 */
static sint32 CpswDma_allocFqRingProxy(CpswDma *dma, CpswDmaCh *ch);

/**
 *  \brief Free proxy for FQ ring if previously allocated
 *
 *  Frees the dedicated proxy for FQ ring if one was previously allocated
 *  via CpswDma_allocFqRingProxy().
 *
 *  \param dma      CpswDma handle
 *  \param ch       CpswDmaCh handle
 *
 *  \return \ref Cpsw_ErrorCodes
 */
static sint32 CpswDma_freeFqRingProxy(CpswDma *dma, CpswDmaCh *ch);

/**
 *  \brief Queue a descriptor to FQ ring
 *
 *  Queues a descriptor to channel's FQ ring either via dedicated proxy
 *  (if one was allocated previously) or common proxy through regular UDMA
 *  ring queue API.
 *
 *  \param dma      CpswDma handle
 *  \param ch       CpswDmaCh handle
 *  \param desc     Descriptor address
 *
 *  \return \ref Cpsw_ErrorCodes
 */
static sint32 CpswDma_fqRingQueue(CpswDma *dma, CpswDmaCh *ch, uint64_t desc);

/* ========================================================================== */
/*                            Global Variables                                */
/* ========================================================================== */

/* None */

/* ========================================================================== */
/*                  Internal Function Definitions                             */
/* ========================================================================== */

sint32 CpswDma_initTxCh(CpswDma *dma, CpswDmaCh *ch, CpswDmaCh_Config *config, Enet_Type enetType)
{
    Udma_DrvHandle udmaDrv = &dma->udmaDrv;
    Udma_ChHandle udmaCh = &ch->udmaCh;
    Udma_ChPrms chPrms;
    Udma_ChTxPrms txPrms;
    uint32 chType = UDMA_CH_TYPE_TX;
    sint32 retVal;
    uint32 txChPeerId = CpswSoc_getTxChPeerId (enetType,
                                                 0U /* instId */,
                                                 0U /* chNum*/);

    if ((config->txChPeerThreadId >= txChPeerId) &&
        (config->txChPeerThreadId < (txChPeerId + 8U /* socConfig->txChCount */)))
    {
        retVal = CPSW_SOK;
    }
    else
    {
        retVal = CPSW_EFAIL;
    }

    if (UDMA_SOK == retVal)
    {
        /* Initialize channel params (PSI-L thread and RingAcc memories) */
        UdmaChPrms_init(&chPrms, chType);
        chPrms.peerChNum            = config->txChPeerThreadId;

        chPrms.fqRingPrms.ringMem   = config->fqRingMem;
        chPrms.fqRingPrms.elemCnt   = config->ringEntryNum;
        chPrms.fqRingPrms.mode      = CSL_RINGACC_RING_MODE_RING;

        chPrms.cqRingPrms.ringMem   = config->cqRingMem;
        chPrms.cqRingPrms.elemCnt   = config->ringEntryNum;
        chPrms.cqRingPrms.mode      = CSL_RINGACC_RING_MODE_RING;

        chPrms.tdCqRingPrms.ringMem = config->tdCqRingMem;
        chPrms.tdCqRingPrms.elemCnt = config->ringEntryNum;
        chPrms.tdCqRingPrms.mode    = CSL_RINGACC_RING_MODE_RING;

        /* Open the UDMA channel */
        retVal = Udma_chOpen(udmaDrv, udmaCh, chType, &chPrms);
    }

    /* Configure TX parameters */
    if (UDMA_SOK == retVal)
    {
        UdmaChTxPrms_init(&txPrms, chType);
        retVal = Udma_chConfigTx(udmaCh, &txPrms);
        if (UDMA_SOK != retVal)
        {
            Udma_chClose(udmaCh);
        }
    }

    return retVal;
}

sint32 CpswDma_initRxCh(CpswDma *dma, CpswDmaCh *ch, CpswDmaCh_Config *config, Enet_Type enetType)
{
    Udma_DrvHandle udmaDrv = &dma->udmaDrv;
    Udma_ChHandle udmaCh = &ch->udmaCh;
    Udma_ChPrms chPrms;
    Udma_ChRxPrms rxPrms;
    uint32 chType = UDMA_CH_TYPE_RX;
    sint32 retVal;
    uint32 rxChPeerId = CpswSoc_getRxChPeerId(enetType, 0U /* instId */);

    if (ch->virtualMacMode == FALSE)
    {
        /* Initialize channel params (PSI-L thread and RingAcc memories) */
        UdmaChPrms_init(&chPrms, chType);
        chPrms.peerChNum            = rxChPeerId;

        chPrms.fqRingPrms.ringMem   = config->fqRingMem;
        chPrms.fqRingPrms.elemCnt   = config->ringEntryNum;
        chPrms.fqRingPrms.mode      = CSL_RINGACC_RING_MODE_RING;

        chPrms.cqRingPrms.ringMem   = config->cqRingMem;
        chPrms.cqRingPrms.elemCnt   = config->ringEntryNum;
        chPrms.cqRingPrms.mode      = CSL_RINGACC_RING_MODE_RING;

        chPrms.tdCqRingPrms.ringMem = config->tdCqRingMem;
        chPrms.tdCqRingPrms.elemCnt = config->ringEntryNum;
        chPrms.tdCqRingPrms.mode    = CSL_RINGACC_RING_MODE_RING;

        /* Open the UDMA channel */
        retVal = Udma_chOpen(udmaDrv, udmaCh, chType, &chPrms);

        /* Configure RX parameters */
        if (UDMA_SOK == retVal)
        {
            UdmaChRxPrms_init(&rxPrms, chType);
            retVal = Udma_chConfigRx(udmaCh, &rxPrms);
            if (UDMA_SOK != retVal)
            {
                Udma_chClose(udmaCh);
            }
        }
    }
    else
    {
        CpswDmaRxFlow_Config rxFlowConfig;
        Udma_RingPrms ringPrms;

        UdmaRingPrms_init(&ringPrms);
        rxFlowConfig.cqOrderId = ringPrms.orderId;
        rxFlowConfig.fqOrderId = ringPrms.orderId;
        rxFlowConfig.cqRingMem = config->cqRingMem;
        rxFlowConfig.fqRingMem = config->fqRingMem;
        rxFlowConfig.startIdx  = config->virtualMacConfig.startIdx;
        rxFlowConfig.flowIdx   = config->virtualMacConfig.flowIdx;
        rxFlowConfig.hUdmaDrv = &dma->udmaDrv;
        rxFlowConfig.numRxPkts = config->ringEntryNum;

        retVal = CpswDma_initRxFlow(dma, &ch->rxFlowObj, &rxFlowConfig);
    }

    return retVal;
}

sint32 CpswDma_initChIntr(CpswDma *dma, CpswDmaCh *ch, uint32 intrNum)
{
    Udma_DrvHandle udmaDrv = &dma->udmaDrv;
    Udma_EventHandle udmaEvt = &ch->udmaEvt;
    Udma_EventPrms evtPrms;
    sint32 retVal = CPSW_SOK;

    /* Register for the UDMA completion event. The UDMA driver sets up
     * the Int Aggr and Int Router according to our preferred interrupt
     * number.  The interrupt registration itself doesn't happen here */
    UdmaEventPrms_init(&evtPrms);
    evtPrms.eventType            = UDMA_EVENT_TYPE_RING;
    evtPrms.eventMode            = UDMA_EVENT_MODE_SHARED;
    evtPrms.ringHandle           = ch->cqRing;
    evtPrms.masterEventHandle    = NULL;
    evtPrms.osalRegisterDisable  = TRUE;
    evtPrms.preferredCoreIntrNum = intrNum;
    evtPrms.eventCb              = NULL;

    retVal = Udma_eventRegister(udmaDrv, udmaEvt, &evtPrms);

    /* Upon a successful event registration, the UDMA driver returns the
     * granted interrupt number, the clear and status registers as well as
     * the bit mask */
    if (UDMA_SOK == retVal)
    {
        if (evtPrms.coreIntrNum == evtPrms.preferredCoreIntrNum)
        {
            ch->intrClearReg  = evtPrms.intrClearReg;
            ch->intrStatusReg = evtPrms.intrStatusReg;
            ch->intrMask      = evtPrms.intrMask;
            ch->intrNum       = evtPrms.coreIntrNum;
            ch->isIntrInit    = TRUE;
        }
        else
        {
            /* Failure to allocate the exact interrupt being requested
             * is treated as an allocation error */
            Udma_eventUnRegister(udmaEvt);
            ch->intrClearReg  = NULL;
            ch->intrStatusReg = NULL;
            ch->intrMask      = 0U;
            retVal = CPSW_EALLOC;
        }
    }

    return retVal;
}

sint32 CpswDma_uninitChIntr(CpswDma *dma, CpswDmaCh *ch)
{
    Udma_EventHandle udmaEvt = &ch->udmaEvt;
    sint32 retVal = CPSW_SOK;

    /* Unregister the UDMA event */
    if (TRUE == ch->isIntrInit)
    {
        retVal = Udma_eventUnRegister(udmaEvt);
        ch->isIntrInit = FALSE;
    }

    return retVal;
}

static sint32 CpswDma_allocFqRingProxy(CpswDma *dma, CpswDmaCh *ch)
{
    Udma_DrvHandle udmaDrv = &dma->udmaDrv;
    Udma_ProxyHandle fqRingProxy = &ch->udmaProxy;
    Udma_ProxyCfg cfg;
    sint32 retVal;

    /* Proxy is only applicable for rings in message mode */
    if (Udma_ringGetMode(ch->fqRing) == CSL_RINGACC_RING_MODE_MESSAGE)
    {
        /* Allocate a proxy for FQ queue operation */
        retVal = Udma_proxyAlloc(udmaDrv, fqRingProxy, UDMA_PROXY_ANY);
        if (UDMA_SOK == retVal)
        {
            /* Config proxy for queue operation */
            cfg.proxyMode = CSL_PROXY_QUEUE_ACCESS_MODE_TAIL;
            cfg.elemSize  = UDMA_RING_ES_8BYTES;
            cfg.ringNum   = Udma_ringGetNum(ch->fqRing);

            retVal = Udma_proxyConfig(fqRingProxy, &cfg);
            if (UDMA_SOK != retVal)
            {
                Udma_proxyFree(fqRingProxy);
            }
        }
    }
    else
    {
        retVal = UDMA_EINVALID_PARAMS;
    }

    if (UDMA_SOK == retVal)
    {
        ch->fqRingProxy = fqRingProxy;
    }
    else
    {
        ch->fqRingProxy = NULL;
    }

    return retVal;
}

static sint32 CpswDma_freeFqRingProxy(CpswDma *dma, CpswDmaCh *ch)
{
    Udma_ProxyHandle fqRingProxy = ch->fqRingProxy;
    sint32 retVal = CPSW_SOK;

    /* Free FQ ring proxy if one was allocated */
    if (NULL != fqRingProxy)
    {
        retVal = Udma_proxyFree(fqRingProxy);
        if (UDMA_SOK == retVal)
        {
            ch->fqRingProxy = NULL;
        }
    }

    return retVal;
}

static sint32 CpswDma_fqRingQueue(CpswDma *dma, CpswDmaCh *ch, uint64_t desc)
{
    Udma_DrvHandle udmaDrv = &dma->udmaDrv;
    Udma_RingHandle fqRing = ch->fqRing;
    Udma_ProxyHandle fqRingProxy = ch->fqRingProxy;
    uint32 ringNum;
    uint32 occ;
    sint32 retVal = CPSW_SOK;

    if (NULL != fqRingProxy)
    {
        ringNum = Udma_ringGetNum(fqRing);

        /* Get ring occupancy. We should write to FIFO (through proxy) only
         * when there is room available. Otherwise ring will overflow */
        occ = CSL_ringaccGetRingHwOcc(&udmaDrv->raRegs, ringNum);
        if (occ < fqRing->cfg.elCnt)
        {
            Udma_proxyQueue(fqRingProxy, desc);
        }
        else
        {
            /* Ring full */
            retVal = UDMA_EFAIL;
        }
    }
    else
    {
        retVal = Udma_ringQueueRaw(fqRing, desc);
    }

    return retVal;
}

/* ========================================================================== */
/*                          Function Definitions                              */
/* ========================================================================== */

sint32 CpswDma_open(CpswDma *dma, CpswMcalDma_Config *cfg, uint32 udmaInstId)
{
    Udma_DrvHandle udmaDrv = &dma->udmaDrv;
    Udma_InitPrms initPrms;
    sint32 retVal = CPSW_SOK;

    /* Initialize the UDMA driver */
    UdmaInitPrms_init(udmaInstId, &initPrms);
    initPrms.skipGlobalEventReg = TRUE;
    initPrms.virtToPhyFxn = &CpswPacket_virtToPhys;
    initPrms.phyToVirtFxn = &CpswPacket_physToVirt;

    if ((NULL == cfg->cacheOps.cacheWbInv) ||
        (NULL == cfg->cacheOps.cacheWb) ||
        (NULL == cfg->cacheOps.cacheInv))
    {
        retVal = CPSW_EBADARGS;
    }

    if (CPSW_SOK == retVal)
    {
        retVal = Udma_init(udmaDrv, &initPrms);
        if (UDMA_SOK == retVal)
        {
            dma->cacheOps = cfg->cacheOps;
            dma->txCh.dir = CPSW_DIR_TX;
            dma->rxCh.dir = CPSW_DIR_RX;
            dma->isInit = TRUE;
        }
    }

    return retVal;
}

void CpswDma_close(CpswDma *dma)
{
    Udma_DrvHandle udmaDrv = &dma->udmaDrv;

    /* Deinitialize the UDMA driver */
    if (TRUE == dma->isInit)
    {
        Udma_deinit(udmaDrv);
        dma->isInit = FALSE;
    }
}

sint32 CpswDma_openCh(CpswDma *dma, CpswDmaCh *ch, CpswDmaCh_Config *config, Enet_Type enetType, boolean virtualMacMode)
{
    Udma_ChHandle udmaCh = &ch->udmaCh;
    CpswPkt *pkt;
    uint32 cqRingNum;
    uint32 i;
    sint32 retVal;

    CpswPktQ_init(&ch->freeQ);

    ch->virtualMacMode = virtualMacMode;

    /* Initialize according to its data direction */
    if (CPSW_DIR_TX == ch->dir)
    {
        retVal = CpswDma_initTxCh(dma, ch, config, enetType);
    }
    else
    {
        retVal = CpswDma_initRxCh(dma, ch, config, enetType);
    }

    if (CPSW_SOK == retVal)
    {
        if ((CPSW_DIR_TX == ch->dir) || (virtualMacMode == FALSE))
        {
            /* Get the fqRing and cqRing handles */
            ch->fqRing = Udma_chGetFqRingHandle(udmaCh);
            ch->cqRing = Udma_chGetCqRingHandle(udmaCh);
        }
        else
        {
            ch->fqRing = &ch->rxFlowObj.fqRingObj;
            ch->cqRing = &ch->rxFlowObj.cqRingObj;
        }

        if ((NULL == ch->fqRing) || (NULL == ch->cqRing))
        {
            ch->fqRing = NULL;
            ch->cqRing = NULL;
            retVal = CPSW_EFAIL;
        }
    }

    /* Allocate proxy for fqRing if possible */
    if (CPSW_SOK == retVal)
    {
        retVal = CpswDma_allocFqRingProxy(dma, ch);
        if (CPSW_SOK != retVal)
        {
            /* Fallback is shared proxy, so don't treat as error */
            retVal = CPSW_SOK;
        }
    }

    /* Setup DMA completion interrupt */
    if (CPSW_SOK == retVal)
    {
        if (CPSW_DMA_NO_INTR != config->intrNum)
        {
            retVal = CpswDma_initChIntr(dma, ch, config->intrNum);
            if (CPSW_SOK != retVal)
            {
                CpswDma_freeFqRingProxy(dma, ch);
                Udma_chClose(udmaCh);
            }
        }
        else
        {
            ch->intrNum = CPSW_DMA_NO_INTR;
        }
    }

    /* Enqueue all packets into freeQ */
    if (CPSW_SOK == retVal)
    {
        cqRingNum = Udma_ringGetNum(ch->cqRing);
        for (i = 0U; i < config->pktNum; i++)
        {
            pkt = &config->pktMem[i];
            CpswPkt_init(pkt);
            CpswPkt_setCqRing(pkt, cqRingNum);
            CpswPktQ_queue(&ch->freeQ, pkt);
        }
    }

    /* Save channel config */
    if (CPSW_SOK == retVal)
    {
        memcpy(&ch->config, config, sizeof(ch->config));
        ch->isInit = TRUE;
    }
    else
    {
        ch->isInit = FALSE;
    }

    return retVal;
}

sint32 CpswDma_closeCh(CpswDma *dma, CpswDmaCh *ch)
{
    Udma_ChHandle udmaCh = &ch->udmaCh;
    uint32 retVal = CPSW_SOK;

    if (TRUE == ch->isInit)
    {
        /* Release DMA completion interrupt */
        if ((CPSW_DMA_NO_INTR != ch->intrNum) &&
            (TRUE == ch->isIntrInit))
        {
            CpswDma_uninitChIntr(dma, ch);
        }

        /* Free fqRing proxy if one was allocated */
        CpswDma_freeFqRingProxy(dma, ch);

        if ((ch->dir == CPSW_DIR_TX) || (ch->virtualMacMode == FALSE))
        {
            /* Close the UDMA channel */
            retVal = Udma_chClose(udmaCh);
        }
        else
        {
            retVal = CpswDma_deInitRxFlow(&ch->rxFlowObj);
        }

        ch->fqRing = NULL;
        ch->cqRing = NULL;
        ch->isInit = FALSE;
    }

    return retVal;
}

boolean CpswDma_isInitCh(CpswDma *dma, CpswDmaCh *ch)
{
    return ch->isInit;
}

sint32 CpswDma_enableCh(CpswDma *dma, CpswDmaCh *ch)
{
    sint32 status;

    if (CPSW_DMA_NO_INTR != ch->intrNum)
    {
        CpswDma_clearIntr(dma, ch);
    }

    if ((ch->dir == CPSW_DIR_TX) || (ch->virtualMacMode == FALSE))
    {
        Udma_ChHandle udmaCh = &ch->udmaCh;

        status = Udma_chEnable(udmaCh);
    }
    else
    {
        status = CPSW_SOK;
    }
    /* Enable the UDMA channel */
    return status;
}

sint32 CpswDma_disableCh(CpswDma *dma, CpswDmaCh *ch)
{
    Udma_ChHandle udmaCh = &ch->udmaCh;
    CSL_UdmapTdResponse tdResp;
    CpswPktQ tempQ;
    CpswPkt *pkt;
    uint64_t desc = 0ULL;
    sint32 retVal;

    retVal = UDMA_SOK;
    if ((CPSW_DIR_TX == ch->dir) || (ch->virtualMacMode == FALSE))
    {
        /* Disable the UDMA channel */
        retVal = Udma_chDisable(udmaCh, CPSW_UDMA_TEARDOWN_TIME_MS);

        /* Dequeue the teardown response */
        if (UDMA_SOK == retVal)
        {
            do
            {
                retVal = Udma_chDequeueTdResponse(udmaCh, &tdResp);
            }
            while (UDMA_SOK != retVal);

            if (FALSE == tdResp.tdIndicator)
            {
                retVal = CPSW_EFAIL;
            }
        }
    }
    /* TODO: Decode response information. Return an error if teardown
     * is incomplete or if teardown was not graceful and data was
     * potentially lost */

    /* Retrieve any packets that haven't been processed yet */
    if (UDMA_SOK == retVal)
    {
        CpswPktQ_init(&tempQ);

        /* Dequeue the packets still sitting in the fqRing */
        retVal = Udma_ringFlushRaw(ch->fqRing, &desc);
        while ((UDMA_SOK == retVal) && (0ULL != desc))
        {
            pkt = (CpswPkt *)(uintptr_t)desc;
            CpswPktQ_queue(&tempQ, pkt);
            retVal = Udma_ringFlushRaw(ch->fqRing, &desc);
        }

        /* Move those packets back into the freeQ */
        CpswPktQ_prepend(&ch->freeQ, &tempQ);
        retVal = CPSW_SOK;
    }

    /* Retrieve any packets already processed */
    if (UDMA_SOK == retVal)
    {
        CpswPktQ_init(&tempQ);

        /* Dequeue the packets still sitting in the cqRing */
        retVal = CpswDma_retrievePktQ(dma, ch, &tempQ);
        if (CPSW_SOK == retVal)
        {
            /* Queue those packets into the freeQ */
            CpswPktQ_prepend(&ch->freeQ, &tempQ);
        }
    }

    return retVal;
}

uint32 CpswDma_getDefaultFlowNum(CpswDma *dma, CpswDmaCh *ch)
{
    Udma_FlowHandle flow;

    flow = Udma_chGetDefaultFlowHandle(&ch->udmaCh);
    return Udma_flowGetNum(flow);
}

void CpswDma_clearIntr(CpswDma *dma, CpswDmaCh *ch)
{
    uint64_t val;

    if (CPSW_DMA_NO_INTR != ch->intrNum)
    {
        /* Check IA status */
        val = CSL_REG64_RD(ch->intrStatusReg);
        if (val & ch->intrMask)
        {
            /* Clear the interrupt */
            CSL_REG64_WR(ch->intrClearReg, ch->intrMask);
        }
    }
}

sint32 CpswDma_submitPkt(CpswDma *dma, CpswDmaCh *ch, CpswPkt *pkt)
{
#if (STD_ON == ETH_RING_IN_CACHED_MEMORY)
    uint8 *ringMemPtr;
    uint32 ringMemEleCnt;
#endif
    sint32 retVal = CPSW_SOK;

    if (NULL != pkt)
    {
#if (STD_ON == ETH_DEV_ERROR_DETECT)
        /* Check that the buffer pointer and length are correct */
        if (FALSE == CpswPkt_check(pkt))
        {
            retVal = CPSW_EBADARGS;
        }
#endif
        /* Enqueue desc to fqRing */
        if (CPSW_SOK == retVal)
        {
#if (STD_ON == ETH_DESC_IN_CACHED_MEMORY)
            CpswDma_wbInvCache(dma, (uint8 *)pkt, sizeof(CpswPkt));
#endif

            if (Udma_ringGetMode(ch->fqRing) == CSL_RINGACC_RING_MODE_RING)
            {
                /* Udma_ringPrime doesn't do any error checks, so be sure about
                 * the values passed before using this function */
                Udma_ringPrime(ch->fqRing, pkt->phyAddr);

#if (STD_ON == ETH_RING_IN_CACHED_MEMORY)
                ringMemPtr = (uint8 *)Udma_ringGetMemPtr(ch->fqRing);
                ringMemEleCnt = Udma_ringGetElementCnt(ch->fqRing);

                /* Write-back the ring memory cache before committing to fqRing */
                CpswDma_wbCache(dma, ringMemPtr, ringMemEleCnt * ENET_UDMA_RING_MEM_SIZE);
#endif

                /* Set ring door bell with number of descs queued */
                Udma_ringSetDoorBell(ch->fqRing, 1U);
            }
            else
            {
                retVal = CpswDma_fqRingQueue(dma, ch, (uint64_t)pkt->phyAddr);
            }
        }
    }
    else
    {
        retVal = CPSW_EBADARGS;
    }

    return retVal;
}

sint32 CpswDma_submitPktQ(CpswDma *dma, CpswDmaCh *ch, CpswPktQ *queue)
{
    CpswPkt *pkt = NULL;
    boolean isExposedRing;
#if (STD_ON == ETH_RING_IN_CACHED_MEMORY)
    uint8 *ringMemPtr;
    uint32 ringMemEleCnt;
#endif
    uint32 cnt = 0U;
    sint32 retVal = CPSW_SOK;

    isExposedRing = (Udma_ringGetMode(ch->fqRing) == CSL_RINGACC_RING_MODE_RING);

    /* Enqueue descs to fqRing regardless of caller's queue state */
    if (CpswPktQ_getCount(queue) > 0U)
    {
        /* Enqueue packets until fqRing is full */
        pkt = CpswPktQ_dequeue(queue);
        while (NULL != pkt)
        {
            /* Check that the buffer pointer and length are correct */
#if (STD_ON == ETH_DEV_ERROR_DETECT)
            if (FALSE == CpswPkt_check(pkt))
            {
                retVal = CPSW_EBADARGS;
                CpswPktQ_queueHead(queue, pkt);
            }
            else
#endif
            {
#if (STD_ON == ETH_DESC_IN_CACHED_MEMORY)
                CpswDma_wbInvCache(dma, (uint8 *)pkt, sizeof(CpswPkt));
#endif

                /* Enqueue desc to fqRing */
                if (isExposedRing)
                {
                    /* Udma_ringPrime doesn't do any error checks, so be sure about
                     * the values passed before using this function */
                    Udma_ringPrime(ch->fqRing, pkt->phyAddr);
                    cnt++;
                }
                else
                {
                    retVal = CpswDma_fqRingQueue(dma, ch, (uint64_t)pkt->phyAddr);
                    if (CPSW_SOK != retVal)
                    {
                        CpswPktQ_queueHead(queue, pkt);
                        break;
                    }
                }

                pkt = CpswPktQ_dequeue(queue);
            }
        }

        if (isExposedRing)
        {
#if (STD_ON == ETH_RING_IN_CACHED_MEMORY)
            ringMemPtr = (uint8 *)Udma_ringGetMemPtr(ch->fqRing);
            ringMemEleCnt = Udma_ringGetElementCnt(ch->fqRing);

            /* Write-back the ring memory cache before committing to the fqRing */
            CpswDma_wbCache(dma, ringMemPtr, ringMemEleCnt * ENET_UDMA_RING_MEM_SIZE);
#endif

            /* Set ring door bell with number of descs queued */
            Udma_ringSetDoorBell(ch->fqRing, cnt);
        }
    }

    return retVal;
}

sint32 CpswDma_retrievePktQ(CpswDma *dma, CpswDmaCh *ch, CpswPktQ *queue)
{
    boolean isExposedRing;
    CpswPkt *pkt = NULL;
    uint64_t desc = 0ULL;
#if (STD_ON == ETH_RING_IN_CACHED_MEMORY)
    uint8 *ringMemPtr;
    uint32 ringMemEleCnt;
#endif
    uint32 ringOcc;
    uint32 i;
    sint32 cnt = 0U;
    sint32 retVal = CPSW_SOK;

    isExposedRing = (Udma_ringGetMode(ch->cqRing) == CSL_RINGACC_RING_MODE_RING);

    if (NULL != queue)
    {
        if (isExposedRing)
        {
#if (STD_ON == ETH_RING_IN_CACHED_MEMORY)
            ringMemPtr = (uint8 *)Udma_ringGetMemPtr(ch->cqRing);
            ringMemEleCnt = Udma_ringGetElementCnt(ch->cqRing);

            /* Invalidate the ring memory before reading from the cqRing */
            CpswDma_invCache(dma, ringMemPtr, (ringMemEleCnt * ENET_UDMA_RING_MEM_SIZE));
#endif

            ringOcc = Udma_ringGetReverseRingOcc(ch->cqRing);
            for (i = 0U; i < ringOcc; i++)
            {
                Udma_ringPrimeRead(ch->cqRing, &desc);

                /* Queue into caller's queue */
                desc = (uint64_t)CpswPacket_physToVirt(desc, NULL, NULL);
                if (0ULL != desc)
                {
                    pkt = (CpswPkt *)(uintptr_t)desc;
#if (STD_ON == ETH_DESC_IN_CACHED_MEMORY)
                    CpswDma_invCache(dma, (uint8 *)pkt, sizeof(CpswPkt));
#endif
                    CpswPktQ_queue(queue, pkt);
                }

                cnt++;
            }

            if (ringOcc > 0U)
            {
                /* Make the count negative as the occupancy needs to be
                 * reduced after popping from cqRing */
                if (cnt != 0)
                {
                    cnt *= -1;

                    /* Set ring door bell with the number of descs dequeued */
                    /* Important Note: Completion interrupt will be cleared
                     * when ring occupancy becomes 0. It's possible that new
                     * descs arrived into ring while we were emptying the
                     * ring for "ringOcc" elements.  In that case, the actual
                     * ring occupancy will not reach 0 and interrupt will
                     * not be cleared, effectively causing ISR to run again.
                     * Otherwise, interrupt gets cleared until next time
                     * more freed descs land in the cqRing.
                     * It's very important that completion interrupt is not
                     * forcefully cleared in the ISR. */
                    Udma_ringSetDoorBell(ch->cqRing, cnt);
                }
            }
            else
            {
                retVal = UDMA_ETIMEOUT;
            }
        }
        else
        {
            /* Dequeue descs from cqRing */
            retVal = Udma_ringDequeueRaw(ch->cqRing, &desc);
            while (UDMA_SOK == retVal)
            {
                /* Check that the desc address from cqRing is valid */
                if (0ULL == desc)
                {
                    /* Packets already dequeued from cqRing will be kept in freeQ */
                    retVal = CPSW_EFAIL;
                }

                /* Queue into caller's queue */
                if (CPSW_SOK == retVal)
                {
                    desc = (uint64_t)CpswPacket_physToVirt(desc, NULL, NULL);
                    CpswPktQ_queue(queue, (CpswPkt *)(uintptr_t)desc);
                    retVal = Udma_ringDequeueRaw(ch->cqRing, &desc);
                }
            }
        }
    }
    else
    {
        retVal = UDMA_EBADARGS;
    }

    /* It's ok if there are no more descs in cqRing */
    if (UDMA_ETIMEOUT == retVal)
    {
        retVal = CPSW_SOK;
    }

    return retVal;
}

void CpswDma_reclaimPktQ(CpswDma *dma, CpswDmaCh *ch, CpswPktQ *queue)
{
    CpswPktQ_copy(queue, &ch->freeQ);
    CpswPktQ_init(&ch->freeQ);
}


static sint32 CpswDma_allocRxRing(CpswDma *dma,
                                   Udma_DrvHandle hUdmaDrv,
                                   Udma_RingHandle hUdmaRing,
                                   Udma_RingPrms *pRingPrms,
                                   void          *ringMem)
{
    sint32 retVal = CPSW_SOK;

    if (NULL == ringMem)
    {
        retVal = CPSW_EBADARGS;
    }

    if (CPSW_SOK == retVal)
    {
        if (!ENET_UTILS_IS_ALIGNED(ringMem, UDMA_CACHELINE_ALIGNMENT))
        {
            retVal  = CPSW_EALLOC;
        }
    }

    if (CPSW_SOK == retVal)
    {
        /* Clear and invalidate ring memory */
        memset(ringMem, 0, (pRingPrms->elemCnt * ENET_UDMA_RING_MEM_SIZE));
        if (FALSE == Udma_isCacheCoherent())
        {
#if (STD_ON == ETH_RING_IN_CACHED_MEMORY)
            CpswDma_wbInvCache(dma, (uint8 *)ringMem, pRingPrms->elemCnt * ENET_UDMA_RING_MEM_SIZE);
#endif
        }

        /* need to allocate rxfree/rxcompletion ring pair */
        pRingPrms->ringMem = ringMem;
        retVal = Udma_ringAlloc(hUdmaDrv,
                                hUdmaRing,
                                UDMA_RING_ANY,
                                pRingPrms);
    }

    return retVal;
}

static sint32 CpswDma_checkRxFlowParams(CpswDmaRxFlow_Config *pRxFlowPrms)
{
    sint32 retVal = CPSW_SOK;

    if (NULL == pRxFlowPrms)
    {
        retVal = CPSW_EBADARGS;
    }
    else
    {
        if (NULL == pRxFlowPrms->hUdmaDrv)
        {
            retVal = CPSW_EBADARGS;
        }
    }

    if (UDMA_SOK == retVal)
    {
        if (ENET_RM_RXFLOWIDX_INVALID == pRxFlowPrms->flowIdx)
        {
            retVal = CPSW_EBADARGS;
        }
    }

    return retVal;
}

static sint32 CpswDma_freeRxRing(Udma_RingHandle hUdmaRing)
{
    sint32 retVal;

    retVal     = Udma_ringFree(hUdmaRing);

    return retVal;
}


static sint32 CpswDma_initRxFlow(CpswDma *dma,
                                  CpswDmaRxFlow *pRxFlow,
                                  CpswDmaRxFlow_Config *pRxFlowPrms)
{
    sint32 retVal;
    Udma_RingHandle fqRing, cqRing;
    Udma_FlowPrms flowPrms;
    Udma_RingPrms ringPrms;
    Udma_FlowHandle hUdmaFlow;
    boolean allocFqRing = FALSE;
    boolean allocCqRing  = FALSE, flowAttachFlag = FALSE;
    uint8 flowStart;

    /* Error check */
    retVal = CpswDma_checkRxFlowParams(pRxFlowPrms);

    if (UDMA_SOK == retVal)
    {
        memset(pRxFlow, 0, sizeof(*pRxFlow));
        /* Save Flow config */
        pRxFlow->rxFlowPrms = *pRxFlowPrms;
        /* Initialize local variables to be used in reminder of this function  */
        pRxFlow->hUdmaDrv = pRxFlowPrms->hUdmaDrv;
        fqRing            = &pRxFlow->fqRingObj;
        cqRing            = &pRxFlow->cqRingObj;
    }

    if (UDMA_SOK == retVal)
    {
        UdmaRingPrms_init(&ringPrms);
        ringPrms.elemCnt = pRxFlowPrms->numRxPkts;
        ringPrms.orderId = pRxFlowPrms->fqOrderId;
        ringPrms.mode    = CSL_RINGACC_RING_MODE_RING;
        retVal           = CpswDma_allocRxRing(dma,
                                               pRxFlow->hUdmaDrv,
                                               fqRing,
                                               &ringPrms,
                                               pRxFlowPrms->fqRingMem);
        if (UDMA_SOK == retVal)
        {
            allocFqRing = TRUE;
        }
    }

    if (UDMA_SOK == retVal)
    {
        UdmaRingPrms_init(&ringPrms);
        ringPrms.elemCnt = pRxFlowPrms->numRxPkts;
        ringPrms.orderId = pRxFlowPrms->cqOrderId;
        ringPrms.mode    = CSL_RINGACC_RING_MODE_RING;
        retVal           = CpswDma_allocRxRing(dma,
                                               pRxFlow->hUdmaDrv,
                                               cqRing,
                                               &ringPrms,
                                               pRxFlowPrms->cqRingMem);
        if (UDMA_SOK == retVal)
        {
            allocCqRing = TRUE;
        }
    }

    if (UDMA_SOK == retVal)
    {
        memset(&flowPrms, 0, sizeof(flowPrms));
        UdmaFlowPrms_init(&flowPrms, UDMA_CH_TYPE_RX);

        /* UdmaFlowPrms_init sets errorHandling to retry(1) by default,
         * in case of descriptor/buffer starvation UDMA retries unless it
         * gets a descriptor. In case of multi flow, this results in bottom
         * of FIFO drop, to avoid this errorHandling must be set to drop(0).
         */
        flowPrms.errorHandling = TISCI_MSG_VALUE_RM_UDMAP_RX_FLOW_ERR_DROP;
        flowPrms.psInfoPresent = TISCI_MSG_VALUE_RM_UDMAP_RX_FLOW_PSINFO_PRESENT;

        flowPrms.defaultRxCQ = cqRing->ringNum;

        flowPrms.fdq0Sz0Qnum = fqRing->ringNum;
        flowPrms.fdq1Qnum    = fqRing->ringNum;
        flowPrms.fdq2Qnum    = fqRing->ringNum;
        flowPrms.fdq3Qnum    = fqRing->ringNum;

        flowStart = pRxFlowPrms->startIdx + pRxFlowPrms->flowIdx;

        /* Attach and configure the flows */
        retVal = Udma_flowAttach(pRxFlow->hUdmaDrv,
                                 &pRxFlow->flowUdmaObj,
                                 flowStart,
                                 1U /* flowCnt */);

        if (UDMA_SOK == retVal)
        {
            flowAttachFlag = TRUE;
            hUdmaFlow      = &pRxFlow->flowUdmaObj;
            retVal         = Udma_flowConfig(hUdmaFlow,
                                             0U, /* Flow Index */
                                             &flowPrms);
        }
    }


    if (UDMA_SOK == retVal)
    {
        pRxFlow->initFlag    = TRUE;
    }
    else
    {
        /* Error. Free-up resource if allocated */
        if (flowAttachFlag)
        {
            Udma_flowDetach(&pRxFlow->flowUdmaObj);
        }

        if (allocFqRing)
        {
            CpswDma_freeRxRing(&pRxFlow->fqRingObj);
        }

        if (allocCqRing)
        {
            /* We free cqRing without retrieving packets here as packets are
             * not submitted by application yet so there wouldn't be any packets
             * in the cqRing */
            CpswDma_freeRxRing(&pRxFlow->cqRingObj);
        }

    }
    return retVal;
}



static sint32 CpswDma_deInitRxFlow(CpswDmaRxFlow * pRxFlow)
{
    sint32 retVal = UDMA_SOK;

    if (UDMA_SOK == retVal)
    {
        retVal = Udma_flowDetach(&pRxFlow->flowUdmaObj);
    }

    if (UDMA_SOK == retVal)
    {
        retVal = CpswDma_freeRxRing(&pRxFlow->fqRingObj);
    }

    if (UDMA_SOK == retVal)
    {
        retVal = CpswDma_freeRxRing(&pRxFlow->cqRingObj);
    }

    return retVal;
}


