/*
*
* Copyright (c) 2019 Texas Instruments Incorporated
*
* All rights reserved not granted herein.
*
* Limited License.
*
* Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
* license under copyrights and patents it now or hereafter owns or controls to make,
* have made, use, import, offer to sell and sell ("Utilize") this software subject to the
* terms herein.  With respect to the foregoing patent license, such license is granted
* solely to the extent that any such patent is necessary to Utilize the software alone.
* The patent license shall not apply to any combinations which include this software,
* other than combinations with devices manufactured by or for TI ("TI Devices").
* No hardware patent is licensed hereunder.
*
* Redistributions must preserve existing copyright notices and reproduce this license
* (including the above copyright notice and the disclaimer and (if applicable) source
* code license limitations below) in the documentation and/or other materials provided
* with the distribution
*
* Redistribution and use in binary form, without modification, are permitted provided
* that the following conditions are met:
*
* *       No reverse engineering, decompilation, or disassembly of this software is
* permitted with respect to any software provided in binary form.
*
* *       any redistribution and use are licensed by TI for use only with TI Devices.
*
* *       Nothing shall obligate TI to provide you with source code for the software
* licensed and provided to you in object code.
*
* If software source code is provided to you, modification and redistribution of the
* source code are permitted provided that the following conditions are met:
*
* *       any redistribution and use of the source code, including any resulting derivative
* works, are licensed by TI for use only with TI Devices.
*
* *       any redistribution and use of any object code compiled from the source code
* and any resulting derivative works, are licensed by TI for use only with TI Devices.
*
* Neither the name of Texas Instruments Incorporated nor the names of its suppliers
*
* may be used to endorse or promote products derived from this software without
* specific prior written permission.
*
* DISCLAIMER.
*
* THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
* OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
* OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
* IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
* BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
* OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
* OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
* OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/**
 *  \file     Cpsw_Packet.c
 *
 *  \brief    This file contains packet and packet queue related functionality.
 */

/* ========================================================================== */
/*                             Include Files                                  */
/* ========================================================================== */

#include <cpsw/Cpsw_Packet.h>

/* ========================================================================== */
/*                           Macros & Typedefs                                */
/* ========================================================================== */

/* None */

/* ========================================================================== */
/*                         Structures and Enums                               */
/* ========================================================================== */

/* None */

/* ========================================================================== */
/*                 Internal Function Declarations                             */
/* ========================================================================== */

/* None */

/* ========================================================================== */
/*                            Global Variables                                */
/* ========================================================================== */

/* None */

/* ========================================================================== */
/*                  Internal Function Definitions                             */
/* ========================================================================== */

/* None */

/* ========================================================================== */
/*                          Function Definitions                              */
/* ========================================================================== */

#if defined (BUILD_MCU1_1)
#define CPSW_BTCM_PHYS_BASE             CSL_MCU_R5FSS0_CORE1_BTCM_BASE
#define CPSW_BTCM_SIZE                  CSL_MCU_R5FSS0_CORE1_BTCM_SIZE
#if defined (SOC_J721E)
#define CPSW_BTCM_VIRT_BASE             CSL_MCU_ARMSS_BTCM_BASE
#elif defined (SOC_J7200)
#define CPSW_BTCM_VIRT_BASE             CSL_MCU_R5FSS0_BTCM_BASE
#endif

#elif defined (BUILD_MCU2_1)
#define CPSW_BTCM_PHYS_BASE             CSL_R5FSS0_CORE1_BTCM_BASE
#define CPSW_BTCM_SIZE                  CSL_R5FSS0_CORE1_BTCM_SIZE
#if defined (SOC_J721E)
#define CPSW_BTCM_VIRT_BASE             CSL_ARMSS_BTCM_BASE
#elif defined (SOC_J7200)
#define CPSW_BTCM_VIRT_BASE             CSL_R5FSS0_BTCM_BASE
#endif
#endif

uint64_t CpswPacket_virtToPhys(const void *virtAddr, uint32 chNum, void *appData)
{
    uint64_t phyAddr = (uint64_t)virtAddr;

    /* Convert local L2RAM address to global space */
#if defined (BUILD_MCU1_0)
    /* No special mapping required for MCU R5F Core 0's BTCM */
#elif defined (BUILD_MCU1_1) || defined (BUILD_MCU2_1)
    if ((phyAddr >= CPSW_BTCM_VIRT_BASE) &&
        (phyAddr < (CPSW_BTCM_VIRT_BASE + CPSW_BTCM_SIZE)))
    {
        phyAddr -= CPSW_BTCM_VIRT_BASE;
        phyAddr += CPSW_BTCM_PHYS_BASE;
    }
#else
#error "Core not supported"
#endif

    return phyAddr;
}

void *CpswPacket_physToVirt(uint64_t phyAddr, uint32 chNum, void *appData)
{
    void *virtAddr;

    /* Convert global L2RAM address to local space */
#if defined (BUILD_MCU1_0)
    /* No special mapping required for MCU R5F Core 0's BTCM */
#elif defined (BUILD_MCU1_1) || defined (BUILD_MCU2_1)
    if ((phyAddr >= CPSW_BTCM_PHYS_BASE) &&
        (phyAddr < (CPSW_BTCM_PHYS_BASE + CPSW_BTCM_SIZE)))
    {
        phyAddr -= CPSW_BTCM_PHYS_BASE;
        phyAddr += CPSW_BTCM_VIRT_BASE;
    }
#else
#error "Core not supported"
#endif

    /* R5 is 32-bit; need to truncate to avoid void * typecast error */
    virtAddr = (void *)(uint32)phyAddr;

    return virtAddr;
}

void CpswPkt_init(CpswPkt *pkt)
{
    pkt->next = NULL;
    pkt->priv = (void *)NULL;
    pkt->phyAddr = CpswPacket_virtToPhys((const void *)pkt, NULL, NULL);
    CpswPkt_initDesc(pkt);
}

void CpswPkt_initDesc(CpswPkt *pkt)
{
    CSL_UdmapCppi5HMPD *desc = (CSL_UdmapCppi5HMPD *)pkt;
    uint32 descType = (uint32)CSL_UDMAP_CPPI5_PD_DESCINFO_DTYPE_VAL_HOST;

    CSL_udmapCppi5SetDescType(desc, descType);
    CSL_udmapCppi5SetEpiDataPresent(desc, FALSE);
    CSL_udmapCppi5SetPsDataLoc(desc, 0U);
    CSL_udmapCppi5SetPsDataLen(desc, 0U);
    CSL_udmapCppi5HostSetPktLen(desc, 0U);
    CSL_udmapCppi5SetPsFlags(desc, 0U);
    CSL_udmapCppi5SetIds(desc, descType, 0U, 0U);
    CSL_udmapCppi5SetSrcTag(desc, 0x0000U);
    CSL_udmapCppi5SetDstTag(desc, 0x0000U);
    CSL_udmapCppi5LinkDesc(desc, 0U);
    CSL_udmapCppi5SetBufferAddr(desc, (uint64_t)NULL);
    CSL_udmapCppi5SetBufferLen(desc, 0U);
    CSL_udmapCppi5SetOrgBufferAddr(desc, (uint64_t)NULL);
    CSL_udmapCppi5SetOrgBufferLen(desc, 0U);
}

void CpswPkt_setBuf(CpswPkt *pkt, void *buf, uint32 len)
{
    CSL_UdmapCppi5HMPD *desc = (CSL_UdmapCppi5HMPD *)pkt;

    CSL_udmapCppi5HostSetPktLen(desc, len);
    CSL_udmapCppi5SetBufferAddr(desc, (uint64_t)buf);
    CSL_udmapCppi5SetBufferLen(desc, len);
    CSL_udmapCppi5SetOrgBufferAddr(desc, (uint64_t)buf);
    CSL_udmapCppi5SetOrgBufferLen(desc, len);
}

void CpswPkt_setCqRing(CpswPkt *pkt, uint32 ringNum)
{
    CSL_UdmapCppi5HMPD *desc = (CSL_UdmapCppi5HMPD *)pkt;
    uint32 descType = (uint32)CSL_UDMAP_CPPI5_PD_DESCINFO_DTYPE_VAL_HOST;

    CSL_udmapCppi5SetReturnPolicy(
        desc,
        descType,
        CSL_UDMAP_CPPI5_PD_PKTINFO2_RETPOLICY_VAL_ENTIRE_PKT,
        CSL_UDMAP_CPPI5_PD_PKTINFO2_EARLYRET_VAL_NO,
        CSL_UDMAP_CPPI5_PD_PKTINFO2_RETPUSHPOLICY_VAL_TO_TAIL,
        ringNum);
}

void CpswPkt_setPriv(CpswPkt *pkt, void *priv)
{
    pkt->priv = priv;
}

void *CpswPkt_getPriv(CpswPkt *pkt)
{
    return pkt->priv;
}

uint32 CpswPkt_getLen(CpswPkt *pkt)
{
    CSL_UdmapCppi5HMPD *desc = (CSL_UdmapCppi5HMPD *)pkt;
    return CSL_udmapCppi5GetBufferLen(desc);
}

boolean CpswPkt_check(CpswPkt *pkt)
{
    CSL_UdmapCppi5HMPD *desc = (CSL_UdmapCppi5HMPD *)pkt;
    boolean valid = TRUE;

    /* Is the desc memory aligned? */
    if (0ULL != ((uint64_t)desc % CPSW_DESC_SIZE))
    {
        valid = FALSE;
    }

    /* Is the desc of Host Packet type? */
    if (valid == TRUE)
    {
        if ((uint32)CSL_UDMAP_CPPI5_PD_DESCINFO_DTYPE_VAL_HOST !=
                CSL_udmapCppi5GetDescType(desc))
        {
            valid = FALSE;
        }
    }

    /* Is the buffer address not NULL? */
    if (valid == TRUE)
    {
        if (0ULL == CSL_udmapCppi5GetBufferAddr(desc))
        {
            valid = FALSE;
        }
    }

    /* Is the buffer length > 0? */
    if (valid == TRUE)
    {
        if (0U == CSL_udmapCppi5GetBufferLen(desc))
        {
            valid = FALSE;
        }
    }

    return valid;
}

void CpswPktQ_init(CpswPktQ *queue)
{
    queue->head = NULL;
    queue->tail = NULL;
    queue->count = 0U;
}

void CpswPktQ_queueHead(CpswPktQ *queue, CpswPkt *pkt)
{
    if (0U == queue->count)
    {
        queue->tail = pkt;
    }

    pkt->next = queue->head;
    queue->head = pkt;
    queue->count++;
}

void CpswPktQ_queue(CpswPktQ *queue, CpswPkt *pkt)
{
    /* Enqueue just one packet */
    pkt->next = NULL;

    if (NULL == queue->head)
    {
        queue->head = pkt;
    }
    else
    {
        queue->tail->next = pkt;
    }

    queue->tail = pkt;
    queue->count++;
}

CpswPkt *CpswPktQ_dequeue(CpswPktQ *queue)
{
    CpswPkt *pkt;

    pkt = queue->head;
    if (NULL != pkt)
    {
        queue->head = pkt->next;
        if (NULL == queue->head)
        {
            queue->tail = NULL;
        }

        queue->count--;
        pkt->next = NULL;
    }

    return pkt;
}

CpswPkt *CpswPktQ_peek(CpswPktQ *queue)
{
    return queue->head;
}

void CpswPktQ_copy(CpswPktQ *dst, CpswPktQ *src)
{
    dst->head = src->head;
    dst->tail = src->tail;
    dst->count = src->count;
}

void CpswPktQ_append(CpswPktQ *dst, CpswPktQ *src)
{
    if (NULL == dst->head)
    {
        dst->head = src->head;
        dst->count = src->count;
    }
    else
    {
        dst->tail->next = src->head;
        dst->count += src->count;
    }

    dst->tail = src->tail;
}

void CpswPktQ_prepend(CpswPktQ *dst, CpswPktQ *src)
{
    if (NULL != src->head)
    {
        if (NULL == dst->head)
        {
            dst->tail = src->tail;
            dst->count = src->count;
        }
        else
        {
            src->tail->next = dst->head;
            dst->count += src->count;
            src->tail = dst->tail;
            src->count = dst->count;
        }

        dst->head = src->head;
    }
}

CpswPkt *CpswPktQ_getFirstPkt(CpswPktQ *queue)
{
    return queue->head;
}

CpswPkt *CpswPktQ_getLastPkt(CpswPktQ *queue)
{
    return queue->tail;
}

uint32 CpswPktQ_getCount(CpswPktQ *queue)
{
    return queue->count;
}
