/*
*
* Copyright (c) 2019 Texas Instruments Incorporated
*
* All rights reserved not granted herein.
*
* Limited License.
*
* Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
* license under copyrights and patents it now or hereafter owns or controls to make,
* have made, use, import, offer to sell and sell ("Utilize") this software subject to the
* terms herein.  With respect to the foregoing patent license, such license is granted
* solely to the extent that any such patent is necessary to Utilize the software alone.
* The patent license shall not apply to any combinations which include this software,
* other than combinations with devices manufactured by or for TI ("TI Devices").
* No hardware patent is licensed hereunder.
*
* Redistributions must preserve existing copyright notices and reproduce this license
* (including the above copyright notice and the disclaimer and (if applicable) source
* code license limitations below) in the documentation and/or other materials provided
* with the distribution
*
* Redistribution and use in binary form, without modification, are permitted provided
* that the following conditions are met:
*
* *       No reverse engineering, decompilation, or disassembly of this software is
* permitted with respect to any software provided in binary form.
*
* *       any redistribution and use are licensed by TI for use only with TI Devices.
*
* *       Nothing shall obligate TI to provide you with source code for the software
* licensed and provided to you in object code.
*
* If software source code is provided to you, modification and redistribution of the
* source code are permitted provided that the following conditions are met:
*
* *       any redistribution and use of the source code, including any resulting derivative
* works, are licensed by TI for use only with TI Devices.
*
* *       any redistribution and use of any object code compiled from the source code
* and any resulting derivative works, are licensed by TI for use only with TI Devices.
*
* Neither the name of Texas Instruments Incorporated nor the names of its suppliers
*
* may be used to endorse or promote products derived from this software without
* specific prior written permission.
*
* DISCLAIMER.
*
* THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
* OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
* OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
* IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
* BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
* OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
* OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
* OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/**
 *  \file     Cpsw_Stats.c
 *
 *  \brief    This file contains statistics related functionality.
 */

/* ========================================================================== */
/*                             Include Files                                  */
/* ========================================================================== */

#include <string.h>
#include <ti/csl/csl_cpswitch.h>
#include <cpsw/Cpsw_Types.h>
#include <cpsw/Cpsw_Soc.h>
#include <cpsw/Cpsw_Stats.h>

/* ========================================================================== */
/*                           Macros & Typedefs                                */
/* ========================================================================== */

/* None */

/* ========================================================================== */
/*                         Structures and Enums                               */
/* ========================================================================== */

/* None */

/* ========================================================================== */
/*                 Internal Function Declarations                             */
/* ========================================================================== */

/* None */

/* ========================================================================== */
/*                            Global Variables                                */
/* ========================================================================== */

/* None */

/* ========================================================================== */
/*                  Internal Function Definitions                             */
/* ========================================================================== */

/* ========================================================================== */
/*                          Function Definitions                              */
/* ========================================================================== */

sint32 CpswMcalStats_open(CpswStats *stats, Enet_Type enetType)
{
    CSL_CPSW_PORTSTAT statEnable;
    uint32 baseAddr = NULL;

    baseAddr = CpswSoc_getBaseAddr(enetType, 0U /*instId*/);
    stats->regs = (CSL_Xge_cpswRegs *)(baseAddr + CPSW_NU_OFFSET);

    /* Clear all statistics counters */
    CSL_CPSW_getStats(stats->regs, (CSL_CPSW_STATS*)stats->stats);
    memset(stats->stats, 0, sizeof(stats->stats));

    /* Enable statistics in all ports */
    memset(&statEnable, 0, sizeof(statEnable));
    statEnable.p0StatEnable = 1U;
    statEnable.p1StatEnable = 1U;
    CSL_CPSW_setPortStatsEnableReg(stats->regs, &statEnable);

    stats->isInit = TRUE;

    return CPSW_SOK;
}

void CpswMcalStats_close(CpswStats *stats)
{
    CSL_CPSW_PORTSTAT statEnable;

    if (TRUE == stats->isInit)
    {
        /* Disable statistics in all ports */
        memset(&statEnable, 0, sizeof(statEnable));
        CSL_CPSW_setPortStatsEnableReg(stats->regs, &statEnable);

        /* Clear all statistics counters */
        memset(stats->stats, 0, sizeof(stats->stats));

        stats->regs = NULL;
        stats->isInit = FALSE;
    }
}

void CpswStats_getStats(CpswStats *stats, CpswPort_Num num, CpswStats_Stats *val)
{
    CSL_CPSW_STATS st;
    uint32 *readStats = (uint32 *)&st;
    uint32 *accStats;
    uint32 block;
    uint32 i;

    if (CPSW_PORT_MAX > num)
    {
        block = (uint32)num;
        accStats = (uint32 *)&stats->stats[block];

        CSL_CPSW_getPortStats(stats->regs, block, &st);

        for (i = 0U; i < CPSW_STATS_BLOCK_ELEM_NUM; i++)
        {
            accStats[i] += readStats[i];
        }

        memcpy(val, &stats->stats[block], sizeof(stats->stats[block]));
    }
}
