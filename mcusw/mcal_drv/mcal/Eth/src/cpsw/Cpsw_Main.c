/*
*
* Copyright (c) 2019 Texas Instruments Incorporated
*
* All rights reserved not granted herein.
*
* Limited License.
*
* Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
* license under copyrights and patents it now or hereafter owns or controls to make,
* have made, use, import, offer to sell and sell ("Utilize") this software subject to the
* terms herein.  With respect to the foregoing patent license, such license is granted
* solely to the extent that any such patent is necessary to Utilize the software alone.
* The patent license shall not apply to any combinations which include this software,
* other than combinations with devices manufactured by or for TI ("TI Devices").
* No hardware patent is licensed hereunder.
*
* Redistributions must preserve existing copyright notices and reproduce this license
* (including the above copyright notice and the disclaimer and (if applicable) source
* code license limitations below) in the documentation and/or other materials provided
* with the distribution
*
* Redistribution and use in binary form, without modification, are permitted provided
* that the following conditions are met:
*
* *       No reverse engineering, decompilation, or disassembly of this software is
* permitted with respect to any software provided in binary form.
*
* *       any redistribution and use are licensed by TI for use only with TI Devices.
*
* *       Nothing shall obligate TI to provide you with source code for the software
* licensed and provided to you in object code.
*
* If software source code is provided to you, modification and redistribution of the
* source code are permitted provided that the following conditions are met:
*
* *       any redistribution and use of the source code, including any resulting derivative
* works, are licensed by TI for use only with TI Devices.
*
* *       any redistribution and use of any object code compiled from the source code
* and any resulting derivative works, are licensed by TI for use only with TI Devices.
*
* Neither the name of Texas Instruments Incorporated nor the names of its suppliers
*
* may be used to endorse or promote products derived from this software without
* specific prior written permission.
*
* DISCLAIMER.
*
* THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
* OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
* OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
* IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
* BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
* OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
* OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
* OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/**
 *  \file     Cpsw_Main.c
 *
 *  \brief    This file contains the main CPSW functions.
 */

/* ========================================================================== */
/*                             Include Files                                  */
/* ========================================================================== */

#include <ti/csl/csl_cpswitch.h>
#include <cpsw/Cpsw.h>
#include <cpsw/Cpsw_Soc.h>
#include <ti/drv/enet/enet.h>

/* ========================================================================== */
/*                           Macros & Typedefs                                */
/* ========================================================================== */

/* None */

/* ========================================================================== */
/*                         Structures and Enums                               */
/* ========================================================================== */

/* None */

/* ========================================================================== */
/*                 Internal Function Declarations                             */
/* ========================================================================== */

/* None */

/* ========================================================================== */
/*                            Global Variables                                */
/* ========================================================================== */

/* None */

/* ========================================================================== */
/*                  Internal Function Definitions                             */
/* ========================================================================== */

/* None */

/* ========================================================================== */
/*                          Function Definitions                              */
/* ========================================================================== */

sint32 CpswMcal_open(Cpsw *cpsw, CpswMcal_Config *cfg, Enet_Type enetType)
{
    CpswDmaCh *ch;
    uint32 i;
    sint32 retVal;

    /* Open UDMA */
    retVal = CpswDma_open(&cpsw->dma, &cfg->dmaCfg, cfg->udmaInstId);

    if (CPSW_SOK == retVal)
    {
        ch = CpswDma_getCh(&cpsw->dma, CPSW_DIR_TX);
        retVal = CpswDma_openCh(&cpsw->dma, ch, &cfg->dmaTxChCfg, enetType, cfg->virtualMacMode);
    }

    if (CPSW_SOK == retVal)
    {
        ch = CpswDma_getCh(&cpsw->dma, CPSW_DIR_RX);
        retVal = CpswDma_openCh(&cpsw->dma, ch, &cfg->dmaRxChCfg, enetType, cfg->virtualMacMode);
    }

    if (cfg->virtualMacMode == FALSE)
    {
        /* Open all ports */
        if (CPSW_SOK == retVal)
        {
            for (i = 0U; i < CPSW_PORT_MAX; i++)
            {
                retVal = CpswPort_open(&cpsw->port[i], (CpswPort_Num)i, &cfg->portCfg[i], enetType);
                if (CPSW_SOK != retVal)
                {
                    break;
                }
            }
        }

        /* Open stats */
        if (CPSW_SOK == retVal)
        {
            retVal = CpswMcalStats_open(&cpsw->stats, enetType);
        }

        /* Open ALE */
        if (CPSW_SOK == retVal)
        {
            retVal = CpswMcalAle_open(&cpsw->ale, &cfg->aleCfg, enetType);
        }

        /* Open MDIO */
        if (CPSW_SOK == retVal)
        {
            retVal = CpswMcalMdio_open(&cpsw->mdio, &cfg->mdioCfg, enetType);
        }

        /* Open all MACs */
        if (CPSW_SOK == retVal)
        {
            for (i = 0U; i < CPSW_MAC_MAX; i++)
            {
                retVal = CpswMcalMac_open(&cpsw->mac[i], (CpswMac_Num)i, &cfg->macCfg[i], enetType);
                if (CPSW_SOK != retVal)
                {
                    break;
                }
            }
        }
    }
    /* If needed, close all modules which were already initialized
     * before the error occurred */
    if (CPSW_SOK != retVal)
    {
        CpswMcal_close(cpsw, cfg->virtualMacMode);
    }

    return retVal;
}

void CpswMcal_close(Cpsw *cpsw, boolean virtualMacMode)
{
    CpswDmaCh *ch;
    uint32 i;

    if (virtualMacMode == FALSE)
    {
        /* Close all MACs */
        for (i = 0U; i < CPSW_MAC_MAX; i++)
        {
            CpswMcalMac_close(&cpsw->mac[i]);
        }

        /* Close MDIO */
        CpswMcalMdio_close(&cpsw->mdio);

        /* Close ALE */
        CpswMcalAle_close(&cpsw->ale);

        /* Close stats */
        CpswMcalStats_close(&cpsw->stats);

        /* Close all ports */
        for (i = 0U; i < CPSW_PORT_MAX; i++)
        {
            CpswPort_close(&cpsw->port[i]);
        }
    }
    /* Close TX and RX channels */
    ch = CpswDma_getCh(&cpsw->dma, CPSW_DIR_TX);
    CpswDma_closeCh(&cpsw->dma, ch);

    ch = CpswDma_getCh(&cpsw->dma, CPSW_DIR_RX);
    CpswDma_closeCh(&cpsw->dma, ch);

    /* Close UDMA */
    CpswDma_close(&cpsw->dma);
}

void Cpsw_getVersion(Cpsw *cpsw, CpswMcal_Version *version, Enet_Type enetType)
{
    CSL_Xge_cpswRegs *regs;
    CSL_CPSW_VERSION info;
    uint32 baseAddr = NULL;

    baseAddr = CpswSoc_getBaseAddr(enetType, 0U /*instId*/);
    regs = (CSL_Xge_cpswRegs *)(baseAddr + CPSW_NU_OFFSET);

    CSL_CPSW_getCpswVersionInfo(regs, &info);

    version->minor = info.minorVer;
    version->major = info.majorVer;
    version->rtl   = info.rtlVer;
    version->id    = info.id;
}
