/*
*
* Copyright (c) 2019 Texas Instruments Incorporated
*
* All rights reserved not granted herein.
*
* Limited License.
*
* Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
* license under copyrights and patents it now or hereafter owns or controls to make,
* have made, use, import, offer to sell and sell ("Utilize") this software subject to the
* terms herein.  With respect to the foregoing patent license, such license is granted
* solely to the extent that any such patent is necessary to Utilize the software alone.
* The patent license shall not apply to any combinations which include this software,
* other than combinations with devices manufactured by or for TI ("TI Devices").
* No hardware patent is licensed hereunder.
*
* Redistributions must preserve existing copyright notices and reproduce this license
* (including the above copyright notice and the disclaimer and (if applicable) source
* code license limitations below) in the documentation and/or other materials provided
* with the distribution
*
* Redistribution and use in binary form, without modification, are permitted provided
* that the following conditions are met:
*
* *       No reverse engineering, decompilation, or disassembly of this software is
* permitted with respect to any software provided in binary form.
*
* *       any redistribution and use are licensed by TI for use only with TI Devices.
*
* *       Nothing shall obligate TI to provide you with source code for the software
* licensed and provided to you in object code.
*
* If software source code is provided to you, modification and redistribution of the
* source code are permitted provided that the following conditions are met:
*
* *       any redistribution and use of the source code, including any resulting derivative
* works, are licensed by TI for use only with TI Devices.
*
* *       any redistribution and use of any object code compiled from the source code
* and any resulting derivative works, are licensed by TI for use only with TI Devices.
*
* Neither the name of Texas Instruments Incorporated nor the names of its suppliers
*
* may be used to endorse or promote products derived from this software without
* specific prior written permission.
*
* DISCLAIMER.
*
* THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
* OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
* OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
* IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
* BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
* OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
* OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
* OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/**
 *  \file     Cpsw_Dma.h
 *
 *  \brief    This file contains the structure definitions and function
 *            prototypes of the DMA module.
 */

/**
 *  \addtogroup CPSW_DMA_API
 *  @{
 */

#ifndef CPSWMCAL_DMA_H_
#define CPSWMCAL_DMA_H_

/* ========================================================================== */
/*                             Include Files                                  */
/* ========================================================================== */

#include <ti/drv/udma/udma.h>

#include "Cpsw_Types.h"
#include "Cpsw_Packet.h"

#ifdef __cplusplus
extern "C" {
#endif

/* ========================================================================== */
/*                                 Macros                                     */
/* ========================================================================== */

/**< \brief No interrupt required. Used in CpswDmaCh_Config */
#define CPSW_DMA_NO_INTR                (0xFFFFFFFFU)

/* ========================================================================== */
/*                         Structures and Enums                               */
/* ========================================================================== */

/**
 *  \brief Data direction
 *
 *  Data transfer direction.
 */
typedef enum
{
    CPSW_DIR_RX = 0U,
    /**< Reception direction */
    CPSW_DIR_TX = 1U
    /**< Transmission direction */
}
CpswDma_Dir;

/**
 *  \brief Cache write-back invalidate function
 *
 *  Pointer to a function that performs the cache write-back invalidate
 *  operation.  This function is to be called on TX buffers before they
 *  are given to the Ethernet controller hardware.
 */
typedef void (*CpswDma_CacheWbInv)(uint8 *buf,
                                   uint32 len);

/**
 *  \brief Cache write-back function
 *
 *  Pointer to a function that performs the cache write-back operation.
 *  This function is to be called on TX buffers before they are given to
 *  the Ethernet controller hardware.
 */
typedef void (*CpswDma_CacheWb)(uint8 *buf,
                                uint32 len);

/**
 *  \brief Cache invalidate function
 *
 *  Pointer to a function that performs the cache invalidate operation.
 *  This function is to be called on RX buffers after they have been
 *  retrieved from the Ethernet controller hardware.
 */
typedef void (*CpswDma_CacheInv)(uint8 *buf,
                                 uint32 len);

/**
 *  \brief Cache operations
 *
 *  Cache operations to be used internally by the DMA driver when operating on
 *  cached memories.
 */
typedef struct
{
    CpswDma_CacheWbInv cacheWbInv;
    /**< Cache write-back invalidate function */
    CpswDma_CacheWb cacheWb;
    /**< Cache write-back function */
    CpswDma_CacheInv cacheInv;
    /**< Cache invalidate function */
} CpswDma_CacheOps;

/**
 *  \brief Virtual MAC mode  configuration information
 *
 *  Configuration information when Ethernet Driver is operating in virtual MAC mode.
 */
typedef struct
{
    /*! Flow start index*/
    uint32 startIdx;
    /*! Flow index to be opened*/
    uint32 flowIdx;
} CpswDmaVirtualMac_Config;
/**
 *  \brief DMA channel configuration information
 *
 *  Configuration information for a DMA channel.
 */
typedef struct
{
    CpswPkt *pktMem;
    /**< Packet memory address */
    uint32 pktNum;
    /**< Number of packets in the packet memory */

    uint32 intrNum;
    /**< DMA completion interrupt. Use CPSW_DMA_NO_INTR for polling mode */

    /* Ring Acc memories */
    uint64_t *fqRingMem;
    /**< Ring Acc Free Queue (FQ) memory */
    uint64_t *cqRingMem;
    /**< Ring Acc Completion Queue (CQ) memory */
    uint64_t *tdCqRingMem;
    /**< Ring Acc Tear-down Completion Queue (TDCQ) memory */
    uint32 ringEntryNum;
    /**< Number of entries in the Ring Acc memories.  All memory areas must
     *   be of the same size */
    CpswDmaVirtualMac_Config virtualMacConfig;
    /*! Tx Channel Cpsw Peer Thread Id */
    uint32 txChPeerThreadId;
} CpswDmaCh_Config;


/*!
 * \brief Param struct for opening rx flow
 *
 *  The Param struct for the Rx Flow open function, containing CPSW instance
 *  Id for RX flow.
 */
typedef struct CpswDmaRxFlow_Config_s
{
    /*! UDMA driver handle */
    Udma_DrvHandle hUdmaDrv;
    /*! Flow start index */
    uint32 startIdx;
    /*! Flow index to be opened */
    uint32 flowIdx;
    /*! Ring bus order ID value to be programmed into the orderid field of
     *   the ring's RING_ORDERID register. */
    uint8 cqOrderId;
    /*! Ring bus order ID value to be programmed into the orderid field of
     *   the ring's RING_ORDERID register. */
    uint8 fqOrderId;
    /*! Number of receive packets, used for allocating number of DMA descriptors
     *  Note - The HW ring element count field is 19-bit */
    uint32 numRxPkts;
    /*! CQ Ring Mem Pointer  */
    void * cqRingMem;
    /*! FQ Ring Mem Pointer  */
    void * fqRingMem;
} CpswDmaRxFlow_Config;

/**
 *  \brief DMA Rx flow
 *
 *  This is an internal/private driver structure and should not be used
 *  or modified by the caller.
 */
typedef struct CpswDmaRxFlow_s
{
    CpswDmaRxFlow_Config rxFlowPrms;
    /**< Channel init params */

    Udma_DrvHandle hUdmaDrv;
    /**< UDMA driver handle */

    struct Udma_FlowObj flowUdmaObj;
    /**< UDMA Rx flow object */

    struct Udma_EventObj udmaEvtObj;
    /**< UDMA event object */

    struct Udma_RingObj fqRingObj;
    /**< Free queue ring handle */

    struct Udma_RingObj cqRingObj;
    /**< Complete queue ring handle */

    boolean evtInitFlag;
    /**< Event initialization state */

    boolean initFlag;
    /**< Channel initialization state */

} CpswDmaRxFlow;

/**
 *  \brief DMA channel
 *
 *  DMA channel.
 */
typedef struct
{
    struct Udma_ChObj udmaCh;
    /**< UDMA channel object */
    struct Udma_EventObj udmaEvt;
    /**< UDMA event object */
    struct Udma_ProxyObj udmaProxy;
    /**< Ring proxy object if using dedicated proxy for FQ ring */
    Udma_ProxyHandle fqRingProxy;
    /**< Free queue ring proxy handle */
    Udma_RingHandle fqRing;
    /**< Free queue ring handle */
    Udma_RingHandle cqRing;
    /**< Complete queue ring handle */
    CpswDmaCh_Config config;
    /**< Saved configuration information */
    CpswDma_Dir dir;
    /**< Channel data direction */
    boolean isInit;
    /**< Channel initialization state */
    boolean isIntrInit;
    /**< Channel interrupt initialization state */

    boolean    virtualMacMode;
     /**< Flag indicating if Ethernet MCAL driver is to operate in virtual mac
      *   mode with another core managing programming of CPSW IP.
      *   In virtual mac mode the ethernet MCAL driver will not perform CPSW
      *   RX DMA channel initialization or any CPSW IP register access
     */
    volatile uint64_t *intrStatusReg;
    /**< Interrupt Status register address */
    volatile uint64_t *intrClearReg;
    /**< Interrupt Clear register address */
    uint64_t intrMask;
    /**< Interrupt mask, used to clear or read the status of the interrupt */
    uint32 intrNum;
    /**< Interrupt number */
    CpswPktQ freeQ;
    /**< Queue that holds packets after channel initialization or after channel
     *   is disabled */
    struct CpswDmaRxFlow_s rxFlowObj;
} CpswDmaCh;

/**
 *  \brief DMA module configuration information
 *
 *  Configuration information for the DMA module.
 */
typedef struct
{
    CpswDma_CacheOps cacheOps;
    /**< Cache operations */
} CpswMcalDma_Config;

/**
 *  \brief DMA module
 *
 *  This is an internal/private driver structure and should not be used
 *  or modified by the caller.
 */
typedef struct
{
    struct Udma_DrvObj udmaDrv;
    /**< UDMA driver object */
    boolean isInit;
    /**< Module initialization state */
    CpswDmaCh rxCh;
    /**< Receive channel handle */
    CpswDmaCh txCh;
    /**< Transmit channel handle */
    CpswDma_CacheOps cacheOps;
    /**< Cache operations */
} CpswDma;

/* ========================================================================== */
/*                         Global Variables Declarations                      */
/* ========================================================================== */

/* None */

/* ========================================================================== */
/*                          Function Declarations                             */
/* ========================================================================== */

/**
 *  \brief CPSW DMA open function
 *
 *  Opens and initialize the CPSW DMA (UDMA).
 *
 *  This function must be called before any other CpswDma_*() function.
 *
 *  \param dma         CpswDma handle which is allocated by the caller
 *  \param config      DMA module configuration
 *  \param udmaInstId  Udma Instance Id
 *
 *  \return \ref Cpsw_ErrorCodes
 */
sint32 CpswDma_open(CpswDma *dma,
                     CpswMcalDma_Config *cfg,
                     uint32 udmaInstId);

/**
 *  \brief CPSW DMA close function
 *
 *  Closes and deinitializes the CPSW DMA (UDMA).
 *
 *  No CpswDma_*() functions must be called after this function.
 *
 *  \param dma      CpswDma handle
 */
void CpswDma_close(CpswDma *dma);

/**
 *  \brief Get DMA channel handle
 *
 *  Gets the DMA channel handle for a given data direction.
 *
 *  \param dma      CpswDma handle
 *  \param dir      Data direction
 *
 *  \return         CPSW Channel handle
 */
inline CpswDmaCh *CpswDma_getCh(CpswDma *dma,
                                CpswDma_Dir dir)
{
    return (CPSW_DIR_TX == dir) ? &dma->txCh : &dma->rxCh;
}

/**
 *  \brief DMA channel open
 *
 *  Opens the DMA channel and performs an initial configuration according
 *  to the config structure being passed.
 *
 *  The UDMA channel is open with the ring memories provided in 
 *  configuration information.  The UDMA event is configured and the DMA
 *  completion interrupt is setup, except for the actual interrupt
 *  registration.
 *
 *  A single internal packet queues (freeQ) is initialized. Packets allocated
 *  from the packet memory given in the configuration data are queued into
 *  the freeQ.  The default CQ ring is set in the underlying CPPI packet
 *  descriptors.
 *
 *  \param dma      CpswDma handle
 *  \param ch       CpswDmaCh handle
 *  \param config   Configuration information
 *  \param enetType CPSW Type enum
 *  \param virtualMacMode Virtual MAC mode
 *
 *  \return \ref Cpsw_ErrorCodes
 */
sint32 CpswDma_openCh(CpswDma *dma,
                       CpswDmaCh *ch,
                       CpswDmaCh_Config *config,
                       Enet_Type enetType,
                       boolean virtualMacMode);

/**
 *  \brief DMA channel close
 *
 *  Closes and deinitializes the DMA channel.
 *
 *  \param dma      CpswDma handle
 *  \param ch       CpswDmaCh handle
 *
 *  \return \ref Cpsw_ErrorCodes
 */
sint32 CpswDma_closeCh(CpswDma *dma,
                        CpswDmaCh *ch);

/**
 *  \brief DMA channel init state query
 *
 *  Queries the init state of the DMA channel.
 *
 *  \param dma      CpswDma handle
 *  \param ch       CpswDmaCh handle
 *
 *  \return         TRUE if the channel has been initialized,
 *                  FALSE otherwise
 */
boolean CpswDma_isInitCh(CpswDma *dma,
                      CpswDmaCh *ch);

/**
 *  \brief Enable DMA channel
 *
 *  Enables the DMA channel.  Data transfers can happen after this function
 *  returns.
 *
 *  \param dma      CpswDma handle
 *  \param ch       CpswDmaCh handle
 *
 *  \return \ref Cpsw_ErrorCodes
 */
sint32 CpswDma_enableCh(CpswDma *dma,
                         CpswDmaCh *ch);

/**
 *  \brief Disable DMA channel
 *
 *  Disables the DMA channel.  Data transfers will stop after this function
 *  returns through a synchronous tear-down call.
 *
 *  Packets sitting on the FQ ring will be dequeued and moved to the freeQ.
 *  Packets sitting on the CQ ring will be dequeued and appended to the
 *  freeQ.
 *
 *  At a later point, CpswDma_reclaimPktQ() can be used to retrieve packets
 *  owned by the driver but not yet retrieved by upper layer.
 *
 *  \param dma      CpswDma handle
 *  \param ch       CpswDmaCh handle
 *
 *  \return \ref Cpsw_ErrorCodes
 */
sint32 CpswDma_disableCh(CpswDma *dma,
                          CpswDmaCh *ch);

/**
 *  \brief Get the default flow number
 *
 *  Gets the default flow number of a receive channel.
 *
 *  \param dma      CpswDma handle
 *  \param ch       CpswDmaCh handle
 *
 *  \return         Channel number
 */
uint32 CpswDma_getDefaultFlowNum(CpswDma *dma,
                                   CpswDmaCh *ch);

/**
 *  \brief Clear DMA interrupt
 *
 *  Clear the DMA interurpt for the given channel.
 *
 *  \param dma      CpswDma handle
 *  \param ch       CpswDmaCh handle
 */
void CpswDma_clearIntr(CpswDma *dma,
                       CpswDmaCh *ch);

/**
 *  \brief Submit packets to CPSW
 *
 *  Submit a queue of packets to the CPSW.  This function is used to feed
 *  empty packets for future data reception as well as new packets for
 *  data transmission.
 *
 *  \param dma      CpswDma handle
 *  \param ch       CpswDmaCh handle
 *  \param queue    CpswPktQ handle with new packets
 *
 *  \return \ref Cpsw_ErrorCodes
 */
sint32 CpswDma_submitPktQ(CpswDma *dma,
                           CpswDmaCh *ch,
                           CpswPktQ *queue);

/**
 *  \brief Retrieve packets from CPSW
 *
 *  Retrieve a queue of packets fromthe CPSW.  This function is used to
 *  get filled packets after data reception as well as consumed packets
 *  from a previuos data transmission.
 *
 *  \param dma      CpswDma handle
 *  \param ch       CpswDmaCh handle
 *  \param queue    CpswPktQ handle to place the retrieve packets
 *
 *  \return \ref Cpsw_ErrorCodes
 */
sint32 CpswDma_retrievePktQ(CpswDma *dma,
                             CpswDmaCh *ch,
                             CpswPktQ *queue);

/**
 *  \brief Submit one packet to CPSW
 *
 *  Submit a single packet to the CPSW.  This function is used to feed an
 *  empty packet for future data reception as well as new packet for data
 *  transmission.
 *
 *  \param dma      CpswDma handle
 *  \param ch       CpswDmaCh handle
 *  \param pkt      CpswPkt pointer
 *
 *  \return \ref Cpsw_ErrorCodes
 */
sint32 CpswDma_submitPkt(CpswDma *dma,
                          CpswDmaCh *ch,
                          CpswPkt *pkt);

/**
 *  \brief Reclaim packets from channel's free queue
 *
 *  Reclaims packets from channel's free queue.  This function is used to
 *  retrieve packets after driver's initialization (see CpswDma_openCh()) or
 *  after channel disabled (see CpswDma_disableCh()).
 *
 *  After initialization, all allocated CPSW packets are placed into channel's
 *  internal free queue.  The upper layer must reclaim them, set packet's
 *  buffer and fill them for transmission or resubmit them for future
 *  reception.
 *
 *  After channel is disabled, the driver's free queue holds the packets that
 *  were not consumed by the hardware and those that were have not yet been
 *  retrieved via CpswDma_retrievePktQ().
 *
 *  \param dma      CpswDma handle
 *  \param ch       CpswDmaCh handle
 *  \param queue    CpswPktQ handle to hold reclaimed packets
 */
void CpswDma_reclaimPktQ(CpswDma *dma,
                         CpswDmaCh *ch,
                         CpswPktQ *queue);

/* ========================================================================== */
/*                        Deprecated Function Declarations                    */
/* ========================================================================== */

/* None */

#ifdef __cplusplus
}
#endif

#endif /* CPSWMCAL_DMA_H_ */

/* @} */
