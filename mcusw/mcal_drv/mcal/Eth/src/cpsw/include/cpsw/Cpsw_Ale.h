/*
*
* Copyright (c) 2019 Texas Instruments Incorporated
*
* All rights reserved not granted herein.
*
* Limited License.
*
* Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
* license under copyrights and patents it now or hereafter owns or controls to make,
* have made, use, import, offer to sell and sell ("Utilize") this software subject to the
* terms herein.  With respect to the foregoing patent license, such license is granted
* solely to the extent that any such patent is necessary to Utilize the software alone.
* The patent license shall not apply to any combinations which include this software,
* other than combinations with devices manufactured by or for TI ("TI Devices").
* No hardware patent is licensed hereunder.
*
* Redistributions must preserve existing copyright notices and reproduce this license
* (including the above copyright notice and the disclaimer and (if applicable) source
* code license limitations below) in the documentation and/or other materials provided
* with the distribution
*
* Redistribution and use in binary form, without modification, are permitted provided
* that the following conditions are met:
*
* *       No reverse engineering, decompilation, or disassembly of this software is
* permitted with respect to any software provided in binary form.
*
* *       any redistribution and use are licensed by TI for use only with TI Devices.
*
* *       Nothing shall obligate TI to provide you with source code for the software
* licensed and provided to you in object code.
*
* If software source code is provided to you, modification and redistribution of the
* source code are permitted provided that the following conditions are met:
*
* *       any redistribution and use of the source code, including any resulting derivative
* works, are licensed by TI for use only with TI Devices.
*
* *       any redistribution and use of any object code compiled from the source code
* and any resulting derivative works, are licensed by TI for use only with TI Devices.
*
* Neither the name of Texas Instruments Incorporated nor the names of its suppliers
*
* may be used to endorse or promote products derived from this software without
* specific prior written permission.
*
* DISCLAIMER.
*
* THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
* OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
* OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
* IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
* BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
* OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
* OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
* OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/**
 *  \file     Cpsw_Ale.h
 *
 *  \brief    This file contains the structure definitions and function
 *            prototypes of the Address Lookup Engine (ALE).
 */

/**
 *  \addtogroup CPSW_ALE_API
 *  @{
 */

#ifndef CPSWMCAL_ALE_H_
#define CPSWMCAL_ALE_H_

/* ========================================================================== */
/*                             Include Files                                  */
/* ========================================================================== */

#include <ti/csl/csl_cpswitch.h>
#include <ti/drv/enet/enet.h>
#include <ti/drv/enet/include/mod/cpsw_ale.h>
#include "Cpsw_Types.h"

#ifdef __cplusplus
extern "C" {
#endif

/* ========================================================================== */
/*                                 Macros                                     */
/* ========================================================================== */

/** \brief Macro to convert from a port number to a bitmask */
#define ALE_PORT_MASK_BIT(n)            (1U << (n))

/** \brief  ALE table size */
#define CPSW_ALE_TABLE_SIZE             (64U)

/**
 *  \anchor ALE_ConfigFlags
 *  \name ALE configuration flags
 *
 *  Configuration flags that can be passed to the ALE module when the module
 *  is being opened.
 *
 *  @{
 */

/** \brief ALE enable flag */
#define ALE_FLAG_ENABLE                 (0x0001U)

/** \brief ALE bypass mode flag */
#define ALE_FLAG_BYPASS                 (0x0002U)

/** \brief ALE VLAN aware mode flag */
#define ALE_FLAG_VLANAWARE              (0x0004U)

/* @} */

/* ========================================================================== */
/*                         Structures and Enums                               */
/* ========================================================================== */

/**
 *  \brief ALE Unicast entry type
 *
 *  Type of the ALE unicast entry.
 */
typedef enum
{
    CPSW_ALE_UCASTTYPE_NOAGE      = 0U,
    /**< Unicast address that is not ageable */
    CPSW_ALE_UCASTTYPE_AGENOTOUCH = 1U,
    /**< Ageable unicast address that has not been touched */
    CPSW_ALE_UCASTTYPE_OUI        = 2U,
    /**< OUI address */
    CPSW_ALE_UCASTTYPE_AGETOUCH   = 3U
    /**< Ageable unicast address that has been touched */
} CpswAle_UcastType;

/**
 *  \brief ALE port state
 *
 *  The state of an ALE port used for lookup operations.
 */
typedef enum
{
    CPSWMCAL_ALE_PORTSTATE_DISABLED = 0U,
    /**< Disabled state */
    CPSWMCAL_ALE_PORTSTATE_BLOCKED  = 1U,
    /**< Blocked state */
    CPSWMCAL_ALE_PORTSTATE_LEARN    = 2U,
    /**< Learning state */
    CPSWMCAL_ALE_PORTSTATE_FORWARD  = 3U
    /**< Forwarding state */
} CpswMcalAle_PortState;

/**
 *  \brief ALE port configuration
 *
 *  Configuration information for an ALE port.
 */
typedef struct
{
    CpswMcalAle_PortState state;
    /**< Port state used for lookup operations */
} CpswMcalAle_PortConfig;

/**
 *  \brief ALE module configuration
 *
 *  Configuration information for the ALE module.
 */
typedef struct
{
    uint32 flags;
    /**< ALE configuration flags \ref ALE_ConfigFlags */
    CpswMcalAle_PortConfig portCfg[CPSW_PORT_MAX];
    /**< Per-port configuration information */
} CpswMcalAle_Config;

/**
 *  \brief ALE module
 *
 *  This is an internal/private driver structure and should not be used
 *  or modified by the caller.
 */
typedef struct
{
    CSL_AleRegs *regs;
    /**< CPSW ALE register overlay */
    boolean enable;
    /**< Enable state */
    boolean bypass;
    /**< Bypass state */
    boolean isInit;
    /**< Module initialization state */
    CpswMcalAle_Config cfg;
    /**< Saved configuration information */
} CpswAle;

/* ========================================================================== */
/*                         Global Variables Declarations                      */
/* ========================================================================== */

/* None */

/* ========================================================================== */
/*                          Function Declarations                             */
/* ========================================================================== */

/**
 *  \brief ALE module open function
 *
 *  Opens the ALE module and performs an initial configuration according to
 *  the config structure being passed.  The ALE table is cleared after this
 *  function returns.
 *
 *  This function must be called before any other CpswAle_*() function.
 *
 *  \param ale      CpswAle handle which is allocated by the caller
 *  \param cfg      Configuration information
 *  \param enetType CPSW Type configuration
 *
 *  \return \ref Cpsw_ErrorCodes
 */
sint32 CpswMcalAle_open(CpswAle *ale,
                     CpswMcalAle_Config *cfg,
                     Enet_Type enetType);

/**
 *  \brief ALE module close function
 *
 *  Clears all ALE table entries, disables the ALE hardware and closes the
 *  ALE module.
 *
 *  No CpswAle_*() functions must be called after this function.
 *
 *  \param ale      CpswAle handle
 */
void CpswMcalAle_close(CpswAle *ale);

/**
 *  \brief Enable the ALE hardware
 *
 *  Enables the ALE hardware.
 *
 *  \param ale      CpswAle handle
 */
void CpswAle_enable(CpswAle *ale);

/**
 *  \brief Disable the ALE hardware
 *
 *  Disables the ALE hardware. No packet forwarding will occur afterwards.
 *
 *  \param ale      CpswAle handle
 */
void CpswAle_disable(CpswAle *ale);

/**
 *  \brief Enable or disable the bypass mode
 *
 *  Enables or disables the bypass mode.  In bypass mode, packet reception
 *  traffic is forwarded only to the host port, while packet transmission
 *  traffic is processed as in normal mode.
 *
 *  \param ale      CpswAle handle
 *  \param enable   TRUE: enable bypass, FALSE: disable bypass
 */
void CpswAle_setBypass(CpswAle *ale,
                       boolean enable);

/**
 *  \brief Clear the ALE table
 *
 *  Clear all entries in the ALE table.
 *
 *  \param ale      CpswAle handle which is allocated by the caller
 */
void CpswAle_clearTable(CpswAle *ale);

/**
 *  \brief Set the port state
 *
 *  Sets the state of the given port, which is used for lookup operations.
 *
 *  \param ale      CpswAle handle
 *  \param port     Port number
 *  \param state    Port state
 */
void CpswAle_setPortState(CpswAle *ale,
                          CpswPort_Num port,
                          CpswMcalAle_PortState state);

/**
 *  \brief Set ALE default thread
 *
 *  Sets the ALE default thread ID.
 *
 *  \param ale      CpswAle handle
 *  \param thread   Thread ID
 */
void CpswAle_setDefThread(CpswAle *ale, uint32 thread);

/**
 *  \brief Add a unicast address entry
 *
 *  Adds a unicast address entry to the ALE table.  Duplicated address
 *  entries are not permitted, so if an entry already exists for the given
 *  address, that entry is updated with the parameters of this function.
 *  Otherwise, a new entry is added.
 *
 *  \param ale      CpswAle handle
 *  \param port     Port number
 *  \param addr     Unicast address
 *  \param vlanId   VLAN ID
 *  \param blocked  Indicates if a packet with matching source or destination
 *                  address should be blocked or not
 *  \param secure   Indicates if a packet with matching source address should
 *                  be dropped if the port doesn't match
 *
 *  \return \ref Cpsw_ErrorCodes
 */
sint32 CpswAle_addUcastAddr(CpswAle *ale,
                             CpswPort_Num port,
                             const uint8 *addr,
                             uint32 vlanId,
                             uint32 blocked,
                             uint32 secure);

/**
 *  \brief Remove a unicast address entry
 *
 *  Removes a unicast address entry to the ALE table.
 *
 *  \param ale      CpswAle handle
 *  \param addr     Unicast address
 *  \param vlanId   VLAN ID
 *
 *  \return \ref Cpsw_ErrorCodes
 */
sint32 CpswAle_removeUcastAddr(CpswAle *ale,
                                const uint8 *addr,
                                uint32 vlanId);

/**
 *  \brief Add a multicast address entry
 *
 *  Adds a multicast address entry to the ALE table.  Duplicated address
 *  entries are not permitted, so if an entry already exists for the given
 *  address, the port mask of that entry is updated (logical 'or' operation)
 *  with the passed port mask.  Otherwise, a new entry is added.
 *
 *  \param ale      CpswAle handle
 *  \param addr     Multicast address
 *  \param vlanId   VLAN ID
 *  \param portMask Port mask
 *  \param super    Indicates if a packet with matching destination address
 *                  is a supervisory packet
 *  \param state    State of the received port(s) on a destination address
 *                  lookup for the packet to be forwarded to the transmit
 *                  ports
 *
 *  \return \ref Cpsw_ErrorCodes
 */
sint32 CpswAle_addMcastAddr(CpswAle *ale,
                             const uint8 *addr,
                             uint32 vlanId,
                             uint32 portMask,
                             uint32 super,
                             CpswMcalAle_PortState state);

/**
 *  \brief Remove a multicast address entry
 *
 *  Removes a multicast address entry to the ALE table.  The port mask of
 *  ALE entry is updated by removing the port mask being passed.
 *
 *  \param ale      CpswAle handle
 *  \param addr     Multicast address
 *  \param vlanId   VLAN ID
 *  \param portMask Port mask
 *
 *  \return \ref Cpsw_ErrorCodes
 */
sint32 CpswAle_removeMcastAddr(CpswAle *ale,
                                const uint8 *addr,
                                uint32 vlanId,
                                uint32 portMask);

/**
 *  \brief Remove an ALE entry
 *
 *  Removes an ALE entry with a matching address and VLAN ID.
 *
 *  \param ale      CpswAle handle
 *  \param addr     Multicast address
 *  \param vlanId   VLAN ID
 *
 *  \return \ref Cpsw_ErrorCodes
 */
sint32 CpswAle_removeAddr(CpswAle *ale,
                           const uint8 *addr,
                           uint32 vlanId);

/**
 *  \brief Set unknown VLAN member list
 *
 *  Sets the unknown VLAN member list mask.
 *
 *  \param ale      CpswAle handle
 *  \param portMask Port mask
 */
void CpswAle_setUnknownVlan(CpswAle *ale,
                            uint32 portMask);

/**
 *  \brief Set unknown VLAN multicast flood mask
 *
 *  Sets the unknown VLAN multicast flood mask.
 *
 *  \param ale      CpswAle handle
 *  \param portMask Port mask
 */
void CpswAle_setUnknownMcastFlood(CpswAle *ale,
                                  uint32 portMask);

/**
 *  \brief Set unknown VLAN registered multicast flood mask
 *
 *  Sets the unknown VLAN registered multicast flood mask.
 *
 *  \param ale      CpswAle handle
 *  \param portMask Port mask
 */
void CpswAle_setUnknownRegMcastFlood(CpswAle *ale,
                                     uint32 portMask);


/* ========================================================================== */
/*                        Deprecated Function Declarations                    */
/* ========================================================================== */

/* None */

#ifdef __cplusplus
}
#endif

#endif /* CPSWMCAL_ALE_H_ */

/* @} */
