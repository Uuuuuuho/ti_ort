/*
*
* Copyright (c) 2019 Texas Instruments Incorporated
*
* All rights reserved not granted herein.
*
* Limited License.
*
* Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
* license under copyrights and patents it now or hereafter owns or controls to make,
* have made, use, import, offer to sell and sell ("Utilize") this software subject to the
* terms herein.  With respect to the foregoing patent license, such license is granted
* solely to the extent that any such patent is necessary to Utilize the software alone.
* The patent license shall not apply to any combinations which include this software,
* other than combinations with devices manufactured by or for TI ("TI Devices").
* No hardware patent is licensed hereunder.
*
* Redistributions must preserve existing copyright notices and reproduce this license
* (including the above copyright notice and the disclaimer and (if applicable) source
* code license limitations below) in the documentation and/or other materials provided
* with the distribution
*
* Redistribution and use in binary form, without modification, are permitted provided
* that the following conditions are met:
*
* *       No reverse engineering, decompilation, or disassembly of this software is
* permitted with respect to any software provided in binary form.
*
* *       any redistribution and use are licensed by TI for use only with TI Devices.
*
* *       Nothing shall obligate TI to provide you with source code for the software
* licensed and provided to you in object code.
*
* If software source code is provided to you, modification and redistribution of the
* source code are permitted provided that the following conditions are met:
*
* *       any redistribution and use of the source code, including any resulting derivative
* works, are licensed by TI for use only with TI Devices.
*
* *       any redistribution and use of any object code compiled from the source code
* and any resulting derivative works, are licensed by TI for use only with TI Devices.
*
* Neither the name of Texas Instruments Incorporated nor the names of its suppliers
*
* may be used to endorse or promote products derived from this software without
* specific prior written permission.
*
* DISCLAIMER.
*
* THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
* OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
* OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
* IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
* BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
* OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
* OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
* OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/**
 *  \file     Cpsw_Soc.h
 *
 *  \brief    This file contains SOC specific functionality.
 */

/**
 *  \addtogroup CPSW_SOC_API
 *  @{
 */

#ifndef CPSWMCAL_SOC_H_
#define CPSWMCAL_SOC_H_

/* ========================================================================== */
/*                             Include Files                                  */
/* ========================================================================== */

#include <ti/csl/csl_cpswitch.h>
#include "Cpsw_Types.h"
#include <ti/drv/enet/enet.h>

#ifdef __cplusplus
extern "C" {
#endif

/* ========================================================================== */
/*                                 Macros                                     */
/* ========================================================================== */

/* ========================================================================== */
/*                         Structures and Enums                               */
/* ========================================================================== */


/* ========================================================================== */
/*                         Global Variables Declarations                      */
/* ========================================================================== */

/* None */

/* ========================================================================== */
/*                          Inline Function Declarations                             */
/* ========================================================================== */

/* None */

/* ========================================================================== */
/*                          Inline Function Declarations                             */
/* ========================================================================== */

static inline uint32 CpswSoc_isIpSupported(Enet_Type enetType,
                                             uint32 instId)
{
    boolean supported = FALSE;

    if ((enetType == ENET_CPSW_2G) && (0U == instId))
    {
        supported = TRUE;
    }
#if defined (SOC_J721E) || defined (SOC_J7200)
    else if ( ((enetType == ENET_CPSW_9G) && (0U == instId)) ||
         ((enetType == ENET_CPSW_5G) && (0U == instId)) )
    {
        supported = TRUE;
    }
#endif
    else
    {
        supported = FALSE;
    }

    return supported;
}

static inline uint32 CpswSoc_getRxChPeerId(Enet_Type enetType,
                                             uint32 instId)
{
    uint32 peerChNum;

    /* Get SoC array index */
    if ((enetType == ENET_CPSW_2G) && (0U == instId))
    {
        peerChNum = CSL_PSILCFG_NAVSS_MCU_CPSW0_PSILS_THREAD_OFFSET;
    }
#if defined (SOC_J721E)
    else if ( (enetType == ENET_CPSW_9G) && (0U == instId) )
    {
        peerChNum = CSL_PSILCFG_NAVSS_MAIN_CPSW9_PSILS_THREAD_OFFSET;
    }
#endif
#if defined (SOC_J7200)
    else if ( (enetType == ENET_CPSW_5G) && (0U == instId) )
    {
        peerChNum = CSL_PSILCFG_NAVSS_MAIN_CPSW5_PSILS_THREAD_OFFSET;
    }
#endif
    else
    {
        /* error */
        peerChNum = 0U;
    }

    return peerChNum;
}

static inline uint32 CpswSoc_getTxChPeerId(Enet_Type enetType,
                                             uint32 instId,
                                             uint32 chNum)
{
    uint32 peerChNum;

    /* Get SoC array index */
    if ((enetType == ENET_CPSW_2G) && (0U == instId))
    {
        peerChNum = CSL_PSILCFG_NAVSS_MCU_CPSW0_PSILD_THREAD_OFFSET;
    }
#if defined (SOC_J721E)
    else if ( (enetType == ENET_CPSW_9G) && (0U == instId) )
    {
        peerChNum = CSL_PSILCFG_NAVSS_MAIN_CPSW9_PSILD_THREAD_OFFSET;
    }
#endif
#if defined (SOC_J7200)
    else if ( (enetType == ENET_CPSW_5G) && (0U == instId) )
    {
        peerChNum = CSL_PSILCFG_NAVSS_MAIN_CPSW5_PSILD_THREAD_OFFSET;
    }
#endif
    else
    {
        /* error */
        peerChNum = 0U;
    }

    /* Get PSI-L destination thread offset for Tx channel */
    peerChNum = (peerChNum + chNum);

    return peerChNum;
}

static inline uint32 CpswSoc_getBaseAddr(Enet_Type enetType,
                                           uint32 instId)
{
    uint32 baseAddr = NULL;

    if (TRUE == CpswSoc_isIpSupported(enetType, 0U /* instId */))
    {
        /* Get SoC array index */
        if ((enetType == ENET_CPSW_2G) && (0U == instId))
        {
            baseAddr = (CSL_MCU_CPSW0_NUSS_BASE);
        }
#if defined (SOC_J721E) || defined (SOC_J7200)
    else if ( ((enetType == ENET_CPSW_9G) && (0U == instId)) ||
               ((enetType == ENET_CPSW_5G) && (0U == instId)) )
        {
            //FIXME - CPSW9G/CPSW5G registers should not be accessed by virt mac
            baseAddr = (CSL_CPSW0_NUSS_BASE);
        }
#endif
        else
        {
            EnetSoc_assert(FALSE, "Invalid Enet type %d\n", enetType);
        }
    }

    return baseAddr;
}


/* ========================================================================== */
/*                        Deprecated Function Declarations                    */
/* ========================================================================== */

/* None */

#ifdef __cplusplus
}
#endif

#endif /* CPSWMCAL_SOC_H_ */

/* @} */
