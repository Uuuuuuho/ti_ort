/*
*
* Copyright (c) 2019 Texas Instruments Incorporated
*
* All rights reserved not granted herein.
*
* Limited License.
*
* Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
* license under copyrights and patents it now or hereafter owns or controls to make,
* have made, use, import, offer to sell and sell ("Utilize") this software subject to the
* terms herein.  With respect to the foregoing patent license, such license is granted
* solely to the extent that any such patent is necessary to Utilize the software alone.
* The patent license shall not apply to any combinations which include this software,
* other than combinations with devices manufactured by or for TI ("TI Devices").
* No hardware patent is licensed hereunder.
*
* Redistributions must preserve existing copyright notices and reproduce this license
* (including the above copyright notice and the disclaimer and (if applicable) source
* code license limitations below) in the documentation and/or other materials provided
* with the distribution
*
* Redistribution and use in binary form, without modification, are permitted provided
* that the following conditions are met:
*
* *       No reverse engineering, decompilation, or disassembly of this software is
* permitted with respect to any software provided in binary form.
*
* *       any redistribution and use are licensed by TI for use only with TI Devices.
*
* *       Nothing shall obligate TI to provide you with source code for the software
* licensed and provided to you in object code.
*
* If software source code is provided to you, modification and redistribution of the
* source code are permitted provided that the following conditions are met:
*
* *       any redistribution and use of the source code, including any resulting derivative
* works, are licensed by TI for use only with TI Devices.
*
* *       any redistribution and use of any object code compiled from the source code
* and any resulting derivative works, are licensed by TI for use only with TI Devices.
*
* Neither the name of Texas Instruments Incorporated nor the names of its suppliers
*
* may be used to endorse or promote products derived from this software without
* specific prior written permission.
*
* DISCLAIMER.
*
* THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
* OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
* OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
* IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
* BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
* OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
* OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
* OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/**
 *  \file     Cpsw_Types.h
 *
 *  \brief    This file contains the generic definitions and structure
 *            definitions of the CPSW driver.
 */

/**
 *  \addtogroup CPSW_MAIN_API
 *  @{
 */

#ifndef CPSWMCAL_TYPES_H_
#define CPSWMCAL_TYPES_H_

/* ========================================================================== */
/*                             Include Files                                  */
/* ========================================================================== */

#include <ti/drv/udma/udma.h>
#include <Eth_Cfg.h>

#ifdef __cplusplus
extern "C" {
#endif

/* ========================================================================== */
/*                                 Macros                                     */
/* ========================================================================== */

/**
 *  \anchor Cpsw_ErrorCodes
 *  \name CPSW Error Codes
 *
 *  Error codes returned by the CPSW driver APIs
 *
 *  @{
 */
/* For simplicity, some CPSW error codes mirror the UDMA's */
/** \brief Success */
#define CPSW_SOK                        (UDMA_SOK)

/** \brief Generic failure error condition (typically caused by hardware) */
#define CPSW_EFAIL                      (UDMA_EFAIL)

/** \brief Bad arguments (i.e. NULL pointer) */
#define CPSW_EBADARGS                   (UDMA_EBADARGS)

/** \brief Invalid parameters (i.e. value out-of-range) */
#define CPSW_EINVALID_PARAMS            (UDMA_EINVALID_PARAMS)

/** \brief Time out while waiting for a given condition to happen */
#define CPSW_ETIMEOUT                   (UDMA_ETIMEOUT)

/** \brief Allocation failure */
#define CPSW_EALLOC                     (UDMA_EALLOC)

/** \brief Unexpected condition occurred (sometimes unrecoverable) */
#define CPSW_EUNEXPECTED                (UDMA_EALLOC - 1)

/** \brief The resource is currently busy performing an operation */
#define CPSW_EBUSY                      (UDMA_EALLOC - 2)

/* @} */

/** \brief CPSW memory alignment */
#define CPSW_MEM_ALIGNMENT              (UDMA_CACHELINE_ALIGNMENT)

/** \brief MAC address length (in bytes) */
#define CPSW_MAC_ADDR_LEN               (6U)

/**
 *  \brief CPSW Port number
 *
 *  CPSW Port number.
 */
typedef enum
{
    CPSW_PORT0     = 0U,
    /**< CPSW Port 0 (host port) */
    CPSW_PORT1     = 1U,
    /**< CPSW Port 1 (MAC port) */
    CPSW_PORT_MAX  = 2U
    /**< Max number of ports */
} CpswPort_Num;

/**
 *  \brief CPSW MAC port number
 *
 *  CPSW MAC port number.
 */
typedef enum
{
    CPSW_PORT1_MAC = 0U,
    /**< CPSW MAC port 0 */
    CPSW_MAC_MAX   = 1U
    /**< Max number of MAC ports */
} CpswMac_Num;

/* ========================================================================== */
/*                         Structures and Enums                               */
/* ========================================================================== */

/* None */

/* ========================================================================== */
/*                         Global Variables Declarations                      */
/* ========================================================================== */

/* None */

/* ========================================================================== */
/*                          Function Declarations                             */
/* ========================================================================== */

/* None */

/* ========================================================================== */
/*                        Deprecated Function Declarations                    */
/* ========================================================================== */

/* None */

#ifdef __cplusplus
}
#endif

#endif /* CPSWMCAL_TYPES_H_ */

/* @} */
