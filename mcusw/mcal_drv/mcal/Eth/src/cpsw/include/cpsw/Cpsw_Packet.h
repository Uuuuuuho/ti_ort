/*
*
* Copyright (c) 2019 Texas Instruments Incorporated
*
* All rights reserved not granted herein.
*
* Limited License.
*
* Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
* license under copyrights and patents it now or hereafter owns or controls to make,
* have made, use, import, offer to sell and sell ("Utilize") this software subject to the
* terms herein.  With respect to the foregoing patent license, such license is granted
* solely to the extent that any such patent is necessary to Utilize the software alone.
* The patent license shall not apply to any combinations which include this software,
* other than combinations with devices manufactured by or for TI ("TI Devices").
* No hardware patent is licensed hereunder.
*
* Redistributions must preserve existing copyright notices and reproduce this license
* (including the above copyright notice and the disclaimer and (if applicable) source
* code license limitations below) in the documentation and/or other materials provided
* with the distribution
*
* Redistribution and use in binary form, without modification, are permitted provided
* that the following conditions are met:
*
* *       No reverse engineering, decompilation, or disassembly of this software is
* permitted with respect to any software provided in binary form.
*
* *       any redistribution and use are licensed by TI for use only with TI Devices.
*
* *       Nothing shall obligate TI to provide you with source code for the software
* licensed and provided to you in object code.
*
* If software source code is provided to you, modification and redistribution of the
* source code are permitted provided that the following conditions are met:
*
* *       any redistribution and use of the source code, including any resulting derivative
* works, are licensed by TI for use only with TI Devices.
*
* *       any redistribution and use of any object code compiled from the source code
* and any resulting derivative works, are licensed by TI for use only with TI Devices.
*
* Neither the name of Texas Instruments Incorporated nor the names of its suppliers
*
* may be used to endorse or promote products derived from this software without
* specific prior written permission.
*
* DISCLAIMER.
*
* THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
* OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
* OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
* IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
* BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
* OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
* OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
* OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/**
 *  \file     Cpsw_Packet.h
 *
 *  \brief    This file contains the structure definitions and function
 *            prototypes of the CPSW packet and packet queue abstactions.
 */

/**
 *  \addtogroup CPSW_DMA_API
 *  @{
 */

#ifndef CPSWMCAL_PACKET_H_
#define CPSWMCAL_PACKET_H_

/* ========================================================================== */
/*                             Include Files                                  */
/* ========================================================================== */

#include <ti/drv/udma/udma.h>

#include "Cpsw_Types.h"

#ifdef __cplusplus
extern "C" {
#endif

/* ========================================================================== */
/*                                 Macros                                     */
/* ========================================================================== */

/** \brief CPPI descriptor size (in bytes) */
#define CPSW_DESC_SIZE                  (128U)

/* ========================================================================== */
/*                         Structures and Enums                               */
/* ========================================================================== */

/**
 *  \struct CpswPkt_s
 *
 *  CPSW packet which contains information about the underlying UDMA packet
 *  descriptor and can be used to form a queue of packets using CpswPktQ_*()
 *  functions.
 */
struct CpswPkt_s
{
    CSL_UdmapCppi5HMPD desc  __attribute__((aligned(UDMA_CACHELINE_ALIGNMENT)));
    /**< Host-mode packet descriptor (must be the first member of the structure */
    uint8 reserved[80U];
    /**< Reserved area. Desc size is 128 bytes (HMPD + reserved = 128) */
    struct CpswPkt_s *next;
    /**< Pointer to the next CPSW packet */
    void *priv;
    uint64_t phyAddr;
    /**< Physical address of the packet buffer */
};

/** CPSW Packet */
typedef struct CpswPkt_s CpswPkt;

/**
 *  \brief CPSW Packet Queue
 *
 *  Queue of CPSW packets.
 */
typedef struct
{
    uint32 count;
    /**< Number of packets in the queue */
    CpswPkt *head;
    /**< Pointer to the head of the queue */
    CpswPkt *tail;
    /**< Pointer to the tail of  the queue */
} CpswPktQ;

/* ========================================================================== */
/*                         Global Variables Declarations                      */
/* ========================================================================== */

/* None */

/* ========================================================================== */
/*                          Function Declarations                             */
/* ========================================================================== */

/* Packet related operations */

/**
 *  \brief Init CPSW packet
 *
 *  Initializes a CPSW packet.  The underlying CPPI packet descriptor is
 *  cleared as well.
 *
 *  \param pkt      CPSW packet handle
 */
void CpswPkt_init(CpswPkt *pkt);

/**
 *  \brief Init CPSW packet descriptor
 *
 *  Initializes the underlying CPPI packet descriptor.
 *
 *  \param pkt      CPSW packet handle
 */
void CpswPkt_initDesc(CpswPkt *pkt);

/**
 *  \brief Set buffer address and length
 *
 *  Sets the buffer address and length of the CPSW packet.
 *
 *  CPPI packet descriptor fields updated:
 *  - Packet length
 *  - Buffer address and buffer length
 *  - Original buffer address and original buffer length
 *
 *  \param pkt      CPSW packet handle
 *  \param buf      Buffer pointer
 *  \param len      Buffer length (in bytes)
 */
void CpswPkt_setBuf(CpswPkt *pkt,
                    void *buf,
                    uint32 len);

/**
 *  \brief Set completion queue ring number
 *
 *  Sets the completion queue ring number.
 *
 *  CPPI packet descriptor fields updated:
 *  - Packet return queue/ring number
 *
 *  \param pkt      CPSW packet handle
 *  \param ringNum  Ring number
 */
void CpswPkt_setCqRing(CpswPkt *pkt,
                       uint32 ringNum);

/**
 *  \brief Set private pointer
 *
 *  Sets the private pointer of the CPSW packet abstraction.  The private
 *  pointer is typically set by upper layers to identify a packet at a
 *  later point (i.e. after packet has been consumed by the hardware).
 *
 *  \param pkt      CPSW packet handle
 *  \param priv     Private data pointer
 */
void CpswPkt_setPriv(CpswPkt *pkt,
                     void *priv);

/**
 *  \brief Get private pointer
 *
 *  Gets the private pointer of the CPSW packet abstraction.  The private
 *  pointer is typically set by upper layers to identify a packet at a
 *  later point (i.e. after packet has been consumed by the hardware).
 *
 *  \param pkt      CPSW packet handle
 *  \return         Private data pointer
 */
void *CpswPkt_getPriv(CpswPkt *pkt);

/**
 *  \brief Get buffer length
 *
 *  Gets the buffer length of the CPSW packet which may have been updated
 *  by UDMA.  In packet reception cases, the length is updated by UDMA
 *  according to the received frame length.  This function can be used to
 *  query that received length, so it's typically called when packets are
 *  retrieved form the CpswDma driver.
 *
 *  \param pkt      CPSW packet handle
 *
 *  \return         Original packet length
 */
uint32 CpswPkt_getLen(CpswPkt *pkt);

/**
 *  \brief Check packet descriptor
 *
 *  Checks the following conditions in the packet descriptor:
 *  - Descriptor memory alignment
 *  - Descriptor type
 *  - Buffer address (not NULL)
 *  - Buffer length (not 0)
 *
 *  \param pkt      CPSW packet handle
 *
 *  \return         TRUE if the packet is correct, FALSE otherwise
 */
boolean CpswPkt_check(CpswPkt *pkt);

/* Packet Queue related operations */

/**
 *  \brief Init CPSW packet queue
 *
 *  Initializes a CPSW packet queue.
 *
 *  \param queue    CPSW packet queue handle
 */
void CpswPktQ_init(CpswPktQ *queue);


/**
 *  \brief Enqueue a packet into the head of CPSW packet queue
 *
 *  Enqueues a packet into the CPSW packet queue's head.
 *
 *  This is not a standard queue operation but it's very handy for restoring
 *  a packet into queue when an error condition has happened.
 *
 *  \param queue    CPSW packet queue handle
 *  \param pkt      CPSW packet handle
 */
void CpswPktQ_queueHead(CpswPktQ *queue,
                        CpswPkt *pkt);

/**
 *  \brief Enqueue a packet into the CPSW packet queue
 *
 *  Enqueues a packet into the CPSW packet queue.
 *
 *  \param queue    CPSW packet queue handle
 *  \param pkt      CPSW packet handle
 */
void CpswPktQ_queue(CpswPktQ *queue,
                    CpswPkt *pkt);

/**
 *  \brief Dequeue a packet from the CPSW packet queue
 *
 *  Dequeues a packet from the CPSW packet queue.  A null pointer is
 *  returned if the queue was already empty.
 *
 *  \param queue    CPSW packet queue handle
 *
 *  \return         CPSW packet handle or NULL if queue was empty
 */
CpswPkt *CpswPktQ_dequeue(CpswPktQ *queue);

/**
 *  \brief Peek into the CPSW packet queue
 *
 *  Shows the first packet in the packet but doesn't dequeue it.  A null
 *  pointer is returned if the queue is empty.
 *
 *  \param queue    CPSW packet queue handle
 *
 *  \return         CPSW packet handle or NULL if queue was empty
 */
CpswPkt *CpswPktQ_peek(CpswPktQ *queue);

/**
 *  \brief Copy CPSW packet queues
 *
 *  Copies packet queues.  The copied queue will have the same head/tail
 *  pointers and count as the source queue.
 *
 *  \param dst      Destination CPSW packet handle
 *  \param src      Source CPSW packet queue handle
 */
void CpswPktQ_copy(CpswPktQ *dst,
                   CpswPktQ *src);

/**
 *  \brief Append CPSW packet queue
 *
 *  Appends a packet queue into another queue.  The packets in the source
 *  queue are queued to the tail of the destination queue.
 *
 *  \param dst      Destination CPSW packet handle
 *  \param src      Source CPSW packet queue handle
 */
void CpswPktQ_append(CpswPktQ *dst,
                     CpswPktQ *src);

/**
 *  \brief Prepend CPSW packet queue
 *
 *  Prepends a packet queue into another queue.  The packets in the source
 *  queue are queued first, followed by the packets in the destination queue.
 *
 *  \param dst      Destination CPSW packet handle
 *  \param src      Source CPSW packet queue handle
 */
void CpswPktQ_prepend(CpswPktQ *dst,
                      CpswPktQ *src);

/**
 *  \brief Get first packet of the CPSW packet queue
 *
 *  Gets the first packet in the packet but doesn't dequeue it.  It is
 *  effectively the same operation as CpswPktQ_peek().
 *
 *  \param queue    CPSW packet queue handle
 *
 *  \return         CPSW packet handle or NULL if queue was empty
 */
CpswPkt *CpswPktQ_getFirstPkt(CpswPktQ *queue);

/**
 *  \brief Get last packet of the CPSW packet queue
 *
 *  Gets the last packet in the packet but doesn't dequeue it.
 *
 *  \param queue    CPSW packet queue handle
 *
 *  \return         CPSW packet handle or NULL if queue was empty
 */
CpswPkt *CpswPktQ_getLastPkt(CpswPktQ *queue);

/**
 *  \brief Get CPSW packet queue count
 *
 *  Gets the number of packets in the queue.
 *
 *  \param queue    CPSW packet queue handle
 *
 *  \return         Number of packets in the queue
 */
uint32 CpswPktQ_getCount(CpswPktQ *queue);

/**
 *  \brief Translates virtual to physical address
 *
 *  Translates virtual to physical address using default one-to-one mapping
 *  for all addresses except for BTCM.
 *
 *  This function implements UDMA's virtual to physical address translation
 *  callback type (Udma_VirtToPhyFxn).  Function parameters and their
 *  descriptions are as per UDMA function defintion.
 *
 *  \param virtAddr Virtual address
 *  \param chNum    Channel number passed during channel open
 *  \param appData  Callback pointer passed during channel open
 *
 *  \return         Corresponding physical address
 */
uint64_t CpswPacket_virtToPhys(const void *virtAddr,
                               uint32 chNum,
                               void *appData);

/**
 *  \brief Translates physical to vritual address
 *
 *  Translates physical to virtual address using default one-to-one mapping
 *  for all addresses except for BTCM.
 *
 *  This function implements UDMA's physical to virtual address translation
 *  callback type (Udma_PhyToVirtFxn).  Function parameters and their
 *  descriptions are as per UDMA function defintion.
 *
 *  \param phyAddr  Physical address
 *  \param chNum    Channel number passed during channel open
 *  \param appData  Callback pointer passed during channel open
 *
 *  \return         Corresponding virtual address
 */
void *CpswPacket_physToVirt(uint64_t phyAddr,
                            uint32 chNum,
                            void *appData);

/* ========================================================================== */
/*                        Deprecated Function Declarations                    */
/* ========================================================================== */

/* None */

#ifdef __cplusplus
}
#endif

#endif /* CPSWMCAL_PACKET_H_ */

/* @} */
