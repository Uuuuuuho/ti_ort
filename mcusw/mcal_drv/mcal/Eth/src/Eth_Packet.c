/*
*
* Copyright (c) 2019 Texas Instruments Incorporated
*
* All rights reserved not granted herein.
*
* Limited License.
*
* Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
* license under copyrights and patents it now or hereafter owns or controls to make,
* have made, use, import, offer to sell and sell ("Utilize") this software subject to the
* terms herein.  With respect to the foregoing patent license, such license is granted
* solely to the extent that any such patent is necessary to Utilize the software alone.
* The patent license shall not apply to any combinations which include this software,
* other than combinations with devices manufactured by or for TI ("TI Devices").
* No hardware patent is licensed hereunder.
*
* Redistributions must preserve existing copyright notices and reproduce this license
* (including the above copyright notice and the disclaimer and (if applicable) source
* code license limitations below) in the documentation and/or other materials provided
* with the distribution
*
* Redistribution and use in binary form, without modification, are permitted provided
* that the following conditions are met:
*
* *       No reverse engineering, decompilation, or disassembly of this software is
* permitted with respect to any software provided in binary form.
*
* *       any redistribution and use are licensed by TI for use only with TI Devices.
*
* *       Nothing shall obligate TI to provide you with source code for the software
* licensed and provided to you in object code.
*
* If software source code is provided to you, modification and redistribution of the
* source code are permitted provided that the following conditions are met:
*
* *       any redistribution and use of the source code, including any resulting derivative
* works, are licensed by TI for use only with TI Devices.
*
* *       any redistribution and use of any object code compiled from the source code
* and any resulting derivative works, are licensed by TI for use only with TI Devices.
*
* Neither the name of Texas Instruments Incorporated nor the names of its suppliers
*
* may be used to endorse or promote products derived from this software without
* specific prior written permission.
*
* DISCLAIMER.
*
* THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
* OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
* OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
* IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
* BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
* OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
* OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
* OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/**
 *  \file     Eth_Packet.c
 *
 *  \brief    This file contains Ethernet packet and packet queue abstractions
 *            related functionality.
 */

/* ========================================================================== */
/*                             Include Files                                  */
/* ========================================================================== */

#include "stddef.h"
#include "Eth_Packet.h"

/* ========================================================================== */
/*                           Macros & Typedefs                                */
/* ========================================================================== */

/* None */

/* ========================================================================== */
/*                         Structures and Enums                               */
/* ========================================================================== */

/* None */

/* ========================================================================== */
/*                 Internal Function Declarations                             */
/* ========================================================================== */

/* None */

/* ========================================================================== */
/*                            Global Variables                                */
/* ========================================================================== */

/* None */

/* ========================================================================== */
/*                  Internal Function Definitions                             */
/* ========================================================================== */

/* None */

/* ========================================================================== */
/*                          Function Definitions                              */
/* ========================================================================== */

void EthPkt_init(Eth_Pkt *pkt)
{
    pkt->next = NULL;
    pkt->buf = (void *)NULL;
    pkt->len = 0U;
    pkt->userLen = 0U;
    pkt->idx = 0U;
    pkt->state = ETH_BUF_STATE_FREE;
}

void EthPktQ_init(Eth_PktQ *queue)
{
    queue->head = NULL;
    queue->tail = NULL;
    queue->count = 0U;
}

void EthPktQ_queue(Eth_PktQ *queue, Eth_Pkt *pkt)
{
    /* Enqueue just one packet */
    pkt->next = NULL;

    if (NULL == queue->head)
    {
        queue->head = pkt;
    }
    else
    {
        queue->tail->next = pkt;
    }

    queue->tail = pkt;
    queue->count++;
}

Eth_Pkt *EthPktQ_dequeue(Eth_PktQ *queue)
{
    Eth_Pkt *pkt;

    pkt = queue->head;
    if (NULL != pkt)
    {
        queue->head = pkt->next;
        if (NULL == queue->head)
        {
            queue->tail = NULL;
        }

        queue->count--;
        pkt->next = NULL;
    }

    return pkt;
}

Eth_Pkt *EthPktQ_get(Eth_PktQ *queue, Eth_BufIdxType idx)
{
    Eth_Pkt *pkt;
    Eth_Pkt *prev = NULL;
    boolean found = FALSE;

    /* Find the packet with the matching index in the queue */
    pkt = queue->head;
    if (NULL != pkt)
    {
        while (NULL != pkt)
        {
            if (idx == pkt->idx)
            {
                found = TRUE;
                break;
            }
            prev = pkt;
            pkt = pkt->next;
        }
    }

    /* If found, take the packet out of the queue */
    if (TRUE == found)
    {
        if (queue->head == pkt)
        {
            queue->head = pkt->next;
            if (NULL == queue->head)
            {
                queue->tail = NULL;
            }
        }
        else if (queue->tail == pkt)
        {
            queue->tail = prev;
            prev->next = NULL;
        }
        else
        {
            prev->next = pkt->next;
        }

        queue->count--;
        pkt->next = NULL;
    }
    else
    {
        pkt = NULL;
    }

    return pkt;
}

Eth_Pkt *EthPktQ_peek(Eth_PktQ *queue)
{
    return queue->head;
}

void EthPktQ_copy(Eth_PktQ *dst, Eth_PktQ *src)
{
    dst->head = src->head;
    dst->tail = src->tail;
    dst->count = src->count;
}

void EthPktQ_append(Eth_PktQ *dst, Eth_PktQ *src)
{
    if (NULL == dst->head)
    {
        dst->head = src->head;
        dst->count = src->count;
    }
    else
    {
        dst->tail->next = src->head;
        dst->count += src->count;
    }

    dst->tail = src->tail;
}

Eth_Pkt *EthPktQ_getFirstPkt(Eth_PktQ *queue)
{
    return queue->head;
}

Eth_Pkt *EthPktQ_getLastPkt(Eth_PktQ *queue)
{
    return queue->tail;
}

uint32 EthPktQ_getCount(Eth_PktQ *queue)
{
    return queue->count;
}
