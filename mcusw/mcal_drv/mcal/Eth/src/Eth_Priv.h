/*
*
* Copyright (c) 2019 Texas Instruments Incorporated
*
* All rights reserved not granted herein.
*
* Limited License.
*
* Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
* license under copyrights and patents it now or hereafter owns or controls to make,
* have made, use, import, offer to sell and sell ("Utilize") this software subject to the
* terms herein.  With respect to the foregoing patent license, such license is granted
* solely to the extent that any such patent is necessary to Utilize the software alone.
* The patent license shall not apply to any combinations which include this software,
* other than combinations with devices manufactured by or for TI ("TI Devices").
* No hardware patent is licensed hereunder.
*
* Redistributions must preserve existing copyright notices and reproduce this license
* (including the above copyright notice and the disclaimer and (if applicable) source
* code license limitations below) in the documentation and/or other materials provided
* with the distribution
*
* Redistribution and use in binary form, without modification, are permitted provided
* that the following conditions are met:
*
* *       No reverse engineering, decompilation, or disassembly of this software is
* permitted with respect to any software provided in binary form.
*
* *       any redistribution and use are licensed by TI for use only with TI Devices.
*
* *       Nothing shall obligate TI to provide you with source code for the software
* licensed and provided to you in object code.
*
* If software source code is provided to you, modification and redistribution of the
* source code are permitted provided that the following conditions are met:
*
* *       any redistribution and use of the source code, including any resulting derivative
* works, are licensed by TI for use only with TI Devices.
*
* *       any redistribution and use of any object code compiled from the source code
* and any resulting derivative works, are licensed by TI for use only with TI Devices.
*
* Neither the name of Texas Instruments Incorporated nor the names of its suppliers
*
* may be used to endorse or promote products derived from this software without
* specific prior written permission.
*
* DISCLAIMER.
*
* THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
* OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
* OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
* IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
* BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
* OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
* OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
* OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/**
 *  \file     Eth_Priv.h
 *
 *  \brief    This file contains the common declarations and macros used across
 *            all the Ethernet driver files.
 */

#ifndef ETH_PRIV_H_
#define ETH_PRIV_H_

/* ========================================================================== */
/*                             Include Files                                  */
/* ========================================================================== */

#include <string.h>
#include <Std_Types.h>
#include <Eth.h>
#include <Eth_Packet.h>

#if (STD_ON == ETH_DEV_ERROR_DETECT)
#include <Det.h>
#endif

#include <cpsw/Cpsw.h>

#include "Eth_Rpc.h"

#ifdef __cplusplus
extern "C" {
#endif

/* ========================================================================== */
/*                                 Macros                                     */
/* ========================================================================== */

/** \brief Host to network byte order conversion for short integer */
#define htons(a)                        ((((a) & 0x00FFU) << 8U) | \
                                         (((a) & 0xFF00U) >> 8U))

/** \brief Host to network byte order conversion for long integer */
#define htonl(a)                        ((((a) & 0xFF000000U) >> 24U) | \
                                         (((a) & 0x00FF0000U) >>  8U) | \
                                         (((a) & 0x0000FF00U) <<  8U) | \
                                         (((a) & 0x000000FFU) << 24U))

/** \brief Network to host byte order conversion for short integer */
#define ntohl(a)                        htonl(a)

/** \brief Network to host byte order conversion for long integer */
#define ntohs(a)                        htons(a)

/** \brief Size of an array */
#define ARRAY_SIZE(x)                   (sizeof((x)) / sizeof(x[0U]))

/** \brief Macro to round 'x' up to the nearest 'y' */
#define ROUND_UP(x, y)                  ((((x) + ((y)- 1)) / (y)) * (y))

/** \brief Macro to align a buffer size to the cache line size */
#define ETH_ALIGN(x)                    (ROUND_UP((x), 128))

/* ========================================================================== */
/*                         Structures and Enums                               */
/* ========================================================================== */

/**
 *  \brief MII Access State
 *
 *  State of the current MII read/write operation.
 */
typedef struct
{
    boolean isActive;
    /**< MII operation is in progress */
    boolean isRead;
    /**< Read or write operation */
    uint8 ctrlIdx;
    /**< Ethernet controller index */
    uint8 trcvIdx;
    /**< Ethernet transceiver index (PHY address) */
    uint8 regIdx;
    /**< PHY register address */
} EthMiiAccess;

/**
 *  \brief Controller errors
 *
 *  Counters of different types of controller errors.
 */
typedef struct
{
    uint32 rxCRC;
    /**< RX packets with CRC errors */
    uint32 rxOverrun;
    /**< Receive overruns */
    uint32 rxUndersized;
    /**< RX packets that were undersized */
    uint32 rxOversized;
    /**< RX packets that were oversized */
    uint32 rxAlignCode;
    /**< RX packets with align/code errors */
    uint32 txSingleColl;
    /**< TX packets that experienced a single collision */
    uint32 txMultiColl;
    /**< TX packets that experienced multiple colisions */
    uint32 txLateColl;
    /**< TX packets that experienced a late collision */
} Eth_CtrlErrors;

typedef struct Eth_VirtualMacInfo_s
{
    Eth_RpcCommonRequestInfo cpswAttachInfo;
    /**< Info on ATTACH to ethernet firmware */
    uint32 hostPortRxMtu;
    /**< Max Rx packet size */
    uint32 txMtu[ETH_PRIORITY_NUM];
    /**< Max Tx packet size per priority */
    uint32 txPSILThreadId;
    /**< Tx Channel Cpsw Peer thread Id */
    uint32 rxFlowStartIdx;
    /**< Flow index base value  */
    uint32 rxFlowIdx;
    /**< Flow index offset for flow used  */
    uint8 macAddress[ETH_MAC_ADDR_LEN];
    /**< Destination Mac Address allocated for the flow */
    Eth_RpcDeviceData fwInfo;
    /**< Firmware info exported by ethernet firmware when it attaches to Eth Driver */
    boolean ethFwAttached;
    /**< Flag indicating firmware handshake completed */
    boolean detachResponsePending;
    /**< Flag indicating DETACH completion notification pending */
    uint32 responseTimeoutCnt;
    /**< timeout counter for resending annoucement if fw attached response
      * is not received from ethernet firmware
      */
} Eth_VirtualMacInfo;


typedef struct
{
    Eth_ConfigType ethConfig;
    /**< Saved Eth driver configuration information */

    Eth_ModeType ctrlMode;
    /**< Controller mode */

    boolean modeChanged;
    /**< Eth driver mode changed */

    boolean enableCacheOps;
    /**< Packet memory is cacheable */

    Eth_CacheWbInv cacheWbInv;
    /**< Cache write-back invalidate function */

    Eth_CacheWb cacheWb;
    /**< Cache write-back function */

    Eth_CacheInv cacheInv;
    /**< Cache invalidate function */

    boolean promiscuousMode;
    /**< Eth driver in promiscuous mode */

    uint8 macAddr[ETH_MAC_ADDR_LEN];
    /**< MAC address */

    EthMiiAccess miiAccess;
    /**< MII operation state */

    Eth_CtrlErrors errors;
    /**< Controller errors */

    CSL_mcu_ctrl_mmr_cfg0Regs *mmrRegs;
    /**< MCU CTRL MMR registers */

    Cpsw cpsw;
    /**< CPSW driver */

    /* TX queues */

    CpswPktQ txFreeDmaQ;
    /**< Queue of free CPSW packets */

    Eth_PktQ txFreeQ;
    /**< Queue of free TX Eth packets */

#if (STD_ON == ETH_ZERO_COPY_TX)
    Eth_PktQ txFreeQNoCpy;
    /**< Queue of free TX Eth packets used in no-copy transmission */
#endif

    Eth_PktQ txGrantedQ;
    /**< Queue of granted TX Eth packets */

    Eth_PktQ txConfQ;
    /**< Queue of TX Eth packets that need confirmation */

    /* RX queues */

    Eth_PktQ rxFreeQ;
    /**< Queue of free RX Eth packets */

    Eth_PktQ rxReadyQ;
    /**< Queue of ready RX Eth packets */

    Eth_VirtualMacInfo virtualMacInfo;
    /**< Virtual MAC config */
} EthDrv;

/* ========================================================================== */
/*                         Global Variables Declarations                      */
/* ========================================================================== */

extern VAR(EthDrv, ETH_VAR_CLEARED) gEthDrv;

extern volatile VAR(Eth_StateType, ETH_VAR_CLEARED) gEthDrvStatus;

/* ========================================================================== */
/*                          Function Declarations                             */
/* ========================================================================== */

/**
 *  \brief Initialize internal packet queues
 *
 *  Initializes the driver's internal queues of Ethernet packets
 *
 *  \param drv      Eth driver handle
 */
void Eth_initEthPktQs(EthDrv *drv);

/**
 *  \brief Open the CPSW driver
 *
 *  Opens the underlying CPSW driver.
 *
 *  \param drv      Eth driver handle
 *  \param cfg      Eth configuration information
 *
 *  \return         E_OK if successful, E_NOT_OK otherwise
 */
Std_ReturnType Eth_openCpsw(EthDrv *drv,
                            Eth_ConfigType *cfg);

/**
 *  \brief Closes the CPSW driver
 *
 *  Closes the underlying CPSW driver.
 *
 *  \param drv      Eth driver handle
 */
void Eth_closeCpsw(EthDrv *drv);

/**
 *  \brief Enable CPSW data transfers
 *
 *  Enables the CPSW for data transfer.  Empty CPSW packets are retrieved
 *  from the CPSW driver and filled with valid Eth driver buffers (reception
 *  packets are resubmitted to the driver for future frame reception).
 *  The DMA channels and the host port are enabled.  Frame transfers can
 *  occur at this point.
 *
 *  \param drv      Eth driver handle
 *
 *  \return         E_OK if successful, E_NOT_OK otherwise
 */
Std_ReturnType Eth_enableCpsw(EthDrv *drv);

/**
 *  \brief Disable CPSW data transfers
 *
 *  Disables the CPSW from further data transfers.  The host port and DMA
 *  channels are disabled, packets are reclaimed from the CPSW driver.
 *
 *  \param drv      Eth driver handle
 *  \param ctrlIdx  Controller index
 */
void Eth_disableCpsw(EthDrv *drv);

/**
 *  \brief Request a free TX packet
 *
 *  Requests a free TX packet from the driver's txFreeQ.  If the required
 *  packet length can't be met, this function updates the length argument with
 *  max length found.
 *
 *  If a packet is found, it's moved from the txFreeQ into the txGrantedQ and
 *  marked as being in-use.
 *
 *  \param drv      Eth driver handle
 *  \param ctrlIdx  Controller index
 *  \param pkt      Pointer to the granted packet
 *  \param len      Requested length. Updated with the nearest length if requested
 *                  length can't be met
 *
 *  \return         BUFREQ_OK if packet is granted
 *                  BUFREQ_E_BUSY if no packets are available
 *                  BUFREQ_E_OVFL if no packet with the request length was found
 */
BufReq_ReturnType Eth_requestTxPkt(EthDrv *drv,
                                   uint8 ctrlIdx,
                                   Eth_Pkt **pkt,
                                   uint16 *len);

/**
 *  \brief Find and get a granted TX packet
 *
 *  Find and gets a TX packet which was prevously granted by calling
 *  Eth_requestTxPacket().  The packet is found it a matching buffer index is
 *  found in the driver's txGrantedQ.
 *
 *  \param drv      Eth driver handle
 *  \param ctrlIdx  Controller index
 *  \param idx      Buffer index to be searched
 *  \param pkt      Pointer to the packet. Set to NULL if not found
 *
 *  \return         E_OK if successful, E_NOT_OK otherwise
 */
Std_ReturnType Eth_getGrantedTxPkt(EthDrv *drv,
                                   uint8 ctrlIdx,
                                   Eth_BufIdxType idx,
                                   Eth_Pkt **pkt);

/**
 *  \brief Release TX packets
 *
 *  Releases TX packets which were granted.  Those packets are moved from the
 *  driver's txGrantedQ to the txFreeQ.
 *
 *  \param drv      Eth driver handle
 *  \param ctrlIdx  Controller index
 */
void Eth_releaseTxPkts(EthDrv *drv,
                       uint8 ctrlIdx);

/**
 *  \brief Transmit a packet
 *
 *  Submits a packet for transmission.  A free CPSW packet is dequeued from
 *  the driver's txFreeDmaQ, filled with the Ethernet packet info and submitted
 *  to the CPSW driver.
 *
 *  If the packet has not requested a TX confirmation, it's freed immediately
 *  by placing it into the driver's txFreeQ.
 *
 *  This function is asynchronous, so the transfer completion is determined
 *  when the TX DMA completion interrupt occurs.
 *
 *  \param drv      Eth driver handle
 *  \param ctrlIdx  Controller index
 *  \param pkt      Packet to be sent
 *
 *  \return         E_OK if successful, E_NOT_OK otherwise
 */
Std_ReturnType Eth_transmitPkt(EthDrv *drv,
                               uint8 ctrlIdx,
                               Eth_Pkt *pkt);

/**
 *  \brief Transmit a queue of packets
 *
 *  Submits a queue of packets for transmission.  A free CPSW packet is dequeued
 *  from the driver's txFreeDmaQ, filled with the Ethernet packet info and
 *  submitted to the CPSW driver.
 *
 *  This function is asynchronous, so the transfer completion is determined
 *  when the TX DMA completion interrupt occurs.
 *
 *  \param drv      Eth driver handle
 *  \param ctrlIdx  Controller index
 *  \param pkt      Packet to be sent
 *
 *  \return         E_OK if successful, E_NOT_OK otherwise
 */
Std_ReturnType Eth_transmitPktQ(EthDrv *drv,
                                uint8 ctrlIdx,
                                Eth_PktQ *submitPktQ);

/**
 *  \brief Retrieve free transmission packets
 *
 *  Retrieves transmission packets which have already been consumed by the
 *  CPSW hardware and that are ready to be refilled with new data.
 *
 *  The retrieved packets are placed into the txConfQ if a confirmation
 *  was originally requested for a given buffer.
 *
 *  \param drv      Eth driver handle
 *  \param ctrlIdx  Controller index
 *
 *  \return         E_OK if successful, E_NOT_OK otherwise
 */
Std_ReturnType Eth_retrieveFreeTxPktQ(EthDrv *drv,
                                      uint8 ctrlIdx,
                                      Eth_PktQ *retrievePktQ);

/**
 *  \brief Receive packets
 *
 *  Retrieves packets that have been received by the controller.  A queue
 *  of CPSW packets is retrieved from the CPSW driver, and the corresponding
 *  Ethernet packets are placed into the driver's rxReadyQ.
 *
 *  The CPSW packets are refilled with free Ethernet packets from the driver's
 *  rxFreeQ, and the queue of CPSW packets in then resubmitted to the CPSW
 *  driver for future frame reception.
 *
 *  Confirmation of the received packets to the EthIf layer is not performed
 *  by this function.
 *
 *  \param drv      Eth driver handle
 *  \param ctrlIdx  Controller index
 *
 *  \return         E_OK if successful, E_NOT_OK otherwise
 */
Std_ReturnType Eth_receivePktQ(EthDrv *drv,
                               uint8 ctrlIdx,
                               Eth_PktQ *recvPktQ);

/**
 *  \brief Process a received packet
 *
 *  Processes a received packet from the driver's rxReadyQ.  The EtherType,
 *  VLAN, length and payload pointer are extracted from the Ethernet packet
 *  descriptor.  The destination MAC address is also checked to determine
 *  if it's a broadcast packet.
 *
 *  This function also calls EthIf_RxIndication() on the received packet,
 *  and the packet is ultimately moved to the driver's rxFreeQ.
 *
 *  Only one packet is processed by this function.  The return value is
 *  set accordingly if there are more packets still sitting on the driver's
 *  rxReadyQ
 *
 *  Confirmation of the received packets to the EthIf layer is not performed
 *  by this function.
 *
 *  \param drv      Eth driver handle
 *  \param ctrlIdx  Controller index
 *
 *  \return         ETH_RECEIVED if a frame has been received, no further
 *                  frames are available
 *                  ETH_NOT_RECEIVED if no frame has been received, no further
 *                  frames are available
 *                  ETH_RECEIVED_MORE_DATA_AVAILABLE if a frame has been
 *                  received, more frames are available
 */
Eth_RxStatusType Eth_processRxPkt(EthDrv *drv,
                                  uint8 ctrlIdx);

/**
 *  \brief Check controller access
 *
 *  Checks if the Ethernet controller hardware is accessible and calls appropriate
 *  Diagnostic Event Manager(DEM) events.
 *
 * Fail - When access to the Ethernet Controller fails extended production error with event
 *        status DEM_EVENT_STATUS_PREFAILED is returned to  DEM.
 * Pass - When access to the Ethernet Controller succeeds extended production error with event
 *        status DEM_EVENT_STATUS_PREPASSED is returned to  DEM.
 *
 *  \param drv      Eth driver handle
 *
 *  \return         E_OK if successful, E_NOT_OK otherwise
 */
Std_ReturnType Eth_checkControllerAccess(EthDrv *drv);

/**
 *  \brief Check controller errors
 *
 *  Checks for the following controller errors from the statistics
 *  reported by CPSW:
 *  - Receive packets with CRC errors
 *  - Receive packets with RX overrun
 *  - Receive frames lost
 *  - Receive undersized packets
 *  - Receive oversized packets
 *  - Receive packets with alignment errors
 *  - Transmit packets that experienced a single collision
 *  - Transmit packets that experienced multiple collisions
 *  - Transmit packets that experienced a late collision
 *
 *  \param drv      Eth driver handle
 */
void Eth_checkControllerErrors(EthDrv *drv);

/**
 *  \brief Get Ethernet RX statistics as per RFC2819
 *
 *  Get the Ethernet RX statistics as per RFC2819. Refer to the
 *  \ref Eth_RxStatsType.
 *
 *  \param drv      Eth driver handle
 *  \param RxStats  Rx Statistics structure pointer.
 */
void Eth_getRxStatsRFC2819(EthDrv *drv,
                           Eth_RxStatsType *RxStats);

/**
 *  \brief Get Ethernet TX statistics as per RFC1213
 *
 *  Get the Ethernet TX statistics as per RFC1213. Refer to the
 *  \ref Eth_TxStatsType.
 *
 *  \param drv      Eth driver handle
 *  \param TxStats  Tx Statistics structure pointer.
 */
void Eth_getTxStatsRFC1213(EthDrv *drv,
                           Eth_TxStatsType *TxStats);

/**
 *  \brief Get Ethernet TX Error statistics
 *
 *  Get the Ethernet TX Error statistics as per IETF RFC1213 and RFC1643. Refer to the
 *  \ref Eth_TxStatsType.
 *
 *  \param drv      Eth driver handle
 *  \param TxErrorCounterValues  Tx Error Statistics structure pointer.
 */
void Eth_getTxErrorCounterValues(EthDrv *drv,
                                 Eth_TxErrorCounterValuesType *TxErrorCounterValues);
/**
 *  \brief Get counter statistics
 *
 *  Get the frame count statistics.  The Count filled by this
 *  function includes hardware dependent counts. Refer to the
 *  \ref Eth_CounterType
 *
 *  \param drv      Eth driver handle
 *  \param CounterPtr Pointer to Eth_CounterType structure array pointer.
 */
void Eth_getCounterValues(EthDrv *drv,
                          Eth_CounterType* CounterPtr);

/**
 *  \brief Check if a MAC address is a broadcast address
 *
 *  Checks if a MAC address is a broadcast address (FF:FF:FF:FF:FF:FF).
 *
 *  \param addr     MAC address
 *
 *  \return         TRUE if it's a broadcast address, FALSE otherwise
 */
boolean Eth_isBcastMacAddr(const uint8 *addr);

/**
 *  \brief Check if a MAC address is a multicast address
 *
 *  Checks if a MAC address is a multicast address.
 *
 *  \param addr     MAC address
 *
 *  \return         TRUE if it's a multicast address, FALSE otherwise
 */
boolean Eth_isMcastMacAddr(const uint8 *addr);

/**
 *  \brief Check if a MAC address is a null address
 *
 *  Checks if a MAC address is a null address (00:00:00:00:00:00).
 *
 *  \param addr     MAC address
 *
 *  \return         TRUE if it's a null address, FALSE otherwise
 */
boolean Eth_isNullMacAddr(const uint8 *addr);

/**
 *  \brief Set the MAC address to a CPSW MAC port
 *
 *  Sets the MAC address to the CPSW MAC port corresponding to the
 *  controller index.
 *
 *  \param drv      Eth driver handle
 *  \param addr     MAC address
 *
 *  \return         E_OK if successful, E_NOT_OK otherwise
 */
Std_ReturnType Eth_setMacAddr(EthDrv *drv,
                              const uint8 *addr);

/**
 *  \brief Get the MAC address set to a CPSW MAC port
 *
 *  Gets the MAC address programmed to the CPSW MAC port corresponding
 *  to the controller index.
 *
 *  \param drv      Eth driver handle
 *  \param addr     MAC address
 *
 *  \return         E_OK if successful, E_NOT_OK otherwise
 */
Std_ReturnType Eth_getMacAddr(EthDrv *drv,
                              uint8 *addr);

/**
 *  \brief Enable promiscuous mode
 *
 *  Enable the promiscuous mode by clearing the ALE table and setting
 *  the ALE in bypass mode.  While in bypass mode, all packets received
 *  in all ports are exclusively forwarded to the host port.
 *
 *  \param drv      Eth driver handle
 *  \param ctrlIdx  Controller index
 *
 *  \return         E_OK if successful, E_NOT_OK otherwise
 */
Std_ReturnType Eth_setPromiscuousMode(EthDrv *drv);

/**
 *  \brief Disable promiscuous mode
 *
 *  Disable the promiscuous mode by setting ALE in normal, non-bypass
 *  mode and adding a unicast rule for the controller's MAC address
 *  and a multicast rule for the broadcast address.
 *
 *  \param drv      Eth driver handle
 *  \param ctrlIdx  Controller index
 *  \param addr     Controller's MAC address
 *
 *  \return         E_OK if successful, E_NOT_OK otherwise
 */
Std_ReturnType Eth_setNonPromiscuousMode(EthDrv *drv,
                                         uint8 *addr);

/**
 *  \brief Replace unicast address
 *
 *  Replaces the given unicast address with a new unicast address.
 *  The ALE entry for the old address is removed and a new entry
 *  is added with the new address.
 *
 *  \param drv      Eth driver handle
 *  \param oldAddr  Old unicast address
 *  \param newAddr  New unicast address
 *
 *  \return         E_OK if successful, E_NOT_OK otherwise
 */
Std_ReturnType Eth_replaceUcastAddr(EthDrv *drv,
                                    const uint8 *oldAddr,
                                    const uint8 *newAddr);

/**
 *  \brief Add a new MAC address to ALE
 *
 *  Adds a new ALE entry for the provided MAC address.  The added entry
 *  can be a unicast or multicast rule, depending on the type of MAC
 *  address being passed.
 *
 *  \param drv      Eth driver handle
 *  \param addr     MAC address
 *
 *  \return         E_OK if successful, E_NOT_OK otherwise
 */
Std_ReturnType Eth_addFilterAddr(EthDrv *drv,
                                 const uint8 *addr);

/**
 *  \brief Delete a MAC address from the ALE
 *
 *  Deletes a ALE entry if the provided MAC address is a unicast address.
 *  Otherwise, the port corresponding to the controller index is removed
 *  from the multicast entry.
 *
 *  \param drv      Eth driver handle
 *  \param addr     MAC address
 *
 *  \return         E_OK if successful, E_NOT_OK otherwise
 */
Std_ReturnType Eth_delFilterAddr(EthDrv *drv,
                                 const uint8 *addr);

/**
 *  \brief Trigger a PHY register read
 *
 *  Initiates the PHY register read operation.  The operation is completed
 *  when the MDIO interrupt occurs.
 *
 *  The MDIO interrupt handler is responsible for passing the read value to
 *  to the EthTrcv driver through the EthTrcv_ReadMiiIndication() call.
 *
 *  \param drv      Eth driver handle
 *  \param ctrlIdx  Controller index
 *  \param trcvIdx  Transceiver index (PHY address)
 *  \param regIdx   Register address
 *
 *  \return         E_OK if successful, E_NOT_OK otherwise
 */
Std_ReturnType Eth_triggerMiiRead(EthDrv *drv,
                                  uint8 ctrlIdx,
                                  uint8 trcvIdx,
                                  uint8 regIdx);

/**
 *  \brief Trigger a PHY register write
 *
 *  Initiates the PHY register write operation.  The operation is completed
 *  when the MDIO interrupt occurs.
 *
 *  The MDIO interrupt handler is responsible for indicating the completion
 *  of the register write to the EthTrcv driver through the
 *  EthTrcv_WriteMiiIndication() call.
 *
 *  \param drv      Eth driver handle
 *  \param ctrlIdx  Controller index
 *  \param trcvIdx  Transceiver index (PHY address)
 *  \param regIdx   Register address
 *  \param regVal   Register value to be written
 *
 *  \return         E_OK if successful, E_NOT_OK otherwise
 */
Std_ReturnType Eth_triggerMiiWrite(EthDrv *drv,
                                   uint8 ctrlIdx,
                                   uint8 trcvIdx,
                                   uint8 regIdx,
                                   uint16 regVal);

/**
 *  \brief Get the value read from a PHY register
 *
 *  Gets the value read from a PHY register after the MDIO interrupt has
 *  occurred indicating the completion of the read operation.
 *
 *  \param drv      Eth driver handle
 *  \param ctrlIdx  Controller index
 *  \param regVal   Register value read
 *
 *  \return         E_OK if successful, E_NOT_OK otherwise
 */
Std_ReturnType Eth_getMiiReadVal(EthDrv *drv,
                                 uint8 ctrlIdx,
                                 uint16 *regVal);

/**
 *  \brief Clear MDIO interrupt
 *
 *  Clears the MDIO interrupt.
 *
 *  \param drv      Eth driver handle
 *  \param ctrlIdx  Controller index
 *
 *  \return         E_OK if successful, E_NOT_OK otherwise
 */
void Eth_clearMdioInt(EthDrv *drv,
                      uint8 ctrlIdx);

/**
 *  \brief Read the PHY MAC address
 *
 *  Gets the PHY MAC address from the MMR registers MAC_ID0 and MAC_ID1.
 *
 *  \param drv      Eth driver handle
 *  \param addr     MAC address pointer
 */
void Eth_readMacAddr(EthDrv *drv,
                     uint8 *addr);

/**
 *  \brief Report DET error
 *
 *  Reports an Ethernet module error to the Development Error Tracer (DET).
 *
 *  \param apiId    API Id \ref Eth_ServiceIds
 *  \param errorId  Error Id \ref Eth_ErrorCodes
 */
#if (STD_ON == ETH_DEV_ERROR_DETECT)
LOCAL_INLINE void Eth_reportDetError(uint8 apiId, uint8 errorId)
{
    (void) Det_ReportError(ETH_MODULE_ID, ETH_INSTANCE_ID, apiId, errorId);
}
#endif

/**
 *  \brief Cache write-back invalidate
 *
 *  Performs a cache write-back invalidate operation using driver's registered
 *  write-back invalidate function.
 *
 *  \param drv      Eth driver handle
 *  \param buf      Buffer address
 *  \param len      Length (in bytes)
 */
LOCAL_INLINE void Eth_wbInvCache(EthDrv *drv, uint8 *buf, uint16 len)
{
    drv->cacheWbInv(buf, len);
}

/**
 *  \brief Cache write-back
 *
 *  Performs a cache write-back operation using driver's registered write-back
 *  function.
 *
 *  \param drv      Eth driver handle
 *  \param buf      Buffer address
 *  \param len      Length (in bytes)
 */
LOCAL_INLINE void Eth_wbCache(EthDrv *drv, uint8 *buf, uint16 len)
{
    drv->cacheWb(buf, len);
}

/**
 *  \brief Cache invalidate
 *
 *  Performs a cache invalidate operation using driver's registered invalidate
 *  function.
 *
 *  \param drv      Eth driver handle
 *  \param buf      Buffer address
 *  \param len      Length (in bytes)
 */
LOCAL_INLINE void Eth_invCache(EthDrv *drv, uint8 *buf, uint16 len)
{
    drv->cacheInv(buf, len);
}

/* ========================================================================== */
/*                        Deprecated Function Declarations                    */
/* ========================================================================== */

/* None */

#ifdef __cplusplus
}
#endif

#endif /* ETH_PRIV_H_ */
