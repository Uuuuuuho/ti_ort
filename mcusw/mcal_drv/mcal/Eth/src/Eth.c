/*
*
* Copyright (c) 2019 Texas Instruments Incorporated
*
* All rights reserved not granted herein.
*
* Limited License.
*
* Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
* license under copyrights and patents it now or hereafter owns or controls to make,
* have made, use, import, offer to sell and sell ("Utilize") this software subject to the
* terms herein.  With respect to the foregoing patent license, such license is granted
* solely to the extent that any such patent is necessary to Utilize the software alone.
* The patent license shall not apply to any combinations which include this software,
* other than combinations with devices manufactured by or for TI ("TI Devices").
* No hardware patent is licensed hereunder.
*
* Redistributions must preserve existing copyright notices and reproduce this license
* (including the above copyright notice and the disclaimer and (if applicable) source
* code license limitations below) in the documentation and/or other materials provided
* with the distribution
*
* Redistribution and use in binary form, without modification, are permitted provided
* that the following conditions are met:
*
* *       No reverse engineering, decompilation, or disassembly of this software is
* permitted with respect to any software provided in binary form.
*
* *       any redistribution and use are licensed by TI for use only with TI Devices.
*
* *       Nothing shall obligate TI to provide you with source code for the software
* licensed and provided to you in object code.
*
* If software source code is provided to you, modification and redistribution of the
* source code are permitted provided that the following conditions are met:
*
* *       any redistribution and use of the source code, including any resulting derivative
* works, are licensed by TI for use only with TI Devices.
*
* *       any redistribution and use of any object code compiled from the source code
* and any resulting derivative works, are licensed by TI for use only with TI Devices.
*
* Neither the name of Texas Instruments Incorporated nor the names of its suppliers
*
* may be used to endorse or promote products derived from this software without
* specific prior written permission.
*
* DISCLAIMER.
*
* THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
* OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
* OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
* IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
* BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
* OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
* OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
* OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/**
 *  \file     Eth.c
 *
 *  \brief    This file contains the implementation of the main APIs
 *            of the Ethernet driver.
 */

/* ========================================================================== */
/*                             Include Files                                  */
/* ========================================================================== */

#include <string.h>
#include <Dem.h>
#if (STD_ON == ETH_DEV_ERROR_DETECT)
#include <Det.h>
#endif
#include <Os.h>
#include <Std_Types.h>
#include <Eth_Cfg.h>
#include <Eth.h>
#include <EthIf_Cbk.h>
#include <SchM_Eth.h>
#if (STD_ON == ETH_VIRTUALMAC_SUPPORT)
#include <Cdd_Ipc.h>
#include "Eth_Rpc.h"
#include "Eth_RpcPriv.h"
#endif

#include "Eth_Priv.h"


/* ========================================================================== */
/*                           Macros & Typedefs                                */
/* ========================================================================== */

/* Consistency check of the Ethernet Driver release version with header file */
/*
 * Design:       ETH_DesignId_001
 * Requirements: MCAL-1523
 */
#if ((ETH_AR_RELEASE_MAJOR_VERSION != (4U)) || \
     (ETH_AR_RELEASE_MINOR_VERSION != (3U)) || \
     (ETH_AR_RELEASE_REVISION_VERSION != (1U)))
#error "Eth: AUTOSAR Version Numbers of Eth are different"
#endif

/* Consistency check of the Ethernet Driver software version with header file */
/*
 * Design:       ETH_DesignId_001
 * Requirements: MCAL-1523
 */
#if ((ETH_SW_MAJOR_VERSION != (1U)) || \
     (ETH_SW_MINOR_VERSION != (3U)) || \
     (ETH_SW_PATCH_VERSION != (2U)))
#error "Eth: Software Version Numbers are inconsistent"
#endif

/* ========================================================================== */
/*                         Structures and Enums                               */
/* ========================================================================== */

/* None */

/* ========================================================================== */
/*                 Internal Function Declarations                             */
/* ========================================================================== */

static FUNC(Std_ReturnType, ETH_CODE)
Eth_ControllerInit(uint8 ctrlIdx);

static FUNC(Std_ReturnType, ETH_CODE)
Eth_checkSetControllerModeErrors(uint8 ctrlIdx,
                                 Eth_ModeType ctrlMode);

static FUNC(Std_ReturnType, ETH_CODE)
Eth_checkGetControllerModeErrors(uint8 ctrlIdx,
                                 Eth_ModeType *ctrlModePtr);

#if (STD_OFF == ETH_USE_Q_APIS)
static FUNC(Std_ReturnType, ETH_CODE)
Eth_checkTransmitErrors(uint8 ctrlIdx,
                        Eth_BufIdxType bufIdx,
                        uint8 *physAddrPtr);
#endif /* (STD_OFF == ETH_USE_Q_APIS) */

#if (STD_ON == ETH_GET_COUNTER_VALUES_API)
static FUNC(Std_ReturnType, ETH_CODE)
Eth_checkGetCounterValuesErrors(uint8 ctrlIdx,
                                Eth_CounterType* CounterPtr);
#endif /* (STD_ON == ETH_GET_COUNTER_VALUES_API) */

#if (STD_ON == ETH_GET_RX_STATS_API)
static FUNC(Std_ReturnType, ETH_CODE)
Eth_checkGetRxStatsErrors(uint8 ctrlIdx,
                          Eth_RxStatsType *RxStats);
#endif /* (STD_ON == ETH_GET_RX_STATS_API) */

#if (STD_ON == ETH_GET_TX_STATS_API)
static FUNC(Std_ReturnType, ETH_CODE)
Eth_checkGetTxStatsErrors(uint8 ctrlIdx,
                          Eth_TxStatsType *TxStats);
#endif /* (STD_ON == ETH_GET_TX_STATS_API) */

#if (STD_ON == ETH_GET_TX_ERROR_COUNTERSVALUES_API)
static FUNC(Std_ReturnType, ETH_CODE)
Eth_checkGetTxErrorCounterValueErrors(uint8 ctrlIdx,
                         Eth_TxErrorCounterValuesType *TxErrorCounterValues);
#endif /* (STD_ON == ETH_GET_TX_ERROR_COUNTERSVALUES_API) */

#if (STD_ON == ETH_DEV_ERROR_DETECT)

LOCAL_INLINE void Eth_reportDetError(uint8 apiId, uint8 errorId);

static FUNC(Std_ReturnType, ETH_CODE)
Eth_checkGetPhysAddrErrors(uint8 ctrlIdx,
                           uint8 *physAddrPtr);

static FUNC(Std_ReturnType, ETH_CODE)
Eth_checkSetPhysAddrErrors(uint8 ctrlIdx,
                           const uint8 *physAddrPtr);

#if (STD_ON == ETH_UPDATE_PHYS_ADDR_FILTER_API)
static FUNC(Std_ReturnType, ETH_CODE)
Eth_checkUpdatePhysAddrFilterErrors(uint8 ctrlIdx,
                                    uint8 *physAddrPtr);
#endif /* (STD_ON == ETH_UPDATE_PHYS_ADDR_FILTER_API) */

#if (STD_ON == ETH_ENABLE_MII_API)
static FUNC(Std_ReturnType, ETH_CODE)
Eth_checkWriteMiiErrors(uint8 ctrlIdx,
                        uint8 trcvIdx,
                        uint8 regIdx,
                        uint16 regVal);
#endif /* (STD_ON == ETH_ENABLE_MII_API) */

#if (STD_ON == ETH_ENABLE_MII_API)
static FUNC(Std_ReturnType, ETH_CODE)
Eth_checkReadMiiErrors(uint8 ctrlIdx,
                       uint8 trcvIdx,
                       uint8 regIdx,
                       uint16 *regValPtr);
#endif /* (STD_ON == ETH_ENABLE_MII_API) */

#if (STD_OFF == ETH_USE_Q_APIS)
static FUNC(BufReq_ReturnType, ETH_CODE)
Eth_checkProvideTxBufferErrors(uint8 ctrlIdx,
                               Eth_BufIdxType *bufIdxPtr,
                               uint8 **bufPtr,
                               uint16 *lenBytePtr);
#endif /* (STD_ON == ETH_USE_Q_APIS) */

#if (STD_OFF == ETH_USE_Q_APIS)
static FUNC(Std_ReturnType, ETH_CODE)
Eth_checkReceiveErrors(uint8 ctrlIdx,
                       Eth_RxStatusType *rxStatusPtr);
#endif /* (STD_OFF == ETH_USE_Q_APIS) */

static FUNC(Std_ReturnType, ETH_CODE)
Eth_checkTxConfirmationErrors(uint8 ctrlIdx);

#if (STD_ON == ETH_GLOBALTIMESUPPORT_API)
static FUNC(Std_ReturnType, ETH_CODE)
Eth_checkGetCurrentTimeErrors(uint8 ctrlIdx,
                              Eth_TimeStampQualType *timeQualPtr,
                              Eth_TimeStampType *timeStampPtr);
#endif /* (STD_ON == ETH_GLOBALTIMESUPPORT_API) */

#if (STD_ON == ETH_GLOBALTIMESUPPORT_API)
static FUNC(Std_ReturnType, ETH_CODE)
Eth_checkEnableEgressTimeStampErrors(uint8 CtrlIdx,
                                     uint8 BufIdx);
#endif /* (STD_ON == ETH_GLOBALTIMESUPPORT_API) */

#if (STD_ON == ETH_GLOBALTIMESUPPORT_API)
static FUNC(Std_ReturnType, ETH_CODE)
Eth_checkGetEgressTimeStampErrors(uint8 ctrlIdx,
                                  Eth_TimeStampQualType *timeQualPtr,
                                  Eth_TimeStampType *timeStampPtr);
#endif /* (STD_ON == ETH_GLOBALTIMESUPPORT_API) */

#if (STD_ON == ETH_GLOBALTIMESUPPORT_API)
static FUNC(Std_ReturnType, ETH_CODE)
Eth_checkGetIngressTimeStampErrors(uint8 ctrlIdx,
                                   Eth_DataType *dataPtr,
                                   Eth_TimeStampQualType *timeQualPtr,
                                   Eth_TimeStampType *timeStampPtr);
#endif /* (STD_ON == ETH_GLOBALTIMESUPPORT_API) */

#if (STD_ON == ETH_GLOBALTIMESUPPORT_API)
static FUNC(Std_ReturnType, ETH_CODE)
Eth_checkSetCorrectionTimeErrors(uint8 ctrlIdx,
                                 Eth_TimeIntDiffType *timeOffsetPtr,
                                 Eth_RateRatioType *rateRatioPtr);
#endif /* (STD_ON == ETH_GLOBALTIMESUPPORT_API) */

#if (STD_ON == ETH_GLOBALTIMESUPPORT_API)
static FUNC(Std_ReturnType, ETH_CODE)
Eth_checkSetGlobalTimeErrors(uint8 ctrlIdx,
                             Eth_TimeStampType *timeStampPtr);
#endif /* (STD_ON == ETH_GLOBALTIMESUPPORT_API) */

#if (STD_ON == ETH_VIRTUALMAC_SUPPORT)
static FUNC(Std_ReturnType, ETH_CODE)
Eth_checkVirtualmacErrors(uint8 ctrlIdx, uint8 apiId);
#endif /* (STD_ON == ETH_VIRTUALMAC_SUPPORT) */

#if ((STD_ON == ETH_ZERO_COPY_TX) && (STD_OFF == ETH_USE_Q_APIS))
static FUNC(Std_ReturnType, ETH_CODE)
Eth_checkTransmitNoCpyErrors(uint8 ctrlIdx,
                             const uint8 *BufPtr,
                             const uint8 *physAddrPtr);
#endif /* ((STD_ON == ETH_ZERO_COPY_TX) && (STD_OFF == ETH_USE_Q_APIS)) */

#if (STD_ON == ETH_USE_Q_APIS)
static FUNC(Std_ReturnType, ETH_CODE)
Eth_checkRetrieveRxReadyQErrors(uint8 ctrlIdx,
                                const Eth_PktQ *pktQ);
#endif /* (STD_ON == ETH_USE_Q_APIS) */

#if (STD_ON == ETH_USE_Q_APIS)
static FUNC(Std_ReturnType, ETH_CODE)
Eth_checkSubmitRxFreeQErrors(uint8 ctrlIdx,
                             const Eth_PktQ *pktQ);
#endif /* (STD_ON == ETH_USE_Q_APIS) */

#if (STD_ON == ETH_USE_Q_APIS)
static FUNC(Std_ReturnType, ETH_CODE)
Eth_checkSubmitTxReadyQErrors(uint8 ctrlIdx,
                              const Eth_PktQ *pktQ);
#endif /* (STD_ON == ETH_USE_Q_APIS) */

#if (STD_ON == ETH_USE_Q_APIS)
static FUNC(Std_ReturnType, ETH_CODE)
Eth_checkRetrieveTxDoneQErrors(uint8 ctrlIdx,
                               const Eth_PktQ *pktQ);
#endif /* (STD_ON == ETH_USE_Q_APIS) */

#endif /* (STD_ON == ETH_DEV_ERROR_DETECT) */

/* ========================================================================== */
/*                            Global Variables                                */
/* ========================================================================== */

#define ETH_START_SEC_VAR_INIT_UNSPECIFIED
#include <Eth_MemMap.h>

/*
 * Design:       ETH_DesignId_033
 * Requirements: MCAL-1644
 */
volatile VAR(Eth_StateType, ETH_VAR_CLEARED) gEthDrvStatus = ETH_STATE_UNINIT;

#define ETH_STOP_SEC_VAR_INIT_UNSPECIFIED
#include <Eth_MemMap.h>

#define ETH_START_SEC_VAR_NO_INIT_UNSPECIFIED
#include <Eth_MemMap.h>

VAR(EthDrv, ETH_VAR_CLEARED) gEthDrv;

#define ETH_STOP_SEC_VAR_NO_INIT_UNSPECIFIED
#include <Eth_MemMap.h>

/* ========================================================================== */
/*                  Internal Function Definitions                             */
/* ========================================================================== */

#define ETH_START_SEC_CODE
#include <Eth_MemMap.h>

/* ========================================================================== */
/*                          Function Definitions                              */
/* ========================================================================== */
#if (STD_ON == ETH_VIRTUALMAC_SUPPORT)
FUNC(Std_ReturnType, ETH_CODE)
Eth_VirtMacRpcInit(P2CONST(Eth_ConfigType, AUTOMATIC, ETH_PBCFG) CfgPtr)
{
    Std_ReturnType status = E_OK;
    const Eth_ConfigType *cfg = CfgPtr;
    const char *announceMsg = ETH_RPC_REMOTE_SERVICE;

#if (STD_ON == ETH_PRE_COMPILE_VARIANT)
    if (NULL_PTR == CfgPtr)
    {
        cfg = ETH_DRV_CONFIG_0;
    }
#endif

    memcpy(&gEthDrv.ethConfig, cfg, sizeof(Eth_ConfigType));

    if (gEthDrv.ethConfig.enableVirtualMac == TRUE)
    {
#if (STD_ON == ETH_DEV_ERROR_DETECT)
        if ((Cdd_IpcIsInitDone() != TRUE) ||
            (Cdd_IpcGetMaxMsgSize(gEthDrv.ethConfig.virtualMacCfg.ethfwRpcComChId) < ETH_RPC_MSG_SIZE))
        {
            status = E_NOT_OK;
        }
#endif
        if (status == E_OK)
        {
            memset(&gEthDrv.virtualMacInfo, 0, sizeof(gEthDrv.virtualMacInfo));
            status = Cdd_IpcAnnounce(announceMsg, gEthDrv.ethConfig.virtualMacCfg.ethfwRpcComChId);
        }
    }
    else
    {
        status = E_NOT_OK;
    }


    return status;
}
#endif

/*
 * Design:       ETH_DesignId_005
 * Requirements: MCAL-1529, MCAL-1532, MCAL-1533, MCAL-1534,
 *               MCAL-1535, MCAL-1536, MCAL-1537, MCAL-1538,
 *               MCAL-1539, MCAL-1540, MCAL-1541, MCAL-1542
 */
FUNC(void, ETH_CODE)
Eth_Init(P2CONST(Eth_ConfigType, AUTOMATIC, ETH_PBCFG) CfgPtr)
{
    const Eth_ConfigType *cfg = CfgPtr;
    Std_ReturnType retVal = E_OK;

#if (STD_ON == ETH_PRE_COMPILE_VARIANT)
    if (NULL_PTR == CfgPtr)
    {
        cfg = ETH_DRV_CONFIG_0;
    }
#endif

    if (((Eth_CacheWbInv)NULL_PTR == cfg->cacheWbInv) ||
        ((Eth_CacheWb)NULL_PTR == cfg->cacheWb) ||
        ((Eth_CacheInv)NULL_PTR == cfg->cacheInv))
    {
        retVal = E_NOT_OK;
    }

    SchM_Enter_Eth_ETH_EXCLUSIVE_AREA_0();

    if (cfg->enableVirtualMac == TRUE)
    {
        /* When virt mac driver is enabled Eth_RpcInit is called before Eth_Init
         * which stores the configuration in the driver object. Here we just make sure
         * the configuration passed to the Eth_RpcInit and Eth_Init is same */
         if (0U != memcmp(cfg, &gEthDrv.ethConfig, sizeof(*cfg)))
         {
#if (STD_ON == ETH_DEV_ERROR_DETECT)
            Eth_reportDetError(ETH_SID_INIT, ETH_E_INV_PARAM);
#endif
            retVal = E_NOT_OK;
         }
    }

    if (E_OK == retVal)
    {
        /* Requirements: MCAL-1533 */
        memcpy(&gEthDrv.ethConfig, cfg, sizeof(gEthDrv.ethConfig));

        /* Initialize driver data */
        memset(&gEthDrv.errors, 0, sizeof(gEthDrv.errors));

        gEthDrv.enableCacheOps = cfg->enableCacheOps;
        gEthDrv.cacheWbInv     = cfg->cacheWbInv;
        gEthDrv.cacheInv       = cfg->cacheInv;
        gEthDrv.cacheWb        = cfg->cacheWb;
        gEthDrv.modeChanged    = FALSE;
        gEthDrv.mmrRegs        =
            (CSL_mcu_ctrl_mmr_cfg0Regs *)(uintptr_t)CSL_MCU_CTRL_MMR0_CFG0_BASE;
    }

    if (E_OK == retVal)
    {
        /* TODO add support for multiple configurations */
        retVal = Eth_ControllerInit(0U /* ctrlIdx */);
    }

    if (E_OK == retVal)
    {
         /* Requirements: MCAL-1534 */
        gEthDrvStatus = ETH_STATE_INIT;
    }

    SchM_Exit_Eth_ETH_EXCLUSIVE_AREA_0();
}

/*
 * Design:       ETH_DesignId_007
 * Requirements: MCAL-1544, MCAL-1545, MCAL-1546, MCAL-1547,
 *               MCAL-1548, MCAL-1625, MCAL-1626, MCAL-1653
 */
FUNC(Std_ReturnType, ETH_CODE)
Eth_SetControllerMode(uint8 CtrlIdx, Eth_ModeType CtrlMode)
{
    Std_ReturnType retVal = E_OK;

    SchM_Enter_Eth_ETH_EXCLUSIVE_AREA_0();

    /* Requirements - SWS_Eth_00043 - return E_NOT_OK if DET is disabled */
    retVal = Eth_checkSetControllerModeErrors(CtrlIdx, CtrlMode);

    if (E_OK == retVal)
    {
        /* Requirements: MCAL-1653 */
        retVal = Eth_checkControllerAccess(&gEthDrv);
    }

    if (E_OK == retVal)
    {
        if (ETH_MODE_ACTIVE == CtrlMode)
        {
            retVal = Eth_enableCpsw(&gEthDrv);
        }
        else
        {
            Eth_disableCpsw(&gEthDrv);

            /* Requirements: MCAL-1625 */
            Eth_releaseTxPkts(&gEthDrv, CtrlIdx);
        }
    }

    if (E_OK == retVal)
    {
        if (CtrlMode != gEthDrv.ctrlMode)
        {
            gEthDrv.modeChanged = TRUE;
        }

        gEthDrv.ctrlMode = CtrlMode;
    }

    SchM_Exit_Eth_ETH_EXCLUSIVE_AREA_0();

    return retVal;
}

/*
 * Design:       ETH_DesignId_008
 * Requirements: MCAL-1549, MCAL-1550, MCAL-1551, MCAL-1552,
 *               MCAL-1553, MCAL-1554
 */
FUNC(Std_ReturnType, ETH_CODE)
Eth_GetControllerMode(uint8 CtrlIdx,
                      P2VAR(Eth_ModeType, AUTOMATIC, ETH_APPL_DATA) CtrlModePtr)
{
    Std_ReturnType retVal = E_OK;

    retVal = Eth_checkGetControllerModeErrors(CtrlIdx, CtrlModePtr);

    if (E_OK == retVal)
    {
        *CtrlModePtr = gEthDrv.ctrlMode;
    }

    return retVal;
}

/*
 * Design:       ETH_DesignId_009
 * Requirements: MCAL-1555, MCAL-1556, MCAL-1557, MCAL-1558,
 *               MCAL-1559, MCAL-1560
 */
FUNC(void, ETH_CODE)
Eth_GetPhysAddr(uint8 CtrlIdx,
                P2VAR(uint8, AUTOMATIC, ETH_APPL_DATA) PhysAddrPtr)
{
    Std_ReturnType retVal = E_OK;

#if (STD_ON == ETH_DEV_ERROR_DETECT)
    retVal = Eth_checkGetPhysAddrErrors(CtrlIdx, PhysAddrPtr);
#endif

    /* Get physical address currently set to the MAC port */
    if (E_OK == retVal)
    {
        retVal = Eth_getMacAddr(&gEthDrv, PhysAddrPtr);
    }
}

/*
 * Design:       ETH_DesignId_019
 * Requirements: MCAL-1627, MCAL-1628, MCAL-1629, MCAL-1630,
 *               MCAL-1631, MCAL-1638
 */
FUNC(void, ETH_CODE)
Eth_SetPhysAddr(uint8 CtrlIdx,
                P2CONST(uint8, AUTOMATIC, ETH_APPL_DATA) PhysAddrPtr)
{
    Std_ReturnType retVal = E_OK;

#if (STD_ON == ETH_DEV_ERROR_DETECT)
    retVal = Eth_checkSetPhysAddrErrors(CtrlIdx, PhysAddrPtr);
#endif

    /* Update ALE entries only if in non-promiscuous mode. Otherwise, just
     * save the physical address because ALE is in bypass and no rules are
     * programmed */
    if (E_OK == retVal)
    {
        if (FALSE == gEthDrv.promiscuousMode)
        {
            retVal = Eth_replaceUcastAddr(&gEthDrv,
                                          gEthDrv.macAddr,
                                          PhysAddrPtr);
        }
    }

    /* Set new MAC address */
    if (E_OK == retVal)
    {
        retVal = Eth_setMacAddr(&gEthDrv, PhysAddrPtr);
    }

    /* Save the MAC address, it'll be used for any buffer transmission
     * from now on */
    if (E_OK == retVal)
    {
        memcpy(gEthDrv.macAddr, PhysAddrPtr, ETH_MAC_ADDR_LEN);
    }
}

#if (STD_ON == ETH_UPDATE_PHYS_ADDR_FILTER_API)
/*
 * Design:       ETH_DesignId_020
 * Requirements: MCAL-1632, MCAL-1633, MCAL-1634, MCAL-1637,
 *               MCAL-1639, MCAL-1649, MCAL-1650, MCAL-1651,
 *               MCAL-1652, MCAL-4827, MCAL-4828
 */
FUNC(Std_ReturnType, ETH_CODE)
Eth_UpdatePhysAddrFilter(uint8 CtrlIdx,
                         P2VAR(uint8, AUTOMATIC, ETH_APPL_DATA) PhysAddrPtr,
                         Eth_FilterActionType Action)
{
    Std_ReturnType retVal = E_OK;

#if (STD_ON == ETH_DEV_ERROR_DETECT)
    retVal = Eth_checkUpdatePhysAddrFilterErrors(CtrlIdx, PhysAddrPtr);
#endif

    if (E_OK == retVal)
    {
        if (TRUE == Eth_isBcastMacAddr(PhysAddrPtr))
        {
            /* Requirements: MCAL-1632 */
            retVal = Eth_setPromiscuousMode(&gEthDrv);
        }
        else if (TRUE == Eth_isNullMacAddr(PhysAddrPtr))
        {
            /* Requirements: MCAL-1634 */
            retVal = Eth_setNonPromiscuousMode(&gEthDrv, gEthDrv.macAddr);
        }
        else
        {
            if (ETH_ADD_TO_FILTER == Action)
            {
                retVal = Eth_addFilterAddr(&gEthDrv, PhysAddrPtr);
            }
            else
            {
                retVal = Eth_delFilterAddr(&gEthDrv, PhysAddrPtr);
            }
        }
    }

    return retVal;
}
#endif /* (STD_ON == ETH_UPDATE_PHYS_ADDR_FILTER_API) */

#if (STD_ON == ETH_ENABLE_MII_API)
/*
 * Design:       ETH_DesignId_010
 * Requirements: MCAL-1561, MCAL-1562, MCAL-1563, MCAL-1564,
 *               MCAL-1565, MCAL-1566, MCAL-1726
 */
FUNC(Std_ReturnType, ETH_CODE)
Eth_WriteMii(uint8 CtrlIdx,
             uint8 TrcvIdx,
             uint8 RegIdx,
             uint16 RegVal)
{
    Std_ReturnType retVal = E_OK;

#if (STD_ON == ETH_DEV_ERROR_DETECT)
    retVal = Eth_checkWriteMiiErrors(CtrlIdx, TrcvIdx, RegIdx, RegVal);
#endif

    if (E_OK == retVal)
    {
        retVal = Eth_triggerMiiWrite(&gEthDrv,
                                     CtrlIdx,
                                     TrcvIdx,
                                     RegIdx,
                                     RegVal);
    }

    return retVal;
}
#endif /* (STD_ON == ETH_ENABLE_MII_API) */

#if (STD_ON == ETH_ENABLE_MII_API)
/*
 * Design:       ETH_DesignId_011
 * Requirements: MCAL-1567, MCAL-1568, MCAL-1569, MCAL-1570,
 *               MCAL-1571, MCAL-1572, MCAL-1573, MCAL-1727
 */
FUNC(Std_ReturnType, ETH_CODE)
Eth_ReadMii(uint8 CtrlIdx,
            uint8 TrcvIdx,
            uint8 RegIdx,
            P2VAR(uint16, AUTOMATIC, ETH_APPL_DATA) RegValPtr)
{
    Std_ReturnType retVal = E_OK;

#if (STD_ON == ETH_DEV_ERROR_DETECT)
    retVal = Eth_checkReadMiiErrors(CtrlIdx, TrcvIdx, RegIdx, RegValPtr);
#endif

    if (E_OK == retVal)
    {
        retVal = Eth_triggerMiiRead(&gEthDrv,
                                    CtrlIdx,
                                    TrcvIdx,
                                    RegIdx);
    }

    return retVal;
}
#endif /* (STD_ON == ETH_ENABLE_MII_API) */

#if (STD_ON == ETH_GET_COUNTER_VALUES_API)
/*
 * Design:       ETH_DesignId_022
 * Requirements: MCAL-1711, MCAL-1712, MCAL-1713, MCAL-1714,
 *               MCAL-1715, MCAL-1716, MCAL-1717
 */
FUNC(Std_ReturnType, ETH_CODE)
Eth_GetCounterValues(uint8 CtrlIdx,
                     Eth_CounterType* CounterPtr)
{
    Std_ReturnType retVal = E_OK;

    retVal = Eth_checkGetCounterValuesErrors(CtrlIdx, CounterPtr);

    if (E_OK == retVal)
    {
        Eth_getCounterValues(&gEthDrv, CounterPtr);
    }

    return retVal;
}
#endif /* #if (STD_ON == ETH_GET_COUNTER_VALUES_API) */

#if (STD_ON == ETH_GET_RX_STATS_API)
/*
 * Design:       ETH_DesignId_023
 * Requirements: MCAL-1718, MCAL-1719, MCAL-1720, MCAL-1721,
 *               MCAL-1722, MCAL-1723, MCAL-1724
 */
FUNC(Std_ReturnType, ETH_CODE)
Eth_GetRxStats(uint8 CtrlIdx,
               P2VAR(Eth_RxStatsType, AUTOMATIC, ETH_APPL_DATA) RxStats)
{
    Std_ReturnType retVal = E_OK;

    retVal = Eth_checkGetRxStatsErrors(CtrlIdx, RxStats);

    if (E_OK == retVal)
    {
       /* Requirements: MCAL-1718 */
        Eth_getRxStatsRFC2819(&gEthDrv, RxStats);
    }

    return retVal;
}
#endif /* (STD_ON == ETH_GET_RX_STATS_API) */

#if (STD_ON == ETH_GET_TX_STATS_API)
/*
 * Design:       ETH_DesignId_040
 * Requirements: MCAL-4844, MCAL-4830, MCAL-4831, MCAL-4832,
 *               MCAL-4833
 */
FUNC(Std_ReturnType, ETH_CODE)
Eth_GetTxStats(uint8 CtrlIdx,
               P2VAR(Eth_TxStatsType, AUTOMATIC, ETH_APPL_DATA) TxStats)
{
    Std_ReturnType retVal = E_OK;

    retVal = Eth_checkGetTxStatsErrors(CtrlIdx, TxStats);

    if (E_OK == retVal)
    {
       /* Requirements: MCAL-4844 */
        Eth_getTxStatsRFC1213(&gEthDrv, TxStats);
    }

    return retVal;
}
#endif /* (STD_ON == ETH_GET_TX_STATS_API) */

#if (STD_ON == ETH_GET_TX_ERROR_COUNTERSVALUES_API)
/*
 * Design:       ETH_DesignId_041
 * Requirements: MCAL-4845, MCAL-4834, MCAL-4835, MCAL-4836,
 *               MCAL-4837
 */
FUNC(Std_ReturnType, ETH_CODE)
Eth_GetTxErrorCounterValues(uint8 CtrlIdx,
               P2VAR(Eth_TxErrorCounterValuesType, AUTOMATIC, ETH_APPL_DATA) TxErrorCounterValues)
{
    Std_ReturnType retVal = E_OK;

    retVal = Eth_checkGetTxErrorCounterValueErrors(CtrlIdx, TxErrorCounterValues);

    if (E_OK == retVal)
    {
       /* Requirements: MCAL-4845 */
        Eth_getTxErrorCounterValues(&gEthDrv, TxErrorCounterValues);
    }

    return retVal;
}
#endif

#if (STD_OFF == ETH_USE_Q_APIS)
/*
 * Design:       ETH_DesignId_012
 * Requirements: MCAL-1574, MCAL-1575, MCAL-1576, MCAL-1577,
 *               MCAL-1578, MCAL-1579, MCAL-1580, MCAL-1581,
 *               MCAL-1582, MCAL-1583, MCAL-1625
 */
FUNC(BufReq_ReturnType, ETH_CODE)
Eth_ProvideTxBuffer(uint8 CtrlIdx,
                    uint8 Priority,
                    P2VAR(Eth_BufIdxType, AUTOMATIC, ETH_APPL_DAT) BufIdxPtr,
                    P2VAR(uint8, AUTOMATIC, ETH_APPL_DAT) *BufPtr,
                    P2VAR(uint16, AUTOMATIC, ETH_APPL_DAT) LenBytePtr)
{
    Eth_Pkt *pkt;
    uint16 totalLen;
    BufReq_ReturnType retVal = BUFREQ_OK;

#if (STD_ON == ETH_DEV_ERROR_DETECT)
    retVal = Eth_checkProvideTxBufferErrors(CtrlIdx,
                                            BufIdxPtr,
                                            BufPtr,
                                            LenBytePtr);
#endif

    if (Priority != 0U)
    {
        /* Priority is not supported and app should always pass it as 0 */
        /* Requirements: MCAL-1578 */
#if (STD_ON == ETH_DEV_ERROR_DETECT)
        Eth_reportDetError(ETH_SID_PROVIDE_TX_BUFFER, ETH_E_INV_PARAM);
#endif
        retVal = BUFREQ_E_NOT_OK;
    }

    /* Request a TX buffer of the total length (header + requested size) */
    if (BUFREQ_OK == retVal)
    {
        totalLen = *LenBytePtr + ETH_HEADER_LEN;
        SchM_Enter_Eth_ETH_EXCLUSIVE_AREA_0();
        retVal = Eth_requestTxPkt(&gEthDrv, CtrlIdx, &pkt, &totalLen);
        SchM_Exit_Eth_ETH_EXCLUSIVE_AREA_0();
    }

    if (BUFREQ_OK == retVal)
    {
        /* Returned buffer pointer and length excludes the MAC header */
        *BufIdxPtr = pkt->idx;
        *BufPtr = (uint8 *)pkt->buf + ETH_HEADER_LEN;
        *LenBytePtr = pkt->userLen - ETH_HEADER_LEN;
    }
    else if (BUFREQ_E_OVFL == retVal)
    {
        /* Requirements: MCAL-1576 */
        *BufPtr = (uint8 *)NULL_PTR;
        *LenBytePtr = totalLen - ETH_HEADER_LEN;
    }

    return retVal;
}
#endif /* (STD_OFF == ETH_USE_Q_APIS) */

#if (STD_OFF == ETH_USE_Q_APIS)
/*
 * Design:       ETH_DesignId_013
 * Requirements: MCAL-1584, MCAL-1585, MCAL-1586, MCAL-1587,
 *               MCAL-1588, MCAL-1589, MCAL-1590, MCAL-1591
 *               MCAL-1621, MCAL-1626
 */
FUNC(Std_ReturnType, ETH_CODE)
Eth_Transmit(uint8 CtrlIdx,
             Eth_BufIdxType BufIdx,
             Eth_FrameType FrameType,
             boolean TxConfirmation,
             uint16 LenByte,
             P2VAR(uint8, AUTOMATIC, ETH_APPL_DAT) PhysAddrPtr)
{
    Eth_Pkt *pkt = NULL;
    Eth_Frame *frame;
    uint16 totalLen;
    Std_ReturnType retVal = E_OK;

    retVal = Eth_checkTransmitErrors(CtrlIdx, BufIdx, PhysAddrPtr);

    /* Get granted packet with the given index.  Invalid indexes will be
     * detected here */
    if (E_OK == retVal)
    {
        SchM_Enter_Eth_ETH_EXCLUSIVE_AREA_0();
        retVal = Eth_getGrantedTxPkt(&gEthDrv, CtrlIdx, BufIdx, &pkt);
        SchM_Exit_Eth_ETH_EXCLUSIVE_AREA_0();
    }

    /* Check that current length doesn't exceed the granted length */
    if (E_OK == retVal)
    {
        totalLen = LenByte + ETH_HEADER_LEN;
        if (totalLen <= pkt->userLen)
        {
            pkt->userLen = totalLen;
        }
        else
        {
            retVal = E_NOT_OK;
        }
    }

    /* Populate the MAC header */
    /* Requirements: MCAL-1585 */
    if (E_OK == retVal)
    {
        frame = (Eth_Frame *)pkt->buf;
        memcpy(frame->hdr.dstMac, PhysAddrPtr, ETH_MAC_ADDR_LEN);
        memcpy(frame->hdr.srcMac, gEthDrv.macAddr, ETH_MAC_ADDR_LEN);
        frame->hdr.etherType = htons(FrameType);
    }

    /* Transmit the indexed buffer */
    if (E_OK == retVal)
    {
        pkt->txConfirmation = TxConfirmation;
#if (STD_ON == ETH_ZERO_COPY_TX)
        pkt->txNoCpy = FALSE;
#endif
        SchM_Enter_Eth_ETH_EXCLUSIVE_AREA_0();
        retVal = Eth_transmitPkt(&gEthDrv, CtrlIdx, pkt);
        SchM_Exit_Eth_ETH_EXCLUSIVE_AREA_0();
    }

    /* Free packet if transmission failed to avoid leaks */
    if (E_OK != retVal)
    {
        if (NULL != pkt)
        {
            /* Requirements: MCAL-4838 */
            if (TRUE == TxConfirmation)
            {
                EthIf_TxConfirmation(CtrlIdx, pkt->idx, E_NOT_OK);
            }

            EthPktQ_queue(&gEthDrv.txFreeQ, pkt);
        }
    }

    return retVal;
}
#endif /* (STD_OFF == ETH_USE_Q_APIS) */

#if ((STD_ON == ETH_ZERO_COPY_TX) && (STD_OFF == ETH_USE_Q_APIS))
/*
 * Design:
 * Requirements: MCAL-4527
 */
FUNC(Std_ReturnType, ETH_CODE)
Eth_TransmitNoCpy(uint8 CtrlIdx,
                  P2VAR(uint8, AUTOMATIC, ETH_APPL_DAT) BufPtr,
                  Eth_FrameType FrameType,
                  uint16 LenByte,
                  P2VAR(uint8, AUTOMATIC, ETH_APPL_DAT) PhysAddrPtr)
{
    Eth_Pkt *pkt;
    Eth_Frame *frame;
    Std_ReturnType retVal = E_OK;

#if (STD_ON == ETH_DEV_ERROR_DETECT)
    retVal = Eth_checkTransmitNoCpyErrors(CtrlIdx, BufPtr, PhysAddrPtr);
#endif

    if (E_OK == retVal)
    {
        SchM_Enter_Eth_ETH_EXCLUSIVE_AREA_0();
        pkt = EthPktQ_dequeue(&gEthDrv.txFreeQNoCpy);
        SchM_Exit_Eth_ETH_EXCLUSIVE_AREA_0();
        if (NULL == pkt)
        {
            retVal = ETH_E_BUSY;
        }
    }

    /* Populate the MAC header */
    /* Requirements: MCAL-1585 */
    if (E_OK == retVal)
    {
        frame = (Eth_Frame *)BufPtr;
        memcpy(frame->hdr.dstMac, PhysAddrPtr, ETH_MAC_ADDR_LEN);
        memcpy(frame->hdr.srcMac, gEthDrv.macAddr, ETH_MAC_ADDR_LEN);
        frame->hdr.etherType = htons(FrameType);
    }

    /* Transmit the indexed buffer */
    if (E_OK == retVal)
    {
        pkt->next = NULL;
        pkt->buf = BufPtr;
        pkt->len = LenByte;
        pkt->userLen = LenByte;
        pkt->state = ETH_BUF_STATE_IN_USE;
        pkt->txConfirmation = FALSE;
        pkt->txNoCpy = TRUE;

        SchM_Enter_Eth_ETH_EXCLUSIVE_AREA_0();
        retVal = Eth_transmitPkt(&gEthDrv, CtrlIdx, pkt);
        SchM_Exit_Eth_ETH_EXCLUSIVE_AREA_0();

        if (E_NOT_OK == retVal)
        {
            EthIf_TxConfirmationNoCpy(CtrlIdx, pkt->buf, E_NOT_OK);
        }
    }

    return retVal;
}
#endif /* (STD_ON == ETH_ZERO_COPY_TX) && (STD_OFF == ETH_USE_Q_APIS)) */

#if (STD_OFF == ETH_USE_Q_APIS)
/*
 * Design:       ETH_DesignId_014
 * Requirements: MCAL-1592, MCAL-1593, MCAL-1594, MCAL-1595,
 *               MCAL-1596, MCAL-1622, MCAL-1640
 */
FUNC(void, ETH_CODE)
Eth_Receive(uint8 CtrlIdx,
            uint8 FifoIdx,
            P2VAR(Eth_RxStatusType, AUTOMATIC, ETH_APPL_DAT) RxStatusPtr)
{
    Eth_RxStatusType status = ETH_NOT_RECEIVED;
    Std_ReturnType retVal = E_OK;

#if (STD_ON == ETH_DEV_ERROR_DETECT)
    retVal = Eth_checkReceiveErrors(CtrlIdx, RxStatusPtr);
#endif

    if (FifoIdx != 0U)
    {
        /* Priority is not supported and app should always pass it as 0 */
        /* Requirements: MCAL-1578 */
#if (STD_ON == ETH_DEV_ERROR_DETECT)
        Eth_reportDetError(ETH_SID_RECEIVE, ETH_E_INV_PARAM);
#endif
        retVal = ETH_NOT_RECEIVED;
    }

    /* Receive packets from CPSW */
    /* Receive packets from CPSW.  Accesses to RX CQ ring are optimized by
     * deferring dequeueing packets only when upper layer has processed
     * all frames already in driver's rxReadyQ */
    if ((E_OK == retVal) &&
        (0U == EthPktQ_getCount(&gEthDrv.rxReadyQ)))
    {
        retVal = Eth_receivePktQ(&gEthDrv, CtrlIdx, &gEthDrv.rxReadyQ);
    }

    /* Dequeue one received packet from rxReadyQ and pass it to EthIf */
    if (E_OK == retVal)
    {
        status = Eth_processRxPkt(&gEthDrv, CtrlIdx);
    }

    *RxStatusPtr = status;
}
#endif /* (STD_OFF == ETH_USE_Q_APIS) */

/*
 * Design:       ETH_DesignId_015
 * Requirements: MCAL-1597, MCAL-1598, MCAL-1599, MCAL-1600,
 *               MCAL-1601, MCAL-1602, MCAL-1623
 */
FUNC(void, ETH_CODE)
Eth_TxConfirmation(uint8 CtrlIdx)
{
    Eth_Pkt *pkt;
    Std_ReturnType retVal = E_OK;

#if (STD_ON == ETH_DEV_ERROR_DETECT)
    retVal = Eth_checkTxConfirmationErrors(CtrlIdx);
#endif

    /* Lazy recycle of consumed TX packets */
#if (STD_OFF == ETH_ENABLE_TX_INTERRUPT)
    if (E_OK == retVal)
    {
        SchM_Enter_Eth_ETH_EXCLUSIVE_AREA_0();
        retVal = Eth_retrieveFreeTxPktQ(&gEthDrv, CtrlIdx, NULL);
        SchM_Exit_Eth_ETH_EXCLUSIVE_AREA_0();
        if (E_OK != retVal)
        {
            (void)Dem_SetEventStatus(ETH_E_TX_INTERNAL,
                                     DEM_EVENT_STATUS_PREFAILED);
        }
    }
#endif

    /* Packets that need confirmation are already in txConfQ */
    if (E_OK == retVal)
    {
        /* Requirements: MCAL-1598 */
        SchM_Enter_Eth_ETH_EXCLUSIVE_AREA_0();
        pkt = EthPktQ_dequeue(&gEthDrv.txConfQ);
        SchM_Exit_Eth_ETH_EXCLUSIVE_AREA_0();
        while (NULL != pkt)
        {
            if (pkt->txConfirmation)
            {
                /* Requirements: MCAL-4825 */
                EthIf_TxConfirmation(CtrlIdx, pkt->idx, E_OK);

                /* Requirements: MCAL-1599 */
                pkt->state = ETH_BUF_STATE_FREE;

                SchM_Enter_Eth_ETH_EXCLUSIVE_AREA_0();
                EthPktQ_queue(&gEthDrv.txFreeQ, pkt);
                SchM_Exit_Eth_ETH_EXCLUSIVE_AREA_0();
            }
#if ((STD_ON == ETH_ZERO_COPY_TX) && (STD_OFF == ETH_USE_Q_APIS))
            else if (pkt->txNoCpy)
            {
                EthIf_TxConfirmationNoCpy(gEthDrv.ethConfig.ctrlIdx,
                                          pkt->buf,
                                          E_OK);
                SchM_Enter_Eth_ETH_EXCLUSIVE_AREA_0();
                EthPktQ_queue(&gEthDrv.txFreeQNoCpy, pkt);
                SchM_Exit_Eth_ETH_EXCLUSIVE_AREA_0();
            }
#endif
            else
            {
                /* nothing */
            }

            SchM_Enter_Eth_ETH_EXCLUSIVE_AREA_0();
            pkt = EthPktQ_dequeue(&gEthDrv.txConfQ);
            SchM_Exit_Eth_ETH_EXCLUSIVE_AREA_0();
        }
    }
}

/*
 * Design:       ETH_DesignId_021
 * Requirements: MCAL-1654, MCAL-1655, MCAL-1656, MCAL-1657,
 *               MCAL-1725
 */
FUNC(void, ETH_CODE)
Eth_MainFunction(void)
{
    uint8 ctrlIdx = 0;
    Std_ReturnType retVal = E_OK;

#if (STD_ON == ETH_DEV_ERROR_DETECT)
    if (ETH_STATE_INIT != gEthDrvStatus)
    {
        /* Not mandatory as per 4.3.1 specification */
        Eth_reportDetError(ETH_SID_MAIN_FUNCTION, ETH_E_UNINIT);
        retVal = E_NOT_OK;
    }
#endif

    if (E_OK == retVal)
    {
        if (FALSE == gEthDrv.ethConfig.enableVirtualMac)
        {
            /* Requirements: MCAL-1657 */
            Eth_checkControllerErrors(&gEthDrv);
        }
        /* Requirements: MCAL-1725 */
        if (TRUE == gEthDrv.modeChanged)
        {
            if (ETH_MODE_ACTIVE == gEthDrv.ctrlMode)
            {
                EthIf_CtrlModeIndication(ctrlIdx, ETH_MODE_ACTIVE);
            }
            else
            {
                EthIf_CtrlModeIndication(ctrlIdx, ETH_MODE_DOWN);
            }

            gEthDrv.modeChanged = FALSE;
        }

#if (STD_ON == ETH_VIRTUALMAC_SUPPORT)
        if (TRUE == gEthDrv.ethConfig.enableVirtualMac)
        {
            if (FALSE == gEthDrv.virtualMacInfo.ethFwAttached)
            {
                gEthDrv.virtualMacInfo.responseTimeoutCnt++;
                if (gEthDrv.virtualMacInfo.responseTimeoutCnt >= ETH_VIRTUALMAC_FWINFO_TIMEOUT)
                {
                    retVal = Eth_RpcAnnounce(&gEthDrv);
#if (STD_ON == ETH_DEV_ERROR_DETECT)
                    if (E_OK != retVal)
                    {
                        Eth_reportDetError(ETH_SID_MAIN_FUNCTION, ETH_E_NOT_OK);
                    }
                    else
#endif /* #if (STD_ON == ETH_DEV_ERROR_DETECT) */
                    {
                        gEthDrv.virtualMacInfo.responseTimeoutCnt = 0;
                    }
                }
            }
            if (E_OK == retVal)
            {
                if (TRUE == gEthDrv.ethConfig.virtualMacCfg.pollRecvMsgInEthMain)
                {
                    /* Process all queued receive msgs . If there are no
                     * queued msgs,Eth_RpcRecvMsg will return E_NOT_OK.
                     * This is not an error condition and just indicates
                     * no recv msgs are queued.
                     */
                    do {
                        retVal = Eth_RpcRecvMsg(&gEthDrv, ETH_SID_MAIN_FUNCTION);
                    } while (retVal == E_OK);
                }
            }
        } /* if (TRUE == gEthDrv.ethConfig.enableVirtualMac) */
#endif /* #if (STD_ON == ETH_VIRTUALMAC_SUPPORT) */

    }
}

#if (STD_ON == ETH_VERSION_INFO_API)
/*
 * Design:        ETH_DesignId_016
 * Requirements:  MCAL-1603, MCAL-1624
 */
FUNC(void, ETH_CODE)
Eth_GetVersionInfo(P2VAR(Std_VersionInfoType, AUTOMATIC, ETH_APPL_DATA) VersionInfo)
{
#if (STD_ON == ETH_DEV_ERROR_DETECT)
    if (NULL_PTR == VersionInfo)
    {
        /* Requirements: MCAL-1624 */
        Eth_reportDetError(ETH_SID_GET_VERSION_INFO, ETH_E_PARAM_POINTER);
    }
    else
#endif
    {
        VersionInfo->vendorID         = (uint16) ETH_VENDOR_ID;
        VersionInfo->moduleID         = (uint16) ETH_MODULE_ID;
        VersionInfo->sw_major_version = (uint8) ETH_SW_MAJOR_VERSION;
        VersionInfo->sw_minor_version = (uint8) ETH_SW_MINOR_VERSION;
        VersionInfo->sw_patch_version = (uint8) ETH_SW_PATCH_VERSION;
    }
}
#endif /* (STD_ON == ETH_VERSION_INFO_API) */

#if (STD_ON == ETH_GLOBALTIMESUPPORT_API)
/*
 * Design:       ETH_DesignId_026
 * Requirements: MCAL-1666, MCAL-1667, MCAL-1668, MCAL-1669,
 *               MCAL-1670, MCAL-1695
 */
FUNC(Std_ReturnType, ETH_CODE)
Eth_GetCurrentTime(uint8 CtrlIdx,
                   P2VAR(Eth_TimeStampQualType, AUTOMATIC, ETH_APPL_DATA) timeQualPtr,
                   P2VAR(Eth_TimeStampType, AUTOMATIC, ETH_APPL_DATA) timeStampPtr)
{
#if (STD_ON == ETH_DEV_ERROR_DETECT)
    Std_ReturnType retVal = E_OK;

    retVal = Eth_checkGetCurrentTimeErrors(CtrlIdx, timeQualPtr, timeStampPtr);
#endif

    /* Not implemented */
    retVal = E_NOT_OK;

    return retVal;
}
#endif /* (STD_ON == ETH_GLOBALTIMESUPPORT_API) */

#if (STD_ON == ETH_GLOBALTIMESUPPORT_API)
/*
 * Design:       ETH_DesignId_027
 * Requirements: MCAL-1671, MCAL-1672, MCAL-1673, MCAL-1674,
 *               MCAL-1696
 */
FUNC(void, ETH_CODE)
Eth_EnableEgressTimeStamp(uint8 CtrlIdx, uint8 BufIdx)
{
    Std_ReturnType retVal = E_OK;

#if (STD_ON == ETH_DEV_ERROR_DETECT)
    retVal = Eth_checkEnableEgressTimeStampErrors(CtrlIdx, BufIdx);
#endif

    /* Unused */
    (void)retVal;
}
#endif /* (STD_ON == ETH_GLOBALTIMESUPPORT_API) */

#if (STD_ON == ETH_GLOBALTIMESUPPORT_API)
/*
 * Design:       ETH_DesignId_028
 * Requirements: MCAL-1675, MCAL-1676, MCAL-1677, MCAL-1678,
 *               MCAL-1679, MCAL-1697
 */
FUNC(void, ETH_CODE)
Eth_GetEgressTimeStamp(uint8 CtrlIdx,
                       uint8 BufIdx,
                       P2VAR(Eth_TimeStampQualType, AUTOMATIC, ETH_APPL_DATA) timeQualPtr,
                       P2VAR(Eth_TimeStampType, AUTOMATIC, ETH_APPL_DATA) timeStampPtr)
{
    Std_ReturnType retVal = E_OK;

#if (STD_ON == ETH_DEV_ERROR_DETECT)
    retVal = Eth_checkGetEgressTimeStampErrors(CtrlIdx, timeQualPtr, timeStampPtr);
#endif

    /* Unused */
    (void)retVal;
}
#endif /* (STD_ON == ETH_GLOBALTIMESUPPORT_API) */

#if (STD_ON == ETH_GLOBALTIMESUPPORT_API)
/*
 * Design:        ETH_DesignId_029
 * Requirements:  MCAL-1680, MCAL-1681, MCAL-1682, MCAL-1683,
 *                MCAL-1684, MCAL-1698
 */
FUNC(void, ETH_CODE)
Eth_GetIngressTimeStamp(uint8 CtrlIdx,
                        P2VAR(Eth_DataType, AUTOMATIC, ETH_APPL_DATA) DataPtr,
                        P2VAR(Eth_TimeStampQualType, AUTOMATIC, ETH_APPL_DATA) timeQualPtr,
                        P2VAR(Eth_TimeStampType, AUTOMATIC, ETH_APPL_DATA) timeStampPtr)
{
    Std_ReturnType retVal = E_OK;

#if (STD_ON == ETH_DEV_ERROR_DETECT)
    retVal = Eth_checkGetIngressTimeStampErrors(CtrlIdx, DataPtr, timeQualPtr, timeStampPtr);
#endif

    /* Unused */
    (void)retVal;
}
#endif /* (STD_ON == ETH_GLOBALTIMESUPPORT_API) */

#if (STD_ON == ETH_GLOBALTIMESUPPORT_API)
/*
 * Design:       ETH_DesignId_030
 * Requirements: MCAL-1685, MCAL-1686, MCAL-1687, MCAL-1688,
 *               MCAL-1689, MCAL-1699
 */
FUNC(void, ETH_CODE)
Eth_SetCorrectionTime(uint8 CtrlIdx,
                      P2VAR(Eth_TimeIntDiffType, AUTOMATIC, ETH_APPL_DAT) timeOffsetPtr,
                      P2VAR(Eth_RateRatioType, AUTOMATIC, ETH_APPL_DAT) rateRatioPtr)
{
    Std_ReturnType retVal = E_OK;

#if (STD_ON == ETH_DEV_ERROR_DETECT)
    retVal = Eth_checkSetCorrectionTimeErrors(CtrlIdx, timeOffsetPtr, rateRatioPtr);
#endif

    /* Unused */
    (void)retVal;
}
#endif /* (STD_ON == ETH_GLOBALTIMESUPPORT_API) */

#if (STD_ON == ETH_GLOBALTIMESUPPORT_API)
/*
 * Design:       ETH_DesignId_031
 * Requirements: MCAL-1690, MCAL-1691, MCAL-1692, MCAL-1693,
 *               MCAL-1694, MCAL-1700
 */
FUNC(Std_ReturnType, ETH_CODE)
Eth_SetGlobalTime(uint8 CtrlIdx,
        P2VAR(Eth_TimeStampType, AUTOMATIC, ETH_APPL_DAT) timeStampPtr)
{
    Std_ReturnType retVal = E_OK;

#if (STD_ON == ETH_DEV_ERROR_DETECT)
    retVal = Eth_checkSetGlobalTimeErrors(CtrlIdx, timeStampPtr);
#endif

    /* Not implemented */
    retVal = E_NOT_OK;

    return retVal;
}
#endif /* (STD_ON == ETH_GLOBALTIMESUPPORT_API) */

#if (STD_ON == ETH_VIRTUALMAC_SUPPORT)
/*
 * Design:
 * Requirements:
 */
FUNC(Std_ReturnType, ETH_CODE)
Eth_DispatchVirtmacInit(uint8 CtrlIdx)
{
    Std_ReturnType retVal = E_OK;

#if (STD_ON == ETH_DEV_ERROR_DETECT)
    if (ETH_CONTROLLER_ID_0 != CtrlIdx)
    {
        Eth_reportDetError(ETH_SID_DISPATCH_VIRTMAC_INIT, ETH_E_INV_CTRL_IDX);
    }
    else if (gEthDrv.ethConfig.enableVirtualMac != TRUE)
    {
        Eth_reportDetError(ETH_SID_DISPATCH_VIRTMAC_INIT, ETH_E_INV_PARAM);
    }
    else
    {
        retVal = E_OK;
    }

    if ((gEthDrv.virtualMacInfo.cpswAttachInfo.id != 0) ||
        (gEthDrv.virtualMacInfo.detachResponsePending == TRUE))
    {
        /* Detach not done . Eth_DispatchVirtmacDeinit not invoked or RESPONSE not yet received */
        Eth_reportDetError(ETH_SID_DISPATCH_VIRTMAC_INIT, ETH_E_UNINIT);
        retVal = E_NOT_OK;
    }
#endif

    if (E_OK == retVal)
    {
        retVal = Eth_RpcSendExtendedAttachReq(&gEthDrv);
    }
    return retVal;
}
#endif /* (STD_ON == ETH_VIRTUALMAC_SUPPORT) */

#if (STD_ON == ETH_VIRTUALMAC_SUPPORT)
/*
 * Design:
 * Requirements:
 */
FUNC(Std_ReturnType, ETH_CODE)
Eth_DispatchVirtmacDeinit(uint8 CtrlIdx)
{
    Std_ReturnType retVal = E_OK;

#if (STD_ON == ETH_DEV_ERROR_DETECT)
    retVal = Eth_checkVirtualmacErrors(CtrlIdx, ETH_SID_DISPATCH_VIRTMAC_DEINIT);
    if (gEthDrv.virtualMacInfo.cpswAttachInfo.id == 0)
    {
        /* Detach not done . Eth_DispatchVirtmacDeinit not invoked or RESPONSE not yet received */
        Eth_reportDetError(ETH_SID_DISPATCH_VIRTMAC_DEINIT, ETH_E_UNINIT);
        retVal = E_NOT_OK;
    }
#endif

    if (E_OK == retVal)
    {
        retVal = Eth_RpcSendDetachReq(&gEthDrv);
    }
    return retVal;
}
#endif /* (STD_ON == ETH_VIRTUALMAC_SUPPORT) */


#if ((STD_ON == ETH_VIRTUALMAC_SUPPORT) && (STD_ON == ETH_VIRTUALMAC_NOTIFYMSGRECEIVED_API))
/*
 * Design:
 * Requirements:
 */
FUNC(Std_ReturnType, ETH_CODE)
Eth_NotifyVirtmacMsgReceived(uint8 CtrlIdx)
{
    Std_ReturnType retVal = E_OK;

#if (STD_ON == ETH_DEV_ERROR_DETECT)
    if (ETH_CONTROLLER_ID_0 != CtrlIdx)
    {
        Eth_reportDetError(ETH_SID_NOTIFY_VIRTMAC_MSGRECV, ETH_E_INV_CTRL_IDX);
    }
    else if (gEthDrv.ethConfig.enableVirtualMac != TRUE)
    {
        Eth_reportDetError(ETH_SID_NOTIFY_VIRTMAC_MSGRECV, ETH_E_INV_PARAM);
    }
    else
    {
        retVal = E_OK;
    }
    #endif

    if (E_OK == retVal)
    {
        retVal = Eth_RpcRecvMsg(&gEthDrv, ETH_SID_NOTIFY_VIRTMAC_MSGRECV);
    }
    return retVal;
}
#endif /* ((STD_ON == ETH_VIRTUALMAC_SUPPORT) && (STD_ON == ETH_VIRTUALMAC_NOTIFYMSGRECEIVED_API)) */

#if ((STD_ON == ETH_VIRTUALMAC_SUPPORT) && (STD_ON == ETH_VIRTUALMAC_SUBSCRIBEALLTRAFFIC_API))
/*
 * Design:
 * Requirements:
 */
FUNC(Std_ReturnType, ETH_CODE)
Eth_DispatchVirtmacSubscribeAllTraffic(uint8 CtrlIdx)
{
    Std_ReturnType retVal = E_OK;

#if (STD_ON == ETH_DEV_ERROR_DETECT)
    retVal = Eth_checkVirtualmacErrors(CtrlIdx, ETH_SID_DISPATCH_VIRTMAC_SUBSCRIBE_ALLTRAFFIC);
#endif

    if (E_OK == retVal)
    {
        retVal = Eth_RpcSendRegisterDefaultReq(&gEthDrv);
    }
    return retVal;
}
#endif /* ((STD_ON == ETH_VIRTUALMAC_SUPPORT) && (STD_ON == ETH_VIRTUALMAC_SUBSCRIBEALLTRAFFIC_API)) */

#if ((STD_ON == ETH_VIRTUALMAC_SUPPORT) && (STD_ON == ETH_VIRTUALMAC_UNSUBSCRIBEALLTRAFFIC_API))
/*
 * Design:
 * Requirements:
 */
FUNC(Std_ReturnType, ETH_CODE)
Eth_DispatchVirtmacUnsubscribeAllTraffic(uint8 CtrlIdx)
{
    Std_ReturnType retVal = E_OK;

#if (STD_ON == ETH_DEV_ERROR_DETECT)
    retVal = Eth_checkVirtualmacErrors(CtrlIdx, ETH_SID_DISPATCH_VIRTMAC_UNSUBSCRIBE_ALLTRAFFIC);
#endif

    if (E_OK == retVal)
    {
        retVal = Eth_RpcSendUnregisterDefaultReq(&gEthDrv);
    }
    return retVal;
}
#endif /* ((STD_ON == ETH_VIRTUALMAC_SUPPORT) && (STD_ON == ETH_VIRTUALMAC_SUBSCRIBEALLTRAFFIC_API)) */


#if ((STD_ON == ETH_VIRTUALMAC_SUPPORT) && (STD_ON == ETH_VIRTUALMAC_SUBSCRIBEDSTMAC_API))
/*
 * Design:
 * Requirements:
 */
FUNC(Std_ReturnType, ETH_CODE)
Eth_DispatchVirtmacSubscribeDstMac(uint8 CtrlIdx, uint8 *macAddress)
{
    Std_ReturnType retVal = E_OK;

#if (STD_ON == ETH_DEV_ERROR_DETECT)
    retVal = Eth_checkVirtualmacErrors(CtrlIdx, ETH_SID_DISPATCH_VIRTMAC_SUBSCRIBE_DSTMAC);
#endif

    if (E_OK == retVal)
    {
        retVal = Eth_RpcSendRegisterMacReq(&gEthDrv, macAddress);
    }
    return retVal;
}
#endif /* ((STD_ON == ETH_VIRTUALMAC_SUPPORT) && (STD_ON == ETH_VIRTUALMAC_SUBSCRIBEDSTMAC_API)) */

#if ((STD_ON == ETH_VIRTUALMAC_SUPPORT) && (STD_ON == ETH_VIRTUALMAC_UNSUBSCRIBEDSTMAC_API))
/*
 * Design:
 * Requirements:
 */
FUNC(Std_ReturnType, ETH_CODE)
Eth_DispatchVirtmacUnsubscribeDstMac(uint8 CtrlIdx, uint8 *macAddress)
{
    Std_ReturnType retVal = E_OK;

#if (STD_ON == ETH_DEV_ERROR_DETECT)
    retVal = Eth_checkVirtualmacErrors(CtrlIdx, ETH_SID_DISPATCH_VIRTMAC_UNSUBSCRIBE_DSTMAC);
#endif

    if (E_OK == retVal)
    {
        retVal = Eth_RpcSendUnregisterMacReq(&gEthDrv, macAddress);
    }
    return retVal;
}
#endif /* ((STD_ON == ETH_VIRTUALMAC_SUPPORT) && (STD_ON == ETH_VIRTUALMAC_UNSUBSCRIBEDSTMAC_API)) */


#if ((STD_ON == ETH_VIRTUALMAC_SUPPORT) && (STD_ON == ETH_VIRTUALMAC_ASSOCIATEIPV4MACADDR_API))
/*
 * Design:
 * Requirements:
 */
FUNC(Std_ReturnType, ETH_CODE)
Eth_DispatchVirtmacAssociateIPv4Macaddr(uint8 CtrlIdx, uint8 *ipv4Address, uint8 *macAddress)
{
    Std_ReturnType retVal = E_OK;

#if (STD_ON == ETH_DEV_ERROR_DETECT)
    retVal = Eth_checkVirtualmacErrors(CtrlIdx, ETH_SID_DISPATCH_VIRTMAC_IPV4_MACADDR_ASSOCIATE);
#endif

    if (E_OK == retVal)
    {
        retVal = Eth_RpcSendIPv4RegisterMacReq(&gEthDrv, ipv4Address, macAddress);
    }
    return retVal;
}
#endif /* ((STD_ON == ETH_VIRTUALMAC_SUPPORT) && (STD_ON == ETH_VIRTUALMAC_ASSOCIATEIPV4MACADDR_API)) */

#if ((STD_ON == ETH_VIRTUALMAC_SUPPORT) && (STD_ON == ETH_VIRTUALMAC_DISASSOCIATEIPV4MACADDR_API))
/*
 * Design:
 * Requirements:
 */
FUNC(Std_ReturnType, ETH_CODE)
Eth_DispatchVirtmacDisassociateIPv4Macaddr(uint8 CtrlIdx, uint8 *ipv4Address)
{
    Std_ReturnType retVal = E_OK;

#if (STD_ON == ETH_DEV_ERROR_DETECT)
    retVal = Eth_checkVirtualmacErrors(CtrlIdx, ETH_SID_DISPATCH_VIRTMAC_IPV4_MACADDR_ASSOCIATE);
#endif

    if (E_OK == retVal)
    {
        retVal = Eth_RpcSendIPv4UnregisterMacReq(&gEthDrv, ipv4Address);
    }
    return retVal;
}
#endif /* ((STD_ON == ETH_VIRTUALMAC_SUPPORT) && (STD_ON == ETH_VIRTUALMAC_DISASSOCIATEIPV4MACADDR_API)) */

#if ((STD_ON == ETH_VIRTUALMAC_SUPPORT) && (STD_ON == ETH_VIRTUALMAC_ADD_UNICAST_MACADDR_API))
/*
 * Design:
 * Requirements:
 */
FUNC(Std_ReturnType, ETH_CODE)
Eth_DispatchVirtmacAddUnicastAddr(uint8 CtrlIdx, uint8 *macAddress, Eth_PortType port , uint32 vlanId)
{
    Std_ReturnType retVal = E_OK;

#if (STD_ON == ETH_DEV_ERROR_DETECT)
    retVal = Eth_checkVirtualmacErrors(CtrlIdx, ETH_SID_DISPATCH_VIRTMAC_ADD_UNICAST_MACADDR);
#endif

    if (E_OK == retVal)
    {
        retVal = Eth_RpcSendAddUnicastAddr(&gEthDrv, macAddress, port, vlanId);
    }
    return retVal;
}
#endif /* ((STD_ON == ETH_VIRTUALMAC_SUPPORT) && (STD_ON == ETH_VIRTUALMAC_DISASSOCIATEIPV4MACADDR_API)) */

#if ((STD_ON == ETH_VIRTUALMAC_SUPPORT) && (STD_ON == ETH_VIRTUALMAC_ADD_MCAST_MACADDR_API))
/*
 * Design:
 * Requirements:
 */
FUNC(Std_ReturnType, ETH_CODE)
Eth_DispatchVirtmacAddMcastAddr(uint8 CtrlIdx, uint8 *macAddress, uint32 numLsbToIgnore, uint32 vlanId, Eth_PortListType *portList)
{
    Std_ReturnType retVal = E_OK;

#if (STD_ON == ETH_DEV_ERROR_DETECT)
    retVal = Eth_checkVirtualmacErrors(CtrlIdx, ETH_SID_DISPATCH_VIRTMAC_ADD_MCAST_MACADDR);
#endif

    if (E_OK == retVal)
    {
        retVal = Eth_RpcSendAddMulticastEntry(&gEthDrv, macAddress, vlanId, numLsbToIgnore, portList);
    }
    return retVal;
}
#endif /* ((STD_ON == ETH_VIRTUALMAC_SUPPORT) && (STD_ON == ETH_VIRTUALMAC_DISASSOCIATEIPV4MACADDR_API)) */

#if ((STD_ON == ETH_VIRTUALMAC_SUPPORT) && (STD_ON == ETH_VIRTUALMAC_DEL_MACADDR_API))
/*
 * Design:
 * Requirements:
 */
FUNC(Std_ReturnType, ETH_CODE)
Eth_DispatchVirtmacDelAddr(uint8 CtrlIdx, uint8 *macAddress, uint32 vlanId)
{
    Std_ReturnType retVal = E_OK;

#if (STD_ON == ETH_DEV_ERROR_DETECT)
    retVal = Eth_checkVirtualmacErrors(CtrlIdx, ETH_SID_DISPATCH_VIRTMAC_DEL_MACADDR);
#endif

    if (E_OK == retVal)
    {
        retVal = Eth_RpcSendDelAddrEntry(&gEthDrv, macAddress, vlanId);
    }
    return retVal;
}
#endif /* ((STD_ON == ETH_VIRTUALMAC_SUPPORT) && (STD_ON == ETH_VIRTUALMAC_DISASSOCIATEIPV4MACADDR_API)) */

#if ((STD_ON == ETH_VIRTUALMAC_SUPPORT) && (STD_ON == ETH_VIRTUALMAC_SEND_CUSTOM_NOTIFY_API))
/*
* Design:
* Requirements:
*/
FUNC(Std_ReturnType, ETH_CODE)
Eth_SendCustomNotify(uint8 CtrlIdx, void *notifyInfo, uint32 notifyLen)
{
    Std_ReturnType retVal = E_OK;

#if (STD_ON == ETH_DEV_ERROR_DETECT)
    retVal = Eth_checkVirtualmacErrors(CtrlIdx, ETH_SID_SEND_CUSTOM_NOTIFY);
#endif

    if (E_OK == retVal)
    {
        retVal = Eth_RpcSendCustomNotify(&gEthDrv, notifyInfo, notifyLen);
    }
    return retVal;
}
#endif /* ((STD_ON == ETH_VIRTUALMAC_SUPPORT) && (STD_ON == ETH_VIRTUALMAC_SEND_CUSTOM_NOTIFY_API)) */

#if ((STD_ON == ETH_VIRTUALMAC_SUPPORT) && (STD_ON == ETH_VIRTUALMAC_ADD_VLAN_API))
/*
 * Design:
 * Requirements:
 */
FUNC(Std_ReturnType, ETH_CODE)
Eth_DispatchVirtmacAddVlan(uint8 CtrlIdx, uint32 vlanId, Eth_PortListType *portList)
{
    Std_ReturnType retVal = E_OK;

#if (STD_ON == ETH_DEV_ERROR_DETECT)
    retVal = Eth_checkVirtualmacErrors(CtrlIdx, ETH_SID_DISPATCH_VIRTMAC_ADD_VLAN);
#endif

    if (E_OK == retVal)
    {
        retVal = Eth_RpcSendAddVlan(&gEthDrv, vlanId, portList);
    }
    return retVal;
}
#endif /* ((STD_ON == ETH_VIRTUALMAC_SUPPORT) && (STD_ON == ETH_VIRTUALMAC_ADD_VLAN_API)) */

#if ((STD_ON == ETH_VIRTUALMAC_SUPPORT) && (STD_ON == ETH_VIRTUALMAC_DEL_VLAN_API))
/*
 * Design:
 * Requirements:
 */
FUNC(Std_ReturnType, ETH_CODE)
Eth_DispatchVirtmacDelVlan(uint8 CtrlIdx, uint32 vlanId)
{
    Std_ReturnType retVal = E_OK;

#if (STD_ON == ETH_DEV_ERROR_DETECT)
    retVal = Eth_checkVirtualmacErrors(CtrlIdx, ETH_SID_DISPATCH_VIRTMAC_DEL_VLAN);
#endif

    if (E_OK == retVal)
    {
        retVal = Eth_RpcSendDelVlan(&gEthDrv, vlanId);
    }
    return retVal;
}
#endif /* ((STD_ON == ETH_VIRTUALMAC_SUPPORT) && (STD_ON == ETH_VIRTUALMAC_DEL_VLAN_API)) */

#if (STD_ON == ETH_USE_Q_APIS)
FUNC(Std_ReturnType, ETH_CODE)
Eth_RetrieveRxReadyQ(uint8 CtrlIdx,
                     Eth_PktQ *retrievePktQ)
{
    Std_ReturnType retVal = E_OK;

#if (STD_ON == ETH_DEV_ERROR_DETECT)
    retVal = Eth_checkRetrieveRxReadyQErrors(CtrlIdx, retrievePktQ);
#endif

    /* Receive packets from CPSW */
    if (E_OK == retVal)
    {
        EthPktQ_init(retrievePktQ);

        SchM_Enter_Eth_ETH_EXCLUSIVE_AREA_0();
        retVal = Eth_receivePktQ(&gEthDrv, CtrlIdx, retrievePktQ);
        SchM_Exit_Eth_ETH_EXCLUSIVE_AREA_0();
    }

    return retVal;
}
#endif /* (STD_ON == ETH_USE_Q_APIS) */

#if (STD_ON == ETH_USE_Q_APIS)
FUNC(Std_ReturnType, ETH_CODE)
Eth_SubmitRxFreeQ(uint8 CtrlIdx,
                  Eth_PktQ *submitPktQ)
{
    Std_ReturnType retVal = E_OK;

#if (STD_ON == ETH_DEV_ERROR_DETECT)
    retVal = Eth_checkSubmitRxFreeQErrors(CtrlIdx, submitPktQ);
#endif

    /* Receive packets from CPSW */
    if ( E_OK == retVal )
    {
        SchM_Enter_Eth_ETH_EXCLUSIVE_AREA_0();
        if (0U != EthPktQ_getCount(submitPktQ))
        {
            EthPktQ_append(&gEthDrv.rxFreeQ, submitPktQ);
            EthPktQ_init(submitPktQ);
        }
        SchM_Exit_Eth_ETH_EXCLUSIVE_AREA_0();
    }

    return retVal;
}
#endif /* (STD_ON == ETH_USE_Q_APIS) */

#if (STD_ON == ETH_USE_Q_APIS)
FUNC(Std_ReturnType, ETH_CODE)
Eth_SubmitTxReadyQ(uint8 CtrlIdx,
                   Eth_PktQ *submitPktQ)
{
    Std_ReturnType retVal = E_OK;

#if (STD_ON == ETH_DEV_ERROR_DETECT)
    retVal = Eth_checkSubmitTxReadyQErrors(CtrlIdx, submitPktQ);
#endif

    /* Receive packets from CPSW */
    if ( E_OK == retVal )
    {
        SchM_Enter_Eth_ETH_EXCLUSIVE_AREA_0();
        if (0U != EthPktQ_getCount(submitPktQ))
        {
            retVal = Eth_transmitPktQ(&gEthDrv, CtrlIdx, submitPktQ);
            EthPktQ_init(submitPktQ);
        }
        SchM_Exit_Eth_ETH_EXCLUSIVE_AREA_0();
    }

    return retVal;
}
#endif /* (STD_ON == ETH_USE_Q_APIS) */

#if (STD_ON == ETH_USE_Q_APIS)
FUNC(Std_ReturnType, ETH_CODE)
Eth_RetrieveTxDoneQ(uint8 CtrlIdx,
                    Eth_PktQ *retrievePktQ)
{
    Std_ReturnType retVal = E_OK;

#if (STD_ON == ETH_DEV_ERROR_DETECT)
    retVal = Eth_checkRetrieveTxDoneQErrors(CtrlIdx, retrievePktQ);
#endif

    /* Receive packets from CPSW */
    if ( E_OK == retVal )
    {
        EthPktQ_init(retrievePktQ);
        SchM_Enter_Eth_ETH_EXCLUSIVE_AREA_0();
        retVal = Eth_retrieveFreeTxPktQ(&gEthDrv, CtrlIdx, retrievePktQ);
        SchM_Exit_Eth_ETH_EXCLUSIVE_AREA_0();
    }

    return retVal;
}
#endif /* (STD_ON == ETH_USE_Q_APIS) */

/* ========================================================================== */
/*                           static functions                                 */
/* ========================================================================== */
static FUNC(Std_ReturnType, ETH_CODE)
Eth_ControllerInit(uint8 ctrlIdx)
{
    Std_ReturnType retVal = E_OK;
    Eth_ConfigType *cfg = &gEthDrv.ethConfig;
#if (STD_ON == ETH_USE_DEFAULT_MAC_ADDR)
    uint8 addr[ETH_MAC_ADDR_LEN];
#else
    uint8 addr[ETH_MAC_ADDR_LEN] = ETH_CTRL_PHY_ADDRESS;
#endif

    if (E_OK == retVal)
    {
        /* Requirements: MCAL-1542 */
        retVal = Eth_checkControllerAccess(&gEthDrv);
    }

    if (E_OK == retVal)
    {
#if (STD_ON == ETH_VIRTUALMAC_SUPPORT)
#if (STD_ON == ETH_DEV_ERROR_DETECT)
        if (gEthDrv.ethConfig.enableVirtualMac != TRUE)
        {
            Eth_reportDetError(ETH_SID_DISPATCH_VIRTMAC_DEL_VLAN, ETH_E_INV_PARAM);
        }
#endif
#endif
    }

    if (E_OK == retVal)
    {
        if (gEthDrv.ethConfig.enableVirtualMac == TRUE)
        {
            /* Check if Eth_DispatchVirtmacInit() completed before Eth_ControllerInit() */
            if (gEthDrv.virtualMacInfo.cpswAttachInfo.id == 0)
            {
                retVal = E_NOT_OK;
            }
        }
    }
    /* Read transceiver MAC address and update configuration params */
    if (E_OK == retVal)
    {
#if (STD_ON == ETH_USE_DEFAULT_MAC_ADDR)
        Eth_readMacAddr(&gEthDrv, addr);
#endif
        memcpy(gEthDrv.macAddr, addr, ETH_MAC_ADDR_LEN);
    }

    /* Initialize CPSW and the TX/RX packet queues */
    if (E_OK == retVal)
    {
        Eth_initEthPktQs(&gEthDrv);
        retVal = Eth_openCpsw(&gEthDrv, cfg);
    }

    /* Set non-promiscuous mode by default */
    if (E_OK == retVal)
    {
        gEthDrv.promiscuousMode = FALSE;
        retVal = Eth_setNonPromiscuousMode(&gEthDrv, addr);
        if (E_OK != retVal)
        {
            Eth_closeCpsw(&gEthDrv);
        }
    }

    /* Set MAC address */
    if (E_OK == retVal)
    {
        retVal = Eth_setMacAddr(&gEthDrv, addr);
    }

    return retVal;
}

static FUNC(Std_ReturnType, ETH_CODE)
Eth_checkSetControllerModeErrors(uint8 ctrlIdx,
                                 Eth_ModeType ctrlMode)
{
    Std_ReturnType retVal = E_NOT_OK;

    if (ETH_STATE_INIT != gEthDrvStatus)
    {
        /* Requirements: MCAL-1546 */
#if (STD_ON == ETH_DEV_ERROR_DETECT)
        Eth_reportDetError(ETH_SID_SET_CONTROLLER_MODE, ETH_E_UNINIT);
#endif
    }
    else if (ETH_CONTROLLER_ID_0 != ctrlIdx)
    {
        /* Requirements: MCAL-1547 */
#if (STD_ON == ETH_DEV_ERROR_DETECT)
        Eth_reportDetError(ETH_SID_SET_CONTROLLER_MODE, ETH_E_INV_CTRL_IDX);
#endif
    }
    else if ((gEthDrv.ethConfig.enableVirtualMac == TRUE) &&
             (gEthDrv.virtualMacInfo.cpswAttachInfo.id == 0))
    {
#if (STD_ON == ETH_DEV_ERROR_DETECT)
        /* Attach Info not populated. Eth_DispatchVirtmacInit not invoked or
         * RESPONSE not yet received */
        Eth_reportDetError(ETH_SID_SET_CONTROLLER_MODE, ETH_E_UNINIT);
#endif
    }
    else
    {
        retVal = E_OK;
    }

    return retVal;
}

static FUNC(Std_ReturnType, ETH_CODE)
Eth_checkGetControllerModeErrors(uint8 ctrlIdx,
                                 Eth_ModeType *ctrlModePtr)
{
    Std_ReturnType retVal = E_NOT_OK;

    if (ETH_STATE_INIT != gEthDrvStatus)
    {
        /* Requirements: MCAL-1551 */
#if (STD_ON == ETH_DEV_ERROR_DETECT)
        Eth_reportDetError(ETH_SID_GET_CONTROLLER_MODE, ETH_E_UNINIT);
#endif
    }
    else if (ETH_CONTROLLER_ID_0 != ctrlIdx)
    {
        /* Requirements: MCAL-1552 */
#if (STD_ON == ETH_DEV_ERROR_DETECT)
        Eth_reportDetError(ETH_SID_GET_CONTROLLER_MODE, ETH_E_INV_CTRL_IDX);
#endif
    }
    else if (NULL_PTR == ctrlModePtr)
    {
        /* Requirements: MCAL-1553 */
#if (STD_ON == ETH_DEV_ERROR_DETECT)
        Eth_reportDetError(ETH_SID_GET_CONTROLLER_MODE, ETH_E_PARAM_POINTER);
#endif
    }
    else
    {
        retVal = E_OK;
    }

    return retVal;
}

#if (STD_OFF == ETH_USE_Q_APIS)
static FUNC(Std_ReturnType, ETH_CODE)
Eth_checkTransmitErrors(uint8 ctrlIdx,
                        Eth_BufIdxType bufIdx,
                        uint8 *physAddrPtr)
{
    Std_ReturnType retVal = E_NOT_OK;

    if (ETH_STATE_INIT != gEthDrvStatus)
    {
        /* Requirements: MCAL-1587 */
#if (STD_ON == ETH_DEV_ERROR_DETECT)
        Eth_reportDetError(ETH_SID_TRANSMIT, ETH_E_UNINIT);
#endif
    }
    else if (ETH_CONTROLLER_ID_0 != ctrlIdx)
    {
        /* Requirements: MCAL-1588 */
#if (STD_ON == ETH_DEV_ERROR_DETECT)
        Eth_reportDetError(ETH_SID_TRANSMIT, ETH_E_INV_CTRL_IDX);
#endif
    }
    else if (ETH_NUM_TX_BUFFERS <= bufIdx)
    {
        /* Requirements: MCAL-1589 */
#if (STD_ON == ETH_DEV_ERROR_DETECT)
       Eth_reportDetError(ETH_SID_TRANSMIT, ETH_E_INV_PARAM);
#endif
    }
    else if (NULL_PTR == physAddrPtr)
    {
        /* Requirements: MCAL-1590 */
#if (STD_ON == ETH_DEV_ERROR_DETECT)
        Eth_reportDetError(ETH_SID_TRANSMIT, ETH_E_PARAM_POINTER);
#endif
    }
    else if (ETH_MODE_ACTIVE != gEthDrv.ctrlMode)
    {
        /* Requirements: MCAL-1621 */
#if (STD_ON == ETH_DEV_ERROR_DETECT)
        Eth_reportDetError(ETH_SID_TRANSMIT, ETH_E_INV_MODE);
#endif
    }
    else
    {
        retVal = E_OK;
    }

    return retVal;
}
#endif /* (STD_OFF == ETH_USE_Q_APIS)) */

#if (STD_ON == ETH_GET_COUNTER_VALUES_API)
static FUNC(Std_ReturnType, ETH_CODE)
Eth_checkGetCounterValuesErrors(uint8 ctrlIdx,
                            Eth_CounterType* CounterPtr)
{
    Std_ReturnType retVal = E_NOT_OK;

    if (ETH_STATE_INIT != gEthDrvStatus)
    {
        /* Requirements: MCAL-1713 */
#if (STD_ON == ETH_DEV_ERROR_DETECT)
        Eth_reportDetError(ETH_SID_GET_COUNTER_VALUES, ETH_E_UNINIT);
#endif
    }
    else if (ETH_CONTROLLER_ID_0 != ctrlIdx)
    {
        /* Requirements: MCAL-1714 */
#if (STD_ON == ETH_DEV_ERROR_DETECT)
        Eth_reportDetError(ETH_SID_GET_COUNTER_VALUES, ETH_E_INV_CTRL_IDX);
#endif
    }
    else if (NULL_PTR == CounterPtr)
    {
        /* Requirements: MCAL-1715 */
#if (STD_ON == ETH_DEV_ERROR_DETECT)
        Eth_reportDetError(ETH_SID_GET_COUNTER_VALUES, ETH_E_PARAM_POINTER);
#endif
    }
    else if (gEthDrv.ethConfig.enableVirtualMac == TRUE)
    {
        /* GetDropCount not supported in virtual mac mode.
         * The CPSW register space is restrictred to ethernet firmware.
         * Currently no RPC API supported to get stats from EthFw
         */
#if (STD_ON == ETH_DEV_ERROR_DETECT)
        Eth_reportDetError(ETH_SID_GET_COUNTER_VALUES, ETH_E_INV_PARAM);
#endif
    }
    else
    {
        retVal = E_OK;
    }

    return retVal;
}
#endif /* (STD_ON == ETH_GET_COUNTER_VALUES_API) */

#if (STD_ON == ETH_GET_RX_STATS_API)
static FUNC(Std_ReturnType, ETH_CODE)
Eth_checkGetRxStatsErrors(uint8 ctrlIdx,
                          Eth_RxStatsType *RxStats)
{
    Std_ReturnType retVal = E_NOT_OK;

    if (ETH_STATE_INIT != gEthDrvStatus)
    {
        /* Requirements: MCAL-1720 */
#if (STD_ON == ETH_DEV_ERROR_DETECT)
        Eth_reportDetError(ETH_SID_GET_RX_STATS, ETH_E_UNINIT);
#endif
    }
    else if (ETH_CONTROLLER_ID_0 != ctrlIdx)
    {
        /* Requirements: MCAL-1721 */
#if (STD_ON == ETH_DEV_ERROR_DETECT)
        Eth_reportDetError(ETH_SID_GET_RX_STATS, ETH_E_INV_CTRL_IDX);
#endif
    }
    else if (NULL_PTR == RxStats)
    {
        /* Requirements: MCAL-1722 */
#if (STD_ON == ETH_DEV_ERROR_DETECT)
        Eth_reportDetError(ETH_SID_GET_RX_STATS, ETH_E_PARAM_POINTER);
#endif
    }
    else if (gEthDrv.ethConfig.enableVirtualMac == TRUE)
    {
        /* GetEtherStats not supported in virtual mac mode.
         * The CPSW register space is restrictred to ethernet firmware.
         * Currently no RPC API supported to get stats from EthFw
         */
#if (STD_ON == ETH_DEV_ERROR_DETECT)
        Eth_reportDetError(ETH_SID_GET_RX_STATS, ETH_E_INV_PARAM);
#endif
    }
    else
    {
        retVal = E_OK;
    }

    return retVal;
}
#endif /* (STD_ON == ETH_GET_RX_STATS_API) */

#if (STD_ON == ETH_GET_TX_STATS_API)
static FUNC(Std_ReturnType, ETH_CODE)
Eth_checkGetTxStatsErrors(uint8 ctrlIdx,
                          Eth_TxStatsType *TxStats)
{
    Std_ReturnType retVal = E_NOT_OK;

    if (ETH_STATE_INIT != gEthDrvStatus)
    {
        /* Requirements: MCAL-4830 */
#if (STD_ON == ETH_DEV_ERROR_DETECT)
        Eth_reportDetError(ETH_SID_GET_TX_STATS, ETH_E_UNINIT);
#endif
    }
    else if (ETH_CONTROLLER_ID_0 != ctrlIdx)
    {
        /* Requirements: MCAL-4831 */
#if (STD_ON == ETH_DEV_ERROR_DETECT)
        Eth_reportDetError(ETH_SID_GET_TX_STATS, ETH_E_INV_CTRL_IDX);
#endif
    }
    else if (NULL_PTR == TxStats)
    {
        /* Requirements: MCAL-4832 */
#if (STD_ON == ETH_DEV_ERROR_DETECT)
        Eth_reportDetError(ETH_SID_GET_TX_STATS, ETH_E_PARAM_POINTER);
#endif
    }
    else if (gEthDrv.ethConfig.enableVirtualMac == TRUE)
    {
        /* GetEtherStats not supported in virtual mac mode.
         * The CPSW register space is restrictred to ethernet firmware.
         * Currently no RPC API supported to get stats from EthFw
         */
#if (STD_ON == ETH_DEV_ERROR_DETECT)
        Eth_reportDetError(ETH_SID_GET_TX_STATS, ETH_E_INV_PARAM);
#endif
    }
    else
    {
        retVal = E_OK;
    }

    return retVal;
}
#endif /* (STD_ON == ETH_GET_TX_STATS_API) */

#if (STD_ON == ETH_GET_TX_ERROR_COUNTERSVALUES_API)
static FUNC(Std_ReturnType, ETH_CODE)
Eth_checkGetTxErrorCounterValueErrors(uint8 ctrlIdx,
                          Eth_TxErrorCounterValuesType *TxErrorCounterValues)
{
    Std_ReturnType retVal = E_NOT_OK;

    if (ETH_STATE_INIT != gEthDrvStatus)
    {
        /* Requirements: MCAL-4834 */
#if (STD_ON == ETH_DEV_ERROR_DETECT)
        Eth_reportDetError(ETH_SID_GET_TXERROR_COUNTERVALUES, ETH_E_UNINIT);
#endif
    }
    else if (ETH_CONTROLLER_ID_0 != ctrlIdx)
    {
        /* Requirements: MCAL-4835 */
#if (STD_ON == ETH_DEV_ERROR_DETECT)
        Eth_reportDetError(ETH_SID_GET_TXERROR_COUNTERVALUES, ETH_E_INV_CTRL_IDX);
#endif
    }
    else if (NULL_PTR == TxErrorCounterValues)
    {
        /* Requirements: MCAL-4836 */
#if (STD_ON == ETH_DEV_ERROR_DETECT)
        Eth_reportDetError(ETH_SID_GET_TXERROR_COUNTERVALUES, ETH_E_PARAM_POINTER);
#endif
    }
    else if (gEthDrv.ethConfig.enableVirtualMac == TRUE)
    {
        /* GetEtherStats not supported in virtual mac mode.
         * The CPSW register space is restrictred to ethernet firmware.
         * Currently no RPC API supported to get stats from EthFw
         */
#if (STD_ON == ETH_DEV_ERROR_DETECT)
        Eth_reportDetError(ETH_SID_GET_TXERROR_COUNTERVALUES, ETH_E_INV_PARAM);
#endif
    }
    else
    {
        retVal = E_OK;
    }

    return retVal;
}
#endif

#if (STD_ON == ETH_DEV_ERROR_DETECT)

static FUNC(Std_ReturnType, ETH_CODE)
Eth_checkGetPhysAddrErrors(uint8 ctrlIdx,
                           uint8 *physAddrPtr)
{
    Std_ReturnType retVal = E_NOT_OK;

    if (ETH_STATE_INIT != gEthDrvStatus)
    {
        /* Requirements: MCAL-1557 */
        Eth_reportDetError(ETH_SID_GET_PHYS_ADDR, ETH_E_UNINIT);
    }
    else if (ETH_CONTROLLER_ID_0 != ctrlIdx)
    {
        /* Requirements: MCAL-1558 */
        Eth_reportDetError(ETH_SID_GET_PHYS_ADDR, ETH_E_INV_CTRL_IDX);
    }
    else if (NULL_PTR == physAddrPtr)
    {
        /* Requirements: MCAL-1559 */
        Eth_reportDetError(ETH_SID_GET_PHYS_ADDR, ETH_E_PARAM_POINTER);
    }
    else
    {
        retVal = E_OK;
    }

    return retVal;
}

static FUNC(Std_ReturnType, ETH_CODE)
Eth_checkSetPhysAddrErrors(uint8 ctrlIdx,
                           const uint8 *physAddrPtr)
{
    Std_ReturnType retVal = E_NOT_OK;

    if (ETH_STATE_INIT != gEthDrvStatus)
    {
        /* Requirements: MCAL-1628 */
        Eth_reportDetError(ETH_SID_SET_PHYS_ADDR, ETH_E_UNINIT);
    }
    else if (ETH_CONTROLLER_ID_0 != ctrlIdx)
    {
        /* Requirements: MCAL-1629 */
        Eth_reportDetError(ETH_SID_SET_PHYS_ADDR, ETH_E_INV_CTRL_IDX);
    }
    else if (NULL_PTR == physAddrPtr)
    {
        /* Requirements: MCAL-1630 */
        Eth_reportDetError(ETH_SID_SET_PHYS_ADDR, ETH_E_PARAM_POINTER);
    }
    else
    {
        retVal = E_OK;
    }

    return retVal;
}

#if (STD_ON == ETH_UPDATE_PHYS_ADDR_FILTER_API)
static FUNC(Std_ReturnType, ETH_CODE)
Eth_checkUpdatePhysAddrFilterErrors(uint8 ctrlIdx,
                                    uint8 *physAddrPtr)
{
    Std_ReturnType retVal = E_NOT_OK;

    if (ETH_STATE_INIT != gEthDrvStatus)
    {
        /* Requirements: MCAL-1649*/
        Eth_reportDetError(ETH_SID_UPDATE_PHYS_ADDR_FILTER, ETH_E_UNINIT);
    }
    else if (ETH_CONTROLLER_ID_0 != ctrlIdx)
    {
        /* Requirements: MCAL-1650 */
        Eth_reportDetError(ETH_SID_UPDATE_PHYS_ADDR_FILTER, ETH_E_INV_CTRL_IDX);
    }
    else if (NULL_PTR == physAddrPtr)
    {
        /* Requirements: MCAL-1651 */
        Eth_reportDetError(ETH_SID_UPDATE_PHYS_ADDR_FILTER, ETH_E_PARAM_POINTER);
    }
    else if (gEthDrv.ethConfig.enableVirtualMac == TRUE)
    {
        /* Update PhyAddr Filter not supported in virtual mac mode.
         * Application must use Eth_DispatchVirtmacAddUnicastAddr()/
         * Eth_DispatchVirtmacDelAddr() APIs instead in virtual mac mode
         */
        Eth_reportDetError(ETH_SID_UPDATE_PHYS_ADDR_FILTER, ETH_E_INV_PARAM);
    }
    else
    {
        retVal = E_OK;
    }

    return retVal;
}
#endif /* (STD_ON == ETH_UPDATE_PHYS_ADDR_FILTER_API) */

#if (STD_ON == ETH_ENABLE_MII_API)
static FUNC(Std_ReturnType, ETH_CODE)
Eth_checkWriteMiiErrors(uint8 ctrlIdx,
                        uint8 trcvIdx,
                        uint8 regIdx,
                        uint16 regVal)
{
    Std_ReturnType retVal = E_NOT_OK;

    if (ETH_STATE_INIT != gEthDrvStatus)
    {
        /* Requirements: MCAL-1563 */
        Eth_reportDetError(ETH_SID_WRITE_MII, ETH_E_UNINIT);
    }
    else if (ETH_CONTROLLER_ID_0 != ctrlIdx)
    {
        /* Requirements: MCAL-1564 */
        Eth_reportDetError(ETH_SID_WRITE_MII, ETH_E_INV_CTRL_IDX);
    }
    else if (gEthDrv.ethConfig.enableVirtualMac == TRUE)
    {
        /* Write Mii not supported in virtual mac mode.
         * The PHY is managed by ethernet firmware and cannot
         * be controller from Eth Driver in virtual mac mode
         */
        Eth_reportDetError(ETH_SID_WRITE_MII, ETH_E_INV_PARAM);
    }
    else
    {
        retVal = E_OK;
    }

    return retVal;
}

static FUNC(Std_ReturnType, ETH_CODE)
Eth_checkReadMiiErrors(uint8 ctrlIdx,
                       uint8 trcvIdx,
                       uint8 regIdx,
                       uint16 *regValPtr)
{
    Std_ReturnType retVal = E_NOT_OK;

    if (ETH_STATE_INIT != gEthDrvStatus)
    {
        /* Requirements: MCAL-1569 */
        Eth_reportDetError(ETH_SID_READ_MII, ETH_E_UNINIT);
    }
    else if (ETH_CONTROLLER_ID_0 != ctrlIdx)
    {
        /* Requirements: MCAL-1570 */
        Eth_reportDetError(ETH_SID_READ_MII, ETH_E_INV_CTRL_IDX);
    }
    else if (NULL_PTR == regValPtr)
    {
        /* Requirements: MCAL-1571 */
        Eth_reportDetError(ETH_SID_READ_MII, ETH_E_PARAM_POINTER);
    }
    else if (gEthDrv.ethConfig.enableVirtualMac == TRUE)
    {
        /* Read Mii not supported in virtual mac mode.
         * The PHY is managed by ethernet firmware and cannot
         * be controller from Eth Driver in virtual mac mode
         */
        Eth_reportDetError(ETH_SID_READ_MII, ETH_E_INV_PARAM);
    }
    else
    {
        retVal = E_OK;
    }

    return retVal;
}
#endif /* (STD_ON == ETH_ENABLE_MII_API) */

#if (STD_OFF == ETH_USE_Q_APIS)
static FUNC(BufReq_ReturnType, ETH_CODE)
Eth_checkProvideTxBufferErrors(uint8 ctrlIdx,
                               Eth_BufIdxType *bufIdxPtr,
                               uint8 **bufPtr,
                               uint16 *lenBytePtr)
{
    BufReq_ReturnType retVal = BUFREQ_E_NOT_OK;

    if (ETH_STATE_INIT != gEthDrvStatus)
    {
        /* Requirements: MCAL-1578 */
        Eth_reportDetError(ETH_SID_PROVIDE_TX_BUFFER, ETH_E_UNINIT);
    }
    else if (ETH_CONTROLLER_ID_0 != ctrlIdx)
    {
        /* Requirements: MCAL-1579 */
        Eth_reportDetError(ETH_SID_PROVIDE_TX_BUFFER, ETH_E_INV_CTRL_IDX);
    }
    else if (NULL_PTR == bufIdxPtr)
    {
        /* Requirements: MCAL-1580 */
        Eth_reportDetError(ETH_SID_PROVIDE_TX_BUFFER, ETH_E_PARAM_POINTER);
    }
    else if (NULL_PTR == bufPtr)
    {
        /* Requirements: MCAL-1581 */
        Eth_reportDetError(ETH_SID_PROVIDE_TX_BUFFER, ETH_E_PARAM_POINTER);
    }
    else if (NULL_PTR == lenBytePtr)
    {
        /* Requirements: MCAL-1582 */
        Eth_reportDetError(ETH_SID_PROVIDE_TX_BUFFER, ETH_E_PARAM_POINTER);
    }
    else
    {
        retVal = BUFREQ_OK;
    }

    return retVal;
}
#endif /* (STD_OFF == ETH_USE_Q_APIS) */

#if (STD_OFF == ETH_USE_Q_APIS)
static FUNC(Std_ReturnType, ETH_CODE)
Eth_checkReceiveErrors(uint8 ctrlIdx,
                       Eth_RxStatusType *rxStatusPtr)
{
    Std_ReturnType retVal = E_NOT_OK;

    if (ETH_STATE_INIT != gEthDrvStatus)
    {
        /* Requirements: MCAL-1594 */
        Eth_reportDetError(ETH_SID_RECEIVE, ETH_E_UNINIT);
    }
    else if (ETH_CONTROLLER_ID_0 != ctrlIdx)
    {
        /* Requirements: MCAL-1595 */
        Eth_reportDetError(ETH_SID_RECEIVE, ETH_E_INV_CTRL_IDX);
    }
    else if (ETH_MODE_ACTIVE != gEthDrv.ctrlMode)
    {
        /* Requirements: MCAL-1622 */
        Eth_reportDetError(ETH_SID_RECEIVE, ETH_E_INV_MODE);
    }
    else if (NULL_PTR == rxStatusPtr)
    {
        Eth_reportDetError(ETH_SID_RECEIVE, ETH_E_PARAM_POINTER);
    }
    else
    {
        retVal = E_OK;
    }

    return retVal;
}
#endif /* (STD_OFF == ETH_USE_Q_APIS) */

static FUNC(Std_ReturnType, ETH_CODE)
Eth_checkTxConfirmationErrors(uint8 ctrlIdx)
{
    Std_ReturnType retVal = E_NOT_OK;

    if (ETH_STATE_INIT != gEthDrvStatus)
    {
        /* Requirements: MCAL-1600 */
        Eth_reportDetError(ETH_SID_TX_CONFIRMATION, ETH_E_UNINIT);
    }
    else if (ETH_CONTROLLER_ID_0 != ctrlIdx)
    {
        /* Requirements: MCAL-1601 */
        Eth_reportDetError(ETH_SID_TX_CONFIRMATION, ETH_E_INV_CTRL_IDX);
    }
    else if (ETH_MODE_ACTIVE != gEthDrv.ctrlMode)
    {
        /* Requirements: MCAL-1623 */
        Eth_reportDetError(ETH_SID_TX_CONFIRMATION, ETH_E_INV_MODE);
    }
    else
    {
        retVal = E_OK;
    }

    return retVal;
}

#if (STD_ON == ETH_GLOBALTIMESUPPORT_API)
static FUNC(Std_ReturnType, ETH_CODE)
Eth_checkGetCurrentTimeErrors(uint8 ctrlIdx,
                              Eth_TimeStampQualType *timeQualPtr,
                              Eth_TimeStampType *timeStampPtr)
{
    Std_ReturnType retVal = E_NOT_OK;

    if (ETH_STATE_INIT != gEthDrvStatus)
    {
        /* Requirements: MCAL-1667 */
        Eth_reportDetError(ETH_SID_GET_CURRENT_TIME, ETH_E_UNINIT);
    }
    else if (ETH_CONTROLLER_ID_0 != ctrlIdx)
    {
        /* Requirements: MCAL-1668 */
        Eth_reportDetError(ETH_SID_GET_CURRENT_TIME, ETH_E_INV_CTRL_IDX);
    }
    else if ((NULL_PTR == timeQualPtr) || (NULL_PTR == timeStampPtr))
    {
        /* Requirements: MCAL-1669 */
        Eth_reportDetError(ETH_SID_GET_CURRENT_TIME, ETH_E_PARAM_POINTER);
    }
    else
    {
        retVal = E_OK;
    }

    return retVal;
}
#endif /* (STD_ON == ETH_GLOBALTIMESUPPORT_API) */

#if (STD_ON == ETH_GLOBALTIMESUPPORT_API)
static FUNC(Std_ReturnType, ETH_CODE)
Eth_checkEnableEgressTimeStampErrors(uint8 ctrlIdx,
                                     uint8 bufIdx)
{
    Std_ReturnType retVal = E_NOT_OK;

    if (ETH_STATE_INIT != gEthDrvStatus)
    {
        /* Requirements: MCAL-1672 */
        Eth_reportDetError(ETH_SID_ENABLE_EGRESS_TIMESTAMP, ETH_E_UNINIT);
    }
    else if (ETH_CONTROLLER_ID_0 != ctrlIdx)
    {
        /* Requirements: MCAL-1673 */
        Eth_reportDetError(ETH_SID_ENABLE_EGRESS_TIMESTAMP, ETH_E_INV_CTRL_IDX);
    }
    else
    {
        retVal = E_OK;
    }

    return retVal;
}
#endif /* (STD_ON == ETH_GLOBALTIMESUPPORT_API) */

#if (STD_ON == ETH_GLOBALTIMESUPPORT_API)
static FUNC(Std_ReturnType, ETH_CODE)
Eth_checkGetEgressTimeStampErrors(uint8 ctrlIdx,
                                  Eth_TimeStampQualType *timeQualPtr,
                                  Eth_TimeStampType *timeStampPtr)
{
    Std_ReturnType retVal = E_NOT_OK;

    if (ETH_STATE_INIT != gEthDrvStatus)
    {
        /* Requirements: MCAL-1676 */
        Eth_reportDetError(ETH_SID_GET_EGRESS_TIMESTAMP, ETH_E_UNINIT);
    }
    else if (ETH_CONTROLLER_ID_0 != ctrlIdx)
    {
        /* Requirements: MCAL-1677 */
        Eth_reportDetError(ETH_SID_GET_EGRESS_TIMESTAMP, ETH_E_INV_CTRL_IDX);
    }
    else if ((NULL_PTR == timeQualPtr) || (NULL_PTR == timeStampPtr))
    {
        /* Requirements: MCAL-1678 */
        Eth_reportDetError(ETH_SID_GET_EGRESS_TIMESTAMP, ETH_E_PARAM_POINTER);
    }
    else
    {
        retVal = E_OK;
    }

    return retVal;
}
#endif /* (STD_ON == ETH_GLOBALTIMESUPPORT_API) */

#if (STD_ON == ETH_GLOBALTIMESUPPORT_API)
static FUNC(Std_ReturnType, ETH_CODE)
Eth_checkGetIngressTimeStampErrors(uint8 ctrlIdx,
                                   Eth_DataType *dataPtr,
                                   Eth_TimeStampQualType *timeQualPtr,
                                   Eth_TimeStampType *timeStampPtr)
{
    Std_ReturnType retVal = E_NOT_OK;

    if (ETH_STATE_INIT != gEthDrvStatus)
    {
        /* Requirements: MCAL-1681 */
        Eth_reportDetError(ETH_SID_GET_INGRESS_TIMESTAMP, ETH_E_UNINIT);
    }
    else if (ETH_CONTROLLER_ID_0 != ctrlIdx)
    {
        /* Requirements: MCAL-1682 */
        Eth_reportDetError(ETH_SID_GET_INGRESS_TIMESTAMP, ETH_E_INV_CTRL_IDX);
    }
    else if ((NULL_PTR == timeQualPtr) ||
             (NULL_PTR == timeStampPtr) ||
             (NULL_PTR == timeStampPtr))
    {
        /* Requirements: MCAL-1683 */
        Eth_reportDetError(ETH_SID_GET_INGRESS_TIMESTAMP, ETH_E_PARAM_POINTER);
    }
    else
    {
        retVal = E_OK;
    }

    return retVal;
}
#endif /* (STD_ON == ETH_GLOBALTIMESUPPORT_API) */

#if (STD_ON == ETH_GLOBALTIMESUPPORT_API)
static FUNC(Std_ReturnType, ETH_CODE)
Eth_checkSetCorrectionTimeErrors(uint8 ctrlIdx,
                                 Eth_TimeIntDiffType *timeOffsetPtr,
                                 Eth_RateRatioType *rateRatioPtr)
{
    Std_ReturnType retVal = E_NOT_OK;

    if (ETH_STATE_INIT != gEthDrvStatus)
    {
        /* Requirements: MCAL-1686 */
        Eth_reportDetError(ETH_SID_SET_CORRECTION_TIME, ETH_E_UNINIT);
    }
    else if (ETH_CONTROLLER_ID_0 != ctrlIdx)
    {
        /* Requirements: MCAL-1687 */
        Eth_reportDetError(ETH_SID_SET_CORRECTION_TIME, ETH_E_INV_CTRL_IDX);
    }
    else if ((NULL_PTR == timeOffsetPtr) || (NULL_PTR == rateRatioPtr))
    {
        /* Requirements: MCAL-1688 */
        Eth_reportDetError(ETH_SID_SET_CORRECTION_TIME, ETH_E_PARAM_POINTER);
    }
    else
    {
        retVal = E_OK;
    }

    return retVal;
}
#endif /* (STD_ON == ETH_GLOBALTIMESUPPORT_API) */

#if (STD_ON == ETH_GLOBALTIMESUPPORT_API)
static FUNC(Std_ReturnType, ETH_CODE)
Eth_checkSetGlobalTimeErrors(uint8 ctrlIdx,
                             Eth_TimeStampType *timeStampPtr)
{
    Std_ReturnType retVal = E_NOT_OK;

    if (ETH_STATE_INIT != gEthDrvStatus)
    {
        /* Requirements: MCAL-1691 */
        Eth_reportDetError(ETH_SID_SET_GLOBAL_TIME, ETH_E_UNINIT);
    }
    else if (ETH_CONTROLLER_ID_0 != ctrlIdx)
    {
        /* Requirements: MCAL-1692 */
        Eth_reportDetError(ETH_SID_SET_GLOBAL_TIME, ETH_E_INV_CTRL_IDX);
    }
    else if (NULL_PTR == timeStampPtr)
    {
        /* Requirements: MCAL-1693 */
        Eth_reportDetError(ETH_SID_SET_GLOBAL_TIME, ETH_E_PARAM_POINTER);
    }
    else
    {
        retVal = E_OK;
    }

    return retVal;
}
#endif /* (STD_ON == ETH_GLOBALTIMESUPPORT_API) */

#if (STD_ON == ETH_VIRTUALMAC_SUPPORT)
static FUNC(Std_ReturnType, ETH_CODE)
Eth_checkVirtualmacErrors(uint8 ctrlIdx, uint8 apiId)
{
    Std_ReturnType retVal = E_NOT_OK;

    if (ETH_STATE_INIT != gEthDrvStatus)
    {
        Eth_reportDetError(apiId, ETH_E_UNINIT);
    }
    else if (ETH_CONTROLLER_ID_0 != ctrlIdx)
    {
        Eth_reportDetError(apiId, ETH_E_INV_CTRL_IDX);
    }
    else if (gEthDrv.ethConfig.enableVirtualMac != TRUE)
    {
        Eth_reportDetError(apiId, ETH_E_INV_PARAM);
    }
    else
    {
        retVal = E_OK;
    }

    return retVal;
}
#endif /* (STD_ON == ETH_VIRTUALMAC_SUPPORT) */

#if ((STD_ON == ETH_ZERO_COPY_TX) && (STD_OFF == ETH_USE_Q_APIS))
static FUNC(Std_ReturnType, ETH_CODE)
Eth_checkTransmitNoCpyErrors(uint8 ctrlIdx,
                             const uint8 *BufPtr,
                             const uint8 *physAddrPtr)
{
    Std_ReturnType retVal = E_NOT_OK;

    if (ETH_STATE_INIT != gEthDrvStatus)
    {
        Eth_reportDetError(ETH_SID_TRANSMIT_NO_COPY, ETH_E_UNINIT);
    }
    else if (ETH_CONTROLLER_ID_0 != ctrlIdx)
    {
        Eth_reportDetError(ETH_SID_TRANSMIT_NO_COPY, ETH_E_INV_CTRL_IDX);
    }
    else if (ETH_MODE_ACTIVE != gEthDrv.ctrlMode)
    {
        Eth_reportDetError(ETH_SID_TRANSMIT_NO_COPY, ETH_E_INV_MODE);
    }
    else if (NULL_PTR == BufPtr)
    {
        Eth_reportDetError(ETH_SID_TRANSMIT_NO_COPY, ETH_E_INV_PARAM);
    }
    else if (NULL_PTR == physAddrPtr)
    {
        Eth_reportDetError(ETH_SID_TRANSMIT_NO_COPY, ETH_E_PARAM_POINTER);
    }
    else
    {
        retVal = E_OK;
    }

    return retVal;
}
#endif /* (STD_ON == ETH_ZERO_COPY_TX) && (STD_OFF == ETH_USE_Q_APIS)) */

#if (STD_ON == ETH_USE_Q_APIS)
static FUNC(Std_ReturnType, ETH_CODE)
Eth_checkRetrieveRxReadyQErrors(uint8 ctrlIdx,
                                const Eth_PktQ *pktQ)
{
    Std_ReturnType retVal = E_NOT_OK;

    if (ETH_STATE_INIT != gEthDrvStatus)
    {
        Eth_reportDetError(ETH_SID_RETRIEVE_RX_READY_QUEUE, ETH_E_UNINIT);
    }
    else if (ETH_CONTROLLER_ID_0 != ctrlIdx)
    {
        Eth_reportDetError(ETH_SID_RETRIEVE_RX_READY_QUEUE, ETH_E_INV_CTRL_IDX);
    }
    else if (ETH_MODE_ACTIVE != gEthDrv.ctrlMode)
    {
        Eth_reportDetError(ETH_SID_RETRIEVE_RX_READY_QUEUE, ETH_E_INV_MODE);
    }
    else if (NULL_PTR == pktQ)
    {
        Eth_reportDetError(ETH_SID_RETRIEVE_RX_READY_QUEUE, ETH_E_PARAM_POINTER);
    }
    else
    {
        retVal = E_OK;
    }

    return retVal;
}
#endif /* (STD_ON == ETH_USE_Q_APIS) */

#if (STD_ON == ETH_USE_Q_APIS)
static FUNC(Std_ReturnType, ETH_CODE)
Eth_checkSubmitRxFreeQErrors(uint8 ctrlIdx,
                             const Eth_PktQ *pktQ)
{
    Std_ReturnType retVal = E_NOT_OK;

    if (ETH_STATE_INIT != gEthDrvStatus)
    {
        Eth_reportDetError(ETH_SID_SUBMIT_RX_FREE_QUEUE, ETH_E_UNINIT);
    }
    else if (ETH_CONTROLLER_ID_0 != ctrlIdx)
    {
        Eth_reportDetError(ETH_SID_SUBMIT_RX_FREE_QUEUE, ETH_E_INV_CTRL_IDX);
    }
    else if (ETH_MODE_ACTIVE != gEthDrv.ctrlMode)
    {
        Eth_reportDetError(ETH_SID_SUBMIT_RX_FREE_QUEUE, ETH_E_INV_MODE);
    }
    else if (NULL_PTR == pktQ)
    {
        Eth_reportDetError(ETH_SID_SUBMIT_RX_FREE_QUEUE, ETH_E_PARAM_POINTER);
    }
    else
    {
        retVal = E_OK;
    }

    return retVal;
}
#endif /* (STD_ON == ETH_USE_Q_APIS) */

#if (STD_ON == ETH_USE_Q_APIS)
static FUNC(Std_ReturnType, ETH_CODE)
Eth_checkSubmitTxReadyQErrors(uint8 ctrlIdx,
                              const Eth_PktQ *pktQ)
{
    Std_ReturnType retVal = E_NOT_OK;

    if (ETH_STATE_INIT != gEthDrvStatus)
    {
        Eth_reportDetError(ETH_SID_SUBMIT_TX_READY_QUEUE, ETH_E_UNINIT);
    }
    else if (ETH_CONTROLLER_ID_0 != ctrlIdx)
    {
        Eth_reportDetError(ETH_SID_SUBMIT_TX_READY_QUEUE, ETH_E_INV_CTRL_IDX);
    }
    else if (ETH_MODE_ACTIVE != gEthDrv.ctrlMode)
    {
        Eth_reportDetError(ETH_SID_SUBMIT_TX_READY_QUEUE, ETH_E_INV_MODE);
    }
    else if (NULL_PTR == pktQ)
    {
        Eth_reportDetError(ETH_SID_SUBMIT_TX_READY_QUEUE, ETH_E_PARAM_POINTER);
    }
    else
    {
        retVal = E_OK;
    }

    return retVal;
}
#endif /* (STD_ON == ETH_USE_Q_APIS) */

#if (STD_ON == ETH_USE_Q_APIS)
static FUNC(Std_ReturnType, ETH_CODE)
Eth_checkRetrieveTxDoneQErrors(uint8 ctrlIdx,
                               const Eth_PktQ *pktQ)
{
    Std_ReturnType retVal = E_NOT_OK;

    if (ETH_STATE_INIT != gEthDrvStatus)
    {
        Eth_reportDetError(ETH_SID_RETRIEVE_TX_DONE_QUEUE, ETH_E_UNINIT);
    }
    else if (ETH_CONTROLLER_ID_0 != ctrlIdx)
    {
        Eth_reportDetError(ETH_SID_RETRIEVE_TX_DONE_QUEUE, ETH_E_INV_CTRL_IDX);
    }
    else if (ETH_MODE_ACTIVE != gEthDrv.ctrlMode)
    {
        Eth_reportDetError(ETH_SID_RETRIEVE_TX_DONE_QUEUE, ETH_E_INV_MODE);
    }
    else if (NULL_PTR == pktQ)
    {
        Eth_reportDetError(ETH_SID_RETRIEVE_TX_DONE_QUEUE, ETH_E_PARAM_POINTER);
    }
    else
    {
        retVal = E_OK;
    }

    return retVal;
}
#endif /* (STD_ON == ETH_USE_Q_APIS) */

#endif /* (STD_ON == ETH_DEV_ERROR_DETECT) */

#define ETH_STOP_SEC_CODE
#include <Eth_MemMap.h>
