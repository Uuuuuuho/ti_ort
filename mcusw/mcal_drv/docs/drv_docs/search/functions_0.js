var searchData=
[
  ['adc_5fdeinit',['Adc_DeInit',['../group__MCAL__ADC__API.html#ga495a6740fcf5bfb843553a001b89fc48',1,'Adc.h']]],
  ['adc_5fdisablegroupnotification',['Adc_DisableGroupNotification',['../group__MCAL__ADC__API.html#gaf301b72cde728d6aab8d9e69104c4635',1,'Adc.h']]],
  ['adc_5fenablegroupnotification',['Adc_EnableGroupNotification',['../group__MCAL__ADC__API.html#ga8b6ea0906f6e9df8551f38dcd2ccc070',1,'Adc.h']]],
  ['adc_5fgetgroupstatus',['Adc_GetGroupStatus',['../group__MCAL__ADC__API.html#gadd00c0c2504cdfddb17eeef2cb4d53cc',1,'Adc.h']]],
  ['adc_5fgetstreamlastpointer',['Adc_GetStreamLastPointer',['../group__MCAL__ADC__API.html#ga3d21acf4ec46a7856eae369aa678c8f8',1,'Adc.h']]],
  ['adc_5fgetversioninfo',['Adc_GetVersionInfo',['../group__MCAL__ADC__API.html#gab178b3971aab4312075dfc2e8b99cd35',1,'Adc.h']]],
  ['adc_5finit',['Adc_Init',['../group__MCAL__ADC__API.html#ga0199f2c19ca665c99cd57c6358a73a2e',1,'Adc.h']]],
  ['adc_5freadgroup',['Adc_ReadGroup',['../group__MCAL__ADC__API.html#gaf7c4b6c47a5f19e501cd7b9635035124',1,'Adc.h']]],
  ['adc_5fregisterreadback',['Adc_RegisterReadback',['../group__MCAL__ADC__API.html#ga4c2c89317305d2311ddad521e977df66',1,'Adc.h']]],
  ['adc_5fsetupresultbuffer',['Adc_SetupResultBuffer',['../group__MCAL__ADC__API.html#gad431cd0ebd312a4d19d1720b0e5905e1',1,'Adc.h']]],
  ['adc_5fstartgroupconversion',['Adc_StartGroupConversion',['../group__MCAL__ADC__API.html#ga5a4a3e6c3e376dbe45f8229585f66948',1,'Adc.h']]],
  ['adc_5fstopgroupconversion',['Adc_StopGroupConversion',['../group__MCAL__ADC__API.html#ga99dbccc596a0b4823da21b8a0ee348a1',1,'Adc.h']]]
];
