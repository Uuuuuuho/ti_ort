var searchData=
[
  ['data_5fline_5f0_5freception',['DATA_LINE_0_RECEPTION',['../group__MCAL__SPI__CFG.html#gga624a4e0f83450998d6abcea3d95b970fa27e9466ba8e0826443e4078f8378463c',1,'Spi.h']]],
  ['data_5fline_5f0_5ftransmission',['DATA_LINE_0_TRANSMISSION',['../group__MCAL__SPI__CFG.html#gga6dd347ac30bb8e5dc18ed12af800a67fac2fe58ef1b06ce4d0b423c7690210ed6',1,'Spi.h']]],
  ['data_5fline_5f1_5freception',['DATA_LINE_1_RECEPTION',['../group__MCAL__SPI__CFG.html#gga624a4e0f83450998d6abcea3d95b970fa21f5d52e1ef7d1776c2d57ef736f632d',1,'Spi.h']]],
  ['data_5fline_5f1_5ftransmission',['DATA_LINE_1_TRANSMISSION',['../group__MCAL__SPI__CFG.html#gga6dd347ac30bb8e5dc18ed12af800a67fa4d319bfd651e0ff249eb64fbecba58f9',1,'Spi.h']]],
  ['data_5fline_5fboth_5ftransmission',['DATA_LINE_BOTH_TRANSMISSION',['../group__MCAL__SPI__CFG.html#gga6dd347ac30bb8e5dc18ed12af800a67fa1475322dc46879252c0e5aebe7952a7f',1,'Spi.h']]],
  ['data_5fline_5fno_5ftransmission',['DATA_LINE_NO_TRANSMISSION',['../group__MCAL__SPI__CFG.html#gga6dd347ac30bb8e5dc18ed12af800a67fad553dfeeaafd3df6f93aca48b8395c3d',1,'Spi.h']]]
];
