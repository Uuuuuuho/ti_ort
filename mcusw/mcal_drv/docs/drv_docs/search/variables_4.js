var searchData=
[
  ['enabledmamode',['enabledmaMode',['../structSpi__HwUnitConfigType.html#a5a3a04f6acf2a14d813144c84b42535c',1,'Spi_HwUnitConfigType']]],
  ['enablehr',['enableHR',['../structPwm__ChannelConfigType.html#a163044778b8ff8b91ec233ac1b227ca9',1,'Pwm_ChannelConfigType']]],
  ['enablewakeupflag',['enableWakeupFlag',['../structGpt__ChannelConfigType.html#a5ddb8c74325872645556490dc75f9b04',1,'Gpt_ChannelConfigType']]],
  ['ethconfig_5f0_5fpc',['EthConfig_0_PC',['../group__MCAL__ETH__CFG.html#gaadb14b6196a5457db8060267977eb2eb',1,'Eth_Cfg.h']]],
  ['ethtrcvconfig_5f0_5fpc',['EthTrcvConfig_0_PC',['../group__MCAL__ETHTRCV__CFG.html#gaf25be2aa436568602ca18fd520e18e19',1,'EthTrcv_Cfg.h']]],
  ['extdevcfg',['extDevCfg',['../structSpi__ConfigType.html#a0802c8d0042610e1de5526386395ee98',1,'Spi_ConfigType']]],
  ['externaldevicecfgid',['externalDeviceCfgId',['../structSpi__JobConfigType__PC.html#a7d4fc95741c6d2cc6a1480c98f5c191e',1,'Spi_JobConfigType_PC']]]
];
