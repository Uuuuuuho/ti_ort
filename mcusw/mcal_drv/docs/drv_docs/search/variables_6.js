var searchData=
[
  ['gpt_5fchannelconfig_5fpc',['Gpt_ChannelConfig_PC',['../group__MCAL__GPT__CFG.html#ga8a3ab2f916014ba3367da790ac488a31',1,'Gpt_Cfg.h']]],
  ['gptchannelconfigset',['GptChannelConfigSet',['../group__MCAL__GPT__CFG.html#ga9b3eb5f124328ff640b981480825e9f0',1,'Gpt_Cfg.h']]],
  ['gptrev',['gptRev',['../structGpt__RegisterReadbackType.html#a20bfcca5f93fbdf283a88abd4372f0a4',1,'Gpt_RegisterReadbackType']]],
  ['gpttimersynctrl',['gptTimerSynCtrl',['../structGpt__RegisterReadbackType.html#ab3117e016715cc658443fa0e21e34b30',1,'Gpt_RegisterReadbackType']]],
  ['gptttgr',['gptTtgr',['../structGpt__RegisterReadbackType.html#a4573cbdcf367ac2f5c98e836e1e3656f',1,'Gpt_RegisterReadbackType']]],
  ['groupcfg',['groupCfg',['../structAdc__ConfigType.html#ac5041b14669011e60ba51be1c945ec80',1,'Adc_ConfigType']]],
  ['groupid',['groupId',['../structAdc__GroupConfigType.html#ad41feeaeb71ce3d8a28434bb72fc8f65',1,'Adc_GroupConfigType::groupId()'],['../structAdc__GroupLogEntryType.html#a53c5abe4088401fcddacc92cd9866c14',1,'Adc_GroupLogEntryType::groupId()'],['../structAdc__FifoErrLogEntryType.html#a7461fbd2cfab290a71245cc311d10d6a',1,'Adc_FifoErrLogEntryType::groupId()']]],
  ['grouppriority',['groupPriority',['../structAdc__GroupConfigType.html#a2031634ea5ae4ac6443f194748f6ab9f',1,'Adc_GroupConfigType']]],
  ['groupreplacement',['groupReplacement',['../structAdc__GroupConfigType.html#a3ab4f372c4e21e4ba611d2b73a5ea1d4',1,'Adc_GroupConfigType']]]
];
