var indexSectionsWithContent =
{
  0: "0abcdefghijlmnoprstuvwx",
  1: "acdfgipsw",
  2: "0acdefgipsw",
  3: "acdefgipsw",
  4: "abcdefghijlmnoprstuvwx",
  5: "acdfgips",
  6: "acgipsw",
  7: "acdegipsw",
  8: "acd",
  9: "acdefgipsw",
  10: "acdefgimpsuvw"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "enums",
  7: "enumvalues",
  8: "defines",
  9: "groups",
  10: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Data Structures",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Enumerations",
  7: "Enumerator",
  8: "Macros",
  9: "Modules",
  10: "Pages"
};

