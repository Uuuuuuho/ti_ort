var group__MCAL__CAN__CFG =
[
    [ "Can_MaskType", "structCan__MaskType.html", [
      [ "MaskValue", "structCan__MaskType.html#ad8694386be43c563da1c646a16622f2e", null ]
    ] ],
    [ "Can_HwFilterType", "structCan__HwFilterType.html", [
      [ "IDValue", "structCan__HwFilterType.html#a3d784fa0219eede2ed4907c0efc0f99c", null ],
      [ "Mask", "structCan__HwFilterType.html#a9c9b324b7b85300729cf336861d4e7e6", null ]
    ] ],
    [ "Can_FdBaudConfigType", "structCan__FdBaudConfigType.html", [
      [ "Baud", "structCan__FdBaudConfigType.html#abacc0853550e44e0deddce9721f54b34", null ],
      [ "PropSeg", "structCan__FdBaudConfigType.html#a7d07bf1b1e69cc7af9ec3ebdde69fccf", null ],
      [ "Pseg1", "structCan__FdBaudConfigType.html#a1b4bab4374af4858b8ceb3b2e928cb58", null ],
      [ "Pseg2", "structCan__FdBaudConfigType.html#ac03429c3691a50fa3a07c992e7609c25", null ],
      [ "Sjw", "structCan__FdBaudConfigType.html#af4b204780142df3429c495290babf9c7", null ],
      [ "TimingValues", "structCan__FdBaudConfigType.html#af26f6a8584f468c57ad2f76904db4766", null ],
      [ "BrpValue", "structCan__FdBaudConfigType.html#aa2731ec4eaae9f4a9f18f08ea21e8a23", null ],
      [ "TrcvCompDelay", "structCan__FdBaudConfigType.html#a8100a193e3d3940356cc0f1fc7716d14", null ],
      [ "BrsSwitch", "structCan__FdBaudConfigType.html#a2d5b279b2b9ca0619a8ed2789ade19c6", null ]
    ] ],
    [ "Can_BaudConfigType", "structCan__BaudConfigType.html", [
      [ "Baud", "structCan__BaudConfigType.html#a4b028ce19bde8167d2cc3fd33154a09d", null ],
      [ "PropSeg", "structCan__BaudConfigType.html#ae02c68633136cb9db7371854eae3f972", null ],
      [ "Pseg1", "structCan__BaudConfigType.html#a9a0a549535f9de078bd3e5a4468cdcf4", null ],
      [ "Pseg2", "structCan__BaudConfigType.html#a0ca6c42d7dd9094f6fde10241bf5d6d6", null ],
      [ "Sjw", "structCan__BaudConfigType.html#ac4cad1bb263b7ce035e6ea14d918e3dc", null ],
      [ "TimingValues", "structCan__BaudConfigType.html#a94d68956bdb1d5c930337764ba69cbc9", null ],
      [ "BrpValue", "structCan__BaudConfigType.html#a709c078b420083cf7bf0b53ff5d6881d", null ],
      [ "BaudFdRateConfig", "structCan__BaudConfigType.html#a5b46d1a6bbd470b7fb0e1880db1157be", null ]
    ] ],
    [ "Can_ControllerType", "structCan__ControllerType.html", [
      [ "DefaultBaud", "structCan__ControllerType.html#ab37f2696fe57e019b50c12641e9b4e96", null ],
      [ "BaudRateConfigList", "structCan__ControllerType.html#abc1727b15efeb09712facbad72e0218d", null ]
    ] ],
    [ "Can_ControllerType_PC", "structCan__ControllerType__PC.html", [
      [ "ControllerId", "structCan__ControllerType__PC.html#a6e8848ab61dd06a23129cc94370c9281", null ],
      [ "CntrActive", "structCan__ControllerType__PC.html#a697da12c572fb86da05400271fa98f4f", null ],
      [ "CntrAddr", "structCan__ControllerType__PC.html#a7a29b096231268408b7d7775568b428e", null ],
      [ "RxProcessingType", "structCan__ControllerType__PC.html#aceb6034c8df82fc55f520264a2cbd7a4", null ],
      [ "TxProcessingType", "structCan__ControllerType__PC.html#a552d73f57fe8bd02b377c5298ce23808", null ],
      [ "BusOffProcessingInterrupt", "structCan__ControllerType__PC.html#a20796f622cff13c4f4cf788d220ff632", null ],
      [ "CanControllerInst", "structCan__ControllerType__PC.html#a0fbb5456dc917c8aadb02e3e64ac18a2", null ],
      [ "CanFDModeEnabled", "structCan__ControllerType__PC.html#a8426f8fa4163e6bd0f31e3299378b272", null ]
    ] ],
    [ "Can_MailboxType", "structCan__MailboxType.html", [
      [ "CanHandleType", "structCan__MailboxType.html#a27785b04cc39672c2866c0b7a6613f01", null ],
      [ "MBIdType", "structCan__MailboxType.html#ab2268687656aba013391eb7a1b819647", null ],
      [ "HwHandle", "structCan__MailboxType.html#a30c63b6d578d79eda552fba042b18d72", null ],
      [ "CanHwObjectCount", "structCan__MailboxType.html#a9a0fa0be5a3d84170343bfe639106bde", null ],
      [ "MBDir", "structCan__MailboxType.html#a726be868a5240015e8890704c7ddc2a8", null ],
      [ "Controller", "structCan__MailboxType.html#a6aa3a0b474ec91e11709ead4b020ec5f", null ],
      [ "HwFilterList", "structCan__MailboxType.html#a3ff061c580192ff17038976fe750cba5", null ],
      [ "HwFilterCnt", "structCan__MailboxType.html#a9d37b104fc701be70134aabbbae1d1a5", null ],
      [ "CanFdPaddingValue", "structCan__MailboxType.html#abbeeb3329f5ef4e2257f91c1dfd64d3f", null ],
      [ "CanHardwareObjectUsesPolling", "structCan__MailboxType.html#a200b64569da6725db93808bcecc5119d", null ]
    ] ],
    [ "Can_MailboxType_PC", "structCan__MailboxType__PC.html", [
      [ "CanObjectId", "structCan__MailboxType__PC.html#addae17d9b18a52155e0eeecaaeff2323", null ]
    ] ],
    [ "Can_DmaPrms", "structCan__DmaPrms.html", [
      [ "Reserved", "structCan__DmaPrms.html#a832f7352182e49ccd2f204b473ef279e", null ]
    ] ],
    [ "Can_ConfigType", "structCan__ConfigType.html", [
      [ "CanControllerList", "structCan__ConfigType.html#a034c55efd1e2c8fb4c2907e5b1f45443", null ],
      [ "CanMaxControllerCount", "structCan__ConfigType.html#a4f3ee9823d88c4e0ba4e1cea0065dbdb", null ],
      [ "MailBoxList", "structCan__ConfigType.html#a3565d78e3e5315cf2815288a67f3be17", null ],
      [ "MaxMbCnt", "structCan__ConfigType.html#a4717e58ae897239d7a4a9a0402e9cb87", null ],
      [ "MaxBaudConfigID", "structCan__ConfigType.html#acb660e24d0f48928680f9c5221a01f93", null ],
      [ "DmaPrms", "structCan__ConfigType.html#a85f0f3b61d4b529e479de2fa4e52da9b", null ]
    ] ],
    [ "Can_RegisterReadbackType", "structCan__RegisterReadbackType.html", [
      [ "CanReadBackRegPID", "structCan__RegisterReadbackType.html#a2c294077e3ba049cb637df5ce6115337", null ],
      [ "CanReadBackRegSTAT", "structCan__RegisterReadbackType.html#ad17d176d246411a0672d1136f0c6670a", null ],
      [ "CanReadBackRegCREL", "structCan__RegisterReadbackType.html#a10bbdc0e4fdbd77c9125188b2f8ef427", null ],
      [ "CanReadBackRegENDN", "structCan__RegisterReadbackType.html#a463b511f43af5b656f72b813cee9e725", null ]
    ] ],
    [ "CAN_VARIANT_PRE_COMPILE", "group__MCAL__CAN__CFG.html#gad93f01ab08f1fc5baa5a0d478d75d57c", null ],
    [ "CAN_VERSION_INFO_API", "group__MCAL__CAN__CFG.html#ga957e2799bfae0197e3f0a7cb5a711f64", null ],
    [ "CAN_DEV_ERROR_DETECT", "group__MCAL__CAN__CFG.html#gacf79cf8699e5d56b2110e7ccfe22ccbd", null ],
    [ "CAN_LOOPBACK_ENABLE", "group__MCAL__CAN__CFG.html#ga7c07a049aaed562150fb860806d40aa3", null ],
    [ "CAN_REGISTER_READBACK_API", "group__MCAL__CAN__CFG.html#gaafb9c19cf04d83495bff567e42bfe36c", null ],
    [ "CAN_WAKEUP_FUNCTIONALITY_API", "group__MCAL__CAN__CFG.html#ga12317154fd58dfa6a56179a168c132d1", null ],
    [ "CAN_MULTIPLEXED_TRANSMISSION_ENABLE", "group__MCAL__CAN__CFG.html#gaba7a9a8e617160cb684c5d70cba95faa", null ],
    [ "CAN_INIT_CONFIG_PC", "group__MCAL__CAN__CFG.html#ga41b8d3501f325456ec94f9d75b2a4ecd", null ],
    [ "CAN_TX_POLLING", "group__MCAL__CAN__CFG.html#ga1f60db0399d87b54a0f054ebef2a8239", null ],
    [ "CAN_RX_POLLING", "group__MCAL__CAN__CFG.html#ga157817de0b0913c31f8506e00f5b7b02", null ],
    [ "CAN_BUSOFF_POLLING", "group__MCAL__CAN__CFG.html#gabc6592d500f1cdf9c2f243f5b628daf2", null ],
    [ "CAN_WAKEUP_POLLING", "group__MCAL__CAN__CFG.html#ga8f8030b81013467e61e774c3716c360b", null ],
    [ "CAN_NUM_CONTROLLER", "group__MCAL__CAN__CFG.html#gae1fffd50ed3177a15e9bf9080ceb20ba", null ],
    [ "CAN_NUM_MAILBOXES", "group__MCAL__CAN__CFG.html#gaed92438f876822a62a26b43490f5b249", null ],
    [ "CAN_MAX_CONTROLLER", "group__MCAL__CAN__CFG.html#ga84123b8f78012e7551cb0cfb2fc21159", null ],
    [ "CAN_MAX_MAILBOXES", "group__MCAL__CAN__CFG.html#ga46b0129b5bce381498d958dbc71703a2", null ],
    [ "CAN_ISR_TYPE", "group__MCAL__CAN__CFG.html#gac8aebce09b82ce3b4b28be1deeea70f5", null ],
    [ "CLK_CAN_FD_FREQ", "group__MCAL__CAN__CFG.html#gac1e4e547bf619308f9193b58ed21c8ba", null ],
    [ "CanConf_CanController_CanController_0", "group__MCAL__CAN__CFG.html#gab27f7c1537193743798b271ce65e6c7b", null ],
    [ "CanConf_CanController_CanController_1", "group__MCAL__CAN__CFG.html#gada6f3cea1d444f36e697ca9c16e19c0a", null ],
    [ "CanConf_CanController_CanController_2", "group__MCAL__CAN__CFG.html#gaef431bdebeb00689485740d353255c88", null ],
    [ "CanControllerState_Type", "group__MCAL__CAN__CFG.html#ga4acc9d227ffba7a3648a169c22c31881", null ],
    [ "Can_InterruptMask_Type", "group__MCAL__CAN__CFG.html#ga89e9bb14b689b92f6292ff6932572d37", null ],
    [ "Can_MailBoxDirectionType", "group__MCAL__CAN__CFG.html#ga68dbd9bfdd6b911400b2f34dfd6d864f", [
      [ "CAN_MAILBOX_DIRECTION_RX", "group__MCAL__CAN__CFG.html#gga68dbd9bfdd6b911400b2f34dfd6d864fa4c359a1e03db35d9e266768e138b120c", null ],
      [ "CAN_MAILBOX_DIRECTION_TX", "group__MCAL__CAN__CFG.html#gga68dbd9bfdd6b911400b2f34dfd6d864fa90bf68487a257a69a5ebfed6f19de4a5", null ]
    ] ],
    [ "Can_HandleType", "group__MCAL__CAN__CFG.html#ga772e8591435291c92799ca59c803c50a", [
      [ "CAN_FULL", "group__MCAL__CAN__CFG.html#gga772e8591435291c92799ca59c803c50aa50a3bbe75ff3b215ffd34bff6f624c5b", null ],
      [ "CAN_BASIC", "group__MCAL__CAN__CFG.html#gga772e8591435291c92799ca59c803c50aa9bd9b8a1ffc144831e0a7ae3cbdd81b1", null ]
    ] ],
    [ "Can_TxRxProcessingType", "group__MCAL__CAN__CFG.html#ga845c8b2574a25149f63a5bd0127ff6bf", [
      [ "CAN_TX_RX_PROCESSING_INTERRUPT", "group__MCAL__CAN__CFG.html#gga845c8b2574a25149f63a5bd0127ff6bfaa07e6357fc760bf7438bdd96f870672a", null ],
      [ "CAN_TX_RX_PROCESSING_MIXED", "group__MCAL__CAN__CFG.html#gga845c8b2574a25149f63a5bd0127ff6bfaf4597b2fc97c5432e5bbbbdae115c2b9", null ],
      [ "CAN_TX_RX_PROCESSING_POLLING", "group__MCAL__CAN__CFG.html#gga845c8b2574a25149f63a5bd0127ff6bfa37af7ffaa1f4a58487856218bf0b84b6", null ]
    ] ],
    [ "Can_ControllerInstance", "group__MCAL__CAN__CFG.html#ga1a13fc87d720c11306d3bd30a7359056", [
      [ "CAN_CONTROLLER_INSTANCE_MCU_MCAN0", "group__MCAL__CAN__CFG.html#gga1a13fc87d720c11306d3bd30a7359056ae70dca9f77b9bac0fe14209d0955ebe8", null ],
      [ "CAN_CONTROLLER_INSTANCE_MCU_MCAN1", "group__MCAL__CAN__CFG.html#gga1a13fc87d720c11306d3bd30a7359056af8348f9cd5dbe3ee9e9c1867b435f300", null ],
      [ "CAN_CONTROLLER_INSTANCE_MCAN0", "group__MCAL__CAN__CFG.html#gga1a13fc87d720c11306d3bd30a7359056aebbb0b5bd614951971ee88bd687d1964", null ],
      [ "CAN_CONTROLLER_INSTANCE_MCAN1", "group__MCAL__CAN__CFG.html#gga1a13fc87d720c11306d3bd30a7359056a52ce3b075b46077137c174161c6b18f2", null ],
      [ "CAN_CONTROLLER_INSTANCE_MCAN2", "group__MCAL__CAN__CFG.html#gga1a13fc87d720c11306d3bd30a7359056a815dcc4c61b285caa237072b04492fab", null ],
      [ "CAN_CONTROLLER_INSTANCE_MCAN3", "group__MCAL__CAN__CFG.html#gga1a13fc87d720c11306d3bd30a7359056aeac9d8254b43288fcbd0f49282e692c4", null ],
      [ "CAN_CONTROLLER_INSTANCE_MCAN4", "group__MCAL__CAN__CFG.html#gga1a13fc87d720c11306d3bd30a7359056a4941a13ac554eb5baf24d8f7d0bc8637", null ],
      [ "CAN_CONTROLLER_INSTANCE_MCAN5", "group__MCAL__CAN__CFG.html#gga1a13fc87d720c11306d3bd30a7359056a8f74204c7a401f16438e4465ae05d354", null ],
      [ "CAN_CONTROLLER_INSTANCE_MCAN6", "group__MCAL__CAN__CFG.html#gga1a13fc87d720c11306d3bd30a7359056a0560e86e325251ed28c5afa0aa404b96", null ],
      [ "CAN_CONTROLLER_INSTANCE_MCAN7", "group__MCAL__CAN__CFG.html#gga1a13fc87d720c11306d3bd30a7359056ae7142fff21ae1c544d4a55d20f7504f3", null ],
      [ "CAN_CONTROLLER_INSTANCE_MCAN8", "group__MCAL__CAN__CFG.html#gga1a13fc87d720c11306d3bd30a7359056ad691a54185a85db86b86c1ab201537ff", null ],
      [ "CAN_CONTROLLER_INSTANCE_MCAN9", "group__MCAL__CAN__CFG.html#gga1a13fc87d720c11306d3bd30a7359056adc3931e646ae7050906118a9b2cfdb47", null ],
      [ "CAN_CONTROLLER_INSTANCE_MCAN10", "group__MCAL__CAN__CFG.html#gga1a13fc87d720c11306d3bd30a7359056a14e0737020aa41e0105ec47d73461201", null ],
      [ "CAN_CONTROLLER_INSTANCE_MCAN11", "group__MCAL__CAN__CFG.html#gga1a13fc87d720c11306d3bd30a7359056a31c4307434be83d2a8ec95c67f4cb958", null ],
      [ "CAN_CONTROLLER_INSTANCE_MCAN12", "group__MCAL__CAN__CFG.html#gga1a13fc87d720c11306d3bd30a7359056a4f9c116d831eb5cb1dada4addae88dad", null ],
      [ "CAN_CONTROLLER_INSTANCE_MCAN13", "group__MCAL__CAN__CFG.html#gga1a13fc87d720c11306d3bd30a7359056aec9a57bb86a5d2ef91d3c9adbbd4a7ce", null ]
    ] ]
];