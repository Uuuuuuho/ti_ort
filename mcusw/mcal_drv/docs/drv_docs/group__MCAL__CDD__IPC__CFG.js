var group__MCAL__CDD__IPC__CFG =
[
    [ "Cdd_IpcMpType", "structCdd__IpcMpType.html", [
      [ "ownProcID", "structCdd__IpcMpType.html#af96f544bc769aaceb35d9bcf9c9570f5", null ],
      [ "numProcs", "structCdd__IpcMpType.html#a55485762bc950c7a256f682e170cc928", null ],
      [ "remoteProcID", "structCdd__IpcMpType.html#a80f5e435af437b26c283461b43569116", null ],
      [ "reserved", "structCdd__IpcMpType.html#a178f8ce265459a42e3ee9c8c8adc67fa", null ]
    ] ],
    [ "Cdd_IpcVertIoType", "structCdd__IpcVertIoType.html", [
      [ "vertIoRingAddr", "structCdd__IpcVertIoType.html#a4a250ac05cd64a7faa5e859a5a3e00b8", null ],
      [ "vertIoRingSize", "structCdd__IpcVertIoType.html#ae997b8e2b3e020f965469ae2784fa2d9", null ],
      [ "vertIoObjSize", "structCdd__IpcVertIoType.html#a57a05394003dbe86a0a91001c9fd938f", null ],
      [ "reserved", "structCdd__IpcVertIoType.html#a04498eae571dd3aa721b70de70debde8", null ]
    ] ],
    [ "Cdd_IpcChannelType", "structCdd__IpcChannelType.html", [
      [ "id", "structCdd__IpcChannelType.html#af2241c7d83ceaf727a75fde3853d3ce9", null ],
      [ "localEp", "structCdd__IpcChannelType.html#ae98ed126348202cbee0ca0149f913116", null ],
      [ "remoteEp", "structCdd__IpcChannelType.html#a82035b1bfcd411991e092a14dfe8c6e6", null ],
      [ "remoteProcId", "structCdd__IpcChannelType.html#a1ec092031286fe6094e9a74fed45eef9", null ],
      [ "numMsgQueued", "structCdd__IpcChannelType.html#a83e1eb1cf6a9a21df4270e30da636c47", null ],
      [ "maxMsgSize", "structCdd__IpcChannelType.html#a06ee2084e9023587c13145eca3ad45dd", null ],
      [ "reserved", "structCdd__IpcChannelType.html#a2fabc775839881f752df9eacb2a4bf25", null ]
    ] ],
    [ "Cdd_IpcConfigType", "structCdd__IpcConfigType.html", [
      [ "coreIds", "structCdd__IpcConfigType.html#ae0b2ff5d5fe73b675a680a9879b9cb44", null ],
      [ "vertIoCfg", "structCdd__IpcConfigType.html#afbd878aadb2464b7f002afb8fa768dca", null ],
      [ "channelCount", "structCdd__IpcConfigType.html#aec2fd1aeb3b87518ef03d0bebaf77d70", null ],
      [ "pChCfg", "structCdd__IpcConfigType.html#adcecc58f0489b4ac6135c035f995c4dc", null ],
      [ "reserved", "structCdd__IpcConfigType.html#a5eca92bccb7fa30f4fd0d43ce8e77a78", null ]
    ] ],
    [ "Cdd_IpcRegRbValues", "structCdd__IpcRegRbValues.html", [
      [ "numRegisters", "structCdd__IpcRegRbValues.html#ab64c9748685c56c26b3d821c9e6c6c94", null ],
      [ "regValues", "structCdd__IpcRegRbValues.html#a2318769517033ab67b881ad605e161f2", null ],
      [ "reserved", "structCdd__IpcRegRbValues.html#a0c1caae334b6e741db8c1ce18ec4fd97", null ]
    ] ],
    [ "Cdd_IpcChannelBufType", "structCdd__IpcChannelBufType.html", [
      [ "pBuf", "structCdd__IpcChannelBufType.html#aba90b44d3af9c25d836304d819324320", null ],
      [ "bufSize", "structCdd__IpcChannelBufType.html#a6d25056bec80989d38c4e3871d4fe9b4", null ]
    ] ]
];