var group__MCAL__ETH__IRQ =
[
    [ "ETH_ISR_VOID", "group__MCAL__ETH__IRQ.html#ga615f91e06fbd8d27f963fe0e4291c6ec", null ],
    [ "ETH_ISR_CAT1", "group__MCAL__ETH__IRQ.html#ga1a41704ff02568453435545310af4c88", null ],
    [ "ETH_ISR_CAT2", "group__MCAL__ETH__IRQ.html#ga57adf49adba083f295c50c4736309314", null ],
    [ "Eth_RxIrqHdlr_0", "group__MCAL__ETH__IRQ.html#gad0b8dba0a7de73650ac84d669d4ed563", null ],
    [ "Eth_TxIrqHdlr_0", "group__MCAL__ETH__IRQ.html#gaaa5c7d89b99ce6fa34c65162257e646a", null ],
    [ "Eth_MdioIrqHdlr_0", "group__MCAL__ETH__IRQ.html#gaced8482253604bb61a62eabb649ef8b0", null ]
];