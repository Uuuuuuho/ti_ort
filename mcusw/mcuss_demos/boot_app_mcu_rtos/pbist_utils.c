/*
*
* Copyright (c) 2020 Texas Instruments Incorporated
*
* All rights reserved not granted herein.
*
* Limited License.
*
* Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
* license under copyrights and patents it now or hereafter owns or controls to make,
* have made, use, import, offer to sell and sell ("Utilize") this software subject to the
* terms herein.  With respect to the foregoing patent license, such license is granted
* solely to the extent that any such patent is necessary to Utilize the software alone.
* The patent license shall not apply to any combinations which include this software,
* other than combinations with devices manufactured by or for TI ("TI Devices").
* No hardware patent is licensed hereunder.
*
* Redistributions must preserve existing copyright notices and reproduce this license
* (including the above copyright notice and the disclaimer and (if applicable) source
* code license limitations below) in the documentation and/or other materials provided
* with the distribution
*
* Redistribution and use in binary form, without modification, are permitted provided
* that the following conditions are met:
*
* *       No reverse engineering, decompilation, or disassembly of this software is
* permitted with respect to any software provided in binary form.
*
* *       any redistribution and use are licensed by TI for use only with TI Devices.
*
* *       Nothing shall obligate TI to provide you with source code for the software
* licensed and provided to you in object code.
*
* If software source code is provided to you, modification and redistribution of the
* source code are permitted provided that the following conditions are met:
*
* *       any redistribution and use of the source code, including any resulting derivative
* works, are licensed by TI for use only with TI Devices.
*
* *       any redistribution and use of any object code compiled from the source code
* and any resulting derivative works, are licensed by TI for use only with TI Devices.
*
* Neither the name of Texas Instruments Incorporated nor the names of its suppliers
*
* may be used to endorse or promote products derived from this software without
* specific prior written permission.
*
* DISCLAIMER.
*
* THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
* OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
* OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
* IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
* BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
* OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
* OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
* OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/**
 *  \file pbist_utils.c
 *
 *  \brief PBIST utility functions
 */

/* ========================================================================== */
/*                             Include Files                                  */
/* ========================================================================== */
#include "Std_Types.h"
#include <string.h>
#include <ti/csl/csl_types.h>
#include <ti/csl/csl_pbist.h>
#include <ti/csl/csl_rat.h>
#include <ti/csl/csl_clec.h>
#include <ti/csl/cslr_vpac.h>
#include <ti/csl/soc.h>
#include <ti/csl/csl_cbass.h>
#include <ti/drv/uart/UART_stdio.h>
#include <ti/drv/sciclient/sciclient.h>

//#include <ti/csl/src/ip/sa/V3/cslr_cp_ace.h>
#include <ti/csl/cslr_cp_ace.h>

/* Osal API header files */
#include <ti/osal/HwiP.h>
#include <ti/osal/TimerP.h>

#include "power_seq.h"

#include "pbist_utils.h"
#include "pbist_defs.h"

#include "app_utils.h"

/* ========================================================================== */
/*                           Macros & Typedefs                                */
/* ========================================================================== */

/* This flag adds more verbose prints */
//#define DEBUG

/* This is to power up the cores before test and power down afterwards */
#define POWERUP_CORES_BEFORE_TEST

/* This flag gathers timing information for each PBIST test */
//#define PROFILE_PBIST_TIMING

/* POST Status definitions */
#define PBIST_POST_COMPLETED_SUCCESS      (0u)
#define PBIST_POST_COMPLETED_FAILURE      (1u)
#define PBIST_POST_ATTEMPTED_TIMEOUT      (2u)
#define PBIST_POST_NOT_RUN                (3u)

/* ========================================================================== */
/*                 Internal Function Declarations                             */
/* ========================================================================== */
void PBIST_eventHandler( uint32_t instanceId );

static int32_t PBIST_isPostPbistTimeout(uint32_t postStatMmrRegVal,
                                        Bool *pIsTimedOut);
static int32_t PBIST_isPostPbistDone(uint32_t postStatMmrRegVal,
                                     Bool *pIsDone);
static int32_t PBIST_postCheckResult(uint32_t postStatMmrRegVal,
                                     Bool *pResult);

/* ========================================================================== */
/*                         Structure Declarations                             */
/* ========================================================================== */

/* None */

/* ========================================================================== */
/*                          Function Declarations                             */
/* ========================================================================== */

/* None */

/* ========================================================================== */
/*                            Global Variables                                */
/* ========================================================================== */

/* None */

/* ========================================================================== */
/*                            External Variables                              */
/* ========================================================================== */

/* None */

/* ========================================================================== */
/*                          Function Definitions                              */
/* ========================================================================== */
void PBIST_eventHandler( uint32_t instanceId)
{
    CSL_ErrType_t status;

    if (instanceId == PBIST_INSTANCE_C7X)
    {
        /* Clear C7x PBIST interrupt event in CLEC */
        status = CSL_clecClearEvent((CSL_CLEC_EVTRegs *)CSL_COMPUTE_CLUSTER0_CLEC_REGS_BASE,
                       CSLR_COMPUTE_CLUSTER0_CLEC_MSMC_EVENT_IN_COMPUTE_CLUSTER0_CORE_CORE_MSMC_INTR_12);
        if (status != CSL_PASS) {
            /* This is really not expected in normal operation:
               Add exception if needed
            */
        }
    }
    if (instanceId == PBIST_INSTANCE_A72)
    {
        /* Clear A72 PBIST interrupt event in CLEC*/
        status = CSL_clecClearEvent((CSL_CLEC_EVTRegs *)CSL_COMPUTE_CLUSTER0_CLEC_REGS_BASE,
                       CSLR_COMPUTE_CLUSTER0_CLEC_MSMC_EVENT_IN_COMPUTE_CLUSTER0_CORE_CORE_MSMC_INTR_8);
        if (status != CSL_PASS) {
            /* This is really not expected in normal operation:
               Add exception if needed
            */
        }
    }

    PBIST_TestHandleArray[instanceId].doneFlag = true;

    return;
}

/* PBIST_setFirewall: Sets firewall settings to be able to access CLEC registers */
static int32_t PBIST_setFirewall(void)
{
    int32_t retVal = CSL_PASS;
    uint32_t reqFlag = TISCI_MSG_FLAG_AOP | TISCI_MSG_FLAG_DEVICE_EXCLUSIVE;
    uint32_t timeout =  SCICLIENT_SERVICE_WAIT_FOREVER;
    struct  tisci_msg_fwl_set_firewall_region_req request;
    Sciclient_ReqPrm_t reqParam;
    Sciclient_RespPrm_t respParam;

    request.fwl_id       = (uint32_t)CSL_STD_FW_NAVSS0_NAV_SRAM0_ID;
    request.region = (uint32_t) 1U; /* Pick up any unused region : 1 */
    request.n_permission_regs = CSL_FW_NUM_CBASS_FW_EP_REGION_PERMISSION;
    request.control = (FW_REGION_ENABLE & CSL_CBASS_ISC_EP_REGION_CONTROL_ENABLE_MASK);
    request.permissions[0] = (FW_MCU_R5F0_PRIVID << CSL_CBASS_FW_EP_REGION_PERMISSION_PRIV_ID_SHIFT)
                             | (1U << CSL_CBASS_FW_EP_REGION_PERMISSION_SEC_SUPV_WRITE_SHIFT)
                             | (1U << CSL_CBASS_FW_EP_REGION_PERMISSION_SEC_SUPV_READ_SHIFT)
                             | (0U << CSL_CBASS_FW_EP_REGION_PERMISSION_SEC_SUPV_CACHEABLE_SHIFT)
                             | (1U << CSL_CBASS_FW_EP_REGION_PERMISSION_SEC_SUPV_DEBUG_SHIFT)
                             | (1U << CSL_CBASS_FW_EP_REGION_PERMISSION_SEC_USER_WRITE_SHIFT)
                             | (1U << CSL_CBASS_FW_EP_REGION_PERMISSION_SEC_USER_READ_SHIFT)
                             | (0U << CSL_CBASS_FW_EP_REGION_PERMISSION_SEC_USER_CACHEABLE_SHIFT)
                             | (1U << CSL_CBASS_FW_EP_REGION_PERMISSION_SEC_USER_DEBUG_SHIFT)
                             | (1U << CSL_CBASS_FW_EP_REGION_PERMISSION_NONSEC_SUPV_WRITE_SHIFT)
                             | (1U << CSL_CBASS_FW_EP_REGION_PERMISSION_NONSEC_SUPV_READ_SHIFT)
                             | (0U << CSL_CBASS_FW_EP_REGION_PERMISSION_NONSEC_SUPV_CACHEABLE_SHIFT)
                             | (1U << CSL_CBASS_FW_EP_REGION_PERMISSION_NONSEC_SUPV_DEBUG_SHIFT)
                             | (1U << CSL_CBASS_FW_EP_REGION_PERMISSION_NONSEC_USER_WRITE_SHIFT)
                             | (1U << CSL_CBASS_FW_EP_REGION_PERMISSION_NONSEC_USER_READ_SHIFT)
                             | (0U << CSL_CBASS_FW_EP_REGION_PERMISSION_NONSEC_USER_CACHEABLE_SHIFT)
                             | (1U << CSL_CBASS_FW_EP_REGION_PERMISSION_NONSEC_USER_DEBUG_SHIFT);
    request.permissions[1] = (FW_MCU_R5F0_PRIVID << CSL_CBASS_FW_EP_REGION_PERMISSION_PRIV_ID_SHIFT)
                             | (1U << CSL_CBASS_FW_EP_REGION_PERMISSION_SEC_SUPV_WRITE_SHIFT)
                             | (1U << CSL_CBASS_FW_EP_REGION_PERMISSION_SEC_SUPV_READ_SHIFT)
                             | (0U << CSL_CBASS_FW_EP_REGION_PERMISSION_SEC_SUPV_CACHEABLE_SHIFT)
                             | (1U << CSL_CBASS_FW_EP_REGION_PERMISSION_SEC_SUPV_DEBUG_SHIFT)
                             | (1U << CSL_CBASS_FW_EP_REGION_PERMISSION_SEC_USER_WRITE_SHIFT)
                             | (1U << CSL_CBASS_FW_EP_REGION_PERMISSION_SEC_USER_READ_SHIFT)
                             | (0U << CSL_CBASS_FW_EP_REGION_PERMISSION_SEC_USER_CACHEABLE_SHIFT)
                             | (1U << CSL_CBASS_FW_EP_REGION_PERMISSION_SEC_USER_DEBUG_SHIFT)
                             | (1U << CSL_CBASS_FW_EP_REGION_PERMISSION_NONSEC_SUPV_WRITE_SHIFT)
                             | (1U << CSL_CBASS_FW_EP_REGION_PERMISSION_NONSEC_SUPV_READ_SHIFT)
                             | (0U << CSL_CBASS_FW_EP_REGION_PERMISSION_NONSEC_SUPV_CACHEABLE_SHIFT)
                             | (1U << CSL_CBASS_FW_EP_REGION_PERMISSION_NONSEC_SUPV_DEBUG_SHIFT)
                             | (1U << CSL_CBASS_FW_EP_REGION_PERMISSION_NONSEC_USER_WRITE_SHIFT)
                             | (1U << CSL_CBASS_FW_EP_REGION_PERMISSION_NONSEC_USER_READ_SHIFT)
                             | (0U << CSL_CBASS_FW_EP_REGION_PERMISSION_NONSEC_USER_CACHEABLE_SHIFT)
                             | (1U << CSL_CBASS_FW_EP_REGION_PERMISSION_NONSEC_USER_DEBUG_SHIFT);
    request.permissions[2] = (FW_MCU_R5F0_PRIVID << CSL_CBASS_FW_EP_REGION_PERMISSION_PRIV_ID_SHIFT)
                             | (1U << CSL_CBASS_FW_EP_REGION_PERMISSION_SEC_SUPV_WRITE_SHIFT)
                             | (1U << CSL_CBASS_FW_EP_REGION_PERMISSION_SEC_SUPV_READ_SHIFT)
                             | (0U << CSL_CBASS_FW_EP_REGION_PERMISSION_SEC_SUPV_CACHEABLE_SHIFT)
                             | (1U << CSL_CBASS_FW_EP_REGION_PERMISSION_SEC_SUPV_DEBUG_SHIFT)
                             | (1U << CSL_CBASS_FW_EP_REGION_PERMISSION_SEC_USER_WRITE_SHIFT)
                             | (1U << CSL_CBASS_FW_EP_REGION_PERMISSION_SEC_USER_READ_SHIFT)
                             | (0U << CSL_CBASS_FW_EP_REGION_PERMISSION_SEC_USER_CACHEABLE_SHIFT)
                             | (1U << CSL_CBASS_FW_EP_REGION_PERMISSION_SEC_USER_DEBUG_SHIFT)
                             | (1U << CSL_CBASS_FW_EP_REGION_PERMISSION_NONSEC_SUPV_WRITE_SHIFT)
                             | (1U << CSL_CBASS_FW_EP_REGION_PERMISSION_NONSEC_SUPV_READ_SHIFT)
                             | (0U << CSL_CBASS_FW_EP_REGION_PERMISSION_NONSEC_SUPV_CACHEABLE_SHIFT)
                             | (1U << CSL_CBASS_FW_EP_REGION_PERMISSION_NONSEC_SUPV_DEBUG_SHIFT)
                             | (1U << CSL_CBASS_FW_EP_REGION_PERMISSION_NONSEC_USER_WRITE_SHIFT)
                             | (1U << CSL_CBASS_FW_EP_REGION_PERMISSION_NONSEC_USER_READ_SHIFT)
                             | (0U << CSL_CBASS_FW_EP_REGION_PERMISSION_NONSEC_USER_CACHEABLE_SHIFT)
                             | (1U << CSL_CBASS_FW_EP_REGION_PERMISSION_NONSEC_USER_DEBUG_SHIFT);
    request.start_address = CSL_COMPUTE_CLUSTER0_CLEC_REGS_BASE;
    request.end_address = CSL_COMPUTE_CLUSTER0_CLEC_REGS_BASE + CSL_COMPUTE_CLUSTER0_CLEC_REGS_SIZE;

    reqParam.messageType    = (uint16_t) TISCI_MSG_SET_FWL_REGION;
    reqParam.flags          = (uint32_t) reqFlag;
    reqParam.pReqPayload    = (const uint8_t *) &request;
    reqParam.reqPayloadSize = (uint32_t) sizeof (request);
    reqParam.timeout        = (uint32_t) timeout;

    respParam.flags           = (uint32_t) 0;   /* Populated by the API */
    respParam.pRespPayload    = (uint8_t *) 0;
    respParam.respPayloadSize = (uint32_t) 0;


    if (((reqFlag & TISCI_MSG_FLAG_AOP) != TISCI_MSG_FLAG_AOP)&&
        (reqFlag != 0U))
    {
        retVal = CSL_EFAIL;
    }
    if (retVal == CSL_PASS)
    {
        retVal = Sciclient_service(&reqParam, &respParam);
    }
    if ((retVal != CSL_PASS) ||
        ((reqFlag != 0U) &&
        ((respParam.flags & TISCI_MSG_FLAG_ACK) != TISCI_MSG_FLAG_ACK)))
    {
        retVal = CSL_EFAIL;
    }
    return retVal;
}


/* Captures common Initialization: currently initializes CLEC interrupt routing
   for C7x & A72 */
int32_t PBIST_commonInit(void)
{
    CSL_ErrType_t status;
    int32_t retValue = 0;
    CSL_ClecEventConfig evtCfg;

    /* Add firewall entry to gain access to CLEC registers */
    status = PBIST_setFirewall();
    if (status != CSL_PASS)
    {
        AppUtils_Printf(MSG_NORMAL, " PBIST_setFirewall failed \n");
        retValue = -1;
    }

    if (retValue == 0)
    {
        evtCfg.secureClaimEnable = 1U;
        evtCfg.evtSendEnable = 1U;
        evtCfg.extEvtNum = CSLR_COMPUTE_CLUSTER0_CLEC_MSMC_EVENT_IN_COMPUTE_CLUSTER0_CORE_CORE_MSMC_INTR_12;
        evtCfg.rtMap = 2U;
        evtCfg.c7xEvtNum = 0U;

        /* Configure interrupt router to take care of routing C7x PBIST interrupt event */
        status =  CSL_clecConfigEvent((CSL_CLEC_EVTRegs *)CSL_COMPUTE_CLUSTER0_CLEC_REGS_BASE,
                                      CSLR_COMPUTE_CLUSTER0_CLEC_MSMC_EVENT_IN_COMPUTE_CLUSTER0_CORE_CORE_MSMC_INTR_12,
                                      &evtCfg);
        if (status != CSL_PASS)
        {
            AppUtils_Printf(MSG_NORMAL, " CSL_clecConfigEvent C7x failed \n");
            retValue = -1;
        }
    }

    if (retValue == 0)
    {
        evtCfg.extEvtNum = CSLR_COMPUTE_CLUSTER0_CLEC_MSMC_EVENT_IN_COMPUTE_CLUSTER0_CORE_CORE_MSMC_INTR_8;
        /* Configure interrupt router to take care of routing A72 PBIST interrupt event */
        status =  CSL_clecConfigEvent((CSL_CLEC_EVTRegs *)CSL_COMPUTE_CLUSTER0_CLEC_REGS_BASE,
                                      CSLR_COMPUTE_CLUSTER0_CLEC_MSMC_EVENT_IN_COMPUTE_CLUSTER0_CORE_CORE_MSMC_INTR_8,
                                      &evtCfg);
        if (status != CSL_PASS)
        {
            AppUtils_Printf(MSG_NORMAL, " CSL_clecConfigEvent A72 failed \n");
            retValue = -1;
        }
    }

    return status;
}


/* This function runs the full PBIST test for a particular section.
 * It includes the following steps (each step has many sub-steps specificied
 * in more details within the function).
 * For HW POST PBIST:
 * Step 1: Configure interrupt handler
 * Step 2: Check POST results (already run at startup)
 *
 * For SW-initiated PBIST:
 * Step 1: Configure interrupt handler
 * Step 2: Configure processor to correct state
 * Step 3: Run PBIST test (includes checking the result)
 * Step 4: Restore cores
 * */

int32_t PBIST_runTest(uint32_t instanceId)
{
    int32_t testResult = 0;
    CSL_ErrType_t status;
    HwiP_Params hwiParams;
    HwiP_Handle PBIST_hwiPHandle;
    CSL_pbistRegs *pPBISTRegs;
    Bool PBISTResult;
    uint32_t postRegVal;
    uint32_t timeoutCount = 0;
    uint32_t moduleState = TISCI_MSG_VALUE_DEVICE_HW_STATE_OFF;
    uint32_t resetState = 0U;
    uint32_t contextLossState = 0U;
    CSL_RatTranslationCfgInfo translationCfg;
    bool result;
    int i;


#if defined(PROFILE_PBIST_TIMING)
#if defined(DEBUG)
    uint8_t  postStatus = PBIST_POST_COMPLETED_SUCCESS;
#endif
    uint64_t startTime , testStartTime,  testEndTime, endTime;
    uint64_t prepTime, diffTime, restoreTime;
    AppUtils_Printf(MSG_NORMAL,
                    "\n Starting PBIST test on %s, index %d...\n",
                    PBIST_TestHandleArray[instanceId].testName,
                    instanceId);
#endif
    /*-- Step 1: Configure interrupt handler --*/
    /* Disable interrupt */
    HwiP_disableInterrupt(PBIST_TestHandleArray[instanceId].interruptNumber);

    /* Default parameter initialization */
    HwiP_Params_init(&hwiParams);

    /* Pass core Index as argument to handler*/
    hwiParams.arg = instanceId;
#ifdef DEBUG
    AppUtils_Printf(MSG_NORMAL, "\n HwiP_Params_init complete \n");
#endif
    /* Register call back function for PBIST Interrupt */
    PBIST_hwiPHandle = HwiP_create(PBIST_TestHandleArray[instanceId].interruptNumber,
                                       (HwiP_Fxn)PBIST_eventHandler,
                                       (void *)&hwiParams);
    if (PBIST_hwiPHandle == NULL)
    {
        AppUtils_Printf(MSG_NORMAL, " Interrupt registration failed \n");
        testResult = -1;
    }

#if defined(PROFILE_PBIST_TIMING)
    /* Get start time of test */
    startTime = TimerP_getTimeInUsecs();
#endif

    /* Step 2: (if HW Power-On Self Test, i.e. POST) Check POST results  */
    if ((testResult == 0) &&
        (PBIST_TestHandleArray[instanceId].numPostPbistToCheck > 0))
    {
#ifdef DEBUG
        UART_printf("  HW POST: Running test on HW POST, %d Instances \n",
                    PBIST_TestHandleArray[instanceId].numPostPbistToCheck);
#endif
        postRegVal = CSL_REG32_RD(CSL_WKUP_CTRL_MMR0_CFG0_BASE +
                                  CSL_WKUP_CTRL_MMR_CFG0_WKUP_POST_STAT);

        /* Check if HW POST PBIST was performed */
        status = PBIST_isPostPbistDone(postRegVal, &PBISTResult);
        if (status != CSL_PASS)
        {
            UART_printf("   HW POST: CSL_PBIST_isPostPbistDone failed\n");
            testResult = -1;
        }
        else
        {
            if (PBISTResult != true)
            {
                /* HW POST: PBIST not completed, check if it timed out */
                status = PBIST_isPostPbistTimeout(postRegVal, &PBISTResult);
                if (PBISTResult != true)
                {
                    /* HW POST: PBIST was not performed at all on this device */
#if defined(DEBUG) && defined(PROFILE_PBIST_TIMING)
                    postStatus = PBIST_POST_NOT_RUN;
#endif
                }
                else
                {
                    /* HW POST: PBIST was attempted but timed out */
#if defined(DEBUG) && defined(PROFILE_PBIST_TIMING)
                    postStatus = PBIST_POST_ATTEMPTED_TIMEOUT;
#endif
                    testResult = -1;
                }
            }
            else
            {
                /* HW POST: PBIST was completed on this device, check the result */
                status = PBIST_postCheckResult(postRegVal, &PBISTResult);
                if (PBISTResult != true)
                {
                    /* HW POST: PBIST was completed, but the test failed */
#if defined(DEBUG) && defined(PROFILE_PBIST_TIMING)
                    postStatus = PBIST_POST_COMPLETED_FAILURE;
#endif
                    testResult = -1;
                    UART_printf("\n HW POST: PBIST test failed\n");
                }
            } /* if (PBISTResult != true) */
        } /* if (status != CSL_PASS) */
    } /* if ((testResult == 0) &&
       *     (PBIST_TestHandleArray[instanceId].numPostPbistToCheck > 0)) */

    /* Step 2: (if SW-initiated PBIST) Configure processor to correct state  */

    /**--- Step 2a: Request Primary core ---*/
    if ((testResult == 0) && (PBIST_TestHandleArray[instanceId].procRstNeeded))
    {
        if (PBIST_TestHandleArray[instanceId].tisciProcId != 0u)
        {
#ifdef DEBUG
            AppUtils_Printf(MSG_NORMAL,
                            "  Primary core: %s: Requesting processor \n",
                            PBIST_TestHandleArray[instanceId].coreName);
#endif
            /* Request Primary core */
            status = Sciclient_procBootRequestProcessor(PBIST_TestHandleArray[instanceId].tisciProcId,
                                                        SCICLIENT_SERVICE_WAIT_FOREVER);
            if (status != CSL_PASS)
            {
                AppUtils_Printf(MSG_NORMAL,
                                "   Primary core: Sciclient_procBootRequestProcessor, ProcId 0x%x...FAILED \n",
                                PBIST_TestHandleArray[instanceId].tisciProcId);
                testResult = -1;
            }
        }
    }

    /**--- Step 2b: Request Secondary core ---*/
    if ((testResult == 0) && (PBIST_TestHandleArray[instanceId].procRstNeeded))
    {
        if ((PBIST_TestHandleArray[instanceId].secondaryCoreNeeded)
            && (PBIST_TestHandleArray[instanceId].tisciSecProcId != 0u))
        {

#ifdef DEBUG
            AppUtils_Printf(MSG_NORMAL,
                            "  Secondary core: %s: Requesting processor \n",
                            PBIST_TestHandleArray[instanceId].secCoreName);
#endif
            /* Request secondary core */
            status = Sciclient_procBootRequestProcessor(PBIST_TestHandleArray[instanceId].tisciSecProcId,
                                                        SCICLIENT_SERVICE_WAIT_FOREVER);
            if (status != CSL_PASS)
            {
                AppUtils_Printf(MSG_NORMAL,
                                "   Secondary core: Sciclient_procBootRequestProcessor, ProcId 0x%x...FAILED \n",
                                PBIST_TestHandleArray[instanceId].tisciSecProcId);
                testResult = -1;
            }
        }
    }

    /**--- Step 2c: Put Primary core in local reset ---*/
    if ((testResult == 0) && (PBIST_TestHandleArray[instanceId].procRstNeeded))
    {
        if (PBIST_TestHandleArray[instanceId].tisciDeviceId != 0u)
        {
            /* Set Local reset for Primary core */
#ifdef DEBUG
            AppUtils_Printf(MSG_NORMAL,
                            "  %s: Primary core: Set module reset \n",
                            PBIST_TestHandleArray[instanceId].coreName);
#endif
            status =  Sciclient_pmSetModuleRst(PBIST_TestHandleArray[instanceId].tisciDeviceId,
                                               0x1, /* Local Reset asserted */
                                               SCICLIENT_SERVICE_WAIT_FOREVER);
            if (status != CSL_PASS)
            {
                AppUtils_Printf(MSG_NORMAL,
                                "   Primary core: Sciclient_pmSetModuleRst...FAILED \n");
                testResult = -1;
            }
        }
    }

    /**--- Step 2d: Put Secondary core in local reset ---*/
    if ((testResult == 0) && (PBIST_TestHandleArray[instanceId].procRstNeeded))
    {
        if ((PBIST_TestHandleArray[instanceId].secondaryCoreNeeded)
            && (PBIST_TestHandleArray[instanceId].tisciSecDeviceId != 0u))
        {
            /* Set Local reset for Secondary core */
#ifdef DEBUG
            AppUtils_Printf(MSG_NORMAL,
                            "  %s: Secondary core: Set Module reset \n",
                            PBIST_TestHandleArray[instanceId].secCoreName);
#endif
            status =  Sciclient_pmSetModuleRst(PBIST_TestHandleArray[instanceId].tisciSecDeviceId,
                                               0x1, /* Local Reset asserted */
                                               SCICLIENT_SERVICE_WAIT_FOREVER);
            if (status != CSL_PASS)
            {
                AppUtils_Printf(MSG_NORMAL,
                                "   Secondary core: Sciclient_pmSetModuleRst...FAILED \n");
                testResult = -1;
            }
        }
    }
#ifdef POWERUP_CORES_BEFORE_TEST
    /**--- Step 2e: Perform any additional power sequencing, if needed ---*/
    /* Custom core power restore sequence - needed to allow core to be powered
     * up later by Secondary Bootloader (SBL) */
    if ((testResult == 0) &&
        (PBIST_TestHandleArray[instanceId].coreCustPwrSeqNeeded) &&
        (PBIST_TestHandleArray[instanceId].tisciProcId != 0u))
    {
        status = customPrepareForPowerUpSequence(PBIST_TestHandleArray[instanceId].tisciProcId);
        if (status != CSL_PASS)
        {
            UART_printf("  Custom core power restore sequence, ProcId 0x%x ...FAILED \n",
                        PBIST_TestHandleArray[instanceId].tisciProcId);
            testResult = -1;
        }
    }

    /**--- Step 2f: Power up of Auxilliary modules needed to run test */
    if (testResult == 0)
    {
        /* Power all modules required for test */
        for ( i = 0; i < PBIST_TestHandleArray[instanceId].numAuxDevices; i++)
        {
#ifdef DEBUG
        AppUtils_Printf(MSG_NORMAL,
                        "  Powering on Device number %d Device Id %x\n",
                        i, PBIST_TestHandleArray[instanceId].auxDeviceIdsP[i]);
#endif

            status = Sciclient_pmSetModuleState(PBIST_TestHandleArray[instanceId].auxDeviceIdsP[i],
                                                TISCI_MSG_VALUE_DEVICE_SW_STATE_ON,
                                                TISCI_MSG_FLAG_AOP,
                                                SCICLIENT_SERVICE_WAIT_FOREVER);
            if (status != CSL_PASS)
            {
                UART_printf("  Sciclient_pmSetModuleState 0x%x ...FAILED \n",
                            PBIST_TestHandleArray[instanceId].auxDeviceIdsP[i]);
                testResult = -1;
                break;
            }
        }
    }

    /**--- Step 2g: Power up Primary core */
    if ((testResult == 0) && (PBIST_TestHandleArray[instanceId].procRstNeeded)
                          && (PBIST_TestHandleArray[instanceId].tisciDeviceId != NULL))
    {
        /* power on Primary core*/
#ifdef DEBUG
        AppUtils_Printf(MSG_NORMAL,
                        "  Primary core: Powering on %s \n",
                        PBIST_TestHandleArray[instanceId].coreName);
#endif
        status = Sciclient_pmSetModuleState(PBIST_TestHandleArray[instanceId].tisciDeviceId,
                                            TISCI_MSG_VALUE_DEVICE_SW_STATE_ON,
                                            TISCI_MSG_FLAG_AOP,
                                            SCICLIENT_SERVICE_WAIT_FOREVER);

        if (status != CSL_PASS)
        {
            AppUtils_Printf(MSG_NORMAL,
                            "   Primary core: Sciclient_pmSetModuleState 0x%x ...FAILED \n",
                            PBIST_TestHandleArray[instanceId].tisciDeviceId);
            testResult = -1;
        }
    }

    /**--- Step 2h: Power up Secondary core */
    if ((testResult == 0) && (PBIST_TestHandleArray[instanceId].procRstNeeded)
                          && (PBIST_TestHandleArray[instanceId].tisciSecDeviceId != NULL))
    {
        if (PBIST_TestHandleArray[instanceId].secondaryCoreNeeded)
        {
            /* power on Secondary core*/
#ifdef DEBUG
            AppUtils_Printf(MSG_NORMAL,
                            "  Secondary core: Powering on %s \n",
                            PBIST_TestHandleArray[instanceId].secCoreName);
#endif
            status = Sciclient_pmSetModuleState(PBIST_TestHandleArray[instanceId].tisciSecDeviceId,
                                                TISCI_MSG_VALUE_DEVICE_SW_STATE_ON,
                                                TISCI_MSG_FLAG_AOP,
                                                SCICLIENT_SERVICE_WAIT_FOREVER);
            if (status != CSL_PASS)
            {
                AppUtils_Printf(MSG_NORMAL,
                                "   Secondary core: Sciclient_pmSetModuleState 0x%x ...FAILED \n",
                                PBIST_TestHandleArray[instanceId].tisciSecDeviceId);
                testResult = -1;
                return testResult;
            }
        }
    }

    /**--- Step 2i: Double check the Power up of Auxilliary modules needed to run test
     * and wait until they are powered up */
    if (testResult == 0)
    {
        /* Wait for all modules required for test to be powered up */
        for ( i = 0; i < PBIST_TestHandleArray[instanceId].numAuxDevices; i++)
        {
#ifdef DEBUG
        AppUtils_Printf(MSG_NORMAL,
                        "  Double checking Powering on Device number %d Device Id %x\n",
                        i, PBIST_TestHandleArray[instanceId].auxDeviceIdsP[i]);
#endif
            do
            {
                status = Sciclient_pmGetModuleState(PBIST_TestHandleArray[instanceId].auxDeviceIdsP[i],
                                                    &moduleState,
                                                    &resetState,
                                                    &contextLossState,
                                                    SCICLIENT_SERVICE_WAIT_FOREVER);
                if (status != CSL_PASS)
                {
                    UART_printf("  Sciclient_pmGetModuleState 0x%x ...FAILED \n",
                                PBIST_TestHandleArray[instanceId].auxDeviceIdsP[i]);
                    testResult = -1;
                    break;
                }
            } while (moduleState != TISCI_MSG_VALUE_DEVICE_HW_STATE_ON);
        }
    }

    /**--- Step 2j: Double check the Power up of Primary core and wait until it is
     * powered up */
    if ((testResult == 0) && (PBIST_TestHandleArray[instanceId].procRstNeeded)
                          && (PBIST_TestHandleArray[instanceId].tisciDeviceId != NULL))
    {
        /* Double check power on Primary core*/
#ifdef DEBUG
        AppUtils_Printf(MSG_NORMAL,
                        "  Primary core: Double checking Powering on %s \n",
                        PBIST_TestHandleArray[instanceId].coreName);
#endif
        do
        {
            status = Sciclient_pmGetModuleState(PBIST_TestHandleArray[instanceId].tisciDeviceId,
                                                &moduleState,
                                                &resetState,
                                                &contextLossState,
                                                SCICLIENT_SERVICE_WAIT_FOREVER);
            if (status != CSL_PASS)
            {
                UART_printf("   Primary core: Sciclient_pmGetModuleState 0x%x ...FAILED \n",
                            PBIST_TestHandleArray[instanceId].tisciDeviceId);
                testResult = -1;
                break;
            }
        } while (moduleState != TISCI_MSG_VALUE_DEVICE_HW_STATE_ON);
    }

    /**--- Step 2k: Double check the Power up of Primary core and wait until it is
     * powered up */
    if ((testResult == 0) && (PBIST_TestHandleArray[instanceId].procRstNeeded)
                          && (PBIST_TestHandleArray[instanceId].tisciSecDeviceId != NULL))
    {
        if (PBIST_TestHandleArray[instanceId].secondaryCoreNeeded)
        {
            /* Double check power on Secondary core*/
#ifdef DEBUG
            AppUtils_Printf(MSG_NORMAL,
                            "  Secondary core: Double checking Powering on %s \n",
                            PBIST_TestHandleArray[instanceId].coreName);
#endif
            do
            {
                status = Sciclient_pmGetModuleState(PBIST_TestHandleArray[instanceId].tisciSecDeviceId,
                                                    &moduleState,
                                                    &resetState,
                                                    &contextLossState,
                                                    SCICLIENT_SERVICE_WAIT_FOREVER);
                if (status != CSL_PASS)
                {
                    UART_printf("   Secondary core: Sciclient_pmGetModuleState 0x%x ...FAILED \n",
                                PBIST_TestHandleArray[instanceId].tisciSecDeviceId);
                    testResult = -1;
                    break;
                }
            } while (moduleState != TISCI_MSG_VALUE_DEVICE_HW_STATE_ON);
        }
    }

#endif /* #ifdef POWERUP_CORES_BEFORE_TEST */
    /**--- Step 2l: Power up PBIST */
    if ((testResult == 0) && (PBIST_TestHandleArray[instanceId].tisciPBISTDeviceId != 0u))
    {
#ifdef DEBUG
        AppUtils_Printf(MSG_NORMAL,
                        "  Powering on PBIST %d \n",
                        PBIST_TestHandleArray[instanceId].tisciPBISTDeviceId);
#endif
        status = Sciclient_pmSetModuleState(PBIST_TestHandleArray[instanceId].tisciPBISTDeviceId,
                                            TISCI_MSG_VALUE_DEVICE_SW_STATE_ON,
                                            TISCI_MSG_FLAG_AOP,
                                            SCICLIENT_SERVICE_WAIT_FOREVER);

        if (status != CSL_PASS)
        {
            AppUtils_Printf(MSG_NORMAL,
                            "   PBIST Sciclient_pmSetModuleState 0x%x ...FAILED: retValue %d\n",
                            PBIST_TestHandleArray[instanceId].tisciPBISTDeviceId, status);
            testResult = -1;
        }
    }

    /**--- Step 2m: Execute Auxialliary init function for any final core-internal register
     * setup needed for the PBIST test */
    if (testResult == 0)
    {
        if (PBIST_TestHandleArray[instanceId].auxInitRestoreFunction != 0)
        {
            status = PBIST_TestHandleArray[instanceId].auxInitRestoreFunction(TRUE);
            if (status != CSL_PASS)
            {
                testResult = -1;
            }
        }
    }

    /**--- Step 2n: Get PBIST register space Pointer */
    pPBISTRegs = PBIST_TestHandleArray[instanceId].pPBISTRegs;

    if ((testResult == 0) && (PBIST_TestHandleArray[instanceId].PBISTRegsHiAddress != 0u))
    {
        /* Add RAT configuration to access address > 32bit address range */
        translationCfg.translatedAddress = PBIST_TestHandleArray[instanceId].PBISTRegsHiAddress;
        translationCfg.sizeInBytes = PBIST_REG_REGION_SIZE;
        translationCfg.baseAddress = (uint32_t)pPBISTRegs;

        /* Set up RAT translation */
        result = CSL_ratConfigRegionTranslation((CSL_ratRegs *)CSL_MCU_ARMSS_RAT_CFG_BASE,
                                                PBIST_RAT_REGION_INDEX, &translationCfg);
        if (result == false) {
            AppUtils_Printf(MSG_NORMAL, "   CSL_ratConfigRegionTranslation...FAILED \n");
            testResult = -1;
        }
    }

    PBIST_TestHandleArray[instanceId].doneFlag = false;

#if defined(PROFILE_PBIST_TIMING)
    /* Get start time for PBIST test */
    testStartTime = TimerP_getTimeInUsecs();
#endif

    /**-- Step 3: Run PBIST test (the test may have multiple passes as designated
     * by the particular SOC, see soc/<SOC Device>/pbist_defs.c for details. --*/
    for (i = 0; i < PBIST_TestHandleArray[instanceId].numPBISTRuns; i++)
    {
        /**--- Step 3a: Start the PBIST test */
#ifdef DEBUG
        AppUtils_Printf(MSG_NORMAL, "\n Starting PBIST Run %d for Instance %s\n",
                        i, pbistName(instanceId));
#endif
        if (testResult == 0)
        {
#ifdef DEBUG
            AppUtils_Printf(MSG_NORMAL, "\n Starting CSL_PBIST_start in PBIST Run %d\n", i);
#endif
            status = CSL_PBIST_start(pPBISTRegs, &PBIST_TestHandleArray[instanceId].PBISTConfigRun[i]);
            if (status != CSL_PASS)
            {
                AppUtils_Printf(MSG_NORMAL, " CSL_PBIST_start failed in PBIST Run %d\n", i);
                testResult = -1;
            }
        }

        /**--- Step 3b: Wait for interrupt indicating completion of the PBIST test */
        if (testResult == 0)
        {
            /* Timeout if exceeds time */
            while ((!PBIST_TestHandleArray[instanceId].doneFlag)
                   && (timeoutCount++ < PBIST_MAX_TIMEOUT_VALUE));

            if (!(PBIST_TestHandleArray[instanceId].doneFlag))
            {
                AppUtils_Printf(MSG_NORMAL, " PBIST test timed out in PBIST Run %d\n", i);
                testResult = -1;
            }
            /* reset Done flag so we can run again */
            PBIST_TestHandleArray[instanceId].doneFlag = false;
        }

        /**--- Step 3c: Check the result of the PBIST test */
        if (testResult == 0)
        {
#ifdef DEBUG
            AppUtils_Printf(MSG_NORMAL, "\n Starting CSL_PBIST_checkResult in PBIST Run %d\n", i);
#endif
            status = CSL_PBIST_checkResult(pPBISTRegs, &PBISTResult);
            if (status != CSL_PASS)
            {
                AppUtils_Printf(MSG_NORMAL, " CSL_PBIST_checkResult failed in PBIST Run %d\n", i);
                testResult = -1;
            }
            else
            {
                /* Check the PBIST result */
                if (PBISTResult != true)
                {
                    AppUtils_Printf(MSG_NORMAL, "\n PBIST test failed in PBIST Run %d\n", i);
                    testResult = -1;
                }
            }
        }

        /**--- Step 3d: Do a Soft Reset */
        if (testResult == 0)
        {
#ifdef DEBUG
            UART_printf("\n Starting CSL_PBIST_softReset \n");
#endif

            /* Run PBIST test */
            status = CSL_PBIST_softReset(pPBISTRegs);
            if (status != CSL_PASS)
            {
                UART_printf(" CSL_PBIST_softReset failed \n");
                testResult = -1;
            }
        }

        /**--- Step 3e: Execute exit sequence */
        if (testResult == 0)
        {
#ifdef DEBUG
            UART_printf("\n Starting CSL_PBIST_releaseTestMode \n");
#endif
            /* Exit PBIST test */
            status = CSL_PBIST_releaseTestMode(pPBISTRegs);
            if (status != CSL_PASS)
            {
                UART_printf(" CSL_PBIST_releaseTestMode failed \n");
                testResult = -1;
            }
        }
    } /* for (i = 0; i < PBIST_TestHandleArray[instanceId].numPBISTRuns; i++) */

#if defined(PROFILE_PBIST_TIMING)
    /* Record test end time */
    testEndTime = TimerP_getTimeInUsecs();
#endif

    /**-- Step 4: Restore cores --*/

    /**--- Step 4a: Execute Auxilliary restore function to restore core-internal
     * registers to original state */
    if (testResult == 0)
    {

        if (PBIST_TestHandleArray[instanceId].auxInitRestoreFunction != 0)
        {
            status = PBIST_TestHandleArray[instanceId].auxInitRestoreFunction(FALSE);
            if (status != CSL_PASS)
            {
                testResult = -1;
            }
        }

    }

    /**--- Step 4b: Power off PBIST */
    if ((testResult == 0) && (PBIST_TestHandleArray[instanceId].tisciPBISTDeviceId != 0u))
    {
#ifdef DEBUG
        AppUtils_Printf(MSG_NORMAL,
                        "  Powering off PBIST %d \n",
                        PBIST_TestHandleArray[instanceId].tisciPBISTDeviceId);
#endif
        status = Sciclient_pmSetModuleState(PBIST_TestHandleArray[instanceId].tisciPBISTDeviceId,
                                            TISCI_MSG_VALUE_DEVICE_SW_STATE_AUTO_OFF,
                                            TISCI_MSG_FLAG_AOP,
                                            SCICLIENT_SERVICE_WAIT_FOREVER);

        if (status != CSL_PASS)
        {
            AppUtils_Printf(MSG_NORMAL,
                            "   PBIST Sciclient_pmSetModuleState 0x%x ...FAILED \n",
                            PBIST_TestHandleArray[instanceId].tisciPBISTDeviceId);
            testResult = -1;
        }
    }

#ifdef POWERUP_CORES_BEFORE_TEST
    /**--- Step 4c: Power off Secondary core */
    if ((testResult == 0) && (PBIST_TestHandleArray[instanceId].procRstNeeded)
                          && (PBIST_TestHandleArray[instanceId].tisciSecDeviceId != NULL))
    {
        if (PBIST_TestHandleArray[instanceId].secondaryCoreNeeded)
        {
            /* power off Secondary core*/
#ifdef DEBUG
            AppUtils_Printf(MSG_NORMAL,
                            "  Secondary core: Powering off %s \n",
                            PBIST_TestHandleArray[instanceId].secCoreName);
#endif
            status = Sciclient_pmSetModuleState(PBIST_TestHandleArray[instanceId].tisciSecDeviceId,
                                                TISCI_MSG_VALUE_DEVICE_SW_STATE_AUTO_OFF,
                                                TISCI_MSG_FLAG_AOP,
                                                SCICLIENT_SERVICE_WAIT_FOREVER);
            if (status != CSL_PASS)
            {
                AppUtils_Printf(MSG_NORMAL,
                                "   Secondary core: Sciclient_pmSetModuleState Power off 0x%x ...FAILED \n",
                                PBIST_TestHandleArray[instanceId].tisciSecDeviceId);
                testResult = -1;
                return testResult;
            }
        }
    }

    /**--- Step 4d: Perform any custom/core-specific power down sequence */
    if ((testResult == 0) &&
        (PBIST_TestHandleArray[instanceId].coreCustPwrSeqNeeded) &&
        (PBIST_TestHandleArray[instanceId].tisciProcId != 0u))
    {
        status = customPowerDownSequence(PBIST_TestHandleArray[instanceId].tisciProcId);
        if (status != CSL_PASS)
        {
            UART_printf("  Custom core power down sequence, ProcId 0x%x ...FAILED \n",
                        PBIST_TestHandleArray[instanceId].tisciProcId);
            testResult = -1;
        }
    }

    /**--- Step 4e: Power off Primary core */
    if ((testResult == 0) && (PBIST_TestHandleArray[instanceId].tisciProcId != 0u)
                    && (PBIST_TestHandleArray[instanceId].procRstNeeded))
    {
        /* power off Primary core*/
#ifdef DEBUG
        AppUtils_Printf(MSG_NORMAL,
                        "  Primary core: Powering off %s \n",
                        PBIST_TestHandleArray[instanceId].coreName);
#endif
        status = Sciclient_pmSetModuleState(PBIST_TestHandleArray[instanceId].tisciDeviceId,
                                            TISCI_MSG_VALUE_DEVICE_SW_STATE_AUTO_OFF,
                                            TISCI_MSG_FLAG_AOP,
                                            SCICLIENT_SERVICE_WAIT_FOREVER);

        if (status != CSL_PASS)
        {
            AppUtils_Printf(MSG_NORMAL,
                            "   Primary core: Sciclient_pmSetModuleState Power off 0x%x ...FAILED \n",
                            PBIST_TestHandleArray[instanceId].tisciDeviceId);
            testResult = -1;
        }
    }

    /**--- Step 4f: Power off of Auxilliary modules needed to run test */
    if (testResult == 0)
    {
        /* Power all modules required for test */
        for ( i = 0; i < PBIST_TestHandleArray[instanceId].numAuxDevices; i++)
        {
#ifdef DEBUG
            AppUtils_Printf(MSG_NORMAL,
                            "  Powering off Device number %d Device Id %x\n",
                            i, PBIST_TestHandleArray[instanceId].auxDeviceIdsP[i]);
#endif
            status = Sciclient_pmSetModuleState(PBIST_TestHandleArray[instanceId].auxDeviceIdsP[i],
                                                TISCI_MSG_VALUE_DEVICE_SW_STATE_AUTO_OFF,
                                                TISCI_MSG_FLAG_AOP,
                                                SCICLIENT_SERVICE_WAIT_FOREVER);
            if (status != CSL_PASS)
            {
                UART_printf("  Sciclient_pmSetModuleState 0x%x ...FAILED \n",
                            PBIST_TestHandleArray[instanceId].auxDeviceIdsP[i]);
                testResult = -1;
                break;
            }
        }
    }

    /**--- Step 4g: Perform any custom/core-specific power restore sequence needed to
     * allow core to be powered up properly later */
    if ((testResult == 0) &&
        (PBIST_TestHandleArray[instanceId].coreCustPwrSeqNeeded) &&
        (PBIST_TestHandleArray[instanceId].tisciProcId != 0u))
    {
        status = customPrepareForPowerUpSequence(PBIST_TestHandleArray[instanceId].tisciProcId);
        if (status != CSL_PASS)
        {
            UART_printf("  Custom core power restore sequence, ProcId 0x%x ...FAILED \n",
                        PBIST_TestHandleArray[instanceId].tisciProcId);
            testResult = -1;
        }
    }

    /**--- Step 4h: Take Primary core out of local reset */
    if ((testResult == 0) && (PBIST_TestHandleArray[instanceId].procRstNeeded)
                          && (PBIST_TestHandleArray[instanceId].tisciDeviceId != NULL))
    {
#ifdef DEBUG
        AppUtils_Printf(MSG_NORMAL,
                        "  Primary core: Taking out of local reset the core %s \n",
                        PBIST_TestHandleArray[instanceId].coreName);
#endif
        status = Sciclient_pmSetModuleRst(PBIST_TestHandleArray[instanceId].tisciDeviceId,
                                          0x0, /* Local Reset de-asserted */
                                          SCICLIENT_SERVICE_WAIT_FOREVER);
        if (status != CSL_PASS)
        {
             UART_printf("  Sciclient_pmSetModuleRst 0x%x ...FAILED \n",
                         PBIST_TestHandleArray[instanceId].tisciDeviceId);
             testResult = -1;
        }
    }

    /**--- Step 4i: Take Secondary core out of local reset */
    if ((testResult == 0) && (PBIST_TestHandleArray[instanceId].procRstNeeded)
                          && (PBIST_TestHandleArray[instanceId].tisciSecDeviceId != NULL))
    {
#ifdef DEBUG
        AppUtils_Printf(MSG_NORMAL,
                        "  Secondary core: Taking out of local reset the core %s \n",
                        PBIST_TestHandleArray[instanceId].secCoreName);
#endif
        status = Sciclient_pmSetModuleRst(PBIST_TestHandleArray[instanceId].tisciSecProcId,
                                          0x0, /* Local Reset de-asserted */
                                          SCICLIENT_SERVICE_WAIT_FOREVER);
        if (status != CSL_PASS)
        {
             UART_printf("  Sciclient_pmSetModuleRst 0x%x ...FAILED \n",
                         PBIST_TestHandleArray[instanceId].tisciSecDeviceId);
             testResult = -1;
        }
    }
#endif /* #ifdef POWERUP_CORES_BEFORE_TEST */
    /* Ensure that cores have been turned off to prepare for booting of the cores */

    /**--- Step 4j: Power off Primary core */
    if ((testResult == 0) && (PBIST_TestHandleArray[instanceId].procRstNeeded))
    {
        if (PBIST_TestHandleArray[instanceId].tisciDeviceId != 0u)
        {
            /* Set Software Reset Disable State for Primary core */
#ifdef DEBUG
            AppUtils_Printf(MSG_NORMAL,
                            "  %s: Primary core: Put in Software Reset Disable \n",
                            PBIST_TestHandleArray[instanceId].coreName);
#endif
            status =  Sciclient_pmSetModuleState(PBIST_TestHandleArray[instanceId].tisciDeviceId,
                                                 TISCI_MSG_VALUE_DEVICE_SW_STATE_AUTO_OFF,
                                                 TISCI_MSG_FLAG_AOP,
                                                 SCICLIENT_SERVICE_WAIT_FOREVER);
            if (status != CSL_PASS)
            {
                AppUtils_Printf(MSG_NORMAL,
                                "   Primary core: Sciclient_pmSetModuleState...FAILED \n");
                testResult = -1;
            }
        }
    }

    /**--- Step 4k: Power off Secondary core */
    if ((testResult == 0) && (PBIST_TestHandleArray[instanceId].procRstNeeded))
    {
        if ((PBIST_TestHandleArray[instanceId].secondaryCoreNeeded)
            && (PBIST_TestHandleArray[instanceId].tisciSecDeviceId != 0u))
        {
            /* Set Software Reset Disable State for Secondary core */
#ifdef DEBUG
            AppUtils_Printf(MSG_NORMAL,
                            "  %s: Secondary Core Put in Software Reset Disable \n",
                            PBIST_TestHandleArray[instanceId].secCoreName);
#endif
            status =  Sciclient_pmSetModuleState(PBIST_TestHandleArray[instanceId].tisciSecDeviceId,
                                                 TISCI_MSG_VALUE_DEVICE_SW_STATE_AUTO_OFF,
                                                 TISCI_MSG_FLAG_AOP,
                                                 SCICLIENT_SERVICE_WAIT_FOREVER);
            if (status != CSL_PASS)
            {
                AppUtils_Printf(MSG_NORMAL,
                                "   Secondary core Sciclient_pmSetModuleState...FAILED \n");
                testResult = -1;
            }
        }
    }

    /**--- Step 4l: Disable RAT translation */
    if ((testResult == 0) && (PBIST_TestHandleArray[instanceId].PBISTRegsHiAddress != 0u))
    {
        /* Disable RAT translation */
        result = CSL_ratDisableRegionTranslation((CSL_ratRegs *)CSL_MCU_ARMSS_RAT_CFG_BASE,
                                                PBIST_RAT_REGION_INDEX);
        if (result == false) {
            AppUtils_Printf(MSG_NORMAL, "   CSL_ratDisableRegionTranslation...FAILED \n");
            testResult = -1;
        }
    }

    /**--- Step 4m: Release Primary core */
    if ((testResult == 0) && (PBIST_TestHandleArray[instanceId].tisciProcId != 0u)
            && (PBIST_TestHandleArray[instanceId].procRstNeeded))
    {
        /* release processor Primary core */
#ifdef DEBUG
        AppUtils_Printf(MSG_NORMAL,
                        "  Primary core: Releasing %s \n",
                        PBIST_TestHandleArray[instanceId].coreName);
#endif

        status = Sciclient_procBootReleaseProcessor(PBIST_TestHandleArray[instanceId].tisciProcId,
                                                    TISCI_MSG_FLAG_AOP,
                                                    SCICLIENT_SERVICE_WAIT_FOREVER);
        if (status != CSL_PASS)
        {
            AppUtils_Printf(MSG_NORMAL,
                            "   Primary core: Sciclient_procBootReleaseProcessor, ProcId 0x%x...FAILED \n",
                            PBIST_TestHandleArray[instanceId].tisciProcId);
            testResult = -1;
        }
    }    

    /**--- Step 4n: Release Secondary core */
    if (testResult == 0)
    {
        if ((PBIST_TestHandleArray[instanceId].secondaryCoreNeeded)
            && (PBIST_TestHandleArray[instanceId].tisciSecProcId != 0u)
            && (PBIST_TestHandleArray[instanceId].procRstNeeded))
        {
            /* release processor Secondary core */
#ifdef DEBUG
            AppUtils_Printf(MSG_NORMAL,
                            "  Secondary core: Releasing %s \n",
                            PBIST_TestHandleArray[instanceId].secCoreName);
#endif
            status = Sciclient_procBootReleaseProcessor(PBIST_TestHandleArray[instanceId].tisciSecProcId,
                                                        TISCI_MSG_FLAG_AOP,
                                                        SCICLIENT_SERVICE_WAIT_FOREVER);
            if (status != CSL_PASS)
            {
                AppUtils_Printf(MSG_NORMAL,
                                "   Secondary core: Sciclient_procBootReleaseProcessor, ProcId 0x%x...FAILED \n",
                                PBIST_TestHandleArray[instanceId].tisciSecProcId);
                testResult = -1;
            }
        }
    }

#if defined(PROFILE_PBIST_TIMING)
    /* Record end time */
    endTime = TimerP_getTimeInUsecs();

    prepTime = testStartTime - startTime;
    diffTime = testEndTime - testStartTime;
    restoreTime = endTime - testEndTime;
    AppUtils_Printf(MSG_NORMAL,
                    "  Delta Cores prep time in micro secs %d \n",
                    (uint32_t)prepTime);
    AppUtils_Printf(MSG_NORMAL,
                    "  Delta PBIST execution time in micro secs %d \n",
                    (uint32_t)diffTime);
    AppUtils_Printf(MSG_NORMAL,
                    "  Delta Cores restore time in micro secs %d \n",
                    (uint32_t)restoreTime);
    AppUtils_Printf(MSG_NORMAL,
                    " PBIST complete %s, test index %d\n",
                    PBIST_TestHandleArray[instanceId].testName,
                    instanceId);
#if defined(DEBUG)
    if (PBIST_TestHandleArray[instanceId].numPostPbistToCheck > 0)
    {
        switch(postStatus)
        {
            case PBIST_POST_COMPLETED_FAILURE:
                UART_printf("\n HW POST: PBIST test failed\n");
                break;

            case PBIST_POST_ATTEMPTED_TIMEOUT:
                UART_printf("\n HW POST: PBIST was attempted but timed out\n");
                break;

            case PBIST_POST_NOT_RUN:
                UART_printf("\n HW POST: PBIST was not performed on this device\n");
                break;

            case PBIST_POST_COMPLETED_SUCCESS:
            default:
                UART_printf("\n HW POST: PBIST ran and succeeded\n");
                break;
        }
    }
#endif
#endif

    return (testResult);
}

/* HW POST-related functions */
static int32_t PBIST_isPostPbistTimeout(uint32_t postStatMmrRegVal, Bool *pIsTimedOut)
{
    int32_t status = CSL_PASS;

    if (pIsTimedOut == NULL)
    {
        status = CSL_EBADARGS;
    }
    else
    {
        *pIsTimedOut = ((postStatMmrRegVal >>
                         CSL_WKUP_CTRL_MMR_CFG0_WKUP_POST_STAT_POST_MCU_PBIST_TIMEOUT_SHIFT) &
                         0x1u) ? CSL_TRUE : CSL_FALSE;
    }
    return status;
}

static int32_t PBIST_isPostPbistDone(uint32_t postStatMmrRegVal, Bool *pIsDone)
{
    int32_t status = CSL_PASS;

    if (pIsDone == NULL)
    {
        status = CSL_EBADARGS;
    }
    else
    {
        *pIsDone = ((postStatMmrRegVal >>
                    CSL_WKUP_CTRL_MMR_CFG0_WKUP_POST_STAT_POST_MCU_PBIST_DONE_SHIFT) &
                    0x1u) ? CSL_TRUE : CSL_FALSE;
    }
    return status;
}

static int32_t PBIST_postCheckResult(uint32_t postStatMmrRegVal, Bool *pResult)
{
    int32_t cslResult= CSL_PASS;

    if(pResult == NULL)
    {
        cslResult = CSL_EFAIL;
    }
    else
    {
        if ((postStatMmrRegVal &
            CSL_WKUP_CTRL_MMR_CFG0_WKUP_POST_STAT_POST_MCU_PBIST_FAIL_MASK) ==
            ((uint32_t)0x00000000u))
        {
            *pResult = CSL_TRUE;
        }
        else
        {
            *pResult = CSL_FALSE;
        }
    }

    return cslResult;
}


