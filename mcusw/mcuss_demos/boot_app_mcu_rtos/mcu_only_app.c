/*
*
* Copyright (c) 2020 Texas Instruments Incorporated
*
* All rights reserved not granted herein.
*
* Limited License.
*
* Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
* license under copyrights and patents it now or hereafter owns or controls to make,
* have made, use, import, offer to sell and sell ("Utilize") this software subject to the
* terms herein.  With respect to the foregoing patent license, such license is granted
* solely to the extent that any such patent is necessary to Utilize the software alone.
* The patent license shall not apply to any combinations which include this software,
* other than combinations with devices manufactured by or for TI ("TI Devices").
* No hardware patent is licensed hereunder.
*
* Redistributions must preserve existing copyright notices and reproduce this license
* (including the above copyright notice and the disclaimer and (if applicable) source
* code license limitations below) in the documentation and/or other materials provided
* with the distribution
*
* Redistribution and use in binary form, without modification, are permitted provided
* that the following conditions are met:
*
* *       No reverse engineering, decompilation, or disassembly of this software is
* permitted with respect to any software provided in binary form.
*
* *       any redistribution and use are licensed by TI for use only with TI Devices.
*
* *       Nothing shall obligate TI to provide you with source code for the software
* licensed and provided to you in object code.
*
* If software source code is provided to you, modification and redistribution of the
* source code are permitted provided that the following conditions are met:
*
* *       any redistribution and use of the source code, including any resulting derivative
* works, are licensed by TI for use only with TI Devices.
*
* *       any redistribution and use of any object code compiled from the source code
* and any resulting derivative works, are licensed by TI for use only with TI Devices.
*
* Neither the name of Texas Instruments Incorporated nor the names of its suppliers
*
* may be used to endorse or promote products derived from this software without
* specific prior written permission.
*
* DISCLAIMER.
*
* THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
* OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
* OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
* IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
* BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
* OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
* OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
* OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/
/**
 *  \file     mcu_only_app.c
 *
 *  \brief    This file implements switching the SoCs state from ACTIVE to
 *            MCU ONLY mode and then from MCU ONLY to ACTIVE mode.
 */

/* ========================================================================== */
/*                             Include Files                                  */
/* ========================================================================== */

#include "Std_Types.h"
#include <stdio.h>

#include <ti/csl/cslr_gtc.h>

#include <ti/drv/spi/soc/SPI_soc.h>
#include <ti/board/board.h>
#include <ti/board/board_cfg.h>
#include <ti/board/src/flash/include/board_flash.h>
#if defined(SOC_J721E)
#include <ti/board/src/j721e_evm/include/board_control.h>
#endif
#if defined(SOC_J7200)
#include <ti/board/src/j7200_evm/include/board_control.h>
#endif

#include "boot_cfg.h"
#include "app_utils.h"
#include "mcu_timer_multicore.h"

/* ========================================================================== */
/*                           Macros & Typedefs                                */
/* ========================================================================== */
#if defined(MCU_ONLY_TASK_ENABLED)
#if defined(VTM_OPS_ENABLED)
#define WKUP_VTM_TMPSENS_CTRL_1        (0x42050320)
#define WKUP_VTM_TMPSENS_CTRL_2        (0x42050340)
#define WKUP_VTM_TMPSENS_CTRL_3        (0x42050360)
#define WKUP_VTM_TMPSENS_CTRL_4        (0x42050380)

#define MAXT_OUTRG_EN_SHIFT            (11U)
#endif

#define DDR_TEST_ADDRESS               (0xA0000000)
#define MSMC_TEST_ADDRESS              (0x70000000)
#define MCAN_REV_REG                   (0x02700000)
#endif

#define MSG_APP_NAME                   "MCU only app: "
/* ========================================================================== */
/*                         Structures and Enums                               */
/* ========================================================================== */

#if defined(MCU_ONLY_TASK_ENABLED)
struct bootApp_boardcfg_rm_resasg
{
    struct tisci_boardcfg_substructure_header subhdr;
    uint16_t                    resasg_entries_size;
    uint16_t                    reserved;
} __attribute__((__packed__));

struct bootApp_boardcfg_rm
{
    struct tisci_boardcfg_abi_rev     rev;
    struct tisci_boardcfg_rm_host_cfg host_cfg;
    struct bootApp_boardcfg_rm_resasg resasg;
} __attribute__((__packed__));

struct bootApp_local_rm_boardcfg
{
    struct bootApp_boardcfg_rm            rm_boardcfg;
    struct tisci_boardcfg_rm_resasg_entry resasg_entries[TISCI_RESASG_ENTRIES_MAX];
};

#pragma DATA_SECTION(bootAppBoardCfg, ".sysfw_data_cfg_board")
struct tisci_boardcfg bootAppBoardCfg;
#pragma DATA_SECTION(bootAppBoardCfg_rm, ".sysfw_data_cfg_board_rm")
struct bootApp_local_rm_boardcfg bootAppBoardCfg_rm;
#pragma DATA_SECTION(bootAppBoardCfg_sec, ".sysfw_data_cfg_board_sec")
struct tisci_boardcfg_sec bootAppBoardCfg_sec;
#endif

/* ========================================================================== */
/*              Internal Function Declarations                                */
/* ========================================================================== */

#if defined(MCU_ONLY_TASK_ENABLED)
uint32_t ActiveToMcuSwitch(void);
uint32_t McuOnly_App(void);
uint32_t McuToActiveSwitch(void);

void VtmMaxOutrgAlertDisableForTmpSens1to4();
void SwResetMainDomain(void);
void PMICStateChangeActiveToMCUOnly(void);
void I2CInitPMIC(void);
void PMICStateChangeMCUOnlyToActive(void);
void BringBackMAINDomain(void);
int32_t EnableMCU2MAINBridges(void);
void AccessMainPeripherals(void);
void BoardConfigurationForMainDomain(void);
#endif

/* ========================================================================== */
/*                            Global Variables                                */
/* ========================================================================== */

#if defined(MCU_ONLY_TASK_ENABLED)
I2C_Handle pmicI2cHandle  = NULL;
#endif

/* ========================================================================== */
/*                            External Variables                              */
/* ========================================================================== */

extern struct tisci_boardcfg bootAppBoardCfg;
extern struct bootApp_local_rm_boardcfg bootAppBoardCfg_rm;
extern struct tisci_boardcfg_sec bootAppBoardCfg_sec;

#if defined(MCU_ONLY_TASK_ENABLED)
/* Sync semaphore for MCU ONLY task */
extern SemaphoreP_Handle mcuOnlySyncSemHandle;
#endif

/* ========================================================================== */
/*                          Function Definitions                              */
/* ========================================================================== */

#if defined(MCU_ONLY_TASK_ENABLED)
void BoardConfigurationForMainDomain(void)
{
    int32_t retVal;
    AppUtils_Printf(MSG_NORMAL, MSG_APP_NAME
                    "Configuring Sciclient_board for MAIN domain\n");

    Sciclient_BoardCfgPrms_t bootAppBoardCfgPrms = {
                                                    .boardConfigLow = (uint32_t)&bootAppBoardCfg,
                                                    .boardConfigHigh = 0,
                                                    .boardConfigSize = sizeof(bootAppBoardCfg),
                                                    .devGrp = DEVGRP_01
                                                   };
    Sciclient_BoardCfgPrms_t bootAppBoardCfgPmPrms = {
                                                      .boardConfigLow = (uint32_t)NULL,
                                                      .boardConfigHigh = 0,
                                                      .boardConfigSize = 0,
                                                      .devGrp = DEVGRP_01
                                                     };
    Sciclient_BoardCfgPrms_t bootAppBoardCfgRmPrms = {
                                                      .boardConfigLow = (uint32_t)&bootAppBoardCfg_rm,
                                                      .boardConfigHigh = 0,
                                                      .boardConfigSize = sizeof(bootAppBoardCfg_rm),
                                                      .devGrp = DEVGRP_01
                                                     };
    Sciclient_BoardCfgPrms_t bootAppBoardCfgSecPrms = {
                                                       .boardConfigLow = (uint32_t)&bootAppBoardCfg_sec,
                                                       .boardConfigHigh = 0,
                                                       .boardConfigSize = sizeof(bootAppBoardCfg_sec),
                                                       .devGrp = DEVGRP_01
                                                      };

    retVal = Sciclient_boardCfg(&bootAppBoardCfgPrms);
    if (retVal != CSL_PASS)
    {
         AppUtils_Printf(MSG_NORMAL, MSG_APP_NAME
                         "Sciclient_boardCfg() failed.\n");
    }
    retVal = Sciclient_boardCfgPm(&bootAppBoardCfgPmPrms);
    if (retVal != CSL_PASS)
    {
         AppUtils_Printf(MSG_NORMAL, MSG_APP_NAME 
                         "Sciclient_boardCfgPm() failed.\n");
    }
    retVal = Sciclient_boardCfgRm(&bootAppBoardCfgRmPrms);
    if (retVal != CSL_PASS)
    {
         AppUtils_Printf(MSG_NORMAL, MSG_APP_NAME
                         "Sciclient_boardCfgRm() failed.\n");
    }
    retVal = Sciclient_boardCfgSec(&bootAppBoardCfgSecPrms);
    if (retVal != CSL_PASS)
    {
         AppUtils_Printf(MSG_NORMAL, MSG_APP_NAME
                         "Sciclient_boardCfgSec() failed.\n");
    }

    AppUtils_Printf(MSG_NORMAL, MSG_APP_NAME "Initiallizing MAIN PLLS...\n");
    Board_init(BOARD_INIT_PLL_MAIN);
    AppUtils_Printf(MSG_NORMAL, MSG_APP_NAME "Initiallizing MAIN CLOCKS...\n");
    Board_init(BOARD_INIT_MODULE_CLOCK_MAIN);
    AppUtils_Printf(MSG_NORMAL, MSG_APP_NAME "Initiallizing DDR...\n");
    Board_init(BOARD_INIT_DDR);
}

void SetupI2CTransfer(I2C_Handle handle,  uint32_t slaveAddr,
                      uint8_t *writeData, uint32_t numWriteBytes,
                      uint8_t *readData,  uint32_t numReadBytes)
{
    bool status;
    I2C_Transaction i2cTransaction;

    I2C_transactionInit(&i2cTransaction);
    i2cTransaction.slaveAddress = slaveAddr;
    i2cTransaction.writeBuf = (uint8 *)&writeData[0];
    i2cTransaction.writeCount = numWriteBytes;
    i2cTransaction.readBuf = (uint8 *)&readData[0];
    i2cTransaction.readCount = numReadBytes;
    status = I2C_transfer(handle, &i2cTransaction);
    if(FALSE == status)
    {
        AppUtils_Printf(MSG_NORMAL, MSG_APP_NAME "\n Data Transfer failed. \n");
    }
}

int32_t EnableMCU2MAINBridges(void)
{
    int32_t status;
    /* Enable both bridges, needed to establish communication between MCU and
       MAIN domain */
    /* These are in the MCU devgrp and hence we can enable then w/o passing the
       board configuration for the MAIN devgrp */
    status = Sciclient_pmSetModuleState(TISCI_DEV_WKUPMCU2MAIN_VD,
                                        TISCI_MSG_VALUE_DEVICE_SW_STATE_ON,
                                        TISCI_MSG_FLAG_AOP | TISCI_MSG_FLAG_DEVICE_RESET_ISO,
                                        0xFFFFFFFFU);
    status += Sciclient_pmSetModuleState(TISCI_DEV_MAIN2WKUPMCU_VD,
                                            TISCI_MSG_VALUE_DEVICE_SW_STATE_ON,
                                            TISCI_MSG_FLAG_AOP | TISCI_MSG_FLAG_DEVICE_RESET_ISO,
                                            0xFFFFFFFFU);

    return status;
}

void AccessMainPeripherals(void)
{
    uint32_t readValDDR;
    uint32_t writeValDDR = 0xDEADBEEF;
    AppUtils_Printf(MSG_NORMAL, MSG_APP_NAME"Writing to DDR...\n");
    HW_WR_REG32(DDR_TEST_ADDRESS, writeValDDR);
    AppUtils_Printf(MSG_NORMAL, MSG_APP_NAME"Reading from DDR...\n");
    readValDDR = HW_RD_REG32(DDR_TEST_ADDRESS);
    if(writeValDDR == readValDDR)
    {
        AppUtils_Printf(MSG_NORMAL, MSG_APP_NAME
                        "Read value matches the value written for DDR!\n");
    }

    uint32_t readValMSMC;
    uint32_t writeValMSMC = 0xC0DEC0DE;
    AppUtils_Printf(MSG_NORMAL, MSG_APP_NAME"Writing to MSMC...\n");
    HW_WR_REG32(MSMC_TEST_ADDRESS, writeValMSMC);
    AppUtils_Printf(MSG_NORMAL, MSG_APP_NAME"Reading from MSMC...\n");
    readValMSMC = HW_RD_REG32(MSMC_TEST_ADDRESS);
    if(writeValMSMC == readValMSMC)
    {
        AppUtils_Printf(MSG_NORMAL, MSG_APP_NAME
                        "Read value matches the value written for MSMC!\n");
    }

    uint32_t readValMCAN;
    readValMCAN = HW_RD_REG32(MCAN_REV_REG);
    AppUtils_Printf(MSG_NORMAL, MSG_APP_NAME
                    "Reading MAIN MCAN0 Revision register...\n");
    AppUtils_Printf(MSG_NORMAL, MSG_APP_NAME
                    "MAIN MCAN0 MCANSS_PID = 0x%x\n", readValMCAN);

    return;
}

void BringBackMAINDomain(void)
{
    int32_t status;

    AppUtils_Printf(MSG_NORMAL, MSG_APP_NAME
                    "Configure WKUPMCU2MAIN and MAIN2WKUPMCU Bridges...\n");
    /* Enable WKUPMCU2MAIN and MAIN2WKUPMCU bridges,
       this needs to be done before sending the RM, PM, Sec and common board cfg
       for DEVGRP01 */
    status = EnableMCU2MAINBridges();

    /* Pass board config for MAIN domain i.e. DEVGRP01 */
    if(status == CSL_PASS)
    {
        AppUtils_Printf(MSG_NORMAL, MSG_APP_NAME
                        "WKUPMCU2MAIN and MAIN2WKUPMCU Bridges configured successfully!\n");
        BoardConfigurationForMainDomain();
    }

    AppUtils_Printf(MSG_NORMAL, MSG_APP_NAME
                    "Trying to access MAIN domain peripherals...\n");
    AccessMainPeripherals();

    return;
}

#if defined(PMIC_OPS_ENABLED)
void PMICStateChangeMCUOnlyToActive(void)
{
    uint8_t dataToSlave[2];
    uint8_t dataFromSlave[2];

    /* Read INT_TOP */
    dataToSlave[0] = 0x5A;
    SetupI2CTransfer(pmicI2cHandle, 0x48, dataToSlave, 1, dataFromSlave, 1);
    AppUtils_Printf(MSG_NORMAL, MSG_APP_NAME
                    "INT_TOP = 0x%x\n", dataFromSlave[0]);

    /* Mask NSLEEP2 and NSLEEP1 bits */
    dataToSlave[0] = 0x7D;
    dataToSlave[1] = 0xC0;
    SetupI2CTransfer(pmicI2cHandle, 0x48, dataToSlave, 2, NULL, 0);
    AppUtils_Printf(MSG_NORMAL, MSG_APP_NAME
                    "Write CONFIG_1 = 0x%x\n", dataToSlave[1]);

    /* Change FSM_NSLEEP_TRIGGERS */
    dataToSlave[0] = 0x86;
    dataToSlave[1] = 0x03;
    SetupI2CTransfer(pmicI2cHandle, 0x48, dataToSlave, 2, NULL, 0);
    AppUtils_Printf(MSG_NORMAL, MSG_APP_NAME
                    "Write FSM_NSLEEP_TRIGGERS = 0x%x\n", dataToSlave[1]);

    /* Un-Mask NSLEEP2 and 1 bit */
    dataToSlave[0] = 0x7D;
    dataToSlave[1] = 0x00;
    SetupI2CTransfer(pmicI2cHandle, 0x48, dataToSlave, 2, NULL, 0);
    AppUtils_Printf(MSG_NORMAL, MSG_APP_NAME
                    "Write CONFIG_1 = 0x%x\n", dataToSlave[1]);

    /* Buffer time to change state */
    TaskP_sleep(100);

    /* Read FSM_NSLEEP_TRIGGERS */
    dataToSlave[0] = 0x86;
    SetupI2CTransfer(pmicI2cHandle, 0x48, dataToSlave, 1, dataFromSlave, 1);
    AppUtils_Printf(MSG_NORMAL, MSG_APP_NAME
                    "Read FSM_NSLEEP_TRIGGERS = 0x%x\n", dataFromSlave[0]);

    /*************** You should now back in ACTIVE mode ****************/

    return;
}
#endif

#if defined(VTM_OPS_ENABLED)
void VtmMaxOutrgAlertDisableForTmpSens1to4()
{
    uint32_t vtmRegVal;

    AppUtils_Printf(MSG_NORMAL, MSG_APP_NAME
                    "Disabling MAXT_OUTRG_EN for TMPSENS1:4 in MAIN domain!\n");

    /* TMPSENS1 */
    vtmRegVal = HW_RD_REG32(WKUP_VTM_TMPSENS_CTRL_1);
    /* un-set 11th bit MAXT_OUTRG_EN */
    vtmRegVal &= (~(1 << MAXT_OUTRG_EN_SHIFT));
    HW_WR_REG32(WKUP_VTM_TMPSENS_CTRL_1, vtmRegVal);
    vtmRegVal = HW_RD_REG32(WKUP_VTM_TMPSENS_CTRL_1);

    /* TMPSENS2 */
    vtmRegVal = HW_RD_REG32(WKUP_VTM_TMPSENS_CTRL_2);
    /* un-set 11th bit MAXT_OUTRG_EN */
    vtmRegVal &= (~(1 << MAXT_OUTRG_EN_SHIFT));
    HW_WR_REG32(WKUP_VTM_TMPSENS_CTRL_2, vtmRegVal);
    vtmRegVal = HW_RD_REG32(WKUP_VTM_TMPSENS_CTRL_2);

    /* TMPSENS3 */
    vtmRegVal = HW_RD_REG32(WKUP_VTM_TMPSENS_CTRL_3);
    /* un-set 11th bit MAXT_OUTRG_EN */
    vtmRegVal &= (~(1 << MAXT_OUTRG_EN_SHIFT));
    HW_WR_REG32(WKUP_VTM_TMPSENS_CTRL_3, vtmRegVal);
    vtmRegVal = HW_RD_REG32(WKUP_VTM_TMPSENS_CTRL_3);

    /* TMPSENS4 */
    vtmRegVal = HW_RD_REG32(WKUP_VTM_TMPSENS_CTRL_4);
    /* un-set 11th bit MAXT_OUTRG_EN */
    vtmRegVal &= (~(1 << MAXT_OUTRG_EN_SHIFT));
    HW_WR_REG32(WKUP_VTM_TMPSENS_CTRL_4, vtmRegVal);
    vtmRegVal = HW_RD_REG32(WKUP_VTM_TMPSENS_CTRL_4);

    return;
}
#endif

/* Bring back main_domain */
uint32_t McuToActiveSwitch(void)
{
#if defined(PMIC_OPS_ENABLED)
    AppUtils_Printf(MSG_NORMAL, MSG_APP_NAME
                    "PMIC STATE CHANGE: MCU ONLY -> ACTIVE...\n");
    PMICStateChangeMCUOnlyToActive();
    AppUtils_Printf(MSG_NORMAL, MSG_APP_NAME
                    "PMIC STATE CHANGE: MCU ONLY -> ACTIVE...Done\n");
#else
    AppUtils_Printf(MSG_NORMAL, MSG_APP_NAME
                    "Skipping PMIC state change from MCU ONLY to ACTIVE!!\n");
#endif

    /* Enable the MAIN domain, till now we have only changed the PMIC state
       This will enable the modules in the MAIN domain */
    BringBackMAINDomain();

    return 0;
}

/* Refer TISCI_MSG_SYS_RESET in TISCI user guide for more details
   http://software-dl.ti.com/tisci/esd/latest/2_tisci_msgs/pm/sysreset.html */
volatile uint32_t loopSwResetMainDomain = 0;
void SwResetMainDomain(void)
{
    struct tisci_msg_sys_reset_req request;
    struct tisci_msg_sys_reset_resp response = {0};

    Sciclient_ReqPrm_t reqParam;
    Sciclient_RespPrm_t respParam;

    memset(&request, 0, sizeof(request));
    request.domain = 0x2; /* 0x2 corresponds to the MAIN domain */

    reqParam.messageType    = (uint16_t) TISCI_MSG_SYS_RESET;
    reqParam.flags          = (uint32_t) TISCI_MSG_FLAG_AOP;
    reqParam.pReqPayload    = (const uint8_t *) &request;
    reqParam.reqPayloadSize = (uint32_t) sizeof (request);
    reqParam.timeout        = (uint32_t) SCICLIENT_SERVICE_WAIT_FOREVER;
    respParam.flags           = (uint32_t) 0;   /* Populated by the API */
    respParam.pRespPayload    = (uint8_t *) &response;
    respParam.respPayloadSize = (uint32_t) sizeof (response);

    /* For debug purpose */
    if(loopSwResetMainDomain == 0xDEADBEEF)
    {
        AppUtils_Printf(MSG_NORMAL, MSG_APP_NAME
                        "Connect CCS and change loopSwResetMainDomain to 0!\n");
        AppUtils_Printf(MSG_NORMAL, MSG_APP_NAME
                        "After that the MAIN domain will be reset!\n");
    }

    while(loopSwResetMainDomain == 0xDEADBEEF);

    int32_t retVal;
    retVal = Sciclient_service(&reqParam, &respParam);

    if ((respParam.flags & TISCI_MSG_FLAG_ACK) == 0) {
            AppUtils_Printf(MSG_NORMAL, MSG_APP_NAME
                            "retVal = %d\nGOT NACK! resp flag = 0x%08x\n",
                            retVal, respParam.flags);
    }
}

#if defined (PMIC_OPS_ENABLED)
void I2CInitPMIC(void)
{
    I2C_Params i2cParams;

    /* Initialize i2c core instances */
    I2C_init();
    uint8_t i2c_instance = 0U;
    uint32_t baseAddr = CSL_WKUP_I2C0_CFG_BASE;

    I2C_HwAttrs i2cCfg;
    I2C_socGetInitCfg(i2c_instance, &i2cCfg);
    i2cCfg.baseAddr   = baseAddr;
    i2cCfg.enableIntr = 0U;
    I2C_socSetInitCfg(i2c_instance, &i2cCfg);

    /* Configured i2cParams.bitRate with standard I2C_100kHz */
    I2C_Params_init(&i2cParams);
    pmicI2cHandle = I2C_open(i2c_instance, &i2cParams);
    if(NULL == pmicI2cHandle)
    {
        AppUtils_Printf(MSG_NORMAL, MSG_APP_NAME"ERROR: I2C_open failed!\n");
        while(1);
    }
}

volatile uint32_t loopPMICStateChangeActiveToMCUOnly = 0;
void PMICStateChangeActiveToMCUOnly(void)
{
    /* Init i2c interface */
    I2CInitPMIC();

    /* Write 0x02 to FSM_NSLEEP_TRIGGERS register 
       This should happen before clearing the interrupts */

    /* If you clear the interrupts before you write the NSLEEP bits,
     * it will transition to S2R state.
     * This is because as soon as you write NSLEEP2 to 0x0,
     * the trigger is present to move to S2R state.
     * By setting the NSLEEP bits before you clear the interrupts,
     * you can configure both NSLEEP bits before the PMIC reacts to the change.
     */

    uint8_t dataToSlave[2];
    uint8_t dataFromSlave[2];

    if(loopPMICStateChangeActiveToMCUOnly == 0xFEEDFACE)
    {
        AppUtils_Printf(MSG_NORMAL, MSG_APP_NAME
                        "Connect CCS and change the loopPMICStateChangeActiveToMCUOnly to 0x0!!!!\n");
        AppUtils_Printf(MSG_NORMAL, MSG_APP_NAME
                        "This will disconnect the JTAG interface too and you can only see the MCU running from UART prints!!!!\n");
    }

    while(loopPMICStateChangeActiveToMCUOnly == 0xFEEDFACE);

    /* Read INT_TOP */
    dataToSlave[0] = 0x5A;
    SetupI2CTransfer(pmicI2cHandle, 0x48, dataToSlave, 1, dataFromSlave, 1);
    AppUtils_Printf(MSG_NORMAL, MSG_APP_NAME "INT_TOP = 0x%x\n", dataFromSlave[0]);

    /* Read INT_STARTUP */
    dataToSlave[0] = 0x65;
    SetupI2CTransfer(pmicI2cHandle, 0x48, dataToSlave, 1, dataFromSlave, 1);
    AppUtils_Printf(MSG_NORMAL, MSG_APP_NAME "INT_STARTUP = 0x%x\n", dataFromSlave[0]);

    /* Read INT_GPIO */
    dataToSlave[0] = 0x63;
    SetupI2CTransfer(pmicI2cHandle, 0x48, dataToSlave, 1, dataFromSlave, 1);
    AppUtils_Printf(MSG_NORMAL, MSG_APP_NAME "INT_GPIO = 0x%x\n", dataFromSlave[0]);

    /* Read INT_GPIO1_8 */
    dataToSlave[0] = 0x64;
    SetupI2CTransfer(pmicI2cHandle, 0x48, dataToSlave, 1, dataFromSlave, 1);
    AppUtils_Printf(MSG_NORMAL, MSG_APP_NAME "INT_GPIO1_8 = 0x%x\n", dataFromSlave[0]);

    /* Read FSM_NSLEEP_TRIGGERS */
    dataToSlave[0] = 0x86;
    SetupI2CTransfer(pmicI2cHandle, 0x48, dataToSlave, 1, dataFromSlave, 1);
    AppUtils_Printf(MSG_NORMAL, MSG_APP_NAME "Read FSM_NSLEEP_TRIGGERS = 0x%x\n", dataFromSlave[0]);

    /**** Start changing states ****/

    /* Change FSM_NSLEEP_TRIGGERS */
    dataToSlave[0] = 0x86;
    dataToSlave[1] = 0x02;
    SetupI2CTransfer(pmicI2cHandle, 0x48, dataToSlave, 2, NULL, 0);
    AppUtils_Printf(MSG_NORMAL, MSG_APP_NAME "Write FSM_NSLEEP_TRIGGERS = 0x%x\n", dataToSlave[1]);

    /* Read FSM_NSLEEP_TRIGGERS */
    dataToSlave[0] = 0x86;
    SetupI2CTransfer(pmicI2cHandle, 0x48, dataToSlave, 1, dataFromSlave, 1);
    AppUtils_Printf(MSG_NORMAL, MSG_APP_NAME "Read FSM_NSLEEP_TRIGGERS = 0x%x\n", dataFromSlave[0]);

    /* Clear INT_STARTUP */
    dataToSlave[0] = 0x65;
    dataToSlave[1] = 0x02;
    SetupI2CTransfer(pmicI2cHandle, 0x48, dataToSlave, 2, NULL, 0);
    AppUtils_Printf(MSG_NORMAL, MSG_APP_NAME "Write INT_STARTUP = 0x%x\n", dataToSlave[1]);

    /* Read INT_TOP */
    dataToSlave[0] = 0x5A;
    SetupI2CTransfer(pmicI2cHandle, 0x48, dataToSlave, 1, dataFromSlave, 1);
    AppUtils_Printf(MSG_NORMAL, MSG_APP_NAME "INT_TOP = 0x%x\n", dataFromSlave[0]);

    /* Clear INT_GPIO1_8 */
    dataToSlave[0] = 0x64;
    dataToSlave[1] = 0xC8;
    SetupI2CTransfer(pmicI2cHandle, 0x48, dataToSlave, 2, NULL, 0);
    AppUtils_Printf(MSG_NORMAL, MSG_APP_NAME "Write INT_STARTUP = 0x%x\n", dataToSlave[1]);

    /* Read INT_GPIO */
    dataToSlave[0] = 0x63;
    SetupI2CTransfer(pmicI2cHandle, 0x48, dataToSlave, 1, dataFromSlave, 1);
    AppUtils_Printf(MSG_NORMAL, MSG_APP_NAME "INT_GPIO = 0x%x\n", dataFromSlave[0]);

    /* Read INT_TOP */
    dataToSlave[0] = 0x5A;
    SetupI2CTransfer(pmicI2cHandle, 0x48, dataToSlave, 1, dataFromSlave, 1);
    AppUtils_Printf(MSG_NORMAL, MSG_APP_NAME "INT_TOP = 0x%x\n", dataFromSlave[0]);

    /* Clear INT_GPIO */
    dataToSlave[0] = 0x63;
    dataToSlave[1] = 0x02;
    SetupI2CTransfer(pmicI2cHandle, 0x48, dataToSlave, 2, NULL, 0);
    AppUtils_Printf(MSG_NORMAL, MSG_APP_NAME "Write INT_GPIO = 0x%x\n", dataToSlave[1]);

    /* Read INT_TOP */
    dataToSlave[0] = 0x5A;
    SetupI2CTransfer(pmicI2cHandle, 0x48, dataToSlave, 1, dataFromSlave, 1);
    AppUtils_Printf(MSG_NORMAL, MSG_APP_NAME "Final Read INT_TOP = 0x%x\n", dataFromSlave[0]);

    /* Read FSM_NSLEEP_TRIGGERS */
    dataToSlave[0] = 0x86;
    SetupI2CTransfer(pmicI2cHandle, 0x48, dataToSlave, 1, dataFromSlave, 1);
    AppUtils_Printf(MSG_NORMAL, MSG_APP_NAME "Final Read FSM_NSLEEP_TRIGGERS = 0x%x\n", dataFromSlave[0]);

    /*************** You should now be in MCU only mode ****************/

    return;
}
#endif

uint32_t ActiveToMcuSwitch()
{
    /* Issue a SW reset to the MAIN domain */
    AppUtils_Printf(MSG_NORMAL, MSG_APP_NAME
                    "Issueing a SW reset to the MAIN domain...\n");
    SwResetMainDomain();

#if defined (PMIC_OPS_ENABLED)
    AppUtils_Printf(MSG_NORMAL, MSG_APP_NAME
                    "PMIC STATE CHANGE: ACTIVE -> MCU ONLY...\n");

    /* Change PMIC state from ACTIVE to MCU ONLY */

    /* NOTE: JTAG power also compromises after the transition, so you will not
       be able to connect JTAG in MCU ONLY mode */
    PMICStateChangeActiveToMCUOnly();
    AppUtils_Printf(MSG_NORMAL, MSG_APP_NAME
                    "PMIC STATE CHANGE: ACTIVE -> MCU ONLY...Done\n");
#else
    AppUtils_Printf(MSG_NORMAL, MSG_APP_NAME
                    "Skipping PMIC state change from ACTIVE to MCU ONLY!!\n");
#endif

    return 0;
}

/* MCU Only task */
volatile uint32_t loopEnd = 0xFEEDFEED;
uint32_t McuOnly_App()
{
    uint32_t status = 0;
    AppUtils_Printf(MSG_NORMAL, MSG_APP_NAME
                    "Waiting for BootApp Task to complete...\n");
    SemaphoreP_pend(mcuOnlySyncSemHandle, osal_WAIT_FOREVER);
    AppUtils_Printf(MSG_NORMAL, MSG_APP_NAME "BootApp Task to completed!\n");
    AppUtils_Printf(MSG_NORMAL, MSG_APP_NAME "Inside MCU ONLY task!\n");

#if defined(VTM_OPS_ENABLED)
    /* Before entering MCU_ONLY mode we need to disable all VTM temp sensors in
       the MAIN domain - VTM_TMPSENS1-4 */
    /* Disabling the VTM MAXT_OUTRG_ALERT_THR */
    VtmMaxOutrgAlertDisableForTmpSens1to4();
#endif

    AppUtils_Printf(MSG_NORMAL, MSG_APP_NAME
                    "STATE INFO :: CURRENTLY IN ACTIVE MODE!\n");
    AppUtils_Printf(MSG_NORMAL, MSG_APP_NAME "LED LD5 should be ON\n");
    AppUtils_Printf(MSG_NORMAL, MSG_APP_NAME
                    "Sleeping for 5s, please measure TP133 and TP134!\n");
    AppUtils_Printf(MSG_NORMAL, MSG_APP_NAME
                    "Expected values in ACTIVE mode:\nTP133: HIGH\nTP134: HIGH\n");
    TaskP_sleep(5000);

    /* Change state from ACTIVE to MCU ONLY */
    if (0 == status)
    {
        AppUtils_Printf(MSG_NORMAL, MSG_APP_NAME
                        "############################ACTIVE -> MCU ONLY MODE############################\n");
        /* The status is dummy currently */
        status = ActiveToMcuSwitch();
        AppUtils_Printf(MSG_NORMAL, MSG_APP_NAME
                        "#########################ACTIVE -> MCU ONLY MODE DONE##########################\n");
    }
    else
    {
        AppUtils_Printf(MSG_NORMAL, MSG_APP_NAME">> ERROR :: Status not correct!!!\n");
    }

    AppUtils_Printf(MSG_NORMAL, MSG_APP_NAME 
                    "STATE INFO :: NOW IN MCU ONLY MODE!\n");
    AppUtils_Printf(MSG_NORMAL, MSG_APP_NAME "LED LD5 should be OFF\n");
    AppUtils_Printf(MSG_NORMAL, MSG_APP_NAME
                    "Sleeping for 5s, please measure TP133 and TP134!\n");
    AppUtils_Printf(MSG_NORMAL, MSG_APP_NAME
                    "Expected values in MCU ONLY mode:\nTP133: HIGH\nTP134: LOW\n");
    TaskP_sleep(5000);

    /* Change state from MCU ONLY to ACTIVE */
    if (0 == status)
    {
        AppUtils_Printf(MSG_NORMAL, MSG_APP_NAME
                        "############################MCU ONLY -> ACTIVE MODE############################\n");
        /* The status is dummy currently */
        status = McuToActiveSwitch();
        AppUtils_Printf(MSG_NORMAL, MSG_APP_NAME
                        "#########################MCU ONLY -> ACTIVE MODE DONE##########################\n");
    }
    else
    {
        AppUtils_Printf(MSG_NORMAL, MSG_APP_NAME">> ERROR :: Status not correct!!!\n");
    }

    AppUtils_Printf(MSG_NORMAL, MSG_APP_NAME
                    "STATE INFO :: CURRENTLY IN ACTIVE MODE!\n");
    AppUtils_Printf(MSG_NORMAL, MSG_APP_NAME "LED LD5 should be ON\n");
    AppUtils_Printf(MSG_NORMAL, MSG_APP_NAME
                    "Expected values in ACTIVE mode:\nTP133: HIGH\nTP134: HIGH\n");

    /* Enter infinite loop, connect CCS at this point and access DDR, MSMC and
       other MAIN domain perepherals (eg - MCAN) */
    /* NOTE: Currenly when we switch back to ACTIVE mode, we do not load the
             firmwares on MAIN domain cores and hence when you try to connect
             to MAIN domain cores from CCS - it will fail.
             You can connect CCS and access DDR, MSMC, MAIN MCANs and other MAIN
             peripherals.
             Loading of cores will be added in the future. */

    if(loopEnd)
    {
        AppUtils_Printf(MSG_NORMAL, MSG_APP_NAME
                        "Enterting an infinite loop...\n");
        AppUtils_Printf(MSG_NORMAL, MSG_APP_NAME
                        "As the full chip is powered ON you can connect JTAG\n");
        AppUtils_Printf(MSG_NORMAL, MSG_APP_NAME
                        "Connect CCS via JTAG at this point and access DDR, MSMC and other MAIN domain perepherals (eg - MCAN)!!\n");
        while(loopEnd);
    }
    return status;
}
#endif
