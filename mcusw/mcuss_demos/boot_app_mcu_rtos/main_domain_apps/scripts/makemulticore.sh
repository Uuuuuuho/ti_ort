#!/bin/bash

MY_BOARD=$1
if [ "${MY_BOARD}" = "j721e_evm" ]
then
  MY_SOC=j721e
  OUTPUT_IMAGE_PATH=../binary/bin/j721e_evm
elif [ "${MY_BOARD}" = "j7200_evm" ]
then
  MY_SOC=j7200
  OUTPUT_IMAGE_PATH=../binary/bin/j7200_evm

fi
if [ "${MY_SOC}" == "j7200" ] || [ "${MY_SOC}" == "j721e" ]
then
  MY_OS=tirtos
  MY_APP=mcusw_ex01_multicore_demo_app
  MY_CORE=all
  MY_PROFILE=release
  MY_CLEAN=
  MY_UART_PORT=1
  MY_BUILDDIR=../../../../build
  RUN_BUILD_STEP=1

  SDK_INSTALL_PATH=${PWD}/../../../../..
  PDK_INSTALL_PATH="${SDK_INSTALL_PATH}/pdk_jacinto_08_00_00_37/packages"
  MULTICORE_GEN_TOOL_PATH="${PDK_INSTALL_PATH}/ti/boot/sbl/tools/multicoreImageGen/bin"

  declare -a cores_j721e=("mpu1_0" "mcu2_0" "mcu2_1" "mcu3_0" "mcu3_1" "c66xdsp_1" "c66xdsp_2" "c7x_1")
  declare -a cores_exten_names_j721e=("xa72fg" "xer5f" "xer5f" "xer5f" "xer5f" "xe66" "xe66" "xe71")
  declare -a cores_j7200=("mpu1_0" "mcu2_0" "mcu2_1")
  declare -a cores_exten_names_j7200=("xa72fg" "xer5f" "xer5f")
  # TODO - Add mpu1_1 core to the Hello World example
  #declare -a cores_j721e=("mpu1_0" "mpu1_1" "mcu2_0" "mcu2_1" "mcu3_0" "mcu3_1" "c66xdsp_1" "c66xdsp_2" "c7x_1")
  #declare -a cores_exten_names_j721e=("xa72fg" "xa72fg" "xer5f" "xer5f" "xer5f" "xer5f" "xe66" "xe66" "xe71")

  USAGE="$0 [-a <app name : mcusw_ex01_bios_2core_echo_test> -b <board : j721e_evm, j7200_evm> -c <core> -d <PDK install directory> -r <run build step : 0 to skip> -p <build profile> -s <SDK install directory>]"

  while getopts 'a:b:c:p:r:s:t:u:d:' opt
  do
      case $opt in
          a) MY_APP=$OPTARG;;
          b) MY_BOARD=$OPTARG;;
          c) MY_CORE=$OPTARG;;
          d) PDK_INSTALL_DIR=$OPTARG;;
          r) RUN_BUILD_STEP=$OPTARG;;
          p) MY_PROFILE=$OPTARG;;
          s) SDK_INSTALL_DIR=$OPTARG;;
          t) MY_CLEAN=$OPTARG;;
          u) MY_UART_PORT=$OPTARG;;
      esac
  done

  #MY_BLD="-s -j ${MY_APP} BOARD=${MY_BOARD} SOC=${MY_SOC} BUILD_PROFILE=${MY_PROFILE} BUILD_OS_TYPE=${MY_OS}"
  MY_BLD="-s -j ${MY_APP}${MY_CLEAN} BOARD=${MY_BOARD} SOC=${MY_SOC} BUILD_PROFILE=${MY_PROFILE} BUILD_OS_TYPE=${MY_OS} UART_PORT=${MY_UART_PORT}"

  pushd $PWD
  cd ${MY_BUILDDIR}

  if [ "${MY_CORE}" = "all" ]
  then
      if [ ${RUN_BUILD_STEP} == 1 ]
      then
          # iterate all cores of J721e
          if [ "${MY_BOARD}" = "j721e_evm" ]
          then
              for i in "${cores_j721e[@]}"
              do
                  echo make ${MY_APP} - ${MY_BOARD} "$i"
                  make ${MY_BLD} CORE="$i"
              done
          fi
          if [ "${MY_BOARD}" = "j7200_evm" ]
          then
              for i in "${cores_j7200[@]}"
              do
                  echo make ${MY_APP} - ${MY_BOARD} "$i"
                  make ${MY_BLD} CORE="$i"
              done
          fi
      fi
  else
      make ${MY_BLD} CORE=${MY_CORE}
  fi

  popd


  if [ "${MY_CORE}" = "all" ]
  then
      # for each of the cores, ensure that the image was properly created
      ALL_CORE_IMAGES_CREATED=1
      if [ "${MY_BOARD}" = "j721e_evm" ]
      then
        for i in "${!cores_j721e[@]}"
        do
            FILE=${OUTPUT_IMAGE_PATH}/${MY_APP}_${cores_j721e[i]}_${MY_PROFILE}.${cores_exten_names_j721e[i]}
            if [ ! -f "$FILE" ]; then
                echo "Error - $FILE does not exist"
                ALL_CORE_IMAGES_CREATED=0
                break
            fi
        done
      fi
      if [ "${MY_BOARD}" = "j7200_evm" ]
      then
        for i in "${!cores_j7200[@]}"
        do
            FILE=${OUTPUT_IMAGE_PATH}/${MY_APP}_${cores_j7200[i]}_${MY_PROFILE}.${cores_exten_names_j7200[i]}
            if [ ! -f "$FILE" ]; then
                echo "Error - $FILE does not exist"
                ALL_CORE_IMAGES_CREATED=0
                break
            fi
        done
      fi

      if [ $ALL_CORE_IMAGES_CREATED == 1 ]
      then
          # Since all core images were created, we can create the
          # RPRC multicore appimage now
          echo "All the Core images have been created"
          ./constructappimage_multistage_${MY_SOC}.sh
      fi

  fi
fi
