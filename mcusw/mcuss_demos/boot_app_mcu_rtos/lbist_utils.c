/*
*
* Copyright (c) 2020 Texas Instruments Incorporated
*
* All rights reserved not granted herein.
*
* Limited License.
*
* Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
* license under copyrights and patents it now or hereafter owns or controls to make,
* have made, use, import, offer to sell and sell ("Utilize") this software subject to the
* terms herein.  With respect to the foregoing patent license, such license is granted
* solely to the extent that any such patent is necessary to Utilize the software alone.
* The patent license shall not apply to any combinations which include this software,
* other than combinations with devices manufactured by or for TI ("TI Devices").
* No hardware patent is licensed hereunder.
*
* Redistributions must preserve existing copyright notices and reproduce this license
* (including the above copyright notice and the disclaimer and (if applicable) source
* code license limitations below) in the documentation and/or other materials provided
* with the distribution
*
* Redistribution and use in binary form, without modification, are permitted provided
* that the following conditions are met:
*
* *       No reverse engineering, decompilation, or disassembly of this software is
* permitted with respect to any software provided in binary form.
*
* *       any redistribution and use are licensed by TI for use only with TI Devices.
*
* *       Nothing shall obligate TI to provide you with source code for the software
* licensed and provided to you in object code.
*
* If software source code is provided to you, modification and redistribution of the
* source code are permitted provided that the following conditions are met:
*
* *       any redistribution and use of the source code, including any resulting derivative
* works, are licensed by TI for use only with TI Devices.
*
* *       any redistribution and use of any object code compiled from the source code
* and any resulting derivative works, are licensed by TI for use only with TI Devices.
*
* Neither the name of Texas Instruments Incorporated nor the names of its suppliers
*
* may be used to endorse or promote products derived from this software without
* specific prior written permission.
*
* DISCLAIMER.
*
* THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
* OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
* OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
* IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
* BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
* OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
* OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
* OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/**
 *  \file lbist_utils.c
 *
 *  \brief LBIST utility functions
 */

/* ========================================================================== */
/*                             Include Files                                  */
/* ========================================================================== */

#include "Std_Types.h"
#include <string.h>
#include <ti/csl/csl_types.h>
#include <ti/csl/csl_lbist.h>
#include <ti/csl/soc.h>
#include <ti/drv/sciclient/sciclient.h>

/* Osal API header files */
#include <ti/osal/HwiP.h>
#include <ti/osal/TimerP.h>

#include "power_seq.h"

#include "lbist_utils.h"
#include "lbist_defs.h"

#include "app_utils.h"

/* ========================================================================== */
/*                           Macros & Typedefs                                */
/* ========================================================================== */

#define LBIST_MAX_TIMEOUT_VALUE    10000000u

/* This flag adds more verbose prints */
//#define DEBUG

/* This flag enables gathering of profile information for each LBIST test run */
//#define PROFILE_LBIST_TIMING

/* ========================================================================== */
/*                 Internal Function Declarations                             */
/* ========================================================================== */

static int32_t LBIST_isPostLbistTimeout(uint32_t postStatMmrRegVal,
                                        uint8_t section,
                                        Bool *pIsTimedOut);
static int32_t LBIST_isPostLbistDone(uint32_t postStatMmrRegVal,
                                     uint8_t section,
                                     Bool *pIsDone);
static int32_t LBIST_runPostLbistCheck(uint32_t hwPostCoreNum,
                                       CSL_lbistRegs *pLBISTRegs,
                                       uint32_t *pLBISTSig);

/* ========================================================================== */
/*                         Structure Declarations                             */
/* ========================================================================== */

/* None */

/* ========================================================================== */
/*                          Function Declarations                             */
/* ========================================================================== */

/* None */

/* ========================================================================== */
/*                            Global Variables                                */
/* ========================================================================== */

/* None */

/* ========================================================================== */
/*                            External Variables                              */
/* ========================================================================== */

/* None */

/* ========================================================================== */
/*                          Function Definitions                              */
/* ========================================================================== */

/* This function runs the full LBIST test for a particular core.
 * It includes the following steps (each step has many sub-steps specificied
 * in more details within the function).
 * For HW POST LBIST:
 * Step 1: Configure interrupt handler
 * Step 2: Check POST results (already run at startup)
 *
 * For SW-initiated LBIST:
 * Step 1: Configure interrupt handler
 * Step 2: Configure processor to correct state
 * Step 3: Configure LBIST
 * Step 4: Run LBIST test
 * Step 5: Restore cores
 * Step 6: Check result of LBIST
 * */
int32_t LBIST_runTest(uint32_t coreIndex)
{
    int32_t testResult = 0;
    int32_t status;
    uint32_t calculatedMISR;
    uint32_t expectedMISR;
    Bool isLBISTRunning = CSL_FALSE;
    CSL_lbistRegs *pLBISTRegs;
    uint32_t *pLBISTSig;
    HwiP_Params       hwiParams;
    HwiP_Handle LBIST_hwiPHandle;
    uint32_t timeoutCount = 0;
#if defined(PROFILE_LBIST_TIMING)
    uint64_t startTime , testStartTime,  testEndTime, endTime;
    uint64_t prepTime, diffTime, restoreTime;

    AppUtils_Printf(MSG_NORMAL,
                    "\n Starting LBIST test on %s, index %d...",
                    LBIST_TestHandleArray[coreIndex].coreName,
                    coreIndex);
#endif
    /* Populate local variables from instance structure */
    pLBISTRegs = LBIST_TestHandleArray[coreIndex].pLBISTRegs;
    pLBISTSig = LBIST_TestHandleArray[coreIndex].pLBISTSig;

    /*-- Step 1: Configure interrupt handler --*/
    /* Register call back function for LBIST Interrupt */
    if (LBIST_TestHandleArray[coreIndex].handler != NULL)
    {
        /* Disable interrupt */
        HwiP_disableInterrupt(LBIST_TestHandleArray[coreIndex].interruptNumber);

        /* Default parameter initialization */
        HwiP_Params_init(&hwiParams);

        /* Pass core Index as argument to handler*/
        hwiParams.arg = coreIndex;
#ifdef DEBUG
    AppUtils_Printf(MSG_NORMAL,
                    "\n  HwiP_Params_init complete \n");
#endif

        LBIST_hwiPHandle = HwiP_create(LBIST_TestHandleArray[coreIndex].interruptNumber,
                                       (HwiP_Fxn)LBIST_TestHandleArray[coreIndex].handler,
                                       (void *)&hwiParams);
        if (LBIST_hwiPHandle == NULL)
        {
            AppUtils_Printf(MSG_NORMAL,
                            "   Interrupt registration failed \n");
            testResult = -1;
        }
    }

    /* Initialize done flag */
    LBIST_TestHandleArray[coreIndex].doneFlag = false;

#if defined(PROFILE_LBIST_TIMING)
    /* Get start time of test */
    startTime = TimerP_getTimeInUsecs();
#endif


    if ((testResult == 0) &&
        (LBIST_TestHandleArray[coreIndex].hwPostCoreCheck == true))
    {
        /* HW POST test flow - test already run, checking the result */
        /* Step 2: (only for POST) Check POST results  */
        testResult = LBIST_runPostLbistCheck(LBIST_TestHandleArray[coreIndex].hwPostCoreNum,
                                             LBIST_TestHandleArray[coreIndex].pLBISTRegs,
                                             LBIST_TestHandleArray[coreIndex].pLBISTSig);
#ifdef DEBUG
        AppUtils_Printf(MSG_NORMAL,
                        "HW POST core: %s: result = %d\n",
                        LBIST_TestHandleArray[coreIndex].coreName,
                        testResult);
#endif
    }
    else
    {
        /* SW-initiated LBIST test flow */
        /*-- Step 2: Configure processor to correct state --*/

        /**--- Step 2a: Request Primary core ---*/
        if (testResult == 0)
        {
            if (LBIST_TestHandleArray[coreIndex].tisciProcId != 0u)
            {
#ifdef DEBUG
                AppUtils_Printf(MSG_NORMAL,
                                "  Primary core: %s: Requesting processor \n",
                                LBIST_TestHandleArray[coreIndex].coreName);
#endif
                /* Request Primary core */
                status = Sciclient_procBootRequestProcessor(LBIST_TestHandleArray[coreIndex].tisciProcId,
                                                            SCICLIENT_SERVICE_WAIT_FOREVER);
                if (status != CSL_PASS)
                {
                    AppUtils_Printf(MSG_NORMAL,
                                    "   Primary core: Sciclient_procBootRequestProcessor, ProcId 0x%x...FAILED \n",
                                    LBIST_TestHandleArray[coreIndex].tisciProcId);
                    testResult = -1;
                }
            }
        }

        /**--- Step 2b: Request Secondary core ---*/
        if (testResult == 0)
        {
            if ((LBIST_TestHandleArray[coreIndex].secondaryCoreNeeded)
                && (LBIST_TestHandleArray[coreIndex].tisciSecProcId != 0u))
            {
#ifdef DEBUG
                AppUtils_Printf(MSG_NORMAL,
                                "  Secondary core: %s: Requesting processor \n",
                                LBIST_TestHandleArray[coreIndex].secCoreName);
#endif
                /* Request secondary core */
                status = Sciclient_procBootRequestProcessor(LBIST_TestHandleArray[coreIndex].tisciSecProcId,
                                                            SCICLIENT_SERVICE_WAIT_FOREVER);
                if (status != CSL_PASS)
                {
                    AppUtils_Printf(MSG_NORMAL,
                                    "   Secondary core: Sciclient_procBootRequestProcessor, ProcId 0x%x...FAILED \n",
                                    LBIST_TestHandleArray[coreIndex].tisciSecProcId);
                    testResult = -1;
                }
            }
        }

        /**--- Step 2c: Place all Auxilliary modules needed to run test into module reset ---*/
        if (testResult == 0)
        {
            int i;

            /* Place all Auxilliary modules required for test into module reset */
            for ( i = 0; i < LBIST_TestHandleArray[coreIndex].numAuxDevices; i++)
            {
#ifdef DEBUG
                AppUtils_Printf(MSG_NORMAL,
                                "  Putting into module reset Device number %d Device Id %x\n",
                                i,
                                LBIST_TestHandleArray[coreIndex].auxDeviceIdsP[i]);
#endif

                status = Sciclient_pmSetModuleRst(LBIST_TestHandleArray[coreIndex].auxDeviceIdsP[i],
                                                  0x2, /* Module Reset asserted */
                                                  SCICLIENT_SERVICE_WAIT_FOREVER);
                if (status != CSL_PASS)
                {
                    AppUtils_Printf(MSG_NORMAL,
                                    "  Sciclient_pmSetModuleState 0x%x ...FAILED \n",
                                    LBIST_TestHandleArray[coreIndex].auxDeviceIdsP[i]);
                    testResult = -1;
                    break;
                }
            }
        }

        /**--- Step 2d: Put Primary core in module reset and local reset ---*/
        if ((testResult == 0) && (LBIST_TestHandleArray[coreIndex].tisciDeviceId != NULL))
        {
#ifdef DEBUG
            AppUtils_Printf(MSG_NORMAL,
                            "  Primary core: Putting in module and local reset the core %s \n",
                            LBIST_TestHandleArray[coreIndex].coreName);
#endif
            status = Sciclient_pmSetModuleRst(LBIST_TestHandleArray[coreIndex].tisciDeviceId,
                                              0x3, /* Module Reset and Local Reset asserted */
                                              SCICLIENT_SERVICE_WAIT_FOREVER);
            if (status != CSL_PASS)
            {
                 AppUtils_Printf(MSG_NORMAL,
                                 "  Sciclient_pmSetModuleRst 0x%x ...FAILED \n",
                                 LBIST_TestHandleArray[coreIndex].tisciDeviceId);
                 testResult = -1;
            }
        }

        /**--- Step 2e: Put Secondary core in module reset and local reset ---*/
        if ((testResult == 0) && (LBIST_TestHandleArray[coreIndex].tisciSecDeviceId != NULL))
        {
#ifdef DEBUG
            AppUtils_Printf(MSG_NORMAL,
                            "  Secondary core: Putting in module and local reset the core %s \n",
                            LBIST_TestHandleArray[coreIndex].secCoreName);
#endif
            status = Sciclient_pmSetModuleRst(LBIST_TestHandleArray[coreIndex].tisciSecDeviceId,
                                              0x3, /* Module Reset and Local Reset asserted */
                                              SCICLIENT_SERVICE_WAIT_FOREVER);
            if (status != CSL_PASS)
            {
                 AppUtils_Printf(MSG_NORMAL,
                                 "  Sciclient_pmSetModuleRst 0x%x ...FAILED \n",
                                 LBIST_TestHandleArray[coreIndex].tisciSecDeviceId);
                 testResult = -1;
            }
        }

        /**--- Step 2f: Place all Auxilliary modules needed to run test into retention ---*/
        if (testResult == 0)
        {
            int i;

            /* Place all Auxilliary modules required for test into retention */
            for ( i = 0; i < LBIST_TestHandleArray[coreIndex].numAuxDevices; i++)
            {
#ifdef DEBUG
                AppUtils_Printf(MSG_NORMAL,
                                "  Putting into Retention Device number %d Device Id %x\n",
                                i,
                                LBIST_TestHandleArray[coreIndex].auxDeviceIdsP[i]);
#endif

                status = Sciclient_pmSetModuleState(LBIST_TestHandleArray[coreIndex].auxDeviceIdsP[i],
                                                    TISCI_MSG_VALUE_DEVICE_SW_STATE_RETENTION,
                                                    TISCI_MSG_FLAG_AOP,
                                                    SCICLIENT_SERVICE_WAIT_FOREVER);
                if (status != CSL_PASS)
                {
                    AppUtils_Printf(MSG_NORMAL,
                                    "  Sciclient_pmSetModuleState 0x%x ...FAILED \n",
                                    LBIST_TestHandleArray[coreIndex].auxDeviceIdsP[i]);
                    testResult = -1;
                    break;
                }
            }
        }

        /**--- Step 2g: Place Primary core into retention ---*/
        if (testResult == 0)
        {
            if (LBIST_TestHandleArray[coreIndex].tisciDeviceId != 0u)
            {
                /* Placing Primary core into Retention */
#ifdef DEBUG
                AppUtils_Printf(MSG_NORMAL,
                                "  Primary core: Putting into Retention %s \n",
                                LBIST_TestHandleArray[coreIndex].coreName);
#endif
                status =  Sciclient_pmSetModuleState(LBIST_TestHandleArray[coreIndex].tisciDeviceId,
                                                     TISCI_MSG_VALUE_DEVICE_SW_STATE_RETENTION,
                                                     TISCI_MSG_FLAG_AOP,
                                                     SCICLIENT_SERVICE_WAIT_FOREVER);
                if (status != CSL_PASS)
                {
                    AppUtils_Printf(MSG_NORMAL,
                                    "   Primary core: Sciclient_pmSetModuleState...FAILED \n");
                    testResult = -1;
                }
            }
        }

        /**--- Step 2h: Place Secondary core into retention ---*/
        if (testResult == 0)
        {
            if (LBIST_TestHandleArray[coreIndex].tisciSecDeviceId != 0u)
            {
                /* Placing Secondary core into Retention */
#ifdef DEBUG
                AppUtils_Printf(MSG_NORMAL,
                                "  Secondary core: Putting into Retention %s \n",
                                LBIST_TestHandleArray[coreIndex].secCoreName);
#endif
                status =  Sciclient_pmSetModuleState(LBIST_TestHandleArray[coreIndex].tisciSecDeviceId,
                                                     TISCI_MSG_VALUE_DEVICE_SW_STATE_RETENTION,
                                                     TISCI_MSG_FLAG_AOP,
                                                     SCICLIENT_SERVICE_WAIT_FOREVER);
                if (status != CSL_PASS)
                {
                    AppUtils_Printf(MSG_NORMAL,
                                    "   Secondary core: Sciclient_pmSetModuleState...FAILED \n");
                    testResult = -1;
                }
            }
        }

        /**-- Step 3: Configure LBIST --*/
        if (testResult == 0)
        {
            /* Configure LBIST */

            status = CSL_LBIST_programConfig(pLBISTRegs, &LBIST_TestHandleArray[coreIndex].LBISTConfig);
            if (status != CSL_PASS)
            {
                AppUtils_Printf(MSG_NORMAL, "    LBIST program config failed \n");
                testResult = -1;
            }
        }

        /**-- Step 4: Run LBIST test --*/
#if defined(PROFILE_LBIST_TIMING)
        /* Get start time for LBIST test */
        testStartTime = TimerP_getTimeInUsecs();
#endif

        /**--- Step 4a: Enable Isolation ---*/
        if (testResult == 0)
        {
#ifdef DEBUG
            AppUtils_Printf(MSG_NORMAL, "\n Starting CSL_LBIST_enableIsolation\n");
#endif
            /* Call CSL API */
            status = CSL_LBIST_enableIsolation(pLBISTRegs);
            if (status != CSL_PASS)
            {
                AppUtils_Printf(MSG_NORMAL, "   CSL_LBIST_enableIsolation...FAILED \n");
                testResult = -1;
            }
        }

        /**--- Step 4b: reset LBIST ---*/
        if (testResult == 0)
        {
#ifdef DEBUG
            AppUtils_Printf(MSG_NORMAL, "\n Starting CSL_LBIST_reset \n");
#endif
            status = CSL_LBIST_reset(pLBISTRegs);
            if (status != CSL_PASS)
            {
                AppUtils_Printf(MSG_NORMAL, "   CSL_LBIST_reset...FAILED \n");
                testResult = -1;
            }
        }

        /**--- Step 4c: Enable Run BIST Mode ---*/
        if (testResult == 0)
        {
#ifdef DEBUG
            AppUtils_Printf(MSG_NORMAL, "\n Starting CSL_LBIST_enabledRunBISTMode\n");
#endif
            status = CSL_LBIST_enableRunBISTMode(pLBISTRegs);
            if (status != CSL_PASS)
            {
                AppUtils_Printf(MSG_NORMAL, "   CSL_LBIST_enableRunBISTMode...FAILED \n");
                testResult = -1;
            }
        }

        /**--- Step 4d: Start LBIST ---*/
        if (testResult == 0)
        {
#ifdef DEBUG
            AppUtils_Printf(MSG_NORMAL, "\n Starting CSL_LBIST_start\n");
#endif
            status = CSL_LBIST_start(pLBISTRegs);
            if (status != CSL_PASS)
            {
                AppUtils_Printf(MSG_NORMAL, "   CSL_LBIST_start...FAILED \n");
                testResult = -1;
            }
        }

        /**--- Step 4e: Check LBIST Running status ---*/
        if (testResult == 0)
        {
#ifdef DEBUG
            AppUtils_Printf(MSG_NORMAL, "\n Starting CSL_LBIST_isRunning\n");
#endif
            status = CSL_LBIST_isRunning(pLBISTRegs, &isLBISTRunning);
            if (status != CSL_PASS)
            {
                AppUtils_Printf(MSG_NORMAL, "   CSL_LBIST_isRunning...FAILED \n");
                testResult = -1;
            }
        }

        /**--- Step 4f: Wait for LBIST Interrupt ---*/
        if (testResult == 0)
        {
            /* Just checking if LBIST in running state */
            if ( isLBISTRunning != CSL_TRUE )
            {
#ifdef DEBUG
                AppUtils_Printf(MSG_NORMAL, "   LBIST not running \n");
#endif
            }

            /* Timeout if exceeds time */
            while ((!LBIST_TestHandleArray[coreIndex].doneFlag)
                   && (timeoutCount++ < LBIST_MAX_TIMEOUT_VALUE));

            if (!(LBIST_TestHandleArray[coreIndex].doneFlag))
            {
                AppUtils_Printf(MSG_NORMAL, "   LBIST test timed out \n");
                testResult = -1;
            }
            /* reset Done flag so we can run again */
            LBIST_TestHandleArray[coreIndex].doneFlag = false;
        }

        /**--- Step 4g: Get Signature of test ---*/
        if (testResult == 0)
        {
#ifdef DEBUG
            AppUtils_Printf(MSG_NORMAL, "\n Starting CSL_LBIST_getMISR\n");
#endif
            status = CSL_LBIST_getMISR(pLBISTRegs, &calculatedMISR);
            if (status != CSL_PASS)
            {
                AppUtils_Printf(MSG_NORMAL, "   Get MISR failed \n");
                testResult = -1;
            }
        }

        /**--- Step 4h: Get Expected Signature ---*/
        if (testResult == 0)
        {
#ifdef DEBUG
            AppUtils_Printf(MSG_NORMAL, "\n Starting CSL_LBIST_getExpectedMISR\n");
#endif
            status = CSL_LBIST_getExpectedMISR(pLBISTSig, &expectedMISR);
            if ( status != CSL_PASS)
            {
                AppUtils_Printf(MSG_NORMAL, "   Get Expected MISR failed \n");
                testResult = -1;
            }
        }

        /**--- Step 4i: Clear Run BIST Mode ---*/
        if (testResult == 0)
        {
#ifdef DEBUG
            AppUtils_Printf(MSG_NORMAL, "\n Starting CSL_LBIST_clearRunBISTMode\n");
#endif
            status = CSL_LBIST_clearRunBISTMode(pLBISTRegs);
            if ( status != CSL_PASS)
            {
                AppUtils_Printf(MSG_NORMAL,
                                "   CSL_LBIST_clearRunBISTMode failed \n");
                testResult = -1;
            }
        }

        /**--- Step 4j: Stop LBIST ---*/
        if (testResult == 0)
        {
#ifdef DEBUG
            AppUtils_Printf(MSG_NORMAL, "\n Starting CSL_LBIST_stop\n");
#endif
            status = CSL_LBIST_stop(pLBISTRegs);
            if ( status != CSL_PASS)
            {
                AppUtils_Printf(MSG_NORMAL, "   CSL_LBIST_stop failed \n");
                testResult = -1;
            }
        }

        /**--- Step 4k: Reset LBIST ---*/
        if (testResult == 0)
        {
#ifdef DEBUG
            AppUtils_Printf(MSG_NORMAL, "\n Starting CSL_LBIST_reset\n");
#endif
            status = CSL_LBIST_reset(pLBISTRegs);
            if ( status != CSL_PASS)
            {
                AppUtils_Printf(MSG_NORMAL, "   CSL_LBIST_reset failed \n");
                testResult = -1;
            }
        }

#if defined(PROFILE_LBIST_TIMING)
        /* Here LBIST test is complete , get end time of test */
        testEndTime = TimerP_getTimeInUsecs();
#endif

        /**-- Step 5: Restore cores --*/
        /* The following sequence is needed to restore core to normal operation */

        /**--- Step 5a: Switch off Secondary core ---*/
        if (testResult == 0)
        {
            if (LBIST_TestHandleArray[coreIndex].secondaryCoreNeeded)
            {
                /* Power off Secondary core */
#ifdef DEBUG
                AppUtils_Printf(MSG_NORMAL,
                                "  Secondary core: Powering off %s \n",
                                LBIST_TestHandleArray[coreIndex].secCoreName);
#endif
                status =  Sciclient_pmSetModuleState(LBIST_TestHandleArray[coreIndex].tisciSecDeviceId,
                                                     TISCI_MSG_VALUE_DEVICE_SW_STATE_AUTO_OFF,
                                                     TISCI_MSG_FLAG_AOP,
                                                     SCICLIENT_SERVICE_WAIT_FOREVER);
                if (status != CSL_PASS)
                {
                    AppUtils_Printf(MSG_NORMAL,
                                    "   Secondary core: Sciclient_pmSetModuleState:  Power off FAILED \n");
                    testResult = -1;
                }
            }
        }

        /**--- Step 5b: Switch off Primary core ---*/
        if (testResult == 0)
        {
            /* Power off Primary core */
#ifdef DEBUG
            AppUtils_Printf(MSG_NORMAL, "  Primary core: Powering off %s \n",
                        LBIST_TestHandleArray[coreIndex].coreName);
#endif
            status =  Sciclient_pmSetModuleState(LBIST_TestHandleArray[coreIndex].tisciDeviceId,
                                                 TISCI_MSG_VALUE_DEVICE_SW_STATE_AUTO_OFF,
                                                 TISCI_MSG_FLAG_AOP,
                                                 SCICLIENT_SERVICE_WAIT_FOREVER);
            if (status != CSL_PASS)
            {
                AppUtils_Printf(MSG_NORMAL,
                                "   Primary core: Sciclient_pmSetModuleState: Power off FAILED \n");
            }
        }

        /**--- Step 5c: Switch off Auxilliary modules ---*/
        if (testResult == 0)
        {
            int i;

            /* Place all Auxilliary modules required for test into retention */
            for ( i = 0; i < LBIST_TestHandleArray[coreIndex].numAuxDevices; i++)
            {
#ifdef DEBUG
                AppUtils_Printf(MSG_NORMAL,
                                "  Powering off Device number %d Device Id %x\n",
                                i,
                                LBIST_TestHandleArray[coreIndex].auxDeviceIdsP[i]);
#endif

                status = Sciclient_pmSetModuleState(LBIST_TestHandleArray[coreIndex].auxDeviceIdsP[i],
                                                    TISCI_MSG_VALUE_DEVICE_SW_STATE_AUTO_OFF,
                                                    TISCI_MSG_FLAG_AOP,
                                                    SCICLIENT_SERVICE_WAIT_FOREVER);
                if (status != CSL_PASS)
                {
                    AppUtils_Printf(MSG_NORMAL,
                                    "  Sciclient_pmSetModuleState 0x%x ...FAILED \n",
                                    LBIST_TestHandleArray[coreIndex].auxDeviceIdsP[i]);
                    testResult = -1;
                    break;
                }
            }
        }

        /**--- Step 5d: Disable Isolation ---*/
        if (testResult == 0)
        {
#ifdef DEBUG
            AppUtils_Printf(MSG_NORMAL, "  Disabling isolation \n");
#endif
            status = CSL_LBIST_disableIsolation(pLBISTRegs);
            if (status != CSL_PASS)
            {
                AppUtils_Printf(MSG_NORMAL, "   CSL_LBIST_disableIsolation ...FAILED \n");
                testResult = -1;
            }
        }

        /**--- Step 5e: Place all Auxilliary modules into retention ---*/
        if (testResult == 0)
        {
            int i;

            /* Place all Auxilliary modules required for test into retention */
            for ( i = 0; i < LBIST_TestHandleArray[coreIndex].numAuxDevices; i++)
            {
#ifdef DEBUG
                AppUtils_Printf(MSG_NORMAL,
                                "  Putting into Retention Device number %d Device Id %x\n",
                                i,
                                LBIST_TestHandleArray[coreIndex].auxDeviceIdsP[i]);
#endif

                status = Sciclient_pmSetModuleState(LBIST_TestHandleArray[coreIndex].auxDeviceIdsP[i],
                                                    TISCI_MSG_VALUE_DEVICE_SW_STATE_RETENTION,
                                                    TISCI_MSG_FLAG_AOP,
                                                    SCICLIENT_SERVICE_WAIT_FOREVER);
                if (status != CSL_PASS)
                {
                    AppUtils_Printf(MSG_NORMAL,
                                    "  Sciclient_pmSetModuleState 0x%x ...FAILED \n",
                                    LBIST_TestHandleArray[coreIndex].auxDeviceIdsP[i]);
                    testResult = -1;
                    break;
                }
            }
        }

        /**--- Step 5f: Place Primary core into retention ---*/
        if (testResult == 0)
        {
            /* Placing Primary core into Retention */
#ifdef DEBUG
            AppUtils_Printf(MSG_NORMAL, "  Primary core: Putting into Retention %s \n",
                            LBIST_TestHandleArray[coreIndex].coreName);
#endif
            status = Sciclient_pmSetModuleState(LBIST_TestHandleArray[coreIndex].tisciDeviceId,
                                                TISCI_MSG_VALUE_DEVICE_SW_STATE_RETENTION,
                                                TISCI_MSG_FLAG_AOP,
                                                SCICLIENT_SERVICE_WAIT_FOREVER);

            if (status != CSL_PASS)
            {
                AppUtils_Printf(MSG_NORMAL,
                                "   Primary core: Sciclient_pmSetModuleState 0x%x ...FAILED \n",
                            LBIST_TestHandleArray[coreIndex].tisciDeviceId);
                testResult = -1;
            }
        }

        /**--- Step 5g: Place Secondary core into retention ---*/
        if (testResult == 0)
        {
            if (LBIST_TestHandleArray[coreIndex].secondaryCoreNeeded)
            {
                /* Placing Secondary core into Retention */
#ifdef DEBUG
                AppUtils_Printf(MSG_NORMAL,
                                "  Secondary core: Putting into Retention %s \n",
                                LBIST_TestHandleArray[coreIndex].secCoreName);
#endif
                status = Sciclient_pmSetModuleState(LBIST_TestHandleArray[coreIndex].tisciSecDeviceId,
                                                    TISCI_MSG_VALUE_DEVICE_SW_STATE_RETENTION,
                                                    TISCI_MSG_FLAG_AOP,
                                                    SCICLIENT_SERVICE_WAIT_FOREVER);
                if (status != CSL_PASS)
                {
                    AppUtils_Printf(MSG_NORMAL,
                                    "   Secondary core: Sciclient_pmSetModuleState 0x%x ...FAILED \n",
                                LBIST_TestHandleArray[coreIndex].tisciSecDeviceId);
                    testResult = -1;
                    return testResult;
                }
            }
        }

        /**--- Step 5h: Double check LBIST not running ---*/
        if (testResult == 0)
        {
#ifdef DEBUG
            AppUtils_Printf(MSG_NORMAL,
                            "  Checking that LBIST is not running\n");
#endif
            /* Check to make sure LBIST is not running */
            status = CSL_LBIST_isRunning(pLBISTRegs, &isLBISTRunning);
            if ( status != CSL_PASS )
            {
                AppUtils_Printf(MSG_NORMAL, "\n CSL_LBIST_isRunning failed \n");
                testResult = -1;
            }
        }

        /**--- Step 5i: Power off Secondary core ---*/
        if (testResult == 0)
        {
            if (LBIST_TestHandleArray[coreIndex].secondaryCoreNeeded)
            {
                /* Power off Secondary core */
#ifdef DEBUG
                AppUtils_Printf(MSG_NORMAL,
                                "  Secondary core: Powering off %s \n",
                                LBIST_TestHandleArray[coreIndex].secCoreName);
#endif
                status =  Sciclient_pmSetModuleState(LBIST_TestHandleArray[coreIndex].tisciSecDeviceId,
                                                     TISCI_MSG_VALUE_DEVICE_SW_STATE_AUTO_OFF,
                                                     TISCI_MSG_FLAG_AOP,
                                                     SCICLIENT_SERVICE_WAIT_FOREVER);
                if (status != CSL_PASS)
                {
                    AppUtils_Printf(MSG_NORMAL,
                                    "   Secondary core: Sciclient_pmSetModuleState:  Power off FAILED \n");
                    testResult = -1;
                }
            }
        }

        /**--- Step 5j: Power off Primary core ---*/
        if (testResult == 0)
        {
            /* Power off Primary core */
#ifdef DEBUG
            AppUtils_Printf(MSG_NORMAL,
                            "  Primary core: Powering off %s \n",
                            LBIST_TestHandleArray[coreIndex].coreName);
#endif
            status =  Sciclient_pmSetModuleState(LBIST_TestHandleArray[coreIndex].tisciDeviceId,
                                                 TISCI_MSG_VALUE_DEVICE_SW_STATE_AUTO_OFF,
                                                 TISCI_MSG_FLAG_AOP,
                                                 SCICLIENT_SERVICE_WAIT_FOREVER);
            if (status != CSL_PASS)
            {
                AppUtils_Printf(MSG_NORMAL,
                                "   Primary core: Sciclient_pmSetModuleState: Power off FAILED \n");
            }
        }

        /**--- Step 5k: Power off Auxilliary modules ---*/
        if (testResult == 0)
        {
            int i;

            /* Place all Auxilliary modules required for test into retention */
            for ( i = 0; i < LBIST_TestHandleArray[coreIndex].numAuxDevices; i++)
            {
#ifdef DEBUG
                AppUtils_Printf(MSG_NORMAL,
                                "  Powering off Device number %d Device Id %x\n",
                                i,
                                LBIST_TestHandleArray[coreIndex].auxDeviceIdsP[i]);
#endif

                status = Sciclient_pmSetModuleState(LBIST_TestHandleArray[coreIndex].auxDeviceIdsP[i],
                                                    TISCI_MSG_VALUE_DEVICE_SW_STATE_AUTO_OFF,
                                                    TISCI_MSG_FLAG_AOP,
                                                    SCICLIENT_SERVICE_WAIT_FOREVER);
                if (status != CSL_PASS)
                {
                    AppUtils_Printf(MSG_NORMAL,
                                    "  Sciclient_pmSetModuleState 0x%x ...FAILED \n",
                                    LBIST_TestHandleArray[coreIndex].auxDeviceIdsP[i]);
                    testResult = -1;
                    break;
                }
            }
        }

        /**--- Step 5l: Take Primary core out of local reset ---*/
        if ((testResult == 0) && (LBIST_TestHandleArray[coreIndex].tisciDeviceId != NULL))
        {
#ifdef DEBUG
            AppUtils_Printf(MSG_NORMAL,
                            "  Primary core: Taking out of local reset the core %s \n",
                            LBIST_TestHandleArray[coreIndex].coreName);
#endif
            status = Sciclient_pmSetModuleRst(LBIST_TestHandleArray[coreIndex].tisciDeviceId,
                                              0x0,
                                              SCICLIENT_SERVICE_WAIT_FOREVER);
            if (status != CSL_PASS)
            {
                AppUtils_Printf(MSG_NORMAL,
                                "  Sciclient_pmSetModuleRst 0x%x ...FAILED \n",
                                LBIST_TestHandleArray[coreIndex].tisciDeviceId);
                testResult = -1;
            }
        }

        /**--- Step 5m: Take Secondary core out of local reset ---*/
        if ((testResult == 0) && (LBIST_TestHandleArray[coreIndex].tisciSecDeviceId != NULL))
        {
#ifdef DEBUG
            AppUtils_Printf(MSG_NORMAL,
                            "  Secondary core: Taking out of local reset the core %s \n",
                            LBIST_TestHandleArray[coreIndex].secCoreName);
#endif
            status = Sciclient_pmSetModuleRst(LBIST_TestHandleArray[coreIndex].tisciSecDeviceId,
                                              0x0,
                                              SCICLIENT_SERVICE_WAIT_FOREVER);
            if (status != CSL_PASS)
            {
                AppUtils_Printf(MSG_NORMAL,
                                "  Sciclient_pmSetModuleRst 0x%x ...FAILED \n",
                                LBIST_TestHandleArray[coreIndex].tisciSecDeviceId);
                testResult = -1;
            }
        }

        /**--- Step 5n: Take Auxilliary modules out of module reset ---*/
        if (testResult == 0)
        {
            int i;

            /* Place all Auxilliary modules required for test into module reset */
            for ( i = 0; i < LBIST_TestHandleArray[coreIndex].numAuxDevices; i++)
            {
#ifdef DEBUG
                AppUtils_Printf(MSG_NORMAL,
                                "  Putting into module reset Device number %d Device Id %x\n",
                                i,
                                LBIST_TestHandleArray[coreIndex].auxDeviceIdsP[i]);
#endif

                status = Sciclient_pmSetModuleRst(LBIST_TestHandleArray[coreIndex].auxDeviceIdsP[i],
                                                  0x0, // Need to keep Local Reset too??
                                                  SCICLIENT_SERVICE_WAIT_FOREVER);
                if (status != CSL_PASS)
                {
                    AppUtils_Printf(MSG_NORMAL,
                                    "  Sciclient_pmSetModuleState 0x%x ...FAILED \n",
                                    LBIST_TestHandleArray[coreIndex].auxDeviceIdsP[i]);
                    testResult = -1;
                    break;
                }
            }
        }

        /**--- Step 5o: Release Primary core ---*/
        if ((testResult == 0) && (LBIST_TestHandleArray[coreIndex].tisciProcId !=0))
        {
            /* release processor Primary core */
#ifdef DEBUG
            AppUtils_Printf(MSG_NORMAL,
                            "  Primary core: Releasing %s \n",
                            LBIST_TestHandleArray[coreIndex].coreName);
#endif

            status = Sciclient_procBootReleaseProcessor(LBIST_TestHandleArray[coreIndex].tisciProcId,
                                                        TISCI_MSG_FLAG_AOP,
                                                        SCICLIENT_SERVICE_WAIT_FOREVER);
            if (status != CSL_PASS)
            {
                AppUtils_Printf(MSG_NORMAL,
                                "   Primary core: Sciclient_procBootReleaseProcessor, ProcId 0x%x...FAILED \n",
                                LBIST_TestHandleArray[coreIndex].tisciProcId);
                testResult = -1;
            }
        }

        /**--- Step 5p: Release Secondary core ---*/
        if (testResult == 0)
        {
            if ((LBIST_TestHandleArray[coreIndex].secondaryCoreNeeded)
                && (LBIST_TestHandleArray[coreIndex].tisciSecDeviceId != 0u))
            {
                /* release processor Secondary core */
#ifdef DEBUG
                AppUtils_Printf(MSG_NORMAL,
                                "  Secondary core: Releasing %s \n",
                                LBIST_TestHandleArray[coreIndex].secCoreName);
#endif
                status = Sciclient_procBootReleaseProcessor(LBIST_TestHandleArray[coreIndex].tisciSecProcId,
                                                            TISCI_MSG_FLAG_AOP,
                                                            SCICLIENT_SERVICE_WAIT_FOREVER);
                if (status != CSL_PASS)
                {
                    AppUtils_Printf(MSG_NORMAL,
                                    "   Secondary core: Sciclient_procBootReleaseProcessor, ProcId 0x%x...FAILED \n",
                                    LBIST_TestHandleArray[coreIndex].tisciSecProcId);
                    testResult = -1;
                }
            }
        }

        /**--- Step 5q: Double check LBIST not running ---*/
        if (testResult == 0)
        {
            status = CSL_LBIST_isRunning(pLBISTRegs, &isLBISTRunning);
            if (status != CSL_PASS)
            {
                AppUtils_Printf(MSG_NORMAL, "    CSL_LBIST_isRunning failed\n");
                testResult = -1;
            }
            else
            {
                /* Make sure that the LBIST is not running */
                if (isLBISTRunning == CSL_TRUE)
                {
                    AppUtils_Printf(MSG_NORMAL,
                                    "\n   LBIST is found to be still running at the end of the test\n");
                    testResult = -1;
                }
            }
        }
#if defined(PROFILE_LBIST_TIMING)
        /* Here LBIST test is complete , get end time of test */
        endTime = TimerP_getTimeInUsecs();
#endif

        /**--- Step 6: Check result of LBIST  ---*/
        if (testResult == 0)
        {
            /* TODO: Temporarily hard coding to expected MISR */
            expectedMISR = LBIST_TestHandleArray[coreIndex].expectedMISR;
            if (calculatedMISR != expectedMISR)
            {
                AppUtils_Printf(MSG_NORMAL,
                                "\n   LBIST failed with MISR mismatch: Expected 0x%x got 0x%x \n",
                            expectedMISR, calculatedMISR);
                testResult = -1;
            }
#if defined(PROFILE_LBIST_TIMING)
            else
            {
                AppUtils_Printf(MSG_NORMAL, "\n   LBIST MISR matched \n");
            }
#endif
        }

#if defined(PROFILE_LBIST_TIMING)
        prepTime = testStartTime - startTime;
        diffTime = testEndTime - testStartTime;
        restoreTime = endTime - testEndTime;

        AppUtils_Printf(MSG_NORMAL,
                        "  Delta Cores prep time in micro secs %d \n",
                        (uint32_t)prepTime);
        AppUtils_Printf(MSG_NORMAL,
                        "  Delta LBIST execution time in micro secs %d \n",
                        (uint32_t)diffTime);
        AppUtils_Printf(MSG_NORMAL,
                        "  Delta Cores restore time in micro secs %d \n",
                        (uint32_t)restoreTime);
        AppUtils_Printf(MSG_NORMAL,
                        "  LBIST complete for %s \n",
                        LBIST_TestHandleArray[coreIndex].coreName);
#endif
    } /* else clause of "if ((testResult == 0) &&
       *     (LBIST_TestHandleArray[coreIndex].hwPostCoreCheck == true))" */

    return (testResult);
}

/* HW POST-related functions */

const char *LBIST_hwPostStatusPrint(int32_t postStatus)
{
    char *name;

    switch(postStatus)
    {
        case (LBIST_POST_COMPLETED_SUCCESS):
            name="LBIST_POST_COMPLETED_SUCCESS";
            break;
        case (LBIST_POST_COMPLETED_FAILURE):
            name="LBIST_POST_COMPLETED_FAILURE";
            break;
        case (LBIST_POST_ATTEMPTED_TIMEOUT):
            name="LBIST_POST_ATTEMPTED_TIMEOUT";
            break;
        case (LBIST_POST_NOT_RUN):
            name="LBIST_POST_NOT_RUN";
            break;
        case (LBIST_POST_INVALID_MISR_SEED_INPUT):
            name="LBIST_POST_INVALID_MISR_SEED_INPUT";
            break;
        default:
            name="INVALID ID";
            break;
    }

    return name;
}

static int32_t LBIST_isPostLbistTimeout(uint32_t postStatMmrRegVal,
                                        uint8_t section,
                                        Bool *pIsTimedOut)
{
    int32_t status = CSL_PASS;
    uint32_t shift;

    if ((pIsTimedOut == NULL) || (section > LBIST_POST_CORE_MAX))
    {
        status = CSL_EBADARGS;
    }
    else
    {
        if (section == LBIST_POST_CORE_DMSC)
        {
            shift = CSL_WKUP_CTRL_MMR_CFG0_WKUP_POST_STAT_POST_DMSC_LBIST_TIMEOUT_SHIFT;
        }
        else if (section == LBIST_POST_CORE_MCU)
        {
            shift = CSL_WKUP_CTRL_MMR_CFG0_WKUP_POST_STAT_POST_MCU_LBIST_TIMEOUT_SHIFT;
        }

        *pIsTimedOut = ((postStatMmrRegVal >> shift) & 0x1u) ? CSL_TRUE : CSL_FALSE;
    }

    return status;
}

static int32_t LBIST_isPostLbistDone(uint32_t postStatMmrRegVal,
                                     uint8_t section,
                                     Bool *pIsDone)
{
    int32_t status = CSL_PASS;
    uint32_t shift;

    if ((pIsDone == NULL) || (section > LBIST_POST_CORE_MAX))
    {
        status = CSL_EBADARGS;
    }
    else
    {
        if (section == LBIST_POST_CORE_DMSC)
        {
            shift = CSL_WKUP_CTRL_MMR_CFG0_WKUP_POST_STAT_POST_DMSC_LBIST_DONE_SHIFT;
        }
        else if (section == LBIST_POST_CORE_MCU)
        {
            shift = CSL_WKUP_CTRL_MMR_CFG0_WKUP_POST_STAT_POST_MCU_LBIST_DONE_SHIFT;
        }

        *pIsDone = ((postStatMmrRegVal >> shift) & 0x1u) ? CSL_TRUE : CSL_FALSE;
    }
    return status;
}

static int32_t LBIST_runPostLbistCheck(uint32_t hwPostCoreNum,
                                       CSL_lbistRegs *pLBISTRegs,
                                       uint32_t *pLBISTSig)
{
    int32_t  status = 0;
    uint32_t calculatedMISR;
    uint32_t expectedMISR;
    int32_t  postStatus = LBIST_POST_COMPLETED_SUCCESS;
    uint32_t postRegVal;
    Bool     LBISTResult;

    /* Read HW POST status register */
    postRegVal = CSL_REG32_RD(CSL_WKUP_CTRL_MMR0_CFG0_BASE +
                              CSL_WKUP_CTRL_MMR_CFG0_WKUP_POST_STAT);

    /* Check if HW POST LBIST was performed */
    status = LBIST_isPostLbistDone(postRegVal, hwPostCoreNum, &LBISTResult);
    if (status != CSL_PASS)
    {
        AppUtils_Printf(MSG_NORMAL,
                        "   HW POST: LBIST_isPostLbistDone failed for LBIST core ID #%d\n",
                        hwPostCoreNum);
    }
    else
    {
        if (LBISTResult != true)
        {
            /* HW POST: LBIST not completed, check if it timed out */
            status = LBIST_isPostLbistTimeout(postRegVal,
                                              hwPostCoreNum,
                                              &LBISTResult);
            if (LBISTResult != true)
            {
                /* HW POST: LBIST was not performed at all on this device
                 * for this core */
                postStatus = LBIST_POST_NOT_RUN;
#ifdef DEBUG
                AppUtils_Printf(MSG_NORMAL,
                                "\n   HW POST: LBIST not run on this device for LBIST core ID #%d\n",
                                hwPostCoreNum);
#endif
            }
            else
            {
                /* HW POST: LBIST was attempted but timed out for this section */
                postStatus = LBIST_POST_ATTEMPTED_TIMEOUT;
#ifdef DEBUG
                AppUtils_Printf(MSG_NORMAL,
                                "\n   HW POST: LBIST failed with HW POST timeout for LBIST core ID #%d\n",
                                hwPostCoreNum);
#endif
            }
        }
        else
        {
            /* Get the output MISR and the expected MISR */
            if (status == 0)
            {
                status = CSL_LBIST_getMISR(pLBISTRegs, &calculatedMISR);
                if (status != CSL_PASS)
                {
                    AppUtils_Printf(MSG_NORMAL,
                                    "\n HW POST: Get MISR failed for LBIST core ID#%d\n",
                                    hwPostCoreNum);
                }
            }

            if (status == 0)
            {
                status = CSL_LBIST_getExpectedMISR(pLBISTSig, &expectedMISR);
                if ( status != CSL_PASS)
                {
                    AppUtils_Printf(MSG_NORMAL,
                                    "\n HW POST: Get Expected MISR failed for LBIST core ID#%d\n",
                                    hwPostCoreNum);
                }
            }

            if (status == 0)
            {
                /* Compare the results, and set failure if they do not match */
#ifdef DEBUG
                AppUtils_Printf(MSG_NORMAL,
                                " For HW POST LBIST core %d, Expected MISR= 0x%x, Calculated MISR = 0x%x\n",
                                hwPostCoreNum,
                                expectedMISR,
                                calculatedMISR);
#endif
                if (calculatedMISR != expectedMISR)
                {
                    /* HW POST: LBIST was completed, but the test failed for this
                     * core */
                    postStatus = LBIST_POST_COMPLETED_FAILURE;
                    AppUtils_Printf(MSG_NORMAL,
                                    "\n   HW POST: LBIST core ID#%d failed with MISR mismatch: Expected 0x%x got 0x%x\n",
                                    hwPostCoreNum,
                                    expectedMISR,
                                    calculatedMISR);
                }
                else if ((calculatedMISR == 0) && (expectedMISR == 0))
                {
                    postStatus = LBIST_POST_INVALID_MISR_SEED_INPUT;
#ifdef DEBUG
                    AppUtils_Printf(MSG_NORMAL,
                                    "\n   HW POST: Device does not contain proper seed for MISR in eFuse - "\
                                    "HW POST LBIST not supported on this device \n");
#endif
                }
            } /* if (status == 0) */
        } /* if (LBISTResult != true) */
    } /* if (status != CSL_PASS) */

    if (status == 0)
    {
        /* All function calls returned successfully */
        return(postStatus);
    }
    else
    {
        /* Error in function calls */
        return(status);
    }
}

