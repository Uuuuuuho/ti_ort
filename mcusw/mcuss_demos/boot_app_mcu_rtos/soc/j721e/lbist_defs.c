/*
*
* Copyright (c) 2020 Texas Instruments Incorporated
*
* All rights reserved not granted herein.
*
* Limited License.
*
* Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
* license under copyrights and patents it now or hereafter owns or controls to make,
* have made, use, import, offer to sell and sell ("Utilize") this software subject to the
* terms herein.  With respect to the foregoing patent license, such license is granted
* solely to the extent that any such patent is necessary to Utilize the software alone.
* The patent license shall not apply to any combinations which include this software,
* other than combinations with devices manufactured by or for TI ("TI Devices").
* No hardware patent is licensed hereunder.
*
* Redistributions must preserve existing copyright notices and reproduce this license
* (including the above copyright notice and the disclaimer and (if applicable) source
* code license limitations below) in the documentation and/or other materials provided
* with the distribution
*
* Redistribution and use in binary form, without modification, are permitted provided
* that the following conditions are met:
*
* *       No reverse engineering, decompilation, or disassembly of this software is
* permitted with respect to any software provided in binary form.
*
* *       any redistribution and use are licensed by TI for use only with TI Devices.
*
* *       Nothing shall obligate TI to provide you with source code for the software
* licensed and provided to you in object code.
*
* If software source code is provided to you, modification and redistribution of the
* source code are permitted provided that the following conditions are met:
*
* *       any redistribution and use of the source code, including any resulting derivative
* works, are licensed by TI for use only with TI Devices.
*
* *       any redistribution and use of any object code compiled from the source code
* and any resulting derivative works, are licensed by TI for use only with TI Devices.
*
* Neither the name of Texas Instruments Incorporated nor the names of its suppliers
*
* may be used to endorse or promote products derived from this software without
* specific prior written permission.
*
* DISCLAIMER.
*
* THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
* OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
* OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
* IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
* BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
* OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
* OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
* OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/**
 *  \file lbist_defs.c
 *
 *  \brief LBIST SOC-specific structures and functions
 */

/* ========================================================================== */
/*                             Include Files                                  */
/* ========================================================================== */
#include <stdbool.h>
#include <ti/csl/csl_types.h>
#include <ti/csl/csl_lbist.h>
#include <ti/csl/soc.h>
#include <ti/drv/sciclient/sciclient.h>

#include "lbist_utils.h"
#include "lbist_defs.h"

/* ========================================================================== */
/*                           Macros & Typedefs                                */
/* ========================================================================== */

/* LBIST clocks should target 200MHz for each core */

/* Lbist Parameters */
#define LBIST_DC_DEF                   (0x3u)
#define LBIST_DIVIDE_RATIO             (0x02u)
#define LBIST_STATIC_PC_DEF            (0x3fffu)
#define LBIST_RESET_PC_DEF             (0x0fu)
#define LBIST_SET_PC_DEF               (0x00u)
#define LBIST_SCAN_PC_DEF              (0x08u)
#define LBIST_PRPG_DEF                 (0x1fffffffffffffu)

/*
* LBIST setup parameters for each core
*/
#define LBIST_MAIN_R5_1_STATIC_PC_DEF  LBIST_STATIC_PC_DEF
#define LBIST_MAIN_R5_2_STATIC_PC_DEF  LBIST_STATIC_PC_DEF
#define LBIST_C7X_STATIC_PC_DEF        (2816)
#define LBIST_A72_STATIC_PC_DEF        (12288)
#define LBIST_DMPAC_STATIC_PC_DEF      (6272)
#define LBIST_VPAC_STATIC_PC_DEF       (5056)

/*
* LBIST expected MISR's (using parameters above)
*/
#define MAIN_R5_MISR_EXP_VAL           (0xad7f4501)
#define A72_MISR_EXP_VAL               (0xdd5cd3b3)
#define C7X_MISR_EXP_VAL               (0xd67bfff1)
#define VPAC_MISR_EXP_VAL              (0x18b373bf)
#define DMPAC_MISR_EXP_VAL             (0xf22e52b5)

#define LBIST_MAX_TIMEOUT_VALUE        10000000u

#define A72_NUM_AUX_DEVICES            1

/* ========================================================================== */
/*                 Internal Function Declarations                             */
/* ========================================================================== */
void LBIST_eventHandler(uint32_t coreIndex);

/* ========================================================================== */
/*                         Structure Declarations                             */
/* ========================================================================== */

/* None */

/* ========================================================================== */
/*                          Function Declarations                             */
/* ========================================================================== */

/* ========================================================================== */
/*                            Global Variables                                */
/* ========================================================================== */

uint32_t LBIST_A72AuxDevList[A72_NUM_AUX_DEVICES] =
{
    TISCI_DEV_A72SS0,
};


LBIST_TestHandle_t LBIST_TestHandleArray[LBIST_MAX_CORE_INDEX+1] =
{
 /* HW POST - DMSC - Checks MISR results only */
 {
  .coreName               = "HWPOST - DMSC",
  .pLBISTRegs             = (CSL_lbistRegs *)(CSL_DMSC_LBIST_BASE),
  .pLBISTSig              = (uint32_t *)(CSL_DMSC_LBIST_SIG),
  .doneFlag               = false,                    /* Initialize done flag */
  .numAuxDevices          = 0u,                       /* No Aux devices */
  .hwPostCoreCheck        = true,
  .hwPostCoreNum          = LBIST_POST_CORE_DMSC,
  .handler                = NULL,                     /* No LBIST event handler */
 },
 /* HW POST - MCU - Checks MISR results only */
 {
  .coreName               = "HWPOST - MCU",
  .pLBISTRegs             = (CSL_lbistRegs *)(CSL_MCU_LBIST_BASE),
  .pLBISTSig              = (uint32_t *)(CSL_MCU_LBIST_SIG),
  .doneFlag               = false,                    /* Initialize done flag */
  .numAuxDevices          = 0u,                       /* No Aux devices */
  .hwPostCoreCheck        = true,
  .hwPostCoreNum          = LBIST_POST_CORE_MCU,
  .handler                = NULL,                     /* No LBIST event handler */
 },
 /* Main R5F 0 */
 {
  .coreName               = "Main R5F0-0",
  .secondaryCoreNeeded    = true,             /* Secondary core needed */
  .wfiCheckNeeded         = false,            /* wfi check needed */
  .secCoreName            = "Main R5F1-0",    /* Secondary core */
  .pLBISTRegs             = (CSL_lbistRegs *)(CSL_MAIN_R5F0_LBIST_BASE),
  .pLBISTSig              = (uint32_t *)(CSL_MAIN_R5F0_LBIST_SIG),
  .expectedMISR           = MAIN_R5_MISR_EXP_VAL,           /* Expected signature for main R5 0*/
  .cpuStatusFlagMask      = TISCI_MSG_VAL_PROC_BOOT_STATUS_FLAG_R5_WFI, /* Expected boot status value for wfi */
  .handler                = LBIST_eventHandler,             /* LBIST event handler */
  .interruptNumber        = CSLR_MCU_R5FSS0_CORE0_INTR_GLUELOGIC_MAIN_PULSAR0_LBIST_GLUE_DFT_LBIST_BIST_DONE_0, /* BIST DONE interrupt number */
  .tisciProcId            = SCICLIENT_PROC_ID_R5FSS0_CORE0, /* Main R5F core 0 Proc Id */
  .tisciSecProcId         = SCICLIENT_PROC_ID_R5FSS0_CORE1, /* Main R5F core 1 Proc Id */
  .tisciDeviceId          = TISCI_DEV_R5FSS0_CORE0,         /* Main R5F core 0 Device Id */
  .tisciSecDeviceId       = TISCI_DEV_R5FSS0_CORE1,         /* Main R5F core 1 Device Id */
  .doneFlag               = false,                          /* Initialize done flag */
  .numAuxDevices          = 0u,                       /* No Aux devices */
  .LBISTConfig = {
      .dc_def        = LBIST_DC_DEF,
      .divide_ratio  = LBIST_DIVIDE_RATIO,
      .static_pc_def = LBIST_STATIC_PC_DEF,
      .set_pc_def    = LBIST_SET_PC_DEF,
      .reset_pc_def  = LBIST_RESET_PC_DEF,
      .scan_pc_def   = LBIST_SCAN_PC_DEF,
      .prpg_def      = LBIST_PRPG_DEF,
  },
 },
  /* Main R5F 1 */
 {
  .coreName               = "Main R5F1-0",
  .secondaryCoreNeeded    = true,            /* Secondary core needed */
  .wfiCheckNeeded         = false,           /* wfi check needed */
  .secCoreName            = "Main R5F1-1",   /* Secondary core */
  .pLBISTRegs             = (CSL_lbistRegs *)(CSL_MAIN_R5F1_LBIST_BASE),
  .pLBISTSig              = (uint32_t *)(CSL_MAIN_R5F1_LBIST_SIG),
  .expectedMISR           = MAIN_R5_MISR_EXP_VAL,           /* Expected signature Main R5 1*/
  .cpuStatusFlagMask      = TISCI_MSG_VAL_PROC_BOOT_STATUS_FLAG_R5_WFI, /* Expected boot status value for wfi */
  .handler                = LBIST_eventHandler,             /* LBIST event handler */
  .interruptNumber        = CSLR_MCU_R5FSS0_CORE0_INTR_GLUELOGIC_MAIN_PULSAR1_LBIST_GLUE_DFT_LBIST_BIST_DONE_0,/* BIST DONE interrupt number */
  .tisciProcId            = SCICLIENT_PROC_ID_R5FSS1_CORE0, /* Main R5F core 0 Proc Id */
  .tisciSecProcId         = SCICLIENT_PROC_ID_R5FSS1_CORE1, /* Main R5F core 1 Proc Id */
  .tisciDeviceId          = TISCI_DEV_R5FSS1_CORE0,         /* Main R5F core 0 Device id */
  .tisciSecDeviceId       = TISCI_DEV_R5FSS1_CORE1,         /* Main R5F core 1 Device id */
  .numAuxDevices          = 0u,                             /* No Aux devices */
  .doneFlag               = false,                          /* Initialize done flag */
  .LBISTConfig = {
      .dc_def        = LBIST_DC_DEF,
      .divide_ratio  = LBIST_DIVIDE_RATIO,
      .static_pc_def = LBIST_STATIC_PC_DEF,
      .set_pc_def    = LBIST_SET_PC_DEF,
      .reset_pc_def  = LBIST_RESET_PC_DEF,
      .scan_pc_def   = LBIST_SCAN_PC_DEF,
      .prpg_def      = LBIST_PRPG_DEF,
  },
 },
 /* C7x */
 {
  .coreName               = "C7x ",
  .secondaryCoreNeeded    = false,    /* Secondary core needed */
  .wfiCheckNeeded         = false,    /* wfi check needed */
  .secCoreName            = "None",   /* Secondary core */
  .pLBISTRegs             = (CSL_lbistRegs *)(CSL_C7X_LBIST_BASE),
  .pLBISTSig              = (uint32_t *)(CSL_C7X_LBIST_SIG),
  .expectedMISR           = C7X_MISR_EXP_VAL,          /* Expected signature for C7x*/
  .cpuStatusFlagMask      = TISCI_MSG_VAL_PROC_BOOT_STATUS_FLAG_R5_WFI, /* Expected boot status value for wfi */
  .handler                = LBIST_eventHandler,        /* LBIST event handler */
  .interruptNumber        = CSLR_MCU_R5FSS0_CORE0_INTR_COMPUTE_CLUSTER0_C7X_4_DFT_LBIST_DFT_LBIST_BIST_DONE_0,/* BIST DONE interrupt number */
  .tisciProcId            = SCICLIENT_PROC_ID_C71SS0,  /* C7x Proc Id */
  .tisciSecProcId         = 0,
  .tisciDeviceId          = TISCI_DEV_C71SS0,          /* C7x Device Id */
  .tisciSecDeviceId       = 0,
  .numAuxDevices          = 0u,                        /* No Aux devices */
  .doneFlag               = false,                     /* Initialize done flag */
  .LBISTConfig = {
      .dc_def        = LBIST_DC_DEF,
      .divide_ratio  = LBIST_DIVIDE_RATIO,
      .static_pc_def = LBIST_C7X_STATIC_PC_DEF,
      .set_pc_def    = LBIST_SET_PC_DEF,
      .reset_pc_def  = LBIST_RESET_PC_DEF,
      .scan_pc_def   = LBIST_SCAN_PC_DEF,
      .prpg_def      = LBIST_PRPG_DEF,
  },
 },
 /* VPAC */
 {
  .coreName               = "VPAC",
  .secondaryCoreNeeded    = false,           /* Secondary core needed */
  .wfiCheckNeeded         = false,           /* wfi check needed */
  .secCoreName            = "None",          /* Secondary core */
  .pLBISTRegs             = (CSL_lbistRegs *)(CSL_VPAC_LBIST_BASE),
  .pLBISTSig              = (uint32_t *)(CSL_VPAC_LBIST_SIG),
  .expectedMISR           = VPAC_MISR_EXP_VAL,                          /* Expected signature for VPAC */
  .cpuStatusFlagMask      = TISCI_MSG_VAL_PROC_BOOT_STATUS_FLAG_R5_WFI, /* Expected boot status value for wfi */
  .handler                = LBIST_eventHandler,                         /* LBIST event handler */
  .interruptNumber        = CSLR_MCU_R5FSS0_CORE0_INTR_GLUELOGIC_VPAC_LBIST_GLUE_DFT_LBIST_BIST_DONE_0,/* BIST DONE interrupt number */
  .tisciProcId            = 0,                /* No proc id */
  .tisciSecProcId         = 0,                /* No Proc Id */
  .tisciDeviceId          = TISCI_DEV_VPAC0,  /* VPAC Device Id */
  .tisciSecDeviceId       = 0,
  .numAuxDevices          = 0u,               /* No Aux devices */
  .doneFlag               = false,            /* Initialize done flag */
  .LBISTConfig = {
      .dc_def        = LBIST_DC_DEF,
      .divide_ratio  = LBIST_DIVIDE_RATIO,
      .static_pc_def = LBIST_VPAC_STATIC_PC_DEF,
      .set_pc_def    = LBIST_SET_PC_DEF,
      .reset_pc_def  = LBIST_RESET_PC_DEF,
      .scan_pc_def   = LBIST_SCAN_PC_DEF,
      .prpg_def      = LBIST_PRPG_DEF,
  },
 },
 /* DMPAC */
 {
  .coreName               = "DMPAC",
  .secondaryCoreNeeded    = false,            /* Secondary core needed */
  .wfiCheckNeeded         = false,            /* wfi check needed */
  .secCoreName            = "None",           /* Secondary core */
  .pLBISTRegs             = (CSL_lbistRegs *)(CSL_DMPAC_LBIST_BASE),
  .pLBISTSig              = (uint32_t *)(CSL_DMPAC_LBIST_SIG),
  .expectedMISR           = DMPAC_MISR_EXP_VAL,                         /* Expected signature for DMPAC */
  .cpuStatusFlagMask      = TISCI_MSG_VAL_PROC_BOOT_STATUS_FLAG_R5_WFI, /* Expected boot status value for wfi */
  .handler                = LBIST_eventHandler,                         /* LBIST event handler */
  .interruptNumber        = CSLR_MCU_R5FSS0_CORE0_INTR_GLUELOGIC_DMPAC_LBIST_GLUE_DFT_LBIST_BIST_DONE_0,/* BIST DONE interrupt number */
  .tisciProcId            = 0,                /* No proc id */
  .tisciSecProcId         = 0,                /* No Proc Id */
  .tisciDeviceId          = TISCI_DEV_DMPAC0, /* DMPAC Device Id */
  .tisciSecDeviceId       = 0,
  .numAuxDevices          = 0u,               /* No Aux devices */
  .doneFlag               = false,            /* Initialize done flag */
  .LBISTConfig = {
      .dc_def        = LBIST_DC_DEF,
      .divide_ratio  = LBIST_DIVIDE_RATIO,
      .static_pc_def = LBIST_DMPAC_STATIC_PC_DEF,
      .set_pc_def    = LBIST_SET_PC_DEF,
      .reset_pc_def  = LBIST_RESET_PC_DEF,
      .scan_pc_def   = LBIST_SCAN_PC_DEF,
      .prpg_def      = LBIST_PRPG_DEF,
  },
 },
 /* A72 */
 {
  .coreName               = "A72 core 0",
  .secondaryCoreNeeded    = true,           /* Secondary core needed */
  .wfiCheckNeeded         = false,          /* wfi check needed */
  .secCoreName            = "A72 core 1",   /* Secondary core */
  .pLBISTRegs                = (CSL_lbistRegs *)(CSL_A72_LBIST_BASE),
  .pLBISTSig              = (uint32_t *)(CSL_A72_LBIST_SIG),
  .expectedMISR           = A72_MISR_EXP_VAL,                           /* Expected signature for A72 */
  .cpuStatusFlagMask      = TISCI_MSG_VAL_PROC_BOOT_STATUS_FLAG_R5_WFI, /* Expected boot status value for wfi */
  .handler                = LBIST_eventHandler,                         /* LBIST event handler */
  .interruptNumber        = CSLR_MCU_R5FSS0_CORE0_INTR_COMPUTE_CLUSTER0_ARM0_DFT_LBIST_DFT_LBIST_BIST_DONE_0,/* BIST DONE interrupt number */
  .tisciProcId            = SCICLIENT_PROC_ID_A72SS0_CORE0, /* A72 core 0 Proc Id */
  .tisciSecProcId         = SCICLIENT_PROC_ID_A72SS0_CORE1, /* A72 core 1 Proc Id */
  .tisciDeviceId          = TISCI_DEV_A72SS0_CORE0,  /* A72 core 0 Device Id */
  .tisciSecDeviceId       = TISCI_DEV_A72SS0_CORE1,  /* A72 core 1 Device Id */
  .numAuxDevices          = A72_NUM_AUX_DEVICES,     /* Number of Aux devices */
  .auxDeviceIdsP          = &LBIST_A72AuxDevList[0], /* Array of Aux device ids */
  .doneFlag               = false,                   /* Initialize done flag */
  .LBISTConfig = {
      .dc_def        = LBIST_DC_DEF,
      .divide_ratio  = LBIST_DIVIDE_RATIO,
      .static_pc_def = LBIST_A72_STATIC_PC_DEF,
      .set_pc_def    = LBIST_SET_PC_DEF,
      .reset_pc_def  = LBIST_RESET_PC_DEF,
      .scan_pc_def   = LBIST_SCAN_PC_DEF,
      .prpg_def      = LBIST_PRPG_DEF,
  },
 },

};

/* ========================================================================== */
/*                            External Variables                              */
/* ========================================================================== */


/* ========================================================================== */
/*                          Function Definitions                              */
/* ========================================================================== */

void LBIST_eventHandler( uint32_t coreIndex )
{
    int32_t status;
    Bool isLBISTDone = CSL_FALSE;
    CSL_lbistRegs *pLBISTRegs = LBIST_TestHandleArray[coreIndex].pLBISTRegs;

    /* Double check if the LBIST done flag is set */
    status = CSL_LBIST_isDone(pLBISTRegs, &isLBISTDone);
    if ((status == CSL_PASS) && (isLBISTDone == CSL_TRUE))
    {
        LBIST_TestHandleArray[coreIndex].doneFlag = true;
        /* Need to pull run down to low to clear the done interrupt */
        CSL_LBIST_stop( pLBISTRegs );
    }
    return;

}

