/*
*
* Copyright (c) 2019 Texas Instruments Incorporated
*
* All rights reserved not granted herein.
*
* Limited License.
*
* Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
* license under copyrights and patents it now or hereafter owns or controls to make,
* have made, use, import, offer to sell and sell ("Utilize") this software subject to the
* terms herein.  With respect to the foregoing patent license, such license is granted
* solely to the extent that any such patent is necessary to Utilize the software alone.
* The patent license shall not apply to any combinations which include this software,
* other than combinations with devices manufactured by or for TI ("TI Devices").
* No hardware patent is licensed hereunder.
*
* Redistributions must preserve existing copyright notices and reproduce this license
* (including the above copyright notice and the disclaimer and (if applicable) source
* code license limitations below) in the documentation and/or other materials provided
* with the distribution
*
* Redistribution and use in binary form, without modification, are permitted provided
* that the following conditions are met:
*
* *       No reverse engineering, decompilation, or disassembly of this software is
* permitted with respect to any software provided in binary form.
*
* *       any redistribution and use are licensed by TI for use only with TI Devices.
*
* *       Nothing shall obligate TI to provide you with source code for the software
* licensed and provided to you in object code.
*
* If software source code is provided to you, modification and redistribution of the
* source code are permitted provided that the following conditions are met:
*
* *       any redistribution and use of the source code, including any resulting derivative
* works, are licensed by TI for use only with TI Devices.
*
* *       any redistribution and use of any object code compiled from the source code
* and any resulting derivative works, are licensed by TI for use only with TI Devices.
*
* Neither the name of Texas Instruments Incorporated nor the names of its suppliers
*
* may be used to endorse or promote products derived from this software without
* specific prior written permission.
*
* DISCLAIMER.
*
* THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
* OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
* OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
* IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
* BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
* OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
* OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
* OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/**
 *  \file     ipc_spi_master_app.c
 *
 *  \brief    This file contains the SPI test example for which
 *            demonstrates McSPI mastermode of operation.
 *
 */

/* ========================================================================== */
/*                             Include Files                                  */
/* ========================================================================== */

#include "string.h"
#include "stdio.h"
#include "stdarg.h"
#include "stdlib.h"
#include "Std_Types.h"
#include "Det.h"
#include "Dem.h"
#include "Os.h"
#include "Spi_Cfg.h"
#include "Spi.h"
#include "Spi_Dbg.h"
#include "SchM_Spi.h"
#include "EcuM_Cbk.h"

#include <ti/csl/arch/csl_arch.h>
#include <ti/csl/soc.h>
#include <ti/csl/cslr.h>
#include <ti/csl/cslr_mcspi.h>
#include <ti/csl/src/ip/mcspi/V0/mcspi.h>
#include <ti/csl/src/ip/mcspi/V0/hw_mcspi.h>

#include "app_utils.h"

#include "ipc_spi_master_app.h"
#include <ti/osal/osal.h>
/* ========================================================================== */
/*                           Macros & Typedefs                                */
/* ========================================================================== */

#if defined (BUILD_MCU)
#define PRAGMA(x) _Pragma(#x)
#ifdef __cplusplus
#define DATAPRAGMA(f,s) PRAGMA(DATA_SECTION(s))
#else
#define DATAPRAGMA(f,s) PRAGMA(DATA_SECTION(f, s))
#endif
#endif /* (BUILD_MCU) */

/* ========================================================================== */
/*                         Structures and Enums                               */
/* ========================================================================== */

/* ========================================================================== */
/*                 Internal Function Declarations                             */
/* ========================================================================== */
static uint32 IpcSpiMasterApp_SetUpBuffer(SpiApp_TestParams *testParams,
    const Spi_ConfigType *cfgPtr);
static uint32 IpcSpiMasterApp_InitPhase(void);
static uint32 IpcSpiMasterApp_AsyncTransmit(const Spi_ConfigType *cfgPtr);
static bool IpcSpiMasterApp_DataCheck(SpiApp_TestParams *testParams,
    const Spi_ConfigType *cfgPtr);
static uint64_t IpcSpiMasterApp_GetTimeSpent(uint64_t preTs, uint64_t postTs);
static void IpcSpiMasterApp_PrintProfiledValues(void);
static uint32 IpcSpiMasterApp_GetTransmitStatus(const Spi_ConfigType *cfgPtr);
/* ========================================================================== */
/*                            Global Variables                                */
/* ========================================================================== */
uint32                          IpcSpiMasterApp_TestPassed = E_OK;
/**< App Status Flag */
static volatile uint32          IpcSpiMasterApp_NtfyCnt = 0U;
/**< Seq Notification Count */
DATAPRAGMA(IpcSpiMasterApp_McspiTxBuffer, ".bss")
uint32                          IpcSpiMasterApp_McspiTxBuffer[SPI_MAX_CHANNELS]
                                                [RX_TX_MAX_BUF_SIZE_WORD];
/**< Master Transmit Buffer */
DATAPRAGMA(IpcSpiMasterApp_McspiRxBuffer, ".bss")
uint32                          IpcSpiMasterApp_McspiRxBuffer[SPI_MAX_CHANNELS]
                                                [RX_TX_MAX_BUF_SIZE_WORD];
/**< Master Receive Buffer */
volatile uint32                 IpcSpiMasterApp_McspiJobDone[SPI_MAX_JOBS],
                                IpcSpiMasterApp_McspiSeqDone[SPI_MAX_SEQ];
/**< Job,Seq End Notification flags */
extern SemaphoreP_Handle        IpcSpiMasterApp_TxConfirmationSemaphore;
/**< TX Confirmation semaphore */
extern SemaphoreP_Handle        IpcSpiMasterApp_RxConfirmationSemaphore;
/**< Rx Confirmation semaphore */
volatile uint64_t IpcSpiMasterAppProfileCumulativeTx = 0U;
/**< Global used to track cumulative time spent for transmission */
/* ========================================================================== */
/*                          Function Definitions                              */
/* ========================================================================== */
int IpcSpiMasterApp_ProfileTest(SpiApp_TestParams *testParams)
{
    uint32              testPassed = E_OK, itr, msgLpCnt;
    Std_ReturnType      retVal;
    Spi_StatusType      status;
    bool                matching;
    IpcSpiMasterApp_TestPassed = E_OK;
    const Spi_ConfigType *cfgPtr = &SpiDriver;
    volatile uint64_t   preTimeStamp, postTimeStamp;

#if (SPI_VERSION_INFO_API == STD_ON)
    Std_VersionInfoType versioninfo;
#endif  /* #if (SPI_VERSION_INFO_API == STD_ON) */

    AppUtils_Printf(APP_UTILS_PRINT_MSG_NORMAL,
                    MSG_APP_NAME ": Sample Application - STARTS !!!\n");

#if (SPI_VERSION_INFO_API == STD_ON)
    /* Get and print version */
    Spi_GetVersionInfo(&versioninfo);
    AppUtils_Printf(MSG_NORMAL, MSG_APP_NAME" \n");
    AppUtils_Printf(MSG_NORMAL, MSG_APP_NAME"SPI MCAL Version Info\n");
    AppUtils_Printf(MSG_NORMAL, MSG_APP_NAME"---------------------\n");
    AppUtils_Printf(MSG_NORMAL, MSG_APP_NAME"Vendor ID           : %d\n",
        versioninfo.vendorID);
    AppUtils_Printf(MSG_NORMAL, MSG_APP_NAME"Module ID           : %d\n",
        versioninfo.moduleID);
    AppUtils_Printf(MSG_NORMAL, MSG_APP_NAME"SW Major Version    : %d\n",
        versioninfo.sw_major_version);
    AppUtils_Printf(MSG_NORMAL, MSG_APP_NAME"SW Minor Version    : %d\n",
        versioninfo.sw_minor_version);
    AppUtils_Printf(MSG_NORMAL, MSG_APP_NAME"SW Patch Version    : %d\n",
        versioninfo.sw_patch_version);
    AppUtils_Printf(MSG_NORMAL, MSG_APP_NAME" \n");
#endif  /* #if (SPI_VERSION_INFO_API == STD_ON) */

/* Print test case information */
    AppUtils_Printf(MSG_NORMAL, MSG_APP_NAME
            "CH %d: JOBS %d: SEQ %d: HWUNIT %d:!!!\n",
        cfgPtr->maxChannels,
        cfgPtr->maxJobs,
        cfgPtr->maxSeq,
        cfgPtr->maxHwUnit);

    /* Initialize SPI Driver */
    testPassed = IpcSpiMasterApp_InitPhase();

    for (itr = 0U; itr < APP_NUM_ITERATION; itr++)
    {
        IpcSpiMasterAppProfileCumulativeTx = 0U;
        Utils_prfLoadCalcReset();
        Utils_prfLoadCalcStart();
        for (msgLpCnt = 0U; msgLpCnt < APP_NUM_MSG_PER_ITERATION; msgLpCnt++)
        {
            /* SetUp Tx and Rx Buffers */
            if (testPassed == E_OK)
            {
                testPassed = IpcSpiMasterApp_SetUpBuffer(testParams, cfgPtr);
            }

            /* Transmit data */
            if (testPassed == E_OK)
            {
                preTimeStamp = TimerP_getTimeInUsecs();
                testPassed = IpcSpiMasterApp_AsyncTransmit(cfgPtr);
                postTimeStamp = TimerP_getTimeInUsecs();
                IpcSpiMasterAppProfileCumulativeTx +=
                        IpcSpiMasterApp_GetTimeSpent(preTimeStamp, postTimeStamp);
#if ((SPI_SCALEABILITY == SPI_LEVEL_1) || \
(SPI_SCALEABILITY == SPI_LEVEL_2))
                if (SPI_INTERRUPT_MODE == SPI_APP_DEFAULT_ASYNC_MODE)
                {
                    /* Wait for TX/RX to get over */
                    SemaphoreP_pend(IpcSpiMasterApp_TxConfirmationSemaphore,
                                     SemaphoreP_WAIT_FOREVER);
                }
#endif
            }

            if (testPassed == E_OK)
            {
                testPassed = IpcSpiMasterApp_GetTransmitStatus(cfgPtr);
            }

            if (testPassed == E_OK)
            {
                /* Wait for timeout value to initiate next transfer.
                 * Allow some time for slave to compose the response */
                Osal_delay(testParams->timeOutValue);
            }

            /* Init transfer again to receive data from slave side */
            /* SetUp Tx and Rx Buffers */
            if (testPassed == E_OK)
            {
                testPassed = IpcSpiMasterApp_SetUpBuffer(testParams, cfgPtr);
            }

            /* Transmit data */
            if (testPassed == E_OK)
            {
                testPassed = IpcSpiMasterApp_AsyncTransmit(cfgPtr);
#if ((SPI_SCALEABILITY == SPI_LEVEL_1) || \
(SPI_SCALEABILITY == SPI_LEVEL_2))
                if (testPassed == E_OK)
                {
                    /* Wait for timeout value to receive data from slave.
                     * Allow some time for slave to send the response */
                    Osal_delay(testParams->timeOutValue);
                }
                if (SPI_INTERRUPT_MODE == SPI_APP_DEFAULT_ASYNC_MODE)
                {
                    /* Wait for TX/RX to get over */
                    SemaphoreP_pend(IpcSpiMasterApp_RxConfirmationSemaphore,
                                     SemaphoreP_WAIT_FOREVER);
                }
#endif
            }

            if (testPassed == E_OK)
            {
                /* Check RX buffer against TX buffer */
                matching = IpcSpiMasterApp_DataCheck(testParams, cfgPtr);
                if (TRUE != matching)
                {
                    testPassed = E_NOT_OK;
                    AppUtils_Printf(APP_UTILS_PRINT_MSG_NORMAL,
                        "MCSPI Data Mismatch Error!! Loop Index %u\n", msgLpCnt);
                    break;
                }
            }
        }
        Utils_prfLoadCalcStop ();
        
        if (testPassed == E_NOT_OK)
        {
            /* Break from outer iteration loop in case of data mismatch */
            break;
        }
    }

    if (testPassed == E_OK)
    {
        /* DeInit */
        retVal = Spi_DeInit();
        if (retVal != E_OK)
        {
            testPassed = E_NOT_OK;
            AppUtils_Printf(APP_UTILS_PRINT_MSG_NORMAL,"SPI Deinit Failed!!\n");
        }

        /* SPI  driver should be uninit now - check */
        status = Spi_GetStatus();
        if (status != SPI_UNINIT)
        {
            testPassed = E_NOT_OK;
            AppUtils_Printf(APP_UTILS_PRINT_MSG_NORMAL,
                "SPI driver is not in uninit state!!\n");
        }
    }

    if (E_OK == testPassed)
    {
        IpcSpiMasterApp_PrintProfiledValues();
		AppUtils_Printf(MSG_NORMAL, MSG_APP_NAME "SPI Test Passed!!!\n");
        AppUtils_LogResult(APP_UTILS_STATUS_PASS);
    }
    else
    {
        AppUtils_Printf(MSG_NORMAL, MSG_APP_NAME "SPI Test Failed!!!\n");
        AppUtils_LogResult(APP_UTILS_STATUS_FAIL);
    }

    return (testPassed);
}

void SpiApp_McuMcspiJob0EndNotification(void)
{
    IpcSpiMasterApp_McspiJobDone[0U] = TRUE;

    return;
}

void SpiApp_McuMcspiSeq0EndNotification(void)
{
    if (IpcSpiMasterApp_NtfyCnt == 0U)
    {
        SemaphoreP_postFromISR(IpcSpiMasterApp_TxConfirmationSemaphore);
        IpcSpiMasterApp_NtfyCnt++;
    }
    else
    {
        SemaphoreP_postFromISR(IpcSpiMasterApp_RxConfirmationSemaphore);
        IpcSpiMasterApp_NtfyCnt = 0U;
    }
    IpcSpiMasterApp_McspiSeqDone[0U] = TRUE;

    return;
}

static bool IpcSpiMasterApp_DataCheck(SpiApp_TestParams *testParams,
    const Spi_ConfigType *cfgPtr)
{
    bool matching = TRUE;
    uint32              index, chIndex;
    uint8              *tempTxPtr, *tempRxPtr;
    uint32              byteLength[SPI_MAX_CHANNELS] = {0U};
    uint32              xferLength[SPI_MAX_CHANNELS] = {0U};

    /* Init test params */
    for (chIndex = 0U; chIndex < SPI_MAX_CHANNELS; chIndex++)
    {
        xferLength[chIndex] = testParams->dataLength;
    }

    for (chIndex = 0U; chIndex < cfgPtr->maxChannels; chIndex++)
    {
        /* Memset RX buffer for every run */
        if (cfgPtr->channelCfg[chIndex].dataWidth <= 8U)
        {
            byteLength[chIndex] = xferLength[chIndex];
        }
        else if (cfgPtr->channelCfg[chIndex].dataWidth <= 16U)
        {
            byteLength[chIndex] = xferLength[chIndex] * 2U;
        }
        else
        {
            byteLength[chIndex] = xferLength[chIndex] * 4U;
        }
    }

    for (chIndex = 0U;
         chIndex < cfgPtr->maxChannels;
         chIndex++)
    {
        tempTxPtr = (uint8 *) testParams->txPtr;
        tempRxPtr = (uint8 *) testParams->rxPtr;
        for (index = 0U; index < byteLength[chIndex]; index++)
        {
            if ((*tempRxPtr == 0x00U) || (*tempRxPtr == 0xFFU))
            {
                matching = FALSE;
                break;
            }
            tempRxPtr++;
        }

        if (matching == FALSE)
        {
            tempTxPtr = (uint8 *) testParams->txPtr;
            tempRxPtr = (uint8 *) testParams->rxPtr;
            for (index = 0U; index < byteLength[chIndex]; index++)
            {
                if (*tempTxPtr++ != *tempRxPtr++)
                {
                    matching = FALSE;
                    break;
                }
            }
        }
    }

    return matching;
}

static uint32 IpcSpiMasterApp_InitPhase(void)
{
    Std_ReturnType      retVal;
    Spi_StatusType      status;
    uint32 index, testPassed = E_OK;
    const Spi_ConfigType *cfgPtr = &SpiDriver;

     /* Spi Init */
#if (STD_ON == SPI_PRE_COMPILE_VARIANT)
    AppUtils_Printf(MSG_NORMAL, MSG_APP_NAME
        "Variant - Pre Compile being used !!!\n");
    Spi_Init((const Spi_ConfigType *) NULL_PTR);
#else
    AppUtils_Printf(MSG_NORMAL, MSG_APP_NAME
        "Variant - Post Build being used !!!\n");
    Spi_Init(&SpiDriver);
#endif

    /* SPI driver should be free now - check */
    status = Spi_GetStatus();
    if (status != SPI_IDLE)
    {
        testPassed = E_NOT_OK;
        AppUtils_Printf(APP_UTILS_PRINT_MSG_NORMAL,"SPI driver is not IDLE!!\n");
    }

#if (SPI_HW_STATUS_API == STD_ON)
    /* SPI HW unit should be free now - check */
    for (index = 0U; index < cfgPtr->maxHwUnit; index++)
    {
        status =
            Spi_GetHWUnitStatus(cfgPtr->hwUnitCfg[index].hwUnitId);
        if (status != SPI_IDLE)
        {
            testPassed = E_NOT_OK;
            AppUtils_Printf(APP_UTILS_PRINT_MSG_NORMAL,"SPI HW %d is not IDLE!!\n",
                            cfgPtr->hwUnitCfg[index].hwUnitId);
        }
    }
#endif  /* #if (SPI_HW_STATUS_API == STD_ON) */

#if (SPI_SCALEABILITY == SPI_LEVEL_2)
    /* Set Async mode */
    /** \brief Default ASYNC/SYNC Transmit flag - TRUE: Async, FALSE: Sync */
    retVal = Spi_SetAsyncMode(SPI_APP_DEFAULT_ASYNC_MODE);
    if (retVal != E_OK)
    {
        testPassed = E_NOT_OK;
        AppUtils_Printf(APP_UTILS_PRINT_MSG_NORMAL,"SPI Set Async Mode Failed!!\n");
    }
#endif  /* #if (SPI_SCALEABILITY == SPI_LEVEL_2) */

    return testPassed;
}

static uint32 IpcSpiMasterApp_SetUpBuffer(SpiApp_TestParams *testParams,
    const Spi_ConfigType *cfgPtr)
{
    uint32              index, chIndex;
    uint32              testPassed = E_OK;
    uint8              *tempTxPtr, *tempRxPtr;
    uint32              byteLength[SPI_MAX_CHANNELS] = {0U};
    uint32              xferLength[SPI_MAX_CHANNELS] = {0U};
    Std_ReturnType      retVal;
    Spi_ChannelType     chId;

    /* Init test params */
    for (chIndex = 0U; chIndex < SPI_MAX_CHANNELS; chIndex++)
    {
        /* Init TX buffer with known data and memset RX buffer */
        tempTxPtr = (uint8 *) testParams->txPtr;
        for (index = 0U; index < (testParams->dataLength * 4U); index++)
        {
            *tempTxPtr++ = index + 1U;
        }

        xferLength[chIndex] = testParams->dataLength;
    }

    for (chIndex = 0U; chIndex < cfgPtr->maxChannels; chIndex++)
    {
        /* Memset RX buffer for every run */
        if (cfgPtr->channelCfg[chIndex].dataWidth <= 8U)
        {
            byteLength[chIndex] = xferLength[chIndex];
        }
        else if (cfgPtr->channelCfg[chIndex].dataWidth <= 16U)
        {
            byteLength[chIndex] = xferLength[chIndex] * 2U;
        }
        else
        {
            byteLength[chIndex] = xferLength[chIndex] * 4U;
        }

        /* Memset RX buffer for every run */
        tempRxPtr = (uint8 *) testParams->rxPtr;
        memset(tempRxPtr, 0, byteLength[chIndex]);

        /* SetUp Buffers */
#if ((SPI_CHANNELBUFFERS == SPI_IB_EB) || (SPI_CHANNELBUFFERS == SPI_EB))
        if (SPI_EB == cfgPtr->channelCfg[chIndex].channelBufType)
        {
            Spi_DataBufferType *srcDataBuf, *destDataBuf;

            /* Setup EB for TX/RX */
            srcDataBuf  = (Spi_DataBufferType *) testParams->txPtr;
            destDataBuf = (Spi_DataBufferType *) testParams->rxPtr;
            chId        = Spi_ChannelConfig_PC[chIndex].channelId;
            retVal      = Spi_SetupEB(
                chId,
                srcDataBuf,
                destDataBuf,
                xferLength[chIndex]);
            if (retVal != E_OK)
            {
                testPassed = E_NOT_OK;
                AppUtils_Printf(APP_UTILS_PRINT_MSG_NORMAL,"SPI Setup EB Failed!!\n");
            }
        }
#endif
    }

    /* Reset flags */
    memset((void *)&IpcSpiMasterApp_McspiJobDone[0U], FALSE, SPI_MAX_JOBS);
    memset((void *)&IpcSpiMasterApp_McspiSeqDone[0U], FALSE, SPI_MAX_SEQ);

    return testPassed;
}

static uint32 IpcSpiMasterApp_AsyncTransmit(const Spi_ConfigType *cfgPtr)
{
    Std_ReturnType      retVal;
    Spi_SequenceType    seqId;
    uint32 index, testPassed = E_OK;

#if ((SPI_SCALEABILITY == SPI_LEVEL_1) || \
(SPI_SCALEABILITY == SPI_LEVEL_2))
    for (index = 0U; index < cfgPtr->maxSeq; index++)
    {
        /* Start the TX/RX */
        seqId  = Spi_SeqConfig_PC[index].seqId;

        retVal = Spi_AsyncTransmit(seqId);
        if (retVal != E_OK)
        {
            testPassed = E_NOT_OK;
            AppUtils_Printf(APP_UTILS_PRINT_MSG_NORMAL,"SPI Async transmit Failed!!\n");
        }
    }
#endif

    return testPassed;
}

static uint32 IpcSpiMasterApp_GetTransmitStatus(const Spi_ConfigType *cfgPtr)
{
    Spi_SeqResultType   seqResult;
    Spi_JobResultType   jobResult;
    Spi_SequenceType    seqId;
    Spi_JobType         jobId;
    Spi_StatusType      status;
    uint32 index, testPassed = E_OK;

    /* SPI driver should be free now - check */
    status = Spi_GetStatus();
    if (status != SPI_IDLE)
    {
        testPassed = E_NOT_OK;
        AppUtils_Printf(APP_UTILS_PRINT_MSG_NORMAL,"SPI driver is not IDLE!!\n");
    }
    for (index = 0U; index < cfgPtr->maxSeq; index++)
    {
        seqId     = Spi_SeqConfig_PC[index].seqId;
        seqResult = Spi_GetSequenceResult(seqId);
        if (seqResult != SPI_SEQ_OK)
        {
            if (seqResult == SPI_SEQ_CANCELLED)
            {
                AppUtils_Printf(APP_UTILS_PRINT_MSG_NORMAL,"SPI Sequence was cancelled!!\n");
            }
            else
            {
                testPassed = E_NOT_OK;
                AppUtils_Printf(APP_UTILS_PRINT_MSG_NORMAL,"SPI Sequence is not OK!!\n");
            }
        }
    }

    for (index = 0U; index < cfgPtr->maxJobs; index++)
    {
        jobId     = Spi_JobConfig_PC[index].jobId;
        jobResult = Spi_GetJobResult(jobId);
        if (jobResult != SPI_JOB_OK)
        {
            testPassed = E_NOT_OK;
            AppUtils_Printf(APP_UTILS_PRINT_MSG_NORMAL,"SPI Job is not OK!!\n");
        }
    }

    return testPassed;
}

static uint64_t IpcSpiMasterApp_GetTimeSpent(uint64_t preTs, uint64_t postTs)
{
    uint64_t rtnTs;

    if (postTs >= preTs)
    {
        rtnTs = postTs - preTs;
    }
    else
    {
        rtnTs = postTs + (0xFFFFFFFFFFFFFFFFU - preTs);
    }

    return (rtnTs);
}

static void IpcSpiMasterApp_PrintProfiledValues(void)
{
    Utils_PrfLoad       computedLoad;

    Utils_prfLoadGetTaskLoad(TaskP_self(), &computedLoad);

    AppUtils_Printf(MSG_NORMAL, MSG_APP_NAME
        "Transmitted Message of size %d bytes, %d times in %llu usecs\n",
        SPI_APP_RX_TX_DATA_SIZE,
        APP_NUM_MSG_PER_ITERATION, IpcSpiMasterAppProfileCumulativeTx);

    AppUtils_Printf(MSG_NORMAL, MSG_APP_NAME
            "Average of %6.2f usecs per transmit \n",
            (float)IpcSpiMasterAppProfileCumulativeTx /
                (float)APP_NUM_MSG_PER_ITERATION);

    AppUtils_Printf(MSG_NORMAL, MSG_APP_NAME
                    "Measured Load: Total CPU: "
                    "%f%%, HWI: %f%%, SWI:%f%% TSK: %f%%\r\n",
                    computedLoad.cpuLoad,
                    computedLoad.hwiLoad,
                    computedLoad.swiLoad,
                    computedLoad.tskLoad);
}

#define SPI_START_SEC_ISR_CODE
#include "Spi_MemMap.h"

SPI_ISR_TEXT_SECTION FUNC(void, SPI_CODE_FAST) IpcSpiMasterApp_SpiXIsr (
                                                            uintptr_t SpiPtr)
{
    volatile uint64_t   preTimeStamp, postTimeStamp;

    preTimeStamp = TimerP_getTimeInUsecs();

    IpcSpiMasterApp_IsrType spiChIsr = (IpcSpiMasterApp_IsrType)SpiPtr;

    /* Associated SPI ISR */
    spiChIsr();

    postTimeStamp = TimerP_getTimeInUsecs();

    IpcSpiMasterAppProfileCumulativeTx +=
        IpcSpiMasterApp_GetTimeSpent(preTimeStamp, postTimeStamp);
}

#define SPI_STOP_SEC_ISR_CODE
#include "Spi_MemMap.h"


