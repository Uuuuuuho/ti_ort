/*
*
* Copyright (c) 2019 Texas Instruments Incorporated
*
* All rights reserved not granted herein.
*
* Limited License.
*
* Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
* license under copyrights and patents it now or hereafter owns or controls to make,
* have made, use, import, offer to sell and sell ("Utilize") this software subject to the
* terms herein.  With respect to the foregoing patent license, such license is granted
* solely to the extent that any such patent is necessary to Utilize the software alone.
* The patent license shall not apply to any combinations which include this software,
* other than combinations with devices manufactured by or for TI ("TI Devices").
* No hardware patent is licensed hereunder.
*
* Redistributions must preserve existing copyright notices and reproduce this license
* (including the above copyright notice and the disclaimer and (if applicable) source
* code license limitations below) in the documentation and/or other materials provided
* with the distribution
*
* Redistribution and use in binary form, without modification, are permitted provided
* that the following conditions are met:
*
* *       No reverse engineering, decompilation, or disassembly of this software is
* permitted with respect to any software provided in binary form.
*
* *       any redistribution and use are licensed by TI for use only with TI Devices.
*
* *       Nothing shall obligate TI to provide you with source code for the software
* licensed and provided to you in object code.
*
* If software source code is provided to you, modification and redistribution of the
* source code are permitted provided that the following conditions are met:
*
* *       any redistribution and use of the source code, including any resulting derivative
* works, are licensed by TI for use only with TI Devices.
*
* *       any redistribution and use of any object code compiled from the source code
* and any resulting derivative works, are licensed by TI for use only with TI Devices.
*
* Neither the name of Texas Instruments Incorporated nor the names of its suppliers
*
* may be used to endorse or promote products derived from this software without
* specific prior written permission.
*
* DISCLAIMER.
*
* THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
* OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
* OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
* IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
* BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
* OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
* OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
* OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/**
 *  \file     ipc_spi_master_app.h
 *
 */

#ifndef IPC_SPI_MASTER_APP_H_
#define IPC_SPI_MASTER_APP_H_

/* ========================================================================== */
/*                             Include Files                                  */
/* ========================================================================== */
#include "string.h"
#include "Std_Types.h"
#include "Spi_Cfg.h"
#include "Spi.h"
#include "ipc_spi_master_app.h"

#include "app_utils.h" /* MCAL Example utilities */
#include "utils_prf.h" /* Demo utilities */

#ifdef __cplusplus
extern "C"
{
#endif

/* ========================================================================== */
/*                           Macros & Typedefs                                */
/* ========================================================================== */
#define APP_NUM_MSG_PER_ITERATION           (1000U)
/**< Number of IPC messages that would be sent per iteration */

#define APP_NUM_ITERATION                   (1U)
/**< Number of different message sizes that would be sent */

/** \brief RX/TX buffer size for allocation in words */
#define RX_TX_MAX_BUF_SIZE_WORD             (10U)

/** \brief Default RX/TX transfer length in words */
#define SPI_APP_RX_TX_LENGTH                (1U)

/** \brief Default RX/TX transfer data size in bytes */
#define SPI_APP_RX_TX_DATA_SIZE             (4U)

/** \brief Default timeout value(ms) wait b/w 2 transfers to ensure slave is
  *  ready */
#define SPI_APP_TIMEOUT_VALUE               (100U)

/** \brief Default ASYNC/SYNC Transmit flag - TRUE: Async, FALSE: Sync */
#define SPI_APP_DEFAULT_ASYNC_SYNC          (TRUE)

/** \brief Default ASYNC mode - interrupt or polled */
#define SPI_APP_DEFAULT_ASYNC_MODE          (SPI_INTERRUPT_MODE)

/** \brief Interrupt IDs for SoC
 *
 */
#if defined (SOC_J721E) || defined (SOC_J7200)
#define APP_SPI_MCU_2_INT      (CSLR_MCU_R5FSS0_CORE0_INTR_MCU_MCSPI2_INTR_SPI_0)
#endif

#define MSG_NORMAL              (APP_UTILS_PRINT_MSG_NORMAL)
/**< Message type */
#define MSG_STATUS              (APP_UTILS_PRINT_MSG_STATUS)
/**< Message type */
#define MSG_APP_NAME            "IPC_SPI Master Demo App:"
/**< App Name */
/* ========================================================================== */
/*                         Structures and Enums                               */
/* ========================================================================== */

typedef void (*IpcSpiMasterApp_IsrType)(void);

/**
 *  \brief IPC SPI application test parameters.
 */
typedef struct
{
    uint32              dataLength;
    /**< Data transfer length. */
    uint8*              txPtr;
    /**< Transmit buffer pointer. */
    uint8*              rxPtr;
    /**< Receive buffer pointer. */
    uint32              timeOutValue;
    /**< timeout value to wait b/w 2 transfers to ensure slave is ready for
      *  next transmission/reception    */
} SpiApp_TestParams;

/* ========================================================================== */
/*                          Extern Var Declarations                           */
/* ========================================================================== */
extern uint32   IpcSpiMasterApp_McspiTxBuffer[SPI_MAX_CHANNELS][RX_TX_MAX_BUF_SIZE_WORD];
extern uint32   IpcSpiMasterApp_McspiRxBuffer[SPI_MAX_CHANNELS][RX_TX_MAX_BUF_SIZE_WORD];

/* ========================================================================== */
/*                          Function Declarations                             */
/* ========================================================================== */

void SpiApp_McuMcspiJob0EndNotification(void);
void SpiApp_McuMcspiSeq0EndNotification(void);
int IpcSpiMasterApp_ProfileTest(SpiApp_TestParams *testParams);
void IpcSpiMasterApp_SpiXIsr(uintptr_t SpiPtr);

#ifdef __cplusplus
}
#endif

#endif  /* #ifndef IPC_SPI_APP_H_ */
