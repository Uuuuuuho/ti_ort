/*
*
* Copyright (c) 2019 Texas Instruments Incorporated
*
* All rights reserved not granted herein.
*
* Limited License.
*
* Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
* license under copyrights and patents it now or hereafter owns or controls to make,
* have made, use, import, offer to sell and sell ("Utilize") this software subject to the
* terms herein.  With respect to the foregoing patent license, such license is granted
* solely to the extent that any such patent is necessary to Utilize the software alone.
* The patent license shall not apply to any combinations which include this software,
* other than combinations with devices manufactured by or for TI ("TI Devices").
* No hardware patent is licensed hereunder.
*
* Redistributions must preserve existing copyright notices and reproduce this license
* (including the above copyright notice and the disclaimer and (if applicable) source
* code license limitations below) in the documentation and/or other materials provided
* with the distribution
*
* Redistribution and use in binary form, without modification, are permitted provided
* that the following conditions are met:
*
* *       No reverse engineering, decompilation, or disassembly of this software is
* permitted with respect to any software provided in binary form.
*
* *       any redistribution and use are licensed by TI for use only with TI Devices.
*
* *       Nothing shall obligate TI to provide you with source code for the software
* licensed and provided to you in object code.
*
* If software source code is provided to you, modification and redistribution of the
* source code are permitted provided that the following conditions are met:
*
* *       any redistribution and use of the source code, including any resulting derivative
* works, are licensed by TI for use only with TI Devices.
*
* *       any redistribution and use of any object code compiled from the source code
* and any resulting derivative works, are licensed by TI for use only with TI Devices.
*
* Neither the name of Texas Instruments Incorporated nor the names of its suppliers
*
* may be used to endorse or promote products derived from this software without
* specific prior written permission.
*
* DISCLAIMER.
*
* THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
* OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
* OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
* IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
* BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
* OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
* OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
* OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/**
 *  \file main_tirtos.c
 *
 *  \brief Main file for TI-RTOS build Master Application
 */

/* ========================================================================== */
/*                             Include Files                                  */
/* ========================================================================== */
/* XDCtools Header files */
#include <xdc/std.h>
#include <xdc/runtime/Error.h>
#include <xdc/runtime/System.h>

/* BIOS Header files */
#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Task.h>

#include <ti/csl/arch/csl_arch.h>
#include <ti/csl/soc.h>
#include <ti/csl/cslr.h>

#include <ti/osal/osal.h>
#include <ti/osal/TaskP.h>

#if (defined (BUILD_MCU1_0) && (defined (SOC_J721E) || defined (SOC_J7200)))
#include <ti/drv/sciclient/sciserver_tirtos.h>
#endif

#include "ipc_spi_master_app.h"


/* ========================================================================== */
/*                           Macros & Typedefs                                */
/* ========================================================================== */

/* Test application stack size */
#define APP_TASK_STACK                  (10U * 1024U)
/**< Stack required for the stack */
#define IPC_SPI_DEMO_TASK_NAME      ("IPC SPI MASTER DEMO APPLICATION")
/**< Task name */

/* ========================================================================== */
/*                         Structure Declarations                             */
/* ========================================================================== */

/* None */

/* ========================================================================== */
/*                          Function Declarations                             */
/* ========================================================================== */

static void IpcSpiMasterApp_TaskFxn(UArg a0, UArg a1);
static void IpcSpiMasterApp_Startup(void);
static void IpcSpiMasterApp_BuildIntList(void);
static void IpcSpiMasterApp_InterruptConfig(void);
static void IpcSpiMasterApp_PowerAndClkSrc(void);
static void IpcSpiMasterApp_PlatformInit();
static void IpcSpiMasterApp_Shutdown(void);
static void IpcSpiMasterApp_Semaphore_Init(void);
sint32 SetupSciServer(void);
/* ========================================================================== */
/*                            Global Variables                                */
/* ========================================================================== */
/* application stack */
static uint8 IpcSpiMasterApp_TaskStack[APP_TASK_STACK] __attribute__((aligned(32)));
/**< Stack for the task */
static uint32           IpcSpiMasterApp_IntNumbers;
/**< Stored the interrupt numbers for enabled all timers */
static IpcSpiMasterApp_IsrType   IpcSpiMasterApp_Isr;
/**< Associated ISR */
static SpiApp_TestParams  IpcSpiMasterApp_TestPrms;
/**< Test parameters */
static HwiP_Handle      IpcSpiMasterApp_IsrHndls;
/**< Stores the ISR handles */
SemaphoreP_Handle IpcSpiMasterApp_TxConfirmationSemaphore;
/**< TX Confirmation semaphore, would be posted when TX completes */
SemaphoreP_Handle IpcSpiMasterApp_RxConfirmationSemaphore;
/**< Rx Confirmation semaphore, would be posted when TX completes */
/* ========================================================================== */
/*                            External Variables                              */
/* ========================================================================== */


/* ========================================================================== */
/*                          Function Definitions                              */
/* ========================================================================== */

int main(void)
{
    Task_Handle task;
    Error_Block eb;
    Task_Params taskParams;
    sint32 ret = CSL_PASS;

#ifdef UART_ENABLED
    AppUtils_Init();
#endif

    ret = SetupSciServer();
    if(ret != CSL_PASS)
    {
        BIOS_exit(0);
    }
    IpcSpiMasterApp_Startup();
    IpcSpiMasterApp_PowerAndClkSrc();
    IpcSpiMasterApp_PlatformInit();

    IpcSpiMasterApp_Semaphore_Init();

    Error_init(&eb);

    /* Initialize the task params */
    Task_Params_init(&taskParams);
    taskParams.instance->name = IPC_SPI_DEMO_TASK_NAME;
    /* Set the task priority higher than the default priority (1) */
    taskParams.priority     = 2;
    taskParams.stack        = IpcSpiMasterApp_TaskStack;
    taskParams.stackSize    = sizeof (IpcSpiMasterApp_TaskStack);

    task = Task_create(IpcSpiMasterApp_TaskFxn, &taskParams, &eb);
    if(NULL == task)
    {
        BIOS_exit(0);
    }
    BIOS_start();    /* does not return */

    return(0);
}

static void IpcSpiMasterApp_Semaphore_Init(void)
{
    SemaphoreP_Params semParams;
    SemaphoreP_Params_init(&semParams);
    semParams.mode = SemaphoreP_Mode_BINARY;

    IpcSpiMasterApp_TxConfirmationSemaphore = SemaphoreP_create(0U, &semParams);
    if (NULL != IpcSpiMasterApp_TxConfirmationSemaphore)
    {
        SemaphoreP_Params_init(&semParams);
        semParams.mode = SemaphoreP_Mode_BINARY;

        IpcSpiMasterApp_RxConfirmationSemaphore = SemaphoreP_create(0U, &semParams);
        if (NULL == IpcSpiMasterApp_RxConfirmationSemaphore)
        {
            AppUtils_Printf(MSG_NORMAL, MSG_APP_NAME
                            " Error!!! Could not create"
                            " Semaphore !!!\n");
        }
    }
    else
    {
        AppUtils_Printf(MSG_NORMAL, MSG_APP_NAME
                        " Error!!! Could not create"
                        " Semaphore !!!\n");
    }

    return;
}

static void IpcSpiMasterApp_TaskFxn(UArg a0, UArg a1)
{
    Utils_prfInit();

    Utils_prfLoadRegister (TaskP_self(), IPC_SPI_DEMO_TASK_NAME);

    TaskP_yield();

    /* Initialize SPI application test params */
    IpcSpiMasterApp_TestPrms.txPtr = (uint8 *) &IpcSpiMasterApp_McspiTxBuffer[0U][0U];
    IpcSpiMasterApp_TestPrms.rxPtr = (uint8 *) &IpcSpiMasterApp_McspiRxBuffer[0U][0U];
    IpcSpiMasterApp_TestPrms.dataLength = SPI_APP_RX_TX_LENGTH;
    IpcSpiMasterApp_TestPrms.timeOutValue = SPI_APP_TIMEOUT_VALUE;
    IpcSpiMasterApp_ProfileTest(&IpcSpiMasterApp_TestPrms);

    IpcSpiMasterApp_Shutdown();

    Utils_prfLoadUnRegister (TaskP_self());
    Utils_prfDeInit();
    return;
}

/** \brief Application tear down functions */
static void IpcSpiMasterApp_Shutdown(void)
{
    if (NULL != IpcSpiMasterApp_IsrHndls)
    {
        if (HwiP_OK != HwiP_delete(IpcSpiMasterApp_IsrHndls))
        {
            AppUtils_Printf(MSG_NORMAL, MSG_APP_NAME
                            " Error!!! Could not De register"
                            " the ISR for instance !!!\n");
        }
    }

    if (NULL != IpcSpiMasterApp_TxConfirmationSemaphore)
    {
        SemaphoreP_delete(IpcSpiMasterApp_TxConfirmationSemaphore);
        if (NULL != IpcSpiMasterApp_RxConfirmationSemaphore)
        {
            SemaphoreP_delete(IpcSpiMasterApp_RxConfirmationSemaphore);
        }
    }

    return;
}

/** \brief Start up sequence : Program the interrupt muxes / priorities */
static void IpcSpiMasterApp_Startup(void)
{
    /* Equivalent to EcuM_AL_SetProgrammableInterrupts */
    IpcSpiMasterApp_BuildIntList();

    IpcSpiMasterApp_InterruptConfig();

    /* Initialize counters, that would be required for timed operations */
    AppUtils_ProfileInit(0);

#if defined (SOC_J721E) || defined (SOC_J7200)
	/* OSAL being generic might not record the correct frequency and hence we
     * are setting it explicitly. Osal_delay function uses PMU counter which
     * runs at CPU speed and uses OSAL_CPU_FREQ_KHZ_DEFAULT to get the CPU
     * frequency. By default OSAL_CPU_FREQ_KHZ_DEFAULT is set to 400MHZ.
	 * In case of J721E this parameter needs to be set explicitly to 1GHZ
     * to get the correct delay functionality.
      */
	AppUtils_SetCpuFrequency();
#endif
}

void IpcSpiMasterApp_BuildIntList(void)
{
    /* Select MCU MCSPI2 Instance */
    IpcSpiMasterApp_IntNumbers = APP_SPI_MCU_2_INT;
    IpcSpiMasterApp_Isr = Spi_IrqUnitMcuMcspi2TxRx;

    return;
}

sint32 SetupSciServer(void)
{
    sint32 ret = CSL_PASS;
#if (defined (BUILD_MCU1_0) && (defined (SOC_J721E) || defined (SOC_J7200)))
    Sciserver_TirtosCfgPrms_t appPrms;
    Sciclient_ConfigPrms_t clientPrms;

    appPrms.taskPriority[SCISERVER_TASK_USER_LO] = 1;
    appPrms.taskPriority[SCISERVER_TASK_USER_HI] = 4;

    /* Sciclient needs to be initialized before Sciserver. Sciserver depends on
     * Sciclient API to execute message forwarding */
    ret = Sciclient_configPrmsInit(&clientPrms);
    if (ret == CSL_PASS)
    {
        ret = Sciclient_init(&clientPrms);
    }

    if (ret == CSL_PASS)
    {
        ret = Sciserver_tirtosInit(&appPrms);
    }

    if (ret == CSL_PASS)
    {
        AppUtils_Printf(MSG_NORMAL, "Starting Sciserver..... PASSED\n");
    }
    else
    {
        AppUtils_Printf(MSG_NORMAL, "Starting Sciserver..... FAILED\n");
    }

#endif
    return ret;
}

static void IpcSpiMasterApp_InterruptConfig(void)
{
    HwiP_Params hwiParams;

    HwiP_Params_init(&hwiParams);
    hwiParams.arg = (uintptr_t)IpcSpiMasterApp_Isr;

    IpcSpiMasterApp_IsrHndls = HwiP_create(IpcSpiMasterApp_IntNumbers,
                                                IpcSpiMasterApp_SpiXIsr, &hwiParams);
    if (NULL == IpcSpiMasterApp_IsrHndls)
    {
        AppUtils_Printf(MSG_NORMAL, MSG_APP_NAME
                        " Error!!! Could not register the ISR for instance"
                        "%d!!!\n");
    }

    return;
}

/** \brief None, SBL/GEL powers up the timers and clock sources */
static void IpcSpiMasterApp_PowerAndClkSrc(void)
{
    /* Mcu module, when included will replace this operation */
    return;
}

static void IpcSpiMasterApp_PlatformInit(void)
{
    return;
}
