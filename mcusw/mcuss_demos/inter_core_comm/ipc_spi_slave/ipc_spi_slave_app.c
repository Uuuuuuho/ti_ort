/*
*
* Copyright (c) 2019 Texas Instruments Incorporated
*
* All rights reserved not granted herein.
*
* Limited License.
*
* Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
* license under copyrights and patents it now or hereafter owns or controls to make,
* have made, use, import, offer to sell and sell ("Utilize") this software subject to the
* terms herein.  With respect to the foregoing patent license, such license is granted
* solely to the extent that any such patent is necessary to Utilize the software alone.
* The patent license shall not apply to any combinations which include this software,
* other than combinations with devices manufactured by or for TI ("TI Devices").
* No hardware patent is licensed hereunder.
*
* Redistributions must preserve existing copyright notices and reproduce this license
* (including the above copyright notice and the disclaimer and (if applicable) source
* code license limitations below) in the documentation and/or other materials provided
* with the distribution
*
* Redistribution and use in binary form, without modification, are permitted provided
* that the following conditions are met:
*
* *       No reverse engineering, decompilation, or disassembly of this software is
* permitted with respect to any software provided in binary form.
*
* *       any redistribution and use are licensed by TI for use only with TI Devices.
*
* *       Nothing shall obligate TI to provide you with source code for the software
* licensed and provided to you in object code.
*
* If software source code is provided to you, modification and redistribution of the
* source code are permitted provided that the following conditions are met:
*
* *       any redistribution and use of the source code, including any resulting derivative
* works, are licensed by TI for use only with TI Devices.
*
* *       any redistribution and use of any object code compiled from the source code
* and any resulting derivative works, are licensed by TI for use only with TI Devices.
*
* Neither the name of Texas Instruments Incorporated nor the names of its suppliers
*
* may be used to endorse or promote products derived from this software without
* specific prior written permission.
*
* DISCLAIMER.
*
* THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
* OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
* OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
* IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
* BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
* OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
* OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
* OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/**
 *  \file     ipc_spi_slave_app.c
 *
 *  \brief    This file contains the SPI test slave example for McSPI
 *
 */

/* ========================================================================== */
/*                             Include Files                                  */
/* ========================================================================== */

#include <stdio.h>
#include <string.h>

/* XDCtools Header files */
#include <xdc/std.h>
#include <xdc/runtime/System.h>
#include <ti/sysbios/knl/Task.h>

/* BIOS Header files */
#include <ti/sysbios/BIOS.h>
#include <xdc/runtime/Error.h>
#if defined (__aarch64__)
#include <ti/sysbios/family/arm/v8a/Mmu.h>
#endif

/* TI-RTOS Header files */
#include <ti/drv/spi/soc/SPI_soc.h>
#include <ti/drv/spi/src/SPI_osal.h>
#include <ti/drv/spi/SPI.h>

#include "ipc_spi_slave_app.h"
/* ========================================================================== */
/*                           Macros & Typedefs                                */
/* ========================================================================== */
#define MCSPI_INSTANCE_ID   (4U)
/* Main Domain Instance MCSPI4 i.e connected as slave to MCU_MCSPI2 */
#define MCSPI_CHANNEL_NUM   (0U)
/* MCSPI Channel Num, Only 0 to be used */
/* ========================================================================== */
/*                         Structures and Enums                               */
/* ========================================================================== */

/* ========================================================================== */
/*                 Internal Function Declarations                             */
/* ========================================================================== */
static void SPI_InitConfig(uint32_t instance, uint32_t chn);
static void SPI_InitConfigDefault(SPI_HWAttrs *cfg, uint32_t chn);
static bool SPI_Test_mst_slv_xfer(void *spi, SpiSlaveApp_TestParams
    *IpcSpiSlaveApp_TestPrms);
Void AppUtils_Printf(const char *str, ...);
/* ========================================================================== */
/*                            Global Variables                                */
/* ========================================================================== */

unsigned char slaveTxBuffer[RX_TX_MAX_BUF_SIZE_WORD] __attribute__ ((aligned (32U)));
/**< Slave Transmit Buffer */
unsigned char slaveRxBuffer[RX_TX_MAX_BUF_SIZE_WORD] __attribute__ ((aligned (32U)));
/**< Slave Receive Buffer */
SPI_Transaction   transaction;
/**< Transaction data */
uint32_t terminateXfer = 1U;
/**< Terminate Transfer Value */
/* ========================================================================== */
/*                            Function Definitions                            */
/* ========================================================================== */
#if defined (__aarch64__)
#include <ti/sysbios/family/arm/v8a/Mmu.h>
volatile int32_t emuwait_mmu = 1;
void InitMmu(void)
{
    Bool            retVal;
    Mmu_MapAttrs    attrs;

    Mmu_initMapAttrs(&attrs);
    attrs.attrIndx = 0;

    retVal = Mmu_map(0x00000000, 0x00000000, 0x20000000, &attrs);
    if(retVal==FALSE)
    {
        goto mmu_exit;
    }

    retVal = Mmu_map(0x0100000, 0x0100000, 0x00900000, &attrs); /* PLL_MMR_CFG registers regs       */
    if(retVal == FALSE)
    {
        goto mmu_exit;
    }

    retVal = Mmu_map(0x00400000, 0x00400000, 0x00001000, &attrs); /* PSC0          */
    if(retVal == FALSE)
    {
        goto mmu_exit;
    }

#if defined(SOC_J721E) || defined (SOC_J7200)
    retVal = Mmu_map(0x01800000, 0x01800000, 0x00200000, &attrs); /* gicv3       */
    if(retVal == FALSE)
    {
        goto mmu_exit;
    }

	/* SCICLIENT UDMA */
	retVal = Mmu_map(0x20000000ul, 0x20000000ul, 0x10000000ul, &attrs);
    if (retVal == FALSE)
    {
         goto mmu_exit;
    }
#else
    retVal = Mmu_map(0x01800000, 0x01800000, 0x00100000, &attrs); /* gicv3       */
    if(retVal == FALSE)
    {
        goto mmu_exit;
    }
#endif

    retVal = Mmu_map(0x02400000, 0x02400000, 0x000c0000, &attrs); /* dmtimer     */
    if(retVal == FALSE)
    {
        goto mmu_exit;
    }

    retVal = Mmu_map(0x02800000, 0x02800000, 0x00040000, &attrs); /* uart        */
    if(retVal == FALSE)
    {
        goto mmu_exit;
    }

    retVal = Mmu_map(0x02000000, 0x02000000, 0x00100000, &attrs); /* I2C            */
    if(retVal == FALSE)
    {
        goto mmu_exit;
    }

    retVal = Mmu_map(0x02100000, 0x02100000, 0x00080000, &attrs); /* McSPI          */
    if(retVal == FALSE)
    {
        goto mmu_exit;
    }

    retVal = Mmu_map(0x40f00000, 0x40f00000, 0x00020000, &attrs); /* MCU MMR0 CFG   */
    if(retVal == FALSE)
    {
        goto mmu_exit;
    }
    retVal = Mmu_map(0x40d00000, 0x40d00000, 0x00002000, &attrs); /* PLL0 CFG       */
    if(retVal == FALSE)
    {
        goto mmu_exit;
    }

    retVal = Mmu_map(0x43000000, 0x43000000, 0x00020000, &attrs); /* WKUP MMR0 cfg  */
    if(retVal == FALSE)
    {
        goto mmu_exit;
    }

    retVal = Mmu_map(0x02C40000, 0x02C40000, 0x00100000, &attrs); /* pinmux ctrl    */
    if(retVal == FALSE)
    {
        goto mmu_exit;
    }

    retVal = Mmu_map(0x2A430000, 0x2A430000, 0x00001000, &attrs); /* ctrcontrol0 */
    if(retVal == FALSE)
    {
        goto mmu_exit;
    }

    retVal = Mmu_map(0x030800000, 0x030800000, 0xC000000, &attrs); /* navss        */
    if(retVal == FALSE)
    {
        goto mmu_exit;
    }

    retVal = Mmu_map(0x28380000, 0x28380000, 0xC000000, &attrs); /* MCU NAVSS */
    if(retVal == FALSE)
    {
        goto mmu_exit;
    }

    retVal = Mmu_map(0x6D000000, 0x6D000000, 0x1000000, &attrs); /* DRU */
    if(retVal == FALSE)
    {
        goto mmu_exit;
    }

    retVal = Mmu_map(0x42000000, 0x42000000, 0x00001000, &attrs); /* PSC WKUP*/
    if (retVal == FALSE)
    {
         goto mmu_exit;
    }

    /*
     * DDR range 0xA0000000 - 0xAA000000 : Used as RAM by multiple
     * remote cores, no need to mmp_map this range.
     * IPC VRing Buffer - uncached
     * */
    attrs.attrIndx = 4;

#if defined (SOC_J721E)
    retVal = Mmu_map(0xAA000000, 0xAA000000, 0x02000000, &attrs);
#endif

#if defined (SOC_J7200)
    retVal = Mmu_map(0xA4000000, 0xA4000000, 0x0800000, &attrs);
#endif

    if(retVal == FALSE)
    {
        goto mmu_exit;
    }

    attrs.attrIndx = 7;
    retVal = Mmu_map(0x80000000, 0x80000000, 0x20000000, &attrs); /* ddr            */
    if(retVal == FALSE)
    {
        goto mmu_exit;
    }

    retVal = Mmu_map(0x70000000, 0x70000000, 0x04000000, &attrs); /* msmc        */
    if(retVal == FALSE)
    {
        goto mmu_exit;
    }

mmu_exit:
    if(retVal == FALSE)
    {
         System_printf("Mmu_map returned error %d",retVal);
         while(emuwait_mmu);
    }

    return;
}
#endif

/*
 *  ======== SPI init config ========
 */
static void SPI_InitConfigDefault(SPI_HWAttrs *cfg, uint32_t chn)
{
    cfg->chNum                        = chn;
    cfg->chnCfg[chn].tcs              = MCSPI_CS_TCS_0PNT5_CLK;
    cfg->chnCfg[chn].dataLineCommMode = MCSPI_DATA_LINE_COMM_MODE_1;
    cfg->chnCfg[chn].trMode           = MCSPI_TX_RX_MODE;
    cfg->initDelay                    = MCSPI_INITDLY_0;
    cfg->rxTrigLvl                    = MCSPI_RX_TX_FIFO_SIZE;
    cfg->txTrigLvl                    = MCSPI_RX_TX_FIFO_SIZE;
}

/*
 *  ======== SPI init config ========
 */
static void SPI_InitConfig(uint32_t instance, uint32_t chn)
{
    SPI_HWAttrs spi_cfg;

    /* Get the default SPI init configurations */
    SPI_socGetInitCfg(instance, &spi_cfg);

    /* Set the default SPI init configurations */
    SPI_InitConfigDefault(&spi_cfg, chn);

    /* interrupt enabled */
    spi_cfg.enableIntr = true;
    spi_cfg.edmaHandle = NULL;
    spi_cfg.dmaMode    = FALSE;

    /* Transmit/Receive mode */
    spi_cfg.chnCfg[chn].trMode = MCSPI_TX_RX_MODE;

    /* Set Pin direction */
    spi_cfg.chnCfg[chn].dataLineCommMode = MCSPI_DATA_LINE_COMM_MODE_1;

    /* Set the SPI init configurations */
    SPI_socSetInitCfg(instance, &spi_cfg);
}

int IpcSpiSlaveApp_ProfileTest(SpiSlaveApp_TestParams  *IpcSpiSlaveApp_TestPrms)
{
    SPI_Handle        spi;
    SPI_Params        spiParams;
    uint32_t          instance, modeIndex, SPI_modeIndex, chNum, itr, msgLpCnt;
    int               ret = true;
    uint32_t          dataLength = IpcSpiSlaveApp_TestPrms->dataLength;
	SPI_FrameFormat   frameFormat[]=
    {
		SPI_POL0_PHA0, /*!< SPI mode Polarity 0 Phase 0 */
		SPI_POL0_PHA1, /*!< SPI mode Polarity 0 Phase 1 */
		SPI_POL1_PHA0, /*!< SPI mode Polarity 1 Phase 0 */
		SPI_POL1_PHA1,  /*!< SPI mode Polarity 1 Phase 1 */
    };

    if(dataLength == 0U || dataLength > RX_TX_MAX_BUF_SIZE_WORD)
    {
        ret = false;
        AppUtils_Printf("Error Data length is Invalid\n");
    }

    if (ret == true)
    {
        SPI_init();
        /* Select Main Domain MCSPI4 i.e connected as slave to MCU_MCSPI2 */
        instance = MCSPI_INSTANCE_ID;

        /* Currently only with SPI mode Polarity 0 Phase 0 is tested */
        SPI_modeIndex = 1;

        /* In case of SPI_TEST_ID_PHA_POL all SPI modes need to be tested */
        for (modeIndex = 0; modeIndex < SPI_modeIndex; modeIndex++)
        {
            ret = true;

            /* In slave mode onlly channel 0 should be used */
            chNum = MCSPI_CHANNEL_NUM;

            /* Initialize SPI handle */
            SPI_InitConfig(instance, chNum);
            SPI_Params_init(&spiParams);

            /* Configure SPI params */
            spiParams.mode = SPI_SLAVE;
            spiParams.transferMode = SPI_MODE_BLOCKING;
            spiParams.transferCallbackFxn = NULL;
            spiParams.transferTimeout = SemaphoreP_WAIT_FOREVER;
            spiParams.frameFormat = frameFormat[modeIndex];
            spiParams.dataSize = SPI_APP_DATA_SIZE_WIDTH;

            /* Initialize SPI instance */
            spi = SPI_open(instance, &spiParams);

            if (spi == NULL)
            {
                AppUtils_Printf("Error initializing SPI\n");
                ret = false;
            }
            else
            {
                AppUtils_Printf(MSG_APP_NAME "SPI initialized\n");
            }

            if (ret == true)
            {
                for (itr = 0U; itr < APP_NUM_ITERATION; itr++)
                {
                    for (msgLpCnt = 0U; msgLpCnt < APP_NUM_MSG_PER_ITERATION;
                        msgLpCnt++)
                    {
                        if (SPI_Test_mst_slv_xfer((void *)spi,
                            IpcSpiSlaveApp_TestPrms)
                            == false)
                        {
                            ret = false;
                        }
                    }
                }
            }
            if (spi)
            {
                SPI_close(spi);
            }
        } /* End of for loop */

    }

    if (ret == true)
    {
        AppUtils_Printf(MSG_APP_NAME "Successful slave SPI transmit/receive \n");
    }

    return (ret);
}

static bool SPI_Test_mst_slv_xfer(void *spi, SpiSlaveApp_TestParams
    *IpcSpiSlaveApp_TestPrms)

{
    bool            transferOK;
    bool            ret = false;
    uint32_t        lpCnt;
    uint32_t        transCount, testLen;

    memset(slaveRxBuffer, 0U, sizeof(slaveRxBuffer));
    memset(slaveTxBuffer, 0U, sizeof(slaveTxBuffer));

    testLen = IpcSpiSlaveApp_TestPrms->dataLength;

    /* Default word length is 32-bit */
    transCount = testLen;

    /* Initialize slave SPI transaction structure */
    transaction.count = transCount;
    transaction.arg = (void *)&terminateXfer;
    transaction.txBuf = (void *)slaveTxBuffer;
    transaction.rxBuf = (void *)slaveRxBuffer;

    /* Initiate SPI transfer */
    transferOK = SPI_transfer((SPI_Handle)spi, &transaction);
    if(transferOK)
    {
        for (lpCnt = 0U; lpCnt < testLen * 4U; lpCnt++)
        {
            slaveTxBuffer[lpCnt] = slaveRxBuffer[lpCnt];
        }
        ret = true;
    }
    else
    {
        if (transaction.status == SPI_TRANSFER_TIMEOUT)
        {
            AppUtils_Printf("SPI Slave receive Timed out for receive length %d\n",
                testLen);
        }
        else
        {
            ret = false;
        }
    }

    /* Initiate Transfer from slave side to master */
    if (ret == true)
    {
        /* Initiate SPI transfer */
        transferOK = SPI_transfer((SPI_Handle)spi, &transaction);
        if(transferOK)
        {

            ret = true;
        }
        else
        {
            if (transaction.status == SPI_TRANSFER_TIMEOUT)
            {
                AppUtils_Printf("SPI Slave transfer Timed out for \
                transfer length %d\n", testLen);
            }
            else
            {
                AppUtils_Printf("Unsuccessful slave SPI transfer");
                ret = false;
            }
        }
    }

    return (ret);
}

/**
 *  \brief Printf utility
 *
 */
Void AppUtils_Printf (const char *pcString, ...)
{
    static char printBuffer[APP_UTILS_PRINT_MAX_SIZE];
    va_list arguments;

    /* Start the varargs processing. */
    va_start(arguments, pcString);
    vsnprintf (printBuffer, sizeof(printBuffer), pcString, arguments);

    printf(printBuffer);

    /* End the varargs processing. */
    va_end(arguments);

    return;
}
