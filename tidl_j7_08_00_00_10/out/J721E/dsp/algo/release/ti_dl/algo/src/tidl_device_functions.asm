;******************************************************************************
;* C7x G3 C/C++ Codegen                                            Unix v1.4.2.LTS *
;* Date/Time created: Tue Oct  5 15:11:29 2021                                *
;******************************************************************************
	.compiler_opts --abi=eabi --endian=little --hll_source=on --object_format=elf --silicon_errata_i2117 --silicon_version=7100 --symdebug:dwarf --symdebug:dwarf_version=4 

;******************************************************************************
;* GLOBAL FILE PARAMETERS                                                     *
;*                                                                            *
;*   Architecture      : C7100                                                *
;*   Endian            : Little                                               *
;*   Pipelining        : Enabled                                              *
;*   Debug Info        : DWARF Debug                                          *
;*                                                                            *
;******************************************************************************

	.asg	D14, FP
	.asg	D15, SP
	.global	$bss


$C$DW$CU	.dwtag  DW_TAG_compile_unit
	.dwattr $C$DW$CU, DW_AT_name("src/tidl_device_functions.c")
	.dwattr $C$DW$CU, DW_AT_producer("TI C7x G3 C/C++ Codegen Unix v1.4.2.LTS Copyright (c) 2014-2019 Texas Instruments Incorporated")
	.dwattr $C$DW$CU, DW_AT_TI_version(0x01)
	.dwattr $C$DW$CU, DW_AT_comp_dir("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/tidl_j7_08_00_00_10/ti_dl/algo")

$C$DW$1	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1, DW_AT_name("void TIDL_AM_conv2dBiasSplit")
	.dwattr $C$DW$1, DW_AT_linkage_name("_Z23TIDL_AM_conv2dBiasSplitIisEvPT0_PT_Piifiiii")
	.dwattr $C$DW$1, DW_AT_declaration
	.dwattr $C$DW$1, DW_AT_external
	.dwattr $C$DW$1, DW_AT_decl_file("./inc/tidl_commonUtils.h")
	.dwattr $C$DW$1, DW_AT_decl_line(0x7c)
	.dwattr $C$DW$1, DW_AT_decl_column(0x06)
$C$DW$2	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$2, DW_AT_type(*$C$DW$T$68)

$C$DW$3	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$3, DW_AT_type(*$C$DW$T$70)

$C$DW$4	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$4, DW_AT_type(*$C$DW$T$70)

$C$DW$5	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$5, DW_AT_type(*$C$DW$T$26)

$C$DW$6	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$6, DW_AT_type(*$C$DW$T$16)

$C$DW$7	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$7, DW_AT_type(*$C$DW$T$26)

$C$DW$8	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$8, DW_AT_type(*$C$DW$T$26)

$C$DW$9	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$9, DW_AT_type(*$C$DW$T$26)

$C$DW$10	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$10, DW_AT_type(*$C$DW$T$26)

	.dwendtag $C$DW$1


$C$DW$17	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$17, DW_AT_name("void TIDL_conv2dBiasSplit")
	.dwattr $C$DW$17, DW_AT_linkage_name("_Z20TIDL_conv2dBiasSplitIasEvPT0_PT_Piifiiii")
	.dwattr $C$DW$17, DW_AT_declaration
	.dwattr $C$DW$17, DW_AT_external
	.dwattr $C$DW$17, DW_AT_decl_file("./inc/tidl_commonUtils.h")
	.dwattr $C$DW$17, DW_AT_decl_line(0x7a)
	.dwattr $C$DW$17, DW_AT_decl_column(0x06)
$C$DW$18	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$18, DW_AT_type(*$C$DW$T$68)

$C$DW$19	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$19, DW_AT_type(*$C$DW$T$69)

$C$DW$20	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$20, DW_AT_type(*$C$DW$T$70)

$C$DW$21	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$21, DW_AT_type(*$C$DW$T$26)

$C$DW$22	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$22, DW_AT_type(*$C$DW$T$16)

$C$DW$23	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$23, DW_AT_type(*$C$DW$T$26)

$C$DW$24	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$24, DW_AT_type(*$C$DW$T$26)

$C$DW$25	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$25, DW_AT_type(*$C$DW$T$26)

$C$DW$26	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$26, DW_AT_type(*$C$DW$T$26)

	.dwendtag $C$DW$17

;	/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/bin/opt7x /tmp/TI4ZrlZ92Ep /tmp/TI4ZrB9Fzva 
	.sect	".text:_Z28TIDL_getRowFlowHandleCountAMiiii"
	.clink
	.global	||_Z28TIDL_getRowFlowHandleCountAMiiii||

$C$DW$27	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$27, DW_AT_name("TIDL_getRowFlowHandleCountAM")
	.dwattr $C$DW$27, DW_AT_low_pc(||_Z28TIDL_getRowFlowHandleCountAMiiii||)
	.dwattr $C$DW$27, DW_AT_high_pc(0x00)
	.dwattr $C$DW$27, DW_AT_linkage_name("_Z28TIDL_getRowFlowHandleCountAMiiii")
	.dwattr $C$DW$27, DW_AT_external
	.dwattr $C$DW$27, DW_AT_decl_file("src/tidl_device_functions.c")
	.dwattr $C$DW$27, DW_AT_decl_line(0x54)
	.dwattr $C$DW$27, DW_AT_decl_column(0x09)
	.dwattr $C$DW$27, DW_AT_TI_max_frame_size(0x00)
	.dwpsn	file "src/tidl_device_functions.c",line 85,column 1,is_stmt,address ||_Z28TIDL_getRowFlowHandleCountAMiiii||,isa 0

	.dwfde $C$DW$CIE, ||_Z28TIDL_getRowFlowHandleCountAMiiii||
$C$DW$28	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$28, DW_AT_name("inWidth")
	.dwattr $C$DW$28, DW_AT_location[DW_OP_reg4]

$C$DW$29	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$29, DW_AT_name("procSize")
	.dwattr $C$DW$29, DW_AT_location[DW_OP_reg5]

$C$DW$30	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$30, DW_AT_name("dmaFreq")
	.dwattr $C$DW$30, DW_AT_location[DW_OP_reg6]

$C$DW$31	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$31, DW_AT_name("Fc")
	.dwattr $C$DW$31, DW_AT_location[DW_OP_reg7]


;******************************************************************************
;* FUNCTION NAME: TIDL_getRowFlowHandleCountAM(int, int, int, int)            *
;*                                                                            *
;*   Regs Modified     : A0,A1,A2,A4,AL0,D0                                   *
;*   Regs Used         : A0,A1,A2,A4,A5,A7,AL0,D0                             *
;*   Local Frame Size  : 0 Args + 0 Auto + 0 Save = 0 byte                    *
;******************************************************************************
||_Z28TIDL_getRowFlowHandleCountAMiiii||:
;** --------------------------------------------------------------------------*
;* A2    assigned to $O$K13
;* A1    assigned to $O$Lr11$nCirc
;* A1    assigned to $O$Lr3$nBottom
;* A2    assigned to $O$Lr15$nTop
;* A5    assigned to procSize
$C$DW$32	.dwtag  DW_TAG_variable
	.dwattr $C$DW$32, DW_AT_name("procSize")
	.dwattr $C$DW$32, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$32, DW_AT_location[DW_OP_reg5]

;* A4    assigned to inWidth
$C$DW$33	.dwtag  DW_TAG_variable
	.dwattr $C$DW$33, DW_AT_name("inWidth")
	.dwattr $C$DW$33, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$33, DW_AT_location[DW_OP_reg4]

	.dwcfi	cfa_offset, 0
;          EXCLUSIVE CPU CYCLES: 8
	.dwpsn	file "src/tidl_device_functions.c",line 90,column 17,is_stmt,isa 0
           SHRUW   .L1     A7,0x1f,D0        ; [A_L1] |90| 
           ADDW    .D1     D0,A7,AL0         ; [A_D1] |90| 

           SHRW    .L1     AL0,0x1,AL0       ; [A_L1] |90| 
||         ADDW    .D1     A5,A4,D0          ; [A_D1] |99| 

           SHLW    .L1     AL0,0x1,D0        ; [A_L1] |90| 
||         ADDW    .D1     D0,0xffffffff,AL0 ; [A_D1] |99| 

           ADDW    .D1     D0,0x1,A1         ; [A_D1] |90| 
||         DIVW    .L1     AL0,A5,A2         ; [A_L1] |99| 

	.dwpsn	file "src/tidl_device_functions.c",line 99,column 3,is_stmt,isa 0
           CMPGTW  .L1     A2,A1,A0          ; [A_L1] |99| 
	.dwpsn	file "src/tidl_device_functions.c",line 107,column 3,is_stmt,isa 0

   [ A0]   MV      .D1     A2,A1             ; [A_D1] 
||         ADDW    .D2     A1,A2,D0          ; [A_D2] |107| 

$C$DW$34	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$34, DW_AT_low_pc(0x00)
	.dwattr $C$DW$34, DW_AT_TI_return


           RET     .B1     ; [A_B] 
||         ADDW    .D1     A1,D0,A4          ; [A_D1] |107| 

           ; RETURN OCCURS {RP}              ; [] 
	.dwattr $C$DW$27, DW_AT_TI_end_file("src/tidl_device_functions.c")
	.dwattr $C$DW$27, DW_AT_TI_end_line(0x6c)
	.dwattr $C$DW$27, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$27

	.sect	".text:_Z22TIDL_getSubHandleCountiiiPK15sDataFlowInfo_ti"
	.clink
	.global	||_Z22TIDL_getSubHandleCountiiiPK15sDataFlowInfo_ti||

$C$DW$35	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$35, DW_AT_name("TIDL_getSubHandleCount")
	.dwattr $C$DW$35, DW_AT_low_pc(||_Z22TIDL_getSubHandleCountiiiPK15sDataFlowInfo_ti||)
	.dwattr $C$DW$35, DW_AT_high_pc(0x00)
	.dwattr $C$DW$35, DW_AT_linkage_name("_Z22TIDL_getSubHandleCountiiiPK15sDataFlowInfo_ti")
	.dwattr $C$DW$35, DW_AT_external
	.dwattr $C$DW$35, DW_AT_decl_file("src/tidl_device_functions.c")
	.dwattr $C$DW$35, DW_AT_decl_line(0x78)
	.dwattr $C$DW$35, DW_AT_decl_column(0x09)
	.dwattr $C$DW$35, DW_AT_TI_max_frame_size(0x00)
	.dwpsn	file "src/tidl_device_functions.c",line 121,column 1,is_stmt,address ||_Z22TIDL_getSubHandleCountiiiPK15sDataFlowInfo_ti||,isa 0

	.dwfde $C$DW$CIE, ||_Z22TIDL_getSubHandleCountiiiPK15sDataFlowInfo_ti||
$C$DW$36	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$36, DW_AT_name("deviceName")
	.dwattr $C$DW$36, DW_AT_location[DW_OP_reg4]

$C$DW$37	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$37, DW_AT_name("dataFlowType")
	.dwattr $C$DW$37, DW_AT_location[DW_OP_reg5]

$C$DW$38	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$38, DW_AT_name("inWidth")
	.dwattr $C$DW$38, DW_AT_location[DW_OP_reg6]

$C$DW$39	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$39, DW_AT_name("dataFlowInfo")
	.dwattr $C$DW$39, DW_AT_location[DW_OP_reg7]

$C$DW$40	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$40, DW_AT_name("kernelWidth")
	.dwattr $C$DW$40, DW_AT_location[DW_OP_reg8]


;******************************************************************************
;* FUNCTION NAME: TIDL_getSubHandleCount(int, int, int, const sDataFlowInfo_t *, int)*
;*                                                                            *
;*   Regs Modified     : A0,A1,A2,A3,A4,VB0,AL0,AL1,AL7,D0,D1,D2              *
;*   Regs Used         : A0,A1,A2,A3,A4,A5,A6,A7,A8,VB0,AL0,AL1,AL7,D0,D1,D2  *
;*   Local Frame Size  : 0 Args + 0 Auto + 0 Save = 0 byte                    *
;******************************************************************************
||_Z22TIDL_getSubHandleCountiiiPK15sDataFlowInfo_ti||:
;** --------------------------------------------------------------------------*
;* A0    assigned to $O$C1
;* D0    assigned to $O$Lr16$nCirc
;* A3    assigned to $O$Lr5$nBottom
;* A2    assigned to $O$Lr20$nTop
;* A8    assigned to kernelWidth
$C$DW$41	.dwtag  DW_TAG_variable
	.dwattr $C$DW$41, DW_AT_name("kernelWidth")
	.dwattr $C$DW$41, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$41, DW_AT_location[DW_OP_reg8]

;* A7    assigned to dataFlowInfo
$C$DW$42	.dwtag  DW_TAG_variable
	.dwattr $C$DW$42, DW_AT_name("dataFlowInfo")
	.dwattr $C$DW$42, DW_AT_type(*$C$DW$T$95)
	.dwattr $C$DW$42, DW_AT_location[DW_OP_reg7]

;* VB0   assigned to inWidth
$C$DW$43	.dwtag  DW_TAG_variable
	.dwattr $C$DW$43, DW_AT_name("inWidth")
	.dwattr $C$DW$43, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$43, DW_AT_location[DW_OP_regx 0x30]

;* A5    assigned to dataFlowType
$C$DW$44	.dwtag  DW_TAG_variable
	.dwattr $C$DW$44, DW_AT_name("dataFlowType")
	.dwattr $C$DW$44, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$44, DW_AT_location[DW_OP_reg5]

;* A4    assigned to deviceName
$C$DW$45	.dwtag  DW_TAG_variable
	.dwattr $C$DW$45, DW_AT_name("deviceName")
	.dwattr $C$DW$45, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$45, DW_AT_location[DW_OP_reg4]

	.dwcfi	cfa_offset, 0
;          EXCLUSIVE CPU CYCLES: 7
	.dwpsn	file "src/tidl_device_functions.c",line 124,column 3,is_stmt,isa 0

           MVKU32  .L1     0,D0              ; [A_L1] |124| 
||         MVK64   .S1     0,AL0             ; [A_S1] |124| 

           CMPEQW  .L1     A4,0x2,AL0        ; [A_L1] |124| 
||         CMPEQD  .S1     A7,AL0,A0         ; [A_S1] |124| 

           XORD    .L1     AL0,0x1,D1        ; [A_L1] |124| 
|| [ A0]   MVKU32  .S1     0x1,D0            ; [A_S1] |124| 

           ANDW    .L1     A5,0xfffffffe,D1  ; [A_L1] |124| 
||         ORW     .D1     D0,D1,D0          ; [A_D1] |124| 

           ORW     .D1     D1,D0,AL0         ; [A_D1] |124| 
           CMPEQW  .L1     AL0,0,A0          ; [A_L1] |124| 
	.dwpsn	file "src/tidl_device_functions.c",line 121,column 1,is_stmt,isa 0

   [!A0]   B       .B1     ||$C$L1||         ; [A_B] |124| 
||         MV      .L2X    A6,B0             ; [B_L2] |121| 

	.dwpsn	file "src/tidl_device_functions.c",line 124,column 3,is_stmt,isa 0
           ; BRANCHCC OCCURS {||$C$L1||}     ; [] |124| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 10
	.dwpsn	file "src/tidl_device_functions.c",line 130,column 6,is_stmt,isa 0

           LDW     .D1     *A7(40),D1        ; [A_D1] |130| 
||         LDW     .D2     *A7(8),A0         ; [A_D2] |130| 

           LDW     .D1     *A7(44),AL0       ; [A_D1] |130| 
	.dwpsn	file "src/tidl_device_functions.c",line 90,column 17,is_stmt,isa 0
           SHRUW   .L1     A8,0x1f,D0        ; [A_L1] |90| 
           ADDW    .D1     D0,A8,AL1         ; [A_D1] |90| 
           SHRW    .L1     AL1,0x1,AL1       ; [A_L1] |90| 

           ADDW    .D1     A0,D1,AL7         ; [A_D1] |130| 
||         ADDW    .M1X    B0,A0,D0          ; [A_M1] |99| 
||         SHLW    .L1     AL1,0x1,D2        ; [A_L1] |90| 

           ADDW    .D1     D2,0x1,D0         ; [A_D1] |90| 
||         ADDW    .D2     D0,0xffffffff,AL1 ; [A_D2] |99| 

	.dwpsn	file "src/tidl_device_functions.c",line 99,column 3,is_stmt,isa 0

           DIVW    .L1     AL1,A0,A2         ; [A_L1] |99| 
||         MV      .D1     D0,A3             ; [A_D1] 
||         CMPGTW  .S1     AL0,AL7,A1        ; [A_S1] |130| 

   [ A1]   B       .B1     ||$C$L2||         ; [A_B] |130| 
||         CMPGTW  .L1     A2,A3,A0          ; [A_L1] |99| 

	.dwpsn	file "src/tidl_device_functions.c",line 130,column 6,is_stmt,isa 0
           ; BRANCHCC OCCURS {||$C$L2||}     ; [] |130| 
;** --------------------------------------------------------------------------*
||$C$L1||:    
;          EXCLUSIVE CPU CYCLES: 1
	.dwpsn	file "src/tidl_device_functions.c",line 142,column 3,is_stmt,isa 0

           B       .B1     ||$C$L3||         ; [A_B] |142| 
||         MVKU32  .L1     0x1,A4            ; [A_L1] |142| 

           ; BRANCH OCCURS {||$C$L3||}       ; [] |142| 
;** --------------------------------------------------------------------------*
||$C$L2||:    
;          EXCLUSIVE CPU CYCLES: 2

           ADDW    .D1     D0,A2,D0          ; [A_D1] |142| 
|| [ A0]   MV      .D2     A2,A3             ; [A_D2] 

           ADDW    .D1     A3,D0,A4          ; [A_D1] |142| 
;** --------------------------------------------------------------------------*
||$C$L3||:    
;          EXCLUSIVE CPU CYCLES: 1
$C$DW$46	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$46, DW_AT_low_pc(0x00)
	.dwattr $C$DW$46, DW_AT_TI_return

           RET     .B1     ; [A_B] 
           ; RETURN OCCURS {RP}              ; [] 
	.dwattr $C$DW$35, DW_AT_TI_end_file("src/tidl_device_functions.c")
	.dwattr $C$DW$35, DW_AT_TI_end_line(0x90)
	.dwattr $C$DW$35, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$35

	.sect	".text:_Z21TIDL_getBiasParamSizeii"
	.clink
	.global	||_Z21TIDL_getBiasParamSizeii||

$C$DW$47	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$47, DW_AT_name("TIDL_getBiasParamSize")
	.dwattr $C$DW$47, DW_AT_low_pc(||_Z21TIDL_getBiasParamSizeii||)
	.dwattr $C$DW$47, DW_AT_high_pc(0x00)
	.dwattr $C$DW$47, DW_AT_linkage_name("_Z21TIDL_getBiasParamSizeii")
	.dwattr $C$DW$47, DW_AT_external
	.dwattr $C$DW$47, DW_AT_decl_file("src/tidl_device_functions.c")
	.dwattr $C$DW$47, DW_AT_decl_line(0x99)
	.dwattr $C$DW$47, DW_AT_decl_column(0x09)
	.dwattr $C$DW$47, DW_AT_TI_max_frame_size(0x00)
	.dwpsn	file "src/tidl_device_functions.c",line 154,column 1,is_stmt,address ||_Z21TIDL_getBiasParamSizeii||,isa 0

	.dwfde $C$DW$CIE, ||_Z21TIDL_getBiasParamSizeii||
$C$DW$48	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$48, DW_AT_name("deviceName")
	.dwattr $C$DW$48, DW_AT_location[DW_OP_reg4]

$C$DW$49	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$49, DW_AT_name("numOutChannels")
	.dwattr $C$DW$49, DW_AT_location[DW_OP_reg5]


;******************************************************************************
;* FUNCTION NAME: TIDL_getBiasParamSize(int, int)                             *
;*                                                                            *
;*   Regs Modified     : A0,A4                                                *
;*   Regs Used         : A0,A4,A5                                             *
;*   Local Frame Size  : 0 Args + 0 Auto + 0 Save = 0 byte                    *
;******************************************************************************
||_Z21TIDL_getBiasParamSizeii||:
;** --------------------------------------------------------------------------*
;* A4    assigned to $O$Lr2$biasParamSize
;* A5    assigned to numOutChannels
$C$DW$50	.dwtag  DW_TAG_variable
	.dwattr $C$DW$50, DW_AT_name("numOutChannels")
	.dwattr $C$DW$50, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$50, DW_AT_location[DW_OP_reg5]

;* A4    assigned to deviceName
$C$DW$51	.dwtag  DW_TAG_variable
	.dwattr $C$DW$51, DW_AT_name("deviceName")
	.dwattr $C$DW$51, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$51, DW_AT_location[DW_OP_reg4]

	.dwcfi	cfa_offset, 0
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "src/tidl_device_functions.c",line 159,column 5,is_stmt,isa 0

           SHLW    .L1     A5,0x3,A4         ; [A_L1] |159| 
||         CMPEQW  .S1     A4,0x2,A0         ; [A_S1] |159| 

$C$DW$52	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$52, DW_AT_low_pc(0x00)
	.dwattr $C$DW$52, DW_AT_TI_return


           RET     .B1     ; [A_B] 
|| [!A0]   SHLW    .L1     A5,0x2,A4         ; [A_L1] |159| 

           ; RETURN OCCURS {RP}              ; [] 
	.dwattr $C$DW$47, DW_AT_TI_end_file("src/tidl_device_functions.c")
	.dwattr $C$DW$47, DW_AT_TI_end_line(0xa8)
	.dwattr $C$DW$47, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$47

	.sect	".text:_Z20TIDL_conv2dBiasSplitIasEviiPT0_PT_Piifiiii"
	.clink
	.global	||_Z20TIDL_conv2dBiasSplitIasEviiPT0_PT_Piifiiii||

$C$DW$53	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$53, DW_AT_name("void TIDL_conv2dBiasSplit")
	.dwattr $C$DW$53, DW_AT_low_pc(||_Z20TIDL_conv2dBiasSplitIasEviiPT0_PT_Piifiiii||)
	.dwattr $C$DW$53, DW_AT_high_pc(0x00)
	.dwattr $C$DW$53, DW_AT_linkage_name("_Z20TIDL_conv2dBiasSplitIasEviiPT0_PT_Piifiiii")
	.dwattr $C$DW$53, DW_AT_external
	.dwattr $C$DW$53, DW_AT_decl_file("src/tidl_device_functions.c")
	.dwattr $C$DW$53, DW_AT_decl_line(0xbb)
	.dwattr $C$DW$53, DW_AT_decl_column(0x06)
	.dwattr $C$DW$53, DW_AT_TI_max_frame_size(0x30)
	.dwpsn	file "src/tidl_device_functions.c",line 188,column 1,is_stmt,address ||_Z20TIDL_conv2dBiasSplitIasEviiPT0_PT_Piifiiii||,isa 0

	.dwfde $C$DW$CIE, ||_Z20TIDL_conv2dBiasSplitIasEviiPT0_PT_Piifiiii||
$C$DW$54	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$54, DW_AT_name("deviceName")
	.dwattr $C$DW$54, DW_AT_location[DW_OP_reg4]

$C$DW$55	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$55, DW_AT_name("dataFlowType")
	.dwattr $C$DW$55, DW_AT_location[DW_OP_reg5]

$C$DW$56	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$56, DW_AT_name("srcPtr")
	.dwattr $C$DW$56, DW_AT_type(*$C$DW$T$68)
	.dwattr $C$DW$56, DW_AT_location[DW_OP_reg6]

$C$DW$57	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$57, DW_AT_name("dstPtr")
	.dwattr $C$DW$57, DW_AT_type(*$C$DW$T$69)
	.dwattr $C$DW$57, DW_AT_location[DW_OP_reg7]

$C$DW$58	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$58, DW_AT_name("biasB")
	.dwattr $C$DW$58, DW_AT_location[DW_OP_reg8]

$C$DW$59	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$59, DW_AT_name("dataSize")
	.dwattr $C$DW$59, DW_AT_location[DW_OP_reg9]

$C$DW$60	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$60, DW_AT_name("inScaleFactor")
	.dwattr $C$DW$60, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$60, DW_AT_location[DW_OP_reg10]

$C$DW$61	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$61, DW_AT_name("satLow")
	.dwattr $C$DW$61, DW_AT_location[DW_OP_reg11]

$C$DW$62	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$62, DW_AT_name("satHigh")
	.dwattr $C$DW$62, DW_AT_location[DW_OP_reg12]

$C$DW$63	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$63, DW_AT_name("biasBMax")
	.dwattr $C$DW$63, DW_AT_location[DW_OP_bregx 0x6f 64]

$C$DW$64	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$64, DW_AT_name("inFeatSign")
	.dwattr $C$DW$64, DW_AT_location[DW_OP_bregx 0x6f 68]


;******************************************************************************
;* FUNCTION NAME: void TIDL_conv2dBiasSplit<signed char, short>(int, int, T2 *, T1 *, int *, int, float, int, int, int, int)*
;*                                                                            *
;*   Regs Modified     : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,VB0,   *
;*                           VB1,VB2,VB3,VB4,VB5,VB6,VB7,VB8,VB9,VB10,VB11,   *
;*                           VB12,VB13,AL0,AL1,AL2,AL3,AL4,AL5,AL6,AL7,AM0,   *
;*                           AM1,AM2,AM3,AM4,AM5,AM6,AM7,D0,D1,D2,D3,D4,D5,D6,*
;*                           D7,D8,D9,D10,D11,D12,D13,D14,SP,VBL0,VBL1,VBL2,  *
;*                           VBL3,VBL4,VBL5,VBL6,VBL7,VBM0,VBM1,VBM2,VBM3,    *
;*                           VBM4,VBM5,VBM6,VBM7,P0,P1,P2,P3,P4,P5,P6,P7,     *
;*                           CUCR0,CUCR1,CUCR2,CUCR3                          *
;*   Regs Used         : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,VB0,   *
;*                           VB1,VB2,VB3,VB4,VB5,VB6,VB7,VB8,VB9,VB10,VB11,   *
;*                           VB12,VB13,AL0,AL1,AL2,AL3,AL4,AL5,AL6,AL7,AM0,   *
;*                           AM1,AM2,AM3,AM4,AM5,AM6,AM7,D0,D1,D2,D3,D4,D5,D6,*
;*                           D7,D8,D9,D10,D11,D12,D13,D14,SP,VBL0,VBL1,VBL2,  *
;*                           VBL3,VBL4,VBL5,VBL6,VBL7,VBM0,VBM1,VBM2,VBM3,    *
;*                           VBM4,VBM5,VBM6,VBM7,P0,P1,P2,P3,P4,P5,P6,P7,     *
;*                           CUCR0,CUCR1,CUCR2,CUCR3                          *
;*   Local Frame Size  : 0 Args + 0 Auto + 48 Save = 48 byte                  *
;******************************************************************************
||_Z20TIDL_conv2dBiasSplitIasEviiPT0_PT_Piifiiii||:
;** --------------------------------------------------------------------------*
;* A12   assigned to inFeatSign
$C$DW$65	.dwtag  DW_TAG_variable
	.dwattr $C$DW$65, DW_AT_name("inFeatSign")
	.dwattr $C$DW$65, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$65, DW_AT_location[DW_OP_reg12]

;* A11   assigned to biasBMax
$C$DW$66	.dwtag  DW_TAG_variable
	.dwattr $C$DW$66, DW_AT_name("biasBMax")
	.dwattr $C$DW$66, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$66, DW_AT_location[DW_OP_reg11]

;* A10   assigned to satHigh
$C$DW$67	.dwtag  DW_TAG_variable
	.dwattr $C$DW$67, DW_AT_name("satHigh")
	.dwattr $C$DW$67, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$67, DW_AT_location[DW_OP_reg10]

;* VB2   assigned to satLow
$C$DW$68	.dwtag  DW_TAG_variable
	.dwattr $C$DW$68, DW_AT_name("satLow")
	.dwattr $C$DW$68, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$68, DW_AT_location[DW_OP_regx 0x32]

;* A8    assigned to inScaleFactor
$C$DW$69	.dwtag  DW_TAG_variable
	.dwattr $C$DW$69, DW_AT_name("inScaleFactor")
	.dwattr $C$DW$69, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$69, DW_AT_location[DW_OP_reg8]

;* VB1   assigned to dataSize
$C$DW$70	.dwtag  DW_TAG_variable
	.dwattr $C$DW$70, DW_AT_name("dataSize")
	.dwattr $C$DW$70, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$70, DW_AT_location[DW_OP_regx 0x31]

;* D0    assigned to biasB
$C$DW$71	.dwtag  DW_TAG_variable
	.dwattr $C$DW$71, DW_AT_name("biasB")
	.dwattr $C$DW$71, DW_AT_type(*$C$DW$T$70)
	.dwattr $C$DW$71, DW_AT_location[DW_OP_regx 0x60]

;* A5    assigned to dstPtr
$C$DW$72	.dwtag  DW_TAG_variable
	.dwattr $C$DW$72, DW_AT_name("dstPtr")
	.dwattr $C$DW$72, DW_AT_type(*$C$DW$T$69)
	.dwattr $C$DW$72, DW_AT_location[DW_OP_reg5]

;* VB0   assigned to srcPtr
$C$DW$73	.dwtag  DW_TAG_variable
	.dwattr $C$DW$73, DW_AT_name("srcPtr")
	.dwattr $C$DW$73, DW_AT_type(*$C$DW$T$68)
	.dwattr $C$DW$73, DW_AT_location[DW_OP_regx 0x30]

;* AL0   assigned to dataFlowType
$C$DW$74	.dwtag  DW_TAG_variable
	.dwattr $C$DW$74, DW_AT_name("dataFlowType")
	.dwattr $C$DW$74, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$74, DW_AT_location[DW_OP_reg16]

;* A4    assigned to deviceName
$C$DW$75	.dwtag  DW_TAG_variable
	.dwattr $C$DW$75, DW_AT_name("deviceName")
	.dwattr $C$DW$75, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$75, DW_AT_location[DW_OP_reg4]

	.dwcfi	cfa_offset, 0
;          EXCLUSIVE CPU CYCLES: 8

           STD     .D1     A8,*SP(8)         ; [A_D1] 
||         STD     .D2X    A9,*SP++(-48)     ; [A_D2] 

	.dwcfi	save_reg_to_mem, 8, 8
	.dwcfi	cfa_offset, 48
	.dwcfi	save_reg_to_mem, 9, -48

           STD     .D1     A10,*SP(40)       ; [A_D1] 
||         MV      .L2X    A11,B2            ; [B_L2] |188| 
	.dwcfi	save_reg_to_mem, 10, 40

           STD     .D1     A12,*SP(24)       ; [A_D1] 
||         STD     .D2X    A11,*SP(32)       ; [A_D2] 
||         MV      .L1     A5,AL0            ; [A_L1] |188| 
||         MV      .S1     A8,D0             ; [A_S1] |188| 
	.dwcfi	save_reg_to_mem, 12, 24
	.dwcfi	save_reg_to_mem, 11, 32

           LDW     .D1     *SP(68),A12       ; [A_D1] |188| 
||         LDW     .D2     *SP(64),A11       ; [A_D2] |188| 
||         MV      .S1     A12,A10           ; [A_S1] |188| 
||         MV      .M1     A10,A8            ; [A_M1] |188| 
||         MV      .L2X    A6,B0             ; [B_L2] |188| 
||         CMPEQW  .L1     A4,0x2,AL1        ; [A_L1] |189| 

           ANDW    .L1     AL0,0xfffffffe,D2 ; [A_L1] |189| 
||         XORD    .S1     AL1,0x1,D1        ; [A_S1] |189| 
||         MV      .L2X    A9,B1             ; [B_L2] |188| 

	.dwpsn	file "src/tidl_device_functions.c",line 189,column 3,is_stmt,isa 0

           ORW     .D1     D2,D1,AL0         ; [A_D1] |189| 
||         MV      .L1X    B0,A4             ; [A_L1] |195| 

	.dwpsn	file "src/tidl_device_functions.c",line 188,column 1,is_stmt,isa 0

           CMPEQW  .L1     AL0,0,A0          ; [A_L1] |189| 
||         MV      .S1X    B1,A7             ; [A_S1] |195| 
||         MV      .D1     A7,A5             ; [A_D1] |188| 

	.dwpsn	file "src/tidl_device_functions.c",line 189,column 3,is_stmt,isa 0

   [ A0]   B       .B1     ||$C$L4||         ; [A_B] |189| 
||         MV      .L1X    B2,A9             ; [A_L1] |195| 
||         MVC     .S1     RP,A13            ; [A_S1] 
||         STD     .D1     A13,*SP(16)       ; [A_D1] 
||         MV      .D2     D0,A6             ; [A_D2] |195| 

	.dwcfi	save_reg_to_reg, 4101, 13
	.dwcfi	save_reg_to_mem, 13, 16
           ; BRANCHCC OCCURS {||$C$L4||}     ; [] |189| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "src/tidl_device_functions.c",line 195,column 5,is_stmt,isa 0
$C$DW$76	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$76, DW_AT_low_pc(0x00)
	.dwattr $C$DW$76, DW_AT_name("_Z20TIDL_conv2dBiasSplitIasEvPT0_PT_Piifiiii")
	.dwattr $C$DW$76, DW_AT_TI_call

           CALL    .B1     ||_Z20TIDL_conv2dBiasSplitIasEvPT0_PT_Piifiiii|| ; [A_B] |195| 
$C$RL0:    ; CALL OCCURS (||_Z20TIDL_conv2dBiasSplitIasEvPT0_PT_Piifiiii||) arg:{A4,A5,A6,A7,A8,A9,A10,A11,A12} ret:{}  ; [] |195| 
           B       .B1     ||$C$L5||         ; [A_B] |195| 
           ; BRANCH OCCURS {||$C$L5||}       ; [] |195| 
;** --------------------------------------------------------------------------*
||$C$L4||:    
;          EXCLUSIVE CPU CYCLES: 1
	.dwpsn	file "src/tidl_device_functions.c",line 191,column 5,is_stmt,isa 0
$C$DW$77	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$77, DW_AT_low_pc(0x00)
	.dwattr $C$DW$77, DW_AT_name("_Z23TIDL_AM_conv2dBiasSplitIisEvPT0_PT_Piifiiii")
	.dwattr $C$DW$77, DW_AT_TI_call

           CALL    .B1     ||_Z23TIDL_AM_conv2dBiasSplitIisEvPT0_PT_Piifiiii|| ; [A_B] |191| 
$C$RL1:    ; CALL OCCURS (||_Z23TIDL_AM_conv2dBiasSplitIisEvPT0_PT_Piifiiii||) arg:{A4,A5,A6,A7,A8,A9,A10,A11,A12} ret:{}  ; [] |191| 
;** --------------------------------------------------------------------------*
||$C$L5||:    
;          EXCLUSIVE CPU CYCLES: 9
           MVC     .S1     A13,RP            ; [A_S1] 
	.dwcfi	restore_reg, 4101

           LDD     .D1     *SP(32),A11       ; [A_D1] 
||         LDD     .D2     *SP(24),A12       ; [A_D2] 
	.dwcfi	restore_reg, 11
	.dwcfi	restore_reg, 12

           LDD     .D1     *SP(48),A9        ; [A_D1] 
||         LDD     .D2     *SP(40),A10       ; [A_D2] 
	.dwcfi	restore_reg, 9
	.dwcfi	restore_reg, 10

           LDD     .D1     *SP(16),A13       ; [A_D1] 
||         LDD     .D2     *SP(56),A8        ; [A_D2] 

	.dwcfi	restore_reg, 13
	.dwcfi	restore_reg, 8
$C$DW$78	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$78, DW_AT_low_pc(0x00)
	.dwattr $C$DW$78, DW_AT_TI_return


           RET     .B1     ; [A_B] 
||         ADDD    .D1     SP,0x30,SP        ; [A_D1] 

	.dwcfi	cfa_offset, 0
           ; RETURN OCCURS {RP}              ; [] 
	.dwattr $C$DW$53, DW_AT_TI_end_file("src/tidl_device_functions.c")
	.dwattr $C$DW$53, DW_AT_TI_end_line(0xc6)
	.dwattr $C$DW$53, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$53

;*****************************************************************************
;* UNDEFINED EXTERNAL REFERENCES                                             *
;*****************************************************************************
	.global	||_Z23TIDL_AM_conv2dBiasSplitIisEvPT0_PT_Piifiiii||
	.global	||_Z20TIDL_conv2dBiasSplitIasEvPT0_PT_Piifiiii||
;*****************************************************************************
;* SECTION GROUPS                                                            *
;*****************************************************************************
	.group    "_Z20TIDL_conv2dBiasSplitIasEviiPT0_PT_Piifiiii", 1
	.gmember  ".text:_Z20TIDL_conv2dBiasSplitIasEviiPT0_PT_Piifiiii"
	.endgroup

	.dwattr $C$DW$CU, DW_AT_language(DW_LANG_C_plus_plus)

;***************************************************************
;* DWARF CIE ENTRIES                                           *
;***************************************************************

$C$DW$CIE	.dwcie 4101
	.dwcfi	cfa_register, 111
	.dwcfi	cfa_offset, 0
	.dwcfi	same_value, 8
	.dwcfi	same_value, 9
	.dwcfi	same_value, 10
	.dwcfi	same_value, 11
	.dwcfi	same_value, 12
	.dwcfi	same_value, 13
	.dwcfi	same_value, 14
	.dwcfi	same_value, 15
	.dwcfi	same_value, 62
	.dwcfi	same_value, 63
	.dwendentry
	.dwendtag $C$DW$CU


;******************************************************************************
;* TYPE INFORMATION                                                           *
;******************************************************************************

$C$DW$TU$2	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$2
$C$DW$T$2	.dwtag  DW_TAG_unspecified_type
	.dwattr $C$DW$T$2, DW_AT_name("void")

	.dwendtag $C$DW$TU$2


$C$DW$TU$4	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$4
$C$DW$T$4	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$4, DW_AT_encoding(DW_ATE_boolean)
	.dwattr $C$DW$T$4, DW_AT_name("bool")
	.dwattr $C$DW$T$4, DW_AT_byte_size(0x01)

	.dwendtag $C$DW$TU$4


$C$DW$TU$5	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$5
$C$DW$T$5	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$5, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$5, DW_AT_name("signed char")
	.dwattr $C$DW$T$5, DW_AT_byte_size(0x01)

	.dwendtag $C$DW$TU$5


$C$DW$TU$69	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$69
$C$DW$T$69	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$69, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$T$69, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$69


$C$DW$TU$6	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$6
$C$DW$T$6	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$6, DW_AT_encoding(DW_ATE_unsigned_char)
	.dwattr $C$DW$T$6, DW_AT_name("unsigned char")
	.dwattr $C$DW$T$6, DW_AT_byte_size(0x01)

	.dwendtag $C$DW$TU$6


$C$DW$TU$7	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$7
$C$DW$T$7	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$7, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$7, DW_AT_name("wchar_t")
	.dwattr $C$DW$T$7, DW_AT_byte_size(0x04)

	.dwendtag $C$DW$TU$7


$C$DW$TU$8	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$8
$C$DW$T$8	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$8, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$8, DW_AT_name("short")
	.dwattr $C$DW$T$8, DW_AT_byte_size(0x02)

	.dwendtag $C$DW$TU$8


$C$DW$TU$68	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$68
$C$DW$T$68	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$68, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$T$68, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$68


$C$DW$TU$9	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$9
$C$DW$T$9	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$9, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$9, DW_AT_name("unsigned short")
	.dwattr $C$DW$T$9, DW_AT_byte_size(0x02)

	.dwendtag $C$DW$TU$9


$C$DW$TU$10	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$10
$C$DW$T$10	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$10, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$10, DW_AT_name("int")
	.dwattr $C$DW$T$10, DW_AT_byte_size(0x04)

	.dwendtag $C$DW$TU$10


$C$DW$TU$25	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$25
$C$DW$T$25	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$25, DW_AT_name("__int32_t")
	.dwattr $C$DW$T$25, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$T$25, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/machine/_types.h")
	.dwattr $C$DW$T$25, DW_AT_decl_line(0x64)
	.dwattr $C$DW$T$25, DW_AT_decl_column(0x0f)

	.dwendtag $C$DW$TU$25


$C$DW$TU$26	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$26
$C$DW$T$26	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$26, DW_AT_name("int32_t")
	.dwattr $C$DW$T$26, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$T$26, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/sys/_stdint.h")
	.dwattr $C$DW$T$26, DW_AT_decl_line(0x30)
	.dwattr $C$DW$T$26, DW_AT_decl_column(0x14)

	.dwendtag $C$DW$TU$26


$C$DW$TU$40	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$40

$C$DW$T$40	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$40, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$T$40, DW_AT_byte_size(0x0c)
$C$DW$79	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$79, DW_AT_upper_bound(0x02)

	.dwendtag $C$DW$T$40

	.dwendtag $C$DW$TU$40


$C$DW$TU$57	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$57

$C$DW$T$57	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$57, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$T$57, DW_AT_byte_size(0x24)
$C$DW$80	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$80, DW_AT_upper_bound(0x02)

$C$DW$81	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$81, DW_AT_upper_bound(0x02)

	.dwendtag $C$DW$T$57

	.dwendtag $C$DW$TU$57


$C$DW$TU$58	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$58

$C$DW$T$58	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$58, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$T$58, DW_AT_byte_size(0x18)
$C$DW$82	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$82, DW_AT_upper_bound(0x01)

$C$DW$83	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$83, DW_AT_upper_bound(0x02)

	.dwendtag $C$DW$T$58

	.dwendtag $C$DW$TU$58


$C$DW$TU$41	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$41

$C$DW$T$41	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$41, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$T$41, DW_AT_byte_size(0xfa0)
$C$DW$84	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$84, DW_AT_upper_bound(0x3e7)

	.dwendtag $C$DW$T$41

	.dwendtag $C$DW$TU$41


$C$DW$TU$70	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$70
$C$DW$T$70	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$70, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$T$70, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$70


$C$DW$TU$11	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$11
$C$DW$T$11	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$11, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$11, DW_AT_name("unsigned int")
	.dwattr $C$DW$T$11, DW_AT_byte_size(0x04)

	.dwendtag $C$DW$TU$11


$C$DW$TU$12	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$12
$C$DW$T$12	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$12, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$12, DW_AT_name("__int40_t")
	.dwattr $C$DW$T$12, DW_AT_byte_size(0x08)

	.dwendtag $C$DW$TU$12


$C$DW$TU$13	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$13
$C$DW$T$13	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$13, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$13, DW_AT_name("unsigned __int40_t")
	.dwattr $C$DW$T$13, DW_AT_byte_size(0x08)

	.dwendtag $C$DW$TU$13


$C$DW$TU$14	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$14
$C$DW$T$14	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$14, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$14, DW_AT_name("long long")
	.dwattr $C$DW$T$14, DW_AT_byte_size(0x08)

	.dwendtag $C$DW$TU$14


$C$DW$TU$15	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$15
$C$DW$T$15	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$15, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$15, DW_AT_name("unsigned long long")
	.dwattr $C$DW$T$15, DW_AT_byte_size(0x08)

	.dwendtag $C$DW$TU$15


$C$DW$TU$16	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$16
$C$DW$T$16	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$16, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$16, DW_AT_name("float")
	.dwattr $C$DW$T$16, DW_AT_byte_size(0x04)

	.dwendtag $C$DW$TU$16


$C$DW$TU$17	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$17
$C$DW$T$17	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$17, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$17, DW_AT_name("double")
	.dwattr $C$DW$T$17, DW_AT_byte_size(0x08)

	.dwendtag $C$DW$TU$17


$C$DW$TU$55	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$55

$C$DW$T$55	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$55, DW_AT_type(*$C$DW$T$17)
	.dwattr $C$DW$T$55, DW_AT_byte_size(0x10)
$C$DW$85	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$85, DW_AT_upper_bound(0x01)

	.dwendtag $C$DW$T$55

	.dwendtag $C$DW$TU$55


$C$DW$TU$56	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$56

$C$DW$T$56	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$56, DW_AT_type(*$C$DW$T$17)
	.dwattr $C$DW$T$56, DW_AT_byte_size(0x30)
$C$DW$86	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$86, DW_AT_upper_bound(0x02)

$C$DW$87	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$87, DW_AT_upper_bound(0x01)

	.dwendtag $C$DW$T$56

	.dwendtag $C$DW$TU$56


$C$DW$TU$18	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$18
$C$DW$T$18	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$18, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$18, DW_AT_name("long double")
	.dwattr $C$DW$T$18, DW_AT_byte_size(0x08)

	.dwendtag $C$DW$TU$18


$C$DW$TU$19	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$19
$C$DW$T$19	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$19, DW_AT_encoding(DW_ATE_TI_complex_signed)
	.dwattr $C$DW$T$19, DW_AT_name("cchar")
	.dwattr $C$DW$T$19, DW_AT_byte_size(0x02)

	.dwendtag $C$DW$TU$19


$C$DW$TU$20	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$20
$C$DW$T$20	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$20, DW_AT_encoding(DW_ATE_TI_complex_signed)
	.dwattr $C$DW$T$20, DW_AT_name("cshort")
	.dwattr $C$DW$T$20, DW_AT_byte_size(0x04)

	.dwendtag $C$DW$TU$20


$C$DW$TU$21	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$21
$C$DW$T$21	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$21, DW_AT_encoding(DW_ATE_TI_complex_signed)
	.dwattr $C$DW$T$21, DW_AT_name("cint")
	.dwattr $C$DW$T$21, DW_AT_byte_size(0x08)

	.dwendtag $C$DW$TU$21


$C$DW$TU$22	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$22
$C$DW$T$22	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$22, DW_AT_encoding(DW_ATE_TI_complex_signed)
	.dwattr $C$DW$T$22, DW_AT_name("clong")
	.dwattr $C$DW$T$22, DW_AT_byte_size(0x10)

	.dwendtag $C$DW$TU$22


$C$DW$TU$23	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$23
$C$DW$T$23	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$23, DW_AT_encoding(DW_ATE_TI_complex_float)
	.dwattr $C$DW$T$23, DW_AT_name("cfloat")
	.dwattr $C$DW$T$23, DW_AT_byte_size(0x08)

	.dwendtag $C$DW$TU$23


$C$DW$TU$24	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$24
$C$DW$T$24	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$24, DW_AT_encoding(DW_ATE_TI_complex_float)
	.dwattr $C$DW$T$24, DW_AT_name("cdouble")
	.dwattr $C$DW$T$24, DW_AT_byte_size(0x10)

	.dwendtag $C$DW$TU$24


$C$DW$TU$32	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$32

$C$DW$T$32	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$32, DW_AT_name("sBufferInfo_t")
	.dwattr $C$DW$T$32, DW_AT_byte_size(0x3c)
$C$DW$88	.dwtag  DW_TAG_member
	.dwattr $C$DW$88, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$88, DW_AT_name("accessor")
	.dwattr $C$DW$88, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$88, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$88, DW_AT_decl_line(0xd8)
	.dwattr $C$DW$88, DW_AT_decl_column(0x0b)

$C$DW$89	.dwtag  DW_TAG_member
	.dwattr $C$DW$89, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$89, DW_AT_name("space")
	.dwattr $C$DW$89, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$89, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$89, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$89, DW_AT_decl_line(0xd9)
	.dwattr $C$DW$89, DW_AT_decl_column(0x0b)

$C$DW$90	.dwtag  DW_TAG_member
	.dwattr $C$DW$90, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$90, DW_AT_name("bufWidth")
	.dwattr $C$DW$90, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$90, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$90, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$90, DW_AT_decl_line(0xda)
	.dwattr $C$DW$90, DW_AT_decl_column(0x0b)

$C$DW$91	.dwtag  DW_TAG_member
	.dwattr $C$DW$91, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$91, DW_AT_name("bufHeight")
	.dwattr $C$DW$91, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$91, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$91, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$91, DW_AT_decl_line(0xdb)
	.dwattr $C$DW$91, DW_AT_decl_column(0x0b)

$C$DW$92	.dwtag  DW_TAG_member
	.dwattr $C$DW$92, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$92, DW_AT_name("bufSize")
	.dwattr $C$DW$92, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$92, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$92, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$92, DW_AT_decl_line(0xdc)
	.dwattr $C$DW$92, DW_AT_decl_column(0x0b)

$C$DW$93	.dwtag  DW_TAG_member
	.dwattr $C$DW$93, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$93, DW_AT_name("activeBufWidth")
	.dwattr $C$DW$93, DW_AT_data_member_location[DW_OP_plus_uconst 0x14]
	.dwattr $C$DW$93, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$93, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$93, DW_AT_decl_line(0xdd)
	.dwattr $C$DW$93, DW_AT_decl_column(0x0b)

$C$DW$94	.dwtag  DW_TAG_member
	.dwattr $C$DW$94, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$94, DW_AT_name("baseMem")
	.dwattr $C$DW$94, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$94, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$94, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$94, DW_AT_decl_line(0xde)
	.dwattr $C$DW$94, DW_AT_decl_column(0x0b)

$C$DW$95	.dwtag  DW_TAG_member
	.dwattr $C$DW$95, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$95, DW_AT_name("accessoffset")
	.dwattr $C$DW$95, DW_AT_data_member_location[DW_OP_plus_uconst 0x1c]
	.dwattr $C$DW$95, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$95, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$95, DW_AT_decl_line(0xdf)
	.dwattr $C$DW$95, DW_AT_decl_column(0x0b)

$C$DW$96	.dwtag  DW_TAG_member
	.dwattr $C$DW$96, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$96, DW_AT_name("padC")
	.dwattr $C$DW$96, DW_AT_data_member_location[DW_OP_plus_uconst 0x20]
	.dwattr $C$DW$96, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$96, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$96, DW_AT_decl_line(0xe0)
	.dwattr $C$DW$96, DW_AT_decl_column(0x0b)

$C$DW$97	.dwtag  DW_TAG_member
	.dwattr $C$DW$97, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$97, DW_AT_name("padR")
	.dwattr $C$DW$97, DW_AT_data_member_location[DW_OP_plus_uconst 0x24]
	.dwattr $C$DW$97, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$97, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$97, DW_AT_decl_line(0xe1)
	.dwattr $C$DW$97, DW_AT_decl_column(0x0b)

$C$DW$98	.dwtag  DW_TAG_member
	.dwattr $C$DW$98, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$98, DW_AT_name("padCZeros")
	.dwattr $C$DW$98, DW_AT_data_member_location[DW_OP_plus_uconst 0x28]
	.dwattr $C$DW$98, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$98, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$98, DW_AT_decl_line(0xe2)
	.dwattr $C$DW$98, DW_AT_decl_column(0x0b)

$C$DW$99	.dwtag  DW_TAG_member
	.dwattr $C$DW$99, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$99, DW_AT_name("padRZeros")
	.dwattr $C$DW$99, DW_AT_data_member_location[DW_OP_plus_uconst 0x2c]
	.dwattr $C$DW$99, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$99, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$99, DW_AT_decl_line(0xe3)
	.dwattr $C$DW$99, DW_AT_decl_column(0x0b)

$C$DW$100	.dwtag  DW_TAG_member
	.dwattr $C$DW$100, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$100, DW_AT_name("padCFillZeros")
	.dwattr $C$DW$100, DW_AT_data_member_location[DW_OP_plus_uconst 0x30]
	.dwattr $C$DW$100, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$100, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$100, DW_AT_decl_line(0xe4)
	.dwattr $C$DW$100, DW_AT_decl_column(0x0b)

$C$DW$101	.dwtag  DW_TAG_member
	.dwattr $C$DW$101, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$101, DW_AT_name("padRFillZeros")
	.dwattr $C$DW$101, DW_AT_data_member_location[DW_OP_plus_uconst 0x34]
	.dwattr $C$DW$101, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$101, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$101, DW_AT_decl_line(0xe5)
	.dwattr $C$DW$101, DW_AT_decl_column(0x0b)

$C$DW$102	.dwtag  DW_TAG_member
	.dwattr $C$DW$102, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$102, DW_AT_name("padCReq")
	.dwattr $C$DW$102, DW_AT_data_member_location[DW_OP_plus_uconst 0x38]
	.dwattr $C$DW$102, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$102, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$102, DW_AT_decl_line(0xe6)
	.dwattr $C$DW$102, DW_AT_decl_column(0x0b)


$C$DW$103	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$103, DW_AT_name("operator =")
	.dwattr $C$DW$103, DW_AT_declaration
	.dwattr $C$DW$103, DW_AT_linkage_name("_ZN13sBufferInfo_taSERKS_")
	.dwattr $C$DW$103, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$103, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$104	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$104, DW_AT_type(*$C$DW$T$29)

	.dwendtag $C$DW$103


$C$DW$105	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$105, DW_AT_name("operator =")
	.dwattr $C$DW$105, DW_AT_declaration
	.dwattr $C$DW$105, DW_AT_linkage_name("_ZN13sBufferInfo_taSEOS_")
	.dwattr $C$DW$105, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$105, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$106	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$106, DW_AT_type(*$C$DW$T$27)

	.dwendtag $C$DW$105

	.dwattr $C$DW$T$32, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$T$32, DW_AT_decl_line(0xd7)
	.dwattr $C$DW$T$32, DW_AT_decl_column(0x01)
	.dwendtag $C$DW$T$32

	.dwendtag $C$DW$TU$32


$C$DW$TU$27	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$27
$C$DW$T$27	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$27, DW_AT_type(*$C$DW$T$32)
	.dwattr $C$DW$T$27, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$27


$C$DW$TU$30	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$30

$C$DW$T$30	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$30, DW_AT_type(*$C$DW$T$27)
$C$DW$107	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$107, DW_AT_type(*$C$DW$T$29)

	.dwendtag $C$DW$T$30

	.dwendtag $C$DW$TU$30


$C$DW$TU$31	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$31

$C$DW$T$31	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$31, DW_AT_type(*$C$DW$T$27)
$C$DW$108	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$108, DW_AT_type(*$C$DW$T$27)

	.dwendtag $C$DW$T$31

	.dwendtag $C$DW$TU$31


$C$DW$TU$28	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$28
$C$DW$T$28	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$28, DW_AT_type(*$C$DW$T$32)

	.dwendtag $C$DW$TU$28


$C$DW$TU$29	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$29
$C$DW$T$29	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$29, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$T$29, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$29


$C$DW$TU$36	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$36
$C$DW$T$36	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$36, DW_AT_name("sBufferInfo_t")
	.dwattr $C$DW$T$36, DW_AT_type(*$C$DW$T$32)
	.dwattr $C$DW$T$36, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$T$36, DW_AT_decl_line(0xe7)
	.dwattr $C$DW$T$36, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$36


$C$DW$TU$37	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$37

$C$DW$T$37	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$37, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$T$37, DW_AT_byte_size(0x78)
$C$DW$109	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$109, DW_AT_upper_bound(0x01)

	.dwendtag $C$DW$T$37

	.dwendtag $C$DW$TU$37


$C$DW$TU$38	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$38

$C$DW$T$38	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$38, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$T$38, DW_AT_byte_size(0x168)
$C$DW$110	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$110, DW_AT_upper_bound(0x02)

$C$DW$111	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$111, DW_AT_upper_bound(0x01)

	.dwendtag $C$DW$T$38

	.dwendtag $C$DW$TU$38


$C$DW$TU$47	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$47

$C$DW$T$47	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$47, DW_AT_name("sDataFlowInfo_t")
	.dwattr $C$DW$T$47, DW_AT_byte_size(0x12a8)
$C$DW$112	.dwtag  DW_TAG_member
	.dwattr $C$DW$112, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$112, DW_AT_name("dataFlowType")
	.dwattr $C$DW$112, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$112, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$112, DW_AT_decl_line(0x10f)
	.dwattr $C$DW$112, DW_AT_decl_column(0x0b)

$C$DW$113	.dwtag  DW_TAG_member
	.dwattr $C$DW$113, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$113, DW_AT_name("numSplit")
	.dwattr $C$DW$113, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$113, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$113, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$113, DW_AT_decl_line(0x110)
	.dwattr $C$DW$113, DW_AT_decl_column(0x0b)

$C$DW$114	.dwtag  DW_TAG_member
	.dwattr $C$DW$114, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$114, DW_AT_name("procSize")
	.dwattr $C$DW$114, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$114, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$114, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$114, DW_AT_decl_line(0x111)
	.dwattr $C$DW$114, DW_AT_decl_column(0x0b)

$C$DW$115	.dwtag  DW_TAG_member
	.dwattr $C$DW$115, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$115, DW_AT_name("Nci")
	.dwattr $C$DW$115, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$115, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$115, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$115, DW_AT_decl_line(0x112)
	.dwattr $C$DW$115, DW_AT_decl_column(0x0b)

$C$DW$116	.dwtag  DW_TAG_member
	.dwattr $C$DW$116, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$116, DW_AT_name("Nco")
	.dwattr $C$DW$116, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$116, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$116, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$116, DW_AT_decl_line(0x113)
	.dwattr $C$DW$116, DW_AT_decl_column(0x0b)

$C$DW$117	.dwtag  DW_TAG_member
	.dwattr $C$DW$117, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$117, DW_AT_name("kernelFreq")
	.dwattr $C$DW$117, DW_AT_data_member_location[DW_OP_plus_uconst 0x14]
	.dwattr $C$DW$117, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$117, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$117, DW_AT_decl_line(0x114)
	.dwattr $C$DW$117, DW_AT_decl_column(0x0b)

$C$DW$118	.dwtag  DW_TAG_member
	.dwattr $C$DW$118, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$118, DW_AT_name("dmaFreq")
	.dwattr $C$DW$118, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$118, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$118, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$118, DW_AT_decl_line(0x115)
	.dwattr $C$DW$118, DW_AT_decl_column(0x0b)

$C$DW$119	.dwtag  DW_TAG_member
	.dwattr $C$DW$119, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$119, DW_AT_name("dmaFreqWt")
	.dwattr $C$DW$119, DW_AT_data_member_location[DW_OP_plus_uconst 0x1c]
	.dwattr $C$DW$119, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$119, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$119, DW_AT_decl_line(0x116)
	.dwattr $C$DW$119, DW_AT_decl_column(0x0b)

$C$DW$120	.dwtag  DW_TAG_member
	.dwattr $C$DW$120, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$120, DW_AT_name("preferenceOrder")
	.dwattr $C$DW$120, DW_AT_data_member_location[DW_OP_plus_uconst 0x20]
	.dwattr $C$DW$120, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$120, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$120, DW_AT_decl_line(0x117)
	.dwattr $C$DW$120, DW_AT_decl_column(0x0b)

$C$DW$121	.dwtag  DW_TAG_member
	.dwattr $C$DW$121, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$121, DW_AT_name("preFetchAligned")
	.dwattr $C$DW$121, DW_AT_data_member_location[DW_OP_plus_uconst 0x24]
	.dwattr $C$DW$121, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$121, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$121, DW_AT_decl_line(0x118)
	.dwattr $C$DW$121, DW_AT_decl_column(0x0b)

$C$DW$122	.dwtag  DW_TAG_member
	.dwattr $C$DW$122, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$122, DW_AT_name("preFetch")
	.dwattr $C$DW$122, DW_AT_data_member_location[DW_OP_plus_uconst 0x28]
	.dwattr $C$DW$122, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$122, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$122, DW_AT_decl_line(0x119)
	.dwattr $C$DW$122, DW_AT_decl_column(0x0b)

$C$DW$123	.dwtag  DW_TAG_member
	.dwattr $C$DW$123, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$123, DW_AT_name("requiredInPlaneSize")
	.dwattr $C$DW$123, DW_AT_data_member_location[DW_OP_plus_uconst 0x2c]
	.dwattr $C$DW$123, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$123, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$123, DW_AT_decl_line(0x11a)
	.dwattr $C$DW$123, DW_AT_decl_column(0x0b)

$C$DW$124	.dwtag  DW_TAG_member
	.dwattr $C$DW$124, DW_AT_type(*$C$DW$T$38)
	.dwattr $C$DW$124, DW_AT_name("bufInfo")
	.dwattr $C$DW$124, DW_AT_data_member_location[DW_OP_plus_uconst 0x30]
	.dwattr $C$DW$124, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$124, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$124, DW_AT_decl_line(0x11b)
	.dwattr $C$DW$124, DW_AT_decl_column(0x12)

$C$DW$125	.dwtag  DW_TAG_member
	.dwattr $C$DW$125, DW_AT_type(*$C$DW$T$37)
	.dwattr $C$DW$125, DW_AT_name("wtOneShot")
	.dwattr $C$DW$125, DW_AT_data_member_location[DW_OP_plus_uconst 0x198]
	.dwattr $C$DW$125, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$125, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$125, DW_AT_decl_line(0x11c)
	.dwattr $C$DW$125, DW_AT_decl_column(0x12)

$C$DW$126	.dwtag  DW_TAG_member
	.dwattr $C$DW$126, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$126, DW_AT_name("privContextMemBuff")
	.dwattr $C$DW$126, DW_AT_data_member_location[DW_OP_plus_uconst 0x210]
	.dwattr $C$DW$126, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$126, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$126, DW_AT_decl_line(0x11d)
	.dwattr $C$DW$126, DW_AT_decl_column(0x11)

$C$DW$127	.dwtag  DW_TAG_member
	.dwattr $C$DW$127, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$127, DW_AT_name("memStats")
	.dwattr $C$DW$127, DW_AT_data_member_location[DW_OP_plus_uconst 0x250]
	.dwattr $C$DW$127, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$127, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$127, DW_AT_decl_line(0x120)
	.dwattr $C$DW$127, DW_AT_decl_column(0x12)

$C$DW$128	.dwtag  DW_TAG_member
	.dwattr $C$DW$128, DW_AT_type(*$C$DW$T$40)
	.dwattr $C$DW$128, DW_AT_name("bufWidth")
	.dwattr $C$DW$128, DW_AT_data_member_location[DW_OP_plus_uconst 0x2d8]
	.dwattr $C$DW$128, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$128, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$128, DW_AT_decl_line(0x121)
	.dwattr $C$DW$128, DW_AT_decl_column(0x0b)

$C$DW$129	.dwtag  DW_TAG_member
	.dwattr $C$DW$129, DW_AT_type(*$C$DW$T$40)
	.dwattr $C$DW$129, DW_AT_name("bufHeight")
	.dwattr $C$DW$129, DW_AT_data_member_location[DW_OP_plus_uconst 0x2e4]
	.dwattr $C$DW$129, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$129, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$129, DW_AT_decl_line(0x122)
	.dwattr $C$DW$129, DW_AT_decl_column(0x0b)

$C$DW$130	.dwtag  DW_TAG_member
	.dwattr $C$DW$130, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$130, DW_AT_name("bufState")
	.dwattr $C$DW$130, DW_AT_data_member_location[DW_OP_plus_uconst 0x2f0]
	.dwattr $C$DW$130, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$130, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$130, DW_AT_decl_line(0x126)
	.dwattr $C$DW$130, DW_AT_decl_column(0x0b)

$C$DW$131	.dwtag  DW_TAG_member
	.dwattr $C$DW$131, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$131, DW_AT_name("forceOutDDR")
	.dwattr $C$DW$131, DW_AT_data_member_location[DW_OP_plus_uconst 0x2f4]
	.dwattr $C$DW$131, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$131, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$131, DW_AT_decl_line(0x12a)
	.dwattr $C$DW$131, DW_AT_decl_column(0x0b)

$C$DW$132	.dwtag  DW_TAG_member
	.dwattr $C$DW$132, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$132, DW_AT_name("forceOutStgDDR")
	.dwattr $C$DW$132, DW_AT_data_member_location[DW_OP_plus_uconst 0x2f8]
	.dwattr $C$DW$132, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$132, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$132, DW_AT_decl_line(0x12b)
	.dwattr $C$DW$132, DW_AT_decl_column(0x0b)

$C$DW$133	.dwtag  DW_TAG_member
	.dwattr $C$DW$133, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$133, DW_AT_name("forceWtStgDDR")
	.dwattr $C$DW$133, DW_AT_data_member_location[DW_OP_plus_uconst 0x2fc]
	.dwattr $C$DW$133, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$133, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$133, DW_AT_decl_line(0x12c)
	.dwattr $C$DW$133, DW_AT_decl_column(0x0b)

$C$DW$134	.dwtag  DW_TAG_member
	.dwattr $C$DW$134, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$134, DW_AT_name("forceContextMemDDR")
	.dwattr $C$DW$134, DW_AT_data_member_location[DW_OP_plus_uconst 0x300]
	.dwattr $C$DW$134, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$134, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$134, DW_AT_decl_line(0x12d)
	.dwattr $C$DW$134, DW_AT_decl_column(0x0b)

$C$DW$135	.dwtag  DW_TAG_member
	.dwattr $C$DW$135, DW_AT_type(*$C$DW$T$41)
	.dwattr $C$DW$135, DW_AT_name("reservedSpace")
	.dwattr $C$DW$135, DW_AT_data_member_location[DW_OP_plus_uconst 0x304]
	.dwattr $C$DW$135, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$135, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$135, DW_AT_decl_line(0x12e)
	.dwattr $C$DW$135, DW_AT_decl_column(0x0b)


$C$DW$136	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$136, DW_AT_name("operator =")
	.dwattr $C$DW$136, DW_AT_declaration
	.dwattr $C$DW$136, DW_AT_linkage_name("_ZN15sDataFlowInfo_taSERKS_")
	.dwattr $C$DW$136, DW_AT_type(*$C$DW$T$42)
	.dwattr $C$DW$136, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$137	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$137, DW_AT_type(*$C$DW$T$44)

	.dwendtag $C$DW$136


$C$DW$138	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$138, DW_AT_name("operator =")
	.dwattr $C$DW$138, DW_AT_declaration
	.dwattr $C$DW$138, DW_AT_linkage_name("_ZN15sDataFlowInfo_taSEOS_")
	.dwattr $C$DW$138, DW_AT_type(*$C$DW$T$42)
	.dwattr $C$DW$138, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$139	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$139, DW_AT_type(*$C$DW$T$42)

	.dwendtag $C$DW$138

	.dwattr $C$DW$T$47, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$T$47, DW_AT_decl_line(0x10e)
	.dwattr $C$DW$T$47, DW_AT_decl_column(0x01)
	.dwendtag $C$DW$T$47

	.dwendtag $C$DW$TU$47


$C$DW$TU$42	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$42
$C$DW$T$42	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$42, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$T$42, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$42


$C$DW$TU$45	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$45

$C$DW$T$45	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$45, DW_AT_type(*$C$DW$T$42)
$C$DW$140	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$140, DW_AT_type(*$C$DW$T$44)

	.dwendtag $C$DW$T$45

	.dwendtag $C$DW$TU$45


$C$DW$TU$46	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$46

$C$DW$T$46	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$46, DW_AT_type(*$C$DW$T$42)
$C$DW$141	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$141, DW_AT_type(*$C$DW$T$42)

	.dwendtag $C$DW$T$46

	.dwendtag $C$DW$TU$46


$C$DW$TU$43	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$43
$C$DW$T$43	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$43, DW_AT_type(*$C$DW$T$47)

	.dwendtag $C$DW$TU$43


$C$DW$TU$44	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$44
$C$DW$T$44	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$44, DW_AT_type(*$C$DW$T$43)
	.dwattr $C$DW$T$44, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$44


$C$DW$TU$93	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$93
$C$DW$T$93	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$93, DW_AT_name("sDataFlowInfo_t")
	.dwattr $C$DW$T$93, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$T$93, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$T$93, DW_AT_decl_line(0x12f)
	.dwattr $C$DW$T$93, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$93


$C$DW$TU$94	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$94
$C$DW$T$94	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$94, DW_AT_type(*$C$DW$T$93)

	.dwendtag $C$DW$TU$94


$C$DW$TU$95	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$95
$C$DW$T$95	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$95, DW_AT_type(*$C$DW$T$94)
	.dwattr $C$DW$T$95, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$95


$C$DW$TU$64	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$64

$C$DW$T$64	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$64, DW_AT_name("sMemoryStats_t")
	.dwattr $C$DW$T$64, DW_AT_byte_size(0x88)
$C$DW$142	.dwtag  DW_TAG_member
	.dwattr $C$DW$142, DW_AT_type(*$C$DW$T$56)
	.dwattr $C$DW$142, DW_AT_name("bandwidth")
	.dwattr $C$DW$142, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$142, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$142, DW_AT_decl_line(0x9e)
	.dwattr $C$DW$142, DW_AT_decl_column(0x0a)

$C$DW$143	.dwtag  DW_TAG_member
	.dwattr $C$DW$143, DW_AT_type(*$C$DW$T$57)
	.dwattr $C$DW$143, DW_AT_name("size")
	.dwattr $C$DW$143, DW_AT_data_member_location[DW_OP_plus_uconst 0x30]
	.dwattr $C$DW$143, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$143, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$143, DW_AT_decl_line(0x9f)
	.dwattr $C$DW$143, DW_AT_decl_column(0x0b)

$C$DW$144	.dwtag  DW_TAG_member
	.dwattr $C$DW$144, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$144, DW_AT_name("outSizeWithCoexc")
	.dwattr $C$DW$144, DW_AT_data_member_location[DW_OP_plus_uconst 0x54]
	.dwattr $C$DW$144, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$144, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$144, DW_AT_decl_line(0xa0)
	.dwattr $C$DW$144, DW_AT_decl_column(0x0b)

$C$DW$145	.dwtag  DW_TAG_member
	.dwattr $C$DW$145, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$145, DW_AT_name("outHeightWithCoexc")
	.dwattr $C$DW$145, DW_AT_data_member_location[DW_OP_plus_uconst 0x58]
	.dwattr $C$DW$145, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$145, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$145, DW_AT_decl_line(0xa1)
	.dwattr $C$DW$145, DW_AT_decl_column(0x0b)

$C$DW$146	.dwtag  DW_TAG_member
	.dwattr $C$DW$146, DW_AT_type(*$C$DW$T$58)
	.dwattr $C$DW$146, DW_AT_name("memSpace")
	.dwattr $C$DW$146, DW_AT_data_member_location[DW_OP_plus_uconst 0x5c]
	.dwattr $C$DW$146, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$146, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$146, DW_AT_decl_line(0xa2)
	.dwattr $C$DW$146, DW_AT_decl_column(0x0b)

$C$DW$147	.dwtag  DW_TAG_member
	.dwattr $C$DW$147, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$147, DW_AT_name("inFeatMapReadFactor")
	.dwattr $C$DW$147, DW_AT_data_member_location[DW_OP_plus_uconst 0x74]
	.dwattr $C$DW$147, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$147, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$147, DW_AT_decl_line(0xa3)
	.dwattr $C$DW$147, DW_AT_decl_column(0x0b)

$C$DW$148	.dwtag  DW_TAG_member
	.dwattr $C$DW$148, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$148, DW_AT_name("wtFeatMapReadFactor")
	.dwattr $C$DW$148, DW_AT_data_member_location[DW_OP_plus_uconst 0x78]
	.dwattr $C$DW$148, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$148, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$148, DW_AT_decl_line(0xa4)
	.dwattr $C$DW$148, DW_AT_decl_column(0x0b)

$C$DW$149	.dwtag  DW_TAG_member
	.dwattr $C$DW$149, DW_AT_type(*$C$DW$T$17)
	.dwattr $C$DW$149, DW_AT_name("wtVolumeFetchKB")
	.dwattr $C$DW$149, DW_AT_data_member_location[DW_OP_plus_uconst 0x80]
	.dwattr $C$DW$149, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$149, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$149, DW_AT_decl_line(0xa5)
	.dwattr $C$DW$149, DW_AT_decl_column(0x0a)


$C$DW$150	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$150, DW_AT_name("operator =")
	.dwattr $C$DW$150, DW_AT_declaration
	.dwattr $C$DW$150, DW_AT_linkage_name("_ZN14sMemoryStats_taSERKS_")
	.dwattr $C$DW$150, DW_AT_type(*$C$DW$T$59)
	.dwattr $C$DW$150, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$151	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$151, DW_AT_type(*$C$DW$T$61)

	.dwendtag $C$DW$150


$C$DW$152	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$152, DW_AT_name("operator =")
	.dwattr $C$DW$152, DW_AT_declaration
	.dwattr $C$DW$152, DW_AT_linkage_name("_ZN14sMemoryStats_taSEOS_")
	.dwattr $C$DW$152, DW_AT_type(*$C$DW$T$59)
	.dwattr $C$DW$152, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$153	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$153, DW_AT_type(*$C$DW$T$59)

	.dwendtag $C$DW$152

	.dwattr $C$DW$T$64, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$T$64, DW_AT_decl_line(0x9c)
	.dwattr $C$DW$T$64, DW_AT_decl_column(0x01)
	.dwendtag $C$DW$T$64

	.dwendtag $C$DW$TU$64


$C$DW$TU$59	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$59
$C$DW$T$59	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$59, DW_AT_type(*$C$DW$T$64)
	.dwattr $C$DW$T$59, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$59


$C$DW$TU$62	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$62

$C$DW$T$62	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$62, DW_AT_type(*$C$DW$T$59)
$C$DW$154	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$154, DW_AT_type(*$C$DW$T$61)

	.dwendtag $C$DW$T$62

	.dwendtag $C$DW$TU$62


$C$DW$TU$63	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$63

$C$DW$T$63	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$63, DW_AT_type(*$C$DW$T$59)
$C$DW$155	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$155, DW_AT_type(*$C$DW$T$59)

	.dwendtag $C$DW$T$63

	.dwendtag $C$DW$TU$63


$C$DW$TU$39	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$39
$C$DW$T$39	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$39, DW_AT_name("sMemoryStats_t")
	.dwattr $C$DW$T$39, DW_AT_type(*$C$DW$T$64)
	.dwattr $C$DW$T$39, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$T$39, DW_AT_decl_line(0xa6)
	.dwattr $C$DW$T$39, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$39


$C$DW$TU$60	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$60
$C$DW$T$60	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$60, DW_AT_type(*$C$DW$T$64)

	.dwendtag $C$DW$TU$60


$C$DW$TU$61	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$61
$C$DW$T$61	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$61, DW_AT_type(*$C$DW$T$60)
	.dwattr $C$DW$T$61, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$61

