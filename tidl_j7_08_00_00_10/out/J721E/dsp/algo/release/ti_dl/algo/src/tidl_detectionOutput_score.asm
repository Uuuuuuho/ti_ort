;******************************************************************************
;* C7x G3 C/C++ Codegen                                            Unix v1.4.2.LTS *
;* Date/Time created: Tue Oct  5 15:12:43 2021                                *
;******************************************************************************
	.compiler_opts --abi=eabi --endian=little --hll_source=on --object_format=elf --silicon_errata_i2117 --silicon_version=7100 --symdebug:dwarf --symdebug:dwarf_version=4 

;******************************************************************************
;* GLOBAL FILE PARAMETERS                                                     *
;*                                                                            *
;*   Architecture      : C7100                                                *
;*   Endian            : Little                                               *
;*   Pipelining        : Enabled                                              *
;*   Debug Info        : DWARF Debug                                          *
;*                                                                            *
;******************************************************************************

	.asg	D14, FP
	.asg	D15, SP
	.global	$bss


$C$DW$CU	.dwtag  DW_TAG_compile_unit
	.dwattr $C$DW$CU, DW_AT_name("src/tidl_detectionOutput_score.c")
	.dwattr $C$DW$CU, DW_AT_producer("TI C7x G3 C/C++ Codegen Unix v1.4.2.LTS Copyright (c) 2014-2019 Texas Instruments Incorporated")
	.dwattr $C$DW$CU, DW_AT_TI_version(0x01)
	.dwattr $C$DW$CU, DW_AT_comp_dir("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/tidl_j7_08_00_00_10/ti_dl/algo")
$C$DW$1	.dwtag  DW_TAG_variable
	.dwattr $C$DW$1, DW_AT_name("Typeinfo for signed char")
	.dwattr $C$DW$1, DW_AT_linkage_name("_ZTIa")
	.dwattr $C$DW$1, DW_AT_type(*$C$DW$T$162)
	.dwattr $C$DW$1, DW_AT_declaration
	.dwattr $C$DW$1, DW_AT_external
	.dwattr $C$DW$1, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/libcxx/typeinfo")
	.dwattr $C$DW$1, DW_AT_decl_line(0xd0)
	.dwattr $C$DW$1, DW_AT_decl_column(0x0a)

$C$DW$21	.dwtag  DW_TAG_variable
	.dwattr $C$DW$21, DW_AT_name("Typeinfo for float")
	.dwattr $C$DW$21, DW_AT_linkage_name("_ZTIf")
	.dwattr $C$DW$21, DW_AT_type(*$C$DW$T$162)
	.dwattr $C$DW$21, DW_AT_declaration
	.dwattr $C$DW$21, DW_AT_external
	.dwattr $C$DW$21, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/libcxx/typeinfo")
	.dwattr $C$DW$21, DW_AT_decl_line(0xd0)
	.dwattr $C$DW$21, DW_AT_decl_column(0x0a)

$C$DW$22	.dwtag  DW_TAG_variable
	.dwattr $C$DW$22, DW_AT_name("Typeinfo for short")
	.dwattr $C$DW$22, DW_AT_linkage_name("_ZTIs")
	.dwattr $C$DW$22, DW_AT_type(*$C$DW$T$162)
	.dwattr $C$DW$22, DW_AT_declaration
	.dwattr $C$DW$22, DW_AT_external
	.dwattr $C$DW$22, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/libcxx/typeinfo")
	.dwattr $C$DW$22, DW_AT_decl_line(0xd0)
	.dwattr $C$DW$22, DW_AT_decl_column(0x0a)


$C$DW$23	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$23, DW_AT_name("logf")
	.dwattr $C$DW$23, DW_AT_linkage_name("logf")
	.dwattr $C$DW$23, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$23, DW_AT_declaration
	.dwattr $C$DW$23, DW_AT_external
	.dwattr $C$DW$23, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/math.h")
	.dwattr $C$DW$23, DW_AT_decl_line(0x181)
	.dwattr $C$DW$23, DW_AT_decl_column(0x1a)
$C$DW$24	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$24, DW_AT_type(*$C$DW$T$16)

	.dwendtag $C$DW$23

;	/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/bin/opt7x /tmp/TI51UAoD0yK /tmp/TI51UJ67L9a 
	.sect	".text:_Z26TIDL_sparseDetScoreCalc_cnIsEiP26sTIDL_DetectOutputParams_tP29sTIDL_ALgDetectOutputParams_t"
	.clink
	.global	||_Z26TIDL_sparseDetScoreCalc_cnIsEiP26sTIDL_DetectOutputParams_tP29sTIDL_ALgDetectOutputParams_t||

$C$DW$25	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$25, DW_AT_name("int TIDL_sparseDetScoreCalc_cn")
	.dwattr $C$DW$25, DW_AT_low_pc(||_Z26TIDL_sparseDetScoreCalc_cnIsEiP26sTIDL_DetectOutputParams_tP29sTIDL_ALgDetectOutputParams_t||)
	.dwattr $C$DW$25, DW_AT_high_pc(0x00)
	.dwattr $C$DW$25, DW_AT_linkage_name("_Z26TIDL_sparseDetScoreCalc_cnIsEiP26sTIDL_DetectOutputParams_tP29sTIDL_ALgDetectOutputParams_t")
	.dwattr $C$DW$25, DW_AT_external
	.dwattr $C$DW$25, DW_AT_decl_file("src/tidl_detectionOutput_score.c")
	.dwattr $C$DW$25, DW_AT_decl_line(0x13a)
	.dwattr $C$DW$25, DW_AT_decl_column(0x09)
	.dwattr $C$DW$25, DW_AT_TI_max_frame_size(0xa8)
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 315,column 1,is_stmt,address ||_Z26TIDL_sparseDetScoreCalc_cnIsEiP26sTIDL_DetectOutputParams_tP29sTIDL_ALgDetectOutputParams_t||,isa 0

	.dwfde $C$DW$CIE, ||_Z26TIDL_sparseDetScoreCalc_cnIsEiP26sTIDL_DetectOutputParams_tP29sTIDL_ALgDetectOutputParams_t||
$C$DW$26	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$26, DW_AT_name("params")
	.dwattr $C$DW$26, DW_AT_location[DW_OP_reg4]

$C$DW$27	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$27, DW_AT_name("algDetLyrParams")
	.dwattr $C$DW$27, DW_AT_location[DW_OP_reg5]


;******************************************************************************
;* FUNCTION NAME: int TIDL_sparseDetScoreCalc_cn<short>(sTIDL_DetectOutputParams_t *, sTIDL_ALgDetectOutputParams_t *)*
;*                                                                            *
;*   Regs Modified     : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,VB0,VB1,   *
;*                           VB2,VB3,VB4,VB5,VB6,VB7,VB8,VB9,VB10,VB11,VB12,  *
;*                           VB13,VB14,VB15,AL0,AL1,AM0,AM1,AM2,AM3,D0,D1,D2, *
;*                           D3,D4,D5,D6,D7,D8,D9,D10,D11,D12,D13,D14,SP,VBL0,*
;*                           VBL1,VBL2,VBL3,VBL4,VBL5,VBL6,VBL7,VBM0,VBM1,    *
;*                           VBM2,VBM3,VBM4,VBM5,VBM6,VBM7,P0,P1,P2           *
;*   Regs Used         : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,VB0,VB1,   *
;*                           VB2,VB3,VB4,VB5,VB6,VB7,VB8,VB9,VB10,VB11,VB12,  *
;*                           VB13,VB14,VB15,AL0,AL1,AM0,AM1,AM2,AM3,D0,D1,D2, *
;*                           D3,D4,D5,D6,D7,D8,D9,D10,D11,D12,D13,D14,SP,VBL0,*
;*                           VBL1,VBL2,VBL3,VBL4,VBL5,VBL6,VBL7,VBM0,VBM1,    *
;*                           VBM2,VBM3,VBM4,VBM5,VBM6,VBM7,P0,P1,P2           *
;*   Local Frame Size  : 0 Args + 0 Auto + 168 Save = 168 byte                *
;******************************************************************************
||_Z26TIDL_sparseDetScoreCalc_cnIsEiP26sTIDL_DetectOutputParams_tP29sTIDL_ALgDetectOutputParams_t||:
;** --------------------------------------------------------------------------*
;* D0    assigned to $O$C36
;* D1    assigned to $O$C37
;* D0    assigned to $O$C38
;* D1    assigned to $O$C39
;* VB0   assigned to $O$C40
;* VBM0  assigned to $O$C41
;* VBM0  assigned to $O$C42
;* VB4   assigned to $O$C43
;* VB3   assigned to $O$C44
;* VB0   assigned to $O$C45
;* A0    assigned to $O$C46
;* A0    assigned to $O$C47
;* D0    assigned to $O$C48
;* D0    assigned to $O$C49
;* VB1   assigned to $O$C50
;* VB1   assigned to $O$C51
;* VB0   assigned to $O$C52
;* VB3   assigned to $O$C53
;* VB1   assigned to $O$C54
;* A2    assigned to $O$C55
;* VB0   assigned to $O$C56
;* A0    assigned to $O$C57
;* D5    assigned to $O$C58
;* VB4   assigned to $O$C59
;* VBM5  assigned to $O$C60
;* VBM5  assigned to $O$C61
;* AM1   assigned to $O$C62
;* AM1   assigned to $O$C63
;* VB5   assigned to $O$C64
;* VB6   assigned to $O$C65
;* VBM5  assigned to $O$C66
;* D0    assigned to $O$C67
;* AM1   assigned to $O$C68
;* VB5   assigned to $O$C69
;* VB4   assigned to $O$C70
;* VB11  assigned to $O$C71
;* VB7   assigned to $O$C72
;* VB4   assigned to $O$C73
;* VBM7  assigned to $O$C74
;* VBM6  assigned to $O$C75
;* A2    assigned to $O$C76
;* D3    assigned to $O$C77
;* VB2   assigned to $O$C78
;* D0    assigned to $O$C79
;* D1    assigned to $O$C80
;* D2    assigned to $O$C81
;* VB6   assigned to $O$K26
;* VB1   assigned to $O$K26
;* VB1   assigned to $O$K62
;* D3    assigned to $O$K62
;* VB14  assigned to $O$K62
;* VB3   assigned to $O$K62
;* VB2   assigned to $O$K159
;* VB2   assigned to $O$K159
;* VBM4  assigned to $O$K159
;* D9    assigned to $O$K153
;* D1    assigned to $O$K153
;* D3    assigned to $O$K153
;* D11   assigned to $O$K137
;* VBL4  assigned to $O$K137
;* VBL7  assigned to $O$K137
;* D5    assigned to $O$K127
;* D4    assigned to $O$K127
;* VB15  assigned to $O$K172
;* VBM1  assigned to $O$K172
;* VB9   assigned to $O$K172
;* VB14  assigned to $O$K187
;* VBM7  assigned to $O$K187
;* VB10  assigned to $O$K187
;* VB13  assigned to $O$K183
;* VBM6  assigned to $O$K183
;* VBM3  assigned to $O$K183
;* VB12  assigned to $O$K170
;* VBM5  assigned to $O$K170
;* VBM2  assigned to $O$K170
;* VB11  assigned to $O$K179
;* VBM4  assigned to $O$K179
;* VBM1  assigned to $O$K179
;* VB10  assigned to $O$K175
;* VBM3  assigned to $O$K175
;* VBM0  assigned to $O$K175
;* VB9   assigned to $O$K164
;* VBL0  assigned to $O$K164
;* VBL0  assigned to $O$K164
;* VB3   assigned to $O$K189
;* VBL3  assigned to $O$K189
;* VB2   assigned to $O$K189
;* VBL2  assigned to $O$K193
;* VB0   assigned to $O$K193
;* VB7   assigned to $O$K237
;* VBM0  assigned to $O$K237
;* VB8   assigned to $O$K237
;* VBM2  assigned to $O$K256
;* VB13  assigned to $O$K256
;* A11   assigned to $O$U441
;* A12   assigned to $O$U441
;* A10   assigned to $O$U450
;* A7    assigned to $O$U439
;* A8    assigned to $O$v1
;* A9    assigned to $O$v1
;* A4    assigned to $O$Lr3$totValidCnt
;* VB1   assigned to $O$Lr9$max
;* VB3   assigned to $O$U29
;* A0    assigned to $O$U36
;* VB2   assigned to $O$U55
;* D2    assigned to $O$U100
;* D2    assigned to $O$U30
;* A1    assigned to $O$U279
;* D2    assigned to $O$U296
;* VB5   assigned to $O$U399
;* VBL7  assigned to $O$K422
;* A11   assigned to $O$U451
;* A10   assigned to $O$U440
;* D0    assigned to $O$U520
;* D10   assigned to $O$K535
;* VB2   assigned to $O$U574
;* VB0   assigned to $O$U590
;* VB0   assigned to $O$U594
;* VB0   assigned to $O$L1
;* A1    assigned to $O$L2
;* A0    assigned to $O$L3
;* A1    assigned to $O$L4
;* A0    assigned to $O$L5
;* A3    assigned to $O$L6
;* VB0   assigned to $O$L7
;* A4    assigned to $O$L8
;* VB2   assigned to $O$I27
;* A9    assigned to $O$v5
;* AL0   assigned to $O$v4
;* AL0   assigned to $O$v3
;* AL0   assigned to $O$v2
;* VBL6  assigned to $O$Lr2$maxInit
;* VB0   assigned to $O$Lr4$classCnt
;* A8    assigned to $O$Lr967$i
;* VB0   assigned to $O$Lr5$totalCnt
;* A0    assigned to $O$Lr901$curIndex
;* D1    assigned to $O$Lr665$totValidCnt
;* A4    assigned to $O$Lr1033$totValidCnt
;* A0    assigned to $O$Lr871$curClassCountM
;* A7    assigned to $O$Lr632$cnt
;* VB3   assigned to $O$Lr768$curIndex
;* VB7   assigned to $O$Lr832$y
;* A3    assigned to $O$Lr684$y
;* VB6   assigned to $O$Lr719$y
;* VB4   assigned to $O$Lr667$y
;* A8    assigned to $O$Lr588$i
;* D4    assigned to $O$Lr459$cnt
;* VB1   assigned to $O$Lr537$y
;* VB0   assigned to $O$Lr431$y
;* VB0   assigned to $O$Lr414$curClassCountM
;* VB1   assigned to $O$Lr419
;* D8    assigned to $O$Lr197$totValidCnt
;* A4    assigned to $O$Lr1034$totValidCnt
;* A4    assigned to $O$Lr389$curIndex
;* AM3   assigned to $O$Lr397$anchor
;* A2    assigned to $O$Lr399$curLoc
;* A9    assigned to $O$Lr393$head
;* VB4   assigned to $O$Lr407$onebyqFact
;* VB1   assigned to $O$Lr362$max
;* VB0   assigned to $O$Lr233$denom
;* A1    assigned to $O$Lr11$classStride
;* A7    assigned to $O$Lr12$anchorStride
;* A0    assigned to $O$Lr364$i4
;* A8    assigned to $O$Lr235$i4
;* VB0   assigned to $O$Lr10$denom
;* VBL0  assigned to $O$Lr312$inVal
;* VB1   assigned to $O$Lr278$ftemp
;* VB1   assigned to $O$Lr243$yI
;* VB0   assigned to $O$Lr13$ePwX
;* VBM1  assigned to $O$Lr220$recp_y
;* D0    assigned to $O$Lr222$curIndex
;* A9    assigned to $O$Lr159$i4
;* VB0   assigned to $O$Lr106$i
;* A0    assigned to $O$Lr20$curIndex
;* VB0   assigned to $O$Lr27$score
;* A5    assigned to algDetLyrParams
$C$DW$28	.dwtag  DW_TAG_variable
	.dwattr $C$DW$28, DW_AT_name("algDetLyrParams")
	.dwattr $C$DW$28, DW_AT_type(*$C$DW$T$146)
	.dwattr $C$DW$28, DW_AT_location[DW_OP_reg5]

;* A6    assigned to params
$C$DW$29	.dwtag  DW_TAG_variable
	.dwattr $C$DW$29, DW_AT_name("params")
	.dwattr $C$DW$29, DW_AT_type(*$C$DW$T$144)
	.dwattr $C$DW$29, DW_AT_location[DW_OP_reg6]

	.dwcfi	cfa_offset, 0
;          EXCLUSIVE CPU CYCLES: 15
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 330,column 3,is_stmt,isa 0
           LDW     .D1     *A5(1304),AL0     ; [A_D1] |330| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 315,column 1,is_stmt,isa 0

           CMPEQW  .L1     AL0,0x1,A0        ; [A_L1] |330| 
||         MV      .D1     A4,A6             ; [A_D1] |315| 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 343,column 3,is_stmt,isa 0

   [!A0]   LDW     .D1     *A6(68),AL0       ; [A_D1] |343| 
|| [ A0]   LDW     .D2     *A6(68),AL0       ; [A_D2] |343| 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 326,column 19,is_stmt,isa 0

           MVK32   .L2     0xffff8000,BL6    ; [B_L2] |326| 
||         STD     .D1     A8,*SP(8)         ; [A_D1] 
||         STD     .D2X    A9,*SP++(-168)    ; [A_D2] 
	.dwcfi	save_reg_to_mem, 8, 8
	.dwcfi	cfa_offset, 168
	.dwcfi	save_reg_to_mem, 9, -168

           STD     .D1     A11,*SP(152)      ; [A_D1] 
||         STD     .D2X    A10,*SP(160)      ; [A_D2] 

	.dwcfi	save_reg_to_mem, 11, 152
	.dwcfi	save_reg_to_mem, 10, 160
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 332,column 5,is_stmt,isa 0

           CMPEQW  .L1     AL0,0x2,A0        ; [A_L1] |343| 
|| [ A0]   MVK32   .L2     0xffffff80,BL6    ; [B_L2] |332| 
||         VST64B  .D2     VB15,*SP(80)      ; [A_D2] 

	.dwcfi	save_reg_to_mem, 63, 80
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 343,column 3,is_stmt,isa 0

   [ A0]   B       .B1     ||$C$L25||        ; [A_B] |343| 
||         VST64B  .D2     VB14,*SP(16)      ; [A_D2] 
||         STD     .D1     A12,*SP(144)      ; [A_D1] 

	.dwcfi	save_reg_to_mem, 62, 16
	.dwcfi	save_reg_to_mem, 12, 144
           ; BRANCHCC OCCURS {||$C$L25||}    ; [] |343| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 8
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 350,column 25,is_stmt,isa 0
           LDW     .D1     *A6(12),A9        ; [A_D1] |350| 

           CMPGTW  .L1     A9,0,A0           ; [A_L1] |350| 
||         MVKU32  .L2     0,B2              ; [B_L2] 
||         MVKU32  .S2     0,B1              ; [B_S2] 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 350,column 20,is_stmt,isa 0

   [!A0]   B       .B1     ||$C$L4||         ; [A_B] |350| 
||         EXT     .L2     B1,0x20,0x20,B3   ; [B_L2] 
||         MVKU32  .L1     0,A8              ; [A_L1] |350| 
||         MVKU32  .S2     0,B0              ; [B_S2] 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 350,column 25,is_stmt,isa 0
           ; BRANCHCC OCCURS {||$C$L4||}     ; [] |350| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 9

           MV      .D1     A5,D4             ; [A_D1] 
||         MV      .L1X    B3,A1             ; [A_L1] 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 358,column 9,is_stmt,isa 0

   [ A1]   SLDD    .D1     *D4(1200),D2      ; [A_D1] |358| <0,1>  ^ [C1]
|| [ A1]   SLDD    .D2     *D4(1192),D6      ; [A_D2] |358| <0,0>  ^ [C1]

   [ A1]   ADDD    .D1     D2,A1,D6          ; [A_D1] |358| <0,7>  ^ 
|| [ A1]   ADDD    .D2     A1,D6,D7          ; [A_D2] |358| <0,7>  ^ 

           ADDD    .D1     D6,0xfffffffc,D0  ; [A_D1] |358| <0,8>  ^ 
||         ADDD    .D2     D7,0xfffffffc,D1  ; [A_D2] |358| <0,8>  ^ 
||         MV      .L1     A6,D3             ; [A_L1] 
||         MV      .L2     B2,BL2            ; [B_L2] 
||         UNPROT          0x1               ; [A_U] 

;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*
;*      Loop found in file               : src/tidl_detectionOutput_score.c
;*      Loop source line                 : 350
;*      Loop opening brace source line   : 351
;*      Loop closing brace source line   : 362
;*      Known Minimum Trip Count         : 1                    
;*      Known Max Trip Count Factor      : 1
;*      Loop Carried Dependency Bound(^) : 25
;*      Unpartitioned Resource Bound     : 6
;*      Partitioned Resource Bound       : 6 (pre-sched)
;*
;*      Searching for software pipeline schedule at ...
;*         ii = 25 Unsafe schedule for irregular loop
;*         ii = 25 Unsafe schedule for irregular loop
;*         ii = 25 Did not find schedule
;*         ii = 26 Unsafe schedule for irregular loop
;*         ii = 26 Unsafe schedule for irregular loop
;*         ii = 26 Unsafe schedule for irregular loop
;*         ii = 26 Did not find schedule
;*         ii = 27 Unsafe schedule for irregular loop
;*         ii = 27 Unsafe schedule for irregular loop
;*         ii = 27 Unsafe schedule for irregular loop
;*         ii = 27 Did not find schedule
;*         ii = 28 Unsafe schedule for irregular loop
;*         ii = 28 Unsafe schedule for irregular loop
;*         ii = 28 Unsafe schedule for irregular loop
;*         ii = 28 Did not find schedule
;*         ii = 29 Unsafe schedule for irregular loop
;*         ii = 29 Unsafe schedule for irregular loop
;*         ii = 29 Unsafe schedule for irregular loop
;*         ii = 29 Did not find schedule
;*         ii = 30 Unsafe schedule for irregular loop
;*         ii = 30 Schedule found with 2 iterations in parallel
;*
;*      Partitioned Resource Bound(*)    : 9 (post-sched)
;*
;*      Constant Extension #0 Used [C0]  : 0
;*      Constant Extension #1 Used [C1]  : 5
;*
;*      Resource Partition (may include "post-sched" split/spill moves):
;*
;*                                                A-side   B-side
;*      .L units                                     0        0     
;*      .S units                                     0        0     
;*      .M units                                     0        0     
;*      .N units                                     0        0     
;*      .D units                                    12        -     
;*      .B units                                     1        -     
;*      .C units                                     -        0     
;*      .P units                                     -        0     
;*
;*      .M/.N units                                  0        0     
;*      .L/.S units                                  1        0     
;*      .L/.S/.C units                               0        0     
;*      .L/.S/.C/.M units                            0        0     
;*      .L/.S/.C/.M/.D units                         8        2     
;*
;*      .X cross paths                               2        0     
;*
;*      Bound(.D1 .D2 .D)                            6        -     
;*      Bound(.M .N .MN)                             0        0     
;*      Bound(.L .S .LS)                             1        0     
;*      Bound(.L .S .C .LS .LSC)                     1        0     
;*      Bound(.L .S .C .M .LS .LSC .LSCM)            1        0     
;*      Bound(.L .S .C .M .D .LS .LSC .LSCM .LSCMD)  5        1     
;*
;*      Register Usage Tables:
;*
;*      +----------------------------------+----------------------------------+
;*      |              ASIDE               |              BSIDE               |
;*      +----------------+--------+--------+----------------+--------+--------+
;*      |      "Axx"     |  "ALx" |  "AMx" |     "VBxx"     | "VBLx" | "VBMx" |
;*      +----------------|--------|--------|----------------|--------|--------+
;*      |0000000000111111|00000000|00000000|0000000000111111|00000000|00000000|
;*      |0123456789012345|01234567|01234567|0123456789012345|01234567|01234567|
;*      +----------------|--------|--------|----------------|--------|--------+
;*   0: | *      *       |        |        | *              |  *     |        |
;*   1: | *      *       |        |        | *              |  *     |        |
;*   2: | *      *       |        |        | *              |  *     |        |
;*   3: | *      *       |        |        | *              |  *     |        |
;*   4: | *      *       |        |        | *              |  *     |        |
;*   5: | *      *       |        |        | *              |  *     |        |
;*   6: | *      *       |        |        | *              |***     |        |
;*   7: | *      *       |        |        |**              |  *     |        |
;*   8: | *      *       |        |        | *              |  *     |        |
;*   9: | *      *       |        |        | *              |  *     |        |
;*  10: | *      *       |        |        | *              |  *     |        |
;*  11: | *      *       |        |        | *              |  *     |        |
;*  12: | *      *       |        |        | *              |  *     |        |
;*  13: | *      *       |        |        | *              |  *     |        |
;*  14: | *      *       |        |        | *              |  *     |        |
;*  15: | *      *       |        |        | *              |  *     |        |
;*  16: | *      *       |        |        | *              |  *     |        |
;*  17: | *      *       |        |        | *              |  *     |        |
;*  18: | *      *       |        |        | *              |  *     |        |
;*  19: | *      *       |        |        | *              |  *     |        |
;*  20: | *      *       |        |        | *              |  *     |        |
;*  21: | *      *       |        |        | *              |  *     |        |
;*  22: | *      *       |        |        | *              |  *     |        |
;*  23: | *      *       |        |        | *              |  *     |        |
;*  24: | *      *       |        |        | *              |  *     |        |
;*  25: | *      *       |        |        | *              |  *     |        |
;*  26: | *      *       |        |        | *              |  *     |        |
;*  27: | *      *       |        |        | *              |  *     |        |
;*  28: | *      *       |*       |        | *              |  *     |        |
;*  29: |**      *       |        |        | *              |* *     |        |
;*      +----------------+--------+--------+----------------+--------+--------+
;*
;*      +----------------+                 +--------+       +-------+
;*      |      "Dxx"     |                 |  "Px"  |       |"CUCRx"|
;*      +----------------+                 +--------+       +-------+
;*      |0000000000111111|                 |00000000|       |0000   |
;*      |0123456789012345|                 |01234567|       |0123   |
;*      +----------------+                 +--------+       +-------+
;*   0: |*****           |                 |        |       |       |
;*   1: |*****           |                 |        |       |       |
;*   2: |*****           |                 |        |       |       |
;*   3: |*****           |                 |        |       |       |
;*   4: |*****           |                 |        |       |       |
;*   5: |*****           |                 |        |       |       |
;*   6: |*****           |                 |        |       |       |
;*   7: |*****           |                 |        |       |       |
;*   8: |*****           |                 |        |       |       |
;*   9: |*****           |                 |        |       |       |
;*  10: |*****           |                 |        |       |       |
;*  11: |*****           |                 |        |       |       |
;*  12: |*****           |                 |        |       |       |
;*  13: |*****           |                 |        |       |       |
;*  14: |******          |                 |        |       |       |
;*  15: |******          |                 |        |       |       |
;*  16: |*******         |                 |        |       |       |
;*  17: |*****           |                 |        |       |       |
;*  18: |*****           |                 |        |       |       |
;*  19: |*****           |                 |        |       |       |
;*  20: |*****           |                 |        |       |       |
;*  21: |*****           |                 |        |       |       |
;*  22: |*****           |                 |        |       |       |
;*  23: |******          |                 |        |       |       |
;*  24: |*****           |                 |        |       |       |
;*  25: |*****           |                 |        |       |       |
;*  26: |*****           |                 |        |       |       |
;*  27: |***** *         |                 |        |       |       |
;*  28: |***** *         |                 |        |       |       |
;*  29: |***** **        |                 |        |       |       |
;*      +----------------+                 +--------+       +-------+
;*
;*      Done
;*
;*      Collapsed epilog stages       : 1
;*      Prolog not removed
;*      Collapsed prolog stages       : 0
;*
;*      Max amt of load speculation   : 0 bytes
;*
;*      Minimum safe trip count       : 1
;*      Min. prof. trip count  (est.) : 2
;*
;*      Mem bank conflicts/iter(est.) : { min 0.000, est 0.000, max 0.000 }
;*      Mem bank perf. penalty (est.) : 0.0%
;*
;*
;*      Total cycles (est.)         : 9 + trip_cnt * 30        
;*----------------------------------------------------------------------------*
;*        SINGLE SCHEDULED ITERATION
;*
;*        ||$C$C823||:
;*   0      [ A1]   SLDD    .D1     *D4(1192),D6      ; [A_D1] |358|  ^ [C1]
;*   1      [ A1]   SLDD    .D1     *D4(1200),D2      ; [A_D1] |358|  ^ [C1]
;*   2              NOP     0x5     ; [A_B] 
;*   7      [ A1]   ADDD    .D2     D2,A1,D6          ; [A_D2] |358|  ^ 
;*     ||   [ A1]   ADDD    .D1     A1,D6,D7          ; [A_D1] |358|  ^ 
;*   8      [ A1]   ADDD    .D1     D6,0xfffffffc,D0  ; [A_D1] |358|  ^ 
;*     ||   [ A1]   ADDD    .D2     D7,0xfffffffc,D1  ; [A_D2] |358|  ^ 
;*   9      [ A1]   LDW     .D2     *D0(0),BL0        ; [A_D2] |358|  ^ 
;*     ||   [ A1]   LDW     .D1     *D1(0),BL1        ; [A_D1] |358|  ^ 
;*  10              NOP     0x5     ; [A_B] 
;*  15      [ A1]   ADDW    .L2     BL1,BL0,B0        ; [B_L2] |358|  ^ 
;*  16      [ A1]   STW     .D1X    B0,*D2[A8]        ; [A_D1] |358|  ^ 
;*  17      [ A1]   LDD     .D1     *D4(1192),D5      ; [A_D1] |359|  ^ [C1]
;*  18              NOP     0x1     ; [A_B] 
;*  19      [!A1]   SLDD    .D1     *D4(1200),D6      ; [A_D1] |354| [C1]
;*  20              NOP     0x3     ; [A_B] 
;*  23      [ A1]   ADDD    .D1     A1,D5,D5          ; [A_D1] |359|  ^ 
;*  24      [ A1]   ADDD    .D1     D5,0xfffffffc,D5  ; [A_D1] |359|  ^ 
;*  25      [!A1]   STW     .D2     B1,*D6[A8]        ; [A_D2] |354| 
;*     ||   [ A1]   STW     .D1X    B1,*D5(0)         ; [A_D1] |359|  ^ 
;*  26              LDD     .D1     *D4(1192),D5      ; [A_D1] |361| [C1]
;*  27              NOP     0x2     ; [A_B] 
;*  29              ADDD    .D1     A1,0x4,A1         ; [A_D1] |350|  ^ 
;*  30              NOP     0x1     ; [A_B] 
;*  31              LDW     .D2     *D3(12),AL0       ; [A_D2] |350| 
;*  32              LDW     .D1     *D5[A8],BL0       ; [A_D1] |361| 
;*  33              NOP     0x3     ; [A_B] 
;*  36              ADDW    .D1     A8,0x1,A8         ; [A_D1] |350| 
;*  37              CMPGTW  .L1     AL0,A8,A0         ; [A_L1] |350| 
;*  38              ADDW    .L2     BL0,BL2,BL2       ; [B_L2] |361| 
;*     ||   [ A0]   B       .B1     ||$C$C823||       ; [A_B] |350| 
;*  39              ; BRANCHCC OCCURS {||$C$C823||}   ; [] |350| 
;*----------------------------------------------------------------------------*
||$C$L1||:    ; PIPED LOOP PROLOG
;** --------------------------------------------------------------------------*
||$C$L2||:    ; PIPED LOOP KERNEL
;          EXCLUSIVE CPU CYCLES: 30

   [ A1]   LDW     .D2     *D0(0),BL0        ; [A_D2] |358| <0,9>  ^ 
|| [ A1]   LDW     .D1     *D1(0),BL1        ; [A_D1] |358| <0,9>  ^ 

           NOP             0x5               ; [A_B] 
   [ A1]   ADDW    .L2     BL1,BL0,B0        ; [B_L2] |358| <0,15>  ^ 
   [ A1]   STW     .D1X    B0,*D2[A8]        ; [A_D1] |358| <0,16>  ^ 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 359,column 9,is_stmt,isa 0
   [ A1]   LDD     .D1     *D4(1192),D5      ; [A_D1] |359| <0,17>  ^ [C1]
           NOP             0x1               ; [A_B] 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 354,column 9,is_stmt,isa 0
   [!A1]   SLDD    .D1     *D4(1200),D6      ; [A_D1] |354| <0,19> [C1]
           NOP             0x3               ; [A_B] 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 359,column 9,is_stmt,isa 0
   [ A1]   ADDD    .D1     A1,D5,D5          ; [A_D1] |359| <0,23>  ^ 
   [ A1]   ADDD    .D1     D5,0xfffffffc,D5  ; [A_D1] |359| <0,24>  ^ 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 354,column 9,is_stmt,isa 0

   [!A1]   STW     .D2     B1,*D6[A8]        ; [A_D2] |354| <0,25> 
|| [ A1]   STW     .D1X    B1,*D5(0)         ; [A_D1] |359| <0,25>  ^ 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 361,column 7,is_stmt,isa 0
           LDD     .D1     *D4(1192),D5      ; [A_D1] |361| <0,26> [C1]
           NOP             0x2               ; [A_B] 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 350,column 25,is_stmt,isa 0
           ADDD    .D1     A1,0x4,A1         ; [A_D1] |350| <0,29>  ^ 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 358,column 9,is_stmt,isa 0
   [ A1]   SLDD    .D1     *D4(1192),D6      ; [A_D1] |358| <1,0>  ^ [C1]
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 350,column 25,is_stmt,isa 0

           LDW     .D2     *D3(12),AL0       ; [A_D2] |350| <0,31> 
|| [ A1]   SLDD    .D1     *D4(1200),D2      ; [A_D1] |358| <1,1>  ^ [C1]

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 361,column 7,is_stmt,isa 0
           LDW     .D1     *D5[A8],BL0       ; [A_D1] |361| <0,32> 
           NOP             0x3               ; [A_B] 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 350,column 25,is_stmt,isa 0
           ADDW    .D1     A8,0x1,A8         ; [A_D1] |350| <0,36> 

           CMPGTW  .L1     AL0,A8,A0         ; [A_L1] |350| <0,37> 
|| [ A1]   ADDD    .D2     D2,A1,D6          ; [A_D2] |358| <1,7>  ^ 
|| [ A1]   ADDD    .D1     A1,D6,D7          ; [A_D1] |358| <1,7>  ^ 

   [ A0]   B       .B1     ||$C$L2||         ; [A_B] |350| <0,38> 
||         ADDW    .L2     BL0,BL2,BL2       ; [B_L2] |361| <0,38> 
|| [ A1]   ADDD    .D2     D7,0xfffffffc,D1  ; [A_D2] |358| <1,8>  ^ 
|| [ A1]   ADDD    .D1     D6,0xfffffffc,D0  ; [A_D1] |358| <1,8>  ^ 

;** --------------------------------------------------------------------------*
||$C$L3||:    ; PIPED LOOP EPILOG
;          EXCLUSIVE CPU CYCLES: 1
           PROTCLR                            ; [A_U] VBL2
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 2

           MV      .D1     D5,A0             ; [A_D1] 
||         MV      .L2     BL2,B2            ; [B_L2] 

           B       .B1     ||$C$L5||         ; [A_B] 
||         MV      .L2     B2,B0             ; [B_L2] 
||         MV      .D1     D3,A6             ; [A_D1] 
||         MV      .D2     D4,A5             ; [A_D2] 
||         MV      .L1     AL0,A9            ; [A_L1] 

           ; BRANCH OCCURS {||$C$L5||}       ; [] 
;** --------------------------------------------------------------------------*
||$C$L4||:    
;          EXCLUSIVE CPU CYCLES: 6
           LDD     .D1     *A5(1192),A0      ; [A_D1] 
;** --------------------------------------------------------------------------*
||$C$L5||:    
;          EXCLUSIVE CPU CYCLES: 3
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 364,column 5,is_stmt,isa 0
           ADDAW   .D1     A0,A9,D0          ; [A_D1] |364| 

           ADDD    .D1     D0,0xfffffffc,D0  ; [A_D1] |364| 
||         CMPGTW  .L1X    B0,0,A0           ; [A_L1] |366| 

   [!A0]   B       .B1     ||$C$L9||         ; [A_B] |366| 
||         STW     .D1X    B1,*D0(0)         ; [A_D1] |364| 
||         MVKU32  .L2     0xff,B3           ; [B_L2] 
||         MVKU32  .S2     0,B2              ; [B_S2] 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 366,column 25,is_stmt,isa 0
           ; BRANCHCC OCCURS {||$C$L9||}     ; [] |366| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 4
           EXT     .L2     B0,0x20,0x20,B0   ; [B_L2] 
           MV      .L1X    B3,D5             ; [A_L1] 

           NLCINIT .S1X    B0,0x1,0          ; [A_S1] 
||         UNPROT          0x1               ; [A_U] 

           TICK                               ; [A_U] <0,0> 
||         MV      .L1X    B2,D4             ; [A_L1] 
||         MV      .D1     A5,D2             ; [A_D1] 

;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*
;*      Loop found in file               : src/tidl_detectionOutput_score.c
;*      Loop source line                 : 366
;*      Loop opening brace source line   : 367
;*      Loop closing brace source line   : 373
;*      Known Minimum Trip Count         : 1                    
;*      Known Max Trip Count Factor      : 1
;*      Loop Carried Dependency Bound(^) : 38
;*      Unpartitioned Resource Bound     : 6
;*      Partitioned Resource Bound       : 6 (pre-sched)
;*
;*      Searching for software pipeline schedule at ...
;*         ii = 38 Schedule found with 2 iterations in parallel
;*
;*      Partitioned Resource Bound(*)    : 8 (post-sched)
;*
;*      Constant Extension #0 Used [C0]  : 1
;*      Constant Extension #1 Used [C1]  : 4
;*
;*      Resource Partition (may include "post-sched" split/spill moves):
;*
;*                                                A-side   B-side
;*      .L units                                     0        0     
;*      .S units                                     0        0     
;*      .M units                                     0        0     
;*      .N units                                     0        0     
;*      .D units                                    12        -     
;*      .B units                                     1        -     
;*      .C units                                     -        0     
;*      .P units                                     -        0     
;*
;*      .M/.N units                                  0        0     
;*      .L/.S units                                  1        0     
;*      .L/.S/.C units                               0        0     
;*      .L/.S/.C/.M units                            0        0     
;*      .L/.S/.C/.M/.D units                         4        1     
;*
;*      .X cross paths                               1        0     
;*
;*      Bound(.D1 .D2 .D)                            6        -     
;*      Bound(.M .N .MN)                             0        0     
;*      Bound(.L .S .LS)                             1        0     
;*      Bound(.L .S .C .LS .LSC)                     1        0     
;*      Bound(.L .S .C .M .LS .LSC .LSCM)            1        0     
;*      Bound(.L .S .C .M .D .LS .LSC .LSCM .LSCMD)  4        1     
;*
;*      Register Usage Tables:
;*
;*      +----------------------------------+----------------------------------+
;*      |              ASIDE               |              BSIDE               |
;*      +----------------+--------+--------+----------------+--------+--------+
;*      |      "Axx"     |  "ALx" |  "AMx" |     "VBxx"     | "VBLx" | "VBMx" |
;*      +----------------|--------|--------|----------------|--------|--------+
;*      |0000000000111111|00000000|00000000|0000000000111111|00000000|00000000|
;*      |0123456789012345|01234567|01234567|0123456789012345|01234567|01234567|
;*      +----------------|--------|--------|----------------|--------|--------+
;*   0: |                |        |        |                |        |        |
;*   1: |                |        |        |                |        |        |
;*   2: |                |        |        |                |        |        |
;*   3: |                |        |        |                |        |        |
;*   4: |                |        |        |                |        |        |
;*   5: |                |        |        |                |        |        |
;*   6: |                |        |        |                |        |        |
;*   7: |                |        |        |                |        |        |
;*   8: |                |        |        |                |        |        |
;*   9: |                |        |        |                |        |        |
;*  10: |                |        |        |                |        |        |
;*  11: |                |        |        |                |        |        |
;*  12: |                |        |        |                |        |        |
;*  13: |*               |        |        |                |        |        |
;*  14: |*               |        |        |                |        |        |
;*  15: |*               |        |        |                |        |        |
;*  16: |*               |        |        |                |        |        |
;*  17: |*               |        |        |                |        |        |
;*  18: |*               |        |        |                |        |        |
;*  19: |*               |        |        |                |        |        |
;*  20: |*               |        |        |                |        |        |
;*  21: |*               |        |        |                |        |        |
;*  22: |*               |        |        |                |        |        |
;*  23: |                |        |        |                |        |        |
;*  24: |                |        |        |                |        |        |
;*  25: |                |        |        |                |        |        |
;*  26: |                |        |        |                |        |        |
;*  27: |                |        |        |                |        |        |
;*  28: |                |        |        |                |        |        |
;*  29: |                |        |        |                |        |        |
;*  30: |                |        |        |                |        |        |
;*  31: |                |        |        |                |        |        |
;*  32: |                |        |        |                |        |        |
;*  33: |                |        |        |                |        |        |
;*  34: |                |        |        |                |        |        |
;*  35: |                |        |        |                |        |        |
;*  36: |                |        |        |                |*       |        |
;*  37: |                |        |        |*               |        |        |
;*      +----------------+--------+--------+----------------+--------+--------+
;*
;*      +----------------+                 +--------+       +-------+
;*      |      "Dxx"     |                 |  "Px"  |       |"CUCRx"|
;*      +----------------+                 +--------+       +-------+
;*      |0000000000111111|                 |00000000|       |0000   |
;*      |0123456789012345|                 |01234567|       |0123   |
;*      +----------------+                 +--------+       +-------+
;*   0: |  * **          |                 |        |       |       |
;*   1: |  * **          |                 |        |       |       |
;*   2: |  * **          |                 |        |       |       |
;*   3: |  * **          |                 |        |       |       |
;*   4: |  * **          |                 |        |       |       |
;*   5: |  * **          |                 |        |       |       |
;*   6: |* * **          |                 |        |       |       |
;*   7: |* * **          |                 |        |       |       |
;*   8: |  * **          |                 |        |       |       |
;*   9: |  * **          |                 |        |       |       |
;*  10: |  * **          |                 |        |       |       |
;*  11: |  * **          |                 |        |       |       |
;*  12: |  * **          |                 |        |       |       |
;*  13: |  * **          |                 |        |       |       |
;*  14: |* * **          |                 |        |       |       |
;*  15: |******          |                 |        |       |       |
;*  16: | ** **          |                 |        |       |       |
;*  17: | ** **          |                 |        |       |       |
;*  18: | ** **          |                 |        |       |       |
;*  19: | ** **          |                 |        |       |       |
;*  20: | ** **          |                 |        |       |       |
;*  21: |******          |                 |        |       |       |
;*  22: |******          |                 |        |       |       |
;*  23: | ** **          |                 |        |       |       |
;*  24: | ** **          |                 |        |       |       |
;*  25: | ** **          |                 |        |       |       |
;*  26: | ** **          |                 |        |       |       |
;*  27: | ** **          |                 |        |       |       |
;*  28: | ** **          |                 |        |       |       |
;*  29: |*** **          |                 |        |       |       |
;*  30: |* * **          |                 |        |       |       |
;*  31: |* * **          |                 |        |       |       |
;*  32: |* * **          |                 |        |       |       |
;*  33: |* * **          |                 |        |       |       |
;*  34: |* * **          |                 |        |       |       |
;*  35: |* * **          |                 |        |       |       |
;*  36: |* * **          |                 |        |       |       |
;*  37: |* * **          |                 |        |       |       |
;*      +----------------+                 +--------+       +-------+
;*
;*      Done
;*
;*      Collapsed epilog stages       : 1
;*      Prolog not removed
;*      Collapsed prolog stages       : 0
;*
;*      Max amt of load speculation   : 0 bytes
;*
;*      Minimum safe trip count       : 1
;*      Min. prof. trip count  (est.) : 2
;*
;*      Mem bank conflicts/iter(est.) : { min 0.000, est 0.000, max 0.000 }
;*      Mem bank perf. penalty (est.) : 0.0%
;*
;*
;*      Total cycles (est.)         : 1 + trip_cnt * 38        
;*----------------------------------------------------------------------------*
;*        SINGLE SCHEDULED ITERATION
;*
;*        ||$C$C729||:
;*   0              TICK                               ; [A_U] 
;*   1              LDD     .D1     *D2(1176),D0      ; [A_D1] |368|  ^ [C1]
;*   2              NOP     0x5     ; [A_B] 
;*   7              ADDD    .D1     D4,D0,D0          ; [A_D1] |368|  ^ 
;*   8              LDW     .D1     *D0(0),A0         ; [A_D1] |368|  ^ 
;*   9              NOP     0x1     ; [A_B] 
;*  10              LDD     .D2     *D2(1200),D0      ; [A_D2] |371| [C0]
;*     ||           LDD     .D1     *D2(1192),D3      ; [A_D1] |371| [C1]
;*  11              NOP     0x3     ; [A_B] 
;*  14              SHRW    .L1     A0,0x10,D0        ; [A_L1] |371|  ^ 
;*  15              ANDW    .D1     D5,D0,D1          ; [A_D1] |371|  ^ 
;*  16              LDW     .D2     *D0[D1],D0        ; [A_D2] |371|  ^ 
;*     ||           LDW     .D1     *D3[D1],D3        ; [A_D1] |371|  ^ 
;*  17              LDD     .D1     *D2(1184),D3      ; [A_D1] |371| [C1]
;*  18              NOP     0x4     ; [A_B] 
;*  22              ADDW    .D1     D3,D0,D0          ; [A_D1] |371|  ^ 
;*  23              STW     .D1     A0,*D3[D0]        ; [A_D1] |371|  ^ 
;*  24              LDD     .D1     *D2(1192),D0      ; [A_D1] |372|  ^ [C1]
;*  25              NOP     0x5     ; [A_B] 
;*  30              ADDAW   .D1     D0,D1,D0          ; [A_D1] |372|  ^ 
;*  31              LDW     .D1     *D0(0),BL0        ; [A_D1] |372|  ^ 
;*  32              NOP     0x5     ; [A_B] 
;*  37              ADDW    .L2     BL0,0x1,B0        ; [B_L2] |372|  ^ 
;*  38              STW     .D1X    B0,*D0(0)         ; [A_D1] |372|  ^ 
;*     ||           ADDD    .D2     D4,0x4,D4         ; [A_D2] |366| 
;*     ||           BNL     .B1     ||$C$C729||       ; [A_B] |366| 
;*  39              ; BRANCHCC OCCURS {||$C$C729||}   ; [] |366| 
;*----------------------------------------------------------------------------*
||$C$L6||:    ; PIPED LOOP PROLOG
;** --------------------------------------------------------------------------*
||$C$L7||:    ; PIPED LOOP KERNEL
;          EXCLUSIVE CPU CYCLES: 38
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 368,column 24,is_stmt,isa 0
           LDD     .D1     *D2(1176),D0      ; [A_D1] |368| <0,1>  ^ [C1]
           NOP             0x5               ; [A_B] 
           ADDD    .D1     D4,D0,D0          ; [A_D1] |368| <0,7>  ^ 
           LDW     .D1     *D0(0),A0         ; [A_D1] |368| <0,8>  ^ 
           NOP             0x1               ; [A_B] 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 371,column 7,is_stmt,isa 0

           LDD     .D2     *D2(1200),D0      ; [A_D2] |371| <0,10> [C0]
||         LDD     .D1     *D2(1192),D3      ; [A_D1] |371| <0,10> [C1]

           NOP             0x3               ; [A_B] 
           SHRW    .L1     A0,0x10,D0        ; [A_L1] |371| <0,14>  ^ 
           ANDW    .D1     D5,D0,D1          ; [A_D1] |371| <0,15>  ^ 

           LDW     .D2     *D0[D1],D0        ; [A_D2] |371| <0,16>  ^ 
||         LDW     .D1     *D3[D1],D3        ; [A_D1] |371| <0,16>  ^ 

           LDD     .D1     *D2(1184),D3      ; [A_D1] |371| <0,17> [C1]
           NOP             0x4               ; [A_B] 
           ADDW    .D1     D3,D0,D0          ; [A_D1] |371| <0,22>  ^ 
           STW     .D1     A0,*D3[D0]        ; [A_D1] |371| <0,23>  ^ 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 372,column 7,is_stmt,isa 0
           LDD     .D1     *D2(1192),D0      ; [A_D1] |372| <0,24>  ^ [C1]
           NOP             0x5               ; [A_B] 
           ADDAW   .D1     D0,D1,D0          ; [A_D1] |372| <0,30>  ^ 
           LDW     .D1     *D0(0),BL0        ; [A_D1] |372| <0,31>  ^ 
           NOP             0x5               ; [A_B] 
           ADDW    .L2     BL0,0x1,B0        ; [B_L2] |372| <0,37>  ^ 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 366,column 25,is_stmt,isa 0

           ADDD    .D2     D4,0x4,D4         ; [A_D2] |366| <0,38> 
||         BNL     .B1     ||$C$L7||         ; [A_B] |366| <0,38> 
||         STW     .D1X    B0,*D0(0)         ; [A_D1] |372| <0,38>  ^ 
||         TICK                               ; [A_U] <1,0> 

;** --------------------------------------------------------------------------*
||$C$L8||:    ; PIPED LOOP EPILOG
;          EXCLUSIVE CPU CYCLES: 1
           PROTCLR                            ; [A_U] 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 1
           MV      .D1     D2,A5             ; [A_D1] 
;** --------------------------------------------------------------------------*
||$C$L9||:    
;          EXCLUSIVE CPU CYCLES: 8
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 377,column 3,is_stmt,isa 0
           LDW     .D1     *A6(68),AL0       ; [A_D1] |377| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 374,column 5,is_stmt,isa 0

           LDW     .D1     *A6(12),B2        ; [A_D1] |374| 
||         LDW     .D2     *A5(1328),BL0     ; [A_D2] |374| 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 377,column 3,is_stmt,isa 0
           CMPEQW  .L1     AL0,0x1,A1        ; [A_L1] |377| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 374,column 5,is_stmt,isa 0

   [ A1]   B       .B1     ||$C$L10||        ; [A_B] |377| 
||         SUBW    .L2     B2,BL0,B0         ; [B_L2] |374| 
||         CMPEQW  .L1     AL0,0x2,A0        ; [A_L1] |466| 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 377,column 3,is_stmt,isa 0
           ; BRANCHCC OCCURS {||$C$L10||}    ; [] |377| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 1
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 466,column 8,is_stmt,isa 0

   [ A0]   B       .B1     ||$C$L25||        ; [A_B] |466| 
||         MVKU32  .L1     0,A4              ; [A_L1] |580| 

           ; BRANCHCC OCCURS {||$C$L25||}    ; [] |466| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 1
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 580,column 3,is_stmt,isa 0
           B       .B1     ||$C$L49||        ; [A_B] |580| 
           ; BRANCH OCCURS {||$C$L49||}      ; [] |580| 
;** --------------------------------------------------------------------------*
||$C$L10||:    
;          EXCLUSIVE CPU CYCLES: 8
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 379,column 5,is_stmt,isa 0
           LDW     .D1     *A6(72),AL0       ; [A_D1] |379| 

           CMPEQW  .L1     AL0,0x4,A1        ; [A_L1] |379| 
||         CMPGTW  .S1X    B0,0,A0           ; [A_S1] |381| 

   [!A1]   B       .B1     ||$C$L20||        ; [A_B] |379| 
||         CMPGTW  .L1X    B0,0,A2           ; [A_L1] |413| 

           ; BRANCHCC OCCURS {||$C$L20||}    ; [] |379| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 451,column 27,is_stmt,isa 0

           CMPGTW  .L1X    B2,0,A0           ; [A_L1] |451| 
||         MVKU32  .S1     0,D2              ; [A_S1] 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 413,column 29,is_stmt,isa 0

   [!A2]   B       .B1     ||$C$L11||        ; [A_B] |413| 
||         MV      .D1     D2,D1             ; [A_D1] 
||         MV      .S1X    B0,A1             ; [A_S1] 
||         MVKU32  .L1     0,A4              ; [A_L1] 

           ; BRANCHCC OCCURS {||$C$L11||}    ; [] |413| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 8

           MVKU32  .L2     0xff,B14          ; [B_L2] 
||         MVKU32  .S2     0xffff,BL7        ; [B_S2] 

           MVKU32  .L2     0x37800000,B10    ; [B_L2] 
||         MVKU32  .S2     0x3d2aaab4,BM3    ; [B_S2] 

           MVKU32  .L2     0x3e2aaaad,BM1    ; [B_L2] 
||         MVKU32  .S2     0x3f000000,BM0    ; [B_S2] 

           MVKU32  .L2     0x10000,BL0       ; [B_L2] 
||         MVKU32  .S2     0x3f800000,B9     ; [B_S2] 

           MVKU32  .L2     0,B2              ; [B_L2] 
||         MVKU32  .S2     0x3f317218,BM2    ; [B_S2] 

           MVKU32  .L2     0x7f7fffff,B0     ; [B_L2] 
||         MVKU32  .S2     0x3fb8aa3b,BM4    ; [B_S2] 

	.dwpsn	file "./inc/tidl_detectionOutput_int.h",line 229,column 1,is_stmt,isa 0

           MVKU32  .L2     0x40000000,B8     ; [B_L2] 
||         MVKU32  .S2     0x46fffe00,B13    ; [B_S2] 
||         MASKB   .P2     0x4,P2            ; [B_P] |229| 

	.dwpsn	file "./inc/tidl_detectionOutput_int.h",line 134,column 25,is_stmt,isa 0

           B       .B1     ||$C$L12||        ; [A_B] |413| 
||         MVKU32  .L1     0xc,AM0           ; [A_L1] |134| 
||         MVKU32  .L2     0,BL5             ; [B_L2] |229| 
||         MVKU32  .S2     0,BL1             ; [B_S2] |229| 
||         MASKB   .P2     0x4,P1            ; [B_P] |229| 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 413,column 29,is_stmt,isa 0
           ; BRANCH OCCURS {||$C$L12||}      ; [] |413| 
;** --------------------------------------------------------------------------*
||$C$L11||:    
;          EXCLUSIVE CPU CYCLES: 1
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 451,column 27,is_stmt,isa 0
   [ A0]   B       .B1     ||$C$L15||        ; [A_B] |451| 
           ; BRANCHCC OCCURS {||$C$L15||}    ; [] |451| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 1
           B       .B1     ||$C$L19||        ; [A_B] |451| 
           ; BRANCH OCCURS {||$C$L19||}      ; [] |451| 
;** --------------------------------------------------------------------------*
;**   BEGIN LOOP ||$C$L12||
;** --------------------------------------------------------------------------*
||$C$L12||:    
;          EXCLUSIVE CPU CYCLES: 15
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 417,column 31,is_stmt,isa 0
           LDD     .D1     *A5(1192),D3      ; [A_D1] |417| 
           ADDD    .D1     D2,D3,D3          ; [A_D1] |417| 
           LDW     .D1     *D3(0),A0         ; [A_D1] |417| 
           CMPGTW  .L1     A0,0,A2           ; [A_L1] |417| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 416,column 9,is_stmt,isa 0

   [!A2]   B       .B1     ||$C$L14||        ; [A_B] |417| 
||         STW     .D1X    B1,*D3(0)         ; [A_D1] |416| 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 417,column 31,is_stmt,isa 0
           ; BRANCHCC OCCURS {||$C$L14||}    ; [] |417| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 1
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 417,column 26,is_stmt,isa 0

           ADDD    .D1     A5,0x140,D3       ; [A_D1] 
||         ADDD    .D2     A5,0x244,D4       ; [A_D2] 
||         MVKU32  .L1     0,A7              ; [A_L1] |417| 

;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*
;*      Loop found in file               : src/tidl_detectionOutput_score.c
;*      Loop source line                 : 417
;*      Loop opening brace source line   : 418
;*      Loop closing brace source line   : 449
;*      Known Minimum Trip Count         : 1                    
;*      Known Max Trip Count Factor      : 1
;*      Loop Carried Dependency Bound(^) : 165
;*      Unpartitioned Resource Bound     : 12
;*      Partitioned Resource Bound       : 16 (pre-sched)
;*      Disqualified loop: Loop carried dependency bound too large
;*----------------------------------------------------------------------------*
||$C$L13||:    
;          EXCLUSIVE CPU CYCLES: 166
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 419,column 11,is_stmt,isa 0
           LDD     .D1     *A5(1200),D0      ; [A_D1] |419| 
           ADDD    .D1     D2,D0,D0          ; [A_D1] |419| 
           LDW     .D1     *D0(0),D0         ; [A_D1] |419| 
           LDD     .D1     *A5(1184),D5      ; [A_D1] |419| 
           ADDW    .D1     A7,D0,D0          ; [A_D1] |419| 
           LDW     .D1     *D5[D0],B3        ; [A_D1] |419| 
	.dwpsn	file "./inc/tidl_detectionOutput_int.h",line 134,column 25,is_stmt,isa 0
           SHRW    .L2     B3,0x1c,BL2       ; [B_L2] |134| 

           ANDW    .L2     BL2,0xf,B4        ; [B_L2] |134| 
||         LDW     .D1     *A6(12),B12       ; [A_D1] |134| 

           EXT     .L1X    B4,0x20,0x20,A2   ; [A_L1] |134| 
           MPYDD   .N1     AM0,A2,D0         ; [A_N1] |134| 
           SHRW    .L2     B3,0x18,BL3       ; [B_L2] |134| 

           ADDW    .L2     B12,0x5,B4        ; [B_L2] |134| 
||         ANDW    .S2     BL3,0xf,B5        ; [B_S2] |134| 

           MPYWW   .N2     B5,B4,BL2         ; [B_N2] |134| 
||         ADDD    .D1     D4,D0,D0          ; [A_D1] |134| 

           LDW     .D1     *D0(0),AM1        ; [A_D1] |134| 
           SHRW    .L2     B3,0x10,BL6       ; [B_L2] |134| 
           ANDW    .L2     B14,BL6,BL3       ; [B_L2] |134| 
           ADDW    .L2     BL3,BL2,B6        ; [B_L2] |134| 
           SHLD    .L1     A2,0x3,D14        ; [A_L1] |134| 

           ADDW    .L1X    B6,0x1,AM3        ; [A_L1] |134| 
||         ADDD    .D1     A5,D14,D0         ; [A_D1] |134| 

           LDD     .D1     *D0(192),D0       ; [A_D1] |134| 
||         MPYWW   .N1X    B5,AM1,AM2        ; [A_N1] |134| 

           MPYWW   .N1     AM1,AM3,AM1       ; [A_N1] |134| 
           ANDW    .L2     BL7,B3,B11        ; [B_L2] |134| 
           MPYWW   .N1X    B4,AM2,AM2        ; [A_N1] |134| 
           ADDW    .M1X    B11,AM1,D5        ; [A_M1] |134| 
           LDH     .D1     *D0[D5],BL2       ; [A_D1] |134| 
           SHRW    .L1X    B3,0x1c,AL0       ; [A_L1] |134| 

           ADDW    .M1X    B11,AM2,D5        ; [A_M1] |134| 
||         ANDW    .L1     AL0,0xf,D6        ; [A_L1] |134| 

           LDH     .D1     *D0[D5],BL3       ; [A_D1] |134| 
||         LDUW    .D2     *D3[D6],B7        ; [A_D2] |134| 

           VINTSP  .L2     BL2,BM5           ; [B_L2] |134| 

           MPYSP   .N2     B7,BM5,BL3        ; [B_N2] |134| 
||         VINTSP  .L2     BL3,BM5           ; [B_L2] |134| 

           MPYSP   .N2     B7,BM5,BL2        ; [B_N2] |134| 
           VXORW   .L2     BL3,0x80000000,BM5 ; [B_L2] |134| 
           MPYSP   .N2     BM4,BM5,B7        ; [B_N2] |134| 
           VXORW   .L2     BL2,0x80000000,BM5 ; [B_L2] |134| 
           MPYSP   .N2     BM4,BM5,B6        ; [B_N2] |134| 
	.dwpsn	file "./inc/tidl_detectionOutput_int.h",line 229,column 1,is_stmt,isa 0
           VSPTRUNC .L2    B7,B4             ; [B_L2] |229| 

           VSPTRUNC .L2    B6,B5             ; [B_L2] |229| 
||         VINTSP  .S2     B4,BM5            ; [B_S2] |229| 

           VINTSP  .L2     B5,BM5            ; [B_L2] |229| 
||         SUBSP   .C2     B7,BM5,BM6        ; [B_C] |229| 

           SUBSP   .C2     B6,BM5,BM5        ; [B_C] |229| 
||         MPYSP   .N2     BM2,BM6,BM7       ; [B_N2] |229| 

           MPYSP   .N2     BM2,BM5,B6        ; [B_N2] |229| 
           MPYSP   .N2     BM7,BM7,BM6       ; [B_N2] |229| 

           MPYSP   .N2     B6,B6,BM5         ; [B_N2] |229| 
||         CMPGEW  .L1X    B4,0xfffffff0,A3  ; [A_L1] |229| 

   [ A3]   MPYSP   .N2     BM7,BM6,B11       ; [B_N2] |229| 
   [ A3]   MPYSP   .N2     BM0,BM6,B7        ; [B_N2] |229| 

   [ A3]   ADDSP   .C2     B9,BM7,BM7        ; [B_C] |229| 
||         CMPGEW  .L1X    B5,0xfffffff0,A2  ; [A_L1] |229| 

   [ A2]   MPYSP   .N2     B6,BM5,BM6        ; [B_N2] |229| 
|| [ A3]   MPYSP   .M2     BM6,BM6,B12       ; [B_M2] |229| 

   [ A2]   MPYSP   .N2     BM0,BM5,BL2       ; [B_N2] |229| 
|| [ A3]   MPYSP   .M2     BM1,B11,B11       ; [B_M2] |229| 

   [ A3]   ADDSP   .C2     B7,BM7,BM7        ; [B_C] |229| 
|| [ A2]   ADDSP   .L2     B9,B6,BL3         ; [B_L2] |229| 

   [ A2]   MPYSP   .N2     BM5,BM5,BM5       ; [B_N2] |229| 

   [ A3]   MPYSP   .N2     BM3,B12,BM6       ; [B_N2] |229| 
|| [ A2]   MPYSP   .M2     BM1,BM6,BL4       ; [B_M2] |229| 

   [ A3]   ADDSP   .C2     B11,BM7,BM7       ; [B_C] |229| 
||         SUBRW   .M2     B4,0,BL3          ; [B_M2] |229| 
|| [ A2]   ADDSP   .S2     BL2,BL3,BL2       ; [B_S2] |229| 
||         VCMPGTW .L2     B4,BL5,P0         ; [B_L2] |229| 

   [ A3]   SHRW    .L2     BL0,BL3,BL3       ; [B_L2] |229| 
|| [ A3]   AND     .P2     P0,P2,P0          ; [B_P] |229| 
||         SHLW    .S2     BL0,B4,BL6        ; [B_S2] |229| 

   [ A3]   VSEL    .L2     P0,BL6,BL3,B6     ; [B_L2] |229| 
|| [ A2]   MPYSP   .N2     BM3,BM5,BM5       ; [B_N2] |229| 

           SUBRW   .M2     B5,0,BL2          ; [B_M2] |229| 
|| [ A2]   ADDSP   .S2     BL4,BL2,BM6       ; [B_S2] |229| 
|| [ A3]   ADDSP   .C2     BM6,BM7,B7        ; [B_C] |229| 
||         VCMPGTW .L2     B5,BL1,P0         ; [B_L2] |229| 

   [ A2]   SHRW    .L2     BL0,BL2,BL2       ; [B_L2] |229| 
|| [ A2]   AND     .P2     P0,P1,P0          ; [B_P] |229| 
||         SHLW    .S2     BL0,B5,BL3        ; [B_S2] |229| 

   [ A2]   VSEL    .L2     P0,BL3,BL2,BL2    ; [B_L2] |229| 
|| [ A3]   VINTSP  .L1X    B6,AM1            ; [A_L1] |229| 

   [ A2]   VINTSP  .L2     BL2,BM5           ; [B_L2] |229| 
|| [ A2]   ADDSP   .C2     BM5,BM6,BM6       ; [B_C] |229| 

   [ A3]   MPYSP   .N1X    B7,AM1,AM1        ; [A_N1] |229| 
   [ A2]   MPYSP   .N2     BM6,BM5,BM5       ; [B_N2] |229| 
   [ A3]   MPYSP   .N1X    B10,AM1,D0        ; [A_N1] |229| 

   [ A2]   MPYSP   .N2     B10,BM5,BL2       ; [B_N2] |229| 
||         CMPGTW  .L1X    B4,0xe,A4         ; [A_L1] |229| 

   [ A4]   MV      .L1X    B0,AL0            ; [A_L1] |229| 
   [!A3]   MV      .L1X    B2,D0             ; [A_L1] |229| 

           CMPGTW  .L1X    B5,0xe,A2         ; [A_L1] |229| 
|| [!A4]   MV      .D1     D0,AL0            ; [A_D1] |229| 
|| [!A2]   MV      .L2     B2,BL2            ; [B_L2] |229| 

   [!A2]   MV      .L2     BL2,BM5           ; [B_L2] |229| 
|| [ A2]   MV      .S2     B0,BM5            ; [B_S2] |229| 
||         ADDSP   .L1X    B9,AL0,A3         ; [A_L1] |229| 

           ADDSP   .C2     B9,BM5,B4         ; [B_C] |229| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 441,column 11,is_stmt,isa 0
           VRCPSP  .S1     A3,AM1            ; [A_S1] |441| 
           VRCPSP  .S2     B4,BM5            ; [B_S2] |441| 
           MPYSP   .N1     A3,AM1,AL0        ; [A_N1] |441| 
           MPYSP   .N2     B4,BM5,BM6        ; [B_N2] |441| 
           SUBSP   .L1X    B8,AL0,AM2        ; [A_L1] |441| 
           SUBSP   .C2     B8,BM6,BM6        ; [B_C] |441| 
           MPYSP   .N1     AM1,AM2,AM1       ; [A_N1] |441| 
           MPYSP   .N2     BM5,BM6,BM5       ; [B_N2] |441| 
           MPYSP   .N1     A3,AM1,AL0        ; [A_N1] |441| 
           MPYSP   .N2     B4,BM5,BM6        ; [B_N2] |441| 
           SUBSP   .L1X    B8,AL0,AM2        ; [A_L1] |441| 
           SUBSP   .C2     B8,BM6,BM6        ; [B_C] |441| 
           MPYSP   .N1     AM1,AM2,A2        ; [A_N1] |441| 
           MPYSP   .N2     BM5,BM6,BM5       ; [B_N2] |441| 

           MPYSP   .N2X    A2,BM5,B4         ; [B_N2] |441| 
||         LDUW    .D1     *A6(24),AL1       ; [A_D1] |441| 

           MV      .L1X    B4,AL0            ; [A_L1] |441| Define a twin register
           CMPLESP .L1     AL1,AL0,A2        ; [A_L1] |441| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 444,column 13,is_stmt,isa 0
   [ A2]   MPYSP   .N2     B13,B4,BL3        ; [B_N2] |444| 
   [ A2]   LDD     .D1     *A5(1152),D5      ; [A_D1] |444| 
   [ A2]   VSPTRUNC .L2    BL3,B4            ; [B_L2] |444| 
   [ A2]   STH     .D1X    B4,*D5[D1]        ; [A_D1] |444| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 445,column 13,is_stmt,isa 0
   [ A2]   LDD     .D1     *A5(1176),D5      ; [A_D1] |445| 
   [ A2]   STW     .D1X    B3,*D5[D1]        ; [A_D1] |445| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 446,column 13,is_stmt,isa 0
   [ A2]   LDD     .D1     *A5(1192),D5      ; [A_D1] |446| 
   [ A2]   ADDD    .D1     D2,D5,D5          ; [A_D1] |446| 
   [ A2]   LDW     .D1     *D5(0),BL3        ; [A_D1] |446| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 417,column 31,is_stmt,isa 0

   [ A2]   ADDW    .L2     BL3,0x1,B3        ; [B_L2] |446| 
||         ADDW    .D1     A0,0xffffffff,A0  ; [A_D1] |417| 

   [ A0]   B       .B1     ||$C$L13||        ; [A_B] |417| 
|| [ A2]   STW     .D1X    B3,*D5(0)         ; [A_D1] |446| 
|| [ A2]   ADDW    .D2     D1,0x1,D1         ; [A_D2] |447| 
||         ADDW    .L1     A7,0x1,A7         ; [A_L1] |417| 

           ; BRANCHCC OCCURS {||$C$L13||}    ; [] |417| 
;** --------------------------------------------------------------------------*
||$C$L14||:    
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 413,column 29,is_stmt,isa 0
           ADDW    .D1     A1,0xffffffff,A1  ; [A_D1] |413| 

   [ A1]   B       .B1     ||$C$L12||        ; [A_B] |413| 
||         ADDD    .D1     D2,0x4,D2         ; [A_D1] |413| 

           ; BRANCHCC OCCURS {||$C$L12||}    ; [] |413| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 8
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 451,column 27,is_stmt,isa 0
           LDW     .D1     *A6(12),AL0       ; [A_D1] |451| 
           CMPGTW  .L1     AL0,0,A0          ; [A_L1] |451| 

   [!A0]   B       .B1     ||$C$L19||        ; [A_B] |451| 
||         MV      .D1     D1,A4             ; [A_D1] 

           ; BRANCHCC OCCURS {||$C$L19||}    ; [] |451| 
;** --------------------------------------------------------------------------*
||$C$L15||:    
;          EXCLUSIVE CPU CYCLES: 2
           MV      .D1     A5,D1             ; [A_D1] 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 451,column 22,is_stmt,isa 0

           SLDD    .D1     *D1(1200),D2      ; [A_D1] <0,0>  ^ [C0]
||         UNPROT          0x1               ; [A_U] 
||         MVKU32  .L1     0,A8              ; [A_L1] |451| 
||         MV      .D2     A6,D0             ; [A_D2] 
||         MVKU32  .S1     0,A1              ; [A_S1] 

;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*
;*      Loop found in file               : src/tidl_detectionOutput_score.c
;*      Loop source line                 : 451
;*      Loop opening brace source line   : 452
;*      Loop closing brace source line   : 461
;*      Known Minimum Trip Count         : 1                    
;*      Known Max Trip Count Factor      : 1
;*      Loop Carried Dependency Bound(^) : 16
;*      Unpartitioned Resource Bound     : 4
;*      Partitioned Resource Bound       : 4 (pre-sched)
;*
;*      Searching for software pipeline schedule at ...
;*         ii = 16 Unsafe schedule for irregular loop
;*         ii = 16 Unsafe schedule for irregular loop
;*         ii = 16 Did not find schedule
;*         ii = 17 Schedule found with 2 iterations in parallel
;*
;*      Partitioned Resource Bound(*)    : 6 (post-sched)
;*
;*      Constant Extension #0 Used [C0]  : 1
;*      Constant Extension #1 Used [C1]  : 1
;*
;*      Resource Partition (may include "post-sched" split/spill moves):
;*
;*                                                A-side   B-side
;*      .L units                                     0        0     
;*      .S units                                     0        0     
;*      .M units                                     0        0     
;*      .N units                                     0        0     
;*      .D units                                     7        -     
;*      .B units                                     1        -     
;*      .C units                                     -        0     
;*      .P units                                     -        0     
;*
;*      .M/.N units                                  0        0     
;*      .L/.S units                                  1        0     
;*      .L/.S/.C units                               0        0     
;*      .L/.S/.C/.M units                            0        0     
;*      .L/.S/.C/.M/.D units                         6        1     
;*
;*      .X cross paths                               1        0     
;*
;*      Bound(.D1 .D2 .D)                            4        -     
;*      Bound(.M .N .MN)                             0        0     
;*      Bound(.L .S .LS)                             1        0     
;*      Bound(.L .S .C .LS .LSC)                     1        0     
;*      Bound(.L .S .C .M .LS .LSC .LSCM)            1        0     
;*      Bound(.L .S .C .M .D .LS .LSC .LSCM .LSCMD)  3        1     
;*
;*      Register Usage Tables:
;*
;*      +----------------------------------+----------------------------------+
;*      |              ASIDE               |              BSIDE               |
;*      +----------------+--------+--------+----------------+--------+--------+
;*      |      "Axx"     |  "ALx" |  "AMx" |     "VBxx"     | "VBLx" | "VBMx" |
;*      +----------------|--------|--------|----------------|--------|--------+
;*      |0000000000111111|00000000|00000000|0000000000111111|00000000|00000000|
;*      |0123456789012345|01234567|01234567|0123456789012345|01234567|01234567|
;*      +----------------|--------|--------|----------------|--------|--------+
;*   0: | *      *       |        |        | *              |        |        |
;*   1: | *      *       |        |        | *              |        |        |
;*   2: | *      *       |        |        | *              |        |        |
;*   3: | *      *       |        |        | *              |        |        |
;*   4: | *      *       |        |        | *              |        |        |
;*   5: | *      *       |        |        | *              |        |        |
;*   6: | *      *       |        |        | *              |        |        |
;*   7: | *      *       |        |        | *              |**      |        |
;*   8: | *      *       |        |        |**              |        |        |
;*   9: | *      *       |        |        | *              |        |        |
;*  10: | *      *       |        |        | *              |        |        |
;*  11: | *      *       |        |        | *              |        |        |
;*  12: | *      *       |        |        | *              |        |        |
;*  13: | *      *       |        |        | *              |        |        |
;*  14: | *      *       |        |        | *              |        |        |
;*  15: | *      *       |*       |        | *              |        |        |
;*  16: |**      *       |        |        | *              |        |        |
;*      +----------------+--------+--------+----------------+--------+--------+
;*
;*      +----------------+                 +--------+       +-------+
;*      |      "Dxx"     |                 |  "Px"  |       |"CUCRx"|
;*      +----------------+                 +--------+       +-------+
;*      |0000000000111111|                 |00000000|       |0000   |
;*      |0123456789012345|                 |01234567|       |0123   |
;*      +----------------+                 +--------+       +-------+
;*   0: |*** * *         |                 |        |       |       |
;*   1: |*******         |                 |        |       |       |
;*   2: |*** * *         |                 |        |       |       |
;*   3: |*** * *         |                 |        |       |       |
;*   4: |*** * *         |                 |        |       |       |
;*   5: |*** * *         |                 |        |       |       |
;*   6: |*** * *         |                 |        |       |       |
;*   7: |*** * *         |                 |        |       |       |
;*   8: |*** * *         |                 |        |       |       |
;*   9: |**  * *         |                 |        |       |       |
;*  10: |**  * *         |                 |        |       |       |
;*  11: |**  * *         |                 |        |       |       |
;*  12: |**  * *         |                 |        |       |       |
;*  13: |**  * *         |                 |        |       |       |
;*  14: |**  * *         |                 |        |       |       |
;*  15: |**  * *         |                 |        |       |       |
;*  16: |***** *         |                 |        |       |       |
;*      +----------------+                 +--------+       +-------+
;*
;*      Done
;*
;*      Collapsed epilog stages       : 1
;*      Prolog not removed
;*      Collapsed prolog stages       : 0
;*
;*      Max amt of load speculation   : 0 bytes
;*
;*      Minimum safe trip count       : 1
;*      Min. prof. trip count  (est.) : 2
;*
;*      Mem bank conflicts/iter(est.) : { min 0.000, est 0.000, max 0.000 }
;*      Mem bank perf. penalty (est.) : 0.0%
;*
;*
;*      Total cycles (est.)         : 7 + trip_cnt * 17        
;*----------------------------------------------------------------------------*
;*        SINGLE SCHEDULED ITERATION
;*
;*        ||$C$C474||:
;*   0              SLDD    .D2     *D1(1200),D2      ; [A_D2]  ^ [C0]
;*     ||   [ A1]   SLDD    .D1     *D1(1192),D3      ; [A_D1] |459|  ^ [C1]
;*   1              NOP     0x5     ; [A_B] 
;*   6      [ A1]   ADDD    .D2     D2,A1,D4          ; [A_D2] |459|  ^ 
;*     ||   [ A1]   ADDD    .D1     A1,D3,D6          ; [A_D1] |459|  ^ 
;*   7      [ A1]   ADDD    .D2     D4,0xfffffffc,D3  ; [A_D2] |459|  ^ 
;*     ||   [ A1]   ADDD    .D1     D6,0xfffffffc,D5  ; [A_D1] |459|  ^ 
;*   8      [ A1]   LDW     .D2     *D3(0),BL0        ; [A_D2] |459|  ^ 
;*     ||   [ A1]   LDW     .D1     *D5(0),BL1        ; [A_D1] |459|  ^ 
;*   9              NOP     0x5     ; [A_B] 
;*  14      [ A1]   ADDW    .L2     BL1,BL0,B0        ; [B_L2] |459|  ^ 
;*  15      [!A1]   STW     .D2     B1,*D2[A8]        ; [A_D2] |455| 
;*     ||   [ A1]   STW     .D1X    B0,*D2[A8]        ; [A_D1] |459|  ^ 
;*  16              ADDD    .D2     A1,0x4,A1         ; [A_D2] |451|  ^ 
;*     ||           LDW     .D1     *D0(12),AL0       ; [A_D1] |451| 
;*  17              NOP     0x4     ; [A_B] 
;*  21              ADDW    .D1     A8,0x1,A8         ; [A_D1] |451| 
;*  22              CMPGTW  .L1     AL0,A8,A0         ; [A_L1] |451| 
;*  23      [ A0]   B       .B1     ||$C$C474||       ; [A_B] |451| 
;*  24              ; BRANCHCC OCCURS {||$C$C474||}   ; [] |451| 
;*----------------------------------------------------------------------------*
||$C$L16||:    ; PIPED LOOP PROLOG
;** --------------------------------------------------------------------------*
||$C$L17||:    ; PIPED LOOP KERNEL
;          EXCLUSIVE CPU CYCLES: 17
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 459,column 11,is_stmt,isa 0

   [ A1]   ADDD    .D2     D4,0xfffffffc,D3  ; [A_D2] |459| <0,7>  ^ 
|| [ A1]   ADDD    .D1     D6,0xfffffffc,D5  ; [A_D1] |459| <0,7>  ^ 

   [ A1]   LDW     .D2     *D3(0),BL0        ; [A_D2] |459| <0,8>  ^ 
|| [ A1]   LDW     .D1     *D5(0),BL1        ; [A_D1] |459| <0,8>  ^ 

           NOP             0x5               ; [A_B] 
   [ A1]   ADDW    .L2     BL1,BL0,B0        ; [B_L2] |459| <0,14>  ^ 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 455,column 11,is_stmt,isa 0

   [!A1]   STW     .D2     B1,*D2[A8]        ; [A_D2] |455| <0,15> 
|| [ A1]   STW     .D1X    B0,*D2[A8]        ; [A_D1] |459| <0,15>  ^ 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 451,column 27,is_stmt,isa 0

           ADDD    .D2     A1,0x4,A1         ; [A_D2] |451| <0,16>  ^ 
||         LDW     .D1     *D0(12),AL0       ; [A_D1] |451| <0,16> 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 459,column 11,is_stmt,isa 0

           SLDD    .D2     *D1(1200),D2      ; [A_D2] <1,0>  ^ [C0]
|| [ A1]   SLDD    .D1     *D1(1192),D3      ; [A_D1] |459| <1,0>  ^ [C1]

           NOP             0x3               ; [A_B] 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 451,column 27,is_stmt,isa 0
           ADDW    .D1     A8,0x1,A8         ; [A_D1] |451| <0,21> 
           CMPGTW  .L1     AL0,A8,A0         ; [A_L1] |451| <0,22> 

   [ A0]   B       .B1     ||$C$L17||        ; [A_B] |451| <0,23> 
|| [ A1]   ADDD    .D2     D2,A1,D4          ; [A_D2] |459| <1,6>  ^ 
|| [ A1]   ADDD    .D1     A1,D3,D6          ; [A_D1] |459| <1,6>  ^ 

;** --------------------------------------------------------------------------*
||$C$L18||:    ; PIPED LOOP EPILOG
;          EXCLUSIVE CPU CYCLES: 1
           PROTCLR                            ; [A_U] 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 1
           MV      .D1     D1,A5             ; [A_D1] 
;** --------------------------------------------------------------------------*
||$C$L19||:    
;          EXCLUSIVE CPU CYCLES: 7
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 462,column 7,is_stmt,isa 0

           LDD     .D1     *A5(1176),B1      ; [A_D1] |462| 
||         LDD     .D2     *A5(1152),B0      ; [A_D2] |463| 

           B       .B1     ||$C$L49||        ; [A_B] |580| 
||         STD     .D1X    B1,*A5(1184)      ; [A_D1] |462| 
||         STD     .D2     B0,*A5(1160)      ; [A_D2] |463| 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 580,column 3,is_stmt,isa 0
           ; BRANCH OCCURS {||$C$L49||}      ; [] |580| 
;** --------------------------------------------------------------------------*
||$C$L20||:    
;          EXCLUSIVE CPU CYCLES: 1
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 381,column 29,is_stmt,isa 0

   [!A0]   B       .B1     ||$C$L24||        ; [A_B] |381| 
||         MVKU32  .L1     0,D2              ; [A_L1] 
||         MV      .S1X    B0,A1             ; [A_S1] 

           ; BRANCHCC OCCURS {||$C$L24||}    ; [] |381| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 8
           MVKU32  .L1     0xff,D3           ; [A_L1] 

           MVKU32  .L2     0xffff,BL4        ; [B_L2] 
||         MVKU32  .S2     0x37800000,BM7    ; [B_S2] 

           MVKU32  .L2     0x3d2aaab4,BM6    ; [B_L2] 
||         MVKU32  .S2     0x3e2aaaad,BM4    ; [B_S2] 

           MVKU32  .L2     0x10000,BL0       ; [B_L2] 
||         MVKU32  .S2     0x3f000000,BM3    ; [B_S2] 

           ADDD    .D1     A5,0x140,D1       ; [A_D1] 
||         MVKU32  .L2     0,BL3             ; [B_L2] 
||         MVKU32  .S2     0x3f800000,BM1    ; [B_S2] 

           MVKU32  .L2     0x7f7fffff,BL2    ; [B_L2] 
||         MVKU32  .S2     0x3f317218,BM5    ; [B_S2] 

           MVKU32  .L2     0x40000000,BM0    ; [B_L2] 
||         MVKU32  .S2     0x3fb8aa3b,B2     ; [B_S2] 

	.dwpsn	file "./inc/tidl_detectionOutput_int.h",line 134,column 25,is_stmt,isa 0

           ADDD    .D2     A5,0x244,D5       ; [A_D2] 
||         MVKU32  .L2     0x46fffe00,BM2    ; [B_L2] 
||         MVKU32  .L1     0xc,AM1           ; [A_L1] |134| 
||         MVKU32  .S2     0,BL1             ; [B_S2] |229| 
||         MASKB   .P2     0x4,P1            ; [B_P] |229| 

;** --------------------------------------------------------------------------*
;**   BEGIN LOOP ||$C$L21||
;** --------------------------------------------------------------------------*
||$C$L21||:    
;          EXCLUSIVE CPU CYCLES: 15
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 386,column 31,is_stmt,isa 0
           LDD     .D1     *A5(1192),D0      ; [A_D1] |386| 
           ADDD    .D1     D2,D0,D0          ; [A_D1] |386| 
           LDW     .D1     *D0(0),A0         ; [A_D1] |386| 
           CMPGTW  .L1     A0,0,A2           ; [A_L1] |386| 
   [!A2]   B       .B1     ||$C$L23||        ; [A_B] |386| 
           ; BRANCHCC OCCURS {||$C$L23||}    ; [] |386| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 1
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 386,column 26,is_stmt,isa 0
           MVKU32  .L1     0,D4              ; [A_L1] |386| 
;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*
;*      Loop found in file               : src/tidl_detectionOutput_score.c
;*      Loop source line                 : 386
;*      Loop opening brace source line   : 387
;*      Loop closing brace source line   : 406
;*      Known Minimum Trip Count         : 1                    
;*      Known Max Trip Count Factor      : 1
;*      Loop Carried Dependency Bound(^) : 133
;*      Unpartitioned Resource Bound     : 7
;*      Partitioned Resource Bound       : 10 (pre-sched)
;*      Disqualified loop: Loop carried dependency bound too large
;*----------------------------------------------------------------------------*
||$C$L22||:    
;          EXCLUSIVE CPU CYCLES: 133
	.dwpsn	file "./inc/tidl_detectionOutput_int.h",line 134,column 25,is_stmt,isa 0
           LDD     .D1     *A5(1200),D0      ; [A_D1] |134| 
           ADDD    .D1     D2,D0,D0          ; [A_D1] |134| 
           LDW     .D1     *D0(0),D0         ; [A_D1] |134| 
           LDD     .D1     *A5(1184),D6      ; [A_D1] |134| 
           ADDW    .D1     D4,D0,D0          ; [A_D1] |134| 
           LDW     .D1     *D6[D0],B0        ; [A_D1] |134| 
           SHRW    .L2     B0,0x1c,BL5       ; [B_L2] |134| 
           ANDW    .L2     BL5,0xf,B1        ; [B_L2] |134| 

           EXT     .L1X    B1,0x20,0x20,A2   ; [A_L1] |134| 
||         LDW     .D1     *A6(12),B13       ; [A_D1] |134| 

           MPYDD   .N1     AM1,A2,D6         ; [A_N1] |134| 
           SHRW    .L2     B0,0x18,BL7       ; [B_L2] |134| 

           ADDD    .D1     D5,D6,D6          ; [A_D1] |134| 
||         ANDW    .L2     BL7,0xf,B3        ; [B_L2] |134| 

           LDW     .D1     *D6(0),AM0        ; [A_D1] |134| 
||         MPYWW   .N2     B13,B3,BL5        ; [B_N2] |134| 

           SHRW    .L1X    B0,0x10,D14       ; [A_L1] |134| 
           ANDW    .D1     D3,D14,A3         ; [A_D1] |134| 
           SHLD    .L1     A2,0x3,D13        ; [A_L1] |134| 

           ADDD    .D1     A5,D13,D6         ; [A_D1] |134| 
||         ADDW    .L2X    A3,BL5,B1         ; [B_L2] |134| 

           LDD     .D1     *D6(192),D7       ; [A_D1] |134| 
           MPYWW   .N1X    B1,AM0,AM0        ; [A_N1] |134| 
           ANDW    .L2     BL4,B0,B12        ; [B_L2] |134| 
           ADDW    .M1X    B12,AM0,D6        ; [A_M1] |134| 
           LDH     .D1     *D7[D6],B1        ; [A_D1] |134| 
           SHRW    .L1X    B0,0x1c,AL0       ; [A_L1] |134| 
           ANDW    .L1     AL0,0xf,D12       ; [A_L1] |134| 
           LDUW    .D1     *D1[D12],B0       ; [A_D1] |134| 
           VINTSP  .L2     B1,B1             ; [B_L2] |134| 
           MPYSP   .N2     B0,B1,BL5         ; [B_N2] |134| 
           VXORW   .L2     BL5,0x80000000,B0 ; [B_L2] |134| 
           MPYSP   .N2     B2,B0,B1          ; [B_N2] |134| 
	.dwpsn	file "./inc/tidl_detectionOutput_int.h",line 229,column 1,is_stmt,isa 0
           VSPTRUNC .L2    B1,B0             ; [B_L2] |229| 
           VINTSP  .L2     B0,B3             ; [B_L2] |229| 
           SUBSP   .C2     B1,B3,B1          ; [B_C] |229| 
           MPYSP   .N2     BM5,B1,B3         ; [B_N2] |229| 
           MPYSP   .N2     B3,B3,B1          ; [B_N2] |229| 
           CMPGEW  .L1X    B0,0xfffffff0,A2  ; [A_L1] |229| 
   [ A2]   MPYSP   .N2     B3,B1,B5          ; [B_N2] |229| 
   [ A2]   MPYSP   .N2     BM3,B1,B4         ; [B_N2] |229| 
   [ A2]   ADDSP   .C2     BM1,B3,B3         ; [B_C] |229| 
   [ A2]   MPYSP   .N2     B1,B1,B1          ; [B_N2] |229| 
   [ A2]   MPYSP   .N2     BM4,B5,B5         ; [B_N2] |229| 
   [ A2]   ADDSP   .C2     B4,B3,B3          ; [B_C] |229| 
   [ A2]   MPYSP   .N2     BM6,B1,B1         ; [B_N2] |229| 

   [ A2]   ADDSP   .C2     B5,B3,B3          ; [B_C] |229| 
||         SUBRW   .S2     B0,0,BL5          ; [B_S2] |229| 
||         VCMPGTW .L2     B0,BL1,P0         ; [B_L2] |229| 

   [ A2]   SHRW    .L2     BL0,BL5,BL5       ; [B_L2] |229| 
|| [ A2]   AND     .P2     P0,P1,P0          ; [B_P] |229| 
||         SHLW    .S2     BL0,B0,BL6        ; [B_S2] |229| 

   [ A2]   VSEL    .L2     P0,BL6,BL5,BL5    ; [B_L2] |229| 

   [ A2]   VINTSP  .L2     BL5,B1            ; [B_L2] |229| 
|| [ A2]   ADDSP   .C2     B1,B3,B3          ; [B_C] |229| 

   [ A2]   MPYSP   .N2     B3,B1,B1          ; [B_N2] |229| 
   [ A2]   MPYSP   .N2     BM7,B1,BL5        ; [B_N2] |229| 

   [!A2]   MV      .L2     BL3,BL5           ; [B_L2] |229| 
||         CMPGTW  .L1X    B0,0xe,A3         ; [A_L1] |229| 

   [!A3]   MV      .L2     BL5,B0            ; [B_L2] |229| 
|| [ A3]   MV      .S2     BL2,B0            ; [B_S2] |229| 

           ADDSP   .C2     BM1,B0,B0         ; [B_C] |229| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 405,column 11,is_stmt,isa 0
           VRCPSP  .S2     B0,B1             ; [B_S2] |405| 
           MPYSP   .N2     B0,B1,B3          ; [B_N2] |405| 
           SUBSP   .C2     BM0,B3,B3         ; [B_C] |405| 
           MPYSP   .N2     B1,B3,B1          ; [B_N2] |405| 
           MPYSP   .N2     B0,B1,B0          ; [B_N2] |405| 
           SUBSP   .C2     BM0,B0,B0         ; [B_C] |405| 
           MPYSP   .N2     B1,B0,B0          ; [B_N2] |405| 
           MPYSP   .N2     BM2,B0,BL6        ; [B_N2] |405| 
           LDD     .D1     *A5(1152),D11     ; [A_D1] |405| 
           VSPTRUNC .L2    BL6,B0            ; [B_L2] |405| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 386,column 31,is_stmt,isa 0
           ADDW    .D1     A0,0xffffffff,A0  ; [A_D1] |386| 

   [ A0]   B       .B1     ||$C$L22||        ; [A_B] |386| 
||         STH     .D1X    B0,*D11[D0]       ; [A_D1] |405| 
||         ADDW    .D2     D4,0x1,D4         ; [A_D2] |386| 

           ; BRANCHCC OCCURS {||$C$L22||}    ; [] |386| 
;** --------------------------------------------------------------------------*
||$C$L23||:    
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 381,column 29,is_stmt,isa 0
           ADDW    .D1     A1,0xffffffff,A1  ; [A_D1] |381| 

   [ A1]   B       .B1     ||$C$L21||        ; [A_B] |381| 
||         ADDD    .D1     D2,0x4,D2         ; [A_D1] |381| 

           ; BRANCHCC OCCURS {||$C$L21||}    ; [] |381| 
;** --------------------------------------------------------------------------*
||$C$L24||:    
;          EXCLUSIVE CPU CYCLES: 7
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 408,column 7,is_stmt,isa 0
           LDD     .D1     *A5(1152),B0      ; [A_D1] |408| 

           B       .B1     ||$C$L49||        ; [A_B] |580| 
||         STD     .D1X    B0,*A5(1160)      ; [A_D1] |408| 
||         MVKU32  .L1     0,A4              ; [A_L1] |580| 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 580,column 3,is_stmt,isa 0
           ; BRANCH OCCURS {||$C$L49||}      ; [] |580| 
;** --------------------------------------------------------------------------*
||$C$L25||:    
;          EXCLUSIVE CPU CYCLES: 17
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 473,column 27,is_stmt,isa 0
           LDD     .D1     *A5(1192),D0      ; [A_D1] |473| 
           LDW     .D1     *D0(0),B0         ; [A_D1] |473| 
           CMPGTW  .L2     B0,0,BL0          ; [B_L2] |473| 
           XORD    .L2     BL0,0x1,B1        ; [B_L2] |473| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 471,column 5,is_stmt,isa 0

           MV      .S1X    B0,A3             ; [A_S1] 
||         MVKU32  .L1     0xffff,D11        ; [A_L1] 
||         MVKU32  .L2     0,B6              ; [B_L2] |471| 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 473,column 27,is_stmt,isa 0

           CMPEQW  .L1X    B1,0,A0           ; [A_L1] |473| 
||         MVKU32  .L2     0x40000000,B7     ; [B_L2] 
||         MVKU32  .S1     0,D8              ; [A_S1] 
||         ADDD    .D2     A5,0x140,D9       ; [A_D2] 
||         MVKU32  .S2     0,B3              ; [B_S2] 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 471,column 5,is_stmt,isa 0

   [!A0]   B       .B1     ||$C$L40||        ; [A_B] |473| 
||         STW     .D1X    B6,*D0(0)         ; [A_D1] |471| 
||         MVKU32  .L2     0,B5              ; [B_L2] 
||         MVKU32  .S2     0xff7fffff,BL7    ; [B_S2] 
||         MVKU32  .L1     0,A4              ; [A_L1] 
||         MVK32   .S1     0xff00ffff,D10    ; [A_S1] 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 473,column 27,is_stmt,isa 0
           ; BRANCHCC OCCURS {||$C$L40||}    ; [] |473| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 5

           MVKU32  .L2     0x3f317218,B12    ; [B_L2] 
||         MVKU32  .S2     0x3e2aaaad,B11    ; [B_S2] 

           MVKU32  .L2     0x10000,B9        ; [B_L2] 
||         MVKU32  .S2     0x37800000,B14    ; [B_S2] 

           MVKU32  .L2     0x3f000000,B10    ; [B_L2] 
||         MVKU32  .S2     0x3d2aaab4,B13    ; [B_S2] 

	.dwpsn	file "./inc/tidl_detectionOutput_int.h",line 157,column 3,is_stmt,isa 0

           MV      .M2     B12,BM4           ; [B_M2] 
||         MV      .C2     B11,BM3           ; [B_C] 
||         MVKU32  .L2     0,B8              ; [B_L2] |157| 
||         MVKU32  .S2     0x3f800000,B15    ; [B_S2] 

           MV      .L2     B10,BM2           ; [B_L2] 
||         MV      .S2     B9,BL4            ; [B_S2] 
||         MV      .M2     B14,BM6           ; [B_M2] 
||         MV      .C2     B13,BM5           ; [B_C] 
||         MASKB   .P2     0x4,P1            ; [B_P] |157| 
||         LDW     .D1     *A6(12),A8        ; [A_D1] 

;** --------------------------------------------------------------------------*
;**   BEGIN LOOP ||$C$L26||
;** --------------------------------------------------------------------------*
||$C$L26||:    
;          EXCLUSIVE CPU CYCLES: 16
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 475,column 7,is_stmt,isa 0
           LDD     .D1     *A5(1184),AM0     ; [A_D1] |475| 
           ADDD    .M1X    B5,AM0,D0         ; [A_M1] |475| 

           LDW     .D1     *D0(0),A4         ; [A_D1] |475| 
||         LDW     .D2     *A5(1304),AL1     ; [A_D2] |484| 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 484,column 7,is_stmt,isa 0
           LDW     .D1     *A6(76),AL0       ; [A_D1] |484| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 486,column 9,is_stmt,isa 0
           VSPTRUNC .L2    BL7,BL0           ; [B_L2] |486| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 483,column 7,is_stmt,isa 0

           SHRW    .L1     A4,0x1c,AM0       ; [A_L1] |483| 
||         CMPEQW  .S1     AL1,0x6,A0        ; [A_S1] |484| 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 478,column 22,is_stmt,isa 0

           ANDW    .M1     AM0,0xf,A9        ; [A_M1] |483| 
||         SHRW    .L1     A4,0x18,AL1       ; [A_L1] |478| 
||         CMPEQW  .S1     AL0,0,A1          ; [A_S1] |493| 

   [!A1]   B       .B1     ||$C$L27||        ; [A_B] |493| 
||         LDUW    .D1     *D9[A9],B4        ; [A_D1] |483| 
||         ANDW    .L1     AL1,0xf,AM3       ; [A_L1] |478| 
||         ANDW    .D2     D11,A4,A2         ; [A_D2] |479| 
|| [ A0]   EXT     .L2     BL0,0x30,0x30,B1  ; [B_L2] |486| 
|| [!A0]   EXT     .S2     BL6,0x30,0x30,B1  ; [B_S2] |490| 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 493,column 7,is_stmt,isa 0
           ; BRANCHCC OCCURS {||$C$L27||}    ; [] |493| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 3
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 496,column 9,is_stmt,isa 0
           MV      .D1     A8,A7             ; [A_D1] |496| 
           CMPGTW  .L1     A7,0,A0           ; [A_L1] |496| 
   [!A0]   B       .B1     ||$C$L35||        ; [A_B] |496| 
           ; BRANCHCC OCCURS {||$C$L35||}    ; [] |496| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 1
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 495,column 9,is_stmt,isa 0

           B       .B1     ||$C$L28||        ; [A_B] 
||         MVKU32  .L1     0x1,A1            ; [A_L1] |495| 

           ; BRANCH OCCURS {||$C$L28||}      ; [] 
;** --------------------------------------------------------------------------*
||$C$L27||:    
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 500,column 9,is_stmt,isa 0

           CMPGTW  .L1     A8,0,A0           ; [A_L1] |505| 
||         ADDAW   .D1     A5,A9,D0          ; [A_D1] |500| 

   [!A0]   B       .B1     ||$C$L35||        ; [A_B] |505| 
||         LDW     .D1     *D0(1088),A1      ; [A_D1] |500| 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 505,column 20,is_stmt,isa 0
           ; BRANCHCC OCCURS {||$C$L35||}    ; [] |505| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 1
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 501,column 9,is_stmt,isa 0
           MVKU32  .L1     0x1,A7            ; [A_L1] |501| 
;** --------------------------------------------------------------------------*
||$C$L28||:    
;          EXCLUSIVE CPU CYCLES: 10

           EXT     .L1     A9,0x20,0x20,A0   ; [A_L1] 
||         MVKU32  .S1     0xc,AM0           ; [A_S1] 

           MPYDD   .N1     AM0,A0,D0         ; [A_N1] 
           MV      .L2X    A8,B0             ; [B_L2] 

           ADDD    .D1     A5,D0,D0          ; [A_D1] 
||         MV      .L2     B0,BL1            ; [B_L2] 

           LDW     .D1     *D0(580),A10      ; [A_D1] 
||         EXT     .S2     BL1,0x20,0x20,B2  ; [B_S2] 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 505,column 12,is_stmt,isa 0

           MVKU32  .L1     0,A0              ; [A_L1] |505| 
||         MV      .D1     A1,AM0            ; [A_D1] 
||         SHLD    .S1     A0,0x3,D1         ; [A_S1] 
||         MPYWW   .N1     A7,AM3,A12        ; [A_N1] 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 507,column 9,is_stmt,isa 0

           MPYWW   .N1     AM0,A0,D0         ; [A_N1] |507| <0,1> 
||         NLCINIT .S1X    B2,0x1,8          ; [A_S1] 
||         UNPROT          0x1               ; [A_U] 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 505,column 20,is_stmt,isa 0

           ADDW    .D1     A0,0x1,A0         ; [A_D1] |505| <0,2> 
||         TICK                               ; [A_U] <0,0> 
||         ADDD    .D2     A5,D1,D1          ; [A_D2] 

;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*
;*      Loop found in file               : src/tidl_detectionOutput_score.c
;*      Loop source line                 : 505
;*      Loop opening brace source line   : 506
;*      Loop closing brace source line   : 509
;*      Known Minimum Trip Count         : 1                    
;*      Known Max Trip Count Factor      : 1
;*      Loop Carried Dependency Bound(^) : 2
;*      Unpartitioned Resource Bound     : 1
;*      Partitioned Resource Bound       : 1 (pre-sched)
;*
;*      Searching for software pipeline schedule at ...
;*         ii = 2  Schedule found with 10 iterations in parallel
;*
;*      Partitioned Resource Bound(*)    : 2 (post-sched)
;*
;*      Constant Extension #0 Used [C0]  : 0
;*      Constant Extension #1 Used [C1]  : 1
;*
;*      Resource Partition (may include "post-sched" split/spill moves):
;*
;*                                                A-side   B-side
;*      .L units                                     0        0     
;*      .S units                                     0        0     
;*      .M units                                     0        0     
;*      .N units                                     0        0     
;*      .D units                                     1        -     
;*      .B units                                     1        -     
;*      .C units                                     -        0     
;*      .P units                                     -        0     
;*
;*      .M/.N units                                  2        0     
;*      .L/.S units                                  0        1     
;*      .L/.S/.C units                               0        0     
;*      .L/.S/.C/.M units                            0        1     
;*      .L/.S/.C/.M/.D units                         3        0     
;*
;*      .X cross paths                               0        0     
;*
;*      Bound(.D1 .D2 .D)                            1        -     
;*      Bound(.M .N .MN)                             1        0     
;*      Bound(.L .S .LS)                             0        1     
;*      Bound(.L .S .C .LS .LSC)                     0        1     
;*      Bound(.L .S .C .M .LS .LSC .LSCM)            0        1     
;*      Bound(.L .S .C .M .D .LS .LSC .LSCM .LSCMD)  1        1     
;*
;*      Register Usage Tables:
;*
;*      +----------------------------------+----------------------------------+
;*      |              ASIDE               |              BSIDE               |
;*      +----------------+--------+--------+----------------+--------+--------+
;*      |      "Axx"     |  "ALx" |  "AMx" |     "VBxx"     | "VBLx" | "VBMx" |
;*      +----------------|--------|--------|----------------|--------|--------+
;*      |0000000000111111|00000000|00000000|0000000000111111|00000000|00000000|
;*      |0123456789012345|01234567|01234567|0123456789012345|01234567|01234567|
;*      +----------------|--------|--------|----------------|--------|--------+
;*   0: |***             |        |**      |                |**      |        |
;*   1: |***             |        |***     |                |*       |        |
;*      +----------------+--------+--------+----------------+--------+--------+
;*
;*      +----------------+                 +--------+       +-------+
;*      |      "Dxx"     |                 |  "Px"  |       |"CUCRx"|
;*      +----------------+                 +--------+       +-------+
;*      |0000000000111111|                 |00000000|       |0000   |
;*      |0123456789012345|                 |01234567|       |0123   |
;*      +----------------+                 +--------+       +-------+
;*   0: |*****           |                 |        |       |       |
;*   1: |****            |                 |        |       |       |
;*      +----------------+                 +--------+       +-------+
;*
;*      Done
;*
;*      Collapsed epilog stages       : 9
;*      Prolog not entirely removed
;*      Collapsed prolog stages       : 2
;*
;*      Max amt of load speculation   : 0 bytes
;*
;*      Minimum safe trip count       : 1
;*      Min. prof. trip count  (est.) : 2
;*
;*      Mem bank conflicts/iter(est.) : { min 0.000, est 0.000, max 0.000 }
;*      Mem bank perf. penalty (est.) : 0.0%
;*
;*
;*      Total cycles (est.)         : 17 + trip_cnt * 2        
;*----------------------------------------------------------------------------*
;*        SINGLE SCHEDULED ITERATION
;*
;*        ||$C$C380||:
;*   0              TICK                               ; [A_U] 
;*   1              MPYWW   .N1     AM0,A0,D4         ; [A_N1] |507| 
;*   2              ADDW    .L1     A0,0x1,A0         ; [A_L1] |505| 
;*   3              NOP     0x2     ; [A_B] 
;*   5              ADDW    .D2     D3,D4,AM2         ; [A_D2] |507| 
;*   6              MPYWW   .N1     AM1,AM2,D0        ; [A_N1] |507| 
;*   7              NOP     0x3     ; [A_B] 
;*  10              ADDW    .D1     D2,D0,D0          ; [A_D1] |507| 
;*  11              LDH     .D1     *D1[D0],BL0       ; [A_D1] |507| 
;*  12              NOP     0x5     ; [A_B] 
;*  17              VMAXH   .L2     BL1,BL0,BL0       ; [B_L2] |507|  ^ 
;*  18              EXT     .L2     BL0,0x30,0x30,BL1 ; [B_L2] |507|  ^ [C1]
;*     ||           BNL     .B1     ||$C$C380||       ; [A_B] |505| 
;*  19              ; BRANCHCC OCCURS {||$C$C380||}   ; [] |505| 
;*----------------------------------------------------------------------------*
||$C$L29||:    ; PIPED LOOP PROLOG
;          EXCLUSIVE CPU CYCLES: 10
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 507,column 9,is_stmt,isa 0

           TICK                               ; [A_U] <1,0> 
||         MPYWW   .N1     AM0,A0,D0         ; [A_N1] |507| <1,1> 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 505,column 20,is_stmt,isa 0

           MV      .D2     A12,D3            ; [A_D2] 
||         LDD     .D1     *D1(192),A11      ; [A_D1] 
||         ADDW    .L1     A0,0x1,A0         ; [A_L1] |505| <1,2> 
||         TICK                               ; [A_U] <2,0> 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 507,column 9,is_stmt,isa 0

           MV      .D1     A10,AM1           ; [A_D1] 
||         ADDW    .D2     D3,D0,AM2         ; [A_D2] |507| <0,5> 
||         MPYWW   .N1     AM0,A0,D0         ; [A_N1] |507| <2,1> 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 505,column 20,is_stmt,isa 0

           MPYWW   .N1     AM1,AM2,D0        ; [A_N1] |507| <0,6> 
||         ADDW    .L1     A0,0x1,A0         ; [A_L1] |505| <2,2> 
||         TICK                               ; [A_U] <3,0> 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 507,column 9,is_stmt,isa 0

           ADDW    .D2     D3,D0,AM2         ; [A_D2] |507| <1,5> 
||         MPYWW   .N1     AM0,A0,D4         ; [A_N1] |507| <3,1> 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 505,column 20,is_stmt,isa 0

           MPYWW   .N1     AM1,AM2,D0        ; [A_N1] |507| <1,6> 
||         ADDW    .L1     A0,0x1,A0         ; [A_L1] |505| <3,2> 
||         TICK                               ; [A_U] <4,0> 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 507,column 9,is_stmt,isa 0

           MV      .D1     A2,D2             ; [A_D1] 
||         ADDW    .D2     D3,D0,AM2         ; [A_D2] |507| <2,5> 
||         MPYWW   .N1     AM0,A0,D4         ; [A_N1] |507| <4,1> 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 505,column 20,is_stmt,isa 0

           MV      .D2     A11,D1            ; [A_D2] 
||         ADDW    .D1     D2,D0,D0          ; [A_D1] |507| <0,10> 
||         MPYWW   .N1     AM1,AM2,D0        ; [A_N1] |507| <2,6> 
||         ADDW    .L1     A0,0x1,A0         ; [A_L1] |505| <4,2> 
||         TICK                               ; [A_U] <5,0> 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 507,column 9,is_stmt,isa 0

           MV      .L1X    B0,D4             ; [A_L1] 
||         LDH     .D1     *D1[D0],BL0       ; [A_D1] |507| <0,11> 
||         ADDW    .D2     D3,D4,AM2         ; [A_D2] |507| <3,5> 
||         MPYWW   .N1     AM0,A0,D4         ; [A_N1] |507| <5,1> 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 505,column 20,is_stmt,isa 0

           MV      .L2     B1,BL1            ; [B_L2] 
||         MVKU32  .S1     0x2,A2            ; [A_S1] init prolog collapse predicate
||         ADDD    .D2     D4,0xffffffff,A1  ; [A_D2] init epilog collapse predicate
||         ADDW    .D1     D2,D0,D0          ; [A_D1] |507| <1,10> 
||         MPYWW   .N1     AM1,AM2,D0        ; [A_N1] |507| <3,6> 
||         ADDW    .L1     A0,0x1,A0         ; [A_L1] |505| <5,2> 
||         TICK                               ; [A_U] <6,0> 

;** --------------------------------------------------------------------------*
||$C$L30||:    ; PIPED LOOP KERNEL
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 507,column 9,is_stmt,isa 0

           VMAXH   .L2     BL1,BL0,BL0       ; [B_L2] |507| <0,17>  ^ 
|| [ A1]   LDH     .D1     *D1[D0],BL0       ; [A_D1] |507| <3,11> 
||         ADDW    .D2     D3,D4,AM2         ; [A_D2] |507| <6,5> 
||         MPYWW   .N1     AM0,A0,D4         ; [A_N1] |507| <8,1> 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 505,column 20,is_stmt,isa 0

   [ A2]   ADDW    .S1     A2,0xffffffff,A2  ; [A_S1] |505| <0,18> collapsing predicate control
|| [ A1]   ADDW    .D2     A1,0xffffffff,A1  ; [A_D2] |505| <0,18> collapsing predicate control
||         BNL     .B1     ||$C$L30||        ; [A_B] |505| <0,18> 
|| [!A2]   EXT     .L2     BL0,0x30,0x30,BL1 ; [B_L2] |507| <0,18>  ^ [C1]
||         ADDW    .D1     D2,D0,D0          ; [A_D1] |507| <4,10> 
||         MPYWW   .N1     AM1,AM2,D0        ; [A_N1] |507| <6,6> 
||         ADDW    .L1     A0,0x1,A0         ; [A_L1] |505| <8,2> 
||         TICK                               ; [A_U] <9,0> 

;** --------------------------------------------------------------------------*
||$C$L31||:    ; PIPED LOOP EPILOG
;          EXCLUSIVE CPU CYCLES: 1
           PROTCLR                            ; [A_U] VBL1
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 511,column 20,is_stmt,isa 0
           CMPGTW  .L1     A8,0,A0           ; [A_L1] |511| 

   [!A0]   B       .B1     ||$C$L35||        ; [A_B] |511| 
||         MV      .M1     AM0,A1            ; [A_M1] 
||         MV      .L2     BL1,B1            ; [B_L2] 
||         MV      .D1     D2,A2             ; [A_D1] 

           ; BRANCHCC OCCURS {||$C$L35||}    ; [] |511| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 17

           MVKU32  .L1     0xc,AM0           ; [A_L1] 
||         EXT     .S1     A9,0x20,0x20,A0   ; [A_S1] 

           MPYDD   .N1     AM0,A0,AL0        ; [A_N1] 
           ADDD    .L1     A5,AL0,D0         ; [A_L1] 

           ADDD    .D1     D0,0x244,A7       ; [A_D1] 
||         MPYWW   .N1     A7,AM3,A11        ; [A_N1] 

           MV      .D1     A7,D3             ; [A_D1] 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 511,column 12,is_stmt,isa 0

           SLDW    .D1     *D3(0),AM0        ; [A_D1] |513| <0,0>  ^ 
||         MVKU32  .L1     0,A8              ; [A_L1] |511| 
||         MV      .D2     A1,AM1            ; [A_D2] 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 513,column 9,is_stmt,isa 0

           SHLD    .L1     A0,0x3,AL0        ; [A_L1] 
||         MPYWW   .N1     AM1,A8,D13        ; [A_N1] |513| <0,1> 

           ADDD    .L1     A5,AL0,D0         ; [A_L1] 
||         LDD     .D1     *PC($PCR_OFFSET(||_ZTIs||+8)),A9 ; [A_D1] 
||         LDD     .D2     *PC($PCR_OFFSET(||_ZTIf||+8)),D14 ; [A_D2] 

           ADDD    .D1     D0,0xc0,A10       ; [A_D1] 

           MV      .D1     A10,D4            ; [A_D1] 
||         MV      .D2     A11,D6            ; [A_D2] 

           SLDD    .D1     *D4(0),D7         ; [A_D1] |513| <0,5> 
||         ADDW    .D2     D6,D13,AM2        ; [A_D2] |513| <0,5> 

           MPYWW   .N1     AM2,AM0,D0        ; [A_N1] |513| <0,6>  ^ 

           MV      .L2     B15,BM7           ; [B_L2] 
||         MV      .S2     B3,BL3            ; [B_S2] 
||         MV      .M2     B8,BL1            ; [B_M2] 
||         MV      .C2     B3,B0             ; [B_C] 

           MV      .D1     D14,AL0           ; [A_D1] 
||         MV      .D2     A9,AL1            ; [A_D2] 
||         MV      .S2     B0,BM0            ; [B_S2] 
||         MVKU32  .L2     0x3fb8aa3b,B2     ; [B_L2] 
||         MV      .M2     B4,BM1            ; [B_M2] 
||         MV      .C2     B1,BL2            ; [B_C] 
||         MV      .L1     A6,D1             ; [A_L1] 
||         MV      .S1     A2,D5             ; [A_S1] 
||         MV      .M1     A5,D2             ; [A_M1] 
||         UNPROT          0x1               ; [A_U] 

;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*
;*      Loop found in file               : src/tidl_detectionOutput_score.c
;*      Loop source line                 : 511
;*      Loop opening brace source line   : 512
;*      Loop closing brace source line   : 528
;*      Known Minimum Trip Count         : 1                    
;*      Known Max Trip Count Factor      : 1
;*      Loop Carried Dependency Bound(^) : 71
;*      Unpartitioned Resource Bound     : 5
;*      Partitioned Resource Bound       : 7 (pre-sched)
;*
;*      Searching for software pipeline schedule at ...
;*         ii = 71 Schedule found with 2 iterations in parallel
;*
;*      Partitioned Resource Bound(*)    : 7 (post-sched)
;*
;*      Constant Extension #0 Used [C0]  : 0
;*      Constant Extension #1 Used [C1]  : 2
;*
;*      Resource Partition (may include "post-sched" split/spill moves):
;*
;*                                                A-side   B-side
;*      .L units                                     0        0     
;*      .S units                                     0        0     
;*      .M units                                     0        0     
;*      .N units                                     0        0     
;*      .D units                                     6        -     
;*      .B units                                     1        -     
;*      .C units                                     -        0     
;*      .P units                                     -        1     
;*
;*      .M/.N units                                  2       11     
;*      .L/.S units                                  4        5     
;*      .L/.S/.C units                               0       10     
;*      .L/.S/.C/.M units                            0        1     
;*      .L/.S/.C/.M/.D units                         3        4     
;*
;*      .X cross paths                               4        0     
;*
;*      Bound(.D1 .D2 .D)                            3        -     
;*      Bound(.M .N .MN)                             1        6     
;*      Bound(.L .S .LS)                             2        3     
;*      Bound(.L .S .C .LS .LSC)                     2        5     
;*      Bound(.L .S .C .M .LS .LSC .LSCM)            2        4     
;*      Bound(.L .S .C .M .D .LS .LSC .LSCM .LSCMD)  3        5     
;*
;*      Register Usage Tables:
;*
;*      +----------------------------------+----------------------------------+
;*      |              ASIDE               |              BSIDE               |
;*      +----------------+--------+--------+----------------+--------+--------+
;*      |      "Axx"     |  "ALx" |  "AMx" |     "VBxx"     | "VBLx" | "VBMx" |
;*      +----------------|--------|--------|----------------|--------|--------+
;*      |0000000000111111|00000000|00000000|0000000000111111|00000000|00000000|
;*      |0123456789012345|01234567|01234567|0123456789012345|01234567|01234567|
;*      +----------------|--------|--------|----------------|--------|--------+
;*   0: |        *       |**      | *      |  *             | ****   |********|
;*   1: |        *       |**      | *      |  *             | ****   |********|
;*   2: |        *       |**      | *      |  *             | ****   |********|
;*   3: |        *       |**      | *      |  *             | ****   |********|
;*   4: |        *       |**      | *      |  *             | ****   |********|
;*   5: |        *       |**      | *      |  *             | ****   |********|
;*   6: |        *       |**      | *      |  *             | ****   |********|
;*   7: |        *       |**      | *      |  *             | ****   |********|
;*   8: |*       *       |**      | *      |  *             |*****   |********|
;*   9: |*       *       |**      | *      |  *             |******  |********|
;*  10: |*       *       |**      | *      |  *             | ****   |********|
;*  11: |*       *       |**      | *      |  *             | ****   |********|
;*  12: |*       *       |**      | *      |***             | ****   |********|
;*  13: |        *       |**      | *      | **             | ****   |********|
;*  14: |        *       |**      | *      | **             | ****   |********|
;*  15: |        *       |**      | *      | **             | ****   |********|
;*  16: |        *       |**      | *      | **             | ****   |********|
;*  17: |        *       |**      | *      |  *             | ****   |********|
;*  18: |        *       |**      | *      |  *             | ****   |********|
;*  19: |        *       |**      | *      |  *             | ****   |********|
;*  20: |        *       |**      | *      |* *             | ****   |********|
;*  21: |        *       |**      | *      |* *             | ****   |********|
;*  22: |        *       |**      | *      |* *             | ****   |********|
;*  23: |        *       |**      | *      |***             | ****   |********|
;*  24: |        *       |**      | *      |***             |*****   |********|
;*  25: | *      *       |**      | *      |***             |******  |********|
;*  26: |**      *       |**      | *      |***             |*****   |********|
;*  27: |**      *       |**      | *      |  *             | ****   |********|
;*  28: |**      *       |**      | *      |  *             | ****   |********|
;*  29: |**      *       |**      | *      |***             | ****   |********|
;*  30: |**      *       |**      | *      |* *             | ****   |********|
;*  31: |**      *       |**      | *      |* *             | ****   |********|
;*  32: |**      *       |**      | *      |* *             | ****   |********|
;*  33: |**      *       |**      | *      |* * *           | ****   |********|
;*  34: |**      *       |**      | *      |* * *           | ****   |********|
;*  35: |**      *       |**      | *      |* * *           | ****   |********|
;*  36: |**      *       |**      | *      |*** *           | ****   |********|
;*  37: |**      *       |**      | *      |*****           | ****   |********|
;*  38: |**      *       |**      | *      |****            | ****   |********|
;*  39: |**      *       |**      | *      |***             | ****   |********|
;*  40: |**      *       |**      | *      |***             | ****   |********|
;*  41: |**      *       |**      | *      |*****           | ****   |********|
;*  42: |**      *       |**      | *      |***             | ****   |********|
;*  43: |**      *       |**      | *      |* *             | ****   |********|
;*  44: |**      *       |**      | *      |***             | ****   |********|
;*  45: |**      *       |**      | *      |****            | ****   |********|
;*  46: |**      *       |**      | *      |* **            | ****   |********|
;*  47: |**      *       |**      | *      |* **            | ****   |********|
;*  48: |**      *       |**      | *      |****            | ****   |********|
;*  49: |**      *       |**      | *      |* *             | ****   |********|
;*  50: |**      *       |**      | *      |* *             | ****   |********|
;*  51: |**      *       |**      | *      |***             | ****   |********|
;*  52: |**      *       |**      | *      |  *             | ****   |********|
;*  53: |**      *       |**      | *      |  *             | ****   |********|
;*  54: |**      *       |**      | *      |  *             | ****   |********|
;*  55: |**      *       |**      | *      |* *             | ****   |********|
;*  56: |**      *       |**      | *      |  *             | ****   |********|
;*  57: |**      *       |**      | *      |  *             | ****   |********|
;*  58: |**      *       |**      | *      |  *             | ****   |********|
;*  59: |**      *       |**      | *      |* *             | ****   |********|
;*  60: |*       *       |**      | *      |* *             | ****   |********|
;*  61: |        *       |**      | *      |* *             | ****   |********|
;*  62: |        *       |**      | *      |  *             | ****   | *******|
;*  63: |        *       |**      | *      |  *             | ****   | *******|
;*  64: |        *       |**      | *      |  *             | ****   |********|
;*  65: |        *       |**      | *      |  *             | ****   |********|
;*  66: |        *       |**      | *      |  *             | ****   |********|
;*  67: |        *       |**      | *      |  *             | ****   |********|
;*  68: |        *       |**      |***     |* *             | ****   |********|
;*  69: |        *       |**      | *      |* *             | ****   |********|
;*  70: |*       *       |**      | *      |  *             | ****   |********|
;*      +----------------+--------+--------+----------------+--------+--------+
;*
;*      +----------------+                 +--------+       +-------+
;*      |      "Dxx"     |                 |  "Px"  |       |"CUCRx"|
;*      +----------------+                 +--------+       +-------+
;*      |0000000000111111|                 |00000000|       |0000   |
;*      |0123456789012345|                 |01234567|       |0123   |
;*      +----------------+                 +--------+       +-------+
;*   0: | ******         |                 | *      |       |       |
;*   1: |*******         |                 | *      |       |       |
;*   2: |********        |                 | *      |       |       |
;*   3: | ******         |                 | *      |       |       |
;*   4: | ******         |                 | *      |       |       |
;*   5: | ******         |                 | *      |       |       |
;*   6: | ******         |                 | *      |       |       |
;*   7: | ******         |                 | *      |       |       |
;*   8: | ******         |                 | *      |       |       |
;*   9: | ******         |                 | *      |       |       |
;*  10: | ******         |                 | *      |       |       |
;*  11: | ******         |                 | *      |       |       |
;*  12: | ******         |                 | *      |       |       |
;*  13: | ******         |                 | *      |       |       |
;*  14: | ******         |                 | *      |       |       |
;*  15: | ******         |                 | *      |       |       |
;*  16: | ******         |                 | *      |       |       |
;*  17: | ******         |                 | *      |       |       |
;*  18: | ******         |                 | *      |       |       |
;*  19: | ******         |                 | *      |       |       |
;*  20: | ******         |                 | *      |       |       |
;*  21: | ******         |                 | *      |       |       |
;*  22: | ******         |                 | *      |       |       |
;*  23: | ******         |                 | *      |       |       |
;*  24: | ******         |                 |**      |       |       |
;*  25: | ******         |                 |**      |       |       |
;*  26: | ******         |                 | *      |       |       |
;*  27: | ******         |                 | *      |       |       |
;*  28: | ******         |                 | *      |       |       |
;*  29: | ******         |                 | *      |       |       |
;*  30: | ******         |                 | *      |       |       |
;*  31: | ******         |                 | *      |       |       |
;*  32: | ******         |                 | *      |       |       |
;*  33: | ******         |                 | *      |       |       |
;*  34: | ******         |                 | *      |       |       |
;*  35: | ******         |                 | *      |       |       |
;*  36: | ******         |                 | *      |       |       |
;*  37: | ******         |                 | *      |       |       |
;*  38: | ******         |                 | *      |       |       |
;*  39: | ******         |                 | *      |       |       |
;*  40: | ******         |                 | *      |       |       |
;*  41: | ******         |                 | *      |       |       |
;*  42: | ******         |                 | *      |       |       |
;*  43: | ******         |                 | *      |       |       |
;*  44: | ******         |                 | *      |       |       |
;*  45: | ******         |                 | *      |       |       |
;*  46: | ******         |                 | *      |       |       |
;*  47: | ******         |                 | *      |       |       |
;*  48: | ******         |                 | *      |       |       |
;*  49: | ******         |                 | *      |       |       |
;*  50: | ******         |                 | *      |       |       |
;*  51: | ******         |                 | *      |       |       |
;*  52: | ******         |                 | *      |       |       |
;*  53: | ******         |                 | *      |       |       |
;*  54: | ******         |                 | *      |       |       |
;*  55: | ******         |                 | *      |       |       |
;*  56: | ******         |                 | *      |       |       |
;*  57: | ******         |                 | *      |       |       |
;*  58: | ******         |                 | *      |       |       |
;*  59: | ******         |                 | *      |       |       |
;*  60: | ******         |                 | *      |       |       |
;*  61: |*******         |                 | *      |       |       |
;*  62: | ******         |                 | *      |       |       |
;*  63: | ******         |                 | *      |       |       |
;*  64: | ******         |                 | *      |       |       |
;*  65: | ******         |                 | *      |       |       |
;*  66: | ******         |                 | *      |       |       |
;*  67: |*******         |                 | *      |       |       |
;*  68: | ******         |                 | *      |       |       |
;*  69: | ******         |                 | *      |       |       |
;*  70: | ******         |                 | *      |       |       |
;*      +----------------+                 +--------+       +-------+
;*
;*      Done
;*
;*      Collapsed epilog stages       : 1
;*      Prolog not removed
;*      Collapsed prolog stages       : 0
;*
;*      Max amt of load speculation   : 0 bytes
;*
;*      Minimum safe trip count       : 1
;*      Min. prof. trip count  (est.) : 2
;*
;*      Mem bank conflicts/iter(est.) : { min 0.000, est 0.000, max 0.000 }
;*      Mem bank perf. penalty (est.) : 0.0%
;*
;*
;*      Total cycles (est.)         : 9 + trip_cnt * 71        
;*----------------------------------------------------------------------------*
;*        SINGLE SCHEDULED ITERATION
;*
;*        ||$C$C290||:
;*   0              SLDW    .D2     *D3(0),AM0        ; [A_D2] |513|  ^ 
;*   1              MPYWW   .N1     AM1,A8,D0         ; [A_N1] |513| 
;*   2              NOP     0x3     ; [A_B] 
;*   5              ADDW    .D2     D6,D0,AM2         ; [A_D2] |513| 
;*     ||           SLDD    .D1     *D4(0),D7         ; [A_D1] |513| 
;*   6              MPYWW   .N1     AM2,AM0,D0        ; [A_N1] |513|  ^ 
;*   7              NOP     0x3     ; [A_B] 
;*  10              ADDW    .D1     D5,D0,D0          ; [A_D1] |513|  ^ 
;*  11              LDH     .D1     *D7[D0],BL0       ; [A_D1] |513|  ^ 
;*  12              NOP     0x4     ; [A_B] 
;*  16              CMPEQD  .L1     AL1,AL0,A0        ; [A_L1] |517| 
;*  17      [ A0]   SUBW    .S2     BL0,BL2,BL5       ; [B_S2] |517| 
;*     ||           SUBW    .L2     BL0,BL2,BL0       ; [B_L2] |517|  ^ 
;*  18      [ A0]   VINTSP  .S2     BL5,B1            ; [B_S2] |517| 
;*     ||   [!A0]   VINTSP  .L2     BL0,B0            ; [B_L2] |517|  ^ 
;*  19              NOP     0x2     ; [A_B] 
;*  21      [!A0]   MPYSP   .N2     BM1,B0,B1         ; [B_N2] |517|  ^ 
;*  22              NOP     0x3     ; [A_B] 
;*  25              MPYSP   .N2     B2,B1,B0          ; [B_N2] |135|  ^ 
;*  26              NOP     0x3     ; [A_B] 
;*  29              VSPTRUNC .L2    B0,B1             ; [B_L2] |135|  ^ 
;*  30              NOP     0x2     ; [A_B] 
;*  32              VINTSP  .S2     B1,B1             ; [B_S2] |157|  ^ 
;*     ||           VCMPGTW .L2     B1,BL1,P0         ; [B_L2] |157| 
;*     ||           SUBRW   .M2     B1,0,BL0          ; [B_M2] |157| 
;*  33              SHLW    .L2     BL4,B1,BL5        ; [B_L2] |157| 
;*     ||           AND     .P2     P0,P1,P0          ; [B_P] |157| 
;*     ||           SHRW    .S2     BL4,BL0,BL0       ; [B_S2] |157| 
;*     ||           CMPGEW  .L1X    B1,0xfffffff0,A1  ; [A_L1] |161| 
;*  34              VSEL    .L2     P0,BL5,BL0,BL0    ; [B_L2] |157| 
;*     ||           CMPGTW  .L1X    B1,0xe,A0         ; [A_L1] |164| 
;*  35              SUBSP   .C2     B0,B1,B1          ; [B_C] |157|  ^ 
;*     ||           VINTSP  .L2     BL0,B0            ; [B_L2] |157| 
;*  36              NOP     0x2     ; [A_B] 
;*  38              MPYSP   .N2     BM4,B1,B4         ; [B_N2] |157|  ^ 
;*  39              NOP     0x3     ; [A_B] 
;*  42              MPYSP   .N2     B4,B4,B3          ; [B_N2] |157|  ^ 
;*     ||           ADDSP   .C2     BM7,B4,B1         ; [B_C] |157| 
;*  43              NOP     0x3     ; [A_B] 
;*  46              MPYSP   .M2     BM2,B3,B3         ; [B_M2] |157| 
;*     ||           MPYSP   .N2     B4,B3,B4          ; [B_N2] |157|  ^ 
;*  47              MPYSP   .N2     B3,B3,B1          ; [B_N2] |157| 
;*  48              NOP     0x2     ; [A_B] 
;*  50              ADDSP   .C2     B3,B1,B1          ; [B_C] |157| 
;*     ||           MPYSP   .N2     BM3,B4,B3         ; [B_N2] |157|  ^ 
;*  51              MPYSP   .N2     BM5,B1,B3         ; [B_N2] |157| 
;*  52              NOP     0x2     ; [A_B] 
;*  54              ADDSP   .C2     B3,B1,B1          ; [B_C] |157|  ^ 
;*  55              NOP     0x2     ; [A_B] 
;*  57              ADDSP   .C2     B3,B1,B1          ; [B_C] |157|  ^ 
;*  58              NOP     0x2     ; [A_B] 
;*  60              MPYSP   .N2     B1,B0,B0          ; [B_N2] |157|  ^ 
;*  61              NOP     0x3     ; [A_B] 
;*  64              LDD     .D1     *D2(1168),D0      ; [A_D1] [C1]
;*     ||           MPYSP   .N2     BM6,B0,B0         ; [B_N2] |157|  ^ 
;*  65              NOP     0x3     ; [A_B] 
;*  68      [!A1]   MV      .L2     BL3,B0            ; [B_L2] |162|  ^ 
;*  69      [ A0]   MVKU32  .L2     0x7f7fffff,B0     ; [B_L2] |165|  ^ [C1]
;*  70              STW     .D1X    B0,*D0[A8]        ; [A_D1] |526|  ^ 
;*     ||           ADDSP   .C2     B0,BM0,BM0        ; [B_C] |527| 
;*  71              LDW     .D1     *D1(12),B0        ; [A_D1] |511| 
;*     ||           ADDW    .L1     A8,0x1,A8         ; [A_L1] |511| 
;*  72              NOP     0x6     ; [A_B] 
;*  78              CMPGTW  .L1X    B0,A8,A0          ; [A_L1] |511| 
;*  79      [ A0]   B       .B1     ||$C$C290||       ; [A_B] |511| 
;*  80              ; BRANCHCC OCCURS {||$C$C290||}   ; [] |511| 
;*----------------------------------------------------------------------------*
||$C$L32||:    ; PIPED LOOP PROLOG
;** --------------------------------------------------------------------------*
||$C$L33||:    ; PIPED LOOP KERNEL
;          EXCLUSIVE CPU CYCLES: 71
           NOP             0x1               ; [A_B] 
           ADDW    .D1     D5,D0,D0          ; [A_D1] |513| <0,10>  ^ 
           LDH     .D1     *D7[D0],BL0       ; [A_D1] |513| <0,11>  ^ 
           NOP             0x4               ; [A_B] 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 517,column 11,is_stmt,isa 0
           CMPEQD  .L1     AL1,AL0,A0        ; [A_L1] |517| <0,16> 

   [ A0]   SUBW    .S2     BL0,BL2,BL5       ; [B_S2] |517| <0,17> 
||         SUBW    .L2     BL0,BL2,BL0       ; [B_L2] |517| <0,17>  ^ 

   [ A0]   VINTSP  .S2     BL5,B1            ; [B_S2] |517| <0,18> 
|| [!A0]   VINTSP  .L2     BL0,B0            ; [B_L2] |517| <0,18>  ^ 

           NOP             0x2               ; [A_B] 
   [!A0]   MPYSP   .N2     BM1,B0,B1         ; [B_N2] |517| <0,21>  ^ 
           NOP             0x3               ; [A_B] 
	.dwpsn	file "./inc/tidl_detectionOutput_int.h",line 135,column 25,is_stmt,isa 0
           MPYSP   .N2     B2,B1,B0          ; [B_N2] |135| <0,25>  ^ 
           NOP             0x3               ; [A_B] 
           VSPTRUNC .L2    B0,B1             ; [B_L2] |135| <0,29>  ^ 
           NOP             0x2               ; [A_B] 
	.dwpsn	file "./inc/tidl_detectionOutput_int.h",line 157,column 3,is_stmt,isa 0

           VCMPGTW .L2     B1,BL1,P0         ; [B_L2] |157| <0,32> 
||         SUBRW   .M2     B1,0,BL0          ; [B_M2] |157| <0,32> 
||         VINTSP  .S2     B1,B1             ; [B_S2] |157| <0,32>  ^ 

           CMPGEW  .L1X    B1,0xfffffff0,A1  ; [A_L1] |161| <0,33> 
||         SHLW    .L2     BL4,B1,BL5        ; [B_L2] |157| <0,33> 
||         SHRW    .S2     BL4,BL0,BL0       ; [B_S2] |157| <0,33> 
||         AND     .P2     P0,P1,P0          ; [B_P] |157| <0,33> 

           CMPGTW  .L1X    B1,0xe,A0         ; [A_L1] |164| <0,34> 
||         VSEL    .L2     P0,BL5,BL0,BL0    ; [B_L2] |157| <0,34> 

           VINTSP  .L2     BL0,B0            ; [B_L2] |157| <0,35> 
||         SUBSP   .C2     B0,B1,B1          ; [B_C] |157| <0,35>  ^ 

           NOP             0x2               ; [A_B] 
           MPYSP   .N2     BM4,B1,B4         ; [B_N2] |157| <0,38>  ^ 
           NOP             0x3               ; [A_B] 

           ADDSP   .C2     BM7,B4,B1         ; [B_C] |157| <0,42> 
||         MPYSP   .N2     B4,B4,B3          ; [B_N2] |157| <0,42>  ^ 

           NOP             0x3               ; [A_B] 

           MPYSP   .M2     BM2,B3,B3         ; [B_M2] |157| <0,46> 
||         MPYSP   .N2     B4,B3,B4          ; [B_N2] |157| <0,46>  ^ 

           MPYSP   .N2     B3,B3,B1          ; [B_N2] |157| <0,47> 
           NOP             0x2               ; [A_B] 

           ADDSP   .C2     B3,B1,B1          ; [B_C] |157| <0,50> 
||         MPYSP   .N2     BM3,B4,B3         ; [B_N2] |157| <0,50>  ^ 

           MPYSP   .N2     BM5,B1,B3         ; [B_N2] |157| <0,51> 
           NOP             0x2               ; [A_B] 
           ADDSP   .C2     B3,B1,B1          ; [B_C] |157| <0,54>  ^ 
           NOP             0x2               ; [A_B] 
           ADDSP   .C2     B3,B1,B1          ; [B_C] |157| <0,57>  ^ 
           NOP             0x2               ; [A_B] 
           MPYSP   .N2     B1,B0,B0          ; [B_N2] |157| <0,60>  ^ 
           NOP             0x3               ; [A_B] 

           LDD     .D1     *D2(1168),D0      ; [A_D1] <0,64> [C1]
||         MPYSP   .N2     BM6,B0,B0         ; [B_N2] |157| <0,64>  ^ 

           NOP             0x3               ; [A_B] 
	.dwpsn	file "./inc/tidl_detectionOutput_int.h",line 162,column 5,is_stmt,isa 0
   [!A1]   MV      .L2     BL3,B0            ; [B_L2] |162| <0,68>  ^ 
	.dwpsn	file "./inc/tidl_detectionOutput_int.h",line 165,column 5,is_stmt,isa 0
   [ A0]   MVKU32  .L2     0x7f7fffff,B0     ; [B_L2] |165| <0,69>  ^ [C1]
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 526,column 9,is_stmt,isa 0

           ADDSP   .C2     B0,BM0,BM0        ; [B_C] |527| <0,70> 
||         STW     .D1X    B0,*D0[A8]        ; [A_D1] |526| <0,70>  ^ 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 511,column 20,is_stmt,isa 0

           ADDW    .L1     A8,0x1,A8         ; [A_L1] |511| <0,71> 
||         LDW     .D1     *D1(12),B0        ; [A_D1] |511| <0,71> 
||         SLDW    .D2     *D3(0),AM0        ; [A_D2] |513| <1,0>  ^ 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 513,column 9,is_stmt,isa 0
           MPYWW   .N1     AM1,A8,D0         ; [A_N1] |513| <1,1> 
           NOP             0x3               ; [A_B] 

           SLDD    .D1     *D4(0),D7         ; [A_D1] |513| <1,5> 
||         ADDW    .D2     D6,D0,AM2         ; [A_D2] |513| <1,5> 

           MPYWW   .N1     AM2,AM0,D0        ; [A_N1] |513| <1,6>  ^ 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 511,column 20,is_stmt,isa 0
           CMPGTW  .L1X    B0,A8,A0          ; [A_L1] |511| <0,78> 
   [ A0]   B       .B1     ||$C$L33||        ; [A_B] |511| <0,79> 
;** --------------------------------------------------------------------------*
||$C$L34||:    ; PIPED LOOP EPILOG
;          EXCLUSIVE CPU CYCLES: 1
           PROTCLR                            ; [A_U] VB0
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 1

           B       .B1     ||$C$L36||        ; [A_B] 
||         MV      .M2     BM0,B0            ; [B_M2] 
||         MV      .D1     D2,A5             ; [A_D1] 
||         MV      .L2     BL3,B3            ; [B_L2] 
||         MV      .D2     D1,A6             ; [A_D2] 
||         MV      .L1X    B0,A8             ; [A_L1] 

           ; BRANCH OCCURS {||$C$L36||}      ; [] 
;** --------------------------------------------------------------------------*
||$C$L35||:    
;          EXCLUSIVE CPU CYCLES: 1
           MV      .L2     B3,B0             ; [B_L2] 
;** --------------------------------------------------------------------------*
||$C$L36||:    
;          EXCLUSIVE CPU CYCLES: 21
	.dwpsn	file "./inc/tidl_detectionOutput_int.h",line 237,column 3,is_stmt,isa 0
           VRCPSP  .S2     B0,BM0            ; [B_S2] |237| 
           MPYSP   .N2     B0,BM0,BM1        ; [B_N2] |237| 
           SUBSP   .C2     B7,BM1,BM1        ; [B_C] |237| 
           MPYSP   .N2     BM0,BM1,BM0       ; [B_N2] |237| 
           MPYSP   .N2     B0,BM0,BM1        ; [B_N2] |237| 
           SUBSP   .C2     B7,BM1,BM1        ; [B_C] |237| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 533,column 20,is_stmt,isa 0
           CMPGTW  .L1     A8,0,A0           ; [A_L1] |533| 
	.dwpsn	file "./inc/tidl_detectionOutput_int.h",line 237,column 3,is_stmt,isa 0

   [!A0]   B       .B1     ||$C$L39||        ; [A_B] |533| 
||         MPYSP   .N2     BM0,BM1,BM1       ; [B_N2] |237| 
||         ANDW    .D1     D10,A4,D0         ; [A_D1] |531| 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 533,column 20,is_stmt,isa 0
           ; BRANCHCC OCCURS {||$C$L39||}    ; [] |533| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 1
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 533,column 12,is_stmt,isa 0

           MVKU32  .L2     0x46fffe00,BM7    ; [B_L2] |545| 
||         MVKU32  .L1     0,A9              ; [A_L1] |533| 

;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*      Disqualified loop: Loop contains control code
;*----------------------------------------------------------------------------*
||$C$L37||:    
;          EXCLUSIVE CPU CYCLES: 8
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 537,column 11,is_stmt,isa 0
           LDW     .D1     *A6(16),AL0       ; [A_D1] |537| 
           CMPEQW  .L1     A9,AL0,A0         ; [A_L1] |537| 
   [ A0]   B       .B1     ||$C$L38||        ; [A_B] |537| 
           ; BRANCHCC OCCURS {||$C$L38||}    ; [] |537| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 54
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 542,column 9,is_stmt,isa 0
           LDD     .D1     *A5(1168),D1      ; [A_D1] |542| 
           LDUW    .D1     *D1[A9],BM0       ; [A_D1] |542| 

           MPYSP   .N2     BM1,BM0,B0        ; [B_N2] |542| 
||         LDUW    .D1     *A6(24),AL1       ; [A_D1] |542| 

           MV      .L1X    B0,AL0            ; [A_L1] |542| Define a twin register
           CMPLESP .L1     AL1,AL0,A0        ; [A_L1] |542| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 545,column 11,is_stmt,isa 0
   [ A0]   MPYSP   .N2     BM7,B0,BL0        ; [B_N2] |545| 
   [ A0]   LDD     .D1     *A5(1152),D1      ; [A_D1] |545| 
   [ A0]   VSPTRUNC .L2    BL0,B0            ; [B_L2] |545| 
   [ A0]   STH     .D1X    B0,*D1[D8]        ; [A_D1] |545| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 546,column 11,is_stmt,isa 0
   [ A0]   LDD     .D1     *A5(1176),D1      ; [A_D1] |546| 
           SHLW    .L1     A9,0x10,D2        ; [A_L1] |546| 
           ORW     .D1     D0,D2,D2          ; [A_D1] |546| 
   [ A0]   STW     .D1     D2,*D1[D8]        ; [A_D1] |546| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 547,column 11,is_stmt,isa 0
   [ A0]   LDD     .D1     *A5(1192),D1      ; [A_D1] |547| 
   [ A0]   ADDAW   .D1     D1,A9,D1          ; [A_D1] |547| 
   [ A0]   LDW     .D1     *D1(0),BL0        ; [A_D1] |547| 
   [ A0]   ADDW    .L2     BL0,0x1,B0        ; [B_L2] |547| 
   [ A0]   STW     .D1X    B0,*D1(0)         ; [A_D1] |547| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 548,column 11,is_stmt,isa 0
   [ A0]   LDW     .D1     *A6(12),A8        ; [A_D1] |548| 
   [ A0]   ADDW    .D1     D8,0x1,D8         ; [A_D1] |548| 
;** --------------------------------------------------------------------------*
||$C$L38||:    
;          EXCLUSIVE CPU CYCLES: 3
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 533,column 20,is_stmt,isa 0
           ADDW    .D1     A9,0x1,A9         ; [A_D1] |533| 
           CMPGTW  .L1     A8,A9,A0          ; [A_L1] |533| 
   [ A0]   B       .B1     ||$C$L37||        ; [A_B] |533| 
           ; BRANCHCC OCCURS {||$C$L37||}    ; [] |533| 
;** --------------------------------------------------------------------------*
||$C$L39||:    
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 473,column 27,is_stmt,isa 0
           ADDW    .D1     A3,0xffffffff,A3  ; [A_D1] |473| 

   [ A3]   B       .B1     ||$C$L26||        ; [A_B] |473| 
||         ADDD    .L2     B5,0x4,B5         ; [B_L2] |473| 

           ; BRANCHCC OCCURS {||$C$L26||}    ; [] |473| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 1
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 553,column 25,is_stmt,isa 0

           B       .B1     ||$C$L41||        ; [A_B] |553| 
||         MV      .D1     D8,A4             ; [A_D1] 

           ; BRANCH OCCURS {||$C$L41||}      ; [] |553| 
;** --------------------------------------------------------------------------*
||$C$L40||:    
;          EXCLUSIVE CPU CYCLES: 6
           LDW     .D1     *A6(12),A8        ; [A_D1] 
;** --------------------------------------------------------------------------*
||$C$L41||:    
;          EXCLUSIVE CPU CYCLES: 2
           CMPGTW  .L1     A8,0,A0           ; [A_L1] |553| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 553,column 20,is_stmt,isa 0

   [!A0]   B       .B1     ||$C$L45||        ; [A_B] |553| 
||         MVKU32  .L2     0,B2              ; [B_L2] 
||         MVKU32  .S2     0,B0              ; [B_S2] |553| 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 553,column 25,is_stmt,isa 0
           ; BRANCHCC OCCURS {||$C$L45||}    ; [] |553| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 7
           MV      .D1     A5,D3             ; [A_D1] 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 557,column 9,is_stmt,isa 0
           SLDD    .D1     *D3(1200),D1      ; [A_D1] |557| <0,1> [C1]
           MV      .L1X    B2,A1             ; [A_L1] 

           UNPROT          0x1               ; [A_U] 
||         MV      .D1     A6,D2             ; [A_D1] 
||         MV      .L2     B6,B1             ; [B_L2] 
||         MV      .L1X    B0,A8             ; [A_L1] 

;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*
;*      Loop found in file               : src/tidl_detectionOutput_score.c
;*      Loop source line                 : 553
;*      Loop opening brace source line   : 554
;*      Loop closing brace source line   : 564
;*      Known Minimum Trip Count         : 1                    
;*      Known Max Trip Count Factor      : 1
;*      Loop Carried Dependency Bound(^) : 25
;*      Unpartitioned Resource Bound     : 5
;*      Partitioned Resource Bound       : 5 (pre-sched)
;*
;*      Searching for software pipeline schedule at ...
;*         ii = 25 Unsafe schedule for irregular loop
;*         ii = 25 Unsafe schedule for irregular loop
;*         ii = 25 Did not find schedule
;*         ii = 26 Schedule found with 2 iterations in parallel
;*
;*      Partitioned Resource Bound(*)    : 8 (post-sched)
;*
;*      Constant Extension #0 Used [C0]  : 1
;*      Constant Extension #1 Used [C1]  : 3
;*
;*      Resource Partition (may include "post-sched" split/spill moves):
;*
;*                                                A-side   B-side
;*      .L units                                     0        0     
;*      .S units                                     0        0     
;*      .M units                                     0        0     
;*      .N units                                     0        0     
;*      .D units                                    10        -     
;*      .B units                                     1        -     
;*      .C units                                     -        0     
;*      .P units                                     -        0     
;*
;*      .M/.N units                                  0        0     
;*      .L/.S units                                  1        0     
;*      .L/.S/.C units                               0        0     
;*      .L/.S/.C/.M units                            0        0     
;*      .L/.S/.C/.M/.D units                         8        1     
;*
;*      .X cross paths                               3        0     
;*
;*      Bound(.D1 .D2 .D)                            5        -     
;*      Bound(.M .N .MN)                             0        0     
;*      Bound(.L .S .LS)                             1        0     
;*      Bound(.L .S .C .LS .LSC)                     1        0     
;*      Bound(.L .S .C .M .LS .LSC .LSCM)            1        0     
;*      Bound(.L .S .C .M .D .LS .LSC .LSCM .LSCMD)  4        1     
;*
;*      Register Usage Tables:
;*
;*      +----------------------------------+----------------------------------+
;*      |              ASIDE               |              BSIDE               |
;*      +----------------+--------+--------+----------------+--------+--------+
;*      |      "Axx"     |  "ALx" |  "AMx" |     "VBxx"     | "VBLx" | "VBMx" |
;*      +----------------|--------|--------|----------------|--------|--------+
;*      |0000000000111111|00000000|00000000|0000000000111111|00000000|00000000|
;*      |0123456789012345|01234567|01234567|0123456789012345|01234567|01234567|
;*      +----------------|--------|--------|----------------|--------|--------+
;*   0: | *      *       | *      |        | *              |        |        |
;*   1: | *      *       | *      |        | *              |        |        |
;*   2: | *      *       | *      |        | *              |        |        |
;*   3: | *      *       | *      |        | *              |        |        |
;*   4: | *      *       | *      |        | *              |        |        |
;*   5: | *      *       | *      |        | *              |        |        |
;*   6: | *      *       | *      |        | *              |        |        |
;*   7: | *      *       | *      |        | *              |**      |        |
;*   8: | *      *       | *      |        |**              |        |        |
;*   9: | *      *       | *      |        | *              |        |        |
;*  10: | *      *       | *      |        | *              |        |        |
;*  11: | *      *       | *      |        | *              |        |        |
;*  12: | *      *       | *      |        | *              |        |        |
;*  13: | *      *       | *      |        | *              |        |        |
;*  14: | *      *       | *      |        | *              |        |        |
;*  15: | *      *       | *      |        | *              |        |        |
;*  16: | *      *       | *      |        | *              |        |        |
;*  17: | *      *       | *      |        | *              |        |        |
;*  18: | *      *       | *      |        | *              |        |        |
;*  19: | *      *       | *      |        | *              |        |        |
;*  20: | *      *       | *      |        | *              |        |        |
;*  21: | *      *       | *      |        | *              |        |        |
;*  22: | *      *       | *      |        | *              |        |        |
;*  23: | *      *       | *      |        | *              |        |        |
;*  24: | *      *       |**      |        | *              |        |        |
;*  25: |**      *       | *      |        | *              |        |        |
;*      +----------------+--------+--------+----------------+--------+--------+
;*
;*      +----------------+                 +--------+       +-------+
;*      |      "Dxx"     |                 |  "Px"  |       |"CUCRx"|
;*      +----------------+                 +--------+       +-------+
;*      |0000000000111111|                 |00000000|       |0000   |
;*      |0123456789012345|                 |01234567|       |0123   |
;*      +----------------+                 +--------+       +-------+
;*   0: |**** *          |                 |        |       |       |
;*   1: |*******         |                 |        |       |       |
;*   2: |**** *          |                 |        |       |       |
;*   3: |**** *          |                 |        |       |       |
;*   4: |**** *          |                 |        |       |       |
;*   5: |**** *          |                 |        |       |       |
;*   6: |**** *          |                 |        |       |       |
;*   7: |**** *          |                 |        |       |       |
;*   8: |**** *          |                 |        |       |       |
;*   9: |**** *          |                 |        |       |       |
;*  10: |**** *          |                 |        |       |       |
;*  11: |**** *          |                 |        |       |       |
;*  12: |**** *          |                 |        |       |       |
;*  13: |**** *          |                 |        |       |       |
;*  14: |**** *          |                 |        |       |       |
;*  15: |******          |                 |        |       |       |
;*  16: |******          |                 |        |       |       |
;*  17: |******          |                 |        |       |       |
;*  18: |**** *          |                 |        |       |       |
;*  19: |**** *          |                 |        |       |       |
;*  20: |**** *          |                 |        |       |       |
;*  21: |**** *          |                 |        |       |       |
;*  22: |**** *          |                 |        |       |       |
;*  23: |**** *          |                 |        |       |       |
;*  24: |**** *          |                 |        |       |       |
;*  25: |******          |                 |        |       |       |
;*      +----------------+                 +--------+       +-------+
;*
;*      Done
;*
;*      Collapsed epilog stages       : 1
;*      Prolog not removed
;*      Collapsed prolog stages       : 0
;*
;*      Max amt of load speculation   : 0 bytes
;*
;*      Minimum safe trip count       : 1
;*      Min. prof. trip count  (est.) : 2
;*
;*      Mem bank conflicts/iter(est.) : { min 0.000, est 0.000, max 0.000 }
;*      Mem bank perf. penalty (est.) : 0.0%
;*
;*
;*      Total cycles (est.)         : 7 + trip_cnt * 26        
;*----------------------------------------------------------------------------*
;*        SINGLE SCHEDULED ITERATION
;*
;*        ||$C$C214||:
;*   0      [ A1]   SLDD    .D2     *D3(1200),D0      ; [A_D2] |561|  ^ [C0]
;*     ||   [ A1]   SLDD    .D1     *D3(1192),D4      ; [A_D1] |561|  ^ [C1]
;*   1      [!A1]   SLDD    .D1     *D3(1200),D1      ; [A_D1] |557| [C1]
;*   2              NOP     0x4     ; [A_B] 
;*   6      [ A1]   ADDD    .D2     D0,A1,D5          ; [A_D2] |561|  ^ 
;*     ||   [ A1]   ADDD    .D1     A1,D4,AL1         ; [A_D1] |561|  ^ 
;*   7      [!A1]   STW     .D1X    B1,*D1[A8]        ; [A_D1] |557| 
;*     ||   [ A1]   ADDD    .D2     D5,0xfffffffc,D4  ; [A_D2] |561|  ^ 
;*     ||   [ A1]   ADDD    .L1     AL1,0xfffffffc,D6 ; [A_L1] |561|  ^ 
;*   8      [ A1]   LDW     .D2     *D4(0),BL0        ; [A_D2] |561|  ^ 
;*     ||   [ A1]   LDW     .D1     *D6(0),BL1        ; [A_D1] |561|  ^ 
;*   9              NOP     0x5     ; [A_B] 
;*  14      [ A1]   ADDW    .L2     BL1,BL0,B0        ; [B_L2] |561|  ^ 
;*  15      [ A1]   STW     .D1X    B0,*D0[A8]        ; [A_D1] |561|  ^ 
;*  16      [ A1]   LDD     .D1     *D3(1192),D4      ; [A_D1] |562|  ^ [C1]
;*  17              NOP     0x5     ; [A_B] 
;*  22      [ A1]   ADDD    .D1     A1,D4,D4          ; [A_D1] |562|  ^ 
;*  23      [ A1]   ADDD    .D1     D4,0xfffffffc,D4  ; [A_D1] |562|  ^ 
;*  24      [ A1]   STW     .D1X    B1,*D4(0)         ; [A_D1] |562|  ^ 
;*  25              LDW     .D1     *D2(12),AL0       ; [A_D1] |553| 
;*     ||           ADDD    .D2     A1,0x4,A1         ; [A_D2] |553|  ^ 
;*  26              NOP     0x4     ; [A_B] 
;*  30              ADDW    .D1     A8,0x1,A8         ; [A_D1] |553| 
;*  31              CMPGTW  .L1     AL0,A8,A0         ; [A_L1] |553| 
;*  32      [ A0]   B       .B1     ||$C$C214||       ; [A_B] |553| 
;*  33              ; BRANCHCC OCCURS {||$C$C214||}   ; [] |553| 
;*----------------------------------------------------------------------------*
||$C$L42||:    ; PIPED LOOP PROLOG
;** --------------------------------------------------------------------------*
||$C$L43||:    ; PIPED LOOP KERNEL
;          EXCLUSIVE CPU CYCLES: 26

   [!A1]   STW     .D1X    B1,*D1[A8]        ; [A_D1] |557| <0,7> 
|| [ A1]   ADDD    .D2     D5,0xfffffffc,D4  ; [A_D2] |561| <0,7>  ^ 
|| [ A1]   ADDD    .L1     AL1,0xfffffffc,D6 ; [A_L1] |561| <0,7>  ^ 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 561,column 9,is_stmt,isa 0

   [ A1]   LDW     .D2     *D4(0),BL0        ; [A_D2] |561| <0,8>  ^ 
|| [ A1]   LDW     .D1     *D6(0),BL1        ; [A_D1] |561| <0,8>  ^ 

           NOP             0x5               ; [A_B] 
   [ A1]   ADDW    .L2     BL1,BL0,B0        ; [B_L2] |561| <0,14>  ^ 
   [ A1]   STW     .D1X    B0,*D0[A8]        ; [A_D1] |561| <0,15>  ^ 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 562,column 9,is_stmt,isa 0
   [ A1]   LDD     .D1     *D3(1192),D4      ; [A_D1] |562| <0,16>  ^ [C1]
           NOP             0x5               ; [A_B] 
   [ A1]   ADDD    .D1     A1,D4,D4          ; [A_D1] |562| <0,22>  ^ 
   [ A1]   ADDD    .D1     D4,0xfffffffc,D4  ; [A_D1] |562| <0,23>  ^ 
   [ A1]   STW     .D1X    B1,*D4(0)         ; [A_D1] |562| <0,24>  ^ 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 553,column 25,is_stmt,isa 0

           ADDD    .D2     A1,0x4,A1         ; [A_D2] |553| <0,25>  ^ 
||         LDW     .D1     *D2(12),AL0       ; [A_D1] |553| <0,25> 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 561,column 9,is_stmt,isa 0

   [ A1]   SLDD    .D2     *D3(1200),D0      ; [A_D2] |561| <1,0>  ^ [C0]
|| [ A1]   SLDD    .D1     *D3(1192),D4      ; [A_D1] |561| <1,0>  ^ [C1]

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 557,column 9,is_stmt,isa 0
   [!A1]   SLDD    .D1     *D3(1200),D1      ; [A_D1] |557| <1,1> [C1]
           NOP             0x2               ; [A_B] 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 553,column 25,is_stmt,isa 0
           ADDW    .D1     A8,0x1,A8         ; [A_D1] |553| <0,30> 
           CMPGTW  .L1     AL0,A8,A0         ; [A_L1] |553| <0,31> 

   [ A0]   B       .B1     ||$C$L43||        ; [A_B] |553| <0,32> 
|| [ A1]   ADDD    .D2     D0,A1,D5          ; [A_D2] |561| <1,6>  ^ 
|| [ A1]   ADDD    .D1     A1,D4,AL1         ; [A_D1] |561| <1,6>  ^ 

;** --------------------------------------------------------------------------*
||$C$L44||:    ; PIPED LOOP EPILOG
;          EXCLUSIVE CPU CYCLES: 1
           PROTCLR                            ; [A_U] AL0
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 1

           MV      .L2     B1,B6             ; [B_L2] 
||         MV      .L1     AL0,A8            ; [A_L1] 
||         MV      .D1     D3,A5             ; [A_D1] 

;** --------------------------------------------------------------------------*
||$C$L45||:    
;          EXCLUSIVE CPU CYCLES: 9
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 566,column 5,is_stmt,isa 0
           LDD     .D1     *A5(1192),D0      ; [A_D1] |566| 
           ADDAW   .D1     D0,A8,D0          ; [A_D1] |566| 

           ADDD    .D1     D0,0xfffffffc,D0  ; [A_D1] |566| 
||         CMPGTW  .L1     A4,0,A0           ; [A_L1] 

   [!A0]   B       .B1     ||$C$L49||        ; [A_B] 
||         STW     .D1X    B6,*D0(0)         ; [A_D1] |566| 
||         MVKU32  .L2     0xff,B1           ; [B_L2] 
||         MVKU32  .S2     0,B0              ; [B_S2] 

           ; BRANCHCC OCCURS {||$C$L49||}    ; [] 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 4
           MV      .D1     A4,AL0            ; [A_D1] 

           EXT     .L1     AL0,0x20,0x20,A0  ; [A_L1] 
||         MV      .S1X    B0,A1             ; [A_S1] 

           NLCINIT .S1     A0,0x1,0          ; [A_S1] 
||         MV      .L1X    B0,D4             ; [A_L1] 
||         UNPROT          0x1               ; [A_U] 

           TICK                               ; [A_U] <0,0> 
||         MV      .L1X    B1,D5             ; [A_L1] 
||         MV      .D1     A5,D2             ; [A_D1] 

;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*
;*      Loop found in file               : src/tidl_detectionOutput_score.c
;*      Loop source line                 : 568
;*      Loop opening brace source line   : 569
;*      Loop closing brace source line   : 577
;*      Known Minimum Trip Count         : 1                    
;*      Known Max Trip Count Factor      : 1
;*      Loop Carried Dependency Bound(^) : 52
;*      Unpartitioned Resource Bound     : 10
;*      Partitioned Resource Bound       : 10 (pre-sched)
;*
;*      Searching for software pipeline schedule at ...
;*         ii = 52 Schedule found with 2 iterations in parallel
;*
;*      Partitioned Resource Bound(*)    : 13 (post-sched)
;*
;*      Constant Extension #0 Used [C0]  : 3
;*      Constant Extension #1 Used [C1]  : 6
;*
;*      Resource Partition (may include "post-sched" split/spill moves):
;*
;*                                                A-side   B-side
;*      .L units                                     0        0     
;*      .S units                                     0        0     
;*      .M units                                     0        0     
;*      .N units                                     0        0     
;*      .D units                                    20        -     
;*      .B units                                     1        -     
;*      .C units                                     -        0     
;*      .P units                                     -        0     
;*
;*      .M/.N units                                  0        0     
;*      .L/.S units                                  1        0     
;*      .L/.S/.C units                               0        0     
;*      .L/.S/.C/.M units                            0        0     
;*      .L/.S/.C/.M/.D units                         7        1     
;*
;*      .X cross paths                               2        0     
;*
;*      Bound(.D1 .D2 .D)                           10        -     
;*      Bound(.M .N .MN)                             0        0     
;*      Bound(.L .S .LS)                             1        0     
;*      Bound(.L .S .C .LS .LSC)                     1        0     
;*      Bound(.L .S .C .M .LS .LSC .LSCM)            1        0     
;*      Bound(.L .S .C .M .D .LS .LSC .LSCM .LSCMD)  6        1     
;*
;*      Register Usage Tables:
;*
;*      +----------------------------------+----------------------------------+
;*      |              ASIDE               |              BSIDE               |
;*      +----------------+--------+--------+----------------+--------+--------+
;*      |      "Axx"     |  "ALx" |  "AMx" |     "VBxx"     | "VBLx" | "VBMx" |
;*      +----------------|--------|--------|----------------|--------|--------+
;*      |0000000000111111|00000000|00000000|0000000000111111|00000000|00000000|
;*      |0123456789012345|01234567|01234567|0123456789012345|01234567|01234567|
;*      +----------------|--------|--------|----------------|--------|--------+
;*   0: | *              |        |        |                |        |        |
;*   1: | *              |        |        |                |        |        |
;*   2: | *              |        |        |                |        |        |
;*   3: | *              |        |        |                |        |        |
;*   4: | *              |        |        |                |        |        |
;*   5: | *              |        |        |                |        |        |
;*   6: | *              |        |        |                |        |        |
;*   7: | *              |        |        |                |        |        |
;*   8: | *              |        |        |                |        |        |
;*   9: | *              |        |        |                |        |        |
;*  10: | *              |        |        |                |        |        |
;*  11: | *              |        |        |                |        |        |
;*  12: | *              |        |        |                |        |        |
;*  13: |**              |        |        |                |        |        |
;*  14: |**              |        |        |                |        |        |
;*  15: |**              |        |        |                |        |        |
;*  16: |**              |        |        |                |        |        |
;*  17: |**              |        |        |                |        |        |
;*  18: |**              |        |        |                |        |        |
;*  19: |**              |        |        |                |        |        |
;*  20: |**              |        |        |                |        |        |
;*  21: |**              |        |        |                |        |        |
;*  22: |**              |        |        |                |        |        |
;*  23: | *              |        |        |                |        |        |
;*  24: | *              |        |        |                |        |        |
;*  25: | *              |        |        |                |        |        |
;*  26: | *              |        |        |                |        |        |
;*  27: | *              |        |        |                |        |        |
;*  28: | *              |        |        |                |        |        |
;*  29: | *              |        |        |                |        |        |
;*  30: | *              |        |        |                |        |        |
;*  31: | *              |        |        |                |        |        |
;*  32: | *              |        |        |                |        |        |
;*  33: | *              |        |        |                |        |        |
;*  34: | *              |        |        |                |        |        |
;*  35: | *              |        |        |                |        |        |
;*  36: | *              |        |        |*               |        |        |
;*  37: | *              |        |        |                |        |        |
;*  38: | *              |        |        |                |        |        |
;*  39: | *              |        |        |                |        |        |
;*  40: | *              |        |        |                |        |        |
;*  41: | *              |        |        |                |        |        |
;*  42: | *              |        |        |                |        |        |
;*  43: | *              |        |        |                |        |        |
;*  44: | *              |        |        |                |        |        |
;*  45: | *              |        |        |                |        |        |
;*  46: | *              |        |        |                |        |        |
;*  47: | *              |        |        |                |        |        |
;*  48: | *              |        |        |                |        |        |
;*  49: | *              |        |        |                |        |        |
;*  50: | *              |        |        |                |*       |        |
;*  51: | *              |        |        |*               |        |        |
;*      +----------------+--------+--------+----------------+--------+--------+
;*
;*      +----------------+                 +--------+       +-------+
;*      |      "Dxx"     |                 |  "Px"  |       |"CUCRx"|
;*      +----------------+                 +--------+       +-------+
;*      |0000000000111111|                 |00000000|       |0000   |
;*      |0123456789012345|                 |01234567|       |0123   |
;*      +----------------+                 +--------+       +-------+
;*   0: |  * **          |                 |        |       |       |
;*   1: |  * **          |                 |        |       |       |
;*   2: |  * **          |                 |        |       |       |
;*   3: |  * **          |                 |        |       |       |
;*   4: |  * **          |                 |        |       |       |
;*   5: |  * **          |                 |        |       |       |
;*   6: |* * **          |                 |        |       |       |
;*   7: |* * **          |                 |        |       |       |
;*   8: |  * **          |                 |        |       |       |
;*   9: |  * **          |                 |        |       |       |
;*  10: |  * **          |                 |        |       |       |
;*  11: |  * **          |                 |        |       |       |
;*  12: |  * **          |                 |        |       |       |
;*  13: |  * **          |                 |        |       |       |
;*  14: |* * **          |                 |        |       |       |
;*  15: |******          |                 |        |       |       |
;*  16: | ** **          |                 |        |       |       |
;*  17: | ** **          |                 |        |       |       |
;*  18: | ** **          |                 |        |       |       |
;*  19: | ** **          |                 |        |       |       |
;*  20: | ** **          |                 |        |       |       |
;*  21: |******          |                 |        |       |       |
;*  22: |******          |                 |        |       |       |
;*  23: | ** **          |                 |        |       |       |
;*  24: | ** **          |                 |        |       |       |
;*  25: | ** **          |                 |        |       |       |
;*  26: | ** **          |                 |        |       |       |
;*  27: | ** **          |                 |        |       |       |
;*  28: |*** **          |                 |        |       |       |
;*  29: |*******         |                 |        |       |       |
;*  30: | ** ***         |                 |        |       |       |
;*  31: | ** **          |                 |        |       |       |
;*  32: | ** **          |                 |        |       |       |
;*  33: | ** **          |                 |        |       |       |
;*  34: | ** **          |                 |        |       |       |
;*  35: |******          |                 |        |       |       |
;*  36: |******          |                 |        |       |       |
;*  37: | ** **          |                 |        |       |       |
;*  38: | ** **          |                 |        |       |       |
;*  39: | ** **          |                 |        |       |       |
;*  40: | ** **          |                 |        |       |       |
;*  41: | ** **          |                 |        |       |       |
;*  42: | ** **          |                 |        |       |       |
;*  43: |*** **          |                 |        |       |       |
;*  44: |* * **          |                 |        |       |       |
;*  45: |* * **          |                 |        |       |       |
;*  46: |* * **          |                 |        |       |       |
;*  47: |* * **          |                 |        |       |       |
;*  48: |* * **          |                 |        |       |       |
;*  49: |* * **          |                 |        |       |       |
;*  50: |* * **          |                 |        |       |       |
;*  51: |* * **          |                 |        |       |       |
;*      +----------------+                 +--------+       +-------+
;*
;*      Done
;*
;*      Collapsed epilog stages       : 1
;*      Prolog not removed
;*      Collapsed prolog stages       : 0
;*
;*      Max amt of load speculation   : 0 bytes
;*
;*      Minimum safe trip count       : 1
;*      Min. prof. trip count  (est.) : 2
;*
;*      Mem bank conflicts/iter(est.) : { min 0.000, est 0.000, max 0.000 }
;*      Mem bank perf. penalty (est.) : 0.0%
;*
;*
;*      Total cycles (est.)         : 1 + trip_cnt * 52        
;*----------------------------------------------------------------------------*
;*        SINGLE SCHEDULED ITERATION
;*
;*        ||$C$C124||:
;*   0              TICK                               ; [A_U] 
;*   1              LDD     .D1     *D2(1176),D0      ; [A_D1] |570|  ^ [C1]
;*   2              NOP     0x5     ; [A_B] 
;*   7              ADDD    .D1     A1,D0,D0          ; [A_D1] |570|  ^ 
;*   8              LDW     .D1     *D0(0),A0         ; [A_D1] |570|  ^ 
;*   9              NOP     0x1     ; [A_B] 
;*  10              LDD     .D2     *D2(1200),D0      ; [A_D2] |574| [C0]
;*     ||           LDD     .D1     *D2(1192),D3      ; [A_D1] |574| [C1]
;*  11              NOP     0x3     ; [A_B] 
;*  14              SHRW    .L1     A0,0x10,D0        ; [A_L1] |574|  ^ 
;*  15              ANDW    .D1     D5,D0,D1          ; [A_D1] |574|  ^ 
;*  16              LDW     .D2     *D0[D1],D0        ; [A_D2] |574|  ^ 
;*     ||           LDW     .D1     *D3[D1],D3        ; [A_D1] |574|  ^ 
;*  17              LDD     .D1     *D2(1184),D3      ; [A_D1] |574| [C1]
;*  18              NOP     0x4     ; [A_B] 
;*  22              ADDW    .D1     D3,D0,D0          ; [A_D1] |574|  ^ 
;*  23              LDD     .D2     *D2(1152),D0      ; [A_D2] |571| [C0]
;*     ||           STW     .D1     A0,*D3[D0]        ; [A_D1] |574|  ^ 
;*  24              LDD     .D2     *D2(1200),D0      ; [A_D2] |575|  ^ [C0]
;*     ||           LDD     .D1     *D2(1192),D3      ; [A_D1] |575|  ^ [C1]
;*  25              NOP     0x4     ; [A_B] 
;*  29              ADDD    .D1     D4,D0,D6          ; [A_D1] |571| 
;*  30              LDW     .D2     *D0[D1],D0        ; [A_D2] |575|  ^ 
;*     ||           LDW     .D1     *D3[D1],D3        ; [A_D1] |575|  ^ 
;*  31              LDUH    .D2     *D6(0),B0         ; [A_D2] |571| 
;*     ||           LDD     .D1     *D2(1160),D3      ; [A_D1] |575| [C1]
;*  32              NOP     0x4     ; [A_B] 
;*  36              ADDW    .D1     D3,D0,D0          ; [A_D1] |575|  ^ 
;*  37              STH     .D1X    B0,*D3[D0]        ; [A_D1] |575|  ^ 
;*  38              LDD     .D1     *D2(1192),D0      ; [A_D1] |576|  ^ [C1]
;*  39              NOP     0x5     ; [A_B] 
;*  44              ADDAW   .D1     D0,D1,D0          ; [A_D1] |576|  ^ 
;*  45              LDW     .D1     *D0(0),BL0        ; [A_D1] |576|  ^ 
;*  46              NOP     0x5     ; [A_B] 
;*  51              ADDW    .L2     BL0,0x1,B0        ; [B_L2] |576|  ^ 
;*  52              STW     .D1X    B0,*D0(0)         ; [A_D1] |576|  ^ 
;*     ||           ADDD    .L1     A1,0x4,A1         ; [A_L1] |568| 
;*     ||           ADDD    .D2     D4,0x2,D4         ; [A_D2] |568| 
;*     ||           BNL     .B1     ||$C$C124||       ; [A_B] |568| 
;*  53              ; BRANCHCC OCCURS {||$C$C124||}   ; [] |568| 
;*----------------------------------------------------------------------------*
||$C$L46||:    ; PIPED LOOP PROLOG
;** --------------------------------------------------------------------------*
||$C$L47||:    ; PIPED LOOP KERNEL
;          EXCLUSIVE CPU CYCLES: 52
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 570,column 24,is_stmt,isa 0
           LDD     .D1     *D2(1176),D0      ; [A_D1] |570| <0,1>  ^ [C1]
           NOP             0x5               ; [A_B] 
           ADDD    .D1     A1,D0,D0          ; [A_D1] |570| <0,7>  ^ 
           LDW     .D1     *D0(0),A0         ; [A_D1] |570| <0,8>  ^ 
           NOP             0x1               ; [A_B] 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 574,column 7,is_stmt,isa 0

           LDD     .D2     *D2(1200),D0      ; [A_D2] |574| <0,10> [C0]
||         LDD     .D1     *D2(1192),D3      ; [A_D1] |574| <0,10> [C1]

           NOP             0x3               ; [A_B] 
           SHRW    .L1     A0,0x10,D0        ; [A_L1] |574| <0,14>  ^ 
           ANDW    .D1     D5,D0,D1          ; [A_D1] |574| <0,15>  ^ 

           LDW     .D2     *D0[D1],D0        ; [A_D2] |574| <0,16>  ^ 
||         LDW     .D1     *D3[D1],D3        ; [A_D1] |574| <0,16>  ^ 

           LDD     .D1     *D2(1184),D3      ; [A_D1] |574| <0,17> [C1]
           NOP             0x4               ; [A_B] 
           ADDW    .D1     D3,D0,D0          ; [A_D1] |574| <0,22>  ^ 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 571,column 22,is_stmt,isa 0

           LDD     .D2     *D2(1152),D0      ; [A_D2] |571| <0,23> [C0]
||         STW     .D1     A0,*D3[D0]        ; [A_D1] |574| <0,23>  ^ 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 575,column 7,is_stmt,isa 0

           LDD     .D2     *D2(1200),D0      ; [A_D2] |575| <0,24>  ^ [C0]
||         LDD     .D1     *D2(1192),D3      ; [A_D1] |575| <0,24>  ^ [C1]

           NOP             0x4               ; [A_B] 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 571,column 22,is_stmt,isa 0
           ADDD    .D1     D4,D0,D6          ; [A_D1] |571| <0,29> 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 575,column 7,is_stmt,isa 0

           LDW     .D2     *D0[D1],D0        ; [A_D2] |575| <0,30>  ^ 
||         LDW     .D1     *D3[D1],D3        ; [A_D1] |575| <0,30>  ^ 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 571,column 22,is_stmt,isa 0

           LDUH    .D2     *D6(0),B0         ; [A_D2] |571| <0,31> 
||         LDD     .D1     *D2(1160),D3      ; [A_D1] |575| <0,31> [C1]

           NOP             0x4               ; [A_B] 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 575,column 7,is_stmt,isa 0
           ADDW    .D1     D3,D0,D0          ; [A_D1] |575| <0,36>  ^ 
           STH     .D1X    B0,*D3[D0]        ; [A_D1] |575| <0,37>  ^ 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 576,column 7,is_stmt,isa 0
           LDD     .D1     *D2(1192),D0      ; [A_D1] |576| <0,38>  ^ [C1]
           NOP             0x5               ; [A_B] 
           ADDAW   .D1     D0,D1,D0          ; [A_D1] |576| <0,44>  ^ 
           LDW     .D1     *D0(0),BL0        ; [A_D1] |576| <0,45>  ^ 
           NOP             0x5               ; [A_B] 
           ADDW    .L2     BL0,0x1,B0        ; [B_L2] |576| <0,51>  ^ 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 568,column 25,is_stmt,isa 0

           ADDD    .L1     A1,0x4,A1         ; [A_L1] |568| <0,52> 
||         ADDD    .D2     D4,0x2,D4         ; [A_D2] |568| <0,52> 
||         BNL     .B1     ||$C$L47||        ; [A_B] |568| <0,52> 
||         STW     .D1X    B0,*D0(0)         ; [A_D1] |576| <0,52>  ^ 
||         TICK                               ; [A_U] <1,0> 

;** --------------------------------------------------------------------------*
||$C$L48||:    ; PIPED LOOP EPILOG
;          EXCLUSIVE CPU CYCLES: 1
           PROTCLR                            ; [A_U] 
;** --------------------------------------------------------------------------*
||$C$L49||:    
;          EXCLUSIVE CPU CYCLES: 9
           VLD64B  .D1     *SP(16),VB14      ; [A_D1] 
	.dwcfi	restore_reg, 62

           LDD     .D2     *SP(144),A12      ; [A_D2] 
||         VLD64B  .D1     *SP(80),VB15      ; [A_D1] 
	.dwcfi	restore_reg, 12
	.dwcfi	restore_reg, 63

           LDD     .D1     *SP(160),A10      ; [A_D1] 
||         LDD     .D2     *SP(152),A11      ; [A_D2] 
	.dwcfi	restore_reg, 10
	.dwcfi	restore_reg, 11

           LDD     .D1     *SP(176),A8       ; [A_D1] 
||         LDD     .D2     *SP(168),A9       ; [A_D2] 

	.dwcfi	restore_reg, 8
	.dwcfi	restore_reg, 9
$C$DW$30	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$30, DW_AT_low_pc(0x00)
	.dwattr $C$DW$30, DW_AT_TI_return


           RET     .B1     ; [A_B] 
||         ADDD    .D1     SP,0xa8,SP        ; [A_D1] 

	.dwcfi	cfa_offset, 0
           ; RETURN OCCURS {RP}              ; [] 
	.dwattr $C$DW$25, DW_AT_TI_end_file("src/tidl_detectionOutput_score.c")
	.dwattr $C$DW$25, DW_AT_TI_end_line(0x245)
	.dwattr $C$DW$25, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$25

	.sect	".text:_Z26TIDL_sparseDetScoreCalc_cnIfEiP26sTIDL_DetectOutputParams_tP29sTIDL_ALgDetectOutputParams_t"
	.clink
	.global	||_Z26TIDL_sparseDetScoreCalc_cnIfEiP26sTIDL_DetectOutputParams_tP29sTIDL_ALgDetectOutputParams_t||

$C$DW$31	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$31, DW_AT_name("int TIDL_sparseDetScoreCalc_cn")
	.dwattr $C$DW$31, DW_AT_low_pc(||_Z26TIDL_sparseDetScoreCalc_cnIfEiP26sTIDL_DetectOutputParams_tP29sTIDL_ALgDetectOutputParams_t||)
	.dwattr $C$DW$31, DW_AT_high_pc(0x00)
	.dwattr $C$DW$31, DW_AT_linkage_name("_Z26TIDL_sparseDetScoreCalc_cnIfEiP26sTIDL_DetectOutputParams_tP29sTIDL_ALgDetectOutputParams_t")
	.dwattr $C$DW$31, DW_AT_external
	.dwattr $C$DW$31, DW_AT_decl_file("src/tidl_detectionOutput_score.c")
	.dwattr $C$DW$31, DW_AT_decl_line(0x13a)
	.dwattr $C$DW$31, DW_AT_decl_column(0x09)
	.dwattr $C$DW$31, DW_AT_TI_max_frame_size(0x80)
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 315,column 1,is_stmt,address ||_Z26TIDL_sparseDetScoreCalc_cnIfEiP26sTIDL_DetectOutputParams_tP29sTIDL_ALgDetectOutputParams_t||,isa 0

	.dwfde $C$DW$CIE, ||_Z26TIDL_sparseDetScoreCalc_cnIfEiP26sTIDL_DetectOutputParams_tP29sTIDL_ALgDetectOutputParams_t||
$C$DW$32	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$32, DW_AT_name("params")
	.dwattr $C$DW$32, DW_AT_location[DW_OP_reg4]

$C$DW$33	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$33, DW_AT_name("algDetLyrParams")
	.dwattr $C$DW$33, DW_AT_location[DW_OP_reg5]


;******************************************************************************
;* FUNCTION NAME: int TIDL_sparseDetScoreCalc_cn<float>(sTIDL_DetectOutputParams_t *, sTIDL_ALgDetectOutputParams_t *)*
;*                                                                            *
;*   Regs Modified     : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,A14,   *
;*                           A15,VB0,VB1,VB2,VB3,VB4,VB5,VB6,VB7,VB8,VB9,VB10,*
;*                           VB11,VB12,VB13,VB14,AL0,AL1,AM0,AM1,AM2,AM3,D0,  *
;*                           D1,D2,D3,D4,D5,D6,D7,D8,D9,D12,D13,D14,SP,VBL0,  *
;*                           VBL1,VBL2,VBL3,VBL4,VBL5,VBL6,VBL7,VBM0,VBM1,    *
;*                           VBM2,VBM3,VBM4,VBM5,VBM6,VBM7,P0,P1,P2           *
;*   Regs Used         : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,A14,   *
;*                           A15,VB0,VB1,VB2,VB3,VB4,VB5,VB6,VB7,VB8,VB9,VB10,*
;*                           VB11,VB12,VB13,VB14,AL0,AL1,AM0,AM1,AM2,AM3,D0,  *
;*                           D1,D2,D3,D4,D5,D6,D7,D8,D9,D12,D13,D14,SP,VBL0,  *
;*                           VBL1,VBL2,VBL3,VBL4,VBL5,VBL6,VBL7,VBM0,VBM1,    *
;*                           VBM2,VBM3,VBM4,VBM5,VBM6,VBM7,P0,P1,P2           *
;*   Local Frame Size  : 0 Args + 0 Auto + 128 Save = 128 byte                *
;******************************************************************************
||_Z26TIDL_sparseDetScoreCalc_cnIfEiP26sTIDL_DetectOutputParams_tP29sTIDL_ALgDetectOutputParams_t||:
;** --------------------------------------------------------------------------*
;* D0    assigned to $O$C36
;* D1    assigned to $O$C37
;* D0    assigned to $O$C38
;* D1    assigned to $O$C39
;* VB0   assigned to $O$C40
;* VBM0  assigned to $O$C41
;* VBM0  assigned to $O$C42
;* VB4   assigned to $O$C43
;* VB1   assigned to $O$C44
;* VB0   assigned to $O$C45
;* A0    assigned to $O$C46
;* A0    assigned to $O$C47
;* D0    assigned to $O$C48
;* D0    assigned to $O$C49
;* VB1   assigned to $O$C50
;* VB1   assigned to $O$C51
;* VB0   assigned to $O$C52
;* VB3   assigned to $O$C53
;* VB1   assigned to $O$C54
;* A2    assigned to $O$C55
;* VB0   assigned to $O$C56
;* A0    assigned to $O$C57
;* D5    assigned to $O$C58
;* VB4   assigned to $O$C59
;* VBM5  assigned to $O$C60
;* VBM5  assigned to $O$C61
;* AM1   assigned to $O$C62
;* AM1   assigned to $O$C63
;* VB5   assigned to $O$C64
;* VB6   assigned to $O$C65
;* VBM5  assigned to $O$C66
;* D0    assigned to $O$C67
;* AM1   assigned to $O$C68
;* VB5   assigned to $O$C69
;* VB4   assigned to $O$C70
;* VB11  assigned to $O$C71
;* VB7   assigned to $O$C72
;* VB4   assigned to $O$C73
;* VBM7  assigned to $O$C74
;* VBM6  assigned to $O$C75
;* A2    assigned to $O$C76
;* D3    assigned to $O$C77
;* VB2   assigned to $O$C78
;* D0    assigned to $O$C79
;* D1    assigned to $O$C80
;* D2    assigned to $O$C81
;* VB14  assigned to $O$K26
;* VB1   assigned to $O$K26
;* VB1   assigned to $O$K62
;* D3    assigned to $O$K62
;* VB14  assigned to $O$K62
;* VB3   assigned to $O$K62
;* VB3   assigned to $O$K158
;* VB2   assigned to $O$K158
;* VBM4  assigned to $O$K158
;* D1    assigned to $O$K152
;* D3    assigned to $O$K152
;* D9    assigned to $O$K137
;* VBL4  assigned to $O$K137
;* VBL7  assigned to $O$K137
;* D5    assigned to $O$K127
;* D4    assigned to $O$K127
;* VB2   assigned to $O$K171
;* VBM1  assigned to $O$K171
;* VB9   assigned to $O$K171
;* VB13  assigned to $O$K186
;* VBM7  assigned to $O$K186
;* VB10  assigned to $O$K186
;* VB12  assigned to $O$K182
;* VBM6  assigned to $O$K182
;* VBM3  assigned to $O$K182
;* VB11  assigned to $O$K169
;* VBM5  assigned to $O$K169
;* VBM2  assigned to $O$K169
;* VB10  assigned to $O$K178
;* VBM4  assigned to $O$K178
;* VBM1  assigned to $O$K178
;* VB9   assigned to $O$K174
;* VBM3  assigned to $O$K174
;* VBM0  assigned to $O$K174
;* VB8   assigned to $O$K163
;* VBL0  assigned to $O$K163
;* VBL0  assigned to $O$K163
;* VB4   assigned to $O$K188
;* VBL3  assigned to $O$K188
;* VB2   assigned to $O$K188
;* VBL2  assigned to $O$K192
;* VB0   assigned to $O$K192
;* VB6   assigned to $O$K235
;* VBM0  assigned to $O$K235
;* VB8   assigned to $O$K235
;* VBM2  assigned to $O$K254
;* VB13  assigned to $O$K254
;* A2    assigned to $O$U433
;* A15   assigned to $O$U433
;* A1    assigned to $O$U442
;* D3    assigned to $O$U431
;* A8    assigned to $O$v1
;* A9    assigned to $O$v1
;* A9    assigned to $O$Lr3$totValidCnt
;* VB1   assigned to $O$Lr9$max
;* VB3   assigned to $O$U29
;* A0    assigned to $O$U36
;* VB2   assigned to $O$U55
;* D2    assigned to $O$U100
;* D2    assigned to $O$U30
;* A1    assigned to $O$U277
;* D2    assigned to $O$U294
;* VB5   assigned to $O$U396
;* VBL6  assigned to $O$K415
;* A7    assigned to $O$U443
;* A14   assigned to $O$U432
;* D0    assigned to $O$U493
;* D8    assigned to $O$K508
;* VB3   assigned to $O$U547
;* VB0   assigned to $O$U563
;* VB0   assigned to $O$U567
;* VB0   assigned to $O$L1
;* A1    assigned to $O$L2
;* A0    assigned to $O$L3
;* A1    assigned to $O$L4
;* A0    assigned to $O$L5
;* A3    assigned to $O$L6
;* A2    assigned to $O$L7
;* A9    assigned to $O$L8
;* VB2   assigned to $O$I27
;* AL0   assigned to $O$v3
;* AL0   assigned to $O$v2
;* VBL5  assigned to $O$Lr2$maxInit
;* VB0   assigned to $O$Lr4$classCnt
;* A8    assigned to $O$Lr934$i
;* VB0   assigned to $O$Lr5$totalCnt
;* A0    assigned to $O$Lr868$curIndex
;* D1    assigned to $O$Lr634$totValidCnt
;* A9    assigned to $O$Lr1000$totValidCnt
;* A0    assigned to $O$Lr838$curClassCountM
;* A7    assigned to $O$Lr601$cnt
;* VB3   assigned to $O$Lr736$curIndex
;* VB7   assigned to $O$Lr799$y
;* A3    assigned to $O$Lr653$y
;* VB6   assigned to $O$Lr687$y
;* VB4   assigned to $O$Lr636$y
;* A8    assigned to $O$Lr557$i
;* D4    assigned to $O$Lr429$cnt
;* VB1   assigned to $O$Lr506$y
;* VB0   assigned to $O$Lr401$y
;* VB0   assigned to $O$Lr384$curClassCountM
;* VB1   assigned to $O$Lr389
;* A9    assigned to $O$Lr197$totValidCnt
;* VB2   assigned to $O$Lr1001$totValidCnt
;* A4    assigned to $O$Lr367$curIndex
;* A10   assigned to $O$Lr371$head
;* AM3   assigned to $O$Lr375$anchor
;* A13   assigned to $O$Lr377$curLoc
;* VB1   assigned to $O$Lr360$max
;* VB0   assigned to $O$Lr233$denom
;* A11   assigned to $O$Lr11$classStride
;* A12   assigned to $O$Lr12$anchorStride
;* A0    assigned to $O$Lr311$i4
;* A8    assigned to $O$Lr235$i4
;* VB0   assigned to $O$Lr10$denom
;* VB1   assigned to $O$Lr274$yI
;* VB0   assigned to $O$Lr13$ePwX
;* VBM1  assigned to $O$Lr220$recp_y
;* D0    assigned to $O$Lr222$curIndex
;* A10   assigned to $O$Lr159$i4
;* VB0   assigned to $O$Lr106$i
;* A0    assigned to $O$Lr20$curIndex
;* VB0   assigned to $O$Lr27$score
;* A5    assigned to algDetLyrParams
$C$DW$34	.dwtag  DW_TAG_variable
	.dwattr $C$DW$34, DW_AT_name("algDetLyrParams")
	.dwattr $C$DW$34, DW_AT_type(*$C$DW$T$146)
	.dwattr $C$DW$34, DW_AT_location[DW_OP_reg5]

;* A6    assigned to params
$C$DW$35	.dwtag  DW_TAG_variable
	.dwattr $C$DW$35, DW_AT_name("params")
	.dwattr $C$DW$35, DW_AT_type(*$C$DW$T$144)
	.dwattr $C$DW$35, DW_AT_location[DW_OP_reg6]

	.dwcfi	cfa_offset, 0
;          EXCLUSIVE CPU CYCLES: 15
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 330,column 3,is_stmt,isa 0
           LDW     .D1     *A5(1304),AL0     ; [A_D1] |330| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 315,column 1,is_stmt,isa 0

           CMPEQW  .L1     AL0,0x1,A0        ; [A_L1] |330| 
||         MV      .D1     A4,A6             ; [A_D1] |315| 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 343,column 3,is_stmt,isa 0

   [!A0]   LDW     .D1     *A6(68),AL0       ; [A_D1] |343| 
|| [ A0]   LDW     .D2     *A6(68),AL0       ; [A_D2] |343| 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 326,column 19,is_stmt,isa 0

           MVK32   .L2     0xffff8000,BL5    ; [B_L2] |326| 
||         STD     .D1     A8,*SP(8)         ; [A_D1] 
||         STD     .D2X    A9,*SP++(-128)    ; [A_D2] 
	.dwcfi	save_reg_to_mem, 8, 8
	.dwcfi	cfa_offset, 128
	.dwcfi	save_reg_to_mem, 9, -128

           STD     .D1     A11,*SP(112)      ; [A_D1] 
||         STD     .D2X    A10,*SP(120)      ; [A_D2] 
	.dwcfi	save_reg_to_mem, 11, 112
	.dwcfi	save_reg_to_mem, 10, 120

           STD     .D1     A13,*SP(96)       ; [A_D1] 
||         STD     .D2X    A12,*SP(104)      ; [A_D2] 

	.dwcfi	save_reg_to_mem, 13, 96
	.dwcfi	save_reg_to_mem, 12, 104
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 332,column 5,is_stmt,isa 0

           CMPEQW  .L1     AL0,0x2,A0        ; [A_L1] |343| 
|| [ A0]   MVK32   .L2     0xffffff80,BL5    ; [B_L2] |332| 
||         STD     .D2X    A14,*SP(88)       ; [A_D2] 

	.dwcfi	save_reg_to_mem, 14, 88
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 343,column 3,is_stmt,isa 0

   [ A0]   B       .B1     ||$C$L74||        ; [A_B] |343| 
||         VST64B  .D2     VB14,*SP(16)      ; [A_D2] 
||         STD     .D1     A15,*SP(80)       ; [A_D1] 

	.dwcfi	save_reg_to_mem, 62, 16
	.dwcfi	save_reg_to_mem, 15, 80
           ; BRANCHCC OCCURS {||$C$L74||}    ; [] |343| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 8
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 350,column 25,is_stmt,isa 0
           LDW     .D1     *A6(12),A9        ; [A_D1] |350| 

           CMPGTW  .L1     A9,0,A0           ; [A_L1] |350| 
||         MVKU32  .L2     0,B2              ; [B_L2] 
||         MVKU32  .S2     0,B1              ; [B_S2] 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 350,column 20,is_stmt,isa 0

   [!A0]   B       .B1     ||$C$L53||        ; [A_B] |350| 
||         EXT     .L2     B1,0x20,0x20,B3   ; [B_L2] 
||         MVKU32  .L1     0,A8              ; [A_L1] |350| 
||         MVKU32  .S2     0,B0              ; [B_S2] 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 350,column 25,is_stmt,isa 0
           ; BRANCHCC OCCURS {||$C$L53||}    ; [] |350| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 9

           MV      .D1     A5,D4             ; [A_D1] 
||         MV      .L1X    B3,A1             ; [A_L1] 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 358,column 9,is_stmt,isa 0

   [ A1]   SLDD    .D1     *D4(1200),D2      ; [A_D1] |358| <0,1>  ^ [C1]
|| [ A1]   SLDD    .D2     *D4(1192),D6      ; [A_D2] |358| <0,0>  ^ [C1]

   [ A1]   ADDD    .D1     D2,A1,D6          ; [A_D1] |358| <0,7>  ^ 
|| [ A1]   ADDD    .D2     A1,D6,D7          ; [A_D2] |358| <0,7>  ^ 

           ADDD    .D1     D6,0xfffffffc,D0  ; [A_D1] |358| <0,8>  ^ 
||         ADDD    .D2     D7,0xfffffffc,D1  ; [A_D2] |358| <0,8>  ^ 
||         MV      .L1     A6,D3             ; [A_L1] 
||         MV      .L2     B2,BL2            ; [B_L2] 
||         UNPROT          0x1               ; [A_U] 

;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*
;*      Loop found in file               : src/tidl_detectionOutput_score.c
;*      Loop source line                 : 350
;*      Loop opening brace source line   : 351
;*      Loop closing brace source line   : 362
;*      Known Minimum Trip Count         : 1                    
;*      Known Max Trip Count Factor      : 1
;*      Loop Carried Dependency Bound(^) : 25
;*      Unpartitioned Resource Bound     : 6
;*      Partitioned Resource Bound       : 6 (pre-sched)
;*
;*      Searching for software pipeline schedule at ...
;*         ii = 25 Unsafe schedule for irregular loop
;*         ii = 25 Unsafe schedule for irregular loop
;*         ii = 25 Did not find schedule
;*         ii = 26 Unsafe schedule for irregular loop
;*         ii = 26 Unsafe schedule for irregular loop
;*         ii = 26 Unsafe schedule for irregular loop
;*         ii = 26 Did not find schedule
;*         ii = 27 Unsafe schedule for irregular loop
;*         ii = 27 Unsafe schedule for irregular loop
;*         ii = 27 Unsafe schedule for irregular loop
;*         ii = 27 Did not find schedule
;*         ii = 28 Unsafe schedule for irregular loop
;*         ii = 28 Unsafe schedule for irregular loop
;*         ii = 28 Unsafe schedule for irregular loop
;*         ii = 28 Did not find schedule
;*         ii = 29 Unsafe schedule for irregular loop
;*         ii = 29 Unsafe schedule for irregular loop
;*         ii = 29 Unsafe schedule for irregular loop
;*         ii = 29 Did not find schedule
;*         ii = 30 Unsafe schedule for irregular loop
;*         ii = 30 Schedule found with 2 iterations in parallel
;*
;*      Partitioned Resource Bound(*)    : 9 (post-sched)
;*
;*      Constant Extension #0 Used [C0]  : 0
;*      Constant Extension #1 Used [C1]  : 5
;*
;*      Resource Partition (may include "post-sched" split/spill moves):
;*
;*                                                A-side   B-side
;*      .L units                                     0        0     
;*      .S units                                     0        0     
;*      .M units                                     0        0     
;*      .N units                                     0        0     
;*      .D units                                    12        -     
;*      .B units                                     1        -     
;*      .C units                                     -        0     
;*      .P units                                     -        0     
;*
;*      .M/.N units                                  0        0     
;*      .L/.S units                                  1        0     
;*      .L/.S/.C units                               0        0     
;*      .L/.S/.C/.M units                            0        0     
;*      .L/.S/.C/.M/.D units                         8        2     
;*
;*      .X cross paths                               2        0     
;*
;*      Bound(.D1 .D2 .D)                            6        -     
;*      Bound(.M .N .MN)                             0        0     
;*      Bound(.L .S .LS)                             1        0     
;*      Bound(.L .S .C .LS .LSC)                     1        0     
;*      Bound(.L .S .C .M .LS .LSC .LSCM)            1        0     
;*      Bound(.L .S .C .M .D .LS .LSC .LSCM .LSCMD)  5        1     
;*
;*      Register Usage Tables:
;*
;*      +----------------------------------+----------------------------------+
;*      |              ASIDE               |              BSIDE               |
;*      +----------------+--------+--------+----------------+--------+--------+
;*      |      "Axx"     |  "ALx" |  "AMx" |     "VBxx"     | "VBLx" | "VBMx" |
;*      +----------------|--------|--------|----------------|--------|--------+
;*      |0000000000111111|00000000|00000000|0000000000111111|00000000|00000000|
;*      |0123456789012345|01234567|01234567|0123456789012345|01234567|01234567|
;*      +----------------|--------|--------|----------------|--------|--------+
;*   0: | *      *       |        |        | *              |  *     |        |
;*   1: | *      *       |        |        | *              |  *     |        |
;*   2: | *      *       |        |        | *              |  *     |        |
;*   3: | *      *       |        |        | *              |  *     |        |
;*   4: | *      *       |        |        | *              |  *     |        |
;*   5: | *      *       |        |        | *              |  *     |        |
;*   6: | *      *       |        |        | *              |***     |        |
;*   7: | *      *       |        |        |**              |  *     |        |
;*   8: | *      *       |        |        | *              |  *     |        |
;*   9: | *      *       |        |        | *              |  *     |        |
;*  10: | *      *       |        |        | *              |  *     |        |
;*  11: | *      *       |        |        | *              |  *     |        |
;*  12: | *      *       |        |        | *              |  *     |        |
;*  13: | *      *       |        |        | *              |  *     |        |
;*  14: | *      *       |        |        | *              |  *     |        |
;*  15: | *      *       |        |        | *              |  *     |        |
;*  16: | *      *       |        |        | *              |  *     |        |
;*  17: | *      *       |        |        | *              |  *     |        |
;*  18: | *      *       |        |        | *              |  *     |        |
;*  19: | *      *       |        |        | *              |  *     |        |
;*  20: | *      *       |        |        | *              |  *     |        |
;*  21: | *      *       |        |        | *              |  *     |        |
;*  22: | *      *       |        |        | *              |  *     |        |
;*  23: | *      *       |        |        | *              |  *     |        |
;*  24: | *      *       |        |        | *              |  *     |        |
;*  25: | *      *       |        |        | *              |  *     |        |
;*  26: | *      *       |        |        | *              |  *     |        |
;*  27: | *      *       |        |        | *              |  *     |        |
;*  28: | *      *       |*       |        | *              |  *     |        |
;*  29: |**      *       |        |        | *              |* *     |        |
;*      +----------------+--------+--------+----------------+--------+--------+
;*
;*      +----------------+                 +--------+       +-------+
;*      |      "Dxx"     |                 |  "Px"  |       |"CUCRx"|
;*      +----------------+                 +--------+       +-------+
;*      |0000000000111111|                 |00000000|       |0000   |
;*      |0123456789012345|                 |01234567|       |0123   |
;*      +----------------+                 +--------+       +-------+
;*   0: |*****           |                 |        |       |       |
;*   1: |*****           |                 |        |       |       |
;*   2: |*****           |                 |        |       |       |
;*   3: |*****           |                 |        |       |       |
;*   4: |*****           |                 |        |       |       |
;*   5: |*****           |                 |        |       |       |
;*   6: |*****           |                 |        |       |       |
;*   7: |*****           |                 |        |       |       |
;*   8: |*****           |                 |        |       |       |
;*   9: |*****           |                 |        |       |       |
;*  10: |*****           |                 |        |       |       |
;*  11: |*****           |                 |        |       |       |
;*  12: |*****           |                 |        |       |       |
;*  13: |*****           |                 |        |       |       |
;*  14: |******          |                 |        |       |       |
;*  15: |******          |                 |        |       |       |
;*  16: |*******         |                 |        |       |       |
;*  17: |*****           |                 |        |       |       |
;*  18: |*****           |                 |        |       |       |
;*  19: |*****           |                 |        |       |       |
;*  20: |*****           |                 |        |       |       |
;*  21: |*****           |                 |        |       |       |
;*  22: |*****           |                 |        |       |       |
;*  23: |******          |                 |        |       |       |
;*  24: |*****           |                 |        |       |       |
;*  25: |*****           |                 |        |       |       |
;*  26: |*****           |                 |        |       |       |
;*  27: |***** *         |                 |        |       |       |
;*  28: |***** *         |                 |        |       |       |
;*  29: |***** **        |                 |        |       |       |
;*      +----------------+                 +--------+       +-------+
;*
;*      Done
;*
;*      Collapsed epilog stages       : 1
;*      Prolog not removed
;*      Collapsed prolog stages       : 0
;*
;*      Max amt of load speculation   : 0 bytes
;*
;*      Minimum safe trip count       : 1
;*      Min. prof. trip count  (est.) : 2
;*
;*      Mem bank conflicts/iter(est.) : { min 0.000, est 0.000, max 0.000 }
;*      Mem bank perf. penalty (est.) : 0.0%
;*
;*
;*      Total cycles (est.)         : 9 + trip_cnt * 30        
;*----------------------------------------------------------------------------*
;*        SINGLE SCHEDULED ITERATION
;*
;*        ||$C$C1751||:
;*   0      [ A1]   SLDD    .D1     *D4(1192),D6      ; [A_D1] |358|  ^ [C1]
;*   1      [ A1]   SLDD    .D1     *D4(1200),D2      ; [A_D1] |358|  ^ [C1]
;*   2              NOP     0x5     ; [A_B] 
;*   7      [ A1]   ADDD    .D2     D2,A1,D6          ; [A_D2] |358|  ^ 
;*     ||   [ A1]   ADDD    .D1     A1,D6,D7          ; [A_D1] |358|  ^ 
;*   8      [ A1]   ADDD    .D1     D6,0xfffffffc,D0  ; [A_D1] |358|  ^ 
;*     ||   [ A1]   ADDD    .D2     D7,0xfffffffc,D1  ; [A_D2] |358|  ^ 
;*   9      [ A1]   LDW     .D2     *D0(0),BL0        ; [A_D2] |358|  ^ 
;*     ||   [ A1]   LDW     .D1     *D1(0),BL1        ; [A_D1] |358|  ^ 
;*  10              NOP     0x5     ; [A_B] 
;*  15      [ A1]   ADDW    .L2     BL1,BL0,B0        ; [B_L2] |358|  ^ 
;*  16      [ A1]   STW     .D1X    B0,*D2[A8]        ; [A_D1] |358|  ^ 
;*  17      [ A1]   LDD     .D1     *D4(1192),D5      ; [A_D1] |359|  ^ [C1]
;*  18              NOP     0x1     ; [A_B] 
;*  19      [!A1]   SLDD    .D1     *D4(1200),D6      ; [A_D1] |354| [C1]
;*  20              NOP     0x3     ; [A_B] 
;*  23      [ A1]   ADDD    .D1     A1,D5,D5          ; [A_D1] |359|  ^ 
;*  24      [ A1]   ADDD    .D1     D5,0xfffffffc,D5  ; [A_D1] |359|  ^ 
;*  25      [!A1]   STW     .D2     B1,*D6[A8]        ; [A_D2] |354| 
;*     ||   [ A1]   STW     .D1X    B1,*D5(0)         ; [A_D1] |359|  ^ 
;*  26              LDD     .D1     *D4(1192),D5      ; [A_D1] |361| [C1]
;*  27              NOP     0x2     ; [A_B] 
;*  29              ADDD    .D1     A1,0x4,A1         ; [A_D1] |350|  ^ 
;*  30              NOP     0x1     ; [A_B] 
;*  31              LDW     .D2     *D3(12),AL0       ; [A_D2] |350| 
;*  32              LDW     .D1     *D5[A8],BL0       ; [A_D1] |361| 
;*  33              NOP     0x3     ; [A_B] 
;*  36              ADDW    .D1     A8,0x1,A8         ; [A_D1] |350| 
;*  37              CMPGTW  .L1     AL0,A8,A0         ; [A_L1] |350| 
;*  38              ADDW    .L2     BL0,BL2,BL2       ; [B_L2] |361| 
;*     ||   [ A0]   B       .B1     ||$C$C1751||      ; [A_B] |350| 
;*  39              ; BRANCHCC OCCURS {||$C$C1751||}  ; [] |350| 
;*----------------------------------------------------------------------------*
||$C$L50||:    ; PIPED LOOP PROLOG
;** --------------------------------------------------------------------------*
||$C$L51||:    ; PIPED LOOP KERNEL
;          EXCLUSIVE CPU CYCLES: 30

   [ A1]   LDW     .D2     *D0(0),BL0        ; [A_D2] |358| <0,9>  ^ 
|| [ A1]   LDW     .D1     *D1(0),BL1        ; [A_D1] |358| <0,9>  ^ 

           NOP             0x5               ; [A_B] 
   [ A1]   ADDW    .L2     BL1,BL0,B0        ; [B_L2] |358| <0,15>  ^ 
   [ A1]   STW     .D1X    B0,*D2[A8]        ; [A_D1] |358| <0,16>  ^ 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 359,column 9,is_stmt,isa 0
   [ A1]   LDD     .D1     *D4(1192),D5      ; [A_D1] |359| <0,17>  ^ [C1]
           NOP             0x1               ; [A_B] 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 354,column 9,is_stmt,isa 0
   [!A1]   SLDD    .D1     *D4(1200),D6      ; [A_D1] |354| <0,19> [C1]
           NOP             0x3               ; [A_B] 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 359,column 9,is_stmt,isa 0
   [ A1]   ADDD    .D1     A1,D5,D5          ; [A_D1] |359| <0,23>  ^ 
   [ A1]   ADDD    .D1     D5,0xfffffffc,D5  ; [A_D1] |359| <0,24>  ^ 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 354,column 9,is_stmt,isa 0

   [!A1]   STW     .D2     B1,*D6[A8]        ; [A_D2] |354| <0,25> 
|| [ A1]   STW     .D1X    B1,*D5(0)         ; [A_D1] |359| <0,25>  ^ 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 361,column 7,is_stmt,isa 0
           LDD     .D1     *D4(1192),D5      ; [A_D1] |361| <0,26> [C1]
           NOP             0x2               ; [A_B] 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 350,column 25,is_stmt,isa 0
           ADDD    .D1     A1,0x4,A1         ; [A_D1] |350| <0,29>  ^ 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 358,column 9,is_stmt,isa 0
   [ A1]   SLDD    .D1     *D4(1192),D6      ; [A_D1] |358| <1,0>  ^ [C1]
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 350,column 25,is_stmt,isa 0

           LDW     .D2     *D3(12),AL0       ; [A_D2] |350| <0,31> 
|| [ A1]   SLDD    .D1     *D4(1200),D2      ; [A_D1] |358| <1,1>  ^ [C1]

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 361,column 7,is_stmt,isa 0
           LDW     .D1     *D5[A8],BL0       ; [A_D1] |361| <0,32> 
           NOP             0x3               ; [A_B] 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 350,column 25,is_stmt,isa 0
           ADDW    .D1     A8,0x1,A8         ; [A_D1] |350| <0,36> 

           CMPGTW  .L1     AL0,A8,A0         ; [A_L1] |350| <0,37> 
|| [ A1]   ADDD    .D2     D2,A1,D6          ; [A_D2] |358| <1,7>  ^ 
|| [ A1]   ADDD    .D1     A1,D6,D7          ; [A_D1] |358| <1,7>  ^ 

   [ A0]   B       .B1     ||$C$L51||        ; [A_B] |350| <0,38> 
||         ADDW    .L2     BL0,BL2,BL2       ; [B_L2] |361| <0,38> 
|| [ A1]   ADDD    .D2     D7,0xfffffffc,D1  ; [A_D2] |358| <1,8>  ^ 
|| [ A1]   ADDD    .D1     D6,0xfffffffc,D0  ; [A_D1] |358| <1,8>  ^ 

;** --------------------------------------------------------------------------*
||$C$L52||:    ; PIPED LOOP EPILOG
;          EXCLUSIVE CPU CYCLES: 1
           PROTCLR                            ; [A_U] VBL2
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 2

           MV      .D1     D5,A0             ; [A_D1] 
||         MV      .L2     BL2,B2            ; [B_L2] 

           B       .B1     ||$C$L54||        ; [A_B] 
||         MV      .L2     B2,B0             ; [B_L2] 
||         MV      .D1     D3,A6             ; [A_D1] 
||         MV      .D2     D4,A5             ; [A_D2] 
||         MV      .L1     AL0,A9            ; [A_L1] 

           ; BRANCH OCCURS {||$C$L54||}      ; [] 
;** --------------------------------------------------------------------------*
||$C$L53||:    
;          EXCLUSIVE CPU CYCLES: 6
           LDD     .D1     *A5(1192),A0      ; [A_D1] 
;** --------------------------------------------------------------------------*
||$C$L54||:    
;          EXCLUSIVE CPU CYCLES: 3
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 364,column 5,is_stmt,isa 0
           ADDAW   .D1     A0,A9,D0          ; [A_D1] |364| 

           ADDD    .D1     D0,0xfffffffc,D0  ; [A_D1] |364| 
||         CMPGTW  .L1X    B0,0,A0           ; [A_L1] |366| 

   [!A0]   B       .B1     ||$C$L58||        ; [A_B] |366| 
||         STW     .D1X    B1,*D0(0)         ; [A_D1] |364| 
||         MVKU32  .L2     0xff,B3           ; [B_L2] 
||         MVKU32  .S2     0,B2              ; [B_S2] 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 366,column 25,is_stmt,isa 0
           ; BRANCHCC OCCURS {||$C$L58||}    ; [] |366| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 4
           EXT     .L2     B0,0x20,0x20,B0   ; [B_L2] 
           MV      .L1X    B3,D5             ; [A_L1] 

           NLCINIT .S1X    B0,0x1,0          ; [A_S1] 
||         UNPROT          0x1               ; [A_U] 

           TICK                               ; [A_U] <0,0> 
||         MV      .L1X    B2,D4             ; [A_L1] 
||         MV      .D1     A5,D2             ; [A_D1] 

;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*
;*      Loop found in file               : src/tidl_detectionOutput_score.c
;*      Loop source line                 : 366
;*      Loop opening brace source line   : 367
;*      Loop closing brace source line   : 373
;*      Known Minimum Trip Count         : 1                    
;*      Known Max Trip Count Factor      : 1
;*      Loop Carried Dependency Bound(^) : 38
;*      Unpartitioned Resource Bound     : 6
;*      Partitioned Resource Bound       : 6 (pre-sched)
;*
;*      Searching for software pipeline schedule at ...
;*         ii = 38 Schedule found with 2 iterations in parallel
;*
;*      Partitioned Resource Bound(*)    : 8 (post-sched)
;*
;*      Constant Extension #0 Used [C0]  : 1
;*      Constant Extension #1 Used [C1]  : 4
;*
;*      Resource Partition (may include "post-sched" split/spill moves):
;*
;*                                                A-side   B-side
;*      .L units                                     0        0     
;*      .S units                                     0        0     
;*      .M units                                     0        0     
;*      .N units                                     0        0     
;*      .D units                                    12        -     
;*      .B units                                     1        -     
;*      .C units                                     -        0     
;*      .P units                                     -        0     
;*
;*      .M/.N units                                  0        0     
;*      .L/.S units                                  1        0     
;*      .L/.S/.C units                               0        0     
;*      .L/.S/.C/.M units                            0        0     
;*      .L/.S/.C/.M/.D units                         4        1     
;*
;*      .X cross paths                               1        0     
;*
;*      Bound(.D1 .D2 .D)                            6        -     
;*      Bound(.M .N .MN)                             0        0     
;*      Bound(.L .S .LS)                             1        0     
;*      Bound(.L .S .C .LS .LSC)                     1        0     
;*      Bound(.L .S .C .M .LS .LSC .LSCM)            1        0     
;*      Bound(.L .S .C .M .D .LS .LSC .LSCM .LSCMD)  4        1     
;*
;*      Register Usage Tables:
;*
;*      +----------------------------------+----------------------------------+
;*      |              ASIDE               |              BSIDE               |
;*      +----------------+--------+--------+----------------+--------+--------+
;*      |      "Axx"     |  "ALx" |  "AMx" |     "VBxx"     | "VBLx" | "VBMx" |
;*      +----------------|--------|--------|----------------|--------|--------+
;*      |0000000000111111|00000000|00000000|0000000000111111|00000000|00000000|
;*      |0123456789012345|01234567|01234567|0123456789012345|01234567|01234567|
;*      +----------------|--------|--------|----------------|--------|--------+
;*   0: |                |        |        |                |        |        |
;*   1: |                |        |        |                |        |        |
;*   2: |                |        |        |                |        |        |
;*   3: |                |        |        |                |        |        |
;*   4: |                |        |        |                |        |        |
;*   5: |                |        |        |                |        |        |
;*   6: |                |        |        |                |        |        |
;*   7: |                |        |        |                |        |        |
;*   8: |                |        |        |                |        |        |
;*   9: |                |        |        |                |        |        |
;*  10: |                |        |        |                |        |        |
;*  11: |                |        |        |                |        |        |
;*  12: |                |        |        |                |        |        |
;*  13: |*               |        |        |                |        |        |
;*  14: |*               |        |        |                |        |        |
;*  15: |*               |        |        |                |        |        |
;*  16: |*               |        |        |                |        |        |
;*  17: |*               |        |        |                |        |        |
;*  18: |*               |        |        |                |        |        |
;*  19: |*               |        |        |                |        |        |
;*  20: |*               |        |        |                |        |        |
;*  21: |*               |        |        |                |        |        |
;*  22: |*               |        |        |                |        |        |
;*  23: |                |        |        |                |        |        |
;*  24: |                |        |        |                |        |        |
;*  25: |                |        |        |                |        |        |
;*  26: |                |        |        |                |        |        |
;*  27: |                |        |        |                |        |        |
;*  28: |                |        |        |                |        |        |
;*  29: |                |        |        |                |        |        |
;*  30: |                |        |        |                |        |        |
;*  31: |                |        |        |                |        |        |
;*  32: |                |        |        |                |        |        |
;*  33: |                |        |        |                |        |        |
;*  34: |                |        |        |                |        |        |
;*  35: |                |        |        |                |        |        |
;*  36: |                |        |        |                |*       |        |
;*  37: |                |        |        |*               |        |        |
;*      +----------------+--------+--------+----------------+--------+--------+
;*
;*      +----------------+                 +--------+       +-------+
;*      |      "Dxx"     |                 |  "Px"  |       |"CUCRx"|
;*      +----------------+                 +--------+       +-------+
;*      |0000000000111111|                 |00000000|       |0000   |
;*      |0123456789012345|                 |01234567|       |0123   |
;*      +----------------+                 +--------+       +-------+
;*   0: |  * **          |                 |        |       |       |
;*   1: |  * **          |                 |        |       |       |
;*   2: |  * **          |                 |        |       |       |
;*   3: |  * **          |                 |        |       |       |
;*   4: |  * **          |                 |        |       |       |
;*   5: |  * **          |                 |        |       |       |
;*   6: |* * **          |                 |        |       |       |
;*   7: |* * **          |                 |        |       |       |
;*   8: |  * **          |                 |        |       |       |
;*   9: |  * **          |                 |        |       |       |
;*  10: |  * **          |                 |        |       |       |
;*  11: |  * **          |                 |        |       |       |
;*  12: |  * **          |                 |        |       |       |
;*  13: |  * **          |                 |        |       |       |
;*  14: |* * **          |                 |        |       |       |
;*  15: |******          |                 |        |       |       |
;*  16: | ** **          |                 |        |       |       |
;*  17: | ** **          |                 |        |       |       |
;*  18: | ** **          |                 |        |       |       |
;*  19: | ** **          |                 |        |       |       |
;*  20: | ** **          |                 |        |       |       |
;*  21: |******          |                 |        |       |       |
;*  22: |******          |                 |        |       |       |
;*  23: | ** **          |                 |        |       |       |
;*  24: | ** **          |                 |        |       |       |
;*  25: | ** **          |                 |        |       |       |
;*  26: | ** **          |                 |        |       |       |
;*  27: | ** **          |                 |        |       |       |
;*  28: | ** **          |                 |        |       |       |
;*  29: |*** **          |                 |        |       |       |
;*  30: |* * **          |                 |        |       |       |
;*  31: |* * **          |                 |        |       |       |
;*  32: |* * **          |                 |        |       |       |
;*  33: |* * **          |                 |        |       |       |
;*  34: |* * **          |                 |        |       |       |
;*  35: |* * **          |                 |        |       |       |
;*  36: |* * **          |                 |        |       |       |
;*  37: |* * **          |                 |        |       |       |
;*      +----------------+                 +--------+       +-------+
;*
;*      Done
;*
;*      Collapsed epilog stages       : 1
;*      Prolog not removed
;*      Collapsed prolog stages       : 0
;*
;*      Max amt of load speculation   : 0 bytes
;*
;*      Minimum safe trip count       : 1
;*      Min. prof. trip count  (est.) : 2
;*
;*      Mem bank conflicts/iter(est.) : { min 0.000, est 0.000, max 0.000 }
;*      Mem bank perf. penalty (est.) : 0.0%
;*
;*
;*      Total cycles (est.)         : 1 + trip_cnt * 38        
;*----------------------------------------------------------------------------*
;*        SINGLE SCHEDULED ITERATION
;*
;*        ||$C$C1657||:
;*   0              TICK                               ; [A_U] 
;*   1              LDD     .D1     *D2(1176),D0      ; [A_D1] |368|  ^ [C1]
;*   2              NOP     0x5     ; [A_B] 
;*   7              ADDD    .D1     D4,D0,D0          ; [A_D1] |368|  ^ 
;*   8              LDW     .D1     *D0(0),A0         ; [A_D1] |368|  ^ 
;*   9              NOP     0x1     ; [A_B] 
;*  10              LDD     .D2     *D2(1200),D0      ; [A_D2] |371| [C0]
;*     ||           LDD     .D1     *D2(1192),D3      ; [A_D1] |371| [C1]
;*  11              NOP     0x3     ; [A_B] 
;*  14              SHRW    .L1     A0,0x10,D0        ; [A_L1] |371|  ^ 
;*  15              ANDW    .D1     D5,D0,D1          ; [A_D1] |371|  ^ 
;*  16              LDW     .D2     *D0[D1],D0        ; [A_D2] |371|  ^ 
;*     ||           LDW     .D1     *D3[D1],D3        ; [A_D1] |371|  ^ 
;*  17              LDD     .D1     *D2(1184),D3      ; [A_D1] |371| [C1]
;*  18              NOP     0x4     ; [A_B] 
;*  22              ADDW    .D1     D3,D0,D0          ; [A_D1] |371|  ^ 
;*  23              STW     .D1     A0,*D3[D0]        ; [A_D1] |371|  ^ 
;*  24              LDD     .D1     *D2(1192),D0      ; [A_D1] |372|  ^ [C1]
;*  25              NOP     0x5     ; [A_B] 
;*  30              ADDAW   .D1     D0,D1,D0          ; [A_D1] |372|  ^ 
;*  31              LDW     .D1     *D0(0),BL0        ; [A_D1] |372|  ^ 
;*  32              NOP     0x5     ; [A_B] 
;*  37              ADDW    .L2     BL0,0x1,B0        ; [B_L2] |372|  ^ 
;*  38              STW     .D1X    B0,*D0(0)         ; [A_D1] |372|  ^ 
;*     ||           ADDD    .D2     D4,0x4,D4         ; [A_D2] |366| 
;*     ||           BNL     .B1     ||$C$C1657||      ; [A_B] |366| 
;*  39              ; BRANCHCC OCCURS {||$C$C1657||}  ; [] |366| 
;*----------------------------------------------------------------------------*
||$C$L55||:    ; PIPED LOOP PROLOG
;** --------------------------------------------------------------------------*
||$C$L56||:    ; PIPED LOOP KERNEL
;          EXCLUSIVE CPU CYCLES: 38
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 368,column 24,is_stmt,isa 0
           LDD     .D1     *D2(1176),D0      ; [A_D1] |368| <0,1>  ^ [C1]
           NOP             0x5               ; [A_B] 
           ADDD    .D1     D4,D0,D0          ; [A_D1] |368| <0,7>  ^ 
           LDW     .D1     *D0(0),A0         ; [A_D1] |368| <0,8>  ^ 
           NOP             0x1               ; [A_B] 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 371,column 7,is_stmt,isa 0

           LDD     .D2     *D2(1200),D0      ; [A_D2] |371| <0,10> [C0]
||         LDD     .D1     *D2(1192),D3      ; [A_D1] |371| <0,10> [C1]

           NOP             0x3               ; [A_B] 
           SHRW    .L1     A0,0x10,D0        ; [A_L1] |371| <0,14>  ^ 
           ANDW    .D1     D5,D0,D1          ; [A_D1] |371| <0,15>  ^ 

           LDW     .D2     *D0[D1],D0        ; [A_D2] |371| <0,16>  ^ 
||         LDW     .D1     *D3[D1],D3        ; [A_D1] |371| <0,16>  ^ 

           LDD     .D1     *D2(1184),D3      ; [A_D1] |371| <0,17> [C1]
           NOP             0x4               ; [A_B] 
           ADDW    .D1     D3,D0,D0          ; [A_D1] |371| <0,22>  ^ 
           STW     .D1     A0,*D3[D0]        ; [A_D1] |371| <0,23>  ^ 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 372,column 7,is_stmt,isa 0
           LDD     .D1     *D2(1192),D0      ; [A_D1] |372| <0,24>  ^ [C1]
           NOP             0x5               ; [A_B] 
           ADDAW   .D1     D0,D1,D0          ; [A_D1] |372| <0,30>  ^ 
           LDW     .D1     *D0(0),BL0        ; [A_D1] |372| <0,31>  ^ 
           NOP             0x5               ; [A_B] 
           ADDW    .L2     BL0,0x1,B0        ; [B_L2] |372| <0,37>  ^ 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 366,column 25,is_stmt,isa 0

           ADDD    .D2     D4,0x4,D4         ; [A_D2] |366| <0,38> 
||         BNL     .B1     ||$C$L56||        ; [A_B] |366| <0,38> 
||         STW     .D1X    B0,*D0(0)         ; [A_D1] |372| <0,38>  ^ 
||         TICK                               ; [A_U] <1,0> 

;** --------------------------------------------------------------------------*
||$C$L57||:    ; PIPED LOOP EPILOG
;          EXCLUSIVE CPU CYCLES: 1
           PROTCLR                            ; [A_U] 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 1
           MV      .D1     D2,A5             ; [A_D1] 
;** --------------------------------------------------------------------------*
||$C$L58||:    
;          EXCLUSIVE CPU CYCLES: 8
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 377,column 3,is_stmt,isa 0
           LDW     .D1     *A6(68),AL0       ; [A_D1] |377| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 374,column 5,is_stmt,isa 0

           LDW     .D1     *A6(12),B2        ; [A_D1] |374| 
||         LDW     .D2     *A5(1328),BL0     ; [A_D2] |374| 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 377,column 3,is_stmt,isa 0
           CMPEQW  .L1     AL0,0x1,A1        ; [A_L1] |377| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 374,column 5,is_stmt,isa 0

   [ A1]   B       .B1     ||$C$L59||        ; [A_B] |377| 
||         SUBW    .L2     B2,BL0,B0         ; [B_L2] |374| 
||         CMPEQW  .L1     AL0,0x2,A0        ; [A_L1] |466| 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 377,column 3,is_stmt,isa 0
           ; BRANCHCC OCCURS {||$C$L59||}    ; [] |377| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 1
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 466,column 8,is_stmt,isa 0

   [ A0]   B       .B1     ||$C$L74||        ; [A_B] |466| 
||         MVKU32  .L1     0,A9              ; [A_L1] |580| 

           ; BRANCHCC OCCURS {||$C$L74||}    ; [] |466| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 1
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 580,column 3,is_stmt,isa 0
           B       .B1     ||$C$L101||       ; [A_B] |580| 
           ; BRANCH OCCURS {||$C$L101||}     ; [] |580| 
;** --------------------------------------------------------------------------*
||$C$L59||:    
;          EXCLUSIVE CPU CYCLES: 8
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 379,column 5,is_stmt,isa 0
           LDW     .D1     *A6(72),AL0       ; [A_D1] |379| 

           CMPEQW  .L1     AL0,0x4,A1        ; [A_L1] |379| 
||         CMPGTW  .S1X    B0,0,A0           ; [A_S1] |381| 

   [!A1]   B       .B1     ||$C$L69||        ; [A_B] |379| 
||         CMPGTW  .L1X    B0,0,A2           ; [A_L1] |413| 

           ; BRANCHCC OCCURS {||$C$L69||}    ; [] |379| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 451,column 27,is_stmt,isa 0

           CMPGTW  .L1X    B2,0,A0           ; [A_L1] |451| 
||         MVKU32  .S1     0,D2              ; [A_S1] 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 413,column 29,is_stmt,isa 0

   [!A2]   B       .B1     ||$C$L60||        ; [A_B] |413| 
||         MV      .D1     D2,D1             ; [A_D1] 
||         MV      .S1X    B0,A1             ; [A_S1] 
||         MVKU32  .L1     0,A9              ; [A_L1] 

           ; BRANCHCC OCCURS {||$C$L60||}    ; [] |413| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 8

           MVKU32  .L2     0xff,B14          ; [B_L2] 
||         MVKU32  .S2     0xffff,BL7        ; [B_S2] 

           MVKU32  .L2     0x37800000,B10    ; [B_L2] 
||         MVKU32  .S2     0x3d2aaab4,BM3    ; [B_S2] 

           MVKU32  .L2     0x3e2aaaad,BM1    ; [B_L2] 
||         MVKU32  .S2     0x3f000000,BM0    ; [B_S2] 

           MVKU32  .L2     0x10000,BL0       ; [B_L2] 
||         MVKU32  .S2     0x3f800000,B9     ; [B_S2] 

           MVKU32  .L2     0,B2              ; [B_L2] 
||         MVKU32  .S2     0x3f317218,BM2    ; [B_S2] 

           MVKU32  .L2     0x7f7fffff,B0     ; [B_L2] 
||         MVKU32  .S2     0x3fb8aa3b,BM4    ; [B_S2] 

	.dwpsn	file "./inc/tidl_detectionOutput_int.h",line 229,column 1,is_stmt,isa 0

           MVKU32  .L2     0x40000000,B8     ; [B_L2] 
||         MVKU32  .S2     0x46fffe00,B13    ; [B_S2] 
||         MASKB   .P2     0x4,P2            ; [B_P] |229| 

	.dwpsn	file "./inc/tidl_detectionOutput_int.h",line 134,column 25,is_stmt,isa 0

           B       .B1     ||$C$L61||        ; [A_B] |413| 
||         MVKU32  .L1     0xc,AM0           ; [A_L1] |134| 
||         MVKU32  .L2     0,BL5             ; [B_L2] |229| 
||         MVKU32  .S2     0,BL1             ; [B_S2] |229| 
||         MASKB   .P2     0x4,P1            ; [B_P] |229| 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 413,column 29,is_stmt,isa 0
           ; BRANCH OCCURS {||$C$L61||}      ; [] |413| 
;** --------------------------------------------------------------------------*
||$C$L60||:    
;          EXCLUSIVE CPU CYCLES: 1
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 451,column 27,is_stmt,isa 0
   [ A0]   B       .B1     ||$C$L64||        ; [A_B] |451| 
           ; BRANCHCC OCCURS {||$C$L64||}    ; [] |451| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 1
           B       .B1     ||$C$L68||        ; [A_B] |451| 
           ; BRANCH OCCURS {||$C$L68||}      ; [] |451| 
;** --------------------------------------------------------------------------*
;**   BEGIN LOOP ||$C$L61||
;** --------------------------------------------------------------------------*
||$C$L61||:    
;          EXCLUSIVE CPU CYCLES: 15
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 417,column 31,is_stmt,isa 0
           LDD     .D1     *A5(1192),D3      ; [A_D1] |417| 
           ADDD    .D1     D2,D3,D3          ; [A_D1] |417| 
           LDW     .D1     *D3(0),A0         ; [A_D1] |417| 
           CMPGTW  .L1     A0,0,A2           ; [A_L1] |417| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 416,column 9,is_stmt,isa 0

   [!A2]   B       .B1     ||$C$L63||        ; [A_B] |417| 
||         STW     .D1X    B1,*D3(0)         ; [A_D1] |416| 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 417,column 31,is_stmt,isa 0
           ; BRANCHCC OCCURS {||$C$L63||}    ; [] |417| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 1
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 417,column 26,is_stmt,isa 0

           ADDD    .D1     A5,0x140,D3       ; [A_D1] 
||         ADDD    .D2     A5,0x244,D4       ; [A_D2] 
||         MVKU32  .L1     0,A7              ; [A_L1] |417| 

;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*
;*      Loop found in file               : src/tidl_detectionOutput_score.c
;*      Loop source line                 : 417
;*      Loop opening brace source line   : 418
;*      Loop closing brace source line   : 449
;*      Known Minimum Trip Count         : 1                    
;*      Known Max Trip Count Factor      : 1
;*      Loop Carried Dependency Bound(^) : 162
;*      Unpartitioned Resource Bound     : 12
;*      Partitioned Resource Bound       : 16 (pre-sched)
;*      Disqualified loop: Loop carried dependency bound too large
;*----------------------------------------------------------------------------*
||$C$L62||:    
;          EXCLUSIVE CPU CYCLES: 163
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 419,column 11,is_stmt,isa 0
           LDD     .D1     *A5(1200),D0      ; [A_D1] |419| 
           ADDD    .D1     D2,D0,D0          ; [A_D1] |419| 
           LDW     .D1     *D0(0),D0         ; [A_D1] |419| 
           LDD     .D1     *A5(1184),D5      ; [A_D1] |419| 
           ADDW    .D1     A7,D0,D0          ; [A_D1] |419| 
           LDW     .D1     *D5[D0],B3        ; [A_D1] |419| 
	.dwpsn	file "./inc/tidl_detectionOutput_int.h",line 134,column 25,is_stmt,isa 0
           SHRW    .L2     B3,0x1c,BL2       ; [B_L2] |134| 

           ANDW    .L2     BL2,0xf,B4        ; [B_L2] |134| 
||         LDW     .D1     *A6(12),B12       ; [A_D1] |134| 

           EXT     .L1X    B4,0x20,0x20,A2   ; [A_L1] |134| 
           MPYDD   .N1     AM0,A2,D0         ; [A_N1] |134| 
           SHRW    .L2     B3,0x18,BL3       ; [B_L2] |134| 

           ADDW    .L2     B12,0x5,B4        ; [B_L2] |134| 
||         ANDW    .S2     BL3,0xf,B5        ; [B_S2] |134| 

           MPYWW   .N2     B5,B4,BL2         ; [B_N2] |134| 
||         ADDD    .D1     D4,D0,D0          ; [A_D1] |134| 

           LDW     .D1     *D0(0),AM1        ; [A_D1] |134| 
           SHRW    .L2     B3,0x10,BL6       ; [B_L2] |134| 
           ANDW    .L2     B14,BL6,BL3       ; [B_L2] |134| 
           ADDW    .L2     BL3,BL2,B6        ; [B_L2] |134| 
           SHLD    .L1     A2,0x3,D14        ; [A_L1] |134| 

           ADDW    .L1X    B6,0x1,AM3        ; [A_L1] |134| 
||         ADDD    .D1     A5,D14,D0         ; [A_D1] |134| 

           LDD     .D1     *D0(192),D0       ; [A_D1] |134| 
||         MPYWW   .N1X    B5,AM1,AM2        ; [A_N1] |134| 

           MPYWW   .N1     AM1,AM3,AM1       ; [A_N1] |134| 

           SHRW    .L1X    B3,0x1c,AL0       ; [A_L1] |134| 
||         ANDW    .L2     BL7,B3,B11        ; [B_L2] |134| 

           MPYWW   .N1X    B4,AM2,AM2        ; [A_N1] |134| 

           ADDW    .M1X    B11,AM1,D5        ; [A_M1] |134| 
||         ANDW    .L1     AL0,0xf,D6        ; [A_L1] |134| 

           LDUW    .D1     *D0[D5],B6        ; [A_D1] |134| 
||         LDUW    .D2     *D3[D6],B7        ; [A_D2] |134| 

           ADDW    .M1X    B11,AM2,D5        ; [A_M1] |134| 
           LDUW    .D1     *D0[D5],B5        ; [A_D1] |134| 
           MPYSP   .N2     B7,B6,BL2         ; [B_N2] |134| 
           MPYSP   .N2     B7,B5,BL3         ; [B_N2] |134| 
           VXORW   .L2     BL2,0x80000000,BM5 ; [B_L2] |134| 
           MPYSP   .N2     BM4,BM5,B7        ; [B_N2] |134| 
           VXORW   .L2     BL3,0x80000000,BM5 ; [B_L2] |134| 
           MPYSP   .N2     BM4,BM5,B6        ; [B_N2] |134| 
	.dwpsn	file "./inc/tidl_detectionOutput_int.h",line 229,column 1,is_stmt,isa 0
           VSPTRUNC .L2    B7,B4             ; [B_L2] |229| 

           VSPTRUNC .L2    B6,B5             ; [B_L2] |229| 
||         VINTSP  .S2     B4,BM5            ; [B_S2] |229| 

           VINTSP  .L2     B5,BM5            ; [B_L2] |229| 
||         SUBSP   .C2     B7,BM5,BM6        ; [B_C] |229| 

           SUBSP   .C2     B6,BM5,BM5        ; [B_C] |229| 
||         MPYSP   .N2     BM2,BM6,BM7       ; [B_N2] |229| 

           MPYSP   .N2     BM2,BM5,B6        ; [B_N2] |229| 
           MPYSP   .N2     BM7,BM7,BM6       ; [B_N2] |229| 

           MPYSP   .N2     B6,B6,BM5         ; [B_N2] |229| 
||         CMPGEW  .L1X    B4,0xfffffff0,A3  ; [A_L1] |229| 

   [ A3]   MPYSP   .N2     BM7,BM6,B11       ; [B_N2] |229| 
   [ A3]   MPYSP   .N2     BM0,BM6,B7        ; [B_N2] |229| 

   [ A3]   ADDSP   .C2     B9,BM7,BM7        ; [B_C] |229| 
||         CMPGEW  .L1X    B5,0xfffffff0,A2  ; [A_L1] |229| 

   [ A2]   MPYSP   .N2     B6,BM5,BM6        ; [B_N2] |229| 
|| [ A3]   MPYSP   .M2     BM6,BM6,B12       ; [B_M2] |229| 

   [ A2]   MPYSP   .N2     BM0,BM5,BL2       ; [B_N2] |229| 
|| [ A3]   MPYSP   .M2     BM1,B11,B11       ; [B_M2] |229| 

   [ A3]   ADDSP   .C2     B7,BM7,BM7        ; [B_C] |229| 
|| [ A2]   ADDSP   .L2     B9,B6,BL3         ; [B_L2] |229| 

   [ A2]   MPYSP   .N2     BM5,BM5,BM5       ; [B_N2] |229| 

   [ A3]   MPYSP   .N2     BM3,B12,BM6       ; [B_N2] |229| 
|| [ A2]   MPYSP   .M2     BM1,BM6,BL4       ; [B_M2] |229| 

   [ A3]   ADDSP   .C2     B11,BM7,BM7       ; [B_C] |229| 
||         SUBRW   .M2     B4,0,BL3          ; [B_M2] |229| 
|| [ A2]   ADDSP   .S2     BL2,BL3,BL2       ; [B_S2] |229| 
||         VCMPGTW .L2     B4,BL5,P0         ; [B_L2] |229| 

   [ A3]   SHRW    .L2     BL0,BL3,BL3       ; [B_L2] |229| 
|| [ A3]   AND     .P2     P0,P2,P0          ; [B_P] |229| 
||         SHLW    .S2     BL0,B4,BL6        ; [B_S2] |229| 

   [ A3]   VSEL    .L2     P0,BL6,BL3,B6     ; [B_L2] |229| 
|| [ A2]   MPYSP   .N2     BM3,BM5,BM5       ; [B_N2] |229| 

           SUBRW   .M2     B5,0,BL2          ; [B_M2] |229| 
|| [ A2]   ADDSP   .S2     BL4,BL2,BM6       ; [B_S2] |229| 
|| [ A3]   ADDSP   .C2     BM6,BM7,B7        ; [B_C] |229| 
||         VCMPGTW .L2     B5,BL1,P0         ; [B_L2] |229| 

   [ A2]   SHRW    .L2     BL0,BL2,BL2       ; [B_L2] |229| 
|| [ A2]   AND     .P2     P0,P1,P0          ; [B_P] |229| 
||         SHLW    .S2     BL0,B5,BL3        ; [B_S2] |229| 

   [ A2]   VSEL    .L2     P0,BL3,BL2,BL2    ; [B_L2] |229| 
|| [ A3]   VINTSP  .L1X    B6,AM1            ; [A_L1] |229| 

   [ A2]   VINTSP  .L2     BL2,BM5           ; [B_L2] |229| 
|| [ A2]   ADDSP   .C2     BM5,BM6,BM6       ; [B_C] |229| 

   [ A3]   MPYSP   .N1X    B7,AM1,AM1        ; [A_N1] |229| 
   [ A2]   MPYSP   .N2     BM6,BM5,BM5       ; [B_N2] |229| 
   [ A3]   MPYSP   .N1X    B10,AM1,D0        ; [A_N1] |229| 

   [ A2]   MPYSP   .N2     B10,BM5,BL2       ; [B_N2] |229| 
||         CMPGTW  .L1X    B4,0xe,A4         ; [A_L1] |229| 

   [ A4]   MV      .L1X    B0,AL0            ; [A_L1] |229| 
   [!A3]   MV      .L1X    B2,D0             ; [A_L1] |229| 

           CMPGTW  .L1X    B5,0xe,A2         ; [A_L1] |229| 
|| [!A4]   MV      .D1     D0,AL0            ; [A_D1] |229| 
|| [!A2]   MV      .L2     B2,BL2            ; [B_L2] |229| 

   [!A2]   MV      .L2     BL2,BM5           ; [B_L2] |229| 
|| [ A2]   MV      .S2     B0,BM5            ; [B_S2] |229| 
||         ADDSP   .L1X    B9,AL0,A3         ; [A_L1] |229| 

           ADDSP   .C2     B9,BM5,B4         ; [B_C] |229| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 441,column 11,is_stmt,isa 0
           VRCPSP  .S1     A3,AM1            ; [A_S1] |441| 
           VRCPSP  .S2     B4,BM5            ; [B_S2] |441| 
           MPYSP   .N1     A3,AM1,AL0        ; [A_N1] |441| 
           MPYSP   .N2     B4,BM5,BM6        ; [B_N2] |441| 
           SUBSP   .L1X    B8,AL0,AM2        ; [A_L1] |441| 
           SUBSP   .C2     B8,BM6,BM6        ; [B_C] |441| 
           MPYSP   .N1     AM1,AM2,AM1       ; [A_N1] |441| 
           MPYSP   .N2     BM5,BM6,BM5       ; [B_N2] |441| 
           MPYSP   .N1     A3,AM1,AL0        ; [A_N1] |441| 
           MPYSP   .N2     B4,BM5,BM6        ; [B_N2] |441| 
           SUBSP   .L1X    B8,AL0,AM2        ; [A_L1] |441| 
           SUBSP   .C2     B8,BM6,BM6        ; [B_C] |441| 
           MPYSP   .N1     AM1,AM2,A2        ; [A_N1] |441| 
           MPYSP   .N2     BM5,BM6,BM5       ; [B_N2] |441| 

           MPYSP   .N2X    A2,BM5,B4         ; [B_N2] |441| 
||         LDUW    .D1     *A6(24),AL1       ; [A_D1] |441| 

           MV      .L1X    B4,AL0            ; [A_L1] |441| Define a twin register
           CMPLESP .L1     AL1,AL0,A2        ; [A_L1] |441| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 444,column 13,is_stmt,isa 0
   [ A2]   MPYSP   .N2     B13,B4,BL3        ; [B_N2] |444| 
   [ A2]   LDD     .D1     *A5(1152),D5      ; [A_D1] |444| 
   [ A2]   VSPTRUNC .L2    BL3,B4            ; [B_L2] |444| 
   [ A2]   STH     .D1X    B4,*D5[D1]        ; [A_D1] |444| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 445,column 13,is_stmt,isa 0
   [ A2]   LDD     .D1     *A5(1176),D5      ; [A_D1] |445| 
   [ A2]   STW     .D1X    B3,*D5[D1]        ; [A_D1] |445| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 446,column 13,is_stmt,isa 0
   [ A2]   LDD     .D1     *A5(1192),D5      ; [A_D1] |446| 
   [ A2]   ADDD    .D1     D2,D5,D5          ; [A_D1] |446| 
   [ A2]   LDW     .D1     *D5(0),BL3        ; [A_D1] |446| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 417,column 31,is_stmt,isa 0

   [ A2]   ADDW    .L2     BL3,0x1,B3        ; [B_L2] |446| 
||         ADDW    .D1     A0,0xffffffff,A0  ; [A_D1] |417| 

   [ A0]   B       .B1     ||$C$L62||        ; [A_B] |417| 
|| [ A2]   STW     .D1X    B3,*D5(0)         ; [A_D1] |446| 
|| [ A2]   ADDW    .D2     D1,0x1,D1         ; [A_D2] |447| 
||         ADDW    .L1     A7,0x1,A7         ; [A_L1] |417| 

           ; BRANCHCC OCCURS {||$C$L62||}    ; [] |417| 
;** --------------------------------------------------------------------------*
||$C$L63||:    
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 413,column 29,is_stmt,isa 0
           ADDW    .D1     A1,0xffffffff,A1  ; [A_D1] |413| 

   [ A1]   B       .B1     ||$C$L61||        ; [A_B] |413| 
||         ADDD    .D1     D2,0x4,D2         ; [A_D1] |413| 

           ; BRANCHCC OCCURS {||$C$L61||}    ; [] |413| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 8
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 451,column 27,is_stmt,isa 0
           LDW     .D1     *A6(12),AL0       ; [A_D1] |451| 
           CMPGTW  .L1     AL0,0,A0          ; [A_L1] |451| 

   [!A0]   B       .B1     ||$C$L68||        ; [A_B] |451| 
||         MV      .D1     D1,A9             ; [A_D1] 

           ; BRANCHCC OCCURS {||$C$L68||}    ; [] |451| 
;** --------------------------------------------------------------------------*
||$C$L64||:    
;          EXCLUSIVE CPU CYCLES: 2
           MV      .D1     A5,D1             ; [A_D1] 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 451,column 22,is_stmt,isa 0

           SLDD    .D1     *D1(1200),D2      ; [A_D1] <0,0>  ^ [C0]
||         UNPROT          0x1               ; [A_U] 
||         MVKU32  .L1     0,A8              ; [A_L1] |451| 
||         MV      .D2     A6,D0             ; [A_D2] 
||         MVKU32  .S1     0,A1              ; [A_S1] 

;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*
;*      Loop found in file               : src/tidl_detectionOutput_score.c
;*      Loop source line                 : 451
;*      Loop opening brace source line   : 452
;*      Loop closing brace source line   : 461
;*      Known Minimum Trip Count         : 1                    
;*      Known Max Trip Count Factor      : 1
;*      Loop Carried Dependency Bound(^) : 16
;*      Unpartitioned Resource Bound     : 4
;*      Partitioned Resource Bound       : 4 (pre-sched)
;*
;*      Searching for software pipeline schedule at ...
;*         ii = 16 Unsafe schedule for irregular loop
;*         ii = 16 Unsafe schedule for irregular loop
;*         ii = 16 Did not find schedule
;*         ii = 17 Schedule found with 2 iterations in parallel
;*
;*      Partitioned Resource Bound(*)    : 6 (post-sched)
;*
;*      Constant Extension #0 Used [C0]  : 1
;*      Constant Extension #1 Used [C1]  : 1
;*
;*      Resource Partition (may include "post-sched" split/spill moves):
;*
;*                                                A-side   B-side
;*      .L units                                     0        0     
;*      .S units                                     0        0     
;*      .M units                                     0        0     
;*      .N units                                     0        0     
;*      .D units                                     7        -     
;*      .B units                                     1        -     
;*      .C units                                     -        0     
;*      .P units                                     -        0     
;*
;*      .M/.N units                                  0        0     
;*      .L/.S units                                  1        0     
;*      .L/.S/.C units                               0        0     
;*      .L/.S/.C/.M units                            0        0     
;*      .L/.S/.C/.M/.D units                         6        1     
;*
;*      .X cross paths                               1        0     
;*
;*      Bound(.D1 .D2 .D)                            4        -     
;*      Bound(.M .N .MN)                             0        0     
;*      Bound(.L .S .LS)                             1        0     
;*      Bound(.L .S .C .LS .LSC)                     1        0     
;*      Bound(.L .S .C .M .LS .LSC .LSCM)            1        0     
;*      Bound(.L .S .C .M .D .LS .LSC .LSCM .LSCMD)  3        1     
;*
;*      Register Usage Tables:
;*
;*      +----------------------------------+----------------------------------+
;*      |              ASIDE               |              BSIDE               |
;*      +----------------+--------+--------+----------------+--------+--------+
;*      |      "Axx"     |  "ALx" |  "AMx" |     "VBxx"     | "VBLx" | "VBMx" |
;*      +----------------|--------|--------|----------------|--------|--------+
;*      |0000000000111111|00000000|00000000|0000000000111111|00000000|00000000|
;*      |0123456789012345|01234567|01234567|0123456789012345|01234567|01234567|
;*      +----------------|--------|--------|----------------|--------|--------+
;*   0: | *      *       |        |        | *              |        |        |
;*   1: | *      *       |        |        | *              |        |        |
;*   2: | *      *       |        |        | *              |        |        |
;*   3: | *      *       |        |        | *              |        |        |
;*   4: | *      *       |        |        | *              |        |        |
;*   5: | *      *       |        |        | *              |        |        |
;*   6: | *      *       |        |        | *              |        |        |
;*   7: | *      *       |        |        | *              |**      |        |
;*   8: | *      *       |        |        |**              |        |        |
;*   9: | *      *       |        |        | *              |        |        |
;*  10: | *      *       |        |        | *              |        |        |
;*  11: | *      *       |        |        | *              |        |        |
;*  12: | *      *       |        |        | *              |        |        |
;*  13: | *      *       |        |        | *              |        |        |
;*  14: | *      *       |        |        | *              |        |        |
;*  15: | *      *       |*       |        | *              |        |        |
;*  16: |**      *       |        |        | *              |        |        |
;*      +----------------+--------+--------+----------------+--------+--------+
;*
;*      +----------------+                 +--------+       +-------+
;*      |      "Dxx"     |                 |  "Px"  |       |"CUCRx"|
;*      +----------------+                 +--------+       +-------+
;*      |0000000000111111|                 |00000000|       |0000   |
;*      |0123456789012345|                 |01234567|       |0123   |
;*      +----------------+                 +--------+       +-------+
;*   0: |*** * *         |                 |        |       |       |
;*   1: |*******         |                 |        |       |       |
;*   2: |*** * *         |                 |        |       |       |
;*   3: |*** * *         |                 |        |       |       |
;*   4: |*** * *         |                 |        |       |       |
;*   5: |*** * *         |                 |        |       |       |
;*   6: |*** * *         |                 |        |       |       |
;*   7: |*** * *         |                 |        |       |       |
;*   8: |*** * *         |                 |        |       |       |
;*   9: |**  * *         |                 |        |       |       |
;*  10: |**  * *         |                 |        |       |       |
;*  11: |**  * *         |                 |        |       |       |
;*  12: |**  * *         |                 |        |       |       |
;*  13: |**  * *         |                 |        |       |       |
;*  14: |**  * *         |                 |        |       |       |
;*  15: |**  * *         |                 |        |       |       |
;*  16: |***** *         |                 |        |       |       |
;*      +----------------+                 +--------+       +-------+
;*
;*      Done
;*
;*      Collapsed epilog stages       : 1
;*      Prolog not removed
;*      Collapsed prolog stages       : 0
;*
;*      Max amt of load speculation   : 0 bytes
;*
;*      Minimum safe trip count       : 1
;*      Min. prof. trip count  (est.) : 2
;*
;*      Mem bank conflicts/iter(est.) : { min 0.000, est 0.000, max 0.000 }
;*      Mem bank perf. penalty (est.) : 0.0%
;*
;*
;*      Total cycles (est.)         : 7 + trip_cnt * 17        
;*----------------------------------------------------------------------------*
;*        SINGLE SCHEDULED ITERATION
;*
;*        ||$C$C1402||:
;*   0              SLDD    .D2     *D1(1200),D2      ; [A_D2]  ^ [C0]
;*     ||   [ A1]   SLDD    .D1     *D1(1192),D3      ; [A_D1] |459|  ^ [C1]
;*   1              NOP     0x5     ; [A_B] 
;*   6      [ A1]   ADDD    .D2     D2,A1,D4          ; [A_D2] |459|  ^ 
;*     ||   [ A1]   ADDD    .D1     A1,D3,D6          ; [A_D1] |459|  ^ 
;*   7      [ A1]   ADDD    .D2     D4,0xfffffffc,D3  ; [A_D2] |459|  ^ 
;*     ||   [ A1]   ADDD    .D1     D6,0xfffffffc,D5  ; [A_D1] |459|  ^ 
;*   8      [ A1]   LDW     .D2     *D3(0),BL0        ; [A_D2] |459|  ^ 
;*     ||   [ A1]   LDW     .D1     *D5(0),BL1        ; [A_D1] |459|  ^ 
;*   9              NOP     0x5     ; [A_B] 
;*  14      [ A1]   ADDW    .L2     BL1,BL0,B0        ; [B_L2] |459|  ^ 
;*  15      [!A1]   STW     .D2     B1,*D2[A8]        ; [A_D2] |455| 
;*     ||   [ A1]   STW     .D1X    B0,*D2[A8]        ; [A_D1] |459|  ^ 
;*  16              ADDD    .D2     A1,0x4,A1         ; [A_D2] |451|  ^ 
;*     ||           LDW     .D1     *D0(12),AL0       ; [A_D1] |451| 
;*  17              NOP     0x4     ; [A_B] 
;*  21              ADDW    .D1     A8,0x1,A8         ; [A_D1] |451| 
;*  22              CMPGTW  .L1     AL0,A8,A0         ; [A_L1] |451| 
;*  23      [ A0]   B       .B1     ||$C$C1402||      ; [A_B] |451| 
;*  24              ; BRANCHCC OCCURS {||$C$C1402||}  ; [] |451| 
;*----------------------------------------------------------------------------*
||$C$L65||:    ; PIPED LOOP PROLOG
;** --------------------------------------------------------------------------*
||$C$L66||:    ; PIPED LOOP KERNEL
;          EXCLUSIVE CPU CYCLES: 17
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 459,column 11,is_stmt,isa 0

   [ A1]   ADDD    .D2     D4,0xfffffffc,D3  ; [A_D2] |459| <0,7>  ^ 
|| [ A1]   ADDD    .D1     D6,0xfffffffc,D5  ; [A_D1] |459| <0,7>  ^ 

   [ A1]   LDW     .D2     *D3(0),BL0        ; [A_D2] |459| <0,8>  ^ 
|| [ A1]   LDW     .D1     *D5(0),BL1        ; [A_D1] |459| <0,8>  ^ 

           NOP             0x5               ; [A_B] 
   [ A1]   ADDW    .L2     BL1,BL0,B0        ; [B_L2] |459| <0,14>  ^ 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 455,column 11,is_stmt,isa 0

   [!A1]   STW     .D2     B1,*D2[A8]        ; [A_D2] |455| <0,15> 
|| [ A1]   STW     .D1X    B0,*D2[A8]        ; [A_D1] |459| <0,15>  ^ 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 451,column 27,is_stmt,isa 0

           ADDD    .D2     A1,0x4,A1         ; [A_D2] |451| <0,16>  ^ 
||         LDW     .D1     *D0(12),AL0       ; [A_D1] |451| <0,16> 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 459,column 11,is_stmt,isa 0

           SLDD    .D2     *D1(1200),D2      ; [A_D2] <1,0>  ^ [C0]
|| [ A1]   SLDD    .D1     *D1(1192),D3      ; [A_D1] |459| <1,0>  ^ [C1]

           NOP             0x3               ; [A_B] 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 451,column 27,is_stmt,isa 0
           ADDW    .D1     A8,0x1,A8         ; [A_D1] |451| <0,21> 
           CMPGTW  .L1     AL0,A8,A0         ; [A_L1] |451| <0,22> 

   [ A0]   B       .B1     ||$C$L66||        ; [A_B] |451| <0,23> 
|| [ A1]   ADDD    .D2     D2,A1,D4          ; [A_D2] |459| <1,6>  ^ 
|| [ A1]   ADDD    .D1     A1,D3,D6          ; [A_D1] |459| <1,6>  ^ 

;** --------------------------------------------------------------------------*
||$C$L67||:    ; PIPED LOOP EPILOG
;          EXCLUSIVE CPU CYCLES: 1
           PROTCLR                            ; [A_U] 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 1
           MV      .D1     D1,A5             ; [A_D1] 
;** --------------------------------------------------------------------------*
||$C$L68||:    
;          EXCLUSIVE CPU CYCLES: 7
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 462,column 7,is_stmt,isa 0

           LDD     .D1     *A5(1176),B1      ; [A_D1] |462| 
||         LDD     .D2     *A5(1152),B0      ; [A_D2] |463| 

           B       .B1     ||$C$L101||       ; [A_B] |580| 
||         STD     .D1X    B1,*A5(1184)      ; [A_D1] |462| 
||         STD     .D2     B0,*A5(1160)      ; [A_D2] |463| 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 580,column 3,is_stmt,isa 0
           ; BRANCH OCCURS {||$C$L101||}     ; [] |580| 
;** --------------------------------------------------------------------------*
||$C$L69||:    
;          EXCLUSIVE CPU CYCLES: 1
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 381,column 29,is_stmt,isa 0

   [!A0]   B       .B1     ||$C$L73||        ; [A_B] |381| 
||         MVKU32  .L1     0,D2              ; [A_L1] 
||         MV      .S1X    B0,A1             ; [A_S1] 

           ; BRANCHCC OCCURS {||$C$L73||}    ; [] |381| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 8
           MVKU32  .L1     0xff,D3           ; [A_L1] 

           MVKU32  .L2     0xffff,BL4        ; [B_L2] 
||         MVKU32  .S2     0x37800000,BM7    ; [B_S2] 

           MVKU32  .L2     0x3d2aaab4,BM6    ; [B_L2] 
||         MVKU32  .S2     0x3e2aaaad,BM4    ; [B_S2] 

           MVKU32  .L2     0x10000,BL0       ; [B_L2] 
||         MVKU32  .S2     0x3f000000,BM3    ; [B_S2] 

           ADDD    .D1     A5,0x140,D1       ; [A_D1] 
||         MVKU32  .L2     0,BL3             ; [B_L2] 
||         MVKU32  .S2     0x3f800000,BM1    ; [B_S2] 

           MVKU32  .L2     0x7f7fffff,BL2    ; [B_L2] 
||         MVKU32  .S2     0x3f317218,BM5    ; [B_S2] 

           MVKU32  .L2     0x40000000,BM0    ; [B_L2] 
||         MVKU32  .S2     0x3fb8aa3b,B2     ; [B_S2] 

	.dwpsn	file "./inc/tidl_detectionOutput_int.h",line 134,column 25,is_stmt,isa 0

           ADDD    .D2     A5,0x244,D5       ; [A_D2] 
||         MVKU32  .L2     0x46fffe00,BM2    ; [B_L2] 
||         MVKU32  .L1     0xc,AM1           ; [A_L1] |134| 
||         MVKU32  .S2     0,BL1             ; [B_S2] |229| 
||         MASKB   .P2     0x4,P1            ; [B_P] |229| 

;** --------------------------------------------------------------------------*
;**   BEGIN LOOP ||$C$L70||
;** --------------------------------------------------------------------------*
||$C$L70||:    
;          EXCLUSIVE CPU CYCLES: 15
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 386,column 31,is_stmt,isa 0
           LDD     .D1     *A5(1192),D0      ; [A_D1] |386| 
           ADDD    .D1     D2,D0,D0          ; [A_D1] |386| 
           LDW     .D1     *D0(0),A0         ; [A_D1] |386| 
           CMPGTW  .L1     A0,0,A2           ; [A_L1] |386| 
   [!A2]   B       .B1     ||$C$L72||        ; [A_B] |386| 
           ; BRANCHCC OCCURS {||$C$L72||}    ; [] |386| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 1
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 386,column 26,is_stmt,isa 0
           MVKU32  .L1     0,D4              ; [A_L1] |386| 
;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*
;*      Loop found in file               : src/tidl_detectionOutput_score.c
;*      Loop source line                 : 386
;*      Loop opening brace source line   : 387
;*      Loop closing brace source line   : 406
;*      Known Minimum Trip Count         : 1                    
;*      Known Max Trip Count Factor      : 1
;*      Loop Carried Dependency Bound(^) : 130
;*      Unpartitioned Resource Bound     : 7
;*      Partitioned Resource Bound       : 9 (pre-sched)
;*      Disqualified loop: Loop carried dependency bound too large
;*----------------------------------------------------------------------------*
||$C$L71||:    
;          EXCLUSIVE CPU CYCLES: 130
	.dwpsn	file "./inc/tidl_detectionOutput_int.h",line 134,column 25,is_stmt,isa 0
           LDD     .D1     *A5(1200),D0      ; [A_D1] |134| 
           ADDD    .D1     D2,D0,D0          ; [A_D1] |134| 
           LDW     .D1     *D0(0),D0         ; [A_D1] |134| 
           LDD     .D1     *A5(1184),D6      ; [A_D1] |134| 
           ADDW    .D1     D4,D0,D0          ; [A_D1] |134| 
           LDW     .D1     *D6[D0],B0        ; [A_D1] |134| 
           SHRW    .L2     B0,0x1c,BL5       ; [B_L2] |134| 
           ANDW    .L2     BL5,0xf,B1        ; [B_L2] |134| 

           EXT     .L1X    B1,0x20,0x20,A2   ; [A_L1] |134| 
||         LDW     .D1     *A6(12),B13       ; [A_D1] |134| 

           MPYDD   .N1     AM1,A2,D6         ; [A_N1] |134| 
           SHRW    .L2     B0,0x18,BL7       ; [B_L2] |134| 

           ADDD    .D1     D5,D6,D6          ; [A_D1] |134| 
||         ANDW    .L2     BL7,0xf,B3        ; [B_L2] |134| 

           LDW     .D1     *D6(0),AM0        ; [A_D1] |134| 
||         MPYWW   .N2     B13,B3,BL5        ; [B_N2] |134| 

           SHRW    .L1X    B0,0x10,D14       ; [A_L1] |134| 
           ANDW    .D1     D3,D14,A3         ; [A_D1] |134| 
           SHLD    .L1     A2,0x3,D13        ; [A_L1] |134| 

           ADDD    .D1     A5,D13,D6         ; [A_D1] |134| 
||         ADDW    .L2X    A3,BL5,B1         ; [B_L2] |134| 

           LDD     .D1     *D6(192),D8       ; [A_D1] |134| 
           MPYWW   .N1X    B1,AM0,AM0        ; [A_N1] |134| 
           ANDW    .L2     BL4,B0,B12        ; [B_L2] |134| 
           SHRW    .L1X    B0,0x1c,AL0       ; [A_L1] |134| 

           ADDW    .M1X    B12,AM0,D6        ; [A_M1] |134| 
||         ANDW    .L1     AL0,0xf,D7        ; [A_L1] |134| 

           LDUW    .D1     *D8[D6],B0        ; [A_D1] |134| 
||         LDUW    .D2     *D1[D7],B11       ; [A_D2] |134| 

           MPYSP   .N2     B11,B0,BL5        ; [B_N2] |134| 
           VXORW   .L2     BL5,0x80000000,B0 ; [B_L2] |134| 
           MPYSP   .N2     B2,B0,B1          ; [B_N2] |134| 
	.dwpsn	file "./inc/tidl_detectionOutput_int.h",line 229,column 1,is_stmt,isa 0
           VSPTRUNC .L2    B1,B0             ; [B_L2] |229| 
           VINTSP  .L2     B0,B3             ; [B_L2] |229| 
           SUBSP   .C2     B1,B3,B1          ; [B_C] |229| 
           MPYSP   .N2     BM5,B1,B3         ; [B_N2] |229| 
           MPYSP   .N2     B3,B3,B1          ; [B_N2] |229| 
           CMPGEW  .L1X    B0,0xfffffff0,A2  ; [A_L1] |229| 
   [ A2]   MPYSP   .N2     B3,B1,B5          ; [B_N2] |229| 
   [ A2]   MPYSP   .N2     BM3,B1,B4         ; [B_N2] |229| 
   [ A2]   ADDSP   .C2     BM1,B3,B3         ; [B_C] |229| 
   [ A2]   MPYSP   .N2     B1,B1,B1          ; [B_N2] |229| 
   [ A2]   MPYSP   .N2     BM4,B5,B5         ; [B_N2] |229| 
   [ A2]   ADDSP   .C2     B4,B3,B3          ; [B_C] |229| 
   [ A2]   MPYSP   .N2     BM6,B1,B1         ; [B_N2] |229| 

   [ A2]   ADDSP   .C2     B5,B3,B3          ; [B_C] |229| 
||         SUBRW   .S2     B0,0,BL5          ; [B_S2] |229| 
||         VCMPGTW .L2     B0,BL1,P0         ; [B_L2] |229| 

   [ A2]   SHRW    .L2     BL0,BL5,BL5       ; [B_L2] |229| 
|| [ A2]   AND     .P2     P0,P1,P0          ; [B_P] |229| 
||         SHLW    .S2     BL0,B0,BL6        ; [B_S2] |229| 

   [ A2]   VSEL    .L2     P0,BL6,BL5,BL5    ; [B_L2] |229| 

   [ A2]   VINTSP  .L2     BL5,B1            ; [B_L2] |229| 
|| [ A2]   ADDSP   .C2     B1,B3,B3          ; [B_C] |229| 

   [ A2]   MPYSP   .N2     B3,B1,B1          ; [B_N2] |229| 
   [ A2]   MPYSP   .N2     BM7,B1,BL5        ; [B_N2] |229| 

   [!A2]   MV      .L2     BL3,BL5           ; [B_L2] |229| 
||         CMPGTW  .L1X    B0,0xe,A3         ; [A_L1] |229| 

   [!A3]   MV      .L2     BL5,B0            ; [B_L2] |229| 
|| [ A3]   MV      .S2     BL2,B0            ; [B_S2] |229| 

           ADDSP   .C2     BM1,B0,B0         ; [B_C] |229| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 405,column 11,is_stmt,isa 0
           VRCPSP  .S2     B0,B1             ; [B_S2] |405| 
           MPYSP   .N2     B0,B1,B3          ; [B_N2] |405| 
           SUBSP   .C2     BM0,B3,B3         ; [B_C] |405| 
           MPYSP   .N2     B1,B3,B1          ; [B_N2] |405| 
           MPYSP   .N2     B0,B1,B0          ; [B_N2] |405| 
           SUBSP   .C2     BM0,B0,B0         ; [B_C] |405| 
           MPYSP   .N2     B1,B0,B0          ; [B_N2] |405| 
           MPYSP   .N2     BM2,B0,BL6        ; [B_N2] |405| 
           LDD     .D1     *A5(1152),D12     ; [A_D1] |405| 
           VSPTRUNC .L2    BL6,B0            ; [B_L2] |405| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 386,column 31,is_stmt,isa 0
           ADDW    .D1     A0,0xffffffff,A0  ; [A_D1] |386| 

   [ A0]   B       .B1     ||$C$L71||        ; [A_B] |386| 
||         STH     .D1X    B0,*D12[D0]       ; [A_D1] |405| 
||         ADDW    .D2     D4,0x1,D4         ; [A_D2] |386| 

           ; BRANCHCC OCCURS {||$C$L71||}    ; [] |386| 
;** --------------------------------------------------------------------------*
||$C$L72||:    
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 381,column 29,is_stmt,isa 0
           ADDW    .D1     A1,0xffffffff,A1  ; [A_D1] |381| 

   [ A1]   B       .B1     ||$C$L70||        ; [A_B] |381| 
||         ADDD    .D1     D2,0x4,D2         ; [A_D1] |381| 

           ; BRANCHCC OCCURS {||$C$L70||}    ; [] |381| 
;** --------------------------------------------------------------------------*
||$C$L73||:    
;          EXCLUSIVE CPU CYCLES: 7
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 408,column 7,is_stmt,isa 0
           LDD     .D1     *A5(1152),B0      ; [A_D1] |408| 

           B       .B1     ||$C$L101||       ; [A_B] |580| 
||         STD     .D1X    B0,*A5(1160)      ; [A_D1] |408| 
||         MVKU32  .L1     0,A9              ; [A_L1] |580| 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 580,column 3,is_stmt,isa 0
           ; BRANCH OCCURS {||$C$L101||}     ; [] |580| 
;** --------------------------------------------------------------------------*
||$C$L74||:    
;          EXCLUSIVE CPU CYCLES: 17
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 473,column 27,is_stmt,isa 0
           LDD     .D1     *A5(1192),D0      ; [A_D1] |473| 
           LDW     .D1     *D0(0),B0         ; [A_D1] |473| 
           CMPGTW  .L2     B0,0,BL0          ; [B_L2] |473| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 471,column 5,is_stmt,isa 0

           XORD    .L2     BL0,0x1,B1        ; [B_L2] |473| 
||         MVKU32  .L1     0,A0              ; [A_L1] |471| 

           MVK32   .L1     0xff00ffff,D8     ; [A_L1] 
||         MVKU32  .L2     0,B4              ; [B_L2] 

           CMPEQW  .L1X    B1,0,A0           ; [A_L1] |473| 
||         STW     .D1     A0,*D0(0)         ; [A_D1] |471| 
||         MV      .M2X    A0,B14            ; [B_M2] |473| 
||         MVKU32  .S1     0xffff,D9         ; [A_S1] 
||         MVKU32  .L2     0,B5              ; [B_L2] 
||         MVKU32  .S2     0x40000000,B6     ; [B_S2] 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 473,column 27,is_stmt,isa 0

   [!A0]   B       .B1     ||$C$L92||        ; [A_B] |473| 
||         MV      .S1X    B0,A3             ; [A_S1] 
||         MVKU32  .L2     0xff7fffff,BL6    ; [B_L2] 
||         MVKU32  .L1     0,A9              ; [A_L1] 
||         MVKU32  .S2     0,B2              ; [B_S2] 

           ; BRANCHCC OCCURS {||$C$L92||}    ; [] |473| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 6
           MVKU32  .L2     0x3e2aaaad,B10    ; [B_L2] 

           MVKU32  .L2     0x10000,B8        ; [B_L2] 
||         MVKU32  .S2     0x3d2aaab4,B12    ; [B_S2] 

           MVKU32  .L2     0x3f000000,B9     ; [B_L2] 
||         MVKU32  .S2     0x3f317218,B11    ; [B_S2] 

	.dwpsn	file "./inc/tidl_detectionOutput_int.h",line 157,column 3,is_stmt,isa 0

           MVKU32  .L2     0,B7              ; [B_L2] |157| 
||         MVKU32  .S2     0x37800000,B13    ; [B_S2] 

           MV      .M2     B10,BM4           ; [B_M2] 
||         MVKU32  .L2     0x3f800000,B2     ; [B_L2] 
||         MVKU32  .S2     0x3fb8aa3b,B3     ; [B_S2] 

           MV      .L2     B9,BM3            ; [B_L2] 
||         MV      .S2     B8,BL2            ; [B_S2] 
||         MV      .M2     B12,BM6           ; [B_M2] 
||         MV      .C2     B11,BM5           ; [B_C] 
||         MASKB   .P2     0x4,P1            ; [B_P] |157| 
||         LDW     .D1     *A6(12),A8        ; [A_D1] 

;** --------------------------------------------------------------------------*
;**   BEGIN LOOP ||$C$L75||
;** --------------------------------------------------------------------------*
||$C$L75||:    
;          EXCLUSIVE CPU CYCLES: 15
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 475,column 7,is_stmt,isa 0
           LDD     .D1     *A5(1184),AM0     ; [A_D1] |475| 

           ADDD    .M1X    B5,AM0,D0         ; [A_M1] |475| 
||         LDW     .D1     *A5(1304),AL1     ; [A_D1] |484| 
||         LDW     .D2     *A6(76),AL0       ; [A_D2] |484| 

           LDW     .D1     *D0(0),A4         ; [A_D1] |475| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 484,column 7,is_stmt,isa 0

           CMPEQW  .L1     AL0,0,A2          ; [A_L1] |493| 
||         CMPEQW  .S1     AL1,0x6,A0        ; [A_S1] |484| 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 477,column 20,is_stmt,isa 0

           SHRW    .L1     A4,0x18,AL1       ; [A_L1] |478| 
||         SHRW    .S1     A4,0x1c,AM0       ; [A_S1] |477| 

   [!A2]   B       .B1     ||$C$L76||        ; [A_B] |493| 
||         ANDW    .L1     AL1,0xf,AM3       ; [A_L1] |478| 
||         ANDW    .M1     AM0,0xf,A10       ; [A_M1] |477| 
||         ANDW    .D1     D9,A4,A13         ; [A_D1] |479| 
|| [!A0]   VINTSP  .L2     BL5,B1            ; [B_L2] |490| 
|| [ A0]   MV      .S2     BL6,B1            ; [B_S2] |486| 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 493,column 7,is_stmt,isa 0
           ; BRANCHCC OCCURS {||$C$L76||}    ; [] |493| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 3
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 496,column 9,is_stmt,isa 0
           MV      .D1     A8,A12            ; [A_D1] |496| 
           CMPGTW  .L1     A12,0,A0          ; [A_L1] |496| 
   [!A0]   B       .B1     ||$C$L87||        ; [A_B] |496| 
           ; BRANCHCC OCCURS {||$C$L87||}    ; [] |496| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 1
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 495,column 9,is_stmt,isa 0

           B       .B1     ||$C$L77||        ; [A_B] 
||         MVKU32  .L1     0x1,A11           ; [A_L1] |495| 

           ; BRANCH OCCURS {||$C$L77||}      ; [] 
;** --------------------------------------------------------------------------*
||$C$L76||:    
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 500,column 9,is_stmt,isa 0

           CMPGTW  .L1     A8,0,A0           ; [A_L1] |505| 
||         ADDAW   .D1     A5,A10,D0         ; [A_D1] |500| 

   [!A0]   B       .B1     ||$C$L87||        ; [A_B] |505| 
||         LDW     .D1     *D0(1088),A11     ; [A_D1] |500| 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 505,column 20,is_stmt,isa 0
           ; BRANCHCC OCCURS {||$C$L87||}    ; [] |505| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 1
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 501,column 9,is_stmt,isa 0
           MVKU32  .L1     0x1,A12           ; [A_L1] |501| 
;** --------------------------------------------------------------------------*
||$C$L77||:    
;          EXCLUSIVE CPU CYCLES: 7

           EXT     .L1     A10,0x20,0x20,A0  ; [A_L1] 
||         MVKU32  .S1     0xc,AM0           ; [A_S1] 

           MPYDD   .N1     AM0,A0,D0         ; [A_N1] 
           MV      .D1     A8,A2             ; [A_D1] 

           CMPGTD  .L1     A2,0x2,A1         ; [A_L1] 
||         SHLD    .S1     A0,0x3,D1         ; [A_S1] 

           ADDD    .D1     A5,D0,D1          ; [A_D1] 
||         ADDD    .D2     A5,D1,D0          ; [A_D2] 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 505,column 12,is_stmt,isa 0

   [ A1]   B       .B1     ||$C$L79||        ; [A_B] 
||         LDD     .D1     *D0(192),A7       ; [A_D1] 
||         LDW     .D2     *D1(580),A14      ; [A_D2] 
||         MVKU32  .L1     0,A0              ; [A_L1] |505| 
||         MPYWW   .N1     A12,AM3,A15       ; [A_N1] 

           ; BRANCHCC OCCURS {||$C$L79||}    ; [] 
;** --------------------------------------------------------------------------*
;**   BEGIN LOOP ||$C$L78||
;** --------------------------------------------------------------------------*
||$C$L78||:    
;          EXCLUSIVE CPU CYCLES: 17
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 507,column 9,is_stmt,isa 0
           MPYWW   .N1     A11,A0,D0         ; [A_N1] |507| 
           ADDW    .D1     A15,D0,AM0        ; [A_D1] |507| 
           MPYWW   .N1     A14,AM0,D0        ; [A_N1] |507| 
           ADDW    .D1     A13,D0,D0         ; [A_D1] |507| 
           LDUW    .D1     *A7[D0],BM0       ; [A_D1] |507| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 505,column 20,is_stmt,isa 0
           ADDW    .D1     A2,0xffffffff,A2  ; [A_D1] |505| 

   [ A2]   B       .B1     ||$C$L78||        ; [A_B] |505| 
||         VMAXSP  .C2     B1,BM0,B1         ; [B_C] |507| 
||         ADDW    .D1     A0,0x1,A0         ; [A_D1] |505| 

           ; BRANCHCC OCCURS {||$C$L78||}    ; [] |505| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 1
           B       .B1     ||$C$L83||        ; [A_B] 
           ; BRANCH OCCURS {||$C$L83||}      ; [] 
;** --------------------------------------------------------------------------*
||$C$L79||:    
;          EXCLUSIVE CPU CYCLES: 4
           MV      .D1     A2,AL0            ; [A_D1] 

           EXT     .L1     AL0,0x20,0x20,AL0 ; [A_L1] 
||         MV      .D1     A11,AM0           ; [A_D1] 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 507,column 9,is_stmt,isa 0

           NLCINIT .S1     AL0,0x1,9         ; [A_S1] 
||         MPYWW   .N1     AM0,A0,D0         ; [A_N1] |507| <0,1> 
||         UNPROT          0x1               ; [A_U] 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 505,column 20,is_stmt,isa 0

           TICK                               ; [A_U] <0,0> 
||         ADDW    .D1     A0,0x1,A0         ; [A_D1] |505| <0,2> 

;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*
;*      Loop found in file               : src/tidl_detectionOutput_score.c
;*      Loop source line                 : 505
;*      Loop opening brace source line   : 506
;*      Loop closing brace source line   : 509
;*      Known Minimum Trip Count         : 1                    
;*      Known Max Trip Count Factor      : 1
;*      Loop Carried Dependency Bound(^) : 2
;*      Unpartitioned Resource Bound     : 1
;*      Partitioned Resource Bound       : 1 (pre-sched)
;*
;*      Searching for software pipeline schedule at ...
;*         ii = 2  Schedule found with 10 iterations in parallel
;*
;*      Partitioned Resource Bound(*)    : 2 (post-sched)
;*
;*      Constant Extension #0 Used [C0]  : 0
;*      Constant Extension #1 Used [C1]  : 0
;*
;*      Resource Partition (may include "post-sched" split/spill moves):
;*
;*                                                A-side   B-side
;*      .L units                                     0        0     
;*      .S units                                     0        0     
;*      .M units                                     0        0     
;*      .N units                                     0        0     
;*      .D units                                     1        -     
;*      .B units                                     1        -     
;*      .C units                                     -        0     
;*      .P units                                     -        0     
;*
;*      .M/.N units                                  2        0     
;*      .L/.S units                                  0        0     
;*      .L/.S/.C units                               0        1     
;*      .L/.S/.C/.M units                            0        0     
;*      .L/.S/.C/.M/.D units                         3        0     
;*
;*      .X cross paths                               0        0     
;*
;*      Bound(.D1 .D2 .D)                            1        -     
;*      Bound(.M .N .MN)                             1        0     
;*      Bound(.L .S .LS)                             0        0     
;*      Bound(.L .S .C .LS .LSC)                     0        1     
;*      Bound(.L .S .C .M .LS .LSC .LSCM)            0        1     
;*      Bound(.L .S .C .M .D .LS .LSC .LSCM .LSCMD)  1        1     
;*
;*      Register Usage Tables:
;*
;*      +----------------------------------+----------------------------------+
;*      |              ASIDE               |              BSIDE               |
;*      +----------------+--------+--------+----------------+--------+--------+
;*      |      "Axx"     |  "ALx" |  "AMx" |     "VBxx"     | "VBLx" | "VBMx" |
;*      +----------------|--------|--------|----------------|--------|--------+
;*      |0000000000111111|00000000|00000000|0000000000111111|00000000|00000000|
;*      |0123456789012345|01234567|01234567|0123456789012345|01234567|01234567|
;*      +----------------|--------|--------|----------------|--------|--------+
;*   0: |***             |        |***     |                |        |        |
;*   1: |***             |        |**      |                |        |**      |
;*      +----------------+--------+--------+----------------+--------+--------+
;*
;*      +----------------+                 +--------+       +-------+
;*      |      "Dxx"     |                 |  "Px"  |       |"CUCRx"|
;*      +----------------+                 +--------+       +-------+
;*      |0000000000111111|                 |00000000|       |0000   |
;*      |0123456789012345|                 |01234567|       |0123   |
;*      +----------------+                 +--------+       +-------+
;*   0: |****            |                 |        |       |       |
;*   1: |*****           |                 |        |       |       |
;*      +----------------+                 +--------+       +-------+
;*
;*      Done
;*
;*      Redundant loop generated
;*      Collapsed epilog stages       : 9
;*      Prolog not entirely removed
;*      Collapsed prolog stages       : 1
;*
;*      Max amt of load speculation   : 0 bytes
;*
;*      Minimum safe trip count       : 3
;*
;*
;*      Mem bank conflicts/iter(est.) : { min 0.000, est 0.000, max 0.000 }
;*      Mem bank perf. penalty (est.) : 0.0%
;*
;*
;*      Total cycles (est.)         : 18 + trip_cnt * 2        
;*----------------------------------------------------------------------------*
;*        SINGLE SCHEDULED ITERATION
;*
;*        ||$C$C1299||:
;*   0              TICK                               ; [A_U] 
;*   1              MPYWW   .N1     AM0,A0,D4         ; [A_N1] |507| 
;*   2              ADDW    .D2     A0,0x1,A0         ; [A_D2] |505| 
;*   3              NOP     0x2     ; [A_B] 
;*   5              ADDW    .D2     D3,D4,AM2         ; [A_D2] |507| 
;*   6              MPYWW   .N1     AM1,AM2,D0        ; [A_N1] |507| 
;*   7              NOP     0x3     ; [A_B] 
;*  10              ADDW    .D1     D2,D0,D0          ; [A_D1] |507| 
;*  11              LDUW    .D1     *D1[D0],BM0       ; [A_D1] |507| 
;*  12              NOP     0x5     ; [A_B] 
;*  17              VMAXSP  .C2     BM1,BM0,BM1       ; [B_C] |507|  ^ 
;*  18              NOP     0x1     ; [A_B] 
;*  19              BNL     .B1     ||$C$C1299||      ; [A_B] |505| 
;*  20              ; BRANCHCC OCCURS {||$C$C1299||}  ; [] |505| 
;*----------------------------------------------------------------------------*
||$C$L80||:    ; PIPED LOOP PROLOG
;          EXCLUSIVE CPU CYCLES: 13
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 507,column 9,is_stmt,isa 0

           TICK                               ; [A_U] <1,0> 
||         MPYWW   .N1     AM0,A0,D0         ; [A_N1] |507| <1,1> 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 505,column 20,is_stmt,isa 0

           MV      .D1     A15,D3            ; [A_D1] 
||         ADDW    .D2     A0,0x1,A0         ; [A_D2] |505| <1,2> 
||         TICK                               ; [A_U] <2,0> 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 507,column 9,is_stmt,isa 0

           MV      .D1     A14,AM1           ; [A_D1] 
||         ADDW    .D2     D3,D0,AM2         ; [A_D2] |507| <0,5> 
||         MPYWW   .N1     AM0,A0,D0         ; [A_N1] |507| <2,1> 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 505,column 20,is_stmt,isa 0

           MPYWW   .N1     AM1,AM2,D0        ; [A_N1] |507| <0,6> 
||         ADDW    .D2     A0,0x1,A0         ; [A_D2] |505| <2,2> 
||         TICK                               ; [A_U] <3,0> 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 507,column 9,is_stmt,isa 0

           ADDW    .D2     D3,D0,AM2         ; [A_D2] |507| <1,5> 
||         MPYWW   .N1     AM0,A0,D6         ; [A_N1] |507| <3,1> 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 505,column 20,is_stmt,isa 0

           MPYWW   .N1     AM1,AM2,D0        ; [A_N1] |507| <1,6> 
||         ADDW    .D2     A0,0x1,A0         ; [A_D2] |505| <3,2> 
||         TICK                               ; [A_U] <4,0> 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 507,column 9,is_stmt,isa 0

           MV      .D1     A13,D2            ; [A_D1] 
||         ADDW    .D2     D3,D0,AM2         ; [A_D2] |507| <2,5> 
||         MPYWW   .N1     AM0,A0,D6         ; [A_N1] |507| <4,1> 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 505,column 20,is_stmt,isa 0

           MV      .L1     A7,D1             ; [A_L1] 
||         ADDW    .D1     D2,D0,D0          ; [A_D1] |507| <0,10> 
||         MPYWW   .N1     AM1,AM2,D0        ; [A_N1] |507| <2,6> 
||         ADDW    .D2     A0,0x1,A0         ; [A_D2] |505| <4,2> 
||         TICK                               ; [A_U] <5,0> 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 507,column 9,is_stmt,isa 0

           LDUW    .D1     *D1[D0],BM0       ; [A_D1] |507| <0,11> 
||         ADDW    .D2     D3,D6,AM2         ; [A_D2] |507| <3,5> 
||         MPYWW   .N1     AM0,A0,D6         ; [A_N1] |507| <5,1> 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 505,column 20,is_stmt,isa 0

           ADDW    .D1     D2,D0,D0          ; [A_D1] |507| <1,10> 
||         MPYWW   .N1     AM1,AM2,D0        ; [A_N1] |507| <3,6> 
||         ADDW    .D2     A0,0x1,A0         ; [A_D2] |505| <5,2> 
||         TICK                               ; [A_U] <6,0> 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 507,column 9,is_stmt,isa 0

           LDUW    .D1     *D1[D0],BM0       ; [A_D1] |507| <1,11> 
||         ADDW    .D2     D3,D6,AM2         ; [A_D2] |507| <4,5> 
||         MPYWW   .N1     AM0,A0,D4         ; [A_N1] |507| <6,1> 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 505,column 20,is_stmt,isa 0

           MV      .L1     A2,AL0            ; [A_L1] 
||         ADDW    .D1     D2,D0,D0          ; [A_D1] |507| <2,10> 
||         MPYWW   .N1     AM1,AM2,D0        ; [A_N1] |507| <4,6> 
||         ADDW    .D2     A0,0x1,A0         ; [A_D2] |505| <6,2> 
||         TICK                               ; [A_U] <7,0> 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 507,column 9,is_stmt,isa 0

           MV      .L2     B1,BM1            ; [B_L2] 
||         ADDD    .L1     AL0,0xfffffffd,A1 ; [A_L1] init epilog collapse predicate
||         LDUW    .D1     *D1[D0],BM0       ; [A_D1] |507| <2,11> 
||         ADDW    .D2     D3,D6,AM2         ; [A_D2] |507| <5,5> 
||         MPYWW   .N1     AM0,A0,D4         ; [A_N1] |507| <7,1> 

;** --------------------------------------------------------------------------*
||$C$L81||:    ; PIPED LOOP KERNEL
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 505,column 20,is_stmt,isa 0

           ADDW    .D1     D2,D0,D0          ; [A_D1] |507| <4,10> 
||         MPYWW   .N1     AM1,AM2,D0        ; [A_N1] |507| <6,6> 
||         ADDW    .D2     A0,0x1,A0         ; [A_D2] |505| <8,2> 
||         TICK                               ; [A_U] <9,0> 

   [ A2]   ADDW    .S1     A2,0xffffffff,A2  ; [A_S1] |505| <0,19> collapsing predicate control
|| [ A1]   ADDW    .L1     A1,0xffffffff,A1  ; [A_L1] |505| <0,19> collapsing predicate control
||         BNL     .B1     ||$C$L81||        ; [A_B] |505| <0,19> 
|| [ A2]   VMAXSP  .C2     BM1,BM0,BM1       ; [B_C] |507| <1,17>  ^ 
|| [ A1]   LDUW    .D1     *D1[D0],BM0       ; [A_D1] |507| <4,11> 
||         ADDW    .D2     D3,D4,AM2         ; [A_D2] |507| <7,5> 
||         MPYWW   .N1     AM0,A0,D4         ; [A_N1] |507| <9,1> 

;** --------------------------------------------------------------------------*
||$C$L82||:    ; PIPED LOOP EPILOG
;          EXCLUSIVE CPU CYCLES: 2

           MV      .D1     D2,A13            ; [A_D1] 
||         MV      .M1     AM0,A11           ; [A_M1] 

           PROTCLR                            ; [A_U] VBM1
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 1
           MV      .M2     BM1,B1            ; [B_M2] 
;** --------------------------------------------------------------------------*
||$C$L83||:    
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 511,column 20,is_stmt,isa 0
           CMPGTW  .L1     A8,0,A0           ; [A_L1] |511| 
   [!A0]   B       .B1     ||$C$L87||        ; [A_B] |511| 
           ; BRANCHCC OCCURS {||$C$L87||}    ; [] |511| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 17

           EXT     .L1     A10,0x20,0x20,A0  ; [A_L1] 
||         MVKU32  .S1     0xc,AM0           ; [A_S1] 

           MPYDD   .N1     AM0,A0,D0         ; [A_N1] 
           SHLD    .L1     A0,0x3,D1         ; [A_L1] 

           ADDD    .D1     A5,D0,D1          ; [A_D1] 
||         ADDD    .D2     A5,D1,D0          ; [A_D2] 

           ADDD    .D1     D1,0x244,D1       ; [A_D1] 
           MV      .D1     D1,D3             ; [A_D1] 
	.dwpsn	file "./inc/tidl_detectionOutput_int.h",line 135,column 25,is_stmt,isa 0

           SLDW    .D1     *D3(0),AM0        ; [A_D1] |135| <0,0>  ^ 
||         MPYWW   .N1     A12,AM3,A2        ; [A_N1] 
||         MV      .D2     A11,AM1           ; [A_D2] 
||         MVKU32  .L1     0,A8              ; [A_L1] |511| 

           MPYWW   .N1     AM1,A8,D14        ; [A_N1] |135| <0,1> 
           ADDD    .D1     D0,0xc0,A1        ; [A_D1] 

           MV      .D1     A1,D4             ; [A_D1] 
||         MV      .D2     A2,D6             ; [A_D2] 

           SLDD    .D1     *D4(0),D7         ; [A_D1] |135| <0,5> 
||         ADDW    .D2     D6,D14,AM2        ; [A_D2] |135| <0,5> 

           MPYWW   .N1     AM2,AM0,D0        ; [A_N1] |135| <0,6>  ^ 

           MV      .L2     B7,BL0            ; [B_L2] 
||         MV      .S2     B4,B0             ; [B_S2] 

           MV      .D1     A6,D1             ; [A_D1] 
||         MV      .L2     B0,BM1            ; [B_L2] 
||         MV      .S2     B4,BL1            ; [B_S2] 
||         MV      .M2     B1,BM2            ; [B_M2] 
||         MV      .C2     B13,BM7           ; [B_C] 
||         MV      .D2     A5,D2             ; [A_D2] 
||         MV      .L1     A13,D5            ; [A_L1] 
||         UNPROT          0x1               ; [A_U] 

;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*
;*      Loop found in file               : src/tidl_detectionOutput_score.c
;*      Loop source line                 : 511
;*      Loop opening brace source line   : 512
;*      Loop closing brace source line   : 528
;*      Known Minimum Trip Count         : 1                    
;*      Known Max Trip Count Factor      : 1
;*      Loop Carried Dependency Bound(^) : 66
;*      Unpartitioned Resource Bound     : 4
;*      Partitioned Resource Bound       : 6 (pre-sched)
;*
;*      Searching for software pipeline schedule at ...
;*         ii = 66 Schedule found with 2 iterations in parallel
;*
;*      Partitioned Resource Bound(*)    : 6 (post-sched)
;*
;*      Constant Extension #0 Used [C0]  : 0
;*      Constant Extension #1 Used [C1]  : 2
;*
;*      Resource Partition (may include "post-sched" split/spill moves):
;*
;*                                                A-side   B-side
;*      .L units                                     0        0     
;*      .S units                                     0        0     
;*      .M units                                     0        0     
;*      .N units                                     0        0     
;*      .D units                                     6        -     
;*      .B units                                     1        -     
;*      .C units                                     -        0     
;*      .P units                                     -        1     
;*
;*      .M/.N units                                  2       10     
;*      .L/.S units                                  3        5     
;*      .L/.S/.C units                               0        9     
;*      .L/.S/.C/.M units                            0        1     
;*      .L/.S/.C/.M/.D units                         3        2     
;*
;*      .X cross paths                               4        0     
;*
;*      Bound(.D1 .D2 .D)                            3        -     
;*      Bound(.M .N .MN)                             1        5     
;*      Bound(.L .S .LS)                             2        3     
;*      Bound(.L .S .C .LS .LSC)                     2        5     
;*      Bound(.L .S .C .M .LS .LSC .LSCM)            1        4     
;*      Bound(.L .S .C .M .D .LS .LSC .LSCM .LSCMD)  3        5     
;*
;*      Register Usage Tables:
;*
;*      +----------------------------------+----------------------------------+
;*      |              ASIDE               |              BSIDE               |
;*      +----------------+--------+--------+----------------+--------+--------+
;*      |      "Axx"     |  "ALx" |  "AMx" |     "VBxx"     | "VBLx" | "VBMx" |
;*      +----------------|--------|--------|----------------|--------|--------+
;*      |0000000000111111|00000000|00000000|0000000000111111|00000000|00000000|
;*      |0123456789012345|01234567|01234567|0123456789012345|01234567|01234567|
;*      +----------------|--------|--------|----------------|--------|--------+
;*   0: |        *       |        | *      |  **            |***     | *******|
;*   1: |        *       |        | *      |  **            |***     | *******|
;*   2: |        *       |        | *      |  **            |***     | *******|
;*   3: |        *       |        | *      |  **            |***     | *******|
;*   4: |        *       |        | *      |  **            |***     | *******|
;*   5: |        *       |        | *      |  **            |***     | *******|
;*   6: |        *       |        | *      |  **            |***     | *******|
;*   7: |        *       |        | *      |  **            |***     | *******|
;*   8: |        *       |        | *      |  **            |***     |********|
;*   9: |        *       |        | *      |  **            |***     | *******|
;*  10: |        *       |        | *      |  **            |***     | *******|
;*  11: |        *       |        | *      |  **            |***     |********|
;*  12: |        *       |        | *      |  **            |***     | *******|
;*  13: |        *       |        | *      |  **            |***     | *******|
;*  14: |        *       |        | *      |  **            |***     | *******|
;*  15: |        *       |        | *      |* **            |***     | *******|
;*  16: |        *       |        | *      |* **            |***     | *******|
;*  17: |        *       |        | *      |* **            |***     | *******|
;*  18: |        *       |        | *      |****            |***     | *******|
;*  19: |        *       |        | *      |****            |****    | *******|
;*  20: | *      *       |        | *      |****            |*****   | *******|
;*  21: |**      *       |        | *      |* **            |****    |********|
;*  22: |**      *       |        | *      |  **            |***     | *******|
;*  23: |**      *       |        | *      |  **            |***     | *******|
;*  24: |**      *       |        | *      |* **            |***     |********|
;*  25: |**      *       |        | *      |  **            |***     |********|
;*  26: |**      *       |        | *      |  **            |***     |********|
;*  27: |**      *       |        | *      |  **            |***     |********|
;*  28: |**      *       |        | *      |  ***           |***     |********|
;*  29: |**      *       |        | *      |  ***           |***     |********|
;*  30: |**      *       |        | *      |  ***           |***     |********|
;*  31: |**      *       |        | *      |* ***           |***     |********|
;*  32: |**      *       |        | *      |*****           |***     |********|
;*  33: |**      *       |        | *      |****            |***     |********|
;*  34: |**      *       |        | *      |* **            |***     |********|
;*  35: |**      *       |        | *      |* **            |***     |********|
;*  36: |**      *       |        | *      |*****           |***     |********|
;*  37: |**      *       |        | *      |* **            |***     |********|
;*  38: |**      *       |        | *      |  **            |***     |********|
;*  39: |**      *       |        | *      |* **            |***     |********|
;*  40: |**      *       |        | *      |****            |***     |********|
;*  41: |**      *       |        | *      | ***            |***     |********|
;*  42: |**      *       |        | *      | ***            |***     |********|
;*  43: |**      *       |        | *      |****            |***     |********|
;*  44: |**      *       |        | *      |  **            |***     |********|
;*  45: |**      *       |        | *      |  **            |***     |********|
;*  46: |**      *       |        | *      |* **            |***     |********|
;*  47: |**      *       |        | *      |  **            |***     | *******|
;*  48: |**      *       |        | *      |  **            |***     | *******|
;*  49: |**      *       |        | *      |  **            |***     | *******|
;*  50: |**      *       |        | *      |  **            |***     |********|
;*  51: |**      *       |        | *      |  **            |***     | *******|
;*  52: |**      *       |        | *      |  **            |***     | *******|
;*  53: |**      *       |        | *      |  **            |***     | *******|
;*  54: |**      *       |        | *      |* **            |***     | *******|
;*  55: |*       *       |        | *      |* **            |***     | *******|
;*  56: |        *       |        | *      |* **            |***     | *******|
;*  57: |        *       |        | *      |  **            |***     |  ******|
;*  58: |        *       |        | *      |  **            |***     |  ******|
;*  59: |        *       |        | *      |  **            |***     | *******|
;*  60: |        *       |        | *      |  **            |***     | *******|
;*  61: |        *       |        | *      |  **            |***     | *******|
;*  62: |        *       |        | *      |  **            |***     | *******|
;*  63: |        *       |        |***     |* **            |***     | *******|
;*  64: |        *       |        | *      |* **            |***     | *******|
;*  65: |*       *       |        | *      |  **            |***     | *******|
;*      +----------------+--------+--------+----------------+--------+--------+
;*
;*      +----------------+                 +--------+       +-------+
;*      |      "Dxx"     |                 |  "Px"  |       |"CUCRx"|
;*      +----------------+                 +--------+       +-------+
;*      |0000000000111111|                 |00000000|       |0000   |
;*      |0123456789012345|                 |01234567|       |0123   |
;*      +----------------+                 +--------+       +-------+
;*   0: | ******         |                 | *      |       |       |
;*   1: |*******         |                 | *      |       |       |
;*   2: |********        |                 | *      |       |       |
;*   3: | ******         |                 | *      |       |       |
;*   4: | ******         |                 | *      |       |       |
;*   5: | ******         |                 | *      |       |       |
;*   6: | ******         |                 | *      |       |       |
;*   7: | ******         |                 | *      |       |       |
;*   8: | ******         |                 | *      |       |       |
;*   9: | ******         |                 | *      |       |       |
;*  10: | ******         |                 | *      |       |       |
;*  11: | ******         |                 | *      |       |       |
;*  12: | ******         |                 | *      |       |       |
;*  13: | ******         |                 | *      |       |       |
;*  14: | ******         |                 | *      |       |       |
;*  15: | ******         |                 | *      |       |       |
;*  16: | ******         |                 | *      |       |       |
;*  17: | ******         |                 | *      |       |       |
;*  18: | ******         |                 | *      |       |       |
;*  19: | ******         |                 |**      |       |       |
;*  20: | ******         |                 |**      |       |       |
;*  21: | ******         |                 | *      |       |       |
;*  22: | ******         |                 | *      |       |       |
;*  23: | ******         |                 | *      |       |       |
;*  24: | ******         |                 | *      |       |       |
;*  25: | ******         |                 | *      |       |       |
;*  26: | ******         |                 | *      |       |       |
;*  27: | ******         |                 | *      |       |       |
;*  28: | ******         |                 | *      |       |       |
;*  29: | ******         |                 | *      |       |       |
;*  30: | ******         |                 | *      |       |       |
;*  31: | ******         |                 | *      |       |       |
;*  32: | ******         |                 | *      |       |       |
;*  33: | ******         |                 | *      |       |       |
;*  34: | ******         |                 | *      |       |       |
;*  35: | ******         |                 | *      |       |       |
;*  36: | ******         |                 | *      |       |       |
;*  37: | ******         |                 | *      |       |       |
;*  38: | ******         |                 | *      |       |       |
;*  39: | ******         |                 | *      |       |       |
;*  40: | ******         |                 | *      |       |       |
;*  41: | ******         |                 | *      |       |       |
;*  42: | ******         |                 | *      |       |       |
;*  43: | ******         |                 | *      |       |       |
;*  44: | ******         |                 | *      |       |       |
;*  45: | ******         |                 | *      |       |       |
;*  46: | ******         |                 | *      |       |       |
;*  47: | ******         |                 | *      |       |       |
;*  48: | ******         |                 | *      |       |       |
;*  49: | ******         |                 | *      |       |       |
;*  50: | ******         |                 | *      |       |       |
;*  51: | ******         |                 | *      |       |       |
;*  52: | ******         |                 | *      |       |       |
;*  53: | ******         |                 | *      |       |       |
;*  54: | ******         |                 | *      |       |       |
;*  55: | ******         |                 | *      |       |       |
;*  56: |*******         |                 | *      |       |       |
;*  57: | ******         |                 | *      |       |       |
;*  58: | ******         |                 | *      |       |       |
;*  59: | ******         |                 | *      |       |       |
;*  60: | ******         |                 | *      |       |       |
;*  61: | ******         |                 | *      |       |       |
;*  62: |*******         |                 | *      |       |       |
;*  63: | ******         |                 | *      |       |       |
;*  64: | ******         |                 | *      |       |       |
;*  65: | ******         |                 | *      |       |       |
;*      +----------------+                 +--------+       +-------+
;*
;*      Done
;*
;*      Collapsed epilog stages       : 1
;*      Prolog not removed
;*      Collapsed prolog stages       : 0
;*
;*      Max amt of load speculation   : 0 bytes
;*
;*      Minimum safe trip count       : 1
;*      Min. prof. trip count  (est.) : 2
;*
;*      Mem bank conflicts/iter(est.) : { min 0.000, est 0.000, max 0.000 }
;*      Mem bank perf. penalty (est.) : 0.0%
;*
;*
;*      Total cycles (est.)         : 9 + trip_cnt * 66        
;*----------------------------------------------------------------------------*
;*        SINGLE SCHEDULED ITERATION
;*
;*        ||$C$C1209||:
;*   0              SLDW    .D2     *D3(0),AM0        ; [A_D2] |135|  ^ 
;*   1              MPYWW   .N1     AM1,A8,D0         ; [A_N1] |135| 
;*   2              NOP     0x3     ; [A_B] 
;*   5              ADDW    .D2     D6,D0,AM2         ; [A_D2] |135| 
;*     ||           SLDD    .D1     *D4(0),D7         ; [A_D1] |135| 
;*   6              MPYWW   .N1     AM2,AM0,D0        ; [A_N1] |135|  ^ 
;*   7              NOP     0x3     ; [A_B] 
;*  10              ADDW    .D1     D5,D0,D0          ; [A_D1] |135|  ^ 
;*  11              LDUW    .D1     *D7[D0],BM0       ; [A_D1] |135|  ^ 
;*  12              NOP     0x5     ; [A_B] 
;*  17              SUBSP   .C2     BM0,BM2,BM0       ; [B_C] |135|  ^ 
;*  18              NOP     0x2     ; [A_B] 
;*  20              MPYSP   .N2     B3,BM0,B0         ; [B_N2] |135|  ^ 
;*  21              NOP     0x3     ; [A_B] 
;*  24              VSPTRUNC .L2    B0,B1             ; [B_L2] |135|  ^ 
;*  25              NOP     0x2     ; [A_B] 
;*  27              VINTSP  .S2     B1,BM0            ; [B_S2] |157|  ^ 
;*     ||           VCMPGTW .L2     B1,BL0,P0         ; [B_L2] |157| 
;*     ||           SUBRW   .M2     B1,0,BL3          ; [B_M2] |157| 
;*  28              SHLW    .L2     BL2,B1,BL4        ; [B_L2] |157| 
;*     ||           AND     .P2     P0,P1,P0          ; [B_P] |157| 
;*     ||           SHRW    .S2     BL2,BL3,BL3       ; [B_S2] |157| 
;*     ||           CMPGEW  .L1X    B1,0xfffffff0,A1  ; [A_L1] |161| 
;*  29              VSEL    .L2     P0,BL4,BL3,BL3    ; [B_L2] |157| 
;*     ||           CMPGTW  .L1X    B1,0xe,A0         ; [A_L1] |164| 
;*  30              SUBSP   .C2     B0,BM0,B0         ; [B_C] |157|  ^ 
;*     ||           VINTSP  .L2     BL3,BM0           ; [B_L2] |157| 
;*  31              NOP     0x2     ; [A_B] 
;*  33              MPYSP   .N2     BM5,B0,B4         ; [B_N2] |157|  ^ 
;*  34              NOP     0x3     ; [A_B] 
;*  37              MPYSP   .N2     B4,B4,B1          ; [B_N2] |157|  ^ 
;*     ||           ADDSP   .C2     B2,B4,B0          ; [B_C] |157| 
;*  38              NOP     0x3     ; [A_B] 
;*  41              MPYSP   .M2     BM3,B1,B1         ; [B_M2] |157| 
;*     ||           MPYSP   .N2     B4,B1,B4          ; [B_N2] |157|  ^ 
;*  42              MPYSP   .N2     B1,B1,B0          ; [B_N2] |157| 
;*  43              NOP     0x2     ; [A_B] 
;*  45              ADDSP   .C2     B1,B0,B0          ; [B_C] |157| 
;*     ||           MPYSP   .N2     BM4,B4,B1         ; [B_N2] |157|  ^ 
;*  46              MPYSP   .N2     BM6,B0,B1         ; [B_N2] |157| 
;*  47              NOP     0x2     ; [A_B] 
;*  49              ADDSP   .C2     B1,B0,B0          ; [B_C] |157|  ^ 
;*  50              NOP     0x2     ; [A_B] 
;*  52              ADDSP   .C2     B1,B0,B0          ; [B_C] |157|  ^ 
;*  53              NOP     0x2     ; [A_B] 
;*  55              MPYSP   .N2     B0,BM0,BM0        ; [B_N2] |157|  ^ 
;*  56              NOP     0x3     ; [A_B] 
;*  59              MPYSP   .N2     BM7,BM0,B0        ; [B_N2] |157|  ^ 
;*     ||           LDD     .D1     *D2(1168),D0      ; [A_D1] [C1]
;*  60              NOP     0x3     ; [A_B] 
;*  63      [!A1]   MV      .L2     BL1,B0            ; [B_L2] |162|  ^ 
;*  64      [ A0]   MVKU32  .L2     0x7f7fffff,B0     ; [B_L2] |165|  ^ [C1]
;*  65              STW     .D1X    B0,*D0[A8]        ; [A_D1] |526|  ^ 
;*     ||           ADDSP   .C2     B0,BM1,BM1        ; [B_C] |527| 
;*  66              LDW     .D1     *D1(12),B0        ; [A_D1] |511| 
;*     ||           ADDW    .L1     A8,0x1,A8         ; [A_L1] |511| 
;*  67              NOP     0x6     ; [A_B] 
;*  73              CMPGTW  .L1X    B0,A8,A0          ; [A_L1] |511| 
;*  74      [ A0]   B       .B1     ||$C$C1209||      ; [A_B] |511| 
;*  75              ; BRANCHCC OCCURS {||$C$C1209||}  ; [] |511| 
;*----------------------------------------------------------------------------*
||$C$L84||:    ; PIPED LOOP PROLOG
;** --------------------------------------------------------------------------*
||$C$L85||:    ; PIPED LOOP KERNEL
;          EXCLUSIVE CPU CYCLES: 66
           NOP             0x1               ; [A_B] 
           ADDW    .D1     D5,D0,D0          ; [A_D1] |135| <0,10>  ^ 
           LDUW    .D1     *D7[D0],BM0       ; [A_D1] |135| <0,11>  ^ 
           NOP             0x5               ; [A_B] 
           SUBSP   .C2     BM0,BM2,BM0       ; [B_C] |135| <0,17>  ^ 
           NOP             0x2               ; [A_B] 
           MPYSP   .N2     B3,BM0,B0         ; [B_N2] |135| <0,20>  ^ 
           NOP             0x3               ; [A_B] 
           VSPTRUNC .L2    B0,B1             ; [B_L2] |135| <0,24>  ^ 
           NOP             0x2               ; [A_B] 
	.dwpsn	file "./inc/tidl_detectionOutput_int.h",line 157,column 3,is_stmt,isa 0

           VCMPGTW .L2     B1,BL0,P0         ; [B_L2] |157| <0,27> 
||         SUBRW   .M2     B1,0,BL3          ; [B_M2] |157| <0,27> 
||         VINTSP  .S2     B1,BM0            ; [B_S2] |157| <0,27>  ^ 

           CMPGEW  .L1X    B1,0xfffffff0,A1  ; [A_L1] |161| <0,28> 
||         SHLW    .L2     BL2,B1,BL4        ; [B_L2] |157| <0,28> 
||         SHRW    .S2     BL2,BL3,BL3       ; [B_S2] |157| <0,28> 
||         AND     .P2     P0,P1,P0          ; [B_P] |157| <0,28> 

           CMPGTW  .L1X    B1,0xe,A0         ; [A_L1] |164| <0,29> 
||         VSEL    .L2     P0,BL4,BL3,BL3    ; [B_L2] |157| <0,29> 

           VINTSP  .L2     BL3,BM0           ; [B_L2] |157| <0,30> 
||         SUBSP   .C2     B0,BM0,B0         ; [B_C] |157| <0,30>  ^ 

           NOP             0x2               ; [A_B] 
           MPYSP   .N2     BM5,B0,B4         ; [B_N2] |157| <0,33>  ^ 
           NOP             0x3               ; [A_B] 

           ADDSP   .C2     B2,B4,B0          ; [B_C] |157| <0,37> 
||         MPYSP   .N2     B4,B4,B1          ; [B_N2] |157| <0,37>  ^ 

           NOP             0x3               ; [A_B] 

           MPYSP   .M2     BM3,B1,B1         ; [B_M2] |157| <0,41> 
||         MPYSP   .N2     B4,B1,B4          ; [B_N2] |157| <0,41>  ^ 

           MPYSP   .N2     B1,B1,B0          ; [B_N2] |157| <0,42> 
           NOP             0x2               ; [A_B] 

           ADDSP   .C2     B1,B0,B0          ; [B_C] |157| <0,45> 
||         MPYSP   .N2     BM4,B4,B1         ; [B_N2] |157| <0,45>  ^ 

           MPYSP   .N2     BM6,B0,B1         ; [B_N2] |157| <0,46> 
           NOP             0x2               ; [A_B] 
           ADDSP   .C2     B1,B0,B0          ; [B_C] |157| <0,49>  ^ 
           NOP             0x2               ; [A_B] 
           ADDSP   .C2     B1,B0,B0          ; [B_C] |157| <0,52>  ^ 
           NOP             0x2               ; [A_B] 
           MPYSP   .N2     B0,BM0,BM0        ; [B_N2] |157| <0,55>  ^ 
           NOP             0x3               ; [A_B] 

           LDD     .D1     *D2(1168),D0      ; [A_D1] <0,59> [C1]
||         MPYSP   .N2     BM7,BM0,B0        ; [B_N2] |157| <0,59>  ^ 

           NOP             0x3               ; [A_B] 
	.dwpsn	file "./inc/tidl_detectionOutput_int.h",line 162,column 5,is_stmt,isa 0
   [!A1]   MV      .L2     BL1,B0            ; [B_L2] |162| <0,63>  ^ 
	.dwpsn	file "./inc/tidl_detectionOutput_int.h",line 165,column 5,is_stmt,isa 0
   [ A0]   MVKU32  .L2     0x7f7fffff,B0     ; [B_L2] |165| <0,64>  ^ [C1]
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 526,column 9,is_stmt,isa 0

           ADDSP   .C2     B0,BM1,BM1        ; [B_C] |527| <0,65> 
||         STW     .D1X    B0,*D0[A8]        ; [A_D1] |526| <0,65>  ^ 

	.dwpsn	file "./inc/tidl_detectionOutput_int.h",line 135,column 25,is_stmt,isa 0

           ADDW    .L1     A8,0x1,A8         ; [A_L1] |511| <0,66> 
||         LDW     .D1     *D1(12),B0        ; [A_D1] |511| <0,66> 
||         SLDW    .D2     *D3(0),AM0        ; [A_D2] |135| <1,0>  ^ 

           MPYWW   .N1     AM1,A8,D0         ; [A_N1] |135| <1,1> 
           NOP             0x3               ; [A_B] 

           SLDD    .D1     *D4(0),D7         ; [A_D1] |135| <1,5> 
||         ADDW    .D2     D6,D0,AM2         ; [A_D2] |135| <1,5> 

           MPYWW   .N1     AM2,AM0,D0        ; [A_N1] |135| <1,6>  ^ 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 511,column 20,is_stmt,isa 0
           CMPGTW  .L1X    B0,A8,A0          ; [A_L1] |511| <0,73> 
   [ A0]   B       .B1     ||$C$L85||        ; [A_B] |511| <0,74> 
;** --------------------------------------------------------------------------*
||$C$L86||:    ; PIPED LOOP EPILOG
;          EXCLUSIVE CPU CYCLES: 1
           PROTCLR                            ; [A_U] VB0
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 1

           B       .B1     ||$C$L88||        ; [A_B] 
||         MV      .M2     BM1,B0            ; [B_M2] 
||         MV      .D1     D2,A5             ; [A_D1] 
||         MV      .L2     BL1,B4            ; [B_L2] 
||         MV      .D2     D1,A6             ; [A_D2] 
||         MV      .L1X    B0,A8             ; [A_L1] 

           ; BRANCH OCCURS {||$C$L88||}      ; [] 
;** --------------------------------------------------------------------------*
||$C$L87||:    
;          EXCLUSIVE CPU CYCLES: 1
           MV      .L2     B4,B0             ; [B_L2] 
;** --------------------------------------------------------------------------*
||$C$L88||:    
;          EXCLUSIVE CPU CYCLES: 21
	.dwpsn	file "./inc/tidl_detectionOutput_int.h",line 237,column 3,is_stmt,isa 0
           VRCPSP  .S2     B0,BM0            ; [B_S2] |237| 
           MPYSP   .N2     B0,BM0,BM1        ; [B_N2] |237| 
           SUBSP   .C2     B6,BM1,BM1        ; [B_C] |237| 
           MPYSP   .N2     BM0,BM1,BM0       ; [B_N2] |237| 
           MPYSP   .N2     B0,BM0,BM1        ; [B_N2] |237| 
           SUBSP   .C2     B6,BM1,BM1        ; [B_C] |237| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 533,column 20,is_stmt,isa 0
           CMPGTW  .L1     A8,0,A0           ; [A_L1] |533| 
	.dwpsn	file "./inc/tidl_detectionOutput_int.h",line 237,column 3,is_stmt,isa 0

   [!A0]   B       .B1     ||$C$L91||        ; [A_B] |533| 
||         MPYSP   .N2     BM0,BM1,BM1       ; [B_N2] |237| 
||         ANDW    .D1     D8,A4,D0          ; [A_D1] |531| 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 533,column 20,is_stmt,isa 0
           ; BRANCHCC OCCURS {||$C$L91||}    ; [] |533| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 1
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 533,column 12,is_stmt,isa 0

           MVKU32  .L2     0x46fffe00,BM7    ; [B_L2] |545| 
||         MVKU32  .L1     0,A10             ; [A_L1] |533| 

;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*      Disqualified loop: Loop contains control code
;*----------------------------------------------------------------------------*
||$C$L89||:    
;          EXCLUSIVE CPU CYCLES: 8
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 537,column 11,is_stmt,isa 0
           LDW     .D1     *A6(16),AL0       ; [A_D1] |537| 
           CMPEQW  .L1     A10,AL0,A0        ; [A_L1] |537| 
   [ A0]   B       .B1     ||$C$L90||        ; [A_B] |537| 
           ; BRANCHCC OCCURS {||$C$L90||}    ; [] |537| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 54
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 542,column 9,is_stmt,isa 0
           LDD     .D1     *A5(1168),D1      ; [A_D1] |542| 
           LDUW    .D1     *D1[A10],BM0      ; [A_D1] |542| 

           MPYSP   .N2     BM1,BM0,B0        ; [B_N2] |542| 
||         LDUW    .D1     *A6(24),AL1       ; [A_D1] |542| 

           MV      .L1X    B0,AL0            ; [A_L1] |542| Define a twin register
           CMPLESP .L1     AL1,AL0,A0        ; [A_L1] |542| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 545,column 11,is_stmt,isa 0
   [ A0]   MPYSP   .N2     BM7,B0,BL0        ; [B_N2] |545| 
   [ A0]   LDD     .D1     *A5(1152),D1      ; [A_D1] |545| 
   [ A0]   VSPTRUNC .L2    BL0,B0            ; [B_L2] |545| 
   [ A0]   STH     .D1X    B0,*D1[A9]        ; [A_D1] |545| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 546,column 11,is_stmt,isa 0
   [ A0]   LDD     .D1     *A5(1176),D1      ; [A_D1] |546| 
           SHLW    .L1     A10,0x10,D2       ; [A_L1] |546| 
           ORW     .D1     D0,D2,D2          ; [A_D1] |546| 
   [ A0]   STW     .D1     D2,*D1[A9]        ; [A_D1] |546| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 547,column 11,is_stmt,isa 0
   [ A0]   LDD     .D1     *A5(1192),D1      ; [A_D1] |547| 
   [ A0]   ADDAW   .D1     D1,A10,D1         ; [A_D1] |547| 
   [ A0]   LDW     .D1     *D1(0),BL0        ; [A_D1] |547| 
   [ A0]   ADDW    .L2     BL0,0x1,B0        ; [B_L2] |547| 
   [ A0]   STW     .D1X    B0,*D1(0)         ; [A_D1] |547| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 548,column 11,is_stmt,isa 0
   [ A0]   LDW     .D1     *A6(12),A8        ; [A_D1] |548| 
   [ A0]   ADDW    .D1     A9,0x1,A9         ; [A_D1] |548| 
;** --------------------------------------------------------------------------*
||$C$L90||:    
;          EXCLUSIVE CPU CYCLES: 3
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 533,column 20,is_stmt,isa 0
           ADDW    .D1     A10,0x1,A10       ; [A_D1] |533| 
           CMPGTW  .L1     A8,A10,A0         ; [A_L1] |533| 
   [ A0]   B       .B1     ||$C$L89||        ; [A_B] |533| 
           ; BRANCHCC OCCURS {||$C$L89||}    ; [] |533| 
;** --------------------------------------------------------------------------*
||$C$L91||:    
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 473,column 27,is_stmt,isa 0
           ADDW    .D1     A3,0xffffffff,A3  ; [A_D1] |473| 

   [ A3]   B       .B1     ||$C$L75||        ; [A_B] |473| 
||         ADDD    .L2     B5,0x4,B5         ; [B_L2] |473| 

           ; BRANCHCC OCCURS {||$C$L75||}    ; [] |473| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 1
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 553,column 25,is_stmt,isa 0

           B       .B1     ||$C$L93||        ; [A_B] |553| 
||         MV      .L2X    A9,B2             ; [B_L2] 

           ; BRANCH OCCURS {||$C$L93||}      ; [] |553| 
;** --------------------------------------------------------------------------*
||$C$L92||:    
;          EXCLUSIVE CPU CYCLES: 6
           LDW     .D1     *A6(12),A8        ; [A_D1] 
;** --------------------------------------------------------------------------*
||$C$L93||:    
;          EXCLUSIVE CPU CYCLES: 2
           CMPGTW  .L1     A8,0,A0           ; [A_L1] |553| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 553,column 20,is_stmt,isa 0

   [!A0]   B       .B1     ||$C$L97||        ; [A_B] |553| 
||         MVKU32  .L2     0,B3              ; [B_L2] 
||         MVKU32  .S2     0,B0              ; [B_S2] |553| 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 553,column 25,is_stmt,isa 0
           ; BRANCHCC OCCURS {||$C$L97||}    ; [] |553| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 7
           MV      .D1     A5,D3             ; [A_D1] 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 557,column 9,is_stmt,isa 0
           SLDD    .D1     *D3(1200),D1      ; [A_D1] |557| <0,1> [C1]
           MV      .L1X    B14,A0            ; [A_L1] 
           MV      .L1X    B3,A1             ; [A_L1] 

           MV      .L2X    A0,B1             ; [B_L2] 
||         MV      .D1     A6,D2             ; [A_D1] 
||         UNPROT          0x1               ; [A_U] 
||         MV      .L1X    B0,A8             ; [A_L1] 

;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*
;*      Loop found in file               : src/tidl_detectionOutput_score.c
;*      Loop source line                 : 553
;*      Loop opening brace source line   : 554
;*      Loop closing brace source line   : 564
;*      Known Minimum Trip Count         : 1                    
;*      Known Max Trip Count Factor      : 1
;*      Loop Carried Dependency Bound(^) : 25
;*      Unpartitioned Resource Bound     : 5
;*      Partitioned Resource Bound       : 5 (pre-sched)
;*
;*      Searching for software pipeline schedule at ...
;*         ii = 25 Unsafe schedule for irregular loop
;*         ii = 25 Unsafe schedule for irregular loop
;*         ii = 25 Did not find schedule
;*         ii = 26 Schedule found with 2 iterations in parallel
;*
;*      Partitioned Resource Bound(*)    : 8 (post-sched)
;*
;*      Constant Extension #0 Used [C0]  : 1
;*      Constant Extension #1 Used [C1]  : 3
;*
;*      Resource Partition (may include "post-sched" split/spill moves):
;*
;*                                                A-side   B-side
;*      .L units                                     0        0     
;*      .S units                                     0        0     
;*      .M units                                     0        0     
;*      .N units                                     0        0     
;*      .D units                                    10        -     
;*      .B units                                     1        -     
;*      .C units                                     -        0     
;*      .P units                                     -        0     
;*
;*      .M/.N units                                  0        0     
;*      .L/.S units                                  1        0     
;*      .L/.S/.C units                               0        0     
;*      .L/.S/.C/.M units                            0        0     
;*      .L/.S/.C/.M/.D units                         8        1     
;*
;*      .X cross paths                               3        0     
;*
;*      Bound(.D1 .D2 .D)                            5        -     
;*      Bound(.M .N .MN)                             0        0     
;*      Bound(.L .S .LS)                             1        0     
;*      Bound(.L .S .C .LS .LSC)                     1        0     
;*      Bound(.L .S .C .M .LS .LSC .LSCM)            1        0     
;*      Bound(.L .S .C .M .D .LS .LSC .LSCM .LSCMD)  4        1     
;*
;*      Register Usage Tables:
;*
;*      +----------------------------------+----------------------------------+
;*      |              ASIDE               |              BSIDE               |
;*      +----------------+--------+--------+----------------+--------+--------+
;*      |      "Axx"     |  "ALx" |  "AMx" |     "VBxx"     | "VBLx" | "VBMx" |
;*      +----------------|--------|--------|----------------|--------|--------+
;*      |0000000000111111|00000000|00000000|0000000000111111|00000000|00000000|
;*      |0123456789012345|01234567|01234567|0123456789012345|01234567|01234567|
;*      +----------------|--------|--------|----------------|--------|--------+
;*   0: | *      *       | *      |        | *              |        |        |
;*   1: | *      *       | *      |        | *              |        |        |
;*   2: | *      *       | *      |        | *              |        |        |
;*   3: | *      *       | *      |        | *              |        |        |
;*   4: | *      *       | *      |        | *              |        |        |
;*   5: | *      *       | *      |        | *              |        |        |
;*   6: | *      *       | *      |        | *              |        |        |
;*   7: | *      *       | *      |        | *              |**      |        |
;*   8: | *      *       | *      |        |**              |        |        |
;*   9: | *      *       | *      |        | *              |        |        |
;*  10: | *      *       | *      |        | *              |        |        |
;*  11: | *      *       | *      |        | *              |        |        |
;*  12: | *      *       | *      |        | *              |        |        |
;*  13: | *      *       | *      |        | *              |        |        |
;*  14: | *      *       | *      |        | *              |        |        |
;*  15: | *      *       | *      |        | *              |        |        |
;*  16: | *      *       | *      |        | *              |        |        |
;*  17: | *      *       | *      |        | *              |        |        |
;*  18: | *      *       | *      |        | *              |        |        |
;*  19: | *      *       | *      |        | *              |        |        |
;*  20: | *      *       | *      |        | *              |        |        |
;*  21: | *      *       | *      |        | *              |        |        |
;*  22: | *      *       | *      |        | *              |        |        |
;*  23: | *      *       | *      |        | *              |        |        |
;*  24: | *      *       |**      |        | *              |        |        |
;*  25: |**      *       | *      |        | *              |        |        |
;*      +----------------+--------+--------+----------------+--------+--------+
;*
;*      +----------------+                 +--------+       +-------+
;*      |      "Dxx"     |                 |  "Px"  |       |"CUCRx"|
;*      +----------------+                 +--------+       +-------+
;*      |0000000000111111|                 |00000000|       |0000   |
;*      |0123456789012345|                 |01234567|       |0123   |
;*      +----------------+                 +--------+       +-------+
;*   0: |**** *          |                 |        |       |       |
;*   1: |*******         |                 |        |       |       |
;*   2: |**** *          |                 |        |       |       |
;*   3: |**** *          |                 |        |       |       |
;*   4: |**** *          |                 |        |       |       |
;*   5: |**** *          |                 |        |       |       |
;*   6: |**** *          |                 |        |       |       |
;*   7: |**** *          |                 |        |       |       |
;*   8: |**** *          |                 |        |       |       |
;*   9: |**** *          |                 |        |       |       |
;*  10: |**** *          |                 |        |       |       |
;*  11: |**** *          |                 |        |       |       |
;*  12: |**** *          |                 |        |       |       |
;*  13: |**** *          |                 |        |       |       |
;*  14: |**** *          |                 |        |       |       |
;*  15: |******          |                 |        |       |       |
;*  16: |******          |                 |        |       |       |
;*  17: |******          |                 |        |       |       |
;*  18: |**** *          |                 |        |       |       |
;*  19: |**** *          |                 |        |       |       |
;*  20: |**** *          |                 |        |       |       |
;*  21: |**** *          |                 |        |       |       |
;*  22: |**** *          |                 |        |       |       |
;*  23: |**** *          |                 |        |       |       |
;*  24: |**** *          |                 |        |       |       |
;*  25: |******          |                 |        |       |       |
;*      +----------------+                 +--------+       +-------+
;*
;*      Done
;*
;*      Collapsed epilog stages       : 1
;*      Prolog not removed
;*      Collapsed prolog stages       : 0
;*
;*      Max amt of load speculation   : 0 bytes
;*
;*      Minimum safe trip count       : 1
;*      Min. prof. trip count  (est.) : 2
;*
;*      Mem bank conflicts/iter(est.) : { min 0.000, est 0.000, max 0.000 }
;*      Mem bank perf. penalty (est.) : 0.0%
;*
;*
;*      Total cycles (est.)         : 7 + trip_cnt * 26        
;*----------------------------------------------------------------------------*
;*        SINGLE SCHEDULED ITERATION
;*
;*        ||$C$C1133||:
;*   0      [ A1]   SLDD    .D2     *D3(1200),D0      ; [A_D2] |561|  ^ [C0]
;*     ||   [ A1]   SLDD    .D1     *D3(1192),D4      ; [A_D1] |561|  ^ [C1]
;*   1      [!A1]   SLDD    .D1     *D3(1200),D1      ; [A_D1] |557| [C1]
;*   2              NOP     0x4     ; [A_B] 
;*   6      [ A1]   ADDD    .D2     D0,A1,D5          ; [A_D2] |561|  ^ 
;*     ||   [ A1]   ADDD    .D1     A1,D4,AL1         ; [A_D1] |561|  ^ 
;*   7      [!A1]   STW     .D1X    B1,*D1[A8]        ; [A_D1] |557| 
;*     ||   [ A1]   ADDD    .D2     D5,0xfffffffc,D4  ; [A_D2] |561|  ^ 
;*     ||   [ A1]   ADDD    .L1     AL1,0xfffffffc,D6 ; [A_L1] |561|  ^ 
;*   8      [ A1]   LDW     .D2     *D4(0),BL0        ; [A_D2] |561|  ^ 
;*     ||   [ A1]   LDW     .D1     *D6(0),BL1        ; [A_D1] |561|  ^ 
;*   9              NOP     0x5     ; [A_B] 
;*  14      [ A1]   ADDW    .L2     BL1,BL0,B0        ; [B_L2] |561|  ^ 
;*  15      [ A1]   STW     .D1X    B0,*D0[A8]        ; [A_D1] |561|  ^ 
;*  16      [ A1]   LDD     .D1     *D3(1192),D4      ; [A_D1] |562|  ^ [C1]
;*  17              NOP     0x5     ; [A_B] 
;*  22      [ A1]   ADDD    .D1     A1,D4,D4          ; [A_D1] |562|  ^ 
;*  23      [ A1]   ADDD    .D1     D4,0xfffffffc,D4  ; [A_D1] |562|  ^ 
;*  24      [ A1]   STW     .D1X    B1,*D4(0)         ; [A_D1] |562|  ^ 
;*  25              LDW     .D1     *D2(12),AL0       ; [A_D1] |553| 
;*     ||           ADDD    .D2     A1,0x4,A1         ; [A_D2] |553|  ^ 
;*  26              NOP     0x4     ; [A_B] 
;*  30              ADDW    .D1     A8,0x1,A8         ; [A_D1] |553| 
;*  31              CMPGTW  .L1     AL0,A8,A0         ; [A_L1] |553| 
;*  32      [ A0]   B       .B1     ||$C$C1133||      ; [A_B] |553| 
;*  33              ; BRANCHCC OCCURS {||$C$C1133||}  ; [] |553| 
;*----------------------------------------------------------------------------*
||$C$L94||:    ; PIPED LOOP PROLOG
;** --------------------------------------------------------------------------*
||$C$L95||:    ; PIPED LOOP KERNEL
;          EXCLUSIVE CPU CYCLES: 26

   [!A1]   STW     .D1X    B1,*D1[A8]        ; [A_D1] |557| <0,7> 
|| [ A1]   ADDD    .D2     D5,0xfffffffc,D4  ; [A_D2] |561| <0,7>  ^ 
|| [ A1]   ADDD    .L1     AL1,0xfffffffc,D6 ; [A_L1] |561| <0,7>  ^ 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 561,column 9,is_stmt,isa 0

   [ A1]   LDW     .D2     *D4(0),BL0        ; [A_D2] |561| <0,8>  ^ 
|| [ A1]   LDW     .D1     *D6(0),BL1        ; [A_D1] |561| <0,8>  ^ 

           NOP             0x5               ; [A_B] 
   [ A1]   ADDW    .L2     BL1,BL0,B0        ; [B_L2] |561| <0,14>  ^ 
   [ A1]   STW     .D1X    B0,*D0[A8]        ; [A_D1] |561| <0,15>  ^ 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 562,column 9,is_stmt,isa 0
   [ A1]   LDD     .D1     *D3(1192),D4      ; [A_D1] |562| <0,16>  ^ [C1]
           NOP             0x5               ; [A_B] 
   [ A1]   ADDD    .D1     A1,D4,D4          ; [A_D1] |562| <0,22>  ^ 
   [ A1]   ADDD    .D1     D4,0xfffffffc,D4  ; [A_D1] |562| <0,23>  ^ 
   [ A1]   STW     .D1X    B1,*D4(0)         ; [A_D1] |562| <0,24>  ^ 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 553,column 25,is_stmt,isa 0

           ADDD    .D2     A1,0x4,A1         ; [A_D2] |553| <0,25>  ^ 
||         LDW     .D1     *D2(12),AL0       ; [A_D1] |553| <0,25> 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 561,column 9,is_stmt,isa 0

   [ A1]   SLDD    .D2     *D3(1200),D0      ; [A_D2] |561| <1,0>  ^ [C0]
|| [ A1]   SLDD    .D1     *D3(1192),D4      ; [A_D1] |561| <1,0>  ^ [C1]

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 557,column 9,is_stmt,isa 0
   [!A1]   SLDD    .D1     *D3(1200),D1      ; [A_D1] |557| <1,1> [C1]
           NOP             0x2               ; [A_B] 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 553,column 25,is_stmt,isa 0
           ADDW    .D1     A8,0x1,A8         ; [A_D1] |553| <0,30> 
           CMPGTW  .L1     AL0,A8,A0         ; [A_L1] |553| <0,31> 

   [ A0]   B       .B1     ||$C$L95||        ; [A_B] |553| <0,32> 
|| [ A1]   ADDD    .D2     D0,A1,D5          ; [A_D2] |561| <1,6>  ^ 
|| [ A1]   ADDD    .D1     A1,D4,AL1         ; [A_D1] |561| <1,6>  ^ 

;** --------------------------------------------------------------------------*
||$C$L96||:    ; PIPED LOOP EPILOG
;          EXCLUSIVE CPU CYCLES: 1
           PROTCLR                            ; [A_U] AL0
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 3
           MV      .L1X    B1,A0             ; [A_L1] 

           MV      .L2X    A0,B14            ; [B_L2] 
||         MV      .L1     AL0,A8            ; [A_L1] 
||         MV      .D1     D3,A5             ; [A_D1] 

;** --------------------------------------------------------------------------*
||$C$L97||:    
;          EXCLUSIVE CPU CYCLES: 9
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 566,column 5,is_stmt,isa 0
           LDD     .D1     *A5(1192),D0      ; [A_D1] |566| 

           ADDAW   .D1     D0,A8,D0          ; [A_D1] |566| 
||         MV      .L1X    B2,A9             ; [A_L1] 

           ADDD    .D1     D0,0xfffffffc,D0  ; [A_D1] |566| 
||         CMPGTW  .L1     A9,0,A4           ; [A_L1] 
||         MV      .S1X    B14,A0            ; [A_S1] |566| 

   [!A4]   B       .B1     ||$C$L101||       ; [A_B] 
||         STW     .D1     A0,*D0(0)         ; [A_D1] |566| 
||         MVKU32  .L2     0,B0              ; [B_L2] 
||         MVKU32  .S2     0xff,B1           ; [B_S2] 

           ; BRANCHCC OCCURS {||$C$L101||}   ; [] 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 4
           MV      .D1     A9,AL0            ; [A_D1] 

           EXT     .L1     AL0,0x20,0x20,A0  ; [A_L1] 
||         MV      .S1X    B0,A1             ; [A_S1] 

           NLCINIT .S1     A0,0x1,0          ; [A_S1] 
||         MV      .L1X    B0,D4             ; [A_L1] 
||         UNPROT          0x1               ; [A_U] 

           TICK                               ; [A_U] <0,0> 
||         MV      .L1X    B1,D5             ; [A_L1] 
||         MV      .D1     A5,D2             ; [A_D1] 

;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*
;*      Loop found in file               : src/tidl_detectionOutput_score.c
;*      Loop source line                 : 568
;*      Loop opening brace source line   : 569
;*      Loop closing brace source line   : 577
;*      Known Minimum Trip Count         : 1                    
;*      Known Max Trip Count Factor      : 1
;*      Loop Carried Dependency Bound(^) : 52
;*      Unpartitioned Resource Bound     : 10
;*      Partitioned Resource Bound       : 10 (pre-sched)
;*
;*      Searching for software pipeline schedule at ...
;*         ii = 52 Schedule found with 2 iterations in parallel
;*
;*      Partitioned Resource Bound(*)    : 13 (post-sched)
;*
;*      Constant Extension #0 Used [C0]  : 3
;*      Constant Extension #1 Used [C1]  : 6
;*
;*      Resource Partition (may include "post-sched" split/spill moves):
;*
;*                                                A-side   B-side
;*      .L units                                     0        0     
;*      .S units                                     0        0     
;*      .M units                                     0        0     
;*      .N units                                     0        0     
;*      .D units                                    20        -     
;*      .B units                                     1        -     
;*      .C units                                     -        0     
;*      .P units                                     -        0     
;*
;*      .M/.N units                                  0        0     
;*      .L/.S units                                  1        0     
;*      .L/.S/.C units                               0        0     
;*      .L/.S/.C/.M units                            0        0     
;*      .L/.S/.C/.M/.D units                         7        1     
;*
;*      .X cross paths                               2        0     
;*
;*      Bound(.D1 .D2 .D)                           10        -     
;*      Bound(.M .N .MN)                             0        0     
;*      Bound(.L .S .LS)                             1        0     
;*      Bound(.L .S .C .LS .LSC)                     1        0     
;*      Bound(.L .S .C .M .LS .LSC .LSCM)            1        0     
;*      Bound(.L .S .C .M .D .LS .LSC .LSCM .LSCMD)  6        1     
;*
;*      Register Usage Tables:
;*
;*      +----------------------------------+----------------------------------+
;*      |              ASIDE               |              BSIDE               |
;*      +----------------+--------+--------+----------------+--------+--------+
;*      |      "Axx"     |  "ALx" |  "AMx" |     "VBxx"     | "VBLx" | "VBMx" |
;*      +----------------|--------|--------|----------------|--------|--------+
;*      |0000000000111111|00000000|00000000|0000000000111111|00000000|00000000|
;*      |0123456789012345|01234567|01234567|0123456789012345|01234567|01234567|
;*      +----------------|--------|--------|----------------|--------|--------+
;*   0: | *              |        |        |                |        |        |
;*   1: | *              |        |        |                |        |        |
;*   2: | *              |        |        |                |        |        |
;*   3: | *              |        |        |                |        |        |
;*   4: | *              |        |        |                |        |        |
;*   5: | *              |        |        |                |        |        |
;*   6: | *              |        |        |                |        |        |
;*   7: | *              |        |        |                |        |        |
;*   8: | *              |        |        |                |        |        |
;*   9: | *              |        |        |                |        |        |
;*  10: | *              |        |        |                |        |        |
;*  11: | *              |        |        |                |        |        |
;*  12: | *              |        |        |                |        |        |
;*  13: |**              |        |        |                |        |        |
;*  14: |**              |        |        |                |        |        |
;*  15: |**              |        |        |                |        |        |
;*  16: |**              |        |        |                |        |        |
;*  17: |**              |        |        |                |        |        |
;*  18: |**              |        |        |                |        |        |
;*  19: |**              |        |        |                |        |        |
;*  20: |**              |        |        |                |        |        |
;*  21: |**              |        |        |                |        |        |
;*  22: |**              |        |        |                |        |        |
;*  23: | *              |        |        |                |        |        |
;*  24: | *              |        |        |                |        |        |
;*  25: | *              |        |        |                |        |        |
;*  26: | *              |        |        |                |        |        |
;*  27: | *              |        |        |                |        |        |
;*  28: | *              |        |        |                |        |        |
;*  29: | *              |        |        |                |        |        |
;*  30: | *              |        |        |                |        |        |
;*  31: | *              |        |        |                |        |        |
;*  32: | *              |        |        |                |        |        |
;*  33: | *              |        |        |                |        |        |
;*  34: | *              |        |        |                |        |        |
;*  35: | *              |        |        |                |        |        |
;*  36: | *              |        |        |*               |        |        |
;*  37: | *              |        |        |                |        |        |
;*  38: | *              |        |        |                |        |        |
;*  39: | *              |        |        |                |        |        |
;*  40: | *              |        |        |                |        |        |
;*  41: | *              |        |        |                |        |        |
;*  42: | *              |        |        |                |        |        |
;*  43: | *              |        |        |                |        |        |
;*  44: | *              |        |        |                |        |        |
;*  45: | *              |        |        |                |        |        |
;*  46: | *              |        |        |                |        |        |
;*  47: | *              |        |        |                |        |        |
;*  48: | *              |        |        |                |        |        |
;*  49: | *              |        |        |                |        |        |
;*  50: | *              |        |        |                |*       |        |
;*  51: | *              |        |        |*               |        |        |
;*      +----------------+--------+--------+----------------+--------+--------+
;*
;*      +----------------+                 +--------+       +-------+
;*      |      "Dxx"     |                 |  "Px"  |       |"CUCRx"|
;*      +----------------+                 +--------+       +-------+
;*      |0000000000111111|                 |00000000|       |0000   |
;*      |0123456789012345|                 |01234567|       |0123   |
;*      +----------------+                 +--------+       +-------+
;*   0: |  * **          |                 |        |       |       |
;*   1: |  * **          |                 |        |       |       |
;*   2: |  * **          |                 |        |       |       |
;*   3: |  * **          |                 |        |       |       |
;*   4: |  * **          |                 |        |       |       |
;*   5: |  * **          |                 |        |       |       |
;*   6: |* * **          |                 |        |       |       |
;*   7: |* * **          |                 |        |       |       |
;*   8: |  * **          |                 |        |       |       |
;*   9: |  * **          |                 |        |       |       |
;*  10: |  * **          |                 |        |       |       |
;*  11: |  * **          |                 |        |       |       |
;*  12: |  * **          |                 |        |       |       |
;*  13: |  * **          |                 |        |       |       |
;*  14: |* * **          |                 |        |       |       |
;*  15: |******          |                 |        |       |       |
;*  16: | ** **          |                 |        |       |       |
;*  17: | ** **          |                 |        |       |       |
;*  18: | ** **          |                 |        |       |       |
;*  19: | ** **          |                 |        |       |       |
;*  20: | ** **          |                 |        |       |       |
;*  21: |******          |                 |        |       |       |
;*  22: |******          |                 |        |       |       |
;*  23: | ** **          |                 |        |       |       |
;*  24: | ** **          |                 |        |       |       |
;*  25: | ** **          |                 |        |       |       |
;*  26: | ** **          |                 |        |       |       |
;*  27: | ** **          |                 |        |       |       |
;*  28: |*** **          |                 |        |       |       |
;*  29: |*******         |                 |        |       |       |
;*  30: | ** ***         |                 |        |       |       |
;*  31: | ** **          |                 |        |       |       |
;*  32: | ** **          |                 |        |       |       |
;*  33: | ** **          |                 |        |       |       |
;*  34: | ** **          |                 |        |       |       |
;*  35: |******          |                 |        |       |       |
;*  36: |******          |                 |        |       |       |
;*  37: | ** **          |                 |        |       |       |
;*  38: | ** **          |                 |        |       |       |
;*  39: | ** **          |                 |        |       |       |
;*  40: | ** **          |                 |        |       |       |
;*  41: | ** **          |                 |        |       |       |
;*  42: | ** **          |                 |        |       |       |
;*  43: |*** **          |                 |        |       |       |
;*  44: |* * **          |                 |        |       |       |
;*  45: |* * **          |                 |        |       |       |
;*  46: |* * **          |                 |        |       |       |
;*  47: |* * **          |                 |        |       |       |
;*  48: |* * **          |                 |        |       |       |
;*  49: |* * **          |                 |        |       |       |
;*  50: |* * **          |                 |        |       |       |
;*  51: |* * **          |                 |        |       |       |
;*      +----------------+                 +--------+       +-------+
;*
;*      Done
;*
;*      Collapsed epilog stages       : 1
;*      Prolog not removed
;*      Collapsed prolog stages       : 0
;*
;*      Max amt of load speculation   : 0 bytes
;*
;*      Minimum safe trip count       : 1
;*      Min. prof. trip count  (est.) : 2
;*
;*      Mem bank conflicts/iter(est.) : { min 0.000, est 0.000, max 0.000 }
;*      Mem bank perf. penalty (est.) : 0.0%
;*
;*
;*      Total cycles (est.)         : 1 + trip_cnt * 52        
;*----------------------------------------------------------------------------*
;*        SINGLE SCHEDULED ITERATION
;*
;*        ||$C$C1043||:
;*   0              TICK                               ; [A_U] 
;*   1              LDD     .D1     *D2(1176),D0      ; [A_D1] |570|  ^ [C1]
;*   2              NOP     0x5     ; [A_B] 
;*   7              ADDD    .D1     A1,D0,D0          ; [A_D1] |570|  ^ 
;*   8              LDW     .D1     *D0(0),A0         ; [A_D1] |570|  ^ 
;*   9              NOP     0x1     ; [A_B] 
;*  10              LDD     .D2     *D2(1200),D0      ; [A_D2] |574| [C0]
;*     ||           LDD     .D1     *D2(1192),D3      ; [A_D1] |574| [C1]
;*  11              NOP     0x3     ; [A_B] 
;*  14              SHRW    .L1     A0,0x10,D0        ; [A_L1] |574|  ^ 
;*  15              ANDW    .D1     D5,D0,D1          ; [A_D1] |574|  ^ 
;*  16              LDW     .D2     *D0[D1],D0        ; [A_D2] |574|  ^ 
;*     ||           LDW     .D1     *D3[D1],D3        ; [A_D1] |574|  ^ 
;*  17              LDD     .D1     *D2(1184),D3      ; [A_D1] |574| [C1]
;*  18              NOP     0x4     ; [A_B] 
;*  22              ADDW    .D1     D3,D0,D0          ; [A_D1] |574|  ^ 
;*  23              LDD     .D2     *D2(1152),D0      ; [A_D2] |571| [C0]
;*     ||           STW     .D1     A0,*D3[D0]        ; [A_D1] |574|  ^ 
;*  24              LDD     .D2     *D2(1200),D0      ; [A_D2] |575|  ^ [C0]
;*     ||           LDD     .D1     *D2(1192),D3      ; [A_D1] |575|  ^ [C1]
;*  25              NOP     0x4     ; [A_B] 
;*  29              ADDD    .D1     D4,D0,D6          ; [A_D1] |571| 
;*  30              LDW     .D2     *D0[D1],D0        ; [A_D2] |575|  ^ 
;*     ||           LDW     .D1     *D3[D1],D3        ; [A_D1] |575|  ^ 
;*  31              LDUH    .D2     *D6(0),B0         ; [A_D2] |571| 
;*     ||           LDD     .D1     *D2(1160),D3      ; [A_D1] |575| [C1]
;*  32              NOP     0x4     ; [A_B] 
;*  36              ADDW    .D1     D3,D0,D0          ; [A_D1] |575|  ^ 
;*  37              STH     .D1X    B0,*D3[D0]        ; [A_D1] |575|  ^ 
;*  38              LDD     .D1     *D2(1192),D0      ; [A_D1] |576|  ^ [C1]
;*  39              NOP     0x5     ; [A_B] 
;*  44              ADDAW   .D1     D0,D1,D0          ; [A_D1] |576|  ^ 
;*  45              LDW     .D1     *D0(0),BL0        ; [A_D1] |576|  ^ 
;*  46              NOP     0x5     ; [A_B] 
;*  51              ADDW    .L2     BL0,0x1,B0        ; [B_L2] |576|  ^ 
;*  52              STW     .D1X    B0,*D0(0)         ; [A_D1] |576|  ^ 
;*     ||           ADDD    .L1     A1,0x4,A1         ; [A_L1] |568| 
;*     ||           ADDD    .D2     D4,0x2,D4         ; [A_D2] |568| 
;*     ||           BNL     .B1     ||$C$C1043||      ; [A_B] |568| 
;*  53              ; BRANCHCC OCCURS {||$C$C1043||}  ; [] |568| 
;*----------------------------------------------------------------------------*
||$C$L98||:    ; PIPED LOOP PROLOG
;** --------------------------------------------------------------------------*
||$C$L99||:    ; PIPED LOOP KERNEL
;          EXCLUSIVE CPU CYCLES: 52
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 570,column 24,is_stmt,isa 0
           LDD     .D1     *D2(1176),D0      ; [A_D1] |570| <0,1>  ^ [C1]
           NOP             0x5               ; [A_B] 
           ADDD    .D1     A1,D0,D0          ; [A_D1] |570| <0,7>  ^ 
           LDW     .D1     *D0(0),A0         ; [A_D1] |570| <0,8>  ^ 
           NOP             0x1               ; [A_B] 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 574,column 7,is_stmt,isa 0

           LDD     .D2     *D2(1200),D0      ; [A_D2] |574| <0,10> [C0]
||         LDD     .D1     *D2(1192),D3      ; [A_D1] |574| <0,10> [C1]

           NOP             0x3               ; [A_B] 
           SHRW    .L1     A0,0x10,D0        ; [A_L1] |574| <0,14>  ^ 
           ANDW    .D1     D5,D0,D1          ; [A_D1] |574| <0,15>  ^ 

           LDW     .D2     *D0[D1],D0        ; [A_D2] |574| <0,16>  ^ 
||         LDW     .D1     *D3[D1],D3        ; [A_D1] |574| <0,16>  ^ 

           LDD     .D1     *D2(1184),D3      ; [A_D1] |574| <0,17> [C1]
           NOP             0x4               ; [A_B] 
           ADDW    .D1     D3,D0,D0          ; [A_D1] |574| <0,22>  ^ 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 571,column 22,is_stmt,isa 0

           LDD     .D2     *D2(1152),D0      ; [A_D2] |571| <0,23> [C0]
||         STW     .D1     A0,*D3[D0]        ; [A_D1] |574| <0,23>  ^ 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 575,column 7,is_stmt,isa 0

           LDD     .D2     *D2(1200),D0      ; [A_D2] |575| <0,24>  ^ [C0]
||         LDD     .D1     *D2(1192),D3      ; [A_D1] |575| <0,24>  ^ [C1]

           NOP             0x4               ; [A_B] 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 571,column 22,is_stmt,isa 0
           ADDD    .D1     D4,D0,D6          ; [A_D1] |571| <0,29> 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 575,column 7,is_stmt,isa 0

           LDW     .D2     *D0[D1],D0        ; [A_D2] |575| <0,30>  ^ 
||         LDW     .D1     *D3[D1],D3        ; [A_D1] |575| <0,30>  ^ 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 571,column 22,is_stmt,isa 0

           LDUH    .D2     *D6(0),B0         ; [A_D2] |571| <0,31> 
||         LDD     .D1     *D2(1160),D3      ; [A_D1] |575| <0,31> [C1]

           NOP             0x4               ; [A_B] 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 575,column 7,is_stmt,isa 0
           ADDW    .D1     D3,D0,D0          ; [A_D1] |575| <0,36>  ^ 
           STH     .D1X    B0,*D3[D0]        ; [A_D1] |575| <0,37>  ^ 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 576,column 7,is_stmt,isa 0
           LDD     .D1     *D2(1192),D0      ; [A_D1] |576| <0,38>  ^ [C1]
           NOP             0x5               ; [A_B] 
           ADDAW   .D1     D0,D1,D0          ; [A_D1] |576| <0,44>  ^ 
           LDW     .D1     *D0(0),BL0        ; [A_D1] |576| <0,45>  ^ 
           NOP             0x5               ; [A_B] 
           ADDW    .L2     BL0,0x1,B0        ; [B_L2] |576| <0,51>  ^ 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 568,column 25,is_stmt,isa 0

           ADDD    .L1     A1,0x4,A1         ; [A_L1] |568| <0,52> 
||         ADDD    .D2     D4,0x2,D4         ; [A_D2] |568| <0,52> 
||         BNL     .B1     ||$C$L99||        ; [A_B] |568| <0,52> 
||         STW     .D1X    B0,*D0(0)         ; [A_D1] |576| <0,52>  ^ 
||         TICK                               ; [A_U] <1,0> 

;** --------------------------------------------------------------------------*
||$C$L100||:    ; PIPED LOOP EPILOG
;          EXCLUSIVE CPU CYCLES: 1
           PROTCLR                            ; [A_U] 
;** --------------------------------------------------------------------------*
||$C$L101||:    
;          EXCLUSIVE CPU CYCLES: 10
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 580,column 3,is_stmt,isa 0
           VLD64B  .D1     *SP(16),VB14      ; [A_D1] |580| 
	.dwcfi	restore_reg, 62

           LDD     .D1     *SP(88),A14       ; [A_D1] |580| 
||         LDD     .D2     *SP(80),A15       ; [A_D2] |580| 
	.dwcfi	restore_reg, 14
	.dwcfi	restore_reg, 15

           LDD     .D1     *SP(104),A12      ; [A_D1] |580| 
||         LDD     .D2     *SP(96),A13       ; [A_D2] |580| 
	.dwcfi	restore_reg, 12
	.dwcfi	restore_reg, 13

           LDD     .D1     *SP(120),A10      ; [A_D1] |580| 
||         LDD     .D2     *SP(112),A11      ; [A_D2] |580| 
	.dwcfi	restore_reg, 10
	.dwcfi	restore_reg, 11

           LDD     .D1     *SP(128),A9       ; [A_D1] |580| 
||         LDD     .D2     *SP(136),A8       ; [A_D2] |580| 
||         MV      .L1     A9,A4             ; [A_L1] |580| 

	.dwcfi	restore_reg, 9
	.dwcfi	restore_reg, 8
$C$DW$36	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$36, DW_AT_low_pc(0x00)
	.dwattr $C$DW$36, DW_AT_TI_return


           RET     .B1     ; [A_B] |580| 
||         ADDD    .D1     SP,0x80,SP        ; [A_D1] |580| 

	.dwcfi	cfa_offset, 0
           ; RETURN OCCURS {RP}              ; [] |580| 
	.dwattr $C$DW$31, DW_AT_TI_end_file("src/tidl_detectionOutput_score.c")
	.dwattr $C$DW$31, DW_AT_TI_end_line(0x245)
	.dwattr $C$DW$31, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$31

	.sect	".text:_Z26TIDL_sparseDetScoreCalc_cnIaEiP26sTIDL_DetectOutputParams_tP29sTIDL_ALgDetectOutputParams_t"
	.clink
	.global	||_Z26TIDL_sparseDetScoreCalc_cnIaEiP26sTIDL_DetectOutputParams_tP29sTIDL_ALgDetectOutputParams_t||

$C$DW$37	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$37, DW_AT_name("int TIDL_sparseDetScoreCalc_cn")
	.dwattr $C$DW$37, DW_AT_low_pc(||_Z26TIDL_sparseDetScoreCalc_cnIaEiP26sTIDL_DetectOutputParams_tP29sTIDL_ALgDetectOutputParams_t||)
	.dwattr $C$DW$37, DW_AT_high_pc(0x00)
	.dwattr $C$DW$37, DW_AT_linkage_name("_Z26TIDL_sparseDetScoreCalc_cnIaEiP26sTIDL_DetectOutputParams_tP29sTIDL_ALgDetectOutputParams_t")
	.dwattr $C$DW$37, DW_AT_external
	.dwattr $C$DW$37, DW_AT_decl_file("src/tidl_detectionOutput_score.c")
	.dwattr $C$DW$37, DW_AT_decl_line(0x13a)
	.dwattr $C$DW$37, DW_AT_decl_column(0x09)
	.dwattr $C$DW$37, DW_AT_TI_max_frame_size(0xa8)
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 315,column 1,is_stmt,address ||_Z26TIDL_sparseDetScoreCalc_cnIaEiP26sTIDL_DetectOutputParams_tP29sTIDL_ALgDetectOutputParams_t||,isa 0

	.dwfde $C$DW$CIE, ||_Z26TIDL_sparseDetScoreCalc_cnIaEiP26sTIDL_DetectOutputParams_tP29sTIDL_ALgDetectOutputParams_t||
$C$DW$38	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$38, DW_AT_name("params")
	.dwattr $C$DW$38, DW_AT_location[DW_OP_reg4]

$C$DW$39	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$39, DW_AT_name("algDetLyrParams")
	.dwattr $C$DW$39, DW_AT_location[DW_OP_reg5]


;******************************************************************************
;* FUNCTION NAME: int TIDL_sparseDetScoreCalc_cn<signed char>(sTIDL_DetectOutputParams_t *, sTIDL_ALgDetectOutputParams_t *)*
;*                                                                            *
;*   Regs Modified     : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,VB0,VB1,   *
;*                           VB2,VB3,VB4,VB5,VB6,VB7,VB8,VB9,VB10,VB11,VB12,  *
;*                           VB13,VB14,VB15,AL0,AL1,AM0,AM1,AM2,AM3,D0,D1,D2, *
;*                           D3,D4,D5,D6,D7,D8,D9,D10,D11,D12,D13,D14,SP,VBL0,*
;*                           VBL1,VBL2,VBL3,VBL4,VBL5,VBL6,VBL7,VBM0,VBM1,    *
;*                           VBM2,VBM3,VBM4,VBM5,VBM6,VBM7,P0,P1,P2           *
;*   Regs Used         : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,VB0,VB1,   *
;*                           VB2,VB3,VB4,VB5,VB6,VB7,VB8,VB9,VB10,VB11,VB12,  *
;*                           VB13,VB14,VB15,AL0,AL1,AM0,AM1,AM2,AM3,D0,D1,D2, *
;*                           D3,D4,D5,D6,D7,D8,D9,D10,D11,D12,D13,D14,SP,VBL0,*
;*                           VBL1,VBL2,VBL3,VBL4,VBL5,VBL6,VBL7,VBM0,VBM1,    *
;*                           VBM2,VBM3,VBM4,VBM5,VBM6,VBM7,P0,P1,P2           *
;*   Local Frame Size  : 0 Args + 0 Auto + 168 Save = 168 byte                *
;******************************************************************************
||_Z26TIDL_sparseDetScoreCalc_cnIaEiP26sTIDL_DetectOutputParams_tP29sTIDL_ALgDetectOutputParams_t||:
;** --------------------------------------------------------------------------*
;* D0    assigned to $O$C36
;* D1    assigned to $O$C37
;* D0    assigned to $O$C38
;* D1    assigned to $O$C39
;* VB0   assigned to $O$C40
;* VBM0  assigned to $O$C41
;* VBM0  assigned to $O$C42
;* VB4   assigned to $O$C43
;* VB3   assigned to $O$C44
;* VB0   assigned to $O$C45
;* A0    assigned to $O$C46
;* A0    assigned to $O$C47
;* D0    assigned to $O$C48
;* D0    assigned to $O$C49
;* VB1   assigned to $O$C50
;* VB1   assigned to $O$C51
;* VB0   assigned to $O$C52
;* VB2   assigned to $O$C53
;* VB1   assigned to $O$C54
;* A2    assigned to $O$C55
;* VB1   assigned to $O$C56
;* A0    assigned to $O$C57
;* D5    assigned to $O$C58
;* VB4   assigned to $O$C59
;* VBM5  assigned to $O$C60
;* VBM5  assigned to $O$C61
;* AM1   assigned to $O$C62
;* AM1   assigned to $O$C63
;* VB5   assigned to $O$C64
;* VB6   assigned to $O$C65
;* VBM5  assigned to $O$C66
;* D0    assigned to $O$C67
;* VB11  assigned to $O$C68
;* AM1   assigned to $O$C69
;* VB5   assigned to $O$C70
;* VB4   assigned to $O$C71
;* VB7   assigned to $O$C72
;* VB4   assigned to $O$C73
;* VBM7  assigned to $O$C74
;* VBM6  assigned to $O$C75
;* A2    assigned to $O$C76
;* D3    assigned to $O$C77
;* VB2   assigned to $O$C78
;* D0    assigned to $O$C79
;* D1    assigned to $O$C80
;* D2    assigned to $O$C81
;* VB6   assigned to $O$K26
;* VB1   assigned to $O$K26
;* VB1   assigned to $O$K62
;* D3    assigned to $O$K62
;* VB14  assigned to $O$K62
;* VB3   assigned to $O$K62
;* VB2   assigned to $O$K157
;* VB3   assigned to $O$K157
;* VBM4  assigned to $O$K157
;* D9    assigned to $O$K151
;* D1    assigned to $O$K151
;* D3    assigned to $O$K151
;* D11   assigned to $O$K137
;* VBL4  assigned to $O$K137
;* VBL7  assigned to $O$K137
;* D6    assigned to $O$K127
;* D4    assigned to $O$K127
;* VB15  assigned to $O$K170
;* VBM1  assigned to $O$K170
;* VB9   assigned to $O$K170
;* VB14  assigned to $O$K185
;* VBM7  assigned to $O$K185
;* VB10  assigned to $O$K185
;* VB13  assigned to $O$K181
;* VBM6  assigned to $O$K181
;* VBM3  assigned to $O$K181
;* VB12  assigned to $O$K168
;* VBM5  assigned to $O$K168
;* VBM2  assigned to $O$K168
;* VB11  assigned to $O$K177
;* VBM4  assigned to $O$K177
;* VBM1  assigned to $O$K177
;* VB10  assigned to $O$K173
;* VBM3  assigned to $O$K173
;* VBM0  assigned to $O$K173
;* VB9   assigned to $O$K162
;* VBL0  assigned to $O$K162
;* VBL0  assigned to $O$K162
;* VB3   assigned to $O$K187
;* VBL3  assigned to $O$K187
;* VB2   assigned to $O$K187
;* VBL2  assigned to $O$K191
;* VB0   assigned to $O$K191
;* VB7   assigned to $O$K233
;* VBM0  assigned to $O$K233
;* VB8   assigned to $O$K233
;* VBM2  assigned to $O$K252
;* VB13  assigned to $O$K252
;* A11   assigned to $O$U435
;* A12   assigned to $O$U435
;* A10   assigned to $O$U442
;* A7    assigned to $O$U433
;* A8    assigned to $O$v1
;* A9    assigned to $O$v1
;* A4    assigned to $O$Lr3$totValidCnt
;* VB1   assigned to $O$Lr9$max
;* VB3   assigned to $O$U29
;* A0    assigned to $O$U36
;* VB2   assigned to $O$U55
;* D2    assigned to $O$U100
;* D2    assigned to $O$U30
;* A1    assigned to $O$U275
;* D2    assigned to $O$U292
;* VB5   assigned to $O$U393
;* VBL7  assigned to $O$K416
;* A11   assigned to $O$U443
;* A10   assigned to $O$U434
;* D0    assigned to $O$U510
;* D10   assigned to $O$K525
;* VB2   assigned to $O$U564
;* VB0   assigned to $O$U580
;* VB0   assigned to $O$U584
;* VB0   assigned to $O$L1
;* A1    assigned to $O$L2
;* A0    assigned to $O$L3
;* A1    assigned to $O$L4
;* A0    assigned to $O$L5
;* A3    assigned to $O$L6
;* VB0   assigned to $O$L7
;* A4    assigned to $O$L8
;* VB2   assigned to $O$I27
;* A9    assigned to $O$v5
;* AL0   assigned to $O$v4
;* AL0   assigned to $O$v3
;* AL0   assigned to $O$v2
;* VBL6  assigned to $O$Lr2$maxInit
;* VB0   assigned to $O$Lr4$classCnt
;* A8    assigned to $O$Lr952$i
;* VB0   assigned to $O$Lr5$totalCnt
;* A0    assigned to $O$Lr886$curIndex
;* D1    assigned to $O$Lr656$totValidCnt
;* A4    assigned to $O$Lr1018$totValidCnt
;* A0    assigned to $O$Lr856$curClassCountM
;* A7    assigned to $O$Lr623$cnt
;* VB3   assigned to $O$Lr756$curIndex
;* VB7   assigned to $O$Lr817$y
;* A3    assigned to $O$Lr675$y
;* VB6   assigned to $O$Lr707$y
;* VB4   assigned to $O$Lr658$y
;* A8    assigned to $O$Lr579$i
;* D4    assigned to $O$Lr453$cnt
;* VB1   assigned to $O$Lr528$y
;* VB0   assigned to $O$Lr425$y
;* VB0   assigned to $O$Lr408$curClassCountM
;* VB1   assigned to $O$Lr413
;* D8    assigned to $O$Lr197$totValidCnt
;* A4    assigned to $O$Lr1019$totValidCnt
;* A4    assigned to $O$Lr383$curIndex
;* AM3   assigned to $O$Lr391$anchor
;* A2    assigned to $O$Lr393$curLoc
;* A9    assigned to $O$Lr387$head
;* VB4   assigned to $O$Lr401$onebyqFact
;* VB1   assigned to $O$Lr356$max
;* VB0   assigned to $O$Lr233$denom
;* A1    assigned to $O$Lr11$classStride
;* A7    assigned to $O$Lr12$anchorStride
;* A0    assigned to $O$Lr358$i4
;* A8    assigned to $O$Lr235$i4
;* VB0   assigned to $O$Lr10$denom
;* VBL0  assigned to $O$Lr309$inVal
;* VB1   assigned to $O$Lr278$ftemp
;* VB1   assigned to $O$Lr243$yI
;* VB0   assigned to $O$Lr13$ePwX
;* VBM1  assigned to $O$Lr220$recp_y
;* D0    assigned to $O$Lr222$curIndex
;* A9    assigned to $O$Lr159$i4
;* VB0   assigned to $O$Lr106$i
;* A0    assigned to $O$Lr20$curIndex
;* VB0   assigned to $O$Lr27$score
;* A5    assigned to algDetLyrParams
$C$DW$40	.dwtag  DW_TAG_variable
	.dwattr $C$DW$40, DW_AT_name("algDetLyrParams")
	.dwattr $C$DW$40, DW_AT_type(*$C$DW$T$146)
	.dwattr $C$DW$40, DW_AT_location[DW_OP_reg5]

;* A6    assigned to params
$C$DW$41	.dwtag  DW_TAG_variable
	.dwattr $C$DW$41, DW_AT_name("params")
	.dwattr $C$DW$41, DW_AT_type(*$C$DW$T$144)
	.dwattr $C$DW$41, DW_AT_location[DW_OP_reg6]

	.dwcfi	cfa_offset, 0
;          EXCLUSIVE CPU CYCLES: 15
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 330,column 3,is_stmt,isa 0
           LDW     .D1     *A5(1304),AL0     ; [A_D1] |330| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 315,column 1,is_stmt,isa 0

           CMPEQW  .L1     AL0,0x1,A0        ; [A_L1] |330| 
||         MV      .D1     A4,A6             ; [A_D1] |315| 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 343,column 3,is_stmt,isa 0

   [!A0]   LDW     .D1     *A6(68),AL0       ; [A_D1] |343| 
|| [ A0]   LDW     .D2     *A6(68),AL0       ; [A_D2] |343| 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 326,column 19,is_stmt,isa 0

           MVK32   .L2     0xffff8000,BL6    ; [B_L2] |326| 
||         STD     .D1     A8,*SP(8)         ; [A_D1] 
||         STD     .D2X    A9,*SP++(-168)    ; [A_D2] 
	.dwcfi	save_reg_to_mem, 8, 8
	.dwcfi	cfa_offset, 168
	.dwcfi	save_reg_to_mem, 9, -168

           STD     .D1     A11,*SP(152)      ; [A_D1] 
||         STD     .D2X    A10,*SP(160)      ; [A_D2] 

	.dwcfi	save_reg_to_mem, 11, 152
	.dwcfi	save_reg_to_mem, 10, 160
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 332,column 5,is_stmt,isa 0

           CMPEQW  .L1     AL0,0x2,A0        ; [A_L1] |343| 
|| [ A0]   MVK32   .L2     0xffffff80,BL6    ; [B_L2] |332| 
||         VST64B  .D2     VB15,*SP(80)      ; [A_D2] 

	.dwcfi	save_reg_to_mem, 63, 80
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 343,column 3,is_stmt,isa 0

   [ A0]   B       .B1     ||$C$L126||       ; [A_B] |343| 
||         VST64B  .D2     VB14,*SP(16)      ; [A_D2] 
||         STD     .D1     A12,*SP(144)      ; [A_D1] 

	.dwcfi	save_reg_to_mem, 62, 16
	.dwcfi	save_reg_to_mem, 12, 144
           ; BRANCHCC OCCURS {||$C$L126||}   ; [] |343| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 8
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 350,column 25,is_stmt,isa 0
           LDW     .D1     *A6(12),A9        ; [A_D1] |350| 

           CMPGTW  .L1     A9,0,A0           ; [A_L1] |350| 
||         MVKU32  .L2     0,B2              ; [B_L2] 
||         MVKU32  .S2     0,B1              ; [B_S2] 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 350,column 20,is_stmt,isa 0

   [!A0]   B       .B1     ||$C$L105||       ; [A_B] |350| 
||         EXT     .L2     B1,0x20,0x20,B3   ; [B_L2] 
||         MVKU32  .L1     0,A8              ; [A_L1] |350| 
||         MVKU32  .S2     0,B0              ; [B_S2] 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 350,column 25,is_stmt,isa 0
           ; BRANCHCC OCCURS {||$C$L105||}   ; [] |350| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 9

           MV      .D1     A5,D4             ; [A_D1] 
||         MV      .L1X    B3,A1             ; [A_L1] 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 358,column 9,is_stmt,isa 0

   [ A1]   SLDD    .D1     *D4(1200),D2      ; [A_D1] |358| <0,1>  ^ [C1]
|| [ A1]   SLDD    .D2     *D4(1192),D6      ; [A_D2] |358| <0,0>  ^ [C1]

   [ A1]   ADDD    .D1     D2,A1,D6          ; [A_D1] |358| <0,7>  ^ 
|| [ A1]   ADDD    .D2     A1,D6,D7          ; [A_D2] |358| <0,7>  ^ 

           ADDD    .D1     D6,0xfffffffc,D0  ; [A_D1] |358| <0,8>  ^ 
||         ADDD    .D2     D7,0xfffffffc,D1  ; [A_D2] |358| <0,8>  ^ 
||         MV      .L1     A6,D3             ; [A_L1] 
||         MV      .L2     B2,BL2            ; [B_L2] 
||         UNPROT          0x1               ; [A_U] 

;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*
;*      Loop found in file               : src/tidl_detectionOutput_score.c
;*      Loop source line                 : 350
;*      Loop opening brace source line   : 351
;*      Loop closing brace source line   : 362
;*      Known Minimum Trip Count         : 1                    
;*      Known Max Trip Count Factor      : 1
;*      Loop Carried Dependency Bound(^) : 25
;*      Unpartitioned Resource Bound     : 6
;*      Partitioned Resource Bound       : 6 (pre-sched)
;*
;*      Searching for software pipeline schedule at ...
;*         ii = 25 Unsafe schedule for irregular loop
;*         ii = 25 Unsafe schedule for irregular loop
;*         ii = 25 Did not find schedule
;*         ii = 26 Unsafe schedule for irregular loop
;*         ii = 26 Unsafe schedule for irregular loop
;*         ii = 26 Unsafe schedule for irregular loop
;*         ii = 26 Did not find schedule
;*         ii = 27 Unsafe schedule for irregular loop
;*         ii = 27 Unsafe schedule for irregular loop
;*         ii = 27 Unsafe schedule for irregular loop
;*         ii = 27 Did not find schedule
;*         ii = 28 Unsafe schedule for irregular loop
;*         ii = 28 Unsafe schedule for irregular loop
;*         ii = 28 Unsafe schedule for irregular loop
;*         ii = 28 Did not find schedule
;*         ii = 29 Unsafe schedule for irregular loop
;*         ii = 29 Unsafe schedule for irregular loop
;*         ii = 29 Unsafe schedule for irregular loop
;*         ii = 29 Did not find schedule
;*         ii = 30 Unsafe schedule for irregular loop
;*         ii = 30 Schedule found with 2 iterations in parallel
;*
;*      Partitioned Resource Bound(*)    : 9 (post-sched)
;*
;*      Constant Extension #0 Used [C0]  : 0
;*      Constant Extension #1 Used [C1]  : 5
;*
;*      Resource Partition (may include "post-sched" split/spill moves):
;*
;*                                                A-side   B-side
;*      .L units                                     0        0     
;*      .S units                                     0        0     
;*      .M units                                     0        0     
;*      .N units                                     0        0     
;*      .D units                                    12        -     
;*      .B units                                     1        -     
;*      .C units                                     -        0     
;*      .P units                                     -        0     
;*
;*      .M/.N units                                  0        0     
;*      .L/.S units                                  1        0     
;*      .L/.S/.C units                               0        0     
;*      .L/.S/.C/.M units                            0        0     
;*      .L/.S/.C/.M/.D units                         8        2     
;*
;*      .X cross paths                               2        0     
;*
;*      Bound(.D1 .D2 .D)                            6        -     
;*      Bound(.M .N .MN)                             0        0     
;*      Bound(.L .S .LS)                             1        0     
;*      Bound(.L .S .C .LS .LSC)                     1        0     
;*      Bound(.L .S .C .M .LS .LSC .LSCM)            1        0     
;*      Bound(.L .S .C .M .D .LS .LSC .LSCM .LSCMD)  5        1     
;*
;*      Register Usage Tables:
;*
;*      +----------------------------------+----------------------------------+
;*      |              ASIDE               |              BSIDE               |
;*      +----------------+--------+--------+----------------+--------+--------+
;*      |      "Axx"     |  "ALx" |  "AMx" |     "VBxx"     | "VBLx" | "VBMx" |
;*      +----------------|--------|--------|----------------|--------|--------+
;*      |0000000000111111|00000000|00000000|0000000000111111|00000000|00000000|
;*      |0123456789012345|01234567|01234567|0123456789012345|01234567|01234567|
;*      +----------------|--------|--------|----------------|--------|--------+
;*   0: | *      *       |        |        | *              |  *     |        |
;*   1: | *      *       |        |        | *              |  *     |        |
;*   2: | *      *       |        |        | *              |  *     |        |
;*   3: | *      *       |        |        | *              |  *     |        |
;*   4: | *      *       |        |        | *              |  *     |        |
;*   5: | *      *       |        |        | *              |  *     |        |
;*   6: | *      *       |        |        | *              |***     |        |
;*   7: | *      *       |        |        |**              |  *     |        |
;*   8: | *      *       |        |        | *              |  *     |        |
;*   9: | *      *       |        |        | *              |  *     |        |
;*  10: | *      *       |        |        | *              |  *     |        |
;*  11: | *      *       |        |        | *              |  *     |        |
;*  12: | *      *       |        |        | *              |  *     |        |
;*  13: | *      *       |        |        | *              |  *     |        |
;*  14: | *      *       |        |        | *              |  *     |        |
;*  15: | *      *       |        |        | *              |  *     |        |
;*  16: | *      *       |        |        | *              |  *     |        |
;*  17: | *      *       |        |        | *              |  *     |        |
;*  18: | *      *       |        |        | *              |  *     |        |
;*  19: | *      *       |        |        | *              |  *     |        |
;*  20: | *      *       |        |        | *              |  *     |        |
;*  21: | *      *       |        |        | *              |  *     |        |
;*  22: | *      *       |        |        | *              |  *     |        |
;*  23: | *      *       |        |        | *              |  *     |        |
;*  24: | *      *       |        |        | *              |  *     |        |
;*  25: | *      *       |        |        | *              |  *     |        |
;*  26: | *      *       |        |        | *              |  *     |        |
;*  27: | *      *       |        |        | *              |  *     |        |
;*  28: | *      *       |*       |        | *              |  *     |        |
;*  29: |**      *       |        |        | *              |* *     |        |
;*      +----------------+--------+--------+----------------+--------+--------+
;*
;*      +----------------+                 +--------+       +-------+
;*      |      "Dxx"     |                 |  "Px"  |       |"CUCRx"|
;*      +----------------+                 +--------+       +-------+
;*      |0000000000111111|                 |00000000|       |0000   |
;*      |0123456789012345|                 |01234567|       |0123   |
;*      +----------------+                 +--------+       +-------+
;*   0: |*****           |                 |        |       |       |
;*   1: |*****           |                 |        |       |       |
;*   2: |*****           |                 |        |       |       |
;*   3: |*****           |                 |        |       |       |
;*   4: |*****           |                 |        |       |       |
;*   5: |*****           |                 |        |       |       |
;*   6: |*****           |                 |        |       |       |
;*   7: |*****           |                 |        |       |       |
;*   8: |*****           |                 |        |       |       |
;*   9: |*****           |                 |        |       |       |
;*  10: |*****           |                 |        |       |       |
;*  11: |*****           |                 |        |       |       |
;*  12: |*****           |                 |        |       |       |
;*  13: |*****           |                 |        |       |       |
;*  14: |******          |                 |        |       |       |
;*  15: |******          |                 |        |       |       |
;*  16: |*******         |                 |        |       |       |
;*  17: |*****           |                 |        |       |       |
;*  18: |*****           |                 |        |       |       |
;*  19: |*****           |                 |        |       |       |
;*  20: |*****           |                 |        |       |       |
;*  21: |*****           |                 |        |       |       |
;*  22: |*****           |                 |        |       |       |
;*  23: |******          |                 |        |       |       |
;*  24: |*****           |                 |        |       |       |
;*  25: |*****           |                 |        |       |       |
;*  26: |*****           |                 |        |       |       |
;*  27: |***** *         |                 |        |       |       |
;*  28: |***** *         |                 |        |       |       |
;*  29: |***** **        |                 |        |       |       |
;*      +----------------+                 +--------+       +-------+
;*
;*      Done
;*
;*      Collapsed epilog stages       : 1
;*      Prolog not removed
;*      Collapsed prolog stages       : 0
;*
;*      Max amt of load speculation   : 0 bytes
;*
;*      Minimum safe trip count       : 1
;*      Min. prof. trip count  (est.) : 2
;*
;*      Mem bank conflicts/iter(est.) : { min 0.000, est 0.000, max 0.000 }
;*      Mem bank perf. penalty (est.) : 0.0%
;*
;*
;*      Total cycles (est.)         : 9 + trip_cnt * 30        
;*----------------------------------------------------------------------------*
;*        SINGLE SCHEDULED ITERATION
;*
;*        ||$C$C2677||:
;*   0      [ A1]   SLDD    .D1     *D4(1192),D6      ; [A_D1] |358|  ^ [C1]
;*   1      [ A1]   SLDD    .D1     *D4(1200),D2      ; [A_D1] |358|  ^ [C1]
;*   2              NOP     0x5     ; [A_B] 
;*   7      [ A1]   ADDD    .D2     D2,A1,D6          ; [A_D2] |358|  ^ 
;*     ||   [ A1]   ADDD    .D1     A1,D6,D7          ; [A_D1] |358|  ^ 
;*   8      [ A1]   ADDD    .D1     D6,0xfffffffc,D0  ; [A_D1] |358|  ^ 
;*     ||   [ A1]   ADDD    .D2     D7,0xfffffffc,D1  ; [A_D2] |358|  ^ 
;*   9      [ A1]   LDW     .D2     *D0(0),BL0        ; [A_D2] |358|  ^ 
;*     ||   [ A1]   LDW     .D1     *D1(0),BL1        ; [A_D1] |358|  ^ 
;*  10              NOP     0x5     ; [A_B] 
;*  15      [ A1]   ADDW    .L2     BL1,BL0,B0        ; [B_L2] |358|  ^ 
;*  16      [ A1]   STW     .D1X    B0,*D2[A8]        ; [A_D1] |358|  ^ 
;*  17      [ A1]   LDD     .D1     *D4(1192),D5      ; [A_D1] |359|  ^ [C1]
;*  18              NOP     0x1     ; [A_B] 
;*  19      [!A1]   SLDD    .D1     *D4(1200),D6      ; [A_D1] |354| [C1]
;*  20              NOP     0x3     ; [A_B] 
;*  23      [ A1]   ADDD    .D1     A1,D5,D5          ; [A_D1] |359|  ^ 
;*  24      [ A1]   ADDD    .D1     D5,0xfffffffc,D5  ; [A_D1] |359|  ^ 
;*  25      [!A1]   STW     .D2     B1,*D6[A8]        ; [A_D2] |354| 
;*     ||   [ A1]   STW     .D1X    B1,*D5(0)         ; [A_D1] |359|  ^ 
;*  26              LDD     .D1     *D4(1192),D5      ; [A_D1] |361| [C1]
;*  27              NOP     0x2     ; [A_B] 
;*  29              ADDD    .D1     A1,0x4,A1         ; [A_D1] |350|  ^ 
;*  30              NOP     0x1     ; [A_B] 
;*  31              LDW     .D2     *D3(12),AL0       ; [A_D2] |350| 
;*  32              LDW     .D1     *D5[A8],BL0       ; [A_D1] |361| 
;*  33              NOP     0x3     ; [A_B] 
;*  36              ADDW    .D1     A8,0x1,A8         ; [A_D1] |350| 
;*  37              CMPGTW  .L1     AL0,A8,A0         ; [A_L1] |350| 
;*  38              ADDW    .L2     BL0,BL2,BL2       ; [B_L2] |361| 
;*     ||   [ A0]   B       .B1     ||$C$C2677||      ; [A_B] |350| 
;*  39              ; BRANCHCC OCCURS {||$C$C2677||}  ; [] |350| 
;*----------------------------------------------------------------------------*
||$C$L102||:    ; PIPED LOOP PROLOG
;** --------------------------------------------------------------------------*
||$C$L103||:    ; PIPED LOOP KERNEL
;          EXCLUSIVE CPU CYCLES: 30

   [ A1]   LDW     .D2     *D0(0),BL0        ; [A_D2] |358| <0,9>  ^ 
|| [ A1]   LDW     .D1     *D1(0),BL1        ; [A_D1] |358| <0,9>  ^ 

           NOP             0x5               ; [A_B] 
   [ A1]   ADDW    .L2     BL1,BL0,B0        ; [B_L2] |358| <0,15>  ^ 
   [ A1]   STW     .D1X    B0,*D2[A8]        ; [A_D1] |358| <0,16>  ^ 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 359,column 9,is_stmt,isa 0
   [ A1]   LDD     .D1     *D4(1192),D5      ; [A_D1] |359| <0,17>  ^ [C1]
           NOP             0x1               ; [A_B] 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 354,column 9,is_stmt,isa 0
   [!A1]   SLDD    .D1     *D4(1200),D6      ; [A_D1] |354| <0,19> [C1]
           NOP             0x3               ; [A_B] 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 359,column 9,is_stmt,isa 0
   [ A1]   ADDD    .D1     A1,D5,D5          ; [A_D1] |359| <0,23>  ^ 
   [ A1]   ADDD    .D1     D5,0xfffffffc,D5  ; [A_D1] |359| <0,24>  ^ 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 354,column 9,is_stmt,isa 0

   [!A1]   STW     .D2     B1,*D6[A8]        ; [A_D2] |354| <0,25> 
|| [ A1]   STW     .D1X    B1,*D5(0)         ; [A_D1] |359| <0,25>  ^ 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 361,column 7,is_stmt,isa 0
           LDD     .D1     *D4(1192),D5      ; [A_D1] |361| <0,26> [C1]
           NOP             0x2               ; [A_B] 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 350,column 25,is_stmt,isa 0
           ADDD    .D1     A1,0x4,A1         ; [A_D1] |350| <0,29>  ^ 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 358,column 9,is_stmt,isa 0
   [ A1]   SLDD    .D1     *D4(1192),D6      ; [A_D1] |358| <1,0>  ^ [C1]
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 350,column 25,is_stmt,isa 0

           LDW     .D2     *D3(12),AL0       ; [A_D2] |350| <0,31> 
|| [ A1]   SLDD    .D1     *D4(1200),D2      ; [A_D1] |358| <1,1>  ^ [C1]

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 361,column 7,is_stmt,isa 0
           LDW     .D1     *D5[A8],BL0       ; [A_D1] |361| <0,32> 
           NOP             0x3               ; [A_B] 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 350,column 25,is_stmt,isa 0
           ADDW    .D1     A8,0x1,A8         ; [A_D1] |350| <0,36> 

           CMPGTW  .L1     AL0,A8,A0         ; [A_L1] |350| <0,37> 
|| [ A1]   ADDD    .D2     D2,A1,D6          ; [A_D2] |358| <1,7>  ^ 
|| [ A1]   ADDD    .D1     A1,D6,D7          ; [A_D1] |358| <1,7>  ^ 

   [ A0]   B       .B1     ||$C$L103||       ; [A_B] |350| <0,38> 
||         ADDW    .L2     BL0,BL2,BL2       ; [B_L2] |361| <0,38> 
|| [ A1]   ADDD    .D2     D7,0xfffffffc,D1  ; [A_D2] |358| <1,8>  ^ 
|| [ A1]   ADDD    .D1     D6,0xfffffffc,D0  ; [A_D1] |358| <1,8>  ^ 

;** --------------------------------------------------------------------------*
||$C$L104||:    ; PIPED LOOP EPILOG
;          EXCLUSIVE CPU CYCLES: 1
           PROTCLR                            ; [A_U] VBL2
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 2

           MV      .D1     D5,A0             ; [A_D1] 
||         MV      .L2     BL2,B2            ; [B_L2] 

           B       .B1     ||$C$L106||       ; [A_B] 
||         MV      .L2     B2,B0             ; [B_L2] 
||         MV      .D1     D3,A6             ; [A_D1] 
||         MV      .D2     D4,A5             ; [A_D2] 
||         MV      .L1     AL0,A9            ; [A_L1] 

           ; BRANCH OCCURS {||$C$L106||}     ; [] 
;** --------------------------------------------------------------------------*
||$C$L105||:    
;          EXCLUSIVE CPU CYCLES: 6
           LDD     .D1     *A5(1192),A0      ; [A_D1] 
;** --------------------------------------------------------------------------*
||$C$L106||:    
;          EXCLUSIVE CPU CYCLES: 3
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 364,column 5,is_stmt,isa 0
           ADDAW   .D1     A0,A9,D0          ; [A_D1] |364| 

           ADDD    .D1     D0,0xfffffffc,D0  ; [A_D1] |364| 
||         CMPGTW  .L1X    B0,0,A0           ; [A_L1] |366| 

   [!A0]   B       .B1     ||$C$L110||       ; [A_B] |366| 
||         STW     .D1X    B1,*D0(0)         ; [A_D1] |364| 
||         MVKU32  .L2     0xff,B3           ; [B_L2] 
||         MVKU32  .S2     0,B2              ; [B_S2] 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 366,column 25,is_stmt,isa 0
           ; BRANCHCC OCCURS {||$C$L110||}   ; [] |366| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 4
           EXT     .L2     B0,0x20,0x20,B0   ; [B_L2] 
           MV      .L1X    B3,D5             ; [A_L1] 

           NLCINIT .S1X    B0,0x1,0          ; [A_S1] 
||         UNPROT          0x1               ; [A_U] 

           TICK                               ; [A_U] <0,0> 
||         MV      .L1X    B2,D4             ; [A_L1] 
||         MV      .D1     A5,D2             ; [A_D1] 

;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*
;*      Loop found in file               : src/tidl_detectionOutput_score.c
;*      Loop source line                 : 366
;*      Loop opening brace source line   : 367
;*      Loop closing brace source line   : 373
;*      Known Minimum Trip Count         : 1                    
;*      Known Max Trip Count Factor      : 1
;*      Loop Carried Dependency Bound(^) : 38
;*      Unpartitioned Resource Bound     : 6
;*      Partitioned Resource Bound       : 6 (pre-sched)
;*
;*      Searching for software pipeline schedule at ...
;*         ii = 38 Schedule found with 2 iterations in parallel
;*
;*      Partitioned Resource Bound(*)    : 8 (post-sched)
;*
;*      Constant Extension #0 Used [C0]  : 1
;*      Constant Extension #1 Used [C1]  : 4
;*
;*      Resource Partition (may include "post-sched" split/spill moves):
;*
;*                                                A-side   B-side
;*      .L units                                     0        0     
;*      .S units                                     0        0     
;*      .M units                                     0        0     
;*      .N units                                     0        0     
;*      .D units                                    12        -     
;*      .B units                                     1        -     
;*      .C units                                     -        0     
;*      .P units                                     -        0     
;*
;*      .M/.N units                                  0        0     
;*      .L/.S units                                  1        0     
;*      .L/.S/.C units                               0        0     
;*      .L/.S/.C/.M units                            0        0     
;*      .L/.S/.C/.M/.D units                         4        1     
;*
;*      .X cross paths                               1        0     
;*
;*      Bound(.D1 .D2 .D)                            6        -     
;*      Bound(.M .N .MN)                             0        0     
;*      Bound(.L .S .LS)                             1        0     
;*      Bound(.L .S .C .LS .LSC)                     1        0     
;*      Bound(.L .S .C .M .LS .LSC .LSCM)            1        0     
;*      Bound(.L .S .C .M .D .LS .LSC .LSCM .LSCMD)  4        1     
;*
;*      Register Usage Tables:
;*
;*      +----------------------------------+----------------------------------+
;*      |              ASIDE               |              BSIDE               |
;*      +----------------+--------+--------+----------------+--------+--------+
;*      |      "Axx"     |  "ALx" |  "AMx" |     "VBxx"     | "VBLx" | "VBMx" |
;*      +----------------|--------|--------|----------------|--------|--------+
;*      |0000000000111111|00000000|00000000|0000000000111111|00000000|00000000|
;*      |0123456789012345|01234567|01234567|0123456789012345|01234567|01234567|
;*      +----------------|--------|--------|----------------|--------|--------+
;*   0: |                |        |        |                |        |        |
;*   1: |                |        |        |                |        |        |
;*   2: |                |        |        |                |        |        |
;*   3: |                |        |        |                |        |        |
;*   4: |                |        |        |                |        |        |
;*   5: |                |        |        |                |        |        |
;*   6: |                |        |        |                |        |        |
;*   7: |                |        |        |                |        |        |
;*   8: |                |        |        |                |        |        |
;*   9: |                |        |        |                |        |        |
;*  10: |                |        |        |                |        |        |
;*  11: |                |        |        |                |        |        |
;*  12: |                |        |        |                |        |        |
;*  13: |*               |        |        |                |        |        |
;*  14: |*               |        |        |                |        |        |
;*  15: |*               |        |        |                |        |        |
;*  16: |*               |        |        |                |        |        |
;*  17: |*               |        |        |                |        |        |
;*  18: |*               |        |        |                |        |        |
;*  19: |*               |        |        |                |        |        |
;*  20: |*               |        |        |                |        |        |
;*  21: |*               |        |        |                |        |        |
;*  22: |*               |        |        |                |        |        |
;*  23: |                |        |        |                |        |        |
;*  24: |                |        |        |                |        |        |
;*  25: |                |        |        |                |        |        |
;*  26: |                |        |        |                |        |        |
;*  27: |                |        |        |                |        |        |
;*  28: |                |        |        |                |        |        |
;*  29: |                |        |        |                |        |        |
;*  30: |                |        |        |                |        |        |
;*  31: |                |        |        |                |        |        |
;*  32: |                |        |        |                |        |        |
;*  33: |                |        |        |                |        |        |
;*  34: |                |        |        |                |        |        |
;*  35: |                |        |        |                |        |        |
;*  36: |                |        |        |                |*       |        |
;*  37: |                |        |        |*               |        |        |
;*      +----------------+--------+--------+----------------+--------+--------+
;*
;*      +----------------+                 +--------+       +-------+
;*      |      "Dxx"     |                 |  "Px"  |       |"CUCRx"|
;*      +----------------+                 +--------+       +-------+
;*      |0000000000111111|                 |00000000|       |0000   |
;*      |0123456789012345|                 |01234567|       |0123   |
;*      +----------------+                 +--------+       +-------+
;*   0: |  * **          |                 |        |       |       |
;*   1: |  * **          |                 |        |       |       |
;*   2: |  * **          |                 |        |       |       |
;*   3: |  * **          |                 |        |       |       |
;*   4: |  * **          |                 |        |       |       |
;*   5: |  * **          |                 |        |       |       |
;*   6: |* * **          |                 |        |       |       |
;*   7: |* * **          |                 |        |       |       |
;*   8: |  * **          |                 |        |       |       |
;*   9: |  * **          |                 |        |       |       |
;*  10: |  * **          |                 |        |       |       |
;*  11: |  * **          |                 |        |       |       |
;*  12: |  * **          |                 |        |       |       |
;*  13: |  * **          |                 |        |       |       |
;*  14: |* * **          |                 |        |       |       |
;*  15: |******          |                 |        |       |       |
;*  16: | ** **          |                 |        |       |       |
;*  17: | ** **          |                 |        |       |       |
;*  18: | ** **          |                 |        |       |       |
;*  19: | ** **          |                 |        |       |       |
;*  20: | ** **          |                 |        |       |       |
;*  21: |******          |                 |        |       |       |
;*  22: |******          |                 |        |       |       |
;*  23: | ** **          |                 |        |       |       |
;*  24: | ** **          |                 |        |       |       |
;*  25: | ** **          |                 |        |       |       |
;*  26: | ** **          |                 |        |       |       |
;*  27: | ** **          |                 |        |       |       |
;*  28: | ** **          |                 |        |       |       |
;*  29: |*** **          |                 |        |       |       |
;*  30: |* * **          |                 |        |       |       |
;*  31: |* * **          |                 |        |       |       |
;*  32: |* * **          |                 |        |       |       |
;*  33: |* * **          |                 |        |       |       |
;*  34: |* * **          |                 |        |       |       |
;*  35: |* * **          |                 |        |       |       |
;*  36: |* * **          |                 |        |       |       |
;*  37: |* * **          |                 |        |       |       |
;*      +----------------+                 +--------+       +-------+
;*
;*      Done
;*
;*      Collapsed epilog stages       : 1
;*      Prolog not removed
;*      Collapsed prolog stages       : 0
;*
;*      Max amt of load speculation   : 0 bytes
;*
;*      Minimum safe trip count       : 1
;*      Min. prof. trip count  (est.) : 2
;*
;*      Mem bank conflicts/iter(est.) : { min 0.000, est 0.000, max 0.000 }
;*      Mem bank perf. penalty (est.) : 0.0%
;*
;*
;*      Total cycles (est.)         : 1 + trip_cnt * 38        
;*----------------------------------------------------------------------------*
;*        SINGLE SCHEDULED ITERATION
;*
;*        ||$C$C2583||:
;*   0              TICK                               ; [A_U] 
;*   1              LDD     .D1     *D2(1176),D0      ; [A_D1] |368|  ^ [C1]
;*   2              NOP     0x5     ; [A_B] 
;*   7              ADDD    .D1     D4,D0,D0          ; [A_D1] |368|  ^ 
;*   8              LDW     .D1     *D0(0),A0         ; [A_D1] |368|  ^ 
;*   9              NOP     0x1     ; [A_B] 
;*  10              LDD     .D2     *D2(1200),D0      ; [A_D2] |371| [C0]
;*     ||           LDD     .D1     *D2(1192),D3      ; [A_D1] |371| [C1]
;*  11              NOP     0x3     ; [A_B] 
;*  14              SHRW    .L1     A0,0x10,D0        ; [A_L1] |371|  ^ 
;*  15              ANDW    .D1     D5,D0,D1          ; [A_D1] |371|  ^ 
;*  16              LDW     .D2     *D0[D1],D0        ; [A_D2] |371|  ^ 
;*     ||           LDW     .D1     *D3[D1],D3        ; [A_D1] |371|  ^ 
;*  17              LDD     .D1     *D2(1184),D3      ; [A_D1] |371| [C1]
;*  18              NOP     0x4     ; [A_B] 
;*  22              ADDW    .D1     D3,D0,D0          ; [A_D1] |371|  ^ 
;*  23              STW     .D1     A0,*D3[D0]        ; [A_D1] |371|  ^ 
;*  24              LDD     .D1     *D2(1192),D0      ; [A_D1] |372|  ^ [C1]
;*  25              NOP     0x5     ; [A_B] 
;*  30              ADDAW   .D1     D0,D1,D0          ; [A_D1] |372|  ^ 
;*  31              LDW     .D1     *D0(0),BL0        ; [A_D1] |372|  ^ 
;*  32              NOP     0x5     ; [A_B] 
;*  37              ADDW    .L2     BL0,0x1,B0        ; [B_L2] |372|  ^ 
;*  38              STW     .D1X    B0,*D0(0)         ; [A_D1] |372|  ^ 
;*     ||           ADDD    .D2     D4,0x4,D4         ; [A_D2] |366| 
;*     ||           BNL     .B1     ||$C$C2583||      ; [A_B] |366| 
;*  39              ; BRANCHCC OCCURS {||$C$C2583||}  ; [] |366| 
;*----------------------------------------------------------------------------*
||$C$L107||:    ; PIPED LOOP PROLOG
;** --------------------------------------------------------------------------*
||$C$L108||:    ; PIPED LOOP KERNEL
;          EXCLUSIVE CPU CYCLES: 38
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 368,column 24,is_stmt,isa 0
           LDD     .D1     *D2(1176),D0      ; [A_D1] |368| <0,1>  ^ [C1]
           NOP             0x5               ; [A_B] 
           ADDD    .D1     D4,D0,D0          ; [A_D1] |368| <0,7>  ^ 
           LDW     .D1     *D0(0),A0         ; [A_D1] |368| <0,8>  ^ 
           NOP             0x1               ; [A_B] 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 371,column 7,is_stmt,isa 0

           LDD     .D2     *D2(1200),D0      ; [A_D2] |371| <0,10> [C0]
||         LDD     .D1     *D2(1192),D3      ; [A_D1] |371| <0,10> [C1]

           NOP             0x3               ; [A_B] 
           SHRW    .L1     A0,0x10,D0        ; [A_L1] |371| <0,14>  ^ 
           ANDW    .D1     D5,D0,D1          ; [A_D1] |371| <0,15>  ^ 

           LDW     .D2     *D0[D1],D0        ; [A_D2] |371| <0,16>  ^ 
||         LDW     .D1     *D3[D1],D3        ; [A_D1] |371| <0,16>  ^ 

           LDD     .D1     *D2(1184),D3      ; [A_D1] |371| <0,17> [C1]
           NOP             0x4               ; [A_B] 
           ADDW    .D1     D3,D0,D0          ; [A_D1] |371| <0,22>  ^ 
           STW     .D1     A0,*D3[D0]        ; [A_D1] |371| <0,23>  ^ 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 372,column 7,is_stmt,isa 0
           LDD     .D1     *D2(1192),D0      ; [A_D1] |372| <0,24>  ^ [C1]
           NOP             0x5               ; [A_B] 
           ADDAW   .D1     D0,D1,D0          ; [A_D1] |372| <0,30>  ^ 
           LDW     .D1     *D0(0),BL0        ; [A_D1] |372| <0,31>  ^ 
           NOP             0x5               ; [A_B] 
           ADDW    .L2     BL0,0x1,B0        ; [B_L2] |372| <0,37>  ^ 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 366,column 25,is_stmt,isa 0

           ADDD    .D2     D4,0x4,D4         ; [A_D2] |366| <0,38> 
||         BNL     .B1     ||$C$L108||       ; [A_B] |366| <0,38> 
||         STW     .D1X    B0,*D0(0)         ; [A_D1] |372| <0,38>  ^ 
||         TICK                               ; [A_U] <1,0> 

;** --------------------------------------------------------------------------*
||$C$L109||:    ; PIPED LOOP EPILOG
;          EXCLUSIVE CPU CYCLES: 1
           PROTCLR                            ; [A_U] 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 1
           MV      .D1     D2,A5             ; [A_D1] 
;** --------------------------------------------------------------------------*
||$C$L110||:    
;          EXCLUSIVE CPU CYCLES: 8
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 377,column 3,is_stmt,isa 0
           LDW     .D1     *A6(68),AL0       ; [A_D1] |377| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 374,column 5,is_stmt,isa 0

           LDW     .D1     *A6(12),B2        ; [A_D1] |374| 
||         LDW     .D2     *A5(1328),BL0     ; [A_D2] |374| 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 377,column 3,is_stmt,isa 0
           CMPEQW  .L1     AL0,0x1,A1        ; [A_L1] |377| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 374,column 5,is_stmt,isa 0

   [ A1]   B       .B1     ||$C$L111||       ; [A_B] |377| 
||         SUBW    .L2     B2,BL0,B0         ; [B_L2] |374| 
||         CMPEQW  .L1     AL0,0x2,A0        ; [A_L1] |466| 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 377,column 3,is_stmt,isa 0
           ; BRANCHCC OCCURS {||$C$L111||}   ; [] |377| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 1
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 466,column 8,is_stmt,isa 0

   [ A0]   B       .B1     ||$C$L126||       ; [A_B] |466| 
||         MVKU32  .L1     0,A4              ; [A_L1] |580| 

           ; BRANCHCC OCCURS {||$C$L126||}   ; [] |466| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 1
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 580,column 3,is_stmt,isa 0
           B       .B1     ||$C$L150||       ; [A_B] |580| 
           ; BRANCH OCCURS {||$C$L150||}     ; [] |580| 
;** --------------------------------------------------------------------------*
||$C$L111||:    
;          EXCLUSIVE CPU CYCLES: 8
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 379,column 5,is_stmt,isa 0
           LDW     .D1     *A6(72),AL0       ; [A_D1] |379| 

           CMPEQW  .L1     AL0,0x4,A1        ; [A_L1] |379| 
||         CMPGTW  .S1X    B0,0,A0           ; [A_S1] |381| 

   [!A1]   B       .B1     ||$C$L121||       ; [A_B] |379| 
||         CMPGTW  .L1X    B0,0,A2           ; [A_L1] |413| 

           ; BRANCHCC OCCURS {||$C$L121||}   ; [] |379| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 451,column 27,is_stmt,isa 0

           CMPGTW  .L1X    B2,0,A0           ; [A_L1] |451| 
||         MVKU32  .S1     0,D2              ; [A_S1] 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 413,column 29,is_stmt,isa 0

   [!A2]   B       .B1     ||$C$L112||       ; [A_B] |413| 
||         MV      .D1     D2,D1             ; [A_D1] 
||         MV      .S1X    B0,A1             ; [A_S1] 
||         MVKU32  .L1     0,A4              ; [A_L1] 

           ; BRANCHCC OCCURS {||$C$L112||}   ; [] |413| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 8

           MVKU32  .L2     0xff,B14          ; [B_L2] 
||         MVKU32  .S2     0xffff,BL7        ; [B_S2] 

           MVKU32  .L2     0x37800000,B10    ; [B_L2] 
||         MVKU32  .S2     0x3d2aaab4,BM3    ; [B_S2] 

           MVKU32  .L2     0x3e2aaaad,BM1    ; [B_L2] 
||         MVKU32  .S2     0x3f000000,BM0    ; [B_S2] 

           MVKU32  .L2     0x10000,BL0       ; [B_L2] 
||         MVKU32  .S2     0x3f800000,B9     ; [B_S2] 

           MVKU32  .L2     0,B2              ; [B_L2] 
||         MVKU32  .S2     0x3f317218,BM2    ; [B_S2] 

           MVKU32  .L2     0x7f7fffff,B0     ; [B_L2] 
||         MVKU32  .S2     0x3fb8aa3b,BM4    ; [B_S2] 

	.dwpsn	file "./inc/tidl_detectionOutput_int.h",line 229,column 1,is_stmt,isa 0

           MVKU32  .L2     0x40000000,B8     ; [B_L2] 
||         MVKU32  .S2     0x46fffe00,B13    ; [B_S2] 
||         MASKB   .P2     0x4,P2            ; [B_P] |229| 

	.dwpsn	file "./inc/tidl_detectionOutput_int.h",line 134,column 25,is_stmt,isa 0

           B       .B1     ||$C$L113||       ; [A_B] |413| 
||         MVKU32  .L1     0xc,AM0           ; [A_L1] |134| 
||         MVKU32  .L2     0,BL5             ; [B_L2] |229| 
||         MVKU32  .S2     0,BL1             ; [B_S2] |229| 
||         MASKB   .P2     0x4,P1            ; [B_P] |229| 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 413,column 29,is_stmt,isa 0
           ; BRANCH OCCURS {||$C$L113||}     ; [] |413| 
;** --------------------------------------------------------------------------*
||$C$L112||:    
;          EXCLUSIVE CPU CYCLES: 1
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 451,column 27,is_stmt,isa 0
   [ A0]   B       .B1     ||$C$L116||       ; [A_B] |451| 
           ; BRANCHCC OCCURS {||$C$L116||}   ; [] |451| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 1
           B       .B1     ||$C$L120||       ; [A_B] |451| 
           ; BRANCH OCCURS {||$C$L120||}     ; [] |451| 
;** --------------------------------------------------------------------------*
;**   BEGIN LOOP ||$C$L113||
;** --------------------------------------------------------------------------*
||$C$L113||:    
;          EXCLUSIVE CPU CYCLES: 15
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 417,column 31,is_stmt,isa 0
           LDD     .D1     *A5(1192),D3      ; [A_D1] |417| 
           ADDD    .D1     D2,D3,D3          ; [A_D1] |417| 
           LDW     .D1     *D3(0),A0         ; [A_D1] |417| 
           CMPGTW  .L1     A0,0,A2           ; [A_L1] |417| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 416,column 9,is_stmt,isa 0

   [!A2]   B       .B1     ||$C$L115||       ; [A_B] |417| 
||         STW     .D1X    B1,*D3(0)         ; [A_D1] |416| 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 417,column 31,is_stmt,isa 0
           ; BRANCHCC OCCURS {||$C$L115||}   ; [] |417| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 1
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 417,column 26,is_stmt,isa 0

           ADDD    .D1     A5,0x140,D3       ; [A_D1] 
||         ADDD    .D2     A5,0x244,D4       ; [A_D2] 
||         MVKU32  .L1     0,A7              ; [A_L1] |417| 

;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*
;*      Loop found in file               : src/tidl_detectionOutput_score.c
;*      Loop source line                 : 417
;*      Loop opening brace source line   : 418
;*      Loop closing brace source line   : 449
;*      Known Minimum Trip Count         : 1                    
;*      Known Max Trip Count Factor      : 1
;*      Loop Carried Dependency Bound(^) : 165
;*      Unpartitioned Resource Bound     : 12
;*      Partitioned Resource Bound       : 16 (pre-sched)
;*      Disqualified loop: Loop carried dependency bound too large
;*----------------------------------------------------------------------------*
||$C$L114||:    
;          EXCLUSIVE CPU CYCLES: 166
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 419,column 11,is_stmt,isa 0
           LDD     .D1     *A5(1200),D0      ; [A_D1] |419| 
           ADDD    .D1     D2,D0,D0          ; [A_D1] |419| 
           LDW     .D1     *D0(0),D0         ; [A_D1] |419| 
           LDD     .D1     *A5(1184),D5      ; [A_D1] |419| 
           ADDW    .D1     A7,D0,D0          ; [A_D1] |419| 
           LDW     .D1     *D5[D0],B3        ; [A_D1] |419| 
	.dwpsn	file "./inc/tidl_detectionOutput_int.h",line 134,column 25,is_stmt,isa 0
           SHRW    .L2     B3,0x1c,BL2       ; [B_L2] |134| 

           ANDW    .L2     BL2,0xf,B4        ; [B_L2] |134| 
||         LDW     .D1     *A6(12),B12       ; [A_D1] |134| 

           EXT     .L1X    B4,0x20,0x20,A2   ; [A_L1] |134| 
           MPYDD   .N1     AM0,A2,D0         ; [A_N1] |134| 
           SHRW    .L2     B3,0x18,BL3       ; [B_L2] |134| 

           ADDW    .L2     B12,0x5,B4        ; [B_L2] |134| 
||         ANDW    .S2     BL3,0xf,B5        ; [B_S2] |134| 

           MPYWW   .N2     B5,B4,BL2         ; [B_N2] |134| 
||         ADDD    .D1     D4,D0,D0          ; [A_D1] |134| 

           LDW     .D1     *D0(0),AM1        ; [A_D1] |134| 
           SHRW    .L2     B3,0x10,BL6       ; [B_L2] |134| 
           ANDW    .L2     B14,BL6,BL3       ; [B_L2] |134| 
           ADDW    .L2     BL3,BL2,B6        ; [B_L2] |134| 
           SHLD    .L1     A2,0x3,D14        ; [A_L1] |134| 

           ADDW    .L1X    B6,0x1,AM3        ; [A_L1] |134| 
||         ADDD    .D1     A5,D14,D0         ; [A_D1] |134| 

           LDD     .D1     *D0(192),D0       ; [A_D1] |134| 
||         MPYWW   .N1X    B5,AM1,AM2        ; [A_N1] |134| 

           MPYWW   .N1     AM1,AM3,AM1       ; [A_N1] |134| 
           ANDW    .L2     BL7,B3,B11        ; [B_L2] |134| 
           MPYWW   .N1X    B4,AM2,AM2        ; [A_N1] |134| 
           ADDW    .M1X    B11,AM1,D5        ; [A_M1] |134| 
           LDB     .D1     *D0[D5],BL2       ; [A_D1] |134| 
           SHRW    .L1X    B3,0x1c,AL0       ; [A_L1] |134| 

           ADDW    .M1X    B11,AM2,D5        ; [A_M1] |134| 
||         ANDW    .L1     AL0,0xf,D6        ; [A_L1] |134| 

           LDB     .D1     *D0[D5],BL3       ; [A_D1] |134| 
||         LDUW    .D2     *D3[D6],B7        ; [A_D2] |134| 

           VINTSP  .L2     BL2,BM5           ; [B_L2] |134| 

           MPYSP   .N2     B7,BM5,BL3        ; [B_N2] |134| 
||         VINTSP  .L2     BL3,BM5           ; [B_L2] |134| 

           MPYSP   .N2     B7,BM5,BL2        ; [B_N2] |134| 
           VXORW   .L2     BL3,0x80000000,BM5 ; [B_L2] |134| 
           MPYSP   .N2     BM4,BM5,B7        ; [B_N2] |134| 
           VXORW   .L2     BL2,0x80000000,BM5 ; [B_L2] |134| 
           MPYSP   .N2     BM4,BM5,B6        ; [B_N2] |134| 
	.dwpsn	file "./inc/tidl_detectionOutput_int.h",line 229,column 1,is_stmt,isa 0
           VSPTRUNC .L2    B7,B4             ; [B_L2] |229| 

           VSPTRUNC .L2    B6,B5             ; [B_L2] |229| 
||         VINTSP  .S2     B4,BM5            ; [B_S2] |229| 

           VINTSP  .L2     B5,BM5            ; [B_L2] |229| 
||         SUBSP   .C2     B7,BM5,BM6        ; [B_C] |229| 

           SUBSP   .C2     B6,BM5,BM5        ; [B_C] |229| 
||         MPYSP   .N2     BM2,BM6,BM7       ; [B_N2] |229| 

           MPYSP   .N2     BM2,BM5,B6        ; [B_N2] |229| 
           MPYSP   .N2     BM7,BM7,BM6       ; [B_N2] |229| 

           MPYSP   .N2     B6,B6,BM5         ; [B_N2] |229| 
||         CMPGEW  .L1X    B4,0xfffffff0,A3  ; [A_L1] |229| 

   [ A3]   MPYSP   .N2     BM7,BM6,B11       ; [B_N2] |229| 
   [ A3]   MPYSP   .N2     BM0,BM6,B7        ; [B_N2] |229| 

   [ A3]   ADDSP   .C2     B9,BM7,BM7        ; [B_C] |229| 
||         CMPGEW  .L1X    B5,0xfffffff0,A2  ; [A_L1] |229| 

   [ A2]   MPYSP   .N2     B6,BM5,BM6        ; [B_N2] |229| 
|| [ A3]   MPYSP   .M2     BM6,BM6,B12       ; [B_M2] |229| 

   [ A2]   MPYSP   .N2     BM0,BM5,BL2       ; [B_N2] |229| 
|| [ A3]   MPYSP   .M2     BM1,B11,B11       ; [B_M2] |229| 

   [ A3]   ADDSP   .C2     B7,BM7,BM7        ; [B_C] |229| 
|| [ A2]   ADDSP   .L2     B9,B6,BL3         ; [B_L2] |229| 

   [ A2]   MPYSP   .N2     BM5,BM5,BM5       ; [B_N2] |229| 

   [ A3]   MPYSP   .N2     BM3,B12,BM6       ; [B_N2] |229| 
|| [ A2]   MPYSP   .M2     BM1,BM6,BL4       ; [B_M2] |229| 

   [ A3]   ADDSP   .C2     B11,BM7,BM7       ; [B_C] |229| 
||         SUBRW   .M2     B4,0,BL3          ; [B_M2] |229| 
|| [ A2]   ADDSP   .S2     BL2,BL3,BL2       ; [B_S2] |229| 
||         VCMPGTW .L2     B4,BL5,P0         ; [B_L2] |229| 

   [ A3]   SHRW    .L2     BL0,BL3,BL3       ; [B_L2] |229| 
|| [ A3]   AND     .P2     P0,P2,P0          ; [B_P] |229| 
||         SHLW    .S2     BL0,B4,BL6        ; [B_S2] |229| 

   [ A3]   VSEL    .L2     P0,BL6,BL3,B6     ; [B_L2] |229| 
|| [ A2]   MPYSP   .N2     BM3,BM5,BM5       ; [B_N2] |229| 

           SUBRW   .M2     B5,0,BL2          ; [B_M2] |229| 
|| [ A2]   ADDSP   .S2     BL4,BL2,BM6       ; [B_S2] |229| 
|| [ A3]   ADDSP   .C2     BM6,BM7,B7        ; [B_C] |229| 
||         VCMPGTW .L2     B5,BL1,P0         ; [B_L2] |229| 

   [ A2]   SHRW    .L2     BL0,BL2,BL2       ; [B_L2] |229| 
|| [ A2]   AND     .P2     P0,P1,P0          ; [B_P] |229| 
||         SHLW    .S2     BL0,B5,BL3        ; [B_S2] |229| 

   [ A2]   VSEL    .L2     P0,BL3,BL2,BL2    ; [B_L2] |229| 
|| [ A3]   VINTSP  .L1X    B6,AM1            ; [A_L1] |229| 

   [ A2]   VINTSP  .L2     BL2,BM5           ; [B_L2] |229| 
|| [ A2]   ADDSP   .C2     BM5,BM6,BM6       ; [B_C] |229| 

   [ A3]   MPYSP   .N1X    B7,AM1,AM1        ; [A_N1] |229| 
   [ A2]   MPYSP   .N2     BM6,BM5,BM5       ; [B_N2] |229| 
   [ A3]   MPYSP   .N1X    B10,AM1,D0        ; [A_N1] |229| 

   [ A2]   MPYSP   .N2     B10,BM5,BL2       ; [B_N2] |229| 
||         CMPGTW  .L1X    B4,0xe,A4         ; [A_L1] |229| 

   [ A4]   MV      .L1X    B0,AL0            ; [A_L1] |229| 
   [!A3]   MV      .L1X    B2,D0             ; [A_L1] |229| 

           CMPGTW  .L1X    B5,0xe,A2         ; [A_L1] |229| 
|| [!A4]   MV      .D1     D0,AL0            ; [A_D1] |229| 
|| [!A2]   MV      .L2     B2,BL2            ; [B_L2] |229| 

   [!A2]   MV      .L2     BL2,BM5           ; [B_L2] |229| 
|| [ A2]   MV      .S2     B0,BM5            ; [B_S2] |229| 
||         ADDSP   .L1X    B9,AL0,A3         ; [A_L1] |229| 

           ADDSP   .C2     B9,BM5,B4         ; [B_C] |229| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 441,column 11,is_stmt,isa 0
           VRCPSP  .S1     A3,AM1            ; [A_S1] |441| 
           VRCPSP  .S2     B4,BM5            ; [B_S2] |441| 
           MPYSP   .N1     A3,AM1,AL0        ; [A_N1] |441| 
           MPYSP   .N2     B4,BM5,BM6        ; [B_N2] |441| 
           SUBSP   .L1X    B8,AL0,AM2        ; [A_L1] |441| 
           SUBSP   .C2     B8,BM6,BM6        ; [B_C] |441| 
           MPYSP   .N1     AM1,AM2,AM1       ; [A_N1] |441| 
           MPYSP   .N2     BM5,BM6,BM5       ; [B_N2] |441| 
           MPYSP   .N1     A3,AM1,AL0        ; [A_N1] |441| 
           MPYSP   .N2     B4,BM5,BM6        ; [B_N2] |441| 
           SUBSP   .L1X    B8,AL0,AM2        ; [A_L1] |441| 
           SUBSP   .C2     B8,BM6,BM6        ; [B_C] |441| 
           MPYSP   .N1     AM1,AM2,A2        ; [A_N1] |441| 
           MPYSP   .N2     BM5,BM6,BM5       ; [B_N2] |441| 

           MPYSP   .N2X    A2,BM5,B4         ; [B_N2] |441| 
||         LDUW    .D1     *A6(24),AL1       ; [A_D1] |441| 

           MV      .L1X    B4,AL0            ; [A_L1] |441| Define a twin register
           CMPLESP .L1     AL1,AL0,A2        ; [A_L1] |441| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 444,column 13,is_stmt,isa 0
   [ A2]   MPYSP   .N2     B13,B4,BL3        ; [B_N2] |444| 
   [ A2]   LDD     .D1     *A5(1152),D5      ; [A_D1] |444| 
   [ A2]   VSPTRUNC .L2    BL3,B4            ; [B_L2] |444| 
   [ A2]   STH     .D1X    B4,*D5[D1]        ; [A_D1] |444| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 445,column 13,is_stmt,isa 0
   [ A2]   LDD     .D1     *A5(1176),D5      ; [A_D1] |445| 
   [ A2]   STW     .D1X    B3,*D5[D1]        ; [A_D1] |445| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 446,column 13,is_stmt,isa 0
   [ A2]   LDD     .D1     *A5(1192),D5      ; [A_D1] |446| 
   [ A2]   ADDD    .D1     D2,D5,D5          ; [A_D1] |446| 
   [ A2]   LDW     .D1     *D5(0),BL3        ; [A_D1] |446| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 417,column 31,is_stmt,isa 0

   [ A2]   ADDW    .L2     BL3,0x1,B3        ; [B_L2] |446| 
||         ADDW    .D1     A0,0xffffffff,A0  ; [A_D1] |417| 

   [ A0]   B       .B1     ||$C$L114||       ; [A_B] |417| 
|| [ A2]   STW     .D1X    B3,*D5(0)         ; [A_D1] |446| 
|| [ A2]   ADDW    .D2     D1,0x1,D1         ; [A_D2] |447| 
||         ADDW    .L1     A7,0x1,A7         ; [A_L1] |417| 

           ; BRANCHCC OCCURS {||$C$L114||}   ; [] |417| 
;** --------------------------------------------------------------------------*
||$C$L115||:    
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 413,column 29,is_stmt,isa 0
           ADDW    .D1     A1,0xffffffff,A1  ; [A_D1] |413| 

   [ A1]   B       .B1     ||$C$L113||       ; [A_B] |413| 
||         ADDD    .D1     D2,0x4,D2         ; [A_D1] |413| 

           ; BRANCHCC OCCURS {||$C$L113||}   ; [] |413| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 8
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 451,column 27,is_stmt,isa 0
           LDW     .D1     *A6(12),AL0       ; [A_D1] |451| 
           CMPGTW  .L1     AL0,0,A0          ; [A_L1] |451| 

   [!A0]   B       .B1     ||$C$L120||       ; [A_B] |451| 
||         MV      .D1     D1,A4             ; [A_D1] 

           ; BRANCHCC OCCURS {||$C$L120||}   ; [] |451| 
;** --------------------------------------------------------------------------*
||$C$L116||:    
;          EXCLUSIVE CPU CYCLES: 2
           MV      .D1     A5,D1             ; [A_D1] 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 451,column 22,is_stmt,isa 0

           SLDD    .D1     *D1(1200),D2      ; [A_D1] <0,0>  ^ [C0]
||         UNPROT          0x1               ; [A_U] 
||         MVKU32  .L1     0,A8              ; [A_L1] |451| 
||         MV      .D2     A6,D0             ; [A_D2] 
||         MVKU32  .S1     0,A1              ; [A_S1] 

;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*
;*      Loop found in file               : src/tidl_detectionOutput_score.c
;*      Loop source line                 : 451
;*      Loop opening brace source line   : 452
;*      Loop closing brace source line   : 461
;*      Known Minimum Trip Count         : 1                    
;*      Known Max Trip Count Factor      : 1
;*      Loop Carried Dependency Bound(^) : 16
;*      Unpartitioned Resource Bound     : 4
;*      Partitioned Resource Bound       : 4 (pre-sched)
;*
;*      Searching for software pipeline schedule at ...
;*         ii = 16 Unsafe schedule for irregular loop
;*         ii = 16 Unsafe schedule for irregular loop
;*         ii = 16 Did not find schedule
;*         ii = 17 Schedule found with 2 iterations in parallel
;*
;*      Partitioned Resource Bound(*)    : 6 (post-sched)
;*
;*      Constant Extension #0 Used [C0]  : 1
;*      Constant Extension #1 Used [C1]  : 1
;*
;*      Resource Partition (may include "post-sched" split/spill moves):
;*
;*                                                A-side   B-side
;*      .L units                                     0        0     
;*      .S units                                     0        0     
;*      .M units                                     0        0     
;*      .N units                                     0        0     
;*      .D units                                     7        -     
;*      .B units                                     1        -     
;*      .C units                                     -        0     
;*      .P units                                     -        0     
;*
;*      .M/.N units                                  0        0     
;*      .L/.S units                                  1        0     
;*      .L/.S/.C units                               0        0     
;*      .L/.S/.C/.M units                            0        0     
;*      .L/.S/.C/.M/.D units                         6        1     
;*
;*      .X cross paths                               1        0     
;*
;*      Bound(.D1 .D2 .D)                            4        -     
;*      Bound(.M .N .MN)                             0        0     
;*      Bound(.L .S .LS)                             1        0     
;*      Bound(.L .S .C .LS .LSC)                     1        0     
;*      Bound(.L .S .C .M .LS .LSC .LSCM)            1        0     
;*      Bound(.L .S .C .M .D .LS .LSC .LSCM .LSCMD)  3        1     
;*
;*      Register Usage Tables:
;*
;*      +----------------------------------+----------------------------------+
;*      |              ASIDE               |              BSIDE               |
;*      +----------------+--------+--------+----------------+--------+--------+
;*      |      "Axx"     |  "ALx" |  "AMx" |     "VBxx"     | "VBLx" | "VBMx" |
;*      +----------------|--------|--------|----------------|--------|--------+
;*      |0000000000111111|00000000|00000000|0000000000111111|00000000|00000000|
;*      |0123456789012345|01234567|01234567|0123456789012345|01234567|01234567|
;*      +----------------|--------|--------|----------------|--------|--------+
;*   0: | *      *       |        |        | *              |        |        |
;*   1: | *      *       |        |        | *              |        |        |
;*   2: | *      *       |        |        | *              |        |        |
;*   3: | *      *       |        |        | *              |        |        |
;*   4: | *      *       |        |        | *              |        |        |
;*   5: | *      *       |        |        | *              |        |        |
;*   6: | *      *       |        |        | *              |        |        |
;*   7: | *      *       |        |        | *              |**      |        |
;*   8: | *      *       |        |        |**              |        |        |
;*   9: | *      *       |        |        | *              |        |        |
;*  10: | *      *       |        |        | *              |        |        |
;*  11: | *      *       |        |        | *              |        |        |
;*  12: | *      *       |        |        | *              |        |        |
;*  13: | *      *       |        |        | *              |        |        |
;*  14: | *      *       |        |        | *              |        |        |
;*  15: | *      *       |*       |        | *              |        |        |
;*  16: |**      *       |        |        | *              |        |        |
;*      +----------------+--------+--------+----------------+--------+--------+
;*
;*      +----------------+                 +--------+       +-------+
;*      |      "Dxx"     |                 |  "Px"  |       |"CUCRx"|
;*      +----------------+                 +--------+       +-------+
;*      |0000000000111111|                 |00000000|       |0000   |
;*      |0123456789012345|                 |01234567|       |0123   |
;*      +----------------+                 +--------+       +-------+
;*   0: |*** * *         |                 |        |       |       |
;*   1: |*******         |                 |        |       |       |
;*   2: |*** * *         |                 |        |       |       |
;*   3: |*** * *         |                 |        |       |       |
;*   4: |*** * *         |                 |        |       |       |
;*   5: |*** * *         |                 |        |       |       |
;*   6: |*** * *         |                 |        |       |       |
;*   7: |*** * *         |                 |        |       |       |
;*   8: |*** * *         |                 |        |       |       |
;*   9: |**  * *         |                 |        |       |       |
;*  10: |**  * *         |                 |        |       |       |
;*  11: |**  * *         |                 |        |       |       |
;*  12: |**  * *         |                 |        |       |       |
;*  13: |**  * *         |                 |        |       |       |
;*  14: |**  * *         |                 |        |       |       |
;*  15: |**  * *         |                 |        |       |       |
;*  16: |***** *         |                 |        |       |       |
;*      +----------------+                 +--------+       +-------+
;*
;*      Done
;*
;*      Collapsed epilog stages       : 1
;*      Prolog not removed
;*      Collapsed prolog stages       : 0
;*
;*      Max amt of load speculation   : 0 bytes
;*
;*      Minimum safe trip count       : 1
;*      Min. prof. trip count  (est.) : 2
;*
;*      Mem bank conflicts/iter(est.) : { min 0.000, est 0.000, max 0.000 }
;*      Mem bank perf. penalty (est.) : 0.0%
;*
;*
;*      Total cycles (est.)         : 7 + trip_cnt * 17        
;*----------------------------------------------------------------------------*
;*        SINGLE SCHEDULED ITERATION
;*
;*        ||$C$C2328||:
;*   0              SLDD    .D2     *D1(1200),D2      ; [A_D2]  ^ [C0]
;*     ||   [ A1]   SLDD    .D1     *D1(1192),D3      ; [A_D1] |459|  ^ [C1]
;*   1              NOP     0x5     ; [A_B] 
;*   6      [ A1]   ADDD    .D2     D2,A1,D4          ; [A_D2] |459|  ^ 
;*     ||   [ A1]   ADDD    .D1     A1,D3,D6          ; [A_D1] |459|  ^ 
;*   7      [ A1]   ADDD    .D2     D4,0xfffffffc,D3  ; [A_D2] |459|  ^ 
;*     ||   [ A1]   ADDD    .D1     D6,0xfffffffc,D5  ; [A_D1] |459|  ^ 
;*   8      [ A1]   LDW     .D2     *D3(0),BL0        ; [A_D2] |459|  ^ 
;*     ||   [ A1]   LDW     .D1     *D5(0),BL1        ; [A_D1] |459|  ^ 
;*   9              NOP     0x5     ; [A_B] 
;*  14      [ A1]   ADDW    .L2     BL1,BL0,B0        ; [B_L2] |459|  ^ 
;*  15      [!A1]   STW     .D2     B1,*D2[A8]        ; [A_D2] |455| 
;*     ||   [ A1]   STW     .D1X    B0,*D2[A8]        ; [A_D1] |459|  ^ 
;*  16              ADDD    .D2     A1,0x4,A1         ; [A_D2] |451|  ^ 
;*     ||           LDW     .D1     *D0(12),AL0       ; [A_D1] |451| 
;*  17              NOP     0x4     ; [A_B] 
;*  21              ADDW    .D1     A8,0x1,A8         ; [A_D1] |451| 
;*  22              CMPGTW  .L1     AL0,A8,A0         ; [A_L1] |451| 
;*  23      [ A0]   B       .B1     ||$C$C2328||      ; [A_B] |451| 
;*  24              ; BRANCHCC OCCURS {||$C$C2328||}  ; [] |451| 
;*----------------------------------------------------------------------------*
||$C$L117||:    ; PIPED LOOP PROLOG
;** --------------------------------------------------------------------------*
||$C$L118||:    ; PIPED LOOP KERNEL
;          EXCLUSIVE CPU CYCLES: 17
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 459,column 11,is_stmt,isa 0

   [ A1]   ADDD    .D2     D4,0xfffffffc,D3  ; [A_D2] |459| <0,7>  ^ 
|| [ A1]   ADDD    .D1     D6,0xfffffffc,D5  ; [A_D1] |459| <0,7>  ^ 

   [ A1]   LDW     .D2     *D3(0),BL0        ; [A_D2] |459| <0,8>  ^ 
|| [ A1]   LDW     .D1     *D5(0),BL1        ; [A_D1] |459| <0,8>  ^ 

           NOP             0x5               ; [A_B] 
   [ A1]   ADDW    .L2     BL1,BL0,B0        ; [B_L2] |459| <0,14>  ^ 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 455,column 11,is_stmt,isa 0

   [!A1]   STW     .D2     B1,*D2[A8]        ; [A_D2] |455| <0,15> 
|| [ A1]   STW     .D1X    B0,*D2[A8]        ; [A_D1] |459| <0,15>  ^ 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 451,column 27,is_stmt,isa 0

           ADDD    .D2     A1,0x4,A1         ; [A_D2] |451| <0,16>  ^ 
||         LDW     .D1     *D0(12),AL0       ; [A_D1] |451| <0,16> 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 459,column 11,is_stmt,isa 0

           SLDD    .D2     *D1(1200),D2      ; [A_D2] <1,0>  ^ [C0]
|| [ A1]   SLDD    .D1     *D1(1192),D3      ; [A_D1] |459| <1,0>  ^ [C1]

           NOP             0x3               ; [A_B] 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 451,column 27,is_stmt,isa 0
           ADDW    .D1     A8,0x1,A8         ; [A_D1] |451| <0,21> 
           CMPGTW  .L1     AL0,A8,A0         ; [A_L1] |451| <0,22> 

   [ A0]   B       .B1     ||$C$L118||       ; [A_B] |451| <0,23> 
|| [ A1]   ADDD    .D2     D2,A1,D4          ; [A_D2] |459| <1,6>  ^ 
|| [ A1]   ADDD    .D1     A1,D3,D6          ; [A_D1] |459| <1,6>  ^ 

;** --------------------------------------------------------------------------*
||$C$L119||:    ; PIPED LOOP EPILOG
;          EXCLUSIVE CPU CYCLES: 1
           PROTCLR                            ; [A_U] 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 1
           MV      .D1     D1,A5             ; [A_D1] 
;** --------------------------------------------------------------------------*
||$C$L120||:    
;          EXCLUSIVE CPU CYCLES: 7
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 462,column 7,is_stmt,isa 0

           LDD     .D1     *A5(1176),B1      ; [A_D1] |462| 
||         LDD     .D2     *A5(1152),B0      ; [A_D2] |463| 

           B       .B1     ||$C$L150||       ; [A_B] |580| 
||         STD     .D1X    B1,*A5(1184)      ; [A_D1] |462| 
||         STD     .D2     B0,*A5(1160)      ; [A_D2] |463| 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 580,column 3,is_stmt,isa 0
           ; BRANCH OCCURS {||$C$L150||}     ; [] |580| 
;** --------------------------------------------------------------------------*
||$C$L121||:    
;          EXCLUSIVE CPU CYCLES: 1
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 381,column 29,is_stmt,isa 0

   [!A0]   B       .B1     ||$C$L125||       ; [A_B] |381| 
||         MVKU32  .L1     0,D2              ; [A_L1] 
||         MV      .S1X    B0,A1             ; [A_S1] 

           ; BRANCHCC OCCURS {||$C$L125||}   ; [] |381| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 8
           MVKU32  .L1     0xff,D3           ; [A_L1] 

           MVKU32  .L2     0xffff,BL4        ; [B_L2] 
||         MVKU32  .S2     0x37800000,BM7    ; [B_S2] 

           MVKU32  .L2     0x3d2aaab4,BM6    ; [B_L2] 
||         MVKU32  .S2     0x3e2aaaad,BM4    ; [B_S2] 

           MVKU32  .L2     0x10000,BL0       ; [B_L2] 
||         MVKU32  .S2     0x3f000000,BM3    ; [B_S2] 

           ADDD    .D1     A5,0x140,D1       ; [A_D1] 
||         MVKU32  .L2     0,BL3             ; [B_L2] 
||         MVKU32  .S2     0x3f800000,BM1    ; [B_S2] 

           MVKU32  .L2     0x7f7fffff,BL2    ; [B_L2] 
||         MVKU32  .S2     0x3f317218,BM5    ; [B_S2] 

           MVKU32  .L2     0x40000000,BM0    ; [B_L2] 
||         MVKU32  .S2     0x3fb8aa3b,B3     ; [B_S2] 

	.dwpsn	file "./inc/tidl_detectionOutput_int.h",line 134,column 25,is_stmt,isa 0

           ADDD    .D2     A5,0x244,D6       ; [A_D2] 
||         MVKU32  .L2     0x46fffe00,BM2    ; [B_L2] 
||         MVKU32  .L1     0xc,AM1           ; [A_L1] |134| 
||         MVKU32  .S2     0,BL1             ; [B_S2] |229| 
||         MASKB   .P2     0x4,P1            ; [B_P] |229| 

;** --------------------------------------------------------------------------*
;**   BEGIN LOOP ||$C$L122||
;** --------------------------------------------------------------------------*
||$C$L122||:    
;          EXCLUSIVE CPU CYCLES: 15
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 386,column 31,is_stmt,isa 0
           LDD     .D1     *A5(1192),D0      ; [A_D1] |386| 
           ADDD    .D1     D2,D0,D0          ; [A_D1] |386| 
           LDW     .D1     *D0(0),A0         ; [A_D1] |386| 
           CMPGTW  .L1     A0,0,A2           ; [A_L1] |386| 
   [!A2]   B       .B1     ||$C$L124||       ; [A_B] |386| 
           ; BRANCHCC OCCURS {||$C$L124||}   ; [] |386| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 1
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 386,column 26,is_stmt,isa 0
           MVKU32  .L1     0,D4              ; [A_L1] |386| 
;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*
;*      Loop found in file               : src/tidl_detectionOutput_score.c
;*      Loop source line                 : 386
;*      Loop opening brace source line   : 387
;*      Loop closing brace source line   : 406
;*      Known Minimum Trip Count         : 1                    
;*      Known Max Trip Count Factor      : 1
;*      Loop Carried Dependency Bound(^) : 134
;*      Unpartitioned Resource Bound     : 7
;*      Partitioned Resource Bound       : 10 (pre-sched)
;*      Disqualified loop: Loop carried dependency bound too large
;*----------------------------------------------------------------------------*
||$C$L123||:    
;          EXCLUSIVE CPU CYCLES: 134
	.dwpsn	file "./inc/tidl_detectionOutput_int.h",line 134,column 25,is_stmt,isa 0
           LDD     .D1     *A5(1200),D0      ; [A_D1] |134| 
           ADDD    .D1     D2,D0,D0          ; [A_D1] |134| 
           LDW     .D1     *D0(0),D0         ; [A_D1] |134| 
           LDD     .D1     *A5(1184),D5      ; [A_D1] |134| 
           ADDW    .D1     D4,D0,D0          ; [A_D1] |134| 
           LDW     .D1     *D5[D0],B1        ; [A_D1] |134| 
           SHRW    .L2     B1,0x1c,BL5       ; [B_L2] |134| 
           ANDW    .L2     BL5,0xf,B0        ; [B_L2] |134| 
           EXT     .L1X    B0,0x20,0x20,A2   ; [A_L1] |134| 

           MPYDD   .N1     AM1,A2,D5         ; [A_N1] |134| 
||         LDW     .D1     *A6(12),B2        ; [A_D1] |134| 

           ADDD    .D1     D6,D5,D5          ; [A_D1] |134| 
||         SHRW    .L2     B1,0x18,BL7       ; [B_L2] |134| 

           LDW     .D1     *D5(0),B0         ; [A_D1] |134| 
||         ANDW    .L2     BL7,0xf,B4        ; [B_L2] |134| 

           MPYWW   .N2     B2,B4,BL5         ; [B_N2] |134| 
           SHRW    .L1X    B1,0x10,D14       ; [A_L1] |134| 
           ANDW    .D1     D3,D14,A3         ; [A_D1] |134| 
           SHLD    .L1     A2,0x3,D13        ; [A_L1] |134| 

           ADDD    .D1     A5,D13,D5         ; [A_D1] |134| 
||         ADDW    .L2X    A3,BL5,B2         ; [B_L2] |134| 

           MPYWW   .N2     B2,B0,B2          ; [B_N2] |134| 
||         LDD     .D1     *D5(192),D7       ; [A_D1] |134| 

           ANDW    .L2     BL4,B1,B13        ; [B_L2] |134| 
           MV      .L1X    B13,AM0           ; [A_L1] |134| Define a twin register
           ADDW    .M1X    B2,AM0,D5         ; [A_M1] |134| 
           LDB     .D1     *D7[D5],B0        ; [A_D1] |134| 
           SHRW    .L1X    B1,0x1c,AL0       ; [A_L1] |134| 
           ANDW    .L1     AL0,0xf,D12       ; [A_L1] |134| 
           LDUW    .D1     *D1[D12],B1       ; [A_D1] |134| 
           VINTSP  .L2     B0,B0             ; [B_L2] |134| 
           MPYSP   .N2     B1,B0,BL5         ; [B_N2] |134| 
           VXORW   .L2     BL5,0x80000000,B0 ; [B_L2] |134| 
           MPYSP   .N2     B3,B0,B1          ; [B_N2] |134| 
	.dwpsn	file "./inc/tidl_detectionOutput_int.h",line 229,column 1,is_stmt,isa 0
           VSPTRUNC .L2    B1,B0             ; [B_L2] |229| 
           VINTSP  .L2     B0,B2             ; [B_L2] |229| 
           SUBSP   .C2     B1,B2,B1          ; [B_C] |229| 
           MPYSP   .N2     BM5,B1,B2         ; [B_N2] |229| 
           MPYSP   .N2     B2,B2,B1          ; [B_N2] |229| 
           CMPGEW  .L1X    B0,0xfffffff0,A2  ; [A_L1] |229| 
   [ A2]   MPYSP   .N2     B2,B1,B5          ; [B_N2] |229| 
   [ A2]   MPYSP   .N2     BM3,B1,B4         ; [B_N2] |229| 
   [ A2]   ADDSP   .C2     BM1,B2,B2         ; [B_C] |229| 
   [ A2]   MPYSP   .N2     B1,B1,B1          ; [B_N2] |229| 
   [ A2]   MPYSP   .N2     BM4,B5,B5         ; [B_N2] |229| 
   [ A2]   ADDSP   .C2     B4,B2,B2          ; [B_C] |229| 
   [ A2]   MPYSP   .N2     BM6,B1,B1         ; [B_N2] |229| 

   [ A2]   ADDSP   .C2     B5,B2,B2          ; [B_C] |229| 
||         SUBRW   .S2     B0,0,BL5          ; [B_S2] |229| 
||         VCMPGTW .L2     B0,BL1,P0         ; [B_L2] |229| 

   [ A2]   SHRW    .L2     BL0,BL5,BL5       ; [B_L2] |229| 
|| [ A2]   AND     .P2     P0,P1,P0          ; [B_P] |229| 
||         SHLW    .S2     BL0,B0,BL6        ; [B_S2] |229| 

   [ A2]   VSEL    .L2     P0,BL6,BL5,BL5    ; [B_L2] |229| 

   [ A2]   VINTSP  .L2     BL5,B1            ; [B_L2] |229| 
|| [ A2]   ADDSP   .C2     B1,B2,B2          ; [B_C] |229| 

   [ A2]   MPYSP   .N2     B2,B1,B1          ; [B_N2] |229| 
   [ A2]   MPYSP   .N2     BM7,B1,BL5        ; [B_N2] |229| 

   [!A2]   MV      .L2     BL3,BL5           ; [B_L2] |229| 
||         CMPGTW  .L1X    B0,0xe,A3         ; [A_L1] |229| 

   [!A3]   MV      .L2     BL5,B0            ; [B_L2] |229| 
|| [ A3]   MV      .S2     BL2,B0            ; [B_S2] |229| 

           ADDSP   .C2     BM1,B0,B0         ; [B_C] |229| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 405,column 11,is_stmt,isa 0
           VRCPSP  .S2     B0,B1             ; [B_S2] |405| 
           MPYSP   .N2     B0,B1,B2          ; [B_N2] |405| 
           SUBSP   .C2     BM0,B2,B2         ; [B_C] |405| 
           MPYSP   .N2     B1,B2,B1          ; [B_N2] |405| 
           MPYSP   .N2     B0,B1,B0          ; [B_N2] |405| 
           SUBSP   .C2     BM0,B0,B0         ; [B_C] |405| 
           MPYSP   .N2     B1,B0,B0          ; [B_N2] |405| 
           MPYSP   .N2     BM2,B0,BL6        ; [B_N2] |405| 
           LDD     .D1     *A5(1152),D11     ; [A_D1] |405| 
           VSPTRUNC .L2    BL6,B0            ; [B_L2] |405| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 386,column 31,is_stmt,isa 0
           ADDW    .D1     A0,0xffffffff,A0  ; [A_D1] |386| 

   [ A0]   B       .B1     ||$C$L123||       ; [A_B] |386| 
||         STH     .D1X    B0,*D11[D0]       ; [A_D1] |405| 
||         ADDW    .D2     D4,0x1,D4         ; [A_D2] |386| 

           ; BRANCHCC OCCURS {||$C$L123||}   ; [] |386| 
;** --------------------------------------------------------------------------*
||$C$L124||:    
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 381,column 29,is_stmt,isa 0
           ADDW    .D1     A1,0xffffffff,A1  ; [A_D1] |381| 

   [ A1]   B       .B1     ||$C$L122||       ; [A_B] |381| 
||         ADDD    .D1     D2,0x4,D2         ; [A_D1] |381| 

           ; BRANCHCC OCCURS {||$C$L122||}   ; [] |381| 
;** --------------------------------------------------------------------------*
||$C$L125||:    
;          EXCLUSIVE CPU CYCLES: 7
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 408,column 7,is_stmt,isa 0
           LDD     .D1     *A5(1152),B0      ; [A_D1] |408| 

           B       .B1     ||$C$L150||       ; [A_B] |580| 
||         STD     .D1X    B0,*A5(1160)      ; [A_D1] |408| 
||         MVKU32  .L1     0,A4              ; [A_L1] |580| 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 580,column 3,is_stmt,isa 0
           ; BRANCH OCCURS {||$C$L150||}     ; [] |580| 
;** --------------------------------------------------------------------------*
||$C$L126||:    
;          EXCLUSIVE CPU CYCLES: 17
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 473,column 27,is_stmt,isa 0
           LDD     .D1     *A5(1192),D0      ; [A_D1] |473| 
           LDW     .D1     *D0(0),B0         ; [A_D1] |473| 
           CMPGTW  .L2     B0,0,BL0          ; [B_L2] |473| 
           XORD    .L2     BL0,0x1,B1        ; [B_L2] |473| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 471,column 5,is_stmt,isa 0

           MV      .S1X    B0,A3             ; [A_S1] 
||         MVKU32  .L1     0xffff,D11        ; [A_L1] 
||         MVKU32  .L2     0,B6              ; [B_L2] |471| 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 473,column 27,is_stmt,isa 0

           CMPEQW  .L1X    B1,0,A0           ; [A_L1] |473| 
||         MVKU32  .L2     0x40000000,B7     ; [B_L2] 
||         MVKU32  .S1     0,D8              ; [A_S1] 
||         ADDD    .D2     A5,0x140,D9       ; [A_D2] 
||         MVKU32  .S2     0,B3              ; [B_S2] 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 471,column 5,is_stmt,isa 0

   [!A0]   B       .B1     ||$C$L141||       ; [A_B] |473| 
||         STW     .D1X    B6,*D0(0)         ; [A_D1] |471| 
||         MVKU32  .L2     0,B5              ; [B_L2] 
||         MVKU32  .S2     0xff7fffff,BL7    ; [B_S2] 
||         MVKU32  .L1     0,A4              ; [A_L1] 
||         MVK32   .S1     0xff00ffff,D10    ; [A_S1] 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 473,column 27,is_stmt,isa 0
           ; BRANCHCC OCCURS {||$C$L141||}   ; [] |473| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 5

           MVKU32  .L2     0x3f317218,B12    ; [B_L2] 
||         MVKU32  .S2     0x3e2aaaad,B11    ; [B_S2] 

           MVKU32  .L2     0x10000,B9        ; [B_L2] 
||         MVKU32  .S2     0x37800000,B14    ; [B_S2] 

           MVKU32  .L2     0x3f000000,B10    ; [B_L2] 
||         MVKU32  .S2     0x3d2aaab4,B13    ; [B_S2] 

	.dwpsn	file "./inc/tidl_detectionOutput_int.h",line 157,column 3,is_stmt,isa 0

           MV      .M2     B12,BM4           ; [B_M2] 
||         MV      .C2     B11,BM3           ; [B_C] 
||         MVKU32  .L2     0,B8              ; [B_L2] |157| 
||         MVKU32  .S2     0x3f800000,B15    ; [B_S2] 

           MV      .L2     B10,BM2           ; [B_L2] 
||         MV      .S2     B9,BL4            ; [B_S2] 
||         MV      .M2     B14,BM6           ; [B_M2] 
||         MV      .C2     B13,BM5           ; [B_C] 
||         MASKB   .P2     0x4,P1            ; [B_P] |157| 
||         LDW     .D1     *A6(12),A8        ; [A_D1] 

;** --------------------------------------------------------------------------*
;**   BEGIN LOOP ||$C$L127||
;** --------------------------------------------------------------------------*
||$C$L127||:    
;          EXCLUSIVE CPU CYCLES: 16
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 475,column 7,is_stmt,isa 0
           LDD     .D1     *A5(1184),AM0     ; [A_D1] |475| 
           ADDD    .M1X    B5,AM0,D0         ; [A_M1] |475| 

           LDW     .D1     *D0(0),A4         ; [A_D1] |475| 
||         LDW     .D2     *A5(1304),AL1     ; [A_D2] |484| 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 484,column 7,is_stmt,isa 0
           LDW     .D1     *A6(76),AL0       ; [A_D1] |484| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 486,column 9,is_stmt,isa 0
           VSPTRUNC .L2    BL7,BL0           ; [B_L2] |486| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 483,column 7,is_stmt,isa 0

           SHRW    .L1     A4,0x1c,AM0       ; [A_L1] |483| 
||         CMPEQW  .S1     AL1,0x6,A0        ; [A_S1] |484| 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 478,column 22,is_stmt,isa 0

           ANDW    .M1     AM0,0xf,A9        ; [A_M1] |483| 
||         SHRW    .L1     A4,0x18,AL1       ; [A_L1] |478| 
||         CMPEQW  .S1     AL0,0,A1          ; [A_S1] |493| 

   [!A1]   B       .B1     ||$C$L128||       ; [A_B] |493| 
||         LDUW    .D1     *D9[A9],B4        ; [A_D1] |483| 
||         ANDW    .L1     AL1,0xf,AM3       ; [A_L1] |478| 
||         ANDW    .D2     D11,A4,A2         ; [A_D2] |479| 
|| [ A0]   EXT     .L2     BL0,0x38,0x38,B1  ; [B_L2] |486| 
|| [!A0]   EXT     .S2     BL6,0x38,0x38,B1  ; [B_S2] |490| 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 493,column 7,is_stmt,isa 0
           ; BRANCHCC OCCURS {||$C$L128||}   ; [] |493| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 3
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 496,column 9,is_stmt,isa 0
           MV      .D1     A8,A7             ; [A_D1] |496| 
           CMPGTW  .L1     A7,0,A0           ; [A_L1] |496| 
   [!A0]   B       .B1     ||$C$L136||       ; [A_B] |496| 
           ; BRANCHCC OCCURS {||$C$L136||}   ; [] |496| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 1
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 495,column 9,is_stmt,isa 0

           B       .B1     ||$C$L129||       ; [A_B] 
||         MVKU32  .L1     0x1,A1            ; [A_L1] |495| 

           ; BRANCH OCCURS {||$C$L129||}     ; [] 
;** --------------------------------------------------------------------------*
||$C$L128||:    
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 500,column 9,is_stmt,isa 0

           CMPGTW  .L1     A8,0,A0           ; [A_L1] |505| 
||         ADDAW   .D1     A5,A9,D0          ; [A_D1] |500| 

   [!A0]   B       .B1     ||$C$L136||       ; [A_B] |505| 
||         LDW     .D1     *D0(1088),A1      ; [A_D1] |500| 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 505,column 20,is_stmt,isa 0
           ; BRANCHCC OCCURS {||$C$L136||}   ; [] |505| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 1
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 501,column 9,is_stmt,isa 0
           MVKU32  .L1     0x1,A7            ; [A_L1] |501| 
;** --------------------------------------------------------------------------*
||$C$L129||:    
;          EXCLUSIVE CPU CYCLES: 10

           EXT     .L1     A9,0x20,0x20,A0   ; [A_L1] 
||         MVKU32  .S1     0xc,AM0           ; [A_S1] 

           MPYDD   .N1     AM0,A0,D0         ; [A_N1] 
           MV      .L2X    A8,B0             ; [B_L2] 

           ADDD    .D1     A5,D0,D0          ; [A_D1] 
||         MV      .L2     B0,BL1            ; [B_L2] 

           LDW     .D1     *D0(580),A10      ; [A_D1] 
||         EXT     .S2     BL1,0x20,0x20,B2  ; [B_S2] 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 505,column 12,is_stmt,isa 0

           MVKU32  .L1     0,A0              ; [A_L1] |505| 
||         MV      .D1     A1,AM0            ; [A_D1] 
||         SHLD    .S1     A0,0x3,D1         ; [A_S1] 
||         MPYWW   .N1     A7,AM3,A12        ; [A_N1] 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 507,column 9,is_stmt,isa 0

           MPYWW   .N1     AM0,A0,D0         ; [A_N1] |507| <0,1> 
||         NLCINIT .S1X    B2,0x1,8          ; [A_S1] 
||         UNPROT          0x1               ; [A_U] 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 505,column 20,is_stmt,isa 0

           ADDW    .D1     A0,0x1,A0         ; [A_D1] |505| <0,2> 
||         TICK                               ; [A_U] <0,0> 
||         ADDD    .D2     A5,D1,D1          ; [A_D2] 

;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*
;*      Loop found in file               : src/tidl_detectionOutput_score.c
;*      Loop source line                 : 505
;*      Loop opening brace source line   : 506
;*      Loop closing brace source line   : 509
;*      Known Minimum Trip Count         : 1                    
;*      Known Max Trip Count Factor      : 1
;*      Loop Carried Dependency Bound(^) : 2
;*      Unpartitioned Resource Bound     : 1
;*      Partitioned Resource Bound       : 1 (pre-sched)
;*
;*      Searching for software pipeline schedule at ...
;*         ii = 2  Schedule found with 10 iterations in parallel
;*
;*      Partitioned Resource Bound(*)    : 2 (post-sched)
;*
;*      Constant Extension #0 Used [C0]  : 0
;*      Constant Extension #1 Used [C1]  : 1
;*
;*      Resource Partition (may include "post-sched" split/spill moves):
;*
;*                                                A-side   B-side
;*      .L units                                     0        0     
;*      .S units                                     0        0     
;*      .M units                                     0        0     
;*      .N units                                     0        0     
;*      .D units                                     1        -     
;*      .B units                                     1        -     
;*      .C units                                     -        0     
;*      .P units                                     -        0     
;*
;*      .M/.N units                                  2        0     
;*      .L/.S units                                  0        1     
;*      .L/.S/.C units                               0        0     
;*      .L/.S/.C/.M units                            0        1     
;*      .L/.S/.C/.M/.D units                         3        0     
;*
;*      .X cross paths                               0        0     
;*
;*      Bound(.D1 .D2 .D)                            1        -     
;*      Bound(.M .N .MN)                             1        0     
;*      Bound(.L .S .LS)                             0        1     
;*      Bound(.L .S .C .LS .LSC)                     0        1     
;*      Bound(.L .S .C .M .LS .LSC .LSCM)            0        1     
;*      Bound(.L .S .C .M .D .LS .LSC .LSCM .LSCMD)  1        1     
;*
;*      Register Usage Tables:
;*
;*      +----------------------------------+----------------------------------+
;*      |              ASIDE               |              BSIDE               |
;*      +----------------+--------+--------+----------------+--------+--------+
;*      |      "Axx"     |  "ALx" |  "AMx" |     "VBxx"     | "VBLx" | "VBMx" |
;*      +----------------|--------|--------|----------------|--------|--------+
;*      |0000000000111111|00000000|00000000|0000000000111111|00000000|00000000|
;*      |0123456789012345|01234567|01234567|0123456789012345|01234567|01234567|
;*      +----------------|--------|--------|----------------|--------|--------+
;*   0: |***             |        |**      |                |**      |        |
;*   1: |***             |        |***     |                |*       |        |
;*      +----------------+--------+--------+----------------+--------+--------+
;*
;*      +----------------+                 +--------+       +-------+
;*      |      "Dxx"     |                 |  "Px"  |       |"CUCRx"|
;*      +----------------+                 +--------+       +-------+
;*      |0000000000111111|                 |00000000|       |0000   |
;*      |0123456789012345|                 |01234567|       |0123   |
;*      +----------------+                 +--------+       +-------+
;*   0: |*****           |                 |        |       |       |
;*   1: |****            |                 |        |       |       |
;*      +----------------+                 +--------+       +-------+
;*
;*      Done
;*
;*      Collapsed epilog stages       : 9
;*      Prolog not entirely removed
;*      Collapsed prolog stages       : 2
;*
;*      Max amt of load speculation   : 0 bytes
;*
;*      Minimum safe trip count       : 1
;*      Min. prof. trip count  (est.) : 2
;*
;*      Mem bank conflicts/iter(est.) : { min 0.000, est 0.000, max 0.000 }
;*      Mem bank perf. penalty (est.) : 0.0%
;*
;*
;*      Total cycles (est.)         : 17 + trip_cnt * 2        
;*----------------------------------------------------------------------------*
;*        SINGLE SCHEDULED ITERATION
;*
;*        ||$C$C2234||:
;*   0              TICK                               ; [A_U] 
;*   1              MPYWW   .N1     AM0,A0,D4         ; [A_N1] |507| 
;*   2              ADDW    .L1     A0,0x1,A0         ; [A_L1] |505| 
;*   3              NOP     0x2     ; [A_B] 
;*   5              ADDW    .D2     D3,D4,AM2         ; [A_D2] |507| 
;*   6              MPYWW   .N1     AM1,AM2,D0        ; [A_N1] |507| 
;*   7              NOP     0x3     ; [A_B] 
;*  10              ADDW    .D1     D2,D0,D0          ; [A_D1] |507| 
;*  11              LDB     .D1     *D1[D0],BL0       ; [A_D1] |507| 
;*  12              NOP     0x5     ; [A_B] 
;*  17              VMAXB   .L2     BL1,BL0,BL0       ; [B_L2] |507|  ^ 
;*  18              EXT     .L2     BL0,0x38,0x38,BL1 ; [B_L2] |507|  ^ [C1]
;*     ||           BNL     .B1     ||$C$C2234||      ; [A_B] |505| 
;*  19              ; BRANCHCC OCCURS {||$C$C2234||}  ; [] |505| 
;*----------------------------------------------------------------------------*
||$C$L130||:    ; PIPED LOOP PROLOG
;          EXCLUSIVE CPU CYCLES: 10
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 507,column 9,is_stmt,isa 0

           TICK                               ; [A_U] <1,0> 
||         MPYWW   .N1     AM0,A0,D0         ; [A_N1] |507| <1,1> 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 505,column 20,is_stmt,isa 0

           MV      .D2     A12,D3            ; [A_D2] 
||         LDD     .D1     *D1(192),A11      ; [A_D1] 
||         ADDW    .L1     A0,0x1,A0         ; [A_L1] |505| <1,2> 
||         TICK                               ; [A_U] <2,0> 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 507,column 9,is_stmt,isa 0

           MV      .D1     A10,AM1           ; [A_D1] 
||         ADDW    .D2     D3,D0,AM2         ; [A_D2] |507| <0,5> 
||         MPYWW   .N1     AM0,A0,D0         ; [A_N1] |507| <2,1> 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 505,column 20,is_stmt,isa 0

           MPYWW   .N1     AM1,AM2,D0        ; [A_N1] |507| <0,6> 
||         ADDW    .L1     A0,0x1,A0         ; [A_L1] |505| <2,2> 
||         TICK                               ; [A_U] <3,0> 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 507,column 9,is_stmt,isa 0

           ADDW    .D2     D3,D0,AM2         ; [A_D2] |507| <1,5> 
||         MPYWW   .N1     AM0,A0,D4         ; [A_N1] |507| <3,1> 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 505,column 20,is_stmt,isa 0

           MPYWW   .N1     AM1,AM2,D0        ; [A_N1] |507| <1,6> 
||         ADDW    .L1     A0,0x1,A0         ; [A_L1] |505| <3,2> 
||         TICK                               ; [A_U] <4,0> 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 507,column 9,is_stmt,isa 0

           MV      .D1     A2,D2             ; [A_D1] 
||         ADDW    .D2     D3,D0,AM2         ; [A_D2] |507| <2,5> 
||         MPYWW   .N1     AM0,A0,D4         ; [A_N1] |507| <4,1> 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 505,column 20,is_stmt,isa 0

           MV      .D2     A11,D1            ; [A_D2] 
||         ADDW    .D1     D2,D0,D0          ; [A_D1] |507| <0,10> 
||         MPYWW   .N1     AM1,AM2,D0        ; [A_N1] |507| <2,6> 
||         ADDW    .L1     A0,0x1,A0         ; [A_L1] |505| <4,2> 
||         TICK                               ; [A_U] <5,0> 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 507,column 9,is_stmt,isa 0

           MV      .L1X    B0,D4             ; [A_L1] 
||         LDB     .D1     *D1[D0],BL0       ; [A_D1] |507| <0,11> 
||         ADDW    .D2     D3,D4,AM2         ; [A_D2] |507| <3,5> 
||         MPYWW   .N1     AM0,A0,D4         ; [A_N1] |507| <5,1> 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 505,column 20,is_stmt,isa 0

           MV      .L2     B1,BL1            ; [B_L2] 
||         MVKU32  .S1     0x2,A2            ; [A_S1] init prolog collapse predicate
||         ADDD    .D2     D4,0xffffffff,A1  ; [A_D2] init epilog collapse predicate
||         ADDW    .D1     D2,D0,D0          ; [A_D1] |507| <1,10> 
||         MPYWW   .N1     AM1,AM2,D0        ; [A_N1] |507| <3,6> 
||         ADDW    .L1     A0,0x1,A0         ; [A_L1] |505| <5,2> 
||         TICK                               ; [A_U] <6,0> 

;** --------------------------------------------------------------------------*
||$C$L131||:    ; PIPED LOOP KERNEL
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 507,column 9,is_stmt,isa 0

           VMAXB   .L2     BL1,BL0,BL0       ; [B_L2] |507| <0,17>  ^ 
|| [ A1]   LDB     .D1     *D1[D0],BL0       ; [A_D1] |507| <3,11> 
||         ADDW    .D2     D3,D4,AM2         ; [A_D2] |507| <6,5> 
||         MPYWW   .N1     AM0,A0,D4         ; [A_N1] |507| <8,1> 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 505,column 20,is_stmt,isa 0

   [ A2]   ADDW    .S1     A2,0xffffffff,A2  ; [A_S1] |505| <0,18> collapsing predicate control
|| [ A1]   ADDW    .D2     A1,0xffffffff,A1  ; [A_D2] |505| <0,18> collapsing predicate control
||         BNL     .B1     ||$C$L131||       ; [A_B] |505| <0,18> 
|| [!A2]   EXT     .L2     BL0,0x38,0x38,BL1 ; [B_L2] |507| <0,18>  ^ [C1]
||         ADDW    .D1     D2,D0,D0          ; [A_D1] |507| <4,10> 
||         MPYWW   .N1     AM1,AM2,D0        ; [A_N1] |507| <6,6> 
||         ADDW    .L1     A0,0x1,A0         ; [A_L1] |505| <8,2> 
||         TICK                               ; [A_U] <9,0> 

;** --------------------------------------------------------------------------*
||$C$L132||:    ; PIPED LOOP EPILOG
;          EXCLUSIVE CPU CYCLES: 1
           PROTCLR                            ; [A_U] VBL1
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 511,column 20,is_stmt,isa 0
           CMPGTW  .L1     A8,0,A0           ; [A_L1] |511| 

   [!A0]   B       .B1     ||$C$L136||       ; [A_B] |511| 
||         MV      .M1     AM0,A1            ; [A_M1] 
||         MV      .L2     BL1,B1            ; [B_L2] 
||         MV      .D1     D2,A2             ; [A_D1] 

           ; BRANCHCC OCCURS {||$C$L136||}   ; [] |511| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 17

           MVKU32  .L1     0xc,AM0           ; [A_L1] 
||         EXT     .S1     A9,0x20,0x20,A0   ; [A_S1] 

           MPYDD   .N1     AM0,A0,AL0        ; [A_N1] 
           ADDD    .L1     A5,AL0,D0         ; [A_L1] 

           ADDD    .D1     D0,0x244,A7       ; [A_D1] 
||         MPYWW   .N1     A7,AM3,A11        ; [A_N1] 

           MV      .D1     A7,D3             ; [A_D1] 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 511,column 12,is_stmt,isa 0

           SLDW    .D1     *D3(0),AM0        ; [A_D1] |513| <0,0>  ^ 
||         MVKU32  .L1     0,A8              ; [A_L1] |511| 
||         MV      .D2     A1,AM1            ; [A_D2] 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 513,column 9,is_stmt,isa 0

           SHLD    .L1     A0,0x3,AL0        ; [A_L1] 
||         MPYWW   .N1     AM1,A8,D13        ; [A_N1] |513| <0,1> 

           ADDD    .L1     A5,AL0,D0         ; [A_L1] 
||         LDD     .D1     *PC($PCR_OFFSET(||_ZTIa||+8)),A9 ; [A_D1] 
||         LDD     .D2     *PC($PCR_OFFSET(||_ZTIf||+8)),D14 ; [A_D2] 

           ADDD    .D1     D0,0xc0,A10       ; [A_D1] 

           MV      .D1     A10,D4            ; [A_D1] 
||         MV      .D2     A11,D6            ; [A_D2] 

           SLDD    .D1     *D4(0),D7         ; [A_D1] |513| <0,5> 
||         ADDW    .D2     D6,D13,AM2        ; [A_D2] |513| <0,5> 

           MPYWW   .N1     AM2,AM0,D0        ; [A_N1] |513| <0,6>  ^ 

           MV      .L2     B15,BM7           ; [B_L2] 
||         MV      .S2     B3,BL3            ; [B_S2] 
||         MV      .M2     B8,BL1            ; [B_M2] 
||         MV      .C2     B3,B0             ; [B_C] 

           MV      .D1     D14,AL0           ; [A_D1] 
||         MV      .D2     A9,AL1            ; [A_D2] 
||         MV      .S2     B0,BM0            ; [B_S2] 
||         MVKU32  .L2     0x3fb8aa3b,B2     ; [B_L2] 
||         MV      .M2     B4,BM1            ; [B_M2] 
||         MV      .C2     B1,BL2            ; [B_C] 
||         MV      .L1     A6,D1             ; [A_L1] 
||         MV      .S1     A2,D5             ; [A_S1] 
||         MV      .M1     A5,D2             ; [A_M1] 
||         UNPROT          0x1               ; [A_U] 

;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*
;*      Loop found in file               : src/tidl_detectionOutput_score.c
;*      Loop source line                 : 511
;*      Loop opening brace source line   : 512
;*      Loop closing brace source line   : 528
;*      Known Minimum Trip Count         : 1                    
;*      Known Max Trip Count Factor      : 1
;*      Loop Carried Dependency Bound(^) : 71
;*      Unpartitioned Resource Bound     : 5
;*      Partitioned Resource Bound       : 7 (pre-sched)
;*
;*      Searching for software pipeline schedule at ...
;*         ii = 71 Schedule found with 2 iterations in parallel
;*
;*      Partitioned Resource Bound(*)    : 7 (post-sched)
;*
;*      Constant Extension #0 Used [C0]  : 0
;*      Constant Extension #1 Used [C1]  : 2
;*
;*      Resource Partition (may include "post-sched" split/spill moves):
;*
;*                                                A-side   B-side
;*      .L units                                     0        0     
;*      .S units                                     0        0     
;*      .M units                                     0        0     
;*      .N units                                     0        0     
;*      .D units                                     6        -     
;*      .B units                                     1        -     
;*      .C units                                     -        0     
;*      .P units                                     -        1     
;*
;*      .M/.N units                                  2       11     
;*      .L/.S units                                  4        5     
;*      .L/.S/.C units                               0       10     
;*      .L/.S/.C/.M units                            0        1     
;*      .L/.S/.C/.M/.D units                         3        4     
;*
;*      .X cross paths                               4        0     
;*
;*      Bound(.D1 .D2 .D)                            3        -     
;*      Bound(.M .N .MN)                             1        6     
;*      Bound(.L .S .LS)                             2        3     
;*      Bound(.L .S .C .LS .LSC)                     2        5     
;*      Bound(.L .S .C .M .LS .LSC .LSCM)            2        4     
;*      Bound(.L .S .C .M .D .LS .LSC .LSCM .LSCMD)  3        5     
;*
;*      Register Usage Tables:
;*
;*      +----------------------------------+----------------------------------+
;*      |              ASIDE               |              BSIDE               |
;*      +----------------+--------+--------+----------------+--------+--------+
;*      |      "Axx"     |  "ALx" |  "AMx" |     "VBxx"     | "VBLx" | "VBMx" |
;*      +----------------|--------|--------|----------------|--------|--------+
;*      |0000000000111111|00000000|00000000|0000000000111111|00000000|00000000|
;*      |0123456789012345|01234567|01234567|0123456789012345|01234567|01234567|
;*      +----------------|--------|--------|----------------|--------|--------+
;*   0: |        *       |**      | *      |  *             | ****   |********|
;*   1: |        *       |**      | *      |  *             | ****   |********|
;*   2: |        *       |**      | *      |  *             | ****   |********|
;*   3: |        *       |**      | *      |  *             | ****   |********|
;*   4: |        *       |**      | *      |  *             | ****   |********|
;*   5: |        *       |**      | *      |  *             | ****   |********|
;*   6: |        *       |**      | *      |  *             | ****   |********|
;*   7: |        *       |**      | *      |  *             | ****   |********|
;*   8: |*       *       |**      | *      |  *             |*****   |********|
;*   9: |*       *       |**      | *      |  *             |******  |********|
;*  10: |*       *       |**      | *      |  *             | ****   |********|
;*  11: |*       *       |**      | *      |  *             | ****   |********|
;*  12: |*       *       |**      | *      |***             | ****   |********|
;*  13: |        *       |**      | *      | **             | ****   |********|
;*  14: |        *       |**      | *      | **             | ****   |********|
;*  15: |        *       |**      | *      | **             | ****   |********|
;*  16: |        *       |**      | *      | **             | ****   |********|
;*  17: |        *       |**      | *      |  *             | ****   |********|
;*  18: |        *       |**      | *      |  *             | ****   |********|
;*  19: |        *       |**      | *      |  *             | ****   |********|
;*  20: |        *       |**      | *      |* *             | ****   |********|
;*  21: |        *       |**      | *      |* *             | ****   |********|
;*  22: |        *       |**      | *      |* *             | ****   |********|
;*  23: |        *       |**      | *      |***             | ****   |********|
;*  24: |        *       |**      | *      |***             |*****   |********|
;*  25: | *      *       |**      | *      |***             |******  |********|
;*  26: |**      *       |**      | *      |***             |*****   |********|
;*  27: |**      *       |**      | *      |  *             | ****   |********|
;*  28: |**      *       |**      | *      |  *             | ****   |********|
;*  29: |**      *       |**      | *      |***             | ****   |********|
;*  30: |**      *       |**      | *      |* *             | ****   |********|
;*  31: |**      *       |**      | *      |* *             | ****   |********|
;*  32: |**      *       |**      | *      |* *             | ****   |********|
;*  33: |**      *       |**      | *      |* * *           | ****   |********|
;*  34: |**      *       |**      | *      |* * *           | ****   |********|
;*  35: |**      *       |**      | *      |* * *           | ****   |********|
;*  36: |**      *       |**      | *      |*** *           | ****   |********|
;*  37: |**      *       |**      | *      |*****           | ****   |********|
;*  38: |**      *       |**      | *      |****            | ****   |********|
;*  39: |**      *       |**      | *      |***             | ****   |********|
;*  40: |**      *       |**      | *      |***             | ****   |********|
;*  41: |**      *       |**      | *      |*****           | ****   |********|
;*  42: |**      *       |**      | *      |***             | ****   |********|
;*  43: |**      *       |**      | *      |* *             | ****   |********|
;*  44: |**      *       |**      | *      |***             | ****   |********|
;*  45: |**      *       |**      | *      |****            | ****   |********|
;*  46: |**      *       |**      | *      |* **            | ****   |********|
;*  47: |**      *       |**      | *      |* **            | ****   |********|
;*  48: |**      *       |**      | *      |****            | ****   |********|
;*  49: |**      *       |**      | *      |* *             | ****   |********|
;*  50: |**      *       |**      | *      |* *             | ****   |********|
;*  51: |**      *       |**      | *      |***             | ****   |********|
;*  52: |**      *       |**      | *      |  *             | ****   |********|
;*  53: |**      *       |**      | *      |  *             | ****   |********|
;*  54: |**      *       |**      | *      |  *             | ****   |********|
;*  55: |**      *       |**      | *      |* *             | ****   |********|
;*  56: |**      *       |**      | *      |  *             | ****   |********|
;*  57: |**      *       |**      | *      |  *             | ****   |********|
;*  58: |**      *       |**      | *      |  *             | ****   |********|
;*  59: |**      *       |**      | *      |* *             | ****   |********|
;*  60: |*       *       |**      | *      |* *             | ****   |********|
;*  61: |        *       |**      | *      |* *             | ****   |********|
;*  62: |        *       |**      | *      |  *             | ****   | *******|
;*  63: |        *       |**      | *      |  *             | ****   | *******|
;*  64: |        *       |**      | *      |  *             | ****   |********|
;*  65: |        *       |**      | *      |  *             | ****   |********|
;*  66: |        *       |**      | *      |  *             | ****   |********|
;*  67: |        *       |**      | *      |  *             | ****   |********|
;*  68: |        *       |**      |***     |* *             | ****   |********|
;*  69: |        *       |**      | *      |* *             | ****   |********|
;*  70: |*       *       |**      | *      |  *             | ****   |********|
;*      +----------------+--------+--------+----------------+--------+--------+
;*
;*      +----------------+                 +--------+       +-------+
;*      |      "Dxx"     |                 |  "Px"  |       |"CUCRx"|
;*      +----------------+                 +--------+       +-------+
;*      |0000000000111111|                 |00000000|       |0000   |
;*      |0123456789012345|                 |01234567|       |0123   |
;*      +----------------+                 +--------+       +-------+
;*   0: | ******         |                 | *      |       |       |
;*   1: |*******         |                 | *      |       |       |
;*   2: |********        |                 | *      |       |       |
;*   3: | ******         |                 | *      |       |       |
;*   4: | ******         |                 | *      |       |       |
;*   5: | ******         |                 | *      |       |       |
;*   6: | ******         |                 | *      |       |       |
;*   7: | ******         |                 | *      |       |       |
;*   8: | ******         |                 | *      |       |       |
;*   9: | ******         |                 | *      |       |       |
;*  10: | ******         |                 | *      |       |       |
;*  11: | ******         |                 | *      |       |       |
;*  12: | ******         |                 | *      |       |       |
;*  13: | ******         |                 | *      |       |       |
;*  14: | ******         |                 | *      |       |       |
;*  15: | ******         |                 | *      |       |       |
;*  16: | ******         |                 | *      |       |       |
;*  17: | ******         |                 | *      |       |       |
;*  18: | ******         |                 | *      |       |       |
;*  19: | ******         |                 | *      |       |       |
;*  20: | ******         |                 | *      |       |       |
;*  21: | ******         |                 | *      |       |       |
;*  22: | ******         |                 | *      |       |       |
;*  23: | ******         |                 | *      |       |       |
;*  24: | ******         |                 |**      |       |       |
;*  25: | ******         |                 |**      |       |       |
;*  26: | ******         |                 | *      |       |       |
;*  27: | ******         |                 | *      |       |       |
;*  28: | ******         |                 | *      |       |       |
;*  29: | ******         |                 | *      |       |       |
;*  30: | ******         |                 | *      |       |       |
;*  31: | ******         |                 | *      |       |       |
;*  32: | ******         |                 | *      |       |       |
;*  33: | ******         |                 | *      |       |       |
;*  34: | ******         |                 | *      |       |       |
;*  35: | ******         |                 | *      |       |       |
;*  36: | ******         |                 | *      |       |       |
;*  37: | ******         |                 | *      |       |       |
;*  38: | ******         |                 | *      |       |       |
;*  39: | ******         |                 | *      |       |       |
;*  40: | ******         |                 | *      |       |       |
;*  41: | ******         |                 | *      |       |       |
;*  42: | ******         |                 | *      |       |       |
;*  43: | ******         |                 | *      |       |       |
;*  44: | ******         |                 | *      |       |       |
;*  45: | ******         |                 | *      |       |       |
;*  46: | ******         |                 | *      |       |       |
;*  47: | ******         |                 | *      |       |       |
;*  48: | ******         |                 | *      |       |       |
;*  49: | ******         |                 | *      |       |       |
;*  50: | ******         |                 | *      |       |       |
;*  51: | ******         |                 | *      |       |       |
;*  52: | ******         |                 | *      |       |       |
;*  53: | ******         |                 | *      |       |       |
;*  54: | ******         |                 | *      |       |       |
;*  55: | ******         |                 | *      |       |       |
;*  56: | ******         |                 | *      |       |       |
;*  57: | ******         |                 | *      |       |       |
;*  58: | ******         |                 | *      |       |       |
;*  59: | ******         |                 | *      |       |       |
;*  60: | ******         |                 | *      |       |       |
;*  61: |*******         |                 | *      |       |       |
;*  62: | ******         |                 | *      |       |       |
;*  63: | ******         |                 | *      |       |       |
;*  64: | ******         |                 | *      |       |       |
;*  65: | ******         |                 | *      |       |       |
;*  66: | ******         |                 | *      |       |       |
;*  67: |*******         |                 | *      |       |       |
;*  68: | ******         |                 | *      |       |       |
;*  69: | ******         |                 | *      |       |       |
;*  70: | ******         |                 | *      |       |       |
;*      +----------------+                 +--------+       +-------+
;*
;*      Done
;*
;*      Collapsed epilog stages       : 1
;*      Prolog not removed
;*      Collapsed prolog stages       : 0
;*
;*      Max amt of load speculation   : 0 bytes
;*
;*      Minimum safe trip count       : 1
;*      Min. prof. trip count  (est.) : 2
;*
;*      Mem bank conflicts/iter(est.) : { min 0.000, est 0.000, max 0.000 }
;*      Mem bank perf. penalty (est.) : 0.0%
;*
;*
;*      Total cycles (est.)         : 9 + trip_cnt * 71        
;*----------------------------------------------------------------------------*
;*        SINGLE SCHEDULED ITERATION
;*
;*        ||$C$C2144||:
;*   0              SLDW    .D2     *D3(0),AM0        ; [A_D2] |513|  ^ 
;*   1              MPYWW   .N1     AM1,A8,D0         ; [A_N1] |513| 
;*   2              NOP     0x3     ; [A_B] 
;*   5              ADDW    .D2     D6,D0,AM2         ; [A_D2] |513| 
;*     ||           SLDD    .D1     *D4(0),D7         ; [A_D1] |513| 
;*   6              MPYWW   .N1     AM2,AM0,D0        ; [A_N1] |513|  ^ 
;*   7              NOP     0x3     ; [A_B] 
;*  10              ADDW    .D1     D5,D0,D0          ; [A_D1] |513|  ^ 
;*  11              LDB     .D1     *D7[D0],BL0       ; [A_D1] |513|  ^ 
;*  12              NOP     0x4     ; [A_B] 
;*  16              CMPEQD  .L1     AL1,AL0,A0        ; [A_L1] |517| 
;*  17      [ A0]   SUBW    .S2     BL0,BL2,BL5       ; [B_S2] |517| 
;*     ||           SUBW    .L2     BL0,BL2,BL0       ; [B_L2] |517|  ^ 
;*  18      [ A0]   VINTSP  .S2     BL5,B1            ; [B_S2] |517| 
;*     ||   [!A0]   VINTSP  .L2     BL0,B0            ; [B_L2] |517|  ^ 
;*  19              NOP     0x2     ; [A_B] 
;*  21      [!A0]   MPYSP   .N2     BM1,B0,B1         ; [B_N2] |517|  ^ 
;*  22              NOP     0x3     ; [A_B] 
;*  25              MPYSP   .N2     B2,B1,B0          ; [B_N2] |135|  ^ 
;*  26              NOP     0x3     ; [A_B] 
;*  29              VSPTRUNC .L2    B0,B1             ; [B_L2] |135|  ^ 
;*  30              NOP     0x2     ; [A_B] 
;*  32              VINTSP  .S2     B1,B1             ; [B_S2] |157|  ^ 
;*     ||           VCMPGTW .L2     B1,BL1,P0         ; [B_L2] |157| 
;*     ||           SUBRW   .M2     B1,0,BL0          ; [B_M2] |157| 
;*  33              SHLW    .L2     BL4,B1,BL5        ; [B_L2] |157| 
;*     ||           AND     .P2     P0,P1,P0          ; [B_P] |157| 
;*     ||           SHRW    .S2     BL4,BL0,BL0       ; [B_S2] |157| 
;*     ||           CMPGEW  .L1X    B1,0xfffffff0,A1  ; [A_L1] |161| 
;*  34              VSEL    .L2     P0,BL5,BL0,BL0    ; [B_L2] |157| 
;*     ||           CMPGTW  .L1X    B1,0xe,A0         ; [A_L1] |164| 
;*  35              SUBSP   .C2     B0,B1,B1          ; [B_C] |157|  ^ 
;*     ||           VINTSP  .L2     BL0,B0            ; [B_L2] |157| 
;*  36              NOP     0x2     ; [A_B] 
;*  38              MPYSP   .N2     BM4,B1,B4         ; [B_N2] |157|  ^ 
;*  39              NOP     0x3     ; [A_B] 
;*  42              MPYSP   .N2     B4,B4,B3          ; [B_N2] |157|  ^ 
;*     ||           ADDSP   .C2     BM7,B4,B1         ; [B_C] |157| 
;*  43              NOP     0x3     ; [A_B] 
;*  46              MPYSP   .M2     BM2,B3,B3         ; [B_M2] |157| 
;*     ||           MPYSP   .N2     B4,B3,B4          ; [B_N2] |157|  ^ 
;*  47              MPYSP   .N2     B3,B3,B1          ; [B_N2] |157| 
;*  48              NOP     0x2     ; [A_B] 
;*  50              ADDSP   .C2     B3,B1,B1          ; [B_C] |157| 
;*     ||           MPYSP   .N2     BM3,B4,B3         ; [B_N2] |157|  ^ 
;*  51              MPYSP   .N2     BM5,B1,B3         ; [B_N2] |157| 
;*  52              NOP     0x2     ; [A_B] 
;*  54              ADDSP   .C2     B3,B1,B1          ; [B_C] |157|  ^ 
;*  55              NOP     0x2     ; [A_B] 
;*  57              ADDSP   .C2     B3,B1,B1          ; [B_C] |157|  ^ 
;*  58              NOP     0x2     ; [A_B] 
;*  60              MPYSP   .N2     B1,B0,B0          ; [B_N2] |157|  ^ 
;*  61              NOP     0x3     ; [A_B] 
;*  64              LDD     .D1     *D2(1168),D0      ; [A_D1] [C1]
;*     ||           MPYSP   .N2     BM6,B0,B0         ; [B_N2] |157|  ^ 
;*  65              NOP     0x3     ; [A_B] 
;*  68      [!A1]   MV      .L2     BL3,B0            ; [B_L2] |162|  ^ 
;*  69      [ A0]   MVKU32  .L2     0x7f7fffff,B0     ; [B_L2] |165|  ^ [C1]
;*  70              STW     .D1X    B0,*D0[A8]        ; [A_D1] |526|  ^ 
;*     ||           ADDSP   .C2     B0,BM0,BM0        ; [B_C] |527| 
;*  71              LDW     .D1     *D1(12),B0        ; [A_D1] |511| 
;*     ||           ADDW    .L1     A8,0x1,A8         ; [A_L1] |511| 
;*  72              NOP     0x6     ; [A_B] 
;*  78              CMPGTW  .L1X    B0,A8,A0          ; [A_L1] |511| 
;*  79      [ A0]   B       .B1     ||$C$C2144||      ; [A_B] |511| 
;*  80              ; BRANCHCC OCCURS {||$C$C2144||}  ; [] |511| 
;*----------------------------------------------------------------------------*
||$C$L133||:    ; PIPED LOOP PROLOG
;** --------------------------------------------------------------------------*
||$C$L134||:    ; PIPED LOOP KERNEL
;          EXCLUSIVE CPU CYCLES: 71
           NOP             0x1               ; [A_B] 
           ADDW    .D1     D5,D0,D0          ; [A_D1] |513| <0,10>  ^ 
           LDB     .D1     *D7[D0],BL0       ; [A_D1] |513| <0,11>  ^ 
           NOP             0x4               ; [A_B] 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 517,column 11,is_stmt,isa 0
           CMPEQD  .L1     AL1,AL0,A0        ; [A_L1] |517| <0,16> 

   [ A0]   SUBW    .S2     BL0,BL2,BL5       ; [B_S2] |517| <0,17> 
||         SUBW    .L2     BL0,BL2,BL0       ; [B_L2] |517| <0,17>  ^ 

   [ A0]   VINTSP  .S2     BL5,B1            ; [B_S2] |517| <0,18> 
|| [!A0]   VINTSP  .L2     BL0,B0            ; [B_L2] |517| <0,18>  ^ 

           NOP             0x2               ; [A_B] 
   [!A0]   MPYSP   .N2     BM1,B0,B1         ; [B_N2] |517| <0,21>  ^ 
           NOP             0x3               ; [A_B] 
	.dwpsn	file "./inc/tidl_detectionOutput_int.h",line 135,column 25,is_stmt,isa 0
           MPYSP   .N2     B2,B1,B0          ; [B_N2] |135| <0,25>  ^ 
           NOP             0x3               ; [A_B] 
           VSPTRUNC .L2    B0,B1             ; [B_L2] |135| <0,29>  ^ 
           NOP             0x2               ; [A_B] 
	.dwpsn	file "./inc/tidl_detectionOutput_int.h",line 157,column 3,is_stmt,isa 0

           VCMPGTW .L2     B1,BL1,P0         ; [B_L2] |157| <0,32> 
||         SUBRW   .M2     B1,0,BL0          ; [B_M2] |157| <0,32> 
||         VINTSP  .S2     B1,B1             ; [B_S2] |157| <0,32>  ^ 

           CMPGEW  .L1X    B1,0xfffffff0,A1  ; [A_L1] |161| <0,33> 
||         SHLW    .L2     BL4,B1,BL5        ; [B_L2] |157| <0,33> 
||         SHRW    .S2     BL4,BL0,BL0       ; [B_S2] |157| <0,33> 
||         AND     .P2     P0,P1,P0          ; [B_P] |157| <0,33> 

           CMPGTW  .L1X    B1,0xe,A0         ; [A_L1] |164| <0,34> 
||         VSEL    .L2     P0,BL5,BL0,BL0    ; [B_L2] |157| <0,34> 

           VINTSP  .L2     BL0,B0            ; [B_L2] |157| <0,35> 
||         SUBSP   .C2     B0,B1,B1          ; [B_C] |157| <0,35>  ^ 

           NOP             0x2               ; [A_B] 
           MPYSP   .N2     BM4,B1,B4         ; [B_N2] |157| <0,38>  ^ 
           NOP             0x3               ; [A_B] 

           ADDSP   .C2     BM7,B4,B1         ; [B_C] |157| <0,42> 
||         MPYSP   .N2     B4,B4,B3          ; [B_N2] |157| <0,42>  ^ 

           NOP             0x3               ; [A_B] 

           MPYSP   .M2     BM2,B3,B3         ; [B_M2] |157| <0,46> 
||         MPYSP   .N2     B4,B3,B4          ; [B_N2] |157| <0,46>  ^ 

           MPYSP   .N2     B3,B3,B1          ; [B_N2] |157| <0,47> 
           NOP             0x2               ; [A_B] 

           ADDSP   .C2     B3,B1,B1          ; [B_C] |157| <0,50> 
||         MPYSP   .N2     BM3,B4,B3         ; [B_N2] |157| <0,50>  ^ 

           MPYSP   .N2     BM5,B1,B3         ; [B_N2] |157| <0,51> 
           NOP             0x2               ; [A_B] 
           ADDSP   .C2     B3,B1,B1          ; [B_C] |157| <0,54>  ^ 
           NOP             0x2               ; [A_B] 
           ADDSP   .C2     B3,B1,B1          ; [B_C] |157| <0,57>  ^ 
           NOP             0x2               ; [A_B] 
           MPYSP   .N2     B1,B0,B0          ; [B_N2] |157| <0,60>  ^ 
           NOP             0x3               ; [A_B] 

           LDD     .D1     *D2(1168),D0      ; [A_D1] <0,64> [C1]
||         MPYSP   .N2     BM6,B0,B0         ; [B_N2] |157| <0,64>  ^ 

           NOP             0x3               ; [A_B] 
	.dwpsn	file "./inc/tidl_detectionOutput_int.h",line 162,column 5,is_stmt,isa 0
   [!A1]   MV      .L2     BL3,B0            ; [B_L2] |162| <0,68>  ^ 
	.dwpsn	file "./inc/tidl_detectionOutput_int.h",line 165,column 5,is_stmt,isa 0
   [ A0]   MVKU32  .L2     0x7f7fffff,B0     ; [B_L2] |165| <0,69>  ^ [C1]
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 526,column 9,is_stmt,isa 0

           ADDSP   .C2     B0,BM0,BM0        ; [B_C] |527| <0,70> 
||         STW     .D1X    B0,*D0[A8]        ; [A_D1] |526| <0,70>  ^ 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 511,column 20,is_stmt,isa 0

           ADDW    .L1     A8,0x1,A8         ; [A_L1] |511| <0,71> 
||         LDW     .D1     *D1(12),B0        ; [A_D1] |511| <0,71> 
||         SLDW    .D2     *D3(0),AM0        ; [A_D2] |513| <1,0>  ^ 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 513,column 9,is_stmt,isa 0
           MPYWW   .N1     AM1,A8,D0         ; [A_N1] |513| <1,1> 
           NOP             0x3               ; [A_B] 

           SLDD    .D1     *D4(0),D7         ; [A_D1] |513| <1,5> 
||         ADDW    .D2     D6,D0,AM2         ; [A_D2] |513| <1,5> 

           MPYWW   .N1     AM2,AM0,D0        ; [A_N1] |513| <1,6>  ^ 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 511,column 20,is_stmt,isa 0
           CMPGTW  .L1X    B0,A8,A0          ; [A_L1] |511| <0,78> 
   [ A0]   B       .B1     ||$C$L134||       ; [A_B] |511| <0,79> 
;** --------------------------------------------------------------------------*
||$C$L135||:    ; PIPED LOOP EPILOG
;          EXCLUSIVE CPU CYCLES: 1
           PROTCLR                            ; [A_U] VB0
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 1

           B       .B1     ||$C$L137||       ; [A_B] 
||         MV      .M2     BM0,B0            ; [B_M2] 
||         MV      .D1     D2,A5             ; [A_D1] 
||         MV      .L2     BL3,B3            ; [B_L2] 
||         MV      .D2     D1,A6             ; [A_D2] 
||         MV      .L1X    B0,A8             ; [A_L1] 

           ; BRANCH OCCURS {||$C$L137||}     ; [] 
;** --------------------------------------------------------------------------*
||$C$L136||:    
;          EXCLUSIVE CPU CYCLES: 1
           MV      .L2     B3,B0             ; [B_L2] 
;** --------------------------------------------------------------------------*
||$C$L137||:    
;          EXCLUSIVE CPU CYCLES: 21
	.dwpsn	file "./inc/tidl_detectionOutput_int.h",line 237,column 3,is_stmt,isa 0
           VRCPSP  .S2     B0,BM0            ; [B_S2] |237| 
           MPYSP   .N2     B0,BM0,BM1        ; [B_N2] |237| 
           SUBSP   .C2     B7,BM1,BM1        ; [B_C] |237| 
           MPYSP   .N2     BM0,BM1,BM0       ; [B_N2] |237| 
           MPYSP   .N2     B0,BM0,BM1        ; [B_N2] |237| 
           SUBSP   .C2     B7,BM1,BM1        ; [B_C] |237| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 533,column 20,is_stmt,isa 0
           CMPGTW  .L1     A8,0,A0           ; [A_L1] |533| 
	.dwpsn	file "./inc/tidl_detectionOutput_int.h",line 237,column 3,is_stmt,isa 0

   [!A0]   B       .B1     ||$C$L140||       ; [A_B] |533| 
||         MPYSP   .N2     BM0,BM1,BM1       ; [B_N2] |237| 
||         ANDW    .D1     D10,A4,D0         ; [A_D1] |531| 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 533,column 20,is_stmt,isa 0
           ; BRANCHCC OCCURS {||$C$L140||}   ; [] |533| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 1
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 533,column 12,is_stmt,isa 0

           MVKU32  .L2     0x46fffe00,BM7    ; [B_L2] |545| 
||         MVKU32  .L1     0,A9              ; [A_L1] |533| 

;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*      Disqualified loop: Loop contains control code
;*----------------------------------------------------------------------------*
||$C$L138||:    
;          EXCLUSIVE CPU CYCLES: 8
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 537,column 11,is_stmt,isa 0
           LDW     .D1     *A6(16),AL0       ; [A_D1] |537| 
           CMPEQW  .L1     A9,AL0,A0         ; [A_L1] |537| 
   [ A0]   B       .B1     ||$C$L139||       ; [A_B] |537| 
           ; BRANCHCC OCCURS {||$C$L139||}   ; [] |537| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 54
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 542,column 9,is_stmt,isa 0
           LDD     .D1     *A5(1168),D1      ; [A_D1] |542| 
           LDUW    .D1     *D1[A9],BM0       ; [A_D1] |542| 

           MPYSP   .N2     BM1,BM0,B0        ; [B_N2] |542| 
||         LDUW    .D1     *A6(24),AL1       ; [A_D1] |542| 

           MV      .L1X    B0,AL0            ; [A_L1] |542| Define a twin register
           CMPLESP .L1     AL1,AL0,A0        ; [A_L1] |542| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 545,column 11,is_stmt,isa 0
   [ A0]   MPYSP   .N2     BM7,B0,BL0        ; [B_N2] |545| 
   [ A0]   LDD     .D1     *A5(1152),D1      ; [A_D1] |545| 
   [ A0]   VSPTRUNC .L2    BL0,B0            ; [B_L2] |545| 
   [ A0]   STH     .D1X    B0,*D1[D8]        ; [A_D1] |545| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 546,column 11,is_stmt,isa 0
   [ A0]   LDD     .D1     *A5(1176),D1      ; [A_D1] |546| 
           SHLW    .L1     A9,0x10,D2        ; [A_L1] |546| 
           ORW     .D1     D0,D2,D2          ; [A_D1] |546| 
   [ A0]   STW     .D1     D2,*D1[D8]        ; [A_D1] |546| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 547,column 11,is_stmt,isa 0
   [ A0]   LDD     .D1     *A5(1192),D1      ; [A_D1] |547| 
   [ A0]   ADDAW   .D1     D1,A9,D1          ; [A_D1] |547| 
   [ A0]   LDW     .D1     *D1(0),BL0        ; [A_D1] |547| 
   [ A0]   ADDW    .L2     BL0,0x1,B0        ; [B_L2] |547| 
   [ A0]   STW     .D1X    B0,*D1(0)         ; [A_D1] |547| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 548,column 11,is_stmt,isa 0
   [ A0]   LDW     .D1     *A6(12),A8        ; [A_D1] |548| 
   [ A0]   ADDW    .D1     D8,0x1,D8         ; [A_D1] |548| 
;** --------------------------------------------------------------------------*
||$C$L139||:    
;          EXCLUSIVE CPU CYCLES: 3
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 533,column 20,is_stmt,isa 0
           ADDW    .D1     A9,0x1,A9         ; [A_D1] |533| 
           CMPGTW  .L1     A8,A9,A0          ; [A_L1] |533| 
   [ A0]   B       .B1     ||$C$L138||       ; [A_B] |533| 
           ; BRANCHCC OCCURS {||$C$L138||}   ; [] |533| 
;** --------------------------------------------------------------------------*
||$C$L140||:    
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 473,column 27,is_stmt,isa 0
           ADDW    .D1     A3,0xffffffff,A3  ; [A_D1] |473| 

   [ A3]   B       .B1     ||$C$L127||       ; [A_B] |473| 
||         ADDD    .L2     B5,0x4,B5         ; [B_L2] |473| 

           ; BRANCHCC OCCURS {||$C$L127||}   ; [] |473| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 1
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 553,column 25,is_stmt,isa 0

           B       .B1     ||$C$L142||       ; [A_B] |553| 
||         MV      .D1     D8,A4             ; [A_D1] 

           ; BRANCH OCCURS {||$C$L142||}     ; [] |553| 
;** --------------------------------------------------------------------------*
||$C$L141||:    
;          EXCLUSIVE CPU CYCLES: 6
           LDW     .D1     *A6(12),A8        ; [A_D1] 
;** --------------------------------------------------------------------------*
||$C$L142||:    
;          EXCLUSIVE CPU CYCLES: 2
           CMPGTW  .L1     A8,0,A0           ; [A_L1] |553| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 553,column 20,is_stmt,isa 0

   [!A0]   B       .B1     ||$C$L146||       ; [A_B] |553| 
||         MVKU32  .L2     0,B2              ; [B_L2] 
||         MVKU32  .S2     0,B0              ; [B_S2] |553| 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 553,column 25,is_stmt,isa 0
           ; BRANCHCC OCCURS {||$C$L146||}   ; [] |553| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 7
           MV      .D1     A5,D3             ; [A_D1] 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 557,column 9,is_stmt,isa 0
           SLDD    .D1     *D3(1200),D1      ; [A_D1] |557| <0,1> [C1]
           MV      .L1X    B2,A1             ; [A_L1] 

           UNPROT          0x1               ; [A_U] 
||         MV      .D1     A6,D2             ; [A_D1] 
||         MV      .L2     B6,B1             ; [B_L2] 
||         MV      .L1X    B0,A8             ; [A_L1] 

;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*
;*      Loop found in file               : src/tidl_detectionOutput_score.c
;*      Loop source line                 : 553
;*      Loop opening brace source line   : 554
;*      Loop closing brace source line   : 564
;*      Known Minimum Trip Count         : 1                    
;*      Known Max Trip Count Factor      : 1
;*      Loop Carried Dependency Bound(^) : 25
;*      Unpartitioned Resource Bound     : 5
;*      Partitioned Resource Bound       : 5 (pre-sched)
;*
;*      Searching for software pipeline schedule at ...
;*         ii = 25 Unsafe schedule for irregular loop
;*         ii = 25 Unsafe schedule for irregular loop
;*         ii = 25 Did not find schedule
;*         ii = 26 Schedule found with 2 iterations in parallel
;*
;*      Partitioned Resource Bound(*)    : 8 (post-sched)
;*
;*      Constant Extension #0 Used [C0]  : 1
;*      Constant Extension #1 Used [C1]  : 3
;*
;*      Resource Partition (may include "post-sched" split/spill moves):
;*
;*                                                A-side   B-side
;*      .L units                                     0        0     
;*      .S units                                     0        0     
;*      .M units                                     0        0     
;*      .N units                                     0        0     
;*      .D units                                    10        -     
;*      .B units                                     1        -     
;*      .C units                                     -        0     
;*      .P units                                     -        0     
;*
;*      .M/.N units                                  0        0     
;*      .L/.S units                                  1        0     
;*      .L/.S/.C units                               0        0     
;*      .L/.S/.C/.M units                            0        0     
;*      .L/.S/.C/.M/.D units                         8        1     
;*
;*      .X cross paths                               3        0     
;*
;*      Bound(.D1 .D2 .D)                            5        -     
;*      Bound(.M .N .MN)                             0        0     
;*      Bound(.L .S .LS)                             1        0     
;*      Bound(.L .S .C .LS .LSC)                     1        0     
;*      Bound(.L .S .C .M .LS .LSC .LSCM)            1        0     
;*      Bound(.L .S .C .M .D .LS .LSC .LSCM .LSCMD)  4        1     
;*
;*      Register Usage Tables:
;*
;*      +----------------------------------+----------------------------------+
;*      |              ASIDE               |              BSIDE               |
;*      +----------------+--------+--------+----------------+--------+--------+
;*      |      "Axx"     |  "ALx" |  "AMx" |     "VBxx"     | "VBLx" | "VBMx" |
;*      +----------------|--------|--------|----------------|--------|--------+
;*      |0000000000111111|00000000|00000000|0000000000111111|00000000|00000000|
;*      |0123456789012345|01234567|01234567|0123456789012345|01234567|01234567|
;*      +----------------|--------|--------|----------------|--------|--------+
;*   0: | *      *       | *      |        | *              |        |        |
;*   1: | *      *       | *      |        | *              |        |        |
;*   2: | *      *       | *      |        | *              |        |        |
;*   3: | *      *       | *      |        | *              |        |        |
;*   4: | *      *       | *      |        | *              |        |        |
;*   5: | *      *       | *      |        | *              |        |        |
;*   6: | *      *       | *      |        | *              |        |        |
;*   7: | *      *       | *      |        | *              |**      |        |
;*   8: | *      *       | *      |        |**              |        |        |
;*   9: | *      *       | *      |        | *              |        |        |
;*  10: | *      *       | *      |        | *              |        |        |
;*  11: | *      *       | *      |        | *              |        |        |
;*  12: | *      *       | *      |        | *              |        |        |
;*  13: | *      *       | *      |        | *              |        |        |
;*  14: | *      *       | *      |        | *              |        |        |
;*  15: | *      *       | *      |        | *              |        |        |
;*  16: | *      *       | *      |        | *              |        |        |
;*  17: | *      *       | *      |        | *              |        |        |
;*  18: | *      *       | *      |        | *              |        |        |
;*  19: | *      *       | *      |        | *              |        |        |
;*  20: | *      *       | *      |        | *              |        |        |
;*  21: | *      *       | *      |        | *              |        |        |
;*  22: | *      *       | *      |        | *              |        |        |
;*  23: | *      *       | *      |        | *              |        |        |
;*  24: | *      *       |**      |        | *              |        |        |
;*  25: |**      *       | *      |        | *              |        |        |
;*      +----------------+--------+--------+----------------+--------+--------+
;*
;*      +----------------+                 +--------+       +-------+
;*      |      "Dxx"     |                 |  "Px"  |       |"CUCRx"|
;*      +----------------+                 +--------+       +-------+
;*      |0000000000111111|                 |00000000|       |0000   |
;*      |0123456789012345|                 |01234567|       |0123   |
;*      +----------------+                 +--------+       +-------+
;*   0: |**** *          |                 |        |       |       |
;*   1: |*******         |                 |        |       |       |
;*   2: |**** *          |                 |        |       |       |
;*   3: |**** *          |                 |        |       |       |
;*   4: |**** *          |                 |        |       |       |
;*   5: |**** *          |                 |        |       |       |
;*   6: |**** *          |                 |        |       |       |
;*   7: |**** *          |                 |        |       |       |
;*   8: |**** *          |                 |        |       |       |
;*   9: |**** *          |                 |        |       |       |
;*  10: |**** *          |                 |        |       |       |
;*  11: |**** *          |                 |        |       |       |
;*  12: |**** *          |                 |        |       |       |
;*  13: |**** *          |                 |        |       |       |
;*  14: |**** *          |                 |        |       |       |
;*  15: |******          |                 |        |       |       |
;*  16: |******          |                 |        |       |       |
;*  17: |******          |                 |        |       |       |
;*  18: |**** *          |                 |        |       |       |
;*  19: |**** *          |                 |        |       |       |
;*  20: |**** *          |                 |        |       |       |
;*  21: |**** *          |                 |        |       |       |
;*  22: |**** *          |                 |        |       |       |
;*  23: |**** *          |                 |        |       |       |
;*  24: |**** *          |                 |        |       |       |
;*  25: |******          |                 |        |       |       |
;*      +----------------+                 +--------+       +-------+
;*
;*      Done
;*
;*      Collapsed epilog stages       : 1
;*      Prolog not removed
;*      Collapsed prolog stages       : 0
;*
;*      Max amt of load speculation   : 0 bytes
;*
;*      Minimum safe trip count       : 1
;*      Min. prof. trip count  (est.) : 2
;*
;*      Mem bank conflicts/iter(est.) : { min 0.000, est 0.000, max 0.000 }
;*      Mem bank perf. penalty (est.) : 0.0%
;*
;*
;*      Total cycles (est.)         : 7 + trip_cnt * 26        
;*----------------------------------------------------------------------------*
;*        SINGLE SCHEDULED ITERATION
;*
;*        ||$C$C2068||:
;*   0      [ A1]   SLDD    .D2     *D3(1200),D0      ; [A_D2] |561|  ^ [C0]
;*     ||   [ A1]   SLDD    .D1     *D3(1192),D4      ; [A_D1] |561|  ^ [C1]
;*   1      [!A1]   SLDD    .D1     *D3(1200),D1      ; [A_D1] |557| [C1]
;*   2              NOP     0x4     ; [A_B] 
;*   6      [ A1]   ADDD    .D2     D0,A1,D5          ; [A_D2] |561|  ^ 
;*     ||   [ A1]   ADDD    .D1     A1,D4,AL1         ; [A_D1] |561|  ^ 
;*   7      [!A1]   STW     .D1X    B1,*D1[A8]        ; [A_D1] |557| 
;*     ||   [ A1]   ADDD    .D2     D5,0xfffffffc,D4  ; [A_D2] |561|  ^ 
;*     ||   [ A1]   ADDD    .L1     AL1,0xfffffffc,D6 ; [A_L1] |561|  ^ 
;*   8      [ A1]   LDW     .D2     *D4(0),BL0        ; [A_D2] |561|  ^ 
;*     ||   [ A1]   LDW     .D1     *D6(0),BL1        ; [A_D1] |561|  ^ 
;*   9              NOP     0x5     ; [A_B] 
;*  14      [ A1]   ADDW    .L2     BL1,BL0,B0        ; [B_L2] |561|  ^ 
;*  15      [ A1]   STW     .D1X    B0,*D0[A8]        ; [A_D1] |561|  ^ 
;*  16      [ A1]   LDD     .D1     *D3(1192),D4      ; [A_D1] |562|  ^ [C1]
;*  17              NOP     0x5     ; [A_B] 
;*  22      [ A1]   ADDD    .D1     A1,D4,D4          ; [A_D1] |562|  ^ 
;*  23      [ A1]   ADDD    .D1     D4,0xfffffffc,D4  ; [A_D1] |562|  ^ 
;*  24      [ A1]   STW     .D1X    B1,*D4(0)         ; [A_D1] |562|  ^ 
;*  25              LDW     .D1     *D2(12),AL0       ; [A_D1] |553| 
;*     ||           ADDD    .D2     A1,0x4,A1         ; [A_D2] |553|  ^ 
;*  26              NOP     0x4     ; [A_B] 
;*  30              ADDW    .D1     A8,0x1,A8         ; [A_D1] |553| 
;*  31              CMPGTW  .L1     AL0,A8,A0         ; [A_L1] |553| 
;*  32      [ A0]   B       .B1     ||$C$C2068||      ; [A_B] |553| 
;*  33              ; BRANCHCC OCCURS {||$C$C2068||}  ; [] |553| 
;*----------------------------------------------------------------------------*
||$C$L143||:    ; PIPED LOOP PROLOG
;** --------------------------------------------------------------------------*
||$C$L144||:    ; PIPED LOOP KERNEL
;          EXCLUSIVE CPU CYCLES: 26

   [!A1]   STW     .D1X    B1,*D1[A8]        ; [A_D1] |557| <0,7> 
|| [ A1]   ADDD    .D2     D5,0xfffffffc,D4  ; [A_D2] |561| <0,7>  ^ 
|| [ A1]   ADDD    .L1     AL1,0xfffffffc,D6 ; [A_L1] |561| <0,7>  ^ 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 561,column 9,is_stmt,isa 0

   [ A1]   LDW     .D2     *D4(0),BL0        ; [A_D2] |561| <0,8>  ^ 
|| [ A1]   LDW     .D1     *D6(0),BL1        ; [A_D1] |561| <0,8>  ^ 

           NOP             0x5               ; [A_B] 
   [ A1]   ADDW    .L2     BL1,BL0,B0        ; [B_L2] |561| <0,14>  ^ 
   [ A1]   STW     .D1X    B0,*D0[A8]        ; [A_D1] |561| <0,15>  ^ 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 562,column 9,is_stmt,isa 0
   [ A1]   LDD     .D1     *D3(1192),D4      ; [A_D1] |562| <0,16>  ^ [C1]
           NOP             0x5               ; [A_B] 
   [ A1]   ADDD    .D1     A1,D4,D4          ; [A_D1] |562| <0,22>  ^ 
   [ A1]   ADDD    .D1     D4,0xfffffffc,D4  ; [A_D1] |562| <0,23>  ^ 
   [ A1]   STW     .D1X    B1,*D4(0)         ; [A_D1] |562| <0,24>  ^ 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 553,column 25,is_stmt,isa 0

           ADDD    .D2     A1,0x4,A1         ; [A_D2] |553| <0,25>  ^ 
||         LDW     .D1     *D2(12),AL0       ; [A_D1] |553| <0,25> 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 561,column 9,is_stmt,isa 0

   [ A1]   SLDD    .D2     *D3(1200),D0      ; [A_D2] |561| <1,0>  ^ [C0]
|| [ A1]   SLDD    .D1     *D3(1192),D4      ; [A_D1] |561| <1,0>  ^ [C1]

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 557,column 9,is_stmt,isa 0
   [!A1]   SLDD    .D1     *D3(1200),D1      ; [A_D1] |557| <1,1> [C1]
           NOP             0x2               ; [A_B] 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 553,column 25,is_stmt,isa 0
           ADDW    .D1     A8,0x1,A8         ; [A_D1] |553| <0,30> 
           CMPGTW  .L1     AL0,A8,A0         ; [A_L1] |553| <0,31> 

   [ A0]   B       .B1     ||$C$L144||       ; [A_B] |553| <0,32> 
|| [ A1]   ADDD    .D2     D0,A1,D5          ; [A_D2] |561| <1,6>  ^ 
|| [ A1]   ADDD    .D1     A1,D4,AL1         ; [A_D1] |561| <1,6>  ^ 

;** --------------------------------------------------------------------------*
||$C$L145||:    ; PIPED LOOP EPILOG
;          EXCLUSIVE CPU CYCLES: 1
           PROTCLR                            ; [A_U] AL0
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 1

           MV      .L2     B1,B6             ; [B_L2] 
||         MV      .L1     AL0,A8            ; [A_L1] 
||         MV      .D1     D3,A5             ; [A_D1] 

;** --------------------------------------------------------------------------*
||$C$L146||:    
;          EXCLUSIVE CPU CYCLES: 9
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 566,column 5,is_stmt,isa 0
           LDD     .D1     *A5(1192),D0      ; [A_D1] |566| 
           ADDAW   .D1     D0,A8,D0          ; [A_D1] |566| 

           ADDD    .D1     D0,0xfffffffc,D0  ; [A_D1] |566| 
||         CMPGTW  .L1     A4,0,A0           ; [A_L1] 

   [!A0]   B       .B1     ||$C$L150||       ; [A_B] 
||         STW     .D1X    B6,*D0(0)         ; [A_D1] |566| 
||         MVKU32  .L2     0xff,B1           ; [B_L2] 
||         MVKU32  .S2     0,B0              ; [B_S2] 

           ; BRANCHCC OCCURS {||$C$L150||}   ; [] 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 4
           MV      .D1     A4,AL0            ; [A_D1] 

           EXT     .L1     AL0,0x20,0x20,A0  ; [A_L1] 
||         MV      .S1X    B0,A1             ; [A_S1] 

           NLCINIT .S1     A0,0x1,0          ; [A_S1] 
||         MV      .L1X    B0,D4             ; [A_L1] 
||         UNPROT          0x1               ; [A_U] 

           TICK                               ; [A_U] <0,0> 
||         MV      .L1X    B1,D5             ; [A_L1] 
||         MV      .D1     A5,D2             ; [A_D1] 

;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*
;*      Loop found in file               : src/tidl_detectionOutput_score.c
;*      Loop source line                 : 568
;*      Loop opening brace source line   : 569
;*      Loop closing brace source line   : 577
;*      Known Minimum Trip Count         : 1                    
;*      Known Max Trip Count Factor      : 1
;*      Loop Carried Dependency Bound(^) : 52
;*      Unpartitioned Resource Bound     : 10
;*      Partitioned Resource Bound       : 10 (pre-sched)
;*
;*      Searching for software pipeline schedule at ...
;*         ii = 52 Schedule found with 2 iterations in parallel
;*
;*      Partitioned Resource Bound(*)    : 13 (post-sched)
;*
;*      Constant Extension #0 Used [C0]  : 3
;*      Constant Extension #1 Used [C1]  : 6
;*
;*      Resource Partition (may include "post-sched" split/spill moves):
;*
;*                                                A-side   B-side
;*      .L units                                     0        0     
;*      .S units                                     0        0     
;*      .M units                                     0        0     
;*      .N units                                     0        0     
;*      .D units                                    20        -     
;*      .B units                                     1        -     
;*      .C units                                     -        0     
;*      .P units                                     -        0     
;*
;*      .M/.N units                                  0        0     
;*      .L/.S units                                  1        0     
;*      .L/.S/.C units                               0        0     
;*      .L/.S/.C/.M units                            0        0     
;*      .L/.S/.C/.M/.D units                         7        1     
;*
;*      .X cross paths                               2        0     
;*
;*      Bound(.D1 .D2 .D)                           10        -     
;*      Bound(.M .N .MN)                             0        0     
;*      Bound(.L .S .LS)                             1        0     
;*      Bound(.L .S .C .LS .LSC)                     1        0     
;*      Bound(.L .S .C .M .LS .LSC .LSCM)            1        0     
;*      Bound(.L .S .C .M .D .LS .LSC .LSCM .LSCMD)  6        1     
;*
;*      Register Usage Tables:
;*
;*      +----------------------------------+----------------------------------+
;*      |              ASIDE               |              BSIDE               |
;*      +----------------+--------+--------+----------------+--------+--------+
;*      |      "Axx"     |  "ALx" |  "AMx" |     "VBxx"     | "VBLx" | "VBMx" |
;*      +----------------|--------|--------|----------------|--------|--------+
;*      |0000000000111111|00000000|00000000|0000000000111111|00000000|00000000|
;*      |0123456789012345|01234567|01234567|0123456789012345|01234567|01234567|
;*      +----------------|--------|--------|----------------|--------|--------+
;*   0: | *              |        |        |                |        |        |
;*   1: | *              |        |        |                |        |        |
;*   2: | *              |        |        |                |        |        |
;*   3: | *              |        |        |                |        |        |
;*   4: | *              |        |        |                |        |        |
;*   5: | *              |        |        |                |        |        |
;*   6: | *              |        |        |                |        |        |
;*   7: | *              |        |        |                |        |        |
;*   8: | *              |        |        |                |        |        |
;*   9: | *              |        |        |                |        |        |
;*  10: | *              |        |        |                |        |        |
;*  11: | *              |        |        |                |        |        |
;*  12: | *              |        |        |                |        |        |
;*  13: |**              |        |        |                |        |        |
;*  14: |**              |        |        |                |        |        |
;*  15: |**              |        |        |                |        |        |
;*  16: |**              |        |        |                |        |        |
;*  17: |**              |        |        |                |        |        |
;*  18: |**              |        |        |                |        |        |
;*  19: |**              |        |        |                |        |        |
;*  20: |**              |        |        |                |        |        |
;*  21: |**              |        |        |                |        |        |
;*  22: |**              |        |        |                |        |        |
;*  23: | *              |        |        |                |        |        |
;*  24: | *              |        |        |                |        |        |
;*  25: | *              |        |        |                |        |        |
;*  26: | *              |        |        |                |        |        |
;*  27: | *              |        |        |                |        |        |
;*  28: | *              |        |        |                |        |        |
;*  29: | *              |        |        |                |        |        |
;*  30: | *              |        |        |                |        |        |
;*  31: | *              |        |        |                |        |        |
;*  32: | *              |        |        |                |        |        |
;*  33: | *              |        |        |                |        |        |
;*  34: | *              |        |        |                |        |        |
;*  35: | *              |        |        |                |        |        |
;*  36: | *              |        |        |*               |        |        |
;*  37: | *              |        |        |                |        |        |
;*  38: | *              |        |        |                |        |        |
;*  39: | *              |        |        |                |        |        |
;*  40: | *              |        |        |                |        |        |
;*  41: | *              |        |        |                |        |        |
;*  42: | *              |        |        |                |        |        |
;*  43: | *              |        |        |                |        |        |
;*  44: | *              |        |        |                |        |        |
;*  45: | *              |        |        |                |        |        |
;*  46: | *              |        |        |                |        |        |
;*  47: | *              |        |        |                |        |        |
;*  48: | *              |        |        |                |        |        |
;*  49: | *              |        |        |                |        |        |
;*  50: | *              |        |        |                |*       |        |
;*  51: | *              |        |        |*               |        |        |
;*      +----------------+--------+--------+----------------+--------+--------+
;*
;*      +----------------+                 +--------+       +-------+
;*      |      "Dxx"     |                 |  "Px"  |       |"CUCRx"|
;*      +----------------+                 +--------+       +-------+
;*      |0000000000111111|                 |00000000|       |0000   |
;*      |0123456789012345|                 |01234567|       |0123   |
;*      +----------------+                 +--------+       +-------+
;*   0: |  * **          |                 |        |       |       |
;*   1: |  * **          |                 |        |       |       |
;*   2: |  * **          |                 |        |       |       |
;*   3: |  * **          |                 |        |       |       |
;*   4: |  * **          |                 |        |       |       |
;*   5: |  * **          |                 |        |       |       |
;*   6: |* * **          |                 |        |       |       |
;*   7: |* * **          |                 |        |       |       |
;*   8: |  * **          |                 |        |       |       |
;*   9: |  * **          |                 |        |       |       |
;*  10: |  * **          |                 |        |       |       |
;*  11: |  * **          |                 |        |       |       |
;*  12: |  * **          |                 |        |       |       |
;*  13: |  * **          |                 |        |       |       |
;*  14: |* * **          |                 |        |       |       |
;*  15: |******          |                 |        |       |       |
;*  16: | ** **          |                 |        |       |       |
;*  17: | ** **          |                 |        |       |       |
;*  18: | ** **          |                 |        |       |       |
;*  19: | ** **          |                 |        |       |       |
;*  20: | ** **          |                 |        |       |       |
;*  21: |******          |                 |        |       |       |
;*  22: |******          |                 |        |       |       |
;*  23: | ** **          |                 |        |       |       |
;*  24: | ** **          |                 |        |       |       |
;*  25: | ** **          |                 |        |       |       |
;*  26: | ** **          |                 |        |       |       |
;*  27: | ** **          |                 |        |       |       |
;*  28: |*** **          |                 |        |       |       |
;*  29: |*******         |                 |        |       |       |
;*  30: | ** ***         |                 |        |       |       |
;*  31: | ** **          |                 |        |       |       |
;*  32: | ** **          |                 |        |       |       |
;*  33: | ** **          |                 |        |       |       |
;*  34: | ** **          |                 |        |       |       |
;*  35: |******          |                 |        |       |       |
;*  36: |******          |                 |        |       |       |
;*  37: | ** **          |                 |        |       |       |
;*  38: | ** **          |                 |        |       |       |
;*  39: | ** **          |                 |        |       |       |
;*  40: | ** **          |                 |        |       |       |
;*  41: | ** **          |                 |        |       |       |
;*  42: | ** **          |                 |        |       |       |
;*  43: |*** **          |                 |        |       |       |
;*  44: |* * **          |                 |        |       |       |
;*  45: |* * **          |                 |        |       |       |
;*  46: |* * **          |                 |        |       |       |
;*  47: |* * **          |                 |        |       |       |
;*  48: |* * **          |                 |        |       |       |
;*  49: |* * **          |                 |        |       |       |
;*  50: |* * **          |                 |        |       |       |
;*  51: |* * **          |                 |        |       |       |
;*      +----------------+                 +--------+       +-------+
;*
;*      Done
;*
;*      Collapsed epilog stages       : 1
;*      Prolog not removed
;*      Collapsed prolog stages       : 0
;*
;*      Max amt of load speculation   : 0 bytes
;*
;*      Minimum safe trip count       : 1
;*      Min. prof. trip count  (est.) : 2
;*
;*      Mem bank conflicts/iter(est.) : { min 0.000, est 0.000, max 0.000 }
;*      Mem bank perf. penalty (est.) : 0.0%
;*
;*
;*      Total cycles (est.)         : 1 + trip_cnt * 52        
;*----------------------------------------------------------------------------*
;*        SINGLE SCHEDULED ITERATION
;*
;*        ||$C$C1978||:
;*   0              TICK                               ; [A_U] 
;*   1              LDD     .D1     *D2(1176),D0      ; [A_D1] |570|  ^ [C1]
;*   2              NOP     0x5     ; [A_B] 
;*   7              ADDD    .D1     A1,D0,D0          ; [A_D1] |570|  ^ 
;*   8              LDW     .D1     *D0(0),A0         ; [A_D1] |570|  ^ 
;*   9              NOP     0x1     ; [A_B] 
;*  10              LDD     .D2     *D2(1200),D0      ; [A_D2] |574| [C0]
;*     ||           LDD     .D1     *D2(1192),D3      ; [A_D1] |574| [C1]
;*  11              NOP     0x3     ; [A_B] 
;*  14              SHRW    .L1     A0,0x10,D0        ; [A_L1] |574|  ^ 
;*  15              ANDW    .D1     D5,D0,D1          ; [A_D1] |574|  ^ 
;*  16              LDW     .D2     *D0[D1],D0        ; [A_D2] |574|  ^ 
;*     ||           LDW     .D1     *D3[D1],D3        ; [A_D1] |574|  ^ 
;*  17              LDD     .D1     *D2(1184),D3      ; [A_D1] |574| [C1]
;*  18              NOP     0x4     ; [A_B] 
;*  22              ADDW    .D1     D3,D0,D0          ; [A_D1] |574|  ^ 
;*  23              LDD     .D2     *D2(1152),D0      ; [A_D2] |571| [C0]
;*     ||           STW     .D1     A0,*D3[D0]        ; [A_D1] |574|  ^ 
;*  24              LDD     .D2     *D2(1200),D0      ; [A_D2] |575|  ^ [C0]
;*     ||           LDD     .D1     *D2(1192),D3      ; [A_D1] |575|  ^ [C1]
;*  25              NOP     0x4     ; [A_B] 
;*  29              ADDD    .D1     D4,D0,D6          ; [A_D1] |571| 
;*  30              LDW     .D2     *D0[D1],D0        ; [A_D2] |575|  ^ 
;*     ||           LDW     .D1     *D3[D1],D3        ; [A_D1] |575|  ^ 
;*  31              LDUH    .D2     *D6(0),B0         ; [A_D2] |571| 
;*     ||           LDD     .D1     *D2(1160),D3      ; [A_D1] |575| [C1]
;*  32              NOP     0x4     ; [A_B] 
;*  36              ADDW    .D1     D3,D0,D0          ; [A_D1] |575|  ^ 
;*  37              STH     .D1X    B0,*D3[D0]        ; [A_D1] |575|  ^ 
;*  38              LDD     .D1     *D2(1192),D0      ; [A_D1] |576|  ^ [C1]
;*  39              NOP     0x5     ; [A_B] 
;*  44              ADDAW   .D1     D0,D1,D0          ; [A_D1] |576|  ^ 
;*  45              LDW     .D1     *D0(0),BL0        ; [A_D1] |576|  ^ 
;*  46              NOP     0x5     ; [A_B] 
;*  51              ADDW    .L2     BL0,0x1,B0        ; [B_L2] |576|  ^ 
;*  52              STW     .D1X    B0,*D0(0)         ; [A_D1] |576|  ^ 
;*     ||           ADDD    .L1     A1,0x4,A1         ; [A_L1] |568| 
;*     ||           ADDD    .D2     D4,0x2,D4         ; [A_D2] |568| 
;*     ||           BNL     .B1     ||$C$C1978||      ; [A_B] |568| 
;*  53              ; BRANCHCC OCCURS {||$C$C1978||}  ; [] |568| 
;*----------------------------------------------------------------------------*
||$C$L147||:    ; PIPED LOOP PROLOG
;** --------------------------------------------------------------------------*
||$C$L148||:    ; PIPED LOOP KERNEL
;          EXCLUSIVE CPU CYCLES: 52
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 570,column 24,is_stmt,isa 0
           LDD     .D1     *D2(1176),D0      ; [A_D1] |570| <0,1>  ^ [C1]
           NOP             0x5               ; [A_B] 
           ADDD    .D1     A1,D0,D0          ; [A_D1] |570| <0,7>  ^ 
           LDW     .D1     *D0(0),A0         ; [A_D1] |570| <0,8>  ^ 
           NOP             0x1               ; [A_B] 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 574,column 7,is_stmt,isa 0

           LDD     .D2     *D2(1200),D0      ; [A_D2] |574| <0,10> [C0]
||         LDD     .D1     *D2(1192),D3      ; [A_D1] |574| <0,10> [C1]

           NOP             0x3               ; [A_B] 
           SHRW    .L1     A0,0x10,D0        ; [A_L1] |574| <0,14>  ^ 
           ANDW    .D1     D5,D0,D1          ; [A_D1] |574| <0,15>  ^ 

           LDW     .D2     *D0[D1],D0        ; [A_D2] |574| <0,16>  ^ 
||         LDW     .D1     *D3[D1],D3        ; [A_D1] |574| <0,16>  ^ 

           LDD     .D1     *D2(1184),D3      ; [A_D1] |574| <0,17> [C1]
           NOP             0x4               ; [A_B] 
           ADDW    .D1     D3,D0,D0          ; [A_D1] |574| <0,22>  ^ 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 571,column 22,is_stmt,isa 0

           LDD     .D2     *D2(1152),D0      ; [A_D2] |571| <0,23> [C0]
||         STW     .D1     A0,*D3[D0]        ; [A_D1] |574| <0,23>  ^ 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 575,column 7,is_stmt,isa 0

           LDD     .D2     *D2(1200),D0      ; [A_D2] |575| <0,24>  ^ [C0]
||         LDD     .D1     *D2(1192),D3      ; [A_D1] |575| <0,24>  ^ [C1]

           NOP             0x4               ; [A_B] 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 571,column 22,is_stmt,isa 0
           ADDD    .D1     D4,D0,D6          ; [A_D1] |571| <0,29> 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 575,column 7,is_stmt,isa 0

           LDW     .D2     *D0[D1],D0        ; [A_D2] |575| <0,30>  ^ 
||         LDW     .D1     *D3[D1],D3        ; [A_D1] |575| <0,30>  ^ 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 571,column 22,is_stmt,isa 0

           LDUH    .D2     *D6(0),B0         ; [A_D2] |571| <0,31> 
||         LDD     .D1     *D2(1160),D3      ; [A_D1] |575| <0,31> [C1]

           NOP             0x4               ; [A_B] 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 575,column 7,is_stmt,isa 0
           ADDW    .D1     D3,D0,D0          ; [A_D1] |575| <0,36>  ^ 
           STH     .D1X    B0,*D3[D0]        ; [A_D1] |575| <0,37>  ^ 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 576,column 7,is_stmt,isa 0
           LDD     .D1     *D2(1192),D0      ; [A_D1] |576| <0,38>  ^ [C1]
           NOP             0x5               ; [A_B] 
           ADDAW   .D1     D0,D1,D0          ; [A_D1] |576| <0,44>  ^ 
           LDW     .D1     *D0(0),BL0        ; [A_D1] |576| <0,45>  ^ 
           NOP             0x5               ; [A_B] 
           ADDW    .L2     BL0,0x1,B0        ; [B_L2] |576| <0,51>  ^ 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 568,column 25,is_stmt,isa 0

           ADDD    .L1     A1,0x4,A1         ; [A_L1] |568| <0,52> 
||         ADDD    .D2     D4,0x2,D4         ; [A_D2] |568| <0,52> 
||         BNL     .B1     ||$C$L148||       ; [A_B] |568| <0,52> 
||         STW     .D1X    B0,*D0(0)         ; [A_D1] |576| <0,52>  ^ 
||         TICK                               ; [A_U] <1,0> 

;** --------------------------------------------------------------------------*
||$C$L149||:    ; PIPED LOOP EPILOG
;          EXCLUSIVE CPU CYCLES: 1
           PROTCLR                            ; [A_U] 
;** --------------------------------------------------------------------------*
||$C$L150||:    
;          EXCLUSIVE CPU CYCLES: 9
           VLD64B  .D1     *SP(16),VB14      ; [A_D1] 
	.dwcfi	restore_reg, 62

           LDD     .D2     *SP(144),A12      ; [A_D2] 
||         VLD64B  .D1     *SP(80),VB15      ; [A_D1] 
	.dwcfi	restore_reg, 12
	.dwcfi	restore_reg, 63

           LDD     .D1     *SP(160),A10      ; [A_D1] 
||         LDD     .D2     *SP(152),A11      ; [A_D2] 
	.dwcfi	restore_reg, 10
	.dwcfi	restore_reg, 11

           LDD     .D1     *SP(176),A8       ; [A_D1] 
||         LDD     .D2     *SP(168),A9       ; [A_D2] 

	.dwcfi	restore_reg, 8
	.dwcfi	restore_reg, 9
$C$DW$42	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$42, DW_AT_low_pc(0x00)
	.dwattr $C$DW$42, DW_AT_TI_return


           RET     .B1     ; [A_B] 
||         ADDD    .D1     SP,0xa8,SP        ; [A_D1] 

	.dwcfi	cfa_offset, 0
           ; RETURN OCCURS {RP}              ; [] 
	.dwattr $C$DW$37, DW_AT_TI_end_file("src/tidl_detectionOutput_score.c")
	.dwattr $C$DW$37, DW_AT_TI_end_line(0x245)
	.dwattr $C$DW$37, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$37

	.sect	".text:_Z25TIDL_findValidLocation_cnIsEiP26sTIDL_DetectOutputParams_tP29sTIDL_ALgDetectOutputParams_tPf"
	.clink
	.global	||_Z25TIDL_findValidLocation_cnIsEiP26sTIDL_DetectOutputParams_tP29sTIDL_ALgDetectOutputParams_tPf||

$C$DW$43	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$43, DW_AT_name("int TIDL_findValidLocation_cn")
	.dwattr $C$DW$43, DW_AT_low_pc(||_Z25TIDL_findValidLocation_cnIsEiP26sTIDL_DetectOutputParams_tP29sTIDL_ALgDetectOutputParams_tPf||)
	.dwattr $C$DW$43, DW_AT_high_pc(0x00)
	.dwattr $C$DW$43, DW_AT_linkage_name("_Z25TIDL_findValidLocation_cnIsEiP26sTIDL_DetectOutputParams_tP29sTIDL_ALgDetectOutputParams_tPf")
	.dwattr $C$DW$43, DW_AT_external
	.dwattr $C$DW$43, DW_AT_decl_file("src/tidl_detectionOutput_score.c")
	.dwattr $C$DW$43, DW_AT_decl_line(0x6e)
	.dwattr $C$DW$43, DW_AT_decl_column(0x09)
	.dwattr $C$DW$43, DW_AT_TI_max_frame_size(0xf0)
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 113,column 1,is_stmt,address ||_Z25TIDL_findValidLocation_cnIsEiP26sTIDL_DetectOutputParams_tP29sTIDL_ALgDetectOutputParams_tPf||,isa 0

	.dwfde $C$DW$CIE, ||_Z25TIDL_findValidLocation_cnIsEiP26sTIDL_DetectOutputParams_tP29sTIDL_ALgDetectOutputParams_tPf||
$C$DW$44	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$44, DW_AT_name("params")
	.dwattr $C$DW$44, DW_AT_location[DW_OP_reg4]

$C$DW$45	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$45, DW_AT_name("algDetLyrParams")
	.dwattr $C$DW$45, DW_AT_location[DW_OP_reg5]

$C$DW$46	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$46, DW_AT_name("priorData")
	.dwattr $C$DW$46, DW_AT_location[DW_OP_reg6]


;******************************************************************************
;* FUNCTION NAME: int TIDL_findValidLocation_cn<short>(sTIDL_DetectOutputParams_t *, sTIDL_ALgDetectOutputParams_t *, float *)*
;*                                                                            *
;*   Regs Modified     : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,A14,   *
;*                           A15,VB0,VB1,VB2,VB3,VB4,VB5,VB6,VB7,VB8,VB9,VB10,*
;*                           VB11,VB12,VB13,VB14,VB15,AL0,AL1,AL2,AL3,AL4,AL5,*
;*                           AL6,AL7,AM0,AM1,AM2,AM3,AM4,AM5,AM6,AM7,D0,D1,D2,*
;*                           D3,D4,D5,D6,D7,D8,D9,D10,D11,D12,D13,D14,SP,VBL0,*
;*                           VBL1,VBL2,VBL3,VBL4,VBL5,VBL6,VBL7,VBM0,VBM1,    *
;*                           VBM2,VBM3,VBM4,VBM5,VBM6,VBM7,P0,P1,P2,P3,P4,P5, *
;*                           P6,P7,CUCR0,CUCR1,CUCR2,CUCR3                    *
;*   Regs Used         : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,A14,   *
;*                           A15,VB0,VB1,VB2,VB3,VB4,VB5,VB6,VB7,VB8,VB9,VB10,*
;*                           VB11,VB12,VB13,VB14,VB15,AL0,AL1,AL2,AL3,AL4,AL5,*
;*                           AL6,AL7,AM0,AM1,AM2,AM3,AM4,AM5,AM6,AM7,D0,D1,D2,*
;*                           D3,D4,D5,D6,D7,D8,D9,D10,D11,D12,D13,D14,SP,VBL0,*
;*                           VBL1,VBL2,VBL3,VBL4,VBL5,VBL6,VBL7,VBM0,VBM1,    *
;*                           VBM2,VBM3,VBM4,VBM5,VBM6,VBM7,P0,P1,P2,P3,P4,P5, *
;*                           P6,P7,CUCR0,CUCR1,CUCR2,CUCR3                    *
;*   Local Frame Size  : 0 Args + 40 Auto + 200 Save = 240 byte               *
;******************************************************************************
||_Z25TIDL_findValidLocation_cnIsEiP26sTIDL_DetectOutputParams_tP29sTIDL_ALgDetectOutputParams_tPf||:
;** --------------------------------------------------------------------------*
;* D0    assigned to $O$C16
;* VBM3  assigned to $O$C17
;* VBM0  assigned to $O$C18
;* VB2   assigned to $O$C19
;* A0    assigned to $O$C20
;* AM0   assigned to $O$C21
;* VB10  assigned to $O$C22
;* D3    assigned to $O$C23
;* A0    assigned to $O$C24
;* A3    assigned to $O$C25
;* A10   assigned to $O$C27
;* A1    assigned to $O$U125
;* VB0   assigned to $O$U125
;* AL0   assigned to $O$v3
;* AL0   assigned to $O$v3
;* VB3   assigned to $O$v2
;* A1    assigned to $O$v2
;* VB0   assigned to $O$K28
;* D14   assigned to $O$U53
;* VB5   assigned to $O$U60
;* VB3   assigned to $O$U65
;* D10   assigned to $O$U70
;* D1    assigned to $O$U102
;* A14   assigned to $O$U138
;* A0    assigned to $O$U133
;* A10   assigned to $O$U130
;* D2    assigned to $O$U175
;* D7    assigned to $O$U172
;* D0    assigned to $O$U85
;* D7    assigned to $O$U211
;* D10   assigned to $O$U233
;* AL0   assigned to $O$U238
;* A8    assigned to $O$U242
;* VBL6  assigned to $O$K253
;* VBL5  assigned to $O$K255
;* D0    assigned to $O$U273
;* A6    assigned to $O$U271
;* A15   assigned to $O$U280
;* A13   assigned to $O$U261
;* A12   assigned to $O$U259
;* VBM2  assigned to $O$K306
;* VBM1  assigned to $O$K333
;* VB9   assigned to $O$K329
;* VB8   assigned to $O$K317
;* VB7   assigned to $O$K325
;* VB6   assigned to $O$K321
;* VB4   assigned to $O$K3
;* VBL4  assigned to $O$K311
;* VB2   assigned to $O$U51
;* D13   assigned to $O$U84
;* D8    assigned to $O$U219
;* D6    assigned to $O$U270
;* D5    assigned to $O$U260
;* A3    assigned to $O$L1
;* A2    assigned to $O$L2
;* A0    assigned to $O$L3
;* A14   assigned to $O$v5
;* AL1   assigned to $O$v4
;* VB15  assigned to $O$Lr593$upperBoundThSoftMax
;* A13   assigned to $O$Lr602$upperBoundThSigmoid
;* VB12  assigned to $O$Lr604$topMLocal
;* VB14  assigned to $O$Lr605$anchorBox
;* VBL3  assigned to $O$Lr3$maxInit
;* VB1   assigned to $O$Lr580$i
;* A8    assigned to $O$Lr421$totalCnt
;* A9    assigned to $O$Lr56$totalCnt
;* A7    assigned to $O$Lr297$i
;* A12   assigned to $O$Lr533$onebyqFact
;* AM3   assigned to $O$Lr550$linePitch
;* A15   assigned to $O$Lr561$chPitch
;* A4    assigned to $O$Lr302$i3
;* A9    assigned to $O$Lr305$i4
;* VB6   assigned to $O$Lr419$curClassValidCnt
;* A11   assigned to $O$Lr514$curPartIndex
;* D12   assigned to $O$Lr473$curPlaneConf
;* D11   assigned to $O$Lr492$curPlaneObjConf
;* A6    assigned to $O$Lr392$cury
;* VB6   assigned to $O$Lr5$curClassValidCnt
;* A2    assigned to $O$Lr401$curx
;* D9    assigned to $O$Lr382$curPlaneConf
;* A6    assigned to $O$Lr316$cury
;* VB6   assigned to $O$Lr343$curClassValidCnt
;* VB0   assigned to $O$Lr345$totalCnt
;* A2    assigned to $O$Lr325$curx
;* A9    assigned to $O$Lr4$totalCnt
;* VB13  assigned to $O$Lr14$i
;* VB5   assigned to $O$Lr268$onebyqFact
;* VB0   assigned to $O$Lr274$numPriors
;* A4    assigned to $O$Lr6$classStride
;* VB11  assigned to $O$Lr7$anchorStride
;* A7    assigned to $O$Lr19$i3
;* D9    assigned to $O$Lr249$curPartIndex
;* A10   assigned to $O$Lr22$cury
;* A11   assigned to $O$Lr31$curx
;* VB2   assigned to $O$Lr154$maxConfObj
;* VB1   assigned to $O$Lr158$minConfObj
;* A1    assigned to $O$Lr147$i4
;* VB2   assigned to $O$Lr8$maxConfObj
;* VB1   assigned to $O$Lr9$minConfObj
;* VB0   assigned to $O$Lr223$bckVal
;* AL0   assigned to $O$Lr10$bckVal
;* VBL2  assigned to $O$Lr610$bckVal
;* VBM3  assigned to $O$Lr110$min_obj_f
;* VBM0  assigned to $O$Lr108$max_obj_f
;* VB1   assigned to $O$Lr114$yI
;* VBM3  assigned to $O$Lr11$ePwX
;* VB0   assigned to $O$Lr71$yI
;* VBM0  assigned to $O$Lr12$ePwX
;* A5    assigned to algDetLyrParams
$C$DW$47	.dwtag  DW_TAG_variable
	.dwattr $C$DW$47, DW_AT_name("algDetLyrParams")
	.dwattr $C$DW$47, DW_AT_type(*$C$DW$T$146)
	.dwattr $C$DW$47, DW_AT_location[DW_OP_reg5]

$C$DW$48	.dwtag  DW_TAG_variable
	.dwattr $C$DW$48, DW_AT_name("params")
	.dwattr $C$DW$48, DW_AT_type(*$C$DW$T$144)
	.dwattr $C$DW$48, DW_AT_location[DW_OP_bregx 0x6f 48]

	.dwcfi	cfa_offset, 0
;          EXCLUSIVE CPU CYCLES: 24

           MV      .L1     A4,A8             ; [A_L1] |113| 
||         STD     .D1     A8,*SP(8)         ; [A_D1] 
||         STD     .D2X    A9,*SP++(-240)    ; [A_D2] 
	.dwcfi	save_reg_to_mem, 8, 8
	.dwcfi	cfa_offset, 240
	.dwcfi	save_reg_to_mem, 9, -240

           LDUW    .D1     *A8(24),A5        ; [A_D1] |118| 
||         STD     .D2X    A13,*SP(208)      ; [A_D2] 
||         MV      .L1     A5,A9             ; [A_L1] |113| 
	.dwcfi	save_reg_to_mem, 13, 208

           VST64B  .D2     VB14,*SP(56)      ; [A_D2] 
||         STD     .D1     A10,*SP(232)      ; [A_D1] 
	.dwcfi	save_reg_to_mem, 62, 56
	.dwcfi	save_reg_to_mem, 10, 232

           STD     .D1     A15,*SP(192)      ; [A_D1] 
||         STD     .D2X    A14,*SP(200)      ; [A_D2] 
	.dwcfi	save_reg_to_mem, 15, 192
	.dwcfi	save_reg_to_mem, 14, 200

           VST64B  .D2     VB15,*SP(120)     ; [A_D2] 
||         MVC     .S1     RP,A13            ; [A_S1] 
||         STD     .D1     A12,*SP(216)      ; [A_D1] 
	.dwcfi	save_reg_to_mem, 63, 120
	.dwcfi	save_reg_to_mem, 12, 216

           MVKU32  .L1     0x3f800000,A10    ; [A_L1] |118| 
||         STD     .D1     A11,*SP(224)      ; [A_D1] 
||         STD     .D2X    A4,*SP(48)        ; [A_D2] |113| 

	.dwcfi	save_reg_to_mem, 11, 224
$C$DW$49	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$49, DW_AT_low_pc(0x00)
	.dwattr $C$DW$49, DW_AT_name("__c7xabi_divf")
	.dwattr $C$DW$49, DW_AT_TI_call


           CALL    .B1     ||__c7xabi_divf|| ; [A_B] |118| 
||         STD     .D1     A13,*SP(184)      ; [A_D1] 
||         MV      .D2     A10,A4            ; [A_D2] |118| 
||         MV      .L2X    A6,B14            ; [B_L2] |113| 

	.dwcfi	save_reg_to_mem, 4101, 184
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 118,column 29,is_stmt,isa 0
$C$RL0:    ; CALL OCCURS (||__c7xabi_divf||) arg:{A4,A5} ret:{A4}  ; [] |118| 
           MV      .L2X    A4,BM1            ; [B_L2] |118| 
           MV      .L2X    A10,BM0           ; [B_L2] |118| Define a twin register
           SUBSP   .C2     BM1,BM0,B15       ; [B_C] |118| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 119,column 29,is_stmt,isa 0
$C$DW$50	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$50, DW_AT_low_pc(0x00)
	.dwattr $C$DW$50, DW_AT_name("logf")
	.dwattr $C$DW$50, DW_AT_TI_call


           CALL    .B1     ||logf||          ; [A_B] |119| 
||         MV      .L1X    B15,A4            ; [A_L1] |119| 

$C$RL1:    ; CALL OCCURS (||logf||) arg:{A4} ret:{A4}  ; [] |119| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 113,column 1,is_stmt,isa 0
           MV      .D1     A9,A5             ; [A_D1] |113| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 119,column 29,is_stmt,isa 0

           LDW     .D1     *A5(1304),AL1     ; [A_D1] |133| 
||         MV      .D2     A8,A4             ; [A_D2] |133| 
||         MV      .L1     A4,AL0            ; [A_L1] |119| 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 126,column 21,is_stmt,isa 0

           LDW     .D1     *A4(12),A1        ; [A_D1] |146| 
||         LDW     .D2     *A5(1324),A2      ; [A_D2] |126| 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 130,column 19,is_stmt,isa 0

           CMPEQW  .L1     AL1,0x1,A0        ; [A_L1] |133| 
||         MVK32   .L2     0xffff8000,BL3    ; [B_L2] |130| 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 135,column 5,is_stmt,isa 0

           CMPGTW  .L1     A1,0,A0           ; [A_L1] |146| 
|| [ A0]   MVK32   .L2     0xffffff80,BL3    ; [B_L2] |135| 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 119,column 29,is_stmt,isa 0

   [!A0]   B       .B1     ||$C$L154||       ; [A_B] |146| 
||         MV      .M2X    A2,B12            ; [B_M2] |135| 
||         VXORW   .L1     AL0,0x80000000,A13 ; [A_L1] |119| 
||         MVKU32  .L2     0,B1              ; [B_L2] |146| 
||         MVKU32  .S2     0,B0              ; [B_S2] 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 146,column 15,is_stmt,isa 0
           ; BRANCHCC OCCURS {||$C$L154||}   ; [] |146| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 8
           MV      .D1     A5,D2             ; [A_D1] 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 148,column 5,is_stmt,isa 0
           SLDD    .D1     *D2(1192),D0      ; [A_D1] |148| <0,0>  ^ [C1]

           MV      .D1     A4,A1             ; [A_D1] 
||         MV      .L1X    B1,A8             ; [A_L1] 

           STW     .D1X    B0,*D0[A8]        ; [A_D1] |148| <0,6>  ^ 
||         MV      .D2     A1,D1             ; [A_D2] 
||         UNPROT          0x1               ; [A_U] 

;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*
;*      Loop found in file               : src/tidl_detectionOutput_score.c
;*      Loop source line                 : 146
;*      Loop opening brace source line   : 147
;*      Loop closing brace source line   : 149
;*      Known Minimum Trip Count         : 1                    
;*      Known Max Trip Count Factor      : 1
;*      Loop Carried Dependency Bound(^) : 7
;*      Unpartitioned Resource Bound     : 2
;*      Partitioned Resource Bound       : 2 (pre-sched)
;*
;*      Searching for software pipeline schedule at ...
;*         ii = 7  Unsafe schedule for irregular loop
;*         ii = 7  Unsafe schedule for irregular loop
;*         ii = 7  Unsafe schedule for irregular loop
;*         ii = 7  Did not find schedule
;*         ii = 8  Unsafe schedule for irregular loop
;*         ii = 8  Unsafe schedule for irregular loop
;*         ii = 8  Unsafe schedule for irregular loop
;*         ii = 8  Did not find schedule
;*         ii = 9  Schedule found with 2 iterations in parallel
;*
;*      Partitioned Resource Bound(*)    : 2 (post-sched)
;*
;*      Constant Extension #0 Used [C0]  : 0
;*      Constant Extension #1 Used [C1]  : 1
;*
;*      Resource Partition (may include "post-sched" split/spill moves):
;*
;*                                                A-side   B-side
;*      .L units                                     0        0     
;*      .S units                                     0        0     
;*      .M units                                     0        0     
;*      .N units                                     0        0     
;*      .D units                                     3        -     
;*      .B units                                     1        -     
;*      .C units                                     -        0     
;*      .P units                                     -        0     
;*
;*      .M/.N units                                  0        0     
;*      .L/.S units                                  1        0     
;*      .L/.S/.C units                               0        0     
;*      .L/.S/.C/.M units                            0        0     
;*      .L/.S/.C/.M/.D units                         1        0     
;*
;*      .X cross paths                               2*       0     
;*
;*      Bound(.D1 .D2 .D)                            2*       -     
;*      Bound(.M .N .MN)                             0        0     
;*      Bound(.L .S .LS)                             1        0     
;*      Bound(.L .S .C .LS .LSC)                     1        0     
;*      Bound(.L .S .C .M .LS .LSC .LSCM)            1        0     
;*      Bound(.L .S .C .M .D .LS .LSC .LSCM .LSCMD)  1        0     
;*
;*      Register Usage Tables:
;*
;*      +----------------------------------+----------------------------------+
;*      |              ASIDE               |              BSIDE               |
;*      +----------------+--------+--------+----------------+--------+--------+
;*      |      "Axx"     |  "ALx" |  "AMx" |     "VBxx"     | "VBLx" | "VBMx" |
;*      +----------------|--------|--------|----------------|--------|--------+
;*      |0000000000111111|00000000|00000000|0000000000111111|00000000|00000000|
;*      |0123456789012345|01234567|01234567|0123456789012345|01234567|01234567|
;*      +----------------|--------|--------|----------------|--------|--------+
;*   0: |        *       |        |        |*               |        |        |
;*   1: |        *       |        |        |*               |        |        |
;*   2: |        *       |        |        |*               |        |        |
;*   3: |        *       |        |        |*               |        |        |
;*   4: |        *       |        |        |*               |        |        |
;*   5: |        *       |        |        |*               |        |        |
;*   6: |        *       |        |        |**              |        |        |
;*   7: |        *       |        |        |**              |        |        |
;*   8: |*       *       |        |        |*               |        |        |
;*      +----------------+--------+--------+----------------+--------+--------+
;*
;*      +----------------+                 +--------+       +-------+
;*      |      "Dxx"     |                 |  "Px"  |       |"CUCRx"|
;*      +----------------+                 +--------+       +-------+
;*      |0000000000111111|                 |00000000|       |0000   |
;*      |0123456789012345|                 |01234567|       |0123   |
;*      +----------------+                 +--------+       +-------+
;*   0: | **             |                 |        |       |       |
;*   1: | **             |                 |        |       |       |
;*   2: | **             |                 |        |       |       |
;*   3: | **             |                 |        |       |       |
;*   4: | **             |                 |        |       |       |
;*   5: | **             |                 |        |       |       |
;*   6: | **             |                 |        |       |       |
;*   7: | **             |                 |        |       |       |
;*   8: |***             |                 |        |       |       |
;*      +----------------+                 +--------+       +-------+
;*
;*      Done
;*
;*      Collapsed epilog stages       : 1
;*      Prolog not removed
;*      Collapsed prolog stages       : 0
;*
;*      Max amt of load speculation   : 0 bytes
;*
;*      Minimum safe trip count       : 1
;*      Min. prof. trip count  (est.) : 3
;*
;*      Mem bank conflicts/iter(est.) : { min 0.000, est 0.000, max 0.000 }
;*      Mem bank perf. penalty (est.) : 0.0%
;*
;*
;*      Total cycles (est.)         : 7 + trip_cnt * 9        
;*----------------------------------------------------------------------------*
;*       SETUP CODE
;*
;*                  MVKU32  0x1,A0  ; [] 
;*
;*        SINGLE SCHEDULED ITERATION
;*
;*        ||$C$C3100||:
;*   0              SLDD    .D1     *D2(1192),D0      ; [A_D1] |148|  ^ [C1]
;*   1              NOP     0x5     ; [A_B] 
;*   6      [ A0]   STW     .D1X    B0,*D0[A8]        ; [A_D1] |148|  ^ 
;*   7              LDW     .D1     *D1(12),B1        ; [A_D1] |146| 
;*   8              NOP     0x5     ; [A_B] 
;*  13              ADDW    .D1     A8,0x1,A8         ; [A_D1] |146| 
;*  14              CMPGTW  .L1X    B1,A8,A0          ; [A_L1] |146| 
;*  15      [ A0]   B       .B1     ||$C$C3100||      ; [A_B] |146| 
;*  16              ; BRANCHCC OCCURS {||$C$C3100||}  ; [] |146| 
;*----------------------------------------------------------------------------*
||$C$L151||:    ; PIPED LOOP PROLOG
;** --------------------------------------------------------------------------*
||$C$L152||:    ; PIPED LOOP KERNEL
;          EXCLUSIVE CPU CYCLES: 9
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 146,column 15,is_stmt,isa 0
           LDW     .D1     *D1(12),B1        ; [A_D1] |146| <0,7> 
           NOP             0x1               ; [A_B] 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 148,column 5,is_stmt,isa 0
           SLDD    .D1     *D2(1192),D0      ; [A_D1] |148| <1,0>  ^ [C1]
           NOP             0x3               ; [A_B] 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 146,column 15,is_stmt,isa 0
           ADDW    .D1     A8,0x1,A8         ; [A_D1] |146| <0,13> 
           CMPGTW  .L1X    B1,A8,A0          ; [A_L1] |146| <0,14> 

   [ A0]   B       .B1     ||$C$L152||       ; [A_B] |146| <0,15> 
|| [ A0]   STW     .D1X    B0,*D0[A8]        ; [A_D1] |148| <1,6>  ^ 

;** --------------------------------------------------------------------------*
||$C$L153||:    ; PIPED LOOP EPILOG
;          EXCLUSIVE CPU CYCLES: 1
           PROTCLR                            ; [A_U] VB1
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 2
           MV      .D1     D1,D0             ; [A_D1] 

           STD     .D1     D0,*SP(48)        ; [A_D1] 
||         MV      .D2     D2,A5             ; [A_D2] 
||         MV      .L1X    B1,A1             ; [A_L1] 

;** --------------------------------------------------------------------------*
||$C$L154||:    
;          EXCLUSIVE CPU CYCLES: 14
           LDD     .D1     *SP(48),A0        ; [A_D1] 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 153,column 3,is_stmt,isa 0
           LDW     .D1     *A0(68),AL1       ; [A_D1] |153| 
           CMPEQW  .L1     AL1,0x1,A0        ; [A_L1] |153| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 119,column 29,is_stmt,isa 0

   [!A0]   B       .B1     ||$C$L173||       ; [A_B] |153| 
||         MVKU32  .L2     0x3f800000,B4     ; [B_L2] |119| 
||         MVKU32  .L1     0,A9              ; [A_L1] 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 153,column 3,is_stmt,isa 0
           ; BRANCHCC OCCURS {||$C$L173||}   ; [] |153| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 14
           LDD     .D1     *SP(48),A0        ; [A_D1] 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 155,column 17,is_stmt,isa 0
           LDW     .D1     *A0(56),AL0       ; [A_D1] |155| 
           MVKU32  .L1     0,A8              ; [A_L1] 
           ADDD    .D1     A5,0x248,A3       ; [A_D1] 

           MV      .L2X    A8,B2             ; [B_L2] 
||         ADDD    .D1     A5,0x244,A2       ; [A_D1] 

           CMPGTW  .L1     AL0,0,A0          ; [A_L1] |155| 
||         MV      .L2X    A3,B5             ; [B_L2] |155| 
||         MV      .S1X    B14,D14           ; [A_S1] 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 155,column 10,is_stmt,isa 0

   [!A0]   B       .B1     ||$C$L173||       ; [A_B] |155| 
||         ADDD    .D2     A5,0x140,D14      ; [A_D2] 
||         MV      .L2X    A2,B3             ; [B_L2] |155| 
||         MV      .D1     D14,D10           ; [A_D1] 
||         MVKU32  .L1     0,A7              ; [A_L1] |155| 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 155,column 17,is_stmt,isa 0
           ; BRANCHCC OCCURS {||$C$L173||}   ; [] |155| 
;** --------------------------------------------------------------------------*
;**   BEGIN LOOP ||$C$L155||
;** --------------------------------------------------------------------------*
||$C$L155||:    
;          EXCLUSIVE CPU CYCLES: 8
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 162,column 20,is_stmt,isa 0
           LDW     .D1     *D10(0),A3        ; [A_D1] |162| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 157,column 7,is_stmt,isa 0
           MV      .L1X    B5,A0             ; [A_L1] |157| 

           CMPGTW  .L1     A3,0,A0           ; [A_L1] |162| 
||         LDW     .D1     *A0(0),AM3        ; [A_D1] |159| 
||         MV      .S1X    B3,A2             ; [A_S1] |157| 

   [!A0]   B       .B1     ||$C$L172||       ; [A_B] |162| 
||         LDW     .D1     *A2(0),A15        ; [A_D1] |160| 
||         LDUW    .D2     *D14(0),A12       ; [A_D2] |157| 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 162,column 20,is_stmt,isa 0
           ; BRANCHCC OCCURS {||$C$L172||}   ; [] |162| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 1
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 162,column 12,is_stmt,isa 0

           MV      .S1X    B2,AL7            ; [A_S1] |171| 
||         MVKU32  .L1     0,A4              ; [A_L1] |162| 

;** --------------------------------------------------------------------------*
;**   BEGIN LOOP ||$C$L156||
;** --------------------------------------------------------------------------*
||$C$L156||:    
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 164,column 22,is_stmt,isa 0
           CMPGTW  .L1     A1,0,A0           ; [A_L1] |164| 
   [!A0]   B       .B1     ||$C$L171||       ; [A_B] |164| 
           ; BRANCHCC OCCURS {||$C$L171||}   ; [] |164| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 1
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 164,column 14,is_stmt,isa 0
           MVKU32  .L1     0,A9              ; [A_L1] |164| 
;** --------------------------------------------------------------------------*
;**   BEGIN LOOP ||$C$L157||
;** --------------------------------------------------------------------------*
||$C$L157||:    
;          EXCLUSIVE CPU CYCLES: 14
           LDD     .D1     *SP(48),A0        ; [A_D1] 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 168,column 13,is_stmt,isa 0
           LDW     .D1     *A0(16),AL0       ; [A_D1] |168| 
           CMPEQW  .L1     AL0,A9,A0         ; [A_L1] |168| 
   [ A0]   B       .B1     ||$C$L170||       ; [A_B] |168| 
           ; BRANCHCC OCCURS {||$C$L170||}   ; [] |168| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 14
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 170,column 11,is_stmt,isa 0
           LDD     .D1     *SP(48),A0        ; [A_D1] |170| 

           LDW     .D1     *A0(72),AL0       ; [A_D1] |173| 
||         LDD     .D2     *A5(1192),D0      ; [A_D2] |170| 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 171,column 11,is_stmt,isa 0
           SHLW    .L1     A7,0x4,D1         ; [A_L1] |171| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 170,column 11,is_stmt,isa 0

           EXT     .L1     A9,0x20,0x20,AL2  ; [A_L1] |170| 
||         ORW     .D1     A4,D1,AL1         ; [A_D1] |171| 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 171,column 11,is_stmt,isa 0

           SHLD    .L1     AL7,0x3,AL1       ; [A_L1] 
||         SHLW    .S1     AL1,0x8,D1        ; [A_S1] |171| 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 170,column 11,is_stmt,isa 0

           CMPEQW  .L1     AL0,0x4,A0        ; [A_L1] |173| 
||         ORW     .D2     A9,D1,AL1         ; [A_D2] |171| 
||         ADDD    .S1     A5,AL1,D1         ; [A_S1] 
||         ADDAW   .D1     D0,A9,D0          ; [A_D1] |170| 

   [!A0]   B       .B1     ||$C$L163||       ; [A_B] |173| 
||         ADDD    .D1     D1,0xc0,D1        ; [A_D1] 
||         SHLW    .L1     AL1,0x10,A11      ; [A_L1] |171| 
||         LDW     .D2     *D0(0),B6         ; [A_D2] |170| 
||         SHLD    .S1     AL2,0x2,D13       ; [A_S1] |170| 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 173,column 11,is_stmt,isa 0
           ; BRANCHCC OCCURS {||$C$L163||}   ; [] |173| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 12
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 197,column 13,is_stmt,isa 0
           ADDW    .D1     A1,0x5,AM0        ; [A_D1] |197| 
           MPYWW   .N1     A4,AM0,A0         ; [A_N1] |197| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 200,column 36,is_stmt,isa 0
           LDW     .D1     *D10(12),AL0      ; [A_D1] |200| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 197,column 13,is_stmt,isa 0

           ADDW    .D2     A9,A0,D1          ; [A_D2] |197| 
||         LDD     .D1     *D1(0),D3         ; [A_D1] |197| 

           ADDW    .D1     D1,0x1,AM0        ; [A_D1] |197| 

           MPYWW   .N1     A15,AM0,D2        ; [A_N1] |197| 
||         MPYWW   .M1     A15,A0,D9         ; [A_M1] |198| 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 200,column 36,is_stmt,isa 0
           CMPGTW  .L1     AL0,0,A0          ; [A_L1] |200| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 197,column 13,is_stmt,isa 0

   [!A0]   B       .B1     ||$C$L169||       ; [A_B] |200| 
||         ADDAH   .D1     D3,D2,D12         ; [A_D1] |197| 
||         ADDAH   .D2     D3,D9,D11         ; [A_D2] |198| 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 200,column 36,is_stmt,isa 0
           ; BRANCHCC OCCURS {||$C$L169||}   ; [] |200| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 12

           EXT     .L1     A7,0x20,0x20,AM0  ; [A_L1] 
||         MVKU32  .S1     0xf8,AM1          ; [A_S1] 

           MPYDD   .N1     AM1,AM0,D0        ; [A_N1] 
           MV      .L1X    B14,A0            ; [A_L1] 
           ADDD    .D1     A0,D0,D0          ; [A_D1] 
           LDW     .D1     *D0(8),B0         ; [A_D1] 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 200,column 31,is_stmt,isa 0
           MVKU32  .L1     0,A6              ; [A_L1] |200| 
;** --------------------------------------------------------------------------*
;**   BEGIN LOOP ||$C$L158||
;** --------------------------------------------------------------------------*
||$C$L158||:    
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 202,column 38,is_stmt,isa 0
           CMPGTW  .L1X    B0,0,A0           ; [A_L1] |202| 
   [!A0]   B       .B1     ||$C$L162||       ; [A_B] |202| 
           ; BRANCHCC OCCURS {||$C$L162||}   ; [] |202| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 12
           MPYWW   .N1     AM3,A6,A10        ; [A_N1] 

           ADDAH   .D1     D11,A10,A14       ; [A_D1] 
||         ADDAH   .D2     D12,A10,A0        ; [A_D2] 

           MV      .D1     A0,D2             ; [A_D1] 
||         MV      .D2     A14,D3            ; [A_D2] 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 208,column 17,is_stmt,isa 0

           SLDH    .D1     *D2++(2),AL3      ; [A_D1] |208| <0,0>  ^ 
||         SLDH    .D2     *D3++(2),AL2      ; [A_D2] |208| <0,0>  ^ 

           UNPROT          0x1               ; [A_U] 
;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*
;*      Loop found in file               : src/tidl_detectionOutput_score.c
;*      Loop source line                 : 202
;*      Loop opening brace source line   : 203
;*      Loop closing brace source line   : 218
;*      Known Minimum Trip Count         : 1                    
;*      Known Max Trip Count Factor      : 1
;*      Loop Carried Dependency Bound(^) : 24
;*      Unpartitioned Resource Bound     : 3
;*      Partitioned Resource Bound       : 6 (pre-sched)
;*
;*      Searching for software pipeline schedule at ...
;*         ii = 24 Schedule found with 2 iterations in parallel
;*
;*      Partitioned Resource Bound(*)    : 6 (post-sched)
;*
;*      Constant Extension #0 Used [C0]  : 1
;*      Constant Extension #1 Used [C1]  : 2
;*
;*      Resource Partition (may include "post-sched" split/spill moves):
;*
;*                                                A-side   B-side
;*      .L units                                     0        0     
;*      .S units                                     0        0     
;*      .M units                                     0        0     
;*      .N units                                     1        0     
;*      .D units                                     5        -     
;*      .B units                                     1        -     
;*      .C units                                     -        0     
;*      .P units                                     -        0     
;*
;*      .M/.N units                                  2        0     
;*      .L/.S units                                 10        0     
;*      .L/.S/.C units                               2        0     
;*      .L/.S/.C/.M units                            0        0     
;*      .L/.S/.C/.M/.D units                         7        3     
;*
;*      .X cross paths                               1        0     
;*
;*      Bound(.D1 .D2 .D)                            3        -     
;*      Bound(.M .N .MN)                             2        0     
;*      Bound(.L .S .LS)                             5        0     
;*      Bound(.L .S .C .LS .LSC)                     6*       0     
;*      Bound(.L .S .C .M .LS .LSC .LSCM)            4        0     
;*      Bound(.L .S .C .M .D .LS .LSC .LSCM .LSCMD)  5        1     
;*
;*      Register Usage Tables:
;*
;*      +----------------------------------+----------------------------------+
;*      |              ASIDE               |              BSIDE               |
;*      +----------------+--------+--------+----------------+--------+--------+
;*      |      "Axx"     |  "ALx" |  "AMx" |     "VBxx"     | "VBLx" | "VBMx" |
;*      +----------------|--------|--------|----------------|--------|--------+
;*      |0000000000111111|00000000|00000000|0000000000111111|00000000|00000000|
;*      |0123456789012345|01234567|01234567|0123456789012345|01234567|01234567|
;*      +----------------|--------|--------|----------------|--------|--------+
;*   0: | **     *       |  ****  |*       |*               |***     |        |
;*   1: | **     *       |******  |*       |*               |***     |        |
;*   2: | **     *       |  ****  |*       |*               |***     |        |
;*   3: | **     *       |* ****  |*       |*               |***     |        |
;*   4: |***     *       |  ****  |*       |*               |***     |        |
;*   5: |***     *       |  ****  |*       |*               |***     |        |
;*   6: |***     *       |  ****  |*       |*               |***     |        |
;*   7: |***     *       |  ****  |*       |*               |***     |        |
;*   8: |***     *       |  ****  |*       |*               |***     |        |
;*   9: |***     *       |  ****  |***     |*               |***     |        |
;*  10: |***     *       |  ****  |*       |*               |***     |        |
;*  11: |***     *       |  ****  |*       |*               |***     |        |
;*  12: |***     *       |  ****  |*       |*               |***     |        |
;*  13: |***     *       |  ****  |*       |*               |***     |        |
;*  14: |***     *       |  ****  |*       |*               |***     |        |
;*  15: |***     *       |  ****  |*       |*               |***     |        |
;*  16: |***     *       |  ****  |*       |*               |***     |        |
;*  17: |***     *       |  ****  |*       |*               |***     |        |
;*  18: |***     *       |******  |*       |*               |***     |        |
;*  19: |***     *       |  ****  |*       |*               |***     |        |
;*  20: |***     *       |  ****  |*       |*               |***     |        |
;*  21: |***     *       |  ****  |***     |*               |***     |        |
;*  22: |***     *       |  ****  |*       |*               |***     |        |
;*  23: | **     *       |  ****  |*       |*               |***     |        |
;*      +----------------+--------+--------+----------------+--------+--------+
;*
;*      +----------------+                 +--------+       +-------+
;*      |      "Dxx"     |                 |  "Px"  |       |"CUCRx"|
;*      +----------------+                 +--------+       +-------+
;*      |0000000000111111|                 |00000000|       |0000   |
;*      |0123456789012345|                 |01234567|       |0123   |
;*      +----------------+                 +--------+       +-------+
;*   0: |********        |                 |        |       |       |
;*   1: |********        |                 |        |       |       |
;*   2: |**********      |                 |        |       |       |
;*   3: |********        |                 |        |       |       |
;*   4: |********        |                 |        |       |       |
;*   5: |********        |                 |        |       |       |
;*   6: |********        |                 |        |       |       |
;*   7: |********        |                 |        |       |       |
;*   8: |********        |                 |        |       |       |
;*   9: |********        |                 |        |       |       |
;*  10: |********        |                 |        |       |       |
;*  11: |********        |                 |        |       |       |
;*  12: |********        |                 |        |       |       |
;*  13: |*********       |                 |        |       |       |
;*  14: |*********       |                 |        |       |       |
;*  15: |********        |                 |        |       |       |
;*  16: |********        |                 |        |       |       |
;*  17: |********        |                 |        |       |       |
;*  18: |********        |                 |        |       |       |
;*  19: |********        |                 |        |       |       |
;*  20: |********        |                 |        |       |       |
;*  21: |********        |                 |        |       |       |
;*  22: |********        |                 |        |       |       |
;*  23: |********        |                 |        |       |       |
;*      +----------------+                 +--------+       +-------+
;*
;*      Done
;*
;*      Collapsed epilog stages       : 1
;*      Prolog not removed
;*      Collapsed prolog stages       : 0
;*
;*      Max amt of load speculation   : 2 bytes
;*
;*      Minimum safe trip count       : 1
;*      Min. prof. trip count  (est.) : 3
;*
;*      Mem bank conflicts/iter(est.) : { min 0.000, est 0.000, max 0.000 }
;*      Mem bank perf. penalty (est.) : 0.0%
;*
;*
;*      Total cycles (est.)         : 12 + trip_cnt * 24        
;*----------------------------------------------------------------------------*
;*       SETUP CODE
;*
;*                  MVKU32  0x1,A1  ; [] 
;*
;*        SINGLE SCHEDULED ITERATION
;*
;*        ||$C$C2990||:
;*   0              SLDH    .D2     *D2++(2),AL0      ; [A_D2] |208|  ^ 
;*     ||           SLDH    .D1     *D3++(2),AL1      ; [A_D1] |208|  ^ 
;*   1              NOP     0x5     ; [A_B] 
;*   6              VINTSP  .L1     AL0,AM1           ; [A_L1] |208|  ^ 
;*     ||           VINTSP  .S1     AL1,AM2           ; [A_S1] |208|  ^ 
;*   7              NOP     0x2     ; [A_B] 
;*   9              MPYSP   .N1     AM0,AM1,AL0       ; [A_N1] |208|  ^ 
;*     ||           MPYSP   .M1     AM0,AM2,AL1       ; [A_M1] |208|  ^ 
;*  10              NOP     0x3     ; [A_B] 
;*  13              CMPLESP .S1     AL4,AL0,D8        ; [A_S1] |208|  ^ 
;*     ||           CMPLESP .L1     AL4,AL1,D9        ; [A_L1] |208|  ^ 
;*  14              ANDW    .D1     D9,D8,AL0         ; [A_D1] |208|  ^ 
;*  15              CMPEQW  .L1     AL0,0,A0          ; [A_L1] |208|  ^ 
;*  16      [!A0]   CMPGEW  .L1     A8,AL3,A0         ; [A_L1] |210|  ^ 
;*     ||   [ A0]   MVKU32  .S1     0x1,A0            ; [A_S1] |210|  ^ 
;*  17      [!A0]   SLDD    .D1     *D1(1176),D0      ; [A_D1] |213|  ^ [C1]
;*  18              NOP     0x1     ; [A_B] 
;*  19      [!A0]   ORW     .D2     D5,D7,D4          ; [A_D2] |213| 
;*     ||   [!A1]   MVKU32  .L1     0x1,A0            ; [A_L1] 
;*  20      [!A0]   EXT     .S1     AL2,0x20,0x20,AM1 ; [A_S1] [C1]
;*     ||   [!A0]   MVKU32  .L1     0xf8,AM2          ; [A_L1] [C0]
;*  21      [!A0]   MPYDD   .N1     AM2,AM1,D8        ; [A_N1] 
;*  22              NOP     0x1     ; [A_B] 
;*  23      [!A0]   STW     .D1     D4,*D0[A8]        ; [A_D1] |213|  ^ 
;*  24              NOP     0x1     ; [A_B] 
;*  25      [!A0]   ADDD    .D2     D6,D8,D8          ; [A_D2] 
;*  26      [!A0]   SLDW    .D1     *D8(8),B0         ; [A_D1] 
;*  27              NOP     0x5     ; [A_B] 
;*  32              ADDW    .D1     A2,0x1,A2         ; [A_D1] |202| 
;*  33      [!A0]   ADDW    .L2     BL0,0x1,BL0       ; [B_L2] |214| 
;*     ||   [!A0]   ADDW    .D1     A8,0x1,A8         ; [A_D1] |215| 
;*     ||           CMPGTW  .L1X    B0,A2,A0          ; [A_L1] |202| 
;*  34      [ A1]   MV      .L2     BL0,BL1           ; [B_L2] |202| 
;*     ||   [ A1]   MV      .D1     A8,AL5            ; [A_D1] |202| 
;*     ||   [ A1]   MV      .S2     B0,BL2            ; [B_S2] |202| 
;*     ||   [!A0]   MVKU32  .L1     0,A1              ; [A_L1] |202| 
;*  35              ADDW    .D1     D7,0x1,D7         ; [A_D1] |202| 
;*     ||   [ A1]   B       .B1     ||$C$C2990||      ; [A_B] |202| 
;*  36              ; BRANCHCC OCCURS {||$C$C2990||}  ; [] |202| 
;*
;*       RESTORE CODE
;*
;*                  MV      BL1,BL0 ; [] 
;*                  MV      AL5,A8  ; [] 
;*                  MV      BL2,B0  ; [] 
;*----------------------------------------------------------------------------*
||$C$L159||:    ; PIPED LOOP PROLOG
;          EXCLUSIVE CPU CYCLES: 6

           VINTSP  .S1     AL2,AM2           ; [A_S1] |208| <0,6>  ^ 
||         VINTSP  .L1     AL3,AM1           ; [A_L1] |208| <0,6>  ^ 

           NOP             0x1               ; [A_B] 
           MV      .D1     A12,AM0           ; [A_D1] 

           MV      .L1X    B14,D8            ; [A_L1] 
||         MPYSP   .M1     AM0,AM2,AL1       ; [A_M1] |208| <0,9>  ^ 
||         MPYSP   .N1     AM0,AM1,AL0       ; [A_N1] |208| <0,9>  ^ 

           MVKU32  .L1     0x1,A1            ; [A_L1] 
||         MV      .D1     A11,D5            ; [A_D1] 
||         MV      .D2     A5,D1             ; [A_D2] 
||         MV      .M1     A13,AL4           ; [A_M1] 
||         MV      .S1X    B12,D9            ; [A_S1] 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 202,column 33,is_stmt,isa 0

           MV      .L2     B6,BL0            ; [B_L2] 
||         MVKU32  .L1     0,A2              ; [A_L1] |202| 
||         MV      .S1     A7,AL2            ; [A_S1] 
||         MV      .D1     D8,D6             ; [A_D1] 
||         MV      .D2     D9,AL3            ; [A_D2] 
||         MV      .M1     A10,D7            ; [A_M1] 

;** --------------------------------------------------------------------------*
||$C$L160||:    ; PIPED LOOP KERNEL
;          EXCLUSIVE CPU CYCLES: 24
           NOP             0x1               ; [A_B] 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 208,column 17,is_stmt,isa 0

           CMPLESP .S1     AL4,AL0,D8        ; [A_S1] |208| <0,13>  ^ 
||         CMPLESP .L1     AL4,AL1,D9        ; [A_L1] |208| <0,13>  ^ 

           ANDW    .D1     D9,D8,AL0         ; [A_D1] |208| <0,14>  ^ 
           CMPEQW  .L1     AL0,0,A0          ; [A_L1] |208| <0,15>  ^ 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 210,column 19,is_stmt,isa 0

   [ A0]   MVKU32  .S1     0x1,A0            ; [A_S1] |210| <0,16>  ^ 
|| [!A0]   CMPGEW  .L1     A8,AL3,A0         ; [A_L1] |210| <0,16>  ^ 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 213,column 21,is_stmt,isa 0
   [!A0]   SLDD    .D1     *D1(1176),D0      ; [A_D1] |213| <0,17>  ^ [C1]
           NOP             0x1               ; [A_B] 

   [!A0]   ORW     .D2     D5,D7,D4          ; [A_D2] |213| <0,19> 
|| [!A1]   MVKU32  .L1     0x1,A0            ; [A_L1] <0,19> 

   [!A0]   EXT     .S1     AL2,0x20,0x20,AM1 ; [A_S1] <0,20> [C1]
|| [!A0]   MVKU32  .L1     0xf8,AM2          ; [A_L1] <0,20> [C0]

   [!A0]   MPYDD   .N1     AM2,AM1,D8        ; [A_N1] <0,21> 
           NOP             0x1               ; [A_B] 
   [!A0]   STW     .D1     D4,*D0[A8]        ; [A_D1] |213| <0,23>  ^ 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 208,column 17,is_stmt,isa 0

           SLDH    .D2     *D2++(2),AL0      ; [A_D2] |208| <1,0>  ^ 
||         SLDH    .D1     *D3++(2),AL1      ; [A_D1] |208| <1,0>  ^ 

   [!A0]   ADDD    .D2     D6,D8,D8          ; [A_D2] <0,25> 
   [!A0]   SLDW    .D1     *D8(8),B0         ; [A_D1] <0,26> 
           NOP             0x3               ; [A_B] 

           VINTSP  .S1     AL1,AM2           ; [A_S1] |208| <1,6>  ^ 
||         VINTSP  .L1     AL0,AM1           ; [A_L1] |208| <1,6>  ^ 

           NOP             0x1               ; [A_B] 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 202,column 38,is_stmt,isa 0
           ADDW    .D1     A2,0x1,A2         ; [A_D1] |202| <0,32> 

   [!A0]   ADDW    .D1     A8,0x1,A8         ; [A_D1] |215| <0,33> 
||         CMPGTW  .L1X    B0,A2,A0          ; [A_L1] |202| <0,33> 
|| [!A0]   ADDW    .L2     BL0,0x1,BL0       ; [B_L2] |214| <0,33> 
||         MPYSP   .M1     AM0,AM2,AL1       ; [A_M1] |208| <1,9>  ^ 
||         MPYSP   .N1     AM0,AM1,AL0       ; [A_N1] |208| <1,9>  ^ 

   [ A1]   MV      .D1     A8,AL5            ; [A_D1] |202| <0,34> 
|| [!A0]   MVKU32  .L1     0,A1              ; [A_L1] |202| <0,34> 
|| [ A1]   MV      .L2     BL0,BL1           ; [B_L2] |202| <0,34> 
|| [ A1]   MV      .S2     B0,BL2            ; [B_S2] |202| <0,34> 

           ADDW    .D1     D7,0x1,D7         ; [A_D1] |202| <0,35> 
|| [ A1]   B       .B1     ||$C$L160||       ; [A_B] |202| <0,35> 

;** --------------------------------------------------------------------------*
||$C$L161||:    ; PIPED LOOP EPILOG
;          EXCLUSIVE CPU CYCLES: 1
           PROTCLR                            ; [A_U] AL5
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 4
           MV      .D1     D6,A2             ; [A_D1] 
           MV      .L1     AL3,A0            ; [A_L1] 

           MV      .L2X    A2,B14            ; [B_L2] 
||         MV      .L1     AL4,A13           ; [A_L1] 

           MV      .M2X    A0,B12            ; [B_M2] 
||         MV      .D1     D5,A11            ; [A_D1] 
||         MV      .L1     AL2,A7            ; [A_L1] 
||         MV      .S1     AL5,A8            ; [A_S1] 
||         MV      .M1     AM0,A12           ; [A_M1] 
||         MV      .D2     D1,A5             ; [A_D2] 
||         MV      .L2     BL1,B6            ; [B_L2] 
||         MV      .S2     BL2,B0            ; [B_S2] 

;** --------------------------------------------------------------------------*
||$C$L162||:    
;          EXCLUSIVE CPU CYCLES: 8
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 200,column 36,is_stmt,isa 0
           LDW     .D1     *D10(12),AL0      ; [A_D1] |200| 
           ADDW    .D1     A6,0x1,A6         ; [A_D1] |200| 
           CMPGTW  .L1     AL0,A6,A0         ; [A_L1] |200| 
   [ A0]   B       .B1     ||$C$L158||       ; [A_B] |200| 
           ; BRANCHCC OCCURS {||$C$L158||}   ; [] |200| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 7
           LDD     .D1     *A5(1192),D0      ; [A_D1] 

           B       .B1     ||$C$L169||       ; [A_B] 
||         ADDD    .D1     D13,D0,D0         ; [A_D1] 

           ; BRANCH OCCURS {||$C$L169||}     ; [] 
;** --------------------------------------------------------------------------*
||$C$L163||:    
;          EXCLUSIVE CPU CYCLES: 10
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 175,column 13,is_stmt,isa 0
           MPYWW   .N1     A4,A1,D3          ; [A_N1] |175| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 176,column 36,is_stmt,isa 0
           LDW     .D1     *D10(12),AL0      ; [A_D1] |176| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 175,column 13,is_stmt,isa 0
           LDD     .D1     *D1(0),D2         ; [A_D1] |175| 
           ADDW    .D1     A9,D3,AM0         ; [A_D1] |175| 
           MPYWW   .N1     A15,AM0,D1        ; [A_N1] |175| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 176,column 36,is_stmt,isa 0
           CMPGTW  .L1     AL0,0,A0          ; [A_L1] |176| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 175,column 13,is_stmt,isa 0

   [!A0]   B       .B1     ||$C$L169||       ; [A_B] |176| 
||         ADDAH   .D1     D2,D1,D9          ; [A_D1] |175| 
||         MV      .L2X    A8,B0             ; [B_L2] 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 176,column 36,is_stmt,isa 0
           ; BRANCHCC OCCURS {||$C$L169||}   ; [] |176| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 12

           EXT     .L1     A7,0x20,0x20,AM0  ; [A_L1] 
||         MVKU32  .S1     0xf8,AM1          ; [A_S1] 

           MPYDD   .N1     AM1,AM0,D0        ; [A_N1] 
           MV      .L1X    B14,A0            ; [A_L1] 
           ADDD    .D1     A0,D0,D0          ; [A_D1] 
           LDW     .D1     *D0(8),A1         ; [A_D1] 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 176,column 31,is_stmt,isa 0
           MVKU32  .L1     0,A6              ; [A_L1] |176| 
;** --------------------------------------------------------------------------*
;**   BEGIN LOOP ||$C$L164||
;** --------------------------------------------------------------------------*
||$C$L164||:    
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 178,column 38,is_stmt,isa 0
           CMPGTW  .L1     A1,0,A0           ; [A_L1] |178| 
   [!A0]   B       .B1     ||$C$L168||       ; [A_B] |178| 
           ; BRANCHCC OCCURS {||$C$L168||}   ; [] |178| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 22
           MPYWW   .N1     AM3,A6,D5         ; [A_N1] 
           ADDAH   .D1     D9,D5,D1          ; [A_D1] 
           MV      .D1     D1,D2             ; [A_D1] 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 182,column 17,is_stmt,isa 0
           SLDH    .D1     *D2++(2),B1       ; [A_D1] |182| <0,0> 
           VINTSP  .L1X    B1,AM1            ; [A_L1] |182| <0,7> 
           MV      .D1     A12,AM0           ; [A_D1] 

           MPYSP   .N1     AM0,AM1,AL0       ; [A_N1] |182| <0,11> 
||         SLDH    .D1     *D2++(2),B7       ; [A_D1] |182| <1,0> 

           MV      .D1     A13,AL2           ; [A_D1] 
||         MV      .L1X    B12,D12           ; [A_L1] 

           CMPLESP .L1     AL2,AL0,A0        ; [A_L1] |182| <0,15> 
||         MV      .D1     D12,AL1           ; [A_D1] 
||         MV      .S1X    B0,A8             ; [A_S1] 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 184,column 19,is_stmt,isa 0

   [ A0]   CMPGEW  .L1     A8,AL1,A1         ; [A_L1] |184| <0,16>  ^ 
|| [!A0]   MVKU32  .S1     0x1,A1            ; [A_S1] |184| <0,16>  ^ 
||         MV      .L2X    A1,B1             ; [B_L2] 
||         MV      .D1     A5,D1             ; [A_D1] 
||         UNPROT          0x1               ; [A_U] 

;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*
;*      Loop found in file               : src/tidl_detectionOutput_score.c
;*      Loop source line                 : 178
;*      Loop opening brace source line   : 179
;*      Loop closing brace source line   : 192
;*      Known Minimum Trip Count         : 1                    
;*      Known Max Trip Count Factor      : 1
;*      Loop Carried Dependency Bound(^) : 8
;*      Unpartitioned Resource Bound     : 4
;*      Partitioned Resource Bound       : 4 (pre-sched)
;*
;*      Searching for software pipeline schedule at ...
;*         ii = 8  Unsafe schedule for irregular loop
;*         ii = 8  Unsafe schedule for irregular loop
;*         ii = 8  Did not find schedule
;*         ii = 9  Unsafe schedule for irregular loop
;*         ii = 9  Unsafe schedule for irregular loop
;*         ii = 9  Unsafe schedule for irregular loop
;*         ii = 9  Did not find schedule
;*         ii = 10 Unsafe schedule for irregular loop
;*         ii = 10 Schedule found with 4 iterations in parallel
;*
;*      Partitioned Resource Bound(*)    : 4 (post-sched)
;*
;*      Constant Extension #0 Used [C0]  : 1
;*      Constant Extension #1 Used [C1]  : 2
;*
;*      Resource Partition (may include "post-sched" split/spill moves):
;*
;*                                                A-side   B-side
;*      .L units                                     0        0     
;*      .S units                                     0        0     
;*      .M units                                     0        0     
;*      .N units                                     1        0     
;*      .D units                                     4        -     
;*      .B units                                     1        -     
;*      .C units                                     -        0     
;*      .P units                                     -        0     
;*
;*      .M/.N units                                  1        0     
;*      .L/.S units                                  6        0     
;*      .L/.S/.C units                               1        0     
;*      .L/.S/.C/.M units                            0        0     
;*      .L/.S/.C/.M/.D units                         5        1     
;*
;*      .X cross paths                               2        0     
;*
;*      Bound(.D1 .D2 .D)                            2        -     
;*      Bound(.M .N .MN)                             1        0     
;*      Bound(.L .S .LS)                             3        0     
;*      Bound(.L .S .C .LS .LSC)                     4*       0     
;*      Bound(.L .S .C .M .LS .LSC .LSCM)            3        0     
;*      Bound(.L .S .C .M .D .LS .LSC .LSCM .LSCMD)  4*       1     
;*
;*      Register Usage Tables:
;*
;*      +----------------------------------+----------------------------------+
;*      |              ASIDE               |              BSIDE               |
;*      +----------------+--------+--------+----------------+--------+--------+
;*      |      "Axx"     |  "ALx" |  "AMx" |     "VBxx"     | "VBLx" | "VBMx" |
;*      +----------------|--------|--------|----------------|--------|--------+
;*      |0000000000111111|00000000|00000000|0000000000111111|00000000|00000000|
;*      |0123456789012345|01234567|01234567|0123456789012345|01234567|01234567|
;*      +----------------|--------|--------|----------------|--------|--------+
;*   0: | **     *       |***     |*       | *              |*       |        |
;*   1: | **     *       |***     |*       | *              |*       |        |
;*   2: | **     *       |****    |*       | *              |*       |        |
;*   3: |***     *       |***     |*       |**              |*       |        |
;*   4: | **     *       |***     |*       |**              |*       |        |
;*   5: | **     *       |***     |*       | *              |*       |        |
;*   6: | **     *       |***     |***     | *              |*       |        |
;*   7: | **     *       |***     |**      | *              |*       |        |
;*   8: | **     *       |***     |**      | *              |*       |        |
;*   9: |***     *       |***     |*       | *              |*       |        |
;*      +----------------+--------+--------+----------------+--------+--------+
;*
;*      +----------------+                 +--------+       +-------+
;*      |      "Dxx"     |                 |  "Px"  |       |"CUCRx"|
;*      +----------------+                 +--------+       +-------+
;*      |0000000000111111|                 |00000000|       |0000   |
;*      |0123456789012345|                 |01234567|       |0123   |
;*      +----------------+                 +--------+       +-------+
;*   0: |********        |                 |        |       |       |
;*   1: |*********       |                 |        |       |       |
;*   2: |********        |                 |        |       |       |
;*   3: |********        |                 |        |       |       |
;*   4: |********        |                 |        |       |       |
;*   5: |********        |                 |        |       |       |
;*   6: |********        |                 |        |       |       |
;*   7: |********        |                 |        |       |       |
;*   8: |********        |                 |        |       |       |
;*   9: |********        |                 |        |       |       |
;*      +----------------+                 +--------+       +-------+
;*
;*      Done
;*
;*      Collapsed epilog stages       : 3
;*      Prolog not removed
;*      Collapsed prolog stages       : 0
;*
;*      Max amt of load speculation   : 6 bytes
;*
;*      Minimum safe trip count       : 1
;*      Min. prof. trip count  (est.) : 3
;*
;*      Mem bank conflicts/iter(est.) : { min 0.000, est 0.000, max 0.000 }
;*      Mem bank perf. penalty (est.) : 0.0%
;*
;*
;*      Total cycles (est.)         : 23 + trip_cnt * 10        
;*----------------------------------------------------------------------------*
;*        SINGLE SCHEDULED ITERATION
;*
;*        ||$C$C3024||:
;*   0              SLDH    .D1     *D2++(2),B0       ; [A_D1] |182| 
;*   1              NOP     0x6     ; [A_B] 
;*   7              VINTSP  .L1X    B0,AM1            ; [A_L1] |182| 
;*   8              NOP     0x3     ; [A_B] 
;*  11              MPYSP   .N1     AM0,AM1,AL3       ; [A_N1] |182| 
;*  12              NOP     0x3     ; [A_B] 
;*  15              CMPLESP .L1     AL2,AL3,A0        ; [A_L1] |182| 
;*  16      [ A0]   CMPGEW  .L1     A8,AL1,A1         ; [A_L1] |184|  ^ 
;*     ||   [!A0]   MVKU32  .S1     0x1,A1            ; [A_S1] |184|  ^ 
;*  17      [!A1]   SLDD    .D1     *D1(1176),D0      ; [A_D1] |187|  ^ [C1]
;*  18      [!A1]   EXT     .S1     AL0,0x20,0x20,AM1 ; [A_S1] [C1]
;*     ||   [!A1]   MVKU32  .L1     0xf8,AM2          ; [A_L1] [C0]
;*  19      [!A1]   MPYDD   .N1     AM2,AM1,D3        ; [A_N1] 
;*  20              NOP     0x2     ; [A_B] 
;*  22      [!A1]   ORW     .D1     D5,D7,D4          ; [A_D1] |187| 
;*  23      [!A1]   STW     .D1     D4,*D0[A8]        ; [A_D1] |187|  ^ 
;*     ||   [!A1]   ADDD    .D2     D6,D3,D8          ; [A_D2] 
;*  24      [!A1]   LDW     .D1     *D8(8),B1         ; [A_D1]  ^ 
;*  25      [!A1]   ADDW    .D1     A8,0x1,A8         ; [A_D1] |189|  ^ 
;*  26      [!A1]   ADDW    .L2     BL0,0x1,BL0       ; [B_L2] |188| 
;*  27              NOP     0x3     ; [A_B] 
;*  30              ADDW    .D2     A2,0x1,A2         ; [A_D2] |178| 
;*  31              ADDW    .D1     D7,0x1,D7         ; [A_D1] |178| 
;*     ||           CMPGTW  .L1X    B1,A2,A0          ; [A_L1] |178| 
;*  32      [ A0]   B       .B1     ||$C$C3024||      ; [A_B] |178| 
;*  33              ; BRANCHCC OCCURS {||$C$C3024||}  ; [] |178| 
;*----------------------------------------------------------------------------*
||$C$L165||:    ; PIPED LOOP PROLOG
;          EXCLUSIVE CPU CYCLES: 6
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 182,column 17,is_stmt,isa 0

           MV      .D2     A7,AL0            ; [A_D2] 
|| [!A1]   SLDD    .D1     *D1(1176),D0      ; [A_D1] |187| <0,17>  ^ [C1]
||         VINTSP  .L1X    B7,AM1            ; [A_L1] |182| <1,7> 

   [!A1]   EXT     .S1     AL0,0x20,0x20,AM1 ; [A_S1] <0,18> [C1]
|| [!A1]   MVKU32  .L1     0xf8,AM2          ; [A_L1] <0,18> [C0]

   [!A1]   MPYDD   .N1     AM2,AM1,D3        ; [A_N1] <0,19> 
           SLDH    .D1     *D2++(2),B0       ; [A_D1] |182| <2,0> 

           MV      .L1X    B14,D11           ; [A_L1] 
||         MV      .D1     A11,D5            ; [A_D1] 
||         MV      .D2     D5,D7             ; [A_D2] 
||         MPYSP   .N1     AM0,AM1,AL3       ; [A_N1] |182| <1,11> 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 178,column 33,is_stmt,isa 0

           MVKU32  .L1     0,A2              ; [A_L1] |178| 
||         MV      .L2     B6,BL0            ; [B_L2] 
||         MV      .D2     D11,D6            ; [A_D2] 
|| [!A1]   ORW     .D1     D5,D7,D4          ; [A_D1] |187| <0,22> 

;** --------------------------------------------------------------------------*
||$C$L166||:    ; PIPED LOOP KERNEL
;          EXCLUSIVE CPU CYCLES: 10
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 187,column 21,is_stmt,isa 0

   [!A1]   ADDD    .D2     D6,D3,D8          ; [A_D2] <0,23> 
|| [!A1]   STW     .D1     D4,*D0[A8]        ; [A_D1] |187| <0,23>  ^ 

   [!A1]   LDW     .D1     *D8(8),B1         ; [A_D1] <0,24>  ^ 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 182,column 17,is_stmt,isa 0

   [!A1]   ADDW    .D1     A8,0x1,A8         ; [A_D1] |189| <0,25>  ^ 
||         CMPLESP .L1     AL2,AL3,A0        ; [A_L1] |182| <1,15> 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 184,column 19,is_stmt,isa 0

   [!A1]   ADDW    .L2     BL0,0x1,BL0       ; [B_L2] |188| <0,26> 
|| [!A0]   MVKU32  .S1     0x1,A1            ; [A_S1] |184| <1,16>  ^ 
|| [ A0]   CMPGEW  .L1     A8,AL1,A1         ; [A_L1] |184| <1,16>  ^ 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 182,column 17,is_stmt,isa 0

   [!A1]   SLDD    .D1     *D1(1176),D0      ; [A_D1] |187| <1,17>  ^ [C1]
||         VINTSP  .L1X    B0,AM1            ; [A_L1] |182| <2,7> 

   [!A1]   EXT     .S1     AL0,0x20,0x20,AM1 ; [A_S1] <1,18> [C1]
|| [!A1]   MVKU32  .L1     0xf8,AM2          ; [A_L1] <1,18> [C0]

   [!A1]   MPYDD   .N1     AM2,AM1,D3        ; [A_N1] <1,19> 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 178,column 38,is_stmt,isa 0

           ADDW    .D2     A2,0x1,A2         ; [A_D2] |178| <0,30> 
||         SLDH    .D1     *D2++(2),B0       ; [A_D1] |182| <3,0> 

           ADDW    .D1     D7,0x1,D7         ; [A_D1] |178| <0,31> 
||         CMPGTW  .L1X    B1,A2,A0          ; [A_L1] |178| <0,31> 
||         MPYSP   .N1     AM0,AM1,AL3       ; [A_N1] |182| <2,11> 

   [ A0]   B       .B1     ||$C$L166||       ; [A_B] |178| <0,32> 
|| [!A1]   ORW     .D1     D5,D7,D4          ; [A_D1] |187| <1,22> 

;** --------------------------------------------------------------------------*
||$C$L167||:    ; PIPED LOOP EPILOG
;          EXCLUSIVE CPU CYCLES: 1
           PROTCLR                            ; [A_U] VB1
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 5
           MV      .D1     D6,A2             ; [A_D1] 

           MV      .L2X    A8,B0             ; [B_L2] 
||         MV      .L1     AL1,A0            ; [A_L1] 

           MV      .L2X    A2,B14            ; [B_L2] 
||         MV      .L1     AL2,A13           ; [A_L1] 

           MV      .S2X    A0,B12            ; [B_S2] 
||         MV      .D1     D5,A11            ; [A_D1] 
||         MV      .L1     AL0,A7            ; [A_L1] 
||         MV      .S1X    B1,A1             ; [A_S1] 
||         MV      .M1     AM0,A12           ; [A_M1] 
||         MV      .L2     BL0,B6            ; [B_L2] 
||         MV      .D2     D1,A5             ; [A_D2] 

;** --------------------------------------------------------------------------*
||$C$L168||:    
;          EXCLUSIVE CPU CYCLES: 8
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 176,column 36,is_stmt,isa 0
           LDW     .D1     *D10(12),AL0      ; [A_D1] |176| 
           ADDW    .D1     A6,0x1,A6         ; [A_D1] |176| 
           CMPGTW  .L1     AL0,A6,A0         ; [A_L1] |176| 
   [ A0]   B       .B1     ||$C$L164||       ; [A_B] |176| 
           ; BRANCHCC OCCURS {||$C$L164||}   ; [] |176| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 7
           LDD     .D1     *A5(1192),D0      ; [A_D1] 
           ADDD    .D1     D13,D0,D0         ; [A_D1] 
;** --------------------------------------------------------------------------*
||$C$L169||:    
;          EXCLUSIVE CPU CYCLES: 11
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 222,column 11,is_stmt,isa 0
           LDD     .D1     *SP(48),A0        ; [A_D1] |222| 
           STW     .D1X    B6,*D0(0)         ; [A_D1] |222| 
           LDW     .D1     *A0(12),A1        ; [A_D1] |222| 
;** --------------------------------------------------------------------------*
||$C$L170||:    
;          EXCLUSIVE CPU CYCLES: 3
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 164,column 22,is_stmt,isa 0
           ADDW    .D1     A9,0x1,A9         ; [A_D1] |164| 
           CMPGTW  .L1     A1,A9,A0          ; [A_L1] |164| 
   [ A0]   B       .B1     ||$C$L157||       ; [A_B] |164| 
           ; BRANCHCC OCCURS {||$C$L157||}   ; [] |164| 
;** --------------------------------------------------------------------------*
||$C$L171||:    
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 162,column 20,is_stmt,isa 0
           ADDW    .D1     A3,0xffffffff,A3  ; [A_D1] |162| 

   [ A3]   B       .B1     ||$C$L156||       ; [A_B] |162| 
||         ADDW    .D1     A4,0x1,A4         ; [A_D1] |162| 

           ; BRANCHCC OCCURS {||$C$L156||}   ; [] |162| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 8
           LDD     .D1     *SP(48),A0        ; [A_D1] 
           LDW     .D1     *A0(56),AL0       ; [A_D1] 
;** --------------------------------------------------------------------------*
||$C$L172||:    
;          EXCLUSIVE CPU CYCLES: 6
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 155,column 17,is_stmt,isa 0
           MV      .L1X    B5,A3             ; [A_L1] |155| 

           ADDD    .D1     A3,0xc,A4         ; [A_D1] |155| 
||         MV      .L1X    B3,A2             ; [A_L1] |155| 

           ADDD    .D1     A2,0xc,A3         ; [A_D1] |155| 
||         MV      .L1X    B2,A0             ; [A_L1] |155| 

           ADDD    .D1     A0,0x1,A2         ; [A_D1] |155| 
||         MV      .L2X    A4,B5             ; [B_L2] |155| 
||         ADDW    .D2     A7,0x1,A7         ; [A_D2] |155| 

           CMPGTW  .L1     AL0,A7,A0         ; [A_L1] |155| 
||         MV      .L2X    A3,B3             ; [B_L2] |155| 

   [ A0]   B       .B1     ||$C$L155||       ; [A_B] |155| 
||         MV      .L2X    A2,B2             ; [B_L2] |155| 
||         ADDD    .D1     D10,0xf8,D10      ; [A_D1] |155| 
||         ADDD    .D2     D14,0x4,D14       ; [A_D2] |155| 

           ; BRANCHCC OCCURS {||$C$L155||}   ; [] |155| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 12
           LDD     .D1     *SP(48),A0        ; [A_D1] 
           LDW     .D1     *A0(68),AL1       ; [A_D1] 
           MV      .D1     A8,A9             ; [A_D1] 
;** --------------------------------------------------------------------------*
||$C$L173||:    
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 227,column 3,is_stmt,isa 0
           CMPEQW  .L1     AL1,0x2,A0        ; [A_L1] |227| 
   [!A0]   B       .B1     ||$C$L190||       ; [A_B] |227| 
           ; BRANCHCC OCCURS {||$C$L190||}   ; [] |227| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 14
           LDD     .D1     *SP(48),A0        ; [A_D1] 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 229,column 17,is_stmt,isa 0
           LDW     .D1     *A0(56),AL0       ; [A_D1] |229| 
           MVKU32  .L1     0,D8              ; [A_L1] 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 229,column 10,is_stmt,isa 0

           MV      .D1     D8,D14            ; [A_D1] 
||         MVKU32  .L1     0,A1              ; [A_L1] |229| 
||         MV      .S1X    B14,A7            ; [A_S1] 
||         ADDD    .D2     A5,0x248,A2       ; [A_D2] 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 229,column 17,is_stmt,isa 0

           CMPGTW  .L1     AL0,0,A0          ; [A_L1] |229| 
||         STD     .D1     A2,*SP(24)        ; [A_D1] |229| 
||         STD     .D2X    A7,*SP(16)        ; [A_D2] 

   [!A0]   B       .B1     ||$C$L190||       ; [A_B] |229| 
||         STD     .D1     D14,*SP(32)       ; [A_D1] 
||         MV      .L2X    A1,B13            ; [B_L2] |229| 
||         ADDD    .D2     A5,0x140,D7       ; [A_D2] 

           ; BRANCHCC OCCURS {||$C$L190||}   ; [] |229| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 5

           MVKU32  .L2     0x7f7fffff,BL5    ; [B_L2] 
||         MVKU32  .S2     0xff7fffff,BL6    ; [B_S2] 

           MVKU32  .L2     0x3d2aaab4,B9     ; [B_L2] 
||         MVKU32  .S2     0x37800000,BM1    ; [B_S2] 

           MVKU32  .L2     0x3e2aaaad,B7     ; [B_L2] 
||         MVKU32  .S2     0x3f317218,B8     ; [B_S2] 

           MV      .L1X    B12,AL7           ; [A_L1] 
||         MVKU32  .L2     0x3f000000,B6     ; [B_L2] 
||         MVKU32  .S2     0x3fb8aa3b,BM2    ; [B_S2] 

	.dwpsn	file "./inc/tidl_detectionOutput_int.h",line 157,column 3,is_stmt,isa 0

           MV      .L1X    B14,D13           ; [A_L1] |294| 
||         MVKU32  .L2     0,BL7             ; [B_L2] |157| 
||         MVKU32  .S2     0x10000,BL4       ; [B_S2] 

;** --------------------------------------------------------------------------*
;**   BEGIN LOOP ||$C$L174||
;** --------------------------------------------------------------------------*
||$C$L174||:    
;          EXCLUSIVE CPU CYCLES: 13
           LDD     .D1     *SP(48),A0        ; [A_D1] 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 233,column 7,is_stmt,isa 0
           LDD     .D1     *SP(16),A1        ; [A_D1] |233| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 235,column 7,is_stmt,isa 0

           LDW     .D1     *A0(76),A0        ; [A_D1] |236| 
||         LDW     .D2     *A2(0),D14        ; [A_D2] |235| 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 234,column 7,is_stmt,isa 0
           LDW     .D1     *A1(0),B0         ; [A_D1] |234| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 233,column 7,is_stmt,isa 0

   [ A0]   B       .B1     ||$C$L175||       ; [A_B] |236| 
||         STD     .D1     D14,*SP(40)       ; [A_D1] |235| 
||         LDUW    .D2     *D7(0),B5         ; [A_D2] |233| 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 236,column 7,is_stmt,isa 0
           ; BRANCHCC OCCURS {||$C$L175||}   ; [] |236| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 14
           LDD     .D1     *SP(48),A0        ; [A_D1] 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 239,column 9,is_stmt,isa 0
           LDW     .D1     *A0(12),A1        ; [A_D1] |239| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 246,column 20,is_stmt,isa 0
           CMPGTW  .L1X    B0,0,A4           ; [A_L1] |246| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 239,column 9,is_stmt,isa 0

   [!A4]   B       .B1     ||$C$L189||       ; [A_B] |246| 
||         MV      .L2X    A1,B11            ; [B_L2] |239| 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 246,column 20,is_stmt,isa 0
           ; BRANCHCC OCCURS {||$C$L189||}   ; [] |246| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 1
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 238,column 9,is_stmt,isa 0

           B       .B1     ||$C$L176||       ; [A_B] 
||         MVKU32  .L1     0x1,A4            ; [A_L1] |238| 

           ; BRANCH OCCURS {||$C$L176||}     ; [] 
;** --------------------------------------------------------------------------*
||$C$L175||:    
;          EXCLUSIVE CPU CYCLES: 3
           MV      .L1X    B0,A4             ; [A_L1] 
           CMPGTW  .L1     A4,0,A0           ; [A_L1] 
   [!A0]   B       .B1     ||$C$L189||       ; [A_B] 
           ; BRANCHCC OCCURS {||$C$L189||}   ; [] 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 3
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 244,column 9,is_stmt,isa 0
           MVKU32  .L1     0x1,A0            ; [A_L1] |244| 
           MV      .L2X    A0,B11            ; [B_L2] |244| 
;** --------------------------------------------------------------------------*
||$C$L176||:    
;          EXCLUSIVE CPU CYCLES: 13
           MV      .L1X    B13,A0            ; [A_L1] 

           EXT     .L1     A0,0x20,0x20,AM0  ; [A_L1] 
||         MVKU32  .S1     0xf8,AM1          ; [A_S1] 

           MPYDD   .N1     AM1,AM0,D0        ; [A_N1] 
           MV      .L1X    B14,A1            ; [A_L1] 
           ADDD    .D1     A1,D0,D0          ; [A_D1] 
           LDW     .D1     *D0(12),AL0       ; [A_D1] 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 246,column 12,is_stmt,isa 0

           SHLW    .L1     A0,0x4,D10        ; [A_L1] 
||         MV      .M1X    B0,A2             ; [A_M1] 
||         MVKU32  .S1     0,A7              ; [A_S1] |246| 

;** --------------------------------------------------------------------------*
;**   BEGIN LOOP ||$C$L177||
;** --------------------------------------------------------------------------*
||$C$L177||:    
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 248,column 9,is_stmt,isa 0

           CMPGTW  .L1     AL0,0,A0          ; [A_L1] |250| 
||         ORW     .D1     A7,D10,AL1        ; [A_D1] |248| 

   [!A0]   B       .B1     ||$C$L188||       ; [A_B] |250| 
||         SHLW    .L1     AL1,0x18,D9       ; [A_L1] |248| 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 250,column 32,is_stmt,isa 0
           ; BRANCHCC OCCURS {||$C$L188||}   ; [] |250| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 13
           MV      .L1X    B13,A0            ; [A_L1] 
           EXT     .L1     A0,0x20,0x20,AM0  ; [A_L1] 
           MPYDD   .N1     AM1,AM0,D0        ; [A_N1] 
           MV      .L1X    B14,D14           ; [A_L1] 
           ADDD    .D1     D14,D0,D0         ; [A_D1] 
           LDW     .D1     *D0(8),A8         ; [A_D1] 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 250,column 27,is_stmt,isa 0
           MVKU32  .L1     0,A10             ; [A_L1] |250| 
;** --------------------------------------------------------------------------*
;**   BEGIN LOOP ||$C$L178||
;** --------------------------------------------------------------------------*
||$C$L178||:    
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 252,column 34,is_stmt,isa 0
           CMPGTW  .L1     A8,0,A0           ; [A_L1] |252| 
   [!A0]   B       .B1     ||$C$L187||       ; [A_B] |252| 
           ; BRANCHCC OCCURS {||$C$L187||}   ; [] |252| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 9
           LDD     .D1     *SP(32),A0        ; [A_D1] 
           LDD     .D1     *SP(40),AM3       ; [A_D1] 
           SHLD    .L1     A0,0x3,D0         ; [A_L1] 

           ADDD    .D1     A5,D0,D0          ; [A_D1] 
||         ADDD    .D2     D8,A5,D1          ; [A_D2] 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 252,column 29,is_stmt,isa 0

           ADDD    .D1     D0,0xc0,D6        ; [A_D1] 
||         MPYWW   .N1     AM3,A10,A12       ; [A_N1] 
||         ADDD    .D2     D1,0x244,D5       ; [A_D2] 
||         MVKU32  .L1     0,A11             ; [A_L1] |252| 

;** --------------------------------------------------------------------------*
;**   BEGIN LOOP ||$C$L179||
;** --------------------------------------------------------------------------*
||$C$L179||:    
;          EXCLUSIVE CPU CYCLES: 25
           LDW     .D1     *D5(0),A13        ; [A_D1] 
           MPYWW   .N1     A7,A13,AM0        ; [A_N1] 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 254,column 13,is_stmt,isa 0
           MV      .L1X    B11,A0            ; [A_L1] |254| 

           MPYWW   .N1     A0,AM0,D0         ; [A_N1] 
||         LDD     .D1     *SP(48),D14       ; [A_D1] 
||         LDD     .D2     *D6(0),A6         ; [A_D2] 

           LDW     .D1     *A5(1304),AL0     ; [A_D1] |254| 
           ADDW    .D1     A12,D0,D0         ; [A_D1] 
           ADDW    .D1     A11,D0,D0         ; [A_D1] 

           ADDAH   .D1     A6,D0,D0          ; [A_D1] 
||         LDW     .D2     *D14(12),B3       ; [A_D2] |254| 

           LDH     .D1     *D0(0),B0         ; [A_D1] 
           CMPEQW  .L1     AL0,0x6,A3        ; [A_L1] |254| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 257,column 15,is_stmt,isa 0
   [ A3]   VSPTRUNC .L2    BL5,BL0           ; [B_L2] |257| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 256,column 15,is_stmt,isa 0
   [ A3]   VSPTRUNC .S2    BL6,BL1           ; [B_S2] |256| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 262,column 15,is_stmt,isa 0
           XORW    .L2     BL3,0xffffffff,BL2 ; [B_L2] |262| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 261,column 15,is_stmt,isa 0

           CMPGTW  .L1X    B3,0,A0           ; [A_L1] |266| 
|| [!A3]   EXT     .L2     BL2,0x30,0x30,B1  ; [B_L2] |262| 
|| [!A3]   EXT     .S2     BL3,0x30,0x30,B2  ; [B_S2] |261| 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 256,column 15,is_stmt,isa 0

   [!A0]   B       .B1     ||$C$L186||       ; [A_B] |266| 
||         MV      .L1X    B0,AL0            ; [A_L1] 
|| [ A3]   EXT     .L2     BL1,0x30,0x30,B2  ; [B_L2] |256| 
|| [ A3]   EXT     .S2     BL0,0x30,0x30,B1  ; [B_S2] |257| 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 266,column 26,is_stmt,isa 0
           ; BRANCHCC OCCURS {||$C$L186||}   ; [] |266| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 11
           LDD     .D1     *SP(48),A1        ; [A_D1] 

           LDW     .D1     *A1(16),A14       ; [A_D1] 
||         MV      .L1X    B3,A0             ; [A_L1] 

           MV      .L1X    B11,A3            ; [A_L1] 

           CMPGTD  .L1     A0,0x3,A3         ; [A_L1] 
||         MPYWW   .N1     A3,A7,A15         ; [A_N1] 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 266,column 18,is_stmt,isa 0

   [ A3]   B       .B1     ||$C$L181||       ; [A_B] 
||         MVKU32  .L1     0,A1              ; [A_L1] |266| 

           ; BRANCHCC OCCURS {||$C$L181||}   ; [] 
;** --------------------------------------------------------------------------*
;**   BEGIN LOOP ||$C$L180||
;** --------------------------------------------------------------------------*
||$C$L180||:    
;          EXCLUSIVE CPU CYCLES: 20
           MPYWW   .N1     A4,A1,D0          ; [A_N1] 
           ADDW    .D1     A15,D0,AM0        ; [A_D1] 
           MPYWW   .N1     A13,AM0,D0        ; [A_N1] 
           ADDW    .D1     A12,D0,D0         ; [A_D1] 
           ADDW    .D1     A11,D0,D0         ; [A_D1] 

           LDH     .D1     *A6[D0],B0        ; [A_D1] 
||         MV      .L2     B0,BL2            ; [B_L2] 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 270,column 15,is_stmt,isa 0

           VMINH   .L2     B0,B1,BL0         ; [B_L2] |273| 
||         CMPEQW  .L1     A1,A14,A3         ; [A_L1] |270| 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 266,column 26,is_stmt,isa 0

   [!A3]   EXT     .L2     BL0,0x30,0x30,B1  ; [B_L2] |273| 
||         VMAXH   .S2     B0,B2,BL1         ; [B_S2] |272| 
||         ADDW    .D1     A0,0xffffffff,A0  ; [A_D1] |266| 

   [ A0]   B       .B1     ||$C$L180||       ; [A_B] |266| 
|| [!A3]   MV      .L2     BL2,B0            ; [B_L2] 
|| [!A3]   EXT     .S2     BL1,0x30,0x30,B2  ; [B_S2] |272| 
||         ADDW    .D1     A1,0x1,A1         ; [A_D1] |266| 

           ; BRANCHCC OCCURS {||$C$L180||}   ; [] |266| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 1
           B       .B1     ||$C$L185||       ; [A_B] 
           ; BRANCH OCCURS {||$C$L185||}     ; [] 
;** --------------------------------------------------------------------------*
||$C$L181||:    
;          EXCLUSIVE CPU CYCLES: 3

           EXT     .L1     A0,0x20,0x20,AL0  ; [A_L1] 
||         MV      .D1     A4,AM2            ; [A_D1] 

           MPYWW   .N1     AM2,A1,D0         ; [A_N1] <0,1> 
||         NLCINIT .S1     AL0,0x1,6         ; [A_S1] 
||         UNPROT          0x1               ; [A_U] 
||         MV      .D1     A14,AL3           ; [A_D1] 

           TICK                               ; [A_U] <0,0> 
||         CMPEQW  .L1     A1,AL3,AM0        ; [A_L1] |270| <0,2> 
||         ADDW    .D1     A1,0x1,A0         ; [A_D1] |266| <0,2> 

;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*
;*      Loop found in file               : src/tidl_detectionOutput_score.c
;*      Loop source line                 : 266
;*      Loop opening brace source line   : 267
;*      Loop closing brace source line   : 279
;*      Known Minimum Trip Count         : 1                    
;*      Known Max Trip Count Factor      : 1
;*      Loop Carried Dependency Bound(^) : 2
;*      Unpartitioned Resource Bound     : 2
;*      Partitioned Resource Bound       : 2 (pre-sched)
;*
;*      Searching for software pipeline schedule at ...
;*         ii = 2  Schedule found with 10 iterations in parallel
;*
;*      Partitioned Resource Bound(*)    : 2 (post-sched)
;*
;*      Constant Extension #0 Used [C0]  : 1
;*      Constant Extension #1 Used [C1]  : 1
;*
;*      Resource Partition (may include "post-sched" split/spill moves):
;*
;*                                                A-side   B-side
;*      .L units                                     0        0     
;*      .S units                                     0        0     
;*      .M units                                     0        0     
;*      .N units                                     0        0     
;*      .D units                                     1        -     
;*      .B units                                     1        -     
;*      .C units                                     -        0     
;*      .P units                                     -        0     
;*
;*      .M/.N units                                  2        0     
;*      .L/.S units                                  2        2     
;*      .L/.S/.C units                               0        0     
;*      .L/.S/.C/.M units                            0        2     
;*      .L/.S/.C/.M/.D units                         5        2     
;*
;*      .X cross paths                               0        0     
;*
;*      Bound(.D1 .D2 .D)                            1        -     
;*      Bound(.M .N .MN)                             1        0     
;*      Bound(.L .S .LS)                             1        1     
;*      Bound(.L .S .C .LS .LSC)                     1        1     
;*      Bound(.L .S .C .M .LS .LSC .LSCM)            1        1     
;*      Bound(.L .S .C .M .D .LS .LSC .LSCM .LSCMD)  2*       2*    
;*
;*      Register Usage Tables:
;*
;*      +----------------------------------+----------------------------------+
;*      |              ASIDE               |              BSIDE               |
;*      +----------------+--------+--------+----------------+--------+--------+
;*      |      "Axx"     |  "ALx" |  "AMx" |     "VBxx"     | "VBLx" | "VBMx" |
;*      +----------------|--------|--------|----------------|--------|--------+
;*      |0000000000111111|00000000|00000000|0000000000111111|00000000|00000000|
;*      |0123456789012345|01234567|01234567|0123456789012345|01234567|01234567|
;*      +----------------|--------|--------|----------------|--------|--------+
;*   0: |**              |****    |*****   |*               |* *     |*       |
;*   1: |**              |****    |* ***   |*               |***     |*       |
;*      +----------------+--------+--------+----------------+--------+--------+
;*
;*      +----------------+                 +--------+       +-------+
;*      |      "Dxx"     |                 |  "Px"  |       |"CUCRx"|
;*      +----------------+                 +--------+       +-------+
;*      |0000000000111111|                 |00000000|       |0000   |
;*      |0123456789012345|                 |01234567|       |0123   |
;*      +----------------+                 +--------+       +-------+
;*   0: |****            |                 |        |       |       |
;*   1: |*****           |                 |        |       |       |
;*      +----------------+                 +--------+       +-------+
;*
;*      Done
;*
;*      Redundant loop generated
;*      Epilog not entirely removed
;*      Collapsed epilog stages       : 6
;*
;*      Prolog not removed
;*      Collapsed prolog stages       : 0
;*
;*      Max amt of load speculation   : 0 bytes
;*
;*      Minimum safe trip count       : 4
;*
;*
;*      Mem bank conflicts/iter(est.) : { min 0.000, est 0.000, max 0.000 }
;*      Mem bank perf. penalty (est.) : 0.0%
;*
;*
;*      Total cycles (est.)         : 18 + trip_cnt * 2        
;*----------------------------------------------------------------------------*
;*        SINGLE SCHEDULED ITERATION
;*
;*        ||$C$C2856||:
;*   0              TICK                               ; [A_U] 
;*   1              MPYWW   .N1     AM2,A1,D4         ; [A_N1] 
;*   2              CMPEQW  .L1     A1,AL3,AM0        ; [A_L1] |270| 
;*     ||           ADDW    .D2     A1,0x1,A1         ; [A_D2] |266| 
;*   3              MV      .M1     AM0,AL0           ; [A_M1] |270| Split a long life
;*   4              NOP     0x1     ; [A_B] 
;*   5              ADDW    .D2     D3,D4,AM1         ; [A_D2] 
;*     ||           MVDLY3  .L1     AL0,AL1           ; [A_L1] |270| Split a long life
;*   6              MPYWW   .N1     AM4,AM1,AM0       ; [A_N1] 
;*   7              NOP     0x2     ; [A_B] 
;*   9              MVDLY4  .S1     AL1,AL2           ; [A_S1] |270| Split a long life
;*  10              ADDW    .M1     AM3,AM0,D0        ; [A_M1] 
;*  11              ADDW    .D1     D2,D0,D0          ; [A_D1] 
;*  12              LDH     .D1     *D1[D0],B0        ; [A_D1]  ^ 
;*  13              NOP     0x1     ; [A_B] 
;*  14              MVDLY4  .S1     AL2,A0            ; [A_S1] |270| Split a long life
;*  15              NOP     0x2     ; [A_B] 
;*  17              MV      .M2     B0,BL0            ; [B_M2]  ^ 
;*  18              VMAXH   .S2     B0,BL2,BL1        ; [B_S2] |272|  ^ 
;*     ||           VMINH   .M2     B0,BM0,BL0        ; [B_M2] |273|  ^ 
;*     ||   [!A0]   MV      .L2     BL0,B0            ; [B_L2]  ^ 
;*  19      [!A0]   EXT     .L2     BL1,0x30,0x30,BL2 ; [B_L2] |272|  ^ [C1]
;*     ||   [!A0]   EXT     .S2     BL0,0x30,0x30,BM0 ; [B_S2] |273|  ^ [C0]
;*     ||           BNL     .B1     ||$C$C2856||      ; [A_B] |266| 
;*  20              ; BRANCHCC OCCURS {||$C$C2856||}  ; [] |266| 
;*----------------------------------------------------------------------------*
||$C$L182||:    ; PIPED LOOP PROLOG
;          EXCLUSIVE CPU CYCLES: 15
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 270,column 15,is_stmt,isa 0

           TICK                               ; [A_U] <1,0> 
||         MV      .M1     AM0,AL0           ; [A_M1] |270| <0,3> Split a long life
||         MPYWW   .N1     AM2,A0,D0         ; [A_N1] <1,1> 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 266,column 26,is_stmt,isa 0

           MV      .D1     A15,D3            ; [A_D1] 
||         ADDW    .D2     A0,0x1,A0         ; [A_D2] |266| <1,2> 
||         CMPEQW  .L1     A0,AL3,AM0        ; [A_L1] |270| <1,2> 
||         TICK                               ; [A_U] <2,0> 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 270,column 15,is_stmt,isa 0

           MV      .D1     A13,AM4           ; [A_D1] 
||         MVDLY3  .L1     AL0,AL0           ; [A_L1] |270| <0,5> Split a long life
||         ADDW    .D2     D3,D0,AM0         ; [A_D2] <0,5> 
||         MV      .M1     AM0,AL0           ; [A_M1] |270| <1,3> Split a long life
||         MPYWW   .N1     AM2,A0,D0         ; [A_N1] <2,1> 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 266,column 26,is_stmt,isa 0

           MPYWW   .N1     AM4,AM0,AM1       ; [A_N1] <0,6> 
||         ADDW    .D2     A0,0x1,A0         ; [A_D2] |266| <2,2> 
||         CMPEQW  .L1     A0,AL3,AM0        ; [A_L1] |270| <2,2> 
||         TICK                               ; [A_U] <3,0> 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 270,column 15,is_stmt,isa 0

           MVDLY3  .L1     AL0,AL0           ; [A_L1] |270| <1,5> Split a long life
||         ADDW    .D2     D3,D0,AM0         ; [A_D2] <1,5> 
||         MV      .M1     AM0,AL1           ; [A_M1] |270| <2,3> Split a long life
||         MPYWW   .N1     AM2,A0,D0         ; [A_N1] <3,1> 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 266,column 26,is_stmt,isa 0

           MPYWW   .N1     AM4,AM0,AM1       ; [A_N1] <1,6> 
||         ADDW    .D2     A0,0x1,A0         ; [A_D2] |266| <3,2> 
||         CMPEQW  .L1     A0,AL3,AM0        ; [A_L1] |270| <3,2> 
||         TICK                               ; [A_U] <4,0> 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 270,column 15,is_stmt,isa 0

           MV      .D1     A12,AM3           ; [A_D1] 
||         MVDLY4  .S1     AL0,AL2           ; [A_S1] |270| <0,9> Split a long life
||         MVDLY3  .L1     AL1,AL0           ; [A_L1] |270| <2,5> Split a long life
||         ADDW    .D2     D3,D0,AM0         ; [A_D2] <2,5> 
||         MV      .M1     AM0,AL1           ; [A_M1] |270| <3,3> Split a long life
||         MPYWW   .N1     AM2,A0,D0         ; [A_N1] <4,1> 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 266,column 26,is_stmt,isa 0

           MV      .S1     A6,D1             ; [A_S1] 
||         MV      .D1     A11,D2            ; [A_D1] 
||         ADDW    .M1     AM3,AM1,D11       ; [A_M1] <0,10> 
||         MPYWW   .N1     AM4,AM0,AM1       ; [A_N1] <2,6> 
||         ADDW    .D2     A0,0x1,A0         ; [A_D2] |266| <4,2> 
||         CMPEQW  .L1     A0,AL3,AM0        ; [A_L1] |270| <4,2> 
||         TICK                               ; [A_U] <5,0> 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 270,column 15,is_stmt,isa 0

           ADDW    .D1     D2,D11,D0         ; [A_D1] <0,11> 
||         MVDLY4  .S1     AL0,AL2           ; [A_S1] |270| <1,9> Split a long life
||         MVDLY3  .L1     AL1,AL0           ; [A_L1] |270| <3,5> Split a long life
||         ADDW    .D2     D3,D0,AM0         ; [A_D2] <3,5> 
||         MV      .M1     AM0,AL1           ; [A_M1] |270| <4,3> Split a long life
||         MPYWW   .N1     AM2,A0,D0         ; [A_N1] <5,1> 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 266,column 26,is_stmt,isa 0

           LDH     .D1     *D1[D0],B0        ; [A_D1] <0,12>  ^ 
||         ADDW    .M1     AM3,AM1,D11       ; [A_M1] <1,10> 
||         MPYWW   .N1     AM4,AM0,AM1       ; [A_N1] <3,6> 
||         ADDW    .D2     A0,0x1,A0         ; [A_D2] |266| <5,2> 
||         CMPEQW  .L1     A0,AL3,AM0        ; [A_L1] |270| <5,2> 
||         TICK                               ; [A_U] <6,0> 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 270,column 15,is_stmt,isa 0

           ADDW    .D1     D2,D11,D0         ; [A_D1] <1,11> 
||         MVDLY4  .S1     AL0,AL2           ; [A_S1] |270| <2,9> Split a long life
||         MVDLY3  .L1     AL1,AL0           ; [A_L1] |270| <4,5> Split a long life
||         ADDW    .D2     D3,D0,AM0         ; [A_D2] <4,5> 
||         MV      .M1     AM0,AL1           ; [A_M1] |270| <5,3> Split a long life
||         MPYWW   .N1     AM2,A0,D0         ; [A_N1] <6,1> 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 266,column 26,is_stmt,isa 0

           MVDLY4  .S1     AL2,A0            ; [A_S1] |270| <0,14> Split a long life
||         LDH     .D1     *D1[D0],B0        ; [A_D1] <1,12>  ^ 
||         ADDW    .M1     AM3,AM1,D11       ; [A_M1] <2,10> 
||         MPYWW   .N1     AM4,AM0,AM0       ; [A_N1] <4,6> 
||         ADDW    .D2     A0,0x1,A0         ; [A_D2] |266| <6,2> 
||         CMPEQW  .L1     A0,AL3,AM0        ; [A_L1] |270| <6,2> 
||         TICK                               ; [A_U] <7,0> 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 270,column 15,is_stmt,isa 0

           ADDW    .D1     D2,D11,D0         ; [A_D1] <2,11> 
||         MVDLY4  .S1     AL0,AL2           ; [A_S1] |270| <3,9> Split a long life
||         MVDLY3  .L1     AL1,AL1           ; [A_L1] |270| <5,5> Split a long life
||         ADDW    .D2     D3,D0,AM0         ; [A_D2] <5,5> 
||         MV      .M1     AM0,AL1           ; [A_M1] |270| <6,3> Split a long life
||         MPYWW   .N1     AM2,A0,D4         ; [A_N1] <7,1> 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 266,column 26,is_stmt,isa 0

           MVDLY4  .S1     AL2,A0            ; [A_S1] |270| <1,14> Split a long life
||         LDH     .D1     *D1[D0],B0        ; [A_D1] <2,12>  ^ 
||         ADDW    .M1     AM3,AM1,D11       ; [A_M1] <3,10> 
||         MPYWW   .N1     AM4,AM0,AM0       ; [A_N1] <5,6> 
||         ADDW    .D2     A0,0x1,A1         ; [A_D2] |266| <7,2> 
||         CMPEQW  .L1     A0,AL3,AM0        ; [A_L1] |270| <7,2> 
||         TICK                               ; [A_U] <8,0> 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 270,column 15,is_stmt,isa 0

           MV      .S2     B2,BL2            ; [B_S2] 
||         MV      .L2     B1,BM0            ; [B_L2] 
||         MV      .M2     B0,BL0            ; [B_M2] <0,17>  ^ 
||         ADDW    .D1     D2,D11,D0         ; [A_D1] <3,11> 
||         MVDLY4  .S1     AL0,AL2           ; [A_S1] |270| <4,9> Split a long life
||         MVDLY3  .L1     AL1,AL1           ; [A_L1] |270| <6,5> Split a long life
||         ADDW    .D2     D3,D0,AM1         ; [A_D2] <6,5> 
||         MV      .M1     AM0,AL0           ; [A_M1] |270| <7,3> Split a long life
||         MPYWW   .N1     AM2,A1,D4         ; [A_N1] <8,1> 

;** --------------------------------------------------------------------------*
||$C$L183||:    ; PIPED LOOP KERNEL
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 266,column 26,is_stmt,isa 0

   [!A0]   MV      .L2     BL0,B0            ; [B_L2] <0,18>  ^ 
||         VMAXH   .S2     B0,BL2,BL1        ; [B_S2] |272| <0,18>  ^ 
||         VMINH   .M2     B0,BM0,BL0        ; [B_M2] |273| <0,18>  ^ 
||         MVDLY4  .S1     AL2,A0            ; [A_S1] |270| <2,14> Split a long life
||         LDH     .D1     *D1[D0],B0        ; [A_D1] <3,12>  ^ 
||         ADDW    .M1     AM3,AM0,D0        ; [A_M1] <4,10> 
||         MPYWW   .N1     AM4,AM1,AM0       ; [A_N1] <6,6> 
||         ADDW    .D2     A1,0x1,A1         ; [A_D2] |266| <8,2> 
||         CMPEQW  .L1     A1,AL3,AM0        ; [A_L1] |270| <8,2> 
||         TICK                               ; [A_U] <9,0> 

           BNL     .B1     ||$C$L183||       ; [A_B] |266| <0,19> 
|| [!A0]   EXT     .L2     BL1,0x30,0x30,BL2 ; [B_L2] |272| <0,19>  ^ [C1]
|| [!A0]   EXT     .S2     BL0,0x30,0x30,BM0 ; [B_S2] |273| <0,19>  ^ [C0]
||         MV      .M2     B0,BL0            ; [B_M2] <1,17>  ^ 
||         ADDW    .D1     D2,D0,D0          ; [A_D1] <4,11> 
||         MVDLY4  .S1     AL1,AL2           ; [A_S1] |270| <5,9> Split a long life
||         MVDLY3  .L1     AL0,AL1           ; [A_L1] |270| <7,5> Split a long life
||         ADDW    .D2     D3,D4,AM1         ; [A_D2] <7,5> 
||         MV      .M1     AM0,AL0           ; [A_M1] |270| <8,3> Split a long life
||         MPYWW   .N1     AM2,A1,D4         ; [A_N1] <9,1> 

;** --------------------------------------------------------------------------*
||$C$L184||:    ; PIPED LOOP EPILOG
;          EXCLUSIVE CPU CYCLES: 7
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 270,column 15,is_stmt,isa 0

           MV      .D1     D2,A11            ; [A_D1] 
||         MV      .M1     AM3,A12           ; [A_M1] 
|| [!A0]   MV      .L2     BL0,B0            ; [B_L2] <7,18>  ^ 
||         VMAXH   .S2     B0,BL2,BL1        ; [B_S2] |272| <7,18>  ^ 
||         VMINH   .M2     B0,BM0,BL0        ; [B_M2] |273| <7,18>  ^ 
||         MVDLY4  .S1     AL2,A0            ; [A_S1] |270| <9,14> Split a long life

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 272,column 17,is_stmt,isa 0

           MV      .M1     AM2,A4            ; [A_M1] 
|| [!A0]   EXT     .L2     BL1,0x30,0x30,BL2 ; [B_L2] |272| <7,19>  ^ [C1]
|| [!A0]   EXT     .S2     BL0,0x30,0x30,BM0 ; [B_S2] |273| <7,19>  ^ [C0]
||         MV      .M2     B0,BL0            ; [B_M2] <8,17>  ^ 

   [!A0]   MV      .L2     BL0,B0            ; [B_L2] <8,18>  ^ 
||         VMAXH   .S2     B0,BL2,BL1        ; [B_S2] |272| <8,18>  ^ 
||         VMINH   .M2     B0,BM0,BL0        ; [B_M2] |273| <8,18>  ^ 

   [!A0]   EXT     .L2     BL1,0x30,0x30,BL2 ; [B_L2] |272| <8,19>  ^ [C1]
|| [!A0]   EXT     .S2     BL0,0x30,0x30,BM0 ; [B_S2] |273| <8,19>  ^ [C0]
||         MV      .M2     B0,BL0            ; [B_M2] <9,17>  ^ 

   [!A0]   MV      .L2     BL0,B0            ; [B_L2] <9,18>  ^ 
||         VMAXH   .S2     B0,BL2,BL1        ; [B_S2] |272| <9,18>  ^ 
||         VMINH   .M2     B0,BM0,BL0        ; [B_M2] |273| <9,18>  ^ 

   [!A0]   EXT     .L2     BL1,0x30,0x30,BL2 ; [B_L2] |272| <9,19>  ^ [C1]
|| [!A0]   EXT     .S2     BL0,0x30,0x30,BM0 ; [B_S2] |273| <9,19>  ^ [C0]

           MV      .L2     BL2,B2            ; [B_L2] 
||         MV      .M2     BM0,B1            ; [B_M2] 
||         PROT                               ; [A_U] 

;** --------------------------------------------------------------------------*
||$C$L185||:    
;          EXCLUSIVE CPU CYCLES: 1
           MV      .L1X    B0,AL0            ; [A_L1] 
;** --------------------------------------------------------------------------*
||$C$L186||:    
;          EXCLUSIVE CPU CYCLES: 95
	.dwpsn	file "./inc/tidl_detectionOutput_int.h",line 134,column 25,is_stmt,isa 0

           VINTSP  .L2     B2,BM0            ; [B_L2] |134| 
||         VINTSP  .S2     B1,BM3            ; [B_S2] |283| 

	.dwpsn	file "./inc/tidl_detectionOutput_int.h",line 135,column 25,is_stmt,isa 0
           VINTSP  .L1     AL0,AM0           ; [A_L1] |135| 
	.dwpsn	file "./inc/tidl_detectionOutput_int.h",line 134,column 25,is_stmt,isa 0

           MPYSP   .N2     B5,BM0,BM0        ; [B_N2] |134| 
||         MPYSP   .M2     B5,BM3,BM3        ; [B_M2] |283| 

	.dwpsn	file "./inc/tidl_detectionOutput_int.h",line 135,column 25,is_stmt,isa 0
           MPYSP   .N1X    B5,AM0,A0         ; [A_N1] |135| 
           SUBSP   .C2     BM3,BM0,BM3       ; [B_C] |135| 
           SUBSP   .C2X    A0,BM0,BM0        ; [B_C] |135| 
           MPYSP   .N2     BM2,BM3,B2        ; [B_N2] |135| 
           MPYSP   .N2     BM2,BM0,B10       ; [B_N2] |135| 
           VSPTRUNC .L2    B2,B0             ; [B_L2] |135| 
           VSPTRUNC .L2    B10,B1            ; [B_L2] |135| 
	.dwpsn	file "./inc/tidl_detectionOutput_int.h",line 157,column 3,is_stmt,isa 0
           VINTSP  .L2     B0,BM0            ; [B_L2] |157| 

           SUBSP   .C2     B2,BM0,BM0        ; [B_C] |157| 
||         VINTSP  .L1X    B1,AL0            ; [A_L1] |157| 

           MPYSP   .N2     B8,BM0,BM3        ; [B_N2] |157| 
||         SUBSP   .L1X    B10,AL0,AM0       ; [A_L1] |157| 

           MPYSP   .N1X    B8,AM0,A0         ; [A_N1] |157| 
           MPYSP   .N2     BM3,BM3,BM0       ; [B_N2] |157| 
           MPYSP   .N1     A0,A0,AM0         ; [A_N1] |157| 
           MPYSP   .N2     BM3,BM0,BM5       ; [B_N2] |157| 
           MPYSP   .N2     B6,BM0,BM4        ; [B_N2] |157| 
           ADDSP   .C2     B4,BM3,BM7        ; [B_C] |157| 

           MPYSP   .N2     BM0,BM0,BM0       ; [B_N2] |157| 
||         MPYSP   .N1     A0,AM0,AM1        ; [A_N1] |157| 

           MPYSP   .N2     B7,BM5,BM5        ; [B_N2] |157| 
||         MPYSP   .N1X    B6,AM0,AL0        ; [A_N1] |157| 

           ADDSP   .C2     BM4,BM7,BM3       ; [B_C] |157| 
||         ADDSP   .L1X    B4,A0,AL1         ; [A_L1] |157| 
||         MASKB   .P2     0x4,P1            ; [B_P] |157| 

           MPYSP   .N1     AM0,AM0,AM0       ; [A_N1] |157| 
||         MVKU32  .L2     0,BL0             ; [B_L2] |157| 

           MPYSP   .N2     B9,BM0,BM0        ; [B_N2] |157| 
||         MPYSP   .N1X    B7,AM1,AL2        ; [A_N1] |157| 
||         SUBRW   .S2     B0,0,BL0          ; [B_S2] |157| 
||         VCMPGTW .L2     B0,BL0,P0         ; [B_L2] |157| 

           ADDSP   .C2     BM5,BM3,BM3       ; [B_C] |157| 
||         ADDSP   .L1     AL0,AL1,AL0       ; [A_L1] |157| 
||         SHRW    .L2     BL4,BL0,BL0       ; [B_L2] |157| 
||         AND     .P2     P0,P1,P0          ; [B_P] |157| 
||         SHLW    .S2     BL4,B0,BL1        ; [B_S2] |157| 

           VSEL    .L2     P0,BL1,BL0,BM6    ; [B_L2] |157| 

           MPYSP   .N1X    B9,AM0,AL1        ; [A_N1] |157| 
||         VINTSP  .C2     BM6,BM4           ; [B_C] |157| 
||         MASKB   .P2     0x4,P1            ; [B_P] |157| 

           ADDSP   .C2     BM0,BM3,BM0       ; [B_C] |157| 
||         ADDSP   .L1     AL2,AL0,AL0       ; [A_L1] |157| 

           VCMPGTW .L2     B1,BL7,P0         ; [B_L2] |157| 
||         SUBRW   .S2     B1,0,BL2          ; [B_S2] |157| 

           SHLW    .L2     BL4,B1,B2         ; [B_L2] |157| 
||         AND     .P2     P0,P1,P0          ; [B_P] |157| 
||         SHRW    .S2     BL4,BL2,BL0       ; [B_S2] |157| 

           MPYSP   .N2     BM0,BM4,BM0       ; [B_N2] |157| 
||         ADDSP   .L1     AL1,AL0,A0        ; [A_L1] |157| 
||         VSEL    .L2     P0,B2,BL0,BL0     ; [B_L2] |157| 

           VINTSP  .L2     BL0,B10           ; [B_L2] |157| 

           MPYSP   .N2     BM1,BM0,BM0       ; [B_N2] |157| 
||         MPYSP   .M2X    A0,B10,BM3        ; [B_M2] |157| 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 288,column 13,is_stmt,isa 0
           ADDW    .L2     B3,0xfffffffe,BL1 ; [B_L2] |288| 
	.dwpsn	file "./inc/tidl_detectionOutput_int.h",line 161,column 3,is_stmt,isa 0

           VINTSP  .S2     BL1,BM7           ; [B_S2] |288| 
||         CMPGEW  .L1X    B0,0xfffffff0,A3  ; [A_L1] |161| 

	.dwpsn	file "./inc/tidl_detectionOutput_int.h",line 157,column 3,is_stmt,isa 0

           MPYSP   .N2     BM1,BM3,BM3       ; [B_N2] |157| 
|| [!A3]   MVKU32  .L2     0,BM0             ; [B_L2] |162| 
||         CMPGTW  .L1X    B0,0xe,A1         ; [A_L1] |164| 

	.dwpsn	file "./inc/tidl_detectionOutput_int.h",line 165,column 5,is_stmt,isa 0
   [ A1]   MV      .L2     BL5,BM0           ; [B_L2] |165| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 288,column 13,is_stmt,isa 0
           MPYSP   .N2     BM0,BM7,BM0       ; [B_N2] |288| 
	.dwpsn	file "./inc/tidl_detectionOutput_int.h",line 161,column 3,is_stmt,isa 0
           CMPGEW  .L1X    B1,0xfffffff0,A3  ; [A_L1] |161| 
	.dwpsn	file "./inc/tidl_detectionOutput_int.h",line 162,column 5,is_stmt,isa 0

           CMPGTW  .L1X    B1,0xe,A1         ; [A_L1] |164| 
|| [!A3]   MVKU32  .L2     0,BM3             ; [B_L2] |162| 

	.dwpsn	file "./inc/tidl_detectionOutput_int.h",line 165,column 5,is_stmt,isa 0
   [ A1]   MV      .L2     BL5,BM3           ; [B_L2] |165| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 288,column 13,is_stmt,isa 0
           ADDSP   .C2     BM0,BM3,BL0       ; [B_C] |288| 
           CMPGTW  .L1     AL7,A9,A0         ; [A_L1] |288| 
           CMPLESP .L2     BL0,B15,BL0       ; [B_L2] |288| 
           ANDW    .L2X    A0,BL0,B0         ; [B_L2] |288| 
           CMPEQW  .L1X    B0,0,A0           ; [A_L1] |288| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 293,column 17,is_stmt,isa 0
   [!A0]   LDD     .D1     *A5(1184),D0      ; [A_D1] |293| 
           ADDW    .D1     A11,A12,D1        ; [A_D1] |293| 
           ORW     .D1     D9,D1,D1          ; [A_D1] |293| 
   [!A0]   STW     .D1     D1,*D0[A9]        ; [A_D1] |293| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 294,column 17,is_stmt,isa 0
   [!A0]   LDD     .D1     *A5(1192),D0      ; [A_D1] |294| 
   [!A0]   LDW     .D1     *D0(0),BL0        ; [A_D1] |294| 
           MV      .L1X    B13,A1            ; [A_L1] 

           EXT     .L1     A1,0x20,0x20,AM7  ; [A_L1] 
||         MVKU32  .S1     0xf8,AM1          ; [A_S1] 

           MPYDD   .N1     AM1,AM7,D14       ; [A_N1] 
   [!A0]   ADDW    .L2     BL0,0x1,B0        ; [B_L2] |294| 

   [!A0]   STW     .D1X    B0,*D0(0)         ; [A_D1] |294| 
||         ADDD    .D2     D13,D14,D12       ; [A_D2] 

   [!A0]   LDW     .D1     *D12(8),A8        ; [A_D1] 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 252,column 34,is_stmt,isa 0
           ADDW    .D1     A11,0x1,A11       ; [A_D1] |252| 

           CMPGTW  .L1     A8,A11,A0         ; [A_L1] |252| 
|| [!A0]   ADDW    .D1     A9,0x1,A9         ; [A_D1] |295| 

   [ A0]   B       .B1     ||$C$L179||       ; [A_B] |252| 
           ; BRANCHCC OCCURS {||$C$L179||}   ; [] |252| 
;** --------------------------------------------------------------------------*
||$C$L187||:    
;          EXCLUSIVE CPU CYCLES: 14
           LDD     .D1     *SP(16),A0        ; [A_D1] 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 250,column 32,is_stmt,isa 0
           LDW     .D1     *A0(12),AL0       ; [A_D1] |250| 
           ADDW    .D1     A10,0x1,A10       ; [A_D1] |250| 
           CMPGTW  .L1     AL0,A10,A0        ; [A_L1] |250| 
   [ A0]   B       .B1     ||$C$L178||       ; [A_B] |250| 
           ; BRANCHCC OCCURS {||$C$L178||}   ; [] |250| 
;** --------------------------------------------------------------------------*
||$C$L188||:    
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 246,column 20,is_stmt,isa 0
           ADDW    .D1     A2,0xffffffff,A2  ; [A_D1] |246| 

   [ A2]   B       .B1     ||$C$L177||       ; [A_B] |246| 
||         ADDW    .D1     A7,0x1,A7         ; [A_D1] |246| 

           ; BRANCHCC OCCURS {||$C$L177||}   ; [] |246| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 7
           LDD     .D1     *SP(48),A0        ; [A_D1] 
           LDW     .D1     *A0(56),AL0       ; [A_D1] 
;** --------------------------------------------------------------------------*
||$C$L189||:    
;          EXCLUSIVE CPU CYCLES: 9
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 229,column 17,is_stmt,isa 0
           LDD     .D1     *SP(16),A3        ; [A_D1] |229| 

           LDD     .D1     *SP(32),A1        ; [A_D1] |229| 
||         LDD     .D2     *SP(24),A2        ; [A_D2] |229| 

           MV      .L1X    B13,A0            ; [A_L1] 
           ADDW    .D1     A0,0x1,A0         ; [A_D1] |229| 

           ADDD    .D2     A3,0xf8,A3        ; [A_D2] |229| 
||         ADDD    .D1     D7,0x4,D7         ; [A_D1] |229| 

           STD     .D1     A3,*SP(16)        ; [A_D1] |229| 
||         ADDD    .S1     A1,0x1,A1         ; [A_S1] |229| 
||         ADDD    .M1     A2,0xc,A2         ; [A_M1] |229| 
||         MV      .L2X    A0,B13            ; [B_L2] |229| 
||         CMPGTW  .L1     AL0,A0,A4         ; [A_L1] |229| 
||         ADDD    .D2     D8,0xc,D8         ; [A_D2] |229| 

   [ A4]   B       .B1     ||$C$L174||       ; [A_B] |229| 
||         STD     .D1     A2,*SP(24)        ; [A_D1] |229| 
||         STD     .D2X    A1,*SP(32)        ; [A_D2] |229| 

           ; BRANCHCC OCCURS {||$C$L174||}   ; [] |229| 
;** --------------------------------------------------------------------------*
||$C$L190||:    
;          EXCLUSIVE CPU CYCLES: 16

           LDD     .D2     *SP(184),A13      ; [A_D2] 
||         VLD64B  .D1     *SP(56),VB14      ; [A_D1] 

	.dwcfi	restore_reg, 62
           VLD64B  .D1     *SP(120),VB15     ; [A_D1] 
	.dwcfi	restore_reg, 63
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 304,column 3,is_stmt,isa 0

           MVC     .S1     A13,RP            ; [A_S1] 
||         MV      .D1     A9,A4             ; [A_D1] |304| 
	.dwcfi	restore_reg, 4101

           LDD     .D1     *SP(200),A14      ; [A_D1] 
||         LDD     .D2     *SP(192),A15      ; [A_D2] 
	.dwcfi	restore_reg, 14
	.dwcfi	restore_reg, 15

           LDD     .D1     *SP(224),A11      ; [A_D1] 
||         LDD     .D2     *SP(216),A12      ; [A_D2] 
	.dwcfi	restore_reg, 11
	.dwcfi	restore_reg, 12

           LDD     .D1     *SP(248),A8       ; [A_D1] 
||         LDD     .D2     *SP(232),A10      ; [A_D2] 
	.dwcfi	restore_reg, 8
	.dwcfi	restore_reg, 10

           LDD     .D1     *SP(208),A13      ; [A_D1] 
||         LDD     .D2     *SP(240),A9       ; [A_D2] 

	.dwcfi	restore_reg, 13
	.dwcfi	restore_reg, 9
$C$DW$51	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$51, DW_AT_low_pc(0x00)
	.dwattr $C$DW$51, DW_AT_TI_return


           RET     .B1     ; [A_B] 
||         ADDD    .D1     SP,0xf0,SP        ; [A_D1] 

	.dwcfi	cfa_offset, 0
           ; RETURN OCCURS {RP}              ; [] 
	.dwattr $C$DW$43, DW_AT_TI_end_file("src/tidl_detectionOutput_score.c")
	.dwattr $C$DW$43, DW_AT_TI_end_line(0x131)
	.dwattr $C$DW$43, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$43

	.sect	".text:_Z25TIDL_findValidLocation_cnIfEiP26sTIDL_DetectOutputParams_tP29sTIDL_ALgDetectOutputParams_tPf"
	.clink
	.global	||_Z25TIDL_findValidLocation_cnIfEiP26sTIDL_DetectOutputParams_tP29sTIDL_ALgDetectOutputParams_tPf||

$C$DW$52	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$52, DW_AT_name("int TIDL_findValidLocation_cn")
	.dwattr $C$DW$52, DW_AT_low_pc(||_Z25TIDL_findValidLocation_cnIfEiP26sTIDL_DetectOutputParams_tP29sTIDL_ALgDetectOutputParams_tPf||)
	.dwattr $C$DW$52, DW_AT_high_pc(0x00)
	.dwattr $C$DW$52, DW_AT_linkage_name("_Z25TIDL_findValidLocation_cnIfEiP26sTIDL_DetectOutputParams_tP29sTIDL_ALgDetectOutputParams_tPf")
	.dwattr $C$DW$52, DW_AT_external
	.dwattr $C$DW$52, DW_AT_decl_file("src/tidl_detectionOutput_score.c")
	.dwattr $C$DW$52, DW_AT_decl_line(0x6e)
	.dwattr $C$DW$52, DW_AT_decl_column(0x09)
	.dwattr $C$DW$52, DW_AT_TI_max_frame_size(0xc8)
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 113,column 1,is_stmt,address ||_Z25TIDL_findValidLocation_cnIfEiP26sTIDL_DetectOutputParams_tP29sTIDL_ALgDetectOutputParams_tPf||,isa 0

	.dwfde $C$DW$CIE, ||_Z25TIDL_findValidLocation_cnIfEiP26sTIDL_DetectOutputParams_tP29sTIDL_ALgDetectOutputParams_tPf||
$C$DW$53	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$53, DW_AT_name("params")
	.dwattr $C$DW$53, DW_AT_location[DW_OP_reg4]

$C$DW$54	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$54, DW_AT_name("algDetLyrParams")
	.dwattr $C$DW$54, DW_AT_location[DW_OP_reg5]

$C$DW$55	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$55, DW_AT_name("priorData")
	.dwattr $C$DW$55, DW_AT_location[DW_OP_reg6]


;******************************************************************************
;* FUNCTION NAME: int TIDL_findValidLocation_cn<float>(sTIDL_DetectOutputParams_t *, sTIDL_ALgDetectOutputParams_t *, float *)*
;*                                                                            *
;*   Regs Modified     : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,A14,   *
;*                           A15,VB0,VB1,VB2,VB3,VB4,VB5,VB6,VB7,VB8,VB9,VB10,*
;*                           VB11,VB12,VB13,VB14,VB15,AL0,AL1,AL2,AL3,AL4,AL5,*
;*                           AL6,AL7,AM0,AM1,AM2,AM3,AM4,AM5,AM6,AM7,D0,D1,D2,*
;*                           D3,D4,D5,D6,D7,D8,D9,D10,D11,D12,D13,D14,SP,VBL0,*
;*                           VBL1,VBL2,VBL3,VBL4,VBL5,VBL6,VBL7,VBM0,VBM1,    *
;*                           VBM2,VBM3,VBM4,VBM5,VBM6,VBM7,P0,P1,P2,P3,P4,P5, *
;*                           P6,P7,CUCR0,CUCR1,CUCR2,CUCR3                    *
;*   Regs Used         : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,A14,   *
;*                           A15,VB0,VB1,VB2,VB3,VB4,VB5,VB6,VB7,VB8,VB9,VB10,*
;*                           VB11,VB12,VB13,VB14,VB15,AL0,AL1,AL2,AL3,AL4,AL5,*
;*                           AL6,AL7,AM0,AM1,AM2,AM3,AM4,AM5,AM6,AM7,D0,D1,D2,*
;*                           D3,D4,D5,D6,D7,D8,D9,D10,D11,D12,D13,D14,SP,VBL0,*
;*                           VBL1,VBL2,VBL3,VBL4,VBL5,VBL6,VBL7,VBM0,VBM1,    *
;*                           VBM2,VBM3,VBM4,VBM5,VBM6,VBM7,P0,P1,P2,P3,P4,P5, *
;*                           P6,P7,CUCR0,CUCR1,CUCR2,CUCR3                    *
;*   Local Frame Size  : 0 Args + 0 Auto + 200 Save = 200 byte                *
;******************************************************************************
||_Z25TIDL_findValidLocation_cnIfEiP26sTIDL_DetectOutputParams_tP29sTIDL_ALgDetectOutputParams_tPf||:
;** --------------------------------------------------------------------------*
;* D5    assigned to $O$C16
;* VBM3  assigned to $O$C17
;* VBM2  assigned to $O$C18
;* VB2   assigned to $O$C19
;* A0    assigned to $O$C20
;* AM0   assigned to $O$C21
;* VB3   assigned to $O$C22
;* D3    assigned to $O$C23
;* A2    assigned to $O$C24
;* A3    assigned to $O$C25
;* A10   assigned to $O$C27
;* A1    assigned to $O$U124
;* VB0   assigned to $O$U124
;* AL0   assigned to $O$v3
;* AL0   assigned to $O$v3
;* VB5   assigned to $O$v2
;* A1    assigned to $O$v2
;* VB0   assigned to $O$K28
;* D13   assigned to $O$U53
;* VB6   assigned to $O$U60
;* VB5   assigned to $O$U65
;* D9    assigned to $O$U70
;* D1    assigned to $O$U102
;* A15   assigned to $O$U136
;* D2    assigned to $O$U132
;* A12   assigned to $O$U129
;* D2    assigned to $O$U172
;* D7    assigned to $O$U169
;* D0    assigned to $O$U85
;* D6    assigned to $O$U208
;* A8    assigned to $O$U212
;* VB15  assigned to $O$U217
;* D9    assigned to $O$U230
;* AL0   assigned to $O$U235
;* AL1   assigned to $O$U239
;* VBL1  assigned to $O$K252
;* VBL0  assigned to $O$K253
;* D7    assigned to $O$U270
;* D5    assigned to $O$U268
;* D8    assigned to $O$U279
;* AM0   assigned to $O$U258
;* D3    assigned to $O$U256
;* VBM1  assigned to $O$K303
;* VBM0  assigned to $O$K330
;* VB13  assigned to $O$K326
;* VB12  assigned to $O$K314
;* VB11  assigned to $O$K322
;* VB10  assigned to $O$K318
;* VB7   assigned to $O$K3
;* VB9   assigned to $O$K308
;* VB4   assigned to $O$U51
;* D12   assigned to $O$U84
;* D0    assigned to $O$U216
;* VB0   assigned to $O$U206
;* D2    assigned to $O$U267
;* D1    assigned to $O$U257
;* A3    assigned to $O$L1
;* A1    assigned to $O$L2
;* A0    assigned to $O$L3
;* AL0   assigned to $O$v5
;* AL1   assigned to $O$v4
;* VB14  assigned to $O$Lr581$upperBoundThSoftMax
;* VB2   assigned to $O$Lr590$upperBoundThSigmoid
;* A6    assigned to $O$Lr592$topMLocal
;* A11   assigned to $O$Lr593$anchorBox
;* VBL3  assigned to $O$Lr3$maxInit
;* VB1   assigned to $O$Lr568$i
;* A8    assigned to $O$Lr411$totalCnt
;* A10   assigned to $O$Lr56$totalCnt
;* A13   assigned to $O$Lr288$i
;* VB1   assigned to $O$Lr521$onebyqFact
;* AM3   assigned to $O$Lr538$linePitch
;* VB3   assigned to $O$Lr549$chPitch
;* A4    assigned to $O$Lr293$i3
;* A10   assigned to $O$Lr296$i4
;* VB9   assigned to $O$Lr409$curClassValidCnt
;* A14   assigned to $O$Lr502$curPartIndex
;* D11   assigned to $O$Lr461$curPlaneConf
;* D10   assigned to $O$Lr480$curPlaneObjConf
;* A7    assigned to $O$Lr382$cury
;* VB9   assigned to $O$Lr5$curClassValidCnt
;* A2    assigned to $O$Lr391$curx
;* D10   assigned to $O$Lr372$curPlaneConf
;* A7    assigned to $O$Lr307$cury
;* VB9   assigned to $O$Lr334$curClassValidCnt
;* VB8   assigned to $O$Lr336$totalCnt
;* A2    assigned to $O$Lr316$curx
;* A10   assigned to $O$Lr4$totalCnt
;* A15   assigned to $O$Lr14$i
;* VB6   assigned to $O$Lr259$onebyqFact
;* VB1   assigned to $O$Lr265$numPriors
;* AM2   assigned to $O$Lr276$linePitch
;* VB8   assigned to $O$Lr6$classStride
;* AM1   assigned to $O$Lr7$anchorStride
;* A13   assigned to $O$Lr19$i3
;* D4    assigned to $O$Lr240$curPartIndex
;* A14   assigned to $O$Lr22$cury
;* A12   assigned to $O$Lr31$curx
;* VBL2  assigned to $O$Lr219$maxConfObj
;* VB2   assigned to $O$Lr220$minConfObj
;* VB4   assigned to $O$Lr144$i4
;* VBM2  assigned to $O$Lr8$maxConfObj
;* AM3   assigned to $O$Lr9$minConfObj
;* VB1   assigned to $O$Lr214$bckVal
;* VB1   assigned to $O$Lr10$bckVal
;* A7    assigned to $O$Lr598$minConfObj
;* VBM2  assigned to $O$Lr600$maxConfObj
;* VBL4  assigned to $O$Lr602$bckVal
;* A0    assigned to $O$Lr107$min_obj_f
;* VBM2  assigned to $O$Lr106$max_obj_f
;* A2    assigned to $O$Lr111$yI
;* VBM3  assigned to $O$Lr11$ePwX
;* VB1   assigned to $O$Lr71$yI
;* VBM2  assigned to $O$Lr12$ePwX
;* A5    assigned to algDetLyrParams
$C$DW$56	.dwtag  DW_TAG_variable
	.dwattr $C$DW$56, DW_AT_name("algDetLyrParams")
	.dwattr $C$DW$56, DW_AT_type(*$C$DW$T$146)
	.dwattr $C$DW$56, DW_AT_location[DW_OP_reg5]

;* A9    assigned to params
$C$DW$57	.dwtag  DW_TAG_variable
	.dwattr $C$DW$57, DW_AT_name("params")
	.dwattr $C$DW$57, DW_AT_type(*$C$DW$T$144)
	.dwattr $C$DW$57, DW_AT_location[DW_OP_reg9]

	.dwcfi	cfa_offset, 0
;          EXCLUSIVE CPU CYCLES: 24

           MV      .L1     A4,A9             ; [A_L1] |113| 
||         STD     .D1     A8,*SP(8)         ; [A_D1] 
||         STD     .D2X    A9,*SP++(-200)    ; [A_D2] 
	.dwcfi	save_reg_to_mem, 8, 8
	.dwcfi	cfa_offset, 200
	.dwcfi	save_reg_to_mem, 9, -200

           LDUW    .D1     *A9(24),A5        ; [A_D1] |118| 
||         MV      .D2     A5,A8             ; [A_D2] |113| 

           STD     .D1     A10,*SP(192)      ; [A_D1] 
||         STD     .D2X    A13,*SP(168)      ; [A_D2] 
	.dwcfi	save_reg_to_mem, 10, 192
	.dwcfi	save_reg_to_mem, 13, 168

           VST64B  .D2     VB15,*SP(80)      ; [A_D2] 
||         STD     .D1     A11,*SP(184)      ; [A_D1] 
	.dwcfi	save_reg_to_mem, 63, 80
	.dwcfi	save_reg_to_mem, 11, 184

           VST64B  .D2     VB14,*SP(16)      ; [A_D2] 
||         MVC     .S1     RP,A13            ; [A_S1] 
||         STD     .D1     A15,*SP(152)      ; [A_D1] 

	.dwcfi	save_reg_to_mem, 62, 16
	.dwcfi	save_reg_to_mem, 15, 152
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 118,column 29,is_stmt,isa 0

           MVKU32  .L1     0x3f800000,A10    ; [A_L1] |118| 
||         STD     .D1     A14,*SP(160)      ; [A_D1] 

	.dwcfi	save_reg_to_mem, 14, 160
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 113,column 1,is_stmt,isa 0
$C$DW$58	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$58, DW_AT_low_pc(0x00)
	.dwattr $C$DW$58, DW_AT_name("__c7xabi_divf")
	.dwattr $C$DW$58, DW_AT_TI_call


           CALL    .B1     ||__c7xabi_divf|| ; [A_B] |118| 
||         STD     .D1     A13,*SP(144)      ; [A_D1] 
||         MV      .L1     A10,A4            ; [A_L1] |118| 
||         MV      .S1     A6,A11            ; [A_S1] |113| 
||         STD     .D2X    A12,*SP(176)      ; [A_D2] 

	.dwcfi	save_reg_to_mem, 4101, 144
	.dwcfi	save_reg_to_mem, 12, 176
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 118,column 29,is_stmt,isa 0
$C$RL2:    ; CALL OCCURS (||__c7xabi_divf||) arg:{A4,A5} ret:{A4}  ; [] |118| 
           MV      .L2X    A4,BM1            ; [B_L2] |118| 
           MV      .L2X    A10,BM0           ; [B_L2] |118| Define a twin register
           SUBSP   .C2     BM1,BM0,B14       ; [B_C] |118| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 119,column 29,is_stmt,isa 0
$C$DW$59	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$59, DW_AT_low_pc(0x00)
	.dwattr $C$DW$59, DW_AT_name("logf")
	.dwattr $C$DW$59, DW_AT_TI_call


           CALL    .B1     ||logf||          ; [A_B] |119| 
||         MV      .L1X    B14,A4            ; [A_L1] |119| 

$C$RL3:    ; CALL OCCURS (||logf||) arg:{A4} ret:{A4}  ; [] |119| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 113,column 1,is_stmt,isa 0
           MV      .D1     A8,A5             ; [A_D1] |113| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 133,column 3,is_stmt,isa 0
           LDW     .D1     *A5(1304),AL0     ; [A_D1] |133| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 146,column 15,is_stmt,isa 0
           LDW     .D1     *A9(12),A1        ; [A_D1] |146| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 130,column 19,is_stmt,isa 0
           MVK32   .L2     0xffff8000,BL3    ; [B_L2] |130| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 119,column 29,is_stmt,isa 0

           CMPEQW  .L1     AL0,0x1,A0        ; [A_L1] |133| 
||         VXORW   .S1     A4,0x80000000,A2  ; [A_S1] |119| 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 135,column 5,is_stmt,isa 0

           CMPGTW  .L1     A1,0,A0           ; [A_L1] |146| 
|| [ A0]   MVK32   .L2     0xffffff80,BL3    ; [B_L2] |135| 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 126,column 21,is_stmt,isa 0

   [!A0]   B       .B1     ||$C$L194||       ; [A_B] |146| 
||         LDW     .D1     *A5(1324),A6      ; [A_D1] |126| 
||         MV      .M2X    A2,B2             ; [B_M2] |133| 
||         MVKU32  .L2     0,B1              ; [B_L2] |146| 
||         MVKU32  .S2     0,B0              ; [B_S2] 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 146,column 15,is_stmt,isa 0
           ; BRANCHCC OCCURS {||$C$L194||}   ; [] |146| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 8
           MV      .D1     A5,D2             ; [A_D1] 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 148,column 5,is_stmt,isa 0
           SLDD    .D1     *D2(1192),D0      ; [A_D1] |148| <0,0>  ^ [C1]
           MV      .L1X    B1,A8             ; [A_L1] 

           STW     .D1X    B0,*D0[A8]        ; [A_D1] |148| <0,6>  ^ 
||         UNPROT          0x1               ; [A_U] 
||         MV      .D2     A9,D1             ; [A_D2] 

;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*
;*      Loop found in file               : src/tidl_detectionOutput_score.c
;*      Loop source line                 : 146
;*      Loop opening brace source line   : 147
;*      Loop closing brace source line   : 149
;*      Known Minimum Trip Count         : 1                    
;*      Known Max Trip Count Factor      : 1
;*      Loop Carried Dependency Bound(^) : 7
;*      Unpartitioned Resource Bound     : 2
;*      Partitioned Resource Bound       : 2 (pre-sched)
;*
;*      Searching for software pipeline schedule at ...
;*         ii = 7  Unsafe schedule for irregular loop
;*         ii = 7  Unsafe schedule for irregular loop
;*         ii = 7  Unsafe schedule for irregular loop
;*         ii = 7  Did not find schedule
;*         ii = 8  Unsafe schedule for irregular loop
;*         ii = 8  Unsafe schedule for irregular loop
;*         ii = 8  Unsafe schedule for irregular loop
;*         ii = 8  Did not find schedule
;*         ii = 9  Schedule found with 2 iterations in parallel
;*
;*      Partitioned Resource Bound(*)    : 2 (post-sched)
;*
;*      Constant Extension #0 Used [C0]  : 0
;*      Constant Extension #1 Used [C1]  : 1
;*
;*      Resource Partition (may include "post-sched" split/spill moves):
;*
;*                                                A-side   B-side
;*      .L units                                     0        0     
;*      .S units                                     0        0     
;*      .M units                                     0        0     
;*      .N units                                     0        0     
;*      .D units                                     3        -     
;*      .B units                                     1        -     
;*      .C units                                     -        0     
;*      .P units                                     -        0     
;*
;*      .M/.N units                                  0        0     
;*      .L/.S units                                  1        0     
;*      .L/.S/.C units                               0        0     
;*      .L/.S/.C/.M units                            0        0     
;*      .L/.S/.C/.M/.D units                         1        0     
;*
;*      .X cross paths                               2*       0     
;*
;*      Bound(.D1 .D2 .D)                            2*       -     
;*      Bound(.M .N .MN)                             0        0     
;*      Bound(.L .S .LS)                             1        0     
;*      Bound(.L .S .C .LS .LSC)                     1        0     
;*      Bound(.L .S .C .M .LS .LSC .LSCM)            1        0     
;*      Bound(.L .S .C .M .D .LS .LSC .LSCM .LSCMD)  1        0     
;*
;*      Register Usage Tables:
;*
;*      +----------------------------------+----------------------------------+
;*      |              ASIDE               |              BSIDE               |
;*      +----------------+--------+--------+----------------+--------+--------+
;*      |      "Axx"     |  "ALx" |  "AMx" |     "VBxx"     | "VBLx" | "VBMx" |
;*      +----------------|--------|--------|----------------|--------|--------+
;*      |0000000000111111|00000000|00000000|0000000000111111|00000000|00000000|
;*      |0123456789012345|01234567|01234567|0123456789012345|01234567|01234567|
;*      +----------------|--------|--------|----------------|--------|--------+
;*   0: |        *       |        |        |*               |        |        |
;*   1: |        *       |        |        |*               |        |        |
;*   2: |        *       |        |        |*               |        |        |
;*   3: |        *       |        |        |*               |        |        |
;*   4: |        *       |        |        |*               |        |        |
;*   5: |        *       |        |        |*               |        |        |
;*   6: |        *       |        |        |**              |        |        |
;*   7: |        *       |        |        |**              |        |        |
;*   8: |*       *       |        |        |*               |        |        |
;*      +----------------+--------+--------+----------------+--------+--------+
;*
;*      +----------------+                 +--------+       +-------+
;*      |      "Dxx"     |                 |  "Px"  |       |"CUCRx"|
;*      +----------------+                 +--------+       +-------+
;*      |0000000000111111|                 |00000000|       |0000   |
;*      |0123456789012345|                 |01234567|       |0123   |
;*      +----------------+                 +--------+       +-------+
;*   0: | **             |                 |        |       |       |
;*   1: | **             |                 |        |       |       |
;*   2: | **             |                 |        |       |       |
;*   3: | **             |                 |        |       |       |
;*   4: | **             |                 |        |       |       |
;*   5: | **             |                 |        |       |       |
;*   6: | **             |                 |        |       |       |
;*   7: | **             |                 |        |       |       |
;*   8: |***             |                 |        |       |       |
;*      +----------------+                 +--------+       +-------+
;*
;*      Done
;*
;*      Collapsed epilog stages       : 1
;*      Prolog not removed
;*      Collapsed prolog stages       : 0
;*
;*      Max amt of load speculation   : 0 bytes
;*
;*      Minimum safe trip count       : 1
;*      Min. prof. trip count  (est.) : 3
;*
;*      Mem bank conflicts/iter(est.) : { min 0.000, est 0.000, max 0.000 }
;*      Mem bank perf. penalty (est.) : 0.0%
;*
;*
;*      Total cycles (est.)         : 7 + trip_cnt * 9        
;*----------------------------------------------------------------------------*
;*       SETUP CODE
;*
;*                  MVKU32  0x1,A0  ; [] 
;*
;*        SINGLE SCHEDULED ITERATION
;*
;*        ||$C$C3444||:
;*   0              SLDD    .D1     *D2(1192),D0      ; [A_D1] |148|  ^ [C1]
;*   1              NOP     0x5     ; [A_B] 
;*   6      [ A0]   STW     .D1X    B0,*D0[A8]        ; [A_D1] |148|  ^ 
;*   7              LDW     .D1     *D1(12),B1        ; [A_D1] |146| 
;*   8              NOP     0x5     ; [A_B] 
;*  13              ADDW    .D1     A8,0x1,A8         ; [A_D1] |146| 
;*  14              CMPGTW  .L1X    B1,A8,A0          ; [A_L1] |146| 
;*  15      [ A0]   B       .B1     ||$C$C3444||      ; [A_B] |146| 
;*  16              ; BRANCHCC OCCURS {||$C$C3444||}  ; [] |146| 
;*----------------------------------------------------------------------------*
||$C$L191||:    ; PIPED LOOP PROLOG
;** --------------------------------------------------------------------------*
||$C$L192||:    ; PIPED LOOP KERNEL
;          EXCLUSIVE CPU CYCLES: 9
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 146,column 15,is_stmt,isa 0
           LDW     .D1     *D1(12),B1        ; [A_D1] |146| <0,7> 
           NOP             0x1               ; [A_B] 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 148,column 5,is_stmt,isa 0
           SLDD    .D1     *D2(1192),D0      ; [A_D1] |148| <1,0>  ^ [C1]
           NOP             0x3               ; [A_B] 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 146,column 15,is_stmt,isa 0
           ADDW    .D1     A8,0x1,A8         ; [A_D1] |146| <0,13> 
           CMPGTW  .L1X    B1,A8,A0          ; [A_L1] |146| <0,14> 

   [ A0]   B       .B1     ||$C$L192||       ; [A_B] |146| <0,15> 
|| [ A0]   STW     .D1X    B0,*D0[A8]        ; [A_D1] |148| <1,6>  ^ 

;** --------------------------------------------------------------------------*
||$C$L193||:    ; PIPED LOOP EPILOG
;          EXCLUSIVE CPU CYCLES: 1
           PROTCLR                            ; [A_U] VB1
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 1

           MV      .D1     D1,A9             ; [A_D1] 
||         MV      .D2     D2,A5             ; [A_D2] 
||         MV      .L1X    B1,A1             ; [A_L1] 

;** --------------------------------------------------------------------------*
||$C$L194||:    
;          EXCLUSIVE CPU CYCLES: 8
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 153,column 3,is_stmt,isa 0
           LDW     .D1     *A9(68),AL1       ; [A_D1] |153| 
           CMPEQW  .L1     AL1,0x1,A0        ; [A_L1] |153| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 119,column 29,is_stmt,isa 0

   [!A0]   B       .B1     ||$C$L213||       ; [A_B] |153| 
||         MVKU32  .L2     0x3f800000,B7     ; [B_L2] |119| 
||         MVKU32  .L1     0,A10             ; [A_L1] 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 153,column 3,is_stmt,isa 0
           ; BRANCHCC OCCURS {||$C$L213||}   ; [] |153| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 8
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 155,column 17,is_stmt,isa 0
           LDW     .D1     *A9(56),AL0       ; [A_D1] |155| 
           ADDD    .D1     A5,0x244,A2       ; [A_D1] 
           MVKU32  .L1     0,A8              ; [A_L1] 

           MV      .L2X    A2,B5             ; [B_L2] |155| 
||         ADDD    .D1     A5,0x248,A3       ; [A_D1] 

           CMPGTW  .L1     AL0,0,A0          ; [A_L1] |155| 
||         MV      .L2X    A8,B4             ; [B_L2] 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 155,column 10,is_stmt,isa 0

   [!A0]   B       .B1     ||$C$L213||       ; [A_B] |155| 
||         MV      .L2X    A3,B6             ; [B_L2] |155| 
||         MVKU32  .L1     0,A13             ; [A_L1] |155| 
||         MV      .D1     A11,D9            ; [A_D1] 
||         ADDD    .D2     A5,0x140,D13      ; [A_D2] 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 155,column 17,is_stmt,isa 0
           ; BRANCHCC OCCURS {||$C$L213||}   ; [] |155| 
;** --------------------------------------------------------------------------*
;**   BEGIN LOOP ||$C$L195||
;** --------------------------------------------------------------------------*
||$C$L195||:    
;          EXCLUSIVE CPU CYCLES: 9
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 157,column 7,is_stmt,isa 0

           LDUW    .D1     *D13(0),A0        ; [A_D1] |157| 
||         MV      .L1X    B5,A2             ; [A_L1] |157| 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 160,column 7,is_stmt,isa 0

           LDW     .D1     *A2(0),A2         ; [A_D1] |160| 
||         LDW     .D2     *D9(0),A3         ; [A_D2] |162| 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 157,column 7,is_stmt,isa 0

           MV      .L2X    A0,B1             ; [B_L2] |157| 
||         CMPGTW  .L1     A3,0,A4           ; [A_L1] |162| 
||         MV      .S1X    B6,D14            ; [A_S1] |157| 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 159,column 7,is_stmt,isa 0

   [!A4]   B       .B1     ||$C$L212||       ; [A_B] |162| 
||         MV      .L2X    A2,B3             ; [B_L2] |162| 
||         LDW     .D1     *D14(0),AM3       ; [A_D1] |159| 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 162,column 20,is_stmt,isa 0
           ; BRANCHCC OCCURS {||$C$L212||}   ; [] |162| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 1
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 162,column 12,is_stmt,isa 0
           MVKU32  .L1     0,A4              ; [A_L1] |162| 
;** --------------------------------------------------------------------------*
;**   BEGIN LOOP ||$C$L196||
;** --------------------------------------------------------------------------*
||$C$L196||:    
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 164,column 22,is_stmt,isa 0
           CMPGTW  .L1     A1,0,A0           ; [A_L1] |164| 
   [!A0]   B       .B1     ||$C$L211||       ; [A_B] |164| 
           ; BRANCHCC OCCURS {||$C$L211||}   ; [] |164| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 1
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 164,column 14,is_stmt,isa 0
           MVKU32  .L1     0,A10             ; [A_L1] |164| 
;** --------------------------------------------------------------------------*
;**   BEGIN LOOP ||$C$L197||
;** --------------------------------------------------------------------------*
||$C$L197||:    
;          EXCLUSIVE CPU CYCLES: 8
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 168,column 13,is_stmt,isa 0
           LDW     .D1     *A9(16),AL0       ; [A_D1] |168| 
           CMPEQW  .L1     AL0,A10,A0        ; [A_L1] |168| 
   [ A0]   B       .B1     ||$C$L210||       ; [A_B] |168| 
           ; BRANCHCC OCCURS {||$C$L210||}   ; [] |168| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 8
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 170,column 11,is_stmt,isa 0

           LDD     .D1     *A5(1192),D0      ; [A_D1] |170| 
||         LDW     .D2     *A9(72),AL0       ; [A_D2] |173| 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 171,column 11,is_stmt,isa 0
           SHLW    .L1     A13,0x4,D1        ; [A_L1] |171| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 170,column 11,is_stmt,isa 0

           ORW     .D1     A4,D1,AL1         ; [A_D1] |171| 
||         EXT     .L1     A10,0x20,0x20,AL2 ; [A_L1] |170| 
||         MV      .S1X    B4,A0             ; [A_S1] |171| 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 171,column 11,is_stmt,isa 0

           SHLW    .L1     AL1,0x8,D1        ; [A_L1] |171| 
||         SHLD    .S1     A0,0x3,AL7        ; [A_S1] 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 170,column 11,is_stmt,isa 0

           ADDD    .S1     A5,AL7,D1         ; [A_S1] 
||         ADDAW   .D1     D0,A10,D0         ; [A_D1] |170| 
||         CMPEQW  .L1     AL0,0x4,A0        ; [A_L1] |173| 
||         ORW     .D2     A10,D1,AL1        ; [A_D2] |171| 

   [!A0]   B       .B1     ||$C$L203||       ; [A_B] |173| 
||         ADDD    .D1     D1,0xc0,D1        ; [A_D1] 
||         LDW     .D2     *D0(0),B9         ; [A_D2] |170| 
||         SHLW    .L1     AL1,0x10,A14      ; [A_L1] |171| 
||         SHLD    .S1     AL2,0x2,D12       ; [A_S1] |170| 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 173,column 11,is_stmt,isa 0
           ; BRANCHCC OCCURS {||$C$L203||}   ; [] |173| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 12
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 197,column 13,is_stmt,isa 0
           ADDW    .D1     A1,0x5,AM0        ; [A_D1] |197| 
           MPYWW   .N1     A4,AM0,A2         ; [A_N1] |197| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 200,column 36,is_stmt,isa 0
           LDW     .D1     *D9(12),AL0       ; [A_D1] |200| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 197,column 13,is_stmt,isa 0

           ADDW    .D2     A10,A2,D1         ; [A_D2] |197| 
||         MV      .L1X    B3,A0             ; [A_L1] |197| 
||         LDD     .D1     *D1(0),D3         ; [A_D1] |197| 

           ADDW    .D1     D1,0x1,AM0        ; [A_D1] |197| 
||         MV      .L1X    B3,A1             ; [A_L1] |197| 

           MPYWW   .N1     A1,AM0,D2         ; [A_N1] |197| 
||         MPYWW   .M1     A0,A2,D14         ; [A_M1] |198| 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 200,column 36,is_stmt,isa 0
           CMPGTW  .L1     AL0,0,A0          ; [A_L1] |200| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 197,column 13,is_stmt,isa 0

   [!A0]   B       .B1     ||$C$L209||       ; [A_B] |200| 
||         ADDAW   .D1     D3,D2,D11         ; [A_D1] |197| 
||         ADDAW   .D2     D3,D14,D10        ; [A_D2] |198| 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 200,column 36,is_stmt,isa 0
           ; BRANCHCC OCCURS {||$C$L209||}   ; [] |200| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 12

           EXT     .L1     A13,0x20,0x20,AM0 ; [A_L1] 
||         MVKU32  .S1     0xf8,AM1          ; [A_S1] 

           MPYDD   .N1     AM1,AM0,D0        ; [A_N1] 
           ADDD    .D1     A11,D0,D0         ; [A_D1] 
           LDW     .D1     *D0(8),B0         ; [A_D1] 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 200,column 31,is_stmt,isa 0
           MVKU32  .L1     0,A7              ; [A_L1] |200| 
;** --------------------------------------------------------------------------*
;**   BEGIN LOOP ||$C$L198||
;** --------------------------------------------------------------------------*
||$C$L198||:    
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 202,column 38,is_stmt,isa 0
           CMPGTW  .L1X    B0,0,A0           ; [A_L1] |202| 
   [!A0]   B       .B1     ||$C$L202||       ; [A_B] |202| 
           ; BRANCHCC OCCURS {||$C$L202||}   ; [] |202| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 12
           MPYWW   .N1     AM3,A7,A12        ; [A_N1] 

           ADDAW   .D1     D10,A12,A15       ; [A_D1] 
||         ADDAW   .D2     D11,A12,D1        ; [A_D2] 

           MV      .D1     D1,D2             ; [A_D1] 
||         MV      .D2     A15,D3            ; [A_D2] 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 208,column 17,is_stmt,isa 0

           SLDUW   .D1     *D3++(4),AM1      ; [A_D1] |208| <0,0>  ^ 
||         SLDUW   .D2     *D2++(4),AM0      ; [A_D2] |208| <0,0>  ^ 

           MV      .L1X    B1,D14            ; [A_L1] 

           MV      .D1     D14,AM2           ; [A_D1] 
||         UNPROT          0x1               ; [A_U] 

;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*
;*      Loop found in file               : src/tidl_detectionOutput_score.c
;*      Loop source line                 : 202
;*      Loop opening brace source line   : 203
;*      Loop closing brace source line   : 218
;*      Known Minimum Trip Count         : 1                    
;*      Known Max Trip Count Factor      : 1
;*      Loop Carried Dependency Bound(^) : 21
;*      Unpartitioned Resource Bound     : 3
;*      Partitioned Resource Bound       : 5 (pre-sched)
;*
;*      Searching for software pipeline schedule at ...
;*         ii = 21 Schedule found with 2 iterations in parallel
;*
;*      Partitioned Resource Bound(*)    : 6 (post-sched)
;*
;*      Constant Extension #0 Used [C0]  : 1
;*      Constant Extension #1 Used [C1]  : 2
;*
;*      Resource Partition (may include "post-sched" split/spill moves):
;*
;*                                                A-side   B-side
;*      .L units                                     0        0     
;*      .S units                                     0        0     
;*      .M units                                     0        0     
;*      .N units                                     1        0     
;*      .D units                                     5        -     
;*      .B units                                     1        -     
;*      .C units                                     -        0     
;*      .P units                                     -        0     
;*
;*      .M/.N units                                  2        0     
;*      .L/.S units                                 10        0     
;*      .L/.S/.C units                               0        0     
;*      .L/.S/.C/.M units                            0        0     
;*      .L/.S/.C/.M/.D units                         7        3     
;*
;*      .X cross paths                               1        0     
;*
;*      Bound(.D1 .D2 .D)                            3        -     
;*      Bound(.M .N .MN)                             2        0     
;*      Bound(.L .S .LS)                             5        0     
;*      Bound(.L .S .C .LS .LSC)                     5        0     
;*      Bound(.L .S .C .M .LS .LSC .LSCM)            4        0     
;*      Bound(.L .S .C .M .D .LS .LSC .LSCM .LSCMD)  5        1     
;*
;*      Register Usage Tables:
;*
;*      +----------------------------------+----------------------------------+
;*      |              ASIDE               |              BSIDE               |
;*      +----------------+--------+--------+----------------+--------+--------+
;*      |      "Axx"     |  "ALx" |  "AMx" |     "VBxx"     | "VBLx" | "VBMx" |
;*      +----------------|--------|--------|----------------|--------|--------+
;*      |0000000000111111|00000000|00000000|0000000000111111|00000000|00000000|
;*      |0123456789012345|01234567|01234567|0123456789012345|01234567|01234567|
;*      +----------------|--------|--------|----------------|--------|--------+
;*   0: | **     *       |****  * |  *     |*               |***     |        |
;*   1: | **     *       |***** * |  *     |*               |***     |        |
;*   2: |***     *       |****  * |  *     |*               |***     |        |
;*   3: |***     *       |****  * |  *     |*               |***     |        |
;*   4: |***     *       |****  * |  *     |*               |***     |        |
;*   5: |***     *       |****  * |  *     |*               |***     |        |
;*   6: |***     *       |****  * |***     |*               |***     |        |
;*   7: |***     *       |****  * |  *     |*               |***     |        |
;*   8: |***     *       |****  * |  *     |*               |***     |        |
;*   9: |***     *       |****  * |  *     |*               |***     |        |
;*  10: |***     *       |***** * |  *     |*               |***     |        |
;*  11: |***     *       |****  * |  *     |*               |***     |        |
;*  12: |***     *       |****  * |  *     |*               |***     |        |
;*  13: |***     *       |****  * |  *     |*               |***     |        |
;*  14: |***     *       |****  * |  *     |*               |***     |        |
;*  15: |***     *       |****  * |  *     |*               |***     |        |
;*  16: |***     *       |****  * |***     |*               |***     |        |
;*  17: |***     *       |****  * |  *     |*               |***     |        |
;*  18: |***     *       |****  * |  *     |*               |***     |        |
;*  19: |***     *       |****  * |  *     |*               |***     |        |
;*  20: | **     *       |******* |  *     |*               |***     |        |
;*      +----------------+--------+--------+----------------+--------+--------+
;*
;*      +----------------+                 +--------+       +-------+
;*      |      "Dxx"     |                 |  "Px"  |       |"CUCRx"|
;*      +----------------+                 +--------+       +-------+
;*      |0000000000111111|                 |00000000|       |0000   |
;*      |0123456789012345|                 |01234567|       |0123   |
;*      +----------------+                 +--------+       +-------+
;*   0: |*********       |                 |        |       |       |
;*   1: |*******         |                 |        |       |       |
;*   2: |*******         |                 |        |       |       |
;*   3: |*******         |                 |        |       |       |
;*   4: |*******         |                 |        |       |       |
;*   5: |*******         |                 |        |       |       |
;*   6: |*******         |                 |        |       |       |
;*   7: |*******         |                 |        |       |       |
;*   8: |*******         |                 |        |       |       |
;*   9: |*******         |                 |        |       |       |
;*  10: |*******         |                 |        |       |       |
;*  11: |********        |                 |        |       |       |
;*  12: |*******         |                 |        |       |       |
;*  13: |*******         |                 |        |       |       |
;*  14: |*******         |                 |        |       |       |
;*  15: |*******         |                 |        |       |       |
;*  16: |*******         |                 |        |       |       |
;*  17: |*******         |                 |        |       |       |
;*  18: |*******         |                 |        |       |       |
;*  19: |*******         |                 |        |       |       |
;*  20: |*******         |                 |        |       |       |
;*      +----------------+                 +--------+       +-------+
;*
;*      Done
;*
;*      Collapsed epilog stages       : 1
;*      Prolog not removed
;*      Collapsed prolog stages       : 0
;*
;*      Max amt of load speculation   : 4 bytes
;*
;*      Minimum safe trip count       : 1
;*      Min. prof. trip count  (est.) : 3
;*
;*      Mem bank conflicts/iter(est.) : { min 0.000, est 0.000, max 0.000 }
;*      Mem bank perf. penalty (est.) : 0.0%
;*
;*
;*      Total cycles (est.)         : 11 + trip_cnt * 21        
;*----------------------------------------------------------------------------*
;*       SETUP CODE
;*
;*                  MVKU32  0x1,A1  ; [] 
;*
;*        SINGLE SCHEDULED ITERATION
;*
;*        ||$C$C3334||:
;*   0              SLDUW   .D1     *D2++(4),AM0      ; [A_D1] |208|  ^ 
;*     ||           SLDUW   .D2     *D3++(4),AM1      ; [A_D2] |208|  ^ 
;*   1              NOP     0x5     ; [A_B] 
;*   6              MPYSP   .M1     AM2,AM0,AL4       ; [A_M1] |208|  ^ 
;*     ||           MPYSP   .N1     AM2,AM1,AL5       ; [A_N1] |208|  ^ 
;*   7              NOP     0x3     ; [A_B] 
;*  10              CMPLESP .L1     AL3,AL4,D7        ; [A_L1] |208|  ^ 
;*     ||           CMPLESP .S1     AL3,AL5,D8        ; [A_S1] |208|  ^ 
;*  11              ANDW    .D1     D8,D7,AL4         ; [A_D1] |208|  ^ 
;*  12              CMPEQW  .L1     AL4,0,A0          ; [A_L1] |208|  ^ 
;*  13      [!A0]   CMPGEW  .S1     A8,AL2,A0         ; [A_S1] |210|  ^ 
;*     ||   [ A0]   MVKU32  .L1     0x1,A0            ; [A_L1] |210|  ^ 
;*  14      [!A0]   SLDD    .D1     *D1(1176),D0      ; [A_D1] |213|  ^ [C1]
;*  15      [!A0]   ORW     .D2     D5,D6,D4          ; [A_D2] |213| 
;*     ||   [!A1]   MVKU32  .L1     0x1,A0            ; [A_L1] 
;*  16      [!A0]   EXT     .S1     AL0,0x20,0x20,AM0 ; [A_S1] [C1]
;*     ||   [!A0]   MVKU32  .L1     0xf8,AM1          ; [A_L1] [C0]
;*  17      [!A0]   MPYDD   .N1     AM1,AM0,AL4       ; [A_N1] 
;*  18              NOP     0x2     ; [A_B] 
;*  20      [!A0]   STW     .D1     D4,*D0[A8]        ; [A_D1] |213|  ^ 
;*  21      [!A0]   ADDD    .L1     AL1,AL4,D7        ; [A_L1] 
;*  22      [!A0]   SLDW    .D2     *D7(8),B0         ; [A_D2] 
;*  23              NOP     0x5     ; [A_B] 
;*  28              ADDW    .D1     A2,0x1,A2         ; [A_D1] |202| 
;*  29      [!A0]   ADDW    .L2     BL0,0x1,BL0       ; [B_L2] |214| 
;*     ||   [!A0]   ADDW    .D1     A8,0x1,A8         ; [A_D1] |215| 
;*     ||           CMPGTW  .L1X    B0,A2,A0          ; [A_L1] |202| 
;*  30      [ A1]   MV      .S2     BL0,BL1           ; [B_S2] |202| 
;*     ||   [ A1]   MV      .D1     A8,AL6            ; [A_D1] |202| 
;*     ||   [ A1]   MV      .L2     B0,BL2            ; [B_L2] |202| 
;*     ||   [!A0]   MVKU32  .L1     0,A1              ; [A_L1] |202| 
;*  31              ADDW    .D1     D6,0x1,D6         ; [A_D1] |202| 
;*     ||   [ A1]   B       .B1     ||$C$C3334||      ; [A_B] |202| 
;*  32              ; BRANCHCC OCCURS {||$C$C3334||}  ; [] |202| 
;*
;*       RESTORE CODE
;*
;*                  MV      BL1,BL0 ; [] 
;*                  MV      AL6,A8  ; [] 
;*                  MV      BL2,B0  ; [] 
;*----------------------------------------------------------------------------*
||$C$L199||:    ; PIPED LOOP PROLOG
;          EXCLUSIVE CPU CYCLES: 5

           MPYSP   .M1     AM2,AM0,AL4       ; [A_M1] |208| <0,6>  ^ 
||         MPYSP   .N1     AM2,AM1,AL5       ; [A_N1] |208| <0,6>  ^ 

           NOP             0x1               ; [A_B] 

           MV      .D2     A14,D5            ; [A_D2] 
||         MV      .D1     A5,D1             ; [A_D1] 
||         MV      .L1X    B2,A0             ; [A_L1] 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 202,column 33,is_stmt,isa 0

           MV      .D2     A11,AL1           ; [A_D2] 
||         MVKU32  .L1     0,A2              ; [A_L1] |202| 
||         MVKU32  .S1     0x1,A1            ; [A_S1] 
||         MV      .D1     A0,AL3            ; [A_D1] 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 208,column 17,is_stmt,isa 0

           MV      .L2     B9,BL0            ; [B_L2] 
||         MV      .D1     A6,AL2            ; [A_D1] 
||         MV      .D2     A13,AL0           ; [A_D2] 
||         MV      .M1     A12,D6            ; [A_M1] 
||         CMPLESP .S1     AL3,AL5,D8        ; [A_S1] |208| <0,10>  ^ 
||         CMPLESP .L1     AL3,AL4,D7        ; [A_L1] |208| <0,10>  ^ 

;** --------------------------------------------------------------------------*
||$C$L200||:    ; PIPED LOOP KERNEL
;          EXCLUSIVE CPU CYCLES: 21
           ANDW    .D1     D8,D7,AL4         ; [A_D1] |208| <0,11>  ^ 
           CMPEQW  .L1     AL4,0,A0          ; [A_L1] |208| <0,12>  ^ 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 210,column 19,is_stmt,isa 0

   [!A0]   CMPGEW  .S1     A8,AL2,A0         ; [A_S1] |210| <0,13>  ^ 
|| [ A0]   MVKU32  .L1     0x1,A0            ; [A_L1] |210| <0,13>  ^ 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 213,column 21,is_stmt,isa 0
   [!A0]   SLDD    .D1     *D1(1176),D0      ; [A_D1] |213| <0,14>  ^ [C1]

   [!A0]   ORW     .D2     D5,D6,D4          ; [A_D2] |213| <0,15> 
|| [!A1]   MVKU32  .L1     0x1,A0            ; [A_L1] <0,15> 

   [!A0]   EXT     .S1     AL0,0x20,0x20,AM0 ; [A_S1] <0,16> [C1]
|| [!A0]   MVKU32  .L1     0xf8,AM1          ; [A_L1] <0,16> [C0]

   [!A0]   MPYDD   .N1     AM1,AM0,AL4       ; [A_N1] <0,17> 
           NOP             0x2               ; [A_B] 
   [!A0]   STW     .D1     D4,*D0[A8]        ; [A_D1] |213| <0,20>  ^ 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 208,column 17,is_stmt,isa 0

   [!A0]   ADDD    .L1     AL1,AL4,D7        ; [A_L1] <0,21> 
||         SLDUW   .D2     *D3++(4),AM1      ; [A_D2] |208| <1,0>  ^ 
||         SLDUW   .D1     *D2++(4),AM0      ; [A_D1] |208| <1,0>  ^ 

   [!A0]   SLDW    .D2     *D7(8),B0         ; [A_D2] <0,22> 
           NOP             0x4               ; [A_B] 

           MPYSP   .M1     AM2,AM0,AL4       ; [A_M1] |208| <1,6>  ^ 
||         MPYSP   .N1     AM2,AM1,AL5       ; [A_N1] |208| <1,6>  ^ 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 202,column 38,is_stmt,isa 0
           ADDW    .D1     A2,0x1,A2         ; [A_D1] |202| <0,28> 

   [!A0]   ADDW    .L2     BL0,0x1,BL0       ; [B_L2] |214| <0,29> 
|| [!A0]   ADDW    .D1     A8,0x1,A8         ; [A_D1] |215| <0,29> 
||         CMPGTW  .L1X    B0,A2,A0          ; [A_L1] |202| <0,29> 

   [ A1]   MV      .S2     BL0,BL1           ; [B_S2] |202| <0,30> 
|| [ A1]   MV      .D1     A8,AL6            ; [A_D1] |202| <0,30> 
|| [ A1]   MV      .L2     B0,BL2            ; [B_L2] |202| <0,30> 
|| [!A0]   MVKU32  .L1     0,A1              ; [A_L1] |202| <0,30> 

           ADDW    .D1     D6,0x1,D6         ; [A_D1] |202| <0,31> 
|| [ A1]   B       .B1     ||$C$L200||       ; [A_B] |202| <0,31> 
||         CMPLESP .S1     AL3,AL5,D8        ; [A_S1] |208| <1,10>  ^ 
||         CMPLESP .L1     AL3,AL4,D7        ; [A_L1] |208| <1,10>  ^ 

;** --------------------------------------------------------------------------*
||$C$L201||:    ; PIPED LOOP EPILOG
;          EXCLUSIVE CPU CYCLES: 1
           PROTCLR                            ; [A_U] VBL1
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 4
           MV      .M1     AM2,A2            ; [A_M1] 
           MV      .L1     AL3,A0            ; [A_L1] 

           MV      .L2X    A2,B1             ; [B_L2] 
||         MV      .L1     AL0,A13           ; [A_L1] 
||         MV      .S1     AL2,A6            ; [A_S1] 

           MV      .M2X    A0,B2             ; [B_M2] 
||         MV      .L1     AL6,A8            ; [A_L1] 
||         MV      .S1     AL1,A11           ; [A_S1] 
||         MV      .D1     D1,A5             ; [A_D1] 
||         MV      .D2     D5,A14            ; [A_D2] 
||         MV      .L2     BL1,B9            ; [B_L2] 
||         MV      .S2     BL2,B0            ; [B_S2] 

;** --------------------------------------------------------------------------*
||$C$L202||:    
;          EXCLUSIVE CPU CYCLES: 8
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 200,column 36,is_stmt,isa 0
           LDW     .D1     *D9(12),AL0       ; [A_D1] |200| 
           ADDW    .D1     A7,0x1,A7         ; [A_D1] |200| 
           CMPGTW  .L1     AL0,A7,A0         ; [A_L1] |200| 
   [ A0]   B       .B1     ||$C$L198||       ; [A_B] |200| 
           ; BRANCHCC OCCURS {||$C$L198||}   ; [] |200| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 7
           LDD     .D1     *A5(1192),D0      ; [A_D1] 

           B       .B1     ||$C$L209||       ; [A_B] 
||         ADDD    .D1     D12,D0,D0         ; [A_D1] 

           ; BRANCH OCCURS {||$C$L209||}     ; [] 
;** --------------------------------------------------------------------------*
||$C$L203||:    
;          EXCLUSIVE CPU CYCLES: 10
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 175,column 13,is_stmt,isa 0
           MPYWW   .N1     A4,A1,D3          ; [A_N1] |175| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 176,column 36,is_stmt,isa 0
           LDW     .D1     *D9(12),AL0       ; [A_D1] |176| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 175,column 13,is_stmt,isa 0
           LDD     .D1     *D1(0),D2         ; [A_D1] |175| 

           ADDW    .D1     A10,D3,AM0        ; [A_D1] |175| 
||         MV      .L1X    B3,A0             ; [A_L1] |175| 

           MPYWW   .N1     A0,AM0,D1         ; [A_N1] |175| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 176,column 36,is_stmt,isa 0
           CMPGTW  .L1     AL0,0,A0          ; [A_L1] |176| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 175,column 13,is_stmt,isa 0

   [!A0]   B       .B1     ||$C$L209||       ; [A_B] |176| 
||         ADDAW   .D1     D2,D1,D10         ; [A_D1] |175| 
||         MV      .L2X    A8,B8             ; [B_L2] 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 176,column 36,is_stmt,isa 0
           ; BRANCHCC OCCURS {||$C$L209||}   ; [] |176| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 12

           EXT     .L1     A13,0x20,0x20,AM0 ; [A_L1] 
||         MVKU32  .S1     0xf8,AM1          ; [A_S1] 

           MPYDD   .N1     AM1,AM0,D0        ; [A_N1] 
           ADDD    .D1     A11,D0,D0         ; [A_D1] 
           LDW     .D1     *D0(8),A1         ; [A_D1] 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 176,column 31,is_stmt,isa 0
           MVKU32  .L1     0,A7              ; [A_L1] |176| 
;** --------------------------------------------------------------------------*
;**   BEGIN LOOP ||$C$L204||
;** --------------------------------------------------------------------------*
||$C$L204||:    
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 178,column 38,is_stmt,isa 0
           CMPGTW  .L1     A1,0,A0           ; [A_L1] |178| 
   [!A0]   B       .B1     ||$C$L208||       ; [A_B] |178| 
           ; BRANCHCC OCCURS {||$C$L208||}   ; [] |178| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 16
           MPYWW   .N1     AM3,A7,D5         ; [A_N1] 
           ADDAW   .D1     D10,D5,D1         ; [A_D1] 
           MV      .D1     D1,D2             ; [A_D1] 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 182,column 17,is_stmt,isa 0
           SLDUW   .D1     *D2++(4),AM0      ; [A_D1] |182| <0,0> 
           MV      .L1X    B1,D11            ; [A_L1] 
           MV      .D1     D11,AM1           ; [A_D1] 
           MPYSP   .N1     AM1,AM0,AL0       ; [A_N1] |182| <0,6> 
           MV      .L1X    B2,D14            ; [A_L1] 

           MV      .D1     D14,AL2           ; [A_D1] 
||         UNPROT          0x1               ; [A_U] 

;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*
;*      Loop found in file               : src/tidl_detectionOutput_score.c
;*      Loop source line                 : 178
;*      Loop opening brace source line   : 179
;*      Loop closing brace source line   : 192
;*      Known Minimum Trip Count         : 1                    
;*      Known Max Trip Count Factor      : 1
;*      Loop Carried Dependency Bound(^) : 8
;*      Unpartitioned Resource Bound     : 3
;*      Partitioned Resource Bound       : 3 (pre-sched)
;*
;*      Searching for software pipeline schedule at ...
;*         ii = 8  Unsafe schedule for irregular loop
;*         ii = 8  Unsafe schedule for irregular loop
;*         ii = 8  Unsafe schedule for irregular loop
;*         ii = 8  Did not find schedule
;*         ii = 9  Unsafe schedule for irregular loop
;*         ii = 9  Unsafe schedule for irregular loop
;*         ii = 9  Unsafe schedule for irregular loop
;*         ii = 9  Did not find schedule
;*         ii = 10 Unsafe schedule for irregular loop
;*         ii = 10 Schedule found with 3 iterations in parallel
;*
;*      Partitioned Resource Bound(*)    : 4 (post-sched)
;*
;*      Constant Extension #0 Used [C0]  : 1
;*      Constant Extension #1 Used [C1]  : 2
;*
;*      Resource Partition (may include "post-sched" split/spill moves):
;*
;*                                                A-side   B-side
;*      .L units                                     0        0     
;*      .S units                                     0        0     
;*      .M units                                     0        0     
;*      .N units                                     1        0     
;*      .D units                                     4        -     
;*      .B units                                     1        -     
;*      .C units                                     -        0     
;*      .P units                                     -        0     
;*
;*      .M/.N units                                  1        0     
;*      .L/.S units                                  6        0     
;*      .L/.S/.C units                               0        0     
;*      .L/.S/.C/.M units                            0        0     
;*      .L/.S/.C/.M/.D units                         5        1     
;*
;*      .X cross paths                               1        0     
;*
;*      Bound(.D1 .D2 .D)                            2        -     
;*      Bound(.M .N .MN)                             1        0     
;*      Bound(.L .S .LS)                             3        0     
;*      Bound(.L .S .C .LS .LSC)                     3        0     
;*      Bound(.L .S .C .M .LS .LSC .LSCM)            2        0     
;*      Bound(.L .S .C .M .D .LS .LSC .LSCM .LSCMD)  3        1     
;*
;*      Register Usage Tables:
;*
;*      +----------------------------------+----------------------------------+
;*      |              ASIDE               |              BSIDE               |
;*      +----------------+--------+--------+----------------+--------+--------+
;*      |      "Axx"     |  "ALx" |  "AMx" |     "VBxx"     | "VBLx" | "VBMx" |
;*      +----------------|--------|--------|----------------|--------|--------+
;*      |0000000000111111|00000000|00000000|0000000000111111|00000000|00000000|
;*      |0123456789012345|01234567|01234567|0123456789012345|01234567|01234567|
;*      +----------------|--------|--------|----------------|--------|--------+
;*   0: | **     *       |***     | *      |*               |*       |        |
;*   1: | **     *       |***     | *      |*               |*       |        |
;*   2: | **     *       |****    | *      |*               |*       |        |
;*   3: |***     *       |***     | *      |*               |*       |        |
;*   4: | **     *       |***     | *      |*               |*       |        |
;*   5: | **     *       |***     | *      |*               |*       |        |
;*   6: | **     *       |***     |***     |*               |*       |        |
;*   7: | **     *       |***     | *      |*               |*       |        |
;*   8: | **     *       |***     |**      |*               |*       |        |
;*   9: |***     *       |***     | *      |*               |*       |        |
;*      +----------------+--------+--------+----------------+--------+--------+
;*
;*      +----------------+                 +--------+       +-------+
;*      |      "Dxx"     |                 |  "Px"  |       |"CUCRx"|
;*      +----------------+                 +--------+       +-------+
;*      |0000000000111111|                 |00000000|       |0000   |
;*      |0123456789012345|                 |01234567|       |0123   |
;*      +----------------+                 +--------+       +-------+
;*   0: |********        |                 |        |       |       |
;*   1: |*********       |                 |        |       |       |
;*   2: |********        |                 |        |       |       |
;*   3: |********        |                 |        |       |       |
;*   4: |********        |                 |        |       |       |
;*   5: |********        |                 |        |       |       |
;*   6: |********        |                 |        |       |       |
;*   7: |********        |                 |        |       |       |
;*   8: |********        |                 |        |       |       |
;*   9: |********        |                 |        |       |       |
;*      +----------------+                 +--------+       +-------+
;*
;*      Done
;*
;*      Collapsed epilog stages       : 2
;*      Prolog not removed
;*      Collapsed prolog stages       : 0
;*
;*      Max amt of load speculation   : 8 bytes
;*
;*      Minimum safe trip count       : 1
;*      Min. prof. trip count  (est.) : 3
;*
;*      Mem bank conflicts/iter(est.) : { min 0.000, est 0.000, max 0.000 }
;*      Mem bank perf. penalty (est.) : 0.0%
;*
;*
;*      Total cycles (est.)         : 18 + trip_cnt * 10        
;*----------------------------------------------------------------------------*
;*        SINGLE SCHEDULED ITERATION
;*
;*        ||$C$C3368||:
;*   0              SLDUW   .D1     *D2++(4),AM0      ; [A_D1] |182| 
;*   1              NOP     0x5     ; [A_B] 
;*   6              MPYSP   .N1     AM1,AM0,AL3       ; [A_N1] |182| 
;*   7              NOP     0x3     ; [A_B] 
;*  10              CMPLESP .L1     AL2,AL3,A0        ; [A_L1] |182| 
;*  11      [ A0]   CMPGEW  .L1     A8,AL1,A1         ; [A_L1] |184|  ^ 
;*     ||   [!A0]   MVKU32  .S1     0x1,A1            ; [A_S1] |184|  ^ 
;*  12      [!A1]   SLDD    .D1     *D1(1176),D0      ; [A_D1] |187|  ^ [C1]
;*  13      [!A1]   EXT     .S1     AL0,0x20,0x20,AM0 ; [A_S1] [C1]
;*     ||   [!A1]   MVKU32  .L1     0xf8,AM2          ; [A_L1] [C0]
;*  14      [!A1]   MPYDD   .N1     AM2,AM0,D3        ; [A_N1] 
;*  15              NOP     0x2     ; [A_B] 
;*  17      [!A1]   ORW     .D1     D5,D7,D4          ; [A_D1] |187| 
;*  18      [!A1]   STW     .D1     D4,*D0[A8]        ; [A_D1] |187|  ^ 
;*     ||   [!A1]   ADDD    .D2     D6,D3,D8          ; [A_D2] 
;*  19      [!A1]   LDW     .D1     *D8(8),B0         ; [A_D1]  ^ 
;*  20      [!A1]   ADDW    .D2     A8,0x1,A8         ; [A_D2] |189|  ^ 
;*  21      [!A1]   ADDW    .L2     BL0,0x1,BL0       ; [B_L2] |188| 
;*  22              NOP     0x3     ; [A_B] 
;*  25              ADDW    .D1     A2,0x1,A2         ; [A_D1] |178| 
;*  26              ADDW    .D1     D7,0x1,D7         ; [A_D1] |178| 
;*     ||           CMPGTW  .L1X    B0,A2,A0          ; [A_L1] |178| 
;*  27      [ A0]   B       .B1     ||$C$C3368||      ; [A_B] |178| 
;*  28              ; BRANCHCC OCCURS {||$C$C3368||}  ; [] |178| 
;*----------------------------------------------------------------------------*
||$C$L205||:    ; PIPED LOOP PROLOG
;          EXCLUSIVE CPU CYCLES: 8

           MV      .S1X    B8,A8             ; [A_S1] 
||         MV      .D2     A6,AL1            ; [A_D2] 
||         CMPLESP .L1     AL2,AL0,A0        ; [A_L1] |182| <0,10> 
||         SLDUW   .D1     *D2++(4),AM0      ; [A_D1] |182| <1,0> 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 184,column 19,is_stmt,isa 0

           MV      .L2X    A1,B0             ; [B_L2] 
||         MV      .D1     A5,D1             ; [A_D1] 
|| [!A0]   MVKU32  .S1     0x1,A1            ; [A_S1] |184| <0,11>  ^ 
|| [ A0]   CMPGEW  .L1     A8,AL1,A1         ; [A_L1] |184| <0,11>  ^ 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 187,column 21,is_stmt,isa 0

           MV      .D2     A13,AL0           ; [A_D2] 
|| [!A1]   SLDD    .D1     *D1(1176),D0      ; [A_D1] |187| <0,12>  ^ [C1]

   [!A1]   EXT     .S1     AL0,0x20,0x20,AM0 ; [A_S1] <0,13> [C1]
|| [!A1]   MVKU32  .L1     0xf8,AM2          ; [A_L1] <0,13> [C0]

   [!A1]   MPYDD   .N1     AM2,AM0,D3        ; [A_N1] <0,14> 
           NOP             0x1               ; [A_B] 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 182,column 17,is_stmt,isa 0

           MV      .D2     A14,D5            ; [A_D2] 
||         MV      .D1     D5,D7             ; [A_D1] 
||         MPYSP   .N1     AM1,AM0,AL3       ; [A_N1] |182| <1,6> 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 178,column 33,is_stmt,isa 0

           MVKU32  .L1     0,A2              ; [A_L1] |178| 
||         MV      .L2     B9,BL0            ; [B_L2] 
||         MV      .D2     A11,D6            ; [A_D2] 
|| [!A1]   ORW     .D1     D5,D7,D4          ; [A_D1] |187| <0,17> 

;** --------------------------------------------------------------------------*
||$C$L206||:    ; PIPED LOOP KERNEL
;          EXCLUSIVE CPU CYCLES: 10
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 187,column 21,is_stmt,isa 0

   [!A1]   ADDD    .D2     D6,D3,D8          ; [A_D2] <0,18> 
|| [!A1]   STW     .D1     D4,*D0[A8]        ; [A_D1] |187| <0,18>  ^ 

   [!A1]   LDW     .D1     *D8(8),B0         ; [A_D1] <0,19>  ^ 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 182,column 17,is_stmt,isa 0

   [!A1]   ADDW    .D2     A8,0x1,A8         ; [A_D2] |189| <0,20>  ^ 
||         CMPLESP .L1     AL2,AL3,A0        ; [A_L1] |182| <1,10> 
||         SLDUW   .D1     *D2++(4),AM0      ; [A_D1] |182| <2,0> 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 184,column 19,is_stmt,isa 0

   [!A1]   ADDW    .L2     BL0,0x1,BL0       ; [B_L2] |188| <0,21> 
|| [!A0]   MVKU32  .S1     0x1,A1            ; [A_S1] |184| <1,11>  ^ 
|| [ A0]   CMPGEW  .L1     A8,AL1,A1         ; [A_L1] |184| <1,11>  ^ 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 187,column 21,is_stmt,isa 0
   [!A1]   SLDD    .D1     *D1(1176),D0      ; [A_D1] |187| <1,12>  ^ [C1]

   [!A1]   EXT     .S1     AL0,0x20,0x20,AM0 ; [A_S1] <1,13> [C1]
|| [!A1]   MVKU32  .L1     0xf8,AM2          ; [A_L1] <1,13> [C0]

   [!A1]   MPYDD   .N1     AM2,AM0,D3        ; [A_N1] <1,14> 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 178,column 38,is_stmt,isa 0
           ADDW    .D1     A2,0x1,A2         ; [A_D1] |178| <0,25> 

           ADDW    .D1     D7,0x1,D7         ; [A_D1] |178| <0,26> 
||         CMPGTW  .L1X    B0,A2,A0          ; [A_L1] |178| <0,26> 
||         MPYSP   .N1     AM1,AM0,AL3       ; [A_N1] |182| <2,6> 

   [ A0]   B       .B1     ||$C$L206||       ; [A_B] |178| <0,27> 
|| [!A1]   ORW     .D1     D5,D7,D4          ; [A_D1] |187| <1,17> 

;** --------------------------------------------------------------------------*
||$C$L207||:    ; PIPED LOOP EPILOG
;          EXCLUSIVE CPU CYCLES: 1
           PROTCLR                            ; [A_U] VB0
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 4
           MV      .L1     AL2,A0            ; [A_L1] 

           MV      .L2X    A8,B8             ; [B_L2] 
||         MV      .M1     AM1,A2            ; [A_M1] 

           MV      .L2X    A0,B2             ; [B_L2] 
||         MV      .D1     D6,A11            ; [A_D1] 

           MV      .S2X    A2,B1             ; [B_S2] 
||         MV      .D1     D5,A14            ; [A_D1] 
||         MV      .L1     AL0,A13           ; [A_L1] 
||         MV      .M1X    B0,A1             ; [A_M1] 
||         MV      .D2     D1,A5             ; [A_D2] 
||         MV      .L2     BL0,B9            ; [B_L2] 
||         MV      .S1     AL1,A6            ; [A_S1] 

;** --------------------------------------------------------------------------*
||$C$L208||:    
;          EXCLUSIVE CPU CYCLES: 8
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 176,column 36,is_stmt,isa 0
           LDW     .D1     *D9(12),AL0       ; [A_D1] |176| 
           ADDW    .D1     A7,0x1,A7         ; [A_D1] |176| 
           CMPGTW  .L1     AL0,A7,A0         ; [A_L1] |176| 
   [ A0]   B       .B1     ||$C$L204||       ; [A_B] |176| 
           ; BRANCHCC OCCURS {||$C$L204||}   ; [] |176| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 7
           LDD     .D1     *A5(1192),D0      ; [A_D1] 
           ADDD    .D1     D12,D0,D0         ; [A_D1] 
;** --------------------------------------------------------------------------*
||$C$L209||:    
;          EXCLUSIVE CPU CYCLES: 6
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 222,column 11,is_stmt,isa 0
           STW     .D1X    B9,*D0(0)         ; [A_D1] |222| 
           LDW     .D1     *A9(12),A1        ; [A_D1] |222| 
;** --------------------------------------------------------------------------*
||$C$L210||:    
;          EXCLUSIVE CPU CYCLES: 3
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 164,column 22,is_stmt,isa 0
           ADDW    .D1     A10,0x1,A10       ; [A_D1] |164| 
           CMPGTW  .L1     A1,A10,A0         ; [A_L1] |164| 
   [ A0]   B       .B1     ||$C$L197||       ; [A_B] |164| 
           ; BRANCHCC OCCURS {||$C$L197||}   ; [] |164| 
;** --------------------------------------------------------------------------*
||$C$L211||:    
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 162,column 20,is_stmt,isa 0
           ADDW    .D1     A3,0xffffffff,A3  ; [A_D1] |162| 

   [ A3]   B       .B1     ||$C$L196||       ; [A_B] |162| 
||         ADDW    .D1     A4,0x1,A4         ; [A_D1] |162| 

           ; BRANCHCC OCCURS {||$C$L196||}   ; [] |162| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 2
           LDW     .D1     *A9(56),AL0       ; [A_D1] 
;** --------------------------------------------------------------------------*
||$C$L212||:    
;          EXCLUSIVE CPU CYCLES: 6
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 155,column 17,is_stmt,isa 0
           MV      .L1X    B6,A3             ; [A_L1] |155| 

           ADDD    .D1     A3,0xc,A4         ; [A_D1] |155| 
||         MV      .L1X    B5,A2             ; [A_L1] |155| 

           ADDD    .D1     A2,0xc,A3         ; [A_D1] |155| 
||         MV      .L1X    B4,A0             ; [A_L1] |155| 

           ADDD    .D1     A0,0x1,A2         ; [A_D1] |155| 
||         MV      .L2X    A4,B6             ; [B_L2] |155| 
||         ADDW    .D2     A13,0x1,A13       ; [A_D2] |155| 

           CMPGTW  .L1     AL0,A13,A0        ; [A_L1] |155| 
||         MV      .L2X    A3,B5             ; [B_L2] |155| 

   [ A0]   B       .B1     ||$C$L195||       ; [A_B] |155| 
||         MV      .L2X    A2,B4             ; [B_L2] |155| 
||         ADDD    .D1     D9,0xf8,D9        ; [A_D1] |155| 
||         ADDD    .D2     D13,0x4,D13       ; [A_D2] |155| 

           ; BRANCHCC OCCURS {||$C$L195||}   ; [] |155| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 6
           LDW     .D1     *A9(68),AL1       ; [A_D1] 
           MV      .D1     A8,A10            ; [A_D1] 
;** --------------------------------------------------------------------------*
||$C$L213||:    
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 227,column 3,is_stmt,isa 0
           CMPEQW  .L1     AL1,0x2,A0        ; [A_L1] |227| 
   [!A0]   B       .B1     ||$C$L225||       ; [A_B] |227| 
           ; BRANCHCC OCCURS {||$C$L225||}   ; [] |227| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 8
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 229,column 17,is_stmt,isa 0
           LDW     .D1     *A9(56),AL0       ; [A_D1] |229| 
           MVKU32  .L1     0,D0              ; [A_L1] 
           MV      .D1     D0,A0             ; [A_D1] 
           ADDD    .D1     A5,0x248,A1       ; [A_D1] 

           CMPGTW  .L1     AL0,0,A0          ; [A_L1] |229| 
||         MV      .L2X    A0,B0             ; [B_L2] 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 229,column 10,is_stmt,isa 0

   [!A0]   B       .B1     ||$C$L225||       ; [A_B] |229| 
||         MV      .L2X    A1,B15            ; [B_L2] |229| 
||         ADDD    .D1     A5,0x140,D6       ; [A_D1] 
||         MVKU32  .L1     0,A15             ; [A_L1] |229| 
||         MV      .D2     A11,A8            ; [A_D2] 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 229,column 17,is_stmt,isa 0
           ; BRANCHCC OCCURS {||$C$L225||}   ; [] |229| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 5
           MVKU32  .L2     0xff7fffff,BL1    ; [B_L2] 

           MVKU32  .L2     0x7f7fffff,BL0    ; [B_L2] 
||         MVKU32  .S2     0x37800000,BM0    ; [B_S2] 

           MVKU32  .L2     0x3d2aaab4,B13    ; [B_L2] 
||         MVKU32  .S2     0x3f317218,B12    ; [B_S2] 

           MVKU32  .L2     0x3e2aaaad,B11    ; [B_L2] 
||         MVKU32  .S2     0x3fb8aa3b,BM1    ; [B_S2] 

           MVKU32  .L2     0x3f000000,B10    ; [B_L2] 
||         MVKU32  .S2     0x10000,B9        ; [B_S2] 

;** --------------------------------------------------------------------------*
;**   BEGIN LOOP ||$C$L214||
;** --------------------------------------------------------------------------*
||$C$L214||:    
;          EXCLUSIVE CPU CYCLES: 7
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 236,column 7,is_stmt,isa 0
           LDW     .D1     *A9(76),A0        ; [A_D1] |236| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 234,column 7,is_stmt,isa 0
           LDW     .D1     *A8(0),B1         ; [A_D1] |234| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 233,column 7,is_stmt,isa 0
           MV      .L1X    B15,A1            ; [A_L1] |233| 

   [ A0]   B       .B1     ||$C$L215||       ; [A_B] |236| 
||         LDW     .D1     *A1(0),AM2        ; [A_D1] |235| 
||         LDUW    .D2     *D6(0),B6         ; [A_D2] |233| 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 236,column 7,is_stmt,isa 0
           ; BRANCHCC OCCURS {||$C$L215||}   ; [] |236| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 246,column 20,is_stmt,isa 0
           CMPGTW  .L1X    B1,0,A0           ; [A_L1] |246| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 239,column 9,is_stmt,isa 0

   [!A0]   B       .B1     ||$C$L224||       ; [A_B] |246| 
||         LDW     .D1     *A9(12),AM1       ; [A_D1] |239| 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 246,column 20,is_stmt,isa 0
           ; BRANCHCC OCCURS {||$C$L224||}   ; [] |246| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 1
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 238,column 9,is_stmt,isa 0

           B       .B1     ||$C$L216||       ; [A_B] 
||         MVKU32  .L2     0x1,B8            ; [B_L2] |238| 

           ; BRANCH OCCURS {||$C$L216||}     ; [] 
;** --------------------------------------------------------------------------*
||$C$L215||:    
;          EXCLUSIVE CPU CYCLES: 4
           MV      .L2     B1,B8             ; [B_L2] 
           CMPGTW  .L1X    B8,0,A0           ; [A_L1] 
   [!A0]   B       .B1     ||$C$L224||       ; [A_B] 
           ; BRANCHCC OCCURS {||$C$L224||}   ; [] 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 1
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 244,column 9,is_stmt,isa 0
           MVKU32  .L1     0x1,AM1           ; [A_L1] |244| 
;** --------------------------------------------------------------------------*
||$C$L216||:    
;          EXCLUSIVE CPU CYCLES: 12

           EXT     .L1     A15,0x20,0x20,AM0 ; [A_L1] 
||         MVKU32  .S1     0xf8,AM3          ; [A_S1] 

           MPYDD   .N1     AM3,AM0,D1        ; [A_N1] 
           ADDD    .D1     A11,D1,D1         ; [A_D1] 
           LDW     .D1     *D1(12),AL0       ; [A_D1] 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 246,column 12,is_stmt,isa 0
           MVKU32  .L1     0,A13             ; [A_L1] |246| 

           EXT     .L1     A15,0x20,0x20,AM7 ; [A_L1] 
||         MV      .M1X    B1,A1             ; [A_M1] 
||         SHLW    .S1     A15,0x4,D9        ; [A_S1] 

;** --------------------------------------------------------------------------*
;**   BEGIN LOOP ||$C$L217||
;** --------------------------------------------------------------------------*
||$C$L217||:    
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 248,column 9,is_stmt,isa 0

           CMPGTW  .L1     AL0,0,A0          ; [A_L1] |250| 
||         ORW     .D1     A13,D9,AL1        ; [A_D1] |248| 

   [!A0]   B       .B1     ||$C$L223||       ; [A_B] |250| 
||         SHLW    .L1     AL1,0x18,D4       ; [A_L1] |248| 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 250,column 32,is_stmt,isa 0
           ; BRANCHCC OCCURS {||$C$L223||}   ; [] |250| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 12
           EXT     .L1     A15,0x20,0x20,AM0 ; [A_L1] 
           MPYDD   .N1     AM3,AM0,D1        ; [A_N1] 
           ADDD    .D1     A11,D1,D1         ; [A_D1] 
           LDW     .D1     *D1(8),AL1        ; [A_D1] 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 250,column 27,is_stmt,isa 0

           MPYWW   .N1     AM1,A13,D8        ; [A_N1] 
||         MVKU32  .L1     0,A14             ; [A_L1] |250| 

;** --------------------------------------------------------------------------*
;**   BEGIN LOOP ||$C$L218||
;** --------------------------------------------------------------------------*
||$C$L218||:    
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 252,column 34,is_stmt,isa 0
           CMPGTW  .L1     AL1,0,A0          ; [A_L1] |252| 
   [!A0]   B       .B1     ||$C$L222||       ; [A_B] |252| 
           ; BRANCHCC OCCURS {||$C$L222||}   ; [] |252| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 4
           MV      .L1X    B0,A0             ; [A_L1] 
           SHLD    .L1     A0,0x3,D1         ; [A_L1] 

           ADDD    .D1     A5,D1,D1          ; [A_D1] 
||         ADDD    .D2     D0,A5,D2          ; [A_D2] 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 252,column 29,is_stmt,isa 0

           ADDD    .D1     D1,0xc0,D2        ; [A_D1] 
||         ADDD    .D2     D2,0x244,D1       ; [A_D2] 
||         MPYWW   .N1     AM2,A14,D3        ; [A_N1] 
||         MVKU32  .L1     0,A12             ; [A_L1] |252| 

;** --------------------------------------------------------------------------*
;**   BEGIN LOOP ||$C$L219||
;** --------------------------------------------------------------------------*
||$C$L219||:    
;          EXCLUSIVE CPU CYCLES: 20
           LDW     .D1     *D1(0),AM0        ; [A_D1] 
           MPYWW   .N1     A13,AM0,AM3       ; [A_N1] 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 254,column 13,is_stmt,isa 0
           LDW     .D1     *A5(1304),AL0     ; [A_D1] |254| 

           MPYWW   .N1     AM1,AM3,D7        ; [A_N1] 
||         LDD     .D1     *D2(0),D5         ; [A_D1] 

           LDW     .D1     *A9(12),B5        ; [A_D1] |254| 

           ADDW    .D1     D3,D7,D7          ; [A_D1] 
||         CMPEQW  .L1     AL0,0x6,A0        ; [A_L1] |254| 
||         XORW    .L2     BL3,0xffffffff,BL4 ; [B_L2] |262| 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 262,column 15,is_stmt,isa 0

           ADDW    .D1     A12,D7,D7         ; [A_D1] 
|| [!A0]   VINTSP  .S2     BL4,B2            ; [B_S2] |262| 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 261,column 15,is_stmt,isa 0

           ADDAW   .D1     D5,D7,D7          ; [A_D1] 
|| [!A0]   VINTSP  .S2     BL3,BL2           ; [B_S2] |261| 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 257,column 15,is_stmt,isa 0

           LDUW    .D1     *D7(0),B1         ; [A_D1] 
|| [ A0]   MV      .L2     BL0,B2            ; [B_L2] |257| 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 256,column 15,is_stmt,isa 0

           CMPGTW  .L1X    B5,0,A0           ; [A_L1] |266| 
|| [ A0]   MV      .L2     BL1,BL2           ; [B_L2] |256| 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 266,column 26,is_stmt,isa 0

   [!A0]   B       .B1     ||$C$L221||       ; [A_B] |266| 
||         MV      .L1X    B2,AM3            ; [A_L1] 
||         MV      .L2     BL2,BM2           ; [B_L2] 

           ; BRANCHCC OCCURS {||$C$L221||}   ; [] |266| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 1
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 266,column 18,is_stmt,isa 0

           LDW     .D1     *A9(16),AL0       ; [A_D1] 
||         MV      .L1X    B5,A0             ; [A_L1] 
||         MVKU32  .L2     0,B4              ; [B_L2] |266| 

;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*      Disqualified loop: Loop contains control code
;*----------------------------------------------------------------------------*
||$C$L220||:    
;          EXCLUSIVE CPU CYCLES: 25
           MPYWW   .N2     B8,B4,B3          ; [B_N2] 
           MV      .L1X    B3,D7             ; [A_L1] Define a twin register
           ADDW    .D1     D8,D7,AM3         ; [A_D1] 
           MPYWW   .N1     AM0,AM3,D7        ; [A_N1] 
           ADDW    .D1     D3,D7,D7          ; [A_D1] 
           ADDW    .D1     A12,D7,D7         ; [A_D1] 

           LDUW    .D1     *D5[D7],B1        ; [A_D1] 
||         MV      .L2     B1,BL4            ; [B_L2] 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 270,column 15,is_stmt,isa 0
           CMPEQW  .L1X    B4,AL0,A2         ; [A_L1] |270| 

   [!A2]   MV      .L2     B1,B2             ; [B_L2] 
||         MV      .L1X    B2,A7             ; [A_L1] 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 273,column 17,is_stmt,isa 0

   [!A2]   CMPLTSP .L1X    B2,A7,A3          ; [A_L1] |273| 
|| [ A2]   MVKU32  .S1     0x1,A3            ; [A_S1] |273| 

           MV      .D1     A3,A4             ; [A_D1] 
||         MV      .L2     BL2,BM2           ; [B_L2] 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 266,column 26,is_stmt,isa 0

   [!A3]   MV      .L2     BL4,B1            ; [B_L2] 
|| [!A3]   MV      .S2X    A7,B2             ; [B_S2] 
|| [ A2]   MVKU32  .L1     0,A4              ; [A_L1] 
|| [!A2]   VMAXSP  .C2     B1,BM2,BL2        ; [B_C] 
||         ADDW    .M2     B4,0x1,B4         ; [B_M2] |266| 
||         ADDW    .D1     A0,0xffffffff,A0  ; [A_D1] |266| 

   [ A0]   B       .B1     ||$C$L220||       ; [A_B] |266| 
|| [ A4]   MV      .L2     BL4,B1            ; [B_L2] 

           ; BRANCHCC OCCURS {||$C$L220||}   ; [] |266| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 1

           MV      .L1X    B2,AM3            ; [A_L1] 
||         MV      .L2     BL2,BM2           ; [B_L2] 

;** --------------------------------------------------------------------------*
||$C$L221||:    
;          EXCLUSIVE CPU CYCLES: 93
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 283,column 29,is_stmt,isa 0
           MPYSP   .N1X    B6,AM3,A0         ; [A_N1] |283| 
	.dwpsn	file "./inc/tidl_detectionOutput_int.h",line 134,column 25,is_stmt,isa 0
           MPYSP   .N2     B6,BM2,BM2        ; [B_N2] |134| 
	.dwpsn	file "./inc/tidl_detectionOutput_int.h",line 135,column 25,is_stmt,isa 0
           MPYSP   .N2     B6,B1,BM4         ; [B_N2] |135| 
           SUBSP   .C2X    A0,BM2,BM3        ; [B_C] |135| 
           SUBSP   .C2     BM4,BM2,BM7       ; [B_C] |135| 
           MPYSP   .N2     BM1,BM3,B2        ; [B_N2] |135| 
           MPYSP   .N2     BM1,BM7,B3        ; [B_N2] |135| 
           VSPTRUNC .L2    B2,B1             ; [B_L2] |135| 

           VINTSP  .L2     B1,BM2            ; [B_L2] |157| 
||         VSPTRUNC .L1X   B3,A2             ; [A_L1] |135| 

	.dwpsn	file "./inc/tidl_detectionOutput_int.h",line 157,column 3,is_stmt,isa 0

           SUBSP   .C2     B2,BM2,BM2        ; [B_C] |157| 
||         VINTSP  .L1     A2,AL0            ; [A_L1] |157| 

           MPYSP   .N2     B12,BM2,BM3       ; [B_N2] |157| 
||         SUBSP   .L1X    B3,AL0,AM0        ; [A_L1] |157| 

           MPYSP   .N1X    B12,AM0,A0        ; [A_N1] |157| 
           MPYSP   .N2     BM3,BM3,BM2       ; [B_N2] |157| 
           MPYSP   .N1     A0,A0,AM0         ; [A_N1] |157| 
           MPYSP   .N2     BM3,BM2,BM5       ; [B_N2] |157| 
           MPYSP   .N2     B10,BM2,BM4       ; [B_N2] |157| 
           ADDSP   .C2     B7,BM3,BM6        ; [B_C] |157| 

           MPYSP   .N2     BM2,BM2,BM2       ; [B_N2] |157| 
||         MPYSP   .N1     A0,AM0,AM3        ; [A_N1] |157| 

           MPYSP   .N2     B11,BM5,BM5       ; [B_N2] |157| 
||         MPYSP   .N1X    B10,AM0,AL0       ; [A_N1] |157| 

           ADDSP   .C2     BM4,BM6,BM3       ; [B_C] |157| 
||         ADDSP   .L1X    B7,A0,AL2         ; [A_L1] |157| 

           MPYSP   .N1     AM0,AM0,AM0       ; [A_N1] |157| 
||         MASKB   .P2     0x4,P1            ; [B_P] |157| 

           MPYSP   .N2     B13,BM2,BM2       ; [B_N2] |157| 
||         MPYSP   .N1X    B11,AM3,AL3       ; [A_N1] |157| 
||         MVKU32  .L2     0,BL2             ; [B_L2] |157| 

           ADDSP   .C2     BM5,BM3,BM3       ; [B_C] |157| 
||         ADDSP   .L1     AL0,AL2,AL0       ; [A_L1] |157| 
||         SUBRW   .S2     B1,0,BL2          ; [B_S2] |157| 
||         VCMPGTW .L2     B1,BL2,P0         ; [B_L2] |157| 

           SHRW    .L2     B9,BL2,BL2        ; [B_L2] |157| 
||         AND     .P2     P0,P1,P0          ; [B_P] |157| 
||         SHLW    .S2     B9,B1,BL4         ; [B_S2] |157| 

           MPYSP   .N1X    B13,AM0,AL2       ; [A_N1] |157| 
||         VSEL    .L2     P0,BL4,BL2,BL2    ; [B_L2] |157| 
||         MASKB   .P2     0x4,P1            ; [B_P] |157| 
||         MVKU32  .S2     0,BL7             ; [B_S2] |157| 

           ADDSP   .C2     BM2,BM3,BM3       ; [B_C] |157| 
||         ADDSP   .L1     AL3,AL0,AL0       ; [A_L1] |157| 
||         VCMPGTW .L2X    A2,BL7,P0         ; [B_L2] |157| 
||         VINTSP  .S2     BL2,B4            ; [B_S2] |157| 

           SUBRW   .L2X    A2,0,BL7          ; [B_L2] |157| 
||         SHLW    .L1X    B9,A2,A7          ; [A_L1] |157| 

           SHRW    .L2     B9,BL7,BL2        ; [B_L2] |157| 
||         AND     .P2     P0,P1,P0          ; [B_P] |157| 

           MPYSP   .N2     BM3,B4,BM2        ; [B_N2] |157| 
||         ADDSP   .L1     AL2,AL0,A4        ; [A_L1] |157| 
||         VSEL    .L2X    P0,A7,BL2,BL2     ; [B_L2] |157| 

           VINTSP  .L2     BL2,BM3           ; [B_L2] |157| 

           MPYSP   .N2X    A4,BM3,BM3        ; [B_N2] |157| 
||         MPYSP   .M2     BM0,BM2,BM2       ; [B_M2] |157| 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 288,column 13,is_stmt,isa 0
           ADDW    .L2     B5,0xfffffffe,BL6 ; [B_L2] |288| 
	.dwpsn	file "./inc/tidl_detectionOutput_int.h",line 161,column 3,is_stmt,isa 0

           VINTSP  .S2     BL6,BM4           ; [B_S2] |288| 
||         CMPGEW  .L1X    B1,0xfffffff0,A3  ; [A_L1] |161| 

	.dwpsn	file "./inc/tidl_detectionOutput_int.h",line 157,column 3,is_stmt,isa 0

           MPYSP   .N2     BM0,BM3,BM3       ; [B_N2] |157| 
||         CMPGTW  .L1X    B1,0xe,A4         ; [A_L1] |164| 
|| [!A3]   MVKU32  .L2     0,BM2             ; [B_L2] |162| 

	.dwpsn	file "./inc/tidl_detectionOutput_int.h",line 165,column 5,is_stmt,isa 0
   [ A4]   MV      .L2     BL0,BM2           ; [B_L2] |165| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 288,column 13,is_stmt,isa 0
           MPYSP   .N2     BM2,BM4,BM2       ; [B_N2] |288| 
	.dwpsn	file "./inc/tidl_detectionOutput_int.h",line 161,column 3,is_stmt,isa 0
           CMPGEW  .L1     A2,0xfffffff0,A3  ; [A_L1] |161| 
	.dwpsn	file "./inc/tidl_detectionOutput_int.h",line 162,column 5,is_stmt,isa 0

           CMPGTW  .L1     A2,0xe,A4         ; [A_L1] |164| 
|| [!A3]   MVKU32  .L2     0,BM3             ; [B_L2] |162| 

	.dwpsn	file "./inc/tidl_detectionOutput_int.h",line 165,column 5,is_stmt,isa 0
   [ A4]   MV      .L2     BL0,BM3           ; [B_L2] |165| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 288,column 13,is_stmt,isa 0
           ADDSP   .C2     BM2,BM3,BL2       ; [B_C] |288| 
           CMPGTW  .L1     A6,A10,A7         ; [A_L1] |288| 
           CMPLESP .L2     BL2,B14,BL2       ; [B_L2] |288| 
           ANDW    .L2X    A7,BL2,B1         ; [B_L2] |288| 
           CMPEQW  .L1X    B1,0,A0           ; [A_L1] |288| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 293,column 17,is_stmt,isa 0
   [!A0]   LDD     .D1     *A5(1184),D5      ; [A_D1] |293| 
           ADDW    .D1     A12,D3,D7         ; [A_D1] |293| 
           ORW     .D1     D4,D7,D7          ; [A_D1] |293| 
   [!A0]   STW     .D1     D7,*D5[A10]       ; [A_D1] |293| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 294,column 17,is_stmt,isa 0
   [!A0]   LDD     .D1     *A5(1192),D5      ; [A_D1] |294| 
   [!A0]   LDW     .D1     *D5(0),BL2        ; [A_D1] |294| 
           MVKU32  .L1     0xf8,AM3          ; [A_L1] 
           MPYDD   .N1     AM3,AM7,D14       ; [A_N1] 
   [!A0]   ADDW    .L2     BL2,0x1,B1        ; [B_L2] |294| 

   [!A0]   STW     .D1X    B1,*D5(0)         ; [A_D1] |294| 
||         ADDD    .D2     A11,D14,D13       ; [A_D2] 

   [!A0]   LDW     .D1     *D13(8),AL1       ; [A_D1] 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 252,column 34,is_stmt,isa 0
           ADDW    .D1     A12,0x1,A12       ; [A_D1] |252| 

           CMPGTW  .L1     AL1,A12,A0        ; [A_L1] |252| 
|| [!A0]   ADDW    .D1     A10,0x1,A10       ; [A_D1] |295| 

   [ A0]   B       .B1     ||$C$L219||       ; [A_B] |252| 
           ; BRANCHCC OCCURS {||$C$L219||}   ; [] |252| 
;** --------------------------------------------------------------------------*
||$C$L222||:    
;          EXCLUSIVE CPU CYCLES: 8
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 250,column 32,is_stmt,isa 0
           LDW     .D1     *A8(12),AL0       ; [A_D1] |250| 
           ADDW    .D1     A14,0x1,A14       ; [A_D1] |250| 
           CMPGTW  .L1     AL0,A14,A0        ; [A_L1] |250| 
   [ A0]   B       .B1     ||$C$L218||       ; [A_B] |250| 
           ; BRANCHCC OCCURS {||$C$L218||}   ; [] |250| 
;** --------------------------------------------------------------------------*
||$C$L223||:    
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 246,column 20,is_stmt,isa 0
           ADDW    .D1     A1,0xffffffff,A1  ; [A_D1] |246| 

   [ A1]   B       .B1     ||$C$L217||       ; [A_B] |246| 
||         ADDW    .D1     A13,0x1,A13       ; [A_D1] |246| 

           ; BRANCHCC OCCURS {||$C$L217||}   ; [] |246| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 3
           LDW     .D1     *A9(56),AL0       ; [A_D1] 
;** --------------------------------------------------------------------------*
||$C$L224||:    
;          EXCLUSIVE CPU CYCLES: 5
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 229,column 17,is_stmt,isa 0
           MV      .L1X    B15,A1            ; [A_L1] |229| 

           ADDD    .D1     A1,0xc,A2         ; [A_D1] |229| 
||         MV      .L1X    B0,A0             ; [A_L1] |229| 

           ADDD    .D1     A0,0x1,A1         ; [A_D1] |229| 
||         ADDW    .D2     A15,0x1,A15       ; [A_D2] |229| 

           CMPGTW  .L1     AL0,A15,A0        ; [A_L1] |229| 
||         MV      .L2X    A2,B15            ; [B_L2] |229| 

   [ A0]   B       .B1     ||$C$L214||       ; [A_B] |229| 
||         MV      .L2X    A1,B0             ; [B_L2] |229| 
||         ADDD    .L1     A8,0xf8,A8        ; [A_L1] |229| 
||         ADDD    .D1     D0,0xc,D0         ; [A_D1] |229| 
||         ADDD    .D2     D6,0x4,D6         ; [A_D2] |229| 

           ; BRANCHCC OCCURS {||$C$L214||}   ; [] |229| 
;** --------------------------------------------------------------------------*
||$C$L225||:    
;          EXCLUSIVE CPU CYCLES: 16

           LDD     .D2     *SP(144),A13      ; [A_D2] 
||         VLD64B  .D1     *SP(16),VB14      ; [A_D1] 

	.dwcfi	restore_reg, 62
           VLD64B  .D1     *SP(80),VB15      ; [A_D1] 
	.dwcfi	restore_reg, 63
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 304,column 3,is_stmt,isa 0

           MVC     .S1     A13,RP            ; [A_S1] 
||         MV      .D1     A10,A4            ; [A_D1] |304| 
	.dwcfi	restore_reg, 4101

           LDD     .D1     *SP(160),A14      ; [A_D1] 
||         LDD     .D2     *SP(152),A15      ; [A_D2] 
	.dwcfi	restore_reg, 14
	.dwcfi	restore_reg, 15

           LDD     .D1     *SP(184),A11      ; [A_D1] 
||         LDD     .D2     *SP(176),A12      ; [A_D2] 
	.dwcfi	restore_reg, 11
	.dwcfi	restore_reg, 12

           LDD     .D1     *SP(208),A8       ; [A_D1] 
||         LDD     .D2     *SP(200),A9       ; [A_D2] 
	.dwcfi	restore_reg, 8
	.dwcfi	restore_reg, 9

           LDD     .D1     *SP(168),A13      ; [A_D1] 
||         LDD     .D2     *SP(192),A10      ; [A_D2] 

	.dwcfi	restore_reg, 13
	.dwcfi	restore_reg, 10
$C$DW$60	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$60, DW_AT_low_pc(0x00)
	.dwattr $C$DW$60, DW_AT_TI_return


           RET     .B1     ; [A_B] 
||         ADDD    .D1     SP,0xc8,SP        ; [A_D1] 

	.dwcfi	cfa_offset, 0
           ; RETURN OCCURS {RP}              ; [] 
	.dwattr $C$DW$52, DW_AT_TI_end_file("src/tidl_detectionOutput_score.c")
	.dwattr $C$DW$52, DW_AT_TI_end_line(0x131)
	.dwattr $C$DW$52, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$52

	.sect	".text:_Z25TIDL_findValidLocation_cnIaEiP26sTIDL_DetectOutputParams_tP29sTIDL_ALgDetectOutputParams_tPf"
	.clink
	.global	||_Z25TIDL_findValidLocation_cnIaEiP26sTIDL_DetectOutputParams_tP29sTIDL_ALgDetectOutputParams_tPf||

$C$DW$61	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$61, DW_AT_name("int TIDL_findValidLocation_cn")
	.dwattr $C$DW$61, DW_AT_low_pc(||_Z25TIDL_findValidLocation_cnIaEiP26sTIDL_DetectOutputParams_tP29sTIDL_ALgDetectOutputParams_tPf||)
	.dwattr $C$DW$61, DW_AT_high_pc(0x00)
	.dwattr $C$DW$61, DW_AT_linkage_name("_Z25TIDL_findValidLocation_cnIaEiP26sTIDL_DetectOutputParams_tP29sTIDL_ALgDetectOutputParams_tPf")
	.dwattr $C$DW$61, DW_AT_external
	.dwattr $C$DW$61, DW_AT_decl_file("src/tidl_detectionOutput_score.c")
	.dwattr $C$DW$61, DW_AT_decl_line(0x6e)
	.dwattr $C$DW$61, DW_AT_decl_column(0x09)
	.dwattr $C$DW$61, DW_AT_TI_max_frame_size(0xf0)
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 113,column 1,is_stmt,address ||_Z25TIDL_findValidLocation_cnIaEiP26sTIDL_DetectOutputParams_tP29sTIDL_ALgDetectOutputParams_tPf||,isa 0

	.dwfde $C$DW$CIE, ||_Z25TIDL_findValidLocation_cnIaEiP26sTIDL_DetectOutputParams_tP29sTIDL_ALgDetectOutputParams_tPf||
$C$DW$62	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$62, DW_AT_name("params")
	.dwattr $C$DW$62, DW_AT_location[DW_OP_reg4]

$C$DW$63	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$63, DW_AT_name("algDetLyrParams")
	.dwattr $C$DW$63, DW_AT_location[DW_OP_reg5]

$C$DW$64	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$64, DW_AT_name("priorData")
	.dwattr $C$DW$64, DW_AT_location[DW_OP_reg6]


;******************************************************************************
;* FUNCTION NAME: int TIDL_findValidLocation_cn<signed char>(sTIDL_DetectOutputParams_t *, sTIDL_ALgDetectOutputParams_t *, float *)*
;*                                                                            *
;*   Regs Modified     : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,A14,   *
;*                           A15,VB0,VB1,VB2,VB3,VB4,VB5,VB6,VB7,VB8,VB9,VB10,*
;*                           VB11,VB12,VB13,VB14,VB15,AL0,AL1,AL2,AL3,AL4,AL5,*
;*                           AL6,AL7,AM0,AM1,AM2,AM3,AM4,AM5,AM6,AM7,D0,D1,D2,*
;*                           D3,D4,D5,D6,D7,D8,D9,D10,D11,D12,D13,D14,SP,VBL0,*
;*                           VBL1,VBL2,VBL3,VBL4,VBL5,VBL6,VBL7,VBM0,VBM1,    *
;*                           VBM2,VBM3,VBM4,VBM5,VBM6,VBM7,P0,P1,P2,P3,P4,P5, *
;*                           P6,P7,CUCR0,CUCR1,CUCR2,CUCR3                    *
;*   Regs Used         : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,A14,   *
;*                           A15,VB0,VB1,VB2,VB3,VB4,VB5,VB6,VB7,VB8,VB9,VB10,*
;*                           VB11,VB12,VB13,VB14,VB15,AL0,AL1,AL2,AL3,AL4,AL5,*
;*                           AL6,AL7,AM0,AM1,AM2,AM3,AM4,AM5,AM6,AM7,D0,D1,D2,*
;*                           D3,D4,D5,D6,D7,D8,D9,D10,D11,D12,D13,D14,SP,VBL0,*
;*                           VBL1,VBL2,VBL3,VBL4,VBL5,VBL6,VBL7,VBM0,VBM1,    *
;*                           VBM2,VBM3,VBM4,VBM5,VBM6,VBM7,P0,P1,P2,P3,P4,P5, *
;*                           P6,P7,CUCR0,CUCR1,CUCR2,CUCR3                    *
;*   Local Frame Size  : 0 Args + 40 Auto + 200 Save = 240 byte               *
;******************************************************************************
||_Z25TIDL_findValidLocation_cnIaEiP26sTIDL_DetectOutputParams_tP29sTIDL_ALgDetectOutputParams_tPf||:
;** --------------------------------------------------------------------------*
;* D0    assigned to $O$C16
;* VBM3  assigned to $O$C17
;* VBM0  assigned to $O$C18
;* VB2   assigned to $O$C19
;* A0    assigned to $O$C20
;* AM0   assigned to $O$C21
;* VB10  assigned to $O$C22
;* D3    assigned to $O$C23
;* A0    assigned to $O$C24
;* A3    assigned to $O$C25
;* A10   assigned to $O$C27
;* A6    assigned to $O$U120
;* VB0   assigned to $O$U120
;* AL0   assigned to $O$v3
;* AL0   assigned to $O$v3
;* VB3   assigned to $O$v2
;* A1    assigned to $O$v2
;* VB0   assigned to $O$K28
;* D14   assigned to $O$U53
;* VB3   assigned to $O$U60
;* VB2   assigned to $O$U65
;* D10   assigned to $O$U70
;* D1    assigned to $O$U102
;* A14   assigned to $O$U131
;* A0    assigned to $O$U126
;* A10   assigned to $O$U125
;* A0    assigned to $O$U164
;* VBL2  assigned to $O$U163
;* D0    assigned to $O$U85
;* D7    assigned to $O$U201
;* D10   assigned to $O$U223
;* AL0   assigned to $O$U228
;* A8    assigned to $O$U232
;* VBL6  assigned to $O$K243
;* VBL5  assigned to $O$K245
;* D0    assigned to $O$U261
;* A6    assigned to $O$U259
;* A15   assigned to $O$U268
;* A13   assigned to $O$U251
;* A12   assigned to $O$U249
;* VBM2  assigned to $O$K292
;* VBM1  assigned to $O$K319
;* VB9   assigned to $O$K315
;* VB8   assigned to $O$K303
;* VB7   assigned to $O$K311
;* VB6   assigned to $O$K307
;* VB4   assigned to $O$K3
;* VBL4  assigned to $O$K297
;* A15   assigned to $O$U51
;* D13   assigned to $O$U84
;* D8    assigned to $O$U209
;* D6    assigned to $O$U258
;* D5    assigned to $O$U250
;* A3    assigned to $O$L1
;* A2    assigned to $O$L2
;* A0    assigned to $O$L3
;* A14   assigned to $O$v5
;* AL1   assigned to $O$v4
;* VB14  assigned to $O$Lr569$upperBoundThSoftMax
;* A12   assigned to $O$Lr578$upperBoundThSigmoid
;* VB12  assigned to $O$Lr580$topMLocal
;* VB15  assigned to $O$Lr581$anchorBox
;* VBL3  assigned to $O$Lr3$maxInit
;* VB1   assigned to $O$Lr556$i
;* A8    assigned to $O$Lr409$totalCnt
;* A9    assigned to $O$Lr56$totalCnt
;* A7    assigned to $O$Lr291$i
;* A11   assigned to $O$Lr509$onebyqFact
;* AM3   assigned to $O$Lr526$linePitch
;* A13   assigned to $O$Lr537$chPitch
;* A4    assigned to $O$Lr296$i3
;* A9    assigned to $O$Lr299$i4
;* VB5   assigned to $O$Lr407$curClassValidCnt
;* VB1   assigned to $O$Lr490$curPartIndex
;* D12   assigned to $O$Lr452$curPlaneConf
;* D11   assigned to $O$Lr468$curPlaneObjConf
;* A6    assigned to $O$Lr380$cury
;* VB5   assigned to $O$Lr5$curClassValidCnt
;* A2    assigned to $O$Lr389$curx
;* D5    assigned to $O$Lr370$curPlaneConf
;* A2    assigned to $O$Lr310$cury
;* VB5   assigned to $O$Lr337$curClassValidCnt
;* VB0   assigned to $O$Lr339$totalCnt
;* A1    assigned to $O$Lr319$curx
;* A9    assigned to $O$Lr4$totalCnt
;* VB13  assigned to $O$Lr14$i
;* VB5   assigned to $O$Lr262$onebyqFact
;* VB0   assigned to $O$Lr268$numPriors
;* A4    assigned to $O$Lr6$classStride
;* VB11  assigned to $O$Lr7$anchorStride
;* A7    assigned to $O$Lr19$i3
;* D9    assigned to $O$Lr243$curPartIndex
;* A10   assigned to $O$Lr22$cury
;* A11   assigned to $O$Lr31$curx
;* VB2   assigned to $O$Lr154$maxConfObj
;* VB1   assigned to $O$Lr158$minConfObj
;* A1    assigned to $O$Lr147$i4
;* VB2   assigned to $O$Lr8$maxConfObj
;* VB1   assigned to $O$Lr9$minConfObj
;* VB0   assigned to $O$Lr217$bckVal
;* AL0   assigned to $O$Lr10$bckVal
;* VBL2  assigned to $O$Lr586$bckVal
;* VBM3  assigned to $O$Lr110$min_obj_f
;* VBM0  assigned to $O$Lr108$max_obj_f
;* VB1   assigned to $O$Lr114$yI
;* VBM3  assigned to $O$Lr11$ePwX
;* VB0   assigned to $O$Lr71$yI
;* VBM0  assigned to $O$Lr12$ePwX
;* A5    assigned to algDetLyrParams
$C$DW$65	.dwtag  DW_TAG_variable
	.dwattr $C$DW$65, DW_AT_name("algDetLyrParams")
	.dwattr $C$DW$65, DW_AT_type(*$C$DW$T$146)
	.dwattr $C$DW$65, DW_AT_location[DW_OP_reg5]

$C$DW$66	.dwtag  DW_TAG_variable
	.dwattr $C$DW$66, DW_AT_name("params")
	.dwattr $C$DW$66, DW_AT_type(*$C$DW$T$144)
	.dwattr $C$DW$66, DW_AT_location[DW_OP_bregx 0x6f 48]

	.dwcfi	cfa_offset, 0
;          EXCLUSIVE CPU CYCLES: 24

           MV      .L1     A4,A8             ; [A_L1] |113| 
||         STD     .D1     A8,*SP(8)         ; [A_D1] 
||         STD     .D2X    A9,*SP++(-240)    ; [A_D2] 
	.dwcfi	save_reg_to_mem, 8, 8
	.dwcfi	cfa_offset, 240
	.dwcfi	save_reg_to_mem, 9, -240

           LDUW    .D1     *A8(24),A5        ; [A_D1] |118| 
||         STD     .D2X    A13,*SP(208)      ; [A_D2] 
||         MV      .L1     A5,A9             ; [A_L1] |113| 
	.dwcfi	save_reg_to_mem, 13, 208

           VST64B  .D2     VB15,*SP(120)     ; [A_D2] 
||         STD     .D1     A10,*SP(232)      ; [A_D1] 
	.dwcfi	save_reg_to_mem, 63, 120
	.dwcfi	save_reg_to_mem, 10, 232

           STD     .D1     A15,*SP(192)      ; [A_D1] 
||         STD     .D2X    A14,*SP(200)      ; [A_D2] 
	.dwcfi	save_reg_to_mem, 15, 192
	.dwcfi	save_reg_to_mem, 14, 200

           VST64B  .D2     VB14,*SP(56)      ; [A_D2] 
||         MVC     .S1     RP,A13            ; [A_S1] 
||         STD     .D1     A12,*SP(216)      ; [A_D1] 
	.dwcfi	save_reg_to_mem, 62, 56
	.dwcfi	save_reg_to_mem, 12, 216

           MVKU32  .L1     0x3f800000,A10    ; [A_L1] |118| 
||         STD     .D1     A11,*SP(224)      ; [A_D1] 
||         STD     .D2X    A4,*SP(48)        ; [A_D2] |113| 

	.dwcfi	save_reg_to_mem, 11, 224
$C$DW$67	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$67, DW_AT_low_pc(0x00)
	.dwattr $C$DW$67, DW_AT_name("__c7xabi_divf")
	.dwattr $C$DW$67, DW_AT_TI_call


           CALL    .B1     ||__c7xabi_divf|| ; [A_B] |118| 
||         STD     .D1     A13,*SP(184)      ; [A_D1] 
||         MV      .D2     A10,A4            ; [A_D2] |118| 
||         MV      .L2X    A6,B15            ; [B_L2] |113| 

	.dwcfi	save_reg_to_mem, 4101, 184
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 118,column 29,is_stmt,isa 0
$C$RL4:    ; CALL OCCURS (||__c7xabi_divf||) arg:{A4,A5} ret:{A4}  ; [] |118| 
           MV      .L2X    A4,BM1            ; [B_L2] |118| 
           MV      .L2X    A10,BM0           ; [B_L2] |118| Define a twin register
           SUBSP   .C2     BM1,BM0,B14       ; [B_C] |118| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 119,column 29,is_stmt,isa 0
$C$DW$68	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$68, DW_AT_low_pc(0x00)
	.dwattr $C$DW$68, DW_AT_name("logf")
	.dwattr $C$DW$68, DW_AT_TI_call


           CALL    .B1     ||logf||          ; [A_B] |119| 
||         MV      .L1X    B14,A4            ; [A_L1] |119| 

$C$RL5:    ; CALL OCCURS (||logf||) arg:{A4} ret:{A4}  ; [] |119| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 113,column 1,is_stmt,isa 0
           MV      .D1     A9,A5             ; [A_D1] |113| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 119,column 29,is_stmt,isa 0

           LDW     .D1     *A5(1304),AL1     ; [A_D1] |133| 
||         MV      .D2     A8,A4             ; [A_D2] |133| 
||         MV      .L1     A4,AL0            ; [A_L1] |119| 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 126,column 21,is_stmt,isa 0

           LDW     .D1     *A4(12),A1        ; [A_D1] |146| 
||         LDW     .D2     *A5(1324),A2      ; [A_D2] |126| 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 130,column 19,is_stmt,isa 0

           CMPEQW  .L1     AL1,0x1,A0        ; [A_L1] |133| 
||         MVK32   .L2     0xffff8000,BL3    ; [B_L2] |130| 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 135,column 5,is_stmt,isa 0

           CMPGTW  .L1     A1,0,A0           ; [A_L1] |146| 
|| [ A0]   MVK32   .L2     0xffffff80,BL3    ; [B_L2] |135| 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 119,column 29,is_stmt,isa 0

   [!A0]   B       .B1     ||$C$L229||       ; [A_B] |146| 
||         MV      .M2X    A2,B12            ; [B_M2] |135| 
||         VXORW   .L1     AL0,0x80000000,A12 ; [A_L1] |119| 
||         MVKU32  .L2     0,B1              ; [B_L2] |146| 
||         MVKU32  .S2     0,B0              ; [B_S2] 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 146,column 15,is_stmt,isa 0
           ; BRANCHCC OCCURS {||$C$L229||}   ; [] |146| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 8
           MV      .D1     A5,D2             ; [A_D1] 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 148,column 5,is_stmt,isa 0
           SLDD    .D1     *D2(1192),D0      ; [A_D1] |148| <0,0>  ^ [C1]

           MV      .D1     A4,A1             ; [A_D1] 
||         MV      .L1X    B1,A8             ; [A_L1] 

           STW     .D1X    B0,*D0[A8]        ; [A_D1] |148| <0,6>  ^ 
||         MV      .D2     A1,D1             ; [A_D2] 
||         UNPROT          0x1               ; [A_U] 

;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*
;*      Loop found in file               : src/tidl_detectionOutput_score.c
;*      Loop source line                 : 146
;*      Loop opening brace source line   : 147
;*      Loop closing brace source line   : 149
;*      Known Minimum Trip Count         : 1                    
;*      Known Max Trip Count Factor      : 1
;*      Loop Carried Dependency Bound(^) : 7
;*      Unpartitioned Resource Bound     : 2
;*      Partitioned Resource Bound       : 2 (pre-sched)
;*
;*      Searching for software pipeline schedule at ...
;*         ii = 7  Unsafe schedule for irregular loop
;*         ii = 7  Unsafe schedule for irregular loop
;*         ii = 7  Unsafe schedule for irregular loop
;*         ii = 7  Did not find schedule
;*         ii = 8  Unsafe schedule for irregular loop
;*         ii = 8  Unsafe schedule for irregular loop
;*         ii = 8  Unsafe schedule for irregular loop
;*         ii = 8  Did not find schedule
;*         ii = 9  Schedule found with 2 iterations in parallel
;*
;*      Partitioned Resource Bound(*)    : 2 (post-sched)
;*
;*      Constant Extension #0 Used [C0]  : 0
;*      Constant Extension #1 Used [C1]  : 1
;*
;*      Resource Partition (may include "post-sched" split/spill moves):
;*
;*                                                A-side   B-side
;*      .L units                                     0        0     
;*      .S units                                     0        0     
;*      .M units                                     0        0     
;*      .N units                                     0        0     
;*      .D units                                     3        -     
;*      .B units                                     1        -     
;*      .C units                                     -        0     
;*      .P units                                     -        0     
;*
;*      .M/.N units                                  0        0     
;*      .L/.S units                                  1        0     
;*      .L/.S/.C units                               0        0     
;*      .L/.S/.C/.M units                            0        0     
;*      .L/.S/.C/.M/.D units                         1        0     
;*
;*      .X cross paths                               2*       0     
;*
;*      Bound(.D1 .D2 .D)                            2*       -     
;*      Bound(.M .N .MN)                             0        0     
;*      Bound(.L .S .LS)                             1        0     
;*      Bound(.L .S .C .LS .LSC)                     1        0     
;*      Bound(.L .S .C .M .LS .LSC .LSCM)            1        0     
;*      Bound(.L .S .C .M .D .LS .LSC .LSCM .LSCMD)  1        0     
;*
;*      Register Usage Tables:
;*
;*      +----------------------------------+----------------------------------+
;*      |              ASIDE               |              BSIDE               |
;*      +----------------+--------+--------+----------------+--------+--------+
;*      |      "Axx"     |  "ALx" |  "AMx" |     "VBxx"     | "VBLx" | "VBMx" |
;*      +----------------|--------|--------|----------------|--------|--------+
;*      |0000000000111111|00000000|00000000|0000000000111111|00000000|00000000|
;*      |0123456789012345|01234567|01234567|0123456789012345|01234567|01234567|
;*      +----------------|--------|--------|----------------|--------|--------+
;*   0: |        *       |        |        |*               |        |        |
;*   1: |        *       |        |        |*               |        |        |
;*   2: |        *       |        |        |*               |        |        |
;*   3: |        *       |        |        |*               |        |        |
;*   4: |        *       |        |        |*               |        |        |
;*   5: |        *       |        |        |*               |        |        |
;*   6: |        *       |        |        |**              |        |        |
;*   7: |        *       |        |        |**              |        |        |
;*   8: |*       *       |        |        |*               |        |        |
;*      +----------------+--------+--------+----------------+--------+--------+
;*
;*      +----------------+                 +--------+       +-------+
;*      |      "Dxx"     |                 |  "Px"  |       |"CUCRx"|
;*      +----------------+                 +--------+       +-------+
;*      |0000000000111111|                 |00000000|       |0000   |
;*      |0123456789012345|                 |01234567|       |0123   |
;*      +----------------+                 +--------+       +-------+
;*   0: | **             |                 |        |       |       |
;*   1: | **             |                 |        |       |       |
;*   2: | **             |                 |        |       |       |
;*   3: | **             |                 |        |       |       |
;*   4: | **             |                 |        |       |       |
;*   5: | **             |                 |        |       |       |
;*   6: | **             |                 |        |       |       |
;*   7: | **             |                 |        |       |       |
;*   8: |***             |                 |        |       |       |
;*      +----------------+                 +--------+       +-------+
;*
;*      Done
;*
;*      Collapsed epilog stages       : 1
;*      Prolog not removed
;*      Collapsed prolog stages       : 0
;*
;*      Max amt of load speculation   : 0 bytes
;*
;*      Minimum safe trip count       : 1
;*      Min. prof. trip count  (est.) : 3
;*
;*      Mem bank conflicts/iter(est.) : { min 0.000, est 0.000, max 0.000 }
;*      Mem bank perf. penalty (est.) : 0.0%
;*
;*
;*      Total cycles (est.)         : 7 + trip_cnt * 9        
;*----------------------------------------------------------------------------*
;*       SETUP CODE
;*
;*                  MVKU32  0x1,A0  ; [] 
;*
;*        SINGLE SCHEDULED ITERATION
;*
;*        ||$C$C3860||:
;*   0              SLDD    .D1     *D2(1192),D0      ; [A_D1] |148|  ^ [C1]
;*   1              NOP     0x5     ; [A_B] 
;*   6      [ A0]   STW     .D1X    B0,*D0[A8]        ; [A_D1] |148|  ^ 
;*   7              LDW     .D1     *D1(12),B1        ; [A_D1] |146| 
;*   8              NOP     0x5     ; [A_B] 
;*  13              ADDW    .D1     A8,0x1,A8         ; [A_D1] |146| 
;*  14              CMPGTW  .L1X    B1,A8,A0          ; [A_L1] |146| 
;*  15      [ A0]   B       .B1     ||$C$C3860||      ; [A_B] |146| 
;*  16              ; BRANCHCC OCCURS {||$C$C3860||}  ; [] |146| 
;*----------------------------------------------------------------------------*
||$C$L226||:    ; PIPED LOOP PROLOG
;** --------------------------------------------------------------------------*
||$C$L227||:    ; PIPED LOOP KERNEL
;          EXCLUSIVE CPU CYCLES: 9
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 146,column 15,is_stmt,isa 0
           LDW     .D1     *D1(12),B1        ; [A_D1] |146| <0,7> 
           NOP             0x1               ; [A_B] 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 148,column 5,is_stmt,isa 0
           SLDD    .D1     *D2(1192),D0      ; [A_D1] |148| <1,0>  ^ [C1]
           NOP             0x3               ; [A_B] 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 146,column 15,is_stmt,isa 0
           ADDW    .D1     A8,0x1,A8         ; [A_D1] |146| <0,13> 
           CMPGTW  .L1X    B1,A8,A0          ; [A_L1] |146| <0,14> 

   [ A0]   B       .B1     ||$C$L227||       ; [A_B] |146| <0,15> 
|| [ A0]   STW     .D1X    B0,*D0[A8]        ; [A_D1] |148| <1,6>  ^ 

;** --------------------------------------------------------------------------*
||$C$L228||:    ; PIPED LOOP EPILOG
;          EXCLUSIVE CPU CYCLES: 1
           PROTCLR                            ; [A_U] VB1
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 2
           MV      .D1     D1,D0             ; [A_D1] 

           STD     .D1     D0,*SP(48)        ; [A_D1] 
||         MV      .D2     D2,A5             ; [A_D2] 
||         MV      .L1X    B1,A1             ; [A_L1] 

;** --------------------------------------------------------------------------*
||$C$L229||:    
;          EXCLUSIVE CPU CYCLES: 14
           LDD     .D1     *SP(48),A0        ; [A_D1] 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 153,column 3,is_stmt,isa 0
           LDW     .D1     *A0(68),AL1       ; [A_D1] |153| 
           CMPEQW  .L1     AL1,0x1,A0        ; [A_L1] |153| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 119,column 29,is_stmt,isa 0

   [!A0]   B       .B1     ||$C$L248||       ; [A_B] |153| 
||         MVKU32  .L2     0x3f800000,B4     ; [B_L2] |119| 
||         MVKU32  .L1     0,A9              ; [A_L1] 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 153,column 3,is_stmt,isa 0
           ; BRANCHCC OCCURS {||$C$L248||}   ; [] |153| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 14
           LDD     .D1     *SP(48),A0        ; [A_D1] 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 155,column 17,is_stmt,isa 0
           LDW     .D1     *A0(56),AL0       ; [A_D1] |155| 
           ADDD    .D1     A5,0x248,A3       ; [A_D1] 
           ADDD    .D1     A5,0x244,A2       ; [A_D1] 

           CMPGTW  .L1     AL0,0,A0          ; [A_L1] |155| 
||         MV      .L2X    A3,B3             ; [B_L2] |155| 
||         MV      .M1X    B15,D14           ; [A_M1] 
||         MVKU32  .S1     0,A15             ; [A_S1] 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 155,column 10,is_stmt,isa 0

   [!A0]   B       .B1     ||$C$L248||       ; [A_B] |155| 
||         ADDD    .D2     A5,0x140,D14      ; [A_D2] 
||         MV      .L2X    A2,B2             ; [B_L2] |155| 
||         MV      .S1     A15,A8            ; [A_S1] 
||         MV      .D1     D14,D10           ; [A_D1] 
||         MVKU32  .L1     0,A7              ; [A_L1] |155| 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 155,column 17,is_stmt,isa 0
           ; BRANCHCC OCCURS {||$C$L248||}   ; [] |155| 
;** --------------------------------------------------------------------------*
;**   BEGIN LOOP ||$C$L230||
;** --------------------------------------------------------------------------*
||$C$L230||:    
;          EXCLUSIVE CPU CYCLES: 8
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 162,column 20,is_stmt,isa 0
           LDW     .D1     *D10(0),A3        ; [A_D1] |162| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 157,column 7,is_stmt,isa 0
           MV      .L1X    B3,A0             ; [A_L1] |157| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 159,column 7,is_stmt,isa 0

           CMPGTW  .L1     A3,0,A0           ; [A_L1] |162| 
||         LDW     .D1     *A0(0),AM3        ; [A_D1] |159| 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 157,column 7,is_stmt,isa 0

   [!A0]   B       .B1     ||$C$L247||       ; [A_B] |162| 
||         LDW     .D1     *A2(0),A13        ; [A_D1] |160| 
||         LDUW    .D2     *D14(0),A11       ; [A_D2] |157| 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 162,column 20,is_stmt,isa 0
           ; BRANCHCC OCCURS {||$C$L247||}   ; [] |162| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 1
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 162,column 12,is_stmt,isa 0
           MVKU32  .L1     0,A4              ; [A_L1] |162| 
;** --------------------------------------------------------------------------*
;**   BEGIN LOOP ||$C$L231||
;** --------------------------------------------------------------------------*
||$C$L231||:    
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 164,column 22,is_stmt,isa 0
           CMPGTW  .L1     A1,0,A0           ; [A_L1] |164| 
   [!A0]   B       .B1     ||$C$L246||       ; [A_B] |164| 
           ; BRANCHCC OCCURS {||$C$L246||}   ; [] |164| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 1
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 164,column 14,is_stmt,isa 0
           MVKU32  .L1     0,A9              ; [A_L1] |164| 
;** --------------------------------------------------------------------------*
;**   BEGIN LOOP ||$C$L232||
;** --------------------------------------------------------------------------*
||$C$L232||:    
;          EXCLUSIVE CPU CYCLES: 14
           LDD     .D1     *SP(48),A0        ; [A_D1] 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 168,column 13,is_stmt,isa 0
           LDW     .D1     *A0(16),AL0       ; [A_D1] |168| 
           CMPEQW  .L1     AL0,A9,A0         ; [A_L1] |168| 
   [ A0]   B       .B1     ||$C$L245||       ; [A_B] |168| 
           ; BRANCHCC OCCURS {||$C$L245||}   ; [] |168| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 14
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 170,column 11,is_stmt,isa 0
           LDD     .D1     *SP(48),A0        ; [A_D1] |170| 

           LDW     .D1     *A0(72),AL0       ; [A_D1] |173| 
||         LDD     .D2     *A5(1192),D0      ; [A_D2] |170| 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 171,column 11,is_stmt,isa 0
           SHLW    .L1     A7,0x4,D1         ; [A_L1] |171| 
           ORW     .D1     A4,D1,AL1         ; [A_D1] |171| 
           SHLW    .L1     AL1,0x8,D1        ; [A_L1] |171| 

           ORW     .D1     A9,D1,A6          ; [A_D1] |171| 
||         SHLD    .L1     A15,0x3,D13       ; [A_L1] 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 170,column 11,is_stmt,isa 0

           CMPEQW  .L1     AL0,0x4,A0        ; [A_L1] |173| 
||         ADDAW   .D1     D0,A9,D0          ; [A_D1] |170| 
||         ADDD    .D2     A5,D13,D1         ; [A_D2] 
||         EXT     .S1     A9,0x20,0x20,AL7  ; [A_S1] |170| 

   [!A0]   B       .B1     ||$C$L238||       ; [A_B] |173| 
||         SHLD    .L1     AL7,0x2,D13       ; [A_L1] |170| 
||         ADDD    .D1     D1,0xc0,D1        ; [A_D1] 
||         LDW     .D2     *D0(0),B5         ; [A_D2] |170| 
||         SHLW    .L2X    A6,0x10,B1        ; [B_L2] |171| 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 173,column 11,is_stmt,isa 0
           ; BRANCHCC OCCURS {||$C$L238||}   ; [] |173| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 12
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 197,column 13,is_stmt,isa 0
           ADDW    .D1     A1,0x5,AM0        ; [A_D1] |197| 
           MPYWW   .N1     A4,AM0,A0         ; [A_N1] |197| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 200,column 36,is_stmt,isa 0
           LDW     .D1     *D10(12),AL0      ; [A_D1] |200| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 197,column 13,is_stmt,isa 0

           ADDW    .D2     A9,A0,D1          ; [A_D2] |197| 
||         LDD     .D1     *D1(0),D3         ; [A_D1] |197| 

           ADDW    .D1     D1,0x1,AM0        ; [A_D1] |197| 

           MPYWW   .N1     A13,AM0,D2        ; [A_N1] |197| 
||         MPYWW   .M1     A13,A0,D9         ; [A_M1] |198| 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 200,column 36,is_stmt,isa 0
           CMPGTW  .L1     AL0,0,A0          ; [A_L1] |200| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 197,column 13,is_stmt,isa 0

   [!A0]   B       .B1     ||$C$L244||       ; [A_B] |200| 
||         ADDAB   .D1     D3,D2,D12         ; [A_D1] |197| 
||         ADDAB   .D2     D3,D9,D11         ; [A_D2] |198| 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 200,column 36,is_stmt,isa 0
           ; BRANCHCC OCCURS {||$C$L244||}   ; [] |200| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 12

           EXT     .L1     A7,0x20,0x20,AM0  ; [A_L1] 
||         MVKU32  .S1     0xf8,AM1          ; [A_S1] 

           MPYDD   .N1     AM1,AM0,D0        ; [A_N1] 
           MV      .L1X    B15,A0            ; [A_L1] 
           ADDD    .D1     A0,D0,D0          ; [A_D1] 
           LDW     .D1     *D0(8),B0         ; [A_D1] 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 200,column 31,is_stmt,isa 0
           MVKU32  .L1     0,A6              ; [A_L1] |200| 
;** --------------------------------------------------------------------------*
;**   BEGIN LOOP ||$C$L233||
;** --------------------------------------------------------------------------*
||$C$L233||:    
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 202,column 38,is_stmt,isa 0
           CMPGTW  .L1X    B0,0,A0           ; [A_L1] |202| 
   [!A0]   B       .B1     ||$C$L237||       ; [A_B] |202| 
           ; BRANCHCC OCCURS {||$C$L237||}   ; [] |202| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 12
           MPYWW   .N1     AM3,A6,A10        ; [A_N1] 

           ADDAB   .D1     D11,A10,A14       ; [A_D1] 
||         ADDAB   .D2     D12,A10,A0        ; [A_D2] 

           MV      .D1     A0,D2             ; [A_D1] 
||         MV      .D2     A14,D3            ; [A_D2] 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 208,column 17,is_stmt,isa 0

           SLDB    .D1     *D2++(1),AL3      ; [A_D1] |208| <0,0>  ^ 
||         SLDB    .D2     *D3++(1),AL2      ; [A_D2] |208| <0,0>  ^ 

           UNPROT          0x1               ; [A_U] 
;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*
;*      Loop found in file               : src/tidl_detectionOutput_score.c
;*      Loop source line                 : 202
;*      Loop opening brace source line   : 203
;*      Loop closing brace source line   : 218
;*      Known Minimum Trip Count         : 1                    
;*      Known Max Trip Count Factor      : 1
;*      Loop Carried Dependency Bound(^) : 24
;*      Unpartitioned Resource Bound     : 3
;*      Partitioned Resource Bound       : 6 (pre-sched)
;*
;*      Searching for software pipeline schedule at ...
;*         ii = 24 Schedule found with 2 iterations in parallel
;*
;*      Partitioned Resource Bound(*)    : 6 (post-sched)
;*
;*      Constant Extension #0 Used [C0]  : 1
;*      Constant Extension #1 Used [C1]  : 2
;*
;*      Resource Partition (may include "post-sched" split/spill moves):
;*
;*                                                A-side   B-side
;*      .L units                                     0        0     
;*      .S units                                     0        0     
;*      .M units                                     0        0     
;*      .N units                                     1        0     
;*      .D units                                     5        -     
;*      .B units                                     1        -     
;*      .C units                                     -        0     
;*      .P units                                     -        0     
;*
;*      .M/.N units                                  2        0     
;*      .L/.S units                                 10        0     
;*      .L/.S/.C units                               2        0     
;*      .L/.S/.C/.M units                            0        0     
;*      .L/.S/.C/.M/.D units                         7        3     
;*
;*      .X cross paths                               1        0     
;*
;*      Bound(.D1 .D2 .D)                            3        -     
;*      Bound(.M .N .MN)                             2        0     
;*      Bound(.L .S .LS)                             5        0     
;*      Bound(.L .S .C .LS .LSC)                     6*       0     
;*      Bound(.L .S .C .M .LS .LSC .LSCM)            4        0     
;*      Bound(.L .S .C .M .D .LS .LSC .LSCM .LSCMD)  5        1     
;*
;*      Register Usage Tables:
;*
;*      +----------------------------------+----------------------------------+
;*      |              ASIDE               |              BSIDE               |
;*      +----------------+--------+--------+----------------+--------+--------+
;*      |      "Axx"     |  "ALx" |  "AMx" |     "VBxx"     | "VBLx" | "VBMx" |
;*      +----------------|--------|--------|----------------|--------|--------+
;*      |0000000000111111|00000000|00000000|0000000000111111|00000000|00000000|
;*      |0123456789012345|01234567|01234567|0123456789012345|01234567|01234567|
;*      +----------------|--------|--------|----------------|--------|--------+
;*   0: | **     *       |  ****  |*       |*               |***     |        |
;*   1: | **     *       |******  |*       |*               |***     |        |
;*   2: | **     *       |  ****  |*       |*               |***     |        |
;*   3: | **     *       |* ****  |*       |*               |***     |        |
;*   4: |***     *       |  ****  |*       |*               |***     |        |
;*   5: |***     *       |  ****  |*       |*               |***     |        |
;*   6: |***     *       |  ****  |*       |*               |***     |        |
;*   7: |***     *       |  ****  |*       |*               |***     |        |
;*   8: |***     *       |  ****  |*       |*               |***     |        |
;*   9: |***     *       |  ****  |***     |*               |***     |        |
;*  10: |***     *       |  ****  |*       |*               |***     |        |
;*  11: |***     *       |  ****  |*       |*               |***     |        |
;*  12: |***     *       |  ****  |*       |*               |***     |        |
;*  13: |***     *       |  ****  |*       |*               |***     |        |
;*  14: |***     *       |  ****  |*       |*               |***     |        |
;*  15: |***     *       |  ****  |*       |*               |***     |        |
;*  16: |***     *       |  ****  |*       |*               |***     |        |
;*  17: |***     *       |  ****  |*       |*               |***     |        |
;*  18: |***     *       |******  |*       |*               |***     |        |
;*  19: |***     *       |  ****  |*       |*               |***     |        |
;*  20: |***     *       |  ****  |*       |*               |***     |        |
;*  21: |***     *       |  ****  |***     |*               |***     |        |
;*  22: |***     *       |  ****  |*       |*               |***     |        |
;*  23: | **     *       |  ****  |*       |*               |***     |        |
;*      +----------------+--------+--------+----------------+--------+--------+
;*
;*      +----------------+                 +--------+       +-------+
;*      |      "Dxx"     |                 |  "Px"  |       |"CUCRx"|
;*      +----------------+                 +--------+       +-------+
;*      |0000000000111111|                 |00000000|       |0000   |
;*      |0123456789012345|                 |01234567|       |0123   |
;*      +----------------+                 +--------+       +-------+
;*   0: |********        |                 |        |       |       |
;*   1: |********        |                 |        |       |       |
;*   2: |**********      |                 |        |       |       |
;*   3: |********        |                 |        |       |       |
;*   4: |********        |                 |        |       |       |
;*   5: |********        |                 |        |       |       |
;*   6: |********        |                 |        |       |       |
;*   7: |********        |                 |        |       |       |
;*   8: |********        |                 |        |       |       |
;*   9: |********        |                 |        |       |       |
;*  10: |********        |                 |        |       |       |
;*  11: |********        |                 |        |       |       |
;*  12: |********        |                 |        |       |       |
;*  13: |*********       |                 |        |       |       |
;*  14: |*********       |                 |        |       |       |
;*  15: |********        |                 |        |       |       |
;*  16: |********        |                 |        |       |       |
;*  17: |********        |                 |        |       |       |
;*  18: |********        |                 |        |       |       |
;*  19: |********        |                 |        |       |       |
;*  20: |********        |                 |        |       |       |
;*  21: |********        |                 |        |       |       |
;*  22: |********        |                 |        |       |       |
;*  23: |********        |                 |        |       |       |
;*      +----------------+                 +--------+       +-------+
;*
;*      Done
;*
;*      Collapsed epilog stages       : 1
;*      Prolog not removed
;*      Collapsed prolog stages       : 0
;*
;*      Max amt of load speculation   : 1 bytes
;*
;*      Minimum safe trip count       : 1
;*      Min. prof. trip count  (est.) : 3
;*
;*      Mem bank conflicts/iter(est.) : { min 0.000, est 0.000, max 0.000 }
;*      Mem bank perf. penalty (est.) : 0.0%
;*
;*
;*      Total cycles (est.)         : 12 + trip_cnt * 24        
;*----------------------------------------------------------------------------*
;*       SETUP CODE
;*
;*                  MVKU32  0x1,A1  ; [] 
;*
;*        SINGLE SCHEDULED ITERATION
;*
;*        ||$C$C3740||:
;*   0              SLDB    .D2     *D2++(1),AL0      ; [A_D2] |208|  ^ 
;*     ||           SLDB    .D1     *D3++(1),AL1      ; [A_D1] |208|  ^ 
;*   1              NOP     0x5     ; [A_B] 
;*   6              VINTSP  .L1     AL0,AM1           ; [A_L1] |208|  ^ 
;*     ||           VINTSP  .S1     AL1,AM2           ; [A_S1] |208|  ^ 
;*   7              NOP     0x2     ; [A_B] 
;*   9              MPYSP   .N1     AM0,AM1,AL0       ; [A_N1] |208|  ^ 
;*     ||           MPYSP   .M1     AM0,AM2,AL1       ; [A_M1] |208|  ^ 
;*  10              NOP     0x3     ; [A_B] 
;*  13              CMPLESP .S1     AL4,AL0,D8        ; [A_S1] |208|  ^ 
;*     ||           CMPLESP .L1     AL4,AL1,D9        ; [A_L1] |208|  ^ 
;*  14              ANDW    .D1     D9,D8,AL0         ; [A_D1] |208|  ^ 
;*  15              CMPEQW  .L1     AL0,0,A0          ; [A_L1] |208|  ^ 
;*  16      [!A0]   CMPGEW  .L1     A8,AL3,A0         ; [A_L1] |210|  ^ 
;*     ||   [ A0]   MVKU32  .S1     0x1,A0            ; [A_S1] |210|  ^ 
;*  17      [!A0]   SLDD    .D1     *D1(1176),D0      ; [A_D1] |213|  ^ [C1]
;*  18              NOP     0x1     ; [A_B] 
;*  19      [!A0]   ORW     .D2     D5,D7,D4          ; [A_D2] |213| 
;*     ||   [!A1]   MVKU32  .L1     0x1,A0            ; [A_L1] 
;*  20      [!A0]   EXT     .S1     AL2,0x20,0x20,AM1 ; [A_S1] [C1]
;*     ||   [!A0]   MVKU32  .L1     0xf8,AM2          ; [A_L1] [C0]
;*  21      [!A0]   MPYDD   .N1     AM2,AM1,D8        ; [A_N1] 
;*  22              NOP     0x1     ; [A_B] 
;*  23      [!A0]   STW     .D1     D4,*D0[A8]        ; [A_D1] |213|  ^ 
;*  24              NOP     0x1     ; [A_B] 
;*  25      [!A0]   ADDD    .D2     D6,D8,D8          ; [A_D2] 
;*  26      [!A0]   SLDW    .D1     *D8(8),B0         ; [A_D1] 
;*  27              NOP     0x5     ; [A_B] 
;*  32              ADDW    .D1     A2,0x1,A2         ; [A_D1] |202| 
;*  33      [!A0]   ADDW    .L2     BL0,0x1,BL0       ; [B_L2] |214| 
;*     ||   [!A0]   ADDW    .D1     A8,0x1,A8         ; [A_D1] |215| 
;*     ||           CMPGTW  .L1X    B0,A2,A0          ; [A_L1] |202| 
;*  34      [ A1]   MV      .L2     BL0,BL1           ; [B_L2] |202| 
;*     ||   [ A1]   MV      .D1     A8,AL5            ; [A_D1] |202| 
;*     ||   [ A1]   MV      .S2     B0,BL2            ; [B_S2] |202| 
;*     ||   [!A0]   MVKU32  .L1     0,A1              ; [A_L1] |202| 
;*  35              ADDW    .D1     D7,0x1,D7         ; [A_D1] |202| 
;*     ||   [ A1]   B       .B1     ||$C$C3740||      ; [A_B] |202| 
;*  36              ; BRANCHCC OCCURS {||$C$C3740||}  ; [] |202| 
;*
;*       RESTORE CODE
;*
;*                  MV      BL1,BL0 ; [] 
;*                  MV      AL5,A8  ; [] 
;*                  MV      BL2,B0  ; [] 
;*----------------------------------------------------------------------------*
||$C$L234||:    ; PIPED LOOP PROLOG
;          EXCLUSIVE CPU CYCLES: 6

           VINTSP  .S1     AL2,AM2           ; [A_S1] |208| <0,6>  ^ 
||         VINTSP  .L1     AL3,AM1           ; [A_L1] |208| <0,6>  ^ 

           NOP             0x1               ; [A_B] 

           MV      .M1X    B12,D6            ; [A_M1] 
||         MV      .D1     A11,AM0           ; [A_D1] 

           MV      .L1X    B1,D9             ; [A_L1] 
||         MPYSP   .M1     AM0,AM2,AL1       ; [A_M1] |208| <0,9>  ^ 
||         MPYSP   .N1     AM0,AM1,AL0       ; [A_N1] |208| <0,9>  ^ 

           MV      .M1     A7,AL2            ; [A_M1] 
||         MV      .D2     A5,D1             ; [A_D2] 
||         MV      .S1     A12,AL4           ; [A_S1] 
||         MV      .L1X    B15,D8            ; [A_L1] 
||         MV      .D1     D9,D5             ; [A_D1] 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 202,column 33,is_stmt,isa 0

           MV      .L2     B5,BL0            ; [B_L2] 
||         MVKU32  .L1     0,A2              ; [A_L1] |202| 
||         MVKU32  .S1     0x1,A1            ; [A_S1] 
||         MV      .D1     D6,AL3            ; [A_D1] 
||         MV      .D2     D8,D6             ; [A_D2] 
||         MV      .M1     A10,D7            ; [A_M1] 

;** --------------------------------------------------------------------------*
||$C$L235||:    ; PIPED LOOP KERNEL
;          EXCLUSIVE CPU CYCLES: 24
           NOP             0x1               ; [A_B] 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 208,column 17,is_stmt,isa 0

           CMPLESP .S1     AL4,AL0,D8        ; [A_S1] |208| <0,13>  ^ 
||         CMPLESP .L1     AL4,AL1,D9        ; [A_L1] |208| <0,13>  ^ 

           ANDW    .D1     D9,D8,AL0         ; [A_D1] |208| <0,14>  ^ 
           CMPEQW  .L1     AL0,0,A0          ; [A_L1] |208| <0,15>  ^ 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 210,column 19,is_stmt,isa 0

   [ A0]   MVKU32  .S1     0x1,A0            ; [A_S1] |210| <0,16>  ^ 
|| [!A0]   CMPGEW  .L1     A8,AL3,A0         ; [A_L1] |210| <0,16>  ^ 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 213,column 21,is_stmt,isa 0
   [!A0]   SLDD    .D1     *D1(1176),D0      ; [A_D1] |213| <0,17>  ^ [C1]
           NOP             0x1               ; [A_B] 

   [!A0]   ORW     .D2     D5,D7,D4          ; [A_D2] |213| <0,19> 
|| [!A1]   MVKU32  .L1     0x1,A0            ; [A_L1] <0,19> 

   [!A0]   EXT     .S1     AL2,0x20,0x20,AM1 ; [A_S1] <0,20> [C1]
|| [!A0]   MVKU32  .L1     0xf8,AM2          ; [A_L1] <0,20> [C0]

   [!A0]   MPYDD   .N1     AM2,AM1,D8        ; [A_N1] <0,21> 
           NOP             0x1               ; [A_B] 
   [!A0]   STW     .D1     D4,*D0[A8]        ; [A_D1] |213| <0,23>  ^ 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 208,column 17,is_stmt,isa 0

           SLDB    .D2     *D2++(1),AL0      ; [A_D2] |208| <1,0>  ^ 
||         SLDB    .D1     *D3++(1),AL1      ; [A_D1] |208| <1,0>  ^ 

   [!A0]   ADDD    .D2     D6,D8,D8          ; [A_D2] <0,25> 
   [!A0]   SLDW    .D1     *D8(8),B0         ; [A_D1] <0,26> 
           NOP             0x3               ; [A_B] 

           VINTSP  .S1     AL1,AM2           ; [A_S1] |208| <1,6>  ^ 
||         VINTSP  .L1     AL0,AM1           ; [A_L1] |208| <1,6>  ^ 

           NOP             0x1               ; [A_B] 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 202,column 38,is_stmt,isa 0
           ADDW    .D1     A2,0x1,A2         ; [A_D1] |202| <0,32> 

   [!A0]   ADDW    .D1     A8,0x1,A8         ; [A_D1] |215| <0,33> 
||         CMPGTW  .L1X    B0,A2,A0          ; [A_L1] |202| <0,33> 
|| [!A0]   ADDW    .L2     BL0,0x1,BL0       ; [B_L2] |214| <0,33> 
||         MPYSP   .M1     AM0,AM2,AL1       ; [A_M1] |208| <1,9>  ^ 
||         MPYSP   .N1     AM0,AM1,AL0       ; [A_N1] |208| <1,9>  ^ 

   [ A1]   MV      .D1     A8,AL5            ; [A_D1] |202| <0,34> 
|| [!A0]   MVKU32  .L1     0,A1              ; [A_L1] |202| <0,34> 
|| [ A1]   MV      .L2     BL0,BL1           ; [B_L2] |202| <0,34> 
|| [ A1]   MV      .S2     B0,BL2            ; [B_S2] |202| <0,34> 

           ADDW    .D1     D7,0x1,D7         ; [A_D1] |202| <0,35> 
|| [ A1]   B       .B1     ||$C$L235||       ; [A_B] |202| <0,35> 

;** --------------------------------------------------------------------------*
||$C$L236||:    ; PIPED LOOP EPILOG
;          EXCLUSIVE CPU CYCLES: 1
           PROTCLR                            ; [A_U] AL5
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 5
           MV      .L1     AL3,A0            ; [A_L1] 
           MV      .D1     D5,A2             ; [A_D1] 

           MV      .L2X    A0,B12            ; [B_L2] 
||         MV      .D1     D6,A1             ; [A_D1] 

           MV      .L2X    A2,B1             ; [B_L2] 
||         MV      .L1     AL4,A12           ; [A_L1] 

           MV      .M2X    A1,B15            ; [B_M2] 
||         MV      .D1     D1,A5             ; [A_D1] 
||         MV      .L1     AL2,A7            ; [A_L1] 
||         MV      .S1     AL5,A8            ; [A_S1] 
||         MV      .M1     AM0,A11           ; [A_M1] 
||         MV      .L2     BL1,B5            ; [B_L2] 
||         MV      .S2     BL2,B0            ; [B_S2] 

;** --------------------------------------------------------------------------*
||$C$L237||:    
;          EXCLUSIVE CPU CYCLES: 8
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 200,column 36,is_stmt,isa 0
           LDW     .D1     *D10(12),AL0      ; [A_D1] |200| 
           ADDW    .D1     A6,0x1,A6         ; [A_D1] |200| 
           CMPGTW  .L1     AL0,A6,A0         ; [A_L1] |200| 
   [ A0]   B       .B1     ||$C$L233||       ; [A_B] |200| 
           ; BRANCHCC OCCURS {||$C$L233||}   ; [] |200| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 7
           LDD     .D1     *A5(1192),D0      ; [A_D1] 

           B       .B1     ||$C$L244||       ; [A_B] 
||         ADDD    .D1     D13,D0,D0         ; [A_D1] 

           ; BRANCH OCCURS {||$C$L244||}     ; [] 
;** --------------------------------------------------------------------------*
||$C$L238||:    
;          EXCLUSIVE CPU CYCLES: 10
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 175,column 13,is_stmt,isa 0
           MPYWW   .N1     A4,A1,D3          ; [A_N1] |175| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 176,column 36,is_stmt,isa 0
           LDW     .D1     *D10(12),AL0      ; [A_D1] |176| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 175,column 13,is_stmt,isa 0
           LDD     .D1     *D1(0),D2         ; [A_D1] |175| 
           ADDW    .D1     A9,D3,AM0         ; [A_D1] |175| 
           MPYWW   .N1     A13,AM0,D1        ; [A_N1] |175| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 176,column 36,is_stmt,isa 0
           CMPGTW  .L1     AL0,0,A0          ; [A_L1] |176| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 175,column 13,is_stmt,isa 0

   [!A0]   B       .B1     ||$C$L244||       ; [A_B] |176| 
||         ADDAB   .D1     D2,D1,D5          ; [A_D1] |175| 
||         MV      .L2X    A8,B0             ; [B_L2] 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 176,column 36,is_stmt,isa 0
           ; BRANCHCC OCCURS {||$C$L244||}   ; [] |176| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 12

           EXT     .L1     A7,0x20,0x20,AM0  ; [A_L1] 
||         MVKU32  .S1     0xf8,AM1          ; [A_S1] 

           MPYDD   .N1     AM1,AM0,D0        ; [A_N1] 
           MV      .L1X    B15,A0            ; [A_L1] 
           ADDD    .D1     A0,D0,D0          ; [A_D1] 
           LDW     .D1     *D0(8),A6         ; [A_D1] 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 176,column 31,is_stmt,isa 0
           MVKU32  .L1     0,A2              ; [A_L1] |176| 
;** --------------------------------------------------------------------------*
;**   BEGIN LOOP ||$C$L239||
;** --------------------------------------------------------------------------*
||$C$L239||:    
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 178,column 38,is_stmt,isa 0
           CMPGTW  .L1     A6,0,A0           ; [A_L1] |178| 
   [!A0]   B       .B1     ||$C$L243||       ; [A_B] |178| 
           ; BRANCHCC OCCURS {||$C$L243||}   ; [] |178| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 14
           MPYWW   .N1     AM3,A2,A10        ; [A_N1] 
           ADDAB   .D1     D5,A10,A0         ; [A_D1] 
           MV      .D1     A0,D1             ; [A_D1] 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 182,column 17,is_stmt,isa 0
           SLDB    .D1     *D1++(1),AL1      ; [A_D1] |182| <0,0>  ^ 
           MV      .L1X    B12,D12           ; [A_L1] 
           MV      .L1X    B15,D11           ; [A_L1] 

           VINTSP  .L1     AL1,AM1           ; [A_L1] |182| <0,6>  ^ 
||         MV      .D1     A6,AL0            ; [A_D1] 
||         MV      .D2     A5,D0             ; [A_D2] 
||         MV      .S1     A11,AM0           ; [A_S1] 
||         MV      .M1X    B0,A8             ; [A_M1] 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 178,column 33,is_stmt,isa 0

           MV      .L2X    A10,BL2           ; [B_L2] 
||         MV      .D1     D12,AL3           ; [A_D1] 
||         MV      .D2     D11,D2            ; [A_D2] 
||         MV      .S1     A7,AL2            ; [A_S1] 
||         MV      .M1     A12,AL4           ; [A_M1] 
||         MVKU32  .L1     0,A1              ; [A_L1] |178| 
||         MV      .S2     B5,BL0            ; [B_S2] 
||         MV      .M2     B1,BL1            ; [B_M2] 
||         UNPROT          0x1               ; [A_U] 

;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*
;*      Loop found in file               : src/tidl_detectionOutput_score.c
;*      Loop source line                 : 178
;*      Loop opening brace source line   : 179
;*      Loop closing brace source line   : 192
;*      Known Minimum Trip Count         : 1                    
;*      Known Max Trip Count Factor      : 1
;*      Loop Carried Dependency Bound(^) : 22
;*      Unpartitioned Resource Bound     : 2
;*      Partitioned Resource Bound       : 4 (pre-sched)
;*
;*      Searching for software pipeline schedule at ...
;*         ii = 22 Schedule found with 2 iterations in parallel
;*
;*      Partitioned Resource Bound(*)    : 4 (post-sched)
;*
;*      Constant Extension #0 Used [C0]  : 1
;*      Constant Extension #1 Used [C1]  : 2
;*
;*      Resource Partition (may include "post-sched" split/spill moves):
;*
;*                                                A-side   B-side
;*      .L units                                     0        0     
;*      .S units                                     0        0     
;*      .M units                                     0        0     
;*      .N units                                     1        0     
;*      .D units                                     4        -     
;*      .B units                                     1        -     
;*      .C units                                     -        0     
;*      .P units                                     -        0     
;*
;*      .M/.N units                                  1        0     
;*      .L/.S units                                  6        0     
;*      .L/.S/.C units                               1        0     
;*      .L/.S/.C/.M units                            0        0     
;*      .L/.S/.C/.M/.D units                         3        3     
;*
;*      .X cross paths                               1        0     
;*
;*      Bound(.D1 .D2 .D)                            2        -     
;*      Bound(.M .N .MN)                             1        0     
;*      Bound(.L .S .LS)                             3        0     
;*      Bound(.L .S .C .LS .LSC)                     4*       0     
;*      Bound(.L .S .C .M .LS .LSC .LSCM)            3        0     
;*      Bound(.L .S .C .M .D .LS .LSC .LSCM .LSCMD)  3        1     
;*
;*      Register Usage Tables:
;*
;*      +----------------------------------+----------------------------------+
;*      |              ASIDE               |              BSIDE               |
;*      +----------------+--------+--------+----------------+--------+--------+
;*      |      "Axx"     |  "ALx" |  "AMx" |     "VBxx"     | "VBLx" | "VBMx" |
;*      +----------------|--------|--------|----------------|--------|--------+
;*      |0000000000111111|00000000|00000000|0000000000111111|00000000|00000000|
;*      |0123456789012345|01234567|01234567|0123456789012345|01234567|01234567|
;*      +----------------|--------|--------|----------------|--------|--------+
;*   0: | *      *       |* ***   |*       |                |***     |        |
;*   1: | *      *       |* ***   |**      |                |***     |        |
;*   2: | *      *       |* ***   |*       |                |***     |        |
;*   3: | *      *       |* ***   |*       |                |***     |        |
;*   4: | *      *       |* ***   |*       |                |***     |        |
;*   5: | *      *       |*****   |*       |                |***     |        |
;*   6: |**      *       |* ***   |*       |                |***     |        |
;*   7: |**      *       |* ***   |*       |                |***     |        |
;*   8: |**      *       |* ***   |*       |                |***     |        |
;*   9: |**      *       |* ***   |***     |                |***     |        |
;*  10: |**      *       |* ***   |*       |                |***     |        |
;*  11: |**      *       |* ***   |*       |                |***     |        |
;*  12: |**      *       |* ***   |*       |                |***     |        |
;*  13: |**      *       |* ***   |*       |*               |***     |        |
;*  14: |**      *       |* ***   |*       |                |***     |        |
;*  15: | *      *       |* ***   |*       |                |***     |        |
;*  16: | *      *       |* ***   |*       |                |***     |        |
;*  17: | *      *       |* ***   |*       |                |***     |        |
;*  18: | *      *       |* ***   |*       |                |***     |        |
;*  19: | *      *       |* ***   |*       |                |***     |        |
;*  20: | *      *       |*****   |*       |                |***     |        |
;*  21: |**      *       |* ***   |*       |                |***     |        |
;*      +----------------+--------+--------+----------------+--------+--------+
;*
;*      +----------------+                 +--------+       +-------+
;*      |      "Dxx"     |                 |  "Px"  |       |"CUCRx"|
;*      +----------------+                 +--------+       +-------+
;*      |0000000000111111|                 |00000000|       |0000   |
;*      |0123456789012345|                 |01234567|       |0123   |
;*      +----------------+                 +--------+       +-------+
;*   0: |***             |                 |        |       |       |
;*   1: |***             |                 |        |       |       |
;*   2: |***             |                 |        |       |       |
;*   3: |***             |                 |        |       |       |
;*   4: |***             |                 |        |       |       |
;*   5: |***             |                 |        |       |       |
;*   6: |***             |                 |        |       |       |
;*   7: |***             |                 |        |       |       |
;*   8: |***             |                 |        |       |       |
;*   9: |***             |                 |        |       |       |
;*  10: |***             |                 |        |       |       |
;*  11: |***             |                 |        |       |       |
;*  12: |***             |                 |        |       |       |
;*  13: |*****           |                 |        |       |       |
;*  14: |****            |                 |        |       |       |
;*  15: |***             |                 |        |       |       |
;*  16: |***             |                 |        |       |       |
;*  17: |***             |                 |        |       |       |
;*  18: |***             |                 |        |       |       |
;*  19: |***             |                 |        |       |       |
;*  20: |***             |                 |        |       |       |
;*  21: |***             |                 |        |       |       |
;*      +----------------+                 +--------+       +-------+
;*
;*      Done
;*
;*      Collapsed epilog stages       : 1
;*      Prolog not removed
;*      Collapsed prolog stages       : 0
;*
;*      Max amt of load speculation   : 1 bytes
;*
;*      Minimum safe trip count       : 1
;*      Min. prof. trip count  (est.) : 2
;*
;*      Mem bank conflicts/iter(est.) : { min 0.000, est 0.000, max 0.000 }
;*      Mem bank perf. penalty (est.) : 0.0%
;*
;*
;*      Total cycles (est.)         : 8 + trip_cnt * 22        
;*----------------------------------------------------------------------------*
;*        SINGLE SCHEDULED ITERATION
;*
;*        ||$C$C3774||:
;*   0              SLDB    .D1     *D1++(1),AL1      ; [A_D1] |182|  ^ 
;*   1              NOP     0x5     ; [A_B] 
;*   6              VINTSP  .L1     AL1,AM1           ; [A_L1] |182|  ^ 
;*   7              NOP     0x2     ; [A_B] 
;*   9              MPYSP   .N1     AM0,AM1,AL1       ; [A_N1] |182|  ^ 
;*  10              NOP     0x3     ; [A_B] 
;*  13              CMPLESP .L1     AL4,AL1,A0        ; [A_L1] |182|  ^ 
;*  14      [ A0]   CMPGEW  .S1     A8,AL3,A0         ; [A_S1] |184|  ^ 
;*     ||   [!A0]   MVKU32  .L1     0x1,A0            ; [A_L1] |184|  ^ 
;*  15      [!A0]   LDD     .D1     *D0(1176),D3      ; [A_D1] |187|  ^ [C1]
;*     ||   [!A0]   ADDW    .L2     BL0,0x1,BL0       ; [B_L2] |188| 
;*  16      [!A0]   EXT     .S1     AL2,0x20,0x20,AM1 ; [A_S1] [C1]
;*     ||   [!A0]   MVKU32  .L1     0xf8,AM2          ; [A_L1] [C0]
;*  17      [!A0]   MPYDD   .N1     AM2,AM1,D4        ; [A_N1] 
;*  18              NOP     0x2     ; [A_B] 
;*  20      [!A0]   ORW     .L2     BL1,BL2,B0        ; [B_L2] |187| 
;*  21      [!A0]   STW     .D1X    B0,*D3[A8]        ; [A_D1] |187|  ^ 
;*     ||   [!A0]   ADDW    .L1     A8,0x1,A8         ; [A_L1] |189| 
;*     ||   [!A0]   ADDD    .D2     D2,D4,D3          ; [A_D2] 
;*  22      [!A0]   LDW     .D2     *D3(8),AL0        ; [A_D2] 
;*  23              NOP     0x4     ; [A_B] 
;*  27              ADDW    .D1     A1,0x1,A1         ; [A_D1] |178| 
;*  28              CMPGTW  .S1     AL0,A1,A0         ; [A_S1] |178| 
;*  29              ADDW    .L2     BL2,0x1,BL2       ; [B_L2] |178| 
;*     ||   [ A0]   B       .B1     ||$C$C3774||      ; [A_B] |178| 
;*  30              ; BRANCHCC OCCURS {||$C$C3774||}  ; [] |178| 
;*----------------------------------------------------------------------------*
||$C$L240||:    ; PIPED LOOP PROLOG
;** --------------------------------------------------------------------------*
||$C$L241||:    ; PIPED LOOP KERNEL
;          EXCLUSIVE CPU CYCLES: 22
           NOP             0x1               ; [A_B] 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 182,column 17,is_stmt,isa 0
           MPYSP   .N1     AM0,AM1,AL1       ; [A_N1] |182| <0,9>  ^ 
           NOP             0x3               ; [A_B] 
           CMPLESP .L1     AL4,AL1,A0        ; [A_L1] |182| <0,13>  ^ 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 184,column 19,is_stmt,isa 0

   [ A0]   CMPGEW  .S1     A8,AL3,A0         ; [A_S1] |184| <0,14>  ^ 
|| [!A0]   MVKU32  .L1     0x1,A0            ; [A_L1] |184| <0,14>  ^ 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 187,column 21,is_stmt,isa 0

   [!A0]   ADDW    .L2     BL0,0x1,BL0       ; [B_L2] |188| <0,15> 
|| [!A0]   LDD     .D1     *D0(1176),D3      ; [A_D1] |187| <0,15>  ^ [C1]

   [!A0]   EXT     .S1     AL2,0x20,0x20,AM1 ; [A_S1] <0,16> [C1]
|| [!A0]   MVKU32  .L1     0xf8,AM2          ; [A_L1] <0,16> [C0]

   [!A0]   MPYDD   .N1     AM2,AM1,D4        ; [A_N1] <0,17> 
           NOP             0x2               ; [A_B] 
   [!A0]   ORW     .L2     BL1,BL2,B0        ; [B_L2] |187| <0,20> 

   [!A0]   ADDW    .L1     A8,0x1,A8         ; [A_L1] |189| <0,21> 
|| [!A0]   ADDD    .D2     D2,D4,D3          ; [A_D2] <0,21> 
|| [!A0]   STW     .D1X    B0,*D3[A8]        ; [A_D1] |187| <0,21>  ^ 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 182,column 17,is_stmt,isa 0

   [!A0]   LDW     .D2     *D3(8),AL0        ; [A_D2] <0,22> 
||         SLDB    .D1     *D1++(1),AL1      ; [A_D1] |182| <1,0>  ^ 

           NOP             0x4               ; [A_B] 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 178,column 38,is_stmt,isa 0
           ADDW    .D1     A1,0x1,A1         ; [A_D1] |178| <0,27> 

           CMPGTW  .S1     AL0,A1,A0         ; [A_S1] |178| <0,28> 
||         VINTSP  .L1     AL1,AM1           ; [A_L1] |182| <1,6>  ^ 

           ADDW    .L2     BL2,0x1,BL2       ; [B_L2] |178| <0,29> 
|| [ A0]   B       .B1     ||$C$L241||       ; [A_B] |178| <0,29> 

;** --------------------------------------------------------------------------*
||$C$L242||:    ; PIPED LOOP EPILOG
;          EXCLUSIVE CPU CYCLES: 1
           PROTCLR                            ; [A_U] AL0
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 4
           MV      .L1     AL3,A0            ; [A_L1] 

           MV      .L2X    A8,B0             ; [B_L2] 
||         MV      .D1     D2,A1             ; [A_D1] 

           MV      .L2X    A0,B12            ; [B_L2] 
||         MV      .L1     AL0,A6            ; [A_L1] 

           MV      .M2X    A1,B15            ; [B_M2] 
||         MV      .L1     AL4,A12           ; [A_L1] 
||         MV      .S1     AL2,A7            ; [A_S1] 
||         MV      .M1     AM0,A11           ; [A_M1] 
||         MV      .D1     D0,A5             ; [A_D1] 
||         MV      .L2     BL0,B5            ; [B_L2] 
||         MV      .S2     BL1,B1            ; [B_S2] 

;** --------------------------------------------------------------------------*
||$C$L243||:    
;          EXCLUSIVE CPU CYCLES: 8
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 176,column 36,is_stmt,isa 0
           LDW     .D1     *D10(12),AL0      ; [A_D1] |176| 
           ADDW    .D1     A2,0x1,A2         ; [A_D1] |176| 
           CMPGTW  .L1     AL0,A2,A0         ; [A_L1] |176| 
   [ A0]   B       .B1     ||$C$L239||       ; [A_B] |176| 
           ; BRANCHCC OCCURS {||$C$L239||}   ; [] |176| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 7
           LDD     .D1     *A5(1192),D0      ; [A_D1] 
           ADDD    .D1     D13,D0,D0         ; [A_D1] 
;** --------------------------------------------------------------------------*
||$C$L244||:    
;          EXCLUSIVE CPU CYCLES: 11
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 222,column 11,is_stmt,isa 0
           LDD     .D1     *SP(48),A0        ; [A_D1] |222| 
           STW     .D1X    B5,*D0(0)         ; [A_D1] |222| 
           LDW     .D1     *A0(12),A1        ; [A_D1] |222| 
;** --------------------------------------------------------------------------*
||$C$L245||:    
;          EXCLUSIVE CPU CYCLES: 3
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 164,column 22,is_stmt,isa 0
           ADDW    .D1     A9,0x1,A9         ; [A_D1] |164| 
           CMPGTW  .L1     A1,A9,A0          ; [A_L1] |164| 
   [ A0]   B       .B1     ||$C$L232||       ; [A_B] |164| 
           ; BRANCHCC OCCURS {||$C$L232||}   ; [] |164| 
;** --------------------------------------------------------------------------*
||$C$L246||:    
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 162,column 20,is_stmt,isa 0
           ADDW    .D1     A3,0xffffffff,A3  ; [A_D1] |162| 

   [ A3]   B       .B1     ||$C$L231||       ; [A_B] |162| 
||         ADDW    .D1     A4,0x1,A4         ; [A_D1] |162| 

           ; BRANCHCC OCCURS {||$C$L231||}   ; [] |162| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 9
           LDD     .D1     *SP(48),A0        ; [A_D1] 
           LDW     .D1     *A0(56),AL0       ; [A_D1] 
;** --------------------------------------------------------------------------*
||$C$L247||:    
;          EXCLUSIVE CPU CYCLES: 5
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 155,column 17,is_stmt,isa 0
           MV      .L1X    B3,A2             ; [A_L1] |155| 

           ADDD    .D1     A2,0xc,A3         ; [A_D1] |155| 
||         MV      .L1X    B2,A0             ; [A_L1] |155| 

           ADDD    .D1     A0,0xc,A2         ; [A_D1] |155| 
||         ADDW    .D2     A7,0x1,A7         ; [A_D2] |155| 

           CMPGTW  .L1     AL0,A7,A0         ; [A_L1] |155| 
||         MV      .L2X    A3,B3             ; [B_L2] |155| 

   [ A0]   B       .B1     ||$C$L230||       ; [A_B] |155| 
||         MV      .L2X    A2,B2             ; [B_L2] |155| 
||         ADDD    .D1     D10,0xf8,D10      ; [A_D1] |155| 
||         ADDD    .L1     A15,0x1,A15       ; [A_L1] |155| 
||         ADDD    .D2     D14,0x4,D14       ; [A_D2] |155| 

           ; BRANCHCC OCCURS {||$C$L230||}   ; [] |155| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 12
           LDD     .D1     *SP(48),A0        ; [A_D1] 
           LDW     .D1     *A0(68),AL1       ; [A_D1] 
           MV      .D1     A8,A9             ; [A_D1] 
;** --------------------------------------------------------------------------*
||$C$L248||:    
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 227,column 3,is_stmt,isa 0
           CMPEQW  .L1     AL1,0x2,A0        ; [A_L1] |227| 
   [!A0]   B       .B1     ||$C$L265||       ; [A_B] |227| 
           ; BRANCHCC OCCURS {||$C$L265||}   ; [] |227| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 14
           LDD     .D1     *SP(48),A0        ; [A_D1] 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 229,column 17,is_stmt,isa 0
           LDW     .D1     *A0(56),AL0       ; [A_D1] |229| 
           MVKU32  .L1     0,D8              ; [A_L1] 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 229,column 10,is_stmt,isa 0

           MV      .D1     D8,D14            ; [A_D1] 
||         MVKU32  .L1     0,A1              ; [A_L1] |229| 
||         MV      .S1X    B15,A7            ; [A_S1] 
||         ADDD    .D2     A5,0x248,A2       ; [A_D2] 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 229,column 17,is_stmt,isa 0

           CMPGTW  .L1     AL0,0,A0          ; [A_L1] |229| 
||         STD     .D1     A2,*SP(24)        ; [A_D1] |229| 
||         STD     .D2X    A7,*SP(16)        ; [A_D2] 

   [!A0]   B       .B1     ||$C$L265||       ; [A_B] |229| 
||         STD     .D1     D14,*SP(32)       ; [A_D1] 
||         MV      .L2X    A1,B13            ; [B_L2] |229| 
||         ADDD    .D2     A5,0x140,D7       ; [A_D2] 

           ; BRANCHCC OCCURS {||$C$L265||}   ; [] |229| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 5

           MVKU32  .L2     0x7f7fffff,BL5    ; [B_L2] 
||         MVKU32  .S2     0xff7fffff,BL6    ; [B_S2] 

           MVKU32  .L2     0x3d2aaab4,B9     ; [B_L2] 
||         MVKU32  .S2     0x37800000,BM1    ; [B_S2] 

           MVKU32  .L2     0x3e2aaaad,B7     ; [B_L2] 
||         MVKU32  .S2     0x3f317218,B8     ; [B_S2] 

           MV      .L1X    B12,AL7           ; [A_L1] 
||         MVKU32  .L2     0x3f000000,B6     ; [B_L2] 
||         MVKU32  .S2     0x3fb8aa3b,BM2    ; [B_S2] 

	.dwpsn	file "./inc/tidl_detectionOutput_int.h",line 157,column 3,is_stmt,isa 0

           MV      .L1X    B15,D13           ; [A_L1] |294| 
||         MVKU32  .L2     0,BL7             ; [B_L2] |157| 
||         MVKU32  .S2     0x10000,BL4       ; [B_S2] 

;** --------------------------------------------------------------------------*
;**   BEGIN LOOP ||$C$L249||
;** --------------------------------------------------------------------------*
||$C$L249||:    
;          EXCLUSIVE CPU CYCLES: 13
           LDD     .D1     *SP(48),A0        ; [A_D1] 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 233,column 7,is_stmt,isa 0
           LDD     .D1     *SP(16),A1        ; [A_D1] |233| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 235,column 7,is_stmt,isa 0

           LDW     .D1     *A0(76),A0        ; [A_D1] |236| 
||         LDW     .D2     *A2(0),D14        ; [A_D2] |235| 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 234,column 7,is_stmt,isa 0
           LDW     .D1     *A1(0),B0         ; [A_D1] |234| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 233,column 7,is_stmt,isa 0

   [ A0]   B       .B1     ||$C$L250||       ; [A_B] |236| 
||         STD     .D1     D14,*SP(40)       ; [A_D1] |235| 
||         LDUW    .D2     *D7(0),B5         ; [A_D2] |233| 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 236,column 7,is_stmt,isa 0
           ; BRANCHCC OCCURS {||$C$L250||}   ; [] |236| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 14
           LDD     .D1     *SP(48),A0        ; [A_D1] 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 239,column 9,is_stmt,isa 0
           LDW     .D1     *A0(12),A1        ; [A_D1] |239| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 246,column 20,is_stmt,isa 0
           CMPGTW  .L1X    B0,0,A4           ; [A_L1] |246| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 239,column 9,is_stmt,isa 0

   [!A4]   B       .B1     ||$C$L264||       ; [A_B] |246| 
||         MV      .L2X    A1,B11            ; [B_L2] |239| 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 246,column 20,is_stmt,isa 0
           ; BRANCHCC OCCURS {||$C$L264||}   ; [] |246| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 1
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 238,column 9,is_stmt,isa 0

           B       .B1     ||$C$L251||       ; [A_B] 
||         MVKU32  .L1     0x1,A4            ; [A_L1] |238| 

           ; BRANCH OCCURS {||$C$L251||}     ; [] 
;** --------------------------------------------------------------------------*
||$C$L250||:    
;          EXCLUSIVE CPU CYCLES: 3
           MV      .L1X    B0,A4             ; [A_L1] 
           CMPGTW  .L1     A4,0,A0           ; [A_L1] 
   [!A0]   B       .B1     ||$C$L264||       ; [A_B] 
           ; BRANCHCC OCCURS {||$C$L264||}   ; [] 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 3
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 244,column 9,is_stmt,isa 0
           MVKU32  .L1     0x1,A0            ; [A_L1] |244| 
           MV      .L2X    A0,B11            ; [B_L2] |244| 
;** --------------------------------------------------------------------------*
||$C$L251||:    
;          EXCLUSIVE CPU CYCLES: 13
           MV      .L1X    B13,A0            ; [A_L1] 

           EXT     .L1     A0,0x20,0x20,AM0  ; [A_L1] 
||         MVKU32  .S1     0xf8,AM1          ; [A_S1] 

           MPYDD   .N1     AM1,AM0,D0        ; [A_N1] 
           MV      .L1X    B15,A1            ; [A_L1] 
           ADDD    .D1     A1,D0,D0          ; [A_D1] 
           LDW     .D1     *D0(12),AL0       ; [A_D1] 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 246,column 12,is_stmt,isa 0

           SHLW    .L1     A0,0x4,D10        ; [A_L1] 
||         MV      .M1X    B0,A2             ; [A_M1] 
||         MVKU32  .S1     0,A7              ; [A_S1] |246| 

;** --------------------------------------------------------------------------*
;**   BEGIN LOOP ||$C$L252||
;** --------------------------------------------------------------------------*
||$C$L252||:    
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 248,column 9,is_stmt,isa 0

           CMPGTW  .L1     AL0,0,A0          ; [A_L1] |250| 
||         ORW     .D1     A7,D10,AL1        ; [A_D1] |248| 

   [!A0]   B       .B1     ||$C$L263||       ; [A_B] |250| 
||         SHLW    .L1     AL1,0x18,D9       ; [A_L1] |248| 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 250,column 32,is_stmt,isa 0
           ; BRANCHCC OCCURS {||$C$L263||}   ; [] |250| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 13
           MV      .L1X    B13,A0            ; [A_L1] 
           EXT     .L1     A0,0x20,0x20,AM0  ; [A_L1] 
           MPYDD   .N1     AM1,AM0,D0        ; [A_N1] 
           MV      .L1X    B15,D14           ; [A_L1] 
           ADDD    .D1     D14,D0,D0         ; [A_D1] 
           LDW     .D1     *D0(8),A8         ; [A_D1] 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 250,column 27,is_stmt,isa 0
           MVKU32  .L1     0,A10             ; [A_L1] |250| 
;** --------------------------------------------------------------------------*
;**   BEGIN LOOP ||$C$L253||
;** --------------------------------------------------------------------------*
||$C$L253||:    
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 252,column 34,is_stmt,isa 0
           CMPGTW  .L1     A8,0,A0           ; [A_L1] |252| 
   [!A0]   B       .B1     ||$C$L262||       ; [A_B] |252| 
           ; BRANCHCC OCCURS {||$C$L262||}   ; [] |252| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 9
           LDD     .D1     *SP(32),A0        ; [A_D1] 
           LDD     .D1     *SP(40),AM3       ; [A_D1] 
           SHLD    .L1     A0,0x3,D0         ; [A_L1] 

           ADDD    .D1     A5,D0,D0          ; [A_D1] 
||         ADDD    .D2     D8,A5,D1          ; [A_D2] 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 252,column 29,is_stmt,isa 0

           ADDD    .D1     D0,0xc0,D6        ; [A_D1] 
||         MPYWW   .N1     AM3,A10,A12       ; [A_N1] 
||         ADDD    .D2     D1,0x244,D5       ; [A_D2] 
||         MVKU32  .L1     0,A11             ; [A_L1] |252| 

;** --------------------------------------------------------------------------*
;**   BEGIN LOOP ||$C$L254||
;** --------------------------------------------------------------------------*
||$C$L254||:    
;          EXCLUSIVE CPU CYCLES: 25
           LDW     .D1     *D5(0),A13        ; [A_D1] 
           MPYWW   .N1     A7,A13,AM0        ; [A_N1] 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 254,column 13,is_stmt,isa 0
           MV      .L1X    B11,A0            ; [A_L1] |254| 

           MPYWW   .N1     A0,AM0,D0         ; [A_N1] 
||         LDD     .D1     *SP(48),D14       ; [A_D1] 
||         LDD     .D2     *D6(0),A6         ; [A_D2] 

           LDW     .D1     *A5(1304),AL0     ; [A_D1] |254| 
           ADDW    .D1     A11,D0,D0         ; [A_D1] 
           ADDW    .D1     A12,D0,D0         ; [A_D1] 

           ADDAB   .D1     A6,D0,D0          ; [A_D1] 
||         LDW     .D2     *D14(12),B3       ; [A_D2] |254| 

           LDB     .D1     *D0(0),B0         ; [A_D1] 
           CMPEQW  .L1     AL0,0x6,A3        ; [A_L1] |254| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 257,column 15,is_stmt,isa 0
   [ A3]   VSPTRUNC .L2    BL5,BL0           ; [B_L2] |257| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 256,column 15,is_stmt,isa 0
   [ A3]   VSPTRUNC .S2    BL6,BL1           ; [B_S2] |256| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 262,column 15,is_stmt,isa 0
           XORW    .L2     BL3,0xffffffff,BL2 ; [B_L2] |262| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 261,column 15,is_stmt,isa 0

           CMPGTW  .L1X    B3,0,A0           ; [A_L1] |266| 
|| [!A3]   EXT     .L2     BL2,0x38,0x38,B1  ; [B_L2] |262| 
|| [!A3]   EXT     .S2     BL3,0x38,0x38,B2  ; [B_S2] |261| 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 256,column 15,is_stmt,isa 0

   [!A0]   B       .B1     ||$C$L261||       ; [A_B] |266| 
||         MV      .L1X    B0,AL0            ; [A_L1] 
|| [ A3]   EXT     .L2     BL1,0x38,0x38,B2  ; [B_L2] |256| 
|| [ A3]   EXT     .S2     BL0,0x38,0x38,B1  ; [B_S2] |257| 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 266,column 26,is_stmt,isa 0
           ; BRANCHCC OCCURS {||$C$L261||}   ; [] |266| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 11
           LDD     .D1     *SP(48),A1        ; [A_D1] 

           LDW     .D1     *A1(16),A14       ; [A_D1] 
||         MV      .L1X    B3,A0             ; [A_L1] 

           MV      .L1X    B11,A3            ; [A_L1] 

           CMPGTD  .L1     A0,0x3,A3         ; [A_L1] 
||         MPYWW   .N1     A3,A7,A15         ; [A_N1] 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 266,column 18,is_stmt,isa 0

   [ A3]   B       .B1     ||$C$L256||       ; [A_B] 
||         MVKU32  .L1     0,A1              ; [A_L1] |266| 

           ; BRANCHCC OCCURS {||$C$L256||}   ; [] 
;** --------------------------------------------------------------------------*
;**   BEGIN LOOP ||$C$L255||
;** --------------------------------------------------------------------------*
||$C$L255||:    
;          EXCLUSIVE CPU CYCLES: 20
           MPYWW   .N1     A4,A1,D0          ; [A_N1] 
           ADDW    .D1     A15,D0,AM0        ; [A_D1] 
           MPYWW   .N1     A13,AM0,D0        ; [A_N1] 
           ADDW    .D1     A11,D0,D0         ; [A_D1] 
           ADDW    .D1     A12,D0,D0         ; [A_D1] 

           LDB     .D1     *A6[D0],B0        ; [A_D1] 
||         MV      .L2     B0,BL2            ; [B_L2] 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 270,column 15,is_stmt,isa 0

           VMINB   .L2     B0,B1,BL0         ; [B_L2] |273| 
||         CMPEQW  .L1     A1,A14,A3         ; [A_L1] |270| 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 266,column 26,is_stmt,isa 0

   [!A3]   EXT     .L2     BL0,0x38,0x38,B1  ; [B_L2] |273| 
||         VMAXB   .S2     B0,B2,BL1         ; [B_S2] |272| 
||         ADDW    .D1     A0,0xffffffff,A0  ; [A_D1] |266| 

   [ A0]   B       .B1     ||$C$L255||       ; [A_B] |266| 
|| [!A3]   MV      .L2     BL2,B0            ; [B_L2] 
|| [!A3]   EXT     .S2     BL1,0x38,0x38,B2  ; [B_S2] |272| 
||         ADDW    .D1     A1,0x1,A1         ; [A_D1] |266| 

           ; BRANCHCC OCCURS {||$C$L255||}   ; [] |266| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 1
           B       .B1     ||$C$L260||       ; [A_B] 
           ; BRANCH OCCURS {||$C$L260||}     ; [] 
;** --------------------------------------------------------------------------*
||$C$L256||:    
;          EXCLUSIVE CPU CYCLES: 3

           EXT     .L1     A0,0x20,0x20,AL0  ; [A_L1] 
||         MV      .D1     A4,AM3            ; [A_D1] 

           MPYWW   .N1     AM3,A1,D0         ; [A_N1] <0,1> 
||         NLCINIT .S1     AL0,0x1,6         ; [A_S1] 
||         UNPROT          0x1               ; [A_U] 
||         MV      .D1     A14,AL3           ; [A_D1] 

           TICK                               ; [A_U] <0,0> 
||         CMPEQW  .L1     A1,AL3,AM0        ; [A_L1] |270| <0,2> 
||         ADDW    .D1     A1,0x1,A0         ; [A_D1] |266| <0,2> 

;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*
;*      Loop found in file               : src/tidl_detectionOutput_score.c
;*      Loop source line                 : 266
;*      Loop opening brace source line   : 267
;*      Loop closing brace source line   : 279
;*      Known Minimum Trip Count         : 1                    
;*      Known Max Trip Count Factor      : 1
;*      Loop Carried Dependency Bound(^) : 2
;*      Unpartitioned Resource Bound     : 2
;*      Partitioned Resource Bound       : 2 (pre-sched)
;*
;*      Searching for software pipeline schedule at ...
;*         ii = 2  Schedule found with 10 iterations in parallel
;*
;*      Partitioned Resource Bound(*)    : 2 (post-sched)
;*
;*      Constant Extension #0 Used [C0]  : 1
;*      Constant Extension #1 Used [C1]  : 1
;*
;*      Resource Partition (may include "post-sched" split/spill moves):
;*
;*                                                A-side   B-side
;*      .L units                                     0        0     
;*      .S units                                     0        0     
;*      .M units                                     0        0     
;*      .N units                                     0        0     
;*      .D units                                     1        -     
;*      .B units                                     1        -     
;*      .C units                                     -        0     
;*      .P units                                     -        0     
;*
;*      .M/.N units                                  2        0     
;*      .L/.S units                                  2        2     
;*      .L/.S/.C units                               0        0     
;*      .L/.S/.C/.M units                            0        2     
;*      .L/.S/.C/.M/.D units                         5        2     
;*
;*      .X cross paths                               0        0     
;*
;*      Bound(.D1 .D2 .D)                            1        -     
;*      Bound(.M .N .MN)                             1        0     
;*      Bound(.L .S .LS)                             1        1     
;*      Bound(.L .S .C .LS .LSC)                     1        1     
;*      Bound(.L .S .C .M .LS .LSC .LSCM)            1        1     
;*      Bound(.L .S .C .M .D .LS .LSC .LSCM .LSCMD)  2*       2*    
;*
;*      Register Usage Tables:
;*
;*      +----------------------------------+----------------------------------+
;*      |              ASIDE               |              BSIDE               |
;*      +----------------+--------+--------+----------------+--------+--------+
;*      |      "Axx"     |  "ALx" |  "AMx" |     "VBxx"     | "VBLx" | "VBMx" |
;*      +----------------|--------|--------|----------------|--------|--------+
;*      |0000000000111111|00000000|00000000|0000000000111111|00000000|00000000|
;*      |0123456789012345|01234567|01234567|0123456789012345|01234567|01234567|
;*      +----------------|--------|--------|----------------|--------|--------+
;*   0: |**              |****    |*****   |*               |* *     |*       |
;*   1: |**              |****    |* ***   |*               |***     |*       |
;*      +----------------+--------+--------+----------------+--------+--------+
;*
;*      +----------------+                 +--------+       +-------+
;*      |      "Dxx"     |                 |  "Px"  |       |"CUCRx"|
;*      +----------------+                 +--------+       +-------+
;*      |0000000000111111|                 |00000000|       |0000   |
;*      |0123456789012345|                 |01234567|       |0123   |
;*      +----------------+                 +--------+       +-------+
;*   0: |****            |                 |        |       |       |
;*   1: |*****           |                 |        |       |       |
;*      +----------------+                 +--------+       +-------+
;*
;*      Done
;*
;*      Redundant loop generated
;*      Epilog not entirely removed
;*      Collapsed epilog stages       : 6
;*
;*      Prolog not removed
;*      Collapsed prolog stages       : 0
;*
;*      Max amt of load speculation   : 0 bytes
;*
;*      Minimum safe trip count       : 4
;*
;*
;*      Mem bank conflicts/iter(est.) : { min 0.000, est 0.000, max 0.000 }
;*      Mem bank perf. penalty (est.) : 0.0%
;*
;*
;*      Total cycles (est.)         : 18 + trip_cnt * 2        
;*----------------------------------------------------------------------------*
;*        SINGLE SCHEDULED ITERATION
;*
;*        ||$C$C3606||:
;*   0              TICK                               ; [A_U] 
;*   1              MPYWW   .N1     AM3,A1,D4         ; [A_N1] 
;*   2              CMPEQW  .L1     A1,AL3,AM0        ; [A_L1] |270| 
;*     ||           ADDW    .D2     A1,0x1,A1         ; [A_D2] |266| 
;*   3              MV      .M1     AM0,AL0           ; [A_M1] |270| Split a long life
;*   4              NOP     0x1     ; [A_B] 
;*   5              ADDW    .D2     D3,D4,AM1         ; [A_D2] 
;*     ||           MVDLY3  .L1     AL0,AL1           ; [A_L1] |270| Split a long life
;*   6              MPYWW   .N1     AM4,AM1,AM0       ; [A_N1] 
;*   7              NOP     0x2     ; [A_B] 
;*   9              MVDLY4  .S1     AL1,AL2           ; [A_S1] |270| Split a long life
;*  10              ADDW    .M1     AM2,AM0,D0        ; [A_M1] 
;*  11              ADDW    .D1     D2,D0,D0          ; [A_D1] 
;*  12              LDB     .D1     *D1[D0],B0        ; [A_D1]  ^ 
;*  13              NOP     0x1     ; [A_B] 
;*  14              MVDLY4  .S1     AL2,A0            ; [A_S1] |270| Split a long life
;*  15              NOP     0x2     ; [A_B] 
;*  17              MV      .M2     B0,BL0            ; [B_M2]  ^ 
;*  18              VMAXB   .S2     B0,BL2,BL1        ; [B_S2] |272|  ^ 
;*     ||           VMINB   .M2     B0,BM0,BL0        ; [B_M2] |273|  ^ 
;*     ||   [!A0]   MV      .L2     BL0,B0            ; [B_L2]  ^ 
;*  19      [!A0]   EXT     .L2     BL1,0x38,0x38,BL2 ; [B_L2] |272|  ^ [C1]
;*     ||   [!A0]   EXT     .S2     BL0,0x38,0x38,BM0 ; [B_S2] |273|  ^ [C0]
;*     ||           BNL     .B1     ||$C$C3606||      ; [A_B] |266| 
;*  20              ; BRANCHCC OCCURS {||$C$C3606||}  ; [] |266| 
;*----------------------------------------------------------------------------*
||$C$L257||:    ; PIPED LOOP PROLOG
;          EXCLUSIVE CPU CYCLES: 15
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 270,column 15,is_stmt,isa 0

           TICK                               ; [A_U] <1,0> 
||         MV      .M1     AM0,AL0           ; [A_M1] |270| <0,3> Split a long life
||         MPYWW   .N1     AM3,A0,D0         ; [A_N1] <1,1> 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 266,column 26,is_stmt,isa 0

           MV      .D1     A15,D3            ; [A_D1] 
||         ADDW    .D2     A0,0x1,A0         ; [A_D2] |266| <1,2> 
||         CMPEQW  .L1     A0,AL3,AM0        ; [A_L1] |270| <1,2> 
||         TICK                               ; [A_U] <2,0> 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 270,column 15,is_stmt,isa 0

           MV      .D1     A13,AM4           ; [A_D1] 
||         MVDLY3  .L1     AL0,AL0           ; [A_L1] |270| <0,5> Split a long life
||         ADDW    .D2     D3,D0,AM0         ; [A_D2] <0,5> 
||         MV      .M1     AM0,AL0           ; [A_M1] |270| <1,3> Split a long life
||         MPYWW   .N1     AM3,A0,D0         ; [A_N1] <2,1> 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 266,column 26,is_stmt,isa 0

           MPYWW   .N1     AM4,AM0,AM1       ; [A_N1] <0,6> 
||         ADDW    .D2     A0,0x1,A0         ; [A_D2] |266| <2,2> 
||         CMPEQW  .L1     A0,AL3,AM0        ; [A_L1] |270| <2,2> 
||         TICK                               ; [A_U] <3,0> 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 270,column 15,is_stmt,isa 0

           MVDLY3  .L1     AL0,AL0           ; [A_L1] |270| <1,5> Split a long life
||         ADDW    .D2     D3,D0,AM0         ; [A_D2] <1,5> 
||         MV      .M1     AM0,AL1           ; [A_M1] |270| <2,3> Split a long life
||         MPYWW   .N1     AM3,A0,D0         ; [A_N1] <3,1> 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 266,column 26,is_stmt,isa 0

           MPYWW   .N1     AM4,AM0,AM1       ; [A_N1] <1,6> 
||         ADDW    .D2     A0,0x1,A0         ; [A_D2] |266| <3,2> 
||         CMPEQW  .L1     A0,AL3,AM0        ; [A_L1] |270| <3,2> 
||         TICK                               ; [A_U] <4,0> 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 270,column 15,is_stmt,isa 0

           MV      .D1     A11,AM2           ; [A_D1] 
||         MVDLY4  .S1     AL0,AL2           ; [A_S1] |270| <0,9> Split a long life
||         MVDLY3  .L1     AL1,AL0           ; [A_L1] |270| <2,5> Split a long life
||         ADDW    .D2     D3,D0,AM0         ; [A_D2] <2,5> 
||         MV      .M1     AM0,AL1           ; [A_M1] |270| <3,3> Split a long life
||         MPYWW   .N1     AM3,A0,D0         ; [A_N1] <4,1> 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 266,column 26,is_stmt,isa 0

           MV      .S1     A6,D1             ; [A_S1] 
||         MV      .D1     A12,D2            ; [A_D1] 
||         ADDW    .M1     AM2,AM1,D11       ; [A_M1] <0,10> 
||         MPYWW   .N1     AM4,AM0,AM1       ; [A_N1] <2,6> 
||         ADDW    .D2     A0,0x1,A0         ; [A_D2] |266| <4,2> 
||         CMPEQW  .L1     A0,AL3,AM0        ; [A_L1] |270| <4,2> 
||         TICK                               ; [A_U] <5,0> 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 270,column 15,is_stmt,isa 0

           ADDW    .D1     D2,D11,D0         ; [A_D1] <0,11> 
||         MVDLY4  .S1     AL0,AL2           ; [A_S1] |270| <1,9> Split a long life
||         MVDLY3  .L1     AL1,AL0           ; [A_L1] |270| <3,5> Split a long life
||         ADDW    .D2     D3,D0,AM0         ; [A_D2] <3,5> 
||         MV      .M1     AM0,AL1           ; [A_M1] |270| <4,3> Split a long life
||         MPYWW   .N1     AM3,A0,D0         ; [A_N1] <5,1> 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 266,column 26,is_stmt,isa 0

           LDB     .D1     *D1[D0],B0        ; [A_D1] <0,12>  ^ 
||         ADDW    .M1     AM2,AM1,D11       ; [A_M1] <1,10> 
||         MPYWW   .N1     AM4,AM0,AM1       ; [A_N1] <3,6> 
||         ADDW    .D2     A0,0x1,A0         ; [A_D2] |266| <5,2> 
||         CMPEQW  .L1     A0,AL3,AM0        ; [A_L1] |270| <5,2> 
||         TICK                               ; [A_U] <6,0> 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 270,column 15,is_stmt,isa 0

           ADDW    .D1     D2,D11,D0         ; [A_D1] <1,11> 
||         MVDLY4  .S1     AL0,AL2           ; [A_S1] |270| <2,9> Split a long life
||         MVDLY3  .L1     AL1,AL0           ; [A_L1] |270| <4,5> Split a long life
||         ADDW    .D2     D3,D0,AM0         ; [A_D2] <4,5> 
||         MV      .M1     AM0,AL1           ; [A_M1] |270| <5,3> Split a long life
||         MPYWW   .N1     AM3,A0,D0         ; [A_N1] <6,1> 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 266,column 26,is_stmt,isa 0

           MVDLY4  .S1     AL2,A0            ; [A_S1] |270| <0,14> Split a long life
||         LDB     .D1     *D1[D0],B0        ; [A_D1] <1,12>  ^ 
||         ADDW    .M1     AM2,AM1,D11       ; [A_M1] <2,10> 
||         MPYWW   .N1     AM4,AM0,AM0       ; [A_N1] <4,6> 
||         ADDW    .D2     A0,0x1,A0         ; [A_D2] |266| <6,2> 
||         CMPEQW  .L1     A0,AL3,AM0        ; [A_L1] |270| <6,2> 
||         TICK                               ; [A_U] <7,0> 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 270,column 15,is_stmt,isa 0

           ADDW    .D1     D2,D11,D0         ; [A_D1] <2,11> 
||         MVDLY4  .S1     AL0,AL2           ; [A_S1] |270| <3,9> Split a long life
||         MVDLY3  .L1     AL1,AL1           ; [A_L1] |270| <5,5> Split a long life
||         ADDW    .D2     D3,D0,AM0         ; [A_D2] <5,5> 
||         MV      .M1     AM0,AL1           ; [A_M1] |270| <6,3> Split a long life
||         MPYWW   .N1     AM3,A0,D4         ; [A_N1] <7,1> 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 266,column 26,is_stmt,isa 0

           MVDLY4  .S1     AL2,A0            ; [A_S1] |270| <1,14> Split a long life
||         LDB     .D1     *D1[D0],B0        ; [A_D1] <2,12>  ^ 
||         ADDW    .M1     AM2,AM1,D11       ; [A_M1] <3,10> 
||         MPYWW   .N1     AM4,AM0,AM0       ; [A_N1] <5,6> 
||         ADDW    .D2     A0,0x1,A1         ; [A_D2] |266| <7,2> 
||         CMPEQW  .L1     A0,AL3,AM0        ; [A_L1] |270| <7,2> 
||         TICK                               ; [A_U] <8,0> 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 270,column 15,is_stmt,isa 0

           MV      .S2     B2,BL2            ; [B_S2] 
||         MV      .L2     B1,BM0            ; [B_L2] 
||         MV      .M2     B0,BL0            ; [B_M2] <0,17>  ^ 
||         ADDW    .D1     D2,D11,D0         ; [A_D1] <3,11> 
||         MVDLY4  .S1     AL0,AL2           ; [A_S1] |270| <4,9> Split a long life
||         MVDLY3  .L1     AL1,AL1           ; [A_L1] |270| <6,5> Split a long life
||         ADDW    .D2     D3,D0,AM1         ; [A_D2] <6,5> 
||         MV      .M1     AM0,AL0           ; [A_M1] |270| <7,3> Split a long life
||         MPYWW   .N1     AM3,A1,D4         ; [A_N1] <8,1> 

;** --------------------------------------------------------------------------*
||$C$L258||:    ; PIPED LOOP KERNEL
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 266,column 26,is_stmt,isa 0

   [!A0]   MV      .L2     BL0,B0            ; [B_L2] <0,18>  ^ 
||         VMAXB   .S2     B0,BL2,BL1        ; [B_S2] |272| <0,18>  ^ 
||         VMINB   .M2     B0,BM0,BL0        ; [B_M2] |273| <0,18>  ^ 
||         MVDLY4  .S1     AL2,A0            ; [A_S1] |270| <2,14> Split a long life
||         LDB     .D1     *D1[D0],B0        ; [A_D1] <3,12>  ^ 
||         ADDW    .M1     AM2,AM0,D0        ; [A_M1] <4,10> 
||         MPYWW   .N1     AM4,AM1,AM0       ; [A_N1] <6,6> 
||         ADDW    .D2     A1,0x1,A1         ; [A_D2] |266| <8,2> 
||         CMPEQW  .L1     A1,AL3,AM0        ; [A_L1] |270| <8,2> 
||         TICK                               ; [A_U] <9,0> 

           BNL     .B1     ||$C$L258||       ; [A_B] |266| <0,19> 
|| [!A0]   EXT     .L2     BL1,0x38,0x38,BL2 ; [B_L2] |272| <0,19>  ^ [C1]
|| [!A0]   EXT     .S2     BL0,0x38,0x38,BM0 ; [B_S2] |273| <0,19>  ^ [C0]
||         MV      .M2     B0,BL0            ; [B_M2] <1,17>  ^ 
||         ADDW    .D1     D2,D0,D0          ; [A_D1] <4,11> 
||         MVDLY4  .S1     AL1,AL2           ; [A_S1] |270| <5,9> Split a long life
||         MVDLY3  .L1     AL0,AL1           ; [A_L1] |270| <7,5> Split a long life
||         ADDW    .D2     D3,D4,AM1         ; [A_D2] <7,5> 
||         MV      .M1     AM0,AL0           ; [A_M1] |270| <8,3> Split a long life
||         MPYWW   .N1     AM3,A1,D4         ; [A_N1] <9,1> 

;** --------------------------------------------------------------------------*
||$C$L259||:    ; PIPED LOOP EPILOG
;          EXCLUSIVE CPU CYCLES: 7
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 270,column 15,is_stmt,isa 0

           MV      .D1     D2,A12            ; [A_D1] 
||         MV      .M1     AM3,A4            ; [A_M1] 
|| [!A0]   MV      .L2     BL0,B0            ; [B_L2] <7,18>  ^ 
||         VMAXB   .S2     B0,BL2,BL1        ; [B_S2] |272| <7,18>  ^ 
||         VMINB   .M2     B0,BM0,BL0        ; [B_M2] |273| <7,18>  ^ 
||         MVDLY4  .S1     AL2,A0            ; [A_S1] |270| <9,14> Split a long life

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 272,column 17,is_stmt,isa 0

           MV      .M1     AM2,A11           ; [A_M1] 
|| [!A0]   EXT     .L2     BL1,0x38,0x38,BL2 ; [B_L2] |272| <7,19>  ^ [C1]
|| [!A0]   EXT     .S2     BL0,0x38,0x38,BM0 ; [B_S2] |273| <7,19>  ^ [C0]
||         MV      .M2     B0,BL0            ; [B_M2] <8,17>  ^ 

   [!A0]   MV      .L2     BL0,B0            ; [B_L2] <8,18>  ^ 
||         VMAXB   .S2     B0,BL2,BL1        ; [B_S2] |272| <8,18>  ^ 
||         VMINB   .M2     B0,BM0,BL0        ; [B_M2] |273| <8,18>  ^ 

   [!A0]   EXT     .L2     BL1,0x38,0x38,BL2 ; [B_L2] |272| <8,19>  ^ [C1]
|| [!A0]   EXT     .S2     BL0,0x38,0x38,BM0 ; [B_S2] |273| <8,19>  ^ [C0]
||         MV      .M2     B0,BL0            ; [B_M2] <9,17>  ^ 

   [!A0]   MV      .L2     BL0,B0            ; [B_L2] <9,18>  ^ 
||         VMAXB   .S2     B0,BL2,BL1        ; [B_S2] |272| <9,18>  ^ 
||         VMINB   .M2     B0,BM0,BL0        ; [B_M2] |273| <9,18>  ^ 

   [!A0]   EXT     .L2     BL1,0x38,0x38,BL2 ; [B_L2] |272| <9,19>  ^ [C1]
|| [!A0]   EXT     .S2     BL0,0x38,0x38,BM0 ; [B_S2] |273| <9,19>  ^ [C0]

           MV      .L2     BL2,B2            ; [B_L2] 
||         MV      .M2     BM0,B1            ; [B_M2] 
||         PROT                               ; [A_U] 

;** --------------------------------------------------------------------------*
||$C$L260||:    
;          EXCLUSIVE CPU CYCLES: 1
           MV      .L1X    B0,AL0            ; [A_L1] 
;** --------------------------------------------------------------------------*
||$C$L261||:    
;          EXCLUSIVE CPU CYCLES: 95
	.dwpsn	file "./inc/tidl_detectionOutput_int.h",line 134,column 25,is_stmt,isa 0

           VINTSP  .L2     B2,BM0            ; [B_L2] |134| 
||         VINTSP  .S2     B1,BM3            ; [B_S2] |283| 

	.dwpsn	file "./inc/tidl_detectionOutput_int.h",line 135,column 25,is_stmt,isa 0
           VINTSP  .L1     AL0,AM0           ; [A_L1] |135| 
	.dwpsn	file "./inc/tidl_detectionOutput_int.h",line 134,column 25,is_stmt,isa 0

           MPYSP   .N2     B5,BM0,BM0        ; [B_N2] |134| 
||         MPYSP   .M2     B5,BM3,BM3        ; [B_M2] |283| 

	.dwpsn	file "./inc/tidl_detectionOutput_int.h",line 135,column 25,is_stmt,isa 0
           MPYSP   .N1X    B5,AM0,A0         ; [A_N1] |135| 
           SUBSP   .C2     BM3,BM0,BM3       ; [B_C] |135| 
           SUBSP   .C2X    A0,BM0,BM0        ; [B_C] |135| 
           MPYSP   .N2     BM2,BM3,B2        ; [B_N2] |135| 
           MPYSP   .N2     BM2,BM0,B10       ; [B_N2] |135| 
           VSPTRUNC .L2    B2,B0             ; [B_L2] |135| 
           VSPTRUNC .L2    B10,B1            ; [B_L2] |135| 
	.dwpsn	file "./inc/tidl_detectionOutput_int.h",line 157,column 3,is_stmt,isa 0
           VINTSP  .L2     B0,BM0            ; [B_L2] |157| 

           SUBSP   .C2     B2,BM0,BM0        ; [B_C] |157| 
||         VINTSP  .L1X    B1,AL0            ; [A_L1] |157| 

           MPYSP   .N2     B8,BM0,BM3        ; [B_N2] |157| 
||         SUBSP   .L1X    B10,AL0,AM0       ; [A_L1] |157| 

           MPYSP   .N1X    B8,AM0,A0         ; [A_N1] |157| 
           MPYSP   .N2     BM3,BM3,BM0       ; [B_N2] |157| 
           MPYSP   .N1     A0,A0,AM0         ; [A_N1] |157| 
           MPYSP   .N2     BM3,BM0,BM5       ; [B_N2] |157| 
           MPYSP   .N2     B6,BM0,BM4        ; [B_N2] |157| 
           ADDSP   .C2     B4,BM3,BM7        ; [B_C] |157| 

           MPYSP   .N2     BM0,BM0,BM0       ; [B_N2] |157| 
||         MPYSP   .N1     A0,AM0,AM1        ; [A_N1] |157| 

           MPYSP   .N2     B7,BM5,BM5        ; [B_N2] |157| 
||         MPYSP   .N1X    B6,AM0,AL0        ; [A_N1] |157| 

           ADDSP   .C2     BM4,BM7,BM3       ; [B_C] |157| 
||         ADDSP   .L1X    B4,A0,AL1         ; [A_L1] |157| 
||         MASKB   .P2     0x4,P1            ; [B_P] |157| 

           MPYSP   .N1     AM0,AM0,AM0       ; [A_N1] |157| 
||         MVKU32  .L2     0,BL0             ; [B_L2] |157| 

           MPYSP   .N2     B9,BM0,BM0        ; [B_N2] |157| 
||         MPYSP   .N1X    B7,AM1,AL2        ; [A_N1] |157| 
||         SUBRW   .S2     B0,0,BL0          ; [B_S2] |157| 
||         VCMPGTW .L2     B0,BL0,P0         ; [B_L2] |157| 

           ADDSP   .C2     BM5,BM3,BM3       ; [B_C] |157| 
||         ADDSP   .L1     AL0,AL1,AL0       ; [A_L1] |157| 
||         SHRW    .L2     BL4,BL0,BL0       ; [B_L2] |157| 
||         AND     .P2     P0,P1,P0          ; [B_P] |157| 
||         SHLW    .S2     BL4,B0,BL1        ; [B_S2] |157| 

           VSEL    .L2     P0,BL1,BL0,BM6    ; [B_L2] |157| 

           MPYSP   .N1X    B9,AM0,AL1        ; [A_N1] |157| 
||         VINTSP  .C2     BM6,BM4           ; [B_C] |157| 
||         MASKB   .P2     0x4,P1            ; [B_P] |157| 

           ADDSP   .C2     BM0,BM3,BM0       ; [B_C] |157| 
||         ADDSP   .L1     AL2,AL0,AL0       ; [A_L1] |157| 

           VCMPGTW .L2     B1,BL7,P0         ; [B_L2] |157| 
||         SUBRW   .S2     B1,0,BL2          ; [B_S2] |157| 

           SHLW    .L2     BL4,B1,B2         ; [B_L2] |157| 
||         AND     .P2     P0,P1,P0          ; [B_P] |157| 
||         SHRW    .S2     BL4,BL2,BL0       ; [B_S2] |157| 

           MPYSP   .N2     BM0,BM4,BM0       ; [B_N2] |157| 
||         ADDSP   .L1     AL1,AL0,A0        ; [A_L1] |157| 
||         VSEL    .L2     P0,B2,BL0,BL0     ; [B_L2] |157| 

           VINTSP  .L2     BL0,B10           ; [B_L2] |157| 

           MPYSP   .N2     BM1,BM0,BM0       ; [B_N2] |157| 
||         MPYSP   .M2X    A0,B10,BM3        ; [B_M2] |157| 

	.dwpsn	file "src/tidl_detectionOutput_score.c",line 288,column 13,is_stmt,isa 0
           ADDW    .L2     B3,0xfffffffe,BL1 ; [B_L2] |288| 
	.dwpsn	file "./inc/tidl_detectionOutput_int.h",line 161,column 3,is_stmt,isa 0

           VINTSP  .S2     BL1,BM7           ; [B_S2] |288| 
||         CMPGEW  .L1X    B0,0xfffffff0,A3  ; [A_L1] |161| 

	.dwpsn	file "./inc/tidl_detectionOutput_int.h",line 157,column 3,is_stmt,isa 0

           MPYSP   .N2     BM1,BM3,BM3       ; [B_N2] |157| 
|| [!A3]   MVKU32  .L2     0,BM0             ; [B_L2] |162| 
||         CMPGTW  .L1X    B0,0xe,A1         ; [A_L1] |164| 

	.dwpsn	file "./inc/tidl_detectionOutput_int.h",line 165,column 5,is_stmt,isa 0
   [ A1]   MV      .L2     BL5,BM0           ; [B_L2] |165| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 288,column 13,is_stmt,isa 0
           MPYSP   .N2     BM0,BM7,BM0       ; [B_N2] |288| 
	.dwpsn	file "./inc/tidl_detectionOutput_int.h",line 161,column 3,is_stmt,isa 0
           CMPGEW  .L1X    B1,0xfffffff0,A3  ; [A_L1] |161| 
	.dwpsn	file "./inc/tidl_detectionOutput_int.h",line 162,column 5,is_stmt,isa 0

           CMPGTW  .L1X    B1,0xe,A1         ; [A_L1] |164| 
|| [!A3]   MVKU32  .L2     0,BM3             ; [B_L2] |162| 

	.dwpsn	file "./inc/tidl_detectionOutput_int.h",line 165,column 5,is_stmt,isa 0
   [ A1]   MV      .L2     BL5,BM3           ; [B_L2] |165| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 288,column 13,is_stmt,isa 0
           ADDSP   .C2     BM0,BM3,BL0       ; [B_C] |288| 
           CMPGTW  .L1     AL7,A9,A0         ; [A_L1] |288| 
           CMPLESP .L2     BL0,B14,BL0       ; [B_L2] |288| 
           ANDW    .L2X    A0,BL0,B0         ; [B_L2] |288| 
           CMPEQW  .L1X    B0,0,A0           ; [A_L1] |288| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 293,column 17,is_stmt,isa 0
   [!A0]   LDD     .D1     *A5(1184),D0      ; [A_D1] |293| 
           ADDW    .D1     A11,A12,D1        ; [A_D1] |293| 
           ORW     .D1     D9,D1,D1          ; [A_D1] |293| 
   [!A0]   STW     .D1     D1,*D0[A9]        ; [A_D1] |293| 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 294,column 17,is_stmt,isa 0
   [!A0]   LDD     .D1     *A5(1192),D0      ; [A_D1] |294| 
   [!A0]   LDW     .D1     *D0(0),BL0        ; [A_D1] |294| 
           MV      .L1X    B13,A1            ; [A_L1] 

           EXT     .L1     A1,0x20,0x20,AM7  ; [A_L1] 
||         MVKU32  .S1     0xf8,AM1          ; [A_S1] 

           MPYDD   .N1     AM1,AM7,D14       ; [A_N1] 
   [!A0]   ADDW    .L2     BL0,0x1,B0        ; [B_L2] |294| 

   [!A0]   STW     .D1X    B0,*D0(0)         ; [A_D1] |294| 
||         ADDD    .D2     D13,D14,D12       ; [A_D2] 

   [!A0]   LDW     .D1     *D12(8),A8        ; [A_D1] 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 252,column 34,is_stmt,isa 0
           ADDW    .D1     A11,0x1,A11       ; [A_D1] |252| 

           CMPGTW  .L1     A8,A11,A0         ; [A_L1] |252| 
|| [!A0]   ADDW    .D1     A9,0x1,A9         ; [A_D1] |295| 

   [ A0]   B       .B1     ||$C$L254||       ; [A_B] |252| 
           ; BRANCHCC OCCURS {||$C$L254||}   ; [] |252| 
;** --------------------------------------------------------------------------*
||$C$L262||:    
;          EXCLUSIVE CPU CYCLES: 14
           LDD     .D1     *SP(16),A0        ; [A_D1] 
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 250,column 32,is_stmt,isa 0
           LDW     .D1     *A0(12),AL0       ; [A_D1] |250| 
           ADDW    .D1     A10,0x1,A10       ; [A_D1] |250| 
           CMPGTW  .L1     AL0,A10,A0        ; [A_L1] |250| 
   [ A0]   B       .B1     ||$C$L253||       ; [A_B] |250| 
           ; BRANCHCC OCCURS {||$C$L253||}   ; [] |250| 
;** --------------------------------------------------------------------------*
||$C$L263||:    
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 246,column 20,is_stmt,isa 0
           ADDW    .D1     A2,0xffffffff,A2  ; [A_D1] |246| 

   [ A2]   B       .B1     ||$C$L252||       ; [A_B] |246| 
||         ADDW    .D1     A7,0x1,A7         ; [A_D1] |246| 

           ; BRANCHCC OCCURS {||$C$L252||}   ; [] |246| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 7
           LDD     .D1     *SP(48),A0        ; [A_D1] 
           LDW     .D1     *A0(56),AL0       ; [A_D1] 
;** --------------------------------------------------------------------------*
||$C$L264||:    
;          EXCLUSIVE CPU CYCLES: 9
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 229,column 17,is_stmt,isa 0
           LDD     .D1     *SP(16),A3        ; [A_D1] |229| 

           LDD     .D1     *SP(32),A1        ; [A_D1] |229| 
||         LDD     .D2     *SP(24),A2        ; [A_D2] |229| 

           MV      .L1X    B13,A0            ; [A_L1] 
           ADDW    .D1     A0,0x1,A0         ; [A_D1] |229| 

           ADDD    .D2     A3,0xf8,A3        ; [A_D2] |229| 
||         ADDD    .D1     D7,0x4,D7         ; [A_D1] |229| 

           STD     .D1     A3,*SP(16)        ; [A_D1] |229| 
||         ADDD    .S1     A1,0x1,A1         ; [A_S1] |229| 
||         ADDD    .M1     A2,0xc,A2         ; [A_M1] |229| 
||         MV      .L2X    A0,B13            ; [B_L2] |229| 
||         CMPGTW  .L1     AL0,A0,A4         ; [A_L1] |229| 
||         ADDD    .D2     D8,0xc,D8         ; [A_D2] |229| 

   [ A4]   B       .B1     ||$C$L249||       ; [A_B] |229| 
||         STD     .D1     A2,*SP(24)        ; [A_D1] |229| 
||         STD     .D2X    A1,*SP(32)        ; [A_D2] |229| 

           ; BRANCHCC OCCURS {||$C$L249||}   ; [] |229| 
;** --------------------------------------------------------------------------*
||$C$L265||:    
;          EXCLUSIVE CPU CYCLES: 16

           LDD     .D2     *SP(184),A13      ; [A_D2] 
||         VLD64B  .D1     *SP(56),VB14      ; [A_D1] 

	.dwcfi	restore_reg, 62
           VLD64B  .D1     *SP(120),VB15     ; [A_D1] 
	.dwcfi	restore_reg, 63
	.dwpsn	file "src/tidl_detectionOutput_score.c",line 304,column 3,is_stmt,isa 0

           MVC     .S1     A13,RP            ; [A_S1] 
||         MV      .D1     A9,A4             ; [A_D1] |304| 
	.dwcfi	restore_reg, 4101

           LDD     .D1     *SP(200),A14      ; [A_D1] 
||         LDD     .D2     *SP(192),A15      ; [A_D2] 
	.dwcfi	restore_reg, 14
	.dwcfi	restore_reg, 15

           LDD     .D1     *SP(224),A11      ; [A_D1] 
||         LDD     .D2     *SP(216),A12      ; [A_D2] 
	.dwcfi	restore_reg, 11
	.dwcfi	restore_reg, 12

           LDD     .D1     *SP(248),A8       ; [A_D1] 
||         LDD     .D2     *SP(232),A10      ; [A_D2] 
	.dwcfi	restore_reg, 8
	.dwcfi	restore_reg, 10

           LDD     .D1     *SP(208),A13      ; [A_D1] 
||         LDD     .D2     *SP(240),A9       ; [A_D2] 

	.dwcfi	restore_reg, 13
	.dwcfi	restore_reg, 9
$C$DW$69	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$69, DW_AT_low_pc(0x00)
	.dwattr $C$DW$69, DW_AT_TI_return


           RET     .B1     ; [A_B] 
||         ADDD    .D1     SP,0xf0,SP        ; [A_D1] 

	.dwcfi	cfa_offset, 0
           ; RETURN OCCURS {RP}              ; [] 
	.dwattr $C$DW$61, DW_AT_TI_end_file("src/tidl_detectionOutput_score.c")
	.dwattr $C$DW$61, DW_AT_TI_end_line(0x131)
	.dwattr $C$DW$61, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$61

;*****************************************************************************
;* UNDEFINED EXTERNAL REFERENCES                                             *
;*****************************************************************************
	.global	||_ZTIa||
	.global	||_ZTIf||
	.global	||_ZTIs||
	.global	||logf||
	.global	||__c7xabi_divf||
;*****************************************************************************
;* SECTION GROUPS                                                            *
;*****************************************************************************
	.group    "_Z25TIDL_findValidLocation_cnIaEiP26sTIDL_DetectOutputParams_tP29sTIDL_ALgDetectOutputParams_tPf", 1
	.gmember  ".text:_Z25TIDL_findValidLocation_cnIaEiP26sTIDL_DetectOutputParams_tP29sTIDL_ALgDetectOutputParams_tPf"
	.endgroup
	.group    "_Z25TIDL_findValidLocation_cnIfEiP26sTIDL_DetectOutputParams_tP29sTIDL_ALgDetectOutputParams_tPf", 1
	.gmember  ".text:_Z25TIDL_findValidLocation_cnIfEiP26sTIDL_DetectOutputParams_tP29sTIDL_ALgDetectOutputParams_tPf"
	.endgroup
	.group    "_Z25TIDL_findValidLocation_cnIsEiP26sTIDL_DetectOutputParams_tP29sTIDL_ALgDetectOutputParams_tPf", 1
	.gmember  ".text:_Z25TIDL_findValidLocation_cnIsEiP26sTIDL_DetectOutputParams_tP29sTIDL_ALgDetectOutputParams_tPf"
	.endgroup
	.group    "_Z26TIDL_sparseDetScoreCalc_cnIaEiP26sTIDL_DetectOutputParams_tP29sTIDL_ALgDetectOutputParams_t", 1
	.gmember  ".text:_Z26TIDL_sparseDetScoreCalc_cnIaEiP26sTIDL_DetectOutputParams_tP29sTIDL_ALgDetectOutputParams_t"
	.endgroup
	.group    "_Z26TIDL_sparseDetScoreCalc_cnIfEiP26sTIDL_DetectOutputParams_tP29sTIDL_ALgDetectOutputParams_t", 1
	.gmember  ".text:_Z26TIDL_sparseDetScoreCalc_cnIfEiP26sTIDL_DetectOutputParams_tP29sTIDL_ALgDetectOutputParams_t"
	.endgroup
	.group    "_Z26TIDL_sparseDetScoreCalc_cnIsEiP26sTIDL_DetectOutputParams_tP29sTIDL_ALgDetectOutputParams_t", 1
	.gmember  ".text:_Z26TIDL_sparseDetScoreCalc_cnIsEiP26sTIDL_DetectOutputParams_tP29sTIDL_ALgDetectOutputParams_t"
	.endgroup

	.dwattr $C$DW$CU, DW_AT_language(DW_LANG_C_plus_plus)

;***************************************************************
;* DWARF CIE ENTRIES                                           *
;***************************************************************

$C$DW$CIE	.dwcie 4101
	.dwcfi	cfa_register, 111
	.dwcfi	cfa_offset, 0
	.dwcfi	same_value, 8
	.dwcfi	same_value, 9
	.dwcfi	same_value, 10
	.dwcfi	same_value, 11
	.dwcfi	same_value, 12
	.dwcfi	same_value, 13
	.dwcfi	same_value, 14
	.dwcfi	same_value, 15
	.dwcfi	same_value, 62
	.dwcfi	same_value, 63
	.dwendentry
	.dwendtag $C$DW$CU


;******************************************************************************
;* TYPE INFORMATION                                                           *
;******************************************************************************

$C$DW$TU$31	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$31

$C$DW$T$31	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$31, DW_AT_name("BBox")
	.dwattr $C$DW$T$31, DW_AT_byte_size(0x10)
$C$DW$70	.dwtag  DW_TAG_member
	.dwattr $C$DW$70, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$70, DW_AT_name("xmin")
	.dwattr $C$DW$70, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$70, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$70, DW_AT_decl_line(0x134)
	.dwattr $C$DW$70, DW_AT_decl_column(0x10)

$C$DW$71	.dwtag  DW_TAG_member
	.dwattr $C$DW$71, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$71, DW_AT_name("ymin")
	.dwattr $C$DW$71, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$71, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$71, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$71, DW_AT_decl_line(0x135)
	.dwattr $C$DW$71, DW_AT_decl_column(0x10)

$C$DW$72	.dwtag  DW_TAG_member
	.dwattr $C$DW$72, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$72, DW_AT_name("xmax")
	.dwattr $C$DW$72, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$72, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$72, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$72, DW_AT_decl_line(0x136)
	.dwattr $C$DW$72, DW_AT_decl_column(0x10)

$C$DW$73	.dwtag  DW_TAG_member
	.dwattr $C$DW$73, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$73, DW_AT_name("ymax")
	.dwattr $C$DW$73, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$73, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$73, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$73, DW_AT_decl_line(0x137)
	.dwattr $C$DW$73, DW_AT_decl_column(0x10)


$C$DW$74	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$74, DW_AT_name("operator =")
	.dwattr $C$DW$74, DW_AT_declaration
	.dwattr $C$DW$74, DW_AT_linkage_name("_ZN4BBoxaSERKS_")
	.dwattr $C$DW$74, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$74, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$75	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$75, DW_AT_type(*$C$DW$T$28)

	.dwendtag $C$DW$74


$C$DW$76	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$76, DW_AT_name("operator =")
	.dwattr $C$DW$76, DW_AT_declaration
	.dwattr $C$DW$76, DW_AT_linkage_name("_ZN4BBoxaSEOS_")
	.dwattr $C$DW$76, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$76, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$77	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$77, DW_AT_type(*$C$DW$T$26)

	.dwendtag $C$DW$76

	.dwattr $C$DW$T$31, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$T$31, DW_AT_decl_line(0x133)
	.dwattr $C$DW$T$31, DW_AT_decl_column(0x01)
	.dwendtag $C$DW$T$31

	.dwendtag $C$DW$TU$31


$C$DW$TU$27	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$27
$C$DW$T$27	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$27, DW_AT_type(*$C$DW$T$31)

	.dwendtag $C$DW$TU$27


$C$DW$TU$28	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$28
$C$DW$T$28	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$28, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$T$28, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$28


$C$DW$TU$77	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$77
$C$DW$T$77	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$77, DW_AT_name("BBox")
	.dwattr $C$DW$T$77, DW_AT_type(*$C$DW$T$31)
	.dwattr $C$DW$T$77, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$T$77, DW_AT_decl_line(0x138)
	.dwattr $C$DW$T$77, DW_AT_decl_column(0x02)

	.dwendtag $C$DW$TU$77


$C$DW$TU$78	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$78
$C$DW$T$78	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$78, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$T$78, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$78


$C$DW$TU$26	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$26
$C$DW$T$26	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$26, DW_AT_type(*$C$DW$T$31)
	.dwattr $C$DW$T$26, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$26


$C$DW$TU$29	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$29

$C$DW$T$29	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$29, DW_AT_type(*$C$DW$T$26)
$C$DW$78	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$78, DW_AT_type(*$C$DW$T$28)

	.dwendtag $C$DW$T$29

	.dwendtag $C$DW$TU$29


$C$DW$TU$30	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$30

$C$DW$T$30	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$30, DW_AT_type(*$C$DW$T$26)
$C$DW$79	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$79, DW_AT_type(*$C$DW$T$26)

	.dwendtag $C$DW$T$30

	.dwendtag $C$DW$TU$30


$C$DW$TU$2	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$2
$C$DW$T$2	.dwtag  DW_TAG_unspecified_type
	.dwattr $C$DW$T$2, DW_AT_name("void")

	.dwendtag $C$DW$TU$2


$C$DW$TU$3	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$3
$C$DW$T$3	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$3, DW_AT_type(*$C$DW$T$2)
	.dwattr $C$DW$T$3, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$3


$C$DW$TU$63	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$63

$C$DW$T$63	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$63, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$T$63, DW_AT_byte_size(0x80)
$C$DW$80	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$80, DW_AT_upper_bound(0x0f)

	.dwendtag $C$DW$T$63

	.dwendtag $C$DW$TU$63


$C$DW$TU$43	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$43

$C$DW$T$43	.dwtag  DW_TAG_subroutine_type
$C$DW$81	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$81, DW_AT_type(*$C$DW$T$41)

	.dwendtag $C$DW$T$43

	.dwendtag $C$DW$TU$43


$C$DW$TU$44	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$44

$C$DW$T$44	.dwtag  DW_TAG_subroutine_type
$C$DW$82	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$82, DW_AT_type(*$C$DW$T$38)

	.dwendtag $C$DW$T$44

	.dwendtag $C$DW$TU$44


$C$DW$TU$45	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$45

$C$DW$T$45	.dwtag  DW_TAG_subroutine_type
	.dwendtag $C$DW$T$45

	.dwendtag $C$DW$TU$45


$C$DW$TU$4	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$4
$C$DW$T$4	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$4, DW_AT_encoding(DW_ATE_boolean)
	.dwattr $C$DW$T$4, DW_AT_name("bool")
	.dwattr $C$DW$T$4, DW_AT_byte_size(0x01)

	.dwendtag $C$DW$TU$4


$C$DW$TU$47	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$47

$C$DW$T$47	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$47, DW_AT_type(*$C$DW$T$4)
$C$DW$83	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$83, DW_AT_type(*$C$DW$T$41)

	.dwendtag $C$DW$T$47

	.dwendtag $C$DW$TU$47


$C$DW$TU$5	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$5
$C$DW$T$5	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$5, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$5, DW_AT_name("signed char")
	.dwattr $C$DW$T$5, DW_AT_byte_size(0x01)

	.dwendtag $C$DW$TU$5


$C$DW$TU$128	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$128
$C$DW$T$128	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$128, DW_AT_name("__int8_t")
	.dwattr $C$DW$T$128, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$T$128, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/machine/_types.h")
	.dwattr $C$DW$T$128, DW_AT_decl_line(0x60)
	.dwattr $C$DW$T$128, DW_AT_decl_column(0x16)

	.dwendtag $C$DW$TU$128


$C$DW$TU$129	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$129
$C$DW$T$129	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$129, DW_AT_name("int8_t")
	.dwattr $C$DW$T$129, DW_AT_type(*$C$DW$T$128)
	.dwattr $C$DW$T$129, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/sys/_stdint.h")
	.dwattr $C$DW$T$129, DW_AT_decl_line(0x25)
	.dwattr $C$DW$T$129, DW_AT_decl_column(0x13)

	.dwendtag $C$DW$TU$129


$C$DW$TU$6	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$6
$C$DW$T$6	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$6, DW_AT_encoding(DW_ATE_unsigned_char)
	.dwattr $C$DW$T$6, DW_AT_name("unsigned char")
	.dwattr $C$DW$T$6, DW_AT_byte_size(0x01)

	.dwendtag $C$DW$TU$6


$C$DW$TU$7	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$7
$C$DW$T$7	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$7, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$7, DW_AT_name("wchar_t")
	.dwattr $C$DW$T$7, DW_AT_byte_size(0x04)

	.dwendtag $C$DW$TU$7


$C$DW$TU$8	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$8
$C$DW$T$8	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$8, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$8, DW_AT_name("short")
	.dwattr $C$DW$T$8, DW_AT_byte_size(0x02)

	.dwendtag $C$DW$TU$8


$C$DW$TU$131	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$131
$C$DW$T$131	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$131, DW_AT_name("__int16_t")
	.dwattr $C$DW$T$131, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$T$131, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/machine/_types.h")
	.dwattr $C$DW$T$131, DW_AT_decl_line(0x62)
	.dwattr $C$DW$T$131, DW_AT_decl_column(0x11)

	.dwendtag $C$DW$TU$131


$C$DW$TU$132	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$132
$C$DW$T$132	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$132, DW_AT_name("int16_t")
	.dwattr $C$DW$T$132, DW_AT_type(*$C$DW$T$131)
	.dwattr $C$DW$T$132, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/sys/_stdint.h")
	.dwattr $C$DW$T$132, DW_AT_decl_line(0x2b)
	.dwattr $C$DW$T$132, DW_AT_decl_column(0x14)

	.dwendtag $C$DW$TU$132


$C$DW$TU$9	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$9
$C$DW$T$9	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$9, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$9, DW_AT_name("unsigned short")
	.dwattr $C$DW$T$9, DW_AT_byte_size(0x02)

	.dwendtag $C$DW$TU$9


$C$DW$TU$72	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$72
$C$DW$T$72	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$72, DW_AT_name("__uint16_t")
	.dwattr $C$DW$T$72, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$T$72, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/machine/_types.h")
	.dwattr $C$DW$T$72, DW_AT_decl_line(0x63)
	.dwattr $C$DW$T$72, DW_AT_decl_column(0x19)

	.dwendtag $C$DW$TU$72


$C$DW$TU$73	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$73
$C$DW$T$73	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$73, DW_AT_name("uint16_t")
	.dwattr $C$DW$T$73, DW_AT_type(*$C$DW$T$72)
	.dwattr $C$DW$T$73, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/sys/_stdint.h")
	.dwattr $C$DW$T$73, DW_AT_decl_line(0x41)
	.dwattr $C$DW$T$73, DW_AT_decl_column(0x15)

	.dwendtag $C$DW$TU$73


$C$DW$TU$74	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$74
$C$DW$T$74	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$74, DW_AT_type(*$C$DW$T$73)
	.dwattr $C$DW$T$74, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$74


$C$DW$TU$10	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$10
$C$DW$T$10	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$10, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$10, DW_AT_name("int")
	.dwattr $C$DW$T$10, DW_AT_byte_size(0x04)

	.dwendtag $C$DW$TU$10


$C$DW$TU$65	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$65
$C$DW$T$65	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$65, DW_AT_name("__int32_t")
	.dwattr $C$DW$T$65, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$T$65, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/machine/_types.h")
	.dwattr $C$DW$T$65, DW_AT_decl_line(0x64)
	.dwattr $C$DW$T$65, DW_AT_decl_column(0x0f)

	.dwendtag $C$DW$TU$65


$C$DW$TU$66	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$66
$C$DW$T$66	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$66, DW_AT_name("int32_t")
	.dwattr $C$DW$T$66, DW_AT_type(*$C$DW$T$65)
	.dwattr $C$DW$T$66, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/sys/_stdint.h")
	.dwattr $C$DW$T$66, DW_AT_decl_line(0x30)
	.dwattr $C$DW$T$66, DW_AT_decl_column(0x14)

	.dwendtag $C$DW$TU$66


$C$DW$TU$67	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$67

$C$DW$T$67	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$67, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$T$67, DW_AT_byte_size(0x0c)
$C$DW$84	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$84, DW_AT_upper_bound(0x02)

	.dwendtag $C$DW$T$67

	.dwendtag $C$DW$TU$67


$C$DW$TU$68	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$68

$C$DW$T$68	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$68, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$T$68, DW_AT_byte_size(0xc0)
$C$DW$85	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$85, DW_AT_upper_bound(0x0f)

$C$DW$86	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$86, DW_AT_upper_bound(0x02)

	.dwendtag $C$DW$T$68

	.dwendtag $C$DW$TU$68


$C$DW$TU$71	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$71

$C$DW$T$71	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$71, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$T$71, DW_AT_byte_size(0x40)
$C$DW$87	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$87, DW_AT_upper_bound(0x0f)

	.dwendtag $C$DW$T$71

	.dwendtag $C$DW$TU$71


$C$DW$TU$76	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$76
$C$DW$T$76	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$76, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$T$76, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$76


$C$DW$TU$11	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$11
$C$DW$T$11	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$11, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$11, DW_AT_name("unsigned int")
	.dwattr $C$DW$T$11, DW_AT_byte_size(0x04)

	.dwendtag $C$DW$TU$11


$C$DW$TU$12	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$12
$C$DW$T$12	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$12, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$12, DW_AT_name("__int40_t")
	.dwattr $C$DW$T$12, DW_AT_byte_size(0x08)

	.dwendtag $C$DW$TU$12


$C$DW$TU$34	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$34
$C$DW$T$34	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$34, DW_AT_type(*$C$DW$T$12)

	.dwendtag $C$DW$TU$34


$C$DW$TU$35	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$35
$C$DW$T$35	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$35, DW_AT_type(*$C$DW$T$34)
	.dwattr $C$DW$T$35, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$35


$C$DW$TU$13	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$13
$C$DW$T$13	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$13, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$13, DW_AT_name("unsigned __int40_t")
	.dwattr $C$DW$T$13, DW_AT_byte_size(0x08)

	.dwendtag $C$DW$TU$13


$C$DW$TU$56	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$56

$C$DW$T$56	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$56, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$56, DW_AT_byte_size(0x08)
$C$DW$88	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$88, DW_AT_upper_bound(0x00)

	.dwendtag $C$DW$T$56

	.dwendtag $C$DW$TU$56


$C$DW$TU$48	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$48
$C$DW$T$48	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$48, DW_AT_name("size_t")
	.dwattr $C$DW$T$48, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$48, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/stdio.h")
	.dwattr $C$DW$T$48, DW_AT_decl_line(0x4d)
	.dwattr $C$DW$T$48, DW_AT_decl_column(0x19)

	.dwendtag $C$DW$TU$48


$C$DW$TU$49	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$49

$C$DW$T$49	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$49, DW_AT_type(*$C$DW$T$48)
	.dwendtag $C$DW$T$49

	.dwendtag $C$DW$TU$49


$C$DW$TU$14	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$14
$C$DW$T$14	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$14, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$14, DW_AT_name("long long")
	.dwattr $C$DW$T$14, DW_AT_byte_size(0x08)

	.dwendtag $C$DW$TU$14


$C$DW$TU$79	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$79
$C$DW$T$79	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$79, DW_AT_type(*$C$DW$T$14)
	.dwattr $C$DW$T$79, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$79


$C$DW$TU$15	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$15
$C$DW$T$15	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$15, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$15, DW_AT_name("unsigned long long")
	.dwattr $C$DW$T$15, DW_AT_byte_size(0x08)

	.dwendtag $C$DW$TU$15


$C$DW$TU$53	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$53

$C$DW$T$53	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$53, DW_AT_type(*$C$DW$T$15)
	.dwattr $C$DW$T$53, DW_AT_byte_size(0x08)
$C$DW$89	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$89, DW_AT_upper_bound(0x00)

	.dwendtag $C$DW$T$53

	.dwendtag $C$DW$TU$53


$C$DW$TU$16	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$16
$C$DW$T$16	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$16, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$16, DW_AT_name("float")
	.dwattr $C$DW$T$16, DW_AT_byte_size(0x04)

	.dwendtag $C$DW$TU$16


$C$DW$TU$75	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$75
$C$DW$T$75	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$75, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$T$75, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$75


$C$DW$TU$25	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$25
$C$DW$T$25	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$25, DW_AT_name("float32_tidl")
	.dwattr $C$DW$T$25, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$T$25, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$25, DW_AT_decl_line(0x71)
	.dwattr $C$DW$T$25, DW_AT_decl_column(0x0f)

	.dwendtag $C$DW$TU$25


$C$DW$TU$64	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$64

$C$DW$T$64	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$64, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$T$64, DW_AT_byte_size(0x40)
$C$DW$90	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$90, DW_AT_upper_bound(0x0f)

	.dwendtag $C$DW$T$64

	.dwendtag $C$DW$TU$64


$C$DW$TU$69	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$69

$C$DW$T$69	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$69, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$T$69, DW_AT_byte_size(0x0c)
$C$DW$91	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$91, DW_AT_upper_bound(0x02)

	.dwendtag $C$DW$T$69

	.dwendtag $C$DW$TU$69


$C$DW$TU$70	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$70

$C$DW$T$70	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$70, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$T$70, DW_AT_byte_size(0xc0)
$C$DW$92	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$92, DW_AT_upper_bound(0x0f)

$C$DW$93	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$93, DW_AT_upper_bound(0x02)

	.dwendtag $C$DW$T$70

	.dwendtag $C$DW$TU$70


$C$DW$TU$101	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$101

$C$DW$T$101	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$101, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$T$101, DW_AT_byte_size(0x10)
$C$DW$94	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$94, DW_AT_upper_bound(0x03)

	.dwendtag $C$DW$T$101

	.dwendtag $C$DW$TU$101


$C$DW$TU$102	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$102
$C$DW$T$102	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$102, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$T$102, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$102


$C$DW$TU$17	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$17
$C$DW$T$17	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$17, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$17, DW_AT_name("double")
	.dwattr $C$DW$T$17, DW_AT_byte_size(0x08)

	.dwendtag $C$DW$TU$17


$C$DW$TU$18	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$18
$C$DW$T$18	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$18, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$18, DW_AT_name("long double")
	.dwattr $C$DW$T$18, DW_AT_byte_size(0x08)

	.dwendtag $C$DW$TU$18


$C$DW$TU$19	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$19
$C$DW$T$19	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$19, DW_AT_encoding(DW_ATE_TI_complex_signed)
	.dwattr $C$DW$T$19, DW_AT_name("cchar")
	.dwattr $C$DW$T$19, DW_AT_byte_size(0x02)

	.dwendtag $C$DW$TU$19


$C$DW$TU$20	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$20
$C$DW$T$20	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$20, DW_AT_encoding(DW_ATE_TI_complex_signed)
	.dwattr $C$DW$T$20, DW_AT_name("cshort")
	.dwattr $C$DW$T$20, DW_AT_byte_size(0x04)

	.dwendtag $C$DW$TU$20


$C$DW$TU$21	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$21
$C$DW$T$21	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$21, DW_AT_encoding(DW_ATE_TI_complex_signed)
	.dwattr $C$DW$T$21, DW_AT_name("cint")
	.dwattr $C$DW$T$21, DW_AT_byte_size(0x08)

	.dwendtag $C$DW$TU$21


$C$DW$TU$22	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$22
$C$DW$T$22	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$22, DW_AT_encoding(DW_ATE_TI_complex_signed)
	.dwattr $C$DW$T$22, DW_AT_name("clong")
	.dwattr $C$DW$T$22, DW_AT_byte_size(0x10)

	.dwendtag $C$DW$TU$22


$C$DW$TU$23	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$23
$C$DW$T$23	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$23, DW_AT_encoding(DW_ATE_TI_complex_float)
	.dwattr $C$DW$T$23, DW_AT_name("cfloat")
	.dwattr $C$DW$T$23, DW_AT_byte_size(0x08)

	.dwendtag $C$DW$TU$23


$C$DW$TU$24	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$24
$C$DW$T$24	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$24, DW_AT_encoding(DW_ATE_TI_complex_float)
	.dwattr $C$DW$T$24, DW_AT_name("cdouble")
	.dwattr $C$DW$T$24, DW_AT_byte_size(0x10)

	.dwendtag $C$DW$TU$24


$C$DW$TU$37	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$37
$C$DW$T$37	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$37, DW_AT_type(*$C$DW$T$5)

	.dwendtag $C$DW$TU$37


$C$DW$TU$38	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$38
$C$DW$T$38	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$38, DW_AT_type(*$C$DW$T$37)
	.dwattr $C$DW$T$38, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$38


$C$DW$TU$46	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$46

$C$DW$T$46	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$46, DW_AT_type(*$C$DW$T$38)
	.dwendtag $C$DW$T$46

	.dwendtag $C$DW$TU$46


$C$DW$TU$160	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$160
$C$DW$T$160	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$160, DW_AT_encoding(DW_ATE_unsigned_char)
	.dwattr $C$DW$T$160, DW_AT_name("unsigned char")
	.dwattr $C$DW$T$160, DW_AT_byte_size(0x01)

	.dwendtag $C$DW$TU$160


$C$DW$TU$52	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$52

$C$DW$T$52	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$52, DW_AT_name("__fundamental_type_info")
	.dwattr $C$DW$T$52, DW_AT_byte_size(0x10)
$C$DW$95	.dwtag  DW_TAG_member
	.dwattr $C$DW$95, DW_AT_type(*$C$DW$T$55)
	.dwattr $C$DW$95, DW_AT_name("user_type_info")
	.dwattr $C$DW$95, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$95, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/libcxx/typeinfo")
	.dwattr $C$DW$95, DW_AT_decl_line(0xd0)
	.dwattr $C$DW$95, DW_AT_decl_column(0x0a)

	.dwendtag $C$DW$T$52

	.dwendtag $C$DW$TU$52


$C$DW$TU$162	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$162
$C$DW$T$162	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$162, DW_AT_type(*$C$DW$T$52)

	.dwendtag $C$DW$TU$162


$C$DW$TU$54	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$54

$C$DW$T$54	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$54, DW_AT_name("__ti_vpred_tp")
	.dwattr $C$DW$T$54, DW_AT_byte_size(0x08)
$C$DW$96	.dwtag  DW_TAG_member
	.dwattr $C$DW$96, DW_AT_type(*$C$DW$T$53)
	.dwattr $C$DW$96, DW_AT_name("__vpred_field")
	.dwattr $C$DW$96, DW_AT_accessibility(DW_ACCESS_private)

	.dwendtag $C$DW$T$54

	.dwendtag $C$DW$TU$54


$C$DW$TU$183	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$183

$C$DW$T$183	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$183, DW_AT_name("__ti_vpred_tp")
	.dwattr $C$DW$T$183, DW_AT_byte_size(0x08)
$C$DW$97	.dwtag  DW_TAG_member
	.dwattr $C$DW$97, DW_AT_type(*$C$DW$T$53)
	.dwattr $C$DW$97, DW_AT_name("__vpred_field")
	.dwattr $C$DW$97, DW_AT_accessibility(DW_ACCESS_private)

	.dwendtag $C$DW$T$183

	.dwendtag $C$DW$TU$183


$C$DW$TU$55	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$55

$C$DW$T$55	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$55, DW_AT_name("__type_info")
	.dwattr $C$DW$T$55, DW_AT_byte_size(0x10)
$C$DW$98	.dwtag  DW_TAG_member
	.dwattr $C$DW$98, DW_AT_type(*$C$DW$T$35)
	.dwattr $C$DW$98, DW_AT_name("__vptr")
	.dwattr $C$DW$98, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$98, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/libcxx/typeinfo")
	.dwattr $C$DW$98, DW_AT_decl_line(0xd0)
	.dwattr $C$DW$98, DW_AT_decl_column(0x0a)

$C$DW$99	.dwtag  DW_TAG_member
	.dwattr $C$DW$99, DW_AT_type(*$C$DW$T$38)
	.dwattr $C$DW$99, DW_AT_name("__name")
	.dwattr $C$DW$99, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$99, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$99, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/libcxx/typeinfo")
	.dwattr $C$DW$99, DW_AT_decl_line(0xd0)
	.dwattr $C$DW$99, DW_AT_decl_column(0x0a)

	.dwendtag $C$DW$T$55

	.dwendtag $C$DW$TU$55


$C$DW$TU$62	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$62

$C$DW$T$62	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$62, DW_AT_name("__vpred_tp")
	.dwattr $C$DW$T$62, DW_AT_byte_size(0x08)
$C$DW$100	.dwtag  DW_TAG_member
	.dwattr $C$DW$100, DW_AT_type(*$C$DW$T$56)
	.dwattr $C$DW$100, DW_AT_name("_v")
	.dwattr $C$DW$100, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$100, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$100, DW_AT_decl_line(0x73)
	.dwattr $C$DW$100, DW_AT_decl_column(0x05)


$C$DW$101	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$101, DW_AT_name("operator =")
	.dwattr $C$DW$101, DW_AT_declaration
	.dwattr $C$DW$101, DW_AT_linkage_name("_ZN10__vpred_tpaSERKS_")
	.dwattr $C$DW$101, DW_AT_type(*$C$DW$T$57)
	.dwattr $C$DW$101, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$102	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$102, DW_AT_type(*$C$DW$T$59)

	.dwendtag $C$DW$101


$C$DW$103	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$103, DW_AT_name("operator =")
	.dwattr $C$DW$103, DW_AT_declaration
	.dwattr $C$DW$103, DW_AT_linkage_name("_ZN10__vpred_tpaSEOS_")
	.dwattr $C$DW$103, DW_AT_type(*$C$DW$T$57)
	.dwattr $C$DW$103, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$104	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$104, DW_AT_type(*$C$DW$T$57)

	.dwendtag $C$DW$103

	.dwendtag $C$DW$T$62

	.dwendtag $C$DW$TU$62


$C$DW$TU$57	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$57
$C$DW$T$57	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$57, DW_AT_type(*$C$DW$T$62)
	.dwattr $C$DW$T$57, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$57


$C$DW$TU$60	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$60

$C$DW$T$60	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$60, DW_AT_type(*$C$DW$T$57)
$C$DW$105	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$105, DW_AT_type(*$C$DW$T$59)

	.dwendtag $C$DW$T$60

	.dwendtag $C$DW$TU$60


$C$DW$TU$61	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$61

$C$DW$T$61	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$61, DW_AT_type(*$C$DW$T$57)
$C$DW$106	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$106, DW_AT_type(*$C$DW$T$57)

	.dwendtag $C$DW$T$61

	.dwendtag $C$DW$TU$61


$C$DW$TU$164	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$164
$C$DW$T$164	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$164, DW_AT_name("__vpred")
	.dwattr $C$DW$T$164, DW_AT_type(*$C$DW$T$62)
	.dwattr $C$DW$T$164, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$T$164, DW_AT_decl_line(0x73)
	.dwattr $C$DW$T$164, DW_AT_decl_column(0x05)

	.dwendtag $C$DW$TU$164


$C$DW$TU$58	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$58
$C$DW$T$58	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$58, DW_AT_type(*$C$DW$T$62)

	.dwendtag $C$DW$TU$58


$C$DW$TU$59	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$59
$C$DW$T$59	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$59, DW_AT_type(*$C$DW$T$58)
	.dwattr $C$DW$T$59, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$59


$C$DW$TU$85	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$85

$C$DW$T$85	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$85, DW_AT_name("sTIDL_ALgDetectOutputParams_t")
	.dwattr $C$DW$T$85, DW_AT_byte_size(0x538)
$C$DW$107	.dwtag  DW_TAG_member
	.dwattr $C$DW$107, DW_AT_type(*$C$DW$T$63)
	.dwattr $C$DW$107, DW_AT_name("inLocDataList")
	.dwattr $C$DW$107, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$107, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$107, DW_AT_decl_line(0x25d)
	.dwattr $C$DW$107, DW_AT_decl_column(0x1d)

$C$DW$108	.dwtag  DW_TAG_member
	.dwattr $C$DW$108, DW_AT_type(*$C$DW$T$64)
	.dwattr $C$DW$108, DW_AT_name("inLocdataQList")
	.dwattr $C$DW$108, DW_AT_data_member_location[DW_OP_plus_uconst 0x80]
	.dwattr $C$DW$108, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$108, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$108, DW_AT_decl_line(0x25e)
	.dwattr $C$DW$108, DW_AT_decl_column(0x1c)

$C$DW$109	.dwtag  DW_TAG_member
	.dwattr $C$DW$109, DW_AT_type(*$C$DW$T$63)
	.dwattr $C$DW$109, DW_AT_name("inConfDataList")
	.dwattr $C$DW$109, DW_AT_data_member_location[DW_OP_plus_uconst 0xc0]
	.dwattr $C$DW$109, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$109, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$109, DW_AT_decl_line(0x25f)
	.dwattr $C$DW$109, DW_AT_decl_column(0x1d)

$C$DW$110	.dwtag  DW_TAG_member
	.dwattr $C$DW$110, DW_AT_type(*$C$DW$T$64)
	.dwattr $C$DW$110, DW_AT_name("inConfdataQList")
	.dwattr $C$DW$110, DW_AT_data_member_location[DW_OP_plus_uconst 0x140]
	.dwattr $C$DW$110, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$110, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$110, DW_AT_decl_line(0x260)
	.dwattr $C$DW$110, DW_AT_decl_column(0x1c)

$C$DW$111	.dwtag  DW_TAG_member
	.dwattr $C$DW$111, DW_AT_type(*$C$DW$T$68)
	.dwattr $C$DW$111, DW_AT_name("locHeadPitchList")
	.dwattr $C$DW$111, DW_AT_data_member_location[DW_OP_plus_uconst 0x180]
	.dwattr $C$DW$111, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$111, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$111, DW_AT_decl_line(0x261)
	.dwattr $C$DW$111, DW_AT_decl_column(0x1c)

$C$DW$112	.dwtag  DW_TAG_member
	.dwattr $C$DW$112, DW_AT_type(*$C$DW$T$68)
	.dwattr $C$DW$112, DW_AT_name("confHeadPitchList")
	.dwattr $C$DW$112, DW_AT_data_member_location[DW_OP_plus_uconst 0x240]
	.dwattr $C$DW$112, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$112, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$112, DW_AT_decl_line(0x262)
	.dwattr $C$DW$112, DW_AT_decl_column(0x1c)

$C$DW$113	.dwtag  DW_TAG_member
	.dwattr $C$DW$113, DW_AT_type(*$C$DW$T$70)
	.dwattr $C$DW$113, DW_AT_name("confHeadInvPitchList")
	.dwattr $C$DW$113, DW_AT_data_member_location[DW_OP_plus_uconst 0x300]
	.dwattr $C$DW$113, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$113, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$113, DW_AT_decl_line(0x263)
	.dwattr $C$DW$113, DW_AT_decl_column(0x1c)

$C$DW$114	.dwtag  DW_TAG_member
	.dwattr $C$DW$114, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$114, DW_AT_name("locDataOffset")
	.dwattr $C$DW$114, DW_AT_data_member_location[DW_OP_plus_uconst 0x3c0]
	.dwattr $C$DW$114, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$114, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$114, DW_AT_decl_line(0x264)
	.dwattr $C$DW$114, DW_AT_decl_column(0x1c)

$C$DW$115	.dwtag  DW_TAG_member
	.dwattr $C$DW$115, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$115, DW_AT_name("confDataOffset")
	.dwattr $C$DW$115, DW_AT_data_member_location[DW_OP_plus_uconst 0x400]
	.dwattr $C$DW$115, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$115, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$115, DW_AT_decl_line(0x265)
	.dwattr $C$DW$115, DW_AT_decl_column(0x1c)

$C$DW$116	.dwtag  DW_TAG_member
	.dwattr $C$DW$116, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$116, DW_AT_name("numAnchors")
	.dwattr $C$DW$116, DW_AT_data_member_location[DW_OP_plus_uconst 0x440]
	.dwattr $C$DW$116, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$116, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$116, DW_AT_decl_line(0x266)
	.dwattr $C$DW$116, DW_AT_decl_column(0x1c)

$C$DW$117	.dwtag  DW_TAG_member
	.dwattr $C$DW$117, DW_AT_type(*$C$DW$T$74)
	.dwattr $C$DW$117, DW_AT_name("topMScore")
	.dwattr $C$DW$117, DW_AT_data_member_location[DW_OP_plus_uconst 0x480]
	.dwattr $C$DW$117, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$117, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$117, DW_AT_decl_line(0x269)
	.dwattr $C$DW$117, DW_AT_decl_column(0x0e)

$C$DW$118	.dwtag  DW_TAG_member
	.dwattr $C$DW$118, DW_AT_type(*$C$DW$T$74)
	.dwattr $C$DW$118, DW_AT_name("topMScoreSorted")
	.dwattr $C$DW$118, DW_AT_data_member_location[DW_OP_plus_uconst 0x488]
	.dwattr $C$DW$118, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$118, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$118, DW_AT_decl_line(0x26a)
	.dwattr $C$DW$118, DW_AT_decl_column(0x0e)

$C$DW$119	.dwtag  DW_TAG_member
	.dwattr $C$DW$119, DW_AT_type(*$C$DW$T$75)
	.dwattr $C$DW$119, DW_AT_name("tempScore")
	.dwattr $C$DW$119, DW_AT_data_member_location[DW_OP_plus_uconst 0x490]
	.dwattr $C$DW$119, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$119, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$119, DW_AT_decl_line(0x26b)
	.dwattr $C$DW$119, DW_AT_decl_column(0x0e)

$C$DW$120	.dwtag  DW_TAG_member
	.dwattr $C$DW$120, DW_AT_type(*$C$DW$T$76)
	.dwattr $C$DW$120, DW_AT_name("topMIndices")
	.dwattr $C$DW$120, DW_AT_data_member_location[DW_OP_plus_uconst 0x498]
	.dwattr $C$DW$120, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$120, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$120, DW_AT_decl_line(0x26c)
	.dwattr $C$DW$120, DW_AT_decl_column(0x0e)

$C$DW$121	.dwtag  DW_TAG_member
	.dwattr $C$DW$121, DW_AT_type(*$C$DW$T$76)
	.dwattr $C$DW$121, DW_AT_name("topMIndicesSorted")
	.dwattr $C$DW$121, DW_AT_data_member_location[DW_OP_plus_uconst 0x4a0]
	.dwattr $C$DW$121, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$121, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$121, DW_AT_decl_line(0x26d)
	.dwattr $C$DW$121, DW_AT_decl_column(0x0e)

$C$DW$122	.dwtag  DW_TAG_member
	.dwattr $C$DW$122, DW_AT_type(*$C$DW$T$76)
	.dwattr $C$DW$122, DW_AT_name("countMList")
	.dwattr $C$DW$122, DW_AT_data_member_location[DW_OP_plus_uconst 0x4a8]
	.dwattr $C$DW$122, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$122, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$122, DW_AT_decl_line(0x26f)
	.dwattr $C$DW$122, DW_AT_decl_column(0x0e)

$C$DW$123	.dwtag  DW_TAG_member
	.dwattr $C$DW$123, DW_AT_type(*$C$DW$T$76)
	.dwattr $C$DW$123, DW_AT_name("countMListAcc")
	.dwattr $C$DW$123, DW_AT_data_member_location[DW_OP_plus_uconst 0x4b0]
	.dwattr $C$DW$123, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$123, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$123, DW_AT_decl_line(0x270)
	.dwattr $C$DW$123, DW_AT_decl_column(0x0e)

$C$DW$124	.dwtag  DW_TAG_member
	.dwattr $C$DW$124, DW_AT_type(*$C$DW$T$76)
	.dwattr $C$DW$124, DW_AT_name("topKIndices")
	.dwattr $C$DW$124, DW_AT_data_member_location[DW_OP_plus_uconst 0x4b8]
	.dwattr $C$DW$124, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$124, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$124, DW_AT_decl_line(0x271)
	.dwattr $C$DW$124, DW_AT_decl_column(0x0e)

$C$DW$125	.dwtag  DW_TAG_member
	.dwattr $C$DW$125, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$125, DW_AT_name("topKLoc")
	.dwattr $C$DW$125, DW_AT_data_member_location[DW_OP_plus_uconst 0x4c0]
	.dwattr $C$DW$125, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$125, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$125, DW_AT_decl_line(0x272)
	.dwattr $C$DW$125, DW_AT_decl_column(0x0e)

$C$DW$126	.dwtag  DW_TAG_member
	.dwattr $C$DW$126, DW_AT_type(*$C$DW$T$78)
	.dwattr $C$DW$126, DW_AT_name("topKBbox")
	.dwattr $C$DW$126, DW_AT_data_member_location[DW_OP_plus_uconst 0x4c8]
	.dwattr $C$DW$126, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$126, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$126, DW_AT_decl_line(0x273)
	.dwattr $C$DW$126, DW_AT_decl_column(0x0e)

$C$DW$127	.dwtag  DW_TAG_member
	.dwattr $C$DW$127, DW_AT_type(*$C$DW$T$74)
	.dwattr $C$DW$127, DW_AT_name("topKScore")
	.dwattr $C$DW$127, DW_AT_data_member_location[DW_OP_plus_uconst 0x4d0]
	.dwattr $C$DW$127, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$127, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$127, DW_AT_decl_line(0x274)
	.dwattr $C$DW$127, DW_AT_decl_column(0x0e)

$C$DW$128	.dwtag  DW_TAG_member
	.dwattr $C$DW$128, DW_AT_type(*$C$DW$T$76)
	.dwattr $C$DW$128, DW_AT_name("nmsKeptIndices")
	.dwattr $C$DW$128, DW_AT_data_member_location[DW_OP_plus_uconst 0x4d8]
	.dwattr $C$DW$128, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$128, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$128, DW_AT_decl_line(0x275)
	.dwattr $C$DW$128, DW_AT_decl_column(0x0e)

$C$DW$129	.dwtag  DW_TAG_member
	.dwattr $C$DW$129, DW_AT_type(*$C$DW$T$79)
	.dwattr $C$DW$129, DW_AT_name("pred")
	.dwattr $C$DW$129, DW_AT_data_member_location[DW_OP_plus_uconst 0x4e0]
	.dwattr $C$DW$129, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$129, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$129, DW_AT_decl_line(0x279)
	.dwattr $C$DW$129, DW_AT_decl_column(0x10)

$C$DW$130	.dwtag  DW_TAG_member
	.dwattr $C$DW$130, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$130, DW_AT_name("featMaxMinVal")
	.dwattr $C$DW$130, DW_AT_data_member_location[DW_OP_plus_uconst 0x4e8]
	.dwattr $C$DW$130, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$130, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$130, DW_AT_decl_line(0x27a)
	.dwattr $C$DW$130, DW_AT_decl_column(0x10)

$C$DW$131	.dwtag  DW_TAG_member
	.dwattr $C$DW$131, DW_AT_type(*$C$DW$T$74)
	.dwattr $C$DW$131, DW_AT_name("topMScoreDdr")
	.dwattr $C$DW$131, DW_AT_data_member_location[DW_OP_plus_uconst 0x4f0]
	.dwattr $C$DW$131, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$131, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$131, DW_AT_decl_line(0x27d)
	.dwattr $C$DW$131, DW_AT_decl_column(0x0e)

$C$DW$132	.dwtag  DW_TAG_member
	.dwattr $C$DW$132, DW_AT_type(*$C$DW$T$74)
	.dwattr $C$DW$132, DW_AT_name("topMScoreSortedDdr")
	.dwattr $C$DW$132, DW_AT_data_member_location[DW_OP_plus_uconst 0x4f8]
	.dwattr $C$DW$132, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$132, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$132, DW_AT_decl_line(0x27e)
	.dwattr $C$DW$132, DW_AT_decl_column(0x0e)

$C$DW$133	.dwtag  DW_TAG_member
	.dwattr $C$DW$133, DW_AT_type(*$C$DW$T$76)
	.dwattr $C$DW$133, DW_AT_name("topMIndicesDdr")
	.dwattr $C$DW$133, DW_AT_data_member_location[DW_OP_plus_uconst 0x500]
	.dwattr $C$DW$133, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$133, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$133, DW_AT_decl_line(0x27f)
	.dwattr $C$DW$133, DW_AT_decl_column(0x0e)

$C$DW$134	.dwtag  DW_TAG_member
	.dwattr $C$DW$134, DW_AT_type(*$C$DW$T$76)
	.dwattr $C$DW$134, DW_AT_name("topMIndicesSortedDdr")
	.dwattr $C$DW$134, DW_AT_data_member_location[DW_OP_plus_uconst 0x508]
	.dwattr $C$DW$134, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$134, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$134, DW_AT_decl_line(0x280)
	.dwattr $C$DW$134, DW_AT_decl_column(0x0e)

$C$DW$135	.dwtag  DW_TAG_member
	.dwattr $C$DW$135, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$135, DW_AT_name("scratchPtr")
	.dwattr $C$DW$135, DW_AT_data_member_location[DW_OP_plus_uconst 0x510]
	.dwattr $C$DW$135, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$135, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$135, DW_AT_decl_line(0x281)
	.dwattr $C$DW$135, DW_AT_decl_column(0x0e)

$C$DW$136	.dwtag  DW_TAG_member
	.dwattr $C$DW$136, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$136, DW_AT_name("elementType")
	.dwattr $C$DW$136, DW_AT_data_member_location[DW_OP_plus_uconst 0x518]
	.dwattr $C$DW$136, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$136, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$136, DW_AT_decl_line(0x283)
	.dwattr $C$DW$136, DW_AT_decl_column(0x0b)

$C$DW$137	.dwtag  DW_TAG_member
	.dwattr $C$DW$137, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$137, DW_AT_name("elmSize")
	.dwattr $C$DW$137, DW_AT_data_member_location[DW_OP_plus_uconst 0x51c]
	.dwattr $C$DW$137, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$137, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$137, DW_AT_decl_line(0x284)
	.dwattr $C$DW$137, DW_AT_decl_column(0x0b)

$C$DW$138	.dwtag  DW_TAG_member
	.dwattr $C$DW$138, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$138, DW_AT_name("topM")
	.dwattr $C$DW$138, DW_AT_data_member_location[DW_OP_plus_uconst 0x520]
	.dwattr $C$DW$138, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$138, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$138, DW_AT_decl_line(0x285)
	.dwattr $C$DW$138, DW_AT_decl_column(0x0b)

$C$DW$139	.dwtag  DW_TAG_member
	.dwattr $C$DW$139, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$139, DW_AT_name("topMDdr")
	.dwattr $C$DW$139, DW_AT_data_member_location[DW_OP_plus_uconst 0x524]
	.dwattr $C$DW$139, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$139, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$139, DW_AT_decl_line(0x286)
	.dwattr $C$DW$139, DW_AT_decl_column(0x0b)

$C$DW$140	.dwtag  DW_TAG_member
	.dwattr $C$DW$140, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$140, DW_AT_name("scratchDDRConsumed")
	.dwattr $C$DW$140, DW_AT_data_member_location[DW_OP_plus_uconst 0x528]
	.dwattr $C$DW$140, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$140, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$140, DW_AT_decl_line(0x287)
	.dwattr $C$DW$140, DW_AT_decl_column(0x0b)

$C$DW$141	.dwtag  DW_TAG_member
	.dwattr $C$DW$141, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$141, DW_AT_name("topMAllClasses")
	.dwattr $C$DW$141, DW_AT_data_member_location[DW_OP_plus_uconst 0x52c]
	.dwattr $C$DW$141, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$141, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$141, DW_AT_decl_line(0x288)
	.dwattr $C$DW$141, DW_AT_decl_column(0x0b)

$C$DW$142	.dwtag  DW_TAG_member
	.dwattr $C$DW$142, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$142, DW_AT_name("isBckClsAvailable")
	.dwattr $C$DW$142, DW_AT_data_member_location[DW_OP_plus_uconst 0x530]
	.dwattr $C$DW$142, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$142, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$142, DW_AT_decl_line(0x289)
	.dwattr $C$DW$142, DW_AT_decl_column(0x0b)


$C$DW$143	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$143, DW_AT_name("operator =")
	.dwattr $C$DW$143, DW_AT_declaration
	.dwattr $C$DW$143, DW_AT_linkage_name("_ZN29sTIDL_ALgDetectOutputParams_taSERKS_")
	.dwattr $C$DW$143, DW_AT_type(*$C$DW$T$80)
	.dwattr $C$DW$143, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$144	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$144, DW_AT_type(*$C$DW$T$82)

	.dwendtag $C$DW$143


$C$DW$145	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$145, DW_AT_name("operator =")
	.dwattr $C$DW$145, DW_AT_declaration
	.dwattr $C$DW$145, DW_AT_linkage_name("_ZN29sTIDL_ALgDetectOutputParams_taSEOS_")
	.dwattr $C$DW$145, DW_AT_type(*$C$DW$T$80)
	.dwattr $C$DW$145, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$146	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$146, DW_AT_type(*$C$DW$T$80)

	.dwendtag $C$DW$145

	.dwattr $C$DW$T$85, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$T$85, DW_AT_decl_line(0x25b)
	.dwattr $C$DW$T$85, DW_AT_decl_column(0x10)
	.dwendtag $C$DW$T$85

	.dwendtag $C$DW$TU$85


$C$DW$TU$80	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$80
$C$DW$T$80	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$80, DW_AT_type(*$C$DW$T$85)
	.dwattr $C$DW$T$80, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$80


$C$DW$TU$83	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$83

$C$DW$T$83	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$83, DW_AT_type(*$C$DW$T$80)
$C$DW$147	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$147, DW_AT_type(*$C$DW$T$82)

	.dwendtag $C$DW$T$83

	.dwendtag $C$DW$TU$83


$C$DW$TU$84	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$84

$C$DW$T$84	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$84, DW_AT_type(*$C$DW$T$80)
$C$DW$148	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$148, DW_AT_type(*$C$DW$T$80)

	.dwendtag $C$DW$T$84

	.dwendtag $C$DW$TU$84


$C$DW$TU$81	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$81
$C$DW$T$81	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$81, DW_AT_type(*$C$DW$T$85)

	.dwendtag $C$DW$TU$81


$C$DW$TU$82	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$82
$C$DW$T$82	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$82, DW_AT_type(*$C$DW$T$81)
	.dwattr $C$DW$T$82, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$82


$C$DW$TU$145	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$145
$C$DW$T$145	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$145, DW_AT_name("sTIDL_ALgDetectOutputParams_t")
	.dwattr $C$DW$T$145, DW_AT_type(*$C$DW$T$85)
	.dwattr $C$DW$T$145, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$T$145, DW_AT_decl_line(0x28a)
	.dwattr $C$DW$T$145, DW_AT_decl_column(0x02)

	.dwendtag $C$DW$TU$145


$C$DW$TU$146	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$146
$C$DW$T$146	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$146, DW_AT_type(*$C$DW$T$145)
	.dwattr $C$DW$T$146, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$146


$C$DW$TU$108	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$108

$C$DW$T$108	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$108, DW_AT_name("sTIDL_AnchorBoxParams_t")
	.dwattr $C$DW$T$108, DW_AT_byte_size(0xf8)
$C$DW$149	.dwtag  DW_TAG_member
	.dwattr $C$DW$149, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$149, DW_AT_name("numAnchors")
	.dwattr $C$DW$149, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$149, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$149, DW_AT_decl_line(0x48f)
	.dwattr $C$DW$149, DW_AT_decl_column(0x0c)

$C$DW$150	.dwtag  DW_TAG_member
	.dwattr $C$DW$150, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$150, DW_AT_name("numKeyPoints")
	.dwattr $C$DW$150, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$150, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$150, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$150, DW_AT_decl_line(0x491)
	.dwattr $C$DW$150, DW_AT_decl_column(0x0c)

$C$DW$151	.dwtag  DW_TAG_member
	.dwattr $C$DW$151, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$151, DW_AT_name("headWidth")
	.dwattr $C$DW$151, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$151, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$151, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$151, DW_AT_decl_line(0x493)
	.dwattr $C$DW$151, DW_AT_decl_column(0x0b)

$C$DW$152	.dwtag  DW_TAG_member
	.dwattr $C$DW$152, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$152, DW_AT_name("headHeight")
	.dwattr $C$DW$152, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$152, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$152, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$152, DW_AT_decl_line(0x495)
	.dwattr $C$DW$152, DW_AT_decl_column(0x0b)

$C$DW$153	.dwtag  DW_TAG_member
	.dwattr $C$DW$153, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$153, DW_AT_name("strideW")
	.dwattr $C$DW$153, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$153, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$153, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$153, DW_AT_decl_line(0x497)
	.dwattr $C$DW$153, DW_AT_decl_column(0x10)

$C$DW$154	.dwtag  DW_TAG_member
	.dwattr $C$DW$154, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$154, DW_AT_name("strideH")
	.dwattr $C$DW$154, DW_AT_data_member_location[DW_OP_plus_uconst 0x14]
	.dwattr $C$DW$154, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$154, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$154, DW_AT_decl_line(0x499)
	.dwattr $C$DW$154, DW_AT_decl_column(0x10)

$C$DW$155	.dwtag  DW_TAG_member
	.dwattr $C$DW$155, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$155, DW_AT_name("offsetW")
	.dwattr $C$DW$155, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$155, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$155, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$155, DW_AT_decl_line(0x49b)
	.dwattr $C$DW$155, DW_AT_decl_column(0x10)

$C$DW$156	.dwtag  DW_TAG_member
	.dwattr $C$DW$156, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$156, DW_AT_name("offsetH")
	.dwattr $C$DW$156, DW_AT_data_member_location[DW_OP_plus_uconst 0x1c]
	.dwattr $C$DW$156, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$156, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$156, DW_AT_decl_line(0x49d)
	.dwattr $C$DW$156, DW_AT_decl_column(0x10)

$C$DW$157	.dwtag  DW_TAG_member
	.dwattr $C$DW$157, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$157, DW_AT_name("boxScales")
	.dwattr $C$DW$157, DW_AT_data_member_location[DW_OP_plus_uconst 0x20]
	.dwattr $C$DW$157, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$157, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$157, DW_AT_decl_line(0x49f)
	.dwattr $C$DW$157, DW_AT_decl_column(0x10)

$C$DW$158	.dwtag  DW_TAG_member
	.dwattr $C$DW$158, DW_AT_type(*$C$DW$T$64)
	.dwattr $C$DW$158, DW_AT_name("kpScales")
	.dwattr $C$DW$158, DW_AT_data_member_location[DW_OP_plus_uconst 0x30]
	.dwattr $C$DW$158, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$158, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$158, DW_AT_decl_line(0x4a1)
	.dwattr $C$DW$158, DW_AT_decl_column(0x10)

$C$DW$159	.dwtag  DW_TAG_member
	.dwattr $C$DW$159, DW_AT_type(*$C$DW$T$64)
	.dwattr $C$DW$159, DW_AT_name("boxWidth")
	.dwattr $C$DW$159, DW_AT_data_member_location[DW_OP_plus_uconst 0x70]
	.dwattr $C$DW$159, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$159, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$159, DW_AT_decl_line(0x4a3)
	.dwattr $C$DW$159, DW_AT_decl_column(0x10)

$C$DW$160	.dwtag  DW_TAG_member
	.dwattr $C$DW$160, DW_AT_type(*$C$DW$T$64)
	.dwattr $C$DW$160, DW_AT_name("boxHeight")
	.dwattr $C$DW$160, DW_AT_data_member_location[DW_OP_plus_uconst 0xb0]
	.dwattr $C$DW$160, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$160, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$160, DW_AT_decl_line(0x4a5)
	.dwattr $C$DW$160, DW_AT_decl_column(0x10)

$C$DW$161	.dwtag  DW_TAG_member
	.dwattr $C$DW$161, DW_AT_type(*$C$DW$T$102)
	.dwattr $C$DW$161, DW_AT_name("anchorInputs")
	.dwattr $C$DW$161, DW_AT_data_member_location[DW_OP_plus_uconst 0xf0]
	.dwattr $C$DW$161, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$161, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$161, DW_AT_decl_line(0x4a6)
	.dwattr $C$DW$161, DW_AT_decl_column(0x12)


$C$DW$162	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$162, DW_AT_name("operator =")
	.dwattr $C$DW$162, DW_AT_declaration
	.dwattr $C$DW$162, DW_AT_linkage_name("_ZN23sTIDL_AnchorBoxParams_taSERKS_")
	.dwattr $C$DW$162, DW_AT_type(*$C$DW$T$103)
	.dwattr $C$DW$162, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$163	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$163, DW_AT_type(*$C$DW$T$105)

	.dwendtag $C$DW$162


$C$DW$164	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$164, DW_AT_name("operator =")
	.dwattr $C$DW$164, DW_AT_declaration
	.dwattr $C$DW$164, DW_AT_linkage_name("_ZN23sTIDL_AnchorBoxParams_taSEOS_")
	.dwattr $C$DW$164, DW_AT_type(*$C$DW$T$103)
	.dwattr $C$DW$164, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$165	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$165, DW_AT_type(*$C$DW$T$103)

	.dwendtag $C$DW$164

	.dwattr $C$DW$T$108, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$108, DW_AT_decl_line(0x48c)
	.dwattr $C$DW$T$108, DW_AT_decl_column(0x10)
	.dwendtag $C$DW$T$108

	.dwendtag $C$DW$TU$108


$C$DW$TU$103	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$103
$C$DW$T$103	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$103, DW_AT_type(*$C$DW$T$108)
	.dwattr $C$DW$T$103, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$103


$C$DW$TU$106	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$106

$C$DW$T$106	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$106, DW_AT_type(*$C$DW$T$103)
$C$DW$166	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$166, DW_AT_type(*$C$DW$T$105)

	.dwendtag $C$DW$T$106

	.dwendtag $C$DW$TU$106


$C$DW$TU$107	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$107

$C$DW$T$107	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$107, DW_AT_type(*$C$DW$T$103)
$C$DW$167	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$167, DW_AT_type(*$C$DW$T$103)

	.dwendtag $C$DW$T$107

	.dwendtag $C$DW$TU$107


$C$DW$TU$104	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$104
$C$DW$T$104	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$104, DW_AT_type(*$C$DW$T$108)

	.dwendtag $C$DW$TU$104


$C$DW$TU$105	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$105
$C$DW$T$105	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$105, DW_AT_type(*$C$DW$T$104)
	.dwattr $C$DW$T$105, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$105


$C$DW$TU$168	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$168
$C$DW$T$168	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$168, DW_AT_name("sTIDL_AnchorBoxParams_t")
	.dwattr $C$DW$T$168, DW_AT_type(*$C$DW$T$108)
	.dwattr $C$DW$T$168, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$168, DW_AT_decl_line(0x4a8)
	.dwattr $C$DW$T$168, DW_AT_decl_column(0x02)

	.dwendtag $C$DW$TU$168


$C$DW$TU$117	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$117

$C$DW$T$117	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$117, DW_AT_name("sTIDL_DetectOutputParams_t")
	.dwattr $C$DW$T$117, DW_AT_byte_size(0x50)
$C$DW$168	.dwtag  DW_TAG_member
	.dwattr $C$DW$168, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$168, DW_AT_name("processingType")
	.dwattr $C$DW$168, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$168, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$168, DW_AT_decl_line(0x44d)
	.dwattr $C$DW$168, DW_AT_decl_column(0x0d)

$C$DW$169	.dwtag  DW_TAG_member
	.dwattr $C$DW$169, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$169, DW_AT_name("priorBox")
	.dwattr $C$DW$169, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$169, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$169, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$169, DW_AT_decl_line(0x44f)
	.dwattr $C$DW$169, DW_AT_decl_column(0x0c)

$C$DW$170	.dwtag  DW_TAG_member
	.dwattr $C$DW$170, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$170, DW_AT_name("priorBoxSize")
	.dwattr $C$DW$170, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$170, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$170, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$170, DW_AT_decl_line(0x451)
	.dwattr $C$DW$170, DW_AT_decl_column(0x0c)

$C$DW$171	.dwtag  DW_TAG_member
	.dwattr $C$DW$171, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$171, DW_AT_name("numClasses")
	.dwattr $C$DW$171, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$171, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$171, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$171, DW_AT_decl_line(0x453)
	.dwattr $C$DW$171, DW_AT_decl_column(0x0c)

$C$DW$172	.dwtag  DW_TAG_member
	.dwattr $C$DW$172, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$172, DW_AT_name("backgroundLabelId")
	.dwattr $C$DW$172, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$172, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$172, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$172, DW_AT_decl_line(0x455)
	.dwattr $C$DW$172, DW_AT_decl_column(0x0c)

$C$DW$173	.dwtag  DW_TAG_member
	.dwattr $C$DW$173, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$173, DW_AT_name("codeType")
	.dwattr $C$DW$173, DW_AT_data_member_location[DW_OP_plus_uconst 0x14]
	.dwattr $C$DW$173, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$173, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$173, DW_AT_decl_line(0x457)
	.dwattr $C$DW$173, DW_AT_decl_column(0x0c)

$C$DW$174	.dwtag  DW_TAG_member
	.dwattr $C$DW$174, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$174, DW_AT_name("confThreshold")
	.dwattr $C$DW$174, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$174, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$174, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$174, DW_AT_decl_line(0x45a)
	.dwattr $C$DW$174, DW_AT_decl_column(0x11)

$C$DW$175	.dwtag  DW_TAG_member
	.dwattr $C$DW$175, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$175, DW_AT_name("nmsThreshold")
	.dwattr $C$DW$175, DW_AT_data_member_location[DW_OP_plus_uconst 0x1c]
	.dwattr $C$DW$175, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$175, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$175, DW_AT_decl_line(0x45d)
	.dwattr $C$DW$175, DW_AT_decl_column(0x11)

$C$DW$176	.dwtag  DW_TAG_member
	.dwattr $C$DW$176, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$176, DW_AT_name("eta")
	.dwattr $C$DW$176, DW_AT_data_member_location[DW_OP_plus_uconst 0x20]
	.dwattr $C$DW$176, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$176, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$176, DW_AT_decl_line(0x45f)
	.dwattr $C$DW$176, DW_AT_decl_column(0x11)

$C$DW$177	.dwtag  DW_TAG_member
	.dwattr $C$DW$177, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$177, DW_AT_name("topK")
	.dwattr $C$DW$177, DW_AT_data_member_location[DW_OP_plus_uconst 0x24]
	.dwattr $C$DW$177, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$177, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$177, DW_AT_decl_line(0x461)
	.dwattr $C$DW$177, DW_AT_decl_column(0x0c)

$C$DW$178	.dwtag  DW_TAG_member
	.dwattr $C$DW$178, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$178, DW_AT_name("keepTopK")
	.dwattr $C$DW$178, DW_AT_data_member_location[DW_OP_plus_uconst 0x28]
	.dwattr $C$DW$178, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$178, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$178, DW_AT_decl_line(0x463)
	.dwattr $C$DW$178, DW_AT_decl_column(0x0c)

$C$DW$179	.dwtag  DW_TAG_member
	.dwattr $C$DW$179, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$179, DW_AT_name("shareLocation")
	.dwattr $C$DW$179, DW_AT_data_member_location[DW_OP_plus_uconst 0x2c]
	.dwattr $C$DW$179, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$179, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$179, DW_AT_decl_line(0x466)
	.dwattr $C$DW$179, DW_AT_decl_column(0x0c)

$C$DW$180	.dwtag  DW_TAG_member
	.dwattr $C$DW$180, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$180, DW_AT_name("varianceEncoded")
	.dwattr $C$DW$180, DW_AT_data_member_location[DW_OP_plus_uconst 0x30]
	.dwattr $C$DW$180, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$180, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$180, DW_AT_decl_line(0x469)
	.dwattr $C$DW$180, DW_AT_decl_column(0x0c)

$C$DW$181	.dwtag  DW_TAG_member
	.dwattr $C$DW$181, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$181, DW_AT_name("numKeypoints")
	.dwattr $C$DW$181, DW_AT_data_member_location[DW_OP_plus_uconst 0x34]
	.dwattr $C$DW$181, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$181, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$181, DW_AT_decl_line(0x46b)
	.dwattr $C$DW$181, DW_AT_decl_column(0x0c)

$C$DW$182	.dwtag  DW_TAG_member
	.dwattr $C$DW$182, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$182, DW_AT_name("numHeads")
	.dwattr $C$DW$182, DW_AT_data_member_location[DW_OP_plus_uconst 0x38]
	.dwattr $C$DW$182, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$182, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$182, DW_AT_decl_line(0x46e)
	.dwattr $C$DW$182, DW_AT_decl_column(0x0c)

$C$DW$183	.dwtag  DW_TAG_member
	.dwattr $C$DW$183, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$183, DW_AT_name("imWidth")
	.dwattr $C$DW$183, DW_AT_data_member_location[DW_OP_plus_uconst 0x3c]
	.dwattr $C$DW$183, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$183, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$183, DW_AT_decl_line(0x471)
	.dwattr $C$DW$183, DW_AT_decl_column(0x0b)

$C$DW$184	.dwtag  DW_TAG_member
	.dwattr $C$DW$184, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$184, DW_AT_name("imHeight")
	.dwattr $C$DW$184, DW_AT_data_member_location[DW_OP_plus_uconst 0x40]
	.dwattr $C$DW$184, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$184, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$184, DW_AT_decl_line(0x474)
	.dwattr $C$DW$184, DW_AT_decl_column(0x0b)

$C$DW$185	.dwtag  DW_TAG_member
	.dwattr $C$DW$185, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$185, DW_AT_name("scoreConverter")
	.dwattr $C$DW$185, DW_AT_data_member_location[DW_OP_plus_uconst 0x44]
	.dwattr $C$DW$185, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$185, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$185, DW_AT_decl_line(0x477)
	.dwattr $C$DW$185, DW_AT_decl_column(0x0b)

$C$DW$186	.dwtag  DW_TAG_member
	.dwattr $C$DW$186, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$186, DW_AT_name("metaArchType")
	.dwattr $C$DW$186, DW_AT_data_member_location[DW_OP_plus_uconst 0x48]
	.dwattr $C$DW$186, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$186, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$186, DW_AT_decl_line(0x47a)
	.dwattr $C$DW$186, DW_AT_decl_column(0x0b)

$C$DW$187	.dwtag  DW_TAG_member
	.dwattr $C$DW$187, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$187, DW_AT_name("dataLayout")
	.dwattr $C$DW$187, DW_AT_data_member_location[DW_OP_plus_uconst 0x4c]
	.dwattr $C$DW$187, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$187, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$187, DW_AT_decl_line(0x47e)
	.dwattr $C$DW$187, DW_AT_decl_column(0x0b)


$C$DW$188	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$188, DW_AT_name("operator =")
	.dwattr $C$DW$188, DW_AT_declaration
	.dwattr $C$DW$188, DW_AT_linkage_name("_ZN26sTIDL_DetectOutputParams_taSERKS_")
	.dwattr $C$DW$188, DW_AT_type(*$C$DW$T$112)
	.dwattr $C$DW$188, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$189	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$189, DW_AT_type(*$C$DW$T$114)

	.dwendtag $C$DW$188


$C$DW$190	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$190, DW_AT_name("operator =")
	.dwattr $C$DW$190, DW_AT_declaration
	.dwattr $C$DW$190, DW_AT_linkage_name("_ZN26sTIDL_DetectOutputParams_taSEOS_")
	.dwattr $C$DW$190, DW_AT_type(*$C$DW$T$112)
	.dwattr $C$DW$190, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$191	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$191, DW_AT_type(*$C$DW$T$112)

	.dwendtag $C$DW$190

	.dwattr $C$DW$T$117, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$117, DW_AT_decl_line(0x44b)
	.dwattr $C$DW$T$117, DW_AT_decl_column(0x10)
	.dwendtag $C$DW$T$117

	.dwendtag $C$DW$TU$117


$C$DW$TU$112	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$112
$C$DW$T$112	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$112, DW_AT_type(*$C$DW$T$117)
	.dwattr $C$DW$T$112, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$112


$C$DW$TU$115	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$115

$C$DW$T$115	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$115, DW_AT_type(*$C$DW$T$112)
$C$DW$192	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$192, DW_AT_type(*$C$DW$T$114)

	.dwendtag $C$DW$T$115

	.dwendtag $C$DW$TU$115


$C$DW$TU$116	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$116

$C$DW$T$116	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$116, DW_AT_type(*$C$DW$T$112)
$C$DW$193	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$193, DW_AT_type(*$C$DW$T$112)

	.dwendtag $C$DW$T$116

	.dwendtag $C$DW$TU$116


$C$DW$TU$113	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$113
$C$DW$T$113	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$113, DW_AT_type(*$C$DW$T$117)

	.dwendtag $C$DW$TU$113


$C$DW$TU$114	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$114
$C$DW$T$114	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$114, DW_AT_type(*$C$DW$T$113)
	.dwattr $C$DW$T$114, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$114


$C$DW$TU$143	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$143
$C$DW$T$143	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$143, DW_AT_name("sTIDL_DetectOutputParams_t")
	.dwattr $C$DW$T$143, DW_AT_type(*$C$DW$T$117)
	.dwattr $C$DW$T$143, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$143, DW_AT_decl_line(0x480)
	.dwattr $C$DW$T$143, DW_AT_decl_column(0x02)

	.dwendtag $C$DW$TU$143


$C$DW$TU$144	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$144
$C$DW$T$144	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$144, DW_AT_type(*$C$DW$T$143)
	.dwattr $C$DW$T$144, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$144


$C$DW$TU$50	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$50

$C$DW$T$50	.dwtag  DW_TAG_class_type
	.dwattr $C$DW$T$50, DW_AT_name("_ZSt9type_info")
	.dwattr $C$DW$T$50, DW_AT_byte_size(0x10)
$C$DW$194	.dwtag  DW_TAG_member
	.dwattr $C$DW$194, DW_AT_type(*$C$DW$T$35)
	.dwattr $C$DW$194, DW_AT_name("__vptr")
	.dwattr $C$DW$194, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$194, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/libcxx/typeinfo")
	.dwattr $C$DW$194, DW_AT_decl_line(0x56)
	.dwattr $C$DW$194, DW_AT_decl_column(0x1d)

$C$DW$195	.dwtag  DW_TAG_member
	.dwattr $C$DW$195, DW_AT_type(*$C$DW$T$38)
	.dwattr $C$DW$195, DW_AT_name("__type_name")
	.dwattr $C$DW$195, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$195, DW_AT_accessibility(DW_ACCESS_protected)
	.dwattr $C$DW$195, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/libcxx/typeinfo")
	.dwattr $C$DW$195, DW_AT_decl_line(0x7f)
	.dwattr $C$DW$195, DW_AT_decl_column(0x11)


$C$DW$196	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$196, DW_AT_name("operator =")
	.dwattr $C$DW$196, DW_AT_declaration
	.dwattr $C$DW$196, DW_AT_linkage_name("_ZNSt9type_infoaSERKS_")
	.dwattr $C$DW$196, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$196, DW_AT_accessibility(DW_ACCESS_private)
$C$DW$197	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$197, DW_AT_type(*$C$DW$T$41)

	.dwendtag $C$DW$196


$C$DW$198	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$198, DW_AT_name("type_info")
	.dwattr $C$DW$198, DW_AT_declaration
	.dwattr $C$DW$198, DW_AT_linkage_name("_ZNSt9type_infoC1ERKS_")
	.dwattr $C$DW$198, DW_AT_accessibility(DW_ACCESS_private)
$C$DW$199	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$199, DW_AT_type(*$C$DW$T$41)

	.dwendtag $C$DW$198


$C$DW$200	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$200, DW_AT_name("type_info")
	.dwattr $C$DW$200, DW_AT_declaration
	.dwattr $C$DW$200, DW_AT_linkage_name("_ZNSt9type_infoC1EPKc")
	.dwattr $C$DW$200, DW_AT_accessibility(DW_ACCESS_protected)
$C$DW$201	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$201, DW_AT_type(*$C$DW$T$38)

	.dwendtag $C$DW$200


$C$DW$202	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$202, DW_AT_name("~type_info")
	.dwattr $C$DW$202, DW_AT_declaration
	.dwattr $C$DW$202, DW_AT_linkage_name("_ZNSt9type_infoD1Ev")
	.dwattr $C$DW$202, DW_AT_virtuality(0x01)
	.dwattr $C$DW$202, DW_AT_vtable_elem_location[DW_OP_constu 0x00]
	.dwattr $C$DW$202, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$202


$C$DW$203	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$203, DW_AT_name("name")
	.dwattr $C$DW$203, DW_AT_declaration
	.dwattr $C$DW$203, DW_AT_linkage_name("_ZNKSt9type_info4nameEv")
	.dwattr $C$DW$203, DW_AT_type(*$C$DW$T$38)
	.dwattr $C$DW$203, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$203


$C$DW$204	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$204, DW_AT_name("before")
	.dwattr $C$DW$204, DW_AT_declaration
	.dwattr $C$DW$204, DW_AT_linkage_name("_ZNKSt9type_info6beforeERKS_")
	.dwattr $C$DW$204, DW_AT_type(*$C$DW$T$4)
	.dwattr $C$DW$204, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$205	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$205, DW_AT_type(*$C$DW$T$41)

	.dwendtag $C$DW$204


$C$DW$206	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$206, DW_AT_name("hash_code")
	.dwattr $C$DW$206, DW_AT_declaration
	.dwattr $C$DW$206, DW_AT_linkage_name("_ZNKSt9type_info9hash_codeEv")
	.dwattr $C$DW$206, DW_AT_type(*$C$DW$T$48)
	.dwattr $C$DW$206, DW_AT_accessibility(DW_ACCESS_public)
	.dwendtag $C$DW$206


$C$DW$207	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$207, DW_AT_name("operator ==")
	.dwattr $C$DW$207, DW_AT_declaration
	.dwattr $C$DW$207, DW_AT_linkage_name("_ZNKSt9type_infoeqERKS_")
	.dwattr $C$DW$207, DW_AT_type(*$C$DW$T$4)
	.dwattr $C$DW$207, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$208	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$208, DW_AT_type(*$C$DW$T$41)

	.dwendtag $C$DW$207


$C$DW$209	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$209, DW_AT_name("operator !=")
	.dwattr $C$DW$209, DW_AT_declaration
	.dwattr $C$DW$209, DW_AT_linkage_name("_ZNKSt9type_infoneERKS_")
	.dwattr $C$DW$209, DW_AT_type(*$C$DW$T$4)
	.dwattr $C$DW$209, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$210	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$210, DW_AT_type(*$C$DW$T$41)

	.dwendtag $C$DW$209

	.dwattr $C$DW$T$50, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/libcxx/typeinfo")
	.dwattr $C$DW$T$50, DW_AT_decl_line(0x56)
	.dwattr $C$DW$T$50, DW_AT_decl_column(0x1d)
	.dwendtag $C$DW$T$50

	.dwendtag $C$DW$TU$50


$C$DW$TU$39	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$39
$C$DW$T$39	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$39, DW_AT_type(*$C$DW$T$50)
	.dwattr $C$DW$T$39, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$39


$C$DW$TU$42	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$42

$C$DW$T$42	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$42, DW_AT_type(*$C$DW$T$39)
$C$DW$211	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$211, DW_AT_type(*$C$DW$T$41)

	.dwendtag $C$DW$T$42

	.dwendtag $C$DW$TU$42


$C$DW$TU$40	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$40
$C$DW$T$40	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$40, DW_AT_type(*$C$DW$T$50)

	.dwendtag $C$DW$TU$40


$C$DW$TU$41	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$41
$C$DW$T$41	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$41, DW_AT_type(*$C$DW$T$40)
	.dwattr $C$DW$T$41, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$41

