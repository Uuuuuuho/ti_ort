;******************************************************************************
;* C7x G3 C/C++ Codegen                                            Unix v1.4.2.LTS *
;* Date/Time created: Tue Oct  5 15:12:30 2021                                *
;******************************************************************************
	.compiler_opts --abi=eabi --endian=little --hll_source=on --object_format=elf --silicon_errata_i2117 --silicon_version=7100 --symdebug:dwarf --symdebug:dwarf_version=4 

;******************************************************************************
;* GLOBAL FILE PARAMETERS                                                     *
;*                                                                            *
;*   Architecture      : C7100                                                *
;*   Endian            : Little                                               *
;*   Pipelining        : Enabled                                              *
;*   Debug Info        : DWARF Debug                                          *
;*                                                                            *
;******************************************************************************

	.asg	D14, FP
	.asg	D15, SP
	.global	$bss


$C$DW$CU	.dwtag  DW_TAG_compile_unit
	.dwattr $C$DW$CU, DW_AT_name("src/tidl_batchReshape.c")
	.dwattr $C$DW$CU, DW_AT_producer("TI C7x G3 C/C++ Codegen Unix v1.4.2.LTS Copyright (c) 2014-2019 Texas Instruments Incorporated")
	.dwattr $C$DW$CU, DW_AT_TI_version(0x01)
	.dwattr $C$DW$CU, DW_AT_comp_dir("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/tidl_j7_08_00_00_10/ti_dl/algo")

$C$DW$1	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1, DW_AT_name("tidl_printf")
	.dwattr $C$DW$1, DW_AT_linkage_name("_Z11tidl_printfiPKcz")
	.dwattr $C$DW$1, DW_AT_declaration
	.dwattr $C$DW$1, DW_AT_external
	.dwattr $C$DW$1, DW_AT_decl_file("./inc/tidl_commonUtils.h")
	.dwattr $C$DW$1, DW_AT_decl_line(0xc2)
	.dwattr $C$DW$1, DW_AT_decl_column(0x06)
$C$DW$2	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$2, DW_AT_type(*$C$DW$T$39)

$C$DW$3	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$3, DW_AT_type(*$C$DW$T$274)

$C$DW$4	.dwtag  DW_TAG_unspecified_parameters

	.dwendtag $C$DW$1


$C$DW$151	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$151, DW_AT_name("TIDL_batchReshapeDspProcess")
	.dwattr $C$DW$151, DW_AT_linkage_name("_Z27TIDL_batchReshapeDspProcessP8TIDL_ObjP16sTIDL_AlgLayer_tPK13sTIDL_Layer_tPPvS7_P20sTIDL_sysMemHandle_t")
	.dwattr $C$DW$151, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$151, DW_AT_declaration
	.dwattr $C$DW$151, DW_AT_external
	.dwattr $C$DW$151, DW_AT_decl_file("./inc/tidl_batchReshape.h")
	.dwattr $C$DW$151, DW_AT_decl_line(0x85)
	.dwattr $C$DW$151, DW_AT_decl_column(0x09)
$C$DW$152	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$152, DW_AT_type(*$C$DW$T$997)

$C$DW$153	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$153, DW_AT_type(*$C$DW$T$361)

$C$DW$154	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$154, DW_AT_type(*$C$DW$T$1062)

$C$DW$155	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$155, DW_AT_type(*$C$DW$T$284)

$C$DW$156	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$156, DW_AT_type(*$C$DW$T$284)

$C$DW$157	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$157, DW_AT_type(*$C$DW$T$1063)

	.dwendtag $C$DW$151


$C$DW$158	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$158, DW_AT_name("TIDL_getDataParams")
	.dwattr $C$DW$158, DW_AT_linkage_name("_Z18TIDL_getDataParamsP15sTIDL_Network_ti")
	.dwattr $C$DW$158, DW_AT_type(*$C$DW$T$1076)
	.dwattr $C$DW$158, DW_AT_declaration
	.dwattr $C$DW$158, DW_AT_external
	.dwattr $C$DW$158, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$158, DW_AT_decl_line(0x446)
	.dwattr $C$DW$158, DW_AT_decl_column(0x16)
$C$DW$159	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$159, DW_AT_type(*$C$DW$T$271)

$C$DW$160	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$160, DW_AT_type(*$C$DW$T$39)

	.dwendtag $C$DW$158


$C$DW$161	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$161, DW_AT_name("TIDL_UpdateScaleFactors")
	.dwattr $C$DW$161, DW_AT_linkage_name("_Z23TIDL_UpdateScaleFactorsP8TIDL_Objiill")
	.dwattr $C$DW$161, DW_AT_declaration
	.dwattr $C$DW$161, DW_AT_external
	.dwattr $C$DW$161, DW_AT_decl_file("./inc/tidl_commonUtils.h")
	.dwattr $C$DW$161, DW_AT_decl_line(0x73)
	.dwattr $C$DW$161, DW_AT_decl_column(0x06)
$C$DW$162	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$162, DW_AT_type(*$C$DW$T$997)

$C$DW$163	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$163, DW_AT_type(*$C$DW$T$39)

$C$DW$164	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$164, DW_AT_type(*$C$DW$T$39)

$C$DW$165	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$165, DW_AT_type(*$C$DW$T$625)

$C$DW$166	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$166, DW_AT_type(*$C$DW$T$625)

	.dwendtag $C$DW$161

;	/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/bin/opt7x /tmp/TI51ANWjVgL /tmp/TI51ARMDJZZ 
	.sect	".text:_Z20TIDL_refBatchReshapeIttEvPKT_PT0_iiiiiiiiiiii"
	.clink
	.global	||_Z20TIDL_refBatchReshapeIttEvPKT_PT0_iiiiiiiiiiii||

$C$DW$167	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$167, DW_AT_name("void TIDL_refBatchReshape")
	.dwattr $C$DW$167, DW_AT_low_pc(||_Z20TIDL_refBatchReshapeIttEvPKT_PT0_iiiiiiiiiiii||)
	.dwattr $C$DW$167, DW_AT_high_pc(0x00)
	.dwattr $C$DW$167, DW_AT_linkage_name("_Z20TIDL_refBatchReshapeIttEvPKT_PT0_iiiiiiiiiiii")
	.dwattr $C$DW$167, DW_AT_external
	.dwattr $C$DW$167, DW_AT_decl_file("src/tidl_batchReshape.c")
	.dwattr $C$DW$167, DW_AT_decl_line(0x60)
	.dwattr $C$DW$167, DW_AT_decl_column(0x06)
	.dwattr $C$DW$167, DW_AT_TI_max_frame_size(0x20)
	.dwpsn	file "src/tidl_batchReshape.c",line 112,column 1,is_stmt,address ||_Z20TIDL_refBatchReshapeIttEvPKT_PT0_iiiiiiiiiiii||,isa 0

	.dwfde $C$DW$CIE, ||_Z20TIDL_refBatchReshapeIttEvPKT_PT0_iiiiiiiiiiii||
$C$DW$168	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$168, DW_AT_name("pIn")
	.dwattr $C$DW$168, DW_AT_location[DW_OP_reg4]

$C$DW$169	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$169, DW_AT_name("pOut")
	.dwattr $C$DW$169, DW_AT_location[DW_OP_reg5]

$C$DW$170	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$170, DW_AT_name("inPtrOffset")
	.dwattr $C$DW$170, DW_AT_location[DW_OP_reg6]

$C$DW$171	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$171, DW_AT_name("outPtrOffset")
	.dwattr $C$DW$171, DW_AT_location[DW_OP_reg7]

$C$DW$172	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$172, DW_AT_name("width")
	.dwattr $C$DW$172, DW_AT_location[DW_OP_reg8]

$C$DW$173	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$173, DW_AT_name("height")
	.dwattr $C$DW$173, DW_AT_location[DW_OP_reg9]

$C$DW$174	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$174, DW_AT_name("numChs")
	.dwattr $C$DW$174, DW_AT_location[DW_OP_reg10]

$C$DW$175	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$175, DW_AT_name("numBatches")
	.dwattr $C$DW$175, DW_AT_location[DW_OP_reg11]

$C$DW$176	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$176, DW_AT_name("inLinePitch")
	.dwattr $C$DW$176, DW_AT_location[DW_OP_reg12]

$C$DW$177	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$177, DW_AT_name("outLinePitch")
	.dwattr $C$DW$177, DW_AT_location[DW_OP_bregx 0x6f 48]

$C$DW$178	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$178, DW_AT_name("inChPitch")
	.dwattr $C$DW$178, DW_AT_location[DW_OP_bregx 0x6f 52]

$C$DW$179	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$179, DW_AT_name("outChPitch")
	.dwattr $C$DW$179, DW_AT_location[DW_OP_bregx 0x6f 56]

$C$DW$180	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$180, DW_AT_name("inBatchPitch")
	.dwattr $C$DW$180, DW_AT_location[DW_OP_bregx 0x6f 60]

$C$DW$181	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$181, DW_AT_name("outBatchPitch")
	.dwattr $C$DW$181, DW_AT_location[DW_OP_bregx 0x6f 64]


;******************************************************************************
;* FUNCTION NAME: void TIDL_refBatchReshape<unsigned short, unsigned short>(const T1 *, T2 *, int, int, int, int, int, int, int, int, int, int, int, int)*
;*                                                                            *
;*   Regs Modified     : A0,A1,A2,A3,A11,A12,A13,A14,VB0,VB1,VB2,AL0,AM0,AM1, *
;*                           AM2,AM3,AM4,D0,D1,D2,D3,D4,D14,SP,VBM0,VBM1      *
;*   Regs Used         : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,A14,   *
;*                           VB0,VB1,VB2,AL0,AM0,AM1,AM2,AM3,AM4,D0,D1,D2,D3, *
;*                           D4,D14,SP,VBM0,VBM1                              *
;*   Local Frame Size  : 0 Args + 0 Auto + 32 Save = 32 byte                  *
;******************************************************************************
||_Z20TIDL_refBatchReshapeIttEvPKT_PT0_iiiiiiiiiiii||:
;** --------------------------------------------------------------------------*
;* A14   assigned to $O$U37
;* A13   assigned to $O$U48
;* A0    assigned to $O$L1
;* A3    assigned to $O$L2
;* A1    assigned to $O$L3
;* A8    assigned to $O$L4
;* A11   assigned to $O$Lr15$i1
;* A12   assigned to $O$Lr18$i2
;* VB1   assigned to $O$Lr21$i3
;* AM3   assigned to outBatchPitch
$C$DW$182	.dwtag  DW_TAG_variable
	.dwattr $C$DW$182, DW_AT_name("outBatchPitch")
	.dwattr $C$DW$182, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$182, DW_AT_location[DW_OP_reg27]

;* AM2   assigned to inBatchPitch
$C$DW$183	.dwtag  DW_TAG_variable
	.dwattr $C$DW$183, DW_AT_name("inBatchPitch")
	.dwattr $C$DW$183, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$183, DW_AT_location[DW_OP_reg26]

;* AM1   assigned to outChPitch
$C$DW$184	.dwtag  DW_TAG_variable
	.dwattr $C$DW$184, DW_AT_name("outChPitch")
	.dwattr $C$DW$184, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$184, DW_AT_location[DW_OP_reg25]

;* AM0   assigned to inChPitch
$C$DW$185	.dwtag  DW_TAG_variable
	.dwattr $C$DW$185, DW_AT_name("inChPitch")
	.dwattr $C$DW$185, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$185, DW_AT_location[DW_OP_reg24]

;* VBM0  assigned to outLinePitch
$C$DW$186	.dwtag  DW_TAG_variable
	.dwattr $C$DW$186, DW_AT_name("outLinePitch")
	.dwattr $C$DW$186, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$186, DW_AT_location[DW_OP_regx 0x58]

;* VBM1  assigned to inLinePitch
$C$DW$187	.dwtag  DW_TAG_variable
	.dwattr $C$DW$187, DW_AT_name("inLinePitch")
	.dwattr $C$DW$187, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$187, DW_AT_location[DW_OP_regx 0x59]

;* A0    assigned to numBatches
$C$DW$188	.dwtag  DW_TAG_variable
	.dwattr $C$DW$188, DW_AT_name("numBatches")
	.dwattr $C$DW$188, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$188, DW_AT_location[DW_OP_reg0]

;* A10   assigned to numChs
$C$DW$189	.dwtag  DW_TAG_variable
	.dwattr $C$DW$189, DW_AT_name("numChs")
	.dwattr $C$DW$189, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$189, DW_AT_location[DW_OP_reg10]

;* A9    assigned to height
$C$DW$190	.dwtag  DW_TAG_variable
	.dwattr $C$DW$190, DW_AT_name("height")
	.dwattr $C$DW$190, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$190, DW_AT_location[DW_OP_reg9]

;* A8    assigned to width
$C$DW$191	.dwtag  DW_TAG_variable
	.dwattr $C$DW$191, DW_AT_name("width")
	.dwattr $C$DW$191, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$191, DW_AT_location[DW_OP_reg8]

;* A7    assigned to outPtrOffset
$C$DW$192	.dwtag  DW_TAG_variable
	.dwattr $C$DW$192, DW_AT_name("outPtrOffset")
	.dwattr $C$DW$192, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$192, DW_AT_location[DW_OP_reg7]

;* A6    assigned to inPtrOffset
$C$DW$193	.dwtag  DW_TAG_variable
	.dwattr $C$DW$193, DW_AT_name("inPtrOffset")
	.dwattr $C$DW$193, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$193, DW_AT_location[DW_OP_reg6]

;* A5    assigned to pOut
$C$DW$194	.dwtag  DW_TAG_variable
	.dwattr $C$DW$194, DW_AT_name("pOut")
	.dwattr $C$DW$194, DW_AT_type(*$C$DW$T$602)
	.dwattr $C$DW$194, DW_AT_location[DW_OP_reg5]

;* A4    assigned to pIn
$C$DW$195	.dwtag  DW_TAG_variable
	.dwattr $C$DW$195, DW_AT_name("pIn")
	.dwattr $C$DW$195, DW_AT_type(*$C$DW$T$1016)
	.dwattr $C$DW$195, DW_AT_location[DW_OP_reg4]

	.dwcfi	cfa_offset, 0
;          EXCLUSIVE CPU CYCLES: 5

           STD     .D1     A11,*SP(8)        ; [A_D1] 
||         STD     .D2X    A12,*SP++(-32)    ; [A_D2] 

	.dwcfi	save_reg_to_mem, 11, 8
	.dwcfi	cfa_offset, 32
	.dwcfi	save_reg_to_mem, 12, -32
           LDW     .D1     *SP(60),AM2       ; [A_D1] |112| 

           LDW     .D1     *SP(52),AM0       ; [A_D1] |112| 
||         LDW     .D2     *SP(56),AM1       ; [A_D2] |112| 
||         MV      .L1     A11,A0            ; [A_L1] |112| 

           LDW     .D1     *SP(48),BM0       ; [A_D1] |112| 
||         LDW     .D2     *SP(64),AM3       ; [A_D2] |112| 
||         CMPGTW  .L1     A0,0,A1           ; [A_L1] |115| 
||         MV      .L2X    A12,BM1           ; [B_L2] |112| 

	.dwpsn	file "src/tidl_batchReshape.c",line 115,column 8,is_stmt,isa 0

   [!A1]   B       .B1     ||$C$L10||        ; [A_B] |115| 
||         STD     .D1     A14,*SP(16)       ; [A_D1] 
||         STD     .D2X    A13,*SP(24)       ; [A_D2] 
||         MVKU32  .L1     0,A11             ; [A_L1] |115| 

	.dwcfi	save_reg_to_mem, 14, 16
	.dwcfi	save_reg_to_mem, 13, 24
	.dwpsn	file "src/tidl_batchReshape.c",line 115,column 16,is_stmt,isa 0
           ; BRANCHCC OCCURS {||$C$L10||}    ; [] |115| 
;** --------------------------------------------------------------------------*
;**   BEGIN LOOP ||$C$L1||
;** --------------------------------------------------------------------------*
||$C$L1||:    
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "src/tidl_batchReshape.c",line 117,column 18,is_stmt,isa 0
           CMPGTW  .L1     A10,0,A1          ; [A_L1] |117| 
   [!A1]   B       .B1     ||$C$L9||         ; [A_B] |117| 
           ; BRANCHCC OCCURS {||$C$L9||}     ; [] |117| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 1
	.dwpsn	file "src/tidl_batchReshape.c",line 117,column 10,is_stmt,isa 0

           MPYWW   .N1     AM2,A11,D3        ; [A_N1] 
||         MVKU32  .L1     0,A12             ; [A_L1] |117| 
||         MV      .D1     A10,A3            ; [A_D1] 

;** --------------------------------------------------------------------------*
;**   BEGIN LOOP ||$C$L2||
;** --------------------------------------------------------------------------*
||$C$L2||:    
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "src/tidl_batchReshape.c",line 119,column 20,is_stmt,isa 0
           CMPGTW  .L1     A9,0,A1           ; [A_L1] |119| 
   [!A1]   B       .B1     ||$C$L8||         ; [A_B] |119| 
           ; BRANCHCC OCCURS {||$C$L8||}     ; [] |119| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 1
	.dwpsn	file "src/tidl_batchReshape.c",line 119,column 12,is_stmt,isa 0

           MPYWW   .N1     AM1,A12,D2        ; [A_N1] 
||         MPYWW   .M1     AM0,A12,D4        ; [A_M1] 
||         MVKU32  .L2     0,B1              ; [B_L2] |119| 
||         MV      .D1     A9,A1             ; [A_D1] 

;** --------------------------------------------------------------------------*
;**   BEGIN LOOP ||$C$L3||
;** --------------------------------------------------------------------------*
||$C$L3||:    
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "src/tidl_batchReshape.c",line 121,column 22,is_stmt,isa 0
           CMPGTW  .L1     A8,0,A2           ; [A_L1] |121| 
   [!A2]   B       .B1     ||$C$L7||         ; [A_B] |121| 
           ; BRANCHCC OCCURS {||$C$L7||}     ; [] |121| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 10
           MPYWW   .N2     BM1,B1,B0         ; [B_N2] 

           MPYWW   .N2     BM0,B1,B2         ; [B_N2] 
||         MPYWW   .N1     AM3,A11,D0        ; [A_N1] 

           ADDW    .D1     D4,D3,D0          ; [A_D1] 
||         MV      .L1X    B0,D1             ; [A_L1] Define a twin register
||         ADDW    .D2     D2,D0,AM4         ; [A_D2] 

           ADDW    .D1     D1,D0,D0          ; [A_D1] 
||         ADDW    .M1X    B2,AM4,D14        ; [A_M1] 
||         MV      .D2     A8,AL0            ; [A_D2] 

           ADDW    .D1     A7,D14,D1         ; [A_D1] 
||         ADDW    .D2     A6,D0,D0          ; [A_D2] 
||         EXT     .L1     AL0,0x20,0x20,AL0 ; [A_L1] 

           ADDAH   .D1     A5,D1,A13         ; [A_D1] 
||         ADDAH   .D2     A4,D0,A14         ; [A_D2] 
||         NLCINIT .S1     AL0,0x1,0         ; [A_S1] 
||         UNPROT          0x1               ; [A_U] 

           MV      .D1     A14,D1            ; [A_D1] 
||         MV      .D2     A13,D0            ; [A_D2] 
||         TICK                               ; [A_U] <0,0> 

;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*
;*      Loop found in file               : src/tidl_batchReshape.c
;*      Loop source line                 : 121
;*      Loop opening brace source line   : 122
;*      Loop closing brace source line   : 126
;*      Known Minimum Trip Count         : 1                    
;*      Known Max Trip Count Factor      : 1
;*      Loop Carried Dependency Bound(^) : 7
;*      Unpartitioned Resource Bound     : 1
;*      Partitioned Resource Bound       : 1 (pre-sched)
;*
;*      Searching for software pipeline schedule at ...
;*         ii = 7  Schedule found with 2 iterations in parallel
;*
;*      Partitioned Resource Bound(*)    : 1 (post-sched)
;*
;*      Constant Extension #0 Used [C0]  : 0
;*      Constant Extension #1 Used [C1]  : 0
;*
;*      Resource Partition (may include "post-sched" split/spill moves):
;*
;*                                                A-side   B-side
;*      .L units                                     0        0     
;*      .S units                                     0        0     
;*      .M units                                     0        0     
;*      .N units                                     0        0     
;*      .D units                                     2        -     
;*      .B units                                     1*       -     
;*      .C units                                     -        0     
;*      .P units                                     -        0     
;*
;*      .M/.N units                                  0        0     
;*      .L/.S units                                  0        0     
;*      .L/.S/.C units                               0        0     
;*      .L/.S/.C/.M units                            0        0     
;*      .L/.S/.C/.M/.D units                         0        0     
;*
;*      .X cross paths                               1*       0     
;*
;*      Bound(.D1 .D2 .D)                            1*       -     
;*      Bound(.M .N .MN)                             0        0     
;*      Bound(.L .S .LS)                             0        0     
;*      Bound(.L .S .C .LS .LSC)                     0        0     
;*      Bound(.L .S .C .M .LS .LSC .LSCM)            0        0     
;*      Bound(.L .S .C .M .D .LS .LSC .LSCM .LSCMD)  1*       0     
;*
;*      Register Usage Tables:
;*
;*      +----------------------------------+----------------------------------+
;*      |              ASIDE               |              BSIDE               |
;*      +----------------+--------+--------+----------------+--------+--------+
;*      |      "Axx"     |  "ALx" |  "AMx" |     "VBxx"     | "VBLx" | "VBMx" |
;*      +----------------|--------|--------|----------------|--------|--------+
;*      |0000000000111111|00000000|00000000|0000000000111111|00000000|00000000|
;*      |0123456789012345|01234567|01234567|0123456789012345|01234567|01234567|
;*      +----------------|--------|--------|----------------|--------|--------+
;*   0: |                |        |        |                |        |        |
;*   1: |                |        |        |                |        |        |
;*   2: |                |        |        |                |        |        |
;*   3: |                |        |        |                |        |        |
;*   4: |                |        |        |                |        |        |
;*   5: |                |        |        |                |        |        |
;*   6: |                |        |        |*               |        |        |
;*      +----------------+--------+--------+----------------+--------+--------+
;*
;*      +----------------+                 +--------+       +-------+
;*      |      "Dxx"     |                 |  "Px"  |       |"CUCRx"|
;*      +----------------+                 +--------+       +-------+
;*      |0000000000111111|                 |00000000|       |0000   |
;*      |0123456789012345|                 |01234567|       |0123   |
;*      +----------------+                 +--------+       +-------+
;*   0: |**              |                 |        |       |       |
;*   1: |**              |                 |        |       |       |
;*   2: |**              |                 |        |       |       |
;*   3: |**              |                 |        |       |       |
;*   4: |**              |                 |        |       |       |
;*   5: |**              |                 |        |       |       |
;*   6: |**              |                 |        |       |       |
;*      +----------------+                 +--------+       +-------+
;*
;*      Done
;*
;*      Collapsed epilog stages       : 1
;*      Prolog not removed
;*      Collapsed prolog stages       : 0
;*
;*      Max amt of load speculation   : 0 bytes
;*
;*      Minimum safe trip count       : 1
;*      Min. prof. trip count  (est.) : 2
;*
;*      Mem bank conflicts/iter(est.) : { min 0.000, est 0.000, max 0.000 }
;*      Mem bank perf. penalty (est.) : 0.0%
;*
;*
;*      Total cycles (est.)         : 1 + trip_cnt * 7        
;*----------------------------------------------------------------------------*
;*        SINGLE SCHEDULED ITERATION
;*
;*        ||$C$C48||:
;*   0              TICK                               ; [A_U] 
;*   1              LDUH    .D1     *D1++(2),B0       ; [A_D1] |125|  ^ 
;*   2              NOP     0x5     ; [A_B] 
;*   7              STH     .D1X    B0,*D0++(2)       ; [A_D1] |125|  ^ 
;*     ||           BNL     .B1     ||$C$C48||        ; [A_B] |121| 
;*   8              ; BRANCHCC OCCURS {||$C$C48||}    ; [] |121| 
;*----------------------------------------------------------------------------*
||$C$L4||:    ; PIPED LOOP PROLOG
;** --------------------------------------------------------------------------*
||$C$L5||:    ; PIPED LOOP KERNEL
;          EXCLUSIVE CPU CYCLES: 7
	.dwpsn	file "src/tidl_batchReshape.c",line 125,column 11,is_stmt,isa 0
           LDUH    .D1     *D1++(2),B0       ; [A_D1] |125| <0,1>  ^ 
           NOP             0x5               ; [A_B] 
	.dwpsn	file "src/tidl_batchReshape.c",line 121,column 22,is_stmt,isa 0

           BNL     .B1     ||$C$L5||         ; [A_B] |121| <0,7> 
||         STH     .D1X    B0,*D0++(2)       ; [A_D1] |125| <0,7>  ^ 
||         TICK                               ; [A_U] <1,0> 

;** --------------------------------------------------------------------------*
||$C$L6||:    ; PIPED LOOP EPILOG
;          EXCLUSIVE CPU CYCLES: 1
           PROTCLR                            ; [A_U] 
;** --------------------------------------------------------------------------*
||$C$L7||:    
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "src/tidl_batchReshape.c",line 119,column 20,is_stmt,isa 0
           ADDW    .D1     A1,0xffffffff,A1  ; [A_D1] |119| 

   [ A1]   B       .B1     ||$C$L3||         ; [A_B] |119| 
||         ADDW    .L2     B1,0x1,B1         ; [B_L2] |119| 

           ; BRANCHCC OCCURS {||$C$L3||}     ; [] |119| 
;** --------------------------------------------------------------------------*
||$C$L8||:    
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "src/tidl_batchReshape.c",line 117,column 18,is_stmt,isa 0
           ADDW    .D1     A3,0xffffffff,A3  ; [A_D1] |117| 

   [ A3]   B       .B1     ||$C$L2||         ; [A_B] |117| 
||         ADDW    .D1     A12,0x1,A12       ; [A_D1] |117| 

           ; BRANCHCC OCCURS {||$C$L2||}     ; [] |117| 
;** --------------------------------------------------------------------------*
||$C$L9||:    
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "src/tidl_batchReshape.c",line 115,column 16,is_stmt,isa 0
           ADDW    .D1     A0,0xffffffff,A0  ; [A_D1] |115| 

   [ A0]   B       .B1     ||$C$L1||         ; [A_B] |115| 
||         ADDW    .D1     A11,0x1,A11       ; [A_D1] |115| 

           ; BRANCHCC OCCURS {||$C$L1||}     ; [] |115| 
;** --------------------------------------------------------------------------*
||$C$L10||:    
;          EXCLUSIVE CPU CYCLES: 7

           LDD     .D1     *SP(24),A13       ; [A_D1] 
||         LDD     .D2     *SP(16),A14       ; [A_D2] 
	.dwcfi	restore_reg, 13
	.dwcfi	restore_reg, 14

           LDD     .D1     *SP(40),A11       ; [A_D1] 
||         LDD     .D2     *SP(32),A12       ; [A_D2] 

	.dwcfi	restore_reg, 11
	.dwcfi	restore_reg, 12
$C$DW$196	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$196, DW_AT_low_pc(0x00)
	.dwattr $C$DW$196, DW_AT_TI_return


           RET     .B1     ; [A_B] 
||         ADDD    .D1     SP,0x20,SP        ; [A_D1] 

	.dwcfi	cfa_offset, 0
           ; RETURN OCCURS {RP}              ; [] 
	.dwattr $C$DW$167, DW_AT_TI_end_file("src/tidl_batchReshape.c")
	.dwattr $C$DW$167, DW_AT_TI_end_line(0x82)
	.dwattr $C$DW$167, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$167

	.sect	".text:_Z20TIDL_refBatchReshapeIhhEvPKT_PT0_iiiiiiiiiiii"
	.clink
	.global	||_Z20TIDL_refBatchReshapeIhhEvPKT_PT0_iiiiiiiiiiii||

$C$DW$197	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$197, DW_AT_name("void TIDL_refBatchReshape")
	.dwattr $C$DW$197, DW_AT_low_pc(||_Z20TIDL_refBatchReshapeIhhEvPKT_PT0_iiiiiiiiiiii||)
	.dwattr $C$DW$197, DW_AT_high_pc(0x00)
	.dwattr $C$DW$197, DW_AT_linkage_name("_Z20TIDL_refBatchReshapeIhhEvPKT_PT0_iiiiiiiiiiii")
	.dwattr $C$DW$197, DW_AT_external
	.dwattr $C$DW$197, DW_AT_decl_file("src/tidl_batchReshape.c")
	.dwattr $C$DW$197, DW_AT_decl_line(0x60)
	.dwattr $C$DW$197, DW_AT_decl_column(0x06)
	.dwattr $C$DW$197, DW_AT_TI_max_frame_size(0x20)
	.dwpsn	file "src/tidl_batchReshape.c",line 112,column 1,is_stmt,address ||_Z20TIDL_refBatchReshapeIhhEvPKT_PT0_iiiiiiiiiiii||,isa 0

	.dwfde $C$DW$CIE, ||_Z20TIDL_refBatchReshapeIhhEvPKT_PT0_iiiiiiiiiiii||
$C$DW$198	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$198, DW_AT_name("pIn")
	.dwattr $C$DW$198, DW_AT_type(*$C$DW$T$1013)
	.dwattr $C$DW$198, DW_AT_location[DW_OP_reg4]

$C$DW$199	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$199, DW_AT_name("pOut")
	.dwattr $C$DW$199, DW_AT_type(*$C$DW$T$364)
	.dwattr $C$DW$199, DW_AT_location[DW_OP_reg5]

$C$DW$200	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$200, DW_AT_name("inPtrOffset")
	.dwattr $C$DW$200, DW_AT_location[DW_OP_reg6]

$C$DW$201	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$201, DW_AT_name("outPtrOffset")
	.dwattr $C$DW$201, DW_AT_location[DW_OP_reg7]

$C$DW$202	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$202, DW_AT_name("width")
	.dwattr $C$DW$202, DW_AT_location[DW_OP_reg8]

$C$DW$203	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$203, DW_AT_name("height")
	.dwattr $C$DW$203, DW_AT_location[DW_OP_reg9]

$C$DW$204	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$204, DW_AT_name("numChs")
	.dwattr $C$DW$204, DW_AT_location[DW_OP_reg10]

$C$DW$205	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$205, DW_AT_name("numBatches")
	.dwattr $C$DW$205, DW_AT_location[DW_OP_reg11]

$C$DW$206	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$206, DW_AT_name("inLinePitch")
	.dwattr $C$DW$206, DW_AT_location[DW_OP_reg12]

$C$DW$207	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$207, DW_AT_name("outLinePitch")
	.dwattr $C$DW$207, DW_AT_location[DW_OP_bregx 0x6f 48]

$C$DW$208	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$208, DW_AT_name("inChPitch")
	.dwattr $C$DW$208, DW_AT_location[DW_OP_bregx 0x6f 52]

$C$DW$209	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$209, DW_AT_name("outChPitch")
	.dwattr $C$DW$209, DW_AT_location[DW_OP_bregx 0x6f 56]

$C$DW$210	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$210, DW_AT_name("inBatchPitch")
	.dwattr $C$DW$210, DW_AT_location[DW_OP_bregx 0x6f 60]

$C$DW$211	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$211, DW_AT_name("outBatchPitch")
	.dwattr $C$DW$211, DW_AT_location[DW_OP_bregx 0x6f 64]


;******************************************************************************
;* FUNCTION NAME: void TIDL_refBatchReshape<unsigned char, unsigned char>(const T1 *, T2 *, int, int, int, int, int, int, int, int, int, int, int, int)*
;*                                                                            *
;*   Regs Modified     : A0,A1,A2,A3,A11,A12,A13,A14,VB0,VB1,VB2,AL0,AM0,AM1, *
;*                           AM2,AM3,D0,D1,D2,D3,D4,D5,SP,VBM0,VBM1           *
;*   Regs Used         : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,A14,   *
;*                           VB0,VB1,VB2,AL0,AM0,AM1,AM2,AM3,D0,D1,D2,D3,D4,  *
;*                           D5,SP,VBM0,VBM1                                  *
;*   Local Frame Size  : 0 Args + 0 Auto + 32 Save = 32 byte                  *
;******************************************************************************
||_Z20TIDL_refBatchReshapeIhhEvPKT_PT0_iiiiiiiiiiii||:
;** --------------------------------------------------------------------------*
;* A14   assigned to $O$U34
;* A13   assigned to $O$U43
;* A0    assigned to $O$L1
;* A3    assigned to $O$L2
;* A1    assigned to $O$L3
;* A8    assigned to $O$L4
;* A11   assigned to $O$Lr15$i1
;* A12   assigned to $O$Lr18$i2
;* VB2   assigned to $O$Lr21$i3
;* AM3   assigned to outBatchPitch
$C$DW$212	.dwtag  DW_TAG_variable
	.dwattr $C$DW$212, DW_AT_name("outBatchPitch")
	.dwattr $C$DW$212, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$212, DW_AT_location[DW_OP_reg27]

;* AM2   assigned to inBatchPitch
$C$DW$213	.dwtag  DW_TAG_variable
	.dwattr $C$DW$213, DW_AT_name("inBatchPitch")
	.dwattr $C$DW$213, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$213, DW_AT_location[DW_OP_reg26]

;* AM1   assigned to outChPitch
$C$DW$214	.dwtag  DW_TAG_variable
	.dwattr $C$DW$214, DW_AT_name("outChPitch")
	.dwattr $C$DW$214, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$214, DW_AT_location[DW_OP_reg25]

;* AM0   assigned to inChPitch
$C$DW$215	.dwtag  DW_TAG_variable
	.dwattr $C$DW$215, DW_AT_name("inChPitch")
	.dwattr $C$DW$215, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$215, DW_AT_location[DW_OP_reg24]

;* VBM0  assigned to outLinePitch
$C$DW$216	.dwtag  DW_TAG_variable
	.dwattr $C$DW$216, DW_AT_name("outLinePitch")
	.dwattr $C$DW$216, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$216, DW_AT_location[DW_OP_regx 0x58]

;* VBM1  assigned to inLinePitch
$C$DW$217	.dwtag  DW_TAG_variable
	.dwattr $C$DW$217, DW_AT_name("inLinePitch")
	.dwattr $C$DW$217, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$217, DW_AT_location[DW_OP_regx 0x59]

;* A0    assigned to numBatches
$C$DW$218	.dwtag  DW_TAG_variable
	.dwattr $C$DW$218, DW_AT_name("numBatches")
	.dwattr $C$DW$218, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$218, DW_AT_location[DW_OP_reg0]

;* A10   assigned to numChs
$C$DW$219	.dwtag  DW_TAG_variable
	.dwattr $C$DW$219, DW_AT_name("numChs")
	.dwattr $C$DW$219, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$219, DW_AT_location[DW_OP_reg10]

;* A9    assigned to height
$C$DW$220	.dwtag  DW_TAG_variable
	.dwattr $C$DW$220, DW_AT_name("height")
	.dwattr $C$DW$220, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$220, DW_AT_location[DW_OP_reg9]

;* A8    assigned to width
$C$DW$221	.dwtag  DW_TAG_variable
	.dwattr $C$DW$221, DW_AT_name("width")
	.dwattr $C$DW$221, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$221, DW_AT_location[DW_OP_reg8]

;* A7    assigned to outPtrOffset
$C$DW$222	.dwtag  DW_TAG_variable
	.dwattr $C$DW$222, DW_AT_name("outPtrOffset")
	.dwattr $C$DW$222, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$222, DW_AT_location[DW_OP_reg7]

;* A6    assigned to inPtrOffset
$C$DW$223	.dwtag  DW_TAG_variable
	.dwattr $C$DW$223, DW_AT_name("inPtrOffset")
	.dwattr $C$DW$223, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$223, DW_AT_location[DW_OP_reg6]

;* A5    assigned to pOut
$C$DW$224	.dwtag  DW_TAG_variable
	.dwattr $C$DW$224, DW_AT_name("pOut")
	.dwattr $C$DW$224, DW_AT_type(*$C$DW$T$364)
	.dwattr $C$DW$224, DW_AT_location[DW_OP_reg5]

;* A4    assigned to pIn
$C$DW$225	.dwtag  DW_TAG_variable
	.dwattr $C$DW$225, DW_AT_name("pIn")
	.dwattr $C$DW$225, DW_AT_type(*$C$DW$T$1013)
	.dwattr $C$DW$225, DW_AT_location[DW_OP_reg4]

	.dwcfi	cfa_offset, 0
;          EXCLUSIVE CPU CYCLES: 5

           STD     .D1     A11,*SP(8)        ; [A_D1] 
||         STD     .D2X    A12,*SP++(-32)    ; [A_D2] 

	.dwcfi	save_reg_to_mem, 11, 8
	.dwcfi	cfa_offset, 32
	.dwcfi	save_reg_to_mem, 12, -32

           LDW     .D1     *SP(60),AM2       ; [A_D1] |112| 
||         LDW     .D2     *SP(64),AM3       ; [A_D2] |112| 

           LDW     .D1     *SP(52),AM0       ; [A_D1] |112| 
||         MV      .D2     A11,A0            ; [A_D2] |112| 

           LDW     .D1     *SP(48),BM0       ; [A_D1] |112| 
||         LDW     .D2     *SP(56),AM1       ; [A_D2] |112| 
||         CMPGTW  .L1     A0,0,A1           ; [A_L1] |115| 
||         MV      .L2X    A12,BM1           ; [B_L2] |112| 

	.dwpsn	file "src/tidl_batchReshape.c",line 115,column 8,is_stmt,isa 0

   [!A1]   B       .B1     ||$C$L20||        ; [A_B] |115| 
||         STD     .D1     A14,*SP(16)       ; [A_D1] 
||         STD     .D2X    A13,*SP(24)       ; [A_D2] 
||         MVKU32  .L1     0,A11             ; [A_L1] |115| 

	.dwcfi	save_reg_to_mem, 14, 16
	.dwcfi	save_reg_to_mem, 13, 24
	.dwpsn	file "src/tidl_batchReshape.c",line 115,column 16,is_stmt,isa 0
           ; BRANCHCC OCCURS {||$C$L20||}    ; [] |115| 
;** --------------------------------------------------------------------------*
;**   BEGIN LOOP ||$C$L11||
;** --------------------------------------------------------------------------*
||$C$L11||:    
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "src/tidl_batchReshape.c",line 117,column 18,is_stmt,isa 0
           CMPGTW  .L1     A10,0,A1          ; [A_L1] |117| 
   [!A1]   B       .B1     ||$C$L19||        ; [A_B] |117| 
           ; BRANCHCC OCCURS {||$C$L19||}    ; [] |117| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 1
	.dwpsn	file "src/tidl_batchReshape.c",line 117,column 10,is_stmt,isa 0

           MPYWW   .N1     AM2,A11,D5        ; [A_N1] 
||         MPYWW   .M1     AM3,A11,D3        ; [A_M1] 
||         MVKU32  .L1     0,A12             ; [A_L1] |117| 
||         MV      .D1     A10,A3            ; [A_D1] 

;** --------------------------------------------------------------------------*
;**   BEGIN LOOP ||$C$L12||
;** --------------------------------------------------------------------------*
||$C$L12||:    
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "src/tidl_batchReshape.c",line 119,column 20,is_stmt,isa 0
           CMPGTW  .L1     A9,0,A1           ; [A_L1] |119| 
   [!A1]   B       .B1     ||$C$L18||        ; [A_B] |119| 
           ; BRANCHCC OCCURS {||$C$L18||}    ; [] |119| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 1
	.dwpsn	file "src/tidl_batchReshape.c",line 119,column 12,is_stmt,isa 0

           MPYWW   .N1     AM0,A12,D4        ; [A_N1] 
||         MPYWW   .M1     AM1,A12,D2        ; [A_M1] 
||         MVKU32  .L2     0,B2              ; [B_L2] |119| 
||         MV      .D1     A9,A1             ; [A_D1] 

;** --------------------------------------------------------------------------*
;**   BEGIN LOOP ||$C$L13||
;** --------------------------------------------------------------------------*
||$C$L13||:    
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "src/tidl_batchReshape.c",line 121,column 22,is_stmt,isa 0
           CMPGTW  .L1     A8,0,A2           ; [A_L1] |121| 
   [!A2]   B       .B1     ||$C$L17||        ; [A_B] |121| 
           ; BRANCHCC OCCURS {||$C$L17||}    ; [] |121| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 12
           MPYWW   .N2     BM0,B2,B0         ; [B_N2] 
           MPYWW   .N2     BM1,B2,B1         ; [B_N2] 
           MV      .L1X    B0,D1             ; [A_L1] Define a twin register
           MV      .L1X    B1,D0             ; [A_L1] Define a twin register

           ADDW    .D1     D4,D0,D0          ; [A_D1] 
||         ADDW    .D2     D2,D1,D1          ; [A_D2] 

           ADDW    .D1     D3,D1,D1          ; [A_D1] 
||         ADDW    .D2     D5,D0,D0          ; [A_D2] 
||         MV      .L1     A8,AL0            ; [A_L1] 

           ADDW    .D1     A6,D0,D0          ; [A_D1] 
||         ADDW    .D2     A7,D1,D1          ; [A_D2] 
||         EXT     .L1     AL0,0x20,0x20,AL0 ; [A_L1] 

           ADDAB   .D1     A5,D1,A13         ; [A_D1] 
||         ADDAB   .D2     A4,D0,A14         ; [A_D2] 
||         NLCINIT .S1     AL0,0x1,0         ; [A_S1] 
||         UNPROT          0x1               ; [A_U] 

           MV      .D1     A13,D0            ; [A_D1] 
||         MV      .D2     A14,D1            ; [A_D2] 
||         TICK                               ; [A_U] <0,0> 

;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*
;*      Loop found in file               : src/tidl_batchReshape.c
;*      Loop source line                 : 121
;*      Loop opening brace source line   : 122
;*      Loop closing brace source line   : 126
;*      Known Minimum Trip Count         : 1                    
;*      Known Max Trip Count Factor      : 1
;*      Loop Carried Dependency Bound(^) : 7
;*      Unpartitioned Resource Bound     : 1
;*      Partitioned Resource Bound       : 1 (pre-sched)
;*
;*      Searching for software pipeline schedule at ...
;*         ii = 7  Schedule found with 2 iterations in parallel
;*
;*      Partitioned Resource Bound(*)    : 1 (post-sched)
;*
;*      Constant Extension #0 Used [C0]  : 0
;*      Constant Extension #1 Used [C1]  : 0
;*
;*      Resource Partition (may include "post-sched" split/spill moves):
;*
;*                                                A-side   B-side
;*      .L units                                     0        0     
;*      .S units                                     0        0     
;*      .M units                                     0        0     
;*      .N units                                     0        0     
;*      .D units                                     2        -     
;*      .B units                                     1*       -     
;*      .C units                                     -        0     
;*      .P units                                     -        0     
;*
;*      .M/.N units                                  0        0     
;*      .L/.S units                                  0        0     
;*      .L/.S/.C units                               0        0     
;*      .L/.S/.C/.M units                            0        0     
;*      .L/.S/.C/.M/.D units                         0        0     
;*
;*      .X cross paths                               1*       0     
;*
;*      Bound(.D1 .D2 .D)                            1*       -     
;*      Bound(.M .N .MN)                             0        0     
;*      Bound(.L .S .LS)                             0        0     
;*      Bound(.L .S .C .LS .LSC)                     0        0     
;*      Bound(.L .S .C .M .LS .LSC .LSCM)            0        0     
;*      Bound(.L .S .C .M .D .LS .LSC .LSCM .LSCMD)  1*       0     
;*
;*      Register Usage Tables:
;*
;*      +----------------------------------+----------------------------------+
;*      |              ASIDE               |              BSIDE               |
;*      +----------------+--------+--------+----------------+--------+--------+
;*      |      "Axx"     |  "ALx" |  "AMx" |     "VBxx"     | "VBLx" | "VBMx" |
;*      +----------------|--------|--------|----------------|--------|--------+
;*      |0000000000111111|00000000|00000000|0000000000111111|00000000|00000000|
;*      |0123456789012345|01234567|01234567|0123456789012345|01234567|01234567|
;*      +----------------|--------|--------|----------------|--------|--------+
;*   0: |                |        |        |                |        |        |
;*   1: |                |        |        |                |        |        |
;*   2: |                |        |        |                |        |        |
;*   3: |                |        |        |                |        |        |
;*   4: |                |        |        |                |        |        |
;*   5: |                |        |        |                |        |        |
;*   6: |                |        |        |*               |        |        |
;*      +----------------+--------+--------+----------------+--------+--------+
;*
;*      +----------------+                 +--------+       +-------+
;*      |      "Dxx"     |                 |  "Px"  |       |"CUCRx"|
;*      +----------------+                 +--------+       +-------+
;*      |0000000000111111|                 |00000000|       |0000   |
;*      |0123456789012345|                 |01234567|       |0123   |
;*      +----------------+                 +--------+       +-------+
;*   0: |**              |                 |        |       |       |
;*   1: |**              |                 |        |       |       |
;*   2: |**              |                 |        |       |       |
;*   3: |**              |                 |        |       |       |
;*   4: |**              |                 |        |       |       |
;*   5: |**              |                 |        |       |       |
;*   6: |**              |                 |        |       |       |
;*      +----------------+                 +--------+       +-------+
;*
;*      Done
;*
;*      Collapsed epilog stages       : 1
;*      Prolog not removed
;*      Collapsed prolog stages       : 0
;*
;*      Max amt of load speculation   : 0 bytes
;*
;*      Minimum safe trip count       : 1
;*      Min. prof. trip count  (est.) : 2
;*
;*      Mem bank conflicts/iter(est.) : { min 0.000, est 0.000, max 0.000 }
;*      Mem bank perf. penalty (est.) : 0.0%
;*
;*
;*      Total cycles (est.)         : 1 + trip_cnt * 7        
;*----------------------------------------------------------------------------*
;*        SINGLE SCHEDULED ITERATION
;*
;*        ||$C$C163||:
;*   0              TICK                               ; [A_U] 
;*   1              LDUB    .D1     *D1++(1),B0       ; [A_D1] |125|  ^ 
;*   2              NOP     0x5     ; [A_B] 
;*   7              STB     .D1X    B0,*D0++(1)       ; [A_D1] |125|  ^ 
;*     ||           BNL     .B1     ||$C$C163||       ; [A_B] |121| 
;*   8              ; BRANCHCC OCCURS {||$C$C163||}   ; [] |121| 
;*----------------------------------------------------------------------------*
||$C$L14||:    ; PIPED LOOP PROLOG
;** --------------------------------------------------------------------------*
||$C$L15||:    ; PIPED LOOP KERNEL
;          EXCLUSIVE CPU CYCLES: 7
	.dwpsn	file "src/tidl_batchReshape.c",line 125,column 11,is_stmt,isa 0
           LDUB    .D1     *D1++(1),B0       ; [A_D1] |125| <0,1>  ^ 
           NOP             0x5               ; [A_B] 
	.dwpsn	file "src/tidl_batchReshape.c",line 121,column 22,is_stmt,isa 0

           BNL     .B1     ||$C$L15||        ; [A_B] |121| <0,7> 
||         STB     .D1X    B0,*D0++(1)       ; [A_D1] |125| <0,7>  ^ 
||         TICK                               ; [A_U] <1,0> 

;** --------------------------------------------------------------------------*
||$C$L16||:    ; PIPED LOOP EPILOG
;          EXCLUSIVE CPU CYCLES: 1
           PROTCLR                            ; [A_U] 
;** --------------------------------------------------------------------------*
||$C$L17||:    
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "src/tidl_batchReshape.c",line 119,column 20,is_stmt,isa 0
           ADDW    .D1     A1,0xffffffff,A1  ; [A_D1] |119| 

   [ A1]   B       .B1     ||$C$L13||        ; [A_B] |119| 
||         ADDW    .L2     B2,0x1,B2         ; [B_L2] |119| 

           ; BRANCHCC OCCURS {||$C$L13||}    ; [] |119| 
;** --------------------------------------------------------------------------*
||$C$L18||:    
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "src/tidl_batchReshape.c",line 117,column 18,is_stmt,isa 0
           ADDW    .D1     A3,0xffffffff,A3  ; [A_D1] |117| 

   [ A3]   B       .B1     ||$C$L12||        ; [A_B] |117| 
||         ADDW    .D1     A12,0x1,A12       ; [A_D1] |117| 

           ; BRANCHCC OCCURS {||$C$L12||}    ; [] |117| 
;** --------------------------------------------------------------------------*
||$C$L19||:    
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "src/tidl_batchReshape.c",line 115,column 16,is_stmt,isa 0
           ADDW    .D1     A0,0xffffffff,A0  ; [A_D1] |115| 

   [ A0]   B       .B1     ||$C$L11||        ; [A_B] |115| 
||         ADDW    .D1     A11,0x1,A11       ; [A_D1] |115| 

           ; BRANCHCC OCCURS {||$C$L11||}    ; [] |115| 
;** --------------------------------------------------------------------------*
||$C$L20||:    
;          EXCLUSIVE CPU CYCLES: 7

           LDD     .D1     *SP(24),A13       ; [A_D1] 
||         LDD     .D2     *SP(16),A14       ; [A_D2] 
	.dwcfi	restore_reg, 13
	.dwcfi	restore_reg, 14

           LDD     .D1     *SP(40),A11       ; [A_D1] 
||         LDD     .D2     *SP(32),A12       ; [A_D2] 

	.dwcfi	restore_reg, 11
	.dwcfi	restore_reg, 12
$C$DW$226	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$226, DW_AT_low_pc(0x00)
	.dwattr $C$DW$226, DW_AT_TI_return


           RET     .B1     ; [A_B] 
||         ADDD    .D1     SP,0x20,SP        ; [A_D1] 

	.dwcfi	cfa_offset, 0
           ; RETURN OCCURS {RP}              ; [] 
	.dwattr $C$DW$197, DW_AT_TI_end_file("src/tidl_batchReshape.c")
	.dwattr $C$DW$197, DW_AT_TI_end_line(0x82)
	.dwattr $C$DW$197, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$197

	.sect	".text:_Z20TIDL_refBatchReshapeIffEvPKT_PT0_iiiiiiiiiiii"
	.clink
	.global	||_Z20TIDL_refBatchReshapeIffEvPKT_PT0_iiiiiiiiiiii||

$C$DW$227	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$227, DW_AT_name("void TIDL_refBatchReshape")
	.dwattr $C$DW$227, DW_AT_low_pc(||_Z20TIDL_refBatchReshapeIffEvPKT_PT0_iiiiiiiiiiii||)
	.dwattr $C$DW$227, DW_AT_high_pc(0x00)
	.dwattr $C$DW$227, DW_AT_linkage_name("_Z20TIDL_refBatchReshapeIffEvPKT_PT0_iiiiiiiiiiii")
	.dwattr $C$DW$227, DW_AT_external
	.dwattr $C$DW$227, DW_AT_decl_file("src/tidl_batchReshape.c")
	.dwattr $C$DW$227, DW_AT_decl_line(0x60)
	.dwattr $C$DW$227, DW_AT_decl_column(0x06)
	.dwattr $C$DW$227, DW_AT_TI_max_frame_size(0x20)
	.dwpsn	file "src/tidl_batchReshape.c",line 112,column 1,is_stmt,address ||_Z20TIDL_refBatchReshapeIffEvPKT_PT0_iiiiiiiiiiii||,isa 0

	.dwfde $C$DW$CIE, ||_Z20TIDL_refBatchReshapeIffEvPKT_PT0_iiiiiiiiiiii||
$C$DW$228	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$228, DW_AT_name("pIn")
	.dwattr $C$DW$228, DW_AT_location[DW_OP_reg4]

$C$DW$229	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$229, DW_AT_name("pOut")
	.dwattr $C$DW$229, DW_AT_location[DW_OP_reg5]

$C$DW$230	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$230, DW_AT_name("inPtrOffset")
	.dwattr $C$DW$230, DW_AT_location[DW_OP_reg6]

$C$DW$231	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$231, DW_AT_name("outPtrOffset")
	.dwattr $C$DW$231, DW_AT_location[DW_OP_reg7]

$C$DW$232	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$232, DW_AT_name("width")
	.dwattr $C$DW$232, DW_AT_location[DW_OP_reg8]

$C$DW$233	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$233, DW_AT_name("height")
	.dwattr $C$DW$233, DW_AT_location[DW_OP_reg9]

$C$DW$234	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$234, DW_AT_name("numChs")
	.dwattr $C$DW$234, DW_AT_location[DW_OP_reg10]

$C$DW$235	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$235, DW_AT_name("numBatches")
	.dwattr $C$DW$235, DW_AT_location[DW_OP_reg11]

$C$DW$236	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$236, DW_AT_name("inLinePitch")
	.dwattr $C$DW$236, DW_AT_location[DW_OP_reg12]

$C$DW$237	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$237, DW_AT_name("outLinePitch")
	.dwattr $C$DW$237, DW_AT_location[DW_OP_bregx 0x6f 48]

$C$DW$238	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$238, DW_AT_name("inChPitch")
	.dwattr $C$DW$238, DW_AT_location[DW_OP_bregx 0x6f 52]

$C$DW$239	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$239, DW_AT_name("outChPitch")
	.dwattr $C$DW$239, DW_AT_location[DW_OP_bregx 0x6f 56]

$C$DW$240	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$240, DW_AT_name("inBatchPitch")
	.dwattr $C$DW$240, DW_AT_location[DW_OP_bregx 0x6f 60]

$C$DW$241	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$241, DW_AT_name("outBatchPitch")
	.dwattr $C$DW$241, DW_AT_location[DW_OP_bregx 0x6f 64]


;******************************************************************************
;* FUNCTION NAME: void TIDL_refBatchReshape<float, float>(const T1 *, T2 *, int, int, int, int, int, int, int, int, int, int, int, int)*
;*                                                                            *
;*   Regs Modified     : A0,A1,A2,A3,A11,A12,A13,A14,VB0,VB1,VB2,AL0,AM0,AM1, *
;*                           AM2,AM3,AM4,D0,D1,D2,D3,D4,D14,SP,VBM0,VBM1      *
;*   Regs Used         : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,A14,   *
;*                           VB0,VB1,VB2,AL0,AM0,AM1,AM2,AM3,AM4,D0,D1,D2,D3, *
;*                           D4,D14,SP,VBM0,VBM1                              *
;*   Local Frame Size  : 0 Args + 0 Auto + 32 Save = 32 byte                  *
;******************************************************************************
||_Z20TIDL_refBatchReshapeIffEvPKT_PT0_iiiiiiiiiiii||:
;** --------------------------------------------------------------------------*
;* A14   assigned to $O$U37
;* A13   assigned to $O$U48
;* A0    assigned to $O$L1
;* A3    assigned to $O$L2
;* A1    assigned to $O$L3
;* A8    assigned to $O$L4
;* A11   assigned to $O$Lr15$i1
;* A12   assigned to $O$Lr18$i2
;* VB1   assigned to $O$Lr21$i3
;* AM3   assigned to outBatchPitch
$C$DW$242	.dwtag  DW_TAG_variable
	.dwattr $C$DW$242, DW_AT_name("outBatchPitch")
	.dwattr $C$DW$242, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$242, DW_AT_location[DW_OP_reg27]

;* AM2   assigned to inBatchPitch
$C$DW$243	.dwtag  DW_TAG_variable
	.dwattr $C$DW$243, DW_AT_name("inBatchPitch")
	.dwattr $C$DW$243, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$243, DW_AT_location[DW_OP_reg26]

;* AM1   assigned to outChPitch
$C$DW$244	.dwtag  DW_TAG_variable
	.dwattr $C$DW$244, DW_AT_name("outChPitch")
	.dwattr $C$DW$244, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$244, DW_AT_location[DW_OP_reg25]

;* AM0   assigned to inChPitch
$C$DW$245	.dwtag  DW_TAG_variable
	.dwattr $C$DW$245, DW_AT_name("inChPitch")
	.dwattr $C$DW$245, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$245, DW_AT_location[DW_OP_reg24]

;* VBM0  assigned to outLinePitch
$C$DW$246	.dwtag  DW_TAG_variable
	.dwattr $C$DW$246, DW_AT_name("outLinePitch")
	.dwattr $C$DW$246, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$246, DW_AT_location[DW_OP_regx 0x58]

;* VBM1  assigned to inLinePitch
$C$DW$247	.dwtag  DW_TAG_variable
	.dwattr $C$DW$247, DW_AT_name("inLinePitch")
	.dwattr $C$DW$247, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$247, DW_AT_location[DW_OP_regx 0x59]

;* A0    assigned to numBatches
$C$DW$248	.dwtag  DW_TAG_variable
	.dwattr $C$DW$248, DW_AT_name("numBatches")
	.dwattr $C$DW$248, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$248, DW_AT_location[DW_OP_reg0]

;* A10   assigned to numChs
$C$DW$249	.dwtag  DW_TAG_variable
	.dwattr $C$DW$249, DW_AT_name("numChs")
	.dwattr $C$DW$249, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$249, DW_AT_location[DW_OP_reg10]

;* A9    assigned to height
$C$DW$250	.dwtag  DW_TAG_variable
	.dwattr $C$DW$250, DW_AT_name("height")
	.dwattr $C$DW$250, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$250, DW_AT_location[DW_OP_reg9]

;* A8    assigned to width
$C$DW$251	.dwtag  DW_TAG_variable
	.dwattr $C$DW$251, DW_AT_name("width")
	.dwattr $C$DW$251, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$251, DW_AT_location[DW_OP_reg8]

;* A7    assigned to outPtrOffset
$C$DW$252	.dwtag  DW_TAG_variable
	.dwattr $C$DW$252, DW_AT_name("outPtrOffset")
	.dwattr $C$DW$252, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$252, DW_AT_location[DW_OP_reg7]

;* A6    assigned to inPtrOffset
$C$DW$253	.dwtag  DW_TAG_variable
	.dwattr $C$DW$253, DW_AT_name("inPtrOffset")
	.dwattr $C$DW$253, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$253, DW_AT_location[DW_OP_reg6]

;* A5    assigned to pOut
$C$DW$254	.dwtag  DW_TAG_variable
	.dwattr $C$DW$254, DW_AT_name("pOut")
	.dwattr $C$DW$254, DW_AT_type(*$C$DW$T$366)
	.dwattr $C$DW$254, DW_AT_location[DW_OP_reg5]

;* A4    assigned to pIn
$C$DW$255	.dwtag  DW_TAG_variable
	.dwattr $C$DW$255, DW_AT_name("pIn")
	.dwattr $C$DW$255, DW_AT_type(*$C$DW$T$1010)
	.dwattr $C$DW$255, DW_AT_location[DW_OP_reg4]

	.dwcfi	cfa_offset, 0
;          EXCLUSIVE CPU CYCLES: 5

           STD     .D1     A11,*SP(8)        ; [A_D1] 
||         STD     .D2X    A12,*SP++(-32)    ; [A_D2] 

	.dwcfi	save_reg_to_mem, 11, 8
	.dwcfi	cfa_offset, 32
	.dwcfi	save_reg_to_mem, 12, -32
           LDW     .D1     *SP(60),AM2       ; [A_D1] |112| 

           LDW     .D1     *SP(52),AM0       ; [A_D1] |112| 
||         LDW     .D2     *SP(56),AM1       ; [A_D2] |112| 
||         MV      .L1     A11,A0            ; [A_L1] |112| 

           LDW     .D1     *SP(48),BM0       ; [A_D1] |112| 
||         LDW     .D2     *SP(64),AM3       ; [A_D2] |112| 
||         CMPGTW  .L1     A0,0,A1           ; [A_L1] |115| 
||         MV      .L2X    A12,BM1           ; [B_L2] |112| 

	.dwpsn	file "src/tidl_batchReshape.c",line 115,column 8,is_stmt,isa 0

   [!A1]   B       .B1     ||$C$L30||        ; [A_B] |115| 
||         STD     .D1     A14,*SP(16)       ; [A_D1] 
||         STD     .D2X    A13,*SP(24)       ; [A_D2] 
||         MVKU32  .L1     0,A11             ; [A_L1] |115| 

	.dwcfi	save_reg_to_mem, 14, 16
	.dwcfi	save_reg_to_mem, 13, 24
	.dwpsn	file "src/tidl_batchReshape.c",line 115,column 16,is_stmt,isa 0
           ; BRANCHCC OCCURS {||$C$L30||}    ; [] |115| 
;** --------------------------------------------------------------------------*
;**   BEGIN LOOP ||$C$L21||
;** --------------------------------------------------------------------------*
||$C$L21||:    
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "src/tidl_batchReshape.c",line 117,column 18,is_stmt,isa 0
           CMPGTW  .L1     A10,0,A1          ; [A_L1] |117| 
   [!A1]   B       .B1     ||$C$L29||        ; [A_B] |117| 
           ; BRANCHCC OCCURS {||$C$L29||}    ; [] |117| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 1
	.dwpsn	file "src/tidl_batchReshape.c",line 117,column 10,is_stmt,isa 0

           MPYWW   .N1     AM2,A11,D3        ; [A_N1] 
||         MVKU32  .L1     0,A12             ; [A_L1] |117| 
||         MV      .D1     A10,A3            ; [A_D1] 

;** --------------------------------------------------------------------------*
;**   BEGIN LOOP ||$C$L22||
;** --------------------------------------------------------------------------*
||$C$L22||:    
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "src/tidl_batchReshape.c",line 119,column 20,is_stmt,isa 0
           CMPGTW  .L1     A9,0,A1           ; [A_L1] |119| 
   [!A1]   B       .B1     ||$C$L28||        ; [A_B] |119| 
           ; BRANCHCC OCCURS {||$C$L28||}    ; [] |119| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 1
	.dwpsn	file "src/tidl_batchReshape.c",line 119,column 12,is_stmt,isa 0

           MPYWW   .N1     AM1,A12,D2        ; [A_N1] 
||         MPYWW   .M1     AM0,A12,D4        ; [A_M1] 
||         MVKU32  .L2     0,B1              ; [B_L2] |119| 
||         MV      .D1     A9,A1             ; [A_D1] 

;** --------------------------------------------------------------------------*
;**   BEGIN LOOP ||$C$L23||
;** --------------------------------------------------------------------------*
||$C$L23||:    
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "src/tidl_batchReshape.c",line 121,column 22,is_stmt,isa 0
           CMPGTW  .L1     A8,0,A2           ; [A_L1] |121| 
   [!A2]   B       .B1     ||$C$L27||        ; [A_B] |121| 
           ; BRANCHCC OCCURS {||$C$L27||}    ; [] |121| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 10
           MPYWW   .N2     BM1,B1,B0         ; [B_N2] 

           MPYWW   .N2     BM0,B1,B2         ; [B_N2] 
||         MPYWW   .N1     AM3,A11,D0        ; [A_N1] 

           ADDW    .D1     D4,D3,D0          ; [A_D1] 
||         MV      .L1X    B0,D1             ; [A_L1] Define a twin register
||         ADDW    .D2     D2,D0,AM4         ; [A_D2] 

           ADDW    .D1     D1,D0,D0          ; [A_D1] 
||         ADDW    .M1X    B2,AM4,D14        ; [A_M1] 
||         MV      .D2     A8,AL0            ; [A_D2] 

           ADDW    .D1     A7,D14,D1         ; [A_D1] 
||         ADDW    .D2     A6,D0,D0          ; [A_D2] 
||         EXT     .L1     AL0,0x20,0x20,AL0 ; [A_L1] 

           ADDAW   .D1     A5,D1,A13         ; [A_D1] 
||         ADDAW   .D2     A4,D0,A14         ; [A_D2] 
||         NLCINIT .S1     AL0,0x1,0         ; [A_S1] 
||         UNPROT          0x1               ; [A_U] 

           MV      .D1     A14,D1            ; [A_D1] 
||         MV      .D2     A13,D0            ; [A_D2] 
||         TICK                               ; [A_U] <0,0> 

;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*
;*      Loop found in file               : src/tidl_batchReshape.c
;*      Loop source line                 : 121
;*      Loop opening brace source line   : 122
;*      Loop closing brace source line   : 126
;*      Known Minimum Trip Count         : 1                    
;*      Known Max Trip Count Factor      : 1
;*      Loop Carried Dependency Bound(^) : 7
;*      Unpartitioned Resource Bound     : 1
;*      Partitioned Resource Bound       : 1 (pre-sched)
;*
;*      Searching for software pipeline schedule at ...
;*         ii = 7  Schedule found with 2 iterations in parallel
;*
;*      Partitioned Resource Bound(*)    : 1 (post-sched)
;*
;*      Constant Extension #0 Used [C0]  : 0
;*      Constant Extension #1 Used [C1]  : 0
;*
;*      Resource Partition (may include "post-sched" split/spill moves):
;*
;*                                                A-side   B-side
;*      .L units                                     0        0     
;*      .S units                                     0        0     
;*      .M units                                     0        0     
;*      .N units                                     0        0     
;*      .D units                                     2        -     
;*      .B units                                     1*       -     
;*      .C units                                     -        0     
;*      .P units                                     -        0     
;*
;*      .M/.N units                                  0        0     
;*      .L/.S units                                  0        0     
;*      .L/.S/.C units                               0        0     
;*      .L/.S/.C/.M units                            0        0     
;*      .L/.S/.C/.M/.D units                         0        0     
;*
;*      .X cross paths                               1*       0     
;*
;*      Bound(.D1 .D2 .D)                            1*       -     
;*      Bound(.M .N .MN)                             0        0     
;*      Bound(.L .S .LS)                             0        0     
;*      Bound(.L .S .C .LS .LSC)                     0        0     
;*      Bound(.L .S .C .M .LS .LSC .LSCM)            0        0     
;*      Bound(.L .S .C .M .D .LS .LSC .LSCM .LSCMD)  1*       0     
;*
;*      Register Usage Tables:
;*
;*      +----------------------------------+----------------------------------+
;*      |              ASIDE               |              BSIDE               |
;*      +----------------+--------+--------+----------------+--------+--------+
;*      |      "Axx"     |  "ALx" |  "AMx" |     "VBxx"     | "VBLx" | "VBMx" |
;*      +----------------|--------|--------|----------------|--------|--------+
;*      |0000000000111111|00000000|00000000|0000000000111111|00000000|00000000|
;*      |0123456789012345|01234567|01234567|0123456789012345|01234567|01234567|
;*      +----------------|--------|--------|----------------|--------|--------+
;*   0: |                |        |        |                |        |        |
;*   1: |                |        |        |                |        |        |
;*   2: |                |        |        |                |        |        |
;*   3: |                |        |        |                |        |        |
;*   4: |                |        |        |                |        |        |
;*   5: |                |        |        |                |        |        |
;*   6: |                |        |        |*               |        |        |
;*      +----------------+--------+--------+----------------+--------+--------+
;*
;*      +----------------+                 +--------+       +-------+
;*      |      "Dxx"     |                 |  "Px"  |       |"CUCRx"|
;*      +----------------+                 +--------+       +-------+
;*      |0000000000111111|                 |00000000|       |0000   |
;*      |0123456789012345|                 |01234567|       |0123   |
;*      +----------------+                 +--------+       +-------+
;*   0: |**              |                 |        |       |       |
;*   1: |**              |                 |        |       |       |
;*   2: |**              |                 |        |       |       |
;*   3: |**              |                 |        |       |       |
;*   4: |**              |                 |        |       |       |
;*   5: |**              |                 |        |       |       |
;*   6: |**              |                 |        |       |       |
;*      +----------------+                 +--------+       +-------+
;*
;*      Done
;*
;*      Collapsed epilog stages       : 1
;*      Prolog not removed
;*      Collapsed prolog stages       : 0
;*
;*      Max amt of load speculation   : 0 bytes
;*
;*      Minimum safe trip count       : 1
;*      Min. prof. trip count  (est.) : 2
;*
;*      Mem bank conflicts/iter(est.) : { min 0.000, est 0.000, max 0.000 }
;*      Mem bank perf. penalty (est.) : 0.0%
;*
;*
;*      Total cycles (est.)         : 1 + trip_cnt * 7        
;*----------------------------------------------------------------------------*
;*        SINGLE SCHEDULED ITERATION
;*
;*        ||$C$C278||:
;*   0              TICK                               ; [A_U] 
;*   1              LDUW    .D1     *D1++(4),B0       ; [A_D1] |125|  ^ 
;*   2              NOP     0x5     ; [A_B] 
;*   7              STW     .D1X    B0,*D0++(4)       ; [A_D1] |125|  ^ 
;*     ||           BNL     .B1     ||$C$C278||       ; [A_B] |121| 
;*   8              ; BRANCHCC OCCURS {||$C$C278||}   ; [] |121| 
;*----------------------------------------------------------------------------*
||$C$L24||:    ; PIPED LOOP PROLOG
;** --------------------------------------------------------------------------*
||$C$L25||:    ; PIPED LOOP KERNEL
;          EXCLUSIVE CPU CYCLES: 7
	.dwpsn	file "src/tidl_batchReshape.c",line 125,column 11,is_stmt,isa 0
           LDUW    .D1     *D1++(4),B0       ; [A_D1] |125| <0,1>  ^ 
           NOP             0x5               ; [A_B] 
	.dwpsn	file "src/tidl_batchReshape.c",line 121,column 22,is_stmt,isa 0

           BNL     .B1     ||$C$L25||        ; [A_B] |121| <0,7> 
||         STW     .D1X    B0,*D0++(4)       ; [A_D1] |125| <0,7>  ^ 
||         TICK                               ; [A_U] <1,0> 

;** --------------------------------------------------------------------------*
||$C$L26||:    ; PIPED LOOP EPILOG
;          EXCLUSIVE CPU CYCLES: 1
           PROTCLR                            ; [A_U] 
;** --------------------------------------------------------------------------*
||$C$L27||:    
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "src/tidl_batchReshape.c",line 119,column 20,is_stmt,isa 0
           ADDW    .D1     A1,0xffffffff,A1  ; [A_D1] |119| 

   [ A1]   B       .B1     ||$C$L23||        ; [A_B] |119| 
||         ADDW    .L2     B1,0x1,B1         ; [B_L2] |119| 

           ; BRANCHCC OCCURS {||$C$L23||}    ; [] |119| 
;** --------------------------------------------------------------------------*
||$C$L28||:    
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "src/tidl_batchReshape.c",line 117,column 18,is_stmt,isa 0
           ADDW    .D1     A3,0xffffffff,A3  ; [A_D1] |117| 

   [ A3]   B       .B1     ||$C$L22||        ; [A_B] |117| 
||         ADDW    .D1     A12,0x1,A12       ; [A_D1] |117| 

           ; BRANCHCC OCCURS {||$C$L22||}    ; [] |117| 
;** --------------------------------------------------------------------------*
||$C$L29||:    
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "src/tidl_batchReshape.c",line 115,column 16,is_stmt,isa 0
           ADDW    .D1     A0,0xffffffff,A0  ; [A_D1] |115| 

   [ A0]   B       .B1     ||$C$L21||        ; [A_B] |115| 
||         ADDW    .D1     A11,0x1,A11       ; [A_D1] |115| 

           ; BRANCHCC OCCURS {||$C$L21||}    ; [] |115| 
;** --------------------------------------------------------------------------*
||$C$L30||:    
;          EXCLUSIVE CPU CYCLES: 7

           LDD     .D1     *SP(24),A13       ; [A_D1] 
||         LDD     .D2     *SP(16),A14       ; [A_D2] 
	.dwcfi	restore_reg, 13
	.dwcfi	restore_reg, 14

           LDD     .D1     *SP(40),A11       ; [A_D1] 
||         LDD     .D2     *SP(32),A12       ; [A_D2] 

	.dwcfi	restore_reg, 11
	.dwcfi	restore_reg, 12
$C$DW$256	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$256, DW_AT_low_pc(0x00)
	.dwattr $C$DW$256, DW_AT_TI_return


           RET     .B1     ; [A_B] 
||         ADDD    .D1     SP,0x20,SP        ; [A_D1] 

	.dwcfi	cfa_offset, 0
           ; RETURN OCCURS {RP}              ; [] 
	.dwattr $C$DW$227, DW_AT_TI_end_file("src/tidl_batchReshape.c")
	.dwattr $C$DW$227, DW_AT_TI_end_line(0x82)
	.dwattr $C$DW$227, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$227

	.sect	".text:_Z22TIDL_refBatchReshapeiXPvS_iiiiiiiiiiiii"
	.clink
	.global	||_Z22TIDL_refBatchReshapeiXPvS_iiiiiiiiiiiii||

$C$DW$257	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$257, DW_AT_name("TIDL_refBatchReshapeiX")
	.dwattr $C$DW$257, DW_AT_low_pc(||_Z22TIDL_refBatchReshapeiXPvS_iiiiiiiiiiiii||)
	.dwattr $C$DW$257, DW_AT_high_pc(0x00)
	.dwattr $C$DW$257, DW_AT_linkage_name("_Z22TIDL_refBatchReshapeiXPvS_iiiiiiiiiiiii")
	.dwattr $C$DW$257, DW_AT_external
	.dwattr $C$DW$257, DW_AT_decl_file("src/tidl_batchReshape.c")
	.dwattr $C$DW$257, DW_AT_decl_line(0x96)
	.dwattr $C$DW$257, DW_AT_decl_column(0x06)
	.dwattr $C$DW$257, DW_AT_TI_max_frame_size(0x28)
	.dwpsn	file "src/tidl_batchReshape.c",line 167,column 1,is_stmt,address ||_Z22TIDL_refBatchReshapeiXPvS_iiiiiiiiiiiii||,isa 0

	.dwfde $C$DW$CIE, ||_Z22TIDL_refBatchReshapeiXPvS_iiiiiiiiiiiii||
$C$DW$258	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$258, DW_AT_name("pIn")
	.dwattr $C$DW$258, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$258, DW_AT_location[DW_OP_reg4]

$C$DW$259	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$259, DW_AT_name("pOut")
	.dwattr $C$DW$259, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$259, DW_AT_location[DW_OP_reg5]

$C$DW$260	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$260, DW_AT_name("inPtrOffset")
	.dwattr $C$DW$260, DW_AT_location[DW_OP_reg6]

$C$DW$261	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$261, DW_AT_name("outPtrOffset")
	.dwattr $C$DW$261, DW_AT_location[DW_OP_reg7]

$C$DW$262	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$262, DW_AT_name("width")
	.dwattr $C$DW$262, DW_AT_location[DW_OP_reg8]

$C$DW$263	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$263, DW_AT_name("height")
	.dwattr $C$DW$263, DW_AT_location[DW_OP_reg9]

$C$DW$264	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$264, DW_AT_name("numChs")
	.dwattr $C$DW$264, DW_AT_location[DW_OP_reg10]

$C$DW$265	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$265, DW_AT_name("numBatches")
	.dwattr $C$DW$265, DW_AT_location[DW_OP_reg11]

$C$DW$266	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$266, DW_AT_name("inLinePitch")
	.dwattr $C$DW$266, DW_AT_location[DW_OP_reg12]

$C$DW$267	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$267, DW_AT_name("outLinePitch")
	.dwattr $C$DW$267, DW_AT_location[DW_OP_bregx 0x6f 56]

$C$DW$268	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$268, DW_AT_name("inChPitch")
	.dwattr $C$DW$268, DW_AT_location[DW_OP_bregx 0x6f 60]

$C$DW$269	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$269, DW_AT_name("outChPitch")
	.dwattr $C$DW$269, DW_AT_location[DW_OP_bregx 0x6f 64]

$C$DW$270	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$270, DW_AT_name("inBatchPitch")
	.dwattr $C$DW$270, DW_AT_location[DW_OP_bregx 0x6f 68]

$C$DW$271	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$271, DW_AT_name("outBatchPitch")
	.dwattr $C$DW$271, DW_AT_location[DW_OP_bregx 0x6f 72]

$C$DW$272	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$272, DW_AT_name("elementType")
	.dwattr $C$DW$272, DW_AT_location[DW_OP_bregx 0x6f 76]


;******************************************************************************
;* FUNCTION NAME: TIDL_refBatchReshapeiX(void *, void *, int, int, int, int, int, int, int, int, int, int, int, int, int)*
;*                                                                            *
;*   Regs Modified     : A0,A1,A2,A3,A4,A5,A6,A7,A8,A13,VB0,VB1,VB2,VB3,VB4,  *
;*                           VB5,VB6,VB7,VB8,VB9,VB10,VB11,VB12,VB13,AL0,AL1, *
;*                           AL2,AL3,AL4,AL5,AL6,AL7,AM0,AM1,AM2,AM3,AM4,AM5, *
;*                           AM6,AM7,D0,D1,D2,D3,D4,D5,D6,D7,D8,D9,D10,D11,   *
;*                           D12,D13,D14,SP,VBL0,VBL1,VBL2,VBL3,VBL4,VBL5,    *
;*                           VBL6,VBL7,VBM0,VBM1,VBM2,VBM3,VBM4,VBM5,VBM6,    *
;*                           VBM7,P0,P1,P2,P3,P4,P5,P6,P7,CUCR0,CUCR1,CUCR2,  *
;*                           CUCR3                                            *
;*   Regs Used         : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,VB0,   *
;*                           VB1,VB2,VB3,VB4,VB5,VB6,VB7,VB8,VB9,VB10,VB11,   *
;*                           VB12,VB13,AL0,AL1,AL2,AL3,AL4,AL5,AL6,AL7,AM0,   *
;*                           AM1,AM2,AM3,AM4,AM5,AM6,AM7,D0,D1,D2,D3,D4,D5,D6,*
;*                           D7,D8,D9,D10,D11,D12,D13,D14,SP,VBL0,VBL1,VBL2,  *
;*                           VBL3,VBL4,VBL5,VBL6,VBL7,VBM0,VBM1,VBM2,VBM3,    *
;*                           VBM4,VBM5,VBM6,VBM7,P0,P1,P2,P3,P4,P5,P6,P7,     *
;*                           CUCR0,CUCR1,CUCR2,CUCR3                          *
;*   Local Frame Size  : 24 Args + 0 Auto + 16 Save = 40 byte                 *
;******************************************************************************
||_Z22TIDL_refBatchReshapeiXPvS_iiiiiiiiiiiii||:
;** --------------------------------------------------------------------------*
;* VB0   assigned to elementType
$C$DW$273	.dwtag  DW_TAG_variable
	.dwattr $C$DW$273, DW_AT_name("elementType")
	.dwattr $C$DW$273, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$273, DW_AT_location[DW_OP_regx 0x30]

;* VB5   assigned to outBatchPitch
$C$DW$274	.dwtag  DW_TAG_variable
	.dwattr $C$DW$274, DW_AT_name("outBatchPitch")
	.dwattr $C$DW$274, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$274, DW_AT_location[DW_OP_regx 0x35]

;* VB4   assigned to inBatchPitch
$C$DW$275	.dwtag  DW_TAG_variable
	.dwattr $C$DW$275, DW_AT_name("inBatchPitch")
	.dwattr $C$DW$275, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$275, DW_AT_location[DW_OP_regx 0x34]

;* VB3   assigned to outChPitch
$C$DW$276	.dwtag  DW_TAG_variable
	.dwattr $C$DW$276, DW_AT_name("outChPitch")
	.dwattr $C$DW$276, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$276, DW_AT_location[DW_OP_regx 0x33]

;* VB2   assigned to inChPitch
$C$DW$277	.dwtag  DW_TAG_variable
	.dwattr $C$DW$277, DW_AT_name("inChPitch")
	.dwattr $C$DW$277, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$277, DW_AT_location[DW_OP_regx 0x32]

;* VB1   assigned to outLinePitch
$C$DW$278	.dwtag  DW_TAG_variable
	.dwattr $C$DW$278, DW_AT_name("outLinePitch")
	.dwattr $C$DW$278, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$278, DW_AT_location[DW_OP_regx 0x31]

;* D0    assigned to pIn
$C$DW$279	.dwtag  DW_TAG_variable
	.dwattr $C$DW$279, DW_AT_name("pIn")
	.dwattr $C$DW$279, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$279, DW_AT_location[DW_OP_regx 0x60]

	.dwcfi	cfa_offset, 0
;          EXCLUSIVE CPU CYCLES: 10

           STD     .D1     A8,*SP(8)         ; [A_D1] 
||         STD     .D2X    A13,*SP++(-40)    ; [A_D2] 

	.dwcfi	save_reg_to_mem, 8, 8
	.dwcfi	cfa_offset, 40
	.dwcfi	save_reg_to_mem, 13, -40
           LDW     .D1     *SP(76),B0        ; [A_D1] |167| 
           LDW     .D1     *SP(60),B2        ; [A_D1] |167| 

           LDW     .D1     *SP(56),B1        ; [A_D1] |167| 
||         LDW     .D2     *SP(64),B3        ; [A_D2] |167| 

           LDW     .D1     *SP(68),B4        ; [A_D1] |167| 
||         LDW     .D2     *SP(72),B5        ; [A_D2] |167| 

	.dwpsn	file "src/tidl_batchReshape.c",line 173,column 8,is_stmt,isa 0
           ANDW    .L2     B0,0xfffffffe,B6  ; [B_L2] |173| 
	.dwpsn	file "src/tidl_batchReshape.c",line 168,column 3,is_stmt,isa 0
           CMPEQW  .L1X    B0,0x6,A0         ; [A_L1] |168| 
	.dwpsn	file "src/tidl_batchReshape.c",line 167,column 1,is_stmt,isa 0

   [ A0]   B       .B1     ||$C$L33||        ; [A_B] |168| 
||         CMPEQW  .L1X    B6,0,A1           ; [A_L1] |173| 
||         MVC     .S1     RP,A13            ; [A_S1] 
||         MV      .D1     A4,D0             ; [A_D1] |167| 

	.dwcfi	save_reg_to_reg, 4101, 13
	.dwpsn	file "src/tidl_batchReshape.c",line 168,column 3,is_stmt,isa 0
           ; BRANCHCC OCCURS {||$C$L33||}    ; [] |168| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 5
	.dwpsn	file "src/tidl_batchReshape.c",line 178,column 9,is_stmt,isa 0

           CMPEQW  .L2     B0,0x3,BL0        ; [B_L2] |178| 
||         CMPEQW  .S2     B0,0x2,BL1        ; [B_S2] |178| 

           XORD    .L2     BL0,0x1,BL0       ; [B_L2] |178| 
||         XORD    .S2     BL1,0x1,BL1       ; [B_S2] |178| 

           ANDW    .L2     BL1,BL0,B0        ; [B_L2] |178| 
	.dwpsn	file "src/tidl_batchReshape.c",line 173,column 8,is_stmt,isa 0

   [ A1]   B       .B1     ||$C$L32||        ; [A_B] |173| 
||         CMPEQW  .L1X    B0,0,A0           ; [A_L1] |178| 

           ; BRANCHCC OCCURS {||$C$L32||}    ; [] |173| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 1
	.dwpsn	file "src/tidl_batchReshape.c",line 178,column 9,is_stmt,isa 0

   [ A0]   B       .B1     ||$C$L31||        ; [A_B] |178| 
||         ADDKPC  .D1     $PCR_OFFSET(($C$SL1+0)),A1 ; [A_D1] |185| 
||         ADDKPC  .D2     $PCR_OFFSET(($C$SL2+0)),D1 ; [A_D2] |185| 
||         MVKU32  .L1     0,A4              ; [A_L1] |185| 

           ; BRANCHCC OCCURS {||$C$L31||}    ; [] |178| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 3
	.dwpsn	file "src/tidl_batchReshape.c",line 185,column 5,is_stmt,isa 0
           STKW    .D1     0xb9,*SP(32)      ; [A_D1] |185| 
$C$DW$280	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$280, DW_AT_low_pc(0x00)
	.dwattr $C$DW$280, DW_AT_name("_Z11tidl_printfiPKcz")
	.dwattr $C$DW$280, DW_AT_TI_call


           CALL    .B1     ||_Z11tidl_printfiPKcz|| ; [A_B] |185| 
||         STD     .D1     D1,*SP(24)        ; [A_D1] |185| 
||         STD     .D2X    A1,*SP(16)        ; [A_D2] |185| 

$C$RL0:    ; CALL OCCURS (||_Z11tidl_printfiPKcz||) arg:{A4} ret:{}  ; [] |185| 
           B       .B1     ||$C$L34||        ; [A_B] |185| 
           ; BRANCH OCCURS {||$C$L34||}      ; [] |185| 
;** --------------------------------------------------------------------------*
||$C$L31||:    
;          EXCLUSIVE CPU CYCLES: 4
	.dwpsn	file "src/tidl_batchReshape.c",line 180,column 5,is_stmt,isa 0

           STW     .D1X    B2,*SP(20)        ; [A_D1] |180| 
||         STW     .D2     B3,*SP(24)        ; [A_D2] |180| 

           STW     .D1X    B1,*SP(16)        ; [A_D1] |180| 
||         STW     .D2     B4,*SP(28)        ; [A_D2] |180| 

$C$DW$281	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$281, DW_AT_low_pc(0x00)
	.dwattr $C$DW$281, DW_AT_name("_Z20TIDL_refBatchReshapeIttEvPKT_PT0_iiiiiiiiiiii")
	.dwattr $C$DW$281, DW_AT_TI_call


           CALL    .B1     ||_Z20TIDL_refBatchReshapeIttEvPKT_PT0_iiiiiiiiiiii|| ; [A_B] |180| 
||         MV      .D1     D0,A4             ; [A_D1] |180| 
||         STW     .D2     B5,*SP(32)        ; [A_D2] |180| 

$C$RL1:    ; CALL OCCURS (||_Z20TIDL_refBatchReshapeIttEvPKT_PT0_iiiiiiiiiiii||) arg:{A4,A5,A6,A7,A8,A9,A10,A11,A12} ret:{}  ; [] |180| 
           B       .B1     ||$C$L34||        ; [A_B] 
           ; BRANCH OCCURS {||$C$L34||}      ; [] 
;** --------------------------------------------------------------------------*
||$C$L32||:    
;          EXCLUSIVE CPU CYCLES: 4
	.dwpsn	file "src/tidl_batchReshape.c",line 175,column 5,is_stmt,isa 0
           STW     .D1X    B2,*SP(20)        ; [A_D1] |175| 

           STW     .D1X    B1,*SP(16)        ; [A_D1] |175| 
||         STW     .D2     B3,*SP(24)        ; [A_D2] |175| 

$C$DW$282	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$282, DW_AT_low_pc(0x00)
	.dwattr $C$DW$282, DW_AT_name("_Z20TIDL_refBatchReshapeIhhEvPKT_PT0_iiiiiiiiiiii")
	.dwattr $C$DW$282, DW_AT_TI_call


           CALL    .B1     ||_Z20TIDL_refBatchReshapeIhhEvPKT_PT0_iiiiiiiiiiii|| ; [A_B] |175| 
||         STW     .D1X    B5,*SP(32)        ; [A_D1] |175| 
||         STW     .D2     B4,*SP(28)        ; [A_D2] |175| 

$C$RL2:    ; CALL OCCURS (||_Z20TIDL_refBatchReshapeIhhEvPKT_PT0_iiiiiiiiiiii||) arg:{A4,A5,A6,A7,A8,A9,A10,A11,A12} ret:{}  ; [] |175| 
           B       .B1     ||$C$L34||        ; [A_B] 
           ; BRANCH OCCURS {||$C$L34||}      ; [] 
;** --------------------------------------------------------------------------*
||$C$L33||:    
;          EXCLUSIVE CPU CYCLES: 3
	.dwpsn	file "src/tidl_batchReshape.c",line 170,column 5,is_stmt,isa 0
           STW     .D1X    B2,*SP(20)        ; [A_D1] |170| 

           STW     .D1X    B1,*SP(16)        ; [A_D1] |170| 
||         STW     .D2     B3,*SP(24)        ; [A_D2] |170| 

$C$DW$283	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$283, DW_AT_low_pc(0x00)
	.dwattr $C$DW$283, DW_AT_name("_Z20TIDL_refBatchReshapeIffEvPKT_PT0_iiiiiiiiiiii")
	.dwattr $C$DW$283, DW_AT_TI_call


           CALL    .B1     ||_Z20TIDL_refBatchReshapeIffEvPKT_PT0_iiiiiiiiiiii|| ; [A_B] |170| 
||         STW     .D1X    B5,*SP(32)        ; [A_D1] |170| 
||         STW     .D2     B4,*SP(28)        ; [A_D2] |170| 

$C$RL3:    ; CALL OCCURS (||_Z20TIDL_refBatchReshapeIffEvPKT_PT0_iiiiiiiiiiii||) arg:{A4,A5,A6,A7,A8,A9,A10,A11,A12} ret:{}  ; [] |170| 
;** --------------------------------------------------------------------------*
||$C$L34||:    
;          EXCLUSIVE CPU CYCLES: 7
           MVC     .S1     A13,RP            ; [A_S1] 
	.dwcfi	restore_reg, 4101

           LDD     .D1     *SP(40),A13       ; [A_D1] 
||         LDD     .D2     *SP(48),A8        ; [A_D2] 

	.dwcfi	restore_reg, 13
	.dwcfi	restore_reg, 8
$C$DW$284	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$284, DW_AT_low_pc(0x00)
	.dwattr $C$DW$284, DW_AT_TI_return


           RET     .B1     ; [A_B] 
||         ADDD    .D1     SP,0x28,SP        ; [A_D1] 

	.dwcfi	cfa_offset, 0
           ; RETURN OCCURS {RP}              ; [] 
	.dwattr $C$DW$257, DW_AT_TI_end_file("src/tidl_batchReshape.c")
	.dwattr $C$DW$257, DW_AT_TI_end_line(0xbc)
	.dwattr $C$DW$257, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$257

	.sect	".text:_Z24TIDL_batchReshapeProcessP8TIDL_ObjP16sTIDL_AlgLayer_tPK13sTIDL_Layer_tPPvS7_P20sTIDL_sysMemHandle_t"
	.clink
	.global	||_Z24TIDL_batchReshapeProcessP8TIDL_ObjP16sTIDL_AlgLayer_tPK13sTIDL_Layer_tPPvS7_P20sTIDL_sysMemHandle_t||

$C$DW$285	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$285, DW_AT_name("TIDL_batchReshapeProcess")
	.dwattr $C$DW$285, DW_AT_low_pc(||_Z24TIDL_batchReshapeProcessP8TIDL_ObjP16sTIDL_AlgLayer_tPK13sTIDL_Layer_tPPvS7_P20sTIDL_sysMemHandle_t||)
	.dwattr $C$DW$285, DW_AT_high_pc(0x00)
	.dwattr $C$DW$285, DW_AT_linkage_name("_Z24TIDL_batchReshapeProcessP8TIDL_ObjP16sTIDL_AlgLayer_tPK13sTIDL_Layer_tPPvS7_P20sTIDL_sysMemHandle_t")
	.dwattr $C$DW$285, DW_AT_external
	.dwattr $C$DW$285, DW_AT_decl_file("src/tidl_batchReshape.c")
	.dwattr $C$DW$285, DW_AT_decl_line(0xcd)
	.dwattr $C$DW$285, DW_AT_decl_column(0x09)
	.dwattr $C$DW$285, DW_AT_TI_max_frame_size(0x120)
	.dwpsn	file "src/tidl_batchReshape.c",line 211,column 1,is_stmt,address ||_Z24TIDL_batchReshapeProcessP8TIDL_ObjP16sTIDL_AlgLayer_tPK13sTIDL_Layer_tPPvS7_P20sTIDL_sysMemHandle_t||,isa 0

	.dwfde $C$DW$CIE, ||_Z24TIDL_batchReshapeProcessP8TIDL_ObjP16sTIDL_AlgLayer_tPK13sTIDL_Layer_tPPvS7_P20sTIDL_sysMemHandle_t||
$C$DW$286	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$286, DW_AT_name("intAlgHandle")
	.dwattr $C$DW$286, DW_AT_location[DW_OP_reg4]

$C$DW$287	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$287, DW_AT_name("algLayer")
	.dwattr $C$DW$287, DW_AT_location[DW_OP_reg5]

$C$DW$288	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$288, DW_AT_name("tidlLayer")
	.dwattr $C$DW$288, DW_AT_location[DW_OP_reg6]

$C$DW$289	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$289, DW_AT_name("inPtrs")
	.dwattr $C$DW$289, DW_AT_type(*$C$DW$T$284)
	.dwattr $C$DW$289, DW_AT_location[DW_OP_reg7]

$C$DW$290	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$290, DW_AT_name("outPtrs")
	.dwattr $C$DW$290, DW_AT_type(*$C$DW$T$284)
	.dwattr $C$DW$290, DW_AT_location[DW_OP_reg8]

$C$DW$291	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$291, DW_AT_name("sysMems")
	.dwattr $C$DW$291, DW_AT_location[DW_OP_reg9]


;******************************************************************************
;* FUNCTION NAME: TIDL_batchReshapeProcess(TIDL_Obj *, sTIDL_AlgLayer_t *, const sTIDL_Layer_t *, void **, void **, sTIDL_sysMemHandle_t *)*
;*                                                                            *
;*   Regs Modified     : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,A14,   *
;*                           A15,VB0,VB1,VB2,VB3,VB4,VB5,VB6,VB7,VB8,VB9,VB10,*
;*                           VB11,VB12,VB13,VB14,VB15,AL0,AL1,AL2,AL3,AL4,AL5,*
;*                           AL6,AL7,AM0,AM1,AM2,AM3,AM4,AM5,AM6,AM7,D0,D1,D2,*
;*                           D3,D4,D5,D6,D7,D8,D9,D10,D11,D12,D13,D14,SP,VBL0,*
;*                           VBL1,VBL2,VBL3,VBL4,VBL5,VBL6,VBL7,VBM0,VBM1,    *
;*                           VBM2,VBM3,VBM4,VBM5,VBM6,VBM7,P0,P1,P2,P3,P4,P5, *
;*                           P6,P7,CUCR0,CUCR1,CUCR2,CUCR3                    *
;*   Regs Used         : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,A14,   *
;*                           A15,VB0,VB1,VB2,VB3,VB4,VB5,VB6,VB7,VB8,VB9,VB10,*
;*                           VB11,VB12,VB13,VB14,VB15,AL0,AL1,AL2,AL3,AL4,AL5,*
;*                           AL6,AL7,AM0,AM1,AM2,AM3,AM4,AM5,AM6,AM7,D0,D1,D2,*
;*                           D3,D4,D5,D6,D7,D8,D9,D10,D11,D12,D13,D14,SP,VBL0,*
;*                           VBL1,VBL2,VBL3,VBL4,VBL5,VBL6,VBL7,VBM0,VBM1,    *
;*                           VBM2,VBM3,VBM4,VBM5,VBM6,VBM7,P0,P1,P2,P3,P4,P5, *
;*                           P6,P7,CUCR0,CUCR1,CUCR2,CUCR3                    *
;*   Local Frame Size  : 24 Args + 64 Auto + 200 Save = 288 byte              *
;******************************************************************************
||_Z24TIDL_batchReshapeProcessP8TIDL_ObjP16sTIDL_AlgLayer_tPK13sTIDL_Layer_tPPvS7_P20sTIDL_sysMemHandle_t||:
;** --------------------------------------------------------------------------*
;* A11   assigned to $O$C1
;* A10   assigned to $O$C3
;* D0    assigned to $O$U7
;* A3    assigned to $O$U74
;* A2    assigned to $O$U88
;* D0    assigned to $O$Lr74$inDataParams
;* VB14  assigned to $O$Lr7$outBatchPitch
;* VB15  assigned to $O$Lr108$outPitch
;* A10   assigned to $O$Lr143$outPtrOffset
;* A14   assigned to $O$Lr153$numBatchs
;* A9    assigned to $O$Lr154$inPtr
;* A15   assigned to $O$Lr156$outPtr
;* A5    assigned to $O$Lr89$inWidth
;* A4    assigned to $O$Lr165$orgWidth
;* A11   assigned to $O$Lr8$orgWidth
;* A8    assigned to outPtrs
$C$DW$292	.dwtag  DW_TAG_variable
	.dwattr $C$DW$292, DW_AT_name("outPtrs")
	.dwattr $C$DW$292, DW_AT_type(*$C$DW$T$284)
	.dwattr $C$DW$292, DW_AT_location[DW_OP_reg8]

;* A15   assigned to inPtrs
$C$DW$293	.dwtag  DW_TAG_variable
	.dwattr $C$DW$293, DW_AT_name("inPtrs")
	.dwattr $C$DW$293, DW_AT_type(*$C$DW$T$284)
	.dwattr $C$DW$293, DW_AT_location[DW_OP_reg15]

;* A13   assigned to tidlLayer
$C$DW$294	.dwtag  DW_TAG_variable
	.dwattr $C$DW$294, DW_AT_name("tidlLayer")
	.dwattr $C$DW$294, DW_AT_type(*$C$DW$T$1062)
	.dwattr $C$DW$294, DW_AT_location[DW_OP_reg13]

$C$DW$295	.dwtag  DW_TAG_variable
	.dwattr $C$DW$295, DW_AT_name("algLayer")
	.dwattr $C$DW$295, DW_AT_type(*$C$DW$T$361)
	.dwattr $C$DW$295, DW_AT_location[DW_OP_bregx 0x6f 96]

;* A12   assigned to intAlgHandle
$C$DW$296	.dwtag  DW_TAG_variable
	.dwattr $C$DW$296, DW_AT_name("intAlgHandle")
	.dwattr $C$DW$296, DW_AT_type(*$C$DW$T$997)
	.dwattr $C$DW$296, DW_AT_location[DW_OP_reg12]

	.dwcfi	cfa_offset, 0
;          EXCLUSIVE CPU CYCLES: 18

           STD     .D1     A8,*SP(8)         ; [A_D1] 
||         STD     .D2X    A9,*SP++(-288)    ; [A_D2] 

	.dwcfi	save_reg_to_mem, 8, 8
	.dwcfi	cfa_offset, 288
	.dwcfi	save_reg_to_mem, 9, -288

           MV      .D2     A4,A12            ; [A_D2] |211| 
||         STD     .D1     A12,*SP(264)      ; [A_D1] 

	.dwcfi	save_reg_to_mem, 12, 264
	.dwpsn	file "src/tidl_batchReshape.c",line 215,column 3,is_stmt,isa 0
           LDD     .D1     *A12(328),D0      ; [A_D1] |215| 
           LDUW    .D1     *D0(48),BL0       ; [A_D1] |215| 

           STD     .D1     A15,*SP(240)      ; [A_D1] 
||         STD     .D2X    A13,*SP(256)      ; [A_D2] 
	.dwcfi	save_reg_to_mem, 15, 240
	.dwcfi	save_reg_to_mem, 13, 256

           ANDW    .L2     BL0,0x1,B0        ; [B_L2] |215| 
||         MVC     .S1     RP,A13            ; [A_S1] 
||         STD     .D1     A10,*SP(280)      ; [A_D1] 
||         VST64B  .D2     VB14,*SP(104)     ; [A_D2] 
	.dwcfi	save_reg_to_mem, 10, 280
	.dwcfi	save_reg_to_mem, 62, 104

           STD     .D1     A14,*SP(248)      ; [A_D1] 
||         STD     .D2X    A11,*SP(272)      ; [A_D2] 

	.dwcfi	save_reg_to_mem, 14, 248
	.dwcfi	save_reg_to_mem, 11, 272
	.dwpsn	file "src/tidl_batchReshape.c",line 211,column 1,is_stmt,isa 0

           CMPEQW  .L1X    B0,0,A0           ; [A_L1] |215| 
||         MV      .S1     A6,A13            ; [A_S1] |211| 
||         STD     .D1     A13,*SP(232)      ; [A_D1] 
||         VST64B  .D2     VB15,*SP(168)     ; [A_D2] 
	.dwcfi	save_reg_to_mem, 4101, 232
	.dwcfi	save_reg_to_mem, 63, 168

   [!A0]   B       .B1     ||$C$L35||        ; [A_B] |215| 
||         ADDD    .D2     A13,0x688,A11     ; [A_D2] |233| 
||         ADDD    .S1     A13,0xc8,A10      ; [A_S1] |221| 
||         MVKU32  .L2     0x2,B14           ; [B_L2] |245| 
||         MV      .L1     A7,A15            ; [A_L1] |211| 
||         STD     .D1     A5,*SP(96)        ; [A_D1] |211| 

	.dwpsn	file "src/tidl_batchReshape.c",line 215,column 3,is_stmt,isa 0
           ; BRANCHCC OCCURS {||$C$L35||}    ; [] |215| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "src/tidl_batchReshape.c",line 275,column 3,is_stmt,isa 0
$C$DW$297	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$297, DW_AT_low_pc(0x00)
	.dwattr $C$DW$297, DW_AT_name("_Z27TIDL_batchReshapeDspProcessP8TIDL_ObjP16sTIDL_AlgLayer_tPK13sTIDL_Layer_tPPvS7_P20sTIDL_sysMemHandle_t")
	.dwattr $C$DW$297, DW_AT_TI_call

           CALL    .B1     ||_Z27TIDL_batchReshapeDspProcessP8TIDL_ObjP16sTIDL_AlgLayer_tPK13sTIDL_Layer_tPPvS7_P20sTIDL_sysMemHandle_t|| ; [A_B] |275| 
$C$RL4:    ; CALL OCCURS (||_Z27TIDL_batchReshapeDspProcessP8TIDL_ObjP16sTIDL_AlgLayer_tPK13sTIDL_Layer_tPPvS7_P20sTIDL_sysMemHandle_t||) arg:{A4,A5,A6,A7,A8,A9} ret:{A4}  ; [] |275| 
           B       .B1     ||$C$L39||        ; [A_B] |275| 
           ; BRANCH OCCURS {||$C$L39||}      ; [] |275| 
;** --------------------------------------------------------------------------*
||$C$L35||:    
;          EXCLUSIVE CPU CYCLES: 35
	.dwpsn	file "src/tidl_batchReshape.c",line 221,column 40,is_stmt,isa 0

           LDD     .D1     *D0(72),A4        ; [A_D1] |221| 
||         LDW     .D2     *A10(0),A5        ; [A_D2] |221| 

$C$DW$298	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$298, DW_AT_low_pc(0x00)
	.dwattr $C$DW$298, DW_AT_name("_Z18TIDL_getDataParamsP15sTIDL_Network_ti")
	.dwattr $C$DW$298, DW_AT_TI_call

           CALL    .B1     ||_Z18TIDL_getDataParamsP15sTIDL_Network_ti|| ; [A_B] |221| 
$C$RL5:    ; CALL OCCURS (||_Z18TIDL_getDataParamsP15sTIDL_Network_ti||) arg:{A4,A5} ret:{A4}  ; [] |221| 
	.dwpsn	file "src/tidl_batchReshape.c",line 233,column 32,is_stmt,isa 0
           LDD     .D1     *A12(328),D0      ; [A_D1] |233| 
	.dwpsn	file "src/tidl_batchReshape.c",line 221,column 40,is_stmt,isa 0

           LDD     .D1     *D0(72),A4        ; [A_D1] |233| 
||         LDW     .D2     *A11(0),A5        ; [A_D2] |233| 
||         MV      .L1     A4,A9             ; [A_L1] |221| 

	.dwpsn	file "src/tidl_batchReshape.c",line 233,column 32,is_stmt,isa 0
$C$DW$299	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$299, DW_AT_low_pc(0x00)
	.dwattr $C$DW$299, DW_AT_name("_Z18TIDL_getDataParamsP15sTIDL_Network_ti")
	.dwattr $C$DW$299, DW_AT_TI_call

           CALL    .B1     ||_Z18TIDL_getDataParamsP15sTIDL_Network_ti|| ; [A_B] |233| 
$C$RL6:    ; CALL OCCURS (||_Z18TIDL_getDataParamsP15sTIDL_Network_ti||) arg:{A4,A5} ret:{A4}  ; [] |233| 
	.dwpsn	file "src/tidl_batchReshape.c",line 236,column 32,is_stmt,isa 0

           LDW     .D1     *A10(52),AM0      ; [A_D1] |236| 
||         LDW     .D2     *A10(76),A3       ; [A_D2] |236| 

	.dwpsn	file "src/tidl_batchReshape.c",line 234,column 32,is_stmt,isa 0

           LDW     .D1     *A10(44),A2       ; [A_D1] |242| 
||         LDW     .D2     *A10(40),AM2      ; [A_D2] |234| 

	.dwpsn	file "src/tidl_batchReshape.c",line 221,column 40,is_stmt,isa 0

           LDW     .D1     *A11(40),AM1      ; [A_D1] |235| 
||         LDW     .D2     *A10(84),A6       ; [A_D2] |226| 
||         MV      .L1     A9,D0             ; [A_L1] |221| 

	.dwpsn	file "src/tidl_batchReshape.c",line 234,column 32,is_stmt,isa 0

           LDW     .D1     *D0(72),A1        ; [A_D1] |234| 
||         LDW     .D2     *A11(72),B15      ; [A_D2] |235| 

	.dwpsn	file "src/tidl_batchReshape.c",line 227,column 32,is_stmt,isa 0

           MPYWW   .N1     A3,AM0,A14        ; [A_N1] |236| 
||         LDW     .D1     *A10(80),D14      ; [A_D1] |227| 
||         LDW     .D2     *A11(68),B0       ; [A_D2] |231| 

	.dwpsn	file "src/tidl_batchReshape.c",line 230,column 32,is_stmt,isa 0
           LDW     .D1     *D0(68),B1        ; [A_D1] |230| 
	.dwpsn	file "src/tidl_batchReshape.c",line 232,column 32,is_stmt,isa 0

           LDW     .D1     *D0(64),B13       ; [A_D1] |232| 
||         CMPGTW  .L1X    B14,A3,A0         ; [A_L1] |245| 
||         LDW     .D2     *A10(88),A5       ; [A_D2] |242| 

	.dwpsn	file "src/tidl_batchReshape.c",line 234,column 32,is_stmt,isa 0

   [!A0]   LDW     .D1     *A11(76),AL0      ; [A_D1] |245| 
||         LDW     .D2     *A10(36),D1       ; [A_D2] |234| 

	.dwpsn	file "src/tidl_batchReshape.c",line 235,column 32,is_stmt,isa 0

           ADDW    .D2     A14,0xffffffff,AM3 ; [A_D2] |242| 
||         LDW     .D1     *A11(36),A7       ; [A_D1] |235| 

	.dwpsn	file "src/tidl_batchReshape.c",line 226,column 32,is_stmt,isa 0

           MPYWW   .N1     A2,AM3,AL1        ; [A_N1] |242| 
||         MPYWW   .M1     A1,AM2,D2         ; [A_M1] |234| 
||         STD     .D1     A6,*SP(40)        ; [A_D1] |226| 

	.dwpsn	file "src/tidl_batchReshape.c",line 234,column 32,is_stmt,isa 0

           MPYWW   .N1X    B15,AM1,AM1       ; [A_N1] |235| 
||         STD     .D2     B0,*SP(64)        ; [A_D2] |234| 
||         STD     .D1     D14,*SP(48)       ; [A_D1] |234| 

           MV      .L1     A15,A1            ; [A_L1] |234| 
||         STD     .D1X    B1,*SP(56)        ; [A_D1] |242| 
||         STD     .D2X    A1,*SP(80)        ; [A_D2] |236| 

	.dwpsn	file "src/tidl_batchReshape.c",line 240,column 18,is_stmt,isa 0

   [ A0]   MVKU32  .L1     0,A1              ; [A_L1] 
||         LDD     .D1     *A1(0),A9         ; [A_D1] |240| 
||         STD     .D2     B13,*SP(72)       ; [A_D2] |242| 

	.dwpsn	file "src/tidl_batchReshape.c",line 233,column 32,is_stmt,isa 0

   [!A0]   CMPEQW  .L1     AL0,0x1,A1        ; [A_L1] |245| 
||         SUBW    .S1     A5,AL1,AL1        ; [A_S1] |242| 
||         ADDW    .D1     D1,D2,D13         ; [A_D1] |234| 
||         LDW     .D2     *A4(64),B14       ; [A_D2] |233| 

	.dwpsn	file "src/tidl_batchReshape.c",line 235,column 32,is_stmt,isa 0

   [ A1]   B       .B1     ||$C$L36||        ; [A_B] |245| 
||         DIVW    .L1     AL1,A14,A4        ; [A_L1] |242| 
||         ADDW    .M1     A7,AM1,A10        ; [A_M1] |235| 
||         STD     .D1     D13,*SP(88)       ; [A_D1] |242| 
||         LDD     .D2     *A8(0),A15        ; [A_D2] |241| 
||         MV      .S1     A5,A11            ; [A_S1] 

	.dwpsn	file "src/tidl_batchReshape.c",line 245,column 5,is_stmt,isa 0
           ; BRANCHCC OCCURS {||$C$L36||}    ; [] |245| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 16
	.dwpsn	file "src/tidl_batchReshape.c",line 250,column 10,is_stmt,isa 0
           LDW     .D1     *A13(1748),AL0    ; [A_D1] |250| 
           MVKU32  .L1     0x2,AL1           ; [A_L1] |250| 
           CMPGTD  .S1     AL1,AL0,A0        ; [A_S1] |250| 
           CMPEQW  .L1     A3,0x1,A1         ; [A_L1] |250| 
   [ A0]   MVKU32  .L1     0,A1              ; [A_L1] 
	.dwpsn	file "src/tidl_batchReshape.c",line 257,column 7,is_stmt,isa 0
   [!A1]   LDW     .D1     *A13(1716),BL0    ; [A_D1] |257| 
	.dwpsn	file "src/tidl_batchReshape.c",line 252,column 7,is_stmt,isa 0
   [!A0]   ADDW    .D1     A4,A2,A3          ; [A_D1] |252| 
	.dwpsn	file "src/tidl_batchReshape.c",line 256,column 7,is_stmt,isa 0

   [!A1]   ADDW    .D2     A4,A2,A0          ; [A_D2] |256| 
|| [!A0]   STD     .D1     A3,*SP(72)        ; [A_D1] 
|| [!A0]   MV      .L1     A4,A11            ; [A_L1] 

	.dwpsn	file "src/tidl_batchReshape.c",line 257,column 7,is_stmt,isa 0

           B       .B1     ||$C$L37||        ; [A_B] |257| 
|| [!A1]   STD     .D1     A0,*SP(72)        ; [A_D1] |257| 
|| [!A1]   ADDW    .L2X    A4,BL0,B14        ; [B_L2] |257| 
|| [!A1]   MV      .D2     A4,A11            ; [A_D2] 

           ; BRANCH OCCURS {||$C$L37||}      ; [] |257| 
;** --------------------------------------------------------------------------*
||$C$L36||:    
;          EXCLUSIVE CPU CYCLES: 7
	.dwpsn	file "src/tidl_batchReshape.c",line 248,column 7,is_stmt,isa 0
           LDW     .D1     *A13(1716),BL0    ; [A_D1] |248| 
           ADDW    .L2X    A5,BL0,B14        ; [B_L2] |248| 
;** --------------------------------------------------------------------------*
||$C$L37||:    
;          EXCLUSIVE CPU CYCLES: 16
	.dwpsn	file "src/tidl_batchReshape.c",line 260,column 5,is_stmt,isa 0
           LDD     .D1     *A12(328),D0      ; [A_D1] |260| 
           LDW     .D1     *D0(48),BL0       ; [A_D1] |260| 
           ANDW    .L2     BL0,0x2,B0        ; [B_L2] |260| 

           CMPEQW  .L1X    B0,0,A0           ; [A_L1] |260| 
||         MVKU32  .S1     0,A6              ; [A_S1] |264| 

   [ A0]   B       .B1     ||$C$L38||        ; [A_B] |260| 
||         MVKU32  .L1     0,A7              ; [A_L1] |264| 
||         MVKU32  .S1     0x1,A8            ; [A_S1] |264| 
||         MV      .D1     A12,A4            ; [A_D1] |264| 

           ; BRANCHCC OCCURS {||$C$L38||}    ; [] |260| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 12
           LDD     .D1     *SP(96),A0        ; [A_D1] 
	.dwpsn	file "src/tidl_batchReshape.c",line 264,column 7,is_stmt,isa 0
           LDW     .D1     *A0(8456),A5      ; [A_D1] |264| 
$C$DW$300	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$300, DW_AT_low_pc(0x00)
	.dwattr $C$DW$300, DW_AT_name("_Z23TIDL_UpdateScaleFactorsP8TIDL_Objiill")
	.dwattr $C$DW$300, DW_AT_TI_call

           CALL    .B1     ||_Z23TIDL_UpdateScaleFactorsP8TIDL_Objiill|| ; [A_B] |264| 
$C$RL7:    ; CALL OCCURS (||_Z23TIDL_UpdateScaleFactorsP8TIDL_Objiill||) arg:{A4,A5,A6,A7,A8} ret:{}  ; [] |264| 
;** --------------------------------------------------------------------------*
||$C$L38||:    
;          EXCLUSIVE CPU CYCLES: 16
	.dwpsn	file "src/tidl_batchReshape.c",line 271,column 5,is_stmt,isa 0
           LDD     .D1     *SP(72),B13       ; [A_D1] |271| 

           LDD     .D1     *SP(56),B1        ; [A_D1] |271| 
||         LDD     .D2     *SP(64),B0        ; [A_D2] 

           STW     .D1X    B15,*SP(16)       ; [A_D1] |271| 
||         STW     .D2     B14,*SP(32)       ; [A_D2] |271| 

           STW     .D1X    B13,*SP(28)       ; [A_D1] |271| 
||         LDD     .D2     *SP(88),A6        ; [A_D2] |271| 

           STW     .D1X    B1,*SP(20)        ; [A_D1] |271| 
||         STW     .D2     B0,*SP(24)        ; [A_D2] |271| 

           LDW     .D1     *A13(1676),B0     ; [A_D1] |271| 
||         LDD     .D2     *SP(80),A12       ; [A_D2] |271| 

           LDD     .D1     *SP(40),A9        ; [A_D1] |271| 
||         LDD     .D2     *SP(48),A10       ; [A_D2] |271| 
||         MV      .L1     A9,A4             ; [A_L1] |271| 
||         MV      .S1     A10,A7            ; [A_S1] |271| 

$C$DW$301	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$301, DW_AT_low_pc(0x00)
	.dwattr $C$DW$301, DW_AT_name("_Z22TIDL_refBatchReshapeiXPvS_iiiiiiiiiiiii")
	.dwattr $C$DW$301, DW_AT_TI_call


           CALL    .B1     ||_Z22TIDL_refBatchReshapeiXPvS_iiiiiiiiiiiii|| ; [A_B] |271| 
||         STW     .D1X    B0,*SP(36)        ; [A_D1] |271| 
||         MV      .D2     A14,A11           ; [A_D2] |271| 
||         MV      .L1     A15,A5            ; [A_L1] |271| 
||         MV      .S1     A11,A8            ; [A_S1] |271| 

$C$RL8:    ; CALL OCCURS (||_Z22TIDL_refBatchReshapeiXPvS_iiiiiiiiiiiii||) arg:{A4,A5,A6,A7,A8,A9,A10,A11,A12} ret:{}  ; [] |271| 
	.dwpsn	file "src/tidl_batchReshape.c",line 275,column 3,is_stmt,isa 0
           MVKU32  .L1     0,A4              ; [A_L1] |275| 
;** --------------------------------------------------------------------------*
||$C$L39||:    
;          EXCLUSIVE CPU CYCLES: 16

           LDD     .D2     *SP(232),A13      ; [A_D2] 
||         VLD64B  .D1     *SP(104),VB14     ; [A_D1] 

	.dwcfi	restore_reg, 62
           VLD64B  .D1     *SP(168),VB15     ; [A_D1] 
	.dwcfi	restore_reg, 63
           MVC     .S1     A13,RP            ; [A_S1] 
	.dwcfi	restore_reg, 4101

           LDD     .D1     *SP(248),A14      ; [A_D1] 
||         LDD     .D2     *SP(240),A15      ; [A_D2] 
	.dwcfi	restore_reg, 14
	.dwcfi	restore_reg, 15

           LDD     .D1     *SP(272),A11      ; [A_D1] 
||         LDD     .D2     *SP(264),A12      ; [A_D2] 
	.dwcfi	restore_reg, 11
	.dwcfi	restore_reg, 12

           LDD     .D1     *SP(288),A9       ; [A_D1] 
||         LDD     .D2     *SP(280),A10      ; [A_D2] 
	.dwcfi	restore_reg, 9
	.dwcfi	restore_reg, 10

           LDD     .D1     *SP(256),A13      ; [A_D1] 
||         LDD     .D2     *SP(296),A8       ; [A_D2] 

	.dwcfi	restore_reg, 13
	.dwcfi	restore_reg, 8
$C$DW$302	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$302, DW_AT_low_pc(0x00)
	.dwattr $C$DW$302, DW_AT_TI_return


           RET     .B1     ; [A_B] 
||         ADDD    .D1     SP,0x120,SP       ; [A_D1] 

	.dwcfi	cfa_offset, 0
           ; RETURN OCCURS {RP}              ; [] 
	.dwattr $C$DW$285, DW_AT_TI_end_file("src/tidl_batchReshape.c")
	.dwattr $C$DW$285, DW_AT_TI_end_line(0x114)
	.dwattr $C$DW$285, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$285

;******************************************************************************
;* STRINGS                                                                    *
;******************************************************************************
	.sect	".const:.string"
||$C$SL1||:	.string	" Un Supported elementType , Exiting from %s, %d",10,0
||$C$SL2||:	.string	"src/tidl_batchReshape.c",0
;*****************************************************************************
;* UNDEFINED EXTERNAL REFERENCES                                             *
;*****************************************************************************
	.global	||_Z11tidl_printfiPKcz||
	.global	||_Z27TIDL_batchReshapeDspProcessP8TIDL_ObjP16sTIDL_AlgLayer_tPK13sTIDL_Layer_tPPvS7_P20sTIDL_sysMemHandle_t||
	.global	||_Z18TIDL_getDataParamsP15sTIDL_Network_ti||
	.global	||_Z23TIDL_UpdateScaleFactorsP8TIDL_Objiill||
;*****************************************************************************
;* SECTION GROUPS                                                            *
;*****************************************************************************
	.group    "_Z20TIDL_refBatchReshapeIffEvPKT_PT0_iiiiiiiiiiii", 1
	.gmember  ".text:_Z20TIDL_refBatchReshapeIffEvPKT_PT0_iiiiiiiiiiii"
	.endgroup
	.group    "_Z20TIDL_refBatchReshapeIhhEvPKT_PT0_iiiiiiiiiiii", 1
	.gmember  ".text:_Z20TIDL_refBatchReshapeIhhEvPKT_PT0_iiiiiiiiiiii"
	.endgroup
	.group    "_Z20TIDL_refBatchReshapeIttEvPKT_PT0_iiiiiiiiiiii", 1
	.gmember  ".text:_Z20TIDL_refBatchReshapeIttEvPKT_PT0_iiiiiiiiiiii"
	.endgroup

	.dwattr $C$DW$CU, DW_AT_language(DW_LANG_C_plus_plus)

;***************************************************************
;* DWARF CIE ENTRIES                                           *
;***************************************************************

$C$DW$CIE	.dwcie 4101
	.dwcfi	cfa_register, 111
	.dwcfi	cfa_offset, 0
	.dwcfi	same_value, 8
	.dwcfi	same_value, 9
	.dwcfi	same_value, 10
	.dwcfi	same_value, 11
	.dwcfi	same_value, 12
	.dwcfi	same_value, 13
	.dwcfi	same_value, 14
	.dwcfi	same_value, 15
	.dwcfi	same_value, 62
	.dwcfi	same_value, 63
	.dwendentry
	.dwendtag $C$DW$CU


;******************************************************************************
;* TYPE INFORMATION                                                           *
;******************************************************************************

$C$DW$TU$31	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$31

$C$DW$T$31	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$31, DW_AT_name("BBox")
	.dwattr $C$DW$T$31, DW_AT_byte_size(0x10)
$C$DW$303	.dwtag  DW_TAG_member
	.dwattr $C$DW$303, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$303, DW_AT_name("xmin")
	.dwattr $C$DW$303, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$303, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$303, DW_AT_decl_line(0x134)
	.dwattr $C$DW$303, DW_AT_decl_column(0x10)

$C$DW$304	.dwtag  DW_TAG_member
	.dwattr $C$DW$304, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$304, DW_AT_name("ymin")
	.dwattr $C$DW$304, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$304, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$304, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$304, DW_AT_decl_line(0x135)
	.dwattr $C$DW$304, DW_AT_decl_column(0x10)

$C$DW$305	.dwtag  DW_TAG_member
	.dwattr $C$DW$305, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$305, DW_AT_name("xmax")
	.dwattr $C$DW$305, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$305, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$305, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$305, DW_AT_decl_line(0x136)
	.dwattr $C$DW$305, DW_AT_decl_column(0x10)

$C$DW$306	.dwtag  DW_TAG_member
	.dwattr $C$DW$306, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$306, DW_AT_name("ymax")
	.dwattr $C$DW$306, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$306, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$306, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$306, DW_AT_decl_line(0x137)
	.dwattr $C$DW$306, DW_AT_decl_column(0x10)


$C$DW$307	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$307, DW_AT_name("operator =")
	.dwattr $C$DW$307, DW_AT_declaration
	.dwattr $C$DW$307, DW_AT_linkage_name("_ZN4BBoxaSERKS_")
	.dwattr $C$DW$307, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$307, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$308	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$308, DW_AT_type(*$C$DW$T$28)

	.dwendtag $C$DW$307


$C$DW$309	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$309, DW_AT_name("operator =")
	.dwattr $C$DW$309, DW_AT_declaration
	.dwattr $C$DW$309, DW_AT_linkage_name("_ZN4BBoxaSEOS_")
	.dwattr $C$DW$309, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$309, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$310	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$310, DW_AT_type(*$C$DW$T$26)

	.dwendtag $C$DW$309

	.dwattr $C$DW$T$31, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$T$31, DW_AT_decl_line(0x133)
	.dwattr $C$DW$T$31, DW_AT_decl_column(0x01)
	.dwendtag $C$DW$T$31

	.dwendtag $C$DW$TU$31


$C$DW$TU$27	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$27
$C$DW$T$27	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$27, DW_AT_type(*$C$DW$T$31)

	.dwendtag $C$DW$TU$27


$C$DW$TU$28	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$28
$C$DW$T$28	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$28, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$T$28, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$28


$C$DW$TU$604	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$604
$C$DW$T$604	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$604, DW_AT_name("BBox")
	.dwattr $C$DW$T$604, DW_AT_type(*$C$DW$T$31)
	.dwattr $C$DW$T$604, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$T$604, DW_AT_decl_line(0x138)
	.dwattr $C$DW$T$604, DW_AT_decl_column(0x02)

	.dwendtag $C$DW$TU$604


$C$DW$TU$605	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$605
$C$DW$T$605	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$605, DW_AT_type(*$C$DW$T$604)
	.dwattr $C$DW$T$605, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$605


$C$DW$TU$26	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$26
$C$DW$T$26	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$26, DW_AT_type(*$C$DW$T$31)
	.dwattr $C$DW$T$26, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$26


$C$DW$TU$29	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$29

$C$DW$T$29	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$29, DW_AT_type(*$C$DW$T$26)
$C$DW$311	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$311, DW_AT_type(*$C$DW$T$28)

	.dwendtag $C$DW$T$29

	.dwendtag $C$DW$TU$29


$C$DW$TU$30	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$30

$C$DW$T$30	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$30, DW_AT_type(*$C$DW$T$26)
$C$DW$312	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$312, DW_AT_type(*$C$DW$T$26)

	.dwendtag $C$DW$T$30

	.dwendtag $C$DW$TU$30


$C$DW$TU$71	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$71

$C$DW$T$71	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$71, DW_AT_name("IALG_Fxns")
	.dwattr $C$DW$T$71, DW_AT_byte_size(0x48)
$C$DW$313	.dwtag  DW_TAG_member
	.dwattr $C$DW$313, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$313, DW_AT_name("implementationId")
	.dwattr $C$DW$313, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$313, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ivision/ti/xdais/ialg.h")
	.dwattr $C$DW$313, DW_AT_decl_line(0xe4)
	.dwattr $C$DW$313, DW_AT_decl_column(0x0e)

$C$DW$314	.dwtag  DW_TAG_member
	.dwattr $C$DW$314, DW_AT_type(*$C$DW$T$37)
	.dwattr $C$DW$314, DW_AT_name("algActivate")
	.dwattr $C$DW$314, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$314, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$314, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ivision/ti/xdais/ialg.h")
	.dwattr $C$DW$314, DW_AT_decl_line(0x117)
	.dwattr $C$DW$314, DW_AT_decl_column(0x0f)

$C$DW$315	.dwtag  DW_TAG_member
	.dwattr $C$DW$315, DW_AT_type(*$C$DW$T$48)
	.dwattr $C$DW$315, DW_AT_name("algAlloc")
	.dwattr $C$DW$315, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$315, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$315, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ivision/ti/xdais/ialg.h")
	.dwattr $C$DW$315, DW_AT_decl_line(0x167)
	.dwattr $C$DW$315, DW_AT_decl_column(0x13)

$C$DW$316	.dwtag  DW_TAG_member
	.dwattr $C$DW$316, DW_AT_type(*$C$DW$T$55)
	.dwattr $C$DW$316, DW_AT_name("algControl")
	.dwattr $C$DW$316, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$316, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$316, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ivision/ti/xdais/ialg.h")
	.dwattr $C$DW$316, DW_AT_decl_line(0x192)
	.dwattr $C$DW$316, DW_AT_decl_column(0x13)

$C$DW$317	.dwtag  DW_TAG_member
	.dwattr $C$DW$317, DW_AT_type(*$C$DW$T$37)
	.dwattr $C$DW$317, DW_AT_name("algDeactivate")
	.dwattr $C$DW$317, DW_AT_data_member_location[DW_OP_plus_uconst 0x20]
	.dwattr $C$DW$317, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$317, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ivision/ti/xdais/ialg.h")
	.dwattr $C$DW$317, DW_AT_decl_line(0x1ca)
	.dwattr $C$DW$317, DW_AT_decl_column(0x0f)

$C$DW$318	.dwtag  DW_TAG_member
	.dwattr $C$DW$318, DW_AT_type(*$C$DW$T$57)
	.dwattr $C$DW$318, DW_AT_name("algFree")
	.dwattr $C$DW$318, DW_AT_data_member_location[DW_OP_plus_uconst 0x28]
	.dwattr $C$DW$318, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$318, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ivision/ti/xdais/ialg.h")
	.dwattr $C$DW$318, DW_AT_decl_line(0x1f4)
	.dwattr $C$DW$318, DW_AT_decl_column(0x13)

$C$DW$319	.dwtag  DW_TAG_member
	.dwattr $C$DW$319, DW_AT_type(*$C$DW$T$61)
	.dwattr $C$DW$319, DW_AT_name("algInit")
	.dwattr $C$DW$319, DW_AT_data_member_location[DW_OP_plus_uconst 0x30]
	.dwattr $C$DW$319, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$319, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ivision/ti/xdais/ialg.h")
	.dwattr $C$DW$319, DW_AT_decl_line(0x2a3)
	.dwattr $C$DW$319, DW_AT_decl_column(0x13)

$C$DW$320	.dwtag  DW_TAG_member
	.dwattr $C$DW$320, DW_AT_type(*$C$DW$T$63)
	.dwattr $C$DW$320, DW_AT_name("algMoved")
	.dwattr $C$DW$320, DW_AT_data_member_location[DW_OP_plus_uconst 0x38]
	.dwattr $C$DW$320, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$320, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ivision/ti/xdais/ialg.h")
	.dwattr $C$DW$320, DW_AT_decl_line(0x2ba)
	.dwattr $C$DW$320, DW_AT_decl_column(0x0f)

$C$DW$321	.dwtag  DW_TAG_member
	.dwattr $C$DW$321, DW_AT_type(*$C$DW$T$65)
	.dwattr $C$DW$321, DW_AT_name("algNumAlloc")
	.dwattr $C$DW$321, DW_AT_data_member_location[DW_OP_plus_uconst 0x40]
	.dwattr $C$DW$321, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$321, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ivision/ti/xdais/ialg.h")
	.dwattr $C$DW$321, DW_AT_decl_line(0x2f5)
	.dwattr $C$DW$321, DW_AT_decl_column(0x13)


$C$DW$322	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$322, DW_AT_name("operator =")
	.dwattr $C$DW$322, DW_AT_declaration
	.dwattr $C$DW$322, DW_AT_linkage_name("_ZN9IALG_FxnsaSERKS_")
	.dwattr $C$DW$322, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$322, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$323	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$323, DW_AT_type(*$C$DW$T$68)

	.dwendtag $C$DW$322


$C$DW$324	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$324, DW_AT_name("operator =")
	.dwattr $C$DW$324, DW_AT_declaration
	.dwattr $C$DW$324, DW_AT_linkage_name("_ZN9IALG_FxnsaSEOS_")
	.dwattr $C$DW$324, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$324, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$325	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$325, DW_AT_type(*$C$DW$T$66)

	.dwendtag $C$DW$324

	.dwattr $C$DW$T$71, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ivision/ti/xdais/ialg.h")
	.dwattr $C$DW$T$71, DW_AT_decl_line(0xdf)
	.dwattr $C$DW$T$71, DW_AT_decl_column(0x10)
	.dwendtag $C$DW$T$71

	.dwendtag $C$DW$TU$71


$C$DW$TU$67	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$67
$C$DW$T$67	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$67, DW_AT_type(*$C$DW$T$71)

	.dwendtag $C$DW$TU$67


$C$DW$TU$68	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$68
$C$DW$T$68	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$68, DW_AT_type(*$C$DW$T$67)
	.dwattr $C$DW$T$68, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$68


$C$DW$TU$174	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$174
$C$DW$T$174	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$174, DW_AT_name("IALG_Fxns")
	.dwattr $C$DW$T$174, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$T$174, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ivision/ti/xdais/ialg.h")
	.dwattr $C$DW$T$174, DW_AT_decl_line(0x2f6)
	.dwattr $C$DW$T$174, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$174


$C$DW$TU$43	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$43
$C$DW$T$43	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$43, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$T$43, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$43


$C$DW$TU$44	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$44
$C$DW$T$44	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$44, DW_AT_type(*$C$DW$T$43)
	.dwattr $C$DW$T$44, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$44


$C$DW$TU$66	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$66
$C$DW$T$66	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$66, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$T$66, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$66


$C$DW$TU$69	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$69

$C$DW$T$69	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$69, DW_AT_type(*$C$DW$T$66)
$C$DW$326	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$326, DW_AT_type(*$C$DW$T$68)

	.dwendtag $C$DW$T$69

	.dwendtag $C$DW$TU$69


$C$DW$TU$70	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$70

$C$DW$T$70	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$70, DW_AT_type(*$C$DW$T$66)
$C$DW$327	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$327, DW_AT_type(*$C$DW$T$66)

	.dwendtag $C$DW$T$70

	.dwendtag $C$DW$TU$70


$C$DW$TU$107	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$107

$C$DW$T$107	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$107, DW_AT_name("IALG_MemAttrs")
	.dwattr $C$DW$T$107, DW_AT_byte_size(0x04)
$C$DW$328	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$328, DW_AT_name("IALG_SCRATCH")
	.dwattr $C$DW$328, DW_AT_const_value(0x00)
	.dwattr $C$DW$328, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ivision/ti/xdais/ialg.h")
	.dwattr $C$DW$328, DW_AT_decl_line(0x50)
	.dwattr $C$DW$328, DW_AT_decl_column(0x05)

$C$DW$329	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$329, DW_AT_name("IALG_PERSIST")
	.dwattr $C$DW$329, DW_AT_const_value(0x01)
	.dwattr $C$DW$329, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ivision/ti/xdais/ialg.h")
	.dwattr $C$DW$329, DW_AT_decl_line(0x51)
	.dwattr $C$DW$329, DW_AT_decl_column(0x05)

$C$DW$330	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$330, DW_AT_name("IALG_WRITEONCE")
	.dwattr $C$DW$330, DW_AT_const_value(0x02)
	.dwattr $C$DW$330, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ivision/ti/xdais/ialg.h")
	.dwattr $C$DW$330, DW_AT_decl_line(0x52)
	.dwattr $C$DW$330, DW_AT_decl_column(0x05)

$C$DW$331	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$331, DW_AT_name("IALG_MEMATTRS_END")
	.dwattr $C$DW$331, DW_AT_const_value(0x7fffffff)
	.dwattr $C$DW$331, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ivision/ti/xdais/ialg.h")
	.dwattr $C$DW$331, DW_AT_decl_line(0x53)
	.dwattr $C$DW$331, DW_AT_decl_column(0x02)

	.dwattr $C$DW$T$107, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ivision/ti/xdais/ialg.h")
	.dwattr $C$DW$T$107, DW_AT_decl_line(0x4f)
	.dwattr $C$DW$T$107, DW_AT_decl_column(0x0e)
	.dwendtag $C$DW$T$107

	.dwendtag $C$DW$TU$107


$C$DW$TU$108	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$108
$C$DW$T$108	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$108, DW_AT_name("IALG_MemAttrs")
	.dwattr $C$DW$T$108, DW_AT_type(*$C$DW$T$107)
	.dwattr $C$DW$T$108, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ivision/ti/xdais/ialg.h")
	.dwattr $C$DW$T$108, DW_AT_decl_line(0x55)
	.dwattr $C$DW$T$108, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$108


$C$DW$TU$114	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$114

$C$DW$T$114	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$114, DW_AT_name("IALG_MemRec")
	.dwattr $C$DW$T$114, DW_AT_byte_size(0x18)
$C$DW$332	.dwtag  DW_TAG_member
	.dwattr $C$DW$332, DW_AT_type(*$C$DW$T$50)
	.dwattr $C$DW$332, DW_AT_name("size")
	.dwattr $C$DW$332, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$332, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ivision/ti/xdais/ialg.h")
	.dwattr $C$DW$332, DW_AT_decl_line(0x8d)
	.dwattr $C$DW$332, DW_AT_decl_column(0x15)

$C$DW$333	.dwtag  DW_TAG_member
	.dwattr $C$DW$333, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$333, DW_AT_name("alignment")
	.dwattr $C$DW$333, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$333, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$333, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ivision/ti/xdais/ialg.h")
	.dwattr $C$DW$333, DW_AT_decl_line(0x8e)
	.dwattr $C$DW$333, DW_AT_decl_column(0x15)

$C$DW$334	.dwtag  DW_TAG_member
	.dwattr $C$DW$334, DW_AT_type(*$C$DW$T$106)
	.dwattr $C$DW$334, DW_AT_name("space")
	.dwattr $C$DW$334, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$334, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$334, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ivision/ti/xdais/ialg.h")
	.dwattr $C$DW$334, DW_AT_decl_line(0x8f)
	.dwattr $C$DW$334, DW_AT_decl_column(0x15)

$C$DW$335	.dwtag  DW_TAG_member
	.dwattr $C$DW$335, DW_AT_type(*$C$DW$T$108)
	.dwattr $C$DW$335, DW_AT_name("attrs")
	.dwattr $C$DW$335, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$335, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$335, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ivision/ti/xdais/ialg.h")
	.dwattr $C$DW$335, DW_AT_decl_line(0x90)
	.dwattr $C$DW$335, DW_AT_decl_column(0x15)

$C$DW$336	.dwtag  DW_TAG_member
	.dwattr $C$DW$336, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$336, DW_AT_name("base")
	.dwattr $C$DW$336, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$336, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$336, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ivision/ti/xdais/ialg.h")
	.dwattr $C$DW$336, DW_AT_decl_line(0x91)
	.dwattr $C$DW$336, DW_AT_decl_column(0x16)


$C$DW$337	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$337, DW_AT_name("operator =")
	.dwattr $C$DW$337, DW_AT_declaration
	.dwattr $C$DW$337, DW_AT_linkage_name("_ZN11IALG_MemRecaSERKS_")
	.dwattr $C$DW$337, DW_AT_type(*$C$DW$T$109)
	.dwattr $C$DW$337, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$338	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$338, DW_AT_type(*$C$DW$T$111)

	.dwendtag $C$DW$337


$C$DW$339	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$339, DW_AT_name("operator =")
	.dwattr $C$DW$339, DW_AT_declaration
	.dwattr $C$DW$339, DW_AT_linkage_name("_ZN11IALG_MemRecaSEOS_")
	.dwattr $C$DW$339, DW_AT_type(*$C$DW$T$109)
	.dwattr $C$DW$339, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$340	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$340, DW_AT_type(*$C$DW$T$109)

	.dwendtag $C$DW$339

	.dwattr $C$DW$T$114, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ivision/ti/xdais/ialg.h")
	.dwattr $C$DW$T$114, DW_AT_decl_line(0x8c)
	.dwattr $C$DW$T$114, DW_AT_decl_column(0x10)
	.dwendtag $C$DW$T$114

	.dwendtag $C$DW$TU$114


$C$DW$TU$45	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$45
$C$DW$T$45	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$45, DW_AT_name("IALG_MemRec")
	.dwattr $C$DW$T$45, DW_AT_type(*$C$DW$T$114)
	.dwattr $C$DW$T$45, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ivision/ti/xdais/ialg.h")
	.dwattr $C$DW$T$45, DW_AT_decl_line(0x92)
	.dwattr $C$DW$T$45, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$45


$C$DW$TU$58	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$58
$C$DW$T$58	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$58, DW_AT_type(*$C$DW$T$45)

	.dwendtag $C$DW$TU$58


$C$DW$TU$59	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$59
$C$DW$T$59	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$59, DW_AT_type(*$C$DW$T$58)
	.dwattr $C$DW$T$59, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$59


$C$DW$TU$46	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$46
$C$DW$T$46	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$46, DW_AT_type(*$C$DW$T$45)
	.dwattr $C$DW$T$46, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$46


$C$DW$TU$355	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$355

$C$DW$T$355	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$355, DW_AT_type(*$C$DW$T$45)
	.dwattr $C$DW$T$355, DW_AT_byte_size(0x138)
$C$DW$341	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$341, DW_AT_upper_bound(0x0c)

	.dwendtag $C$DW$T$355

	.dwendtag $C$DW$TU$355


$C$DW$TU$110	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$110
$C$DW$T$110	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$110, DW_AT_type(*$C$DW$T$114)

	.dwendtag $C$DW$TU$110


$C$DW$TU$111	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$111
$C$DW$T$111	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$111, DW_AT_type(*$C$DW$T$110)
	.dwattr $C$DW$T$111, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$111


$C$DW$TU$109	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$109
$C$DW$T$109	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$109, DW_AT_type(*$C$DW$T$114)
	.dwattr $C$DW$T$109, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$109


$C$DW$TU$112	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$112

$C$DW$T$112	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$112, DW_AT_type(*$C$DW$T$109)
$C$DW$342	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$342, DW_AT_type(*$C$DW$T$111)

	.dwendtag $C$DW$T$112

	.dwendtag $C$DW$TU$112


$C$DW$TU$113	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$113

$C$DW$T$113	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$113, DW_AT_type(*$C$DW$T$109)
$C$DW$343	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$343, DW_AT_type(*$C$DW$T$109)

	.dwendtag $C$DW$T$113

	.dwendtag $C$DW$TU$113


$C$DW$TU$105	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$105

$C$DW$T$105	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$105, DW_AT_name("IALG_MemSpace")
	.dwattr $C$DW$T$105, DW_AT_byte_size(0x04)
$C$DW$344	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$344, DW_AT_name("IALG_EPROG")
	.dwattr $C$DW$344, DW_AT_const_value(0x18)
	.dwattr $C$DW$344, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ivision/ti/xdais/ialg.h")
	.dwattr $C$DW$344, DW_AT_decl_line(0x61)
	.dwattr $C$DW$344, DW_AT_decl_column(0x05)

$C$DW$345	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$345, DW_AT_name("IALG_IPROG")
	.dwattr $C$DW$345, DW_AT_const_value(0x08)
	.dwattr $C$DW$345, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ivision/ti/xdais/ialg.h")
	.dwattr $C$DW$345, DW_AT_decl_line(0x64)
	.dwattr $C$DW$345, DW_AT_decl_column(0x05)

$C$DW$346	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$346, DW_AT_name("IALG_ESDATA")
	.dwattr $C$DW$346, DW_AT_const_value(0x10)
	.dwattr $C$DW$346, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ivision/ti/xdais/ialg.h")
	.dwattr $C$DW$346, DW_AT_decl_line(0x67)
	.dwattr $C$DW$346, DW_AT_decl_column(0x05)

$C$DW$347	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$347, DW_AT_name("IALG_EXTERNAL")
	.dwattr $C$DW$347, DW_AT_const_value(0x11)
	.dwattr $C$DW$347, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ivision/ti/xdais/ialg.h")
	.dwattr $C$DW$347, DW_AT_decl_line(0x6a)
	.dwattr $C$DW$347, DW_AT_decl_column(0x05)

$C$DW$348	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$348, DW_AT_name("IALG_DARAM0")
	.dwattr $C$DW$348, DW_AT_const_value(0x00)
	.dwattr $C$DW$348, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ivision/ti/xdais/ialg.h")
	.dwattr $C$DW$348, DW_AT_decl_line(0x6d)
	.dwattr $C$DW$348, DW_AT_decl_column(0x05)

$C$DW$349	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$349, DW_AT_name("IALG_DARAM1")
	.dwattr $C$DW$349, DW_AT_const_value(0x01)
	.dwattr $C$DW$349, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ivision/ti/xdais/ialg.h")
	.dwattr $C$DW$349, DW_AT_decl_line(0x6e)
	.dwattr $C$DW$349, DW_AT_decl_column(0x05)

$C$DW$350	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$350, DW_AT_name("IALG_SARAM")
	.dwattr $C$DW$350, DW_AT_const_value(0x02)
	.dwattr $C$DW$350, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ivision/ti/xdais/ialg.h")
	.dwattr $C$DW$350, DW_AT_decl_line(0x70)
	.dwattr $C$DW$350, DW_AT_decl_column(0x05)

$C$DW$351	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$351, DW_AT_name("IALG_SARAM0")
	.dwattr $C$DW$351, DW_AT_const_value(0x02)
	.dwattr $C$DW$351, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ivision/ti/xdais/ialg.h")
	.dwattr $C$DW$351, DW_AT_decl_line(0x71)
	.dwattr $C$DW$351, DW_AT_decl_column(0x05)

$C$DW$352	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$352, DW_AT_name("IALG_SARAM1")
	.dwattr $C$DW$352, DW_AT_const_value(0x03)
	.dwattr $C$DW$352, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ivision/ti/xdais/ialg.h")
	.dwattr $C$DW$352, DW_AT_decl_line(0x72)
	.dwattr $C$DW$352, DW_AT_decl_column(0x05)

$C$DW$353	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$353, DW_AT_name("IALG_DARAM2")
	.dwattr $C$DW$353, DW_AT_const_value(0x04)
	.dwattr $C$DW$353, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ivision/ti/xdais/ialg.h")
	.dwattr $C$DW$353, DW_AT_decl_line(0x74)
	.dwattr $C$DW$353, DW_AT_decl_column(0x05)

$C$DW$354	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$354, DW_AT_name("IALG_SARAM2")
	.dwattr $C$DW$354, DW_AT_const_value(0x05)
	.dwattr $C$DW$354, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ivision/ti/xdais/ialg.h")
	.dwattr $C$DW$354, DW_AT_decl_line(0x75)
	.dwattr $C$DW$354, DW_AT_decl_column(0x05)

$C$DW$355	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$355, DW_AT_name("IALG_MEMSPACE_END")
	.dwattr $C$DW$355, DW_AT_const_value(0x7fffffff)
	.dwattr $C$DW$355, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ivision/ti/xdais/ialg.h")
	.dwattr $C$DW$355, DW_AT_decl_line(0x76)
	.dwattr $C$DW$355, DW_AT_decl_column(0x02)

	.dwattr $C$DW$T$105, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ivision/ti/xdais/ialg.h")
	.dwattr $C$DW$T$105, DW_AT_decl_line(0x60)
	.dwattr $C$DW$T$105, DW_AT_decl_column(0x0e)
	.dwendtag $C$DW$T$105

	.dwendtag $C$DW$TU$105


$C$DW$TU$106	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$106
$C$DW$T$106	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$106, DW_AT_name("IALG_MemSpace")
	.dwattr $C$DW$T$106, DW_AT_type(*$C$DW$T$105)
	.dwattr $C$DW$T$106, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ivision/ti/xdais/ialg.h")
	.dwattr $C$DW$T$106, DW_AT_decl_line(0x78)
	.dwattr $C$DW$T$106, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$106


$C$DW$TU$123	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$123

$C$DW$T$123	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$123, DW_AT_name("IALG_Obj")
	.dwattr $C$DW$T$123, DW_AT_byte_size(0x08)
$C$DW$356	.dwtag  DW_TAG_member
	.dwattr $C$DW$356, DW_AT_type(*$C$DW$T$43)
	.dwattr $C$DW$356, DW_AT_name("fxns")
	.dwattr $C$DW$356, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$356, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ivision/ti/xdais/ialg.h")
	.dwattr $C$DW$356, DW_AT_decl_line(0x9e)
	.dwattr $C$DW$356, DW_AT_decl_column(0x17)


$C$DW$357	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$357, DW_AT_name("operator =")
	.dwattr $C$DW$357, DW_AT_declaration
	.dwattr $C$DW$357, DW_AT_linkage_name("_ZN8IALG_ObjaSERKS_")
	.dwattr $C$DW$357, DW_AT_type(*$C$DW$T$118)
	.dwattr $C$DW$357, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$358	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$358, DW_AT_type(*$C$DW$T$120)

	.dwendtag $C$DW$357


$C$DW$359	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$359, DW_AT_name("operator =")
	.dwattr $C$DW$359, DW_AT_declaration
	.dwattr $C$DW$359, DW_AT_linkage_name("_ZN8IALG_ObjaSEOS_")
	.dwattr $C$DW$359, DW_AT_type(*$C$DW$T$118)
	.dwattr $C$DW$359, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$360	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$360, DW_AT_type(*$C$DW$T$118)

	.dwendtag $C$DW$359

	.dwattr $C$DW$T$123, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ivision/ti/xdais/ialg.h")
	.dwattr $C$DW$T$123, DW_AT_decl_line(0x9d)
	.dwattr $C$DW$T$123, DW_AT_decl_column(0x10)
	.dwendtag $C$DW$T$123

	.dwendtag $C$DW$TU$123


$C$DW$TU$119	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$119
$C$DW$T$119	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$119, DW_AT_type(*$C$DW$T$123)

	.dwendtag $C$DW$TU$119


$C$DW$TU$120	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$120
$C$DW$T$120	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$120, DW_AT_type(*$C$DW$T$119)
	.dwattr $C$DW$T$120, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$120


$C$DW$TU$34	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$34
$C$DW$T$34	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$34, DW_AT_type(*$C$DW$T$123)
	.dwattr $C$DW$T$34, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$34


$C$DW$TU$35	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$35
$C$DW$T$35	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$35, DW_AT_name("IALG_Handle")
	.dwattr $C$DW$T$35, DW_AT_type(*$C$DW$T$34)
	.dwattr $C$DW$T$35, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ivision/ti/xdais/ialg.h")
	.dwattr $C$DW$T$35, DW_AT_decl_line(0xa5)
	.dwattr $C$DW$T$35, DW_AT_decl_column(0x1a)

	.dwendtag $C$DW$TU$35


$C$DW$TU$118	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$118
$C$DW$T$118	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$118, DW_AT_type(*$C$DW$T$123)
	.dwattr $C$DW$T$118, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$118


$C$DW$TU$121	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$121

$C$DW$T$121	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$121, DW_AT_type(*$C$DW$T$118)
$C$DW$361	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$361, DW_AT_type(*$C$DW$T$120)

	.dwendtag $C$DW$T$121

	.dwendtag $C$DW$TU$121


$C$DW$TU$122	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$122

$C$DW$T$122	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$122, DW_AT_type(*$C$DW$T$118)
$C$DW$362	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$362, DW_AT_type(*$C$DW$T$118)

	.dwendtag $C$DW$T$122

	.dwendtag $C$DW$TU$122


$C$DW$TU$130	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$130

$C$DW$T$130	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$130, DW_AT_name("IALG_Params")
	.dwattr $C$DW$T$130, DW_AT_byte_size(0x04)
$C$DW$363	.dwtag  DW_TAG_member
	.dwattr $C$DW$363, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$363, DW_AT_name("size")
	.dwattr $C$DW$363, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$363, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ivision/ti/xdais/ialg.h")
	.dwattr $C$DW$363, DW_AT_decl_line(0xaf)
	.dwattr $C$DW$363, DW_AT_decl_column(0x0e)


$C$DW$364	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$364, DW_AT_name("operator =")
	.dwattr $C$DW$364, DW_AT_declaration
	.dwattr $C$DW$364, DW_AT_linkage_name("_ZN11IALG_ParamsaSERKS_")
	.dwattr $C$DW$364, DW_AT_type(*$C$DW$T$125)
	.dwattr $C$DW$364, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$365	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$365, DW_AT_type(*$C$DW$T$127)

	.dwendtag $C$DW$364


$C$DW$366	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$366, DW_AT_name("operator =")
	.dwattr $C$DW$366, DW_AT_declaration
	.dwattr $C$DW$366, DW_AT_linkage_name("_ZN11IALG_ParamsaSEOS_")
	.dwattr $C$DW$366, DW_AT_type(*$C$DW$T$125)
	.dwattr $C$DW$366, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$367	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$367, DW_AT_type(*$C$DW$T$125)

	.dwendtag $C$DW$366

	.dwattr $C$DW$T$130, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ivision/ti/xdais/ialg.h")
	.dwattr $C$DW$T$130, DW_AT_decl_line(0xae)
	.dwattr $C$DW$T$130, DW_AT_decl_column(0x10)
	.dwendtag $C$DW$T$130

	.dwendtag $C$DW$TU$130


$C$DW$TU$40	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$40
$C$DW$T$40	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$40, DW_AT_name("IALG_Params")
	.dwattr $C$DW$T$40, DW_AT_type(*$C$DW$T$130)
	.dwattr $C$DW$T$40, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ivision/ti/xdais/ialg.h")
	.dwattr $C$DW$T$40, DW_AT_decl_line(0xb0)
	.dwattr $C$DW$T$40, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$40


$C$DW$TU$41	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$41
$C$DW$T$41	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$41, DW_AT_type(*$C$DW$T$40)

	.dwendtag $C$DW$TU$41


$C$DW$TU$42	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$42
$C$DW$T$42	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$42, DW_AT_type(*$C$DW$T$41)
	.dwattr $C$DW$T$42, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$42


$C$DW$TU$188	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$188
$C$DW$T$188	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$188, DW_AT_type(*$C$DW$T$40)
	.dwattr $C$DW$T$188, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$188


$C$DW$TU$126	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$126
$C$DW$T$126	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$126, DW_AT_type(*$C$DW$T$130)

	.dwendtag $C$DW$TU$126


$C$DW$TU$127	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$127
$C$DW$T$127	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$127, DW_AT_type(*$C$DW$T$126)
	.dwattr $C$DW$T$127, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$127


$C$DW$TU$125	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$125
$C$DW$T$125	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$125, DW_AT_type(*$C$DW$T$130)
	.dwattr $C$DW$T$125, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$125


$C$DW$TU$128	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$128

$C$DW$T$128	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$128, DW_AT_type(*$C$DW$T$125)
$C$DW$368	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$368, DW_AT_type(*$C$DW$T$127)

	.dwendtag $C$DW$T$128

	.dwendtag $C$DW$TU$128


$C$DW$TU$129	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$129

$C$DW$T$129	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$129, DW_AT_type(*$C$DW$T$125)
$C$DW$369	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$369, DW_AT_type(*$C$DW$T$125)

	.dwendtag $C$DW$T$129

	.dwendtag $C$DW$TU$129


$C$DW$TU$137	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$137

$C$DW$T$137	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$137, DW_AT_name("IALG_Status")
	.dwattr $C$DW$T$137, DW_AT_byte_size(0x04)
$C$DW$370	.dwtag  DW_TAG_member
	.dwattr $C$DW$370, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$370, DW_AT_name("size")
	.dwattr $C$DW$370, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$370, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ivision/ti/xdais/ialg.h")
	.dwattr $C$DW$370, DW_AT_decl_line(0xba)
	.dwattr $C$DW$370, DW_AT_decl_column(0x0d)


$C$DW$371	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$371, DW_AT_name("operator =")
	.dwattr $C$DW$371, DW_AT_declaration
	.dwattr $C$DW$371, DW_AT_linkage_name("_ZN11IALG_StatusaSERKS_")
	.dwattr $C$DW$371, DW_AT_type(*$C$DW$T$132)
	.dwattr $C$DW$371, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$372	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$372, DW_AT_type(*$C$DW$T$134)

	.dwendtag $C$DW$371


$C$DW$373	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$373, DW_AT_name("operator =")
	.dwattr $C$DW$373, DW_AT_declaration
	.dwattr $C$DW$373, DW_AT_linkage_name("_ZN11IALG_StatusaSEOS_")
	.dwattr $C$DW$373, DW_AT_type(*$C$DW$T$132)
	.dwattr $C$DW$373, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$374	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$374, DW_AT_type(*$C$DW$T$132)

	.dwendtag $C$DW$373

	.dwattr $C$DW$T$137, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ivision/ti/xdais/ialg.h")
	.dwattr $C$DW$T$137, DW_AT_decl_line(0xb9)
	.dwattr $C$DW$T$137, DW_AT_decl_column(0x10)
	.dwendtag $C$DW$T$137

	.dwendtag $C$DW$TU$137


$C$DW$TU$52	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$52
$C$DW$T$52	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$52, DW_AT_name("IALG_Status")
	.dwattr $C$DW$T$52, DW_AT_type(*$C$DW$T$137)
	.dwattr $C$DW$T$52, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ivision/ti/xdais/ialg.h")
	.dwattr $C$DW$T$52, DW_AT_decl_line(0xbb)
	.dwattr $C$DW$T$52, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$52


$C$DW$TU$53	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$53
$C$DW$T$53	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$53, DW_AT_type(*$C$DW$T$52)
	.dwattr $C$DW$T$53, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$53


$C$DW$TU$133	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$133
$C$DW$T$133	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$133, DW_AT_type(*$C$DW$T$137)

	.dwendtag $C$DW$TU$133


$C$DW$TU$134	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$134
$C$DW$T$134	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$134, DW_AT_type(*$C$DW$T$133)
	.dwattr $C$DW$T$134, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$134


$C$DW$TU$132	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$132
$C$DW$T$132	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$132, DW_AT_type(*$C$DW$T$137)
	.dwattr $C$DW$T$132, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$132


$C$DW$TU$135	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$135

$C$DW$T$135	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$135, DW_AT_type(*$C$DW$T$132)
$C$DW$375	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$375, DW_AT_type(*$C$DW$T$134)

	.dwendtag $C$DW$T$135

	.dwendtag $C$DW$TU$135


$C$DW$TU$136	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$136

$C$DW$T$136	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$136, DW_AT_type(*$C$DW$T$132)
$C$DW$376	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$376, DW_AT_type(*$C$DW$T$132)

	.dwendtag $C$DW$T$136

	.dwendtag $C$DW$TU$136


$C$DW$TU$147	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$147

$C$DW$T$147	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$147, DW_AT_name("IVISION_BufDesc")
	.dwattr $C$DW$T$147, DW_AT_byte_size(0x180)
$C$DW$377	.dwtag  DW_TAG_member
	.dwattr $C$DW$377, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$377, DW_AT_name("numPlanes")
	.dwattr $C$DW$377, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$377, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ivision/ivision.h")
	.dwattr $C$DW$377, DW_AT_decl_line(0xf7)
	.dwattr $C$DW$377, DW_AT_decl_column(0x0b)

$C$DW$378	.dwtag  DW_TAG_member
	.dwattr $C$DW$378, DW_AT_type(*$C$DW$T$140)
	.dwattr $C$DW$378, DW_AT_name("bufPlanes")
	.dwattr $C$DW$378, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$378, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$378, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ivision/ivision.h")
	.dwattr $C$DW$378, DW_AT_decl_line(0xfe)
	.dwattr $C$DW$378, DW_AT_decl_column(0x15)

$C$DW$379	.dwtag  DW_TAG_member
	.dwattr $C$DW$379, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$379, DW_AT_name("formatType")
	.dwattr $C$DW$379, DW_AT_data_member_location[DW_OP_plus_uconst 0x170]
	.dwattr $C$DW$379, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$379, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ivision/ivision.h")
	.dwattr $C$DW$379, DW_AT_decl_line(0x100)
	.dwattr $C$DW$379, DW_AT_decl_column(0x0b)

$C$DW$380	.dwtag  DW_TAG_member
	.dwattr $C$DW$380, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$380, DW_AT_name("bufferId")
	.dwattr $C$DW$380, DW_AT_data_member_location[DW_OP_plus_uconst 0x174]
	.dwattr $C$DW$380, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$380, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ivision/ivision.h")
	.dwattr $C$DW$380, DW_AT_decl_line(0x103)
	.dwattr $C$DW$380, DW_AT_decl_column(0x0b)

$C$DW$381	.dwtag  DW_TAG_member
	.dwattr $C$DW$381, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$381, DW_AT_name("reserved")
	.dwattr $C$DW$381, DW_AT_data_member_location[DW_OP_plus_uconst 0x178]
	.dwattr $C$DW$381, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$381, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ivision/ivision.h")
	.dwattr $C$DW$381, DW_AT_decl_line(0x111)
	.dwattr $C$DW$381, DW_AT_decl_column(0x0c)


$C$DW$382	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$382, DW_AT_name("operator =")
	.dwattr $C$DW$382, DW_AT_declaration
	.dwattr $C$DW$382, DW_AT_linkage_name("_ZN15IVISION_BufDescaSERKS_")
	.dwattr $C$DW$382, DW_AT_type(*$C$DW$T$142)
	.dwattr $C$DW$382, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$383	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$383, DW_AT_type(*$C$DW$T$144)

	.dwendtag $C$DW$382


$C$DW$384	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$384, DW_AT_name("operator =")
	.dwattr $C$DW$384, DW_AT_declaration
	.dwattr $C$DW$384, DW_AT_linkage_name("_ZN15IVISION_BufDescaSEOS_")
	.dwattr $C$DW$384, DW_AT_type(*$C$DW$T$142)
	.dwattr $C$DW$384, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$385	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$385, DW_AT_type(*$C$DW$T$142)

	.dwendtag $C$DW$384

	.dwattr $C$DW$T$147, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ivision/ivision.h")
	.dwattr $C$DW$T$147, DW_AT_decl_line(0xf6)
	.dwattr $C$DW$T$147, DW_AT_decl_column(0x01)
	.dwendtag $C$DW$T$147

	.dwendtag $C$DW$TU$147


$C$DW$TU$143	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$143
$C$DW$T$143	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$143, DW_AT_type(*$C$DW$T$147)

	.dwendtag $C$DW$TU$143


$C$DW$TU$144	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$144
$C$DW$T$144	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$144, DW_AT_type(*$C$DW$T$143)
	.dwattr $C$DW$T$144, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$144


$C$DW$TU$152	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$152
$C$DW$T$152	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$152, DW_AT_name("IVISION_BufDesc")
	.dwattr $C$DW$T$152, DW_AT_type(*$C$DW$T$147)
	.dwattr $C$DW$T$152, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ivision/ivision.h")
	.dwattr $C$DW$T$152, DW_AT_decl_line(0x112)
	.dwattr $C$DW$T$152, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$152


$C$DW$TU$153	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$153
$C$DW$T$153	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$153, DW_AT_type(*$C$DW$T$152)
	.dwattr $C$DW$T$153, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$153


$C$DW$TU$154	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$154
$C$DW$T$154	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$154, DW_AT_type(*$C$DW$T$153)
	.dwattr $C$DW$T$154, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$154


$C$DW$TU$142	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$142
$C$DW$T$142	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$142, DW_AT_type(*$C$DW$T$147)
	.dwattr $C$DW$T$142, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$142


$C$DW$TU$145	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$145

$C$DW$T$145	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$145, DW_AT_type(*$C$DW$T$142)
$C$DW$386	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$386, DW_AT_type(*$C$DW$T$144)

	.dwendtag $C$DW$T$145

	.dwendtag $C$DW$TU$145


$C$DW$TU$146	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$146

$C$DW$T$146	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$146, DW_AT_type(*$C$DW$T$142)
$C$DW$387	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$387, DW_AT_type(*$C$DW$T$142)

	.dwendtag $C$DW$T$146

	.dwendtag $C$DW$TU$146


$C$DW$TU$160	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$160

$C$DW$T$160	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$160, DW_AT_name("IVISION_BufDescList")
	.dwattr $C$DW$T$160, DW_AT_byte_size(0x10)
$C$DW$388	.dwtag  DW_TAG_member
	.dwattr $C$DW$388, DW_AT_type(*$C$DW$T$50)
	.dwattr $C$DW$388, DW_AT_name("size")
	.dwattr $C$DW$388, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$388, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ivision/ivision.h")
	.dwattr $C$DW$388, DW_AT_decl_line(0x11c)
	.dwattr $C$DW$388, DW_AT_decl_column(0x0c)

$C$DW$389	.dwtag  DW_TAG_member
	.dwattr $C$DW$389, DW_AT_type(*$C$DW$T$50)
	.dwattr $C$DW$389, DW_AT_name("numBufs")
	.dwattr $C$DW$389, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$389, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$389, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ivision/ivision.h")
	.dwattr $C$DW$389, DW_AT_decl_line(0x11d)
	.dwattr $C$DW$389, DW_AT_decl_column(0x0c)

$C$DW$390	.dwtag  DW_TAG_member
	.dwattr $C$DW$390, DW_AT_type(*$C$DW$T$154)
	.dwattr $C$DW$390, DW_AT_name("bufDesc")
	.dwattr $C$DW$390, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$390, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$390, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ivision/ivision.h")
	.dwattr $C$DW$390, DW_AT_decl_line(0x11e)
	.dwattr $C$DW$390, DW_AT_decl_column(0x15)


$C$DW$391	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$391, DW_AT_name("operator =")
	.dwattr $C$DW$391, DW_AT_declaration
	.dwattr $C$DW$391, DW_AT_linkage_name("_ZN19IVISION_BufDescListaSERKS_")
	.dwattr $C$DW$391, DW_AT_type(*$C$DW$T$155)
	.dwattr $C$DW$391, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$392	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$392, DW_AT_type(*$C$DW$T$157)

	.dwendtag $C$DW$391


$C$DW$393	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$393, DW_AT_name("operator =")
	.dwattr $C$DW$393, DW_AT_declaration
	.dwattr $C$DW$393, DW_AT_linkage_name("_ZN19IVISION_BufDescListaSEOS_")
	.dwattr $C$DW$393, DW_AT_type(*$C$DW$T$155)
	.dwattr $C$DW$393, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$394	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$394, DW_AT_type(*$C$DW$T$155)

	.dwendtag $C$DW$393

	.dwattr $C$DW$T$160, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ivision/ivision.h")
	.dwattr $C$DW$T$160, DW_AT_decl_line(0x11b)
	.dwattr $C$DW$T$160, DW_AT_decl_column(0x01)
	.dwendtag $C$DW$T$160

	.dwendtag $C$DW$TU$160


$C$DW$TU$156	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$156
$C$DW$T$156	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$156, DW_AT_type(*$C$DW$T$160)

	.dwendtag $C$DW$TU$156


$C$DW$TU$157	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$157
$C$DW$T$157	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$157, DW_AT_type(*$C$DW$T$156)
	.dwattr $C$DW$T$157, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$157


$C$DW$TU$177	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$177
$C$DW$T$177	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$177, DW_AT_name("IVISION_BufDescList")
	.dwattr $C$DW$T$177, DW_AT_type(*$C$DW$T$160)
	.dwattr $C$DW$T$177, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ivision/ivision.h")
	.dwattr $C$DW$T$177, DW_AT_decl_line(0x11f)
	.dwattr $C$DW$T$177, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$177


$C$DW$TU$178	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$178
$C$DW$T$178	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$178, DW_AT_name("IVISION_InBufs")
	.dwattr $C$DW$T$178, DW_AT_type(*$C$DW$T$177)
	.dwattr $C$DW$T$178, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ivision/ivision.h")
	.dwattr $C$DW$T$178, DW_AT_decl_line(0x126)
	.dwattr $C$DW$T$178, DW_AT_decl_column(0x1d)

	.dwendtag $C$DW$TU$178


$C$DW$TU$179	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$179
$C$DW$T$179	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$179, DW_AT_type(*$C$DW$T$178)
	.dwattr $C$DW$T$179, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$179


$C$DW$TU$180	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$180
$C$DW$T$180	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$180, DW_AT_name("IVISION_OutBufs")
	.dwattr $C$DW$T$180, DW_AT_type(*$C$DW$T$177)
	.dwattr $C$DW$T$180, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ivision/ivision.h")
	.dwattr $C$DW$T$180, DW_AT_decl_line(0x12d)
	.dwattr $C$DW$T$180, DW_AT_decl_column(0x1d)

	.dwendtag $C$DW$TU$180


$C$DW$TU$181	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$181
$C$DW$T$181	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$181, DW_AT_type(*$C$DW$T$180)
	.dwattr $C$DW$T$181, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$181


$C$DW$TU$155	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$155
$C$DW$T$155	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$155, DW_AT_type(*$C$DW$T$160)
	.dwattr $C$DW$T$155, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$155


$C$DW$TU$158	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$158

$C$DW$T$158	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$158, DW_AT_type(*$C$DW$T$155)
$C$DW$395	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$395, DW_AT_type(*$C$DW$T$157)

	.dwendtag $C$DW$T$158

	.dwendtag $C$DW$TU$158


$C$DW$TU$159	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$159

$C$DW$T$159	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$159, DW_AT_type(*$C$DW$T$155)
$C$DW$396	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$396, DW_AT_type(*$C$DW$T$155)

	.dwendtag $C$DW$T$159

	.dwendtag $C$DW$TU$159


$C$DW$TU$171	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$171

$C$DW$T$171	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$171, DW_AT_name("IVISION_BufPlanes")
	.dwattr $C$DW$T$171, DW_AT_byte_size(0x48)
$C$DW$397	.dwtag  DW_TAG_member
	.dwattr $C$DW$397, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$397, DW_AT_name("buf")
	.dwattr $C$DW$397, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$397, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ivision/ivision.h")
	.dwattr $C$DW$397, DW_AT_decl_line(0xa6)
	.dwattr $C$DW$397, DW_AT_decl_column(0x09)

$C$DW$398	.dwtag  DW_TAG_member
	.dwattr $C$DW$398, DW_AT_type(*$C$DW$T$50)
	.dwattr $C$DW$398, DW_AT_name("width")
	.dwattr $C$DW$398, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$398, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$398, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ivision/ivision.h")
	.dwattr $C$DW$398, DW_AT_decl_line(0xa7)
	.dwattr $C$DW$398, DW_AT_decl_column(0x0c)

$C$DW$399	.dwtag  DW_TAG_member
	.dwattr $C$DW$399, DW_AT_type(*$C$DW$T$50)
	.dwattr $C$DW$399, DW_AT_name("height")
	.dwattr $C$DW$399, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$399, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$399, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ivision/ivision.h")
	.dwattr $C$DW$399, DW_AT_decl_line(0xab)
	.dwattr $C$DW$399, DW_AT_decl_column(0x0c)

$C$DW$400	.dwtag  DW_TAG_member
	.dwattr $C$DW$400, DW_AT_type(*$C$DW$T$165)
	.dwattr $C$DW$400, DW_AT_name("frameROI")
	.dwattr $C$DW$400, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$400, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$400, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ivision/ivision.h")
	.dwattr $C$DW$400, DW_AT_decl_line(0xac)
	.dwattr $C$DW$400, DW_AT_decl_column(0x10)

$C$DW$401	.dwtag  DW_TAG_member
	.dwattr $C$DW$401, DW_AT_type(*$C$DW$T$165)
	.dwattr $C$DW$401, DW_AT_name("subFrameROI")
	.dwattr $C$DW$401, DW_AT_data_member_location[DW_OP_plus_uconst 0x20]
	.dwattr $C$DW$401, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$401, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ivision/ivision.h")
	.dwattr $C$DW$401, DW_AT_decl_line(0xb6)
	.dwattr $C$DW$401, DW_AT_decl_column(0x10)

$C$DW$402	.dwtag  DW_TAG_member
	.dwattr $C$DW$402, DW_AT_type(*$C$DW$T$165)
	.dwattr $C$DW$402, DW_AT_name("freeSubFrameROI")
	.dwattr $C$DW$402, DW_AT_data_member_location[DW_OP_plus_uconst 0x30]
	.dwattr $C$DW$402, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$402, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ivision/ivision.h")
	.dwattr $C$DW$402, DW_AT_decl_line(0xbf)
	.dwattr $C$DW$402, DW_AT_decl_column(0x10)

$C$DW$403	.dwtag  DW_TAG_member
	.dwattr $C$DW$403, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$403, DW_AT_name("planeType")
	.dwattr $C$DW$403, DW_AT_data_member_location[DW_OP_plus_uconst 0x40]
	.dwattr $C$DW$403, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$403, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ivision/ivision.h")
	.dwattr $C$DW$403, DW_AT_decl_line(0xc9)
	.dwattr $C$DW$403, DW_AT_decl_column(0x0b)

$C$DW$404	.dwtag  DW_TAG_member
	.dwattr $C$DW$404, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$404, DW_AT_name("accessMask")
	.dwattr $C$DW$404, DW_AT_data_member_location[DW_OP_plus_uconst 0x44]
	.dwattr $C$DW$404, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$404, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ivision/ivision.h")
	.dwattr $C$DW$404, DW_AT_decl_line(0xcb)
	.dwattr $C$DW$404, DW_AT_decl_column(0x0c)


$C$DW$405	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$405, DW_AT_name("operator =")
	.dwattr $C$DW$405, DW_AT_declaration
	.dwattr $C$DW$405, DW_AT_linkage_name("_ZN17IVISION_BufPlanesaSERKS_")
	.dwattr $C$DW$405, DW_AT_type(*$C$DW$T$166)
	.dwattr $C$DW$405, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$406	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$406, DW_AT_type(*$C$DW$T$168)

	.dwendtag $C$DW$405


$C$DW$407	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$407, DW_AT_name("operator =")
	.dwattr $C$DW$407, DW_AT_declaration
	.dwattr $C$DW$407, DW_AT_linkage_name("_ZN17IVISION_BufPlanesaSEOS_")
	.dwattr $C$DW$407, DW_AT_type(*$C$DW$T$166)
	.dwattr $C$DW$407, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$408	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$408, DW_AT_type(*$C$DW$T$166)

	.dwendtag $C$DW$407

	.dwattr $C$DW$T$171, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ivision/ivision.h")
	.dwattr $C$DW$T$171, DW_AT_decl_line(0xa5)
	.dwattr $C$DW$T$171, DW_AT_decl_column(0x01)
	.dwendtag $C$DW$T$171

	.dwendtag $C$DW$TU$171


$C$DW$TU$139	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$139
$C$DW$T$139	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$139, DW_AT_name("IVISION_BufPlanes")
	.dwattr $C$DW$T$139, DW_AT_type(*$C$DW$T$171)
	.dwattr $C$DW$T$139, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ivision/ivision.h")
	.dwattr $C$DW$T$139, DW_AT_decl_line(0xec)
	.dwattr $C$DW$T$139, DW_AT_decl_column(0x02)

	.dwendtag $C$DW$TU$139


$C$DW$TU$140	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$140

$C$DW$T$140	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$140, DW_AT_type(*$C$DW$T$139)
	.dwattr $C$DW$T$140, DW_AT_byte_size(0x168)
$C$DW$409	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$409, DW_AT_upper_bound(0x04)

	.dwendtag $C$DW$T$140

	.dwendtag $C$DW$TU$140


$C$DW$TU$167	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$167
$C$DW$T$167	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$167, DW_AT_type(*$C$DW$T$171)

	.dwendtag $C$DW$TU$167


$C$DW$TU$168	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$168
$C$DW$T$168	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$168, DW_AT_type(*$C$DW$T$167)
	.dwattr $C$DW$T$168, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$168


$C$DW$TU$166	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$166
$C$DW$T$166	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$166, DW_AT_type(*$C$DW$T$171)
	.dwattr $C$DW$T$166, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$166


$C$DW$TU$169	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$169

$C$DW$T$169	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$169, DW_AT_type(*$C$DW$T$166)
$C$DW$410	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$410, DW_AT_type(*$C$DW$T$168)

	.dwendtag $C$DW$T$169

	.dwendtag $C$DW$TU$169


$C$DW$TU$170	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$170

$C$DW$T$170	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$170, DW_AT_type(*$C$DW$T$166)
$C$DW$411	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$411, DW_AT_type(*$C$DW$T$166)

	.dwendtag $C$DW$T$170

	.dwendtag $C$DW$TU$170


$C$DW$TU$196	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$196

$C$DW$T$196	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$196, DW_AT_name("IVISION_Fxns")
	.dwattr $C$DW$T$196, DW_AT_byte_size(0x58)
$C$DW$412	.dwtag  DW_TAG_member
	.dwattr $C$DW$412, DW_AT_type(*$C$DW$T$174)
	.dwattr $C$DW$412, DW_AT_name("ialg")
	.dwattr $C$DW$412, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$412, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ivision/ivision.h")
	.dwattr $C$DW$412, DW_AT_decl_line(0x191)
	.dwattr $C$DW$412, DW_AT_decl_column(0x0e)

$C$DW$413	.dwtag  DW_TAG_member
	.dwattr $C$DW$413, DW_AT_type(*$C$DW$T$187)
	.dwattr $C$DW$413, DW_AT_name("algProcess")
	.dwattr $C$DW$413, DW_AT_data_member_location[DW_OP_plus_uconst 0x48]
	.dwattr $C$DW$413, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$413, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ivision/ivision.h")
	.dwattr $C$DW$413, DW_AT_decl_line(0x1a3)
	.dwattr $C$DW$413, DW_AT_decl_column(0x0d)

$C$DW$414	.dwtag  DW_TAG_member
	.dwattr $C$DW$414, DW_AT_type(*$C$DW$T$190)
	.dwattr $C$DW$414, DW_AT_name("algControl")
	.dwattr $C$DW$414, DW_AT_data_member_location[DW_OP_plus_uconst 0x50]
	.dwattr $C$DW$414, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$414, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ivision/ivision.h")
	.dwattr $C$DW$414, DW_AT_decl_line(0x1de)
	.dwattr $C$DW$414, DW_AT_decl_column(0x0d)


$C$DW$415	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$415, DW_AT_name("operator =")
	.dwattr $C$DW$415, DW_AT_declaration
	.dwattr $C$DW$415, DW_AT_linkage_name("_ZN12IVISION_FxnsaSERKS_")
	.dwattr $C$DW$415, DW_AT_type(*$C$DW$T$191)
	.dwattr $C$DW$415, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$416	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$416, DW_AT_type(*$C$DW$T$193)

	.dwendtag $C$DW$415


$C$DW$417	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$417, DW_AT_name("operator =")
	.dwattr $C$DW$417, DW_AT_declaration
	.dwattr $C$DW$417, DW_AT_linkage_name("_ZN12IVISION_FxnsaSEOS_")
	.dwattr $C$DW$417, DW_AT_type(*$C$DW$T$191)
	.dwattr $C$DW$417, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$418	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$418, DW_AT_type(*$C$DW$T$191)

	.dwendtag $C$DW$417

	.dwattr $C$DW$T$196, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ivision/ivision.h")
	.dwattr $C$DW$T$196, DW_AT_decl_line(0x190)
	.dwattr $C$DW$T$196, DW_AT_decl_column(0x10)
	.dwendtag $C$DW$T$196

	.dwendtag $C$DW$TU$196


$C$DW$TU$192	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$192
$C$DW$T$192	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$192, DW_AT_type(*$C$DW$T$196)

	.dwendtag $C$DW$TU$192


$C$DW$TU$193	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$193
$C$DW$T$193	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$193, DW_AT_type(*$C$DW$T$192)
	.dwattr $C$DW$T$193, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$193


$C$DW$TU$352	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$352
$C$DW$T$352	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$352, DW_AT_name("IVISION_Fxns")
	.dwattr $C$DW$T$352, DW_AT_type(*$C$DW$T$196)
	.dwattr $C$DW$T$352, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ivision/ivision.h")
	.dwattr $C$DW$T$352, DW_AT_decl_line(0x1e2)
	.dwattr $C$DW$T$352, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$352


$C$DW$TU$353	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$353
$C$DW$T$353	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$353, DW_AT_type(*$C$DW$T$352)

	.dwendtag $C$DW$TU$353


$C$DW$TU$354	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$354
$C$DW$T$354	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$354, DW_AT_type(*$C$DW$T$353)
	.dwattr $C$DW$T$354, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$354


$C$DW$TU$191	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$191
$C$DW$T$191	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$191, DW_AT_type(*$C$DW$T$196)
	.dwattr $C$DW$T$191, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$191


$C$DW$TU$194	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$194

$C$DW$T$194	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$194, DW_AT_type(*$C$DW$T$191)
$C$DW$419	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$419, DW_AT_type(*$C$DW$T$193)

	.dwendtag $C$DW$T$194

	.dwendtag $C$DW$TU$194


$C$DW$TU$195	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$195

$C$DW$T$195	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$195, DW_AT_type(*$C$DW$T$191)
$C$DW$420	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$420, DW_AT_type(*$C$DW$T$191)

	.dwendtag $C$DW$T$195

	.dwendtag $C$DW$TU$195


$C$DW$TU$222	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$222
$C$DW$T$222	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$222, DW_AT_type(*$C$DW$T$196)
	.dwattr $C$DW$T$222, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$222


$C$DW$TU$220	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$220

$C$DW$T$220	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$220, DW_AT_name("IVISION_InArgs")
	.dwattr $C$DW$T$220, DW_AT_byte_size(0x08)
$C$DW$421	.dwtag  DW_TAG_member
	.dwattr $C$DW$421, DW_AT_type(*$C$DW$T$50)
	.dwattr $C$DW$421, DW_AT_name("size")
	.dwattr $C$DW$421, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$421, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ivision/ivision.h")
	.dwattr $C$DW$421, DW_AT_decl_line(0x136)
	.dwattr $C$DW$421, DW_AT_decl_column(0x0c)

$C$DW$422	.dwtag  DW_TAG_member
	.dwattr $C$DW$422, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$422, DW_AT_name("subFrameInfo")
	.dwattr $C$DW$422, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$422, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$422, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ivision/ivision.h")
	.dwattr $C$DW$422, DW_AT_decl_line(0x137)
	.dwattr $C$DW$422, DW_AT_decl_column(0x0c)


$C$DW$423	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$423, DW_AT_name("operator =")
	.dwattr $C$DW$423, DW_AT_declaration
	.dwattr $C$DW$423, DW_AT_linkage_name("_ZN14IVISION_InArgsaSERKS_")
	.dwattr $C$DW$423, DW_AT_type(*$C$DW$T$215)
	.dwattr $C$DW$423, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$424	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$424, DW_AT_type(*$C$DW$T$217)

	.dwendtag $C$DW$423


$C$DW$425	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$425, DW_AT_name("operator =")
	.dwattr $C$DW$425, DW_AT_declaration
	.dwattr $C$DW$425, DW_AT_linkage_name("_ZN14IVISION_InArgsaSEOS_")
	.dwattr $C$DW$425, DW_AT_type(*$C$DW$T$215)
	.dwattr $C$DW$425, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$426	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$426, DW_AT_type(*$C$DW$T$215)

	.dwendtag $C$DW$425

	.dwattr $C$DW$T$220, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ivision/ivision.h")
	.dwattr $C$DW$T$220, DW_AT_decl_line(0x135)
	.dwattr $C$DW$T$220, DW_AT_decl_column(0x01)
	.dwendtag $C$DW$T$220

	.dwendtag $C$DW$TU$220


$C$DW$TU$182	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$182
$C$DW$T$182	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$182, DW_AT_name("IVISION_InArgs")
	.dwattr $C$DW$T$182, DW_AT_type(*$C$DW$T$220)
	.dwattr $C$DW$T$182, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ivision/ivision.h")
	.dwattr $C$DW$T$182, DW_AT_decl_line(0x13d)
	.dwattr $C$DW$T$182, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$182


$C$DW$TU$183	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$183
$C$DW$T$183	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$183, DW_AT_type(*$C$DW$T$182)
	.dwattr $C$DW$T$183, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$183


$C$DW$TU$216	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$216
$C$DW$T$216	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$216, DW_AT_type(*$C$DW$T$220)

	.dwendtag $C$DW$TU$216


$C$DW$TU$217	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$217
$C$DW$T$217	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$217, DW_AT_type(*$C$DW$T$216)
	.dwattr $C$DW$T$217, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$217


$C$DW$TU$215	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$215
$C$DW$T$215	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$215, DW_AT_type(*$C$DW$T$220)
	.dwattr $C$DW$T$215, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$215


$C$DW$TU$218	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$218

$C$DW$T$218	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$218, DW_AT_type(*$C$DW$T$215)
$C$DW$427	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$427, DW_AT_type(*$C$DW$T$217)

	.dwendtag $C$DW$T$218

	.dwendtag $C$DW$TU$218


$C$DW$TU$219	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$219

$C$DW$T$219	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$219, DW_AT_type(*$C$DW$T$215)
$C$DW$428	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$428, DW_AT_type(*$C$DW$T$215)

	.dwendtag $C$DW$T$219

	.dwendtag $C$DW$TU$219


$C$DW$TU$228	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$228

$C$DW$T$228	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$228, DW_AT_name("IVISION_Obj")
	.dwattr $C$DW$T$228, DW_AT_byte_size(0x08)
$C$DW$429	.dwtag  DW_TAG_member
	.dwattr $C$DW$429, DW_AT_type(*$C$DW$T$222)
	.dwattr $C$DW$429, DW_AT_name("fxns")
	.dwattr $C$DW$429, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$429, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ivision/ivision.h")
	.dwattr $C$DW$429, DW_AT_decl_line(0x186)
	.dwattr $C$DW$429, DW_AT_decl_column(0x1a)


$C$DW$430	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$430, DW_AT_name("operator =")
	.dwattr $C$DW$430, DW_AT_declaration
	.dwattr $C$DW$430, DW_AT_linkage_name("_ZN11IVISION_ObjaSERKS_")
	.dwattr $C$DW$430, DW_AT_type(*$C$DW$T$223)
	.dwattr $C$DW$430, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$431	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$431, DW_AT_type(*$C$DW$T$225)

	.dwendtag $C$DW$430


$C$DW$432	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$432, DW_AT_name("operator =")
	.dwattr $C$DW$432, DW_AT_declaration
	.dwattr $C$DW$432, DW_AT_linkage_name("_ZN11IVISION_ObjaSEOS_")
	.dwattr $C$DW$432, DW_AT_type(*$C$DW$T$223)
	.dwattr $C$DW$432, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$433	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$433, DW_AT_type(*$C$DW$T$223)

	.dwendtag $C$DW$432

	.dwattr $C$DW$T$228, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ivision/ivision.h")
	.dwattr $C$DW$T$228, DW_AT_decl_line(0x185)
	.dwattr $C$DW$T$228, DW_AT_decl_column(0x10)
	.dwendtag $C$DW$T$228

	.dwendtag $C$DW$TU$228


$C$DW$TU$224	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$224
$C$DW$T$224	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$224, DW_AT_type(*$C$DW$T$228)

	.dwendtag $C$DW$TU$224


$C$DW$TU$225	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$225
$C$DW$T$225	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$225, DW_AT_type(*$C$DW$T$224)
	.dwattr $C$DW$T$225, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$225


$C$DW$TU$175	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$175
$C$DW$T$175	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$175, DW_AT_type(*$C$DW$T$228)
	.dwattr $C$DW$T$175, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$175


$C$DW$TU$176	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$176
$C$DW$T$176	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$176, DW_AT_name("IVISION_Handle")
	.dwattr $C$DW$T$176, DW_AT_type(*$C$DW$T$175)
	.dwattr $C$DW$T$176, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ivision/ivision.h")
	.dwattr $C$DW$T$176, DW_AT_decl_line(0x18d)
	.dwattr $C$DW$T$176, DW_AT_decl_column(0x1e)

	.dwendtag $C$DW$TU$176


$C$DW$TU$223	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$223
$C$DW$T$223	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$223, DW_AT_type(*$C$DW$T$228)
	.dwattr $C$DW$T$223, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$223


$C$DW$TU$226	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$226

$C$DW$T$226	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$226, DW_AT_type(*$C$DW$T$223)
$C$DW$434	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$434, DW_AT_type(*$C$DW$T$225)

	.dwendtag $C$DW$T$226

	.dwendtag $C$DW$TU$226


$C$DW$TU$227	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$227

$C$DW$T$227	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$227, DW_AT_type(*$C$DW$T$223)
$C$DW$435	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$435, DW_AT_type(*$C$DW$T$223)

	.dwendtag $C$DW$T$227

	.dwendtag $C$DW$TU$227


$C$DW$TU$237	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$237

$C$DW$T$237	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$237, DW_AT_name("IVISION_OutArgs")
	.dwattr $C$DW$T$237, DW_AT_byte_size(0xfc)
$C$DW$436	.dwtag  DW_TAG_member
	.dwattr $C$DW$436, DW_AT_type(*$C$DW$T$50)
	.dwattr $C$DW$436, DW_AT_name("size")
	.dwattr $C$DW$436, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$436, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ivision/ivision.h")
	.dwattr $C$DW$436, DW_AT_decl_line(0x146)
	.dwattr $C$DW$436, DW_AT_decl_column(0x0c)

$C$DW$437	.dwtag  DW_TAG_member
	.dwattr $C$DW$437, DW_AT_type(*$C$DW$T$231)
	.dwattr $C$DW$437, DW_AT_name("inFreeBufIDs")
	.dwattr $C$DW$437, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$437, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$437, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ivision/ivision.h")
	.dwattr $C$DW$437, DW_AT_decl_line(0x147)
	.dwattr $C$DW$437, DW_AT_decl_column(0x0b)

$C$DW$438	.dwtag  DW_TAG_member
	.dwattr $C$DW$438, DW_AT_type(*$C$DW$T$231)
	.dwattr $C$DW$438, DW_AT_name("outFreeBufIDs")
	.dwattr $C$DW$438, DW_AT_data_member_location[DW_OP_plus_uconst 0x7c]
	.dwattr $C$DW$438, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$438, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ivision/ivision.h")
	.dwattr $C$DW$438, DW_AT_decl_line(0x162)
	.dwattr $C$DW$438, DW_AT_decl_column(0x0b)

$C$DW$439	.dwtag  DW_TAG_member
	.dwattr $C$DW$439, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$439, DW_AT_name("reserved")
	.dwattr $C$DW$439, DW_AT_data_member_location[DW_OP_plus_uconst 0xf4]
	.dwattr $C$DW$439, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$439, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ivision/ivision.h")
	.dwattr $C$DW$439, DW_AT_decl_line(0x17d)
	.dwattr $C$DW$439, DW_AT_decl_column(0x0c)


$C$DW$440	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$440, DW_AT_name("operator =")
	.dwattr $C$DW$440, DW_AT_declaration
	.dwattr $C$DW$440, DW_AT_linkage_name("_ZN15IVISION_OutArgsaSERKS_")
	.dwattr $C$DW$440, DW_AT_type(*$C$DW$T$232)
	.dwattr $C$DW$440, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$441	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$441, DW_AT_type(*$C$DW$T$234)

	.dwendtag $C$DW$440


$C$DW$442	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$442, DW_AT_name("operator =")
	.dwattr $C$DW$442, DW_AT_declaration
	.dwattr $C$DW$442, DW_AT_linkage_name("_ZN15IVISION_OutArgsaSEOS_")
	.dwattr $C$DW$442, DW_AT_type(*$C$DW$T$232)
	.dwattr $C$DW$442, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$443	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$443, DW_AT_type(*$C$DW$T$232)

	.dwendtag $C$DW$442

	.dwattr $C$DW$T$237, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ivision/ivision.h")
	.dwattr $C$DW$T$237, DW_AT_decl_line(0x145)
	.dwattr $C$DW$T$237, DW_AT_decl_column(0x01)
	.dwendtag $C$DW$T$237

	.dwendtag $C$DW$TU$237


$C$DW$TU$184	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$184
$C$DW$T$184	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$184, DW_AT_name("IVISION_OutArgs")
	.dwattr $C$DW$T$184, DW_AT_type(*$C$DW$T$237)
	.dwattr $C$DW$T$184, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ivision/ivision.h")
	.dwattr $C$DW$T$184, DW_AT_decl_line(0x17e)
	.dwattr $C$DW$T$184, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$184


$C$DW$TU$185	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$185
$C$DW$T$185	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$185, DW_AT_type(*$C$DW$T$184)
	.dwattr $C$DW$T$185, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$185


$C$DW$TU$233	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$233
$C$DW$T$233	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$233, DW_AT_type(*$C$DW$T$237)

	.dwendtag $C$DW$TU$233


$C$DW$TU$234	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$234
$C$DW$T$234	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$234, DW_AT_type(*$C$DW$T$233)
	.dwattr $C$DW$T$234, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$234


$C$DW$TU$232	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$232
$C$DW$T$232	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$232, DW_AT_type(*$C$DW$T$237)
	.dwattr $C$DW$T$232, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$232


$C$DW$TU$235	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$235

$C$DW$T$235	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$235, DW_AT_type(*$C$DW$T$232)
$C$DW$444	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$444, DW_AT_type(*$C$DW$T$234)

	.dwendtag $C$DW$T$235

	.dwendtag $C$DW$TU$235


$C$DW$TU$236	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$236

$C$DW$T$236	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$236, DW_AT_type(*$C$DW$T$232)
$C$DW$445	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$445, DW_AT_type(*$C$DW$T$232)

	.dwendtag $C$DW$T$236

	.dwendtag $C$DW$TU$236


$C$DW$TU$248	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$248

$C$DW$T$248	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$248, DW_AT_name("IVISION_Params")
	.dwattr $C$DW$T$248, DW_AT_byte_size(0x10)
$C$DW$446	.dwtag  DW_TAG_member
	.dwattr $C$DW$446, DW_AT_type(*$C$DW$T$40)
	.dwattr $C$DW$446, DW_AT_name("algParams")
	.dwattr $C$DW$446, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$446, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ivision/ivision.h")
	.dwattr $C$DW$446, DW_AT_decl_line(0x73)
	.dwattr $C$DW$446, DW_AT_decl_column(0x11)

$C$DW$447	.dwtag  DW_TAG_member
	.dwattr $C$DW$447, DW_AT_type(*$C$DW$T$242)
	.dwattr $C$DW$447, DW_AT_name("cacheWriteBack")
	.dwattr $C$DW$447, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$447, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$447, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ivision/ivision.h")
	.dwattr $C$DW$447, DW_AT_decl_line(0x74)
	.dwattr $C$DW$447, DW_AT_decl_column(0x1b)


$C$DW$448	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$448, DW_AT_name("operator =")
	.dwattr $C$DW$448, DW_AT_declaration
	.dwattr $C$DW$448, DW_AT_linkage_name("_ZN14IVISION_ParamsaSERKS_")
	.dwattr $C$DW$448, DW_AT_type(*$C$DW$T$243)
	.dwattr $C$DW$448, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$449	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$449, DW_AT_type(*$C$DW$T$245)

	.dwendtag $C$DW$448


$C$DW$450	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$450, DW_AT_name("operator =")
	.dwattr $C$DW$450, DW_AT_declaration
	.dwattr $C$DW$450, DW_AT_linkage_name("_ZN14IVISION_ParamsaSEOS_")
	.dwattr $C$DW$450, DW_AT_type(*$C$DW$T$243)
	.dwattr $C$DW$450, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$451	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$451, DW_AT_type(*$C$DW$T$243)

	.dwendtag $C$DW$450

	.dwattr $C$DW$T$248, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ivision/ivision.h")
	.dwattr $C$DW$T$248, DW_AT_decl_line(0x72)
	.dwattr $C$DW$T$248, DW_AT_decl_column(0x10)
	.dwendtag $C$DW$T$248

	.dwendtag $C$DW$TU$248


$C$DW$TU$244	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$244
$C$DW$T$244	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$244, DW_AT_type(*$C$DW$T$248)

	.dwendtag $C$DW$TU$244


$C$DW$TU$245	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$245
$C$DW$T$245	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$245, DW_AT_type(*$C$DW$T$244)
	.dwattr $C$DW$T$245, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$245


$C$DW$TU$269	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$269
$C$DW$T$269	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$269, DW_AT_name("IVISION_Params")
	.dwattr $C$DW$T$269, DW_AT_type(*$C$DW$T$248)
	.dwattr $C$DW$T$269, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ivision/ivision.h")
	.dwattr $C$DW$T$269, DW_AT_decl_line(0x7e)
	.dwattr $C$DW$T$269, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$269


$C$DW$TU$243	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$243
$C$DW$T$243	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$243, DW_AT_type(*$C$DW$T$248)
	.dwattr $C$DW$T$243, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$243


$C$DW$TU$246	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$246

$C$DW$T$246	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$246, DW_AT_type(*$C$DW$T$243)
$C$DW$452	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$452, DW_AT_type(*$C$DW$T$245)

	.dwendtag $C$DW$T$246

	.dwendtag $C$DW$TU$246


$C$DW$TU$247	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$247

$C$DW$T$247	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$247, DW_AT_type(*$C$DW$T$243)
$C$DW$453	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$453, DW_AT_type(*$C$DW$T$243)

	.dwendtag $C$DW$T$247

	.dwendtag $C$DW$TU$247


$C$DW$TU$258	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$258

$C$DW$T$258	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$258, DW_AT_name("IVISION_Point")
	.dwattr $C$DW$T$258, DW_AT_byte_size(0x08)
$C$DW$454	.dwtag  DW_TAG_member
	.dwattr $C$DW$454, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$454, DW_AT_name("x")
	.dwattr $C$DW$454, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$454, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ivision/ivision.h")
	.dwattr $C$DW$454, DW_AT_decl_line(0x87)
	.dwattr $C$DW$454, DW_AT_decl_column(0x0b)

$C$DW$455	.dwtag  DW_TAG_member
	.dwattr $C$DW$455, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$455, DW_AT_name("y")
	.dwattr $C$DW$455, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$455, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$455, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ivision/ivision.h")
	.dwattr $C$DW$455, DW_AT_decl_line(0x88)
	.dwattr $C$DW$455, DW_AT_decl_column(0x0b)


$C$DW$456	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$456, DW_AT_name("operator =")
	.dwattr $C$DW$456, DW_AT_declaration
	.dwattr $C$DW$456, DW_AT_linkage_name("_ZN13IVISION_PointaSERKS_")
	.dwattr $C$DW$456, DW_AT_type(*$C$DW$T$253)
	.dwattr $C$DW$456, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$457	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$457, DW_AT_type(*$C$DW$T$255)

	.dwendtag $C$DW$456


$C$DW$458	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$458, DW_AT_name("operator =")
	.dwattr $C$DW$458, DW_AT_declaration
	.dwattr $C$DW$458, DW_AT_linkage_name("_ZN13IVISION_PointaSEOS_")
	.dwattr $C$DW$458, DW_AT_type(*$C$DW$T$253)
	.dwattr $C$DW$458, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$459	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$459, DW_AT_type(*$C$DW$T$253)

	.dwendtag $C$DW$458

	.dwattr $C$DW$T$258, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ivision/ivision.h")
	.dwattr $C$DW$T$258, DW_AT_decl_line(0x86)
	.dwattr $C$DW$T$258, DW_AT_decl_column(0x01)
	.dwendtag $C$DW$T$258

	.dwendtag $C$DW$TU$258


$C$DW$TU$254	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$254
$C$DW$T$254	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$254, DW_AT_type(*$C$DW$T$258)

	.dwendtag $C$DW$TU$254


$C$DW$TU$255	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$255
$C$DW$T$255	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$255, DW_AT_type(*$C$DW$T$254)
	.dwattr $C$DW$T$255, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$255


$C$DW$TU$260	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$260
$C$DW$T$260	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$260, DW_AT_name("IVISION_Point")
	.dwattr $C$DW$T$260, DW_AT_type(*$C$DW$T$258)
	.dwattr $C$DW$T$260, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ivision/ivision.h")
	.dwattr $C$DW$T$260, DW_AT_decl_line(0x89)
	.dwattr $C$DW$T$260, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$260


$C$DW$TU$253	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$253
$C$DW$T$253	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$253, DW_AT_type(*$C$DW$T$258)
	.dwattr $C$DW$T$253, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$253


$C$DW$TU$256	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$256

$C$DW$T$256	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$256, DW_AT_type(*$C$DW$T$253)
$C$DW$460	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$460, DW_AT_type(*$C$DW$T$255)

	.dwendtag $C$DW$T$256

	.dwendtag $C$DW$TU$256


$C$DW$TU$257	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$257

$C$DW$T$257	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$257, DW_AT_type(*$C$DW$T$253)
$C$DW$461	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$461, DW_AT_type(*$C$DW$T$253)

	.dwendtag $C$DW$T$257

	.dwendtag $C$DW$TU$257


$C$DW$TU$266	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$266

$C$DW$T$266	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$266, DW_AT_name("IVISION_Rect")
	.dwattr $C$DW$T$266, DW_AT_byte_size(0x10)
$C$DW$462	.dwtag  DW_TAG_member
	.dwattr $C$DW$462, DW_AT_type(*$C$DW$T$260)
	.dwattr $C$DW$462, DW_AT_name("topLeft")
	.dwattr $C$DW$462, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$462, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ivision/ivision.h")
	.dwattr $C$DW$462, DW_AT_decl_line(0x90)
	.dwattr $C$DW$462, DW_AT_decl_column(0x11)

$C$DW$463	.dwtag  DW_TAG_member
	.dwattr $C$DW$463, DW_AT_type(*$C$DW$T$50)
	.dwattr $C$DW$463, DW_AT_name("width")
	.dwattr $C$DW$463, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$463, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$463, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ivision/ivision.h")
	.dwattr $C$DW$463, DW_AT_decl_line(0x91)
	.dwattr $C$DW$463, DW_AT_decl_column(0x0c)

$C$DW$464	.dwtag  DW_TAG_member
	.dwattr $C$DW$464, DW_AT_type(*$C$DW$T$50)
	.dwattr $C$DW$464, DW_AT_name("height")
	.dwattr $C$DW$464, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$464, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$464, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ivision/ivision.h")
	.dwattr $C$DW$464, DW_AT_decl_line(0x92)
	.dwattr $C$DW$464, DW_AT_decl_column(0x0c)


$C$DW$465	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$465, DW_AT_name("operator =")
	.dwattr $C$DW$465, DW_AT_declaration
	.dwattr $C$DW$465, DW_AT_linkage_name("_ZN12IVISION_RectaSERKS_")
	.dwattr $C$DW$465, DW_AT_type(*$C$DW$T$261)
	.dwattr $C$DW$465, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$466	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$466, DW_AT_type(*$C$DW$T$263)

	.dwendtag $C$DW$465


$C$DW$467	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$467, DW_AT_name("operator =")
	.dwattr $C$DW$467, DW_AT_declaration
	.dwattr $C$DW$467, DW_AT_linkage_name("_ZN12IVISION_RectaSEOS_")
	.dwattr $C$DW$467, DW_AT_type(*$C$DW$T$261)
	.dwattr $C$DW$467, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$468	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$468, DW_AT_type(*$C$DW$T$261)

	.dwendtag $C$DW$467

	.dwattr $C$DW$T$266, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ivision/ivision.h")
	.dwattr $C$DW$T$266, DW_AT_decl_line(0x8f)
	.dwattr $C$DW$T$266, DW_AT_decl_column(0x01)
	.dwendtag $C$DW$T$266

	.dwendtag $C$DW$TU$266


$C$DW$TU$165	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$165
$C$DW$T$165	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$165, DW_AT_name("IVISION_Rect")
	.dwattr $C$DW$T$165, DW_AT_type(*$C$DW$T$266)
	.dwattr $C$DW$T$165, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ivision/ivision.h")
	.dwattr $C$DW$T$165, DW_AT_decl_line(0x93)
	.dwattr $C$DW$T$165, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$165


$C$DW$TU$262	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$262
$C$DW$T$262	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$262, DW_AT_type(*$C$DW$T$266)

	.dwendtag $C$DW$TU$262


$C$DW$TU$263	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$263
$C$DW$T$263	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$263, DW_AT_type(*$C$DW$T$262)
	.dwattr $C$DW$T$263, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$263


$C$DW$TU$261	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$261
$C$DW$T$261	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$261, DW_AT_type(*$C$DW$T$266)
	.dwattr $C$DW$T$261, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$261


$C$DW$TU$264	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$264

$C$DW$T$264	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$264, DW_AT_type(*$C$DW$T$261)
$C$DW$469	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$469, DW_AT_type(*$C$DW$T$263)

	.dwendtag $C$DW$T$264

	.dwendtag $C$DW$TU$264


$C$DW$TU$265	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$265

$C$DW$T$265	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$265, DW_AT_type(*$C$DW$T$261)
$C$DW$470	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$470, DW_AT_type(*$C$DW$T$261)

	.dwendtag $C$DW$T$265

	.dwendtag $C$DW$TU$265


$C$DW$TU$295	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$295

$C$DW$T$295	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$295, DW_AT_name("TIDL_CreateParams")
	.dwattr $C$DW$T$295, DW_AT_byte_size(0x70)
$C$DW$471	.dwtag  DW_TAG_member
	.dwattr $C$DW$471, DW_AT_type(*$C$DW$T$269)
	.dwattr $C$DW$471, DW_AT_name("visionParams")
	.dwattr $C$DW$471, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$471, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$471, DW_AT_decl_line(0x63d)
	.dwattr $C$DW$471, DW_AT_decl_column(0x12)

$C$DW$472	.dwtag  DW_TAG_member
	.dwattr $C$DW$472, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$472, DW_AT_name("currLayersGroupId")
	.dwattr $C$DW$472, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$472, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$472, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$472, DW_AT_decl_line(0x63f)
	.dwattr $C$DW$472, DW_AT_decl_column(0x0b)

$C$DW$473	.dwtag  DW_TAG_member
	.dwattr $C$DW$473, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$473, DW_AT_name("isInbufsPaded")
	.dwattr $C$DW$473, DW_AT_data_member_location[DW_OP_plus_uconst 0x14]
	.dwattr $C$DW$473, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$473, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$473, DW_AT_decl_line(0x641)
	.dwattr $C$DW$473, DW_AT_decl_column(0x0b)

$C$DW$474	.dwtag  DW_TAG_member
	.dwattr $C$DW$474, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$474, DW_AT_name("optimiseExtMem")
	.dwattr $C$DW$474, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$474, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$474, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$474, DW_AT_decl_line(0x643)
	.dwattr $C$DW$474, DW_AT_decl_column(0x0b)

$C$DW$475	.dwtag  DW_TAG_member
	.dwattr $C$DW$475, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$475, DW_AT_name("quantRangeExpansionFactor")
	.dwattr $C$DW$475, DW_AT_data_member_location[DW_OP_plus_uconst 0x1c]
	.dwattr $C$DW$475, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$475, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$475, DW_AT_decl_line(0x645)
	.dwattr $C$DW$475, DW_AT_decl_column(0x10)

$C$DW$476	.dwtag  DW_TAG_member
	.dwattr $C$DW$476, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$476, DW_AT_name("quantRangeUpdateFactor")
	.dwattr $C$DW$476, DW_AT_data_member_location[DW_OP_plus_uconst 0x20]
	.dwattr $C$DW$476, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$476, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$476, DW_AT_decl_line(0x648)
	.dwattr $C$DW$476, DW_AT_decl_column(0x10)

$C$DW$477	.dwtag  DW_TAG_member
	.dwattr $C$DW$477, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$477, DW_AT_name("traceLogLevel")
	.dwattr $C$DW$477, DW_AT_data_member_location[DW_OP_plus_uconst 0x24]
	.dwattr $C$DW$477, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$477, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$477, DW_AT_decl_line(0x64a)
	.dwattr $C$DW$477, DW_AT_decl_column(0x0b)

$C$DW$478	.dwtag  DW_TAG_member
	.dwattr $C$DW$478, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$478, DW_AT_name("traceWriteLevel")
	.dwattr $C$DW$478, DW_AT_data_member_location[DW_OP_plus_uconst 0x28]
	.dwattr $C$DW$478, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$478, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$478, DW_AT_decl_line(0x64c)
	.dwattr $C$DW$478, DW_AT_decl_column(0x0b)

$C$DW$479	.dwtag  DW_TAG_member
	.dwattr $C$DW$479, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$479, DW_AT_name("reservedCtrl")
	.dwattr $C$DW$479, DW_AT_data_member_location[DW_OP_plus_uconst 0x2c]
	.dwattr $C$DW$479, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$479, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$479, DW_AT_decl_line(0x64e)
	.dwattr $C$DW$479, DW_AT_decl_column(0x0b)

$C$DW$480	.dwtag  DW_TAG_member
	.dwattr $C$DW$480, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$480, DW_AT_name("flowCtrl")
	.dwattr $C$DW$480, DW_AT_data_member_location[DW_OP_plus_uconst 0x30]
	.dwattr $C$DW$480, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$480, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$480, DW_AT_decl_line(0x650)
	.dwattr $C$DW$480, DW_AT_decl_column(0x0b)

$C$DW$481	.dwtag  DW_TAG_member
	.dwattr $C$DW$481, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$481, DW_AT_name("traceBaseName")
	.dwattr $C$DW$481, DW_AT_data_member_location[DW_OP_plus_uconst 0x38]
	.dwattr $C$DW$481, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$481, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$481, DW_AT_decl_line(0x652)
	.dwattr $C$DW$481, DW_AT_decl_column(0x0a)

$C$DW$482	.dwtag  DW_TAG_member
	.dwattr $C$DW$482, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$482, DW_AT_name("udmaDrvObj")
	.dwattr $C$DW$482, DW_AT_data_member_location[DW_OP_plus_uconst 0x40]
	.dwattr $C$DW$482, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$482, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$482, DW_AT_decl_line(0x657)
	.dwattr $C$DW$482, DW_AT_decl_column(0x0a)

$C$DW$483	.dwtag  DW_TAG_member
	.dwattr $C$DW$483, DW_AT_type(*$C$DW$T$271)
	.dwattr $C$DW$483, DW_AT_name("net")
	.dwattr $C$DW$483, DW_AT_data_member_location[DW_OP_plus_uconst 0x48]
	.dwattr $C$DW$483, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$483, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$483, DW_AT_decl_line(0x659)
	.dwattr $C$DW$483, DW_AT_decl_column(0x15)

$C$DW$484	.dwtag  DW_TAG_member
	.dwattr $C$DW$484, DW_AT_type(*$C$DW$T$279)
	.dwattr $C$DW$484, DW_AT_name("TIDLVprintf")
	.dwattr $C$DW$484, DW_AT_data_member_location[DW_OP_plus_uconst 0x50]
	.dwattr $C$DW$484, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$484, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$484, DW_AT_decl_line(0x65b)
	.dwattr $C$DW$484, DW_AT_decl_column(0x0c)

$C$DW$485	.dwtag  DW_TAG_member
	.dwattr $C$DW$485, DW_AT_type(*$C$DW$T$281)
	.dwattr $C$DW$485, DW_AT_name("TIDLWriteBinToFile")
	.dwattr $C$DW$485, DW_AT_data_member_location[DW_OP_plus_uconst 0x58]
	.dwattr $C$DW$485, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$485, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$485, DW_AT_decl_line(0x65d)
	.dwattr $C$DW$485, DW_AT_decl_column(0x0c)

$C$DW$486	.dwtag  DW_TAG_member
	.dwattr $C$DW$486, DW_AT_type(*$C$DW$T$281)
	.dwattr $C$DW$486, DW_AT_name("TIDLReadBinFromFile")
	.dwattr $C$DW$486, DW_AT_data_member_location[DW_OP_plus_uconst 0x60]
	.dwattr $C$DW$486, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$486, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$486, DW_AT_decl_line(0x65f)
	.dwattr $C$DW$486, DW_AT_decl_column(0x0c)

$C$DW$487	.dwtag  DW_TAG_member
	.dwattr $C$DW$487, DW_AT_type(*$C$DW$T$289)
	.dwattr $C$DW$487, DW_AT_name("TIDL_CustomLayerProcess")
	.dwattr $C$DW$487, DW_AT_data_member_location[DW_OP_plus_uconst 0x68]
	.dwattr $C$DW$487, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$487, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$487, DW_AT_decl_line(0x671)
	.dwattr $C$DW$487, DW_AT_decl_column(0x0d)


$C$DW$488	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$488, DW_AT_name("operator =")
	.dwattr $C$DW$488, DW_AT_declaration
	.dwattr $C$DW$488, DW_AT_linkage_name("_ZN17TIDL_CreateParamsaSERKS_")
	.dwattr $C$DW$488, DW_AT_type(*$C$DW$T$290)
	.dwattr $C$DW$488, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$489	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$489, DW_AT_type(*$C$DW$T$292)

	.dwendtag $C$DW$488


$C$DW$490	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$490, DW_AT_name("operator =")
	.dwattr $C$DW$490, DW_AT_declaration
	.dwattr $C$DW$490, DW_AT_linkage_name("_ZN17TIDL_CreateParamsaSEOS_")
	.dwattr $C$DW$490, DW_AT_type(*$C$DW$T$290)
	.dwattr $C$DW$490, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$491	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$491, DW_AT_type(*$C$DW$T$290)

	.dwendtag $C$DW$490

	.dwattr $C$DW$T$295, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$295, DW_AT_decl_line(0x63b)
	.dwattr $C$DW$T$295, DW_AT_decl_column(0x01)
	.dwendtag $C$DW$T$295

	.dwendtag $C$DW$TU$295


$C$DW$TU$291	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$291
$C$DW$T$291	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$291, DW_AT_type(*$C$DW$T$295)

	.dwendtag $C$DW$TU$291


$C$DW$TU$292	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$292
$C$DW$T$292	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$292, DW_AT_type(*$C$DW$T$291)
	.dwattr $C$DW$T$292, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$292


$C$DW$TU$356	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$356
$C$DW$T$356	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$356, DW_AT_name("TIDL_CreateParams")
	.dwattr $C$DW$T$356, DW_AT_type(*$C$DW$T$295)
	.dwattr $C$DW$T$356, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$356, DW_AT_decl_line(0x67b)
	.dwattr $C$DW$T$356, DW_AT_decl_column(0x01)

	.dwendtag $C$DW$TU$356


$C$DW$TU$357	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$357
$C$DW$T$357	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$357, DW_AT_type(*$C$DW$T$356)
	.dwattr $C$DW$T$357, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$357


$C$DW$TU$290	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$290
$C$DW$T$290	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$290, DW_AT_type(*$C$DW$T$295)
	.dwattr $C$DW$T$290, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$290


$C$DW$TU$293	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$293

$C$DW$T$293	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$293, DW_AT_type(*$C$DW$T$290)
$C$DW$492	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$492, DW_AT_type(*$C$DW$T$292)

	.dwendtag $C$DW$T$293

	.dwendtag $C$DW$TU$293


$C$DW$TU$294	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$294

$C$DW$T$294	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$294, DW_AT_type(*$C$DW$T$290)
$C$DW$493	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$493, DW_AT_type(*$C$DW$T$290)

	.dwendtag $C$DW$T$294

	.dwendtag $C$DW$TU$294


$C$DW$TU$337	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$337

$C$DW$T$337	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$337, DW_AT_name("TIDL_GenericHandle")
	.dwattr $C$DW$T$337, DW_AT_byte_size(0x610)
$C$DW$494	.dwtag  DW_TAG_member
	.dwattr $C$DW$494, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$494, DW_AT_name("numberTensors")
	.dwattr $C$DW$494, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$494, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$494, DW_AT_decl_line(0x38c)
	.dwattr $C$DW$494, DW_AT_decl_column(0x0b)

$C$DW$495	.dwtag  DW_TAG_member
	.dwattr $C$DW$495, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$495, DW_AT_name("disableMSMCstaging")
	.dwattr $C$DW$495, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$495, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$495, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$495, DW_AT_decl_line(0x38d)
	.dwattr $C$DW$495, DW_AT_decl_column(0x0b)

$C$DW$496	.dwtag  DW_TAG_member
	.dwattr $C$DW$496, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$496, DW_AT_name("inBlkProcessingJump")
	.dwattr $C$DW$496, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$496, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$496, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$496, DW_AT_decl_line(0x38e)
	.dwattr $C$DW$496, DW_AT_decl_column(0x0b)

$C$DW$497	.dwtag  DW_TAG_member
	.dwattr $C$DW$497, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$497, DW_AT_name("inChProcessingJump")
	.dwattr $C$DW$497, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$497, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$497, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$497, DW_AT_decl_line(0x38f)
	.dwattr $C$DW$497, DW_AT_decl_column(0x0b)

$C$DW$498	.dwtag  DW_TAG_member
	.dwattr $C$DW$498, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$498, DW_AT_name("outBlkProcessingJump")
	.dwattr $C$DW$498, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$498, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$498, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$498, DW_AT_decl_line(0x390)
	.dwattr $C$DW$498, DW_AT_decl_column(0x0b)

$C$DW$499	.dwtag  DW_TAG_member
	.dwattr $C$DW$499, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$499, DW_AT_name("outChProcessingJump")
	.dwattr $C$DW$499, DW_AT_data_member_location[DW_OP_plus_uconst 0x14]
	.dwattr $C$DW$499, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$499, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$499, DW_AT_decl_line(0x391)
	.dwattr $C$DW$499, DW_AT_decl_column(0x0b)

$C$DW$500	.dwtag  DW_TAG_member
	.dwattr $C$DW$500, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$500, DW_AT_name("outBatchProcessingJump")
	.dwattr $C$DW$500, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$500, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$500, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$500, DW_AT_decl_line(0x392)
	.dwattr $C$DW$500, DW_AT_decl_column(0x0b)

$C$DW$501	.dwtag  DW_TAG_member
	.dwattr $C$DW$501, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$501, DW_AT_name("numSplitsPerBatch")
	.dwattr $C$DW$501, DW_AT_data_member_location[DW_OP_plus_uconst 0x1c]
	.dwattr $C$DW$501, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$501, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$501, DW_AT_decl_line(0x393)
	.dwattr $C$DW$501, DW_AT_decl_column(0x0b)

$C$DW$502	.dwtag  DW_TAG_member
	.dwattr $C$DW$502, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$502, DW_AT_name("isCPUcopyNeeded")
	.dwattr $C$DW$502, DW_AT_data_member_location[DW_OP_plus_uconst 0x20]
	.dwattr $C$DW$502, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$502, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$502, DW_AT_decl_line(0x394)
	.dwattr $C$DW$502, DW_AT_decl_column(0x0b)

$C$DW$503	.dwtag  DW_TAG_member
	.dwattr $C$DW$503, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$503, DW_AT_name("mixPrecisionWithSignedInput")
	.dwattr $C$DW$503, DW_AT_data_member_location[DW_OP_plus_uconst 0x24]
	.dwattr $C$DW$503, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$503, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$503, DW_AT_decl_line(0x395)
	.dwattr $C$DW$503, DW_AT_decl_column(0x0b)

$C$DW$504	.dwtag  DW_TAG_member
	.dwattr $C$DW$504, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$504, DW_AT_name("mixPrecisionEnabledForOutput")
	.dwattr $C$DW$504, DW_AT_data_member_location[DW_OP_plus_uconst 0x28]
	.dwattr $C$DW$504, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$504, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$504, DW_AT_decl_line(0x396)
	.dwattr $C$DW$504, DW_AT_decl_column(0x0b)

$C$DW$505	.dwtag  DW_TAG_member
	.dwattr $C$DW$505, DW_AT_type(*$C$DW$T$313)
	.dwattr $C$DW$505, DW_AT_name("inDMAChList")
	.dwattr $C$DW$505, DW_AT_data_member_location[DW_OP_plus_uconst 0x2c]
	.dwattr $C$DW$505, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$505, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$505, DW_AT_decl_line(0x397)
	.dwattr $C$DW$505, DW_AT_decl_column(0x0b)

$C$DW$506	.dwtag  DW_TAG_member
	.dwattr $C$DW$506, DW_AT_type(*$C$DW$T$313)
	.dwattr $C$DW$506, DW_AT_name("outDMAChList")
	.dwattr $C$DW$506, DW_AT_data_member_location[DW_OP_plus_uconst 0x64]
	.dwattr $C$DW$506, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$506, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$506, DW_AT_decl_line(0x398)
	.dwattr $C$DW$506, DW_AT_decl_column(0x0b)

$C$DW$507	.dwtag  DW_TAG_member
	.dwattr $C$DW$507, DW_AT_type(*$C$DW$T$313)
	.dwattr $C$DW$507, DW_AT_name("paramDMAChList")
	.dwattr $C$DW$507, DW_AT_data_member_location[DW_OP_plus_uconst 0x9c]
	.dwattr $C$DW$507, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$507, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$507, DW_AT_decl_line(0x399)
	.dwattr $C$DW$507, DW_AT_decl_column(0x0b)

$C$DW$508	.dwtag  DW_TAG_member
	.dwattr $C$DW$508, DW_AT_type(*$C$DW$T$314)
	.dwattr $C$DW$508, DW_AT_name("localDataFlowInfo")
	.dwattr $C$DW$508, DW_AT_data_member_location[DW_OP_plus_uconst 0xd4]
	.dwattr $C$DW$508, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$508, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$508, DW_AT_decl_line(0x39a)
	.dwattr $C$DW$508, DW_AT_decl_column(0x17)

$C$DW$509	.dwtag  DW_TAG_member
	.dwattr $C$DW$509, DW_AT_type(*$C$DW$T$316)
	.dwattr $C$DW$509, DW_AT_name("tensorTransferInfo")
	.dwattr $C$DW$509, DW_AT_data_member_location[DW_OP_plus_uconst 0x104]
	.dwattr $C$DW$509, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$509, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$509, DW_AT_decl_line(0x39b)
	.dwattr $C$DW$509, DW_AT_decl_column(0x1b)

$C$DW$510	.dwtag  DW_TAG_member
	.dwattr $C$DW$510, DW_AT_type(*$C$DW$T$320)
	.dwattr $C$DW$510, DW_AT_name("trMemTr")
	.dwattr $C$DW$510, DW_AT_data_member_location[DW_OP_plus_uconst 0x26c]
	.dwattr $C$DW$510, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$510, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$510, DW_AT_decl_line(0x39c)
	.dwattr $C$DW$510, DW_AT_decl_column(0x0b)

$C$DW$511	.dwtag  DW_TAG_member
	.dwattr $C$DW$511, DW_AT_type(*$C$DW$T$321)
	.dwattr $C$DW$511, DW_AT_name("execInArgs")
	.dwattr $C$DW$511, DW_AT_data_member_location[DW_OP_plus_uconst 0x5f0]
	.dwattr $C$DW$511, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$511, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$511, DW_AT_decl_line(0x39d)
	.dwattr $C$DW$511, DW_AT_decl_column(0x13)

$C$DW$512	.dwtag  DW_TAG_member
	.dwattr $C$DW$512, DW_AT_type(*$C$DW$T$322)
	.dwattr $C$DW$512, DW_AT_name("execOutArgs")
	.dwattr $C$DW$512, DW_AT_data_member_location[DW_OP_plus_uconst 0x5f8]
	.dwattr $C$DW$512, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$512, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$512, DW_AT_decl_line(0x39e)
	.dwattr $C$DW$512, DW_AT_decl_column(0x14)

$C$DW$513	.dwtag  DW_TAG_member
	.dwattr $C$DW$513, DW_AT_type(*$C$DW$T$328)
	.dwattr $C$DW$513, DW_AT_name("functionPtr")
	.dwattr $C$DW$513, DW_AT_data_member_location[DW_OP_plus_uconst 0x600]
	.dwattr $C$DW$513, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$513, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$513, DW_AT_decl_line(0x39f)
	.dwattr $C$DW$513, DW_AT_decl_column(0x1b)

$C$DW$514	.dwtag  DW_TAG_member
	.dwattr $C$DW$514, DW_AT_type(*$C$DW$T$331)
	.dwattr $C$DW$514, DW_AT_name("updateFunctionPtr")
	.dwattr $C$DW$514, DW_AT_data_member_location[DW_OP_plus_uconst 0x608]
	.dwattr $C$DW$514, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$514, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$514, DW_AT_decl_line(0x3a0)
	.dwattr $C$DW$514, DW_AT_decl_column(0x1f)


$C$DW$515	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$515, DW_AT_name("operator =")
	.dwattr $C$DW$515, DW_AT_declaration
	.dwattr $C$DW$515, DW_AT_linkage_name("_ZN18TIDL_GenericHandleaSERKS_")
	.dwattr $C$DW$515, DW_AT_type(*$C$DW$T$332)
	.dwattr $C$DW$515, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$516	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$516, DW_AT_type(*$C$DW$T$334)

	.dwendtag $C$DW$515


$C$DW$517	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$517, DW_AT_name("operator =")
	.dwattr $C$DW$517, DW_AT_declaration
	.dwattr $C$DW$517, DW_AT_linkage_name("_ZN18TIDL_GenericHandleaSEOS_")
	.dwattr $C$DW$517, DW_AT_type(*$C$DW$T$332)
	.dwattr $C$DW$517, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$518	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$518, DW_AT_type(*$C$DW$T$332)

	.dwendtag $C$DW$517

	.dwattr $C$DW$T$337, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$T$337, DW_AT_decl_line(0x38b)
	.dwattr $C$DW$T$337, DW_AT_decl_column(0x01)
	.dwendtag $C$DW$T$337

	.dwendtag $C$DW$TU$337


$C$DW$TU$333	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$333
$C$DW$T$333	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$333, DW_AT_type(*$C$DW$T$337)

	.dwendtag $C$DW$TU$333


$C$DW$TU$334	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$334
$C$DW$T$334	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$334, DW_AT_type(*$C$DW$T$333)
	.dwattr $C$DW$T$334, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$334


$C$DW$TU$695	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$695
$C$DW$T$695	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$695, DW_AT_name("TIDL_GenericHandle")
	.dwattr $C$DW$T$695, DW_AT_type(*$C$DW$T$337)
	.dwattr $C$DW$T$695, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$T$695, DW_AT_decl_line(0x3a1)
	.dwattr $C$DW$T$695, DW_AT_decl_column(0x02)

	.dwendtag $C$DW$TU$695


$C$DW$TU$696	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$696
$C$DW$T$696	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$696, DW_AT_type(*$C$DW$T$695)
	.dwattr $C$DW$T$696, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$696


$C$DW$TU$697	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$697

$C$DW$T$697	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$697, DW_AT_type(*$C$DW$T$696)
	.dwattr $C$DW$T$697, DW_AT_byte_size(0x40)
$C$DW$519	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$519, DW_AT_upper_bound(0x07)

	.dwendtag $C$DW$T$697

	.dwendtag $C$DW$TU$697


$C$DW$TU$698	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$698

$C$DW$T$698	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$698, DW_AT_type(*$C$DW$T$696)
	.dwattr $C$DW$T$698, DW_AT_byte_size(0xc0)
$C$DW$520	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$520, DW_AT_upper_bound(0x02)

$C$DW$521	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$521, DW_AT_upper_bound(0x07)

	.dwendtag $C$DW$T$698

	.dwendtag $C$DW$TU$698


$C$DW$TU$332	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$332
$C$DW$T$332	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$332, DW_AT_type(*$C$DW$T$337)
	.dwattr $C$DW$T$332, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$332


$C$DW$TU$335	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$335

$C$DW$T$335	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$335, DW_AT_type(*$C$DW$T$332)
$C$DW$522	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$522, DW_AT_type(*$C$DW$T$334)

	.dwendtag $C$DW$T$335

	.dwendtag $C$DW$TU$335


$C$DW$TU$336	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$336

$C$DW$T$336	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$336, DW_AT_type(*$C$DW$T$332)
$C$DW$523	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$523, DW_AT_type(*$C$DW$T$332)

	.dwendtag $C$DW$T$336

	.dwendtag $C$DW$TU$336


$C$DW$TU$377	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$377

$C$DW$T$377	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$377, DW_AT_name("TIDL_Obj")
	.dwattr $C$DW$T$377, DW_AT_byte_size(0x2330)
$C$DW$524	.dwtag  DW_TAG_member
	.dwattr $C$DW$524, DW_AT_type(*$C$DW$T$354)
	.dwattr $C$DW$524, DW_AT_name("ivision")
	.dwattr $C$DW$524, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$524, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$524, DW_AT_decl_line(0x3e6)
	.dwattr $C$DW$524, DW_AT_decl_column(0x19)

$C$DW$525	.dwtag  DW_TAG_member
	.dwattr $C$DW$525, DW_AT_type(*$C$DW$T$318)
	.dwattr $C$DW$525, DW_AT_name("algState")
	.dwattr $C$DW$525, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$525, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$525, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$525, DW_AT_decl_line(0x3e7)
	.dwattr $C$DW$525, DW_AT_decl_column(0x19)

$C$DW$526	.dwtag  DW_TAG_member
	.dwattr $C$DW$526, DW_AT_type(*$C$DW$T$50)
	.dwattr $C$DW$526, DW_AT_name("numMemRecs")
	.dwattr $C$DW$526, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$526, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$526, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$526, DW_AT_decl_line(0x3e8)
	.dwattr $C$DW$526, DW_AT_decl_column(0x19)

$C$DW$527	.dwtag  DW_TAG_member
	.dwattr $C$DW$527, DW_AT_type(*$C$DW$T$355)
	.dwattr $C$DW$527, DW_AT_name("memRec")
	.dwattr $C$DW$527, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$527, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$527, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$527, DW_AT_decl_line(0x3e9)
	.dwattr $C$DW$527, DW_AT_decl_column(0x19)

$C$DW$528	.dwtag  DW_TAG_member
	.dwattr $C$DW$528, DW_AT_type(*$C$DW$T$357)
	.dwattr $C$DW$528, DW_AT_name("createParams")
	.dwattr $C$DW$528, DW_AT_data_member_location[DW_OP_plus_uconst 0x148]
	.dwattr $C$DW$528, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$528, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$528, DW_AT_decl_line(0x3ea)
	.dwattr $C$DW$528, DW_AT_decl_column(0x1a)

$C$DW$529	.dwtag  DW_TAG_member
	.dwattr $C$DW$529, DW_AT_type(*$C$DW$T$358)
	.dwattr $C$DW$529, DW_AT_name("dataBuf")
	.dwattr $C$DW$529, DW_AT_data_member_location[DW_OP_plus_uconst 0x150]
	.dwattr $C$DW$529, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$529, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$529, DW_AT_decl_line(0x3eb)
	.dwattr $C$DW$529, DW_AT_decl_column(0x19)

$C$DW$530	.dwtag  DW_TAG_member
	.dwattr $C$DW$530, DW_AT_type(*$C$DW$T$359)
	.dwattr $C$DW$530, DW_AT_name("sysMems")
	.dwattr $C$DW$530, DW_AT_data_member_location[DW_OP_plus_uconst 0x2150]
	.dwattr $C$DW$530, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$530, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$530, DW_AT_decl_line(0x3ec)
	.dwattr $C$DW$530, DW_AT_decl_column(0x18)

$C$DW$531	.dwtag  DW_TAG_member
	.dwattr $C$DW$531, DW_AT_type(*$C$DW$T$361)
	.dwattr $C$DW$531, DW_AT_name("alglayerParams")
	.dwattr $C$DW$531, DW_AT_data_member_location[DW_OP_plus_uconst 0x2190]
	.dwattr $C$DW$531, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$531, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$531, DW_AT_decl_line(0x3ed)
	.dwattr $C$DW$531, DW_AT_decl_column(0x19)

$C$DW$532	.dwtag  DW_TAG_member
	.dwattr $C$DW$532, DW_AT_type(*$C$DW$T$50)
	.dwattr $C$DW$532, DW_AT_name("procCallCounter")
	.dwattr $C$DW$532, DW_AT_data_member_location[DW_OP_plus_uconst 0x2198]
	.dwattr $C$DW$532, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$532, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$532, DW_AT_decl_line(0x3ee)
	.dwattr $C$DW$532, DW_AT_decl_column(0x19)

$C$DW$533	.dwtag  DW_TAG_member
	.dwattr $C$DW$533, DW_AT_type(*$C$DW$T$363)
	.dwattr $C$DW$533, DW_AT_name("TIDLLayersBuf")
	.dwattr $C$DW$533, DW_AT_data_member_location[DW_OP_plus_uconst 0x21a0]
	.dwattr $C$DW$533, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$533, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$533, DW_AT_decl_line(0x3ef)
	.dwattr $C$DW$533, DW_AT_decl_column(0x19)

$C$DW$534	.dwtag  DW_TAG_member
	.dwattr $C$DW$534, DW_AT_type(*$C$DW$T$364)
	.dwattr $C$DW$534, DW_AT_name("refScratchBuf")
	.dwattr $C$DW$534, DW_AT_data_member_location[DW_OP_plus_uconst 0x21a8]
	.dwattr $C$DW$534, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$534, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$534, DW_AT_decl_line(0x3f0)
	.dwattr $C$DW$534, DW_AT_decl_column(0x1d)

$C$DW$535	.dwtag  DW_TAG_member
	.dwattr $C$DW$535, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$535, DW_AT_name("refScratchBufSize")
	.dwattr $C$DW$535, DW_AT_data_member_location[DW_OP_plus_uconst 0x21b0]
	.dwattr $C$DW$535, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$535, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$535, DW_AT_decl_line(0x3f1)
	.dwattr $C$DW$535, DW_AT_decl_column(0x1d)

$C$DW$536	.dwtag  DW_TAG_member
	.dwattr $C$DW$536, DW_AT_type(*$C$DW$T$365)
	.dwattr $C$DW$536, DW_AT_name("activationHistPtr")
	.dwattr $C$DW$536, DW_AT_data_member_location[DW_OP_plus_uconst 0x21b8]
	.dwattr $C$DW$536, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$536, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$536, DW_AT_decl_line(0x3f2)
	.dwattr $C$DW$536, DW_AT_decl_column(0x1a)

$C$DW$537	.dwtag  DW_TAG_member
	.dwattr $C$DW$537, DW_AT_type(*$C$DW$T$366)
	.dwattr $C$DW$537, DW_AT_name("activationRangePtr")
	.dwattr $C$DW$537, DW_AT_data_member_location[DW_OP_plus_uconst 0x21c0]
	.dwattr $C$DW$537, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$537, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$537, DW_AT_decl_line(0x3f3)
	.dwattr $C$DW$537, DW_AT_decl_column(0x1a)

$C$DW$538	.dwtag  DW_TAG_member
	.dwattr $C$DW$538, DW_AT_type(*$C$DW$T$367)
	.dwattr $C$DW$538, DW_AT_name("sysScratchPtr")
	.dwattr $C$DW$538, DW_AT_data_member_location[DW_OP_plus_uconst 0x21c8]
	.dwattr $C$DW$538, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$538, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$538, DW_AT_decl_line(0x3f4)
	.dwattr $C$DW$538, DW_AT_decl_column(0x17)

$C$DW$539	.dwtag  DW_TAG_member
	.dwattr $C$DW$539, DW_AT_type(*$C$DW$T$369)
	.dwattr $C$DW$539, DW_AT_name("perfSimOutput")
	.dwattr $C$DW$539, DW_AT_data_member_location[DW_OP_plus_uconst 0x21e8]
	.dwattr $C$DW$539, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$539, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$539, DW_AT_decl_line(0x3f5)
	.dwattr $C$DW$539, DW_AT_decl_column(0x1c)

$C$DW$540	.dwtag  DW_TAG_member
	.dwattr $C$DW$540, DW_AT_type(*$C$DW$T$364)
	.dwattr $C$DW$540, DW_AT_name("zeroVector1k")
	.dwattr $C$DW$540, DW_AT_data_member_location[DW_OP_plus_uconst 0x21f0]
	.dwattr $C$DW$540, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$540, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$540, DW_AT_decl_line(0x3f6)
	.dwattr $C$DW$540, DW_AT_decl_column(0x0d)

$C$DW$541	.dwtag  DW_TAG_member
	.dwattr $C$DW$541, DW_AT_type(*$C$DW$T$364)
	.dwattr $C$DW$541, DW_AT_name("intAlgHandle")
	.dwattr $C$DW$541, DW_AT_data_member_location[DW_OP_plus_uconst 0x21f8]
	.dwattr $C$DW$541, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$541, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$541, DW_AT_decl_line(0x3f7)
	.dwattr $C$DW$541, DW_AT_decl_column(0x1f)

$C$DW$542	.dwtag  DW_TAG_member
	.dwattr $C$DW$542, DW_AT_type(*$C$DW$T$50)
	.dwattr $C$DW$542, DW_AT_name("memAvailForFeat")
	.dwattr $C$DW$542, DW_AT_data_member_location[DW_OP_plus_uconst 0x2200]
	.dwattr $C$DW$542, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$542, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$542, DW_AT_decl_line(0x3f8)
	.dwattr $C$DW$542, DW_AT_decl_column(0x1f)

$C$DW$543	.dwtag  DW_TAG_member
	.dwattr $C$DW$543, DW_AT_type(*$C$DW$T$318)
	.dwattr $C$DW$543, DW_AT_name("isPadDmaPending")
	.dwattr $C$DW$543, DW_AT_data_member_location[DW_OP_plus_uconst 0x2204]
	.dwattr $C$DW$543, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$543, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$543, DW_AT_decl_line(0x3f9)
	.dwattr $C$DW$543, DW_AT_decl_column(0x20)

$C$DW$544	.dwtag  DW_TAG_member
	.dwattr $C$DW$544, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$544, DW_AT_name("udmaDrvObj")
	.dwattr $C$DW$544, DW_AT_data_member_location[DW_OP_plus_uconst 0x2208]
	.dwattr $C$DW$544, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$544, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$544, DW_AT_decl_line(0x3fa)
	.dwattr $C$DW$544, DW_AT_decl_column(0x1f)

$C$DW$545	.dwtag  DW_TAG_member
	.dwattr $C$DW$545, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$545, DW_AT_name("dmaUtilsContext")
	.dwattr $C$DW$545, DW_AT_data_member_location[DW_OP_plus_uconst 0x2210]
	.dwattr $C$DW$545, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$545, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$545, DW_AT_decl_line(0x3fb)
	.dwattr $C$DW$545, DW_AT_decl_column(0x1f)

$C$DW$546	.dwtag  DW_TAG_member
	.dwattr $C$DW$546, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$546, DW_AT_name("privContext")
	.dwattr $C$DW$546, DW_AT_data_member_location[DW_OP_plus_uconst 0x2218]
	.dwattr $C$DW$546, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$546, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$546, DW_AT_decl_line(0x3fc)
	.dwattr $C$DW$546, DW_AT_decl_column(0x1f)

$C$DW$547	.dwtag  DW_TAG_member
	.dwattr $C$DW$547, DW_AT_type(*$C$DW$T$370)
	.dwattr $C$DW$547, DW_AT_name("layerShare")
	.dwattr $C$DW$547, DW_AT_data_member_location[DW_OP_plus_uconst 0x2220]
	.dwattr $C$DW$547, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$547, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$547, DW_AT_decl_line(0x3fd)
	.dwattr $C$DW$547, DW_AT_decl_column(0x1a)

$C$DW$548	.dwtag  DW_TAG_member
	.dwattr $C$DW$548, DW_AT_type(*$C$DW$T$371)
	.dwattr $C$DW$548, DW_AT_name("weightStageSync")
	.dwattr $C$DW$548, DW_AT_data_member_location[DW_OP_plus_uconst 0x2280]
	.dwattr $C$DW$548, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$548, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$548, DW_AT_decl_line(0x3fe)
	.dwattr $C$DW$548, DW_AT_decl_column(0x1b)

$C$DW$549	.dwtag  DW_TAG_member
	.dwattr $C$DW$549, DW_AT_type(*$C$DW$T$319)
	.dwattr $C$DW$549, DW_AT_name("memcpyTr")
	.dwattr $C$DW$549, DW_AT_data_member_location[DW_OP_plus_uconst 0x22e0]
	.dwattr $C$DW$549, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$549, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$549, DW_AT_decl_line(0x3ff)
	.dwattr $C$DW$549, DW_AT_decl_column(0x0b)

$C$DW$550	.dwtag  DW_TAG_member
	.dwattr $C$DW$550, DW_AT_type(*$C$DW$T$364)
	.dwattr $C$DW$550, DW_AT_name("stagingMemPtr")
	.dwattr $C$DW$550, DW_AT_data_member_location[DW_OP_plus_uconst 0x2320]
	.dwattr $C$DW$550, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$550, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$550, DW_AT_decl_line(0x400)
	.dwattr $C$DW$550, DW_AT_decl_column(0x0c)

$C$DW$551	.dwtag  DW_TAG_member
	.dwattr $C$DW$551, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$551, DW_AT_name("currAlgLayer")
	.dwattr $C$DW$551, DW_AT_data_member_location[DW_OP_plus_uconst 0x2328]
	.dwattr $C$DW$551, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$551, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$551, DW_AT_decl_line(0x401)
	.dwattr $C$DW$551, DW_AT_decl_column(0x0b)


$C$DW$552	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$552, DW_AT_name("operator =")
	.dwattr $C$DW$552, DW_AT_declaration
	.dwattr $C$DW$552, DW_AT_linkage_name("_ZN8TIDL_ObjaSERKS_")
	.dwattr $C$DW$552, DW_AT_type(*$C$DW$T$372)
	.dwattr $C$DW$552, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$553	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$553, DW_AT_type(*$C$DW$T$374)

	.dwendtag $C$DW$552


$C$DW$554	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$554, DW_AT_name("operator =")
	.dwattr $C$DW$554, DW_AT_declaration
	.dwattr $C$DW$554, DW_AT_linkage_name("_ZN8TIDL_ObjaSEOS_")
	.dwattr $C$DW$554, DW_AT_type(*$C$DW$T$372)
	.dwattr $C$DW$554, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$555	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$555, DW_AT_type(*$C$DW$T$372)

	.dwendtag $C$DW$554

	.dwattr $C$DW$T$377, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$T$377, DW_AT_decl_line(0x3e5)
	.dwattr $C$DW$T$377, DW_AT_decl_column(0x01)
	.dwendtag $C$DW$T$377

	.dwendtag $C$DW$TU$377


$C$DW$TU$373	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$373
$C$DW$T$373	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$373, DW_AT_type(*$C$DW$T$377)

	.dwendtag $C$DW$TU$373


$C$DW$TU$374	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$374
$C$DW$T$374	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$374, DW_AT_type(*$C$DW$T$373)
	.dwattr $C$DW$T$374, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$374


$C$DW$TU$995	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$995
$C$DW$T$995	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$995, DW_AT_name("TIDL_Obj")
	.dwattr $C$DW$T$995, DW_AT_type(*$C$DW$T$377)
	.dwattr $C$DW$T$995, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$T$995, DW_AT_decl_line(0x402)
	.dwattr $C$DW$T$995, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$995


$C$DW$TU$996	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$996
$C$DW$T$996	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$996, DW_AT_type(*$C$DW$T$995)
	.dwattr $C$DW$T$996, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$996


$C$DW$TU$997	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$997
$C$DW$T$997	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$997, DW_AT_name("TIDL_Handle")
	.dwattr $C$DW$T$997, DW_AT_type(*$C$DW$T$996)
	.dwattr $C$DW$T$997, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$T$997, DW_AT_decl_line(0x404)
	.dwattr $C$DW$T$997, DW_AT_decl_column(0x14)

	.dwendtag $C$DW$TU$997


$C$DW$TU$372	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$372
$C$DW$T$372	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$372, DW_AT_type(*$C$DW$T$377)
	.dwattr $C$DW$T$372, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$372


$C$DW$TU$375	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$375

$C$DW$T$375	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$375, DW_AT_type(*$C$DW$T$372)
$C$DW$556	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$556, DW_AT_type(*$C$DW$T$374)

	.dwendtag $C$DW$T$375

	.dwendtag $C$DW$TU$375


$C$DW$TU$376	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$376

$C$DW$T$376	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$376, DW_AT_type(*$C$DW$T$372)
$C$DW$557	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$557, DW_AT_type(*$C$DW$T$372)

	.dwendtag $C$DW$T$376

	.dwendtag $C$DW$TU$376


$C$DW$TU$404	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$404

$C$DW$T$404	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$404, DW_AT_name("TIDL_TensorTransferInfo")
	.dwattr $C$DW$T$404, DW_AT_byte_size(0x24)
$C$DW$558	.dwtag  DW_TAG_member
	.dwattr $C$DW$558, DW_AT_type(*$C$DW$T$398)
	.dwattr $C$DW$558, DW_AT_name("tensorType")
	.dwattr $C$DW$558, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$558, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$558, DW_AT_decl_line(0x333)
	.dwattr $C$DW$558, DW_AT_decl_column(0x0f)

$C$DW$559	.dwtag  DW_TAG_member
	.dwattr $C$DW$559, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$559, DW_AT_name("transferSize")
	.dwattr $C$DW$559, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$559, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$559, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$559, DW_AT_decl_line(0x334)
	.dwattr $C$DW$559, DW_AT_decl_column(0x0b)

$C$DW$560	.dwtag  DW_TAG_member
	.dwattr $C$DW$560, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$560, DW_AT_name("elementSizeinBytes")
	.dwattr $C$DW$560, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$560, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$560, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$560, DW_AT_decl_line(0x335)
	.dwattr $C$DW$560, DW_AT_decl_column(0x0b)

$C$DW$561	.dwtag  DW_TAG_member
	.dwattr $C$DW$561, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$561, DW_AT_name("dmaNumChs")
	.dwattr $C$DW$561, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$561, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$561, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$561, DW_AT_decl_line(0x336)
	.dwattr $C$DW$561, DW_AT_decl_column(0x0b)

$C$DW$562	.dwtag  DW_TAG_member
	.dwattr $C$DW$562, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$562, DW_AT_name("dmaStartChIndex")
	.dwattr $C$DW$562, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$562, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$562, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$562, DW_AT_decl_line(0x337)
	.dwattr $C$DW$562, DW_AT_decl_column(0x0b)

$C$DW$563	.dwtag  DW_TAG_member
	.dwattr $C$DW$563, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$563, DW_AT_name("perChTrSize")
	.dwattr $C$DW$563, DW_AT_data_member_location[DW_OP_plus_uconst 0x14]
	.dwattr $C$DW$563, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$563, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$563, DW_AT_decl_line(0x338)
	.dwattr $C$DW$563, DW_AT_decl_column(0x0b)

$C$DW$564	.dwtag  DW_TAG_member
	.dwattr $C$DW$564, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$564, DW_AT_name("lastChTrSize")
	.dwattr $C$DW$564, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$564, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$564, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$564, DW_AT_decl_line(0x339)
	.dwattr $C$DW$564, DW_AT_decl_column(0x0b)

$C$DW$565	.dwtag  DW_TAG_member
	.dwattr $C$DW$565, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$565, DW_AT_name("srcChOffset")
	.dwattr $C$DW$565, DW_AT_data_member_location[DW_OP_plus_uconst 0x1c]
	.dwattr $C$DW$565, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$565, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$565, DW_AT_decl_line(0x33a)
	.dwattr $C$DW$565, DW_AT_decl_column(0x0b)

$C$DW$566	.dwtag  DW_TAG_member
	.dwattr $C$DW$566, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$566, DW_AT_name("chPitchL3InBytes")
	.dwattr $C$DW$566, DW_AT_data_member_location[DW_OP_plus_uconst 0x20]
	.dwattr $C$DW$566, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$566, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$566, DW_AT_decl_line(0x33b)
	.dwattr $C$DW$566, DW_AT_decl_column(0x0b)


$C$DW$567	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$567, DW_AT_name("operator =")
	.dwattr $C$DW$567, DW_AT_declaration
	.dwattr $C$DW$567, DW_AT_linkage_name("_ZN23TIDL_TensorTransferInfoaSERKS_")
	.dwattr $C$DW$567, DW_AT_type(*$C$DW$T$399)
	.dwattr $C$DW$567, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$568	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$568, DW_AT_type(*$C$DW$T$401)

	.dwendtag $C$DW$567


$C$DW$569	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$569, DW_AT_name("operator =")
	.dwattr $C$DW$569, DW_AT_declaration
	.dwattr $C$DW$569, DW_AT_linkage_name("_ZN23TIDL_TensorTransferInfoaSEOS_")
	.dwattr $C$DW$569, DW_AT_type(*$C$DW$T$399)
	.dwattr $C$DW$569, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$570	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$570, DW_AT_type(*$C$DW$T$399)

	.dwendtag $C$DW$569

	.dwattr $C$DW$T$404, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$T$404, DW_AT_decl_line(0x332)
	.dwattr $C$DW$T$404, DW_AT_decl_column(0x01)
	.dwendtag $C$DW$T$404

	.dwendtag $C$DW$TU$404


$C$DW$TU$315	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$315
$C$DW$T$315	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$315, DW_AT_name("TIDL_TensorTransferInfo")
	.dwattr $C$DW$T$315, DW_AT_type(*$C$DW$T$404)
	.dwattr $C$DW$T$315, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$T$315, DW_AT_decl_line(0x33c)
	.dwattr $C$DW$T$315, DW_AT_decl_column(0x02)

	.dwendtag $C$DW$TU$315


$C$DW$TU$316	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$316

$C$DW$T$316	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$316, DW_AT_type(*$C$DW$T$315)
	.dwattr $C$DW$T$316, DW_AT_byte_size(0x168)
$C$DW$571	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$571, DW_AT_upper_bound(0x09)

	.dwendtag $C$DW$T$316

	.dwendtag $C$DW$TU$316


$C$DW$TU$400	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$400
$C$DW$T$400	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$400, DW_AT_type(*$C$DW$T$404)

	.dwendtag $C$DW$TU$400


$C$DW$TU$401	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$401
$C$DW$T$401	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$401, DW_AT_type(*$C$DW$T$400)
	.dwattr $C$DW$T$401, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$401


$C$DW$TU$399	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$399
$C$DW$T$399	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$399, DW_AT_type(*$C$DW$T$404)
	.dwattr $C$DW$T$399, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$399


$C$DW$TU$402	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$402

$C$DW$T$402	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$402, DW_AT_type(*$C$DW$T$399)
$C$DW$572	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$572, DW_AT_type(*$C$DW$T$401)

	.dwendtag $C$DW$T$402

	.dwendtag $C$DW$TU$402


$C$DW$TU$403	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$403

$C$DW$T$403	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$403, DW_AT_type(*$C$DW$T$399)
$C$DW$573	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$573, DW_AT_type(*$C$DW$T$399)

	.dwendtag $C$DW$T$403

	.dwendtag $C$DW$TU$403


$C$DW$TU$412	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$412

$C$DW$T$412	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$412, DW_AT_name("TIDL_sysScratchPtr")
	.dwattr $C$DW$T$412, DW_AT_byte_size(0x20)
$C$DW$574	.dwtag  DW_TAG_member
	.dwattr $C$DW$574, DW_AT_type(*$C$DW$T$364)
	.dwattr $C$DW$574, DW_AT_name("l1BasePtr")
	.dwattr $C$DW$574, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$574, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$574, DW_AT_decl_line(0x3ba)
	.dwattr $C$DW$574, DW_AT_decl_column(0x1d)

$C$DW$575	.dwtag  DW_TAG_member
	.dwattr $C$DW$575, DW_AT_type(*$C$DW$T$364)
	.dwattr $C$DW$575, DW_AT_name("l2BasePtr")
	.dwattr $C$DW$575, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$575, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$575, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$575, DW_AT_decl_line(0x3bb)
	.dwattr $C$DW$575, DW_AT_decl_column(0x1d)

$C$DW$576	.dwtag  DW_TAG_member
	.dwattr $C$DW$576, DW_AT_type(*$C$DW$T$364)
	.dwattr $C$DW$576, DW_AT_name("l3BasePtr")
	.dwattr $C$DW$576, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$576, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$576, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$576, DW_AT_decl_line(0x3bc)
	.dwattr $C$DW$576, DW_AT_decl_column(0x1d)

$C$DW$577	.dwtag  DW_TAG_member
	.dwattr $C$DW$577, DW_AT_type(*$C$DW$T$364)
	.dwattr $C$DW$577, DW_AT_name("ddrBasePtr")
	.dwattr $C$DW$577, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$577, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$577, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$577, DW_AT_decl_line(0x3bd)
	.dwattr $C$DW$577, DW_AT_decl_column(0x1d)


$C$DW$578	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$578, DW_AT_name("operator =")
	.dwattr $C$DW$578, DW_AT_declaration
	.dwattr $C$DW$578, DW_AT_linkage_name("_ZN18TIDL_sysScratchPtraSERKS_")
	.dwattr $C$DW$578, DW_AT_type(*$C$DW$T$407)
	.dwattr $C$DW$578, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$579	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$579, DW_AT_type(*$C$DW$T$409)

	.dwendtag $C$DW$578


$C$DW$580	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$580, DW_AT_name("operator =")
	.dwattr $C$DW$580, DW_AT_declaration
	.dwattr $C$DW$580, DW_AT_linkage_name("_ZN18TIDL_sysScratchPtraSEOS_")
	.dwattr $C$DW$580, DW_AT_type(*$C$DW$T$407)
	.dwattr $C$DW$580, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$581	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$581, DW_AT_type(*$C$DW$T$407)

	.dwendtag $C$DW$580

	.dwattr $C$DW$T$412, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$T$412, DW_AT_decl_line(0x3b9)
	.dwattr $C$DW$T$412, DW_AT_decl_column(0x01)
	.dwendtag $C$DW$T$412

	.dwendtag $C$DW$TU$412


$C$DW$TU$367	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$367
$C$DW$T$367	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$367, DW_AT_name("TIDL_sysScratchPtr")
	.dwattr $C$DW$T$367, DW_AT_type(*$C$DW$T$412)
	.dwattr $C$DW$T$367, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$T$367, DW_AT_decl_line(0x3be)
	.dwattr $C$DW$T$367, DW_AT_decl_column(0x02)

	.dwendtag $C$DW$TU$367


$C$DW$TU$408	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$408
$C$DW$T$408	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$408, DW_AT_type(*$C$DW$T$412)

	.dwendtag $C$DW$TU$408


$C$DW$TU$409	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$409
$C$DW$T$409	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$409, DW_AT_type(*$C$DW$T$408)
	.dwattr $C$DW$T$409, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$409


$C$DW$TU$407	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$407
$C$DW$T$407	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$407, DW_AT_type(*$C$DW$T$412)
	.dwattr $C$DW$T$407, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$407


$C$DW$TU$410	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$410

$C$DW$T$410	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$410, DW_AT_type(*$C$DW$T$407)
$C$DW$582	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$582, DW_AT_type(*$C$DW$T$409)

	.dwendtag $C$DW$T$410

	.dwendtag $C$DW$TU$410


$C$DW$TU$411	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$411

$C$DW$T$411	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$411, DW_AT_type(*$C$DW$T$407)
$C$DW$583	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$583, DW_AT_type(*$C$DW$T$407)

	.dwendtag $C$DW$T$411

	.dwendtag $C$DW$TU$411


$C$DW$TU$442	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$442

$C$DW$T$442	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$442, DW_AT_name("__HWA_CONFIG_REG_v1")
	.dwattr $C$DW$T$442, DW_AT_byte_size(0x40)
$C$DW$584	.dwtag  DW_TAG_member
	.dwattr $C$DW$584, DW_AT_type(*$C$DW$T$415)
	.dwattr $C$DW$584, DW_AT_name("A_ATYPE")
	.dwattr $C$DW$584, DW_AT_bit_offset(0x05)
	.dwattr $C$DW$584, DW_AT_bit_size(0x03)
	.dwattr $C$DW$584, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$584, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$584, DW_AT_decl_line(0x13a)
	.dwattr $C$DW$584, DW_AT_decl_column(0x1a)

$C$DW$585	.dwtag  DW_TAG_member
	.dwattr $C$DW$585, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$585, DW_AT_name("A_RSVD1")
	.dwattr $C$DW$585, DW_AT_bit_offset(0x38)
	.dwattr $C$DW$585, DW_AT_bit_size(0x05)
	.dwattr $C$DW$585, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$585, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$585, DW_AT_decl_line(0x13b)
	.dwattr $C$DW$585, DW_AT_decl_column(0x0e)

$C$DW$586	.dwtag  DW_TAG_member
	.dwattr $C$DW$586, DW_AT_type(*$C$DW$T$419)
	.dwattr $C$DW$586, DW_AT_name("A_ALUTEN")
	.dwattr $C$DW$586, DW_AT_bit_offset(0x07)
	.dwattr $C$DW$586, DW_AT_bit_size(0x01)
	.dwattr $C$DW$586, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$586, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$586, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$586, DW_AT_decl_line(0x13c)
	.dwattr $C$DW$586, DW_AT_decl_column(0x13)

$C$DW$587	.dwtag  DW_TAG_member
	.dwattr $C$DW$587, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$587, DW_AT_name("A_RSVD2")
	.dwattr $C$DW$587, DW_AT_bit_offset(0x20)
	.dwattr $C$DW$587, DW_AT_bit_size(0x17)
	.dwattr $C$DW$587, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$587, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$587, DW_AT_decl_line(0x13d)
	.dwattr $C$DW$587, DW_AT_decl_column(0x0e)

$C$DW$588	.dwtag  DW_TAG_member
	.dwattr $C$DW$588, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$588, DW_AT_name("B_BSWPER")
	.dwattr $C$DW$588, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$588, DW_AT_bit_size(0x20)
	.dwattr $C$DW$588, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$588, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$588, DW_AT_decl_line(0x13f)
	.dwattr $C$DW$588, DW_AT_decl_column(0x0e)

$C$DW$589	.dwtag  DW_TAG_member
	.dwattr $C$DW$589, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$589, DW_AT_name("B_BRSTPER")
	.dwattr $C$DW$589, DW_AT_bit_offset(0x38)
	.dwattr $C$DW$589, DW_AT_bit_size(0x08)
	.dwattr $C$DW$589, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$589, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$589, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$589, DW_AT_decl_line(0x141)
	.dwattr $C$DW$589, DW_AT_decl_column(0x0e)

$C$DW$590	.dwtag  DW_TAG_member
	.dwattr $C$DW$590, DW_AT_type(*$C$DW$T$421)
	.dwattr $C$DW$590, DW_AT_name("B_BTYPE")
	.dwattr $C$DW$590, DW_AT_bit_offset(0x06)
	.dwattr $C$DW$590, DW_AT_bit_size(0x02)
	.dwattr $C$DW$590, DW_AT_data_member_location[DW_OP_plus_uconst 0x9]
	.dwattr $C$DW$590, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$590, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$590, DW_AT_decl_line(0x142)
	.dwattr $C$DW$590, DW_AT_decl_column(0x13)

$C$DW$591	.dwtag  DW_TAG_member
	.dwattr $C$DW$591, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$591, DW_AT_name("B_RSVD1")
	.dwattr $C$DW$591, DW_AT_bit_offset(0x30)
	.dwattr $C$DW$591, DW_AT_bit_size(0x06)
	.dwattr $C$DW$591, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$591, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$591, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$591, DW_AT_decl_line(0x143)
	.dwattr $C$DW$591, DW_AT_decl_column(0x0e)

$C$DW$592	.dwtag  DW_TAG_member
	.dwattr $C$DW$592, DW_AT_type(*$C$DW$T$423)
	.dwattr $C$DW$592, DW_AT_name("B_ORDER")
	.dwattr $C$DW$592, DW_AT_bit_offset(0x07)
	.dwattr $C$DW$592, DW_AT_bit_size(0x01)
	.dwattr $C$DW$592, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr $C$DW$592, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$592, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$592, DW_AT_decl_line(0x144)
	.dwattr $C$DW$592, DW_AT_decl_column(0x2c)

$C$DW$593	.dwtag  DW_TAG_member
	.dwattr $C$DW$593, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$593, DW_AT_name("B_RSVD2")
	.dwattr $C$DW$593, DW_AT_bit_offset(0x28)
	.dwattr $C$DW$593, DW_AT_bit_size(0x07)
	.dwattr $C$DW$593, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$593, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$593, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$593, DW_AT_decl_line(0x145)
	.dwattr $C$DW$593, DW_AT_decl_column(0x0e)

$C$DW$594	.dwtag  DW_TAG_member
	.dwattr $C$DW$594, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$594, DW_AT_name("B_BSTART")
	.dwattr $C$DW$594, DW_AT_bit_offset(0x27)
	.dwattr $C$DW$594, DW_AT_bit_size(0x01)
	.dwattr $C$DW$594, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$594, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$594, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$594, DW_AT_decl_line(0x146)
	.dwattr $C$DW$594, DW_AT_decl_column(0x0e)

$C$DW$595	.dwtag  DW_TAG_member
	.dwattr $C$DW$595, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$595, DW_AT_name("B_RSVD3")
	.dwattr $C$DW$595, DW_AT_bit_offset(0x20)
	.dwattr $C$DW$595, DW_AT_bit_size(0x07)
	.dwattr $C$DW$595, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$595, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$595, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$595, DW_AT_decl_line(0x148)
	.dwattr $C$DW$595, DW_AT_decl_column(0x0e)

$C$DW$596	.dwtag  DW_TAG_member
	.dwattr $C$DW$596, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$596, DW_AT_name("B_BOFFSET")
	.dwattr $C$DW$596, DW_AT_bit_offset(0x18)
	.dwattr $C$DW$596, DW_AT_bit_size(0x08)
	.dwattr $C$DW$596, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$596, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$596, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$596, DW_AT_decl_line(0x149)
	.dwattr $C$DW$596, DW_AT_decl_column(0x0e)

$C$DW$597	.dwtag  DW_TAG_member
	.dwattr $C$DW$597, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$597, DW_AT_name("B_RSVD4")
	.dwattr $C$DW$597, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$597, DW_AT_bit_size(0x18)
	.dwattr $C$DW$597, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$597, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$597, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$597, DW_AT_decl_line(0x14a)
	.dwattr $C$DW$597, DW_AT_decl_column(0x0e)

$C$DW$598	.dwtag  DW_TAG_member
	.dwattr $C$DW$598, DW_AT_type(*$C$DW$T$425)
	.dwattr $C$DW$598, DW_AT_name("C_ATYPE")
	.dwattr $C$DW$598, DW_AT_bit_offset(0x07)
	.dwattr $C$DW$598, DW_AT_bit_size(0x01)
	.dwattr $C$DW$598, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$598, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$598, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$598, DW_AT_decl_line(0x14d)
	.dwattr $C$DW$598, DW_AT_decl_column(0x1a)

$C$DW$599	.dwtag  DW_TAG_member
	.dwattr $C$DW$599, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$599, DW_AT_name("C_RSVD1")
	.dwattr $C$DW$599, DW_AT_bit_offset(0x38)
	.dwattr $C$DW$599, DW_AT_bit_size(0x07)
	.dwattr $C$DW$599, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$599, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$599, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$599, DW_AT_decl_line(0x14e)
	.dwattr $C$DW$599, DW_AT_decl_column(0x0e)

$C$DW$600	.dwtag  DW_TAG_member
	.dwattr $C$DW$600, DW_AT_type(*$C$DW$T$427)
	.dwattr $C$DW$600, DW_AT_name("C_BTYPE")
	.dwattr $C$DW$600, DW_AT_bit_offset(0x05)
	.dwattr $C$DW$600, DW_AT_bit_size(0x03)
	.dwattr $C$DW$600, DW_AT_data_member_location[DW_OP_plus_uconst 0x11]
	.dwattr $C$DW$600, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$600, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$600, DW_AT_decl_line(0x14f)
	.dwattr $C$DW$600, DW_AT_decl_column(0x1a)

$C$DW$601	.dwtag  DW_TAG_member
	.dwattr $C$DW$601, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$601, DW_AT_name("C_RSVD2")
	.dwattr $C$DW$601, DW_AT_bit_offset(0x30)
	.dwattr $C$DW$601, DW_AT_bit_size(0x05)
	.dwattr $C$DW$601, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$601, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$601, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$601, DW_AT_decl_line(0x152)
	.dwattr $C$DW$601, DW_AT_decl_column(0x0e)

$C$DW$602	.dwtag  DW_TAG_member
	.dwattr $C$DW$602, DW_AT_type(*$C$DW$T$429)
	.dwattr $C$DW$602, DW_AT_name("C_OPERATION0")
	.dwattr $C$DW$602, DW_AT_bit_offset(0x06)
	.dwattr $C$DW$602, DW_AT_bit_size(0x02)
	.dwattr $C$DW$602, DW_AT_data_member_location[DW_OP_plus_uconst 0x12]
	.dwattr $C$DW$602, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$602, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$602, DW_AT_decl_line(0x153)
	.dwattr $C$DW$602, DW_AT_decl_column(0x1e)

$C$DW$603	.dwtag  DW_TAG_member
	.dwattr $C$DW$603, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$603, DW_AT_name("C_RSVD3")
	.dwattr $C$DW$603, DW_AT_bit_offset(0x28)
	.dwattr $C$DW$603, DW_AT_bit_size(0x06)
	.dwattr $C$DW$603, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$603, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$603, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$603, DW_AT_decl_line(0x154)
	.dwattr $C$DW$603, DW_AT_decl_column(0x0e)

$C$DW$604	.dwtag  DW_TAG_member
	.dwattr $C$DW$604, DW_AT_type(*$C$DW$T$429)
	.dwattr $C$DW$604, DW_AT_name("C_OPERATION1")
	.dwattr $C$DW$604, DW_AT_bit_offset(0x06)
	.dwattr $C$DW$604, DW_AT_bit_size(0x02)
	.dwattr $C$DW$604, DW_AT_data_member_location[DW_OP_plus_uconst 0x13]
	.dwattr $C$DW$604, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$604, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$604, DW_AT_decl_line(0x155)
	.dwattr $C$DW$604, DW_AT_decl_column(0x1e)

$C$DW$605	.dwtag  DW_TAG_member
	.dwattr $C$DW$605, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$605, DW_AT_name("C_RSVD4")
	.dwattr $C$DW$605, DW_AT_bit_offset(0x20)
	.dwattr $C$DW$605, DW_AT_bit_size(0x06)
	.dwattr $C$DW$605, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$605, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$605, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$605, DW_AT_decl_line(0x156)
	.dwattr $C$DW$605, DW_AT_decl_column(0x0e)

$C$DW$606	.dwtag  DW_TAG_member
	.dwattr $C$DW$606, DW_AT_type(*$C$DW$T$431)
	.dwattr $C$DW$606, DW_AT_name("C_HWLDDST")
	.dwattr $C$DW$606, DW_AT_bit_offset(0x05)
	.dwattr $C$DW$606, DW_AT_bit_size(0x03)
	.dwattr $C$DW$606, DW_AT_data_member_location[DW_OP_plus_uconst 0x14]
	.dwattr $C$DW$606, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$606, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$606, DW_AT_decl_line(0x157)
	.dwattr $C$DW$606, DW_AT_decl_column(0x1c)

$C$DW$607	.dwtag  DW_TAG_member
	.dwattr $C$DW$607, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$607, DW_AT_name("C_RSVD5")
	.dwattr $C$DW$607, DW_AT_bit_offset(0x18)
	.dwattr $C$DW$607, DW_AT_bit_size(0x05)
	.dwattr $C$DW$607, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$607, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$607, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$607, DW_AT_decl_line(0x158)
	.dwattr $C$DW$607, DW_AT_decl_column(0x0e)

$C$DW$608	.dwtag  DW_TAG_member
	.dwattr $C$DW$608, DW_AT_type(*$C$DW$T$433)
	.dwattr $C$DW$608, DW_AT_name("C_HWLDTYPE")
	.dwattr $C$DW$608, DW_AT_bit_offset(0x04)
	.dwattr $C$DW$608, DW_AT_bit_size(0x04)
	.dwattr $C$DW$608, DW_AT_data_member_location[DW_OP_plus_uconst 0x15]
	.dwattr $C$DW$608, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$608, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$608, DW_AT_decl_line(0x159)
	.dwattr $C$DW$608, DW_AT_decl_column(0x1d)

$C$DW$609	.dwtag  DW_TAG_member
	.dwattr $C$DW$609, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$609, DW_AT_name("C_RSVD6")
	.dwattr $C$DW$609, DW_AT_bit_offset(0x10)
	.dwattr $C$DW$609, DW_AT_bit_size(0x04)
	.dwattr $C$DW$609, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$609, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$609, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$609, DW_AT_decl_line(0x15b)
	.dwattr $C$DW$609, DW_AT_decl_column(0x0e)

$C$DW$610	.dwtag  DW_TAG_member
	.dwattr $C$DW$610, DW_AT_type(*$C$DW$T$435)
	.dwattr $C$DW$610, DW_AT_name("C_OPSTART")
	.dwattr $C$DW$610, DW_AT_bit_offset(0x07)
	.dwattr $C$DW$610, DW_AT_bit_size(0x01)
	.dwattr $C$DW$610, DW_AT_data_member_location[DW_OP_plus_uconst 0x16]
	.dwattr $C$DW$610, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$610, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$610, DW_AT_decl_line(0x15c)
	.dwattr $C$DW$610, DW_AT_decl_column(0x1c)

$C$DW$611	.dwtag  DW_TAG_member
	.dwattr $C$DW$611, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$611, DW_AT_name("C_BSTART")
	.dwattr $C$DW$611, DW_AT_bit_offset(0x0e)
	.dwattr $C$DW$611, DW_AT_bit_size(0x01)
	.dwattr $C$DW$611, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$611, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$611, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$611, DW_AT_decl_line(0x15d)
	.dwattr $C$DW$611, DW_AT_decl_column(0x0e)

$C$DW$612	.dwtag  DW_TAG_member
	.dwattr $C$DW$612, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$612, DW_AT_name("C_CRSTART")
	.dwattr $C$DW$612, DW_AT_bit_offset(0x0d)
	.dwattr $C$DW$612, DW_AT_bit_size(0x01)
	.dwattr $C$DW$612, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$612, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$612, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$612, DW_AT_decl_line(0x160)
	.dwattr $C$DW$612, DW_AT_decl_column(0x0e)

$C$DW$613	.dwtag  DW_TAG_member
	.dwattr $C$DW$613, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$613, DW_AT_name("C_CWSTART")
	.dwattr $C$DW$613, DW_AT_bit_offset(0x0c)
	.dwattr $C$DW$613, DW_AT_bit_size(0x01)
	.dwattr $C$DW$613, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$613, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$613, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$613, DW_AT_decl_line(0x161)
	.dwattr $C$DW$613, DW_AT_decl_column(0x0e)

$C$DW$614	.dwtag  DW_TAG_member
	.dwattr $C$DW$614, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$614, DW_AT_name("C_CLSTART")
	.dwattr $C$DW$614, DW_AT_bit_offset(0x0b)
	.dwattr $C$DW$614, DW_AT_bit_size(0x01)
	.dwattr $C$DW$614, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$614, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$614, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$614, DW_AT_decl_line(0x163)
	.dwattr $C$DW$614, DW_AT_decl_column(0x0e)

$C$DW$615	.dwtag  DW_TAG_member
	.dwattr $C$DW$615, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$615, DW_AT_name("C_RSVD7")
	.dwattr $C$DW$615, DW_AT_bit_offset(0x08)
	.dwattr $C$DW$615, DW_AT_bit_size(0x03)
	.dwattr $C$DW$615, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$615, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$615, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$615, DW_AT_decl_line(0x165)
	.dwattr $C$DW$615, DW_AT_decl_column(0x0e)

$C$DW$616	.dwtag  DW_TAG_member
	.dwattr $C$DW$616, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$616, DW_AT_name("C_CROFFSET")
	.dwattr $C$DW$616, DW_AT_bit_offset(0x02)
	.dwattr $C$DW$616, DW_AT_bit_size(0x06)
	.dwattr $C$DW$616, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$616, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$616, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$616, DW_AT_decl_line(0x166)
	.dwattr $C$DW$616, DW_AT_decl_column(0x0e)

$C$DW$617	.dwtag  DW_TAG_member
	.dwattr $C$DW$617, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$617, DW_AT_name("C_RSVD8")
	.dwattr $C$DW$617, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$617, DW_AT_bit_size(0x02)
	.dwattr $C$DW$617, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$617, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$617, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$617, DW_AT_decl_line(0x167)
	.dwattr $C$DW$617, DW_AT_decl_column(0x0e)

$C$DW$618	.dwtag  DW_TAG_member
	.dwattr $C$DW$618, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$618, DW_AT_name("C_CWOFFSET")
	.dwattr $C$DW$618, DW_AT_bit_offset(0x3a)
	.dwattr $C$DW$618, DW_AT_bit_size(0x06)
	.dwattr $C$DW$618, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$618, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$618, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$618, DW_AT_decl_line(0x169)
	.dwattr $C$DW$618, DW_AT_decl_column(0x0e)

$C$DW$619	.dwtag  DW_TAG_member
	.dwattr $C$DW$619, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$619, DW_AT_name("C_RSVD9")
	.dwattr $C$DW$619, DW_AT_bit_offset(0x38)
	.dwattr $C$DW$619, DW_AT_bit_size(0x02)
	.dwattr $C$DW$619, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$619, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$619, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$619, DW_AT_decl_line(0x16a)
	.dwattr $C$DW$619, DW_AT_decl_column(0x0e)

$C$DW$620	.dwtag  DW_TAG_member
	.dwattr $C$DW$620, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$620, DW_AT_name("C_CLOFFSET")
	.dwattr $C$DW$620, DW_AT_bit_offset(0x32)
	.dwattr $C$DW$620, DW_AT_bit_size(0x06)
	.dwattr $C$DW$620, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$620, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$620, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$620, DW_AT_decl_line(0x16b)
	.dwattr $C$DW$620, DW_AT_decl_column(0x0e)

$C$DW$621	.dwtag  DW_TAG_member
	.dwattr $C$DW$621, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$621, DW_AT_name("C_RSVD10")
	.dwattr $C$DW$621, DW_AT_bit_offset(0x30)
	.dwattr $C$DW$621, DW_AT_bit_size(0x02)
	.dwattr $C$DW$621, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$621, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$621, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$621, DW_AT_decl_line(0x16c)
	.dwattr $C$DW$621, DW_AT_decl_column(0x0e)

$C$DW$622	.dwtag  DW_TAG_member
	.dwattr $C$DW$622, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$622, DW_AT_name("C_CLSWPER")
	.dwattr $C$DW$622, DW_AT_bit_offset(0x28)
	.dwattr $C$DW$622, DW_AT_bit_size(0x08)
	.dwattr $C$DW$622, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$622, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$622, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$622, DW_AT_decl_line(0x16d)
	.dwattr $C$DW$622, DW_AT_decl_column(0x0e)

$C$DW$623	.dwtag  DW_TAG_member
	.dwattr $C$DW$623, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$623, DW_AT_name("C_CLRSTPER")
	.dwattr $C$DW$623, DW_AT_bit_offset(0x20)
	.dwattr $C$DW$623, DW_AT_bit_size(0x08)
	.dwattr $C$DW$623, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$623, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$623, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$623, DW_AT_decl_line(0x16f)
	.dwattr $C$DW$623, DW_AT_decl_column(0x0e)

$C$DW$624	.dwtag  DW_TAG_member
	.dwattr $C$DW$624, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$624, DW_AT_name("C_OP1PER")
	.dwattr $C$DW$624, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$624, DW_AT_bit_size(0x20)
	.dwattr $C$DW$624, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$624, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$624, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$624, DW_AT_decl_line(0x170)
	.dwattr $C$DW$624, DW_AT_decl_column(0x0e)

$C$DW$625	.dwtag  DW_TAG_member
	.dwattr $C$DW$625, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$625, DW_AT_name("C_OP0PER")
	.dwattr $C$DW$625, DW_AT_bit_offset(0x20)
	.dwattr $C$DW$625, DW_AT_bit_size(0x20)
	.dwattr $C$DW$625, DW_AT_data_member_location[DW_OP_plus_uconst 0x20]
	.dwattr $C$DW$625, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$625, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$625, DW_AT_decl_line(0x172)
	.dwattr $C$DW$625, DW_AT_decl_column(0x0e)

$C$DW$626	.dwtag  DW_TAG_member
	.dwattr $C$DW$626, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$626, DW_AT_name("C_BSWPER")
	.dwattr $C$DW$626, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$626, DW_AT_bit_size(0x20)
	.dwattr $C$DW$626, DW_AT_data_member_location[DW_OP_plus_uconst 0x20]
	.dwattr $C$DW$626, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$626, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$626, DW_AT_decl_line(0x173)
	.dwattr $C$DW$626, DW_AT_decl_column(0x0e)

$C$DW$627	.dwtag  DW_TAG_member
	.dwattr $C$DW$627, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$627, DW_AT_name("C_CRSWPER")
	.dwattr $C$DW$627, DW_AT_bit_offset(0x20)
	.dwattr $C$DW$627, DW_AT_bit_size(0x20)
	.dwattr $C$DW$627, DW_AT_data_member_location[DW_OP_plus_uconst 0x28]
	.dwattr $C$DW$627, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$627, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$627, DW_AT_decl_line(0x175)
	.dwattr $C$DW$627, DW_AT_decl_column(0x0e)

$C$DW$628	.dwtag  DW_TAG_member
	.dwattr $C$DW$628, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$628, DW_AT_name("C_CWSWPER")
	.dwattr $C$DW$628, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$628, DW_AT_bit_size(0x20)
	.dwattr $C$DW$628, DW_AT_data_member_location[DW_OP_plus_uconst 0x28]
	.dwattr $C$DW$628, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$628, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$628, DW_AT_decl_line(0x176)
	.dwattr $C$DW$628, DW_AT_decl_column(0x0e)

$C$DW$629	.dwtag  DW_TAG_member
	.dwattr $C$DW$629, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$629, DW_AT_name("C_CRRSTPER")
	.dwattr $C$DW$629, DW_AT_bit_offset(0x38)
	.dwattr $C$DW$629, DW_AT_bit_size(0x08)
	.dwattr $C$DW$629, DW_AT_data_member_location[DW_OP_plus_uconst 0x30]
	.dwattr $C$DW$629, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$629, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$629, DW_AT_decl_line(0x178)
	.dwattr $C$DW$629, DW_AT_decl_column(0x0e)

$C$DW$630	.dwtag  DW_TAG_member
	.dwattr $C$DW$630, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$630, DW_AT_name("C_CWRSTPER")
	.dwattr $C$DW$630, DW_AT_bit_offset(0x30)
	.dwattr $C$DW$630, DW_AT_bit_size(0x08)
	.dwattr $C$DW$630, DW_AT_data_member_location[DW_OP_plus_uconst 0x30]
	.dwattr $C$DW$630, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$630, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$630, DW_AT_decl_line(0x179)
	.dwattr $C$DW$630, DW_AT_decl_column(0x0e)

$C$DW$631	.dwtag  DW_TAG_member
	.dwattr $C$DW$631, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$631, DW_AT_name("X_ReLU")
	.dwattr $C$DW$631, DW_AT_bit_offset(0x2f)
	.dwattr $C$DW$631, DW_AT_bit_size(0x01)
	.dwattr $C$DW$631, DW_AT_data_member_location[DW_OP_plus_uconst 0x30]
	.dwattr $C$DW$631, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$631, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$631, DW_AT_decl_line(0x17c)
	.dwattr $C$DW$631, DW_AT_decl_column(0x0e)

$C$DW$632	.dwtag  DW_TAG_member
	.dwattr $C$DW$632, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$632, DW_AT_name("X_RSVD1")
	.dwattr $C$DW$632, DW_AT_bit_offset(0x28)
	.dwattr $C$DW$632, DW_AT_bit_size(0x07)
	.dwattr $C$DW$632, DW_AT_data_member_location[DW_OP_plus_uconst 0x30]
	.dwattr $C$DW$632, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$632, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$632, DW_AT_decl_line(0x17e)
	.dwattr $C$DW$632, DW_AT_decl_column(0x0e)

$C$DW$633	.dwtag  DW_TAG_member
	.dwattr $C$DW$633, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$633, DW_AT_name("X_SAT")
	.dwattr $C$DW$633, DW_AT_bit_offset(0x27)
	.dwattr $C$DW$633, DW_AT_bit_size(0x01)
	.dwattr $C$DW$633, DW_AT_data_member_location[DW_OP_plus_uconst 0x30]
	.dwattr $C$DW$633, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$633, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$633, DW_AT_decl_line(0x17f)
	.dwattr $C$DW$633, DW_AT_decl_column(0x0e)

$C$DW$634	.dwtag  DW_TAG_member
	.dwattr $C$DW$634, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$634, DW_AT_name("X_RSVD2")
	.dwattr $C$DW$634, DW_AT_bit_offset(0x20)
	.dwattr $C$DW$634, DW_AT_bit_size(0x07)
	.dwattr $C$DW$634, DW_AT_data_member_location[DW_OP_plus_uconst 0x30]
	.dwattr $C$DW$634, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$634, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$634, DW_AT_decl_line(0x181)
	.dwattr $C$DW$634, DW_AT_decl_column(0x0e)

$C$DW$635	.dwtag  DW_TAG_member
	.dwattr $C$DW$635, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$635, DW_AT_name("X_RE")
	.dwattr $C$DW$635, DW_AT_bit_offset(0x1f)
	.dwattr $C$DW$635, DW_AT_bit_size(0x01)
	.dwattr $C$DW$635, DW_AT_data_member_location[DW_OP_plus_uconst 0x30]
	.dwattr $C$DW$635, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$635, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$635, DW_AT_decl_line(0x182)
	.dwattr $C$DW$635, DW_AT_decl_column(0x0e)

$C$DW$636	.dwtag  DW_TAG_member
	.dwattr $C$DW$636, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$636, DW_AT_name("X_RSVD3")
	.dwattr $C$DW$636, DW_AT_bit_offset(0x18)
	.dwattr $C$DW$636, DW_AT_bit_size(0x07)
	.dwattr $C$DW$636, DW_AT_data_member_location[DW_OP_plus_uconst 0x30]
	.dwattr $C$DW$636, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$636, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$636, DW_AT_decl_line(0x184)
	.dwattr $C$DW$636, DW_AT_decl_column(0x0e)

$C$DW$637	.dwtag  DW_TAG_member
	.dwattr $C$DW$637, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$637, DW_AT_name("X_SHIFT")
	.dwattr $C$DW$637, DW_AT_bit_offset(0x11)
	.dwattr $C$DW$637, DW_AT_bit_size(0x07)
	.dwattr $C$DW$637, DW_AT_data_member_location[DW_OP_plus_uconst 0x30]
	.dwattr $C$DW$637, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$637, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$637, DW_AT_decl_line(0x185)
	.dwattr $C$DW$637, DW_AT_decl_column(0x0e)

$C$DW$638	.dwtag  DW_TAG_member
	.dwattr $C$DW$638, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$638, DW_AT_name("X_RSVD4")
	.dwattr $C$DW$638, DW_AT_bit_offset(0x10)
	.dwattr $C$DW$638, DW_AT_bit_size(0x01)
	.dwattr $C$DW$638, DW_AT_data_member_location[DW_OP_plus_uconst 0x30]
	.dwattr $C$DW$638, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$638, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$638, DW_AT_decl_line(0x187)
	.dwattr $C$DW$638, DW_AT_decl_column(0x0e)

$C$DW$639	.dwtag  DW_TAG_member
	.dwattr $C$DW$639, DW_AT_type(*$C$DW$T$437)
	.dwattr $C$DW$639, DW_AT_name("X_XTYPE")
	.dwattr $C$DW$639, DW_AT_bit_offset(0x04)
	.dwattr $C$DW$639, DW_AT_bit_size(0x04)
	.dwattr $C$DW$639, DW_AT_data_member_location[DW_OP_plus_uconst 0x36]
	.dwattr $C$DW$639, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$639, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$639, DW_AT_decl_line(0x188)
	.dwattr $C$DW$639, DW_AT_decl_column(0x1a)

$C$DW$640	.dwtag  DW_TAG_member
	.dwattr $C$DW$640, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$640, DW_AT_name("X_RSVD5")
	.dwattr $C$DW$640, DW_AT_bit_offset(0x08)
	.dwattr $C$DW$640, DW_AT_bit_size(0x04)
	.dwattr $C$DW$640, DW_AT_data_member_location[DW_OP_plus_uconst 0x30]
	.dwattr $C$DW$640, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$640, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$640, DW_AT_decl_line(0x18b)
	.dwattr $C$DW$640, DW_AT_decl_column(0x0e)

$C$DW$641	.dwtag  DW_TAG_member
	.dwattr $C$DW$641, DW_AT_type(*$C$DW$T$439)
	.dwattr $C$DW$641, DW_AT_name("X_CTYPE")
	.dwattr $C$DW$641, DW_AT_bit_offset(0x05)
	.dwattr $C$DW$641, DW_AT_bit_size(0x03)
	.dwattr $C$DW$641, DW_AT_data_member_location[DW_OP_plus_uconst 0x37]
	.dwattr $C$DW$641, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$641, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$641, DW_AT_decl_line(0x18c)
	.dwattr $C$DW$641, DW_AT_decl_column(0x1a)

$C$DW$642	.dwtag  DW_TAG_member
	.dwattr $C$DW$642, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$642, DW_AT_name("X_RSVD6")
	.dwattr $C$DW$642, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$642, DW_AT_bit_size(0x05)
	.dwattr $C$DW$642, DW_AT_data_member_location[DW_OP_plus_uconst 0x30]
	.dwattr $C$DW$642, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$642, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$642, DW_AT_decl_line(0x18e)
	.dwattr $C$DW$642, DW_AT_decl_column(0x0e)

$C$DW$643	.dwtag  DW_TAG_member
	.dwattr $C$DW$643, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$643, DW_AT_name("X_CSWPER")
	.dwattr $C$DW$643, DW_AT_bit_offset(0x20)
	.dwattr $C$DW$643, DW_AT_bit_size(0x20)
	.dwattr $C$DW$643, DW_AT_data_member_location[DW_OP_plus_uconst 0x38]
	.dwattr $C$DW$643, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$643, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$643, DW_AT_decl_line(0x190)
	.dwattr $C$DW$643, DW_AT_decl_column(0x0e)

$C$DW$644	.dwtag  DW_TAG_member
	.dwattr $C$DW$644, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$644, DW_AT_name("X_CRRSTPER")
	.dwattr $C$DW$644, DW_AT_bit_offset(0x18)
	.dwattr $C$DW$644, DW_AT_bit_size(0x08)
	.dwattr $C$DW$644, DW_AT_data_member_location[DW_OP_plus_uconst 0x38]
	.dwattr $C$DW$644, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$644, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$644, DW_AT_decl_line(0x191)
	.dwattr $C$DW$644, DW_AT_decl_column(0x0e)

$C$DW$645	.dwtag  DW_TAG_member
	.dwattr $C$DW$645, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$645, DW_AT_name("X_COFFSET")
	.dwattr $C$DW$645, DW_AT_bit_offset(0x10)
	.dwattr $C$DW$645, DW_AT_bit_size(0x08)
	.dwattr $C$DW$645, DW_AT_data_member_location[DW_OP_plus_uconst 0x38]
	.dwattr $C$DW$645, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$645, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$645, DW_AT_decl_line(0x192)
	.dwattr $C$DW$645, DW_AT_decl_column(0x0e)

$C$DW$646	.dwtag  DW_TAG_member
	.dwattr $C$DW$646, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$646, DW_AT_name("X_CSTART")
	.dwattr $C$DW$646, DW_AT_bit_offset(0x0f)
	.dwattr $C$DW$646, DW_AT_bit_size(0x01)
	.dwattr $C$DW$646, DW_AT_data_member_location[DW_OP_plus_uconst 0x38]
	.dwattr $C$DW$646, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$646, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$646, DW_AT_decl_line(0x193)
	.dwattr $C$DW$646, DW_AT_decl_column(0x0e)

$C$DW$647	.dwtag  DW_TAG_member
	.dwattr $C$DW$647, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$647, DW_AT_name("X_RSVD7")
	.dwattr $C$DW$647, DW_AT_bit_offset(0x08)
	.dwattr $C$DW$647, DW_AT_bit_size(0x07)
	.dwattr $C$DW$647, DW_AT_data_member_location[DW_OP_plus_uconst 0x38]
	.dwattr $C$DW$647, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$647, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$647, DW_AT_decl_line(0x194)
	.dwattr $C$DW$647, DW_AT_decl_column(0x0e)

$C$DW$648	.dwtag  DW_TAG_member
	.dwattr $C$DW$648, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$648, DW_AT_name("RSVD")
	.dwattr $C$DW$648, DW_AT_bit_offset(0x02)
	.dwattr $C$DW$648, DW_AT_bit_size(0x06)
	.dwattr $C$DW$648, DW_AT_data_member_location[DW_OP_plus_uconst 0x38]
	.dwattr $C$DW$648, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$648, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$648, DW_AT_decl_line(0x196)
	.dwattr $C$DW$648, DW_AT_decl_column(0x0e)

$C$DW$649	.dwtag  DW_TAG_member
	.dwattr $C$DW$649, DW_AT_type(*$C$DW$T$441)
	.dwattr $C$DW$649, DW_AT_name("PARITYCTRL")
	.dwattr $C$DW$649, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$649, DW_AT_bit_size(0x02)
	.dwattr $C$DW$649, DW_AT_data_member_location[DW_OP_plus_uconst 0x3f]
	.dwattr $C$DW$649, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$649, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$649, DW_AT_decl_line(0x197)
	.dwattr $C$DW$649, DW_AT_decl_column(0x16)

	.dwattr $C$DW$T$442, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$T$442, DW_AT_decl_line(0x137)
	.dwattr $C$DW$T$442, DW_AT_decl_column(0x2c)
	.dwendtag $C$DW$T$442

	.dwendtag $C$DW$TU$442


$C$DW$TU$474	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$474

$C$DW$T$474	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$474, DW_AT_name("__HWA_CONFIG_REG_v1")
	.dwattr $C$DW$T$474, DW_AT_byte_size(0x40)
$C$DW$650	.dwtag  DW_TAG_member
	.dwattr $C$DW$650, DW_AT_type(*$C$DW$T$444)
	.dwattr $C$DW$650, DW_AT_name("A_ATYPE")
	.dwattr $C$DW$650, DW_AT_bit_offset(0x05)
	.dwattr $C$DW$650, DW_AT_bit_size(0x03)
	.dwattr $C$DW$650, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$650, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$650, DW_AT_decl_line(0x13a)
	.dwattr $C$DW$650, DW_AT_decl_column(0x1a)

$C$DW$651	.dwtag  DW_TAG_member
	.dwattr $C$DW$651, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$651, DW_AT_name("A_RSVD1")
	.dwattr $C$DW$651, DW_AT_bit_offset(0x38)
	.dwattr $C$DW$651, DW_AT_bit_size(0x05)
	.dwattr $C$DW$651, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$651, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$651, DW_AT_decl_line(0x13b)
	.dwattr $C$DW$651, DW_AT_decl_column(0x0e)

$C$DW$652	.dwtag  DW_TAG_member
	.dwattr $C$DW$652, DW_AT_type(*$C$DW$T$446)
	.dwattr $C$DW$652, DW_AT_name("A_ALUTEN")
	.dwattr $C$DW$652, DW_AT_bit_offset(0x07)
	.dwattr $C$DW$652, DW_AT_bit_size(0x01)
	.dwattr $C$DW$652, DW_AT_data_member_location[DW_OP_plus_uconst 0x1]
	.dwattr $C$DW$652, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$652, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$652, DW_AT_decl_line(0x13c)
	.dwattr $C$DW$652, DW_AT_decl_column(0x13)

$C$DW$653	.dwtag  DW_TAG_member
	.dwattr $C$DW$653, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$653, DW_AT_name("A_RSVD2")
	.dwattr $C$DW$653, DW_AT_bit_offset(0x20)
	.dwattr $C$DW$653, DW_AT_bit_size(0x17)
	.dwattr $C$DW$653, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$653, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$653, DW_AT_decl_line(0x13d)
	.dwattr $C$DW$653, DW_AT_decl_column(0x0e)

$C$DW$654	.dwtag  DW_TAG_member
	.dwattr $C$DW$654, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$654, DW_AT_name("B_BSWPER")
	.dwattr $C$DW$654, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$654, DW_AT_bit_size(0x20)
	.dwattr $C$DW$654, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$654, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$654, DW_AT_decl_line(0x13f)
	.dwattr $C$DW$654, DW_AT_decl_column(0x0e)

$C$DW$655	.dwtag  DW_TAG_member
	.dwattr $C$DW$655, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$655, DW_AT_name("B_BRSTPER")
	.dwattr $C$DW$655, DW_AT_bit_offset(0x38)
	.dwattr $C$DW$655, DW_AT_bit_size(0x08)
	.dwattr $C$DW$655, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$655, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$655, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$655, DW_AT_decl_line(0x141)
	.dwattr $C$DW$655, DW_AT_decl_column(0x0e)

$C$DW$656	.dwtag  DW_TAG_member
	.dwattr $C$DW$656, DW_AT_type(*$C$DW$T$448)
	.dwattr $C$DW$656, DW_AT_name("B_BTYPE")
	.dwattr $C$DW$656, DW_AT_bit_offset(0x06)
	.dwattr $C$DW$656, DW_AT_bit_size(0x02)
	.dwattr $C$DW$656, DW_AT_data_member_location[DW_OP_plus_uconst 0x9]
	.dwattr $C$DW$656, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$656, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$656, DW_AT_decl_line(0x142)
	.dwattr $C$DW$656, DW_AT_decl_column(0x13)

$C$DW$657	.dwtag  DW_TAG_member
	.dwattr $C$DW$657, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$657, DW_AT_name("B_RSVD1")
	.dwattr $C$DW$657, DW_AT_bit_offset(0x30)
	.dwattr $C$DW$657, DW_AT_bit_size(0x06)
	.dwattr $C$DW$657, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$657, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$657, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$657, DW_AT_decl_line(0x143)
	.dwattr $C$DW$657, DW_AT_decl_column(0x0e)

$C$DW$658	.dwtag  DW_TAG_member
	.dwattr $C$DW$658, DW_AT_type(*$C$DW$T$450)
	.dwattr $C$DW$658, DW_AT_name("B_ORDER")
	.dwattr $C$DW$658, DW_AT_bit_offset(0x07)
	.dwattr $C$DW$658, DW_AT_bit_size(0x01)
	.dwattr $C$DW$658, DW_AT_data_member_location[DW_OP_plus_uconst 0xa]
	.dwattr $C$DW$658, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$658, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$658, DW_AT_decl_line(0x144)
	.dwattr $C$DW$658, DW_AT_decl_column(0x2c)

$C$DW$659	.dwtag  DW_TAG_member
	.dwattr $C$DW$659, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$659, DW_AT_name("B_RSVD2")
	.dwattr $C$DW$659, DW_AT_bit_offset(0x28)
	.dwattr $C$DW$659, DW_AT_bit_size(0x07)
	.dwattr $C$DW$659, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$659, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$659, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$659, DW_AT_decl_line(0x145)
	.dwattr $C$DW$659, DW_AT_decl_column(0x0e)

$C$DW$660	.dwtag  DW_TAG_member
	.dwattr $C$DW$660, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$660, DW_AT_name("B_BSTART")
	.dwattr $C$DW$660, DW_AT_bit_offset(0x27)
	.dwattr $C$DW$660, DW_AT_bit_size(0x01)
	.dwattr $C$DW$660, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$660, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$660, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$660, DW_AT_decl_line(0x146)
	.dwattr $C$DW$660, DW_AT_decl_column(0x0e)

$C$DW$661	.dwtag  DW_TAG_member
	.dwattr $C$DW$661, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$661, DW_AT_name("B_RSVD3")
	.dwattr $C$DW$661, DW_AT_bit_offset(0x20)
	.dwattr $C$DW$661, DW_AT_bit_size(0x07)
	.dwattr $C$DW$661, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$661, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$661, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$661, DW_AT_decl_line(0x148)
	.dwattr $C$DW$661, DW_AT_decl_column(0x0e)

$C$DW$662	.dwtag  DW_TAG_member
	.dwattr $C$DW$662, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$662, DW_AT_name("B_BOFFSET")
	.dwattr $C$DW$662, DW_AT_bit_offset(0x18)
	.dwattr $C$DW$662, DW_AT_bit_size(0x08)
	.dwattr $C$DW$662, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$662, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$662, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$662, DW_AT_decl_line(0x149)
	.dwattr $C$DW$662, DW_AT_decl_column(0x0e)

$C$DW$663	.dwtag  DW_TAG_member
	.dwattr $C$DW$663, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$663, DW_AT_name("B_RSVD4")
	.dwattr $C$DW$663, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$663, DW_AT_bit_size(0x18)
	.dwattr $C$DW$663, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$663, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$663, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$663, DW_AT_decl_line(0x14a)
	.dwattr $C$DW$663, DW_AT_decl_column(0x0e)

$C$DW$664	.dwtag  DW_TAG_member
	.dwattr $C$DW$664, DW_AT_type(*$C$DW$T$452)
	.dwattr $C$DW$664, DW_AT_name("C_ATYPE")
	.dwattr $C$DW$664, DW_AT_bit_offset(0x07)
	.dwattr $C$DW$664, DW_AT_bit_size(0x01)
	.dwattr $C$DW$664, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$664, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$664, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$664, DW_AT_decl_line(0x14d)
	.dwattr $C$DW$664, DW_AT_decl_column(0x1a)

$C$DW$665	.dwtag  DW_TAG_member
	.dwattr $C$DW$665, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$665, DW_AT_name("C_RSVD1")
	.dwattr $C$DW$665, DW_AT_bit_offset(0x38)
	.dwattr $C$DW$665, DW_AT_bit_size(0x07)
	.dwattr $C$DW$665, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$665, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$665, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$665, DW_AT_decl_line(0x14e)
	.dwattr $C$DW$665, DW_AT_decl_column(0x0e)

$C$DW$666	.dwtag  DW_TAG_member
	.dwattr $C$DW$666, DW_AT_type(*$C$DW$T$454)
	.dwattr $C$DW$666, DW_AT_name("C_BTYPE")
	.dwattr $C$DW$666, DW_AT_bit_offset(0x05)
	.dwattr $C$DW$666, DW_AT_bit_size(0x03)
	.dwattr $C$DW$666, DW_AT_data_member_location[DW_OP_plus_uconst 0x11]
	.dwattr $C$DW$666, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$666, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$666, DW_AT_decl_line(0x14f)
	.dwattr $C$DW$666, DW_AT_decl_column(0x1a)

$C$DW$667	.dwtag  DW_TAG_member
	.dwattr $C$DW$667, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$667, DW_AT_name("C_RSVD2")
	.dwattr $C$DW$667, DW_AT_bit_offset(0x30)
	.dwattr $C$DW$667, DW_AT_bit_size(0x05)
	.dwattr $C$DW$667, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$667, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$667, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$667, DW_AT_decl_line(0x152)
	.dwattr $C$DW$667, DW_AT_decl_column(0x0e)

$C$DW$668	.dwtag  DW_TAG_member
	.dwattr $C$DW$668, DW_AT_type(*$C$DW$T$456)
	.dwattr $C$DW$668, DW_AT_name("C_OPERATION0")
	.dwattr $C$DW$668, DW_AT_bit_offset(0x06)
	.dwattr $C$DW$668, DW_AT_bit_size(0x02)
	.dwattr $C$DW$668, DW_AT_data_member_location[DW_OP_plus_uconst 0x12]
	.dwattr $C$DW$668, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$668, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$668, DW_AT_decl_line(0x153)
	.dwattr $C$DW$668, DW_AT_decl_column(0x1e)

$C$DW$669	.dwtag  DW_TAG_member
	.dwattr $C$DW$669, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$669, DW_AT_name("C_RSVD3")
	.dwattr $C$DW$669, DW_AT_bit_offset(0x28)
	.dwattr $C$DW$669, DW_AT_bit_size(0x06)
	.dwattr $C$DW$669, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$669, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$669, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$669, DW_AT_decl_line(0x154)
	.dwattr $C$DW$669, DW_AT_decl_column(0x0e)

$C$DW$670	.dwtag  DW_TAG_member
	.dwattr $C$DW$670, DW_AT_type(*$C$DW$T$456)
	.dwattr $C$DW$670, DW_AT_name("C_OPERATION1")
	.dwattr $C$DW$670, DW_AT_bit_offset(0x06)
	.dwattr $C$DW$670, DW_AT_bit_size(0x02)
	.dwattr $C$DW$670, DW_AT_data_member_location[DW_OP_plus_uconst 0x13]
	.dwattr $C$DW$670, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$670, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$670, DW_AT_decl_line(0x155)
	.dwattr $C$DW$670, DW_AT_decl_column(0x1e)

$C$DW$671	.dwtag  DW_TAG_member
	.dwattr $C$DW$671, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$671, DW_AT_name("C_RSVD4")
	.dwattr $C$DW$671, DW_AT_bit_offset(0x20)
	.dwattr $C$DW$671, DW_AT_bit_size(0x06)
	.dwattr $C$DW$671, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$671, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$671, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$671, DW_AT_decl_line(0x156)
	.dwattr $C$DW$671, DW_AT_decl_column(0x0e)

$C$DW$672	.dwtag  DW_TAG_member
	.dwattr $C$DW$672, DW_AT_type(*$C$DW$T$458)
	.dwattr $C$DW$672, DW_AT_name("C_HWLDDST")
	.dwattr $C$DW$672, DW_AT_bit_offset(0x05)
	.dwattr $C$DW$672, DW_AT_bit_size(0x03)
	.dwattr $C$DW$672, DW_AT_data_member_location[DW_OP_plus_uconst 0x14]
	.dwattr $C$DW$672, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$672, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$672, DW_AT_decl_line(0x157)
	.dwattr $C$DW$672, DW_AT_decl_column(0x1c)

$C$DW$673	.dwtag  DW_TAG_member
	.dwattr $C$DW$673, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$673, DW_AT_name("C_RSVD5")
	.dwattr $C$DW$673, DW_AT_bit_offset(0x18)
	.dwattr $C$DW$673, DW_AT_bit_size(0x05)
	.dwattr $C$DW$673, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$673, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$673, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$673, DW_AT_decl_line(0x158)
	.dwattr $C$DW$673, DW_AT_decl_column(0x0e)

$C$DW$674	.dwtag  DW_TAG_member
	.dwattr $C$DW$674, DW_AT_type(*$C$DW$T$460)
	.dwattr $C$DW$674, DW_AT_name("C_HWLDTYPE")
	.dwattr $C$DW$674, DW_AT_bit_offset(0x04)
	.dwattr $C$DW$674, DW_AT_bit_size(0x04)
	.dwattr $C$DW$674, DW_AT_data_member_location[DW_OP_plus_uconst 0x15]
	.dwattr $C$DW$674, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$674, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$674, DW_AT_decl_line(0x159)
	.dwattr $C$DW$674, DW_AT_decl_column(0x1d)

$C$DW$675	.dwtag  DW_TAG_member
	.dwattr $C$DW$675, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$675, DW_AT_name("C_RSVD6")
	.dwattr $C$DW$675, DW_AT_bit_offset(0x10)
	.dwattr $C$DW$675, DW_AT_bit_size(0x04)
	.dwattr $C$DW$675, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$675, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$675, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$675, DW_AT_decl_line(0x15b)
	.dwattr $C$DW$675, DW_AT_decl_column(0x0e)

$C$DW$676	.dwtag  DW_TAG_member
	.dwattr $C$DW$676, DW_AT_type(*$C$DW$T$462)
	.dwattr $C$DW$676, DW_AT_name("C_OPSTART")
	.dwattr $C$DW$676, DW_AT_bit_offset(0x07)
	.dwattr $C$DW$676, DW_AT_bit_size(0x01)
	.dwattr $C$DW$676, DW_AT_data_member_location[DW_OP_plus_uconst 0x16]
	.dwattr $C$DW$676, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$676, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$676, DW_AT_decl_line(0x15c)
	.dwattr $C$DW$676, DW_AT_decl_column(0x1c)

$C$DW$677	.dwtag  DW_TAG_member
	.dwattr $C$DW$677, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$677, DW_AT_name("C_BSTART")
	.dwattr $C$DW$677, DW_AT_bit_offset(0x0e)
	.dwattr $C$DW$677, DW_AT_bit_size(0x01)
	.dwattr $C$DW$677, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$677, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$677, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$677, DW_AT_decl_line(0x15d)
	.dwattr $C$DW$677, DW_AT_decl_column(0x0e)

$C$DW$678	.dwtag  DW_TAG_member
	.dwattr $C$DW$678, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$678, DW_AT_name("C_CRSTART")
	.dwattr $C$DW$678, DW_AT_bit_offset(0x0d)
	.dwattr $C$DW$678, DW_AT_bit_size(0x01)
	.dwattr $C$DW$678, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$678, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$678, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$678, DW_AT_decl_line(0x160)
	.dwattr $C$DW$678, DW_AT_decl_column(0x0e)

$C$DW$679	.dwtag  DW_TAG_member
	.dwattr $C$DW$679, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$679, DW_AT_name("C_CWSTART")
	.dwattr $C$DW$679, DW_AT_bit_offset(0x0c)
	.dwattr $C$DW$679, DW_AT_bit_size(0x01)
	.dwattr $C$DW$679, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$679, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$679, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$679, DW_AT_decl_line(0x161)
	.dwattr $C$DW$679, DW_AT_decl_column(0x0e)

$C$DW$680	.dwtag  DW_TAG_member
	.dwattr $C$DW$680, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$680, DW_AT_name("C_CLSTART")
	.dwattr $C$DW$680, DW_AT_bit_offset(0x0b)
	.dwattr $C$DW$680, DW_AT_bit_size(0x01)
	.dwattr $C$DW$680, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$680, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$680, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$680, DW_AT_decl_line(0x163)
	.dwattr $C$DW$680, DW_AT_decl_column(0x0e)

$C$DW$681	.dwtag  DW_TAG_member
	.dwattr $C$DW$681, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$681, DW_AT_name("C_RSVD7")
	.dwattr $C$DW$681, DW_AT_bit_offset(0x08)
	.dwattr $C$DW$681, DW_AT_bit_size(0x03)
	.dwattr $C$DW$681, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$681, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$681, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$681, DW_AT_decl_line(0x165)
	.dwattr $C$DW$681, DW_AT_decl_column(0x0e)

$C$DW$682	.dwtag  DW_TAG_member
	.dwattr $C$DW$682, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$682, DW_AT_name("C_CROFFSET")
	.dwattr $C$DW$682, DW_AT_bit_offset(0x02)
	.dwattr $C$DW$682, DW_AT_bit_size(0x06)
	.dwattr $C$DW$682, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$682, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$682, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$682, DW_AT_decl_line(0x166)
	.dwattr $C$DW$682, DW_AT_decl_column(0x0e)

$C$DW$683	.dwtag  DW_TAG_member
	.dwattr $C$DW$683, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$683, DW_AT_name("C_RSVD8")
	.dwattr $C$DW$683, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$683, DW_AT_bit_size(0x02)
	.dwattr $C$DW$683, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$683, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$683, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$683, DW_AT_decl_line(0x167)
	.dwattr $C$DW$683, DW_AT_decl_column(0x0e)

$C$DW$684	.dwtag  DW_TAG_member
	.dwattr $C$DW$684, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$684, DW_AT_name("C_CWOFFSET")
	.dwattr $C$DW$684, DW_AT_bit_offset(0x3a)
	.dwattr $C$DW$684, DW_AT_bit_size(0x06)
	.dwattr $C$DW$684, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$684, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$684, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$684, DW_AT_decl_line(0x169)
	.dwattr $C$DW$684, DW_AT_decl_column(0x0e)

$C$DW$685	.dwtag  DW_TAG_member
	.dwattr $C$DW$685, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$685, DW_AT_name("C_RSVD9")
	.dwattr $C$DW$685, DW_AT_bit_offset(0x38)
	.dwattr $C$DW$685, DW_AT_bit_size(0x02)
	.dwattr $C$DW$685, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$685, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$685, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$685, DW_AT_decl_line(0x16a)
	.dwattr $C$DW$685, DW_AT_decl_column(0x0e)

$C$DW$686	.dwtag  DW_TAG_member
	.dwattr $C$DW$686, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$686, DW_AT_name("C_CLOFFSET")
	.dwattr $C$DW$686, DW_AT_bit_offset(0x32)
	.dwattr $C$DW$686, DW_AT_bit_size(0x06)
	.dwattr $C$DW$686, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$686, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$686, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$686, DW_AT_decl_line(0x16b)
	.dwattr $C$DW$686, DW_AT_decl_column(0x0e)

$C$DW$687	.dwtag  DW_TAG_member
	.dwattr $C$DW$687, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$687, DW_AT_name("C_RSVD10")
	.dwattr $C$DW$687, DW_AT_bit_offset(0x30)
	.dwattr $C$DW$687, DW_AT_bit_size(0x02)
	.dwattr $C$DW$687, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$687, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$687, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$687, DW_AT_decl_line(0x16c)
	.dwattr $C$DW$687, DW_AT_decl_column(0x0e)

$C$DW$688	.dwtag  DW_TAG_member
	.dwattr $C$DW$688, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$688, DW_AT_name("C_CLSWPER")
	.dwattr $C$DW$688, DW_AT_bit_offset(0x28)
	.dwattr $C$DW$688, DW_AT_bit_size(0x08)
	.dwattr $C$DW$688, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$688, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$688, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$688, DW_AT_decl_line(0x16d)
	.dwattr $C$DW$688, DW_AT_decl_column(0x0e)

$C$DW$689	.dwtag  DW_TAG_member
	.dwattr $C$DW$689, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$689, DW_AT_name("C_CLRSTPER")
	.dwattr $C$DW$689, DW_AT_bit_offset(0x20)
	.dwattr $C$DW$689, DW_AT_bit_size(0x08)
	.dwattr $C$DW$689, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$689, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$689, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$689, DW_AT_decl_line(0x16f)
	.dwattr $C$DW$689, DW_AT_decl_column(0x0e)

$C$DW$690	.dwtag  DW_TAG_member
	.dwattr $C$DW$690, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$690, DW_AT_name("C_OP1PER")
	.dwattr $C$DW$690, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$690, DW_AT_bit_size(0x20)
	.dwattr $C$DW$690, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$690, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$690, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$690, DW_AT_decl_line(0x170)
	.dwattr $C$DW$690, DW_AT_decl_column(0x0e)

$C$DW$691	.dwtag  DW_TAG_member
	.dwattr $C$DW$691, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$691, DW_AT_name("C_OP0PER")
	.dwattr $C$DW$691, DW_AT_bit_offset(0x20)
	.dwattr $C$DW$691, DW_AT_bit_size(0x20)
	.dwattr $C$DW$691, DW_AT_data_member_location[DW_OP_plus_uconst 0x20]
	.dwattr $C$DW$691, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$691, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$691, DW_AT_decl_line(0x172)
	.dwattr $C$DW$691, DW_AT_decl_column(0x0e)

$C$DW$692	.dwtag  DW_TAG_member
	.dwattr $C$DW$692, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$692, DW_AT_name("C_BSWPER")
	.dwattr $C$DW$692, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$692, DW_AT_bit_size(0x20)
	.dwattr $C$DW$692, DW_AT_data_member_location[DW_OP_plus_uconst 0x20]
	.dwattr $C$DW$692, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$692, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$692, DW_AT_decl_line(0x173)
	.dwattr $C$DW$692, DW_AT_decl_column(0x0e)

$C$DW$693	.dwtag  DW_TAG_member
	.dwattr $C$DW$693, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$693, DW_AT_name("C_CRSWPER")
	.dwattr $C$DW$693, DW_AT_bit_offset(0x20)
	.dwattr $C$DW$693, DW_AT_bit_size(0x20)
	.dwattr $C$DW$693, DW_AT_data_member_location[DW_OP_plus_uconst 0x28]
	.dwattr $C$DW$693, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$693, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$693, DW_AT_decl_line(0x175)
	.dwattr $C$DW$693, DW_AT_decl_column(0x0e)

$C$DW$694	.dwtag  DW_TAG_member
	.dwattr $C$DW$694, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$694, DW_AT_name("C_CWSWPER")
	.dwattr $C$DW$694, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$694, DW_AT_bit_size(0x20)
	.dwattr $C$DW$694, DW_AT_data_member_location[DW_OP_plus_uconst 0x28]
	.dwattr $C$DW$694, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$694, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$694, DW_AT_decl_line(0x176)
	.dwattr $C$DW$694, DW_AT_decl_column(0x0e)

$C$DW$695	.dwtag  DW_TAG_member
	.dwattr $C$DW$695, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$695, DW_AT_name("C_CRRSTPER")
	.dwattr $C$DW$695, DW_AT_bit_offset(0x38)
	.dwattr $C$DW$695, DW_AT_bit_size(0x08)
	.dwattr $C$DW$695, DW_AT_data_member_location[DW_OP_plus_uconst 0x30]
	.dwattr $C$DW$695, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$695, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$695, DW_AT_decl_line(0x178)
	.dwattr $C$DW$695, DW_AT_decl_column(0x0e)

$C$DW$696	.dwtag  DW_TAG_member
	.dwattr $C$DW$696, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$696, DW_AT_name("C_CWRSTPER")
	.dwattr $C$DW$696, DW_AT_bit_offset(0x30)
	.dwattr $C$DW$696, DW_AT_bit_size(0x08)
	.dwattr $C$DW$696, DW_AT_data_member_location[DW_OP_plus_uconst 0x30]
	.dwattr $C$DW$696, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$696, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$696, DW_AT_decl_line(0x179)
	.dwattr $C$DW$696, DW_AT_decl_column(0x0e)

$C$DW$697	.dwtag  DW_TAG_member
	.dwattr $C$DW$697, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$697, DW_AT_name("X_ReLU")
	.dwattr $C$DW$697, DW_AT_bit_offset(0x2f)
	.dwattr $C$DW$697, DW_AT_bit_size(0x01)
	.dwattr $C$DW$697, DW_AT_data_member_location[DW_OP_plus_uconst 0x30]
	.dwattr $C$DW$697, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$697, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$697, DW_AT_decl_line(0x17c)
	.dwattr $C$DW$697, DW_AT_decl_column(0x0e)

$C$DW$698	.dwtag  DW_TAG_member
	.dwattr $C$DW$698, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$698, DW_AT_name("X_RSVD1")
	.dwattr $C$DW$698, DW_AT_bit_offset(0x28)
	.dwattr $C$DW$698, DW_AT_bit_size(0x07)
	.dwattr $C$DW$698, DW_AT_data_member_location[DW_OP_plus_uconst 0x30]
	.dwattr $C$DW$698, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$698, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$698, DW_AT_decl_line(0x17e)
	.dwattr $C$DW$698, DW_AT_decl_column(0x0e)

$C$DW$699	.dwtag  DW_TAG_member
	.dwattr $C$DW$699, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$699, DW_AT_name("X_SAT")
	.dwattr $C$DW$699, DW_AT_bit_offset(0x27)
	.dwattr $C$DW$699, DW_AT_bit_size(0x01)
	.dwattr $C$DW$699, DW_AT_data_member_location[DW_OP_plus_uconst 0x30]
	.dwattr $C$DW$699, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$699, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$699, DW_AT_decl_line(0x17f)
	.dwattr $C$DW$699, DW_AT_decl_column(0x0e)

$C$DW$700	.dwtag  DW_TAG_member
	.dwattr $C$DW$700, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$700, DW_AT_name("X_RSVD2")
	.dwattr $C$DW$700, DW_AT_bit_offset(0x20)
	.dwattr $C$DW$700, DW_AT_bit_size(0x07)
	.dwattr $C$DW$700, DW_AT_data_member_location[DW_OP_plus_uconst 0x30]
	.dwattr $C$DW$700, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$700, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$700, DW_AT_decl_line(0x181)
	.dwattr $C$DW$700, DW_AT_decl_column(0x0e)

$C$DW$701	.dwtag  DW_TAG_member
	.dwattr $C$DW$701, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$701, DW_AT_name("X_RE")
	.dwattr $C$DW$701, DW_AT_bit_offset(0x1f)
	.dwattr $C$DW$701, DW_AT_bit_size(0x01)
	.dwattr $C$DW$701, DW_AT_data_member_location[DW_OP_plus_uconst 0x30]
	.dwattr $C$DW$701, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$701, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$701, DW_AT_decl_line(0x182)
	.dwattr $C$DW$701, DW_AT_decl_column(0x0e)

$C$DW$702	.dwtag  DW_TAG_member
	.dwattr $C$DW$702, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$702, DW_AT_name("X_RSVD3")
	.dwattr $C$DW$702, DW_AT_bit_offset(0x18)
	.dwattr $C$DW$702, DW_AT_bit_size(0x07)
	.dwattr $C$DW$702, DW_AT_data_member_location[DW_OP_plus_uconst 0x30]
	.dwattr $C$DW$702, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$702, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$702, DW_AT_decl_line(0x184)
	.dwattr $C$DW$702, DW_AT_decl_column(0x0e)

$C$DW$703	.dwtag  DW_TAG_member
	.dwattr $C$DW$703, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$703, DW_AT_name("X_SHIFT")
	.dwattr $C$DW$703, DW_AT_bit_offset(0x11)
	.dwattr $C$DW$703, DW_AT_bit_size(0x07)
	.dwattr $C$DW$703, DW_AT_data_member_location[DW_OP_plus_uconst 0x30]
	.dwattr $C$DW$703, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$703, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$703, DW_AT_decl_line(0x185)
	.dwattr $C$DW$703, DW_AT_decl_column(0x0e)

$C$DW$704	.dwtag  DW_TAG_member
	.dwattr $C$DW$704, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$704, DW_AT_name("X_RSVD4")
	.dwattr $C$DW$704, DW_AT_bit_offset(0x10)
	.dwattr $C$DW$704, DW_AT_bit_size(0x01)
	.dwattr $C$DW$704, DW_AT_data_member_location[DW_OP_plus_uconst 0x30]
	.dwattr $C$DW$704, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$704, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$704, DW_AT_decl_line(0x187)
	.dwattr $C$DW$704, DW_AT_decl_column(0x0e)

$C$DW$705	.dwtag  DW_TAG_member
	.dwattr $C$DW$705, DW_AT_type(*$C$DW$T$464)
	.dwattr $C$DW$705, DW_AT_name("X_XTYPE")
	.dwattr $C$DW$705, DW_AT_bit_offset(0x04)
	.dwattr $C$DW$705, DW_AT_bit_size(0x04)
	.dwattr $C$DW$705, DW_AT_data_member_location[DW_OP_plus_uconst 0x36]
	.dwattr $C$DW$705, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$705, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$705, DW_AT_decl_line(0x188)
	.dwattr $C$DW$705, DW_AT_decl_column(0x1a)

$C$DW$706	.dwtag  DW_TAG_member
	.dwattr $C$DW$706, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$706, DW_AT_name("X_RSVD5")
	.dwattr $C$DW$706, DW_AT_bit_offset(0x08)
	.dwattr $C$DW$706, DW_AT_bit_size(0x04)
	.dwattr $C$DW$706, DW_AT_data_member_location[DW_OP_plus_uconst 0x30]
	.dwattr $C$DW$706, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$706, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$706, DW_AT_decl_line(0x18b)
	.dwattr $C$DW$706, DW_AT_decl_column(0x0e)

$C$DW$707	.dwtag  DW_TAG_member
	.dwattr $C$DW$707, DW_AT_type(*$C$DW$T$466)
	.dwattr $C$DW$707, DW_AT_name("X_CTYPE")
	.dwattr $C$DW$707, DW_AT_bit_offset(0x05)
	.dwattr $C$DW$707, DW_AT_bit_size(0x03)
	.dwattr $C$DW$707, DW_AT_data_member_location[DW_OP_plus_uconst 0x37]
	.dwattr $C$DW$707, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$707, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$707, DW_AT_decl_line(0x18c)
	.dwattr $C$DW$707, DW_AT_decl_column(0x1a)

$C$DW$708	.dwtag  DW_TAG_member
	.dwattr $C$DW$708, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$708, DW_AT_name("X_RSVD6")
	.dwattr $C$DW$708, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$708, DW_AT_bit_size(0x05)
	.dwattr $C$DW$708, DW_AT_data_member_location[DW_OP_plus_uconst 0x30]
	.dwattr $C$DW$708, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$708, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$708, DW_AT_decl_line(0x18e)
	.dwattr $C$DW$708, DW_AT_decl_column(0x0e)

$C$DW$709	.dwtag  DW_TAG_member
	.dwattr $C$DW$709, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$709, DW_AT_name("X_CSWPER")
	.dwattr $C$DW$709, DW_AT_bit_offset(0x20)
	.dwattr $C$DW$709, DW_AT_bit_size(0x20)
	.dwattr $C$DW$709, DW_AT_data_member_location[DW_OP_plus_uconst 0x38]
	.dwattr $C$DW$709, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$709, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$709, DW_AT_decl_line(0x190)
	.dwattr $C$DW$709, DW_AT_decl_column(0x0e)

$C$DW$710	.dwtag  DW_TAG_member
	.dwattr $C$DW$710, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$710, DW_AT_name("X_CRRSTPER")
	.dwattr $C$DW$710, DW_AT_bit_offset(0x18)
	.dwattr $C$DW$710, DW_AT_bit_size(0x08)
	.dwattr $C$DW$710, DW_AT_data_member_location[DW_OP_plus_uconst 0x38]
	.dwattr $C$DW$710, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$710, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$710, DW_AT_decl_line(0x191)
	.dwattr $C$DW$710, DW_AT_decl_column(0x0e)

$C$DW$711	.dwtag  DW_TAG_member
	.dwattr $C$DW$711, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$711, DW_AT_name("X_COFFSET")
	.dwattr $C$DW$711, DW_AT_bit_offset(0x10)
	.dwattr $C$DW$711, DW_AT_bit_size(0x08)
	.dwattr $C$DW$711, DW_AT_data_member_location[DW_OP_plus_uconst 0x38]
	.dwattr $C$DW$711, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$711, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$711, DW_AT_decl_line(0x192)
	.dwattr $C$DW$711, DW_AT_decl_column(0x0e)

$C$DW$712	.dwtag  DW_TAG_member
	.dwattr $C$DW$712, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$712, DW_AT_name("X_CSTART")
	.dwattr $C$DW$712, DW_AT_bit_offset(0x0f)
	.dwattr $C$DW$712, DW_AT_bit_size(0x01)
	.dwattr $C$DW$712, DW_AT_data_member_location[DW_OP_plus_uconst 0x38]
	.dwattr $C$DW$712, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$712, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$712, DW_AT_decl_line(0x193)
	.dwattr $C$DW$712, DW_AT_decl_column(0x0e)

$C$DW$713	.dwtag  DW_TAG_member
	.dwattr $C$DW$713, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$713, DW_AT_name("X_RSVD7")
	.dwattr $C$DW$713, DW_AT_bit_offset(0x08)
	.dwattr $C$DW$713, DW_AT_bit_size(0x07)
	.dwattr $C$DW$713, DW_AT_data_member_location[DW_OP_plus_uconst 0x38]
	.dwattr $C$DW$713, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$713, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$713, DW_AT_decl_line(0x194)
	.dwattr $C$DW$713, DW_AT_decl_column(0x0e)

$C$DW$714	.dwtag  DW_TAG_member
	.dwattr $C$DW$714, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$714, DW_AT_name("RSVD")
	.dwattr $C$DW$714, DW_AT_bit_offset(0x02)
	.dwattr $C$DW$714, DW_AT_bit_size(0x06)
	.dwattr $C$DW$714, DW_AT_data_member_location[DW_OP_plus_uconst 0x38]
	.dwattr $C$DW$714, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$714, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$714, DW_AT_decl_line(0x196)
	.dwattr $C$DW$714, DW_AT_decl_column(0x0e)

$C$DW$715	.dwtag  DW_TAG_member
	.dwattr $C$DW$715, DW_AT_type(*$C$DW$T$468)
	.dwattr $C$DW$715, DW_AT_name("PARITYCTRL")
	.dwattr $C$DW$715, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$715, DW_AT_bit_size(0x02)
	.dwattr $C$DW$715, DW_AT_data_member_location[DW_OP_plus_uconst 0x3f]
	.dwattr $C$DW$715, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$715, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$715, DW_AT_decl_line(0x197)
	.dwattr $C$DW$715, DW_AT_decl_column(0x16)


$C$DW$716	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$716, DW_AT_name("operator =")
	.dwattr $C$DW$716, DW_AT_declaration
	.dwattr $C$DW$716, DW_AT_linkage_name("_ZN19__HWA_CONFIG_REG_v1aSERKS_")
	.dwattr $C$DW$716, DW_AT_type(*$C$DW$T$469)
	.dwattr $C$DW$716, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$717	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$717, DW_AT_type(*$C$DW$T$471)

	.dwendtag $C$DW$716


$C$DW$718	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$718, DW_AT_name("operator =")
	.dwattr $C$DW$718, DW_AT_declaration
	.dwattr $C$DW$718, DW_AT_linkage_name("_ZN19__HWA_CONFIG_REG_v1aSEOS_")
	.dwattr $C$DW$718, DW_AT_type(*$C$DW$T$469)
	.dwattr $C$DW$718, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$719	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$719, DW_AT_type(*$C$DW$T$469)

	.dwendtag $C$DW$718

	.dwattr $C$DW$T$474, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$T$474, DW_AT_decl_line(0x137)
	.dwattr $C$DW$T$474, DW_AT_decl_column(0x2c)
	.dwendtag $C$DW$T$474

	.dwendtag $C$DW$TU$474


$C$DW$TU$470	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$470
$C$DW$T$470	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$470, DW_AT_type(*$C$DW$T$474)

	.dwendtag $C$DW$TU$470


$C$DW$TU$471	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$471
$C$DW$T$471	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$471, DW_AT_type(*$C$DW$T$470)
	.dwattr $C$DW$T$471, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$471


$C$DW$TU$571	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$571
$C$DW$T$571	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$571, DW_AT_name("__HWA_CONFIG_REG_v1")
	.dwattr $C$DW$T$571, DW_AT_type(*$C$DW$T$474)
	.dwattr $C$DW$T$571, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$T$571, DW_AT_decl_line(0x1fc)
	.dwattr $C$DW$T$571, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$571


$C$DW$TU$469	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$469
$C$DW$T$469	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$469, DW_AT_type(*$C$DW$T$474)
	.dwattr $C$DW$T$469, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$469


$C$DW$TU$472	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$472

$C$DW$T$472	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$472, DW_AT_type(*$C$DW$T$469)
$C$DW$720	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$720, DW_AT_type(*$C$DW$T$471)

	.dwendtag $C$DW$T$472

	.dwendtag $C$DW$TU$472


$C$DW$TU$473	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$473

$C$DW$T$473	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$473, DW_AT_type(*$C$DW$T$469)
$C$DW$721	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$721, DW_AT_type(*$C$DW$T$469)

	.dwendtag $C$DW$T$473

	.dwendtag $C$DW$TU$473


$C$DW$TU$475	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$475

$C$DW$T$475	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$475, DW_AT_name("__HWA_OFFSET_REG_t")
	.dwattr $C$DW$T$475, DW_AT_byte_size(0x40)
$C$DW$722	.dwtag  DW_TAG_member
	.dwattr $C$DW$722, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$722, DW_AT_name("offset0")
	.dwattr $C$DW$722, DW_AT_bit_offset(0x3a)
	.dwattr $C$DW$722, DW_AT_bit_size(0x06)
	.dwattr $C$DW$722, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$722, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$722, DW_AT_decl_line(0x204)
	.dwattr $C$DW$722, DW_AT_decl_column(0x0e)

$C$DW$723	.dwtag  DW_TAG_member
	.dwattr $C$DW$723, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$723, DW_AT_name("offset1")
	.dwattr $C$DW$723, DW_AT_bit_offset(0x34)
	.dwattr $C$DW$723, DW_AT_bit_size(0x06)
	.dwattr $C$DW$723, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$723, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$723, DW_AT_decl_line(0x205)
	.dwattr $C$DW$723, DW_AT_decl_column(0x0e)

$C$DW$724	.dwtag  DW_TAG_member
	.dwattr $C$DW$724, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$724, DW_AT_name("offset2")
	.dwattr $C$DW$724, DW_AT_bit_offset(0x2e)
	.dwattr $C$DW$724, DW_AT_bit_size(0x06)
	.dwattr $C$DW$724, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$724, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$724, DW_AT_decl_line(0x206)
	.dwattr $C$DW$724, DW_AT_decl_column(0x0e)

$C$DW$725	.dwtag  DW_TAG_member
	.dwattr $C$DW$725, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$725, DW_AT_name("offset3")
	.dwattr $C$DW$725, DW_AT_bit_offset(0x28)
	.dwattr $C$DW$725, DW_AT_bit_size(0x06)
	.dwattr $C$DW$725, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$725, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$725, DW_AT_decl_line(0x207)
	.dwattr $C$DW$725, DW_AT_decl_column(0x0e)

$C$DW$726	.dwtag  DW_TAG_member
	.dwattr $C$DW$726, DW_AT_type(*$C$DW$T$318)
	.dwattr $C$DW$726, DW_AT_name("A_LUT_VAL_0")
	.dwattr $C$DW$726, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$726, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$726, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$726, DW_AT_decl_line(0x208)
	.dwattr $C$DW$726, DW_AT_decl_column(0x0d)

$C$DW$727	.dwtag  DW_TAG_member
	.dwattr $C$DW$727, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$727, DW_AT_name("offset4")
	.dwattr $C$DW$727, DW_AT_bit_offset(0x1a)
	.dwattr $C$DW$727, DW_AT_bit_size(0x06)
	.dwattr $C$DW$727, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$727, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$727, DW_AT_decl_line(0x209)
	.dwattr $C$DW$727, DW_AT_decl_column(0x0e)

$C$DW$728	.dwtag  DW_TAG_member
	.dwattr $C$DW$728, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$728, DW_AT_name("offset5")
	.dwattr $C$DW$728, DW_AT_bit_offset(0x14)
	.dwattr $C$DW$728, DW_AT_bit_size(0x06)
	.dwattr $C$DW$728, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$728, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$728, DW_AT_decl_line(0x20a)
	.dwattr $C$DW$728, DW_AT_decl_column(0x0e)

$C$DW$729	.dwtag  DW_TAG_member
	.dwattr $C$DW$729, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$729, DW_AT_name("offset6")
	.dwattr $C$DW$729, DW_AT_bit_offset(0x0e)
	.dwattr $C$DW$729, DW_AT_bit_size(0x06)
	.dwattr $C$DW$729, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$729, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$729, DW_AT_decl_line(0x20b)
	.dwattr $C$DW$729, DW_AT_decl_column(0x0e)

$C$DW$730	.dwtag  DW_TAG_member
	.dwattr $C$DW$730, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$730, DW_AT_name("offset7")
	.dwattr $C$DW$730, DW_AT_bit_offset(0x08)
	.dwattr $C$DW$730, DW_AT_bit_size(0x06)
	.dwattr $C$DW$730, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$730, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$730, DW_AT_decl_line(0x20c)
	.dwattr $C$DW$730, DW_AT_decl_column(0x0e)

$C$DW$731	.dwtag  DW_TAG_member
	.dwattr $C$DW$731, DW_AT_type(*$C$DW$T$318)
	.dwattr $C$DW$731, DW_AT_name("A_LUT_VAL_1")
	.dwattr $C$DW$731, DW_AT_data_member_location[DW_OP_plus_uconst 0x7]
	.dwattr $C$DW$731, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$731, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$731, DW_AT_decl_line(0x20d)
	.dwattr $C$DW$731, DW_AT_decl_column(0x0d)

$C$DW$732	.dwtag  DW_TAG_member
	.dwattr $C$DW$732, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$732, DW_AT_name("offset8")
	.dwattr $C$DW$732, DW_AT_bit_offset(0x3a)
	.dwattr $C$DW$732, DW_AT_bit_size(0x06)
	.dwattr $C$DW$732, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$732, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$732, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$732, DW_AT_decl_line(0x20f)
	.dwattr $C$DW$732, DW_AT_decl_column(0x0e)

$C$DW$733	.dwtag  DW_TAG_member
	.dwattr $C$DW$733, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$733, DW_AT_name("offset9")
	.dwattr $C$DW$733, DW_AT_bit_offset(0x34)
	.dwattr $C$DW$733, DW_AT_bit_size(0x06)
	.dwattr $C$DW$733, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$733, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$733, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$733, DW_AT_decl_line(0x210)
	.dwattr $C$DW$733, DW_AT_decl_column(0x0e)

$C$DW$734	.dwtag  DW_TAG_member
	.dwattr $C$DW$734, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$734, DW_AT_name("offset10")
	.dwattr $C$DW$734, DW_AT_bit_offset(0x2e)
	.dwattr $C$DW$734, DW_AT_bit_size(0x06)
	.dwattr $C$DW$734, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$734, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$734, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$734, DW_AT_decl_line(0x211)
	.dwattr $C$DW$734, DW_AT_decl_column(0x0e)

$C$DW$735	.dwtag  DW_TAG_member
	.dwattr $C$DW$735, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$735, DW_AT_name("offset11")
	.dwattr $C$DW$735, DW_AT_bit_offset(0x28)
	.dwattr $C$DW$735, DW_AT_bit_size(0x06)
	.dwattr $C$DW$735, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$735, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$735, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$735, DW_AT_decl_line(0x212)
	.dwattr $C$DW$735, DW_AT_decl_column(0x0e)

$C$DW$736	.dwtag  DW_TAG_member
	.dwattr $C$DW$736, DW_AT_type(*$C$DW$T$318)
	.dwattr $C$DW$736, DW_AT_name("A_LUT_VAL_2")
	.dwattr $C$DW$736, DW_AT_data_member_location[DW_OP_plus_uconst 0xb]
	.dwattr $C$DW$736, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$736, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$736, DW_AT_decl_line(0x213)
	.dwattr $C$DW$736, DW_AT_decl_column(0x0d)

$C$DW$737	.dwtag  DW_TAG_member
	.dwattr $C$DW$737, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$737, DW_AT_name("offset12")
	.dwattr $C$DW$737, DW_AT_bit_offset(0x1a)
	.dwattr $C$DW$737, DW_AT_bit_size(0x06)
	.dwattr $C$DW$737, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$737, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$737, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$737, DW_AT_decl_line(0x214)
	.dwattr $C$DW$737, DW_AT_decl_column(0x0e)

$C$DW$738	.dwtag  DW_TAG_member
	.dwattr $C$DW$738, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$738, DW_AT_name("offset13")
	.dwattr $C$DW$738, DW_AT_bit_offset(0x14)
	.dwattr $C$DW$738, DW_AT_bit_size(0x06)
	.dwattr $C$DW$738, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$738, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$738, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$738, DW_AT_decl_line(0x215)
	.dwattr $C$DW$738, DW_AT_decl_column(0x0e)

$C$DW$739	.dwtag  DW_TAG_member
	.dwattr $C$DW$739, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$739, DW_AT_name("offset14")
	.dwattr $C$DW$739, DW_AT_bit_offset(0x0e)
	.dwattr $C$DW$739, DW_AT_bit_size(0x06)
	.dwattr $C$DW$739, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$739, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$739, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$739, DW_AT_decl_line(0x216)
	.dwattr $C$DW$739, DW_AT_decl_column(0x0e)

$C$DW$740	.dwtag  DW_TAG_member
	.dwattr $C$DW$740, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$740, DW_AT_name("offset15")
	.dwattr $C$DW$740, DW_AT_bit_offset(0x08)
	.dwattr $C$DW$740, DW_AT_bit_size(0x06)
	.dwattr $C$DW$740, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$740, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$740, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$740, DW_AT_decl_line(0x217)
	.dwattr $C$DW$740, DW_AT_decl_column(0x0e)

$C$DW$741	.dwtag  DW_TAG_member
	.dwattr $C$DW$741, DW_AT_type(*$C$DW$T$318)
	.dwattr $C$DW$741, DW_AT_name("A_LUT_VAL_3")
	.dwattr $C$DW$741, DW_AT_data_member_location[DW_OP_plus_uconst 0xf]
	.dwattr $C$DW$741, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$741, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$741, DW_AT_decl_line(0x218)
	.dwattr $C$DW$741, DW_AT_decl_column(0x0d)

$C$DW$742	.dwtag  DW_TAG_member
	.dwattr $C$DW$742, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$742, DW_AT_name("offset16")
	.dwattr $C$DW$742, DW_AT_bit_offset(0x3a)
	.dwattr $C$DW$742, DW_AT_bit_size(0x06)
	.dwattr $C$DW$742, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$742, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$742, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$742, DW_AT_decl_line(0x21a)
	.dwattr $C$DW$742, DW_AT_decl_column(0x0e)

$C$DW$743	.dwtag  DW_TAG_member
	.dwattr $C$DW$743, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$743, DW_AT_name("offset17")
	.dwattr $C$DW$743, DW_AT_bit_offset(0x34)
	.dwattr $C$DW$743, DW_AT_bit_size(0x06)
	.dwattr $C$DW$743, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$743, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$743, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$743, DW_AT_decl_line(0x21b)
	.dwattr $C$DW$743, DW_AT_decl_column(0x0e)

$C$DW$744	.dwtag  DW_TAG_member
	.dwattr $C$DW$744, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$744, DW_AT_name("offset18")
	.dwattr $C$DW$744, DW_AT_bit_offset(0x2e)
	.dwattr $C$DW$744, DW_AT_bit_size(0x06)
	.dwattr $C$DW$744, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$744, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$744, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$744, DW_AT_decl_line(0x21c)
	.dwattr $C$DW$744, DW_AT_decl_column(0x0e)

$C$DW$745	.dwtag  DW_TAG_member
	.dwattr $C$DW$745, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$745, DW_AT_name("offset19")
	.dwattr $C$DW$745, DW_AT_bit_offset(0x28)
	.dwattr $C$DW$745, DW_AT_bit_size(0x06)
	.dwattr $C$DW$745, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$745, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$745, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$745, DW_AT_decl_line(0x21d)
	.dwattr $C$DW$745, DW_AT_decl_column(0x0e)

$C$DW$746	.dwtag  DW_TAG_member
	.dwattr $C$DW$746, DW_AT_type(*$C$DW$T$318)
	.dwattr $C$DW$746, DW_AT_name("A_LUT_VAL_4")
	.dwattr $C$DW$746, DW_AT_data_member_location[DW_OP_plus_uconst 0x13]
	.dwattr $C$DW$746, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$746, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$746, DW_AT_decl_line(0x21e)
	.dwattr $C$DW$746, DW_AT_decl_column(0x0d)

$C$DW$747	.dwtag  DW_TAG_member
	.dwattr $C$DW$747, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$747, DW_AT_name("offset20")
	.dwattr $C$DW$747, DW_AT_bit_offset(0x1a)
	.dwattr $C$DW$747, DW_AT_bit_size(0x06)
	.dwattr $C$DW$747, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$747, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$747, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$747, DW_AT_decl_line(0x21f)
	.dwattr $C$DW$747, DW_AT_decl_column(0x0e)

$C$DW$748	.dwtag  DW_TAG_member
	.dwattr $C$DW$748, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$748, DW_AT_name("offset21")
	.dwattr $C$DW$748, DW_AT_bit_offset(0x14)
	.dwattr $C$DW$748, DW_AT_bit_size(0x06)
	.dwattr $C$DW$748, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$748, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$748, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$748, DW_AT_decl_line(0x220)
	.dwattr $C$DW$748, DW_AT_decl_column(0x0e)

$C$DW$749	.dwtag  DW_TAG_member
	.dwattr $C$DW$749, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$749, DW_AT_name("offset22")
	.dwattr $C$DW$749, DW_AT_bit_offset(0x0e)
	.dwattr $C$DW$749, DW_AT_bit_size(0x06)
	.dwattr $C$DW$749, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$749, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$749, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$749, DW_AT_decl_line(0x221)
	.dwattr $C$DW$749, DW_AT_decl_column(0x0e)

$C$DW$750	.dwtag  DW_TAG_member
	.dwattr $C$DW$750, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$750, DW_AT_name("offset23")
	.dwattr $C$DW$750, DW_AT_bit_offset(0x08)
	.dwattr $C$DW$750, DW_AT_bit_size(0x06)
	.dwattr $C$DW$750, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$750, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$750, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$750, DW_AT_decl_line(0x222)
	.dwattr $C$DW$750, DW_AT_decl_column(0x0e)

$C$DW$751	.dwtag  DW_TAG_member
	.dwattr $C$DW$751, DW_AT_type(*$C$DW$T$318)
	.dwattr $C$DW$751, DW_AT_name("A_LUT_VAL_5")
	.dwattr $C$DW$751, DW_AT_data_member_location[DW_OP_plus_uconst 0x17]
	.dwattr $C$DW$751, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$751, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$751, DW_AT_decl_line(0x223)
	.dwattr $C$DW$751, DW_AT_decl_column(0x0d)

$C$DW$752	.dwtag  DW_TAG_member
	.dwattr $C$DW$752, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$752, DW_AT_name("offset24")
	.dwattr $C$DW$752, DW_AT_bit_offset(0x3a)
	.dwattr $C$DW$752, DW_AT_bit_size(0x06)
	.dwattr $C$DW$752, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$752, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$752, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$752, DW_AT_decl_line(0x225)
	.dwattr $C$DW$752, DW_AT_decl_column(0x0e)

$C$DW$753	.dwtag  DW_TAG_member
	.dwattr $C$DW$753, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$753, DW_AT_name("offset25")
	.dwattr $C$DW$753, DW_AT_bit_offset(0x34)
	.dwattr $C$DW$753, DW_AT_bit_size(0x06)
	.dwattr $C$DW$753, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$753, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$753, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$753, DW_AT_decl_line(0x226)
	.dwattr $C$DW$753, DW_AT_decl_column(0x0e)

$C$DW$754	.dwtag  DW_TAG_member
	.dwattr $C$DW$754, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$754, DW_AT_name("offset26")
	.dwattr $C$DW$754, DW_AT_bit_offset(0x2e)
	.dwattr $C$DW$754, DW_AT_bit_size(0x06)
	.dwattr $C$DW$754, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$754, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$754, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$754, DW_AT_decl_line(0x227)
	.dwattr $C$DW$754, DW_AT_decl_column(0x0e)

$C$DW$755	.dwtag  DW_TAG_member
	.dwattr $C$DW$755, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$755, DW_AT_name("offset27")
	.dwattr $C$DW$755, DW_AT_bit_offset(0x28)
	.dwattr $C$DW$755, DW_AT_bit_size(0x06)
	.dwattr $C$DW$755, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$755, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$755, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$755, DW_AT_decl_line(0x228)
	.dwattr $C$DW$755, DW_AT_decl_column(0x0e)

$C$DW$756	.dwtag  DW_TAG_member
	.dwattr $C$DW$756, DW_AT_type(*$C$DW$T$318)
	.dwattr $C$DW$756, DW_AT_name("A_LUT_VAL_6")
	.dwattr $C$DW$756, DW_AT_data_member_location[DW_OP_plus_uconst 0x1b]
	.dwattr $C$DW$756, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$756, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$756, DW_AT_decl_line(0x229)
	.dwattr $C$DW$756, DW_AT_decl_column(0x0d)

$C$DW$757	.dwtag  DW_TAG_member
	.dwattr $C$DW$757, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$757, DW_AT_name("offset28")
	.dwattr $C$DW$757, DW_AT_bit_offset(0x1a)
	.dwattr $C$DW$757, DW_AT_bit_size(0x06)
	.dwattr $C$DW$757, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$757, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$757, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$757, DW_AT_decl_line(0x22a)
	.dwattr $C$DW$757, DW_AT_decl_column(0x0e)

$C$DW$758	.dwtag  DW_TAG_member
	.dwattr $C$DW$758, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$758, DW_AT_name("offset29")
	.dwattr $C$DW$758, DW_AT_bit_offset(0x14)
	.dwattr $C$DW$758, DW_AT_bit_size(0x06)
	.dwattr $C$DW$758, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$758, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$758, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$758, DW_AT_decl_line(0x22b)
	.dwattr $C$DW$758, DW_AT_decl_column(0x0e)

$C$DW$759	.dwtag  DW_TAG_member
	.dwattr $C$DW$759, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$759, DW_AT_name("offset30")
	.dwattr $C$DW$759, DW_AT_bit_offset(0x0e)
	.dwattr $C$DW$759, DW_AT_bit_size(0x06)
	.dwattr $C$DW$759, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$759, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$759, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$759, DW_AT_decl_line(0x22c)
	.dwattr $C$DW$759, DW_AT_decl_column(0x0e)

$C$DW$760	.dwtag  DW_TAG_member
	.dwattr $C$DW$760, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$760, DW_AT_name("offset31")
	.dwattr $C$DW$760, DW_AT_bit_offset(0x08)
	.dwattr $C$DW$760, DW_AT_bit_size(0x06)
	.dwattr $C$DW$760, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$760, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$760, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$760, DW_AT_decl_line(0x22d)
	.dwattr $C$DW$760, DW_AT_decl_column(0x0e)

$C$DW$761	.dwtag  DW_TAG_member
	.dwattr $C$DW$761, DW_AT_type(*$C$DW$T$318)
	.dwattr $C$DW$761, DW_AT_name("A_LUT_VAL_7")
	.dwattr $C$DW$761, DW_AT_data_member_location[DW_OP_plus_uconst 0x1f]
	.dwattr $C$DW$761, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$761, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$761, DW_AT_decl_line(0x22e)
	.dwattr $C$DW$761, DW_AT_decl_column(0x0d)

$C$DW$762	.dwtag  DW_TAG_member
	.dwattr $C$DW$762, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$762, DW_AT_name("offset32")
	.dwattr $C$DW$762, DW_AT_bit_offset(0x3a)
	.dwattr $C$DW$762, DW_AT_bit_size(0x06)
	.dwattr $C$DW$762, DW_AT_data_member_location[DW_OP_plus_uconst 0x20]
	.dwattr $C$DW$762, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$762, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$762, DW_AT_decl_line(0x230)
	.dwattr $C$DW$762, DW_AT_decl_column(0x0e)

$C$DW$763	.dwtag  DW_TAG_member
	.dwattr $C$DW$763, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$763, DW_AT_name("offset33")
	.dwattr $C$DW$763, DW_AT_bit_offset(0x34)
	.dwattr $C$DW$763, DW_AT_bit_size(0x06)
	.dwattr $C$DW$763, DW_AT_data_member_location[DW_OP_plus_uconst 0x20]
	.dwattr $C$DW$763, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$763, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$763, DW_AT_decl_line(0x231)
	.dwattr $C$DW$763, DW_AT_decl_column(0x0e)

$C$DW$764	.dwtag  DW_TAG_member
	.dwattr $C$DW$764, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$764, DW_AT_name("offset34")
	.dwattr $C$DW$764, DW_AT_bit_offset(0x2e)
	.dwattr $C$DW$764, DW_AT_bit_size(0x06)
	.dwattr $C$DW$764, DW_AT_data_member_location[DW_OP_plus_uconst 0x20]
	.dwattr $C$DW$764, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$764, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$764, DW_AT_decl_line(0x232)
	.dwattr $C$DW$764, DW_AT_decl_column(0x0e)

$C$DW$765	.dwtag  DW_TAG_member
	.dwattr $C$DW$765, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$765, DW_AT_name("offset35")
	.dwattr $C$DW$765, DW_AT_bit_offset(0x28)
	.dwattr $C$DW$765, DW_AT_bit_size(0x06)
	.dwattr $C$DW$765, DW_AT_data_member_location[DW_OP_plus_uconst 0x20]
	.dwattr $C$DW$765, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$765, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$765, DW_AT_decl_line(0x233)
	.dwattr $C$DW$765, DW_AT_decl_column(0x0e)

$C$DW$766	.dwtag  DW_TAG_member
	.dwattr $C$DW$766, DW_AT_type(*$C$DW$T$318)
	.dwattr $C$DW$766, DW_AT_name("A_LUT_VAL_8")
	.dwattr $C$DW$766, DW_AT_data_member_location[DW_OP_plus_uconst 0x23]
	.dwattr $C$DW$766, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$766, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$766, DW_AT_decl_line(0x234)
	.dwattr $C$DW$766, DW_AT_decl_column(0x0d)

$C$DW$767	.dwtag  DW_TAG_member
	.dwattr $C$DW$767, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$767, DW_AT_name("offset36")
	.dwattr $C$DW$767, DW_AT_bit_offset(0x1a)
	.dwattr $C$DW$767, DW_AT_bit_size(0x06)
	.dwattr $C$DW$767, DW_AT_data_member_location[DW_OP_plus_uconst 0x20]
	.dwattr $C$DW$767, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$767, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$767, DW_AT_decl_line(0x235)
	.dwattr $C$DW$767, DW_AT_decl_column(0x0e)

$C$DW$768	.dwtag  DW_TAG_member
	.dwattr $C$DW$768, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$768, DW_AT_name("offset37")
	.dwattr $C$DW$768, DW_AT_bit_offset(0x14)
	.dwattr $C$DW$768, DW_AT_bit_size(0x06)
	.dwattr $C$DW$768, DW_AT_data_member_location[DW_OP_plus_uconst 0x20]
	.dwattr $C$DW$768, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$768, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$768, DW_AT_decl_line(0x236)
	.dwattr $C$DW$768, DW_AT_decl_column(0x0e)

$C$DW$769	.dwtag  DW_TAG_member
	.dwattr $C$DW$769, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$769, DW_AT_name("offset38")
	.dwattr $C$DW$769, DW_AT_bit_offset(0x0e)
	.dwattr $C$DW$769, DW_AT_bit_size(0x06)
	.dwattr $C$DW$769, DW_AT_data_member_location[DW_OP_plus_uconst 0x20]
	.dwattr $C$DW$769, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$769, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$769, DW_AT_decl_line(0x237)
	.dwattr $C$DW$769, DW_AT_decl_column(0x0e)

$C$DW$770	.dwtag  DW_TAG_member
	.dwattr $C$DW$770, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$770, DW_AT_name("offset39")
	.dwattr $C$DW$770, DW_AT_bit_offset(0x08)
	.dwattr $C$DW$770, DW_AT_bit_size(0x06)
	.dwattr $C$DW$770, DW_AT_data_member_location[DW_OP_plus_uconst 0x20]
	.dwattr $C$DW$770, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$770, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$770, DW_AT_decl_line(0x238)
	.dwattr $C$DW$770, DW_AT_decl_column(0x0e)

$C$DW$771	.dwtag  DW_TAG_member
	.dwattr $C$DW$771, DW_AT_type(*$C$DW$T$318)
	.dwattr $C$DW$771, DW_AT_name("A_LUT_VAL_9")
	.dwattr $C$DW$771, DW_AT_data_member_location[DW_OP_plus_uconst 0x27]
	.dwattr $C$DW$771, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$771, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$771, DW_AT_decl_line(0x239)
	.dwattr $C$DW$771, DW_AT_decl_column(0x0d)

$C$DW$772	.dwtag  DW_TAG_member
	.dwattr $C$DW$772, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$772, DW_AT_name("offset40")
	.dwattr $C$DW$772, DW_AT_bit_offset(0x3a)
	.dwattr $C$DW$772, DW_AT_bit_size(0x06)
	.dwattr $C$DW$772, DW_AT_data_member_location[DW_OP_plus_uconst 0x28]
	.dwattr $C$DW$772, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$772, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$772, DW_AT_decl_line(0x23b)
	.dwattr $C$DW$772, DW_AT_decl_column(0x0e)

$C$DW$773	.dwtag  DW_TAG_member
	.dwattr $C$DW$773, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$773, DW_AT_name("offset41")
	.dwattr $C$DW$773, DW_AT_bit_offset(0x34)
	.dwattr $C$DW$773, DW_AT_bit_size(0x06)
	.dwattr $C$DW$773, DW_AT_data_member_location[DW_OP_plus_uconst 0x28]
	.dwattr $C$DW$773, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$773, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$773, DW_AT_decl_line(0x23c)
	.dwattr $C$DW$773, DW_AT_decl_column(0x0e)

$C$DW$774	.dwtag  DW_TAG_member
	.dwattr $C$DW$774, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$774, DW_AT_name("offset42")
	.dwattr $C$DW$774, DW_AT_bit_offset(0x2e)
	.dwattr $C$DW$774, DW_AT_bit_size(0x06)
	.dwattr $C$DW$774, DW_AT_data_member_location[DW_OP_plus_uconst 0x28]
	.dwattr $C$DW$774, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$774, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$774, DW_AT_decl_line(0x23d)
	.dwattr $C$DW$774, DW_AT_decl_column(0x0e)

$C$DW$775	.dwtag  DW_TAG_member
	.dwattr $C$DW$775, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$775, DW_AT_name("offset43")
	.dwattr $C$DW$775, DW_AT_bit_offset(0x28)
	.dwattr $C$DW$775, DW_AT_bit_size(0x06)
	.dwattr $C$DW$775, DW_AT_data_member_location[DW_OP_plus_uconst 0x28]
	.dwattr $C$DW$775, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$775, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$775, DW_AT_decl_line(0x23e)
	.dwattr $C$DW$775, DW_AT_decl_column(0x0e)

$C$DW$776	.dwtag  DW_TAG_member
	.dwattr $C$DW$776, DW_AT_type(*$C$DW$T$318)
	.dwattr $C$DW$776, DW_AT_name("A_LUT_VAL_10")
	.dwattr $C$DW$776, DW_AT_data_member_location[DW_OP_plus_uconst 0x2b]
	.dwattr $C$DW$776, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$776, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$776, DW_AT_decl_line(0x23f)
	.dwattr $C$DW$776, DW_AT_decl_column(0x0d)

$C$DW$777	.dwtag  DW_TAG_member
	.dwattr $C$DW$777, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$777, DW_AT_name("offset44")
	.dwattr $C$DW$777, DW_AT_bit_offset(0x1a)
	.dwattr $C$DW$777, DW_AT_bit_size(0x06)
	.dwattr $C$DW$777, DW_AT_data_member_location[DW_OP_plus_uconst 0x28]
	.dwattr $C$DW$777, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$777, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$777, DW_AT_decl_line(0x240)
	.dwattr $C$DW$777, DW_AT_decl_column(0x0e)

$C$DW$778	.dwtag  DW_TAG_member
	.dwattr $C$DW$778, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$778, DW_AT_name("offset45")
	.dwattr $C$DW$778, DW_AT_bit_offset(0x14)
	.dwattr $C$DW$778, DW_AT_bit_size(0x06)
	.dwattr $C$DW$778, DW_AT_data_member_location[DW_OP_plus_uconst 0x28]
	.dwattr $C$DW$778, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$778, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$778, DW_AT_decl_line(0x241)
	.dwattr $C$DW$778, DW_AT_decl_column(0x0e)

$C$DW$779	.dwtag  DW_TAG_member
	.dwattr $C$DW$779, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$779, DW_AT_name("offset46")
	.dwattr $C$DW$779, DW_AT_bit_offset(0x0e)
	.dwattr $C$DW$779, DW_AT_bit_size(0x06)
	.dwattr $C$DW$779, DW_AT_data_member_location[DW_OP_plus_uconst 0x28]
	.dwattr $C$DW$779, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$779, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$779, DW_AT_decl_line(0x242)
	.dwattr $C$DW$779, DW_AT_decl_column(0x0e)

$C$DW$780	.dwtag  DW_TAG_member
	.dwattr $C$DW$780, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$780, DW_AT_name("offset47")
	.dwattr $C$DW$780, DW_AT_bit_offset(0x08)
	.dwattr $C$DW$780, DW_AT_bit_size(0x06)
	.dwattr $C$DW$780, DW_AT_data_member_location[DW_OP_plus_uconst 0x28]
	.dwattr $C$DW$780, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$780, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$780, DW_AT_decl_line(0x243)
	.dwattr $C$DW$780, DW_AT_decl_column(0x0e)

$C$DW$781	.dwtag  DW_TAG_member
	.dwattr $C$DW$781, DW_AT_type(*$C$DW$T$318)
	.dwattr $C$DW$781, DW_AT_name("A_LUT_VAL_11")
	.dwattr $C$DW$781, DW_AT_data_member_location[DW_OP_plus_uconst 0x2f]
	.dwattr $C$DW$781, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$781, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$781, DW_AT_decl_line(0x244)
	.dwattr $C$DW$781, DW_AT_decl_column(0x0d)

$C$DW$782	.dwtag  DW_TAG_member
	.dwattr $C$DW$782, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$782, DW_AT_name("offset48")
	.dwattr $C$DW$782, DW_AT_bit_offset(0x3a)
	.dwattr $C$DW$782, DW_AT_bit_size(0x06)
	.dwattr $C$DW$782, DW_AT_data_member_location[DW_OP_plus_uconst 0x30]
	.dwattr $C$DW$782, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$782, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$782, DW_AT_decl_line(0x246)
	.dwattr $C$DW$782, DW_AT_decl_column(0x0e)

$C$DW$783	.dwtag  DW_TAG_member
	.dwattr $C$DW$783, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$783, DW_AT_name("offset49")
	.dwattr $C$DW$783, DW_AT_bit_offset(0x34)
	.dwattr $C$DW$783, DW_AT_bit_size(0x06)
	.dwattr $C$DW$783, DW_AT_data_member_location[DW_OP_plus_uconst 0x30]
	.dwattr $C$DW$783, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$783, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$783, DW_AT_decl_line(0x247)
	.dwattr $C$DW$783, DW_AT_decl_column(0x0e)

$C$DW$784	.dwtag  DW_TAG_member
	.dwattr $C$DW$784, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$784, DW_AT_name("offset50")
	.dwattr $C$DW$784, DW_AT_bit_offset(0x2e)
	.dwattr $C$DW$784, DW_AT_bit_size(0x06)
	.dwattr $C$DW$784, DW_AT_data_member_location[DW_OP_plus_uconst 0x30]
	.dwattr $C$DW$784, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$784, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$784, DW_AT_decl_line(0x248)
	.dwattr $C$DW$784, DW_AT_decl_column(0x0e)

$C$DW$785	.dwtag  DW_TAG_member
	.dwattr $C$DW$785, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$785, DW_AT_name("offset51")
	.dwattr $C$DW$785, DW_AT_bit_offset(0x28)
	.dwattr $C$DW$785, DW_AT_bit_size(0x06)
	.dwattr $C$DW$785, DW_AT_data_member_location[DW_OP_plus_uconst 0x30]
	.dwattr $C$DW$785, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$785, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$785, DW_AT_decl_line(0x249)
	.dwattr $C$DW$785, DW_AT_decl_column(0x0e)

$C$DW$786	.dwtag  DW_TAG_member
	.dwattr $C$DW$786, DW_AT_type(*$C$DW$T$318)
	.dwattr $C$DW$786, DW_AT_name("A_LUT_VAL_12")
	.dwattr $C$DW$786, DW_AT_data_member_location[DW_OP_plus_uconst 0x33]
	.dwattr $C$DW$786, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$786, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$786, DW_AT_decl_line(0x24a)
	.dwattr $C$DW$786, DW_AT_decl_column(0x0d)

$C$DW$787	.dwtag  DW_TAG_member
	.dwattr $C$DW$787, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$787, DW_AT_name("offset52")
	.dwattr $C$DW$787, DW_AT_bit_offset(0x1a)
	.dwattr $C$DW$787, DW_AT_bit_size(0x06)
	.dwattr $C$DW$787, DW_AT_data_member_location[DW_OP_plus_uconst 0x30]
	.dwattr $C$DW$787, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$787, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$787, DW_AT_decl_line(0x24b)
	.dwattr $C$DW$787, DW_AT_decl_column(0x0e)

$C$DW$788	.dwtag  DW_TAG_member
	.dwattr $C$DW$788, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$788, DW_AT_name("offset53")
	.dwattr $C$DW$788, DW_AT_bit_offset(0x14)
	.dwattr $C$DW$788, DW_AT_bit_size(0x06)
	.dwattr $C$DW$788, DW_AT_data_member_location[DW_OP_plus_uconst 0x30]
	.dwattr $C$DW$788, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$788, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$788, DW_AT_decl_line(0x24c)
	.dwattr $C$DW$788, DW_AT_decl_column(0x0e)

$C$DW$789	.dwtag  DW_TAG_member
	.dwattr $C$DW$789, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$789, DW_AT_name("offset54")
	.dwattr $C$DW$789, DW_AT_bit_offset(0x0e)
	.dwattr $C$DW$789, DW_AT_bit_size(0x06)
	.dwattr $C$DW$789, DW_AT_data_member_location[DW_OP_plus_uconst 0x30]
	.dwattr $C$DW$789, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$789, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$789, DW_AT_decl_line(0x24d)
	.dwattr $C$DW$789, DW_AT_decl_column(0x0e)

$C$DW$790	.dwtag  DW_TAG_member
	.dwattr $C$DW$790, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$790, DW_AT_name("offset55")
	.dwattr $C$DW$790, DW_AT_bit_offset(0x08)
	.dwattr $C$DW$790, DW_AT_bit_size(0x06)
	.dwattr $C$DW$790, DW_AT_data_member_location[DW_OP_plus_uconst 0x30]
	.dwattr $C$DW$790, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$790, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$790, DW_AT_decl_line(0x24e)
	.dwattr $C$DW$790, DW_AT_decl_column(0x0e)

$C$DW$791	.dwtag  DW_TAG_member
	.dwattr $C$DW$791, DW_AT_type(*$C$DW$T$318)
	.dwattr $C$DW$791, DW_AT_name("A_LUT_VAL_13")
	.dwattr $C$DW$791, DW_AT_data_member_location[DW_OP_plus_uconst 0x37]
	.dwattr $C$DW$791, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$791, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$791, DW_AT_decl_line(0x24f)
	.dwattr $C$DW$791, DW_AT_decl_column(0x0d)

$C$DW$792	.dwtag  DW_TAG_member
	.dwattr $C$DW$792, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$792, DW_AT_name("offset56")
	.dwattr $C$DW$792, DW_AT_bit_offset(0x3a)
	.dwattr $C$DW$792, DW_AT_bit_size(0x06)
	.dwattr $C$DW$792, DW_AT_data_member_location[DW_OP_plus_uconst 0x38]
	.dwattr $C$DW$792, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$792, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$792, DW_AT_decl_line(0x251)
	.dwattr $C$DW$792, DW_AT_decl_column(0x0e)

$C$DW$793	.dwtag  DW_TAG_member
	.dwattr $C$DW$793, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$793, DW_AT_name("offset57")
	.dwattr $C$DW$793, DW_AT_bit_offset(0x34)
	.dwattr $C$DW$793, DW_AT_bit_size(0x06)
	.dwattr $C$DW$793, DW_AT_data_member_location[DW_OP_plus_uconst 0x38]
	.dwattr $C$DW$793, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$793, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$793, DW_AT_decl_line(0x252)
	.dwattr $C$DW$793, DW_AT_decl_column(0x0e)

$C$DW$794	.dwtag  DW_TAG_member
	.dwattr $C$DW$794, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$794, DW_AT_name("offset58")
	.dwattr $C$DW$794, DW_AT_bit_offset(0x2e)
	.dwattr $C$DW$794, DW_AT_bit_size(0x06)
	.dwattr $C$DW$794, DW_AT_data_member_location[DW_OP_plus_uconst 0x38]
	.dwattr $C$DW$794, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$794, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$794, DW_AT_decl_line(0x253)
	.dwattr $C$DW$794, DW_AT_decl_column(0x0e)

$C$DW$795	.dwtag  DW_TAG_member
	.dwattr $C$DW$795, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$795, DW_AT_name("offset59")
	.dwattr $C$DW$795, DW_AT_bit_offset(0x28)
	.dwattr $C$DW$795, DW_AT_bit_size(0x06)
	.dwattr $C$DW$795, DW_AT_data_member_location[DW_OP_plus_uconst 0x38]
	.dwattr $C$DW$795, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$795, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$795, DW_AT_decl_line(0x254)
	.dwattr $C$DW$795, DW_AT_decl_column(0x0e)

$C$DW$796	.dwtag  DW_TAG_member
	.dwattr $C$DW$796, DW_AT_type(*$C$DW$T$318)
	.dwattr $C$DW$796, DW_AT_name("A_LUT_VAL_14")
	.dwattr $C$DW$796, DW_AT_data_member_location[DW_OP_plus_uconst 0x3b]
	.dwattr $C$DW$796, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$796, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$796, DW_AT_decl_line(0x255)
	.dwattr $C$DW$796, DW_AT_decl_column(0x0d)

$C$DW$797	.dwtag  DW_TAG_member
	.dwattr $C$DW$797, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$797, DW_AT_name("offset60")
	.dwattr $C$DW$797, DW_AT_bit_offset(0x1a)
	.dwattr $C$DW$797, DW_AT_bit_size(0x06)
	.dwattr $C$DW$797, DW_AT_data_member_location[DW_OP_plus_uconst 0x38]
	.dwattr $C$DW$797, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$797, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$797, DW_AT_decl_line(0x256)
	.dwattr $C$DW$797, DW_AT_decl_column(0x0e)

$C$DW$798	.dwtag  DW_TAG_member
	.dwattr $C$DW$798, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$798, DW_AT_name("offset61")
	.dwattr $C$DW$798, DW_AT_bit_offset(0x14)
	.dwattr $C$DW$798, DW_AT_bit_size(0x06)
	.dwattr $C$DW$798, DW_AT_data_member_location[DW_OP_plus_uconst 0x38]
	.dwattr $C$DW$798, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$798, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$798, DW_AT_decl_line(0x257)
	.dwattr $C$DW$798, DW_AT_decl_column(0x0e)

$C$DW$799	.dwtag  DW_TAG_member
	.dwattr $C$DW$799, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$799, DW_AT_name("offset62")
	.dwattr $C$DW$799, DW_AT_bit_offset(0x0e)
	.dwattr $C$DW$799, DW_AT_bit_size(0x06)
	.dwattr $C$DW$799, DW_AT_data_member_location[DW_OP_plus_uconst 0x38]
	.dwattr $C$DW$799, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$799, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$799, DW_AT_decl_line(0x258)
	.dwattr $C$DW$799, DW_AT_decl_column(0x0e)

$C$DW$800	.dwtag  DW_TAG_member
	.dwattr $C$DW$800, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$800, DW_AT_name("offset63")
	.dwattr $C$DW$800, DW_AT_bit_offset(0x08)
	.dwattr $C$DW$800, DW_AT_bit_size(0x06)
	.dwattr $C$DW$800, DW_AT_data_member_location[DW_OP_plus_uconst 0x38]
	.dwattr $C$DW$800, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$800, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$800, DW_AT_decl_line(0x259)
	.dwattr $C$DW$800, DW_AT_decl_column(0x0e)

$C$DW$801	.dwtag  DW_TAG_member
	.dwattr $C$DW$801, DW_AT_type(*$C$DW$T$318)
	.dwattr $C$DW$801, DW_AT_name("A_LUT_VAL_15")
	.dwattr $C$DW$801, DW_AT_data_member_location[DW_OP_plus_uconst 0x3f]
	.dwattr $C$DW$801, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$801, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$801, DW_AT_decl_line(0x25a)
	.dwattr $C$DW$801, DW_AT_decl_column(0x0d)

	.dwattr $C$DW$T$475, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$T$475, DW_AT_decl_line(0x201)
	.dwattr $C$DW$T$475, DW_AT_decl_column(0x2c)
	.dwendtag $C$DW$T$475

	.dwendtag $C$DW$TU$475


$C$DW$TU$481	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$481

$C$DW$T$481	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$481, DW_AT_name("__HWA_OFFSET_REG_t")
	.dwattr $C$DW$T$481, DW_AT_byte_size(0x40)
$C$DW$802	.dwtag  DW_TAG_member
	.dwattr $C$DW$802, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$802, DW_AT_name("offset0")
	.dwattr $C$DW$802, DW_AT_bit_offset(0x3a)
	.dwattr $C$DW$802, DW_AT_bit_size(0x06)
	.dwattr $C$DW$802, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$802, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$802, DW_AT_decl_line(0x204)
	.dwattr $C$DW$802, DW_AT_decl_column(0x0e)

$C$DW$803	.dwtag  DW_TAG_member
	.dwattr $C$DW$803, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$803, DW_AT_name("offset1")
	.dwattr $C$DW$803, DW_AT_bit_offset(0x34)
	.dwattr $C$DW$803, DW_AT_bit_size(0x06)
	.dwattr $C$DW$803, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$803, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$803, DW_AT_decl_line(0x205)
	.dwattr $C$DW$803, DW_AT_decl_column(0x0e)

$C$DW$804	.dwtag  DW_TAG_member
	.dwattr $C$DW$804, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$804, DW_AT_name("offset2")
	.dwattr $C$DW$804, DW_AT_bit_offset(0x2e)
	.dwattr $C$DW$804, DW_AT_bit_size(0x06)
	.dwattr $C$DW$804, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$804, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$804, DW_AT_decl_line(0x206)
	.dwattr $C$DW$804, DW_AT_decl_column(0x0e)

$C$DW$805	.dwtag  DW_TAG_member
	.dwattr $C$DW$805, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$805, DW_AT_name("offset3")
	.dwattr $C$DW$805, DW_AT_bit_offset(0x28)
	.dwattr $C$DW$805, DW_AT_bit_size(0x06)
	.dwattr $C$DW$805, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$805, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$805, DW_AT_decl_line(0x207)
	.dwattr $C$DW$805, DW_AT_decl_column(0x0e)

$C$DW$806	.dwtag  DW_TAG_member
	.dwattr $C$DW$806, DW_AT_type(*$C$DW$T$318)
	.dwattr $C$DW$806, DW_AT_name("A_LUT_VAL_0")
	.dwattr $C$DW$806, DW_AT_data_member_location[DW_OP_plus_uconst 0x3]
	.dwattr $C$DW$806, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$806, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$806, DW_AT_decl_line(0x208)
	.dwattr $C$DW$806, DW_AT_decl_column(0x0d)

$C$DW$807	.dwtag  DW_TAG_member
	.dwattr $C$DW$807, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$807, DW_AT_name("offset4")
	.dwattr $C$DW$807, DW_AT_bit_offset(0x1a)
	.dwattr $C$DW$807, DW_AT_bit_size(0x06)
	.dwattr $C$DW$807, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$807, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$807, DW_AT_decl_line(0x209)
	.dwattr $C$DW$807, DW_AT_decl_column(0x0e)

$C$DW$808	.dwtag  DW_TAG_member
	.dwattr $C$DW$808, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$808, DW_AT_name("offset5")
	.dwattr $C$DW$808, DW_AT_bit_offset(0x14)
	.dwattr $C$DW$808, DW_AT_bit_size(0x06)
	.dwattr $C$DW$808, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$808, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$808, DW_AT_decl_line(0x20a)
	.dwattr $C$DW$808, DW_AT_decl_column(0x0e)

$C$DW$809	.dwtag  DW_TAG_member
	.dwattr $C$DW$809, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$809, DW_AT_name("offset6")
	.dwattr $C$DW$809, DW_AT_bit_offset(0x0e)
	.dwattr $C$DW$809, DW_AT_bit_size(0x06)
	.dwattr $C$DW$809, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$809, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$809, DW_AT_decl_line(0x20b)
	.dwattr $C$DW$809, DW_AT_decl_column(0x0e)

$C$DW$810	.dwtag  DW_TAG_member
	.dwattr $C$DW$810, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$810, DW_AT_name("offset7")
	.dwattr $C$DW$810, DW_AT_bit_offset(0x08)
	.dwattr $C$DW$810, DW_AT_bit_size(0x06)
	.dwattr $C$DW$810, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$810, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$810, DW_AT_decl_line(0x20c)
	.dwattr $C$DW$810, DW_AT_decl_column(0x0e)

$C$DW$811	.dwtag  DW_TAG_member
	.dwattr $C$DW$811, DW_AT_type(*$C$DW$T$318)
	.dwattr $C$DW$811, DW_AT_name("A_LUT_VAL_1")
	.dwattr $C$DW$811, DW_AT_data_member_location[DW_OP_plus_uconst 0x7]
	.dwattr $C$DW$811, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$811, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$811, DW_AT_decl_line(0x20d)
	.dwattr $C$DW$811, DW_AT_decl_column(0x0d)

$C$DW$812	.dwtag  DW_TAG_member
	.dwattr $C$DW$812, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$812, DW_AT_name("offset8")
	.dwattr $C$DW$812, DW_AT_bit_offset(0x3a)
	.dwattr $C$DW$812, DW_AT_bit_size(0x06)
	.dwattr $C$DW$812, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$812, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$812, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$812, DW_AT_decl_line(0x20f)
	.dwattr $C$DW$812, DW_AT_decl_column(0x0e)

$C$DW$813	.dwtag  DW_TAG_member
	.dwattr $C$DW$813, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$813, DW_AT_name("offset9")
	.dwattr $C$DW$813, DW_AT_bit_offset(0x34)
	.dwattr $C$DW$813, DW_AT_bit_size(0x06)
	.dwattr $C$DW$813, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$813, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$813, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$813, DW_AT_decl_line(0x210)
	.dwattr $C$DW$813, DW_AT_decl_column(0x0e)

$C$DW$814	.dwtag  DW_TAG_member
	.dwattr $C$DW$814, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$814, DW_AT_name("offset10")
	.dwattr $C$DW$814, DW_AT_bit_offset(0x2e)
	.dwattr $C$DW$814, DW_AT_bit_size(0x06)
	.dwattr $C$DW$814, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$814, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$814, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$814, DW_AT_decl_line(0x211)
	.dwattr $C$DW$814, DW_AT_decl_column(0x0e)

$C$DW$815	.dwtag  DW_TAG_member
	.dwattr $C$DW$815, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$815, DW_AT_name("offset11")
	.dwattr $C$DW$815, DW_AT_bit_offset(0x28)
	.dwattr $C$DW$815, DW_AT_bit_size(0x06)
	.dwattr $C$DW$815, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$815, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$815, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$815, DW_AT_decl_line(0x212)
	.dwattr $C$DW$815, DW_AT_decl_column(0x0e)

$C$DW$816	.dwtag  DW_TAG_member
	.dwattr $C$DW$816, DW_AT_type(*$C$DW$T$318)
	.dwattr $C$DW$816, DW_AT_name("A_LUT_VAL_2")
	.dwattr $C$DW$816, DW_AT_data_member_location[DW_OP_plus_uconst 0xb]
	.dwattr $C$DW$816, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$816, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$816, DW_AT_decl_line(0x213)
	.dwattr $C$DW$816, DW_AT_decl_column(0x0d)

$C$DW$817	.dwtag  DW_TAG_member
	.dwattr $C$DW$817, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$817, DW_AT_name("offset12")
	.dwattr $C$DW$817, DW_AT_bit_offset(0x1a)
	.dwattr $C$DW$817, DW_AT_bit_size(0x06)
	.dwattr $C$DW$817, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$817, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$817, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$817, DW_AT_decl_line(0x214)
	.dwattr $C$DW$817, DW_AT_decl_column(0x0e)

$C$DW$818	.dwtag  DW_TAG_member
	.dwattr $C$DW$818, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$818, DW_AT_name("offset13")
	.dwattr $C$DW$818, DW_AT_bit_offset(0x14)
	.dwattr $C$DW$818, DW_AT_bit_size(0x06)
	.dwattr $C$DW$818, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$818, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$818, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$818, DW_AT_decl_line(0x215)
	.dwattr $C$DW$818, DW_AT_decl_column(0x0e)

$C$DW$819	.dwtag  DW_TAG_member
	.dwattr $C$DW$819, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$819, DW_AT_name("offset14")
	.dwattr $C$DW$819, DW_AT_bit_offset(0x0e)
	.dwattr $C$DW$819, DW_AT_bit_size(0x06)
	.dwattr $C$DW$819, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$819, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$819, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$819, DW_AT_decl_line(0x216)
	.dwattr $C$DW$819, DW_AT_decl_column(0x0e)

$C$DW$820	.dwtag  DW_TAG_member
	.dwattr $C$DW$820, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$820, DW_AT_name("offset15")
	.dwattr $C$DW$820, DW_AT_bit_offset(0x08)
	.dwattr $C$DW$820, DW_AT_bit_size(0x06)
	.dwattr $C$DW$820, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$820, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$820, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$820, DW_AT_decl_line(0x217)
	.dwattr $C$DW$820, DW_AT_decl_column(0x0e)

$C$DW$821	.dwtag  DW_TAG_member
	.dwattr $C$DW$821, DW_AT_type(*$C$DW$T$318)
	.dwattr $C$DW$821, DW_AT_name("A_LUT_VAL_3")
	.dwattr $C$DW$821, DW_AT_data_member_location[DW_OP_plus_uconst 0xf]
	.dwattr $C$DW$821, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$821, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$821, DW_AT_decl_line(0x218)
	.dwattr $C$DW$821, DW_AT_decl_column(0x0d)

$C$DW$822	.dwtag  DW_TAG_member
	.dwattr $C$DW$822, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$822, DW_AT_name("offset16")
	.dwattr $C$DW$822, DW_AT_bit_offset(0x3a)
	.dwattr $C$DW$822, DW_AT_bit_size(0x06)
	.dwattr $C$DW$822, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$822, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$822, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$822, DW_AT_decl_line(0x21a)
	.dwattr $C$DW$822, DW_AT_decl_column(0x0e)

$C$DW$823	.dwtag  DW_TAG_member
	.dwattr $C$DW$823, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$823, DW_AT_name("offset17")
	.dwattr $C$DW$823, DW_AT_bit_offset(0x34)
	.dwattr $C$DW$823, DW_AT_bit_size(0x06)
	.dwattr $C$DW$823, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$823, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$823, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$823, DW_AT_decl_line(0x21b)
	.dwattr $C$DW$823, DW_AT_decl_column(0x0e)

$C$DW$824	.dwtag  DW_TAG_member
	.dwattr $C$DW$824, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$824, DW_AT_name("offset18")
	.dwattr $C$DW$824, DW_AT_bit_offset(0x2e)
	.dwattr $C$DW$824, DW_AT_bit_size(0x06)
	.dwattr $C$DW$824, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$824, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$824, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$824, DW_AT_decl_line(0x21c)
	.dwattr $C$DW$824, DW_AT_decl_column(0x0e)

$C$DW$825	.dwtag  DW_TAG_member
	.dwattr $C$DW$825, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$825, DW_AT_name("offset19")
	.dwattr $C$DW$825, DW_AT_bit_offset(0x28)
	.dwattr $C$DW$825, DW_AT_bit_size(0x06)
	.dwattr $C$DW$825, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$825, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$825, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$825, DW_AT_decl_line(0x21d)
	.dwattr $C$DW$825, DW_AT_decl_column(0x0e)

$C$DW$826	.dwtag  DW_TAG_member
	.dwattr $C$DW$826, DW_AT_type(*$C$DW$T$318)
	.dwattr $C$DW$826, DW_AT_name("A_LUT_VAL_4")
	.dwattr $C$DW$826, DW_AT_data_member_location[DW_OP_plus_uconst 0x13]
	.dwattr $C$DW$826, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$826, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$826, DW_AT_decl_line(0x21e)
	.dwattr $C$DW$826, DW_AT_decl_column(0x0d)

$C$DW$827	.dwtag  DW_TAG_member
	.dwattr $C$DW$827, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$827, DW_AT_name("offset20")
	.dwattr $C$DW$827, DW_AT_bit_offset(0x1a)
	.dwattr $C$DW$827, DW_AT_bit_size(0x06)
	.dwattr $C$DW$827, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$827, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$827, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$827, DW_AT_decl_line(0x21f)
	.dwattr $C$DW$827, DW_AT_decl_column(0x0e)

$C$DW$828	.dwtag  DW_TAG_member
	.dwattr $C$DW$828, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$828, DW_AT_name("offset21")
	.dwattr $C$DW$828, DW_AT_bit_offset(0x14)
	.dwattr $C$DW$828, DW_AT_bit_size(0x06)
	.dwattr $C$DW$828, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$828, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$828, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$828, DW_AT_decl_line(0x220)
	.dwattr $C$DW$828, DW_AT_decl_column(0x0e)

$C$DW$829	.dwtag  DW_TAG_member
	.dwattr $C$DW$829, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$829, DW_AT_name("offset22")
	.dwattr $C$DW$829, DW_AT_bit_offset(0x0e)
	.dwattr $C$DW$829, DW_AT_bit_size(0x06)
	.dwattr $C$DW$829, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$829, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$829, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$829, DW_AT_decl_line(0x221)
	.dwattr $C$DW$829, DW_AT_decl_column(0x0e)

$C$DW$830	.dwtag  DW_TAG_member
	.dwattr $C$DW$830, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$830, DW_AT_name("offset23")
	.dwattr $C$DW$830, DW_AT_bit_offset(0x08)
	.dwattr $C$DW$830, DW_AT_bit_size(0x06)
	.dwattr $C$DW$830, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$830, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$830, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$830, DW_AT_decl_line(0x222)
	.dwattr $C$DW$830, DW_AT_decl_column(0x0e)

$C$DW$831	.dwtag  DW_TAG_member
	.dwattr $C$DW$831, DW_AT_type(*$C$DW$T$318)
	.dwattr $C$DW$831, DW_AT_name("A_LUT_VAL_5")
	.dwattr $C$DW$831, DW_AT_data_member_location[DW_OP_plus_uconst 0x17]
	.dwattr $C$DW$831, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$831, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$831, DW_AT_decl_line(0x223)
	.dwattr $C$DW$831, DW_AT_decl_column(0x0d)

$C$DW$832	.dwtag  DW_TAG_member
	.dwattr $C$DW$832, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$832, DW_AT_name("offset24")
	.dwattr $C$DW$832, DW_AT_bit_offset(0x3a)
	.dwattr $C$DW$832, DW_AT_bit_size(0x06)
	.dwattr $C$DW$832, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$832, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$832, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$832, DW_AT_decl_line(0x225)
	.dwattr $C$DW$832, DW_AT_decl_column(0x0e)

$C$DW$833	.dwtag  DW_TAG_member
	.dwattr $C$DW$833, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$833, DW_AT_name("offset25")
	.dwattr $C$DW$833, DW_AT_bit_offset(0x34)
	.dwattr $C$DW$833, DW_AT_bit_size(0x06)
	.dwattr $C$DW$833, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$833, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$833, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$833, DW_AT_decl_line(0x226)
	.dwattr $C$DW$833, DW_AT_decl_column(0x0e)

$C$DW$834	.dwtag  DW_TAG_member
	.dwattr $C$DW$834, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$834, DW_AT_name("offset26")
	.dwattr $C$DW$834, DW_AT_bit_offset(0x2e)
	.dwattr $C$DW$834, DW_AT_bit_size(0x06)
	.dwattr $C$DW$834, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$834, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$834, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$834, DW_AT_decl_line(0x227)
	.dwattr $C$DW$834, DW_AT_decl_column(0x0e)

$C$DW$835	.dwtag  DW_TAG_member
	.dwattr $C$DW$835, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$835, DW_AT_name("offset27")
	.dwattr $C$DW$835, DW_AT_bit_offset(0x28)
	.dwattr $C$DW$835, DW_AT_bit_size(0x06)
	.dwattr $C$DW$835, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$835, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$835, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$835, DW_AT_decl_line(0x228)
	.dwattr $C$DW$835, DW_AT_decl_column(0x0e)

$C$DW$836	.dwtag  DW_TAG_member
	.dwattr $C$DW$836, DW_AT_type(*$C$DW$T$318)
	.dwattr $C$DW$836, DW_AT_name("A_LUT_VAL_6")
	.dwattr $C$DW$836, DW_AT_data_member_location[DW_OP_plus_uconst 0x1b]
	.dwattr $C$DW$836, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$836, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$836, DW_AT_decl_line(0x229)
	.dwattr $C$DW$836, DW_AT_decl_column(0x0d)

$C$DW$837	.dwtag  DW_TAG_member
	.dwattr $C$DW$837, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$837, DW_AT_name("offset28")
	.dwattr $C$DW$837, DW_AT_bit_offset(0x1a)
	.dwattr $C$DW$837, DW_AT_bit_size(0x06)
	.dwattr $C$DW$837, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$837, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$837, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$837, DW_AT_decl_line(0x22a)
	.dwattr $C$DW$837, DW_AT_decl_column(0x0e)

$C$DW$838	.dwtag  DW_TAG_member
	.dwattr $C$DW$838, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$838, DW_AT_name("offset29")
	.dwattr $C$DW$838, DW_AT_bit_offset(0x14)
	.dwattr $C$DW$838, DW_AT_bit_size(0x06)
	.dwattr $C$DW$838, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$838, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$838, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$838, DW_AT_decl_line(0x22b)
	.dwattr $C$DW$838, DW_AT_decl_column(0x0e)

$C$DW$839	.dwtag  DW_TAG_member
	.dwattr $C$DW$839, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$839, DW_AT_name("offset30")
	.dwattr $C$DW$839, DW_AT_bit_offset(0x0e)
	.dwattr $C$DW$839, DW_AT_bit_size(0x06)
	.dwattr $C$DW$839, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$839, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$839, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$839, DW_AT_decl_line(0x22c)
	.dwattr $C$DW$839, DW_AT_decl_column(0x0e)

$C$DW$840	.dwtag  DW_TAG_member
	.dwattr $C$DW$840, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$840, DW_AT_name("offset31")
	.dwattr $C$DW$840, DW_AT_bit_offset(0x08)
	.dwattr $C$DW$840, DW_AT_bit_size(0x06)
	.dwattr $C$DW$840, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$840, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$840, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$840, DW_AT_decl_line(0x22d)
	.dwattr $C$DW$840, DW_AT_decl_column(0x0e)

$C$DW$841	.dwtag  DW_TAG_member
	.dwattr $C$DW$841, DW_AT_type(*$C$DW$T$318)
	.dwattr $C$DW$841, DW_AT_name("A_LUT_VAL_7")
	.dwattr $C$DW$841, DW_AT_data_member_location[DW_OP_plus_uconst 0x1f]
	.dwattr $C$DW$841, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$841, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$841, DW_AT_decl_line(0x22e)
	.dwattr $C$DW$841, DW_AT_decl_column(0x0d)

$C$DW$842	.dwtag  DW_TAG_member
	.dwattr $C$DW$842, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$842, DW_AT_name("offset32")
	.dwattr $C$DW$842, DW_AT_bit_offset(0x3a)
	.dwattr $C$DW$842, DW_AT_bit_size(0x06)
	.dwattr $C$DW$842, DW_AT_data_member_location[DW_OP_plus_uconst 0x20]
	.dwattr $C$DW$842, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$842, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$842, DW_AT_decl_line(0x230)
	.dwattr $C$DW$842, DW_AT_decl_column(0x0e)

$C$DW$843	.dwtag  DW_TAG_member
	.dwattr $C$DW$843, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$843, DW_AT_name("offset33")
	.dwattr $C$DW$843, DW_AT_bit_offset(0x34)
	.dwattr $C$DW$843, DW_AT_bit_size(0x06)
	.dwattr $C$DW$843, DW_AT_data_member_location[DW_OP_plus_uconst 0x20]
	.dwattr $C$DW$843, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$843, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$843, DW_AT_decl_line(0x231)
	.dwattr $C$DW$843, DW_AT_decl_column(0x0e)

$C$DW$844	.dwtag  DW_TAG_member
	.dwattr $C$DW$844, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$844, DW_AT_name("offset34")
	.dwattr $C$DW$844, DW_AT_bit_offset(0x2e)
	.dwattr $C$DW$844, DW_AT_bit_size(0x06)
	.dwattr $C$DW$844, DW_AT_data_member_location[DW_OP_plus_uconst 0x20]
	.dwattr $C$DW$844, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$844, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$844, DW_AT_decl_line(0x232)
	.dwattr $C$DW$844, DW_AT_decl_column(0x0e)

$C$DW$845	.dwtag  DW_TAG_member
	.dwattr $C$DW$845, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$845, DW_AT_name("offset35")
	.dwattr $C$DW$845, DW_AT_bit_offset(0x28)
	.dwattr $C$DW$845, DW_AT_bit_size(0x06)
	.dwattr $C$DW$845, DW_AT_data_member_location[DW_OP_plus_uconst 0x20]
	.dwattr $C$DW$845, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$845, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$845, DW_AT_decl_line(0x233)
	.dwattr $C$DW$845, DW_AT_decl_column(0x0e)

$C$DW$846	.dwtag  DW_TAG_member
	.dwattr $C$DW$846, DW_AT_type(*$C$DW$T$318)
	.dwattr $C$DW$846, DW_AT_name("A_LUT_VAL_8")
	.dwattr $C$DW$846, DW_AT_data_member_location[DW_OP_plus_uconst 0x23]
	.dwattr $C$DW$846, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$846, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$846, DW_AT_decl_line(0x234)
	.dwattr $C$DW$846, DW_AT_decl_column(0x0d)

$C$DW$847	.dwtag  DW_TAG_member
	.dwattr $C$DW$847, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$847, DW_AT_name("offset36")
	.dwattr $C$DW$847, DW_AT_bit_offset(0x1a)
	.dwattr $C$DW$847, DW_AT_bit_size(0x06)
	.dwattr $C$DW$847, DW_AT_data_member_location[DW_OP_plus_uconst 0x20]
	.dwattr $C$DW$847, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$847, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$847, DW_AT_decl_line(0x235)
	.dwattr $C$DW$847, DW_AT_decl_column(0x0e)

$C$DW$848	.dwtag  DW_TAG_member
	.dwattr $C$DW$848, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$848, DW_AT_name("offset37")
	.dwattr $C$DW$848, DW_AT_bit_offset(0x14)
	.dwattr $C$DW$848, DW_AT_bit_size(0x06)
	.dwattr $C$DW$848, DW_AT_data_member_location[DW_OP_plus_uconst 0x20]
	.dwattr $C$DW$848, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$848, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$848, DW_AT_decl_line(0x236)
	.dwattr $C$DW$848, DW_AT_decl_column(0x0e)

$C$DW$849	.dwtag  DW_TAG_member
	.dwattr $C$DW$849, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$849, DW_AT_name("offset38")
	.dwattr $C$DW$849, DW_AT_bit_offset(0x0e)
	.dwattr $C$DW$849, DW_AT_bit_size(0x06)
	.dwattr $C$DW$849, DW_AT_data_member_location[DW_OP_plus_uconst 0x20]
	.dwattr $C$DW$849, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$849, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$849, DW_AT_decl_line(0x237)
	.dwattr $C$DW$849, DW_AT_decl_column(0x0e)

$C$DW$850	.dwtag  DW_TAG_member
	.dwattr $C$DW$850, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$850, DW_AT_name("offset39")
	.dwattr $C$DW$850, DW_AT_bit_offset(0x08)
	.dwattr $C$DW$850, DW_AT_bit_size(0x06)
	.dwattr $C$DW$850, DW_AT_data_member_location[DW_OP_plus_uconst 0x20]
	.dwattr $C$DW$850, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$850, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$850, DW_AT_decl_line(0x238)
	.dwattr $C$DW$850, DW_AT_decl_column(0x0e)

$C$DW$851	.dwtag  DW_TAG_member
	.dwattr $C$DW$851, DW_AT_type(*$C$DW$T$318)
	.dwattr $C$DW$851, DW_AT_name("A_LUT_VAL_9")
	.dwattr $C$DW$851, DW_AT_data_member_location[DW_OP_plus_uconst 0x27]
	.dwattr $C$DW$851, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$851, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$851, DW_AT_decl_line(0x239)
	.dwattr $C$DW$851, DW_AT_decl_column(0x0d)

$C$DW$852	.dwtag  DW_TAG_member
	.dwattr $C$DW$852, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$852, DW_AT_name("offset40")
	.dwattr $C$DW$852, DW_AT_bit_offset(0x3a)
	.dwattr $C$DW$852, DW_AT_bit_size(0x06)
	.dwattr $C$DW$852, DW_AT_data_member_location[DW_OP_plus_uconst 0x28]
	.dwattr $C$DW$852, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$852, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$852, DW_AT_decl_line(0x23b)
	.dwattr $C$DW$852, DW_AT_decl_column(0x0e)

$C$DW$853	.dwtag  DW_TAG_member
	.dwattr $C$DW$853, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$853, DW_AT_name("offset41")
	.dwattr $C$DW$853, DW_AT_bit_offset(0x34)
	.dwattr $C$DW$853, DW_AT_bit_size(0x06)
	.dwattr $C$DW$853, DW_AT_data_member_location[DW_OP_plus_uconst 0x28]
	.dwattr $C$DW$853, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$853, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$853, DW_AT_decl_line(0x23c)
	.dwattr $C$DW$853, DW_AT_decl_column(0x0e)

$C$DW$854	.dwtag  DW_TAG_member
	.dwattr $C$DW$854, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$854, DW_AT_name("offset42")
	.dwattr $C$DW$854, DW_AT_bit_offset(0x2e)
	.dwattr $C$DW$854, DW_AT_bit_size(0x06)
	.dwattr $C$DW$854, DW_AT_data_member_location[DW_OP_plus_uconst 0x28]
	.dwattr $C$DW$854, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$854, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$854, DW_AT_decl_line(0x23d)
	.dwattr $C$DW$854, DW_AT_decl_column(0x0e)

$C$DW$855	.dwtag  DW_TAG_member
	.dwattr $C$DW$855, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$855, DW_AT_name("offset43")
	.dwattr $C$DW$855, DW_AT_bit_offset(0x28)
	.dwattr $C$DW$855, DW_AT_bit_size(0x06)
	.dwattr $C$DW$855, DW_AT_data_member_location[DW_OP_plus_uconst 0x28]
	.dwattr $C$DW$855, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$855, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$855, DW_AT_decl_line(0x23e)
	.dwattr $C$DW$855, DW_AT_decl_column(0x0e)

$C$DW$856	.dwtag  DW_TAG_member
	.dwattr $C$DW$856, DW_AT_type(*$C$DW$T$318)
	.dwattr $C$DW$856, DW_AT_name("A_LUT_VAL_10")
	.dwattr $C$DW$856, DW_AT_data_member_location[DW_OP_plus_uconst 0x2b]
	.dwattr $C$DW$856, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$856, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$856, DW_AT_decl_line(0x23f)
	.dwattr $C$DW$856, DW_AT_decl_column(0x0d)

$C$DW$857	.dwtag  DW_TAG_member
	.dwattr $C$DW$857, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$857, DW_AT_name("offset44")
	.dwattr $C$DW$857, DW_AT_bit_offset(0x1a)
	.dwattr $C$DW$857, DW_AT_bit_size(0x06)
	.dwattr $C$DW$857, DW_AT_data_member_location[DW_OP_plus_uconst 0x28]
	.dwattr $C$DW$857, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$857, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$857, DW_AT_decl_line(0x240)
	.dwattr $C$DW$857, DW_AT_decl_column(0x0e)

$C$DW$858	.dwtag  DW_TAG_member
	.dwattr $C$DW$858, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$858, DW_AT_name("offset45")
	.dwattr $C$DW$858, DW_AT_bit_offset(0x14)
	.dwattr $C$DW$858, DW_AT_bit_size(0x06)
	.dwattr $C$DW$858, DW_AT_data_member_location[DW_OP_plus_uconst 0x28]
	.dwattr $C$DW$858, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$858, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$858, DW_AT_decl_line(0x241)
	.dwattr $C$DW$858, DW_AT_decl_column(0x0e)

$C$DW$859	.dwtag  DW_TAG_member
	.dwattr $C$DW$859, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$859, DW_AT_name("offset46")
	.dwattr $C$DW$859, DW_AT_bit_offset(0x0e)
	.dwattr $C$DW$859, DW_AT_bit_size(0x06)
	.dwattr $C$DW$859, DW_AT_data_member_location[DW_OP_plus_uconst 0x28]
	.dwattr $C$DW$859, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$859, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$859, DW_AT_decl_line(0x242)
	.dwattr $C$DW$859, DW_AT_decl_column(0x0e)

$C$DW$860	.dwtag  DW_TAG_member
	.dwattr $C$DW$860, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$860, DW_AT_name("offset47")
	.dwattr $C$DW$860, DW_AT_bit_offset(0x08)
	.dwattr $C$DW$860, DW_AT_bit_size(0x06)
	.dwattr $C$DW$860, DW_AT_data_member_location[DW_OP_plus_uconst 0x28]
	.dwattr $C$DW$860, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$860, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$860, DW_AT_decl_line(0x243)
	.dwattr $C$DW$860, DW_AT_decl_column(0x0e)

$C$DW$861	.dwtag  DW_TAG_member
	.dwattr $C$DW$861, DW_AT_type(*$C$DW$T$318)
	.dwattr $C$DW$861, DW_AT_name("A_LUT_VAL_11")
	.dwattr $C$DW$861, DW_AT_data_member_location[DW_OP_plus_uconst 0x2f]
	.dwattr $C$DW$861, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$861, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$861, DW_AT_decl_line(0x244)
	.dwattr $C$DW$861, DW_AT_decl_column(0x0d)

$C$DW$862	.dwtag  DW_TAG_member
	.dwattr $C$DW$862, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$862, DW_AT_name("offset48")
	.dwattr $C$DW$862, DW_AT_bit_offset(0x3a)
	.dwattr $C$DW$862, DW_AT_bit_size(0x06)
	.dwattr $C$DW$862, DW_AT_data_member_location[DW_OP_plus_uconst 0x30]
	.dwattr $C$DW$862, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$862, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$862, DW_AT_decl_line(0x246)
	.dwattr $C$DW$862, DW_AT_decl_column(0x0e)

$C$DW$863	.dwtag  DW_TAG_member
	.dwattr $C$DW$863, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$863, DW_AT_name("offset49")
	.dwattr $C$DW$863, DW_AT_bit_offset(0x34)
	.dwattr $C$DW$863, DW_AT_bit_size(0x06)
	.dwattr $C$DW$863, DW_AT_data_member_location[DW_OP_plus_uconst 0x30]
	.dwattr $C$DW$863, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$863, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$863, DW_AT_decl_line(0x247)
	.dwattr $C$DW$863, DW_AT_decl_column(0x0e)

$C$DW$864	.dwtag  DW_TAG_member
	.dwattr $C$DW$864, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$864, DW_AT_name("offset50")
	.dwattr $C$DW$864, DW_AT_bit_offset(0x2e)
	.dwattr $C$DW$864, DW_AT_bit_size(0x06)
	.dwattr $C$DW$864, DW_AT_data_member_location[DW_OP_plus_uconst 0x30]
	.dwattr $C$DW$864, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$864, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$864, DW_AT_decl_line(0x248)
	.dwattr $C$DW$864, DW_AT_decl_column(0x0e)

$C$DW$865	.dwtag  DW_TAG_member
	.dwattr $C$DW$865, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$865, DW_AT_name("offset51")
	.dwattr $C$DW$865, DW_AT_bit_offset(0x28)
	.dwattr $C$DW$865, DW_AT_bit_size(0x06)
	.dwattr $C$DW$865, DW_AT_data_member_location[DW_OP_plus_uconst 0x30]
	.dwattr $C$DW$865, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$865, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$865, DW_AT_decl_line(0x249)
	.dwattr $C$DW$865, DW_AT_decl_column(0x0e)

$C$DW$866	.dwtag  DW_TAG_member
	.dwattr $C$DW$866, DW_AT_type(*$C$DW$T$318)
	.dwattr $C$DW$866, DW_AT_name("A_LUT_VAL_12")
	.dwattr $C$DW$866, DW_AT_data_member_location[DW_OP_plus_uconst 0x33]
	.dwattr $C$DW$866, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$866, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$866, DW_AT_decl_line(0x24a)
	.dwattr $C$DW$866, DW_AT_decl_column(0x0d)

$C$DW$867	.dwtag  DW_TAG_member
	.dwattr $C$DW$867, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$867, DW_AT_name("offset52")
	.dwattr $C$DW$867, DW_AT_bit_offset(0x1a)
	.dwattr $C$DW$867, DW_AT_bit_size(0x06)
	.dwattr $C$DW$867, DW_AT_data_member_location[DW_OP_plus_uconst 0x30]
	.dwattr $C$DW$867, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$867, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$867, DW_AT_decl_line(0x24b)
	.dwattr $C$DW$867, DW_AT_decl_column(0x0e)

$C$DW$868	.dwtag  DW_TAG_member
	.dwattr $C$DW$868, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$868, DW_AT_name("offset53")
	.dwattr $C$DW$868, DW_AT_bit_offset(0x14)
	.dwattr $C$DW$868, DW_AT_bit_size(0x06)
	.dwattr $C$DW$868, DW_AT_data_member_location[DW_OP_plus_uconst 0x30]
	.dwattr $C$DW$868, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$868, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$868, DW_AT_decl_line(0x24c)
	.dwattr $C$DW$868, DW_AT_decl_column(0x0e)

$C$DW$869	.dwtag  DW_TAG_member
	.dwattr $C$DW$869, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$869, DW_AT_name("offset54")
	.dwattr $C$DW$869, DW_AT_bit_offset(0x0e)
	.dwattr $C$DW$869, DW_AT_bit_size(0x06)
	.dwattr $C$DW$869, DW_AT_data_member_location[DW_OP_plus_uconst 0x30]
	.dwattr $C$DW$869, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$869, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$869, DW_AT_decl_line(0x24d)
	.dwattr $C$DW$869, DW_AT_decl_column(0x0e)

$C$DW$870	.dwtag  DW_TAG_member
	.dwattr $C$DW$870, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$870, DW_AT_name("offset55")
	.dwattr $C$DW$870, DW_AT_bit_offset(0x08)
	.dwattr $C$DW$870, DW_AT_bit_size(0x06)
	.dwattr $C$DW$870, DW_AT_data_member_location[DW_OP_plus_uconst 0x30]
	.dwattr $C$DW$870, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$870, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$870, DW_AT_decl_line(0x24e)
	.dwattr $C$DW$870, DW_AT_decl_column(0x0e)

$C$DW$871	.dwtag  DW_TAG_member
	.dwattr $C$DW$871, DW_AT_type(*$C$DW$T$318)
	.dwattr $C$DW$871, DW_AT_name("A_LUT_VAL_13")
	.dwattr $C$DW$871, DW_AT_data_member_location[DW_OP_plus_uconst 0x37]
	.dwattr $C$DW$871, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$871, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$871, DW_AT_decl_line(0x24f)
	.dwattr $C$DW$871, DW_AT_decl_column(0x0d)

$C$DW$872	.dwtag  DW_TAG_member
	.dwattr $C$DW$872, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$872, DW_AT_name("offset56")
	.dwattr $C$DW$872, DW_AT_bit_offset(0x3a)
	.dwattr $C$DW$872, DW_AT_bit_size(0x06)
	.dwattr $C$DW$872, DW_AT_data_member_location[DW_OP_plus_uconst 0x38]
	.dwattr $C$DW$872, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$872, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$872, DW_AT_decl_line(0x251)
	.dwattr $C$DW$872, DW_AT_decl_column(0x0e)

$C$DW$873	.dwtag  DW_TAG_member
	.dwattr $C$DW$873, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$873, DW_AT_name("offset57")
	.dwattr $C$DW$873, DW_AT_bit_offset(0x34)
	.dwattr $C$DW$873, DW_AT_bit_size(0x06)
	.dwattr $C$DW$873, DW_AT_data_member_location[DW_OP_plus_uconst 0x38]
	.dwattr $C$DW$873, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$873, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$873, DW_AT_decl_line(0x252)
	.dwattr $C$DW$873, DW_AT_decl_column(0x0e)

$C$DW$874	.dwtag  DW_TAG_member
	.dwattr $C$DW$874, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$874, DW_AT_name("offset58")
	.dwattr $C$DW$874, DW_AT_bit_offset(0x2e)
	.dwattr $C$DW$874, DW_AT_bit_size(0x06)
	.dwattr $C$DW$874, DW_AT_data_member_location[DW_OP_plus_uconst 0x38]
	.dwattr $C$DW$874, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$874, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$874, DW_AT_decl_line(0x253)
	.dwattr $C$DW$874, DW_AT_decl_column(0x0e)

$C$DW$875	.dwtag  DW_TAG_member
	.dwattr $C$DW$875, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$875, DW_AT_name("offset59")
	.dwattr $C$DW$875, DW_AT_bit_offset(0x28)
	.dwattr $C$DW$875, DW_AT_bit_size(0x06)
	.dwattr $C$DW$875, DW_AT_data_member_location[DW_OP_plus_uconst 0x38]
	.dwattr $C$DW$875, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$875, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$875, DW_AT_decl_line(0x254)
	.dwattr $C$DW$875, DW_AT_decl_column(0x0e)

$C$DW$876	.dwtag  DW_TAG_member
	.dwattr $C$DW$876, DW_AT_type(*$C$DW$T$318)
	.dwattr $C$DW$876, DW_AT_name("A_LUT_VAL_14")
	.dwattr $C$DW$876, DW_AT_data_member_location[DW_OP_plus_uconst 0x3b]
	.dwattr $C$DW$876, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$876, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$876, DW_AT_decl_line(0x255)
	.dwattr $C$DW$876, DW_AT_decl_column(0x0d)

$C$DW$877	.dwtag  DW_TAG_member
	.dwattr $C$DW$877, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$877, DW_AT_name("offset60")
	.dwattr $C$DW$877, DW_AT_bit_offset(0x1a)
	.dwattr $C$DW$877, DW_AT_bit_size(0x06)
	.dwattr $C$DW$877, DW_AT_data_member_location[DW_OP_plus_uconst 0x38]
	.dwattr $C$DW$877, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$877, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$877, DW_AT_decl_line(0x256)
	.dwattr $C$DW$877, DW_AT_decl_column(0x0e)

$C$DW$878	.dwtag  DW_TAG_member
	.dwattr $C$DW$878, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$878, DW_AT_name("offset61")
	.dwattr $C$DW$878, DW_AT_bit_offset(0x14)
	.dwattr $C$DW$878, DW_AT_bit_size(0x06)
	.dwattr $C$DW$878, DW_AT_data_member_location[DW_OP_plus_uconst 0x38]
	.dwattr $C$DW$878, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$878, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$878, DW_AT_decl_line(0x257)
	.dwattr $C$DW$878, DW_AT_decl_column(0x0e)

$C$DW$879	.dwtag  DW_TAG_member
	.dwattr $C$DW$879, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$879, DW_AT_name("offset62")
	.dwattr $C$DW$879, DW_AT_bit_offset(0x0e)
	.dwattr $C$DW$879, DW_AT_bit_size(0x06)
	.dwattr $C$DW$879, DW_AT_data_member_location[DW_OP_plus_uconst 0x38]
	.dwattr $C$DW$879, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$879, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$879, DW_AT_decl_line(0x258)
	.dwattr $C$DW$879, DW_AT_decl_column(0x0e)

$C$DW$880	.dwtag  DW_TAG_member
	.dwattr $C$DW$880, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$880, DW_AT_name("offset63")
	.dwattr $C$DW$880, DW_AT_bit_offset(0x08)
	.dwattr $C$DW$880, DW_AT_bit_size(0x06)
	.dwattr $C$DW$880, DW_AT_data_member_location[DW_OP_plus_uconst 0x38]
	.dwattr $C$DW$880, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$880, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$880, DW_AT_decl_line(0x259)
	.dwattr $C$DW$880, DW_AT_decl_column(0x0e)

$C$DW$881	.dwtag  DW_TAG_member
	.dwattr $C$DW$881, DW_AT_type(*$C$DW$T$318)
	.dwattr $C$DW$881, DW_AT_name("A_LUT_VAL_15")
	.dwattr $C$DW$881, DW_AT_data_member_location[DW_OP_plus_uconst 0x3f]
	.dwattr $C$DW$881, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$881, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$881, DW_AT_decl_line(0x25a)
	.dwattr $C$DW$881, DW_AT_decl_column(0x0d)


$C$DW$882	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$882, DW_AT_name("operator =")
	.dwattr $C$DW$882, DW_AT_declaration
	.dwattr $C$DW$882, DW_AT_linkage_name("_ZN18__HWA_OFFSET_REG_taSERKS_")
	.dwattr $C$DW$882, DW_AT_type(*$C$DW$T$476)
	.dwattr $C$DW$882, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$883	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$883, DW_AT_type(*$C$DW$T$478)

	.dwendtag $C$DW$882


$C$DW$884	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$884, DW_AT_name("operator =")
	.dwattr $C$DW$884, DW_AT_declaration
	.dwattr $C$DW$884, DW_AT_linkage_name("_ZN18__HWA_OFFSET_REG_taSEOS_")
	.dwattr $C$DW$884, DW_AT_type(*$C$DW$T$476)
	.dwattr $C$DW$884, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$885	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$885, DW_AT_type(*$C$DW$T$476)

	.dwendtag $C$DW$884

	.dwattr $C$DW$T$481, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$T$481, DW_AT_decl_line(0x201)
	.dwattr $C$DW$T$481, DW_AT_decl_column(0x2c)
	.dwendtag $C$DW$T$481

	.dwendtag $C$DW$TU$481


$C$DW$TU$572	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$572
$C$DW$T$572	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$572, DW_AT_name("__HWA_OFFSET_REG")
	.dwattr $C$DW$T$572, DW_AT_type(*$C$DW$T$481)
	.dwattr $C$DW$T$572, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$T$572, DW_AT_decl_line(0x2b6)
	.dwattr $C$DW$T$572, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$572


$C$DW$TU$477	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$477
$C$DW$T$477	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$477, DW_AT_type(*$C$DW$T$481)

	.dwendtag $C$DW$TU$477


$C$DW$TU$478	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$478
$C$DW$T$478	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$478, DW_AT_type(*$C$DW$T$477)
	.dwattr $C$DW$T$478, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$478


$C$DW$TU$476	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$476
$C$DW$T$476	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$476, DW_AT_type(*$C$DW$T$481)
	.dwattr $C$DW$T$476, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$476


$C$DW$TU$479	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$479

$C$DW$T$479	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$479, DW_AT_type(*$C$DW$T$476)
$C$DW$886	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$886, DW_AT_type(*$C$DW$T$478)

	.dwendtag $C$DW$T$479

	.dwendtag $C$DW$TU$479


$C$DW$TU$480	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$480

$C$DW$T$480	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$480, DW_AT_type(*$C$DW$T$476)
$C$DW$887	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$887, DW_AT_type(*$C$DW$T$476)

	.dwendtag $C$DW$T$480

	.dwendtag $C$DW$TU$480


$C$DW$TU$414	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$414

$C$DW$T$414	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$414, DW_AT_name("__MMA_A_CONFIG_ATYPE")
	.dwattr $C$DW$T$414, DW_AT_byte_size(0x01)
$C$DW$888	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$888, DW_AT_name("__MMA_A_CONFIG_ATYPE_UINT8")
	.dwattr $C$DW$888, DW_AT_const_value(0x00)
	.dwattr $C$DW$888, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$888, DW_AT_decl_line(0x8b)
	.dwattr $C$DW$888, DW_AT_decl_column(0x05)

$C$DW$889	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$889, DW_AT_name("__MMA_A_CONFIG_ATYPE_UINT16")
	.dwattr $C$DW$889, DW_AT_const_value(0x01)
	.dwattr $C$DW$889, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$889, DW_AT_decl_line(0x8c)
	.dwattr $C$DW$889, DW_AT_decl_column(0x05)

$C$DW$890	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$890, DW_AT_name("__MMA_A_CONFIG_ATYPE_UINT32")
	.dwattr $C$DW$890, DW_AT_const_value(0x03)
	.dwattr $C$DW$890, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$890, DW_AT_decl_line(0x8e)
	.dwattr $C$DW$890, DW_AT_decl_column(0x05)

$C$DW$891	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$891, DW_AT_name("__MMA_A_CONFIG_ATYPE_INT8")
	.dwattr $C$DW$891, DW_AT_const_value(0x04)
	.dwattr $C$DW$891, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$891, DW_AT_decl_line(0x8f)
	.dwattr $C$DW$891, DW_AT_decl_column(0x05)

$C$DW$892	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$892, DW_AT_name("__MMA_A_CONFIG_ATYPE_INT16")
	.dwattr $C$DW$892, DW_AT_const_value(0x05)
	.dwattr $C$DW$892, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$892, DW_AT_decl_line(0x90)
	.dwattr $C$DW$892, DW_AT_decl_column(0x05)

$C$DW$893	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$893, DW_AT_name("__MMA_A_CONFIG_ATYPE_INT32")
	.dwattr $C$DW$893, DW_AT_const_value(0x07)
	.dwattr $C$DW$893, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$893, DW_AT_decl_line(0x92)
	.dwattr $C$DW$893, DW_AT_decl_column(0x05)

	.dwattr $C$DW$T$414, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$T$414, DW_AT_decl_line(0x8a)
	.dwattr $C$DW$T$414, DW_AT_decl_column(0x01)
	.dwendtag $C$DW$T$414

	.dwendtag $C$DW$TU$414


$C$DW$TU$415	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$415
$C$DW$T$415	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$415, DW_AT_name("__MMA_A_CONFIG_ATYPE")
	.dwattr $C$DW$T$415, DW_AT_type(*$C$DW$T$414)
	.dwattr $C$DW$T$415, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$T$415, DW_AT_decl_line(0x93)
	.dwattr $C$DW$T$415, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$415


$C$DW$TU$443	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$443

$C$DW$T$443	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$443, DW_AT_name("__MMA_A_CONFIG_ATYPE")
	.dwattr $C$DW$T$443, DW_AT_byte_size(0x01)
$C$DW$894	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$894, DW_AT_name("__MMA_A_CONFIG_ATYPE_UINT8")
	.dwattr $C$DW$894, DW_AT_const_value(0x00)
	.dwattr $C$DW$894, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$894, DW_AT_decl_line(0x8b)
	.dwattr $C$DW$894, DW_AT_decl_column(0x05)

$C$DW$895	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$895, DW_AT_name("__MMA_A_CONFIG_ATYPE_UINT16")
	.dwattr $C$DW$895, DW_AT_const_value(0x01)
	.dwattr $C$DW$895, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$895, DW_AT_decl_line(0x8c)
	.dwattr $C$DW$895, DW_AT_decl_column(0x05)

$C$DW$896	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$896, DW_AT_name("__MMA_A_CONFIG_ATYPE_UINT32")
	.dwattr $C$DW$896, DW_AT_const_value(0x03)
	.dwattr $C$DW$896, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$896, DW_AT_decl_line(0x8e)
	.dwattr $C$DW$896, DW_AT_decl_column(0x05)

$C$DW$897	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$897, DW_AT_name("__MMA_A_CONFIG_ATYPE_INT8")
	.dwattr $C$DW$897, DW_AT_const_value(0x04)
	.dwattr $C$DW$897, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$897, DW_AT_decl_line(0x8f)
	.dwattr $C$DW$897, DW_AT_decl_column(0x05)

$C$DW$898	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$898, DW_AT_name("__MMA_A_CONFIG_ATYPE_INT16")
	.dwattr $C$DW$898, DW_AT_const_value(0x05)
	.dwattr $C$DW$898, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$898, DW_AT_decl_line(0x90)
	.dwattr $C$DW$898, DW_AT_decl_column(0x05)

$C$DW$899	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$899, DW_AT_name("__MMA_A_CONFIG_ATYPE_INT32")
	.dwattr $C$DW$899, DW_AT_const_value(0x07)
	.dwattr $C$DW$899, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$899, DW_AT_decl_line(0x92)
	.dwattr $C$DW$899, DW_AT_decl_column(0x05)

	.dwattr $C$DW$T$443, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$T$443, DW_AT_decl_line(0x8a)
	.dwattr $C$DW$T$443, DW_AT_decl_column(0x01)
	.dwendtag $C$DW$T$443

	.dwendtag $C$DW$TU$443


$C$DW$TU$444	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$444
$C$DW$T$444	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$444, DW_AT_name("__MMA_A_CONFIG_ATYPE")
	.dwattr $C$DW$T$444, DW_AT_type(*$C$DW$T$443)
	.dwattr $C$DW$T$444, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$T$444, DW_AT_decl_line(0x93)
	.dwattr $C$DW$T$444, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$444


$C$DW$TU$418	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$418

$C$DW$T$418	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$418, DW_AT_name("__MMA_A_LUTEN")
	.dwattr $C$DW$T$418, DW_AT_byte_size(0x01)
$C$DW$900	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$900, DW_AT_name("__MMA_A_CONFIG_NOLUT")
	.dwattr $C$DW$900, DW_AT_const_value(0x00)
	.dwattr $C$DW$900, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$900, DW_AT_decl_line(0x9a)
	.dwattr $C$DW$900, DW_AT_decl_column(0x05)

$C$DW$901	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$901, DW_AT_name("__MMA_A_CONFIG_LUT")
	.dwattr $C$DW$901, DW_AT_const_value(0x01)
	.dwattr $C$DW$901, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$901, DW_AT_decl_line(0x9b)
	.dwattr $C$DW$901, DW_AT_decl_column(0x05)

	.dwattr $C$DW$T$418, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$T$418, DW_AT_decl_line(0x99)
	.dwattr $C$DW$T$418, DW_AT_decl_column(0x01)
	.dwendtag $C$DW$T$418

	.dwendtag $C$DW$TU$418


$C$DW$TU$419	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$419
$C$DW$T$419	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$419, DW_AT_name("__MMA_A_LUTEN")
	.dwattr $C$DW$T$419, DW_AT_type(*$C$DW$T$418)
	.dwattr $C$DW$T$419, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$T$419, DW_AT_decl_line(0x9c)
	.dwattr $C$DW$T$419, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$419


$C$DW$TU$445	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$445

$C$DW$T$445	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$445, DW_AT_name("__MMA_A_LUTEN")
	.dwattr $C$DW$T$445, DW_AT_byte_size(0x01)
$C$DW$902	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$902, DW_AT_name("__MMA_A_CONFIG_NOLUT")
	.dwattr $C$DW$902, DW_AT_const_value(0x00)
	.dwattr $C$DW$902, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$902, DW_AT_decl_line(0x9a)
	.dwattr $C$DW$902, DW_AT_decl_column(0x05)

$C$DW$903	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$903, DW_AT_name("__MMA_A_CONFIG_LUT")
	.dwattr $C$DW$903, DW_AT_const_value(0x01)
	.dwattr $C$DW$903, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$903, DW_AT_decl_line(0x9b)
	.dwattr $C$DW$903, DW_AT_decl_column(0x05)

	.dwattr $C$DW$T$445, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$T$445, DW_AT_decl_line(0x99)
	.dwattr $C$DW$T$445, DW_AT_decl_column(0x01)
	.dwendtag $C$DW$T$445

	.dwendtag $C$DW$TU$445


$C$DW$TU$446	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$446
$C$DW$T$446	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$446, DW_AT_name("__MMA_A_LUTEN")
	.dwattr $C$DW$T$446, DW_AT_type(*$C$DW$T$445)
	.dwattr $C$DW$T$446, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$T$446, DW_AT_decl_line(0x9c)
	.dwattr $C$DW$T$446, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$446


$C$DW$TU$420	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$420

$C$DW$T$420	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$420, DW_AT_name("__MMA_B_BTYPE")
	.dwattr $C$DW$T$420, DW_AT_byte_size(0x01)
$C$DW$904	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$904, DW_AT_name("__MMA_B_CONFIG_SIZE8")
	.dwattr $C$DW$904, DW_AT_const_value(0x00)
	.dwattr $C$DW$904, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$904, DW_AT_decl_line(0xa5)
	.dwattr $C$DW$904, DW_AT_decl_column(0x05)

$C$DW$905	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$905, DW_AT_name("__MMA_B_CONFIG_SIZE16")
	.dwattr $C$DW$905, DW_AT_const_value(0x01)
	.dwattr $C$DW$905, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$905, DW_AT_decl_line(0xa6)
	.dwattr $C$DW$905, DW_AT_decl_column(0x05)

$C$DW$906	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$906, DW_AT_name("__MMA_B_CONFIG_SIZE32")
	.dwattr $C$DW$906, DW_AT_const_value(0x03)
	.dwattr $C$DW$906, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$906, DW_AT_decl_line(0xa8)
	.dwattr $C$DW$906, DW_AT_decl_column(0x05)

	.dwattr $C$DW$T$420, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$T$420, DW_AT_decl_line(0xa4)
	.dwattr $C$DW$T$420, DW_AT_decl_column(0x01)
	.dwendtag $C$DW$T$420

	.dwendtag $C$DW$TU$420


$C$DW$TU$421	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$421
$C$DW$T$421	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$421, DW_AT_name("__MMA_B_BTYPE")
	.dwattr $C$DW$T$421, DW_AT_type(*$C$DW$T$420)
	.dwattr $C$DW$T$421, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$T$421, DW_AT_decl_line(0xa9)
	.dwattr $C$DW$T$421, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$421


$C$DW$TU$447	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$447

$C$DW$T$447	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$447, DW_AT_name("__MMA_B_BTYPE")
	.dwattr $C$DW$T$447, DW_AT_byte_size(0x01)
$C$DW$907	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$907, DW_AT_name("__MMA_B_CONFIG_SIZE8")
	.dwattr $C$DW$907, DW_AT_const_value(0x00)
	.dwattr $C$DW$907, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$907, DW_AT_decl_line(0xa5)
	.dwattr $C$DW$907, DW_AT_decl_column(0x05)

$C$DW$908	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$908, DW_AT_name("__MMA_B_CONFIG_SIZE16")
	.dwattr $C$DW$908, DW_AT_const_value(0x01)
	.dwattr $C$DW$908, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$908, DW_AT_decl_line(0xa6)
	.dwattr $C$DW$908, DW_AT_decl_column(0x05)

$C$DW$909	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$909, DW_AT_name("__MMA_B_CONFIG_SIZE32")
	.dwattr $C$DW$909, DW_AT_const_value(0x03)
	.dwattr $C$DW$909, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$909, DW_AT_decl_line(0xa8)
	.dwattr $C$DW$909, DW_AT_decl_column(0x05)

	.dwattr $C$DW$T$447, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$T$447, DW_AT_decl_line(0xa4)
	.dwattr $C$DW$T$447, DW_AT_decl_column(0x01)
	.dwendtag $C$DW$T$447

	.dwendtag $C$DW$TU$447


$C$DW$TU$448	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$448
$C$DW$T$448	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$448, DW_AT_name("__MMA_B_BTYPE")
	.dwattr $C$DW$T$448, DW_AT_type(*$C$DW$T$447)
	.dwattr $C$DW$T$448, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$T$448, DW_AT_decl_line(0xa9)
	.dwattr $C$DW$T$448, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$448


$C$DW$TU$422	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$422

$C$DW$T$422	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$422, DW_AT_name("__MMA_B_CONFIG_ORDER_TRANSPOSE_CONTROL")
	.dwattr $C$DW$T$422, DW_AT_byte_size(0x01)
$C$DW$910	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$910, DW_AT_name("__MMA_B_CONFIG_ROW")
	.dwattr $C$DW$910, DW_AT_const_value(0x00)
	.dwattr $C$DW$910, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$910, DW_AT_decl_line(0xb0)
	.dwattr $C$DW$910, DW_AT_decl_column(0x05)

$C$DW$911	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$911, DW_AT_name("__MMA_B_CONFIG_COL")
	.dwattr $C$DW$911, DW_AT_const_value(0x01)
	.dwattr $C$DW$911, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$911, DW_AT_decl_line(0xb1)
	.dwattr $C$DW$911, DW_AT_decl_column(0x05)

	.dwattr $C$DW$T$422, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$T$422, DW_AT_decl_line(0xaf)
	.dwattr $C$DW$T$422, DW_AT_decl_column(0x01)
	.dwendtag $C$DW$T$422

	.dwendtag $C$DW$TU$422


$C$DW$TU$423	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$423
$C$DW$T$423	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$423, DW_AT_name("__MMA_B_CONFIG_ORDER_TRANSPOSE_CONTROL")
	.dwattr $C$DW$T$423, DW_AT_type(*$C$DW$T$422)
	.dwattr $C$DW$T$423, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$T$423, DW_AT_decl_line(0xb2)
	.dwattr $C$DW$T$423, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$423


$C$DW$TU$449	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$449

$C$DW$T$449	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$449, DW_AT_name("__MMA_B_CONFIG_ORDER_TRANSPOSE_CONTROL")
	.dwattr $C$DW$T$449, DW_AT_byte_size(0x01)
$C$DW$912	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$912, DW_AT_name("__MMA_B_CONFIG_ROW")
	.dwattr $C$DW$912, DW_AT_const_value(0x00)
	.dwattr $C$DW$912, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$912, DW_AT_decl_line(0xb0)
	.dwattr $C$DW$912, DW_AT_decl_column(0x05)

$C$DW$913	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$913, DW_AT_name("__MMA_B_CONFIG_COL")
	.dwattr $C$DW$913, DW_AT_const_value(0x01)
	.dwattr $C$DW$913, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$913, DW_AT_decl_line(0xb1)
	.dwattr $C$DW$913, DW_AT_decl_column(0x05)

	.dwattr $C$DW$T$449, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$T$449, DW_AT_decl_line(0xaf)
	.dwattr $C$DW$T$449, DW_AT_decl_column(0x01)
	.dwendtag $C$DW$T$449

	.dwendtag $C$DW$TU$449


$C$DW$TU$450	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$450
$C$DW$T$450	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$450, DW_AT_name("__MMA_B_CONFIG_ORDER_TRANSPOSE_CONTROL")
	.dwattr $C$DW$T$450, DW_AT_type(*$C$DW$T$449)
	.dwattr $C$DW$T$450, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$T$450, DW_AT_decl_line(0xb2)
	.dwattr $C$DW$T$450, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$450


$C$DW$TU$424	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$424

$C$DW$T$424	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$424, DW_AT_name("__MMA_C_CONFIG_ATYPE")
	.dwattr $C$DW$T$424, DW_AT_byte_size(0x01)
$C$DW$914	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$914, DW_AT_name("__MMA_C_CONFIG_ATYPE_UA")
	.dwattr $C$DW$914, DW_AT_const_value(0x00)
	.dwattr $C$DW$914, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$914, DW_AT_decl_line(0xbc)
	.dwattr $C$DW$914, DW_AT_decl_column(0x05)

$C$DW$915	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$915, DW_AT_name("__MMA_C_CONFIG_ATYPE_SA")
	.dwattr $C$DW$915, DW_AT_const_value(0x01)
	.dwattr $C$DW$915, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$915, DW_AT_decl_line(0xbd)
	.dwattr $C$DW$915, DW_AT_decl_column(0x05)

	.dwattr $C$DW$T$424, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$T$424, DW_AT_decl_line(0xbb)
	.dwattr $C$DW$T$424, DW_AT_decl_column(0x01)
	.dwendtag $C$DW$T$424

	.dwendtag $C$DW$TU$424


$C$DW$TU$425	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$425
$C$DW$T$425	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$425, DW_AT_name("__MMA_C_CONFIG_ATYPE")
	.dwattr $C$DW$T$425, DW_AT_type(*$C$DW$T$424)
	.dwattr $C$DW$T$425, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$T$425, DW_AT_decl_line(0xbe)
	.dwattr $C$DW$T$425, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$425


$C$DW$TU$451	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$451

$C$DW$T$451	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$451, DW_AT_name("__MMA_C_CONFIG_ATYPE")
	.dwattr $C$DW$T$451, DW_AT_byte_size(0x01)
$C$DW$916	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$916, DW_AT_name("__MMA_C_CONFIG_ATYPE_UA")
	.dwattr $C$DW$916, DW_AT_const_value(0x00)
	.dwattr $C$DW$916, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$916, DW_AT_decl_line(0xbc)
	.dwattr $C$DW$916, DW_AT_decl_column(0x05)

$C$DW$917	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$917, DW_AT_name("__MMA_C_CONFIG_ATYPE_SA")
	.dwattr $C$DW$917, DW_AT_const_value(0x01)
	.dwattr $C$DW$917, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$917, DW_AT_decl_line(0xbd)
	.dwattr $C$DW$917, DW_AT_decl_column(0x05)

	.dwattr $C$DW$T$451, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$T$451, DW_AT_decl_line(0xbb)
	.dwattr $C$DW$T$451, DW_AT_decl_column(0x01)
	.dwendtag $C$DW$T$451

	.dwendtag $C$DW$TU$451


$C$DW$TU$452	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$452
$C$DW$T$452	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$452, DW_AT_name("__MMA_C_CONFIG_ATYPE")
	.dwattr $C$DW$T$452, DW_AT_type(*$C$DW$T$451)
	.dwattr $C$DW$T$452, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$T$452, DW_AT_decl_line(0xbe)
	.dwattr $C$DW$T$452, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$452


$C$DW$TU$426	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$426

$C$DW$T$426	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$426, DW_AT_name("__MMA_C_CONFIG_BTYPE")
	.dwattr $C$DW$T$426, DW_AT_byte_size(0x01)
$C$DW$918	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$918, DW_AT_name("__MMA_C_CONFIG_BTYPE_UINT8")
	.dwattr $C$DW$918, DW_AT_const_value(0x00)
	.dwattr $C$DW$918, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$918, DW_AT_decl_line(0xc7)
	.dwattr $C$DW$918, DW_AT_decl_column(0x05)

$C$DW$919	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$919, DW_AT_name("__MMA_C_CONFIG_BTYPE_UINT16")
	.dwattr $C$DW$919, DW_AT_const_value(0x01)
	.dwattr $C$DW$919, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$919, DW_AT_decl_line(0xc8)
	.dwattr $C$DW$919, DW_AT_decl_column(0x05)

$C$DW$920	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$920, DW_AT_name("__MMA_C_CONFIG_BTYPE_UINT32")
	.dwattr $C$DW$920, DW_AT_const_value(0x03)
	.dwattr $C$DW$920, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$920, DW_AT_decl_line(0xca)
	.dwattr $C$DW$920, DW_AT_decl_column(0x05)

$C$DW$921	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$921, DW_AT_name("__MMA_C_CONFIG_BTYPE_INT8")
	.dwattr $C$DW$921, DW_AT_const_value(0x04)
	.dwattr $C$DW$921, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$921, DW_AT_decl_line(0xcb)
	.dwattr $C$DW$921, DW_AT_decl_column(0x05)

$C$DW$922	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$922, DW_AT_name("__MMA_C_CONFIG_BTYPE_INT16")
	.dwattr $C$DW$922, DW_AT_const_value(0x05)
	.dwattr $C$DW$922, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$922, DW_AT_decl_line(0xcc)
	.dwattr $C$DW$922, DW_AT_decl_column(0x05)

$C$DW$923	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$923, DW_AT_name("__MMA_C_CONFIG_BTYPE_INT32")
	.dwattr $C$DW$923, DW_AT_const_value(0x07)
	.dwattr $C$DW$923, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$923, DW_AT_decl_line(0xce)
	.dwattr $C$DW$923, DW_AT_decl_column(0x05)

	.dwattr $C$DW$T$426, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$T$426, DW_AT_decl_line(0xc6)
	.dwattr $C$DW$T$426, DW_AT_decl_column(0x01)
	.dwendtag $C$DW$T$426

	.dwendtag $C$DW$TU$426


$C$DW$TU$427	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$427
$C$DW$T$427	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$427, DW_AT_name("__MMA_C_CONFIG_BTYPE")
	.dwattr $C$DW$T$427, DW_AT_type(*$C$DW$T$426)
	.dwattr $C$DW$T$427, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$T$427, DW_AT_decl_line(0xcf)
	.dwattr $C$DW$T$427, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$427


$C$DW$TU$453	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$453

$C$DW$T$453	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$453, DW_AT_name("__MMA_C_CONFIG_BTYPE")
	.dwattr $C$DW$T$453, DW_AT_byte_size(0x01)
$C$DW$924	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$924, DW_AT_name("__MMA_C_CONFIG_BTYPE_UINT8")
	.dwattr $C$DW$924, DW_AT_const_value(0x00)
	.dwattr $C$DW$924, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$924, DW_AT_decl_line(0xc7)
	.dwattr $C$DW$924, DW_AT_decl_column(0x05)

$C$DW$925	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$925, DW_AT_name("__MMA_C_CONFIG_BTYPE_UINT16")
	.dwattr $C$DW$925, DW_AT_const_value(0x01)
	.dwattr $C$DW$925, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$925, DW_AT_decl_line(0xc8)
	.dwattr $C$DW$925, DW_AT_decl_column(0x05)

$C$DW$926	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$926, DW_AT_name("__MMA_C_CONFIG_BTYPE_UINT32")
	.dwattr $C$DW$926, DW_AT_const_value(0x03)
	.dwattr $C$DW$926, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$926, DW_AT_decl_line(0xca)
	.dwattr $C$DW$926, DW_AT_decl_column(0x05)

$C$DW$927	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$927, DW_AT_name("__MMA_C_CONFIG_BTYPE_INT8")
	.dwattr $C$DW$927, DW_AT_const_value(0x04)
	.dwattr $C$DW$927, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$927, DW_AT_decl_line(0xcb)
	.dwattr $C$DW$927, DW_AT_decl_column(0x05)

$C$DW$928	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$928, DW_AT_name("__MMA_C_CONFIG_BTYPE_INT16")
	.dwattr $C$DW$928, DW_AT_const_value(0x05)
	.dwattr $C$DW$928, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$928, DW_AT_decl_line(0xcc)
	.dwattr $C$DW$928, DW_AT_decl_column(0x05)

$C$DW$929	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$929, DW_AT_name("__MMA_C_CONFIG_BTYPE_INT32")
	.dwattr $C$DW$929, DW_AT_const_value(0x07)
	.dwattr $C$DW$929, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$929, DW_AT_decl_line(0xce)
	.dwattr $C$DW$929, DW_AT_decl_column(0x05)

	.dwattr $C$DW$T$453, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$T$453, DW_AT_decl_line(0xc6)
	.dwattr $C$DW$T$453, DW_AT_decl_column(0x01)
	.dwendtag $C$DW$T$453

	.dwendtag $C$DW$TU$453


$C$DW$TU$454	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$454
$C$DW$T$454	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$454, DW_AT_name("__MMA_C_CONFIG_BTYPE")
	.dwattr $C$DW$T$454, DW_AT_type(*$C$DW$T$453)
	.dwattr $C$DW$T$454, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$T$454, DW_AT_decl_line(0xcf)
	.dwattr $C$DW$T$454, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$454


$C$DW$TU$430	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$430

$C$DW$T$430	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$430, DW_AT_name("__MMA_C_CONFIG_HWLDDST")
	.dwattr $C$DW$T$430, DW_AT_byte_size(0x01)
$C$DW$930	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$930, DW_AT_name("__MMA_C_CONFIG_HWLDDST_X4_0")
	.dwattr $C$DW$930, DW_AT_const_value(0x00)
	.dwattr $C$DW$930, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$930, DW_AT_decl_line(0xdf)
	.dwattr $C$DW$930, DW_AT_decl_column(0x05)

$C$DW$931	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$931, DW_AT_name("__MMA_C_CONFIG_HWLDDST_X4_1")
	.dwattr $C$DW$931, DW_AT_const_value(0x01)
	.dwattr $C$DW$931, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$931, DW_AT_decl_line(0xe0)
	.dwattr $C$DW$931, DW_AT_decl_column(0x05)

$C$DW$932	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$932, DW_AT_name("__MMA_C_CONFIG_HWLDDST_X4_2")
	.dwattr $C$DW$932, DW_AT_const_value(0x02)
	.dwattr $C$DW$932, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$932, DW_AT_decl_line(0xe1)
	.dwattr $C$DW$932, DW_AT_decl_column(0x05)

$C$DW$933	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$933, DW_AT_name("__MMA_C_CONFIG_HWLDDST_X4_3")
	.dwattr $C$DW$933, DW_AT_const_value(0x03)
	.dwattr $C$DW$933, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$933, DW_AT_decl_line(0xe2)
	.dwattr $C$DW$933, DW_AT_decl_column(0x05)

$C$DW$934	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$934, DW_AT_name("__MMA_C_CONFIG_HWLDDST_X1")
	.dwattr $C$DW$934, DW_AT_const_value(0x07)
	.dwattr $C$DW$934, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$934, DW_AT_decl_line(0xe3)
	.dwattr $C$DW$934, DW_AT_decl_column(0x05)

	.dwattr $C$DW$T$430, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$T$430, DW_AT_decl_line(0xdd)
	.dwattr $C$DW$T$430, DW_AT_decl_column(0x01)
	.dwendtag $C$DW$T$430

	.dwendtag $C$DW$TU$430


$C$DW$TU$431	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$431
$C$DW$T$431	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$431, DW_AT_name("__MMA_C_CONFIG_HWLDDST")
	.dwattr $C$DW$T$431, DW_AT_type(*$C$DW$T$430)
	.dwattr $C$DW$T$431, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$T$431, DW_AT_decl_line(0xe5)
	.dwattr $C$DW$T$431, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$431


$C$DW$TU$457	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$457

$C$DW$T$457	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$457, DW_AT_name("__MMA_C_CONFIG_HWLDDST")
	.dwattr $C$DW$T$457, DW_AT_byte_size(0x01)
$C$DW$935	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$935, DW_AT_name("__MMA_C_CONFIG_HWLDDST_X4_0")
	.dwattr $C$DW$935, DW_AT_const_value(0x00)
	.dwattr $C$DW$935, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$935, DW_AT_decl_line(0xdf)
	.dwattr $C$DW$935, DW_AT_decl_column(0x05)

$C$DW$936	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$936, DW_AT_name("__MMA_C_CONFIG_HWLDDST_X4_1")
	.dwattr $C$DW$936, DW_AT_const_value(0x01)
	.dwattr $C$DW$936, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$936, DW_AT_decl_line(0xe0)
	.dwattr $C$DW$936, DW_AT_decl_column(0x05)

$C$DW$937	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$937, DW_AT_name("__MMA_C_CONFIG_HWLDDST_X4_2")
	.dwattr $C$DW$937, DW_AT_const_value(0x02)
	.dwattr $C$DW$937, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$937, DW_AT_decl_line(0xe1)
	.dwattr $C$DW$937, DW_AT_decl_column(0x05)

$C$DW$938	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$938, DW_AT_name("__MMA_C_CONFIG_HWLDDST_X4_3")
	.dwattr $C$DW$938, DW_AT_const_value(0x03)
	.dwattr $C$DW$938, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$938, DW_AT_decl_line(0xe2)
	.dwattr $C$DW$938, DW_AT_decl_column(0x05)

$C$DW$939	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$939, DW_AT_name("__MMA_C_CONFIG_HWLDDST_X1")
	.dwattr $C$DW$939, DW_AT_const_value(0x07)
	.dwattr $C$DW$939, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$939, DW_AT_decl_line(0xe3)
	.dwattr $C$DW$939, DW_AT_decl_column(0x05)

	.dwattr $C$DW$T$457, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$T$457, DW_AT_decl_line(0xdd)
	.dwattr $C$DW$T$457, DW_AT_decl_column(0x01)
	.dwendtag $C$DW$T$457

	.dwendtag $C$DW$TU$457


$C$DW$TU$458	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$458
$C$DW$T$458	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$458, DW_AT_name("__MMA_C_CONFIG_HWLDDST")
	.dwattr $C$DW$T$458, DW_AT_type(*$C$DW$T$457)
	.dwattr $C$DW$T$458, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$T$458, DW_AT_decl_line(0xe5)
	.dwattr $C$DW$T$458, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$458


$C$DW$TU$432	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$432

$C$DW$T$432	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$432, DW_AT_name("__MMA_C_CONFIG_HWLDTYPE")
	.dwattr $C$DW$T$432, DW_AT_byte_size(0x01)
$C$DW$940	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$940, DW_AT_name("__MMA_C_CONFIG_HWLDTYPE_UINT8")
	.dwattr $C$DW$940, DW_AT_const_value(0x00)
	.dwattr $C$DW$940, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$940, DW_AT_decl_line(0xec)
	.dwattr $C$DW$940, DW_AT_decl_column(0x05)

$C$DW$941	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$941, DW_AT_name("__MMA_C_CONFIG_HWLDTYPE_UINT16")
	.dwattr $C$DW$941, DW_AT_const_value(0x01)
	.dwattr $C$DW$941, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$941, DW_AT_decl_line(0xed)
	.dwattr $C$DW$941, DW_AT_decl_column(0x05)

$C$DW$942	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$942, DW_AT_name("__MMA_C_CONFIG_HWLDTYPE_UINT32")
	.dwattr $C$DW$942, DW_AT_const_value(0x03)
	.dwattr $C$DW$942, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$942, DW_AT_decl_line(0xef)
	.dwattr $C$DW$942, DW_AT_decl_column(0x05)

$C$DW$943	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$943, DW_AT_name("__MMA_C_CONFIG_HWLDTYPE_INT8")
	.dwattr $C$DW$943, DW_AT_const_value(0x08)
	.dwattr $C$DW$943, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$943, DW_AT_decl_line(0xf4)
	.dwattr $C$DW$943, DW_AT_decl_column(0x05)

$C$DW$944	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$944, DW_AT_name("__MMA_C_CONFIG_HWLDTYPE_INT16")
	.dwattr $C$DW$944, DW_AT_const_value(0x09)
	.dwattr $C$DW$944, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$944, DW_AT_decl_line(0xf5)
	.dwattr $C$DW$944, DW_AT_decl_column(0x05)

$C$DW$945	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$945, DW_AT_name("__MMA_C_CONFIG_HWLDTYPE_INT32")
	.dwattr $C$DW$945, DW_AT_const_value(0x0b)
	.dwattr $C$DW$945, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$945, DW_AT_decl_line(0xf7)
	.dwattr $C$DW$945, DW_AT_decl_column(0x05)

	.dwattr $C$DW$T$432, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$T$432, DW_AT_decl_line(0xeb)
	.dwattr $C$DW$T$432, DW_AT_decl_column(0x01)
	.dwendtag $C$DW$T$432

	.dwendtag $C$DW$TU$432


$C$DW$TU$433	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$433
$C$DW$T$433	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$433, DW_AT_name("__MMA_C_CONFIG_HWLDTYPE")
	.dwattr $C$DW$T$433, DW_AT_type(*$C$DW$T$432)
	.dwattr $C$DW$T$433, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$T$433, DW_AT_decl_line(0xfc)
	.dwattr $C$DW$T$433, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$433


$C$DW$TU$459	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$459

$C$DW$T$459	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$459, DW_AT_name("__MMA_C_CONFIG_HWLDTYPE")
	.dwattr $C$DW$T$459, DW_AT_byte_size(0x01)
$C$DW$946	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$946, DW_AT_name("__MMA_C_CONFIG_HWLDTYPE_UINT8")
	.dwattr $C$DW$946, DW_AT_const_value(0x00)
	.dwattr $C$DW$946, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$946, DW_AT_decl_line(0xec)
	.dwattr $C$DW$946, DW_AT_decl_column(0x05)

$C$DW$947	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$947, DW_AT_name("__MMA_C_CONFIG_HWLDTYPE_UINT16")
	.dwattr $C$DW$947, DW_AT_const_value(0x01)
	.dwattr $C$DW$947, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$947, DW_AT_decl_line(0xed)
	.dwattr $C$DW$947, DW_AT_decl_column(0x05)

$C$DW$948	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$948, DW_AT_name("__MMA_C_CONFIG_HWLDTYPE_UINT32")
	.dwattr $C$DW$948, DW_AT_const_value(0x03)
	.dwattr $C$DW$948, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$948, DW_AT_decl_line(0xef)
	.dwattr $C$DW$948, DW_AT_decl_column(0x05)

$C$DW$949	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$949, DW_AT_name("__MMA_C_CONFIG_HWLDTYPE_INT8")
	.dwattr $C$DW$949, DW_AT_const_value(0x08)
	.dwattr $C$DW$949, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$949, DW_AT_decl_line(0xf4)
	.dwattr $C$DW$949, DW_AT_decl_column(0x05)

$C$DW$950	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$950, DW_AT_name("__MMA_C_CONFIG_HWLDTYPE_INT16")
	.dwattr $C$DW$950, DW_AT_const_value(0x09)
	.dwattr $C$DW$950, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$950, DW_AT_decl_line(0xf5)
	.dwattr $C$DW$950, DW_AT_decl_column(0x05)

$C$DW$951	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$951, DW_AT_name("__MMA_C_CONFIG_HWLDTYPE_INT32")
	.dwattr $C$DW$951, DW_AT_const_value(0x0b)
	.dwattr $C$DW$951, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$951, DW_AT_decl_line(0xf7)
	.dwattr $C$DW$951, DW_AT_decl_column(0x05)

	.dwattr $C$DW$T$459, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$T$459, DW_AT_decl_line(0xeb)
	.dwattr $C$DW$T$459, DW_AT_decl_column(0x01)
	.dwendtag $C$DW$T$459

	.dwendtag $C$DW$TU$459


$C$DW$TU$460	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$460
$C$DW$T$460	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$460, DW_AT_name("__MMA_C_CONFIG_HWLDTYPE")
	.dwattr $C$DW$T$460, DW_AT_type(*$C$DW$T$459)
	.dwattr $C$DW$T$460, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$T$460, DW_AT_decl_line(0xfc)
	.dwattr $C$DW$T$460, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$460


$C$DW$TU$428	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$428

$C$DW$T$428	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$428, DW_AT_name("__MMA_C_CONFIG_OPERATION")
	.dwattr $C$DW$T$428, DW_AT_byte_size(0x01)
$C$DW$952	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$952, DW_AT_name("__MMA_C_CONFIG_MUL")
	.dwattr $C$DW$952, DW_AT_const_value(0x00)
	.dwattr $C$DW$952, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$952, DW_AT_decl_line(0xd6)
	.dwattr $C$DW$952, DW_AT_decl_column(0x05)

$C$DW$953	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$953, DW_AT_name("__MMA_C_CONFIG_MULNEGATE")
	.dwattr $C$DW$953, DW_AT_const_value(0x01)
	.dwattr $C$DW$953, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$953, DW_AT_decl_line(0xd7)
	.dwattr $C$DW$953, DW_AT_decl_column(0x05)

$C$DW$954	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$954, DW_AT_name("__MMA_C_CONFIG_MULMINUS")
	.dwattr $C$DW$954, DW_AT_const_value(0x02)
	.dwattr $C$DW$954, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$954, DW_AT_decl_line(0xd8)
	.dwattr $C$DW$954, DW_AT_decl_column(0x05)

$C$DW$955	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$955, DW_AT_name("__MMA_C_CONFIG_MULPLUS")
	.dwattr $C$DW$955, DW_AT_const_value(0x03)
	.dwattr $C$DW$955, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$955, DW_AT_decl_line(0xd9)
	.dwattr $C$DW$955, DW_AT_decl_column(0x05)

	.dwattr $C$DW$T$428, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$T$428, DW_AT_decl_line(0xd5)
	.dwattr $C$DW$T$428, DW_AT_decl_column(0x01)
	.dwendtag $C$DW$T$428

	.dwendtag $C$DW$TU$428


$C$DW$TU$429	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$429
$C$DW$T$429	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$429, DW_AT_name("__MMA_C_CONFIG_OPERATION")
	.dwattr $C$DW$T$429, DW_AT_type(*$C$DW$T$428)
	.dwattr $C$DW$T$429, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$T$429, DW_AT_decl_line(0xda)
	.dwattr $C$DW$T$429, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$429


$C$DW$TU$455	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$455

$C$DW$T$455	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$455, DW_AT_name("__MMA_C_CONFIG_OPERATION")
	.dwattr $C$DW$T$455, DW_AT_byte_size(0x01)
$C$DW$956	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$956, DW_AT_name("__MMA_C_CONFIG_MUL")
	.dwattr $C$DW$956, DW_AT_const_value(0x00)
	.dwattr $C$DW$956, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$956, DW_AT_decl_line(0xd6)
	.dwattr $C$DW$956, DW_AT_decl_column(0x05)

$C$DW$957	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$957, DW_AT_name("__MMA_C_CONFIG_MULNEGATE")
	.dwattr $C$DW$957, DW_AT_const_value(0x01)
	.dwattr $C$DW$957, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$957, DW_AT_decl_line(0xd7)
	.dwattr $C$DW$957, DW_AT_decl_column(0x05)

$C$DW$958	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$958, DW_AT_name("__MMA_C_CONFIG_MULMINUS")
	.dwattr $C$DW$958, DW_AT_const_value(0x02)
	.dwattr $C$DW$958, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$958, DW_AT_decl_line(0xd8)
	.dwattr $C$DW$958, DW_AT_decl_column(0x05)

$C$DW$959	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$959, DW_AT_name("__MMA_C_CONFIG_MULPLUS")
	.dwattr $C$DW$959, DW_AT_const_value(0x03)
	.dwattr $C$DW$959, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$959, DW_AT_decl_line(0xd9)
	.dwattr $C$DW$959, DW_AT_decl_column(0x05)

	.dwattr $C$DW$T$455, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$T$455, DW_AT_decl_line(0xd5)
	.dwattr $C$DW$T$455, DW_AT_decl_column(0x01)
	.dwendtag $C$DW$T$455

	.dwendtag $C$DW$TU$455


$C$DW$TU$456	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$456
$C$DW$T$456	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$456, DW_AT_name("__MMA_C_CONFIG_OPERATION")
	.dwattr $C$DW$T$456, DW_AT_type(*$C$DW$T$455)
	.dwattr $C$DW$T$456, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$T$456, DW_AT_decl_line(0xda)
	.dwattr $C$DW$T$456, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$456


$C$DW$TU$434	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$434

$C$DW$T$434	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$434, DW_AT_name("__MMA_C_CONFIG_OPSTART")
	.dwattr $C$DW$T$434, DW_AT_byte_size(0x01)
$C$DW$960	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$960, DW_AT_name("__MMA_C_CONFIG_OPSTART_OPERATION0")
	.dwattr $C$DW$960, DW_AT_const_value(0x00)
	.dwattr $C$DW$960, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$960, DW_AT_decl_line(0x103)
	.dwattr $C$DW$960, DW_AT_decl_column(0x05)

$C$DW$961	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$961, DW_AT_name("__MMA_C_CONFIG_OPSTART_OPERATION1")
	.dwattr $C$DW$961, DW_AT_const_value(0x01)
	.dwattr $C$DW$961, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$961, DW_AT_decl_line(0x104)
	.dwattr $C$DW$961, DW_AT_decl_column(0x05)

	.dwattr $C$DW$T$434, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$T$434, DW_AT_decl_line(0x102)
	.dwattr $C$DW$T$434, DW_AT_decl_column(0x01)
	.dwendtag $C$DW$T$434

	.dwendtag $C$DW$TU$434


$C$DW$TU$435	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$435
$C$DW$T$435	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$435, DW_AT_name("__MMA_C_CONFIG_OPSTART")
	.dwattr $C$DW$T$435, DW_AT_type(*$C$DW$T$434)
	.dwattr $C$DW$T$435, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$T$435, DW_AT_decl_line(0x105)
	.dwattr $C$DW$T$435, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$435


$C$DW$TU$461	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$461

$C$DW$T$461	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$461, DW_AT_name("__MMA_C_CONFIG_OPSTART")
	.dwattr $C$DW$T$461, DW_AT_byte_size(0x01)
$C$DW$962	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$962, DW_AT_name("__MMA_C_CONFIG_OPSTART_OPERATION0")
	.dwattr $C$DW$962, DW_AT_const_value(0x00)
	.dwattr $C$DW$962, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$962, DW_AT_decl_line(0x103)
	.dwattr $C$DW$962, DW_AT_decl_column(0x05)

$C$DW$963	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$963, DW_AT_name("__MMA_C_CONFIG_OPSTART_OPERATION1")
	.dwattr $C$DW$963, DW_AT_const_value(0x01)
	.dwattr $C$DW$963, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$963, DW_AT_decl_line(0x104)
	.dwattr $C$DW$963, DW_AT_decl_column(0x05)

	.dwattr $C$DW$T$461, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$T$461, DW_AT_decl_line(0x102)
	.dwattr $C$DW$T$461, DW_AT_decl_column(0x01)
	.dwendtag $C$DW$T$461

	.dwendtag $C$DW$TU$461


$C$DW$TU$462	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$462
$C$DW$T$462	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$462, DW_AT_name("__MMA_C_CONFIG_OPSTART")
	.dwattr $C$DW$T$462, DW_AT_type(*$C$DW$T$461)
	.dwattr $C$DW$T$462, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$T$462, DW_AT_decl_line(0x105)
	.dwattr $C$DW$T$462, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$462


$C$DW$TU$440	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$440

$C$DW$T$440	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$440, DW_AT_name("__MMA_PARITYCTRL")
	.dwattr $C$DW$T$440, DW_AT_byte_size(0x01)
$C$DW$964	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$964, DW_AT_name("__MMA_NORMAL")
	.dwattr $C$DW$964, DW_AT_const_value(0x00)
	.dwattr $C$DW$964, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$964, DW_AT_decl_line(0x80)
	.dwattr $C$DW$964, DW_AT_decl_column(0x05)

$C$DW$965	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$965, DW_AT_name("__MMA_PNCM_CK")
	.dwattr $C$DW$965, DW_AT_const_value(0x01)
	.dwattr $C$DW$965, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$965, DW_AT_decl_line(0x81)
	.dwattr $C$DW$965, DW_AT_decl_column(0x05)

$C$DW$966	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$966, DW_AT_name("__MMA_PCM_NCK")
	.dwattr $C$DW$966, DW_AT_const_value(0x02)
	.dwattr $C$DW$966, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$966, DW_AT_decl_line(0x82)
	.dwattr $C$DW$966, DW_AT_decl_column(0x05)

$C$DW$967	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$967, DW_AT_name("__MMA_PNCM_NCK")
	.dwattr $C$DW$967, DW_AT_const_value(0x03)
	.dwattr $C$DW$967, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$967, DW_AT_decl_line(0x83)
	.dwattr $C$DW$967, DW_AT_decl_column(0x05)

	.dwattr $C$DW$T$440, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$T$440, DW_AT_decl_line(0x7f)
	.dwattr $C$DW$T$440, DW_AT_decl_column(0x01)
	.dwendtag $C$DW$T$440

	.dwendtag $C$DW$TU$440


$C$DW$TU$441	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$441
$C$DW$T$441	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$441, DW_AT_name("__MMA_PARITYCTRL")
	.dwattr $C$DW$T$441, DW_AT_type(*$C$DW$T$440)
	.dwattr $C$DW$T$441, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$T$441, DW_AT_decl_line(0x84)
	.dwattr $C$DW$T$441, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$441


$C$DW$TU$467	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$467

$C$DW$T$467	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$467, DW_AT_name("__MMA_PARITYCTRL")
	.dwattr $C$DW$T$467, DW_AT_byte_size(0x01)
$C$DW$968	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$968, DW_AT_name("__MMA_NORMAL")
	.dwattr $C$DW$968, DW_AT_const_value(0x00)
	.dwattr $C$DW$968, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$968, DW_AT_decl_line(0x80)
	.dwattr $C$DW$968, DW_AT_decl_column(0x05)

$C$DW$969	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$969, DW_AT_name("__MMA_PNCM_CK")
	.dwattr $C$DW$969, DW_AT_const_value(0x01)
	.dwattr $C$DW$969, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$969, DW_AT_decl_line(0x81)
	.dwattr $C$DW$969, DW_AT_decl_column(0x05)

$C$DW$970	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$970, DW_AT_name("__MMA_PCM_NCK")
	.dwattr $C$DW$970, DW_AT_const_value(0x02)
	.dwattr $C$DW$970, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$970, DW_AT_decl_line(0x82)
	.dwattr $C$DW$970, DW_AT_decl_column(0x05)

$C$DW$971	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$971, DW_AT_name("__MMA_PNCM_NCK")
	.dwattr $C$DW$971, DW_AT_const_value(0x03)
	.dwattr $C$DW$971, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$971, DW_AT_decl_line(0x83)
	.dwattr $C$DW$971, DW_AT_decl_column(0x05)

	.dwattr $C$DW$T$467, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$T$467, DW_AT_decl_line(0x7f)
	.dwattr $C$DW$T$467, DW_AT_decl_column(0x01)
	.dwendtag $C$DW$T$467

	.dwendtag $C$DW$TU$467


$C$DW$TU$468	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$468
$C$DW$T$468	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$468, DW_AT_name("__MMA_PARITYCTRL")
	.dwattr $C$DW$T$468, DW_AT_type(*$C$DW$T$467)
	.dwattr $C$DW$T$468, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$T$468, DW_AT_decl_line(0x84)
	.dwattr $C$DW$T$468, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$468


$C$DW$TU$438	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$438

$C$DW$T$438	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$438, DW_AT_name("__MMA_X_CONFIG_CTYPE")
	.dwattr $C$DW$T$438, DW_AT_byte_size(0x01)
$C$DW$972	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$972, DW_AT_name("__MMA_X_CONFIG_CTYPE_UINT32")
	.dwattr $C$DW$972, DW_AT_const_value(0x00)
	.dwattr $C$DW$972, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$972, DW_AT_decl_line(0x128)
	.dwattr $C$DW$972, DW_AT_decl_column(0x05)

$C$DW$973	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$973, DW_AT_name("__MMA_X_CONFIG_CTYPE_UINT64")
	.dwattr $C$DW$973, DW_AT_const_value(0x01)
	.dwattr $C$DW$973, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$973, DW_AT_decl_line(0x129)
	.dwattr $C$DW$973, DW_AT_decl_column(0x05)

$C$DW$974	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$974, DW_AT_name("__MMA_X_CONFIG_CTYPE_UINT128")
	.dwattr $C$DW$974, DW_AT_const_value(0x03)
	.dwattr $C$DW$974, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$974, DW_AT_decl_line(0x12b)
	.dwattr $C$DW$974, DW_AT_decl_column(0x05)

$C$DW$975	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$975, DW_AT_name("__MMA_X_CONFIG_CTYPE_INT32")
	.dwattr $C$DW$975, DW_AT_const_value(0x04)
	.dwattr $C$DW$975, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$975, DW_AT_decl_line(0x12c)
	.dwattr $C$DW$975, DW_AT_decl_column(0x05)

$C$DW$976	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$976, DW_AT_name("__MMA_X_CONFIG_CTYPE_INT64")
	.dwattr $C$DW$976, DW_AT_const_value(0x05)
	.dwattr $C$DW$976, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$976, DW_AT_decl_line(0x12d)
	.dwattr $C$DW$976, DW_AT_decl_column(0x05)

$C$DW$977	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$977, DW_AT_name("__MMA_X_CONFIG_CTYPE_INT128")
	.dwattr $C$DW$977, DW_AT_const_value(0x07)
	.dwattr $C$DW$977, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$977, DW_AT_decl_line(0x12f)
	.dwattr $C$DW$977, DW_AT_decl_column(0x05)

	.dwattr $C$DW$T$438, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$T$438, DW_AT_decl_line(0x127)
	.dwattr $C$DW$T$438, DW_AT_decl_column(0x01)
	.dwendtag $C$DW$T$438

	.dwendtag $C$DW$TU$438


$C$DW$TU$439	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$439
$C$DW$T$439	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$439, DW_AT_name("__MMA_X_CONFIG_CTYPE")
	.dwattr $C$DW$T$439, DW_AT_type(*$C$DW$T$438)
	.dwattr $C$DW$T$439, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$T$439, DW_AT_decl_line(0x130)
	.dwattr $C$DW$T$439, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$439


$C$DW$TU$465	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$465

$C$DW$T$465	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$465, DW_AT_name("__MMA_X_CONFIG_CTYPE")
	.dwattr $C$DW$T$465, DW_AT_byte_size(0x01)
$C$DW$978	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$978, DW_AT_name("__MMA_X_CONFIG_CTYPE_UINT32")
	.dwattr $C$DW$978, DW_AT_const_value(0x00)
	.dwattr $C$DW$978, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$978, DW_AT_decl_line(0x128)
	.dwattr $C$DW$978, DW_AT_decl_column(0x05)

$C$DW$979	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$979, DW_AT_name("__MMA_X_CONFIG_CTYPE_UINT64")
	.dwattr $C$DW$979, DW_AT_const_value(0x01)
	.dwattr $C$DW$979, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$979, DW_AT_decl_line(0x129)
	.dwattr $C$DW$979, DW_AT_decl_column(0x05)

$C$DW$980	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$980, DW_AT_name("__MMA_X_CONFIG_CTYPE_UINT128")
	.dwattr $C$DW$980, DW_AT_const_value(0x03)
	.dwattr $C$DW$980, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$980, DW_AT_decl_line(0x12b)
	.dwattr $C$DW$980, DW_AT_decl_column(0x05)

$C$DW$981	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$981, DW_AT_name("__MMA_X_CONFIG_CTYPE_INT32")
	.dwattr $C$DW$981, DW_AT_const_value(0x04)
	.dwattr $C$DW$981, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$981, DW_AT_decl_line(0x12c)
	.dwattr $C$DW$981, DW_AT_decl_column(0x05)

$C$DW$982	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$982, DW_AT_name("__MMA_X_CONFIG_CTYPE_INT64")
	.dwattr $C$DW$982, DW_AT_const_value(0x05)
	.dwattr $C$DW$982, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$982, DW_AT_decl_line(0x12d)
	.dwattr $C$DW$982, DW_AT_decl_column(0x05)

$C$DW$983	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$983, DW_AT_name("__MMA_X_CONFIG_CTYPE_INT128")
	.dwattr $C$DW$983, DW_AT_const_value(0x07)
	.dwattr $C$DW$983, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$983, DW_AT_decl_line(0x12f)
	.dwattr $C$DW$983, DW_AT_decl_column(0x05)

	.dwattr $C$DW$T$465, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$T$465, DW_AT_decl_line(0x127)
	.dwattr $C$DW$T$465, DW_AT_decl_column(0x01)
	.dwendtag $C$DW$T$465

	.dwendtag $C$DW$TU$465


$C$DW$TU$466	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$466
$C$DW$T$466	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$466, DW_AT_name("__MMA_X_CONFIG_CTYPE")
	.dwattr $C$DW$T$466, DW_AT_type(*$C$DW$T$465)
	.dwattr $C$DW$T$466, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$T$466, DW_AT_decl_line(0x130)
	.dwattr $C$DW$T$466, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$466


$C$DW$TU$436	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$436

$C$DW$T$436	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$436, DW_AT_name("__MMA_X_CONFIG_XTYPE")
	.dwattr $C$DW$T$436, DW_AT_byte_size(0x01)
$C$DW$984	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$984, DW_AT_name("__MMA_X_CONFIG_XTYPE_UINT8")
	.dwattr $C$DW$984, DW_AT_const_value(0x00)
	.dwattr $C$DW$984, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$984, DW_AT_decl_line(0x111)
	.dwattr $C$DW$984, DW_AT_decl_column(0x05)

$C$DW$985	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$985, DW_AT_name("__MMA_X_CONFIG_XTYPE_UINT16")
	.dwattr $C$DW$985, DW_AT_const_value(0x01)
	.dwattr $C$DW$985, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$985, DW_AT_decl_line(0x112)
	.dwattr $C$DW$985, DW_AT_decl_column(0x05)

$C$DW$986	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$986, DW_AT_name("__MMA_X_CONFIG_XTYPE_UINT32")
	.dwattr $C$DW$986, DW_AT_const_value(0x03)
	.dwattr $C$DW$986, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$986, DW_AT_decl_line(0x114)
	.dwattr $C$DW$986, DW_AT_decl_column(0x05)

$C$DW$987	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$987, DW_AT_name("__MMA_X_CONFIG_XTYPE_UINT64")
	.dwattr $C$DW$987, DW_AT_const_value(0x04)
	.dwattr $C$DW$987, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$987, DW_AT_decl_line(0x115)
	.dwattr $C$DW$987, DW_AT_decl_column(0x05)

$C$DW$988	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$988, DW_AT_name("__MMA_X_CONFIG_XTYPE_UINT128")
	.dwattr $C$DW$988, DW_AT_const_value(0x05)
	.dwattr $C$DW$988, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$988, DW_AT_decl_line(0x116)
	.dwattr $C$DW$988, DW_AT_decl_column(0x05)

$C$DW$989	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$989, DW_AT_name("__MMA_X_CONFIG_XTYPE_INT8")
	.dwattr $C$DW$989, DW_AT_const_value(0x08)
	.dwattr $C$DW$989, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$989, DW_AT_decl_line(0x119)
	.dwattr $C$DW$989, DW_AT_decl_column(0x05)

$C$DW$990	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$990, DW_AT_name("__MMA_X_CONFIG_XTYPE_INT16")
	.dwattr $C$DW$990, DW_AT_const_value(0x09)
	.dwattr $C$DW$990, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$990, DW_AT_decl_line(0x11a)
	.dwattr $C$DW$990, DW_AT_decl_column(0x05)

$C$DW$991	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$991, DW_AT_name("__MMA_X_CONFIG_XTYPE_INT32")
	.dwattr $C$DW$991, DW_AT_const_value(0x0b)
	.dwattr $C$DW$991, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$991, DW_AT_decl_line(0x11c)
	.dwattr $C$DW$991, DW_AT_decl_column(0x05)

$C$DW$992	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$992, DW_AT_name("__MMA_X_CONFIG_XTYPE_INT64")
	.dwattr $C$DW$992, DW_AT_const_value(0x0c)
	.dwattr $C$DW$992, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$992, DW_AT_decl_line(0x11d)
	.dwattr $C$DW$992, DW_AT_decl_column(0x05)

$C$DW$993	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$993, DW_AT_name("__MMA_X_CONFIG_XTYPE_INT128")
	.dwattr $C$DW$993, DW_AT_const_value(0x0d)
	.dwattr $C$DW$993, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$993, DW_AT_decl_line(0x11e)
	.dwattr $C$DW$993, DW_AT_decl_column(0x05)

	.dwattr $C$DW$T$436, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$T$436, DW_AT_decl_line(0x110)
	.dwattr $C$DW$T$436, DW_AT_decl_column(0x01)
	.dwendtag $C$DW$T$436

	.dwendtag $C$DW$TU$436


$C$DW$TU$437	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$437
$C$DW$T$437	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$437, DW_AT_name("__MMA_X_CONFIG_XTYPE")
	.dwattr $C$DW$T$437, DW_AT_type(*$C$DW$T$436)
	.dwattr $C$DW$T$437, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$T$437, DW_AT_decl_line(0x121)
	.dwattr $C$DW$T$437, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$437


$C$DW$TU$463	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$463

$C$DW$T$463	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$463, DW_AT_name("__MMA_X_CONFIG_XTYPE")
	.dwattr $C$DW$T$463, DW_AT_byte_size(0x01)
$C$DW$994	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$994, DW_AT_name("__MMA_X_CONFIG_XTYPE_UINT8")
	.dwattr $C$DW$994, DW_AT_const_value(0x00)
	.dwattr $C$DW$994, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$994, DW_AT_decl_line(0x111)
	.dwattr $C$DW$994, DW_AT_decl_column(0x05)

$C$DW$995	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$995, DW_AT_name("__MMA_X_CONFIG_XTYPE_UINT16")
	.dwattr $C$DW$995, DW_AT_const_value(0x01)
	.dwattr $C$DW$995, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$995, DW_AT_decl_line(0x112)
	.dwattr $C$DW$995, DW_AT_decl_column(0x05)

$C$DW$996	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$996, DW_AT_name("__MMA_X_CONFIG_XTYPE_UINT32")
	.dwattr $C$DW$996, DW_AT_const_value(0x03)
	.dwattr $C$DW$996, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$996, DW_AT_decl_line(0x114)
	.dwattr $C$DW$996, DW_AT_decl_column(0x05)

$C$DW$997	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$997, DW_AT_name("__MMA_X_CONFIG_XTYPE_UINT64")
	.dwattr $C$DW$997, DW_AT_const_value(0x04)
	.dwattr $C$DW$997, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$997, DW_AT_decl_line(0x115)
	.dwattr $C$DW$997, DW_AT_decl_column(0x05)

$C$DW$998	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$998, DW_AT_name("__MMA_X_CONFIG_XTYPE_UINT128")
	.dwattr $C$DW$998, DW_AT_const_value(0x05)
	.dwattr $C$DW$998, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$998, DW_AT_decl_line(0x116)
	.dwattr $C$DW$998, DW_AT_decl_column(0x05)

$C$DW$999	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$999, DW_AT_name("__MMA_X_CONFIG_XTYPE_INT8")
	.dwattr $C$DW$999, DW_AT_const_value(0x08)
	.dwattr $C$DW$999, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$999, DW_AT_decl_line(0x119)
	.dwattr $C$DW$999, DW_AT_decl_column(0x05)

$C$DW$1000	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$1000, DW_AT_name("__MMA_X_CONFIG_XTYPE_INT16")
	.dwattr $C$DW$1000, DW_AT_const_value(0x09)
	.dwattr $C$DW$1000, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$1000, DW_AT_decl_line(0x11a)
	.dwattr $C$DW$1000, DW_AT_decl_column(0x05)

$C$DW$1001	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$1001, DW_AT_name("__MMA_X_CONFIG_XTYPE_INT32")
	.dwattr $C$DW$1001, DW_AT_const_value(0x0b)
	.dwattr $C$DW$1001, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$1001, DW_AT_decl_line(0x11c)
	.dwattr $C$DW$1001, DW_AT_decl_column(0x05)

$C$DW$1002	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$1002, DW_AT_name("__MMA_X_CONFIG_XTYPE_INT64")
	.dwattr $C$DW$1002, DW_AT_const_value(0x0c)
	.dwattr $C$DW$1002, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$1002, DW_AT_decl_line(0x11d)
	.dwattr $C$DW$1002, DW_AT_decl_column(0x05)

$C$DW$1003	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$1003, DW_AT_name("__MMA_X_CONFIG_XTYPE_INT128")
	.dwattr $C$DW$1003, DW_AT_const_value(0x0d)
	.dwattr $C$DW$1003, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$1003, DW_AT_decl_line(0x11e)
	.dwattr $C$DW$1003, DW_AT_decl_column(0x05)

	.dwattr $C$DW$T$463, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$T$463, DW_AT_decl_line(0x110)
	.dwattr $C$DW$T$463, DW_AT_decl_column(0x01)
	.dwendtag $C$DW$T$463

	.dwendtag $C$DW$TU$463


$C$DW$TU$464	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$464
$C$DW$T$464	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$464, DW_AT_name("__MMA_X_CONFIG_XTYPE")
	.dwattr $C$DW$T$464, DW_AT_type(*$C$DW$T$463)
	.dwattr $C$DW$T$464, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$T$464, DW_AT_decl_line(0x121)
	.dwattr $C$DW$T$464, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$464


$C$DW$TU$2	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$2
$C$DW$T$2	.dwtag  DW_TAG_unspecified_type
	.dwattr $C$DW$T$2, DW_AT_name("void")

	.dwendtag $C$DW$TU$2


$C$DW$TU$3	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$3
$C$DW$T$3	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$3, DW_AT_type(*$C$DW$T$2)
	.dwattr $C$DW$T$3, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$3


$C$DW$TU$321	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$321
$C$DW$T$321	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$321, DW_AT_name("TIDL_ExecInArgs")
	.dwattr $C$DW$T$321, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$T$321, DW_AT_decl_file("./inc/tidl_generic_datatypes.h")
	.dwattr $C$DW$T$321, DW_AT_decl_line(0x48)
	.dwattr $C$DW$T$321, DW_AT_decl_column(0x0f)

	.dwendtag $C$DW$TU$321


$C$DW$TU$322	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$322
$C$DW$T$322	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$322, DW_AT_name("TIDL_ExecOutArgs")
	.dwattr $C$DW$T$322, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$T$322, DW_AT_decl_file("./inc/tidl_generic_datatypes.h")
	.dwattr $C$DW$T$322, DW_AT_decl_line(0x4a)
	.dwattr $C$DW$T$322, DW_AT_decl_column(0x0f)

	.dwendtag $C$DW$TU$322


$C$DW$TU$323	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$323
$C$DW$T$323	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$323, DW_AT_name("TIDL_KernelHandle")
	.dwattr $C$DW$T$323, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$T$323, DW_AT_decl_file("./inc/tidl_types.h")
	.dwattr $C$DW$T$323, DW_AT_decl_line(0x8c)
	.dwattr $C$DW$T$323, DW_AT_decl_column(0x0f)

	.dwendtag $C$DW$TU$323


$C$DW$TU$284	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$284
$C$DW$T$284	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$284, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$T$284, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$284


$C$DW$TU$358	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$358

$C$DW$T$358	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$358, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$T$358, DW_AT_byte_size(0x2000)
$C$DW$1004	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$1004, DW_AT_upper_bound(0x3ff)

	.dwendtag $C$DW$T$358

	.dwendtag $C$DW$TU$358


$C$DW$TU$582	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$582

$C$DW$T$582	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$582, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$T$582, DW_AT_byte_size(0x18)
$C$DW$1005	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$1005, DW_AT_upper_bound(0x02)

	.dwendtag $C$DW$T$582

	.dwendtag $C$DW$TU$582


$C$DW$TU$594	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$594

$C$DW$T$594	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$594, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$T$594, DW_AT_byte_size(0x80)
$C$DW$1006	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$1006, DW_AT_upper_bound(0x0f)

	.dwendtag $C$DW$T$594

	.dwendtag $C$DW$TU$594


$C$DW$TU$699	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$699

$C$DW$T$699	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$699, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$T$699, DW_AT_byte_size(0x40)
$C$DW$1007	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$1007, DW_AT_upper_bound(0x07)

	.dwendtag $C$DW$T$699

	.dwendtag $C$DW$TU$699


$C$DW$TU$700	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$700

$C$DW$T$700	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$700, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$T$700, DW_AT_byte_size(0xc0)
$C$DW$1008	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$1008, DW_AT_upper_bound(0x02)

$C$DW$1009	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$1009, DW_AT_upper_bound(0x07)

	.dwendtag $C$DW$T$700

	.dwendtag $C$DW$TU$700


$C$DW$TU$36	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$36

$C$DW$T$36	.dwtag  DW_TAG_subroutine_type
$C$DW$1010	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1010, DW_AT_type(*$C$DW$T$35)

	.dwendtag $C$DW$T$36

	.dwendtag $C$DW$TU$36


$C$DW$TU$37	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$37
$C$DW$T$37	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$37, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$T$37, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$37


$C$DW$TU$62	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$62

$C$DW$T$62	.dwtag  DW_TAG_subroutine_type
$C$DW$1011	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1011, DW_AT_type(*$C$DW$T$35)

$C$DW$1012	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1012, DW_AT_type(*$C$DW$T$59)

$C$DW$1013	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1013, DW_AT_type(*$C$DW$T$35)

$C$DW$1014	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1014, DW_AT_type(*$C$DW$T$42)

	.dwendtag $C$DW$T$62

	.dwendtag $C$DW$TU$62


$C$DW$TU$63	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$63
$C$DW$T$63	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$63, DW_AT_type(*$C$DW$T$62)
	.dwattr $C$DW$T$63, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$63


$C$DW$TU$324	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$324
$C$DW$T$324	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$324, DW_AT_type(*$C$DW$T$2)

	.dwendtag $C$DW$TU$324


$C$DW$TU$325	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$325
$C$DW$T$325	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$325, DW_AT_type(*$C$DW$T$324)
	.dwattr $C$DW$T$325, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$325


$C$DW$TU$329	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$329

$C$DW$T$329	.dwtag  DW_TAG_subroutine_type
$C$DW$1015	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1015, DW_AT_type(*$C$DW$T$323)

$C$DW$1016	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1016, DW_AT_type(*$C$DW$T$3)

$C$DW$1017	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1017, DW_AT_type(*$C$DW$T$39)

$C$DW$1018	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1018, DW_AT_type(*$C$DW$T$39)

$C$DW$1019	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1019, DW_AT_type(*$C$DW$T$39)

	.dwendtag $C$DW$T$329

	.dwendtag $C$DW$TU$329


$C$DW$TU$330	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$330
$C$DW$T$330	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$330, DW_AT_type(*$C$DW$T$329)
	.dwattr $C$DW$T$330, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$330


$C$DW$TU$331	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$331
$C$DW$T$331	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$331, DW_AT_name("TIDL_genericUpdateInArgsPtr")
	.dwattr $C$DW$T$331, DW_AT_type(*$C$DW$T$330)
	.dwattr $C$DW$T$331, DW_AT_decl_file("./inc/tidl_generic_datatypes.h")
	.dwattr $C$DW$T$331, DW_AT_decl_line(0x52)
	.dwattr $C$DW$T$331, DW_AT_decl_column(0x10)

	.dwendtag $C$DW$TU$331


$C$DW$TU$4	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$4
$C$DW$T$4	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$4, DW_AT_encoding(DW_ATE_boolean)
	.dwattr $C$DW$T$4, DW_AT_name("bool")
	.dwattr $C$DW$T$4, DW_AT_byte_size(0x01)

	.dwendtag $C$DW$TU$4


$C$DW$TU$5	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$5
$C$DW$T$5	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$5, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$5, DW_AT_name("signed char")
	.dwattr $C$DW$T$5, DW_AT_byte_size(0x01)

	.dwendtag $C$DW$TU$5


$C$DW$TU$6	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$6
$C$DW$T$6	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$6, DW_AT_encoding(DW_ATE_unsigned_char)
	.dwattr $C$DW$T$6, DW_AT_name("unsigned char")
	.dwattr $C$DW$T$6, DW_AT_byte_size(0x01)

	.dwendtag $C$DW$TU$6


$C$DW$TU$317	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$317
$C$DW$T$317	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$317, DW_AT_name("__uint8_t")
	.dwattr $C$DW$T$317, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$317, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/machine/_types.h")
	.dwattr $C$DW$T$317, DW_AT_decl_line(0x61)
	.dwattr $C$DW$T$317, DW_AT_decl_column(0x18)

	.dwendtag $C$DW$TU$317


$C$DW$TU$318	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$318
$C$DW$T$318	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$318, DW_AT_name("uint8_t")
	.dwattr $C$DW$T$318, DW_AT_type(*$C$DW$T$317)
	.dwattr $C$DW$T$318, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/sys/_stdint.h")
	.dwattr $C$DW$T$318, DW_AT_decl_line(0x3b)
	.dwattr $C$DW$T$318, DW_AT_decl_column(0x14)

	.dwendtag $C$DW$TU$318


$C$DW$TU$319	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$319

$C$DW$T$319	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$319, DW_AT_type(*$C$DW$T$318)
	.dwattr $C$DW$T$319, DW_AT_byte_size(0x40)
$C$DW$1020	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$1020, DW_AT_upper_bound(0x3f)

	.dwendtag $C$DW$T$319

	.dwendtag $C$DW$TU$319


$C$DW$TU$320	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$320

$C$DW$T$320	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$320, DW_AT_type(*$C$DW$T$318)
	.dwattr $C$DW$T$320, DW_AT_byte_size(0x380)
$C$DW$1021	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$1021, DW_AT_upper_bound(0x0d)

$C$DW$1022	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$1022, DW_AT_upper_bound(0x3f)

	.dwendtag $C$DW$T$320

	.dwendtag $C$DW$TU$320


$C$DW$TU$364	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$364
$C$DW$T$364	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$364, DW_AT_type(*$C$DW$T$318)
	.dwattr $C$DW$T$364, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$364


$C$DW$TU$1012	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$1012
$C$DW$T$1012	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$1012, DW_AT_type(*$C$DW$T$318)

	.dwendtag $C$DW$TU$1012


$C$DW$TU$1013	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$1013
$C$DW$T$1013	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$1013, DW_AT_type(*$C$DW$T$1012)
	.dwattr $C$DW$T$1013, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$1013


$C$DW$TU$7	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$7
$C$DW$T$7	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$7, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$7, DW_AT_name("wchar_t")
	.dwattr $C$DW$T$7, DW_AT_byte_size(0x04)

	.dwendtag $C$DW$TU$7


$C$DW$TU$8	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$8
$C$DW$T$8	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$8, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$8, DW_AT_name("short")
	.dwattr $C$DW$T$8, DW_AT_byte_size(0x02)

	.dwendtag $C$DW$TU$8


$C$DW$TU$9	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$9
$C$DW$T$9	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$9, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$9, DW_AT_name("unsigned short")
	.dwattr $C$DW$T$9, DW_AT_byte_size(0x02)

	.dwendtag $C$DW$TU$9


$C$DW$TU$600	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$600
$C$DW$T$600	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$600, DW_AT_name("__uint16_t")
	.dwattr $C$DW$T$600, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$T$600, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/machine/_types.h")
	.dwattr $C$DW$T$600, DW_AT_decl_line(0x63)
	.dwattr $C$DW$T$600, DW_AT_decl_column(0x19)

	.dwendtag $C$DW$TU$600


$C$DW$TU$601	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$601
$C$DW$T$601	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$601, DW_AT_name("uint16_t")
	.dwattr $C$DW$T$601, DW_AT_type(*$C$DW$T$600)
	.dwattr $C$DW$T$601, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/sys/_stdint.h")
	.dwattr $C$DW$T$601, DW_AT_decl_line(0x41)
	.dwattr $C$DW$T$601, DW_AT_decl_column(0x15)

	.dwendtag $C$DW$TU$601


$C$DW$TU$602	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$602
$C$DW$T$602	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$602, DW_AT_type(*$C$DW$T$601)
	.dwattr $C$DW$T$602, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$602


$C$DW$TU$627	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$627

$C$DW$T$627	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$627, DW_AT_type(*$C$DW$T$601)
	.dwattr $C$DW$T$627, DW_AT_byte_size(0x40)
$C$DW$1023	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$1023, DW_AT_upper_bound(0x1f)

	.dwendtag $C$DW$T$627

	.dwendtag $C$DW$TU$627


$C$DW$TU$1015	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$1015
$C$DW$T$1015	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$1015, DW_AT_type(*$C$DW$T$601)

	.dwendtag $C$DW$TU$1015


$C$DW$TU$1016	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$1016
$C$DW$T$1016	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$1016, DW_AT_type(*$C$DW$T$1015)
	.dwattr $C$DW$T$1016, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$1016


$C$DW$TU$10	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$10
$C$DW$T$10	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$10, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$10, DW_AT_name("int")
	.dwattr $C$DW$T$10, DW_AT_byte_size(0x04)

	.dwendtag $C$DW$TU$10


$C$DW$TU$38	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$38
$C$DW$T$38	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$38, DW_AT_name("__int32_t")
	.dwattr $C$DW$T$38, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$T$38, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/machine/_types.h")
	.dwattr $C$DW$T$38, DW_AT_decl_line(0x64)
	.dwattr $C$DW$T$38, DW_AT_decl_column(0x0f)

	.dwendtag $C$DW$TU$38


$C$DW$TU$39	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$39
$C$DW$T$39	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$39, DW_AT_name("int32_t")
	.dwattr $C$DW$T$39, DW_AT_type(*$C$DW$T$38)
	.dwattr $C$DW$T$39, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/sys/_stdint.h")
	.dwattr $C$DW$T$39, DW_AT_decl_line(0x30)
	.dwattr $C$DW$T$39, DW_AT_decl_column(0x14)

	.dwendtag $C$DW$TU$39


$C$DW$TU$47	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$47

$C$DW$T$47	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$47, DW_AT_type(*$C$DW$T$39)
$C$DW$1024	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1024, DW_AT_type(*$C$DW$T$42)

$C$DW$1025	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1025, DW_AT_type(*$C$DW$T$44)

$C$DW$1026	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1026, DW_AT_type(*$C$DW$T$46)

	.dwendtag $C$DW$T$47

	.dwendtag $C$DW$TU$47


$C$DW$TU$48	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$48
$C$DW$T$48	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$48, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$T$48, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$48


$C$DW$TU$54	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$54

$C$DW$T$54	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$54, DW_AT_type(*$C$DW$T$39)
$C$DW$1027	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1027, DW_AT_type(*$C$DW$T$35)

$C$DW$1028	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1028, DW_AT_type(*$C$DW$T$51)

$C$DW$1029	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1029, DW_AT_type(*$C$DW$T$53)

	.dwendtag $C$DW$T$54

	.dwendtag $C$DW$TU$54


$C$DW$TU$55	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$55
$C$DW$T$55	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$55, DW_AT_type(*$C$DW$T$54)
	.dwattr $C$DW$T$55, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$55


$C$DW$TU$56	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$56

$C$DW$T$56	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$56, DW_AT_type(*$C$DW$T$39)
$C$DW$1030	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1030, DW_AT_type(*$C$DW$T$35)

$C$DW$1031	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1031, DW_AT_type(*$C$DW$T$46)

	.dwendtag $C$DW$T$56

	.dwendtag $C$DW$TU$56


$C$DW$TU$57	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$57
$C$DW$T$57	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$57, DW_AT_type(*$C$DW$T$56)
	.dwattr $C$DW$T$57, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$57


$C$DW$TU$60	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$60

$C$DW$T$60	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$60, DW_AT_type(*$C$DW$T$39)
$C$DW$1032	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1032, DW_AT_type(*$C$DW$T$35)

$C$DW$1033	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1033, DW_AT_type(*$C$DW$T$59)

$C$DW$1034	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1034, DW_AT_type(*$C$DW$T$35)

$C$DW$1035	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1035, DW_AT_type(*$C$DW$T$42)

	.dwendtag $C$DW$T$60

	.dwendtag $C$DW$TU$60


$C$DW$TU$61	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$61
$C$DW$T$61	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$61, DW_AT_type(*$C$DW$T$60)
	.dwattr $C$DW$T$61, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$61


$C$DW$TU$64	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$64

$C$DW$T$64	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$64, DW_AT_type(*$C$DW$T$39)
	.dwendtag $C$DW$T$64

	.dwendtag $C$DW$TU$64


$C$DW$TU$65	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$65
$C$DW$T$65	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$65, DW_AT_type(*$C$DW$T$64)
	.dwattr $C$DW$T$65, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$65


$C$DW$TU$186	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$186

$C$DW$T$186	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$186, DW_AT_type(*$C$DW$T$39)
$C$DW$1036	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1036, DW_AT_type(*$C$DW$T$176)

$C$DW$1037	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1037, DW_AT_type(*$C$DW$T$179)

$C$DW$1038	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1038, DW_AT_type(*$C$DW$T$181)

$C$DW$1039	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1039, DW_AT_type(*$C$DW$T$183)

$C$DW$1040	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1040, DW_AT_type(*$C$DW$T$185)

	.dwendtag $C$DW$T$186

	.dwendtag $C$DW$TU$186


$C$DW$TU$187	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$187
$C$DW$T$187	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$187, DW_AT_type(*$C$DW$T$186)
	.dwattr $C$DW$T$187, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$187


$C$DW$TU$189	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$189

$C$DW$T$189	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$189, DW_AT_type(*$C$DW$T$39)
$C$DW$1041	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1041, DW_AT_type(*$C$DW$T$176)

$C$DW$1042	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1042, DW_AT_type(*$C$DW$T$51)

$C$DW$1043	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1043, DW_AT_type(*$C$DW$T$42)

$C$DW$1044	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1044, DW_AT_type(*$C$DW$T$188)

	.dwendtag $C$DW$T$189

	.dwendtag $C$DW$TU$189


$C$DW$TU$190	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$190
$C$DW$T$190	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$190, DW_AT_type(*$C$DW$T$189)
	.dwattr $C$DW$T$190, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$190


$C$DW$TU$231	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$231

$C$DW$T$231	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$231, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$T$231, DW_AT_byte_size(0x78)
$C$DW$1045	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$1045, DW_AT_upper_bound(0x1d)

	.dwendtag $C$DW$T$231

	.dwendtag $C$DW$TU$231


$C$DW$TU$240	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$240

$C$DW$T$240	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$240, DW_AT_type(*$C$DW$T$39)
$C$DW$1046	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1046, DW_AT_type(*$C$DW$T$3)

$C$DW$1047	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1047, DW_AT_type(*$C$DW$T$50)

	.dwendtag $C$DW$T$240

	.dwendtag $C$DW$TU$240


$C$DW$TU$241	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$241
$C$DW$T$241	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$241, DW_AT_type(*$C$DW$T$240)
	.dwattr $C$DW$T$241, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$241


$C$DW$TU$242	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$242
$C$DW$T$242	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$242, DW_AT_name("ivisionCacheWriteBack")
	.dwattr $C$DW$T$242, DW_AT_type(*$C$DW$T$241)
	.dwattr $C$DW$T$242, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ivision/ivision.h")
	.dwattr $C$DW$T$242, DW_AT_decl_line(0x70)
	.dwattr $C$DW$T$242, DW_AT_decl_column(0x13)

	.dwendtag $C$DW$TU$242


$C$DW$TU$278	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$278

$C$DW$T$278	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$278, DW_AT_type(*$C$DW$T$39)
$C$DW$1048	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1048, DW_AT_type(*$C$DW$T$274)

$C$DW$1049	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1049, DW_AT_type(*$C$DW$T$277)

	.dwendtag $C$DW$T$278

	.dwendtag $C$DW$TU$278


$C$DW$TU$279	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$279
$C$DW$T$279	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$279, DW_AT_type(*$C$DW$T$278)
	.dwattr $C$DW$T$279, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$279


$C$DW$TU$280	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$280

$C$DW$T$280	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$280, DW_AT_type(*$C$DW$T$39)
$C$DW$1050	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1050, DW_AT_type(*$C$DW$T$274)

$C$DW$1051	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1051, DW_AT_type(*$C$DW$T$3)

$C$DW$1052	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1052, DW_AT_type(*$C$DW$T$39)

	.dwendtag $C$DW$T$280

	.dwendtag $C$DW$TU$280


$C$DW$TU$281	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$281
$C$DW$T$281	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$281, DW_AT_type(*$C$DW$T$280)
	.dwattr $C$DW$T$281, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$281


$C$DW$TU$288	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$288

$C$DW$T$288	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$288, DW_AT_type(*$C$DW$T$39)
$C$DW$1053	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1053, DW_AT_type(*$C$DW$T$3)

$C$DW$1054	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1054, DW_AT_type(*$C$DW$T$283)

$C$DW$1055	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1055, DW_AT_type(*$C$DW$T$284)

$C$DW$1056	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1056, DW_AT_type(*$C$DW$T$284)

$C$DW$1057	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1057, DW_AT_type(*$C$DW$T$3)

$C$DW$1058	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1058, DW_AT_type(*$C$DW$T$3)

$C$DW$1059	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1059, DW_AT_type(*$C$DW$T$287)

$C$DW$1060	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1060, DW_AT_type(*$C$DW$T$39)

$C$DW$1061	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1061, DW_AT_type(*$C$DW$T$16)

	.dwendtag $C$DW$T$288

	.dwendtag $C$DW$TU$288


$C$DW$TU$289	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$289
$C$DW$T$289	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$289, DW_AT_type(*$C$DW$T$288)
	.dwattr $C$DW$T$289, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$289


$C$DW$TU$313	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$313

$C$DW$T$313	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$313, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$T$313, DW_AT_byte_size(0x38)
$C$DW$1062	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$1062, DW_AT_upper_bound(0x0d)

	.dwendtag $C$DW$T$313

	.dwendtag $C$DW$TU$313


$C$DW$TU$326	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$326

$C$DW$T$326	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$326, DW_AT_type(*$C$DW$T$39)
$C$DW$1063	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1063, DW_AT_type(*$C$DW$T$323)

$C$DW$1064	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1064, DW_AT_type(*$C$DW$T$325)

$C$DW$1065	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1065, DW_AT_type(*$C$DW$T$3)

$C$DW$1066	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1066, DW_AT_type(*$C$DW$T$325)

$C$DW$1067	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1067, DW_AT_type(*$C$DW$T$3)

	.dwendtag $C$DW$T$326

	.dwendtag $C$DW$TU$326


$C$DW$TU$327	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$327
$C$DW$T$327	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$327, DW_AT_type(*$C$DW$T$326)
	.dwattr $C$DW$T$327, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$327


$C$DW$TU$328	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$328
$C$DW$T$328	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$328, DW_AT_name("TIDL_genericExecFuncPtr")
	.dwattr $C$DW$T$328, DW_AT_type(*$C$DW$T$327)
	.dwattr $C$DW$T$328, DW_AT_decl_file("./inc/tidl_generic_datatypes.h")
	.dwattr $C$DW$T$328, DW_AT_decl_line(0x4c)
	.dwattr $C$DW$T$328, DW_AT_decl_column(0x13)

	.dwendtag $C$DW$TU$328


$C$DW$TU$365	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$365
$C$DW$T$365	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$365, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$T$365, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$365


$C$DW$TU$500	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$500

$C$DW$T$500	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$500, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$T$500, DW_AT_byte_size(0x0c)
$C$DW$1068	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$1068, DW_AT_upper_bound(0x02)

	.dwendtag $C$DW$T$500

	.dwendtag $C$DW$TU$500


$C$DW$TU$517	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$517

$C$DW$T$517	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$517, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$T$517, DW_AT_byte_size(0x24)
$C$DW$1069	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$1069, DW_AT_upper_bound(0x02)

$C$DW$1070	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$1070, DW_AT_upper_bound(0x02)

	.dwendtag $C$DW$T$517

	.dwendtag $C$DW$TU$517


$C$DW$TU$518	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$518

$C$DW$T$518	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$518, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$T$518, DW_AT_byte_size(0x18)
$C$DW$1071	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$1071, DW_AT_upper_bound(0x01)

$C$DW$1072	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$1072, DW_AT_upper_bound(0x02)

	.dwendtag $C$DW$T$518

	.dwendtag $C$DW$TU$518


$C$DW$TU$596	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$596

$C$DW$T$596	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$596, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$T$596, DW_AT_byte_size(0xc0)
$C$DW$1073	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$1073, DW_AT_upper_bound(0x0f)

$C$DW$1074	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$1074, DW_AT_upper_bound(0x02)

	.dwendtag $C$DW$T$596

	.dwendtag $C$DW$TU$596


$C$DW$TU$501	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$501

$C$DW$T$501	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$501, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$T$501, DW_AT_byte_size(0xfa0)
$C$DW$1075	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$1075, DW_AT_upper_bound(0x3e7)

	.dwendtag $C$DW$T$501

	.dwendtag $C$DW$TU$501


$C$DW$TU$528	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$528

$C$DW$T$528	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$528, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$T$528, DW_AT_byte_size(0x1000)
$C$DW$1076	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$1076, DW_AT_upper_bound(0x3ff)

	.dwendtag $C$DW$T$528

	.dwendtag $C$DW$TU$528


$C$DW$TU$599	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$599

$C$DW$T$599	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$599, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$T$599, DW_AT_byte_size(0x40)
$C$DW$1077	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$1077, DW_AT_upper_bound(0x0f)

	.dwendtag $C$DW$T$599

	.dwendtag $C$DW$TU$599


$C$DW$TU$816	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$816

$C$DW$T$816	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$816, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$T$816, DW_AT_byte_size(0x10000)
$C$DW$1078	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$1078, DW_AT_upper_bound(0x3ff)

$C$DW$1079	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$1079, DW_AT_upper_bound(0x0f)

	.dwendtag $C$DW$T$816

	.dwendtag $C$DW$TU$816


$C$DW$TU$779	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$779

$C$DW$T$779	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$779, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$T$779, DW_AT_byte_size(0x10)
$C$DW$1080	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$1080, DW_AT_upper_bound(0x03)

	.dwendtag $C$DW$T$779

	.dwendtag $C$DW$TU$779


$C$DW$TU$935	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$935

$C$DW$T$935	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$935, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$T$935, DW_AT_byte_size(0x44)
$C$DW$1081	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$1081, DW_AT_upper_bound(0x10)

	.dwendtag $C$DW$T$935

	.dwendtag $C$DW$TU$935


$C$DW$TU$11	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$11
$C$DW$T$11	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$11, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$11, DW_AT_name("unsigned int")
	.dwattr $C$DW$T$11, DW_AT_byte_size(0x04)

	.dwendtag $C$DW$TU$11


$C$DW$TU$49	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$49
$C$DW$T$49	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$49, DW_AT_name("__uint32_t")
	.dwattr $C$DW$T$49, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$T$49, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/machine/_types.h")
	.dwattr $C$DW$T$49, DW_AT_decl_line(0x65)
	.dwattr $C$DW$T$49, DW_AT_decl_column(0x17)

	.dwendtag $C$DW$TU$49


$C$DW$TU$50	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$50
$C$DW$T$50	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$50, DW_AT_name("uint32_t")
	.dwattr $C$DW$T$50, DW_AT_type(*$C$DW$T$49)
	.dwattr $C$DW$T$50, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/sys/_stdint.h")
	.dwattr $C$DW$T$50, DW_AT_decl_line(0x46)
	.dwattr $C$DW$T$50, DW_AT_decl_column(0x15)

	.dwendtag $C$DW$TU$50


$C$DW$TU$51	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$51
$C$DW$T$51	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$51, DW_AT_name("IALG_Cmd")
	.dwattr $C$DW$T$51, DW_AT_type(*$C$DW$T$50)
	.dwattr $C$DW$T$51, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ivision/ti/xdais/ialg.h")
	.dwattr $C$DW$T$51, DW_AT_decl_line(0xc5)
	.dwattr $C$DW$T$51, DW_AT_decl_column(0x12)

	.dwendtag $C$DW$TU$51


$C$DW$TU$141	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$141

$C$DW$T$141	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$141, DW_AT_type(*$C$DW$T$50)
	.dwattr $C$DW$T$141, DW_AT_byte_size(0x08)
$C$DW$1082	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$1082, DW_AT_upper_bound(0x01)

	.dwendtag $C$DW$T$141

	.dwendtag $C$DW$TU$141


$C$DW$TU$584	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$584

$C$DW$T$584	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$584, DW_AT_type(*$C$DW$T$50)
	.dwattr $C$DW$T$584, DW_AT_byte_size(0x1000)
$C$DW$1083	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$1083, DW_AT_upper_bound(0x3ff)

	.dwendtag $C$DW$T$584

	.dwendtag $C$DW$TU$584


$C$DW$TU$643	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$643

$C$DW$T$643	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$643, DW_AT_type(*$C$DW$T$50)
	.dwattr $C$DW$T$643, DW_AT_byte_size(0x0c)
$C$DW$1084	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$1084, DW_AT_upper_bound(0x02)

	.dwendtag $C$DW$T$643

	.dwendtag $C$DW$TU$643


$C$DW$TU$12	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$12
$C$DW$T$12	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$12, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$12, DW_AT_name("__int40_t")
	.dwattr $C$DW$T$12, DW_AT_byte_size(0x08)

	.dwendtag $C$DW$TU$12


$C$DW$TU$624	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$624
$C$DW$T$624	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$624, DW_AT_name("__int64_t")
	.dwattr $C$DW$T$624, DW_AT_type(*$C$DW$T$12)
	.dwattr $C$DW$T$624, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/machine/_types.h")
	.dwattr $C$DW$T$624, DW_AT_decl_line(0x6a)
	.dwattr $C$DW$T$624, DW_AT_decl_column(0x17)

	.dwendtag $C$DW$TU$624


$C$DW$TU$625	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$625
$C$DW$T$625	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$625, DW_AT_name("int64_t")
	.dwattr $C$DW$T$625, DW_AT_type(*$C$DW$T$624)
	.dwattr $C$DW$T$625, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/sys/_stdint.h")
	.dwattr $C$DW$T$625, DW_AT_decl_line(0x35)
	.dwattr $C$DW$T$625, DW_AT_decl_column(0x14)

	.dwendtag $C$DW$TU$625


$C$DW$TU$626	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$626

$C$DW$T$626	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$626, DW_AT_type(*$C$DW$T$625)
	.dwattr $C$DW$T$626, DW_AT_byte_size(0x18)
$C$DW$1085	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$1085, DW_AT_upper_bound(0x02)

	.dwendtag $C$DW$T$626

	.dwendtag $C$DW$TU$626


$C$DW$TU$13	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$13
$C$DW$T$13	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$13, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$13, DW_AT_name("unsigned __int40_t")
	.dwattr $C$DW$T$13, DW_AT_byte_size(0x08)

	.dwendtag $C$DW$TU$13


$C$DW$TU$416	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$416
$C$DW$T$416	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$416, DW_AT_name("__uint64_t")
	.dwattr $C$DW$T$416, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$416, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/machine/_types.h")
	.dwattr $C$DW$T$416, DW_AT_decl_line(0x6f)
	.dwattr $C$DW$T$416, DW_AT_decl_column(0x1f)

	.dwendtag $C$DW$TU$416


$C$DW$TU$417	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$417
$C$DW$T$417	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$417, DW_AT_name("uint64_t")
	.dwattr $C$DW$T$417, DW_AT_type(*$C$DW$T$416)
	.dwattr $C$DW$T$417, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/sys/_stdint.h")
	.dwattr $C$DW$T$417, DW_AT_decl_line(0x4b)
	.dwattr $C$DW$T$417, DW_AT_decl_column(0x15)

	.dwendtag $C$DW$TU$417


$C$DW$TU$958	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$958

$C$DW$T$958	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$958, DW_AT_type(*$C$DW$T$417)
	.dwattr $C$DW$T$958, DW_AT_byte_size(0x80)
$C$DW$1086	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$1086, DW_AT_upper_bound(0x0f)

	.dwendtag $C$DW$T$958

	.dwendtag $C$DW$TU$958


$C$DW$TU$14	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$14
$C$DW$T$14	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$14, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$14, DW_AT_name("long long")
	.dwattr $C$DW$T$14, DW_AT_byte_size(0x08)

	.dwendtag $C$DW$TU$14


$C$DW$TU$606	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$606
$C$DW$T$606	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$606, DW_AT_type(*$C$DW$T$14)
	.dwattr $C$DW$T$606, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$606


$C$DW$TU$15	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$15
$C$DW$T$15	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$15, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$15, DW_AT_name("unsigned long long")
	.dwattr $C$DW$T$15, DW_AT_byte_size(0x08)

	.dwendtag $C$DW$TU$15


$C$DW$TU$1101	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$1101

$C$DW$T$1101	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$1101, DW_AT_type(*$C$DW$T$15)
	.dwattr $C$DW$T$1101, DW_AT_byte_size(0x08)
$C$DW$1087	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$1087, DW_AT_upper_bound(0x00)

	.dwendtag $C$DW$T$1101

	.dwendtag $C$DW$TU$1101


$C$DW$TU$16	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$16
$C$DW$T$16	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$16, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$16, DW_AT_name("float")
	.dwattr $C$DW$T$16, DW_AT_byte_size(0x04)

	.dwendtag $C$DW$TU$16


$C$DW$TU$603	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$603
$C$DW$T$603	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$603, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$T$603, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$603


$C$DW$TU$25	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$25
$C$DW$T$25	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$25, DW_AT_name("float32_tidl")
	.dwattr $C$DW$T$25, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$T$25, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$25, DW_AT_decl_line(0x71)
	.dwattr $C$DW$T$25, DW_AT_decl_column(0x0f)

	.dwendtag $C$DW$TU$25


$C$DW$TU$366	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$366
$C$DW$T$366	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$366, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$T$366, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$366


$C$DW$TU$595	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$595

$C$DW$T$595	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$595, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$T$595, DW_AT_byte_size(0x40)
$C$DW$1088	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$1088, DW_AT_upper_bound(0x0f)

	.dwendtag $C$DW$T$595

	.dwendtag $C$DW$TU$595


$C$DW$TU$597	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$597

$C$DW$T$597	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$597, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$T$597, DW_AT_byte_size(0x0c)
$C$DW$1089	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$1089, DW_AT_upper_bound(0x02)

	.dwendtag $C$DW$T$597

	.dwendtag $C$DW$TU$597


$C$DW$TU$598	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$598

$C$DW$T$598	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$598, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$T$598, DW_AT_byte_size(0xc0)
$C$DW$1090	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$1090, DW_AT_upper_bound(0x0f)

$C$DW$1091	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$1091, DW_AT_upper_bound(0x02)

	.dwendtag $C$DW$T$598

	.dwendtag $C$DW$TU$598


$C$DW$TU$912	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$912

$C$DW$T$912	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$912, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$T$912, DW_AT_byte_size(0x10)
$C$DW$1092	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$1092, DW_AT_upper_bound(0x03)

	.dwendtag $C$DW$T$912

	.dwendtag $C$DW$TU$912


$C$DW$TU$1009	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$1009
$C$DW$T$1009	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$1009, DW_AT_type(*$C$DW$T$25)

	.dwendtag $C$DW$TU$1009


$C$DW$TU$1010	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$1010
$C$DW$T$1010	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$1010, DW_AT_type(*$C$DW$T$1009)
	.dwattr $C$DW$T$1010, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$1010


$C$DW$TU$17	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$17
$C$DW$T$17	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$17, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$17, DW_AT_name("double")
	.dwattr $C$DW$T$17, DW_AT_byte_size(0x08)

	.dwendtag $C$DW$TU$17


$C$DW$TU$515	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$515

$C$DW$T$515	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$515, DW_AT_type(*$C$DW$T$17)
	.dwattr $C$DW$T$515, DW_AT_byte_size(0x10)
$C$DW$1093	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$1093, DW_AT_upper_bound(0x01)

	.dwendtag $C$DW$T$515

	.dwendtag $C$DW$TU$515


$C$DW$TU$516	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$516

$C$DW$T$516	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$516, DW_AT_type(*$C$DW$T$17)
	.dwattr $C$DW$T$516, DW_AT_byte_size(0x30)
$C$DW$1094	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$1094, DW_AT_upper_bound(0x02)

$C$DW$1095	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$1095, DW_AT_upper_bound(0x01)

	.dwendtag $C$DW$T$516

	.dwendtag $C$DW$TU$516


$C$DW$TU$18	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$18
$C$DW$T$18	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$18, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$18, DW_AT_name("long double")
	.dwattr $C$DW$T$18, DW_AT_byte_size(0x08)

	.dwendtag $C$DW$TU$18


$C$DW$TU$19	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$19
$C$DW$T$19	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$19, DW_AT_encoding(DW_ATE_TI_complex_signed)
	.dwattr $C$DW$T$19, DW_AT_name("cchar")
	.dwattr $C$DW$T$19, DW_AT_byte_size(0x02)

	.dwendtag $C$DW$TU$19


$C$DW$TU$20	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$20
$C$DW$T$20	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$20, DW_AT_encoding(DW_ATE_TI_complex_signed)
	.dwattr $C$DW$T$20, DW_AT_name("cshort")
	.dwattr $C$DW$T$20, DW_AT_byte_size(0x04)

	.dwendtag $C$DW$TU$20


$C$DW$TU$21	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$21
$C$DW$T$21	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$21, DW_AT_encoding(DW_ATE_TI_complex_signed)
	.dwattr $C$DW$T$21, DW_AT_name("cint")
	.dwattr $C$DW$T$21, DW_AT_byte_size(0x08)

	.dwendtag $C$DW$TU$21


$C$DW$TU$22	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$22
$C$DW$T$22	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$22, DW_AT_encoding(DW_ATE_TI_complex_signed)
	.dwattr $C$DW$T$22, DW_AT_name("clong")
	.dwattr $C$DW$T$22, DW_AT_byte_size(0x10)

	.dwendtag $C$DW$TU$22


$C$DW$TU$23	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$23
$C$DW$T$23	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$23, DW_AT_encoding(DW_ATE_TI_complex_float)
	.dwattr $C$DW$T$23, DW_AT_name("cfloat")
	.dwattr $C$DW$T$23, DW_AT_byte_size(0x08)

	.dwendtag $C$DW$TU$23


$C$DW$TU$24	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$24
$C$DW$T$24	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$24, DW_AT_encoding(DW_ATE_TI_complex_float)
	.dwattr $C$DW$T$24, DW_AT_name("cdouble")
	.dwattr $C$DW$T$24, DW_AT_byte_size(0x10)

	.dwendtag $C$DW$TU$24


$C$DW$TU$273	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$273
$C$DW$T$273	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$273, DW_AT_type(*$C$DW$T$5)

	.dwendtag $C$DW$TU$273


$C$DW$TU$274	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$274
$C$DW$T$274	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$274, DW_AT_type(*$C$DW$T$273)
	.dwattr $C$DW$T$274, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$274


$C$DW$TU$275	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$275
$C$DW$T$275	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$275, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$T$275, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$275


$C$DW$TU$276	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$276
$C$DW$T$276	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$276, DW_AT_name("__va_list")
	.dwattr $C$DW$T$276, DW_AT_type(*$C$DW$T$275)
	.dwattr $C$DW$T$276, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/machine/_types.h")
	.dwattr $C$DW$T$276, DW_AT_decl_line(0xb2)
	.dwattr $C$DW$T$276, DW_AT_decl_column(0x13)

	.dwendtag $C$DW$TU$276


$C$DW$TU$277	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$277
$C$DW$T$277	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$277, DW_AT_name("va_list")
	.dwattr $C$DW$T$277, DW_AT_type(*$C$DW$T$276)
	.dwattr $C$DW$T$277, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/stdarg.h")
	.dwattr $C$DW$T$277, DW_AT_decl_line(0x33)
	.dwattr $C$DW$T$277, DW_AT_decl_column(0x17)

	.dwendtag $C$DW$TU$277


$C$DW$TU$1102	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$1102

$C$DW$T$1102	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$1102, DW_AT_name("__ti_vpred_tp")
	.dwattr $C$DW$T$1102, DW_AT_byte_size(0x08)
$C$DW$1096	.dwtag  DW_TAG_member
	.dwattr $C$DW$1096, DW_AT_type(*$C$DW$T$1101)
	.dwattr $C$DW$1096, DW_AT_name("__vpred_field")
	.dwattr $C$DW$1096, DW_AT_accessibility(DW_ACCESS_private)

	.dwendtag $C$DW$T$1102

	.dwendtag $C$DW$TU$1102


$C$DW$TU$397	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$397

$C$DW$T$397	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$397, DW_AT_name("eTensorType")
	.dwattr $C$DW$T$397, DW_AT_byte_size(0x04)
$C$DW$1097	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$1097, DW_AT_name("TIDL_INPUT_TENSOR")
	.dwattr $C$DW$1097, DW_AT_const_value(0x00)
	.dwattr $C$DW$1097, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1097, DW_AT_decl_line(0x312)
	.dwattr $C$DW$1097, DW_AT_decl_column(0x03)

$C$DW$1098	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$1098, DW_AT_name("TIDL_OUTPUT_TENSOR")
	.dwattr $C$DW$1098, DW_AT_const_value(0x01)
	.dwattr $C$DW$1098, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1098, DW_AT_decl_line(0x314)
	.dwattr $C$DW$1098, DW_AT_decl_column(0x03)

$C$DW$1099	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$1099, DW_AT_name("TIDL_PARAMETER_TENSOR")
	.dwattr $C$DW$1099, DW_AT_const_value(0x02)
	.dwattr $C$DW$1099, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1099, DW_AT_decl_line(0x316)
	.dwattr $C$DW$1099, DW_AT_decl_column(0x03)

$C$DW$1100	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$1100, DW_AT_name("TIDL_MAX_TENSOR_TYPES")
	.dwattr $C$DW$1100, DW_AT_const_value(0x03)
	.dwattr $C$DW$1100, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1100, DW_AT_decl_line(0x318)
	.dwattr $C$DW$1100, DW_AT_decl_column(0x03)

	.dwattr $C$DW$T$397, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$T$397, DW_AT_decl_line(0x310)
	.dwattr $C$DW$T$397, DW_AT_decl_column(0x01)
	.dwendtag $C$DW$T$397

	.dwendtag $C$DW$TU$397


$C$DW$TU$398	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$398
$C$DW$T$398	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$398, DW_AT_name("eTensorType")
	.dwattr $C$DW$T$398, DW_AT_type(*$C$DW$T$397)
	.dwattr $C$DW$T$398, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$T$398, DW_AT_decl_line(0x319)
	.dwattr $C$DW$T$398, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$398


$C$DW$TU$487	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$487

$C$DW$T$487	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$487, DW_AT_name("localDataFlowInfo_t")
	.dwattr $C$DW$T$487, DW_AT_byte_size(0x30)
$C$DW$1101	.dwtag  DW_TAG_member
	.dwattr $C$DW$1101, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1101, DW_AT_name("Nci")
	.dwattr $C$DW$1101, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1101, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1101, DW_AT_decl_line(0x35a)
	.dwattr $C$DW$1101, DW_AT_decl_column(0x0b)

$C$DW$1102	.dwtag  DW_TAG_member
	.dwattr $C$DW$1102, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1102, DW_AT_name("numSplit")
	.dwattr $C$DW$1102, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$1102, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1102, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1102, DW_AT_decl_line(0x35b)
	.dwattr $C$DW$1102, DW_AT_decl_column(0x0b)

$C$DW$1103	.dwtag  DW_TAG_member
	.dwattr $C$DW$1103, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1103, DW_AT_name("dmaFreq")
	.dwattr $C$DW$1103, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$1103, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1103, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1103, DW_AT_decl_line(0x35c)
	.dwattr $C$DW$1103, DW_AT_decl_column(0x0b)

$C$DW$1104	.dwtag  DW_TAG_member
	.dwattr $C$DW$1104, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1104, DW_AT_name("kernelFreq")
	.dwattr $C$DW$1104, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$1104, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1104, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1104, DW_AT_decl_line(0x35d)
	.dwattr $C$DW$1104, DW_AT_decl_column(0x0b)

$C$DW$1105	.dwtag  DW_TAG_member
	.dwattr $C$DW$1105, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1105, DW_AT_name("activeBufWidth")
	.dwattr $C$DW$1105, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$1105, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1105, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1105, DW_AT_decl_line(0x35e)
	.dwattr $C$DW$1105, DW_AT_decl_column(0x0b)

$C$DW$1106	.dwtag  DW_TAG_member
	.dwattr $C$DW$1106, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1106, DW_AT_name("isInTensorDmaReq")
	.dwattr $C$DW$1106, DW_AT_data_member_location[DW_OP_plus_uconst 0x14]
	.dwattr $C$DW$1106, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1106, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1106, DW_AT_decl_line(0x35f)
	.dwattr $C$DW$1106, DW_AT_decl_column(0x0b)

$C$DW$1107	.dwtag  DW_TAG_member
	.dwattr $C$DW$1107, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1107, DW_AT_name("memSpaceIN")
	.dwattr $C$DW$1107, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$1107, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1107, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1107, DW_AT_decl_line(0x360)
	.dwattr $C$DW$1107, DW_AT_decl_column(0x0b)

$C$DW$1108	.dwtag  DW_TAG_member
	.dwattr $C$DW$1108, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1108, DW_AT_name("addrOffsetIN")
	.dwattr $C$DW$1108, DW_AT_data_member_location[DW_OP_plus_uconst 0x1c]
	.dwattr $C$DW$1108, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1108, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1108, DW_AT_decl_line(0x361)
	.dwattr $C$DW$1108, DW_AT_decl_column(0x0b)

$C$DW$1109	.dwtag  DW_TAG_member
	.dwattr $C$DW$1109, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1109, DW_AT_name("memSpaceOUT")
	.dwattr $C$DW$1109, DW_AT_data_member_location[DW_OP_plus_uconst 0x20]
	.dwattr $C$DW$1109, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1109, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1109, DW_AT_decl_line(0x362)
	.dwattr $C$DW$1109, DW_AT_decl_column(0x0b)

$C$DW$1110	.dwtag  DW_TAG_member
	.dwattr $C$DW$1110, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1110, DW_AT_name("addrOffsetOUT")
	.dwattr $C$DW$1110, DW_AT_data_member_location[DW_OP_plus_uconst 0x24]
	.dwattr $C$DW$1110, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1110, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1110, DW_AT_decl_line(0x363)
	.dwattr $C$DW$1110, DW_AT_decl_column(0x0b)

$C$DW$1111	.dwtag  DW_TAG_member
	.dwattr $C$DW$1111, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1111, DW_AT_name("accessOffsetRead")
	.dwattr $C$DW$1111, DW_AT_data_member_location[DW_OP_plus_uconst 0x28]
	.dwattr $C$DW$1111, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1111, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1111, DW_AT_decl_line(0x364)
	.dwattr $C$DW$1111, DW_AT_decl_column(0x0b)

$C$DW$1112	.dwtag  DW_TAG_member
	.dwattr $C$DW$1112, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1112, DW_AT_name("accessOffsetWrite")
	.dwattr $C$DW$1112, DW_AT_data_member_location[DW_OP_plus_uconst 0x2c]
	.dwattr $C$DW$1112, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1112, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1112, DW_AT_decl_line(0x365)
	.dwattr $C$DW$1112, DW_AT_decl_column(0x0b)


$C$DW$1113	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1113, DW_AT_name("operator =")
	.dwattr $C$DW$1113, DW_AT_declaration
	.dwattr $C$DW$1113, DW_AT_linkage_name("_ZN19localDataFlowInfo_taSERKS_")
	.dwattr $C$DW$1113, DW_AT_type(*$C$DW$T$482)
	.dwattr $C$DW$1113, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1114	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1114, DW_AT_type(*$C$DW$T$484)

	.dwendtag $C$DW$1113


$C$DW$1115	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1115, DW_AT_name("operator =")
	.dwattr $C$DW$1115, DW_AT_declaration
	.dwattr $C$DW$1115, DW_AT_linkage_name("_ZN19localDataFlowInfo_taSEOS_")
	.dwattr $C$DW$1115, DW_AT_type(*$C$DW$T$482)
	.dwattr $C$DW$1115, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1116	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1116, DW_AT_type(*$C$DW$T$482)

	.dwendtag $C$DW$1115

	.dwattr $C$DW$T$487, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$T$487, DW_AT_decl_line(0x359)
	.dwattr $C$DW$T$487, DW_AT_decl_column(0x01)
	.dwendtag $C$DW$T$487

	.dwendtag $C$DW$TU$487


$C$DW$TU$482	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$482
$C$DW$T$482	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$482, DW_AT_type(*$C$DW$T$487)
	.dwattr $C$DW$T$482, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$482


$C$DW$TU$485	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$485

$C$DW$T$485	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$485, DW_AT_type(*$C$DW$T$482)
$C$DW$1117	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1117, DW_AT_type(*$C$DW$T$484)

	.dwendtag $C$DW$T$485

	.dwendtag $C$DW$TU$485


$C$DW$TU$486	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$486

$C$DW$T$486	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$486, DW_AT_type(*$C$DW$T$482)
$C$DW$1118	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1118, DW_AT_type(*$C$DW$T$482)

	.dwendtag $C$DW$T$486

	.dwendtag $C$DW$TU$486


$C$DW$TU$314	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$314
$C$DW$T$314	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$314, DW_AT_name("localDataFlowInfo_t")
	.dwattr $C$DW$T$314, DW_AT_type(*$C$DW$T$487)
	.dwattr $C$DW$T$314, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$T$314, DW_AT_decl_line(0x366)
	.dwattr $C$DW$T$314, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$314


$C$DW$TU$483	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$483
$C$DW$T$483	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$483, DW_AT_type(*$C$DW$T$487)

	.dwendtag $C$DW$TU$483


$C$DW$TU$484	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$484
$C$DW$T$484	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$484, DW_AT_type(*$C$DW$T$483)
	.dwattr $C$DW$T$484, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$484


$C$DW$TU$494	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$494

$C$DW$T$494	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$494, DW_AT_name("sBufferInfo_t")
	.dwattr $C$DW$T$494, DW_AT_byte_size(0x3c)
$C$DW$1119	.dwtag  DW_TAG_member
	.dwattr $C$DW$1119, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1119, DW_AT_name("accessor")
	.dwattr $C$DW$1119, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1119, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$1119, DW_AT_decl_line(0xd8)
	.dwattr $C$DW$1119, DW_AT_decl_column(0x0b)

$C$DW$1120	.dwtag  DW_TAG_member
	.dwattr $C$DW$1120, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1120, DW_AT_name("space")
	.dwattr $C$DW$1120, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$1120, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1120, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$1120, DW_AT_decl_line(0xd9)
	.dwattr $C$DW$1120, DW_AT_decl_column(0x0b)

$C$DW$1121	.dwtag  DW_TAG_member
	.dwattr $C$DW$1121, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1121, DW_AT_name("bufWidth")
	.dwattr $C$DW$1121, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$1121, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1121, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$1121, DW_AT_decl_line(0xda)
	.dwattr $C$DW$1121, DW_AT_decl_column(0x0b)

$C$DW$1122	.dwtag  DW_TAG_member
	.dwattr $C$DW$1122, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1122, DW_AT_name("bufHeight")
	.dwattr $C$DW$1122, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$1122, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1122, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$1122, DW_AT_decl_line(0xdb)
	.dwattr $C$DW$1122, DW_AT_decl_column(0x0b)

$C$DW$1123	.dwtag  DW_TAG_member
	.dwattr $C$DW$1123, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1123, DW_AT_name("bufSize")
	.dwattr $C$DW$1123, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$1123, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1123, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$1123, DW_AT_decl_line(0xdc)
	.dwattr $C$DW$1123, DW_AT_decl_column(0x0b)

$C$DW$1124	.dwtag  DW_TAG_member
	.dwattr $C$DW$1124, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1124, DW_AT_name("activeBufWidth")
	.dwattr $C$DW$1124, DW_AT_data_member_location[DW_OP_plus_uconst 0x14]
	.dwattr $C$DW$1124, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1124, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$1124, DW_AT_decl_line(0xdd)
	.dwattr $C$DW$1124, DW_AT_decl_column(0x0b)

$C$DW$1125	.dwtag  DW_TAG_member
	.dwattr $C$DW$1125, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1125, DW_AT_name("baseMem")
	.dwattr $C$DW$1125, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$1125, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1125, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$1125, DW_AT_decl_line(0xde)
	.dwattr $C$DW$1125, DW_AT_decl_column(0x0b)

$C$DW$1126	.dwtag  DW_TAG_member
	.dwattr $C$DW$1126, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1126, DW_AT_name("accessoffset")
	.dwattr $C$DW$1126, DW_AT_data_member_location[DW_OP_plus_uconst 0x1c]
	.dwattr $C$DW$1126, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1126, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$1126, DW_AT_decl_line(0xdf)
	.dwattr $C$DW$1126, DW_AT_decl_column(0x0b)

$C$DW$1127	.dwtag  DW_TAG_member
	.dwattr $C$DW$1127, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1127, DW_AT_name("padC")
	.dwattr $C$DW$1127, DW_AT_data_member_location[DW_OP_plus_uconst 0x20]
	.dwattr $C$DW$1127, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1127, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$1127, DW_AT_decl_line(0xe0)
	.dwattr $C$DW$1127, DW_AT_decl_column(0x0b)

$C$DW$1128	.dwtag  DW_TAG_member
	.dwattr $C$DW$1128, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1128, DW_AT_name("padR")
	.dwattr $C$DW$1128, DW_AT_data_member_location[DW_OP_plus_uconst 0x24]
	.dwattr $C$DW$1128, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1128, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$1128, DW_AT_decl_line(0xe1)
	.dwattr $C$DW$1128, DW_AT_decl_column(0x0b)

$C$DW$1129	.dwtag  DW_TAG_member
	.dwattr $C$DW$1129, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1129, DW_AT_name("padCZeros")
	.dwattr $C$DW$1129, DW_AT_data_member_location[DW_OP_plus_uconst 0x28]
	.dwattr $C$DW$1129, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1129, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$1129, DW_AT_decl_line(0xe2)
	.dwattr $C$DW$1129, DW_AT_decl_column(0x0b)

$C$DW$1130	.dwtag  DW_TAG_member
	.dwattr $C$DW$1130, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1130, DW_AT_name("padRZeros")
	.dwattr $C$DW$1130, DW_AT_data_member_location[DW_OP_plus_uconst 0x2c]
	.dwattr $C$DW$1130, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1130, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$1130, DW_AT_decl_line(0xe3)
	.dwattr $C$DW$1130, DW_AT_decl_column(0x0b)

$C$DW$1131	.dwtag  DW_TAG_member
	.dwattr $C$DW$1131, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1131, DW_AT_name("padCFillZeros")
	.dwattr $C$DW$1131, DW_AT_data_member_location[DW_OP_plus_uconst 0x30]
	.dwattr $C$DW$1131, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1131, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$1131, DW_AT_decl_line(0xe4)
	.dwattr $C$DW$1131, DW_AT_decl_column(0x0b)

$C$DW$1132	.dwtag  DW_TAG_member
	.dwattr $C$DW$1132, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1132, DW_AT_name("padRFillZeros")
	.dwattr $C$DW$1132, DW_AT_data_member_location[DW_OP_plus_uconst 0x34]
	.dwattr $C$DW$1132, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1132, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$1132, DW_AT_decl_line(0xe5)
	.dwattr $C$DW$1132, DW_AT_decl_column(0x0b)

$C$DW$1133	.dwtag  DW_TAG_member
	.dwattr $C$DW$1133, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1133, DW_AT_name("padCReq")
	.dwattr $C$DW$1133, DW_AT_data_member_location[DW_OP_plus_uconst 0x38]
	.dwattr $C$DW$1133, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1133, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$1133, DW_AT_decl_line(0xe6)
	.dwattr $C$DW$1133, DW_AT_decl_column(0x0b)


$C$DW$1134	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1134, DW_AT_name("operator =")
	.dwattr $C$DW$1134, DW_AT_declaration
	.dwattr $C$DW$1134, DW_AT_linkage_name("_ZN13sBufferInfo_taSERKS_")
	.dwattr $C$DW$1134, DW_AT_type(*$C$DW$T$489)
	.dwattr $C$DW$1134, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1135	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1135, DW_AT_type(*$C$DW$T$491)

	.dwendtag $C$DW$1134


$C$DW$1136	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1136, DW_AT_name("operator =")
	.dwattr $C$DW$1136, DW_AT_declaration
	.dwattr $C$DW$1136, DW_AT_linkage_name("_ZN13sBufferInfo_taSEOS_")
	.dwattr $C$DW$1136, DW_AT_type(*$C$DW$T$489)
	.dwattr $C$DW$1136, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1137	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1137, DW_AT_type(*$C$DW$T$489)

	.dwendtag $C$DW$1136

	.dwattr $C$DW$T$494, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$T$494, DW_AT_decl_line(0xd7)
	.dwattr $C$DW$T$494, DW_AT_decl_column(0x01)
	.dwendtag $C$DW$T$494

	.dwendtag $C$DW$TU$494


$C$DW$TU$489	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$489
$C$DW$T$489	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$489, DW_AT_type(*$C$DW$T$494)
	.dwattr $C$DW$T$489, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$489


$C$DW$TU$492	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$492

$C$DW$T$492	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$492, DW_AT_type(*$C$DW$T$489)
$C$DW$1138	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1138, DW_AT_type(*$C$DW$T$491)

	.dwendtag $C$DW$T$492

	.dwendtag $C$DW$TU$492


$C$DW$TU$493	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$493

$C$DW$T$493	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$493, DW_AT_type(*$C$DW$T$489)
$C$DW$1139	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1139, DW_AT_type(*$C$DW$T$489)

	.dwendtag $C$DW$T$493

	.dwendtag $C$DW$TU$493


$C$DW$TU$490	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$490
$C$DW$T$490	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$490, DW_AT_type(*$C$DW$T$494)

	.dwendtag $C$DW$TU$490


$C$DW$TU$491	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$491
$C$DW$T$491	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$491, DW_AT_type(*$C$DW$T$490)
	.dwattr $C$DW$T$491, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$491


$C$DW$TU$496	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$496
$C$DW$T$496	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$496, DW_AT_name("sBufferInfo_t")
	.dwattr $C$DW$T$496, DW_AT_type(*$C$DW$T$494)
	.dwattr $C$DW$T$496, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$T$496, DW_AT_decl_line(0xe7)
	.dwattr $C$DW$T$496, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$496


$C$DW$TU$497	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$497

$C$DW$T$497	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$497, DW_AT_type(*$C$DW$T$496)
	.dwattr $C$DW$T$497, DW_AT_byte_size(0x78)
$C$DW$1140	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$1140, DW_AT_upper_bound(0x01)

	.dwendtag $C$DW$T$497

	.dwendtag $C$DW$TU$497


$C$DW$TU$498	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$498

$C$DW$T$498	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$498, DW_AT_type(*$C$DW$T$496)
	.dwattr $C$DW$T$498, DW_AT_byte_size(0x168)
$C$DW$1141	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$1141, DW_AT_upper_bound(0x02)

$C$DW$1142	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$1142, DW_AT_upper_bound(0x01)

	.dwendtag $C$DW$T$498

	.dwendtag $C$DW$TU$498


$C$DW$TU$507	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$507

$C$DW$T$507	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$507, DW_AT_name("sDataFlowInfo_t")
	.dwattr $C$DW$T$507, DW_AT_byte_size(0x12a8)
$C$DW$1143	.dwtag  DW_TAG_member
	.dwattr $C$DW$1143, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1143, DW_AT_name("dataFlowType")
	.dwattr $C$DW$1143, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1143, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$1143, DW_AT_decl_line(0x10f)
	.dwattr $C$DW$1143, DW_AT_decl_column(0x0b)

$C$DW$1144	.dwtag  DW_TAG_member
	.dwattr $C$DW$1144, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1144, DW_AT_name("numSplit")
	.dwattr $C$DW$1144, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$1144, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1144, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$1144, DW_AT_decl_line(0x110)
	.dwattr $C$DW$1144, DW_AT_decl_column(0x0b)

$C$DW$1145	.dwtag  DW_TAG_member
	.dwattr $C$DW$1145, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1145, DW_AT_name("procSize")
	.dwattr $C$DW$1145, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$1145, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1145, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$1145, DW_AT_decl_line(0x111)
	.dwattr $C$DW$1145, DW_AT_decl_column(0x0b)

$C$DW$1146	.dwtag  DW_TAG_member
	.dwattr $C$DW$1146, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1146, DW_AT_name("Nci")
	.dwattr $C$DW$1146, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$1146, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1146, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$1146, DW_AT_decl_line(0x112)
	.dwattr $C$DW$1146, DW_AT_decl_column(0x0b)

$C$DW$1147	.dwtag  DW_TAG_member
	.dwattr $C$DW$1147, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1147, DW_AT_name("Nco")
	.dwattr $C$DW$1147, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$1147, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1147, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$1147, DW_AT_decl_line(0x113)
	.dwattr $C$DW$1147, DW_AT_decl_column(0x0b)

$C$DW$1148	.dwtag  DW_TAG_member
	.dwattr $C$DW$1148, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1148, DW_AT_name("kernelFreq")
	.dwattr $C$DW$1148, DW_AT_data_member_location[DW_OP_plus_uconst 0x14]
	.dwattr $C$DW$1148, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1148, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$1148, DW_AT_decl_line(0x114)
	.dwattr $C$DW$1148, DW_AT_decl_column(0x0b)

$C$DW$1149	.dwtag  DW_TAG_member
	.dwattr $C$DW$1149, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1149, DW_AT_name("dmaFreq")
	.dwattr $C$DW$1149, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$1149, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1149, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$1149, DW_AT_decl_line(0x115)
	.dwattr $C$DW$1149, DW_AT_decl_column(0x0b)

$C$DW$1150	.dwtag  DW_TAG_member
	.dwattr $C$DW$1150, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1150, DW_AT_name("dmaFreqWt")
	.dwattr $C$DW$1150, DW_AT_data_member_location[DW_OP_plus_uconst 0x1c]
	.dwattr $C$DW$1150, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1150, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$1150, DW_AT_decl_line(0x116)
	.dwattr $C$DW$1150, DW_AT_decl_column(0x0b)

$C$DW$1151	.dwtag  DW_TAG_member
	.dwattr $C$DW$1151, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1151, DW_AT_name("preferenceOrder")
	.dwattr $C$DW$1151, DW_AT_data_member_location[DW_OP_plus_uconst 0x20]
	.dwattr $C$DW$1151, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1151, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$1151, DW_AT_decl_line(0x117)
	.dwattr $C$DW$1151, DW_AT_decl_column(0x0b)

$C$DW$1152	.dwtag  DW_TAG_member
	.dwattr $C$DW$1152, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1152, DW_AT_name("preFetchAligned")
	.dwattr $C$DW$1152, DW_AT_data_member_location[DW_OP_plus_uconst 0x24]
	.dwattr $C$DW$1152, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1152, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$1152, DW_AT_decl_line(0x118)
	.dwattr $C$DW$1152, DW_AT_decl_column(0x0b)

$C$DW$1153	.dwtag  DW_TAG_member
	.dwattr $C$DW$1153, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1153, DW_AT_name("preFetch")
	.dwattr $C$DW$1153, DW_AT_data_member_location[DW_OP_plus_uconst 0x28]
	.dwattr $C$DW$1153, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1153, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$1153, DW_AT_decl_line(0x119)
	.dwattr $C$DW$1153, DW_AT_decl_column(0x0b)

$C$DW$1154	.dwtag  DW_TAG_member
	.dwattr $C$DW$1154, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1154, DW_AT_name("requiredInPlaneSize")
	.dwattr $C$DW$1154, DW_AT_data_member_location[DW_OP_plus_uconst 0x2c]
	.dwattr $C$DW$1154, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1154, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$1154, DW_AT_decl_line(0x11a)
	.dwattr $C$DW$1154, DW_AT_decl_column(0x0b)

$C$DW$1155	.dwtag  DW_TAG_member
	.dwattr $C$DW$1155, DW_AT_type(*$C$DW$T$498)
	.dwattr $C$DW$1155, DW_AT_name("bufInfo")
	.dwattr $C$DW$1155, DW_AT_data_member_location[DW_OP_plus_uconst 0x30]
	.dwattr $C$DW$1155, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1155, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$1155, DW_AT_decl_line(0x11b)
	.dwattr $C$DW$1155, DW_AT_decl_column(0x12)

$C$DW$1156	.dwtag  DW_TAG_member
	.dwattr $C$DW$1156, DW_AT_type(*$C$DW$T$497)
	.dwattr $C$DW$1156, DW_AT_name("wtOneShot")
	.dwattr $C$DW$1156, DW_AT_data_member_location[DW_OP_plus_uconst 0x198]
	.dwattr $C$DW$1156, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1156, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$1156, DW_AT_decl_line(0x11c)
	.dwattr $C$DW$1156, DW_AT_decl_column(0x12)

$C$DW$1157	.dwtag  DW_TAG_member
	.dwattr $C$DW$1157, DW_AT_type(*$C$DW$T$496)
	.dwattr $C$DW$1157, DW_AT_name("privContextMemBuff")
	.dwattr $C$DW$1157, DW_AT_data_member_location[DW_OP_plus_uconst 0x210]
	.dwattr $C$DW$1157, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1157, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$1157, DW_AT_decl_line(0x11d)
	.dwattr $C$DW$1157, DW_AT_decl_column(0x11)

$C$DW$1158	.dwtag  DW_TAG_member
	.dwattr $C$DW$1158, DW_AT_type(*$C$DW$T$499)
	.dwattr $C$DW$1158, DW_AT_name("memStats")
	.dwattr $C$DW$1158, DW_AT_data_member_location[DW_OP_plus_uconst 0x250]
	.dwattr $C$DW$1158, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1158, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$1158, DW_AT_decl_line(0x120)
	.dwattr $C$DW$1158, DW_AT_decl_column(0x12)

$C$DW$1159	.dwtag  DW_TAG_member
	.dwattr $C$DW$1159, DW_AT_type(*$C$DW$T$500)
	.dwattr $C$DW$1159, DW_AT_name("bufWidth")
	.dwattr $C$DW$1159, DW_AT_data_member_location[DW_OP_plus_uconst 0x2d8]
	.dwattr $C$DW$1159, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1159, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$1159, DW_AT_decl_line(0x121)
	.dwattr $C$DW$1159, DW_AT_decl_column(0x0b)

$C$DW$1160	.dwtag  DW_TAG_member
	.dwattr $C$DW$1160, DW_AT_type(*$C$DW$T$500)
	.dwattr $C$DW$1160, DW_AT_name("bufHeight")
	.dwattr $C$DW$1160, DW_AT_data_member_location[DW_OP_plus_uconst 0x2e4]
	.dwattr $C$DW$1160, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1160, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$1160, DW_AT_decl_line(0x122)
	.dwattr $C$DW$1160, DW_AT_decl_column(0x0b)

$C$DW$1161	.dwtag  DW_TAG_member
	.dwattr $C$DW$1161, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1161, DW_AT_name("bufState")
	.dwattr $C$DW$1161, DW_AT_data_member_location[DW_OP_plus_uconst 0x2f0]
	.dwattr $C$DW$1161, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1161, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$1161, DW_AT_decl_line(0x126)
	.dwattr $C$DW$1161, DW_AT_decl_column(0x0b)

$C$DW$1162	.dwtag  DW_TAG_member
	.dwattr $C$DW$1162, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1162, DW_AT_name("forceOutDDR")
	.dwattr $C$DW$1162, DW_AT_data_member_location[DW_OP_plus_uconst 0x2f4]
	.dwattr $C$DW$1162, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1162, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$1162, DW_AT_decl_line(0x12a)
	.dwattr $C$DW$1162, DW_AT_decl_column(0x0b)

$C$DW$1163	.dwtag  DW_TAG_member
	.dwattr $C$DW$1163, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1163, DW_AT_name("forceOutStgDDR")
	.dwattr $C$DW$1163, DW_AT_data_member_location[DW_OP_plus_uconst 0x2f8]
	.dwattr $C$DW$1163, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1163, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$1163, DW_AT_decl_line(0x12b)
	.dwattr $C$DW$1163, DW_AT_decl_column(0x0b)

$C$DW$1164	.dwtag  DW_TAG_member
	.dwattr $C$DW$1164, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1164, DW_AT_name("forceWtStgDDR")
	.dwattr $C$DW$1164, DW_AT_data_member_location[DW_OP_plus_uconst 0x2fc]
	.dwattr $C$DW$1164, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1164, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$1164, DW_AT_decl_line(0x12c)
	.dwattr $C$DW$1164, DW_AT_decl_column(0x0b)

$C$DW$1165	.dwtag  DW_TAG_member
	.dwattr $C$DW$1165, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1165, DW_AT_name("forceContextMemDDR")
	.dwattr $C$DW$1165, DW_AT_data_member_location[DW_OP_plus_uconst 0x300]
	.dwattr $C$DW$1165, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1165, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$1165, DW_AT_decl_line(0x12d)
	.dwattr $C$DW$1165, DW_AT_decl_column(0x0b)

$C$DW$1166	.dwtag  DW_TAG_member
	.dwattr $C$DW$1166, DW_AT_type(*$C$DW$T$501)
	.dwattr $C$DW$1166, DW_AT_name("reservedSpace")
	.dwattr $C$DW$1166, DW_AT_data_member_location[DW_OP_plus_uconst 0x304]
	.dwattr $C$DW$1166, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1166, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$1166, DW_AT_decl_line(0x12e)
	.dwattr $C$DW$1166, DW_AT_decl_column(0x0b)


$C$DW$1167	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1167, DW_AT_name("operator =")
	.dwattr $C$DW$1167, DW_AT_declaration
	.dwattr $C$DW$1167, DW_AT_linkage_name("_ZN15sDataFlowInfo_taSERKS_")
	.dwattr $C$DW$1167, DW_AT_type(*$C$DW$T$502)
	.dwattr $C$DW$1167, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1168	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1168, DW_AT_type(*$C$DW$T$504)

	.dwendtag $C$DW$1167


$C$DW$1169	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1169, DW_AT_name("operator =")
	.dwattr $C$DW$1169, DW_AT_declaration
	.dwattr $C$DW$1169, DW_AT_linkage_name("_ZN15sDataFlowInfo_taSEOS_")
	.dwattr $C$DW$1169, DW_AT_type(*$C$DW$T$502)
	.dwattr $C$DW$1169, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1170	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1170, DW_AT_type(*$C$DW$T$502)

	.dwendtag $C$DW$1169

	.dwattr $C$DW$T$507, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$T$507, DW_AT_decl_line(0x10e)
	.dwattr $C$DW$T$507, DW_AT_decl_column(0x01)
	.dwendtag $C$DW$T$507

	.dwendtag $C$DW$TU$507


$C$DW$TU$502	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$502
$C$DW$T$502	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$502, DW_AT_type(*$C$DW$T$507)
	.dwattr $C$DW$T$502, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$502


$C$DW$TU$505	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$505

$C$DW$T$505	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$505, DW_AT_type(*$C$DW$T$502)
$C$DW$1171	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1171, DW_AT_type(*$C$DW$T$504)

	.dwendtag $C$DW$T$505

	.dwendtag $C$DW$TU$505


$C$DW$TU$506	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$506

$C$DW$T$506	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$506, DW_AT_type(*$C$DW$T$502)
$C$DW$1172	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1172, DW_AT_type(*$C$DW$T$502)

	.dwendtag $C$DW$T$506

	.dwendtag $C$DW$TU$506


$C$DW$TU$503	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$503
$C$DW$T$503	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$503, DW_AT_type(*$C$DW$T$507)

	.dwendtag $C$DW$TU$503


$C$DW$TU$504	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$504
$C$DW$T$504	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$504, DW_AT_type(*$C$DW$T$503)
	.dwattr $C$DW$T$504, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$504


$C$DW$TU$538	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$538
$C$DW$T$538	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$538, DW_AT_name("sDataFlowInfo_t")
	.dwattr $C$DW$T$538, DW_AT_type(*$C$DW$T$507)
	.dwattr $C$DW$T$538, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$T$538, DW_AT_decl_line(0x12f)
	.dwattr $C$DW$T$538, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$538


$C$DW$TU$539	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$539

$C$DW$T$539	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$539, DW_AT_type(*$C$DW$T$538)
	.dwattr $C$DW$T$539, DW_AT_byte_size(0x4aa000)
$C$DW$1173	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$1173, DW_AT_upper_bound(0x3ff)

	.dwendtag $C$DW$T$539

	.dwendtag $C$DW$TU$539


$C$DW$TU$583	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$583
$C$DW$T$583	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$583, DW_AT_type(*$C$DW$T$538)
	.dwattr $C$DW$T$583, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$583


$C$DW$TU$524	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$524

$C$DW$T$524	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$524, DW_AT_name("sMemoryStats_t")
	.dwattr $C$DW$T$524, DW_AT_byte_size(0x88)
$C$DW$1174	.dwtag  DW_TAG_member
	.dwattr $C$DW$1174, DW_AT_type(*$C$DW$T$516)
	.dwattr $C$DW$1174, DW_AT_name("bandwidth")
	.dwattr $C$DW$1174, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1174, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$1174, DW_AT_decl_line(0x9e)
	.dwattr $C$DW$1174, DW_AT_decl_column(0x0a)

$C$DW$1175	.dwtag  DW_TAG_member
	.dwattr $C$DW$1175, DW_AT_type(*$C$DW$T$517)
	.dwattr $C$DW$1175, DW_AT_name("size")
	.dwattr $C$DW$1175, DW_AT_data_member_location[DW_OP_plus_uconst 0x30]
	.dwattr $C$DW$1175, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1175, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$1175, DW_AT_decl_line(0x9f)
	.dwattr $C$DW$1175, DW_AT_decl_column(0x0b)

$C$DW$1176	.dwtag  DW_TAG_member
	.dwattr $C$DW$1176, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1176, DW_AT_name("outSizeWithCoexc")
	.dwattr $C$DW$1176, DW_AT_data_member_location[DW_OP_plus_uconst 0x54]
	.dwattr $C$DW$1176, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1176, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$1176, DW_AT_decl_line(0xa0)
	.dwattr $C$DW$1176, DW_AT_decl_column(0x0b)

$C$DW$1177	.dwtag  DW_TAG_member
	.dwattr $C$DW$1177, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1177, DW_AT_name("outHeightWithCoexc")
	.dwattr $C$DW$1177, DW_AT_data_member_location[DW_OP_plus_uconst 0x58]
	.dwattr $C$DW$1177, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1177, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$1177, DW_AT_decl_line(0xa1)
	.dwattr $C$DW$1177, DW_AT_decl_column(0x0b)

$C$DW$1178	.dwtag  DW_TAG_member
	.dwattr $C$DW$1178, DW_AT_type(*$C$DW$T$518)
	.dwattr $C$DW$1178, DW_AT_name("memSpace")
	.dwattr $C$DW$1178, DW_AT_data_member_location[DW_OP_plus_uconst 0x5c]
	.dwattr $C$DW$1178, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1178, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$1178, DW_AT_decl_line(0xa2)
	.dwattr $C$DW$1178, DW_AT_decl_column(0x0b)

$C$DW$1179	.dwtag  DW_TAG_member
	.dwattr $C$DW$1179, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1179, DW_AT_name("inFeatMapReadFactor")
	.dwattr $C$DW$1179, DW_AT_data_member_location[DW_OP_plus_uconst 0x74]
	.dwattr $C$DW$1179, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1179, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$1179, DW_AT_decl_line(0xa3)
	.dwattr $C$DW$1179, DW_AT_decl_column(0x0b)

$C$DW$1180	.dwtag  DW_TAG_member
	.dwattr $C$DW$1180, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1180, DW_AT_name("wtFeatMapReadFactor")
	.dwattr $C$DW$1180, DW_AT_data_member_location[DW_OP_plus_uconst 0x78]
	.dwattr $C$DW$1180, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1180, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$1180, DW_AT_decl_line(0xa4)
	.dwattr $C$DW$1180, DW_AT_decl_column(0x0b)

$C$DW$1181	.dwtag  DW_TAG_member
	.dwattr $C$DW$1181, DW_AT_type(*$C$DW$T$17)
	.dwattr $C$DW$1181, DW_AT_name("wtVolumeFetchKB")
	.dwattr $C$DW$1181, DW_AT_data_member_location[DW_OP_plus_uconst 0x80]
	.dwattr $C$DW$1181, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1181, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$1181, DW_AT_decl_line(0xa5)
	.dwattr $C$DW$1181, DW_AT_decl_column(0x0a)


$C$DW$1182	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1182, DW_AT_name("operator =")
	.dwattr $C$DW$1182, DW_AT_declaration
	.dwattr $C$DW$1182, DW_AT_linkage_name("_ZN14sMemoryStats_taSERKS_")
	.dwattr $C$DW$1182, DW_AT_type(*$C$DW$T$519)
	.dwattr $C$DW$1182, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1183	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1183, DW_AT_type(*$C$DW$T$521)

	.dwendtag $C$DW$1182


$C$DW$1184	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1184, DW_AT_name("operator =")
	.dwattr $C$DW$1184, DW_AT_declaration
	.dwattr $C$DW$1184, DW_AT_linkage_name("_ZN14sMemoryStats_taSEOS_")
	.dwattr $C$DW$1184, DW_AT_type(*$C$DW$T$519)
	.dwattr $C$DW$1184, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1185	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1185, DW_AT_type(*$C$DW$T$519)

	.dwendtag $C$DW$1184

	.dwattr $C$DW$T$524, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$T$524, DW_AT_decl_line(0x9c)
	.dwattr $C$DW$T$524, DW_AT_decl_column(0x01)
	.dwendtag $C$DW$T$524

	.dwendtag $C$DW$TU$524


$C$DW$TU$519	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$519
$C$DW$T$519	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$519, DW_AT_type(*$C$DW$T$524)
	.dwattr $C$DW$T$519, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$519


$C$DW$TU$522	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$522

$C$DW$T$522	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$522, DW_AT_type(*$C$DW$T$519)
$C$DW$1186	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1186, DW_AT_type(*$C$DW$T$521)

	.dwendtag $C$DW$T$522

	.dwendtag $C$DW$TU$522


$C$DW$TU$523	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$523

$C$DW$T$523	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$523, DW_AT_type(*$C$DW$T$519)
$C$DW$1187	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1187, DW_AT_type(*$C$DW$T$519)

	.dwendtag $C$DW$T$523

	.dwendtag $C$DW$TU$523


$C$DW$TU$499	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$499
$C$DW$T$499	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$499, DW_AT_name("sMemoryStats_t")
	.dwattr $C$DW$T$499, DW_AT_type(*$C$DW$T$524)
	.dwattr $C$DW$T$499, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$T$499, DW_AT_decl_line(0xa6)
	.dwattr $C$DW$T$499, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$499


$C$DW$TU$520	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$520
$C$DW$T$520	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$520, DW_AT_type(*$C$DW$T$524)

	.dwendtag $C$DW$TU$520


$C$DW$TU$521	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$521
$C$DW$T$521	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$521, DW_AT_type(*$C$DW$T$520)
	.dwattr $C$DW$T$521, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$521


$C$DW$TU$534	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$534

$C$DW$T$534	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$534, DW_AT_name("sPerfSimConfig_t")
	.dwattr $C$DW$T$534, DW_AT_byte_size(0x107c)
$C$DW$1188	.dwtag  DW_TAG_member
	.dwattr $C$DW$1188, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1188, DW_AT_name("freqMHz")
	.dwattr $C$DW$1188, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1188, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$1188, DW_AT_decl_line(0x157)
	.dwattr $C$DW$1188, DW_AT_decl_column(0x0d)

$C$DW$1189	.dwtag  DW_TAG_member
	.dwattr $C$DW$1189, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1189, DW_AT_name("sizeL2MemKB")
	.dwattr $C$DW$1189, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$1189, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1189, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$1189, DW_AT_decl_line(0x158)
	.dwattr $C$DW$1189, DW_AT_decl_column(0x0d)

$C$DW$1190	.dwtag  DW_TAG_member
	.dwattr $C$DW$1190, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1190, DW_AT_name("matPanelSize")
	.dwattr $C$DW$1190, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$1190, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1190, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$1190, DW_AT_decl_line(0x159)
	.dwattr $C$DW$1190, DW_AT_decl_column(0x0d)

$C$DW$1191	.dwtag  DW_TAG_member
	.dwattr $C$DW$1191, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1191, DW_AT_name("drainLatnecy")
	.dwattr $C$DW$1191, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$1191, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1191, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$1191, DW_AT_decl_line(0x15a)
	.dwattr $C$DW$1191, DW_AT_decl_column(0x0d)

$C$DW$1192	.dwtag  DW_TAG_member
	.dwattr $C$DW$1192, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1192, DW_AT_name("setupCycles")
	.dwattr $C$DW$1192, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$1192, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1192, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$1192, DW_AT_decl_line(0x15b)
	.dwattr $C$DW$1192, DW_AT_decl_column(0x0d)

$C$DW$1193	.dwtag  DW_TAG_member
	.dwattr $C$DW$1193, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1193, DW_AT_name("layerSetupCycles")
	.dwattr $C$DW$1193, DW_AT_data_member_location[DW_OP_plus_uconst 0x14]
	.dwattr $C$DW$1193, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1193, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$1193, DW_AT_decl_line(0x15c)
	.dwattr $C$DW$1193, DW_AT_decl_column(0x0d)

$C$DW$1194	.dwtag  DW_TAG_member
	.dwattr $C$DW$1194, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1194, DW_AT_name("mmaCoreLoopOH")
	.dwattr $C$DW$1194, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$1194, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1194, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$1194, DW_AT_decl_line(0x15d)
	.dwattr $C$DW$1194, DW_AT_decl_column(0x0d)

$C$DW$1195	.dwtag  DW_TAG_member
	.dwattr $C$DW$1195, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1195, DW_AT_name("numCores")
	.dwattr $C$DW$1195, DW_AT_data_member_location[DW_OP_plus_uconst 0x1c]
	.dwattr $C$DW$1195, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1195, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$1195, DW_AT_decl_line(0x15e)
	.dwattr $C$DW$1195, DW_AT_decl_column(0x0d)

$C$DW$1196	.dwtag  DW_TAG_member
	.dwattr $C$DW$1196, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1196, DW_AT_name("sizeL3MemKB")
	.dwattr $C$DW$1196, DW_AT_data_member_location[DW_OP_plus_uconst 0x20]
	.dwattr $C$DW$1196, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1196, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$1196, DW_AT_decl_line(0x15f)
	.dwattr $C$DW$1196, DW_AT_decl_column(0x0d)

$C$DW$1197	.dwtag  DW_TAG_member
	.dwattr $C$DW$1197, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1197, DW_AT_name("busWidth_L3_L2")
	.dwattr $C$DW$1197, DW_AT_data_member_location[DW_OP_plus_uconst 0x24]
	.dwattr $C$DW$1197, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1197, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$1197, DW_AT_decl_line(0x160)
	.dwattr $C$DW$1197, DW_AT_decl_column(0x0d)

$C$DW$1198	.dwtag  DW_TAG_member
	.dwattr $C$DW$1198, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1198, DW_AT_name("busWidthDDR")
	.dwattr $C$DW$1198, DW_AT_data_member_location[DW_OP_plus_uconst 0x28]
	.dwattr $C$DW$1198, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1198, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$1198, DW_AT_decl_line(0x161)
	.dwattr $C$DW$1198, DW_AT_decl_column(0x0d)

$C$DW$1199	.dwtag  DW_TAG_member
	.dwattr $C$DW$1199, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1199, DW_AT_name("freqDDR")
	.dwattr $C$DW$1199, DW_AT_data_member_location[DW_OP_plus_uconst 0x2c]
	.dwattr $C$DW$1199, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1199, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$1199, DW_AT_decl_line(0x162)
	.dwattr $C$DW$1199, DW_AT_decl_column(0x0d)

$C$DW$1200	.dwtag  DW_TAG_member
	.dwattr $C$DW$1200, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1200, DW_AT_name("numEMIFPorts")
	.dwattr $C$DW$1200, DW_AT_data_member_location[DW_OP_plus_uconst 0x30]
	.dwattr $C$DW$1200, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1200, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$1200, DW_AT_decl_line(0x163)
	.dwattr $C$DW$1200, DW_AT_decl_column(0x0d)

$C$DW$1201	.dwtag  DW_TAG_member
	.dwattr $C$DW$1201, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$1201, DW_AT_name("ddrEfficiency")
	.dwattr $C$DW$1201, DW_AT_data_member_location[DW_OP_plus_uconst 0x34]
	.dwattr $C$DW$1201, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1201, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$1201, DW_AT_decl_line(0x164)
	.dwattr $C$DW$1201, DW_AT_decl_column(0x0d)

$C$DW$1202	.dwtag  DW_TAG_member
	.dwattr $C$DW$1202, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$1202, DW_AT_name("L2Efficiency")
	.dwattr $C$DW$1202, DW_AT_data_member_location[DW_OP_plus_uconst 0x38]
	.dwattr $C$DW$1202, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1202, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$1202, DW_AT_decl_line(0x165)
	.dwattr $C$DW$1202, DW_AT_decl_column(0x0d)

$C$DW$1203	.dwtag  DW_TAG_member
	.dwattr $C$DW$1203, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$1203, DW_AT_name("msmcEfficiency")
	.dwattr $C$DW$1203, DW_AT_data_member_location[DW_OP_plus_uconst 0x3c]
	.dwattr $C$DW$1203, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1203, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$1203, DW_AT_decl_line(0x166)
	.dwattr $C$DW$1203, DW_AT_decl_column(0x0d)

$C$DW$1204	.dwtag  DW_TAG_member
	.dwattr $C$DW$1204, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1204, DW_AT_name("dataType")
	.dwattr $C$DW$1204, DW_AT_data_member_location[DW_OP_plus_uconst 0x40]
	.dwattr $C$DW$1204, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1204, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$1204, DW_AT_decl_line(0x167)
	.dwattr $C$DW$1204, DW_AT_decl_column(0x0d)

$C$DW$1205	.dwtag  DW_TAG_member
	.dwattr $C$DW$1205, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1205, DW_AT_name("mmaDoesPooling")
	.dwattr $C$DW$1205, DW_AT_data_member_location[DW_OP_plus_uconst 0x44]
	.dwattr $C$DW$1205, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1205, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$1205, DW_AT_decl_line(0x168)
	.dwattr $C$DW$1205, DW_AT_decl_column(0x0d)

$C$DW$1206	.dwtag  DW_TAG_member
	.dwattr $C$DW$1206, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1206, DW_AT_name("numBiasInstance")
	.dwattr $C$DW$1206, DW_AT_data_member_location[DW_OP_plus_uconst 0x48]
	.dwattr $C$DW$1206, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1206, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$1206, DW_AT_decl_line(0x169)
	.dwattr $C$DW$1206, DW_AT_decl_column(0x0d)

$C$DW$1207	.dwtag  DW_TAG_member
	.dwattr $C$DW$1207, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1207, DW_AT_name("alignmentOption")
	.dwattr $C$DW$1207, DW_AT_data_member_location[DW_OP_plus_uconst 0x4c]
	.dwattr $C$DW$1207, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1207, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$1207, DW_AT_decl_line(0x16a)
	.dwattr $C$DW$1207, DW_AT_decl_column(0x0d)

$C$DW$1208	.dwtag  DW_TAG_member
	.dwattr $C$DW$1208, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1208, DW_AT_name("fileFormatNet")
	.dwattr $C$DW$1208, DW_AT_data_member_location[DW_OP_plus_uconst 0x50]
	.dwattr $C$DW$1208, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1208, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$1208, DW_AT_decl_line(0x16b)
	.dwattr $C$DW$1208, DW_AT_decl_column(0x0d)

$C$DW$1209	.dwtag  DW_TAG_member
	.dwattr $C$DW$1209, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1209, DW_AT_name("batchSize")
	.dwattr $C$DW$1209, DW_AT_data_member_location[DW_OP_plus_uconst 0x54]
	.dwattr $C$DW$1209, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1209, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$1209, DW_AT_decl_line(0x16c)
	.dwattr $C$DW$1209, DW_AT_decl_column(0x0d)

$C$DW$1210	.dwtag  DW_TAG_member
	.dwattr $C$DW$1210, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1210, DW_AT_name("deviceName")
	.dwattr $C$DW$1210, DW_AT_data_member_location[DW_OP_plus_uconst 0x58]
	.dwattr $C$DW$1210, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1210, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$1210, DW_AT_decl_line(0x16d)
	.dwattr $C$DW$1210, DW_AT_decl_column(0x0d)

$C$DW$1211	.dwtag  DW_TAG_member
	.dwattr $C$DW$1211, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1211, DW_AT_name("enablePersistWtAlloc")
	.dwattr $C$DW$1211, DW_AT_data_member_location[DW_OP_plus_uconst 0x5c]
	.dwattr $C$DW$1211, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1211, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$1211, DW_AT_decl_line(0x16e)
	.dwattr $C$DW$1211, DW_AT_decl_column(0x0d)

$C$DW$1212	.dwtag  DW_TAG_member
	.dwattr $C$DW$1212, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1212, DW_AT_name("numPanelSplitsVertical")
	.dwattr $C$DW$1212, DW_AT_data_member_location[DW_OP_plus_uconst 0x60]
	.dwattr $C$DW$1212, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1212, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$1212, DW_AT_decl_line(0x170)
	.dwattr $C$DW$1212, DW_AT_decl_column(0x0d)

$C$DW$1213	.dwtag  DW_TAG_member
	.dwattr $C$DW$1213, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1213, DW_AT_name("numWritePortsL3")
	.dwattr $C$DW$1213, DW_AT_data_member_location[DW_OP_plus_uconst 0x64]
	.dwattr $C$DW$1213, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1213, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$1213, DW_AT_decl_line(0x171)
	.dwattr $C$DW$1213, DW_AT_decl_column(0x0d)

$C$DW$1214	.dwtag  DW_TAG_member
	.dwattr $C$DW$1214, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1214, DW_AT_name("numSEReadPortsL3")
	.dwattr $C$DW$1214, DW_AT_data_member_location[DW_OP_plus_uconst 0x68]
	.dwattr $C$DW$1214, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1214, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$1214, DW_AT_decl_line(0x172)
	.dwattr $C$DW$1214, DW_AT_decl_column(0x0d)

$C$DW$1215	.dwtag  DW_TAG_member
	.dwattr $C$DW$1215, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1215, DW_AT_name("numSEreadPortsL2")
	.dwattr $C$DW$1215, DW_AT_data_member_location[DW_OP_plus_uconst 0x6c]
	.dwattr $C$DW$1215, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1215, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$1215, DW_AT_decl_line(0x173)
	.dwattr $C$DW$1215, DW_AT_decl_column(0x0d)

$C$DW$1216	.dwtag  DW_TAG_member
	.dwattr $C$DW$1216, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1216, DW_AT_name("numDRUPorts")
	.dwattr $C$DW$1216, DW_AT_data_member_location[DW_OP_plus_uconst 0x70]
	.dwattr $C$DW$1216, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1216, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$1216, DW_AT_decl_line(0x174)
	.dwattr $C$DW$1216, DW_AT_decl_column(0x0d)

$C$DW$1217	.dwtag  DW_TAG_member
	.dwattr $C$DW$1217, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1217, DW_AT_name("BPanelWritePorts")
	.dwattr $C$DW$1217, DW_AT_data_member_location[DW_OP_plus_uconst 0x74]
	.dwattr $C$DW$1217, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1217, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$1217, DW_AT_decl_line(0x175)
	.dwattr $C$DW$1217, DW_AT_decl_column(0x0d)

$C$DW$1218	.dwtag  DW_TAG_member
	.dwattr $C$DW$1218, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1218, DW_AT_name("numLUTWriteMMA")
	.dwattr $C$DW$1218, DW_AT_data_member_location[DW_OP_plus_uconst 0x78]
	.dwattr $C$DW$1218, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1218, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$1218, DW_AT_decl_line(0x176)
	.dwattr $C$DW$1218, DW_AT_decl_column(0x0d)

$C$DW$1219	.dwtag  DW_TAG_member
	.dwattr $C$DW$1219, DW_AT_type(*$C$DW$T$528)
	.dwattr $C$DW$1219, DW_AT_name("ddrLayers")
	.dwattr $C$DW$1219, DW_AT_data_member_location[DW_OP_plus_uconst 0x7c]
	.dwattr $C$DW$1219, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1219, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$1219, DW_AT_decl_line(0x178)
	.dwattr $C$DW$1219, DW_AT_decl_column(0x0d)


$C$DW$1220	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1220, DW_AT_name("operator =")
	.dwattr $C$DW$1220, DW_AT_declaration
	.dwattr $C$DW$1220, DW_AT_linkage_name("_ZN16sPerfSimConfig_taSERKS_")
	.dwattr $C$DW$1220, DW_AT_type(*$C$DW$T$529)
	.dwattr $C$DW$1220, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1221	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1221, DW_AT_type(*$C$DW$T$531)

	.dwendtag $C$DW$1220


$C$DW$1222	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1222, DW_AT_name("operator =")
	.dwattr $C$DW$1222, DW_AT_declaration
	.dwattr $C$DW$1222, DW_AT_linkage_name("_ZN16sPerfSimConfig_taSEOS_")
	.dwattr $C$DW$1222, DW_AT_type(*$C$DW$T$529)
	.dwattr $C$DW$1222, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1223	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1223, DW_AT_type(*$C$DW$T$529)

	.dwendtag $C$DW$1222

	.dwattr $C$DW$T$534, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$T$534, DW_AT_decl_line(0x156)
	.dwattr $C$DW$T$534, DW_AT_decl_column(0x01)
	.dwendtag $C$DW$T$534

	.dwendtag $C$DW$TU$534


$C$DW$TU$529	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$529
$C$DW$T$529	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$529, DW_AT_type(*$C$DW$T$534)
	.dwattr $C$DW$T$529, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$529


$C$DW$TU$532	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$532

$C$DW$T$532	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$532, DW_AT_type(*$C$DW$T$529)
$C$DW$1224	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1224, DW_AT_type(*$C$DW$T$531)

	.dwendtag $C$DW$T$532

	.dwendtag $C$DW$TU$532


$C$DW$TU$533	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$533

$C$DW$T$533	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$533, DW_AT_type(*$C$DW$T$529)
$C$DW$1225	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1225, DW_AT_type(*$C$DW$T$529)

	.dwendtag $C$DW$T$533

	.dwendtag $C$DW$TU$533


$C$DW$TU$530	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$530
$C$DW$T$530	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$530, DW_AT_type(*$C$DW$T$534)

	.dwendtag $C$DW$TU$530


$C$DW$TU$531	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$531
$C$DW$T$531	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$531, DW_AT_type(*$C$DW$T$530)
	.dwattr $C$DW$T$531, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$531


$C$DW$TU$537	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$537
$C$DW$T$537	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$537, DW_AT_name("sPerfSimConfig_t")
	.dwattr $C$DW$T$537, DW_AT_type(*$C$DW$T$534)
	.dwattr $C$DW$T$537, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$T$537, DW_AT_decl_line(0x17a)
	.dwattr $C$DW$T$537, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$537


$C$DW$TU$545	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$545

$C$DW$T$545	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$545, DW_AT_name("sPerfSim_t")
	.dwattr $C$DW$T$545, DW_AT_byte_size(0x4ac088)
$C$DW$1226	.dwtag  DW_TAG_member
	.dwattr $C$DW$1226, DW_AT_type(*$C$DW$T$537)
	.dwattr $C$DW$1226, DW_AT_name("simConfig")
	.dwattr $C$DW$1226, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1226, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$1226, DW_AT_decl_line(0x186)
	.dwattr $C$DW$1226, DW_AT_decl_column(0x16)

$C$DW$1227	.dwtag  DW_TAG_member
	.dwattr $C$DW$1227, DW_AT_type(*$C$DW$T$500)
	.dwattr $C$DW$1227, DW_AT_name("memorySizeRequirement")
	.dwattr $C$DW$1227, DW_AT_data_member_location[DW_OP_plus_uconst 0x107c]
	.dwattr $C$DW$1227, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1227, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$1227, DW_AT_decl_line(0x187)
	.dwattr $C$DW$1227, DW_AT_decl_column(0x16)

$C$DW$1228	.dwtag  DW_TAG_member
	.dwattr $C$DW$1228, DW_AT_type(*$C$DW$T$539)
	.dwattr $C$DW$1228, DW_AT_name("sdataFlowInfo")
	.dwattr $C$DW$1228, DW_AT_data_member_location[DW_OP_plus_uconst 0x1088]
	.dwattr $C$DW$1228, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1228, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$1228, DW_AT_decl_line(0x188)
	.dwattr $C$DW$1228, DW_AT_decl_column(0x16)

$C$DW$1229	.dwtag  DW_TAG_member
	.dwattr $C$DW$1229, DW_AT_type(*$C$DW$T$528)
	.dwattr $C$DW$1229, DW_AT_name("layerExecutionOrder")
	.dwattr $C$DW$1229, DW_AT_data_member_location[DW_OP_plus_uconst 0x4ab088]
	.dwattr $C$DW$1229, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1229, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$1229, DW_AT_decl_line(0x189)
	.dwattr $C$DW$1229, DW_AT_decl_column(0x16)


$C$DW$1230	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1230, DW_AT_name("operator =")
	.dwattr $C$DW$1230, DW_AT_declaration
	.dwattr $C$DW$1230, DW_AT_linkage_name("_ZN10sPerfSim_taSERKS_")
	.dwattr $C$DW$1230, DW_AT_type(*$C$DW$T$540)
	.dwattr $C$DW$1230, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1231	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1231, DW_AT_type(*$C$DW$T$542)

	.dwendtag $C$DW$1230


$C$DW$1232	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1232, DW_AT_name("operator =")
	.dwattr $C$DW$1232, DW_AT_declaration
	.dwattr $C$DW$1232, DW_AT_linkage_name("_ZN10sPerfSim_taSEOS_")
	.dwattr $C$DW$1232, DW_AT_type(*$C$DW$T$540)
	.dwattr $C$DW$1232, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1233	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1233, DW_AT_type(*$C$DW$T$540)

	.dwendtag $C$DW$1232

	.dwattr $C$DW$T$545, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$T$545, DW_AT_decl_line(0x185)
	.dwattr $C$DW$T$545, DW_AT_decl_column(0x01)
	.dwendtag $C$DW$T$545

	.dwendtag $C$DW$TU$545


$C$DW$TU$540	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$540
$C$DW$T$540	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$540, DW_AT_type(*$C$DW$T$545)
	.dwattr $C$DW$T$540, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$540


$C$DW$TU$543	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$543

$C$DW$T$543	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$543, DW_AT_type(*$C$DW$T$540)
$C$DW$1234	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1234, DW_AT_type(*$C$DW$T$542)

	.dwendtag $C$DW$T$543

	.dwendtag $C$DW$TU$543


$C$DW$TU$544	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$544

$C$DW$T$544	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$544, DW_AT_type(*$C$DW$T$540)
$C$DW$1235	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1235, DW_AT_type(*$C$DW$T$540)

	.dwendtag $C$DW$T$544

	.dwendtag $C$DW$TU$544


$C$DW$TU$368	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$368
$C$DW$T$368	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$368, DW_AT_name("sPerfSim_t")
	.dwattr $C$DW$T$368, DW_AT_type(*$C$DW$T$545)
	.dwattr $C$DW$T$368, DW_AT_decl_file("../utils/perfsim/perfsim.h")
	.dwattr $C$DW$T$368, DW_AT_decl_line(0x18a)
	.dwattr $C$DW$T$368, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$368


$C$DW$TU$369	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$369
$C$DW$T$369	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$369, DW_AT_type(*$C$DW$T$368)
	.dwattr $C$DW$T$369, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$369


$C$DW$TU$541	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$541
$C$DW$T$541	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$541, DW_AT_type(*$C$DW$T$545)

	.dwendtag $C$DW$TU$541


$C$DW$TU$542	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$542
$C$DW$T$542	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$542, DW_AT_type(*$C$DW$T$541)
	.dwattr $C$DW$T$542, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$542


$C$DW$TU$555	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$555

$C$DW$T$555	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$555, DW_AT_name("sTIDL_ALgArgMaxLayer_t")
	.dwattr $C$DW$T$555, DW_AT_byte_size(0x08)
$C$DW$1236	.dwtag  DW_TAG_member
	.dwattr $C$DW$1236, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$1236, DW_AT_name("reserved")
	.dwattr $C$DW$1236, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1236, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1236, DW_AT_decl_line(0x2ae)
	.dwattr $C$DW$1236, DW_AT_decl_column(0x09)


$C$DW$1237	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1237, DW_AT_name("operator =")
	.dwattr $C$DW$1237, DW_AT_declaration
	.dwattr $C$DW$1237, DW_AT_linkage_name("_ZN22sTIDL_ALgArgMaxLayer_taSERKS_")
	.dwattr $C$DW$1237, DW_AT_type(*$C$DW$T$550)
	.dwattr $C$DW$1237, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1238	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1238, DW_AT_type(*$C$DW$T$552)

	.dwendtag $C$DW$1237


$C$DW$1239	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1239, DW_AT_name("operator =")
	.dwattr $C$DW$1239, DW_AT_declaration
	.dwattr $C$DW$1239, DW_AT_linkage_name("_ZN22sTIDL_ALgArgMaxLayer_taSEOS_")
	.dwattr $C$DW$1239, DW_AT_type(*$C$DW$T$550)
	.dwattr $C$DW$1239, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1240	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1240, DW_AT_type(*$C$DW$T$550)

	.dwendtag $C$DW$1239

	.dwattr $C$DW$T$555, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$T$555, DW_AT_decl_line(0x2ad)
	.dwattr $C$DW$T$555, DW_AT_decl_column(0x10)
	.dwendtag $C$DW$T$555

	.dwendtag $C$DW$TU$555


$C$DW$TU$550	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$550
$C$DW$T$550	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$550, DW_AT_type(*$C$DW$T$555)
	.dwattr $C$DW$T$550, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$550


$C$DW$TU$553	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$553

$C$DW$T$553	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$553, DW_AT_type(*$C$DW$T$550)
$C$DW$1241	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1241, DW_AT_type(*$C$DW$T$552)

	.dwendtag $C$DW$T$553

	.dwendtag $C$DW$TU$553


$C$DW$TU$554	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$554

$C$DW$T$554	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$554, DW_AT_type(*$C$DW$T$550)
$C$DW$1242	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1242, DW_AT_type(*$C$DW$T$550)

	.dwendtag $C$DW$T$554

	.dwendtag $C$DW$TU$554


$C$DW$TU$551	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$551
$C$DW$T$551	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$551, DW_AT_type(*$C$DW$T$555)

	.dwendtag $C$DW$TU$551


$C$DW$TU$552	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$552
$C$DW$T$552	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$552, DW_AT_type(*$C$DW$T$551)
	.dwattr $C$DW$T$552, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$552


$C$DW$TU$669	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$669
$C$DW$T$669	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$669, DW_AT_name("sTIDL_ALgArgMaxLayer_t")
	.dwattr $C$DW$T$669, DW_AT_type(*$C$DW$T$555)
	.dwattr $C$DW$T$669, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$T$669, DW_AT_decl_line(0x2af)
	.dwattr $C$DW$T$669, DW_AT_decl_column(0x02)

	.dwendtag $C$DW$TU$669


$C$DW$TU$562	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$562

$C$DW$T$562	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$562, DW_AT_name("sTIDL_ALgBatchNormParams_t")
	.dwattr $C$DW$T$562, DW_AT_byte_size(0x30)
$C$DW$1243	.dwtag  DW_TAG_member
	.dwattr $C$DW$1243, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$1243, DW_AT_name("slopeFactMem")
	.dwattr $C$DW$1243, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1243, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1243, DW_AT_decl_line(0x2e8)
	.dwattr $C$DW$1243, DW_AT_decl_column(0x09)

$C$DW$1244	.dwtag  DW_TAG_member
	.dwattr $C$DW$1244, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1244, DW_AT_name("slopeFactSize")
	.dwattr $C$DW$1244, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$1244, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1244, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1244, DW_AT_decl_line(0x2e9)
	.dwattr $C$DW$1244, DW_AT_decl_column(0x0b)

$C$DW$1245	.dwtag  DW_TAG_member
	.dwattr $C$DW$1245, DW_AT_type(*$C$DW$T$365)
	.dwattr $C$DW$1245, DW_AT_name("slopeFact")
	.dwattr $C$DW$1245, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$1245, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1245, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1245, DW_AT_decl_line(0x2ea)
	.dwattr $C$DW$1245, DW_AT_decl_column(0x0c)

$C$DW$1246	.dwtag  DW_TAG_member
	.dwattr $C$DW$1246, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$1246, DW_AT_name("biasParamMem")
	.dwattr $C$DW$1246, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$1246, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1246, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1246, DW_AT_decl_line(0x2eb)
	.dwattr $C$DW$1246, DW_AT_decl_column(0x0a)

$C$DW$1247	.dwtag  DW_TAG_member
	.dwattr $C$DW$1247, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1247, DW_AT_name("biasParamSize")
	.dwattr $C$DW$1247, DW_AT_data_member_location[DW_OP_plus_uconst 0x20]
	.dwattr $C$DW$1247, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1247, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1247, DW_AT_decl_line(0x2ec)
	.dwattr $C$DW$1247, DW_AT_decl_column(0x0b)

$C$DW$1248	.dwtag  DW_TAG_member
	.dwattr $C$DW$1248, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1248, DW_AT_name("biasB")
	.dwattr $C$DW$1248, DW_AT_data_member_location[DW_OP_plus_uconst 0x24]
	.dwattr $C$DW$1248, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1248, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1248, DW_AT_decl_line(0x2ed)
	.dwattr $C$DW$1248, DW_AT_decl_column(0x0b)

$C$DW$1249	.dwtag  DW_TAG_member
	.dwattr $C$DW$1249, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1249, DW_AT_name("isMmaCiAvailable")
	.dwattr $C$DW$1249, DW_AT_data_member_location[DW_OP_plus_uconst 0x28]
	.dwattr $C$DW$1249, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1249, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1249, DW_AT_decl_line(0x2ee)
	.dwattr $C$DW$1249, DW_AT_decl_column(0x0b)

$C$DW$1250	.dwtag  DW_TAG_member
	.dwattr $C$DW$1250, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1250, DW_AT_name("inNumBytes")
	.dwattr $C$DW$1250, DW_AT_data_member_location[DW_OP_plus_uconst 0x2c]
	.dwattr $C$DW$1250, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1250, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1250, DW_AT_decl_line(0x2ef)
	.dwattr $C$DW$1250, DW_AT_decl_column(0x0b)


$C$DW$1251	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1251, DW_AT_name("operator =")
	.dwattr $C$DW$1251, DW_AT_declaration
	.dwattr $C$DW$1251, DW_AT_linkage_name("_ZN26sTIDL_ALgBatchNormParams_taSERKS_")
	.dwattr $C$DW$1251, DW_AT_type(*$C$DW$T$557)
	.dwattr $C$DW$1251, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1252	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1252, DW_AT_type(*$C$DW$T$559)

	.dwendtag $C$DW$1251


$C$DW$1253	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1253, DW_AT_name("operator =")
	.dwattr $C$DW$1253, DW_AT_declaration
	.dwattr $C$DW$1253, DW_AT_linkage_name("_ZN26sTIDL_ALgBatchNormParams_taSEOS_")
	.dwattr $C$DW$1253, DW_AT_type(*$C$DW$T$557)
	.dwattr $C$DW$1253, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1254	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1254, DW_AT_type(*$C$DW$T$557)

	.dwendtag $C$DW$1253

	.dwattr $C$DW$T$562, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$T$562, DW_AT_decl_line(0x2e7)
	.dwattr $C$DW$T$562, DW_AT_decl_column(0x10)
	.dwendtag $C$DW$T$562

	.dwendtag $C$DW$TU$562


$C$DW$TU$557	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$557
$C$DW$T$557	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$557, DW_AT_type(*$C$DW$T$562)
	.dwattr $C$DW$T$557, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$557


$C$DW$TU$560	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$560

$C$DW$T$560	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$560, DW_AT_type(*$C$DW$T$557)
$C$DW$1255	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1255, DW_AT_type(*$C$DW$T$559)

	.dwendtag $C$DW$T$560

	.dwendtag $C$DW$TU$560


$C$DW$TU$561	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$561

$C$DW$T$561	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$561, DW_AT_type(*$C$DW$T$557)
$C$DW$1256	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1256, DW_AT_type(*$C$DW$T$557)

	.dwendtag $C$DW$T$561

	.dwendtag $C$DW$TU$561


$C$DW$TU$558	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$558
$C$DW$T$558	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$558, DW_AT_type(*$C$DW$T$562)

	.dwendtag $C$DW$TU$558


$C$DW$TU$559	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$559
$C$DW$T$559	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$559, DW_AT_type(*$C$DW$T$558)
	.dwattr $C$DW$T$559, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$559


$C$DW$TU$674	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$674
$C$DW$T$674	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$674, DW_AT_name("sTIDL_ALgBatchNormParams_t")
	.dwattr $C$DW$T$674, DW_AT_type(*$C$DW$T$562)
	.dwattr $C$DW$T$674, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$T$674, DW_AT_decl_line(0x2f0)
	.dwattr $C$DW$T$674, DW_AT_decl_column(0x02)

	.dwendtag $C$DW$TU$674


$C$DW$TU$569	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$569

$C$DW$T$569	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$569, DW_AT_name("sTIDL_ALgColorConversionParams_t")
	.dwattr $C$DW$T$569, DW_AT_byte_size(0x08)
$C$DW$1257	.dwtag  DW_TAG_member
	.dwattr $C$DW$1257, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$1257, DW_AT_name("reserved")
	.dwattr $C$DW$1257, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1257, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1257, DW_AT_decl_line(0x2bb)
	.dwattr $C$DW$1257, DW_AT_decl_column(0x09)


$C$DW$1258	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1258, DW_AT_name("operator =")
	.dwattr $C$DW$1258, DW_AT_declaration
	.dwattr $C$DW$1258, DW_AT_linkage_name("_ZN32sTIDL_ALgColorConversionParams_taSERKS_")
	.dwattr $C$DW$1258, DW_AT_type(*$C$DW$T$564)
	.dwattr $C$DW$1258, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1259	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1259, DW_AT_type(*$C$DW$T$566)

	.dwendtag $C$DW$1258


$C$DW$1260	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1260, DW_AT_name("operator =")
	.dwattr $C$DW$1260, DW_AT_declaration
	.dwattr $C$DW$1260, DW_AT_linkage_name("_ZN32sTIDL_ALgColorConversionParams_taSEOS_")
	.dwattr $C$DW$1260, DW_AT_type(*$C$DW$T$564)
	.dwattr $C$DW$1260, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1261	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1261, DW_AT_type(*$C$DW$T$564)

	.dwendtag $C$DW$1260

	.dwattr $C$DW$T$569, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$T$569, DW_AT_decl_line(0x2ba)
	.dwattr $C$DW$T$569, DW_AT_decl_column(0x10)
	.dwendtag $C$DW$T$569

	.dwendtag $C$DW$TU$569


$C$DW$TU$564	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$564
$C$DW$T$564	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$564, DW_AT_type(*$C$DW$T$569)
	.dwattr $C$DW$T$564, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$564


$C$DW$TU$567	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$567

$C$DW$T$567	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$567, DW_AT_type(*$C$DW$T$564)
$C$DW$1262	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1262, DW_AT_type(*$C$DW$T$566)

	.dwendtag $C$DW$T$567

	.dwendtag $C$DW$TU$567


$C$DW$TU$568	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$568

$C$DW$T$568	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$568, DW_AT_type(*$C$DW$T$564)
$C$DW$1263	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1263, DW_AT_type(*$C$DW$T$564)

	.dwendtag $C$DW$T$568

	.dwendtag $C$DW$TU$568


$C$DW$TU$565	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$565
$C$DW$T$565	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$565, DW_AT_type(*$C$DW$T$569)

	.dwendtag $C$DW$TU$565


$C$DW$TU$566	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$566
$C$DW$T$566	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$566, DW_AT_type(*$C$DW$T$565)
	.dwattr $C$DW$T$566, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$566


$C$DW$TU$675	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$675
$C$DW$T$675	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$675, DW_AT_name("sTIDL_ALgColorConversionParams_t")
	.dwattr $C$DW$T$675, DW_AT_type(*$C$DW$T$569)
	.dwattr $C$DW$T$675, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$T$675, DW_AT_decl_line(0x2bc)
	.dwattr $C$DW$T$675, DW_AT_decl_column(0x02)

	.dwendtag $C$DW$TU$675


$C$DW$TU$578	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$578

$C$DW$T$578	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$578, DW_AT_name("sTIDL_ALgConcatParams_t")
	.dwattr $C$DW$T$578, DW_AT_byte_size(0x88)
$C$DW$1264	.dwtag  DW_TAG_member
	.dwattr $C$DW$1264, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1264, DW_AT_name("outOffsetIncPerTensor")
	.dwattr $C$DW$1264, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1264, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1264, DW_AT_decl_line(0x2e1)
	.dwattr $C$DW$1264, DW_AT_decl_column(0x0b)

$C$DW$1265	.dwtag  DW_TAG_member
	.dwattr $C$DW$1265, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1265, DW_AT_name("isMmaCiAvailable")
	.dwattr $C$DW$1265, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$1265, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1265, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1265, DW_AT_decl_line(0x2e2)
	.dwattr $C$DW$1265, DW_AT_decl_column(0x0b)

$C$DW$1266	.dwtag  DW_TAG_member
	.dwattr $C$DW$1266, DW_AT_type(*$C$DW$T$571)
	.dwattr $C$DW$1266, DW_AT_name("mma_config_reg")
	.dwattr $C$DW$1266, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$1266, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1266, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1266, DW_AT_decl_line(0x2e3)
	.dwattr $C$DW$1266, DW_AT_decl_column(0x17)

$C$DW$1267	.dwtag  DW_TAG_member
	.dwattr $C$DW$1267, DW_AT_type(*$C$DW$T$572)
	.dwattr $C$DW$1267, DW_AT_name("mma_offset_reg")
	.dwattr $C$DW$1267, DW_AT_data_member_location[DW_OP_plus_uconst 0x48]
	.dwattr $C$DW$1267, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1267, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1267, DW_AT_decl_line(0x2e4)
	.dwattr $C$DW$1267, DW_AT_decl_column(0x14)


$C$DW$1268	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1268, DW_AT_name("operator =")
	.dwattr $C$DW$1268, DW_AT_declaration
	.dwattr $C$DW$1268, DW_AT_linkage_name("_ZN23sTIDL_ALgConcatParams_taSERKS_")
	.dwattr $C$DW$1268, DW_AT_type(*$C$DW$T$573)
	.dwattr $C$DW$1268, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1269	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1269, DW_AT_type(*$C$DW$T$575)

	.dwendtag $C$DW$1268


$C$DW$1270	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1270, DW_AT_name("operator =")
	.dwattr $C$DW$1270, DW_AT_declaration
	.dwattr $C$DW$1270, DW_AT_linkage_name("_ZN23sTIDL_ALgConcatParams_taSEOS_")
	.dwattr $C$DW$1270, DW_AT_type(*$C$DW$T$573)
	.dwattr $C$DW$1270, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1271	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1271, DW_AT_type(*$C$DW$T$573)

	.dwendtag $C$DW$1270

	.dwattr $C$DW$T$578, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$T$578, DW_AT_decl_line(0x2e0)
	.dwattr $C$DW$T$578, DW_AT_decl_column(0x10)
	.dwendtag $C$DW$T$578

	.dwendtag $C$DW$TU$578


$C$DW$TU$573	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$573
$C$DW$T$573	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$573, DW_AT_type(*$C$DW$T$578)
	.dwattr $C$DW$T$573, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$573


$C$DW$TU$576	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$576

$C$DW$T$576	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$576, DW_AT_type(*$C$DW$T$573)
$C$DW$1272	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1272, DW_AT_type(*$C$DW$T$575)

	.dwendtag $C$DW$T$576

	.dwendtag $C$DW$TU$576


$C$DW$TU$577	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$577

$C$DW$T$577	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$577, DW_AT_type(*$C$DW$T$573)
$C$DW$1273	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1273, DW_AT_type(*$C$DW$T$573)

	.dwendtag $C$DW$T$577

	.dwendtag $C$DW$TU$577


$C$DW$TU$574	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$574
$C$DW$T$574	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$574, DW_AT_type(*$C$DW$T$578)

	.dwendtag $C$DW$TU$574


$C$DW$TU$575	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$575
$C$DW$T$575	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$575, DW_AT_type(*$C$DW$T$574)
	.dwattr $C$DW$T$575, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$575


$C$DW$TU$673	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$673
$C$DW$T$673	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$673, DW_AT_name("sTIDL_ALgConcatParams_t")
	.dwattr $C$DW$T$673, DW_AT_type(*$C$DW$T$578)
	.dwattr $C$DW$T$673, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$T$673, DW_AT_decl_line(0x2e5)
	.dwattr $C$DW$T$673, DW_AT_decl_column(0x02)

	.dwendtag $C$DW$TU$673


$C$DW$TU$590	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$590

$C$DW$T$590	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$590, DW_AT_name("sTIDL_ALgConvParams_t")
	.dwattr $C$DW$T$590, DW_AT_byte_size(0x2080)
$C$DW$1274	.dwtag  DW_TAG_member
	.dwattr $C$DW$1274, DW_AT_type(*$C$DW$T$582)
	.dwattr $C$DW$1274, DW_AT_name("pCoeffs")
	.dwattr $C$DW$1274, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1274, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1274, DW_AT_decl_line(0x28d)
	.dwattr $C$DW$1274, DW_AT_decl_column(0x0a)

$C$DW$1275	.dwtag  DW_TAG_member
	.dwattr $C$DW$1275, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$1275, DW_AT_name("pCoeffStagePtr")
	.dwattr $C$DW$1275, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$1275, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1275, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1275, DW_AT_decl_line(0x28e)
	.dwattr $C$DW$1275, DW_AT_decl_column(0x0a)

$C$DW$1276	.dwtag  DW_TAG_member
	.dwattr $C$DW$1276, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$1276, DW_AT_name("tempOutPr")
	.dwattr $C$DW$1276, DW_AT_data_member_location[DW_OP_plus_uconst 0x20]
	.dwattr $C$DW$1276, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1276, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1276, DW_AT_decl_line(0x28f)
	.dwattr $C$DW$1276, DW_AT_decl_column(0x0a)

$C$DW$1277	.dwtag  DW_TAG_member
	.dwattr $C$DW$1277, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$1277, DW_AT_name("biasParamMem")
	.dwattr $C$DW$1277, DW_AT_data_member_location[DW_OP_plus_uconst 0x28]
	.dwattr $C$DW$1277, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1277, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1277, DW_AT_decl_line(0x290)
	.dwattr $C$DW$1277, DW_AT_decl_column(0x0a)

$C$DW$1278	.dwtag  DW_TAG_member
	.dwattr $C$DW$1278, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$1278, DW_AT_name("outRoundBitsPtr")
	.dwattr $C$DW$1278, DW_AT_data_member_location[DW_OP_plus_uconst 0x30]
	.dwattr $C$DW$1278, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1278, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1278, DW_AT_decl_line(0x292)
	.dwattr $C$DW$1278, DW_AT_decl_column(0x0a)

$C$DW$1279	.dwtag  DW_TAG_member
	.dwattr $C$DW$1279, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$1279, DW_AT_name("biasBPtr")
	.dwattr $C$DW$1279, DW_AT_data_member_location[DW_OP_plus_uconst 0x38]
	.dwattr $C$DW$1279, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1279, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1279, DW_AT_decl_line(0x294)
	.dwattr $C$DW$1279, DW_AT_decl_column(0x0a)

$C$DW$1280	.dwtag  DW_TAG_member
	.dwattr $C$DW$1280, DW_AT_type(*$C$DW$T$582)
	.dwattr $C$DW$1280, DW_AT_name("mmaHandleArgsMem")
	.dwattr $C$DW$1280, DW_AT_data_member_location[DW_OP_plus_uconst 0x40]
	.dwattr $C$DW$1280, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1280, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1280, DW_AT_decl_line(0x295)
	.dwattr $C$DW$1280, DW_AT_decl_column(0x0a)

$C$DW$1281	.dwtag  DW_TAG_member
	.dwattr $C$DW$1281, DW_AT_type(*$C$DW$T$50)
	.dwattr $C$DW$1281, DW_AT_name("dataFlow")
	.dwattr $C$DW$1281, DW_AT_data_member_location[DW_OP_plus_uconst 0x58]
	.dwattr $C$DW$1281, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1281, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1281, DW_AT_decl_line(0x296)
	.dwattr $C$DW$1281, DW_AT_decl_column(0x0c)

$C$DW$1282	.dwtag  DW_TAG_member
	.dwattr $C$DW$1282, DW_AT_type(*$C$DW$T$583)
	.dwattr $C$DW$1282, DW_AT_name("dataFlowInfo")
	.dwattr $C$DW$1282, DW_AT_data_member_location[DW_OP_plus_uconst 0x60]
	.dwattr $C$DW$1282, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1282, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1282, DW_AT_decl_line(0x297)
	.dwattr $C$DW$1282, DW_AT_decl_column(0x15)

$C$DW$1283	.dwtag  DW_TAG_member
	.dwattr $C$DW$1283, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1283, DW_AT_name("biasParamSize")
	.dwattr $C$DW$1283, DW_AT_data_member_location[DW_OP_plus_uconst 0x68]
	.dwattr $C$DW$1283, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1283, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1283, DW_AT_decl_line(0x298)
	.dwattr $C$DW$1283, DW_AT_decl_column(0x0b)

$C$DW$1284	.dwtag  DW_TAG_member
	.dwattr $C$DW$1284, DW_AT_type(*$C$DW$T$500)
	.dwattr $C$DW$1284, DW_AT_name("mmaHandleArgsSize")
	.dwattr $C$DW$1284, DW_AT_data_member_location[DW_OP_plus_uconst 0x6c]
	.dwattr $C$DW$1284, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1284, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1284, DW_AT_decl_line(0x299)
	.dwattr $C$DW$1284, DW_AT_decl_column(0x0b)

$C$DW$1285	.dwtag  DW_TAG_member
	.dwattr $C$DW$1285, DW_AT_type(*$C$DW$T$584)
	.dwattr $C$DW$1285, DW_AT_name("totalNumCoeffs")
	.dwattr $C$DW$1285, DW_AT_data_member_location[DW_OP_plus_uconst 0x78]
	.dwattr $C$DW$1285, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1285, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1285, DW_AT_decl_line(0x29a)
	.dwattr $C$DW$1285, DW_AT_decl_column(0x0c)

$C$DW$1286	.dwtag  DW_TAG_member
	.dwattr $C$DW$1286, DW_AT_type(*$C$DW$T$584)
	.dwattr $C$DW$1286, DW_AT_name("totalActualCoeffs")
	.dwattr $C$DW$1286, DW_AT_data_member_location[DW_OP_plus_uconst 0x1078]
	.dwattr $C$DW$1286, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1286, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1286, DW_AT_decl_line(0x29b)
	.dwattr $C$DW$1286, DW_AT_decl_column(0x0c)

$C$DW$1287	.dwtag  DW_TAG_member
	.dwattr $C$DW$1287, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1287, DW_AT_name("procElemSize")
	.dwattr $C$DW$1287, DW_AT_data_member_location[DW_OP_plus_uconst 0x2078]
	.dwattr $C$DW$1287, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1287, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1287, DW_AT_decl_line(0x29d)
	.dwattr $C$DW$1287, DW_AT_decl_column(0x0c)


$C$DW$1288	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1288, DW_AT_name("operator =")
	.dwattr $C$DW$1288, DW_AT_declaration
	.dwattr $C$DW$1288, DW_AT_linkage_name("_ZN21sTIDL_ALgConvParams_taSERKS_")
	.dwattr $C$DW$1288, DW_AT_type(*$C$DW$T$585)
	.dwattr $C$DW$1288, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1289	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1289, DW_AT_type(*$C$DW$T$587)

	.dwendtag $C$DW$1288


$C$DW$1290	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1290, DW_AT_name("operator =")
	.dwattr $C$DW$1290, DW_AT_declaration
	.dwattr $C$DW$1290, DW_AT_linkage_name("_ZN21sTIDL_ALgConvParams_taSEOS_")
	.dwattr $C$DW$1290, DW_AT_type(*$C$DW$T$585)
	.dwattr $C$DW$1290, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1291	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1291, DW_AT_type(*$C$DW$T$585)

	.dwendtag $C$DW$1290

	.dwattr $C$DW$T$590, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$T$590, DW_AT_decl_line(0x28c)
	.dwattr $C$DW$T$590, DW_AT_decl_column(0x10)
	.dwendtag $C$DW$T$590

	.dwendtag $C$DW$TU$590


$C$DW$TU$585	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$585
$C$DW$T$585	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$585, DW_AT_type(*$C$DW$T$590)
	.dwattr $C$DW$T$585, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$585


$C$DW$TU$588	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$588

$C$DW$T$588	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$588, DW_AT_type(*$C$DW$T$585)
$C$DW$1292	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1292, DW_AT_type(*$C$DW$T$587)

	.dwendtag $C$DW$T$588

	.dwendtag $C$DW$TU$588


$C$DW$TU$589	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$589

$C$DW$T$589	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$589, DW_AT_type(*$C$DW$T$585)
$C$DW$1293	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1293, DW_AT_type(*$C$DW$T$585)

	.dwendtag $C$DW$T$589

	.dwendtag $C$DW$TU$589


$C$DW$TU$586	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$586
$C$DW$T$586	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$586, DW_AT_type(*$C$DW$T$590)

	.dwendtag $C$DW$TU$586


$C$DW$TU$587	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$587
$C$DW$T$587	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$587, DW_AT_type(*$C$DW$T$586)
	.dwattr $C$DW$T$587, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$587


$C$DW$TU$666	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$666
$C$DW$T$666	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$666, DW_AT_name("sTIDL_ALgConvParams_t")
	.dwattr $C$DW$T$666, DW_AT_type(*$C$DW$T$590)
	.dwattr $C$DW$T$666, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$T$666, DW_AT_decl_line(0x29e)
	.dwattr $C$DW$T$666, DW_AT_decl_column(0x02)

	.dwendtag $C$DW$TU$666


$C$DW$TU$612	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$612

$C$DW$T$612	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$612, DW_AT_name("sTIDL_ALgDetectOutputParams_t")
	.dwattr $C$DW$T$612, DW_AT_byte_size(0x538)
$C$DW$1294	.dwtag  DW_TAG_member
	.dwattr $C$DW$1294, DW_AT_type(*$C$DW$T$594)
	.dwattr $C$DW$1294, DW_AT_name("inLocDataList")
	.dwattr $C$DW$1294, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1294, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1294, DW_AT_decl_line(0x25d)
	.dwattr $C$DW$1294, DW_AT_decl_column(0x1d)

$C$DW$1295	.dwtag  DW_TAG_member
	.dwattr $C$DW$1295, DW_AT_type(*$C$DW$T$595)
	.dwattr $C$DW$1295, DW_AT_name("inLocdataQList")
	.dwattr $C$DW$1295, DW_AT_data_member_location[DW_OP_plus_uconst 0x80]
	.dwattr $C$DW$1295, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1295, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1295, DW_AT_decl_line(0x25e)
	.dwattr $C$DW$1295, DW_AT_decl_column(0x1c)

$C$DW$1296	.dwtag  DW_TAG_member
	.dwattr $C$DW$1296, DW_AT_type(*$C$DW$T$594)
	.dwattr $C$DW$1296, DW_AT_name("inConfDataList")
	.dwattr $C$DW$1296, DW_AT_data_member_location[DW_OP_plus_uconst 0xc0]
	.dwattr $C$DW$1296, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1296, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1296, DW_AT_decl_line(0x25f)
	.dwattr $C$DW$1296, DW_AT_decl_column(0x1d)

$C$DW$1297	.dwtag  DW_TAG_member
	.dwattr $C$DW$1297, DW_AT_type(*$C$DW$T$595)
	.dwattr $C$DW$1297, DW_AT_name("inConfdataQList")
	.dwattr $C$DW$1297, DW_AT_data_member_location[DW_OP_plus_uconst 0x140]
	.dwattr $C$DW$1297, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1297, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1297, DW_AT_decl_line(0x260)
	.dwattr $C$DW$1297, DW_AT_decl_column(0x1c)

$C$DW$1298	.dwtag  DW_TAG_member
	.dwattr $C$DW$1298, DW_AT_type(*$C$DW$T$596)
	.dwattr $C$DW$1298, DW_AT_name("locHeadPitchList")
	.dwattr $C$DW$1298, DW_AT_data_member_location[DW_OP_plus_uconst 0x180]
	.dwattr $C$DW$1298, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1298, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1298, DW_AT_decl_line(0x261)
	.dwattr $C$DW$1298, DW_AT_decl_column(0x1c)

$C$DW$1299	.dwtag  DW_TAG_member
	.dwattr $C$DW$1299, DW_AT_type(*$C$DW$T$596)
	.dwattr $C$DW$1299, DW_AT_name("confHeadPitchList")
	.dwattr $C$DW$1299, DW_AT_data_member_location[DW_OP_plus_uconst 0x240]
	.dwattr $C$DW$1299, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1299, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1299, DW_AT_decl_line(0x262)
	.dwattr $C$DW$1299, DW_AT_decl_column(0x1c)

$C$DW$1300	.dwtag  DW_TAG_member
	.dwattr $C$DW$1300, DW_AT_type(*$C$DW$T$598)
	.dwattr $C$DW$1300, DW_AT_name("confHeadInvPitchList")
	.dwattr $C$DW$1300, DW_AT_data_member_location[DW_OP_plus_uconst 0x300]
	.dwattr $C$DW$1300, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1300, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1300, DW_AT_decl_line(0x263)
	.dwattr $C$DW$1300, DW_AT_decl_column(0x1c)

$C$DW$1301	.dwtag  DW_TAG_member
	.dwattr $C$DW$1301, DW_AT_type(*$C$DW$T$599)
	.dwattr $C$DW$1301, DW_AT_name("locDataOffset")
	.dwattr $C$DW$1301, DW_AT_data_member_location[DW_OP_plus_uconst 0x3c0]
	.dwattr $C$DW$1301, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1301, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1301, DW_AT_decl_line(0x264)
	.dwattr $C$DW$1301, DW_AT_decl_column(0x1c)

$C$DW$1302	.dwtag  DW_TAG_member
	.dwattr $C$DW$1302, DW_AT_type(*$C$DW$T$599)
	.dwattr $C$DW$1302, DW_AT_name("confDataOffset")
	.dwattr $C$DW$1302, DW_AT_data_member_location[DW_OP_plus_uconst 0x400]
	.dwattr $C$DW$1302, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1302, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1302, DW_AT_decl_line(0x265)
	.dwattr $C$DW$1302, DW_AT_decl_column(0x1c)

$C$DW$1303	.dwtag  DW_TAG_member
	.dwattr $C$DW$1303, DW_AT_type(*$C$DW$T$599)
	.dwattr $C$DW$1303, DW_AT_name("numAnchors")
	.dwattr $C$DW$1303, DW_AT_data_member_location[DW_OP_plus_uconst 0x440]
	.dwattr $C$DW$1303, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1303, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1303, DW_AT_decl_line(0x266)
	.dwattr $C$DW$1303, DW_AT_decl_column(0x1c)

$C$DW$1304	.dwtag  DW_TAG_member
	.dwattr $C$DW$1304, DW_AT_type(*$C$DW$T$602)
	.dwattr $C$DW$1304, DW_AT_name("topMScore")
	.dwattr $C$DW$1304, DW_AT_data_member_location[DW_OP_plus_uconst 0x480]
	.dwattr $C$DW$1304, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1304, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1304, DW_AT_decl_line(0x269)
	.dwattr $C$DW$1304, DW_AT_decl_column(0x0e)

$C$DW$1305	.dwtag  DW_TAG_member
	.dwattr $C$DW$1305, DW_AT_type(*$C$DW$T$602)
	.dwattr $C$DW$1305, DW_AT_name("topMScoreSorted")
	.dwattr $C$DW$1305, DW_AT_data_member_location[DW_OP_plus_uconst 0x488]
	.dwattr $C$DW$1305, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1305, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1305, DW_AT_decl_line(0x26a)
	.dwattr $C$DW$1305, DW_AT_decl_column(0x0e)

$C$DW$1306	.dwtag  DW_TAG_member
	.dwattr $C$DW$1306, DW_AT_type(*$C$DW$T$603)
	.dwattr $C$DW$1306, DW_AT_name("tempScore")
	.dwattr $C$DW$1306, DW_AT_data_member_location[DW_OP_plus_uconst 0x490]
	.dwattr $C$DW$1306, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1306, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1306, DW_AT_decl_line(0x26b)
	.dwattr $C$DW$1306, DW_AT_decl_column(0x0e)

$C$DW$1307	.dwtag  DW_TAG_member
	.dwattr $C$DW$1307, DW_AT_type(*$C$DW$T$365)
	.dwattr $C$DW$1307, DW_AT_name("topMIndices")
	.dwattr $C$DW$1307, DW_AT_data_member_location[DW_OP_plus_uconst 0x498]
	.dwattr $C$DW$1307, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1307, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1307, DW_AT_decl_line(0x26c)
	.dwattr $C$DW$1307, DW_AT_decl_column(0x0e)

$C$DW$1308	.dwtag  DW_TAG_member
	.dwattr $C$DW$1308, DW_AT_type(*$C$DW$T$365)
	.dwattr $C$DW$1308, DW_AT_name("topMIndicesSorted")
	.dwattr $C$DW$1308, DW_AT_data_member_location[DW_OP_plus_uconst 0x4a0]
	.dwattr $C$DW$1308, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1308, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1308, DW_AT_decl_line(0x26d)
	.dwattr $C$DW$1308, DW_AT_decl_column(0x0e)

$C$DW$1309	.dwtag  DW_TAG_member
	.dwattr $C$DW$1309, DW_AT_type(*$C$DW$T$365)
	.dwattr $C$DW$1309, DW_AT_name("countMList")
	.dwattr $C$DW$1309, DW_AT_data_member_location[DW_OP_plus_uconst 0x4a8]
	.dwattr $C$DW$1309, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1309, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1309, DW_AT_decl_line(0x26f)
	.dwattr $C$DW$1309, DW_AT_decl_column(0x0e)

$C$DW$1310	.dwtag  DW_TAG_member
	.dwattr $C$DW$1310, DW_AT_type(*$C$DW$T$365)
	.dwattr $C$DW$1310, DW_AT_name("countMListAcc")
	.dwattr $C$DW$1310, DW_AT_data_member_location[DW_OP_plus_uconst 0x4b0]
	.dwattr $C$DW$1310, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1310, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1310, DW_AT_decl_line(0x270)
	.dwattr $C$DW$1310, DW_AT_decl_column(0x0e)

$C$DW$1311	.dwtag  DW_TAG_member
	.dwattr $C$DW$1311, DW_AT_type(*$C$DW$T$365)
	.dwattr $C$DW$1311, DW_AT_name("topKIndices")
	.dwattr $C$DW$1311, DW_AT_data_member_location[DW_OP_plus_uconst 0x4b8]
	.dwattr $C$DW$1311, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1311, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1311, DW_AT_decl_line(0x271)
	.dwattr $C$DW$1311, DW_AT_decl_column(0x0e)

$C$DW$1312	.dwtag  DW_TAG_member
	.dwattr $C$DW$1312, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$1312, DW_AT_name("topKLoc")
	.dwattr $C$DW$1312, DW_AT_data_member_location[DW_OP_plus_uconst 0x4c0]
	.dwattr $C$DW$1312, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1312, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1312, DW_AT_decl_line(0x272)
	.dwattr $C$DW$1312, DW_AT_decl_column(0x0e)

$C$DW$1313	.dwtag  DW_TAG_member
	.dwattr $C$DW$1313, DW_AT_type(*$C$DW$T$605)
	.dwattr $C$DW$1313, DW_AT_name("topKBbox")
	.dwattr $C$DW$1313, DW_AT_data_member_location[DW_OP_plus_uconst 0x4c8]
	.dwattr $C$DW$1313, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1313, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1313, DW_AT_decl_line(0x273)
	.dwattr $C$DW$1313, DW_AT_decl_column(0x0e)

$C$DW$1314	.dwtag  DW_TAG_member
	.dwattr $C$DW$1314, DW_AT_type(*$C$DW$T$602)
	.dwattr $C$DW$1314, DW_AT_name("topKScore")
	.dwattr $C$DW$1314, DW_AT_data_member_location[DW_OP_plus_uconst 0x4d0]
	.dwattr $C$DW$1314, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1314, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1314, DW_AT_decl_line(0x274)
	.dwattr $C$DW$1314, DW_AT_decl_column(0x0e)

$C$DW$1315	.dwtag  DW_TAG_member
	.dwattr $C$DW$1315, DW_AT_type(*$C$DW$T$365)
	.dwattr $C$DW$1315, DW_AT_name("nmsKeptIndices")
	.dwattr $C$DW$1315, DW_AT_data_member_location[DW_OP_plus_uconst 0x4d8]
	.dwattr $C$DW$1315, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1315, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1315, DW_AT_decl_line(0x275)
	.dwattr $C$DW$1315, DW_AT_decl_column(0x0e)

$C$DW$1316	.dwtag  DW_TAG_member
	.dwattr $C$DW$1316, DW_AT_type(*$C$DW$T$606)
	.dwattr $C$DW$1316, DW_AT_name("pred")
	.dwattr $C$DW$1316, DW_AT_data_member_location[DW_OP_plus_uconst 0x4e0]
	.dwattr $C$DW$1316, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1316, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1316, DW_AT_decl_line(0x279)
	.dwattr $C$DW$1316, DW_AT_decl_column(0x10)

$C$DW$1317	.dwtag  DW_TAG_member
	.dwattr $C$DW$1317, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$1317, DW_AT_name("featMaxMinVal")
	.dwattr $C$DW$1317, DW_AT_data_member_location[DW_OP_plus_uconst 0x4e8]
	.dwattr $C$DW$1317, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1317, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1317, DW_AT_decl_line(0x27a)
	.dwattr $C$DW$1317, DW_AT_decl_column(0x10)

$C$DW$1318	.dwtag  DW_TAG_member
	.dwattr $C$DW$1318, DW_AT_type(*$C$DW$T$602)
	.dwattr $C$DW$1318, DW_AT_name("topMScoreDdr")
	.dwattr $C$DW$1318, DW_AT_data_member_location[DW_OP_plus_uconst 0x4f0]
	.dwattr $C$DW$1318, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1318, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1318, DW_AT_decl_line(0x27d)
	.dwattr $C$DW$1318, DW_AT_decl_column(0x0e)

$C$DW$1319	.dwtag  DW_TAG_member
	.dwattr $C$DW$1319, DW_AT_type(*$C$DW$T$602)
	.dwattr $C$DW$1319, DW_AT_name("topMScoreSortedDdr")
	.dwattr $C$DW$1319, DW_AT_data_member_location[DW_OP_plus_uconst 0x4f8]
	.dwattr $C$DW$1319, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1319, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1319, DW_AT_decl_line(0x27e)
	.dwattr $C$DW$1319, DW_AT_decl_column(0x0e)

$C$DW$1320	.dwtag  DW_TAG_member
	.dwattr $C$DW$1320, DW_AT_type(*$C$DW$T$365)
	.dwattr $C$DW$1320, DW_AT_name("topMIndicesDdr")
	.dwattr $C$DW$1320, DW_AT_data_member_location[DW_OP_plus_uconst 0x500]
	.dwattr $C$DW$1320, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1320, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1320, DW_AT_decl_line(0x27f)
	.dwattr $C$DW$1320, DW_AT_decl_column(0x0e)

$C$DW$1321	.dwtag  DW_TAG_member
	.dwattr $C$DW$1321, DW_AT_type(*$C$DW$T$365)
	.dwattr $C$DW$1321, DW_AT_name("topMIndicesSortedDdr")
	.dwattr $C$DW$1321, DW_AT_data_member_location[DW_OP_plus_uconst 0x508]
	.dwattr $C$DW$1321, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1321, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1321, DW_AT_decl_line(0x280)
	.dwattr $C$DW$1321, DW_AT_decl_column(0x0e)

$C$DW$1322	.dwtag  DW_TAG_member
	.dwattr $C$DW$1322, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$1322, DW_AT_name("scratchPtr")
	.dwattr $C$DW$1322, DW_AT_data_member_location[DW_OP_plus_uconst 0x510]
	.dwattr $C$DW$1322, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1322, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1322, DW_AT_decl_line(0x281)
	.dwattr $C$DW$1322, DW_AT_decl_column(0x0e)

$C$DW$1323	.dwtag  DW_TAG_member
	.dwattr $C$DW$1323, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1323, DW_AT_name("elementType")
	.dwattr $C$DW$1323, DW_AT_data_member_location[DW_OP_plus_uconst 0x518]
	.dwattr $C$DW$1323, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1323, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1323, DW_AT_decl_line(0x283)
	.dwattr $C$DW$1323, DW_AT_decl_column(0x0b)

$C$DW$1324	.dwtag  DW_TAG_member
	.dwattr $C$DW$1324, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1324, DW_AT_name("elmSize")
	.dwattr $C$DW$1324, DW_AT_data_member_location[DW_OP_plus_uconst 0x51c]
	.dwattr $C$DW$1324, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1324, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1324, DW_AT_decl_line(0x284)
	.dwattr $C$DW$1324, DW_AT_decl_column(0x0b)

$C$DW$1325	.dwtag  DW_TAG_member
	.dwattr $C$DW$1325, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1325, DW_AT_name("topM")
	.dwattr $C$DW$1325, DW_AT_data_member_location[DW_OP_plus_uconst 0x520]
	.dwattr $C$DW$1325, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1325, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1325, DW_AT_decl_line(0x285)
	.dwattr $C$DW$1325, DW_AT_decl_column(0x0b)

$C$DW$1326	.dwtag  DW_TAG_member
	.dwattr $C$DW$1326, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1326, DW_AT_name("topMDdr")
	.dwattr $C$DW$1326, DW_AT_data_member_location[DW_OP_plus_uconst 0x524]
	.dwattr $C$DW$1326, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1326, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1326, DW_AT_decl_line(0x286)
	.dwattr $C$DW$1326, DW_AT_decl_column(0x0b)

$C$DW$1327	.dwtag  DW_TAG_member
	.dwattr $C$DW$1327, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1327, DW_AT_name("scratchDDRConsumed")
	.dwattr $C$DW$1327, DW_AT_data_member_location[DW_OP_plus_uconst 0x528]
	.dwattr $C$DW$1327, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1327, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1327, DW_AT_decl_line(0x287)
	.dwattr $C$DW$1327, DW_AT_decl_column(0x0b)

$C$DW$1328	.dwtag  DW_TAG_member
	.dwattr $C$DW$1328, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1328, DW_AT_name("topMAllClasses")
	.dwattr $C$DW$1328, DW_AT_data_member_location[DW_OP_plus_uconst 0x52c]
	.dwattr $C$DW$1328, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1328, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1328, DW_AT_decl_line(0x288)
	.dwattr $C$DW$1328, DW_AT_decl_column(0x0b)

$C$DW$1329	.dwtag  DW_TAG_member
	.dwattr $C$DW$1329, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1329, DW_AT_name("isBckClsAvailable")
	.dwattr $C$DW$1329, DW_AT_data_member_location[DW_OP_plus_uconst 0x530]
	.dwattr $C$DW$1329, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1329, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1329, DW_AT_decl_line(0x289)
	.dwattr $C$DW$1329, DW_AT_decl_column(0x0b)


$C$DW$1330	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1330, DW_AT_name("operator =")
	.dwattr $C$DW$1330, DW_AT_declaration
	.dwattr $C$DW$1330, DW_AT_linkage_name("_ZN29sTIDL_ALgDetectOutputParams_taSERKS_")
	.dwattr $C$DW$1330, DW_AT_type(*$C$DW$T$607)
	.dwattr $C$DW$1330, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1331	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1331, DW_AT_type(*$C$DW$T$609)

	.dwendtag $C$DW$1330


$C$DW$1332	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1332, DW_AT_name("operator =")
	.dwattr $C$DW$1332, DW_AT_declaration
	.dwattr $C$DW$1332, DW_AT_linkage_name("_ZN29sTIDL_ALgDetectOutputParams_taSEOS_")
	.dwattr $C$DW$1332, DW_AT_type(*$C$DW$T$607)
	.dwattr $C$DW$1332, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1333	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1333, DW_AT_type(*$C$DW$T$607)

	.dwendtag $C$DW$1332

	.dwattr $C$DW$T$612, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$T$612, DW_AT_decl_line(0x25b)
	.dwattr $C$DW$T$612, DW_AT_decl_column(0x10)
	.dwendtag $C$DW$T$612

	.dwendtag $C$DW$TU$612


$C$DW$TU$607	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$607
$C$DW$T$607	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$607, DW_AT_type(*$C$DW$T$612)
	.dwattr $C$DW$T$607, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$607


$C$DW$TU$610	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$610

$C$DW$T$610	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$610, DW_AT_type(*$C$DW$T$607)
$C$DW$1334	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1334, DW_AT_type(*$C$DW$T$609)

	.dwendtag $C$DW$T$610

	.dwendtag $C$DW$TU$610


$C$DW$TU$611	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$611

$C$DW$T$611	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$611, DW_AT_type(*$C$DW$T$607)
$C$DW$1335	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1335, DW_AT_type(*$C$DW$T$607)

	.dwendtag $C$DW$T$611

	.dwendtag $C$DW$TU$611


$C$DW$TU$608	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$608
$C$DW$T$608	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$608, DW_AT_type(*$C$DW$T$612)

	.dwendtag $C$DW$TU$608


$C$DW$TU$609	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$609
$C$DW$T$609	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$609, DW_AT_type(*$C$DW$T$608)
	.dwattr $C$DW$T$609, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$609


$C$DW$TU$672	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$672
$C$DW$T$672	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$672, DW_AT_name("sTIDL_ALgDetectOutputParams_t")
	.dwattr $C$DW$T$672, DW_AT_type(*$C$DW$T$612)
	.dwattr $C$DW$T$672, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$T$672, DW_AT_decl_line(0x28a)
	.dwattr $C$DW$T$672, DW_AT_decl_column(0x02)

	.dwendtag $C$DW$TU$672


$C$DW$TU$633	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$633

$C$DW$T$633	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$633, DW_AT_name("sTIDL_ALgEltWiseParams_t")
	.dwattr $C$DW$T$633, DW_AT_byte_size(0xe8)
$C$DW$1336	.dwtag  DW_TAG_member
	.dwattr $C$DW$1336, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1336, DW_AT_name("whichInputIsSigned")
	.dwattr $C$DW$1336, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1336, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1336, DW_AT_decl_line(0x2cd)
	.dwattr $C$DW$1336, DW_AT_decl_column(0x0b)

$C$DW$1337	.dwtag  DW_TAG_member
	.dwattr $C$DW$1337, DW_AT_type(*$C$DW$T$626)
	.dwattr $C$DW$1337, DW_AT_name("buffInputBlockOffset")
	.dwattr $C$DW$1337, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$1337, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1337, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1337, DW_AT_decl_line(0x2ce)
	.dwattr $C$DW$1337, DW_AT_decl_column(0x0b)

$C$DW$1338	.dwtag  DW_TAG_member
	.dwattr $C$DW$1338, DW_AT_type(*$C$DW$T$627)
	.dwattr $C$DW$1338, DW_AT_name("inputScale")
	.dwattr $C$DW$1338, DW_AT_data_member_location[DW_OP_plus_uconst 0x20]
	.dwattr $C$DW$1338, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1338, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1338, DW_AT_decl_line(0x2cf)
	.dwattr $C$DW$1338, DW_AT_decl_column(0x0c)

$C$DW$1339	.dwtag  DW_TAG_member
	.dwattr $C$DW$1339, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1339, DW_AT_name("isMmaCiAvailable")
	.dwattr $C$DW$1339, DW_AT_data_member_location[DW_OP_plus_uconst 0x60]
	.dwattr $C$DW$1339, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1339, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1339, DW_AT_decl_line(0x2d0)
	.dwattr $C$DW$1339, DW_AT_decl_column(0x0b)

$C$DW$1340	.dwtag  DW_TAG_member
	.dwattr $C$DW$1340, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1340, DW_AT_name("inNumBytes")
	.dwattr $C$DW$1340, DW_AT_data_member_location[DW_OP_plus_uconst 0x64]
	.dwattr $C$DW$1340, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1340, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1340, DW_AT_decl_line(0x2d1)
	.dwattr $C$DW$1340, DW_AT_decl_column(0x0b)

$C$DW$1341	.dwtag  DW_TAG_member
	.dwattr $C$DW$1341, DW_AT_type(*$C$DW$T$571)
	.dwattr $C$DW$1341, DW_AT_name("mma_config_reg")
	.dwattr $C$DW$1341, DW_AT_data_member_location[DW_OP_plus_uconst 0x68]
	.dwattr $C$DW$1341, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1341, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1341, DW_AT_decl_line(0x2d2)
	.dwattr $C$DW$1341, DW_AT_decl_column(0x17)

$C$DW$1342	.dwtag  DW_TAG_member
	.dwattr $C$DW$1342, DW_AT_type(*$C$DW$T$572)
	.dwattr $C$DW$1342, DW_AT_name("mma_offset_reg")
	.dwattr $C$DW$1342, DW_AT_data_member_location[DW_OP_plus_uconst 0xa8]
	.dwattr $C$DW$1342, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1342, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1342, DW_AT_decl_line(0x2d3)
	.dwattr $C$DW$1342, DW_AT_decl_column(0x14)


$C$DW$1343	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1343, DW_AT_name("operator =")
	.dwattr $C$DW$1343, DW_AT_declaration
	.dwattr $C$DW$1343, DW_AT_linkage_name("_ZN24sTIDL_ALgEltWiseParams_taSERKS_")
	.dwattr $C$DW$1343, DW_AT_type(*$C$DW$T$628)
	.dwattr $C$DW$1343, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1344	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1344, DW_AT_type(*$C$DW$T$630)

	.dwendtag $C$DW$1343


$C$DW$1345	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1345, DW_AT_name("operator =")
	.dwattr $C$DW$1345, DW_AT_declaration
	.dwattr $C$DW$1345, DW_AT_linkage_name("_ZN24sTIDL_ALgEltWiseParams_taSEOS_")
	.dwattr $C$DW$1345, DW_AT_type(*$C$DW$T$628)
	.dwattr $C$DW$1345, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1346	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1346, DW_AT_type(*$C$DW$T$628)

	.dwendtag $C$DW$1345

	.dwattr $C$DW$T$633, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$T$633, DW_AT_decl_line(0x2cc)
	.dwattr $C$DW$T$633, DW_AT_decl_column(0x10)
	.dwendtag $C$DW$T$633

	.dwendtag $C$DW$TU$633


$C$DW$TU$628	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$628
$C$DW$T$628	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$628, DW_AT_type(*$C$DW$T$633)
	.dwattr $C$DW$T$628, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$628


$C$DW$TU$631	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$631

$C$DW$T$631	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$631, DW_AT_type(*$C$DW$T$628)
$C$DW$1347	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1347, DW_AT_type(*$C$DW$T$630)

	.dwendtag $C$DW$T$631

	.dwendtag $C$DW$TU$631


$C$DW$TU$632	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$632

$C$DW$T$632	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$632, DW_AT_type(*$C$DW$T$628)
$C$DW$1348	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1348, DW_AT_type(*$C$DW$T$628)

	.dwendtag $C$DW$T$632

	.dwendtag $C$DW$TU$632


$C$DW$TU$629	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$629
$C$DW$T$629	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$629, DW_AT_type(*$C$DW$T$633)

	.dwendtag $C$DW$TU$629


$C$DW$TU$630	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$630
$C$DW$T$630	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$630, DW_AT_type(*$C$DW$T$629)
	.dwattr $C$DW$T$630, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$630


$C$DW$TU$667	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$667
$C$DW$T$667	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$667, DW_AT_name("sTIDL_ALgEltWiseParams_t")
	.dwattr $C$DW$T$667, DW_AT_type(*$C$DW$T$633)
	.dwattr $C$DW$T$667, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$T$667, DW_AT_decl_line(0x2d4)
	.dwattr $C$DW$T$667, DW_AT_decl_column(0x02)

	.dwendtag $C$DW$TU$667


$C$DW$TU$641	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$641

$C$DW$T$641	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$641, DW_AT_name("sTIDL_ALgInnerProductParams_t")
	.dwattr $C$DW$T$641, DW_AT_byte_size(0x28)
$C$DW$1349	.dwtag  DW_TAG_member
	.dwattr $C$DW$1349, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$1349, DW_AT_name("biasParamMem")
	.dwattr $C$DW$1349, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1349, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1349, DW_AT_decl_line(0x2a1)
	.dwattr $C$DW$1349, DW_AT_decl_column(0x0a)

$C$DW$1350	.dwtag  DW_TAG_member
	.dwattr $C$DW$1350, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$1350, DW_AT_name("wtTranformMem")
	.dwattr $C$DW$1350, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$1350, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1350, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1350, DW_AT_decl_line(0x2a2)
	.dwattr $C$DW$1350, DW_AT_decl_column(0x0a)

$C$DW$1351	.dwtag  DW_TAG_member
	.dwattr $C$DW$1351, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$1351, DW_AT_name("accMem")
	.dwattr $C$DW$1351, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$1351, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1351, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1351, DW_AT_decl_line(0x2a3)
	.dwattr $C$DW$1351, DW_AT_decl_column(0x0a)

$C$DW$1352	.dwtag  DW_TAG_member
	.dwattr $C$DW$1352, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1352, DW_AT_name("wtTranformSize")
	.dwattr $C$DW$1352, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$1352, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1352, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1352, DW_AT_decl_line(0x2a4)
	.dwattr $C$DW$1352, DW_AT_decl_column(0x0b)

$C$DW$1353	.dwtag  DW_TAG_member
	.dwattr $C$DW$1353, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1353, DW_AT_name("biasParamSize")
	.dwattr $C$DW$1353, DW_AT_data_member_location[DW_OP_plus_uconst 0x1c]
	.dwattr $C$DW$1353, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1353, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1353, DW_AT_decl_line(0x2a5)
	.dwattr $C$DW$1353, DW_AT_decl_column(0x0b)

$C$DW$1354	.dwtag  DW_TAG_member
	.dwattr $C$DW$1354, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1354, DW_AT_name("accMemSize")
	.dwattr $C$DW$1354, DW_AT_data_member_location[DW_OP_plus_uconst 0x20]
	.dwattr $C$DW$1354, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1354, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1354, DW_AT_decl_line(0x2a6)
	.dwattr $C$DW$1354, DW_AT_decl_column(0x0b)


$C$DW$1355	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1355, DW_AT_name("operator =")
	.dwattr $C$DW$1355, DW_AT_declaration
	.dwattr $C$DW$1355, DW_AT_linkage_name("_ZN29sTIDL_ALgInnerProductParams_taSERKS_")
	.dwattr $C$DW$1355, DW_AT_type(*$C$DW$T$636)
	.dwattr $C$DW$1355, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1356	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1356, DW_AT_type(*$C$DW$T$638)

	.dwendtag $C$DW$1355


$C$DW$1357	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1357, DW_AT_name("operator =")
	.dwattr $C$DW$1357, DW_AT_declaration
	.dwattr $C$DW$1357, DW_AT_linkage_name("_ZN29sTIDL_ALgInnerProductParams_taSEOS_")
	.dwattr $C$DW$1357, DW_AT_type(*$C$DW$T$636)
	.dwattr $C$DW$1357, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1358	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1358, DW_AT_type(*$C$DW$T$636)

	.dwendtag $C$DW$1357

	.dwattr $C$DW$T$641, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$T$641, DW_AT_decl_line(0x2a0)
	.dwattr $C$DW$T$641, DW_AT_decl_column(0x10)
	.dwendtag $C$DW$T$641

	.dwendtag $C$DW$TU$641


$C$DW$TU$636	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$636
$C$DW$T$636	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$636, DW_AT_type(*$C$DW$T$641)
	.dwattr $C$DW$T$636, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$636


$C$DW$TU$639	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$639

$C$DW$T$639	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$639, DW_AT_type(*$C$DW$T$636)
$C$DW$1359	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1359, DW_AT_type(*$C$DW$T$638)

	.dwendtag $C$DW$T$639

	.dwendtag $C$DW$TU$639


$C$DW$TU$640	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$640

$C$DW$T$640	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$640, DW_AT_type(*$C$DW$T$636)
$C$DW$1360	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1360, DW_AT_type(*$C$DW$T$636)

	.dwendtag $C$DW$T$640

	.dwendtag $C$DW$TU$640


$C$DW$TU$637	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$637
$C$DW$T$637	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$637, DW_AT_type(*$C$DW$T$641)

	.dwendtag $C$DW$TU$637


$C$DW$TU$638	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$638
$C$DW$T$638	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$638, DW_AT_type(*$C$DW$T$637)
	.dwattr $C$DW$T$638, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$638


$C$DW$TU$671	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$671
$C$DW$T$671	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$671, DW_AT_name("sTIDL_ALgInnerProductParams_t")
	.dwattr $C$DW$T$671, DW_AT_type(*$C$DW$T$641)
	.dwattr $C$DW$T$671, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$T$671, DW_AT_decl_line(0x2a7)
	.dwattr $C$DW$T$671, DW_AT_decl_column(0x02)

	.dwendtag $C$DW$TU$671


$C$DW$TU$649	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$649

$C$DW$T$649	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$649, DW_AT_name("sTIDL_ALgPoolingParams_t")
	.dwattr $C$DW$T$649, DW_AT_byte_size(0x38)
$C$DW$1361	.dwtag  DW_TAG_member
	.dwattr $C$DW$1361, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1361, DW_AT_name("predicateBufSize")
	.dwattr $C$DW$1361, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1361, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1361, DW_AT_decl_line(0x2d7)
	.dwattr $C$DW$1361, DW_AT_decl_column(0x0b)

$C$DW$1362	.dwtag  DW_TAG_member
	.dwattr $C$DW$1362, DW_AT_type(*$C$DW$T$643)
	.dwattr $C$DW$1362, DW_AT_name("scaleMemSize")
	.dwattr $C$DW$1362, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$1362, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1362, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1362, DW_AT_decl_line(0x2d8)
	.dwattr $C$DW$1362, DW_AT_decl_column(0x0c)

$C$DW$1363	.dwtag  DW_TAG_member
	.dwattr $C$DW$1363, DW_AT_type(*$C$DW$T$582)
	.dwattr $C$DW$1363, DW_AT_name("scalePtr")
	.dwattr $C$DW$1363, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$1363, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1363, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1363, DW_AT_decl_line(0x2d9)
	.dwattr $C$DW$1363, DW_AT_decl_column(0x09)

$C$DW$1364	.dwtag  DW_TAG_member
	.dwattr $C$DW$1364, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1364, DW_AT_name("internalPoolingWeightQ")
	.dwattr $C$DW$1364, DW_AT_data_member_location[DW_OP_plus_uconst 0x28]
	.dwattr $C$DW$1364, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1364, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1364, DW_AT_decl_line(0x2db)
	.dwattr $C$DW$1364, DW_AT_decl_column(0x0b)

$C$DW$1365	.dwtag  DW_TAG_member
	.dwattr $C$DW$1365, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1365, DW_AT_name("nextOutOffset")
	.dwattr $C$DW$1365, DW_AT_data_member_location[DW_OP_plus_uconst 0x2c]
	.dwattr $C$DW$1365, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1365, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1365, DW_AT_decl_line(0x2dc)
	.dwattr $C$DW$1365, DW_AT_decl_column(0x0b)

$C$DW$1366	.dwtag  DW_TAG_member
	.dwattr $C$DW$1366, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1366, DW_AT_name("startRowNumberInTensor")
	.dwattr $C$DW$1366, DW_AT_data_member_location[DW_OP_plus_uconst 0x30]
	.dwattr $C$DW$1366, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1366, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1366, DW_AT_decl_line(0x2dd)
	.dwattr $C$DW$1366, DW_AT_decl_column(0x0b)


$C$DW$1367	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1367, DW_AT_name("operator =")
	.dwattr $C$DW$1367, DW_AT_declaration
	.dwattr $C$DW$1367, DW_AT_linkage_name("_ZN24sTIDL_ALgPoolingParams_taSERKS_")
	.dwattr $C$DW$1367, DW_AT_type(*$C$DW$T$644)
	.dwattr $C$DW$1367, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1368	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1368, DW_AT_type(*$C$DW$T$646)

	.dwendtag $C$DW$1367


$C$DW$1369	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1369, DW_AT_name("operator =")
	.dwattr $C$DW$1369, DW_AT_declaration
	.dwattr $C$DW$1369, DW_AT_linkage_name("_ZN24sTIDL_ALgPoolingParams_taSEOS_")
	.dwattr $C$DW$1369, DW_AT_type(*$C$DW$T$644)
	.dwattr $C$DW$1369, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1370	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1370, DW_AT_type(*$C$DW$T$644)

	.dwendtag $C$DW$1369

	.dwattr $C$DW$T$649, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$T$649, DW_AT_decl_line(0x2d6)
	.dwattr $C$DW$T$649, DW_AT_decl_column(0x10)
	.dwendtag $C$DW$T$649

	.dwendtag $C$DW$TU$649


$C$DW$TU$644	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$644
$C$DW$T$644	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$644, DW_AT_type(*$C$DW$T$649)
	.dwattr $C$DW$T$644, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$644


$C$DW$TU$647	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$647

$C$DW$T$647	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$647, DW_AT_type(*$C$DW$T$644)
$C$DW$1371	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1371, DW_AT_type(*$C$DW$T$646)

	.dwendtag $C$DW$T$647

	.dwendtag $C$DW$TU$647


$C$DW$TU$648	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$648

$C$DW$T$648	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$648, DW_AT_type(*$C$DW$T$644)
$C$DW$1372	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1372, DW_AT_type(*$C$DW$T$644)

	.dwendtag $C$DW$T$648

	.dwendtag $C$DW$TU$648


$C$DW$TU$645	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$645
$C$DW$T$645	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$645, DW_AT_type(*$C$DW$T$649)

	.dwendtag $C$DW$TU$645


$C$DW$TU$646	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$646
$C$DW$T$646	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$646, DW_AT_type(*$C$DW$T$645)
	.dwattr $C$DW$T$646, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$646


$C$DW$TU$668	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$668
$C$DW$T$668	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$668, DW_AT_name("sTIDL_ALgPoolingParams_t")
	.dwattr $C$DW$T$668, DW_AT_type(*$C$DW$T$649)
	.dwattr $C$DW$T$668, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$T$668, DW_AT_decl_line(0x2de)
	.dwattr $C$DW$T$668, DW_AT_decl_column(0x02)

	.dwendtag $C$DW$TU$668


$C$DW$TU$657	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$657

$C$DW$T$657	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$657, DW_AT_name("sTIDL_ALgResizeLayer_t")
	.dwattr $C$DW$T$657, DW_AT_byte_size(0x58)
$C$DW$1373	.dwtag  DW_TAG_member
	.dwattr $C$DW$1373, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1373, DW_AT_name("isOptResizeAvail")
	.dwattr $C$DW$1373, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1373, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1373, DW_AT_decl_line(0x2b2)
	.dwattr $C$DW$1373, DW_AT_decl_column(0x0b)

$C$DW$1374	.dwtag  DW_TAG_member
	.dwattr $C$DW$1374, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1374, DW_AT_name("scratchSize")
	.dwattr $C$DW$1374, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$1374, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1374, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1374, DW_AT_decl_line(0x2b3)
	.dwattr $C$DW$1374, DW_AT_decl_column(0x0b)

$C$DW$1375	.dwtag  DW_TAG_member
	.dwattr $C$DW$1375, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1375, DW_AT_name("isMmaCiAvailable")
	.dwattr $C$DW$1375, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$1375, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1375, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1375, DW_AT_decl_line(0x2b4)
	.dwattr $C$DW$1375, DW_AT_decl_column(0x0b)

$C$DW$1376	.dwtag  DW_TAG_member
	.dwattr $C$DW$1376, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1376, DW_AT_name("inNumBytes")
	.dwattr $C$DW$1376, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$1376, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1376, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1376, DW_AT_decl_line(0x2b5)
	.dwattr $C$DW$1376, DW_AT_decl_column(0x0b)

$C$DW$1377	.dwtag  DW_TAG_member
	.dwattr $C$DW$1377, DW_AT_type(*$C$DW$T$571)
	.dwattr $C$DW$1377, DW_AT_name("mma_config_reg")
	.dwattr $C$DW$1377, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$1377, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1377, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1377, DW_AT_decl_line(0x2b6)
	.dwattr $C$DW$1377, DW_AT_decl_column(0x17)

$C$DW$1378	.dwtag  DW_TAG_member
	.dwattr $C$DW$1378, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$1378, DW_AT_name("reserved")
	.dwattr $C$DW$1378, DW_AT_data_member_location[DW_OP_plus_uconst 0x50]
	.dwattr $C$DW$1378, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1378, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1378, DW_AT_decl_line(0x2b7)
	.dwattr $C$DW$1378, DW_AT_decl_column(0x09)


$C$DW$1379	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1379, DW_AT_name("operator =")
	.dwattr $C$DW$1379, DW_AT_declaration
	.dwattr $C$DW$1379, DW_AT_linkage_name("_ZN22sTIDL_ALgResizeLayer_taSERKS_")
	.dwattr $C$DW$1379, DW_AT_type(*$C$DW$T$652)
	.dwattr $C$DW$1379, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1380	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1380, DW_AT_type(*$C$DW$T$654)

	.dwendtag $C$DW$1379


$C$DW$1381	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1381, DW_AT_name("operator =")
	.dwattr $C$DW$1381, DW_AT_declaration
	.dwattr $C$DW$1381, DW_AT_linkage_name("_ZN22sTIDL_ALgResizeLayer_taSEOS_")
	.dwattr $C$DW$1381, DW_AT_type(*$C$DW$T$652)
	.dwattr $C$DW$1381, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1382	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1382, DW_AT_type(*$C$DW$T$652)

	.dwendtag $C$DW$1381

	.dwattr $C$DW$T$657, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$T$657, DW_AT_decl_line(0x2b1)
	.dwattr $C$DW$T$657, DW_AT_decl_column(0x10)
	.dwendtag $C$DW$T$657

	.dwendtag $C$DW$TU$657


$C$DW$TU$652	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$652
$C$DW$T$652	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$652, DW_AT_type(*$C$DW$T$657)
	.dwattr $C$DW$T$652, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$652


$C$DW$TU$655	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$655

$C$DW$T$655	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$655, DW_AT_type(*$C$DW$T$652)
$C$DW$1383	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1383, DW_AT_type(*$C$DW$T$654)

	.dwendtag $C$DW$T$655

	.dwendtag $C$DW$TU$655


$C$DW$TU$656	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$656

$C$DW$T$656	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$656, DW_AT_type(*$C$DW$T$652)
$C$DW$1384	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1384, DW_AT_type(*$C$DW$T$652)

	.dwendtag $C$DW$T$656

	.dwendtag $C$DW$TU$656


$C$DW$TU$653	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$653
$C$DW$T$653	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$653, DW_AT_type(*$C$DW$T$657)

	.dwendtag $C$DW$TU$653


$C$DW$TU$654	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$654
$C$DW$T$654	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$654, DW_AT_type(*$C$DW$T$653)
	.dwattr $C$DW$T$654, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$654


$C$DW$TU$670	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$670
$C$DW$T$670	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$670, DW_AT_name("sTIDL_ALgResizeLayer_t")
	.dwattr $C$DW$T$670, DW_AT_type(*$C$DW$T$657)
	.dwattr $C$DW$T$670, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$T$670, DW_AT_decl_line(0x2b8)
	.dwattr $C$DW$T$670, DW_AT_decl_column(0x02)

	.dwendtag $C$DW$TU$670


$C$DW$TU$664	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$664

$C$DW$T$664	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$664, DW_AT_name("sTIDL_ActParams_t")
	.dwattr $C$DW$T$664, DW_AT_byte_size(0x14)
$C$DW$1385	.dwtag  DW_TAG_member
	.dwattr $C$DW$1385, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1385, DW_AT_name("slope")
	.dwattr $C$DW$1385, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1385, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1385, DW_AT_decl_line(0x33c)
	.dwattr $C$DW$1385, DW_AT_decl_column(0x0d)

$C$DW$1386	.dwtag  DW_TAG_member
	.dwattr $C$DW$1386, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$1386, DW_AT_name("slopeScale")
	.dwattr $C$DW$1386, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$1386, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1386, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1386, DW_AT_decl_line(0x33e)
	.dwattr $C$DW$1386, DW_AT_decl_column(0x14)

$C$DW$1387	.dwtag  DW_TAG_member
	.dwattr $C$DW$1387, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$1387, DW_AT_name("clipMin")
	.dwattr $C$DW$1387, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$1387, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1387, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1387, DW_AT_decl_line(0x340)
	.dwattr $C$DW$1387, DW_AT_decl_column(0x14)

$C$DW$1388	.dwtag  DW_TAG_member
	.dwattr $C$DW$1388, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$1388, DW_AT_name("clipMax")
	.dwattr $C$DW$1388, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$1388, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1388, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1388, DW_AT_decl_line(0x342)
	.dwattr $C$DW$1388, DW_AT_decl_column(0x14)

$C$DW$1389	.dwtag  DW_TAG_member
	.dwattr $C$DW$1389, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1389, DW_AT_name("actType")
	.dwattr $C$DW$1389, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$1389, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1389, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1389, DW_AT_decl_line(0x344)
	.dwattr $C$DW$1389, DW_AT_decl_column(0x0d)


$C$DW$1390	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1390, DW_AT_name("operator =")
	.dwattr $C$DW$1390, DW_AT_declaration
	.dwattr $C$DW$1390, DW_AT_linkage_name("_ZN17sTIDL_ActParams_taSERKS_")
	.dwattr $C$DW$1390, DW_AT_type(*$C$DW$T$659)
	.dwattr $C$DW$1390, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1391	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1391, DW_AT_type(*$C$DW$T$661)

	.dwendtag $C$DW$1390


$C$DW$1392	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1392, DW_AT_name("operator =")
	.dwattr $C$DW$1392, DW_AT_declaration
	.dwattr $C$DW$1392, DW_AT_linkage_name("_ZN17sTIDL_ActParams_taSEOS_")
	.dwattr $C$DW$1392, DW_AT_type(*$C$DW$T$659)
	.dwattr $C$DW$1392, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1393	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1393, DW_AT_type(*$C$DW$T$659)

	.dwendtag $C$DW$1392

	.dwattr $C$DW$T$664, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$664, DW_AT_decl_line(0x33a)
	.dwattr $C$DW$T$664, DW_AT_decl_column(0x10)
	.dwendtag $C$DW$T$664

	.dwendtag $C$DW$TU$664


$C$DW$TU$659	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$659
$C$DW$T$659	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$659, DW_AT_type(*$C$DW$T$664)
	.dwattr $C$DW$T$659, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$659


$C$DW$TU$662	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$662

$C$DW$T$662	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$662, DW_AT_type(*$C$DW$T$659)
$C$DW$1394	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1394, DW_AT_type(*$C$DW$T$661)

	.dwendtag $C$DW$T$662

	.dwendtag $C$DW$TU$662


$C$DW$TU$663	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$663

$C$DW$T$663	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$663, DW_AT_type(*$C$DW$T$659)
$C$DW$1395	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1395, DW_AT_type(*$C$DW$T$659)

	.dwendtag $C$DW$T$663

	.dwendtag $C$DW$TU$663


$C$DW$TU$660	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$660
$C$DW$T$660	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$660, DW_AT_type(*$C$DW$T$664)

	.dwendtag $C$DW$TU$660


$C$DW$TU$661	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$661
$C$DW$T$661	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$661, DW_AT_type(*$C$DW$T$660)
	.dwattr $C$DW$T$661, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$661


$C$DW$TU$873	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$873
$C$DW$T$873	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$873, DW_AT_name("sTIDL_ActParams_t")
	.dwattr $C$DW$T$873, DW_AT_type(*$C$DW$T$664)
	.dwattr $C$DW$T$873, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$873, DW_AT_decl_line(0x345)
	.dwattr $C$DW$T$873, DW_AT_decl_column(0x02)

	.dwendtag $C$DW$TU$873


$C$DW$TU$681	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$681

$C$DW$T$681	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$681, DW_AT_name("sTIDL_AlgLayerParams_t")
	.dwattr $C$DW$T$681, DW_AT_byte_size(0x2080)
$C$DW$1396	.dwtag  DW_TAG_member
	.dwattr $C$DW$1396, DW_AT_type(*$C$DW$T$666)
	.dwattr $C$DW$1396, DW_AT_name("convParams")
	.dwattr $C$DW$1396, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1396, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1396, DW_AT_decl_line(0x2f6)
	.dwattr $C$DW$1396, DW_AT_decl_column(0x26)

$C$DW$1397	.dwtag  DW_TAG_member
	.dwattr $C$DW$1397, DW_AT_type(*$C$DW$T$667)
	.dwattr $C$DW$1397, DW_AT_name("eltWiseParams")
	.dwattr $C$DW$1397, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1397, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1397, DW_AT_decl_line(0x2f7)
	.dwattr $C$DW$1397, DW_AT_decl_column(0x26)

$C$DW$1398	.dwtag  DW_TAG_member
	.dwattr $C$DW$1398, DW_AT_type(*$C$DW$T$668)
	.dwattr $C$DW$1398, DW_AT_name("poolParams")
	.dwattr $C$DW$1398, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1398, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1398, DW_AT_decl_line(0x2f8)
	.dwattr $C$DW$1398, DW_AT_decl_column(0x26)

$C$DW$1399	.dwtag  DW_TAG_member
	.dwattr $C$DW$1399, DW_AT_type(*$C$DW$T$669)
	.dwattr $C$DW$1399, DW_AT_name("argMaxParams")
	.dwattr $C$DW$1399, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1399, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1399, DW_AT_decl_line(0x2f9)
	.dwattr $C$DW$1399, DW_AT_decl_column(0x26)

$C$DW$1400	.dwtag  DW_TAG_member
	.dwattr $C$DW$1400, DW_AT_type(*$C$DW$T$670)
	.dwattr $C$DW$1400, DW_AT_name("resizeParams")
	.dwattr $C$DW$1400, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1400, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1400, DW_AT_decl_line(0x2fa)
	.dwattr $C$DW$1400, DW_AT_decl_column(0x26)

$C$DW$1401	.dwtag  DW_TAG_member
	.dwattr $C$DW$1401, DW_AT_type(*$C$DW$T$671)
	.dwattr $C$DW$1401, DW_AT_name("innerProductParams")
	.dwattr $C$DW$1401, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1401, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1401, DW_AT_decl_line(0x2fb)
	.dwattr $C$DW$1401, DW_AT_decl_column(0x26)

$C$DW$1402	.dwtag  DW_TAG_member
	.dwattr $C$DW$1402, DW_AT_type(*$C$DW$T$672)
	.dwattr $C$DW$1402, DW_AT_name("detectionOutputParams")
	.dwattr $C$DW$1402, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1402, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1402, DW_AT_decl_line(0x2fc)
	.dwattr $C$DW$1402, DW_AT_decl_column(0x26)

$C$DW$1403	.dwtag  DW_TAG_member
	.dwattr $C$DW$1403, DW_AT_type(*$C$DW$T$673)
	.dwattr $C$DW$1403, DW_AT_name("concatParams")
	.dwattr $C$DW$1403, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1403, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1403, DW_AT_decl_line(0x2fd)
	.dwattr $C$DW$1403, DW_AT_decl_column(0x26)

$C$DW$1404	.dwtag  DW_TAG_member
	.dwattr $C$DW$1404, DW_AT_type(*$C$DW$T$674)
	.dwattr $C$DW$1404, DW_AT_name("batchNormParams")
	.dwattr $C$DW$1404, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1404, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1404, DW_AT_decl_line(0x2fe)
	.dwattr $C$DW$1404, DW_AT_decl_column(0x26)

$C$DW$1405	.dwtag  DW_TAG_member
	.dwattr $C$DW$1405, DW_AT_type(*$C$DW$T$675)
	.dwattr $C$DW$1405, DW_AT_name("colorConversionParams")
	.dwattr $C$DW$1405, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1405, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1405, DW_AT_decl_line(0x2ff)
	.dwattr $C$DW$1405, DW_AT_decl_column(0x26)


$C$DW$1406	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1406, DW_AT_name("operator =")
	.dwattr $C$DW$1406, DW_AT_declaration
	.dwattr $C$DW$1406, DW_AT_linkage_name("_ZN22sTIDL_AlgLayerParams_taSERKS_")
	.dwattr $C$DW$1406, DW_AT_type(*$C$DW$T$676)
	.dwattr $C$DW$1406, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1407	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1407, DW_AT_type(*$C$DW$T$678)

	.dwendtag $C$DW$1406


$C$DW$1408	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1408, DW_AT_name("operator =")
	.dwattr $C$DW$1408, DW_AT_declaration
	.dwattr $C$DW$1408, DW_AT_linkage_name("_ZN22sTIDL_AlgLayerParams_taSEOS_")
	.dwattr $C$DW$1408, DW_AT_type(*$C$DW$T$676)
	.dwattr $C$DW$1408, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1409	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1409, DW_AT_type(*$C$DW$T$676)

	.dwendtag $C$DW$1408

	.dwattr $C$DW$T$681, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$T$681, DW_AT_decl_line(0x2f5)
	.dwattr $C$DW$T$681, DW_AT_decl_column(0x0f)
	.dwendtag $C$DW$T$681

	.dwendtag $C$DW$TU$681


$C$DW$TU$676	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$676
$C$DW$T$676	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$676, DW_AT_type(*$C$DW$T$681)
	.dwattr $C$DW$T$676, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$676


$C$DW$TU$679	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$679

$C$DW$T$679	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$679, DW_AT_type(*$C$DW$T$676)
$C$DW$1410	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1410, DW_AT_type(*$C$DW$T$678)

	.dwendtag $C$DW$T$679

	.dwendtag $C$DW$TU$679


$C$DW$TU$680	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$680

$C$DW$T$680	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$680, DW_AT_type(*$C$DW$T$676)
$C$DW$1411	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1411, DW_AT_type(*$C$DW$T$676)

	.dwendtag $C$DW$T$680

	.dwendtag $C$DW$TU$680


$C$DW$TU$677	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$677
$C$DW$T$677	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$677, DW_AT_type(*$C$DW$T$681)

	.dwendtag $C$DW$TU$677


$C$DW$TU$678	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$678
$C$DW$T$678	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$678, DW_AT_type(*$C$DW$T$677)
	.dwattr $C$DW$T$678, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$678


$C$DW$TU$693	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$693
$C$DW$T$693	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$693, DW_AT_name("sTIDL_AlgLayerParams_t")
	.dwattr $C$DW$T$693, DW_AT_type(*$C$DW$T$681)
	.dwattr $C$DW$T$693, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$T$693, DW_AT_decl_line(0x300)
	.dwattr $C$DW$T$693, DW_AT_decl_column(0x02)

	.dwendtag $C$DW$TU$693


$C$DW$TU$706	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$706

$C$DW$T$706	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$706, DW_AT_name("sTIDL_AlgLayer_t")
	.dwattr $C$DW$T$706, DW_AT_byte_size(0x23c0)
$C$DW$1412	.dwtag  DW_TAG_member
	.dwattr $C$DW$1412, DW_AT_type(*$C$DW$T$693)
	.dwattr $C$DW$1412, DW_AT_name("layerParams")
	.dwattr $C$DW$1412, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1412, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1412, DW_AT_decl_line(0x3a4)
	.dwattr $C$DW$1412, DW_AT_decl_column(0x1a)

$C$DW$1413	.dwtag  DW_TAG_member
	.dwattr $C$DW$1413, DW_AT_type(*$C$DW$T$694)
	.dwattr $C$DW$1413, DW_AT_name("metaData")
	.dwattr $C$DW$1413, DW_AT_data_member_location[DW_OP_plus_uconst 0x2080]
	.dwattr $C$DW$1413, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1413, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1413, DW_AT_decl_line(0x3a5)
	.dwattr $C$DW$1413, DW_AT_decl_column(0x1f)

$C$DW$1414	.dwtag  DW_TAG_member
	.dwattr $C$DW$1414, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1414, DW_AT_name("layerIdx")
	.dwattr $C$DW$1414, DW_AT_data_member_location[DW_OP_plus_uconst 0x2108]
	.dwattr $C$DW$1414, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1414, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1414, DW_AT_decl_line(0x3a6)
	.dwattr $C$DW$1414, DW_AT_decl_column(0x0b)

$C$DW$1415	.dwtag  DW_TAG_member
	.dwattr $C$DW$1415, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1415, DW_AT_name("procType")
	.dwattr $C$DW$1415, DW_AT_data_member_location[DW_OP_plus_uconst 0x210c]
	.dwattr $C$DW$1415, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1415, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1415, DW_AT_decl_line(0x3a7)
	.dwattr $C$DW$1415, DW_AT_decl_column(0x0b)

$C$DW$1416	.dwtag  DW_TAG_member
	.dwattr $C$DW$1416, DW_AT_type(*$C$DW$T$599)
	.dwattr $C$DW$1416, DW_AT_name("inLayerIdx")
	.dwattr $C$DW$1416, DW_AT_data_member_location[DW_OP_plus_uconst 0x2110]
	.dwattr $C$DW$1416, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1416, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1416, DW_AT_decl_line(0x3a8)
	.dwattr $C$DW$1416, DW_AT_decl_column(0x0b)

$C$DW$1417	.dwtag  DW_TAG_member
	.dwattr $C$DW$1417, DW_AT_type(*$C$DW$T$599)
	.dwattr $C$DW$1417, DW_AT_name("isInData")
	.dwattr $C$DW$1417, DW_AT_data_member_location[DW_OP_plus_uconst 0x2150]
	.dwattr $C$DW$1417, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1417, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1417, DW_AT_decl_line(0x3a9)
	.dwattr $C$DW$1417, DW_AT_decl_column(0x0b)

$C$DW$1418	.dwtag  DW_TAG_member
	.dwattr $C$DW$1418, DW_AT_type(*$C$DW$T$599)
	.dwattr $C$DW$1418, DW_AT_name("isInOutData")
	.dwattr $C$DW$1418, DW_AT_data_member_location[DW_OP_plus_uconst 0x2190]
	.dwattr $C$DW$1418, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1418, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1418, DW_AT_decl_line(0x3aa)
	.dwattr $C$DW$1418, DW_AT_decl_column(0x0b)

$C$DW$1419	.dwtag  DW_TAG_member
	.dwattr $C$DW$1419, DW_AT_type(*$C$DW$T$599)
	.dwattr $C$DW$1419, DW_AT_name("isOutData")
	.dwattr $C$DW$1419, DW_AT_data_member_location[DW_OP_plus_uconst 0x21d0]
	.dwattr $C$DW$1419, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1419, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1419, DW_AT_decl_line(0x3ab)
	.dwattr $C$DW$1419, DW_AT_decl_column(0x0b)

$C$DW$1420	.dwtag  DW_TAG_member
	.dwattr $C$DW$1420, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$1420, DW_AT_name("scratchMem")
	.dwattr $C$DW$1420, DW_AT_data_member_location[DW_OP_plus_uconst 0x2210]
	.dwattr $C$DW$1420, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1420, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1420, DW_AT_decl_line(0x3ac)
	.dwattr $C$DW$1420, DW_AT_decl_column(0x0b)

$C$DW$1421	.dwtag  DW_TAG_member
	.dwattr $C$DW$1421, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1421, DW_AT_name("scratchSize")
	.dwattr $C$DW$1421, DW_AT_data_member_location[DW_OP_plus_uconst 0x2218]
	.dwattr $C$DW$1421, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1421, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1421, DW_AT_decl_line(0x3ad)
	.dwattr $C$DW$1421, DW_AT_decl_column(0x0b)

$C$DW$1422	.dwtag  DW_TAG_member
	.dwattr $C$DW$1422, DW_AT_type(*$C$DW$T$583)
	.dwattr $C$DW$1422, DW_AT_name("dataFlowInfo")
	.dwattr $C$DW$1422, DW_AT_data_member_location[DW_OP_plus_uconst 0x2220]
	.dwattr $C$DW$1422, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1422, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1422, DW_AT_decl_line(0x3af)
	.dwattr $C$DW$1422, DW_AT_decl_column(0x15)

$C$DW$1423	.dwtag  DW_TAG_member
	.dwattr $C$DW$1423, DW_AT_type(*$C$DW$T$698)
	.dwattr $C$DW$1423, DW_AT_name("TIDL_GenericFlowHandle")
	.dwattr $C$DW$1423, DW_AT_data_member_location[DW_OP_plus_uconst 0x2228]
	.dwattr $C$DW$1423, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1423, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1423, DW_AT_decl_line(0x3b0)
	.dwattr $C$DW$1423, DW_AT_decl_column(0x18)

$C$DW$1424	.dwtag  DW_TAG_member
	.dwattr $C$DW$1424, DW_AT_type(*$C$DW$T$700)
	.dwattr $C$DW$1424, DW_AT_name("kernelHandle")
	.dwattr $C$DW$1424, DW_AT_data_member_location[DW_OP_plus_uconst 0x22e8]
	.dwattr $C$DW$1424, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1424, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1424, DW_AT_decl_line(0x3b1)
	.dwattr $C$DW$1424, DW_AT_decl_column(0x09)

$C$DW$1425	.dwtag  DW_TAG_member
	.dwattr $C$DW$1425, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1425, DW_AT_name("kerHandleSize")
	.dwattr $C$DW$1425, DW_AT_data_member_location[DW_OP_plus_uconst 0x23a8]
	.dwattr $C$DW$1425, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1425, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1425, DW_AT_decl_line(0x3b2)
	.dwattr $C$DW$1425, DW_AT_decl_column(0x0b)

$C$DW$1426	.dwtag  DW_TAG_member
	.dwattr $C$DW$1426, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1426, DW_AT_name("totalL1Size")
	.dwattr $C$DW$1426, DW_AT_data_member_location[DW_OP_plus_uconst 0x23ac]
	.dwattr $C$DW$1426, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1426, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1426, DW_AT_decl_line(0x3b3)
	.dwattr $C$DW$1426, DW_AT_decl_column(0x0b)

$C$DW$1427	.dwtag  DW_TAG_member
	.dwattr $C$DW$1427, DW_AT_type(*$C$DW$T$364)
	.dwattr $C$DW$1427, DW_AT_name("dataFlowMemPtr")
	.dwattr $C$DW$1427, DW_AT_data_member_location[DW_OP_plus_uconst 0x23b0]
	.dwattr $C$DW$1427, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1427, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1427, DW_AT_decl_line(0x3b4)
	.dwattr $C$DW$1427, DW_AT_decl_column(0x0c)

$C$DW$1428	.dwtag  DW_TAG_member
	.dwattr $C$DW$1428, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1428, DW_AT_name("dataFlowMemSize")
	.dwattr $C$DW$1428, DW_AT_data_member_location[DW_OP_plus_uconst 0x23b8]
	.dwattr $C$DW$1428, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1428, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1428, DW_AT_decl_line(0x3b5)
	.dwattr $C$DW$1428, DW_AT_decl_column(0x0b)


$C$DW$1429	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1429, DW_AT_name("operator =")
	.dwattr $C$DW$1429, DW_AT_declaration
	.dwattr $C$DW$1429, DW_AT_linkage_name("_ZN16sTIDL_AlgLayer_taSERKS_")
	.dwattr $C$DW$1429, DW_AT_type(*$C$DW$T$701)
	.dwattr $C$DW$1429, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1430	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1430, DW_AT_type(*$C$DW$T$703)

	.dwendtag $C$DW$1429


$C$DW$1431	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1431, DW_AT_name("operator =")
	.dwattr $C$DW$1431, DW_AT_declaration
	.dwattr $C$DW$1431, DW_AT_linkage_name("_ZN16sTIDL_AlgLayer_taSEOS_")
	.dwattr $C$DW$1431, DW_AT_type(*$C$DW$T$701)
	.dwattr $C$DW$1431, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1432	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1432, DW_AT_type(*$C$DW$T$701)

	.dwendtag $C$DW$1431

	.dwattr $C$DW$T$706, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$T$706, DW_AT_decl_line(0x3a3)
	.dwattr $C$DW$T$706, DW_AT_decl_column(0x10)
	.dwendtag $C$DW$T$706

	.dwendtag $C$DW$TU$706


$C$DW$TU$701	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$701
$C$DW$T$701	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$701, DW_AT_type(*$C$DW$T$706)
	.dwattr $C$DW$T$701, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$701


$C$DW$TU$704	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$704

$C$DW$T$704	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$704, DW_AT_type(*$C$DW$T$701)
$C$DW$1433	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1433, DW_AT_type(*$C$DW$T$703)

	.dwendtag $C$DW$T$704

	.dwendtag $C$DW$TU$704


$C$DW$TU$705	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$705

$C$DW$T$705	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$705, DW_AT_type(*$C$DW$T$701)
$C$DW$1434	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1434, DW_AT_type(*$C$DW$T$701)

	.dwendtag $C$DW$T$705

	.dwendtag $C$DW$TU$705


$C$DW$TU$360	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$360
$C$DW$T$360	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$360, DW_AT_name("sTIDL_AlgLayer_t")
	.dwattr $C$DW$T$360, DW_AT_type(*$C$DW$T$706)
	.dwattr $C$DW$T$360, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$T$360, DW_AT_decl_line(0x3b6)
	.dwattr $C$DW$T$360, DW_AT_decl_column(0x02)

	.dwendtag $C$DW$TU$360


$C$DW$TU$361	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$361
$C$DW$T$361	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$361, DW_AT_type(*$C$DW$T$360)
	.dwattr $C$DW$T$361, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$361


$C$DW$TU$702	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$702
$C$DW$T$702	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$702, DW_AT_type(*$C$DW$T$706)

	.dwendtag $C$DW$TU$702


$C$DW$TU$703	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$703
$C$DW$T$703	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$703, DW_AT_type(*$C$DW$T$702)
	.dwattr $C$DW$T$703, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$703


$C$DW$TU$719	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$719

$C$DW$T$719	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$719, DW_AT_name("sTIDL_ArgMaxParams_t")
	.dwattr $C$DW$T$719, DW_AT_byte_size(0x0c)
$C$DW$1435	.dwtag  DW_TAG_member
	.dwattr $C$DW$1435, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1435, DW_AT_name("numChannels")
	.dwattr $C$DW$1435, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1435, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1435, DW_AT_decl_line(0x379)
	.dwattr $C$DW$1435, DW_AT_decl_column(0x0d)

$C$DW$1436	.dwtag  DW_TAG_member
	.dwattr $C$DW$1436, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1436, DW_AT_name("inDataQ")
	.dwattr $C$DW$1436, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$1436, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1436, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1436, DW_AT_decl_line(0x37b)
	.dwattr $C$DW$1436, DW_AT_decl_column(0x0d)

$C$DW$1437	.dwtag  DW_TAG_member
	.dwattr $C$DW$1437, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1437, DW_AT_name("outDataQ")
	.dwattr $C$DW$1437, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$1437, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1437, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1437, DW_AT_decl_line(0x37d)
	.dwattr $C$DW$1437, DW_AT_decl_column(0x0d)


$C$DW$1438	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1438, DW_AT_name("operator =")
	.dwattr $C$DW$1438, DW_AT_declaration
	.dwattr $C$DW$1438, DW_AT_linkage_name("_ZN20sTIDL_ArgMaxParams_taSERKS_")
	.dwattr $C$DW$1438, DW_AT_type(*$C$DW$T$714)
	.dwattr $C$DW$1438, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1439	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1439, DW_AT_type(*$C$DW$T$716)

	.dwendtag $C$DW$1438


$C$DW$1440	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1440, DW_AT_name("operator =")
	.dwattr $C$DW$1440, DW_AT_declaration
	.dwattr $C$DW$1440, DW_AT_linkage_name("_ZN20sTIDL_ArgMaxParams_taSEOS_")
	.dwattr $C$DW$1440, DW_AT_type(*$C$DW$T$714)
	.dwattr $C$DW$1440, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1441	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1441, DW_AT_type(*$C$DW$T$714)

	.dwendtag $C$DW$1440

	.dwattr $C$DW$T$719, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$719, DW_AT_decl_line(0x377)
	.dwattr $C$DW$T$719, DW_AT_decl_column(0x10)
	.dwendtag $C$DW$T$719

	.dwendtag $C$DW$TU$719


$C$DW$TU$714	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$714
$C$DW$T$714	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$714, DW_AT_type(*$C$DW$T$719)
	.dwattr $C$DW$T$714, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$714


$C$DW$TU$717	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$717

$C$DW$T$717	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$717, DW_AT_type(*$C$DW$T$714)
$C$DW$1442	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1442, DW_AT_type(*$C$DW$T$716)

	.dwendtag $C$DW$T$717

	.dwendtag $C$DW$TU$717


$C$DW$TU$718	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$718

$C$DW$T$718	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$718, DW_AT_type(*$C$DW$T$714)
$C$DW$1443	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1443, DW_AT_type(*$C$DW$T$714)

	.dwendtag $C$DW$T$718

	.dwendtag $C$DW$TU$718


$C$DW$TU$715	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$715
$C$DW$T$715	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$715, DW_AT_type(*$C$DW$T$719)

	.dwendtag $C$DW$TU$715


$C$DW$TU$716	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$716
$C$DW$T$716	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$716, DW_AT_type(*$C$DW$T$715)
	.dwattr $C$DW$T$716, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$716


$C$DW$TU$829	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$829
$C$DW$T$829	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$829, DW_AT_name("sTIDL_ArgMaxParams_t")
	.dwattr $C$DW$T$829, DW_AT_type(*$C$DW$T$719)
	.dwattr $C$DW$T$829, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$829, DW_AT_decl_line(0x37e)
	.dwattr $C$DW$T$829, DW_AT_decl_column(0x02)

	.dwendtag $C$DW$TU$829


$C$DW$TU$726	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$726

$C$DW$T$726	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$726, DW_AT_name("sTIDL_BatchNormParams_t")
	.dwattr $C$DW$T$726, DW_AT_byte_size(0x28)
$C$DW$1444	.dwtag  DW_TAG_member
	.dwattr $C$DW$1444, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1444, DW_AT_name("weights")
	.dwattr $C$DW$1444, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1444, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1444, DW_AT_decl_line(0x4bf)
	.dwattr $C$DW$1444, DW_AT_decl_column(0x0d)

$C$DW$1445	.dwtag  DW_TAG_member
	.dwattr $C$DW$1445, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1445, DW_AT_name("bias")
	.dwattr $C$DW$1445, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$1445, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1445, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1445, DW_AT_decl_line(0x4c1)
	.dwattr $C$DW$1445, DW_AT_decl_column(0x0d)

$C$DW$1446	.dwtag  DW_TAG_member
	.dwattr $C$DW$1446, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1446, DW_AT_name("numChannels")
	.dwattr $C$DW$1446, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$1446, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1446, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1446, DW_AT_decl_line(0x4c3)
	.dwattr $C$DW$1446, DW_AT_decl_column(0x0d)

$C$DW$1447	.dwtag  DW_TAG_member
	.dwattr $C$DW$1447, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1447, DW_AT_name("biasQ")
	.dwattr $C$DW$1447, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$1447, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1447, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1447, DW_AT_decl_line(0x4c5)
	.dwattr $C$DW$1447, DW_AT_decl_column(0x0d)

$C$DW$1448	.dwtag  DW_TAG_member
	.dwattr $C$DW$1448, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1448, DW_AT_name("inDataQ")
	.dwattr $C$DW$1448, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$1448, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1448, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1448, DW_AT_decl_line(0x4c7)
	.dwattr $C$DW$1448, DW_AT_decl_column(0x0d)

$C$DW$1449	.dwtag  DW_TAG_member
	.dwattr $C$DW$1449, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1449, DW_AT_name("outDataQ")
	.dwattr $C$DW$1449, DW_AT_data_member_location[DW_OP_plus_uconst 0x14]
	.dwattr $C$DW$1449, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1449, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1449, DW_AT_decl_line(0x4c9)
	.dwattr $C$DW$1449, DW_AT_decl_column(0x0d)

$C$DW$1450	.dwtag  DW_TAG_member
	.dwattr $C$DW$1450, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1450, DW_AT_name("weightsQ")
	.dwattr $C$DW$1450, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$1450, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1450, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1450, DW_AT_decl_line(0x4cb)
	.dwattr $C$DW$1450, DW_AT_decl_column(0x0d)

$C$DW$1451	.dwtag  DW_TAG_member
	.dwattr $C$DW$1451, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$1451, DW_AT_name("weightScale")
	.dwattr $C$DW$1451, DW_AT_data_member_location[DW_OP_plus_uconst 0x1c]
	.dwattr $C$DW$1451, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1451, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1451, DW_AT_decl_line(0x4cd)
	.dwattr $C$DW$1451, DW_AT_decl_column(0x10)

$C$DW$1452	.dwtag  DW_TAG_member
	.dwattr $C$DW$1452, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$1452, DW_AT_name("biasScale")
	.dwattr $C$DW$1452, DW_AT_data_member_location[DW_OP_plus_uconst 0x20]
	.dwattr $C$DW$1452, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1452, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1452, DW_AT_decl_line(0x4cf)
	.dwattr $C$DW$1452, DW_AT_decl_column(0x10)

$C$DW$1453	.dwtag  DW_TAG_member
	.dwattr $C$DW$1453, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1453, DW_AT_name("zeroWeightValue")
	.dwattr $C$DW$1453, DW_AT_data_member_location[DW_OP_plus_uconst 0x24]
	.dwattr $C$DW$1453, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1453, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1453, DW_AT_decl_line(0x4d1)
	.dwattr $C$DW$1453, DW_AT_decl_column(0x0d)


$C$DW$1454	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1454, DW_AT_name("operator =")
	.dwattr $C$DW$1454, DW_AT_declaration
	.dwattr $C$DW$1454, DW_AT_linkage_name("_ZN23sTIDL_BatchNormParams_taSERKS_")
	.dwattr $C$DW$1454, DW_AT_type(*$C$DW$T$721)
	.dwattr $C$DW$1454, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1455	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1455, DW_AT_type(*$C$DW$T$723)

	.dwendtag $C$DW$1454


$C$DW$1456	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1456, DW_AT_name("operator =")
	.dwattr $C$DW$1456, DW_AT_declaration
	.dwattr $C$DW$1456, DW_AT_linkage_name("_ZN23sTIDL_BatchNormParams_taSEOS_")
	.dwattr $C$DW$1456, DW_AT_type(*$C$DW$T$721)
	.dwattr $C$DW$1456, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1457	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1457, DW_AT_type(*$C$DW$T$721)

	.dwendtag $C$DW$1456

	.dwattr $C$DW$T$726, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$726, DW_AT_decl_line(0x4bd)
	.dwattr $C$DW$T$726, DW_AT_decl_column(0x10)
	.dwendtag $C$DW$T$726

	.dwendtag $C$DW$TU$726


$C$DW$TU$721	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$721
$C$DW$T$721	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$721, DW_AT_type(*$C$DW$T$726)
	.dwattr $C$DW$T$721, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$721


$C$DW$TU$724	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$724

$C$DW$T$724	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$724, DW_AT_type(*$C$DW$T$721)
$C$DW$1458	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1458, DW_AT_type(*$C$DW$T$723)

	.dwendtag $C$DW$T$724

	.dwendtag $C$DW$TU$724


$C$DW$TU$725	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$725

$C$DW$T$725	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$725, DW_AT_type(*$C$DW$T$721)
$C$DW$1459	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1459, DW_AT_type(*$C$DW$T$721)

	.dwendtag $C$DW$T$725

	.dwendtag $C$DW$TU$725


$C$DW$TU$722	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$722
$C$DW$T$722	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$722, DW_AT_type(*$C$DW$T$726)

	.dwendtag $C$DW$TU$722


$C$DW$TU$723	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$723
$C$DW$T$723	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$723, DW_AT_type(*$C$DW$T$722)
	.dwattr $C$DW$T$723, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$723


$C$DW$TU$835	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$835
$C$DW$T$835	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$835, DW_AT_name("sTIDL_BatchNormParams_t")
	.dwattr $C$DW$T$835, DW_AT_type(*$C$DW$T$726)
	.dwattr $C$DW$T$835, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$835, DW_AT_decl_line(0x4d2)
	.dwattr $C$DW$T$835, DW_AT_decl_column(0x02)

	.dwendtag $C$DW$TU$835


$C$DW$TU$733	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$733

$C$DW$T$733	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$733, DW_AT_name("sTIDL_BiasParams_t")
	.dwattr $C$DW$T$733, DW_AT_byte_size(0x14)
$C$DW$1460	.dwtag  DW_TAG_member
	.dwattr $C$DW$1460, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1460, DW_AT_name("bias")
	.dwattr $C$DW$1460, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1460, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1460, DW_AT_decl_line(0x4db)
	.dwattr $C$DW$1460, DW_AT_decl_column(0x0d)

$C$DW$1461	.dwtag  DW_TAG_member
	.dwattr $C$DW$1461, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1461, DW_AT_name("numChannels")
	.dwattr $C$DW$1461, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$1461, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1461, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1461, DW_AT_decl_line(0x4dd)
	.dwattr $C$DW$1461, DW_AT_decl_column(0x0d)

$C$DW$1462	.dwtag  DW_TAG_member
	.dwattr $C$DW$1462, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1462, DW_AT_name("biasQ")
	.dwattr $C$DW$1462, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$1462, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1462, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1462, DW_AT_decl_line(0x4df)
	.dwattr $C$DW$1462, DW_AT_decl_column(0x0d)

$C$DW$1463	.dwtag  DW_TAG_member
	.dwattr $C$DW$1463, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1463, DW_AT_name("inDataQ")
	.dwattr $C$DW$1463, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$1463, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1463, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1463, DW_AT_decl_line(0x4e1)
	.dwattr $C$DW$1463, DW_AT_decl_column(0x0d)

$C$DW$1464	.dwtag  DW_TAG_member
	.dwattr $C$DW$1464, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1464, DW_AT_name("outDataQ")
	.dwattr $C$DW$1464, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$1464, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1464, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1464, DW_AT_decl_line(0x4e3)
	.dwattr $C$DW$1464, DW_AT_decl_column(0x0d)


$C$DW$1465	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1465, DW_AT_name("operator =")
	.dwattr $C$DW$1465, DW_AT_declaration
	.dwattr $C$DW$1465, DW_AT_linkage_name("_ZN18sTIDL_BiasParams_taSERKS_")
	.dwattr $C$DW$1465, DW_AT_type(*$C$DW$T$728)
	.dwattr $C$DW$1465, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1466	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1466, DW_AT_type(*$C$DW$T$730)

	.dwendtag $C$DW$1465


$C$DW$1467	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1467, DW_AT_name("operator =")
	.dwattr $C$DW$1467, DW_AT_declaration
	.dwattr $C$DW$1467, DW_AT_linkage_name("_ZN18sTIDL_BiasParams_taSEOS_")
	.dwattr $C$DW$1467, DW_AT_type(*$C$DW$T$728)
	.dwattr $C$DW$1467, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1468	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1468, DW_AT_type(*$C$DW$T$728)

	.dwendtag $C$DW$1467

	.dwattr $C$DW$T$733, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$733, DW_AT_decl_line(0x4d9)
	.dwattr $C$DW$T$733, DW_AT_decl_column(0x10)
	.dwendtag $C$DW$T$733

	.dwendtag $C$DW$TU$733


$C$DW$TU$728	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$728
$C$DW$T$728	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$728, DW_AT_type(*$C$DW$T$733)
	.dwattr $C$DW$T$728, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$728


$C$DW$TU$731	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$731

$C$DW$T$731	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$731, DW_AT_type(*$C$DW$T$728)
$C$DW$1469	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1469, DW_AT_type(*$C$DW$T$730)

	.dwendtag $C$DW$T$731

	.dwendtag $C$DW$TU$731


$C$DW$TU$732	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$732

$C$DW$T$732	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$732, DW_AT_type(*$C$DW$T$728)
$C$DW$1470	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1470, DW_AT_type(*$C$DW$T$728)

	.dwendtag $C$DW$T$732

	.dwendtag $C$DW$TU$732


$C$DW$TU$729	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$729
$C$DW$T$729	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$729, DW_AT_type(*$C$DW$T$733)

	.dwendtag $C$DW$TU$729


$C$DW$TU$730	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$730
$C$DW$T$730	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$730, DW_AT_type(*$C$DW$T$729)
	.dwattr $C$DW$T$730, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$730


$C$DW$TU$834	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$834
$C$DW$T$834	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$834, DW_AT_name("sTIDL_BiasParams_t")
	.dwattr $C$DW$T$834, DW_AT_type(*$C$DW$T$733)
	.dwattr $C$DW$T$834, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$834, DW_AT_decl_line(0x4e4)
	.dwattr $C$DW$T$834, DW_AT_decl_column(0x02)

	.dwendtag $C$DW$TU$834


$C$DW$TU$740	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$740

$C$DW$T$740	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$740, DW_AT_name("sTIDL_CalibParams_t")
	.dwattr $C$DW$T$740, DW_AT_byte_size(0x18)
$C$DW$1471	.dwtag  DW_TAG_member
	.dwattr $C$DW$1471, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1471, DW_AT_name("activationRangeMethod")
	.dwattr $C$DW$1471, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1471, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1471, DW_AT_decl_line(0x351)
	.dwattr $C$DW$1471, DW_AT_decl_column(0x0b)

$C$DW$1472	.dwtag  DW_TAG_member
	.dwattr $C$DW$1472, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1472, DW_AT_name("weightRangeMethod")
	.dwattr $C$DW$1472, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$1472, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1472, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1472, DW_AT_decl_line(0x358)
	.dwattr $C$DW$1472, DW_AT_decl_column(0x0b)

$C$DW$1473	.dwtag  DW_TAG_member
	.dwattr $C$DW$1473, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$1473, DW_AT_name("percentileActRangeShrink")
	.dwattr $C$DW$1473, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$1473, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1473, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1473, DW_AT_decl_line(0x360)
	.dwattr $C$DW$1473, DW_AT_decl_column(0x10)

$C$DW$1474	.dwtag  DW_TAG_member
	.dwattr $C$DW$1474, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$1474, DW_AT_name("percentileWtRangeShrink")
	.dwattr $C$DW$1474, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$1474, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1474, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1474, DW_AT_decl_line(0x367)
	.dwattr $C$DW$1474, DW_AT_decl_column(0x10)

$C$DW$1475	.dwtag  DW_TAG_member
	.dwattr $C$DW$1475, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$1475, DW_AT_name("biasCalibrationFactor")
	.dwattr $C$DW$1475, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$1475, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1475, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1475, DW_AT_decl_line(0x36a)
	.dwattr $C$DW$1475, DW_AT_decl_column(0x10)

$C$DW$1476	.dwtag  DW_TAG_member
	.dwattr $C$DW$1476, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1476, DW_AT_name("biasCalibrationIterations")
	.dwattr $C$DW$1476, DW_AT_data_member_location[DW_OP_plus_uconst 0x14]
	.dwattr $C$DW$1476, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1476, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1476, DW_AT_decl_line(0x36e)
	.dwattr $C$DW$1476, DW_AT_decl_column(0x0b)


$C$DW$1477	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1477, DW_AT_name("operator =")
	.dwattr $C$DW$1477, DW_AT_declaration
	.dwattr $C$DW$1477, DW_AT_linkage_name("_ZN19sTIDL_CalibParams_taSERKS_")
	.dwattr $C$DW$1477, DW_AT_type(*$C$DW$T$735)
	.dwattr $C$DW$1477, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1478	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1478, DW_AT_type(*$C$DW$T$737)

	.dwendtag $C$DW$1477


$C$DW$1479	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1479, DW_AT_name("operator =")
	.dwattr $C$DW$1479, DW_AT_declaration
	.dwattr $C$DW$1479, DW_AT_linkage_name("_ZN19sTIDL_CalibParams_taSEOS_")
	.dwattr $C$DW$1479, DW_AT_type(*$C$DW$T$735)
	.dwattr $C$DW$1479, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1480	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1480, DW_AT_type(*$C$DW$T$735)

	.dwendtag $C$DW$1479

	.dwattr $C$DW$T$740, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$740, DW_AT_decl_line(0x34c)
	.dwattr $C$DW$T$740, DW_AT_decl_column(0x10)
	.dwendtag $C$DW$T$740

	.dwendtag $C$DW$TU$740


$C$DW$TU$735	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$735
$C$DW$T$735	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$735, DW_AT_type(*$C$DW$T$740)
	.dwattr $C$DW$T$735, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$735


$C$DW$TU$738	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$738

$C$DW$T$738	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$738, DW_AT_type(*$C$DW$T$735)
$C$DW$1481	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1481, DW_AT_type(*$C$DW$T$737)

	.dwendtag $C$DW$T$738

	.dwendtag $C$DW$TU$738


$C$DW$TU$739	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$739

$C$DW$T$739	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$739, DW_AT_type(*$C$DW$T$735)
$C$DW$1482	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1482, DW_AT_type(*$C$DW$T$735)

	.dwendtag $C$DW$T$739

	.dwendtag $C$DW$TU$739


$C$DW$TU$736	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$736
$C$DW$T$736	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$736, DW_AT_type(*$C$DW$T$740)

	.dwendtag $C$DW$TU$736


$C$DW$TU$737	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$737
$C$DW$T$737	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$737, DW_AT_type(*$C$DW$T$736)
	.dwattr $C$DW$T$737, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$737


$C$DW$TU$887	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$887
$C$DW$T$887	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$887, DW_AT_name("sTIDL_CalibParams_t")
	.dwattr $C$DW$T$887, DW_AT_type(*$C$DW$T$740)
	.dwattr $C$DW$T$887, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$887, DW_AT_decl_line(0x36f)
	.dwattr $C$DW$T$887, DW_AT_decl_column(0x02)

	.dwendtag $C$DW$TU$887


$C$DW$TU$747	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$747

$C$DW$T$747	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$747, DW_AT_name("sTIDL_ConcatParams_t")
	.dwattr $C$DW$T$747, DW_AT_byte_size(0x08)
$C$DW$1483	.dwtag  DW_TAG_member
	.dwattr $C$DW$1483, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1483, DW_AT_name("axis")
	.dwattr $C$DW$1483, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1483, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1483, DW_AT_decl_line(0x4b1)
	.dwattr $C$DW$1483, DW_AT_decl_column(0x0c)

$C$DW$1484	.dwtag  DW_TAG_member
	.dwattr $C$DW$1484, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1484, DW_AT_name("outDataQ")
	.dwattr $C$DW$1484, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$1484, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1484, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1484, DW_AT_decl_line(0x4b3)
	.dwattr $C$DW$1484, DW_AT_decl_column(0x0c)


$C$DW$1485	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1485, DW_AT_name("operator =")
	.dwattr $C$DW$1485, DW_AT_declaration
	.dwattr $C$DW$1485, DW_AT_linkage_name("_ZN20sTIDL_ConcatParams_taSERKS_")
	.dwattr $C$DW$1485, DW_AT_type(*$C$DW$T$742)
	.dwattr $C$DW$1485, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1486	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1486, DW_AT_type(*$C$DW$T$744)

	.dwendtag $C$DW$1485


$C$DW$1487	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1487, DW_AT_name("operator =")
	.dwattr $C$DW$1487, DW_AT_declaration
	.dwattr $C$DW$1487, DW_AT_linkage_name("_ZN20sTIDL_ConcatParams_taSEOS_")
	.dwattr $C$DW$1487, DW_AT_type(*$C$DW$T$742)
	.dwattr $C$DW$1487, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1488	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1488, DW_AT_type(*$C$DW$T$742)

	.dwendtag $C$DW$1487

	.dwattr $C$DW$T$747, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$747, DW_AT_decl_line(0x4af)
	.dwattr $C$DW$T$747, DW_AT_decl_column(0x10)
	.dwendtag $C$DW$T$747

	.dwendtag $C$DW$TU$747


$C$DW$TU$742	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$742
$C$DW$T$742	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$742, DW_AT_type(*$C$DW$T$747)
	.dwattr $C$DW$T$742, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$742


$C$DW$TU$745	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$745

$C$DW$T$745	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$745, DW_AT_type(*$C$DW$T$742)
$C$DW$1489	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1489, DW_AT_type(*$C$DW$T$744)

	.dwendtag $C$DW$T$745

	.dwendtag $C$DW$TU$745


$C$DW$TU$746	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$746

$C$DW$T$746	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$746, DW_AT_type(*$C$DW$T$742)
$C$DW$1490	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1490, DW_AT_type(*$C$DW$T$742)

	.dwendtag $C$DW$T$746

	.dwendtag $C$DW$TU$746


$C$DW$TU$743	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$743
$C$DW$T$743	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$743, DW_AT_type(*$C$DW$T$747)

	.dwendtag $C$DW$TU$743


$C$DW$TU$744	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$744
$C$DW$T$744	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$744, DW_AT_type(*$C$DW$T$743)
	.dwattr $C$DW$T$744, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$744


$C$DW$TU$832	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$832
$C$DW$T$832	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$832, DW_AT_name("sTIDL_ConcatParams_t")
	.dwattr $C$DW$T$832, DW_AT_type(*$C$DW$T$747)
	.dwattr $C$DW$T$832, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$832, DW_AT_decl_line(0x4b4)
	.dwattr $C$DW$T$832, DW_AT_decl_column(0x02)

	.dwendtag $C$DW$TU$832


$C$DW$TU$755	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$755

$C$DW$T$755	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$755, DW_AT_name("sTIDL_ConvParams_t")
	.dwattr $C$DW$T$755, DW_AT_byte_size(0xa8)
$C$DW$1491	.dwtag  DW_TAG_member
	.dwattr $C$DW$1491, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1491, DW_AT_name("weights")
	.dwattr $C$DW$1491, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1491, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1491, DW_AT_decl_line(0x3dc)
	.dwattr $C$DW$1491, DW_AT_decl_column(0x0d)

$C$DW$1492	.dwtag  DW_TAG_member
	.dwattr $C$DW$1492, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1492, DW_AT_name("bias")
	.dwattr $C$DW$1492, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$1492, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1492, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1492, DW_AT_decl_line(0x3de)
	.dwattr $C$DW$1492, DW_AT_decl_column(0x0d)

$C$DW$1493	.dwtag  DW_TAG_member
	.dwattr $C$DW$1493, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1493, DW_AT_name("perChannelWeightScaleOffset")
	.dwattr $C$DW$1493, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$1493, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1493, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1493, DW_AT_decl_line(0x3e1)
	.dwattr $C$DW$1493, DW_AT_decl_column(0x0d)

$C$DW$1494	.dwtag  DW_TAG_member
	.dwattr $C$DW$1494, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1494, DW_AT_name("convolutionType")
	.dwattr $C$DW$1494, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$1494, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1494, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1494, DW_AT_decl_line(0x3e3)
	.dwattr $C$DW$1494, DW_AT_decl_column(0x0d)

$C$DW$1495	.dwtag  DW_TAG_member
	.dwattr $C$DW$1495, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1495, DW_AT_name("numInChannels")
	.dwattr $C$DW$1495, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$1495, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1495, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1495, DW_AT_decl_line(0x3e5)
	.dwattr $C$DW$1495, DW_AT_decl_column(0x0d)

$C$DW$1496	.dwtag  DW_TAG_member
	.dwattr $C$DW$1496, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1496, DW_AT_name("numOutChannels")
	.dwattr $C$DW$1496, DW_AT_data_member_location[DW_OP_plus_uconst 0x14]
	.dwattr $C$DW$1496, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1496, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1496, DW_AT_decl_line(0x3e7)
	.dwattr $C$DW$1496, DW_AT_decl_column(0x0d)

$C$DW$1497	.dwtag  DW_TAG_member
	.dwattr $C$DW$1497, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1497, DW_AT_name("numGroups")
	.dwattr $C$DW$1497, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$1497, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1497, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1497, DW_AT_decl_line(0x3e9)
	.dwattr $C$DW$1497, DW_AT_decl_column(0x0d)

$C$DW$1498	.dwtag  DW_TAG_member
	.dwattr $C$DW$1498, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1498, DW_AT_name("kernelW")
	.dwattr $C$DW$1498, DW_AT_data_member_location[DW_OP_plus_uconst 0x1c]
	.dwattr $C$DW$1498, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1498, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1498, DW_AT_decl_line(0x3eb)
	.dwattr $C$DW$1498, DW_AT_decl_column(0x0d)

$C$DW$1499	.dwtag  DW_TAG_member
	.dwattr $C$DW$1499, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1499, DW_AT_name("kernelH")
	.dwattr $C$DW$1499, DW_AT_data_member_location[DW_OP_plus_uconst 0x20]
	.dwattr $C$DW$1499, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1499, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1499, DW_AT_decl_line(0x3ed)
	.dwattr $C$DW$1499, DW_AT_decl_column(0x0d)

$C$DW$1500	.dwtag  DW_TAG_member
	.dwattr $C$DW$1500, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1500, DW_AT_name("strideW")
	.dwattr $C$DW$1500, DW_AT_data_member_location[DW_OP_plus_uconst 0x24]
	.dwattr $C$DW$1500, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1500, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1500, DW_AT_decl_line(0x3ef)
	.dwattr $C$DW$1500, DW_AT_decl_column(0x0d)

$C$DW$1501	.dwtag  DW_TAG_member
	.dwattr $C$DW$1501, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1501, DW_AT_name("strideH")
	.dwattr $C$DW$1501, DW_AT_data_member_location[DW_OP_plus_uconst 0x28]
	.dwattr $C$DW$1501, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1501, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1501, DW_AT_decl_line(0x3f1)
	.dwattr $C$DW$1501, DW_AT_decl_column(0x0d)

$C$DW$1502	.dwtag  DW_TAG_member
	.dwattr $C$DW$1502, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1502, DW_AT_name("dilationW")
	.dwattr $C$DW$1502, DW_AT_data_member_location[DW_OP_plus_uconst 0x2c]
	.dwattr $C$DW$1502, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1502, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1502, DW_AT_decl_line(0x3f3)
	.dwattr $C$DW$1502, DW_AT_decl_column(0x0d)

$C$DW$1503	.dwtag  DW_TAG_member
	.dwattr $C$DW$1503, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1503, DW_AT_name("dilationH")
	.dwattr $C$DW$1503, DW_AT_data_member_location[DW_OP_plus_uconst 0x30]
	.dwattr $C$DW$1503, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1503, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1503, DW_AT_decl_line(0x3f5)
	.dwattr $C$DW$1503, DW_AT_decl_column(0x0d)

$C$DW$1504	.dwtag  DW_TAG_member
	.dwattr $C$DW$1504, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1504, DW_AT_name("padW")
	.dwattr $C$DW$1504, DW_AT_data_member_location[DW_OP_plus_uconst 0x34]
	.dwattr $C$DW$1504, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1504, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1504, DW_AT_decl_line(0x3f7)
	.dwattr $C$DW$1504, DW_AT_decl_column(0x0d)

$C$DW$1505	.dwtag  DW_TAG_member
	.dwattr $C$DW$1505, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1505, DW_AT_name("padH")
	.dwattr $C$DW$1505, DW_AT_data_member_location[DW_OP_plus_uconst 0x38]
	.dwattr $C$DW$1505, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1505, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1505, DW_AT_decl_line(0x3f9)
	.dwattr $C$DW$1505, DW_AT_decl_column(0x0d)

$C$DW$1506	.dwtag  DW_TAG_member
	.dwattr $C$DW$1506, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$1506, DW_AT_name("weightScale")
	.dwattr $C$DW$1506, DW_AT_data_member_location[DW_OP_plus_uconst 0x3c]
	.dwattr $C$DW$1506, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1506, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1506, DW_AT_decl_line(0x3fb)
	.dwattr $C$DW$1506, DW_AT_decl_column(0x14)

$C$DW$1507	.dwtag  DW_TAG_member
	.dwattr $C$DW$1507, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$1507, DW_AT_name("biasScale")
	.dwattr $C$DW$1507, DW_AT_data_member_location[DW_OP_plus_uconst 0x40]
	.dwattr $C$DW$1507, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1507, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1507, DW_AT_decl_line(0x3fd)
	.dwattr $C$DW$1507, DW_AT_decl_column(0x14)

$C$DW$1508	.dwtag  DW_TAG_member
	.dwattr $C$DW$1508, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1508, DW_AT_name("weightsQ")
	.dwattr $C$DW$1508, DW_AT_data_member_location[DW_OP_plus_uconst 0x44]
	.dwattr $C$DW$1508, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1508, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1508, DW_AT_decl_line(0x3ff)
	.dwattr $C$DW$1508, DW_AT_decl_column(0x0d)

$C$DW$1509	.dwtag  DW_TAG_member
	.dwattr $C$DW$1509, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1509, DW_AT_name("zeroWeightValue")
	.dwattr $C$DW$1509, DW_AT_data_member_location[DW_OP_plus_uconst 0x48]
	.dwattr $C$DW$1509, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1509, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1509, DW_AT_decl_line(0x401)
	.dwattr $C$DW$1509, DW_AT_decl_column(0x0d)

$C$DW$1510	.dwtag  DW_TAG_member
	.dwattr $C$DW$1510, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1510, DW_AT_name("biasB")
	.dwattr $C$DW$1510, DW_AT_data_member_location[DW_OP_plus_uconst 0x4c]
	.dwattr $C$DW$1510, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1510, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1510, DW_AT_decl_line(0x403)
	.dwattr $C$DW$1510, DW_AT_decl_column(0x0d)

$C$DW$1511	.dwtag  DW_TAG_member
	.dwattr $C$DW$1511, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1511, DW_AT_name("biasQ")
	.dwattr $C$DW$1511, DW_AT_data_member_location[DW_OP_plus_uconst 0x50]
	.dwattr $C$DW$1511, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1511, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1511, DW_AT_decl_line(0x405)
	.dwattr $C$DW$1511, DW_AT_decl_column(0x0d)

$C$DW$1512	.dwtag  DW_TAG_member
	.dwattr $C$DW$1512, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1512, DW_AT_name("inDataQ")
	.dwattr $C$DW$1512, DW_AT_data_member_location[DW_OP_plus_uconst 0x54]
	.dwattr $C$DW$1512, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1512, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1512, DW_AT_decl_line(0x407)
	.dwattr $C$DW$1512, DW_AT_decl_column(0x0d)

$C$DW$1513	.dwtag  DW_TAG_member
	.dwattr $C$DW$1513, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1513, DW_AT_name("outDataQ")
	.dwattr $C$DW$1513, DW_AT_data_member_location[DW_OP_plus_uconst 0x58]
	.dwattr $C$DW$1513, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1513, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1513, DW_AT_decl_line(0x409)
	.dwattr $C$DW$1513, DW_AT_decl_column(0x0d)

$C$DW$1514	.dwtag  DW_TAG_member
	.dwattr $C$DW$1514, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1514, DW_AT_name("interDataQ")
	.dwattr $C$DW$1514, DW_AT_data_member_location[DW_OP_plus_uconst 0x5c]
	.dwattr $C$DW$1514, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1514, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1514, DW_AT_decl_line(0x40b)
	.dwattr $C$DW$1514, DW_AT_decl_column(0x0d)

$C$DW$1515	.dwtag  DW_TAG_member
	.dwattr $C$DW$1515, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1515, DW_AT_name("enableBias")
	.dwattr $C$DW$1515, DW_AT_data_member_location[DW_OP_plus_uconst 0x60]
	.dwattr $C$DW$1515, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1515, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1515, DW_AT_decl_line(0x40d)
	.dwattr $C$DW$1515, DW_AT_decl_column(0x0d)

$C$DW$1516	.dwtag  DW_TAG_member
	.dwattr $C$DW$1516, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1516, DW_AT_name("enablePooling")
	.dwattr $C$DW$1516, DW_AT_data_member_location[DW_OP_plus_uconst 0x64]
	.dwattr $C$DW$1516, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1516, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1516, DW_AT_decl_line(0x40f)
	.dwattr $C$DW$1516, DW_AT_decl_column(0x0d)

$C$DW$1517	.dwtag  DW_TAG_member
	.dwattr $C$DW$1517, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1517, DW_AT_name("enableEltWise")
	.dwattr $C$DW$1517, DW_AT_data_member_location[DW_OP_plus_uconst 0x68]
	.dwattr $C$DW$1517, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1517, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1517, DW_AT_decl_line(0x411)
	.dwattr $C$DW$1517, DW_AT_decl_column(0x0d)

$C$DW$1518	.dwtag  DW_TAG_member
	.dwattr $C$DW$1518, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1518, DW_AT_name("enableEWRelU")
	.dwattr $C$DW$1518, DW_AT_data_member_location[DW_OP_plus_uconst 0x6c]
	.dwattr $C$DW$1518, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1518, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1518, DW_AT_decl_line(0x413)
	.dwattr $C$DW$1518, DW_AT_decl_column(0x0d)

$C$DW$1519	.dwtag  DW_TAG_member
	.dwattr $C$DW$1519, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1519, DW_AT_name("kernelType")
	.dwattr $C$DW$1519, DW_AT_data_member_location[DW_OP_plus_uconst 0x70]
	.dwattr $C$DW$1519, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1519, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1519, DW_AT_decl_line(0x415)
	.dwattr $C$DW$1519, DW_AT_decl_column(0x0d)

$C$DW$1520	.dwtag  DW_TAG_member
	.dwattr $C$DW$1520, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1520, DW_AT_name("enableDepthToSpace")
	.dwattr $C$DW$1520, DW_AT_data_member_location[DW_OP_plus_uconst 0x74]
	.dwattr $C$DW$1520, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1520, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1520, DW_AT_decl_line(0x417)
	.dwattr $C$DW$1520, DW_AT_decl_column(0x0d)

$C$DW$1521	.dwtag  DW_TAG_member
	.dwattr $C$DW$1521, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1521, DW_AT_name("upscaleFactor")
	.dwattr $C$DW$1521, DW_AT_data_member_location[DW_OP_plus_uconst 0x78]
	.dwattr $C$DW$1521, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1521, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1521, DW_AT_decl_line(0x41a)
	.dwattr $C$DW$1521, DW_AT_decl_column(0x0d)

$C$DW$1522	.dwtag  DW_TAG_member
	.dwattr $C$DW$1522, DW_AT_type(*$C$DW$T$749)
	.dwattr $C$DW$1522, DW_AT_name("poolParams")
	.dwattr $C$DW$1522, DW_AT_data_member_location[DW_OP_plus_uconst 0x7c]
	.dwattr $C$DW$1522, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1522, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1522, DW_AT_decl_line(0x41c)
	.dwattr $C$DW$1522, DW_AT_decl_column(0x19)


$C$DW$1523	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1523, DW_AT_name("operator =")
	.dwattr $C$DW$1523, DW_AT_declaration
	.dwattr $C$DW$1523, DW_AT_linkage_name("_ZN18sTIDL_ConvParams_taSERKS_")
	.dwattr $C$DW$1523, DW_AT_type(*$C$DW$T$750)
	.dwattr $C$DW$1523, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1524	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1524, DW_AT_type(*$C$DW$T$752)

	.dwendtag $C$DW$1523


$C$DW$1525	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1525, DW_AT_name("operator =")
	.dwattr $C$DW$1525, DW_AT_declaration
	.dwattr $C$DW$1525, DW_AT_linkage_name("_ZN18sTIDL_ConvParams_taSEOS_")
	.dwattr $C$DW$1525, DW_AT_type(*$C$DW$T$750)
	.dwattr $C$DW$1525, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1526	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1526, DW_AT_type(*$C$DW$T$750)

	.dwendtag $C$DW$1525

	.dwattr $C$DW$T$755, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$755, DW_AT_decl_line(0x3da)
	.dwattr $C$DW$T$755, DW_AT_decl_column(0x10)
	.dwendtag $C$DW$T$755

	.dwendtag $C$DW$TU$755


$C$DW$TU$750	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$750
$C$DW$T$750	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$750, DW_AT_type(*$C$DW$T$755)
	.dwattr $C$DW$T$750, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$750


$C$DW$TU$753	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$753

$C$DW$T$753	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$753, DW_AT_type(*$C$DW$T$750)
$C$DW$1527	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1527, DW_AT_type(*$C$DW$T$752)

	.dwendtag $C$DW$T$753

	.dwendtag $C$DW$TU$753


$C$DW$TU$754	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$754

$C$DW$T$754	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$754, DW_AT_type(*$C$DW$T$750)
$C$DW$1528	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1528, DW_AT_type(*$C$DW$T$750)

	.dwendtag $C$DW$T$754

	.dwendtag $C$DW$TU$754


$C$DW$TU$751	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$751
$C$DW$T$751	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$751, DW_AT_type(*$C$DW$T$755)

	.dwendtag $C$DW$TU$751


$C$DW$TU$752	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$752
$C$DW$T$752	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$752, DW_AT_type(*$C$DW$T$751)
	.dwattr $C$DW$T$752, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$752


$C$DW$TU$825	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$825
$C$DW$T$825	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$825, DW_AT_name("sTIDL_ConvParams_t")
	.dwattr $C$DW$T$825, DW_AT_type(*$C$DW$T$755)
	.dwattr $C$DW$T$825, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$825, DW_AT_decl_line(0x41d)
	.dwattr $C$DW$T$825, DW_AT_decl_column(0x02)

	.dwendtag $C$DW$TU$825


$C$DW$TU$763	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$763

$C$DW$T$763	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$763, DW_AT_name("sTIDL_CropParams_t")
	.dwattr $C$DW$T$763, DW_AT_byte_size(0x14)
$C$DW$1529	.dwtag  DW_TAG_member
	.dwattr $C$DW$1529, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1529, DW_AT_name("numChannels")
	.dwattr $C$DW$1529, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1529, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1529, DW_AT_decl_line(0x535)
	.dwattr $C$DW$1529, DW_AT_decl_column(0x0d)

$C$DW$1530	.dwtag  DW_TAG_member
	.dwattr $C$DW$1530, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1530, DW_AT_name("inDataQ")
	.dwattr $C$DW$1530, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$1530, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1530, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1530, DW_AT_decl_line(0x537)
	.dwattr $C$DW$1530, DW_AT_decl_column(0x0d)

$C$DW$1531	.dwtag  DW_TAG_member
	.dwattr $C$DW$1531, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1531, DW_AT_name("outDataQ")
	.dwattr $C$DW$1531, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$1531, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1531, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1531, DW_AT_decl_line(0x539)
	.dwattr $C$DW$1531, DW_AT_decl_column(0x0d)

$C$DW$1532	.dwtag  DW_TAG_member
	.dwattr $C$DW$1532, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1532, DW_AT_name("offsetW")
	.dwattr $C$DW$1532, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$1532, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1532, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1532, DW_AT_decl_line(0x53b)
	.dwattr $C$DW$1532, DW_AT_decl_column(0x0c)

$C$DW$1533	.dwtag  DW_TAG_member
	.dwattr $C$DW$1533, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1533, DW_AT_name("offsetH")
	.dwattr $C$DW$1533, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$1533, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1533, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1533, DW_AT_decl_line(0x53d)
	.dwattr $C$DW$1533, DW_AT_decl_column(0x0c)


$C$DW$1534	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1534, DW_AT_name("operator =")
	.dwattr $C$DW$1534, DW_AT_declaration
	.dwattr $C$DW$1534, DW_AT_linkage_name("_ZN18sTIDL_CropParams_taSERKS_")
	.dwattr $C$DW$1534, DW_AT_type(*$C$DW$T$758)
	.dwattr $C$DW$1534, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1535	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1535, DW_AT_type(*$C$DW$T$760)

	.dwendtag $C$DW$1534


$C$DW$1536	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1536, DW_AT_name("operator =")
	.dwattr $C$DW$1536, DW_AT_declaration
	.dwattr $C$DW$1536, DW_AT_linkage_name("_ZN18sTIDL_CropParams_taSEOS_")
	.dwattr $C$DW$1536, DW_AT_type(*$C$DW$T$758)
	.dwattr $C$DW$1536, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1537	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1537, DW_AT_type(*$C$DW$T$758)

	.dwendtag $C$DW$1536

	.dwattr $C$DW$T$763, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$763, DW_AT_decl_line(0x533)
	.dwattr $C$DW$T$763, DW_AT_decl_column(0x10)
	.dwendtag $C$DW$T$763

	.dwendtag $C$DW$TU$763


$C$DW$TU$758	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$758
$C$DW$T$758	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$758, DW_AT_type(*$C$DW$T$763)
	.dwattr $C$DW$T$758, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$758


$C$DW$TU$761	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$761

$C$DW$T$761	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$761, DW_AT_type(*$C$DW$T$758)
$C$DW$1538	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1538, DW_AT_type(*$C$DW$T$760)

	.dwendtag $C$DW$T$761

	.dwendtag $C$DW$TU$761


$C$DW$TU$762	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$762

$C$DW$T$762	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$762, DW_AT_type(*$C$DW$T$758)
$C$DW$1539	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1539, DW_AT_type(*$C$DW$T$758)

	.dwendtag $C$DW$T$762

	.dwendtag $C$DW$TU$762


$C$DW$TU$759	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$759
$C$DW$T$759	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$759, DW_AT_type(*$C$DW$T$763)

	.dwendtag $C$DW$TU$759


$C$DW$TU$760	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$760
$C$DW$T$760	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$760, DW_AT_type(*$C$DW$T$759)
	.dwattr $C$DW$T$760, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$760


$C$DW$TU$831	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$831
$C$DW$T$831	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$831, DW_AT_name("sTIDL_CropParams_t")
	.dwattr $C$DW$T$831, DW_AT_type(*$C$DW$T$763)
	.dwattr $C$DW$T$831, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$831, DW_AT_decl_line(0x53e)
	.dwattr $C$DW$T$831, DW_AT_decl_column(0x02)

	.dwendtag $C$DW$TU$831


$C$DW$TU$770	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$770

$C$DW$T$770	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$770, DW_AT_name("sTIDL_CustomParams_t")
	.dwattr $C$DW$T$770, DW_AT_byte_size(0x1c)
$C$DW$1540	.dwtag  DW_TAG_member
	.dwattr $C$DW$1540, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1540, DW_AT_name("customLayerType")
	.dwattr $C$DW$1540, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1540, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1540, DW_AT_decl_line(0x3a4)
	.dwattr $C$DW$1540, DW_AT_decl_column(0x0d)

$C$DW$1541	.dwtag  DW_TAG_member
	.dwattr $C$DW$1541, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1541, DW_AT_name("padW")
	.dwattr $C$DW$1541, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$1541, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1541, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1541, DW_AT_decl_line(0x3a7)
	.dwattr $C$DW$1541, DW_AT_decl_column(0x0b)

$C$DW$1542	.dwtag  DW_TAG_member
	.dwattr $C$DW$1542, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1542, DW_AT_name("padH")
	.dwattr $C$DW$1542, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$1542, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1542, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1542, DW_AT_decl_line(0x3aa)
	.dwattr $C$DW$1542, DW_AT_decl_column(0x0b)

$C$DW$1543	.dwtag  DW_TAG_member
	.dwattr $C$DW$1543, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1543, DW_AT_name("memOverlapType")
	.dwattr $C$DW$1543, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$1543, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1543, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1543, DW_AT_decl_line(0x3b0)
	.dwattr $C$DW$1543, DW_AT_decl_column(0x0b)

$C$DW$1544	.dwtag  DW_TAG_member
	.dwattr $C$DW$1544, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1544, DW_AT_name("doesLayerChangePadding")
	.dwattr $C$DW$1544, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$1544, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1544, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1544, DW_AT_decl_line(0x3b6)
	.dwattr $C$DW$1544, DW_AT_decl_column(0x0b)

$C$DW$1545	.dwtag  DW_TAG_member
	.dwattr $C$DW$1545, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1545, DW_AT_name("doesLayerFillOutXPadding")
	.dwattr $C$DW$1545, DW_AT_data_member_location[DW_OP_plus_uconst 0x14]
	.dwattr $C$DW$1545, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1545, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1545, DW_AT_decl_line(0x3d0)
	.dwattr $C$DW$1545, DW_AT_decl_column(0x0b)

$C$DW$1546	.dwtag  DW_TAG_member
	.dwattr $C$DW$1546, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1546, DW_AT_name("rsvdPassThrough")
	.dwattr $C$DW$1546, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$1546, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1546, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1546, DW_AT_decl_line(0x3d2)
	.dwattr $C$DW$1546, DW_AT_decl_column(0x0b)


$C$DW$1547	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1547, DW_AT_name("operator =")
	.dwattr $C$DW$1547, DW_AT_declaration
	.dwattr $C$DW$1547, DW_AT_linkage_name("_ZN20sTIDL_CustomParams_taSERKS_")
	.dwattr $C$DW$1547, DW_AT_type(*$C$DW$T$765)
	.dwattr $C$DW$1547, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1548	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1548, DW_AT_type(*$C$DW$T$767)

	.dwendtag $C$DW$1547


$C$DW$1549	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1549, DW_AT_name("operator =")
	.dwattr $C$DW$1549, DW_AT_declaration
	.dwattr $C$DW$1549, DW_AT_linkage_name("_ZN20sTIDL_CustomParams_taSEOS_")
	.dwattr $C$DW$1549, DW_AT_type(*$C$DW$T$765)
	.dwattr $C$DW$1549, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1550	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1550, DW_AT_type(*$C$DW$T$765)

	.dwendtag $C$DW$1549

	.dwattr $C$DW$T$770, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$770, DW_AT_decl_line(0x3a2)
	.dwattr $C$DW$T$770, DW_AT_decl_column(0x10)
	.dwendtag $C$DW$T$770

	.dwendtag $C$DW$TU$770


$C$DW$TU$765	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$765
$C$DW$T$765	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$765, DW_AT_type(*$C$DW$T$770)
	.dwattr $C$DW$T$765, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$765


$C$DW$TU$768	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$768

$C$DW$T$768	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$768, DW_AT_type(*$C$DW$T$765)
$C$DW$1551	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1551, DW_AT_type(*$C$DW$T$767)

	.dwendtag $C$DW$T$768

	.dwendtag $C$DW$TU$768


$C$DW$TU$769	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$769

$C$DW$T$769	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$769, DW_AT_type(*$C$DW$T$765)
$C$DW$1552	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1552, DW_AT_type(*$C$DW$T$765)

	.dwendtag $C$DW$T$769

	.dwendtag $C$DW$TU$769


$C$DW$TU$766	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$766
$C$DW$T$766	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$766, DW_AT_type(*$C$DW$T$770)

	.dwendtag $C$DW$TU$766


$C$DW$TU$767	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$767
$C$DW$T$767	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$767, DW_AT_type(*$C$DW$T$766)
	.dwattr $C$DW$T$767, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$767


$C$DW$TU$844	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$844
$C$DW$T$844	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$844, DW_AT_name("sTIDL_CustomParams_t")
	.dwattr $C$DW$T$844, DW_AT_type(*$C$DW$T$770)
	.dwattr $C$DW$T$844, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$844, DW_AT_decl_line(0x3d3)
	.dwattr $C$DW$T$844, DW_AT_decl_column(0x02)

	.dwendtag $C$DW$TU$844


$C$DW$TU$777	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$777

$C$DW$T$777	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$777, DW_AT_name("sTIDL_DataLayerParams_t")
	.dwattr $C$DW$T$777, DW_AT_byte_size(0x08)
$C$DW$1553	.dwtag  DW_TAG_member
	.dwattr $C$DW$1553, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1553, DW_AT_name("numChannels")
	.dwattr $C$DW$1553, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1553, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1553, DW_AT_decl_line(0x296)
	.dwattr $C$DW$1553, DW_AT_decl_column(0x0d)

$C$DW$1554	.dwtag  DW_TAG_member
	.dwattr $C$DW$1554, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1554, DW_AT_name("dataQ")
	.dwattr $C$DW$1554, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$1554, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1554, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1554, DW_AT_decl_line(0x298)
	.dwattr $C$DW$1554, DW_AT_decl_column(0x0d)


$C$DW$1555	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1555, DW_AT_name("operator =")
	.dwattr $C$DW$1555, DW_AT_declaration
	.dwattr $C$DW$1555, DW_AT_linkage_name("_ZN23sTIDL_DataLayerParams_taSERKS_")
	.dwattr $C$DW$1555, DW_AT_type(*$C$DW$T$772)
	.dwattr $C$DW$1555, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1556	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1556, DW_AT_type(*$C$DW$T$774)

	.dwendtag $C$DW$1555


$C$DW$1557	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1557, DW_AT_name("operator =")
	.dwattr $C$DW$1557, DW_AT_declaration
	.dwattr $C$DW$1557, DW_AT_linkage_name("_ZN23sTIDL_DataLayerParams_taSEOS_")
	.dwattr $C$DW$1557, DW_AT_type(*$C$DW$T$772)
	.dwattr $C$DW$1557, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1558	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1558, DW_AT_type(*$C$DW$T$772)

	.dwendtag $C$DW$1557

	.dwattr $C$DW$T$777, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$777, DW_AT_decl_line(0x294)
	.dwattr $C$DW$T$777, DW_AT_decl_column(0x10)
	.dwendtag $C$DW$T$777

	.dwendtag $C$DW$TU$777


$C$DW$TU$772	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$772
$C$DW$T$772	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$772, DW_AT_type(*$C$DW$T$777)
	.dwattr $C$DW$T$772, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$772


$C$DW$TU$775	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$775

$C$DW$T$775	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$775, DW_AT_type(*$C$DW$T$772)
$C$DW$1559	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1559, DW_AT_type(*$C$DW$T$774)

	.dwendtag $C$DW$T$775

	.dwendtag $C$DW$TU$775


$C$DW$TU$776	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$776

$C$DW$T$776	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$776, DW_AT_type(*$C$DW$T$772)
$C$DW$1560	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1560, DW_AT_type(*$C$DW$T$772)

	.dwendtag $C$DW$T$776

	.dwendtag $C$DW$TU$776


$C$DW$TU$773	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$773
$C$DW$T$773	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$773, DW_AT_type(*$C$DW$T$777)

	.dwendtag $C$DW$TU$773


$C$DW$TU$774	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$774
$C$DW$T$774	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$774, DW_AT_type(*$C$DW$T$773)
	.dwattr $C$DW$T$774, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$774


$C$DW$TU$828	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$828
$C$DW$T$828	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$828, DW_AT_name("sTIDL_DataLayerParams_t")
	.dwattr $C$DW$T$828, DW_AT_type(*$C$DW$T$777)
	.dwattr $C$DW$T$828, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$828, DW_AT_decl_line(0x299)
	.dwattr $C$DW$T$828, DW_AT_decl_column(0x02)

	.dwendtag $C$DW$TU$828


$C$DW$TU$785	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$785

$C$DW$T$785	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$785, DW_AT_name("sTIDL_DataParams_t")
	.dwattr $C$DW$T$785, DW_AT_byte_size(0x5c)
$C$DW$1561	.dwtag  DW_TAG_member
	.dwattr $C$DW$1561, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1561, DW_AT_name("dataId")
	.dwattr $C$DW$1561, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1561, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1561, DW_AT_decl_line(0x268)
	.dwattr $C$DW$1561, DW_AT_decl_column(0x0b)

$C$DW$1562	.dwtag  DW_TAG_member
	.dwattr $C$DW$1562, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1562, DW_AT_name("elementType")
	.dwattr $C$DW$1562, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$1562, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1562, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1562, DW_AT_decl_line(0x26a)
	.dwattr $C$DW$1562, DW_AT_decl_column(0x0b)

$C$DW$1563	.dwtag  DW_TAG_member
	.dwattr $C$DW$1563, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1563, DW_AT_name("numDim")
	.dwattr $C$DW$1563, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$1563, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1563, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1563, DW_AT_decl_line(0x26c)
	.dwattr $C$DW$1563, DW_AT_decl_column(0x0b)

$C$DW$1564	.dwtag  DW_TAG_member
	.dwattr $C$DW$1564, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1564, DW_AT_name("dataQ")
	.dwattr $C$DW$1564, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$1564, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1564, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1564, DW_AT_decl_line(0x26e)
	.dwattr $C$DW$1564, DW_AT_decl_column(0x0b)

$C$DW$1565	.dwtag  DW_TAG_member
	.dwattr $C$DW$1565, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1565, DW_AT_name("minValue")
	.dwattr $C$DW$1565, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$1565, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1565, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1565, DW_AT_decl_line(0x270)
	.dwattr $C$DW$1565, DW_AT_decl_column(0x0b)

$C$DW$1566	.dwtag  DW_TAG_member
	.dwattr $C$DW$1566, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1566, DW_AT_name("maxValue")
	.dwattr $C$DW$1566, DW_AT_data_member_location[DW_OP_plus_uconst 0x14]
	.dwattr $C$DW$1566, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1566, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1566, DW_AT_decl_line(0x272)
	.dwattr $C$DW$1566, DW_AT_decl_column(0x0b)

$C$DW$1567	.dwtag  DW_TAG_member
	.dwattr $C$DW$1567, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$1567, DW_AT_name("minTensorValue")
	.dwattr $C$DW$1567, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$1567, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1567, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1567, DW_AT_decl_line(0x274)
	.dwattr $C$DW$1567, DW_AT_decl_column(0x12)

$C$DW$1568	.dwtag  DW_TAG_member
	.dwattr $C$DW$1568, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$1568, DW_AT_name("maxTensorValue")
	.dwattr $C$DW$1568, DW_AT_data_member_location[DW_OP_plus_uconst 0x1c]
	.dwattr $C$DW$1568, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1568, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1568, DW_AT_decl_line(0x276)
	.dwattr $C$DW$1568, DW_AT_decl_column(0x12)

$C$DW$1569	.dwtag  DW_TAG_member
	.dwattr $C$DW$1569, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$1569, DW_AT_name("tensorScale")
	.dwattr $C$DW$1569, DW_AT_data_member_location[DW_OP_plus_uconst 0x20]
	.dwattr $C$DW$1569, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1569, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1569, DW_AT_decl_line(0x278)
	.dwattr $C$DW$1569, DW_AT_decl_column(0x12)

$C$DW$1570	.dwtag  DW_TAG_member
	.dwattr $C$DW$1570, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1570, DW_AT_name("padW")
	.dwattr $C$DW$1570, DW_AT_data_member_location[DW_OP_plus_uconst 0x24]
	.dwattr $C$DW$1570, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1570, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1570, DW_AT_decl_line(0x27a)
	.dwattr $C$DW$1570, DW_AT_decl_column(0x0b)

$C$DW$1571	.dwtag  DW_TAG_member
	.dwattr $C$DW$1571, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1571, DW_AT_name("padH")
	.dwattr $C$DW$1571, DW_AT_data_member_location[DW_OP_plus_uconst 0x28]
	.dwattr $C$DW$1571, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1571, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1571, DW_AT_decl_line(0x27c)
	.dwattr $C$DW$1571, DW_AT_decl_column(0x0b)

$C$DW$1572	.dwtag  DW_TAG_member
	.dwattr $C$DW$1572, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1572, DW_AT_name("batchPadW")
	.dwattr $C$DW$1572, DW_AT_data_member_location[DW_OP_plus_uconst 0x2c]
	.dwattr $C$DW$1572, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1572, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1572, DW_AT_decl_line(0x27e)
	.dwattr $C$DW$1572, DW_AT_decl_column(0x0b)

$C$DW$1573	.dwtag  DW_TAG_member
	.dwattr $C$DW$1573, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1573, DW_AT_name("batchPadH")
	.dwattr $C$DW$1573, DW_AT_data_member_location[DW_OP_plus_uconst 0x30]
	.dwattr $C$DW$1573, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1573, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1573, DW_AT_decl_line(0x280)
	.dwattr $C$DW$1573, DW_AT_decl_column(0x0b)

$C$DW$1574	.dwtag  DW_TAG_member
	.dwattr $C$DW$1574, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1574, DW_AT_name("numBatchW")
	.dwattr $C$DW$1574, DW_AT_data_member_location[DW_OP_plus_uconst 0x34]
	.dwattr $C$DW$1574, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1574, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1574, DW_AT_decl_line(0x282)
	.dwattr $C$DW$1574, DW_AT_decl_column(0x0b)

$C$DW$1575	.dwtag  DW_TAG_member
	.dwattr $C$DW$1575, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1575, DW_AT_name("numBatchH")
	.dwattr $C$DW$1575, DW_AT_data_member_location[DW_OP_plus_uconst 0x38]
	.dwattr $C$DW$1575, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1575, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1575, DW_AT_decl_line(0x284)
	.dwattr $C$DW$1575, DW_AT_decl_column(0x0b)

$C$DW$1576	.dwtag  DW_TAG_member
	.dwattr $C$DW$1576, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1576, DW_AT_name("roundBits")
	.dwattr $C$DW$1576, DW_AT_data_member_location[DW_OP_plus_uconst 0x3c]
	.dwattr $C$DW$1576, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1576, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1576, DW_AT_decl_line(0x286)
	.dwattr $C$DW$1576, DW_AT_decl_column(0x0b)

$C$DW$1577	.dwtag  DW_TAG_member
	.dwattr $C$DW$1577, DW_AT_type(*$C$DW$T$500)
	.dwattr $C$DW$1577, DW_AT_name("pitch")
	.dwattr $C$DW$1577, DW_AT_data_member_location[DW_OP_plus_uconst 0x40]
	.dwattr $C$DW$1577, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1577, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1577, DW_AT_decl_line(0x288)
	.dwattr $C$DW$1577, DW_AT_decl_column(0x0b)

$C$DW$1578	.dwtag  DW_TAG_member
	.dwattr $C$DW$1578, DW_AT_type(*$C$DW$T$779)
	.dwattr $C$DW$1578, DW_AT_name("dimValues")
	.dwattr $C$DW$1578, DW_AT_data_member_location[DW_OP_plus_uconst 0x4c]
	.dwattr $C$DW$1578, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1578, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1578, DW_AT_decl_line(0x28a)
	.dwattr $C$DW$1578, DW_AT_decl_column(0x0b)


$C$DW$1579	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1579, DW_AT_name("operator =")
	.dwattr $C$DW$1579, DW_AT_declaration
	.dwattr $C$DW$1579, DW_AT_linkage_name("_ZN18sTIDL_DataParams_taSERKS_")
	.dwattr $C$DW$1579, DW_AT_type(*$C$DW$T$780)
	.dwattr $C$DW$1579, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1580	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1580, DW_AT_type(*$C$DW$T$782)

	.dwendtag $C$DW$1579


$C$DW$1581	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1581, DW_AT_name("operator =")
	.dwattr $C$DW$1581, DW_AT_declaration
	.dwattr $C$DW$1581, DW_AT_linkage_name("_ZN18sTIDL_DataParams_taSEOS_")
	.dwattr $C$DW$1581, DW_AT_type(*$C$DW$T$780)
	.dwattr $C$DW$1581, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1582	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1582, DW_AT_type(*$C$DW$T$780)

	.dwendtag $C$DW$1581

	.dwattr $C$DW$T$785, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$785, DW_AT_decl_line(0x266)
	.dwattr $C$DW$T$785, DW_AT_decl_column(0x10)
	.dwendtag $C$DW$T$785

	.dwendtag $C$DW$TU$785


$C$DW$TU$780	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$780
$C$DW$T$780	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$780, DW_AT_type(*$C$DW$T$785)
	.dwattr $C$DW$T$780, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$780


$C$DW$TU$783	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$783

$C$DW$T$783	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$783, DW_AT_type(*$C$DW$T$780)
$C$DW$1583	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1583, DW_AT_type(*$C$DW$T$782)

	.dwendtag $C$DW$T$783

	.dwendtag $C$DW$TU$783


$C$DW$TU$784	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$784

$C$DW$T$784	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$784, DW_AT_type(*$C$DW$T$780)
$C$DW$1584	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1584, DW_AT_type(*$C$DW$T$780)

	.dwendtag $C$DW$T$784

	.dwendtag $C$DW$TU$784


$C$DW$TU$781	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$781
$C$DW$T$781	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$781, DW_AT_type(*$C$DW$T$785)

	.dwendtag $C$DW$TU$781


$C$DW$TU$782	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$782
$C$DW$T$782	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$782, DW_AT_type(*$C$DW$T$781)
	.dwattr $C$DW$T$782, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$782


$C$DW$TU$874	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$874
$C$DW$T$874	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$874, DW_AT_name("sTIDL_DataParams_t")
	.dwattr $C$DW$T$874, DW_AT_type(*$C$DW$T$785)
	.dwattr $C$DW$T$874, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$874, DW_AT_decl_line(0x28b)
	.dwattr $C$DW$T$874, DW_AT_decl_column(0x02)

	.dwendtag $C$DW$TU$874


$C$DW$TU$875	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$875

$C$DW$T$875	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$875, DW_AT_type(*$C$DW$T$874)
	.dwattr $C$DW$T$875, DW_AT_byte_size(0x5c0)
$C$DW$1585	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$1585, DW_AT_upper_bound(0x0f)

	.dwendtag $C$DW$T$875

	.dwendtag $C$DW$TU$875


$C$DW$TU$1076	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$1076
$C$DW$T$1076	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$1076, DW_AT_type(*$C$DW$T$874)
	.dwattr $C$DW$T$1076, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$1076


$C$DW$TU$793	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$793

$C$DW$T$793	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$793, DW_AT_name("sTIDL_DepthToSpaceParams_t")
	.dwattr $C$DW$T$793, DW_AT_byte_size(0x04)
$C$DW$1586	.dwtag  DW_TAG_member
	.dwattr $C$DW$1586, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1586, DW_AT_name("blockSize")
	.dwattr $C$DW$1586, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1586, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1586, DW_AT_decl_line(0x2af)
	.dwattr $C$DW$1586, DW_AT_decl_column(0x0d)


$C$DW$1587	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1587, DW_AT_name("operator =")
	.dwattr $C$DW$1587, DW_AT_declaration
	.dwattr $C$DW$1587, DW_AT_linkage_name("_ZN26sTIDL_DepthToSpaceParams_taSERKS_")
	.dwattr $C$DW$1587, DW_AT_type(*$C$DW$T$788)
	.dwattr $C$DW$1587, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1588	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1588, DW_AT_type(*$C$DW$T$790)

	.dwendtag $C$DW$1587


$C$DW$1589	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1589, DW_AT_name("operator =")
	.dwattr $C$DW$1589, DW_AT_declaration
	.dwattr $C$DW$1589, DW_AT_linkage_name("_ZN26sTIDL_DepthToSpaceParams_taSEOS_")
	.dwattr $C$DW$1589, DW_AT_type(*$C$DW$T$788)
	.dwattr $C$DW$1589, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1590	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1590, DW_AT_type(*$C$DW$T$788)

	.dwendtag $C$DW$1589

	.dwattr $C$DW$T$793, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$793, DW_AT_decl_line(0x2ad)
	.dwattr $C$DW$T$793, DW_AT_decl_column(0x10)
	.dwendtag $C$DW$T$793

	.dwendtag $C$DW$TU$793


$C$DW$TU$788	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$788
$C$DW$T$788	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$788, DW_AT_type(*$C$DW$T$793)
	.dwattr $C$DW$T$788, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$788


$C$DW$TU$791	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$791

$C$DW$T$791	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$791, DW_AT_type(*$C$DW$T$788)
$C$DW$1591	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1591, DW_AT_type(*$C$DW$T$790)

	.dwendtag $C$DW$T$791

	.dwendtag $C$DW$TU$791


$C$DW$TU$792	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$792

$C$DW$T$792	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$792, DW_AT_type(*$C$DW$T$788)
$C$DW$1592	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1592, DW_AT_type(*$C$DW$T$788)

	.dwendtag $C$DW$T$792

	.dwendtag $C$DW$TU$792


$C$DW$TU$789	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$789
$C$DW$T$789	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$789, DW_AT_type(*$C$DW$T$793)

	.dwendtag $C$DW$TU$789


$C$DW$TU$790	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$790
$C$DW$T$790	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$790, DW_AT_type(*$C$DW$T$789)
	.dwattr $C$DW$T$790, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$790


$C$DW$TU$840	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$840
$C$DW$T$840	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$840, DW_AT_name("sTIDL_DepthToSpaceParams_t")
	.dwattr $C$DW$T$840, DW_AT_type(*$C$DW$T$793)
	.dwattr $C$DW$T$840, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$840, DW_AT_decl_line(0x2b0)
	.dwattr $C$DW$T$840, DW_AT_decl_column(0x02)

	.dwendtag $C$DW$TU$840


$C$DW$TU$800	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$800

$C$DW$T$800	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$800, DW_AT_name("sTIDL_DetectOutputParams_t")
	.dwattr $C$DW$T$800, DW_AT_byte_size(0x50)
$C$DW$1593	.dwtag  DW_TAG_member
	.dwattr $C$DW$1593, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1593, DW_AT_name("processingType")
	.dwattr $C$DW$1593, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1593, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1593, DW_AT_decl_line(0x44d)
	.dwattr $C$DW$1593, DW_AT_decl_column(0x0d)

$C$DW$1594	.dwtag  DW_TAG_member
	.dwattr $C$DW$1594, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1594, DW_AT_name("priorBox")
	.dwattr $C$DW$1594, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$1594, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1594, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1594, DW_AT_decl_line(0x44f)
	.dwattr $C$DW$1594, DW_AT_decl_column(0x0c)

$C$DW$1595	.dwtag  DW_TAG_member
	.dwattr $C$DW$1595, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1595, DW_AT_name("priorBoxSize")
	.dwattr $C$DW$1595, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$1595, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1595, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1595, DW_AT_decl_line(0x451)
	.dwattr $C$DW$1595, DW_AT_decl_column(0x0c)

$C$DW$1596	.dwtag  DW_TAG_member
	.dwattr $C$DW$1596, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1596, DW_AT_name("numClasses")
	.dwattr $C$DW$1596, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$1596, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1596, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1596, DW_AT_decl_line(0x453)
	.dwattr $C$DW$1596, DW_AT_decl_column(0x0c)

$C$DW$1597	.dwtag  DW_TAG_member
	.dwattr $C$DW$1597, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1597, DW_AT_name("backgroundLabelId")
	.dwattr $C$DW$1597, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$1597, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1597, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1597, DW_AT_decl_line(0x455)
	.dwattr $C$DW$1597, DW_AT_decl_column(0x0c)

$C$DW$1598	.dwtag  DW_TAG_member
	.dwattr $C$DW$1598, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1598, DW_AT_name("codeType")
	.dwattr $C$DW$1598, DW_AT_data_member_location[DW_OP_plus_uconst 0x14]
	.dwattr $C$DW$1598, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1598, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1598, DW_AT_decl_line(0x457)
	.dwattr $C$DW$1598, DW_AT_decl_column(0x0c)

$C$DW$1599	.dwtag  DW_TAG_member
	.dwattr $C$DW$1599, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$1599, DW_AT_name("confThreshold")
	.dwattr $C$DW$1599, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$1599, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1599, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1599, DW_AT_decl_line(0x45a)
	.dwattr $C$DW$1599, DW_AT_decl_column(0x11)

$C$DW$1600	.dwtag  DW_TAG_member
	.dwattr $C$DW$1600, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$1600, DW_AT_name("nmsThreshold")
	.dwattr $C$DW$1600, DW_AT_data_member_location[DW_OP_plus_uconst 0x1c]
	.dwattr $C$DW$1600, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1600, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1600, DW_AT_decl_line(0x45d)
	.dwattr $C$DW$1600, DW_AT_decl_column(0x11)

$C$DW$1601	.dwtag  DW_TAG_member
	.dwattr $C$DW$1601, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$1601, DW_AT_name("eta")
	.dwattr $C$DW$1601, DW_AT_data_member_location[DW_OP_plus_uconst 0x20]
	.dwattr $C$DW$1601, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1601, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1601, DW_AT_decl_line(0x45f)
	.dwattr $C$DW$1601, DW_AT_decl_column(0x11)

$C$DW$1602	.dwtag  DW_TAG_member
	.dwattr $C$DW$1602, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1602, DW_AT_name("topK")
	.dwattr $C$DW$1602, DW_AT_data_member_location[DW_OP_plus_uconst 0x24]
	.dwattr $C$DW$1602, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1602, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1602, DW_AT_decl_line(0x461)
	.dwattr $C$DW$1602, DW_AT_decl_column(0x0c)

$C$DW$1603	.dwtag  DW_TAG_member
	.dwattr $C$DW$1603, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1603, DW_AT_name("keepTopK")
	.dwattr $C$DW$1603, DW_AT_data_member_location[DW_OP_plus_uconst 0x28]
	.dwattr $C$DW$1603, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1603, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1603, DW_AT_decl_line(0x463)
	.dwattr $C$DW$1603, DW_AT_decl_column(0x0c)

$C$DW$1604	.dwtag  DW_TAG_member
	.dwattr $C$DW$1604, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1604, DW_AT_name("shareLocation")
	.dwattr $C$DW$1604, DW_AT_data_member_location[DW_OP_plus_uconst 0x2c]
	.dwattr $C$DW$1604, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1604, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1604, DW_AT_decl_line(0x466)
	.dwattr $C$DW$1604, DW_AT_decl_column(0x0c)

$C$DW$1605	.dwtag  DW_TAG_member
	.dwattr $C$DW$1605, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1605, DW_AT_name("varianceEncoded")
	.dwattr $C$DW$1605, DW_AT_data_member_location[DW_OP_plus_uconst 0x30]
	.dwattr $C$DW$1605, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1605, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1605, DW_AT_decl_line(0x469)
	.dwattr $C$DW$1605, DW_AT_decl_column(0x0c)

$C$DW$1606	.dwtag  DW_TAG_member
	.dwattr $C$DW$1606, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1606, DW_AT_name("numKeypoints")
	.dwattr $C$DW$1606, DW_AT_data_member_location[DW_OP_plus_uconst 0x34]
	.dwattr $C$DW$1606, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1606, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1606, DW_AT_decl_line(0x46b)
	.dwattr $C$DW$1606, DW_AT_decl_column(0x0c)

$C$DW$1607	.dwtag  DW_TAG_member
	.dwattr $C$DW$1607, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1607, DW_AT_name("numHeads")
	.dwattr $C$DW$1607, DW_AT_data_member_location[DW_OP_plus_uconst 0x38]
	.dwattr $C$DW$1607, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1607, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1607, DW_AT_decl_line(0x46e)
	.dwattr $C$DW$1607, DW_AT_decl_column(0x0c)

$C$DW$1608	.dwtag  DW_TAG_member
	.dwattr $C$DW$1608, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1608, DW_AT_name("imWidth")
	.dwattr $C$DW$1608, DW_AT_data_member_location[DW_OP_plus_uconst 0x3c]
	.dwattr $C$DW$1608, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1608, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1608, DW_AT_decl_line(0x471)
	.dwattr $C$DW$1608, DW_AT_decl_column(0x0b)

$C$DW$1609	.dwtag  DW_TAG_member
	.dwattr $C$DW$1609, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1609, DW_AT_name("imHeight")
	.dwattr $C$DW$1609, DW_AT_data_member_location[DW_OP_plus_uconst 0x40]
	.dwattr $C$DW$1609, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1609, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1609, DW_AT_decl_line(0x474)
	.dwattr $C$DW$1609, DW_AT_decl_column(0x0b)

$C$DW$1610	.dwtag  DW_TAG_member
	.dwattr $C$DW$1610, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1610, DW_AT_name("scoreConverter")
	.dwattr $C$DW$1610, DW_AT_data_member_location[DW_OP_plus_uconst 0x44]
	.dwattr $C$DW$1610, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1610, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1610, DW_AT_decl_line(0x477)
	.dwattr $C$DW$1610, DW_AT_decl_column(0x0b)

$C$DW$1611	.dwtag  DW_TAG_member
	.dwattr $C$DW$1611, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1611, DW_AT_name("metaArchType")
	.dwattr $C$DW$1611, DW_AT_data_member_location[DW_OP_plus_uconst 0x48]
	.dwattr $C$DW$1611, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1611, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1611, DW_AT_decl_line(0x47a)
	.dwattr $C$DW$1611, DW_AT_decl_column(0x0b)

$C$DW$1612	.dwtag  DW_TAG_member
	.dwattr $C$DW$1612, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1612, DW_AT_name("dataLayout")
	.dwattr $C$DW$1612, DW_AT_data_member_location[DW_OP_plus_uconst 0x4c]
	.dwattr $C$DW$1612, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1612, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1612, DW_AT_decl_line(0x47e)
	.dwattr $C$DW$1612, DW_AT_decl_column(0x0b)


$C$DW$1613	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1613, DW_AT_name("operator =")
	.dwattr $C$DW$1613, DW_AT_declaration
	.dwattr $C$DW$1613, DW_AT_linkage_name("_ZN26sTIDL_DetectOutputParams_taSERKS_")
	.dwattr $C$DW$1613, DW_AT_type(*$C$DW$T$795)
	.dwattr $C$DW$1613, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1614	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1614, DW_AT_type(*$C$DW$T$797)

	.dwendtag $C$DW$1613


$C$DW$1615	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1615, DW_AT_name("operator =")
	.dwattr $C$DW$1615, DW_AT_declaration
	.dwattr $C$DW$1615, DW_AT_linkage_name("_ZN26sTIDL_DetectOutputParams_taSEOS_")
	.dwattr $C$DW$1615, DW_AT_type(*$C$DW$T$795)
	.dwattr $C$DW$1615, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1616	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1616, DW_AT_type(*$C$DW$T$795)

	.dwendtag $C$DW$1615

	.dwattr $C$DW$T$800, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$800, DW_AT_decl_line(0x44b)
	.dwattr $C$DW$T$800, DW_AT_decl_column(0x10)
	.dwendtag $C$DW$T$800

	.dwendtag $C$DW$TU$800


$C$DW$TU$795	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$795
$C$DW$T$795	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$795, DW_AT_type(*$C$DW$T$800)
	.dwattr $C$DW$T$795, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$795


$C$DW$TU$798	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$798

$C$DW$T$798	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$798, DW_AT_type(*$C$DW$T$795)
$C$DW$1617	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1617, DW_AT_type(*$C$DW$T$797)

	.dwendtag $C$DW$T$798

	.dwendtag $C$DW$TU$798


$C$DW$TU$799	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$799

$C$DW$T$799	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$799, DW_AT_type(*$C$DW$T$795)
$C$DW$1618	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1618, DW_AT_type(*$C$DW$T$795)

	.dwendtag $C$DW$T$799

	.dwendtag $C$DW$TU$799


$C$DW$TU$796	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$796
$C$DW$T$796	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$796, DW_AT_type(*$C$DW$T$800)

	.dwendtag $C$DW$TU$796


$C$DW$TU$797	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$797
$C$DW$T$797	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$797, DW_AT_type(*$C$DW$T$796)
	.dwattr $C$DW$T$797, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$797


$C$DW$TU$833	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$833
$C$DW$T$833	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$833, DW_AT_name("sTIDL_DetectOutputParams_t")
	.dwattr $C$DW$T$833, DW_AT_type(*$C$DW$T$800)
	.dwattr $C$DW$T$833, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$833, DW_AT_decl_line(0x480)
	.dwattr $C$DW$T$833, DW_AT_decl_column(0x02)

	.dwendtag $C$DW$TU$833


$C$DW$TU$807	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$807

$C$DW$T$807	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$807, DW_AT_name("sTIDL_EltWiseParams_t")
	.dwattr $C$DW$T$807, DW_AT_byte_size(0x58)
$C$DW$1619	.dwtag  DW_TAG_member
	.dwattr $C$DW$1619, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1619, DW_AT_name("bias")
	.dwattr $C$DW$1619, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1619, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1619, DW_AT_decl_line(0x511)
	.dwattr $C$DW$1619, DW_AT_decl_column(0x0b)

$C$DW$1620	.dwtag  DW_TAG_member
	.dwattr $C$DW$1620, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1620, DW_AT_name("numChannels")
	.dwattr $C$DW$1620, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$1620, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1620, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1620, DW_AT_decl_line(0x513)
	.dwattr $C$DW$1620, DW_AT_decl_column(0x0b)

$C$DW$1621	.dwtag  DW_TAG_member
	.dwattr $C$DW$1621, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1621, DW_AT_name("eltWiseType")
	.dwattr $C$DW$1621, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$1621, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1621, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1621, DW_AT_decl_line(0x515)
	.dwattr $C$DW$1621, DW_AT_decl_column(0x0b)

$C$DW$1622	.dwtag  DW_TAG_member
	.dwattr $C$DW$1622, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1622, DW_AT_name("numInData")
	.dwattr $C$DW$1622, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$1622, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1622, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1622, DW_AT_decl_line(0x517)
	.dwattr $C$DW$1622, DW_AT_decl_column(0x0b)

$C$DW$1623	.dwtag  DW_TAG_member
	.dwattr $C$DW$1623, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1623, DW_AT_name("biasQ")
	.dwattr $C$DW$1623, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$1623, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1623, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1623, DW_AT_decl_line(0x519)
	.dwattr $C$DW$1623, DW_AT_decl_column(0x0b)

$C$DW$1624	.dwtag  DW_TAG_member
	.dwattr $C$DW$1624, DW_AT_type(*$C$DW$T$599)
	.dwattr $C$DW$1624, DW_AT_name("inDataQ")
	.dwattr $C$DW$1624, DW_AT_data_member_location[DW_OP_plus_uconst 0x14]
	.dwattr $C$DW$1624, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1624, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1624, DW_AT_decl_line(0x51b)
	.dwattr $C$DW$1624, DW_AT_decl_column(0x0b)

$C$DW$1625	.dwtag  DW_TAG_member
	.dwattr $C$DW$1625, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1625, DW_AT_name("outDataQ")
	.dwattr $C$DW$1625, DW_AT_data_member_location[DW_OP_plus_uconst 0x54]
	.dwattr $C$DW$1625, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1625, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1625, DW_AT_decl_line(0x51d)
	.dwattr $C$DW$1625, DW_AT_decl_column(0x0b)


$C$DW$1626	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1626, DW_AT_name("operator =")
	.dwattr $C$DW$1626, DW_AT_declaration
	.dwattr $C$DW$1626, DW_AT_linkage_name("_ZN21sTIDL_EltWiseParams_taSERKS_")
	.dwattr $C$DW$1626, DW_AT_type(*$C$DW$T$802)
	.dwattr $C$DW$1626, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1627	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1627, DW_AT_type(*$C$DW$T$804)

	.dwendtag $C$DW$1626


$C$DW$1628	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1628, DW_AT_name("operator =")
	.dwattr $C$DW$1628, DW_AT_declaration
	.dwattr $C$DW$1628, DW_AT_linkage_name("_ZN21sTIDL_EltWiseParams_taSEOS_")
	.dwattr $C$DW$1628, DW_AT_type(*$C$DW$T$802)
	.dwattr $C$DW$1628, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1629	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1629, DW_AT_type(*$C$DW$T$802)

	.dwendtag $C$DW$1628

	.dwattr $C$DW$T$807, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$807, DW_AT_decl_line(0x50f)
	.dwattr $C$DW$T$807, DW_AT_decl_column(0x10)
	.dwendtag $C$DW$T$807

	.dwendtag $C$DW$TU$807


$C$DW$TU$802	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$802
$C$DW$T$802	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$802, DW_AT_type(*$C$DW$T$807)
	.dwattr $C$DW$T$802, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$802


$C$DW$TU$805	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$805

$C$DW$T$805	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$805, DW_AT_type(*$C$DW$T$802)
$C$DW$1630	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1630, DW_AT_type(*$C$DW$T$804)

	.dwendtag $C$DW$T$805

	.dwendtag $C$DW$TU$805


$C$DW$TU$806	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$806

$C$DW$T$806	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$806, DW_AT_type(*$C$DW$T$802)
$C$DW$1631	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1631, DW_AT_type(*$C$DW$T$802)

	.dwendtag $C$DW$T$806

	.dwendtag $C$DW$TU$806


$C$DW$TU$803	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$803
$C$DW$T$803	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$803, DW_AT_type(*$C$DW$T$807)

	.dwendtag $C$DW$TU$803


$C$DW$TU$804	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$804
$C$DW$T$804	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$804, DW_AT_type(*$C$DW$T$803)
	.dwattr $C$DW$T$804, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$804


$C$DW$TU$826	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$826
$C$DW$T$826	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$826, DW_AT_name("sTIDL_EltWiseParams_t")
	.dwattr $C$DW$T$826, DW_AT_type(*$C$DW$T$807)
	.dwattr $C$DW$T$826, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$826, DW_AT_decl_line(0x51e)
	.dwattr $C$DW$T$826, DW_AT_decl_column(0x02)

	.dwendtag $C$DW$TU$826


$C$DW$TU$814	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$814

$C$DW$T$814	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$814, DW_AT_name("sTIDL_InnerProductParams_t")
	.dwattr $C$DW$T$814, DW_AT_byte_size(0x38)
$C$DW$1632	.dwtag  DW_TAG_member
	.dwattr $C$DW$1632, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1632, DW_AT_name("weights")
	.dwattr $C$DW$1632, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1632, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1632, DW_AT_decl_line(0x4ed)
	.dwattr $C$DW$1632, DW_AT_decl_column(0x0d)

$C$DW$1633	.dwtag  DW_TAG_member
	.dwattr $C$DW$1633, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1633, DW_AT_name("bias")
	.dwattr $C$DW$1633, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$1633, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1633, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1633, DW_AT_decl_line(0x4ef)
	.dwattr $C$DW$1633, DW_AT_decl_column(0x0d)

$C$DW$1634	.dwtag  DW_TAG_member
	.dwattr $C$DW$1634, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1634, DW_AT_name("activationType")
	.dwattr $C$DW$1634, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$1634, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1634, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1634, DW_AT_decl_line(0x4f1)
	.dwattr $C$DW$1634, DW_AT_decl_column(0x0d)

$C$DW$1635	.dwtag  DW_TAG_member
	.dwattr $C$DW$1635, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1635, DW_AT_name("numInNodes")
	.dwattr $C$DW$1635, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$1635, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1635, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1635, DW_AT_decl_line(0x4f3)
	.dwattr $C$DW$1635, DW_AT_decl_column(0x0d)

$C$DW$1636	.dwtag  DW_TAG_member
	.dwattr $C$DW$1636, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1636, DW_AT_name("numOutNodes")
	.dwattr $C$DW$1636, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$1636, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1636, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1636, DW_AT_decl_line(0x4f5)
	.dwattr $C$DW$1636, DW_AT_decl_column(0x0d)

$C$DW$1637	.dwtag  DW_TAG_member
	.dwattr $C$DW$1637, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1637, DW_AT_name("weightsQ")
	.dwattr $C$DW$1637, DW_AT_data_member_location[DW_OP_plus_uconst 0x14]
	.dwattr $C$DW$1637, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1637, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1637, DW_AT_decl_line(0x4f7)
	.dwattr $C$DW$1637, DW_AT_decl_column(0x0d)

$C$DW$1638	.dwtag  DW_TAG_member
	.dwattr $C$DW$1638, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$1638, DW_AT_name("weightScale")
	.dwattr $C$DW$1638, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$1638, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1638, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1638, DW_AT_decl_line(0x4f9)
	.dwattr $C$DW$1638, DW_AT_decl_column(0x14)

$C$DW$1639	.dwtag  DW_TAG_member
	.dwattr $C$DW$1639, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$1639, DW_AT_name("biasScale")
	.dwattr $C$DW$1639, DW_AT_data_member_location[DW_OP_plus_uconst 0x1c]
	.dwattr $C$DW$1639, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1639, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1639, DW_AT_decl_line(0x4fb)
	.dwattr $C$DW$1639, DW_AT_decl_column(0x14)

$C$DW$1640	.dwtag  DW_TAG_member
	.dwattr $C$DW$1640, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1640, DW_AT_name("zeroWeightValue")
	.dwattr $C$DW$1640, DW_AT_data_member_location[DW_OP_plus_uconst 0x20]
	.dwattr $C$DW$1640, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1640, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1640, DW_AT_decl_line(0x4fd)
	.dwattr $C$DW$1640, DW_AT_decl_column(0x0d)

$C$DW$1641	.dwtag  DW_TAG_member
	.dwattr $C$DW$1641, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1641, DW_AT_name("biasQ")
	.dwattr $C$DW$1641, DW_AT_data_member_location[DW_OP_plus_uconst 0x24]
	.dwattr $C$DW$1641, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1641, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1641, DW_AT_decl_line(0x4ff)
	.dwattr $C$DW$1641, DW_AT_decl_column(0x0d)

$C$DW$1642	.dwtag  DW_TAG_member
	.dwattr $C$DW$1642, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1642, DW_AT_name("inDataQ")
	.dwattr $C$DW$1642, DW_AT_data_member_location[DW_OP_plus_uconst 0x28]
	.dwattr $C$DW$1642, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1642, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1642, DW_AT_decl_line(0x501)
	.dwattr $C$DW$1642, DW_AT_decl_column(0x0d)

$C$DW$1643	.dwtag  DW_TAG_member
	.dwattr $C$DW$1643, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1643, DW_AT_name("outDataQ")
	.dwattr $C$DW$1643, DW_AT_data_member_location[DW_OP_plus_uconst 0x2c]
	.dwattr $C$DW$1643, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1643, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1643, DW_AT_decl_line(0x503)
	.dwattr $C$DW$1643, DW_AT_decl_column(0x0d)

$C$DW$1644	.dwtag  DW_TAG_member
	.dwattr $C$DW$1644, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1644, DW_AT_name("interDataQ")
	.dwattr $C$DW$1644, DW_AT_data_member_location[DW_OP_plus_uconst 0x30]
	.dwattr $C$DW$1644, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1644, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1644, DW_AT_decl_line(0x505)
	.dwattr $C$DW$1644, DW_AT_decl_column(0x0d)

$C$DW$1645	.dwtag  DW_TAG_member
	.dwattr $C$DW$1645, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1645, DW_AT_name("biasB")
	.dwattr $C$DW$1645, DW_AT_data_member_location[DW_OP_plus_uconst 0x34]
	.dwattr $C$DW$1645, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1645, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1645, DW_AT_decl_line(0x507)
	.dwattr $C$DW$1645, DW_AT_decl_column(0x0d)


$C$DW$1646	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1646, DW_AT_name("operator =")
	.dwattr $C$DW$1646, DW_AT_declaration
	.dwattr $C$DW$1646, DW_AT_linkage_name("_ZN26sTIDL_InnerProductParams_taSERKS_")
	.dwattr $C$DW$1646, DW_AT_type(*$C$DW$T$809)
	.dwattr $C$DW$1646, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1647	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1647, DW_AT_type(*$C$DW$T$811)

	.dwendtag $C$DW$1646


$C$DW$1648	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1648, DW_AT_name("operator =")
	.dwattr $C$DW$1648, DW_AT_declaration
	.dwattr $C$DW$1648, DW_AT_linkage_name("_ZN26sTIDL_InnerProductParams_taSEOS_")
	.dwattr $C$DW$1648, DW_AT_type(*$C$DW$T$809)
	.dwattr $C$DW$1648, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1649	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1649, DW_AT_type(*$C$DW$T$809)

	.dwendtag $C$DW$1648

	.dwattr $C$DW$T$814, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$814, DW_AT_decl_line(0x4eb)
	.dwattr $C$DW$T$814, DW_AT_decl_column(0x10)
	.dwendtag $C$DW$T$814

	.dwendtag $C$DW$TU$814


$C$DW$TU$809	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$809
$C$DW$T$809	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$809, DW_AT_type(*$C$DW$T$814)
	.dwattr $C$DW$T$809, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$809


$C$DW$TU$812	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$812

$C$DW$T$812	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$812, DW_AT_type(*$C$DW$T$809)
$C$DW$1650	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1650, DW_AT_type(*$C$DW$T$811)

	.dwendtag $C$DW$T$812

	.dwendtag $C$DW$TU$812


$C$DW$TU$813	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$813

$C$DW$T$813	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$813, DW_AT_type(*$C$DW$T$809)
$C$DW$1651	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1651, DW_AT_type(*$C$DW$T$809)

	.dwendtag $C$DW$T$813

	.dwendtag $C$DW$TU$813


$C$DW$TU$810	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$810
$C$DW$T$810	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$810, DW_AT_type(*$C$DW$T$814)

	.dwendtag $C$DW$TU$810


$C$DW$TU$811	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$811
$C$DW$T$811	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$811, DW_AT_type(*$C$DW$T$810)
	.dwattr $C$DW$T$811, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$811


$C$DW$TU$827	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$827
$C$DW$T$827	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$827, DW_AT_name("sTIDL_InnerProductParams_t")
	.dwattr $C$DW$T$827, DW_AT_type(*$C$DW$T$814)
	.dwattr $C$DW$T$827, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$827, DW_AT_decl_line(0x508)
	.dwattr $C$DW$T$827, DW_AT_decl_column(0x02)

	.dwendtag $C$DW$TU$827


$C$DW$TU$822	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$822

$C$DW$T$822	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$822, DW_AT_name("sTIDL_LayerBuf_t")
	.dwattr $C$DW$T$822, DW_AT_byte_size(0x21000)
$C$DW$1652	.dwtag  DW_TAG_member
	.dwattr $C$DW$1652, DW_AT_type(*$C$DW$T$528)
	.dwattr $C$DW$1652, DW_AT_name("outDataSize")
	.dwattr $C$DW$1652, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1652, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1652, DW_AT_decl_line(0x1bb)
	.dwattr $C$DW$1652, DW_AT_decl_column(0x0b)

$C$DW$1653	.dwtag  DW_TAG_member
	.dwattr $C$DW$1653, DW_AT_type(*$C$DW$T$816)
	.dwattr $C$DW$1653, DW_AT_name("newInDataId")
	.dwattr $C$DW$1653, DW_AT_data_member_location[DW_OP_plus_uconst 0x1000]
	.dwattr $C$DW$1653, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1653, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1653, DW_AT_decl_line(0x1bc)
	.dwattr $C$DW$1653, DW_AT_decl_column(0x0b)

$C$DW$1654	.dwtag  DW_TAG_member
	.dwattr $C$DW$1654, DW_AT_type(*$C$DW$T$816)
	.dwattr $C$DW$1654, DW_AT_name("newOutDataId")
	.dwattr $C$DW$1654, DW_AT_data_member_location[DW_OP_plus_uconst 0x11000]
	.dwattr $C$DW$1654, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1654, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1654, DW_AT_decl_line(0x1bd)
	.dwattr $C$DW$1654, DW_AT_decl_column(0x0b)


$C$DW$1655	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1655, DW_AT_name("operator =")
	.dwattr $C$DW$1655, DW_AT_declaration
	.dwattr $C$DW$1655, DW_AT_linkage_name("_ZN16sTIDL_LayerBuf_taSERKS_")
	.dwattr $C$DW$1655, DW_AT_type(*$C$DW$T$817)
	.dwattr $C$DW$1655, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1656	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1656, DW_AT_type(*$C$DW$T$819)

	.dwendtag $C$DW$1655


$C$DW$1657	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1657, DW_AT_name("operator =")
	.dwattr $C$DW$1657, DW_AT_declaration
	.dwattr $C$DW$1657, DW_AT_linkage_name("_ZN16sTIDL_LayerBuf_taSEOS_")
	.dwattr $C$DW$1657, DW_AT_type(*$C$DW$T$817)
	.dwattr $C$DW$1657, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1658	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1658, DW_AT_type(*$C$DW$T$817)

	.dwendtag $C$DW$1657

	.dwattr $C$DW$T$822, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$T$822, DW_AT_decl_line(0x1ba)
	.dwattr $C$DW$T$822, DW_AT_decl_column(0x10)
	.dwendtag $C$DW$T$822

	.dwendtag $C$DW$TU$822


$C$DW$TU$817	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$817
$C$DW$T$817	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$817, DW_AT_type(*$C$DW$T$822)
	.dwattr $C$DW$T$817, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$817


$C$DW$TU$820	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$820

$C$DW$T$820	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$820, DW_AT_type(*$C$DW$T$817)
$C$DW$1659	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1659, DW_AT_type(*$C$DW$T$819)

	.dwendtag $C$DW$T$820

	.dwendtag $C$DW$TU$820


$C$DW$TU$821	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$821

$C$DW$T$821	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$821, DW_AT_type(*$C$DW$T$817)
$C$DW$1660	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1660, DW_AT_type(*$C$DW$T$817)

	.dwendtag $C$DW$T$821

	.dwendtag $C$DW$TU$821


$C$DW$TU$362	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$362
$C$DW$T$362	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$362, DW_AT_name("sTIDL_LayerBuf_t")
	.dwattr $C$DW$T$362, DW_AT_type(*$C$DW$T$822)
	.dwattr $C$DW$T$362, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$T$362, DW_AT_decl_line(0x1be)
	.dwattr $C$DW$T$362, DW_AT_decl_column(0x02)

	.dwendtag $C$DW$TU$362


$C$DW$TU$363	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$363
$C$DW$T$363	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$363, DW_AT_type(*$C$DW$T$362)
	.dwattr $C$DW$T$363, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$363


$C$DW$TU$818	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$818
$C$DW$T$818	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$818, DW_AT_type(*$C$DW$T$822)

	.dwendtag $C$DW$TU$818


$C$DW$TU$819	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$819
$C$DW$T$819	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$819, DW_AT_type(*$C$DW$T$818)
	.dwattr $C$DW$T$819, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$819


$C$DW$TU$850	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$850

$C$DW$T$850	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$850, DW_AT_name("sTIDL_LayerParams_t")
	.dwattr $C$DW$T$850, DW_AT_byte_size(0xa8)
$C$DW$1661	.dwtag  DW_TAG_member
	.dwattr $C$DW$1661, DW_AT_type(*$C$DW$T$825)
	.dwattr $C$DW$1661, DW_AT_name("convParams")
	.dwattr $C$DW$1661, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1661, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1661, DW_AT_decl_line(0x554)
	.dwattr $C$DW$1661, DW_AT_decl_column(0x29)

$C$DW$1662	.dwtag  DW_TAG_member
	.dwattr $C$DW$1662, DW_AT_type(*$C$DW$T$826)
	.dwattr $C$DW$1662, DW_AT_name("eltWiseParams")
	.dwattr $C$DW$1662, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1662, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1662, DW_AT_decl_line(0x555)
	.dwattr $C$DW$1662, DW_AT_decl_column(0x29)

$C$DW$1663	.dwtag  DW_TAG_member
	.dwattr $C$DW$1663, DW_AT_type(*$C$DW$T$749)
	.dwattr $C$DW$1663, DW_AT_name("poolParams")
	.dwattr $C$DW$1663, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1663, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1663, DW_AT_decl_line(0x556)
	.dwattr $C$DW$1663, DW_AT_decl_column(0x29)

$C$DW$1664	.dwtag  DW_TAG_member
	.dwattr $C$DW$1664, DW_AT_type(*$C$DW$T$827)
	.dwattr $C$DW$1664, DW_AT_name("innerProductParams")
	.dwattr $C$DW$1664, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1664, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1664, DW_AT_decl_line(0x557)
	.dwattr $C$DW$1664, DW_AT_decl_column(0x29)

$C$DW$1665	.dwtag  DW_TAG_member
	.dwattr $C$DW$1665, DW_AT_type(*$C$DW$T$828)
	.dwattr $C$DW$1665, DW_AT_name("dataLayerParams")
	.dwattr $C$DW$1665, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1665, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1665, DW_AT_decl_line(0x558)
	.dwattr $C$DW$1665, DW_AT_decl_column(0x29)

$C$DW$1666	.dwtag  DW_TAG_member
	.dwattr $C$DW$1666, DW_AT_type(*$C$DW$T$829)
	.dwattr $C$DW$1666, DW_AT_name("argMaxParams")
	.dwattr $C$DW$1666, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1666, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1666, DW_AT_decl_line(0x559)
	.dwattr $C$DW$1666, DW_AT_decl_column(0x29)

$C$DW$1667	.dwtag  DW_TAG_member
	.dwattr $C$DW$1667, DW_AT_type(*$C$DW$T$830)
	.dwattr $C$DW$1667, DW_AT_name("softMaxParams")
	.dwattr $C$DW$1667, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1667, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1667, DW_AT_decl_line(0x55a)
	.dwattr $C$DW$1667, DW_AT_decl_column(0x29)

$C$DW$1668	.dwtag  DW_TAG_member
	.dwattr $C$DW$1668, DW_AT_type(*$C$DW$T$831)
	.dwattr $C$DW$1668, DW_AT_name("cropParams")
	.dwattr $C$DW$1668, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1668, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1668, DW_AT_decl_line(0x55b)
	.dwattr $C$DW$1668, DW_AT_decl_column(0x29)

$C$DW$1669	.dwtag  DW_TAG_member
	.dwattr $C$DW$1669, DW_AT_type(*$C$DW$T$832)
	.dwattr $C$DW$1669, DW_AT_name("concatParams")
	.dwattr $C$DW$1669, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1669, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1669, DW_AT_decl_line(0x55c)
	.dwattr $C$DW$1669, DW_AT_decl_column(0x29)

$C$DW$1670	.dwtag  DW_TAG_member
	.dwattr $C$DW$1670, DW_AT_type(*$C$DW$T$833)
	.dwattr $C$DW$1670, DW_AT_name("detectOutParams")
	.dwattr $C$DW$1670, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1670, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1670, DW_AT_decl_line(0x55d)
	.dwattr $C$DW$1670, DW_AT_decl_column(0x29)

$C$DW$1671	.dwtag  DW_TAG_member
	.dwattr $C$DW$1671, DW_AT_type(*$C$DW$T$834)
	.dwattr $C$DW$1671, DW_AT_name("biasParams")
	.dwattr $C$DW$1671, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1671, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1671, DW_AT_decl_line(0x55e)
	.dwattr $C$DW$1671, DW_AT_decl_column(0x29)

$C$DW$1672	.dwtag  DW_TAG_member
	.dwattr $C$DW$1672, DW_AT_type(*$C$DW$T$835)
	.dwattr $C$DW$1672, DW_AT_name("batchNormParams")
	.dwattr $C$DW$1672, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1672, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1672, DW_AT_decl_line(0x55f)
	.dwattr $C$DW$1672, DW_AT_decl_column(0x29)

$C$DW$1673	.dwtag  DW_TAG_member
	.dwattr $C$DW$1673, DW_AT_type(*$C$DW$T$836)
	.dwattr $C$DW$1673, DW_AT_name("shuffleLayerParams")
	.dwattr $C$DW$1673, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1673, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1673, DW_AT_decl_line(0x560)
	.dwattr $C$DW$1673, DW_AT_decl_column(0x29)

$C$DW$1674	.dwtag  DW_TAG_member
	.dwattr $C$DW$1674, DW_AT_type(*$C$DW$T$837)
	.dwattr $C$DW$1674, DW_AT_name("sliceParams")
	.dwattr $C$DW$1674, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1674, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1674, DW_AT_decl_line(0x561)
	.dwattr $C$DW$1674, DW_AT_decl_column(0x29)

$C$DW$1675	.dwtag  DW_TAG_member
	.dwattr $C$DW$1675, DW_AT_type(*$C$DW$T$838)
	.dwattr $C$DW$1675, DW_AT_name("resizeParams")
	.dwattr $C$DW$1675, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1675, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1675, DW_AT_decl_line(0x562)
	.dwattr $C$DW$1675, DW_AT_decl_column(0x29)

$C$DW$1676	.dwtag  DW_TAG_member
	.dwattr $C$DW$1676, DW_AT_type(*$C$DW$T$839)
	.dwattr $C$DW$1676, DW_AT_name("roiPoolingParams")
	.dwattr $C$DW$1676, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1676, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1676, DW_AT_decl_line(0x563)
	.dwattr $C$DW$1676, DW_AT_decl_column(0x29)

$C$DW$1677	.dwtag  DW_TAG_member
	.dwattr $C$DW$1677, DW_AT_type(*$C$DW$T$840)
	.dwattr $C$DW$1677, DW_AT_name("depthToSpaceParams")
	.dwattr $C$DW$1677, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1677, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1677, DW_AT_decl_line(0x564)
	.dwattr $C$DW$1677, DW_AT_decl_column(0x29)

$C$DW$1678	.dwtag  DW_TAG_member
	.dwattr $C$DW$1678, DW_AT_type(*$C$DW$T$841)
	.dwattr $C$DW$1678, DW_AT_name("padLayerParams")
	.dwattr $C$DW$1678, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1678, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1678, DW_AT_decl_line(0x565)
	.dwattr $C$DW$1678, DW_AT_decl_column(0x29)

$C$DW$1679	.dwtag  DW_TAG_member
	.dwattr $C$DW$1679, DW_AT_type(*$C$DW$T$842)
	.dwattr $C$DW$1679, DW_AT_name("odOutputReformatLayerParams")
	.dwattr $C$DW$1679, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1679, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1679, DW_AT_decl_line(0x566)
	.dwattr $C$DW$1679, DW_AT_decl_column(0x29)

$C$DW$1680	.dwtag  DW_TAG_member
	.dwattr $C$DW$1680, DW_AT_type(*$C$DW$T$843)
	.dwattr $C$DW$1680, DW_AT_name("dataConvertParams")
	.dwattr $C$DW$1680, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1680, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1680, DW_AT_decl_line(0x567)
	.dwattr $C$DW$1680, DW_AT_decl_column(0x29)

$C$DW$1681	.dwtag  DW_TAG_member
	.dwattr $C$DW$1681, DW_AT_type(*$C$DW$T$844)
	.dwattr $C$DW$1681, DW_AT_name("customParams")
	.dwattr $C$DW$1681, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1681, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1681, DW_AT_decl_line(0x568)
	.dwattr $C$DW$1681, DW_AT_decl_column(0x29)


$C$DW$1682	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1682, DW_AT_name("operator =")
	.dwattr $C$DW$1682, DW_AT_declaration
	.dwattr $C$DW$1682, DW_AT_linkage_name("_ZN19sTIDL_LayerParams_taSERKS_")
	.dwattr $C$DW$1682, DW_AT_type(*$C$DW$T$845)
	.dwattr $C$DW$1682, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1683	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1683, DW_AT_type(*$C$DW$T$847)

	.dwendtag $C$DW$1682


$C$DW$1684	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1684, DW_AT_name("operator =")
	.dwattr $C$DW$1684, DW_AT_declaration
	.dwattr $C$DW$1684, DW_AT_linkage_name("_ZN19sTIDL_LayerParams_taSEOS_")
	.dwattr $C$DW$1684, DW_AT_type(*$C$DW$T$845)
	.dwattr $C$DW$1684, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1685	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1685, DW_AT_type(*$C$DW$T$845)

	.dwendtag $C$DW$1684

	.dwattr $C$DW$T$850, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$850, DW_AT_decl_line(0x553)
	.dwattr $C$DW$T$850, DW_AT_decl_column(0x0f)
	.dwendtag $C$DW$T$850

	.dwendtag $C$DW$TU$850


$C$DW$TU$845	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$845
$C$DW$T$845	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$845, DW_AT_type(*$C$DW$T$850)
	.dwattr $C$DW$T$845, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$845


$C$DW$TU$848	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$848

$C$DW$T$848	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$848, DW_AT_type(*$C$DW$T$845)
$C$DW$1686	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1686, DW_AT_type(*$C$DW$T$847)

	.dwendtag $C$DW$T$848

	.dwendtag $C$DW$TU$848


$C$DW$TU$849	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$849

$C$DW$T$849	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$849, DW_AT_type(*$C$DW$T$845)
$C$DW$1687	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1687, DW_AT_type(*$C$DW$T$845)

	.dwendtag $C$DW$T$849

	.dwendtag $C$DW$TU$849


$C$DW$TU$846	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$846
$C$DW$T$846	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$846, DW_AT_type(*$C$DW$T$850)

	.dwendtag $C$DW$TU$846


$C$DW$TU$847	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$847
$C$DW$T$847	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$847, DW_AT_type(*$C$DW$T$846)
	.dwattr $C$DW$T$847, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$847


$C$DW$TU$872	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$872
$C$DW$T$872	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$872, DW_AT_name("sTIDL_LayerParams_t")
	.dwattr $C$DW$T$872, DW_AT_type(*$C$DW$T$850)
	.dwattr $C$DW$T$872, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$872, DW_AT_decl_line(0x569)
	.dwattr $C$DW$T$872, DW_AT_decl_column(0x02)

	.dwendtag $C$DW$TU$872


$C$DW$TU$881	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$881

$C$DW$T$881	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$881, DW_AT_name("sTIDL_Layer_t")
	.dwattr $C$DW$T$881, DW_AT_byte_size(0xc58)
$C$DW$1688	.dwtag  DW_TAG_member
	.dwattr $C$DW$1688, DW_AT_type(*$C$DW$T$872)
	.dwattr $C$DW$1688, DW_AT_name("layerParams")
	.dwattr $C$DW$1688, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1688, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1688, DW_AT_decl_line(0x572)
	.dwattr $C$DW$1688, DW_AT_decl_column(0x17)

$C$DW$1689	.dwtag  DW_TAG_member
	.dwattr $C$DW$1689, DW_AT_type(*$C$DW$T$873)
	.dwattr $C$DW$1689, DW_AT_name("actParams")
	.dwattr $C$DW$1689, DW_AT_data_member_location[DW_OP_plus_uconst 0xa8]
	.dwattr $C$DW$1689, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1689, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1689, DW_AT_decl_line(0x574)
	.dwattr $C$DW$1689, DW_AT_decl_column(0x18)

$C$DW$1690	.dwtag  DW_TAG_member
	.dwattr $C$DW$1690, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1690, DW_AT_name("layerType")
	.dwattr $C$DW$1690, DW_AT_data_member_location[DW_OP_plus_uconst 0xbc]
	.dwattr $C$DW$1690, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1690, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1690, DW_AT_decl_line(0x576)
	.dwattr $C$DW$1690, DW_AT_decl_column(0x0b)

$C$DW$1691	.dwtag  DW_TAG_member
	.dwattr $C$DW$1691, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1691, DW_AT_name("numInBufs")
	.dwattr $C$DW$1691, DW_AT_data_member_location[DW_OP_plus_uconst 0xc0]
	.dwattr $C$DW$1691, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1691, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1691, DW_AT_decl_line(0x578)
	.dwattr $C$DW$1691, DW_AT_decl_column(0x0b)

$C$DW$1692	.dwtag  DW_TAG_member
	.dwattr $C$DW$1692, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1692, DW_AT_name("numOutBufs")
	.dwattr $C$DW$1692, DW_AT_data_member_location[DW_OP_plus_uconst 0xc4]
	.dwattr $C$DW$1692, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1692, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1692, DW_AT_decl_line(0x57a)
	.dwattr $C$DW$1692, DW_AT_decl_column(0x0b)

$C$DW$1693	.dwtag  DW_TAG_member
	.dwattr $C$DW$1693, DW_AT_type(*$C$DW$T$875)
	.dwattr $C$DW$1693, DW_AT_name("inData")
	.dwattr $C$DW$1693, DW_AT_data_member_location[DW_OP_plus_uconst 0xc8]
	.dwattr $C$DW$1693, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1693, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1693, DW_AT_decl_line(0x57c)
	.dwattr $C$DW$1693, DW_AT_decl_column(0x16)

$C$DW$1694	.dwtag  DW_TAG_member
	.dwattr $C$DW$1694, DW_AT_type(*$C$DW$T$875)
	.dwattr $C$DW$1694, DW_AT_name("outData")
	.dwattr $C$DW$1694, DW_AT_data_member_location[DW_OP_plus_uconst 0x688]
	.dwattr $C$DW$1694, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1694, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1694, DW_AT_decl_line(0x57e)
	.dwattr $C$DW$1694, DW_AT_decl_column(0x16)

$C$DW$1695	.dwtag  DW_TAG_member
	.dwattr $C$DW$1695, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1695, DW_AT_name("coreID")
	.dwattr $C$DW$1695, DW_AT_data_member_location[DW_OP_plus_uconst 0xc48]
	.dwattr $C$DW$1695, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1695, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1695, DW_AT_decl_line(0x580)
	.dwattr $C$DW$1695, DW_AT_decl_column(0x0b)

$C$DW$1696	.dwtag  DW_TAG_member
	.dwattr $C$DW$1696, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1696, DW_AT_name("layersGroupId")
	.dwattr $C$DW$1696, DW_AT_data_member_location[DW_OP_plus_uconst 0xc4c]
	.dwattr $C$DW$1696, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1696, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1696, DW_AT_decl_line(0x583)
	.dwattr $C$DW$1696, DW_AT_decl_column(0x0b)

$C$DW$1697	.dwtag  DW_TAG_member
	.dwattr $C$DW$1697, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1697, DW_AT_name("weightsElementSizeInBits")
	.dwattr $C$DW$1697, DW_AT_data_member_location[DW_OP_plus_uconst 0xc50]
	.dwattr $C$DW$1697, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1697, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1697, DW_AT_decl_line(0x585)
	.dwattr $C$DW$1697, DW_AT_decl_column(0x0b)

$C$DW$1698	.dwtag  DW_TAG_member
	.dwattr $C$DW$1698, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1698, DW_AT_name("strideOffsetMethod")
	.dwattr $C$DW$1698, DW_AT_data_member_location[DW_OP_plus_uconst 0xc54]
	.dwattr $C$DW$1698, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1698, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1698, DW_AT_decl_line(0x587)
	.dwattr $C$DW$1698, DW_AT_decl_column(0x0b)


$C$DW$1699	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1699, DW_AT_name("operator =")
	.dwattr $C$DW$1699, DW_AT_declaration
	.dwattr $C$DW$1699, DW_AT_linkage_name("_ZN13sTIDL_Layer_taSERKS_")
	.dwattr $C$DW$1699, DW_AT_type(*$C$DW$T$876)
	.dwattr $C$DW$1699, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1700	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1700, DW_AT_type(*$C$DW$T$878)

	.dwendtag $C$DW$1699


$C$DW$1701	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1701, DW_AT_name("operator =")
	.dwattr $C$DW$1701, DW_AT_declaration
	.dwattr $C$DW$1701, DW_AT_linkage_name("_ZN13sTIDL_Layer_taSEOS_")
	.dwattr $C$DW$1701, DW_AT_type(*$C$DW$T$876)
	.dwattr $C$DW$1701, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1702	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1702, DW_AT_type(*$C$DW$T$876)

	.dwendtag $C$DW$1701

	.dwattr $C$DW$T$881, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$881, DW_AT_decl_line(0x571)
	.dwattr $C$DW$T$881, DW_AT_decl_column(0x10)
	.dwendtag $C$DW$T$881

	.dwendtag $C$DW$TU$881


$C$DW$TU$876	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$876
$C$DW$T$876	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$876, DW_AT_type(*$C$DW$T$881)
	.dwattr $C$DW$T$876, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$876


$C$DW$TU$879	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$879

$C$DW$T$879	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$879, DW_AT_type(*$C$DW$T$876)
$C$DW$1703	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1703, DW_AT_type(*$C$DW$T$878)

	.dwendtag $C$DW$T$879

	.dwendtag $C$DW$TU$879


$C$DW$TU$880	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$880

$C$DW$T$880	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$880, DW_AT_type(*$C$DW$T$876)
$C$DW$1704	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1704, DW_AT_type(*$C$DW$T$876)

	.dwendtag $C$DW$T$880

	.dwendtag $C$DW$TU$880


$C$DW$TU$282	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$282
$C$DW$T$282	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$282, DW_AT_name("sTIDL_Layer_t")
	.dwattr $C$DW$T$282, DW_AT_type(*$C$DW$T$881)
	.dwattr $C$DW$T$282, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$282, DW_AT_decl_line(0x588)
	.dwattr $C$DW$T$282, DW_AT_decl_column(0x02)

	.dwendtag $C$DW$TU$282


$C$DW$TU$283	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$283
$C$DW$T$283	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$283, DW_AT_type(*$C$DW$T$282)
	.dwattr $C$DW$T$283, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$283


$C$DW$TU$888	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$888

$C$DW$T$888	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$888, DW_AT_type(*$C$DW$T$282)
	.dwattr $C$DW$T$888, DW_AT_byte_size(0x316000)
$C$DW$1705	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$1705, DW_AT_upper_bound(0x3ff)

	.dwendtag $C$DW$T$888

	.dwendtag $C$DW$TU$888


$C$DW$TU$1061	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$1061
$C$DW$T$1061	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$1061, DW_AT_type(*$C$DW$T$282)

	.dwendtag $C$DW$TU$1061


$C$DW$TU$1062	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$1062
$C$DW$T$1062	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$1062, DW_AT_type(*$C$DW$T$1061)
	.dwattr $C$DW$T$1062, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$1062


$C$DW$TU$877	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$877
$C$DW$T$877	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$877, DW_AT_type(*$C$DW$T$881)

	.dwendtag $C$DW$TU$877


$C$DW$TU$878	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$878
$C$DW$T$878	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$878, DW_AT_type(*$C$DW$T$877)
	.dwattr $C$DW$T$878, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$878


$C$DW$TU$894	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$894

$C$DW$T$894	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$894, DW_AT_name("sTIDL_Network_t")
	.dwattr $C$DW$T$894, DW_AT_byte_size(0x31604c)
$C$DW$1706	.dwtag  DW_TAG_member
	.dwattr $C$DW$1706, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1706, DW_AT_name("netVersion")
	.dwattr $C$DW$1706, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1706, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1706, DW_AT_decl_line(0x593)
	.dwattr $C$DW$1706, DW_AT_decl_column(0x0b)

$C$DW$1707	.dwtag  DW_TAG_member
	.dwattr $C$DW$1707, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1707, DW_AT_name("deviceName")
	.dwattr $C$DW$1707, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$1707, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1707, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1707, DW_AT_decl_line(0x595)
	.dwattr $C$DW$1707, DW_AT_decl_column(0x0b)

$C$DW$1708	.dwtag  DW_TAG_member
	.dwattr $C$DW$1708, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1708, DW_AT_name("numLayers")
	.dwattr $C$DW$1708, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$1708, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1708, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1708, DW_AT_decl_line(0x597)
	.dwattr $C$DW$1708, DW_AT_decl_column(0x0b)

$C$DW$1709	.dwtag  DW_TAG_member
	.dwattr $C$DW$1709, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1709, DW_AT_name("weightsElementSize")
	.dwattr $C$DW$1709, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$1709, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1709, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1709, DW_AT_decl_line(0x599)
	.dwattr $C$DW$1709, DW_AT_decl_column(0x0b)

$C$DW$1710	.dwtag  DW_TAG_member
	.dwattr $C$DW$1710, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1710, DW_AT_name("slopeElementSize")
	.dwattr $C$DW$1710, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$1710, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1710, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1710, DW_AT_decl_line(0x59b)
	.dwattr $C$DW$1710, DW_AT_decl_column(0x0b)

$C$DW$1711	.dwtag  DW_TAG_member
	.dwattr $C$DW$1711, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1711, DW_AT_name("biasElementSize")
	.dwattr $C$DW$1711, DW_AT_data_member_location[DW_OP_plus_uconst 0x14]
	.dwattr $C$DW$1711, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1711, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1711, DW_AT_decl_line(0x59d)
	.dwattr $C$DW$1711, DW_AT_decl_column(0x0b)

$C$DW$1712	.dwtag  DW_TAG_member
	.dwattr $C$DW$1712, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1712, DW_AT_name("dataElementSize")
	.dwattr $C$DW$1712, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$1712, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1712, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1712, DW_AT_decl_line(0x59f)
	.dwattr $C$DW$1712, DW_AT_decl_column(0x0b)

$C$DW$1713	.dwtag  DW_TAG_member
	.dwattr $C$DW$1713, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1713, DW_AT_name("interElementSize")
	.dwattr $C$DW$1713, DW_AT_data_member_location[DW_OP_plus_uconst 0x1c]
	.dwattr $C$DW$1713, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1713, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1713, DW_AT_decl_line(0x5a1)
	.dwattr $C$DW$1713, DW_AT_decl_column(0x0b)

$C$DW$1714	.dwtag  DW_TAG_member
	.dwattr $C$DW$1714, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1714, DW_AT_name("quantizationStyle")
	.dwattr $C$DW$1714, DW_AT_data_member_location[DW_OP_plus_uconst 0x20]
	.dwattr $C$DW$1714, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1714, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1714, DW_AT_decl_line(0x5a3)
	.dwattr $C$DW$1714, DW_AT_decl_column(0x0b)

$C$DW$1715	.dwtag  DW_TAG_member
	.dwattr $C$DW$1715, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1715, DW_AT_name("calibrationOption")
	.dwattr $C$DW$1715, DW_AT_data_member_location[DW_OP_plus_uconst 0x24]
	.dwattr $C$DW$1715, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1715, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1715, DW_AT_decl_line(0x5aa)
	.dwattr $C$DW$1715, DW_AT_decl_column(0x0b)

$C$DW$1716	.dwtag  DW_TAG_member
	.dwattr $C$DW$1716, DW_AT_type(*$C$DW$T$887)
	.dwattr $C$DW$1716, DW_AT_name("calibrationParams")
	.dwattr $C$DW$1716, DW_AT_data_member_location[DW_OP_plus_uconst 0x28]
	.dwattr $C$DW$1716, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1716, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1716, DW_AT_decl_line(0x5ae)
	.dwattr $C$DW$1716, DW_AT_decl_column(0x1a)

$C$DW$1717	.dwtag  DW_TAG_member
	.dwattr $C$DW$1717, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1717, DW_AT_name("dataFlowInfo")
	.dwattr $C$DW$1717, DW_AT_data_member_location[DW_OP_plus_uconst 0x40]
	.dwattr $C$DW$1717, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1717, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1717, DW_AT_decl_line(0x5b0)
	.dwattr $C$DW$1717, DW_AT_decl_column(0x0b)

$C$DW$1718	.dwtag  DW_TAG_member
	.dwattr $C$DW$1718, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1718, DW_AT_name("isQuantStatsAvailable")
	.dwattr $C$DW$1718, DW_AT_data_member_location[DW_OP_plus_uconst 0x44]
	.dwattr $C$DW$1718, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1718, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1718, DW_AT_decl_line(0x5b2)
	.dwattr $C$DW$1718, DW_AT_decl_column(0x0b)

$C$DW$1719	.dwtag  DW_TAG_member
	.dwattr $C$DW$1719, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1719, DW_AT_name("reserved")
	.dwattr $C$DW$1719, DW_AT_data_member_location[DW_OP_plus_uconst 0x48]
	.dwattr $C$DW$1719, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1719, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1719, DW_AT_decl_line(0x5b4)
	.dwattr $C$DW$1719, DW_AT_decl_column(0x0b)

$C$DW$1720	.dwtag  DW_TAG_member
	.dwattr $C$DW$1720, DW_AT_type(*$C$DW$T$888)
	.dwattr $C$DW$1720, DW_AT_name("TIDLLayers")
	.dwattr $C$DW$1720, DW_AT_data_member_location[DW_OP_plus_uconst 0x4c]
	.dwattr $C$DW$1720, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1720, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1720, DW_AT_decl_line(0x5b6)
	.dwattr $C$DW$1720, DW_AT_decl_column(0x11)


$C$DW$1721	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1721, DW_AT_name("operator =")
	.dwattr $C$DW$1721, DW_AT_declaration
	.dwattr $C$DW$1721, DW_AT_linkage_name("_ZN15sTIDL_Network_taSERKS_")
	.dwattr $C$DW$1721, DW_AT_type(*$C$DW$T$889)
	.dwattr $C$DW$1721, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1722	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1722, DW_AT_type(*$C$DW$T$891)

	.dwendtag $C$DW$1721


$C$DW$1723	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1723, DW_AT_name("operator =")
	.dwattr $C$DW$1723, DW_AT_declaration
	.dwattr $C$DW$1723, DW_AT_linkage_name("_ZN15sTIDL_Network_taSEOS_")
	.dwattr $C$DW$1723, DW_AT_type(*$C$DW$T$889)
	.dwattr $C$DW$1723, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1724	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1724, DW_AT_type(*$C$DW$T$889)

	.dwendtag $C$DW$1723

	.dwattr $C$DW$T$894, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$894, DW_AT_decl_line(0x591)
	.dwattr $C$DW$T$894, DW_AT_decl_column(0x10)
	.dwendtag $C$DW$T$894

	.dwendtag $C$DW$TU$894


$C$DW$TU$889	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$889
$C$DW$T$889	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$889, DW_AT_type(*$C$DW$T$894)
	.dwattr $C$DW$T$889, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$889


$C$DW$TU$892	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$892

$C$DW$T$892	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$892, DW_AT_type(*$C$DW$T$889)
$C$DW$1725	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1725, DW_AT_type(*$C$DW$T$891)

	.dwendtag $C$DW$T$892

	.dwendtag $C$DW$TU$892


$C$DW$TU$893	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$893

$C$DW$T$893	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$893, DW_AT_type(*$C$DW$T$889)
$C$DW$1726	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1726, DW_AT_type(*$C$DW$T$889)

	.dwendtag $C$DW$T$893

	.dwendtag $C$DW$TU$893


$C$DW$TU$270	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$270
$C$DW$T$270	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$270, DW_AT_name("sTIDL_Network_t")
	.dwattr $C$DW$T$270, DW_AT_type(*$C$DW$T$894)
	.dwattr $C$DW$T$270, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$270, DW_AT_decl_line(0x5b7)
	.dwattr $C$DW$T$270, DW_AT_decl_column(0x02)

	.dwendtag $C$DW$TU$270


$C$DW$TU$271	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$271
$C$DW$T$271	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$271, DW_AT_type(*$C$DW$T$270)
	.dwattr $C$DW$T$271, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$271


$C$DW$TU$890	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$890
$C$DW$T$890	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$890, DW_AT_type(*$C$DW$T$894)

	.dwendtag $C$DW$TU$890


$C$DW$TU$891	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$891
$C$DW$T$891	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$891, DW_AT_type(*$C$DW$T$890)
	.dwattr $C$DW$T$891, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$891


$C$DW$TU$903	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$903

$C$DW$T$903	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$903, DW_AT_name("sTIDL_PadLayerParams_t")
	.dwattr $C$DW$T$903, DW_AT_byte_size(0x1c)
$C$DW$1727	.dwtag  DW_TAG_member
	.dwattr $C$DW$1727, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1727, DW_AT_name("padT")
	.dwattr $C$DW$1727, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1727, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1727, DW_AT_decl_line(0x2b9)
	.dwattr $C$DW$1727, DW_AT_decl_column(0x0b)

$C$DW$1728	.dwtag  DW_TAG_member
	.dwattr $C$DW$1728, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1728, DW_AT_name("padB")
	.dwattr $C$DW$1728, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$1728, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1728, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1728, DW_AT_decl_line(0x2bb)
	.dwattr $C$DW$1728, DW_AT_decl_column(0x0b)

$C$DW$1729	.dwtag  DW_TAG_member
	.dwattr $C$DW$1729, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1729, DW_AT_name("padL")
	.dwattr $C$DW$1729, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$1729, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1729, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1729, DW_AT_decl_line(0x2bd)
	.dwattr $C$DW$1729, DW_AT_decl_column(0x0b)

$C$DW$1730	.dwtag  DW_TAG_member
	.dwattr $C$DW$1730, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1730, DW_AT_name("padR")
	.dwattr $C$DW$1730, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$1730, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1730, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1730, DW_AT_decl_line(0x2bf)
	.dwattr $C$DW$1730, DW_AT_decl_column(0x0b)

$C$DW$1731	.dwtag  DW_TAG_member
	.dwattr $C$DW$1731, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1731, DW_AT_name("padConstValue")
	.dwattr $C$DW$1731, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$1731, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1731, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1731, DW_AT_decl_line(0x2c1)
	.dwattr $C$DW$1731, DW_AT_decl_column(0x0d)

$C$DW$1732	.dwtag  DW_TAG_member
	.dwattr $C$DW$1732, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1732, DW_AT_name("padType")
	.dwattr $C$DW$1732, DW_AT_data_member_location[DW_OP_plus_uconst 0x14]
	.dwattr $C$DW$1732, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1732, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1732, DW_AT_decl_line(0x2c3)
	.dwattr $C$DW$1732, DW_AT_decl_column(0x0d)

$C$DW$1733	.dwtag  DW_TAG_member
	.dwattr $C$DW$1733, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1733, DW_AT_name("perChannelPadConstTensorOffset")
	.dwattr $C$DW$1733, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$1733, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1733, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1733, DW_AT_decl_line(0x2c6)
	.dwattr $C$DW$1733, DW_AT_decl_column(0x0d)


$C$DW$1734	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1734, DW_AT_name("operator =")
	.dwattr $C$DW$1734, DW_AT_declaration
	.dwattr $C$DW$1734, DW_AT_linkage_name("_ZN22sTIDL_PadLayerParams_taSERKS_")
	.dwattr $C$DW$1734, DW_AT_type(*$C$DW$T$898)
	.dwattr $C$DW$1734, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1735	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1735, DW_AT_type(*$C$DW$T$900)

	.dwendtag $C$DW$1734


$C$DW$1736	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1736, DW_AT_name("operator =")
	.dwattr $C$DW$1736, DW_AT_declaration
	.dwattr $C$DW$1736, DW_AT_linkage_name("_ZN22sTIDL_PadLayerParams_taSEOS_")
	.dwattr $C$DW$1736, DW_AT_type(*$C$DW$T$898)
	.dwattr $C$DW$1736, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1737	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1737, DW_AT_type(*$C$DW$T$898)

	.dwendtag $C$DW$1736

	.dwattr $C$DW$T$903, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$903, DW_AT_decl_line(0x2b7)
	.dwattr $C$DW$T$903, DW_AT_decl_column(0x10)
	.dwendtag $C$DW$T$903

	.dwendtag $C$DW$TU$903


$C$DW$TU$898	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$898
$C$DW$T$898	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$898, DW_AT_type(*$C$DW$T$903)
	.dwattr $C$DW$T$898, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$898


$C$DW$TU$901	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$901

$C$DW$T$901	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$901, DW_AT_type(*$C$DW$T$898)
$C$DW$1738	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1738, DW_AT_type(*$C$DW$T$900)

	.dwendtag $C$DW$T$901

	.dwendtag $C$DW$TU$901


$C$DW$TU$902	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$902

$C$DW$T$902	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$902, DW_AT_type(*$C$DW$T$898)
$C$DW$1739	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1739, DW_AT_type(*$C$DW$T$898)

	.dwendtag $C$DW$T$902

	.dwendtag $C$DW$TU$902


$C$DW$TU$841	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$841
$C$DW$T$841	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$841, DW_AT_name("sTIDL_PadLayerParams_t")
	.dwattr $C$DW$T$841, DW_AT_type(*$C$DW$T$903)
	.dwattr $C$DW$T$841, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$841, DW_AT_decl_line(0x2c7)
	.dwattr $C$DW$T$841, DW_AT_decl_column(0x02)

	.dwendtag $C$DW$TU$841


$C$DW$TU$899	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$899
$C$DW$T$899	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$899, DW_AT_type(*$C$DW$T$903)

	.dwendtag $C$DW$TU$899


$C$DW$TU$900	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$900
$C$DW$T$900	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$900, DW_AT_type(*$C$DW$T$899)
	.dwattr $C$DW$T$900, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$900


$C$DW$TU$910	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$910

$C$DW$T$910	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$910, DW_AT_name("sTIDL_PoolingParams_t")
	.dwattr $C$DW$T$910, DW_AT_byte_size(0x2c)
$C$DW$1740	.dwtag  DW_TAG_member
	.dwattr $C$DW$1740, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1740, DW_AT_name("numChannels")
	.dwattr $C$DW$1740, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1740, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1740, DW_AT_decl_line(0x387)
	.dwattr $C$DW$1740, DW_AT_decl_column(0x0d)

$C$DW$1741	.dwtag  DW_TAG_member
	.dwattr $C$DW$1741, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1741, DW_AT_name("poolingType")
	.dwattr $C$DW$1741, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$1741, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1741, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1741, DW_AT_decl_line(0x389)
	.dwattr $C$DW$1741, DW_AT_decl_column(0x0d)

$C$DW$1742	.dwtag  DW_TAG_member
	.dwattr $C$DW$1742, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1742, DW_AT_name("kernelW")
	.dwattr $C$DW$1742, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$1742, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1742, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1742, DW_AT_decl_line(0x38b)
	.dwattr $C$DW$1742, DW_AT_decl_column(0x0d)

$C$DW$1743	.dwtag  DW_TAG_member
	.dwattr $C$DW$1743, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1743, DW_AT_name("kernelH")
	.dwattr $C$DW$1743, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$1743, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1743, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1743, DW_AT_decl_line(0x38d)
	.dwattr $C$DW$1743, DW_AT_decl_column(0x0d)

$C$DW$1744	.dwtag  DW_TAG_member
	.dwattr $C$DW$1744, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1744, DW_AT_name("strideW")
	.dwattr $C$DW$1744, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$1744, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1744, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1744, DW_AT_decl_line(0x38f)
	.dwattr $C$DW$1744, DW_AT_decl_column(0x0d)

$C$DW$1745	.dwtag  DW_TAG_member
	.dwattr $C$DW$1745, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1745, DW_AT_name("strideH")
	.dwattr $C$DW$1745, DW_AT_data_member_location[DW_OP_plus_uconst 0x14]
	.dwattr $C$DW$1745, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1745, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1745, DW_AT_decl_line(0x391)
	.dwattr $C$DW$1745, DW_AT_decl_column(0x0d)

$C$DW$1746	.dwtag  DW_TAG_member
	.dwattr $C$DW$1746, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1746, DW_AT_name("padW")
	.dwattr $C$DW$1746, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$1746, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1746, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1746, DW_AT_decl_line(0x393)
	.dwattr $C$DW$1746, DW_AT_decl_column(0x0d)

$C$DW$1747	.dwtag  DW_TAG_member
	.dwattr $C$DW$1747, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1747, DW_AT_name("padH")
	.dwattr $C$DW$1747, DW_AT_data_member_location[DW_OP_plus_uconst 0x1c]
	.dwattr $C$DW$1747, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1747, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1747, DW_AT_decl_line(0x395)
	.dwattr $C$DW$1747, DW_AT_decl_column(0x0d)

$C$DW$1748	.dwtag  DW_TAG_member
	.dwattr $C$DW$1748, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1748, DW_AT_name("inDataQ")
	.dwattr $C$DW$1748, DW_AT_data_member_location[DW_OP_plus_uconst 0x20]
	.dwattr $C$DW$1748, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1748, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1748, DW_AT_decl_line(0x397)
	.dwattr $C$DW$1748, DW_AT_decl_column(0x0d)

$C$DW$1749	.dwtag  DW_TAG_member
	.dwattr $C$DW$1749, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1749, DW_AT_name("outDataQ")
	.dwattr $C$DW$1749, DW_AT_data_member_location[DW_OP_plus_uconst 0x24]
	.dwattr $C$DW$1749, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1749, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1749, DW_AT_decl_line(0x399)
	.dwattr $C$DW$1749, DW_AT_decl_column(0x0d)

$C$DW$1750	.dwtag  DW_TAG_member
	.dwattr $C$DW$1750, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1750, DW_AT_name("useCeil")
	.dwattr $C$DW$1750, DW_AT_data_member_location[DW_OP_plus_uconst 0x28]
	.dwattr $C$DW$1750, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1750, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1750, DW_AT_decl_line(0x39b)
	.dwattr $C$DW$1750, DW_AT_decl_column(0x0d)


$C$DW$1751	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1751, DW_AT_name("operator =")
	.dwattr $C$DW$1751, DW_AT_declaration
	.dwattr $C$DW$1751, DW_AT_linkage_name("_ZN21sTIDL_PoolingParams_taSERKS_")
	.dwattr $C$DW$1751, DW_AT_type(*$C$DW$T$905)
	.dwattr $C$DW$1751, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1752	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1752, DW_AT_type(*$C$DW$T$907)

	.dwendtag $C$DW$1751


$C$DW$1753	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1753, DW_AT_name("operator =")
	.dwattr $C$DW$1753, DW_AT_declaration
	.dwattr $C$DW$1753, DW_AT_linkage_name("_ZN21sTIDL_PoolingParams_taSEOS_")
	.dwattr $C$DW$1753, DW_AT_type(*$C$DW$T$905)
	.dwattr $C$DW$1753, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1754	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1754, DW_AT_type(*$C$DW$T$905)

	.dwendtag $C$DW$1753

	.dwattr $C$DW$T$910, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$910, DW_AT_decl_line(0x385)
	.dwattr $C$DW$T$910, DW_AT_decl_column(0x10)
	.dwendtag $C$DW$T$910

	.dwendtag $C$DW$TU$910


$C$DW$TU$905	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$905
$C$DW$T$905	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$905, DW_AT_type(*$C$DW$T$910)
	.dwattr $C$DW$T$905, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$905


$C$DW$TU$908	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$908

$C$DW$T$908	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$908, DW_AT_type(*$C$DW$T$905)
$C$DW$1755	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1755, DW_AT_type(*$C$DW$T$907)

	.dwendtag $C$DW$T$908

	.dwendtag $C$DW$TU$908


$C$DW$TU$909	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$909

$C$DW$T$909	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$909, DW_AT_type(*$C$DW$T$905)
$C$DW$1756	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1756, DW_AT_type(*$C$DW$T$905)

	.dwendtag $C$DW$T$909

	.dwendtag $C$DW$TU$909


$C$DW$TU$749	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$749
$C$DW$T$749	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$749, DW_AT_name("sTIDL_PoolingParams_t")
	.dwattr $C$DW$T$749, DW_AT_type(*$C$DW$T$910)
	.dwattr $C$DW$T$749, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$749, DW_AT_decl_line(0x39c)
	.dwattr $C$DW$T$749, DW_AT_decl_column(0x02)

	.dwendtag $C$DW$TU$749


$C$DW$TU$906	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$906
$C$DW$T$906	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$906, DW_AT_type(*$C$DW$T$910)

	.dwendtag $C$DW$TU$906


$C$DW$TU$907	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$907
$C$DW$T$907	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$907, DW_AT_type(*$C$DW$T$906)
	.dwattr $C$DW$T$907, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$907


$C$DW$TU$918	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$918

$C$DW$T$918	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$918, DW_AT_name("sTIDL_ResizeLayerParams_t")
	.dwattr $C$DW$T$918, DW_AT_byte_size(0x14)
$C$DW$1757	.dwtag  DW_TAG_member
	.dwattr $C$DW$1757, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1757, DW_AT_name("mode")
	.dwattr $C$DW$1757, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1757, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1757, DW_AT_decl_line(0x2e8)
	.dwattr $C$DW$1757, DW_AT_decl_column(0x0d)

$C$DW$1758	.dwtag  DW_TAG_member
	.dwattr $C$DW$1758, DW_AT_type(*$C$DW$T$912)
	.dwattr $C$DW$1758, DW_AT_name("resizeRatio")
	.dwattr $C$DW$1758, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$1758, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1758, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1758, DW_AT_decl_line(0x2ea)
	.dwattr $C$DW$1758, DW_AT_decl_column(0x14)


$C$DW$1759	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1759, DW_AT_name("operator =")
	.dwattr $C$DW$1759, DW_AT_declaration
	.dwattr $C$DW$1759, DW_AT_linkage_name("_ZN25sTIDL_ResizeLayerParams_taSERKS_")
	.dwattr $C$DW$1759, DW_AT_type(*$C$DW$T$913)
	.dwattr $C$DW$1759, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1760	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1760, DW_AT_type(*$C$DW$T$915)

	.dwendtag $C$DW$1759


$C$DW$1761	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1761, DW_AT_name("operator =")
	.dwattr $C$DW$1761, DW_AT_declaration
	.dwattr $C$DW$1761, DW_AT_linkage_name("_ZN25sTIDL_ResizeLayerParams_taSEOS_")
	.dwattr $C$DW$1761, DW_AT_type(*$C$DW$T$913)
	.dwattr $C$DW$1761, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1762	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1762, DW_AT_type(*$C$DW$T$913)

	.dwendtag $C$DW$1761

	.dwattr $C$DW$T$918, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$918, DW_AT_decl_line(0x2e6)
	.dwattr $C$DW$T$918, DW_AT_decl_column(0x10)
	.dwendtag $C$DW$T$918

	.dwendtag $C$DW$TU$918


$C$DW$TU$913	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$913
$C$DW$T$913	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$913, DW_AT_type(*$C$DW$T$918)
	.dwattr $C$DW$T$913, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$913


$C$DW$TU$916	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$916

$C$DW$T$916	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$916, DW_AT_type(*$C$DW$T$913)
$C$DW$1763	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1763, DW_AT_type(*$C$DW$T$915)

	.dwendtag $C$DW$T$916

	.dwendtag $C$DW$TU$916


$C$DW$TU$917	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$917

$C$DW$T$917	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$917, DW_AT_type(*$C$DW$T$913)
$C$DW$1764	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1764, DW_AT_type(*$C$DW$T$913)

	.dwendtag $C$DW$T$917

	.dwendtag $C$DW$TU$917


$C$DW$TU$838	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$838
$C$DW$T$838	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$838, DW_AT_name("sTIDL_ResizeLayerParams_t")
	.dwattr $C$DW$T$838, DW_AT_type(*$C$DW$T$918)
	.dwattr $C$DW$T$838, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$838, DW_AT_decl_line(0x2eb)
	.dwattr $C$DW$T$838, DW_AT_decl_column(0x02)

	.dwendtag $C$DW$TU$838


$C$DW$TU$914	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$914
$C$DW$T$914	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$914, DW_AT_type(*$C$DW$T$918)

	.dwendtag $C$DW$TU$914


$C$DW$TU$915	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$915
$C$DW$T$915	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$915, DW_AT_type(*$C$DW$T$914)
	.dwattr $C$DW$T$915, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$915


$C$DW$TU$926	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$926

$C$DW$T$926	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$926, DW_AT_name("sTIDL_RoiPoolingLayerParams_t")
	.dwattr $C$DW$T$926, DW_AT_byte_size(0x0c)
$C$DW$1765	.dwtag  DW_TAG_member
	.dwattr $C$DW$1765, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1765, DW_AT_name("poolingType")
	.dwattr $C$DW$1765, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1765, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1765, DW_AT_decl_line(0x31f)
	.dwattr $C$DW$1765, DW_AT_decl_column(0x0d)

$C$DW$1766	.dwtag  DW_TAG_member
	.dwattr $C$DW$1766, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1766, DW_AT_name("imWidth")
	.dwattr $C$DW$1766, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$1766, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1766, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1766, DW_AT_decl_line(0x321)
	.dwattr $C$DW$1766, DW_AT_decl_column(0x0b)

$C$DW$1767	.dwtag  DW_TAG_member
	.dwattr $C$DW$1767, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1767, DW_AT_name("imHeight")
	.dwattr $C$DW$1767, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$1767, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1767, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1767, DW_AT_decl_line(0x323)
	.dwattr $C$DW$1767, DW_AT_decl_column(0x0b)


$C$DW$1768	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1768, DW_AT_name("operator =")
	.dwattr $C$DW$1768, DW_AT_declaration
	.dwattr $C$DW$1768, DW_AT_linkage_name("_ZN29sTIDL_RoiPoolingLayerParams_taSERKS_")
	.dwattr $C$DW$1768, DW_AT_type(*$C$DW$T$921)
	.dwattr $C$DW$1768, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1769	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1769, DW_AT_type(*$C$DW$T$923)

	.dwendtag $C$DW$1768


$C$DW$1770	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1770, DW_AT_name("operator =")
	.dwattr $C$DW$1770, DW_AT_declaration
	.dwattr $C$DW$1770, DW_AT_linkage_name("_ZN29sTIDL_RoiPoolingLayerParams_taSEOS_")
	.dwattr $C$DW$1770, DW_AT_type(*$C$DW$T$921)
	.dwattr $C$DW$1770, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1771	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1771, DW_AT_type(*$C$DW$T$921)

	.dwendtag $C$DW$1770

	.dwattr $C$DW$T$926, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$926, DW_AT_decl_line(0x31d)
	.dwattr $C$DW$T$926, DW_AT_decl_column(0x10)
	.dwendtag $C$DW$T$926

	.dwendtag $C$DW$TU$926


$C$DW$TU$921	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$921
$C$DW$T$921	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$921, DW_AT_type(*$C$DW$T$926)
	.dwattr $C$DW$T$921, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$921


$C$DW$TU$924	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$924

$C$DW$T$924	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$924, DW_AT_type(*$C$DW$T$921)
$C$DW$1772	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1772, DW_AT_type(*$C$DW$T$923)

	.dwendtag $C$DW$T$924

	.dwendtag $C$DW$TU$924


$C$DW$TU$925	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$925

$C$DW$T$925	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$925, DW_AT_type(*$C$DW$T$921)
$C$DW$1773	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1773, DW_AT_type(*$C$DW$T$921)

	.dwendtag $C$DW$T$925

	.dwendtag $C$DW$TU$925


$C$DW$TU$839	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$839
$C$DW$T$839	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$839, DW_AT_name("sTIDL_RoiPoolingLayerParams_t")
	.dwattr $C$DW$T$839, DW_AT_type(*$C$DW$T$926)
	.dwattr $C$DW$T$839, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$839, DW_AT_decl_line(0x324)
	.dwattr $C$DW$T$839, DW_AT_decl_column(0x02)

	.dwendtag $C$DW$TU$839


$C$DW$TU$922	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$922
$C$DW$T$922	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$922, DW_AT_type(*$C$DW$T$926)

	.dwendtag $C$DW$TU$922


$C$DW$TU$923	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$923
$C$DW$T$923	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$923, DW_AT_type(*$C$DW$T$922)
	.dwattr $C$DW$T$923, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$923


$C$DW$TU$933	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$933

$C$DW$T$933	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$933, DW_AT_name("sTIDL_ShuffleLayerParams_t")
	.dwattr $C$DW$T$933, DW_AT_byte_size(0x08)
$C$DW$1774	.dwtag  DW_TAG_member
	.dwattr $C$DW$1774, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1774, DW_AT_name("numGroups")
	.dwattr $C$DW$1774, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1774, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1774, DW_AT_decl_line(0x2a3)
	.dwattr $C$DW$1774, DW_AT_decl_column(0x0d)

$C$DW$1775	.dwtag  DW_TAG_member
	.dwattr $C$DW$1775, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1775, DW_AT_name("resvd")
	.dwattr $C$DW$1775, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$1775, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1775, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1775, DW_AT_decl_line(0x2a5)
	.dwattr $C$DW$1775, DW_AT_decl_column(0x0d)


$C$DW$1776	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1776, DW_AT_name("operator =")
	.dwattr $C$DW$1776, DW_AT_declaration
	.dwattr $C$DW$1776, DW_AT_linkage_name("_ZN26sTIDL_ShuffleLayerParams_taSERKS_")
	.dwattr $C$DW$1776, DW_AT_type(*$C$DW$T$928)
	.dwattr $C$DW$1776, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1777	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1777, DW_AT_type(*$C$DW$T$930)

	.dwendtag $C$DW$1776


$C$DW$1778	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1778, DW_AT_name("operator =")
	.dwattr $C$DW$1778, DW_AT_declaration
	.dwattr $C$DW$1778, DW_AT_linkage_name("_ZN26sTIDL_ShuffleLayerParams_taSEOS_")
	.dwattr $C$DW$1778, DW_AT_type(*$C$DW$T$928)
	.dwattr $C$DW$1778, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1779	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1779, DW_AT_type(*$C$DW$T$928)

	.dwendtag $C$DW$1778

	.dwattr $C$DW$T$933, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$933, DW_AT_decl_line(0x2a1)
	.dwattr $C$DW$T$933, DW_AT_decl_column(0x10)
	.dwendtag $C$DW$T$933

	.dwendtag $C$DW$TU$933


$C$DW$TU$928	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$928
$C$DW$T$928	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$928, DW_AT_type(*$C$DW$T$933)
	.dwattr $C$DW$T$928, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$928


$C$DW$TU$931	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$931

$C$DW$T$931	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$931, DW_AT_type(*$C$DW$T$928)
$C$DW$1780	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1780, DW_AT_type(*$C$DW$T$930)

	.dwendtag $C$DW$T$931

	.dwendtag $C$DW$TU$931


$C$DW$TU$932	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$932

$C$DW$T$932	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$932, DW_AT_type(*$C$DW$T$928)
$C$DW$1781	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1781, DW_AT_type(*$C$DW$T$928)

	.dwendtag $C$DW$T$932

	.dwendtag $C$DW$TU$932


$C$DW$TU$836	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$836
$C$DW$T$836	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$836, DW_AT_name("sTIDL_ShuffleLayerParams_t")
	.dwattr $C$DW$T$836, DW_AT_type(*$C$DW$T$933)
	.dwattr $C$DW$T$836, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$836, DW_AT_decl_line(0x2a6)
	.dwattr $C$DW$T$836, DW_AT_decl_column(0x02)

	.dwendtag $C$DW$TU$836


$C$DW$TU$929	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$929
$C$DW$T$929	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$929, DW_AT_type(*$C$DW$T$933)

	.dwendtag $C$DW$TU$929


$C$DW$TU$930	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$930
$C$DW$T$930	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$930, DW_AT_type(*$C$DW$T$929)
	.dwattr $C$DW$T$930, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$930


$C$DW$TU$941	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$941

$C$DW$T$941	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$941, DW_AT_name("sTIDL_SliceLayerParams_t")
	.dwattr $C$DW$T$941, DW_AT_byte_size(0x4c)
$C$DW$1782	.dwtag  DW_TAG_member
	.dwattr $C$DW$1782, DW_AT_type(*$C$DW$T$935)
	.dwattr $C$DW$1782, DW_AT_name("slicePoints")
	.dwattr $C$DW$1782, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1782, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1782, DW_AT_decl_line(0x32e)
	.dwattr $C$DW$1782, DW_AT_decl_column(0x0d)

$C$DW$1783	.dwtag  DW_TAG_member
	.dwattr $C$DW$1783, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1783, DW_AT_name("axis")
	.dwattr $C$DW$1783, DW_AT_data_member_location[DW_OP_plus_uconst 0x44]
	.dwattr $C$DW$1783, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1783, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1783, DW_AT_decl_line(0x330)
	.dwattr $C$DW$1783, DW_AT_decl_column(0x0c)

$C$DW$1784	.dwtag  DW_TAG_member
	.dwattr $C$DW$1784, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1784, DW_AT_name("stride")
	.dwattr $C$DW$1784, DW_AT_data_member_location[DW_OP_plus_uconst 0x48]
	.dwattr $C$DW$1784, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1784, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1784, DW_AT_decl_line(0x332)
	.dwattr $C$DW$1784, DW_AT_decl_column(0x0c)


$C$DW$1785	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1785, DW_AT_name("operator =")
	.dwattr $C$DW$1785, DW_AT_declaration
	.dwattr $C$DW$1785, DW_AT_linkage_name("_ZN24sTIDL_SliceLayerParams_taSERKS_")
	.dwattr $C$DW$1785, DW_AT_type(*$C$DW$T$936)
	.dwattr $C$DW$1785, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1786	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1786, DW_AT_type(*$C$DW$T$938)

	.dwendtag $C$DW$1785


$C$DW$1787	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1787, DW_AT_name("operator =")
	.dwattr $C$DW$1787, DW_AT_declaration
	.dwattr $C$DW$1787, DW_AT_linkage_name("_ZN24sTIDL_SliceLayerParams_taSEOS_")
	.dwattr $C$DW$1787, DW_AT_type(*$C$DW$T$936)
	.dwattr $C$DW$1787, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1788	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1788, DW_AT_type(*$C$DW$T$936)

	.dwendtag $C$DW$1787

	.dwattr $C$DW$T$941, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$941, DW_AT_decl_line(0x32c)
	.dwattr $C$DW$T$941, DW_AT_decl_column(0x10)
	.dwendtag $C$DW$T$941

	.dwendtag $C$DW$TU$941


$C$DW$TU$936	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$936
$C$DW$T$936	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$936, DW_AT_type(*$C$DW$T$941)
	.dwattr $C$DW$T$936, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$936


$C$DW$TU$939	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$939

$C$DW$T$939	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$939, DW_AT_type(*$C$DW$T$936)
$C$DW$1789	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1789, DW_AT_type(*$C$DW$T$938)

	.dwendtag $C$DW$T$939

	.dwendtag $C$DW$TU$939


$C$DW$TU$940	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$940

$C$DW$T$940	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$940, DW_AT_type(*$C$DW$T$936)
$C$DW$1790	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1790, DW_AT_type(*$C$DW$T$936)

	.dwendtag $C$DW$T$940

	.dwendtag $C$DW$TU$940


$C$DW$TU$837	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$837
$C$DW$T$837	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$837, DW_AT_name("sTIDL_SliceLayerParams_t")
	.dwattr $C$DW$T$837, DW_AT_type(*$C$DW$T$941)
	.dwattr $C$DW$T$837, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$837, DW_AT_decl_line(0x333)
	.dwattr $C$DW$T$837, DW_AT_decl_column(0x02)

	.dwendtag $C$DW$TU$837


$C$DW$TU$937	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$937
$C$DW$T$937	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$937, DW_AT_type(*$C$DW$T$941)

	.dwendtag $C$DW$TU$937


$C$DW$TU$938	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$938
$C$DW$T$938	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$938, DW_AT_type(*$C$DW$T$937)
	.dwattr $C$DW$T$938, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$938


$C$DW$TU$949	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$949

$C$DW$T$949	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$949, DW_AT_name("sTIDL_SoftMaxParams_t")
	.dwattr $C$DW$T$949, DW_AT_byte_size(0x0c)
$C$DW$1791	.dwtag  DW_TAG_member
	.dwattr $C$DW$1791, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1791, DW_AT_name("numChannels")
	.dwattr $C$DW$1791, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1791, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1791, DW_AT_decl_line(0x527)
	.dwattr $C$DW$1791, DW_AT_decl_column(0x0d)

$C$DW$1792	.dwtag  DW_TAG_member
	.dwattr $C$DW$1792, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1792, DW_AT_name("inDataQ")
	.dwattr $C$DW$1792, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$1792, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1792, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1792, DW_AT_decl_line(0x529)
	.dwattr $C$DW$1792, DW_AT_decl_column(0x0d)

$C$DW$1793	.dwtag  DW_TAG_member
	.dwattr $C$DW$1793, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1793, DW_AT_name("outDataQ")
	.dwattr $C$DW$1793, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$1793, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1793, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1793, DW_AT_decl_line(0x52b)
	.dwattr $C$DW$1793, DW_AT_decl_column(0x0d)


$C$DW$1794	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1794, DW_AT_name("operator =")
	.dwattr $C$DW$1794, DW_AT_declaration
	.dwattr $C$DW$1794, DW_AT_linkage_name("_ZN21sTIDL_SoftMaxParams_taSERKS_")
	.dwattr $C$DW$1794, DW_AT_type(*$C$DW$T$944)
	.dwattr $C$DW$1794, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1795	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1795, DW_AT_type(*$C$DW$T$946)

	.dwendtag $C$DW$1794


$C$DW$1796	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1796, DW_AT_name("operator =")
	.dwattr $C$DW$1796, DW_AT_declaration
	.dwattr $C$DW$1796, DW_AT_linkage_name("_ZN21sTIDL_SoftMaxParams_taSEOS_")
	.dwattr $C$DW$1796, DW_AT_type(*$C$DW$T$944)
	.dwattr $C$DW$1796, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1797	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1797, DW_AT_type(*$C$DW$T$944)

	.dwendtag $C$DW$1796

	.dwattr $C$DW$T$949, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$949, DW_AT_decl_line(0x525)
	.dwattr $C$DW$T$949, DW_AT_decl_column(0x10)
	.dwendtag $C$DW$T$949

	.dwendtag $C$DW$TU$949


$C$DW$TU$944	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$944
$C$DW$T$944	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$944, DW_AT_type(*$C$DW$T$949)
	.dwattr $C$DW$T$944, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$944


$C$DW$TU$947	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$947

$C$DW$T$947	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$947, DW_AT_type(*$C$DW$T$944)
$C$DW$1798	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1798, DW_AT_type(*$C$DW$T$946)

	.dwendtag $C$DW$T$947

	.dwendtag $C$DW$TU$947


$C$DW$TU$948	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$948

$C$DW$T$948	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$948, DW_AT_type(*$C$DW$T$944)
$C$DW$1799	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1799, DW_AT_type(*$C$DW$T$944)

	.dwendtag $C$DW$T$948

	.dwendtag $C$DW$TU$948


$C$DW$TU$830	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$830
$C$DW$T$830	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$830, DW_AT_name("sTIDL_SoftMaxParams_t")
	.dwattr $C$DW$T$830, DW_AT_type(*$C$DW$T$949)
	.dwattr $C$DW$T$830, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$830, DW_AT_decl_line(0x52c)
	.dwattr $C$DW$T$830, DW_AT_decl_column(0x02)

	.dwendtag $C$DW$TU$830


$C$DW$TU$945	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$945
$C$DW$T$945	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$945, DW_AT_type(*$C$DW$T$949)

	.dwendtag $C$DW$TU$945


$C$DW$TU$946	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$946
$C$DW$T$946	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$946, DW_AT_type(*$C$DW$T$945)
	.dwattr $C$DW$T$946, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$946


$C$DW$TU$956	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$956

$C$DW$T$956	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$956, DW_AT_name("sTIDL_acrossLayerShare")
	.dwattr $C$DW$T$956, DW_AT_byte_size(0x60)
$C$DW$1800	.dwtag  DW_TAG_member
	.dwattr $C$DW$1800, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1800, DW_AT_name("isWtDmaPending")
	.dwattr $C$DW$1800, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1800, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1800, DW_AT_decl_line(0x3ca)
	.dwattr $C$DW$1800, DW_AT_decl_column(0x0e)

$C$DW$1801	.dwtag  DW_TAG_member
	.dwattr $C$DW$1801, DW_AT_type(*$C$DW$T$364)
	.dwattr $C$DW$1801, DW_AT_name("wtMemBasePtr")
	.dwattr $C$DW$1801, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$1801, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1801, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1801, DW_AT_decl_line(0x3cb)
	.dwattr $C$DW$1801, DW_AT_decl_column(0x11)

$C$DW$1802	.dwtag  DW_TAG_member
	.dwattr $C$DW$1802, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1802, DW_AT_name("currPtrOffset")
	.dwattr $C$DW$1802, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$1802, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1802, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1802, DW_AT_decl_line(0x3cc)
	.dwattr $C$DW$1802, DW_AT_decl_column(0x0f)

$C$DW$1803	.dwtag  DW_TAG_member
	.dwattr $C$DW$1803, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1803, DW_AT_name("lastPtrOffset")
	.dwattr $C$DW$1803, DW_AT_data_member_location[DW_OP_plus_uconst 0x14]
	.dwattr $C$DW$1803, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1803, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1803, DW_AT_decl_line(0x3cd)
	.dwattr $C$DW$1803, DW_AT_decl_column(0x0f)

$C$DW$1804	.dwtag  DW_TAG_member
	.dwattr $C$DW$1804, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1804, DW_AT_name("totStageMemAvail")
	.dwattr $C$DW$1804, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$1804, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1804, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1804, DW_AT_decl_line(0x3ce)
	.dwattr $C$DW$1804, DW_AT_decl_column(0x0f)

$C$DW$1805	.dwtag  DW_TAG_member
	.dwattr $C$DW$1805, DW_AT_type(*$C$DW$T$319)
	.dwattr $C$DW$1805, DW_AT_name("trMem")
	.dwattr $C$DW$1805, DW_AT_data_member_location[DW_OP_plus_uconst 0x1c]
	.dwattr $C$DW$1805, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1805, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1805, DW_AT_decl_line(0x3cf)
	.dwattr $C$DW$1805, DW_AT_decl_column(0x10)


$C$DW$1806	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1806, DW_AT_name("operator =")
	.dwattr $C$DW$1806, DW_AT_declaration
	.dwattr $C$DW$1806, DW_AT_linkage_name("_ZN22sTIDL_acrossLayerShareaSERKS_")
	.dwattr $C$DW$1806, DW_AT_type(*$C$DW$T$951)
	.dwattr $C$DW$1806, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1807	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1807, DW_AT_type(*$C$DW$T$953)

	.dwendtag $C$DW$1806


$C$DW$1808	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1808, DW_AT_name("operator =")
	.dwattr $C$DW$1808, DW_AT_declaration
	.dwattr $C$DW$1808, DW_AT_linkage_name("_ZN22sTIDL_acrossLayerShareaSEOS_")
	.dwattr $C$DW$1808, DW_AT_type(*$C$DW$T$951)
	.dwattr $C$DW$1808, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1809	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1809, DW_AT_type(*$C$DW$T$951)

	.dwendtag $C$DW$1808

	.dwattr $C$DW$T$956, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$T$956, DW_AT_decl_line(0x3c9)
	.dwattr $C$DW$T$956, DW_AT_decl_column(0x10)
	.dwendtag $C$DW$T$956

	.dwendtag $C$DW$TU$956


$C$DW$TU$951	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$951
$C$DW$T$951	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$951, DW_AT_type(*$C$DW$T$956)
	.dwattr $C$DW$T$951, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$951


$C$DW$TU$954	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$954

$C$DW$T$954	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$954, DW_AT_type(*$C$DW$T$951)
$C$DW$1810	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1810, DW_AT_type(*$C$DW$T$953)

	.dwendtag $C$DW$T$954

	.dwendtag $C$DW$TU$954


$C$DW$TU$955	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$955

$C$DW$T$955	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$955, DW_AT_type(*$C$DW$T$951)
$C$DW$1811	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1811, DW_AT_type(*$C$DW$T$951)

	.dwendtag $C$DW$T$955

	.dwendtag $C$DW$TU$955


$C$DW$TU$370	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$370
$C$DW$T$370	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$370, DW_AT_name("sTIDL_acrossLayerShare")
	.dwattr $C$DW$T$370, DW_AT_type(*$C$DW$T$956)
	.dwattr $C$DW$T$370, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$T$370, DW_AT_decl_line(0x3d0)
	.dwattr $C$DW$T$370, DW_AT_decl_column(0x04)

	.dwendtag $C$DW$TU$370


$C$DW$TU$952	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$952
$C$DW$T$952	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$952, DW_AT_type(*$C$DW$T$956)

	.dwendtag $C$DW$TU$952


$C$DW$TU$953	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$953
$C$DW$T$953	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$953, DW_AT_type(*$C$DW$T$952)
	.dwattr $C$DW$T$953, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$953


$C$DW$TU$964	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$964

$C$DW$T$964	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$964, DW_AT_name("sTIDL_commonLayerMetaData_t")
	.dwattr $C$DW$T$964, DW_AT_byte_size(0x88)
$C$DW$1812	.dwtag  DW_TAG_member
	.dwattr $C$DW$1812, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1812, DW_AT_name("totalOps")
	.dwattr $C$DW$1812, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1812, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1812, DW_AT_decl_line(0x304)
	.dwattr $C$DW$1812, DW_AT_decl_column(0x0b)

$C$DW$1813	.dwtag  DW_TAG_member
	.dwattr $C$DW$1813, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1813, DW_AT_name("actualOps")
	.dwattr $C$DW$1813, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$1813, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1813, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1813, DW_AT_decl_line(0x305)
	.dwattr $C$DW$1813, DW_AT_decl_column(0x0b)

$C$DW$1814	.dwtag  DW_TAG_member
	.dwattr $C$DW$1814, DW_AT_type(*$C$DW$T$958)
	.dwattr $C$DW$1814, DW_AT_name("profilePoint")
	.dwattr $C$DW$1814, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$1814, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1814, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1814, DW_AT_decl_line(0x306)
	.dwattr $C$DW$1814, DW_AT_decl_column(0x0c)


$C$DW$1815	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1815, DW_AT_name("operator =")
	.dwattr $C$DW$1815, DW_AT_declaration
	.dwattr $C$DW$1815, DW_AT_linkage_name("_ZN27sTIDL_commonLayerMetaData_taSERKS_")
	.dwattr $C$DW$1815, DW_AT_type(*$C$DW$T$959)
	.dwattr $C$DW$1815, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1816	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1816, DW_AT_type(*$C$DW$T$961)

	.dwendtag $C$DW$1815


$C$DW$1817	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1817, DW_AT_name("operator =")
	.dwattr $C$DW$1817, DW_AT_declaration
	.dwattr $C$DW$1817, DW_AT_linkage_name("_ZN27sTIDL_commonLayerMetaData_taSEOS_")
	.dwattr $C$DW$1817, DW_AT_type(*$C$DW$T$959)
	.dwattr $C$DW$1817, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1818	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1818, DW_AT_type(*$C$DW$T$959)

	.dwendtag $C$DW$1817

	.dwattr $C$DW$T$964, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$T$964, DW_AT_decl_line(0x303)
	.dwattr $C$DW$T$964, DW_AT_decl_column(0x10)
	.dwendtag $C$DW$T$964

	.dwendtag $C$DW$TU$964


$C$DW$TU$959	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$959
$C$DW$T$959	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$959, DW_AT_type(*$C$DW$T$964)
	.dwattr $C$DW$T$959, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$959


$C$DW$TU$962	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$962

$C$DW$T$962	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$962, DW_AT_type(*$C$DW$T$959)
$C$DW$1819	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1819, DW_AT_type(*$C$DW$T$961)

	.dwendtag $C$DW$T$962

	.dwendtag $C$DW$TU$962


$C$DW$TU$963	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$963

$C$DW$T$963	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$963, DW_AT_type(*$C$DW$T$959)
$C$DW$1820	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1820, DW_AT_type(*$C$DW$T$959)

	.dwendtag $C$DW$T$963

	.dwendtag $C$DW$TU$963


$C$DW$TU$694	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$694
$C$DW$T$694	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$694, DW_AT_name("sTIDL_commonLayerMetaData_t")
	.dwattr $C$DW$T$694, DW_AT_type(*$C$DW$T$964)
	.dwattr $C$DW$T$694, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$T$694, DW_AT_decl_line(0x307)
	.dwattr $C$DW$T$694, DW_AT_decl_column(0x02)

	.dwendtag $C$DW$TU$694


$C$DW$TU$960	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$960
$C$DW$T$960	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$960, DW_AT_type(*$C$DW$T$964)

	.dwendtag $C$DW$TU$960


$C$DW$TU$961	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$961
$C$DW$T$961	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$961, DW_AT_type(*$C$DW$T$960)
	.dwattr $C$DW$T$961, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$961


$C$DW$TU$971	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$971

$C$DW$T$971	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$971, DW_AT_name("sTIDL_dataConvertParams_t")
	.dwattr $C$DW$T$971, DW_AT_byte_size(0x0c)
$C$DW$1821	.dwtag  DW_TAG_member
	.dwattr $C$DW$1821, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1821, DW_AT_name("type")
	.dwattr $C$DW$1821, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1821, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1821, DW_AT_decl_line(0x548)
	.dwattr $C$DW$1821, DW_AT_decl_column(0x0b)

$C$DW$1822	.dwtag  DW_TAG_member
	.dwattr $C$DW$1822, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1822, DW_AT_name("layout")
	.dwattr $C$DW$1822, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$1822, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1822, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1822, DW_AT_decl_line(0x549)
	.dwattr $C$DW$1822, DW_AT_decl_column(0x0b)

$C$DW$1823	.dwtag  DW_TAG_member
	.dwattr $C$DW$1823, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1823, DW_AT_name("zeroPoint")
	.dwattr $C$DW$1823, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$1823, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1823, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1823, DW_AT_decl_line(0x54a)
	.dwattr $C$DW$1823, DW_AT_decl_column(0x0b)


$C$DW$1824	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1824, DW_AT_name("operator =")
	.dwattr $C$DW$1824, DW_AT_declaration
	.dwattr $C$DW$1824, DW_AT_linkage_name("_ZN25sTIDL_dataConvertParams_taSERKS_")
	.dwattr $C$DW$1824, DW_AT_type(*$C$DW$T$966)
	.dwattr $C$DW$1824, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1825	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1825, DW_AT_type(*$C$DW$T$968)

	.dwendtag $C$DW$1824


$C$DW$1826	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1826, DW_AT_name("operator =")
	.dwattr $C$DW$1826, DW_AT_declaration
	.dwattr $C$DW$1826, DW_AT_linkage_name("_ZN25sTIDL_dataConvertParams_taSEOS_")
	.dwattr $C$DW$1826, DW_AT_type(*$C$DW$T$966)
	.dwattr $C$DW$1826, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1827	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1827, DW_AT_type(*$C$DW$T$966)

	.dwendtag $C$DW$1826

	.dwattr $C$DW$T$971, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$971, DW_AT_decl_line(0x546)
	.dwattr $C$DW$T$971, DW_AT_decl_column(0x10)
	.dwendtag $C$DW$T$971

	.dwendtag $C$DW$TU$971


$C$DW$TU$966	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$966
$C$DW$T$966	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$966, DW_AT_type(*$C$DW$T$971)
	.dwattr $C$DW$T$966, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$966


$C$DW$TU$969	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$969

$C$DW$T$969	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$969, DW_AT_type(*$C$DW$T$966)
$C$DW$1828	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1828, DW_AT_type(*$C$DW$T$968)

	.dwendtag $C$DW$T$969

	.dwendtag $C$DW$TU$969


$C$DW$TU$970	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$970

$C$DW$T$970	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$970, DW_AT_type(*$C$DW$T$966)
$C$DW$1829	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1829, DW_AT_type(*$C$DW$T$966)

	.dwendtag $C$DW$T$970

	.dwendtag $C$DW$TU$970


$C$DW$TU$843	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$843
$C$DW$T$843	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$843, DW_AT_name("sTIDL_dataConvertParams_t")
	.dwattr $C$DW$T$843, DW_AT_type(*$C$DW$T$971)
	.dwattr $C$DW$T$843, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$843, DW_AT_decl_line(0x54b)
	.dwattr $C$DW$T$843, DW_AT_decl_column(0x02)

	.dwendtag $C$DW$TU$843


$C$DW$TU$967	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$967
$C$DW$T$967	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$967, DW_AT_type(*$C$DW$T$971)

	.dwendtag $C$DW$TU$967


$C$DW$TU$968	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$968
$C$DW$T$968	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$968, DW_AT_type(*$C$DW$T$967)
	.dwattr $C$DW$T$968, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$968


$C$DW$TU$978	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$978

$C$DW$T$978	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$978, DW_AT_name("sTIDL_odOutputReformatLayerParams_t")
	.dwattr $C$DW$T$978, DW_AT_byte_size(0x0c)
$C$DW$1830	.dwtag  DW_TAG_member
	.dwattr $C$DW$1830, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1830, DW_AT_name("layerType")
	.dwattr $C$DW$1830, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1830, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1830, DW_AT_decl_line(0x2d0)
	.dwattr $C$DW$1830, DW_AT_decl_column(0x0b)

$C$DW$1831	.dwtag  DW_TAG_member
	.dwattr $C$DW$1831, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1831, DW_AT_name("inWidthOdNetwork")
	.dwattr $C$DW$1831, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$1831, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1831, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1831, DW_AT_decl_line(0x2d1)
	.dwattr $C$DW$1831, DW_AT_decl_column(0x0b)

$C$DW$1832	.dwtag  DW_TAG_member
	.dwattr $C$DW$1832, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1832, DW_AT_name("inHeightOdNetwork")
	.dwattr $C$DW$1832, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$1832, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1832, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1832, DW_AT_decl_line(0x2d2)
	.dwattr $C$DW$1832, DW_AT_decl_column(0x0b)


$C$DW$1833	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1833, DW_AT_name("operator =")
	.dwattr $C$DW$1833, DW_AT_declaration
	.dwattr $C$DW$1833, DW_AT_linkage_name("_ZN35sTIDL_odOutputReformatLayerParams_taSERKS_")
	.dwattr $C$DW$1833, DW_AT_type(*$C$DW$T$973)
	.dwattr $C$DW$1833, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1834	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1834, DW_AT_type(*$C$DW$T$975)

	.dwendtag $C$DW$1833


$C$DW$1835	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1835, DW_AT_name("operator =")
	.dwattr $C$DW$1835, DW_AT_declaration
	.dwattr $C$DW$1835, DW_AT_linkage_name("_ZN35sTIDL_odOutputReformatLayerParams_taSEOS_")
	.dwattr $C$DW$1835, DW_AT_type(*$C$DW$T$973)
	.dwattr $C$DW$1835, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1836	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1836, DW_AT_type(*$C$DW$T$973)

	.dwendtag $C$DW$1835

	.dwattr $C$DW$T$978, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$978, DW_AT_decl_line(0x2ce)
	.dwattr $C$DW$T$978, DW_AT_decl_column(0x10)
	.dwendtag $C$DW$T$978

	.dwendtag $C$DW$TU$978


$C$DW$TU$973	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$973
$C$DW$T$973	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$973, DW_AT_type(*$C$DW$T$978)
	.dwattr $C$DW$T$973, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$973


$C$DW$TU$976	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$976

$C$DW$T$976	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$976, DW_AT_type(*$C$DW$T$973)
$C$DW$1837	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1837, DW_AT_type(*$C$DW$T$975)

	.dwendtag $C$DW$T$976

	.dwendtag $C$DW$TU$976


$C$DW$TU$977	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$977

$C$DW$T$977	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$977, DW_AT_type(*$C$DW$T$973)
$C$DW$1838	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1838, DW_AT_type(*$C$DW$T$973)

	.dwendtag $C$DW$T$977

	.dwendtag $C$DW$TU$977


$C$DW$TU$842	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$842
$C$DW$T$842	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$842, DW_AT_name("sTIDL_odOutputReformatLayerParams_t")
	.dwattr $C$DW$T$842, DW_AT_type(*$C$DW$T$978)
	.dwattr $C$DW$T$842, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$842, DW_AT_decl_line(0x2d3)
	.dwattr $C$DW$T$842, DW_AT_decl_column(0x02)

	.dwendtag $C$DW$TU$842


$C$DW$TU$974	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$974
$C$DW$T$974	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$974, DW_AT_type(*$C$DW$T$978)

	.dwendtag $C$DW$TU$974


$C$DW$TU$975	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$975
$C$DW$T$975	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$975, DW_AT_type(*$C$DW$T$974)
	.dwattr $C$DW$T$975, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$975


$C$DW$TU$985	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$985

$C$DW$T$985	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$985, DW_AT_name("sTIDL_sysMemHandle_t")
	.dwattr $C$DW$T$985, DW_AT_byte_size(0x10)
$C$DW$1839	.dwtag  DW_TAG_member
	.dwattr $C$DW$1839, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$1839, DW_AT_name("base")
	.dwattr $C$DW$1839, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1839, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1839, DW_AT_decl_line(0x25c)
	.dwattr $C$DW$1839, DW_AT_decl_column(0x0c)

$C$DW$1840	.dwtag  DW_TAG_member
	.dwattr $C$DW$1840, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1840, DW_AT_name("size")
	.dwattr $C$DW$1840, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$1840, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1840, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1840, DW_AT_decl_line(0x25d)
	.dwattr $C$DW$1840, DW_AT_decl_column(0x0c)

$C$DW$1841	.dwtag  DW_TAG_member
	.dwattr $C$DW$1841, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$1841, DW_AT_name("offset")
	.dwattr $C$DW$1841, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$1841, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1841, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$1841, DW_AT_decl_line(0x25e)
	.dwattr $C$DW$1841, DW_AT_decl_column(0x0c)


$C$DW$1842	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1842, DW_AT_name("operator =")
	.dwattr $C$DW$1842, DW_AT_declaration
	.dwattr $C$DW$1842, DW_AT_linkage_name("_ZN20sTIDL_sysMemHandle_taSERKS_")
	.dwattr $C$DW$1842, DW_AT_type(*$C$DW$T$980)
	.dwattr $C$DW$1842, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1843	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1843, DW_AT_type(*$C$DW$T$982)

	.dwendtag $C$DW$1842


$C$DW$1844	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1844, DW_AT_name("operator =")
	.dwattr $C$DW$1844, DW_AT_declaration
	.dwattr $C$DW$1844, DW_AT_linkage_name("_ZN20sTIDL_sysMemHandle_taSEOS_")
	.dwattr $C$DW$1844, DW_AT_type(*$C$DW$T$980)
	.dwattr $C$DW$1844, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1845	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1845, DW_AT_type(*$C$DW$T$980)

	.dwendtag $C$DW$1844

	.dwattr $C$DW$T$985, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$985, DW_AT_decl_line(0x25b)
	.dwattr $C$DW$T$985, DW_AT_decl_column(0x01)
	.dwendtag $C$DW$T$985

	.dwendtag $C$DW$TU$985


$C$DW$TU$980	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$980
$C$DW$T$980	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$980, DW_AT_type(*$C$DW$T$985)
	.dwattr $C$DW$T$980, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$980


$C$DW$TU$983	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$983

$C$DW$T$983	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$983, DW_AT_type(*$C$DW$T$980)
$C$DW$1846	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1846, DW_AT_type(*$C$DW$T$982)

	.dwendtag $C$DW$T$983

	.dwendtag $C$DW$TU$983


$C$DW$TU$984	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$984

$C$DW$T$984	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$984, DW_AT_type(*$C$DW$T$980)
$C$DW$1847	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1847, DW_AT_type(*$C$DW$T$980)

	.dwendtag $C$DW$T$984

	.dwendtag $C$DW$TU$984


$C$DW$TU$285	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$285
$C$DW$T$285	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$285, DW_AT_name("sTIDL_sysMemHandle_t")
	.dwattr $C$DW$T$285, DW_AT_type(*$C$DW$T$985)
	.dwattr $C$DW$T$285, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$285, DW_AT_decl_line(0x25f)
	.dwattr $C$DW$T$285, DW_AT_decl_column(0x02)

	.dwendtag $C$DW$TU$285


$C$DW$TU$359	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$359

$C$DW$T$359	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$359, DW_AT_type(*$C$DW$T$285)
	.dwattr $C$DW$T$359, DW_AT_byte_size(0x40)
$C$DW$1848	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$1848, DW_AT_upper_bound(0x03)

	.dwendtag $C$DW$T$359

	.dwendtag $C$DW$TU$359


$C$DW$TU$1063	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$1063
$C$DW$T$1063	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$1063, DW_AT_type(*$C$DW$T$285)
	.dwattr $C$DW$T$1063, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$1063


$C$DW$TU$286	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$286
$C$DW$T$286	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$286, DW_AT_type(*$C$DW$T$285)

	.dwendtag $C$DW$TU$286


$C$DW$TU$287	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$287
$C$DW$T$287	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$287, DW_AT_type(*$C$DW$T$286)
	.dwattr $C$DW$T$287, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$287


$C$DW$TU$981	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$981
$C$DW$T$981	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$981, DW_AT_type(*$C$DW$T$985)

	.dwendtag $C$DW$TU$981


$C$DW$TU$982	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$982
$C$DW$T$982	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$982, DW_AT_type(*$C$DW$T$981)
	.dwattr $C$DW$T$982, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$982


$C$DW$TU$992	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$992

$C$DW$T$992	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$992, DW_AT_name("sTIDL_weightStagingSync")
	.dwattr $C$DW$T$992, DW_AT_byte_size(0x60)
$C$DW$1849	.dwtag  DW_TAG_member
	.dwattr $C$DW$1849, DW_AT_type(*$C$DW$T$50)
	.dwattr $C$DW$1849, DW_AT_name("isWtDmaPending")
	.dwattr $C$DW$1849, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1849, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1849, DW_AT_decl_line(0x3c1)
	.dwattr $C$DW$1849, DW_AT_decl_column(0x0f)

$C$DW$1850	.dwtag  DW_TAG_member
	.dwattr $C$DW$1850, DW_AT_type(*$C$DW$T$364)
	.dwattr $C$DW$1850, DW_AT_name("wtMemBasePtr")
	.dwattr $C$DW$1850, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$1850, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1850, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1850, DW_AT_decl_line(0x3c2)
	.dwattr $C$DW$1850, DW_AT_decl_column(0x11)

$C$DW$1851	.dwtag  DW_TAG_member
	.dwattr $C$DW$1851, DW_AT_type(*$C$DW$T$50)
	.dwattr $C$DW$1851, DW_AT_name("currPtrOffset")
	.dwattr $C$DW$1851, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$1851, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1851, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1851, DW_AT_decl_line(0x3c3)
	.dwattr $C$DW$1851, DW_AT_decl_column(0x10)

$C$DW$1852	.dwtag  DW_TAG_member
	.dwattr $C$DW$1852, DW_AT_type(*$C$DW$T$50)
	.dwattr $C$DW$1852, DW_AT_name("lastPtrOffset")
	.dwattr $C$DW$1852, DW_AT_data_member_location[DW_OP_plus_uconst 0x14]
	.dwattr $C$DW$1852, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1852, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1852, DW_AT_decl_line(0x3c4)
	.dwattr $C$DW$1852, DW_AT_decl_column(0x10)

$C$DW$1853	.dwtag  DW_TAG_member
	.dwattr $C$DW$1853, DW_AT_type(*$C$DW$T$50)
	.dwattr $C$DW$1853, DW_AT_name("totStageMemAvail")
	.dwattr $C$DW$1853, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$1853, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1853, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1853, DW_AT_decl_line(0x3c5)
	.dwattr $C$DW$1853, DW_AT_decl_column(0x10)

$C$DW$1854	.dwtag  DW_TAG_member
	.dwattr $C$DW$1854, DW_AT_type(*$C$DW$T$319)
	.dwattr $C$DW$1854, DW_AT_name("trMem")
	.dwattr $C$DW$1854, DW_AT_data_member_location[DW_OP_plus_uconst 0x1c]
	.dwattr $C$DW$1854, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$1854, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$1854, DW_AT_decl_line(0x3c6)
	.dwattr $C$DW$1854, DW_AT_decl_column(0x10)


$C$DW$1855	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1855, DW_AT_name("operator =")
	.dwattr $C$DW$1855, DW_AT_declaration
	.dwattr $C$DW$1855, DW_AT_linkage_name("_ZN23sTIDL_weightStagingSyncaSERKS_")
	.dwattr $C$DW$1855, DW_AT_type(*$C$DW$T$987)
	.dwattr $C$DW$1855, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1856	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1856, DW_AT_type(*$C$DW$T$989)

	.dwendtag $C$DW$1855


$C$DW$1857	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1857, DW_AT_name("operator =")
	.dwattr $C$DW$1857, DW_AT_declaration
	.dwattr $C$DW$1857, DW_AT_linkage_name("_ZN23sTIDL_weightStagingSyncaSEOS_")
	.dwattr $C$DW$1857, DW_AT_type(*$C$DW$T$987)
	.dwattr $C$DW$1857, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$1858	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1858, DW_AT_type(*$C$DW$T$987)

	.dwendtag $C$DW$1857

	.dwattr $C$DW$T$992, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$T$992, DW_AT_decl_line(0x3c0)
	.dwattr $C$DW$T$992, DW_AT_decl_column(0x10)
	.dwendtag $C$DW$T$992

	.dwendtag $C$DW$TU$992


$C$DW$TU$987	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$987
$C$DW$T$987	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$987, DW_AT_type(*$C$DW$T$992)
	.dwattr $C$DW$T$987, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$987


$C$DW$TU$990	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$990

$C$DW$T$990	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$990, DW_AT_type(*$C$DW$T$987)
$C$DW$1859	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1859, DW_AT_type(*$C$DW$T$989)

	.dwendtag $C$DW$T$990

	.dwendtag $C$DW$TU$990


$C$DW$TU$991	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$991

$C$DW$T$991	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$991, DW_AT_type(*$C$DW$T$987)
$C$DW$1860	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$1860, DW_AT_type(*$C$DW$T$987)

	.dwendtag $C$DW$T$991

	.dwendtag $C$DW$TU$991


$C$DW$TU$371	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$371
$C$DW$T$371	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$371, DW_AT_name("sTIDL_weightStagingSync")
	.dwattr $C$DW$T$371, DW_AT_type(*$C$DW$T$992)
	.dwattr $C$DW$T$371, DW_AT_decl_file("./inc/tidl_alg_int.h")
	.dwattr $C$DW$T$371, DW_AT_decl_line(0x3c7)
	.dwattr $C$DW$T$371, DW_AT_decl_column(0x02)

	.dwendtag $C$DW$TU$371


$C$DW$TU$988	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$988
$C$DW$T$988	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$988, DW_AT_type(*$C$DW$T$992)

	.dwendtag $C$DW$TU$988


$C$DW$TU$989	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$989
$C$DW$T$989	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$989, DW_AT_type(*$C$DW$T$988)
	.dwattr $C$DW$T$989, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$989

