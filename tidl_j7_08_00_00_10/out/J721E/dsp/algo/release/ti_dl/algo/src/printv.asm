;******************************************************************************
;* C7x G3 C/C++ Codegen                                            Unix v1.4.2.LTS *
;* Date/Time created: Tue Oct  5 15:11:29 2021                                *
;******************************************************************************
	.compiler_opts --abi=eabi --endian=little --hll_source=on --object_format=elf --silicon_errata_i2117 --silicon_version=7100 --symdebug:dwarf --symdebug:dwarf_version=4 

;******************************************************************************
;* GLOBAL FILE PARAMETERS                                                     *
;*                                                                            *
;*   Architecture      : C7100                                                *
;*   Endian            : Little                                               *
;*   Pipelining        : Enabled                                              *
;*   Debug Info        : DWARF Debug                                          *
;*                                                                            *
;******************************************************************************

	.asg	D14, FP
	.asg	D15, SP
	.global	$bss


$C$DW$CU	.dwtag  DW_TAG_compile_unit
	.dwattr $C$DW$CU, DW_AT_name("src/printv.c")
	.dwattr $C$DW$CU, DW_AT_producer("TI C7x G3 C/C++ Codegen Unix v1.4.2.LTS Copyright (c) 2014-2019 Texas Instruments Incorporated")
	.dwattr $C$DW$CU, DW_AT_TI_version(0x01)
	.dwattr $C$DW$CU, DW_AT_comp_dir("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/tidl_j7_08_00_00_10/ti_dl/algo")

$C$DW$1	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1, DW_AT_name("tidl_printf")
	.dwattr $C$DW$1, DW_AT_linkage_name("_Z11tidl_printfiPKcz")
	.dwattr $C$DW$1, DW_AT_declaration
	.dwattr $C$DW$1, DW_AT_external
	.dwattr $C$DW$1, DW_AT_decl_file("./inc/tidl_commonUtils.h")
	.dwattr $C$DW$1, DW_AT_decl_line(0xc2)
	.dwattr $C$DW$1, DW_AT_decl_column(0x06)
$C$DW$2	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$2, DW_AT_type(*$C$DW$T$26)

$C$DW$3	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$3, DW_AT_type(*$C$DW$T$29)

$C$DW$4	.dwtag  DW_TAG_unspecified_parameters

	.dwendtag $C$DW$1

;	/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/bin/opt7x /tmp/TI4ZcIN74rq /tmp/TI4ZczC4vgb 
	.sect	".text:_Z6PRINTVPKcu7__long8jj"
	.clink
	.global	||_Z6PRINTVPKcu7__long8jj||

$C$DW$5	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$5, DW_AT_name("PRINTV")
	.dwattr $C$DW$5, DW_AT_low_pc(||_Z6PRINTVPKcu7__long8jj||)
	.dwattr $C$DW$5, DW_AT_high_pc(0x00)
	.dwattr $C$DW$5, DW_AT_linkage_name("_Z6PRINTVPKcu7__long8jj")
	.dwattr $C$DW$5, DW_AT_external
	.dwattr $C$DW$5, DW_AT_decl_file("src/printv.c")
	.dwattr $C$DW$5, DW_AT_decl_line(0x57)
	.dwattr $C$DW$5, DW_AT_decl_column(0x06)
	.dwattr $C$DW$5, DW_AT_TI_max_frame_size(0xb0)
	.dwpsn	file "src/printv.c",line 88,column 1,is_stmt,address ||_Z6PRINTVPKcu7__long8jj||,isa 0

	.dwfde $C$DW$CIE, ||_Z6PRINTVPKcu7__long8jj||
$C$DW$6	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$6, DW_AT_name("msg")
	.dwattr $C$DW$6, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$6, DW_AT_location[DW_OP_reg4]

$C$DW$7	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$7, DW_AT_name("inVec")
	.dwattr $C$DW$7, DW_AT_location[DW_OP_regx 0x30]

$C$DW$8	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$8, DW_AT_name("data_type")
	.dwattr $C$DW$8, DW_AT_location[DW_OP_reg5]

$C$DW$9	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$9, DW_AT_name("mode")
	.dwattr $C$DW$9, DW_AT_location[DW_OP_reg6]


;******************************************************************************
;* FUNCTION NAME: PRINTV(const char *, __long8, unsigned int, unsigned int)   *
;*                                                                            *
;*   Regs Modified     : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,VB0,VB1,VB2,   *
;*                           VB3,VB4,VB5,VB6,VB7,VB8,VB9,VB10,VB11,VB12,VB13, *
;*                           VB14,VB15,AL0,AL1,AL2,AL3,AL4,AL5,AL6,AL7,AM0,   *
;*                           AM1,AM2,AM3,AM4,AM5,AM6,AM7,D0,D1,D2,D3,D4,D5,D6,*
;*                           D7,D8,D9,D10,D11,D12,D13,D14,SP,VBL0,VBL1,VBL2,  *
;*                           VBL3,VBL4,VBL5,VBL6,VBL7,VBM0,VBM1,VBM2,VBM3,    *
;*                           VBM4,VBM5,VBM6,VBM7,P0,P1,P2,P3,P4,P5,P6,P7,     *
;*                           CUCR0,CUCR1,CUCR2,CUCR3                          *
;*   Regs Used         : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,VB0,VB1,VB2,   *
;*                           VB3,VB4,VB5,VB6,VB7,VB8,VB9,VB10,VB11,VB12,VB13, *
;*                           VB14,VB15,AL0,AL1,AL2,AL3,AL4,AL5,AL6,AL7,AM0,   *
;*                           AM1,AM2,AM3,AM4,AM5,AM6,AM7,D0,D1,D2,D3,D4,D5,D6,*
;*                           D7,D8,D9,D10,D11,D12,D13,D14,SP,VBL0,VBL1,VBL2,  *
;*                           VBL3,VBL4,VBL5,VBL6,VBL7,VBM0,VBM1,VBM2,VBM3,    *
;*                           VBM4,VBM5,VBM6,VBM7,P0,P1,P2,P3,P4,P5,P6,P7,     *
;*                           CUCR0,CUCR1,CUCR2,CUCR3                          *
;*   Local Frame Size  : 16 Args + 0 Auto + 160 Save = 176 byte               *
;******************************************************************************
||_Z6PRINTVPKcu7__long8jj||:
;** --------------------------------------------------------------------------*
;* VB15  assigned to $O$K77
;* VB15  assigned to $O$K90
;* VB15  assigned to $O$L1
;* VB15  assigned to $O$L2
;* VB15  assigned to $O$L3
;* VB15  assigned to $O$L4
;* A8    assigned to $O$L5
;* A8    assigned to $O$L6
;* VB15  assigned to $O$L7
;* VB15  assigned to $O$L8
;* VB15  assigned to $O$L9
;* A9    assigned to mode
$C$DW$10	.dwtag  DW_TAG_variable
	.dwattr $C$DW$10, DW_AT_name("mode")
	.dwattr $C$DW$10, DW_AT_type(*$C$DW$T$34)
	.dwattr $C$DW$10, DW_AT_location[DW_OP_reg9]

;* A10   assigned to data_type
$C$DW$11	.dwtag  DW_TAG_variable
	.dwattr $C$DW$11, DW_AT_name("data_type")
	.dwattr $C$DW$11, DW_AT_type(*$C$DW$T$34)
	.dwattr $C$DW$11, DW_AT_location[DW_OP_reg10]

;* A4    assigned to msg
$C$DW$12	.dwtag  DW_TAG_variable
	.dwattr $C$DW$12, DW_AT_name("msg")
	.dwattr $C$DW$12, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$12, DW_AT_location[DW_OP_reg4]

;* VB14  assigned to Vec
$C$DW$13	.dwtag  DW_TAG_variable
	.dwattr $C$DW$13, DW_AT_name("Vec")
	.dwattr $C$DW$13, DW_AT_type(*$C$DW$T$32)
	.dwattr $C$DW$13, DW_AT_location[DW_OP_regx 0x3e]

;* VBL0  assigned to newVec
$C$DW$14	.dwtag  DW_TAG_variable
	.dwattr $C$DW$14, DW_AT_name("newVec")
	.dwattr $C$DW$14, DW_AT_type(*$C$DW$T$53)
	.dwattr $C$DW$14, DW_AT_location[DW_OP_regx 0x48]

;* VBL0  assigned to newVec
$C$DW$15	.dwtag  DW_TAG_variable
	.dwattr $C$DW$15, DW_AT_name("newVec")
	.dwattr $C$DW$15, DW_AT_type(*$C$DW$T$61)
	.dwattr $C$DW$15, DW_AT_location[DW_OP_regx 0x48]

;* VB0   assigned to newVec
$C$DW$16	.dwtag  DW_TAG_variable
	.dwattr $C$DW$16, DW_AT_name("newVec")
	.dwattr $C$DW$16, DW_AT_type(*$C$DW$T$69)
	.dwattr $C$DW$16, DW_AT_location[DW_OP_regx 0x30]

;* VBL0  assigned to newVec
$C$DW$17	.dwtag  DW_TAG_variable
	.dwattr $C$DW$17, DW_AT_name("newVec")
	.dwattr $C$DW$17, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$17, DW_AT_location[DW_OP_regx 0x48]

;* VBL0  assigned to newVec
$C$DW$18	.dwtag  DW_TAG_variable
	.dwattr $C$DW$18, DW_AT_name("newVec")
	.dwattr $C$DW$18, DW_AT_type(*$C$DW$T$46)
	.dwattr $C$DW$18, DW_AT_location[DW_OP_regx 0x48]

;* VBL0  assigned to newVec
$C$DW$19	.dwtag  DW_TAG_variable
	.dwattr $C$DW$19, DW_AT_name("newVec")
	.dwattr $C$DW$19, DW_AT_type(*$C$DW$T$57)
	.dwattr $C$DW$19, DW_AT_location[DW_OP_regx 0x48]

;* VB0   assigned to newVec
$C$DW$20	.dwtag  DW_TAG_variable
	.dwattr $C$DW$20, DW_AT_name("newVec")
	.dwattr $C$DW$20, DW_AT_type(*$C$DW$T$65)
	.dwattr $C$DW$20, DW_AT_location[DW_OP_regx 0x30]

;* VBL0  assigned to newVec
$C$DW$21	.dwtag  DW_TAG_variable
	.dwattr $C$DW$21, DW_AT_name("newVec")
	.dwattr $C$DW$21, DW_AT_type(*$C$DW$T$32)
	.dwattr $C$DW$21, DW_AT_location[DW_OP_regx 0x48]

;* VB14  assigned to newVec
$C$DW$22	.dwtag  DW_TAG_variable
	.dwattr $C$DW$22, DW_AT_name("newVec")
	.dwattr $C$DW$22, DW_AT_type(*$C$DW$T$81)
	.dwattr $C$DW$22, DW_AT_location[DW_OP_regx 0x3e]

	.dwcfi	cfa_offset, 0
;          EXCLUSIVE CPU CYCLES: 8

           STD     .D1     A8,*SP(8)         ; [A_D1] 
||         STD     .D2X    A9,*SP++(-176)    ; [A_D2] 

	.dwcfi	save_reg_to_mem, 8, 8
	.dwcfi	cfa_offset, 176
	.dwcfi	save_reg_to_mem, 9, -176
	.dwpsn	file "src/printv.c",line 95,column 5,is_stmt,isa 0
           ADDKPC  .D1     $PCR_OFFSET(($C$SL1+0)),D0 ; [A_D1] |95| 

           MVC     .S1     RP,A11            ; [A_S1] 
||         VST64B  .D2     VB14,*SP(32)      ; [A_D2] 
||         STD     .D1     A11,*SP(160)      ; [A_D1] 
	.dwcfi	save_reg_to_reg, 4101, 11
	.dwcfi	save_reg_to_mem, 62, 32
	.dwcfi	save_reg_to_mem, 11, 160

           VST64B  .D2     VB15,*SP(96)      ; [A_D2] 
||         STD     .D1     A4,*SP(24)        ; [A_D1] |95| 

	.dwcfi	save_reg_to_mem, 63, 96
	.dwpsn	file "src/printv.c",line 88,column 1,is_stmt,isa 0
$C$DW$23	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$23, DW_AT_low_pc(0x00)
	.dwattr $C$DW$23, DW_AT_name("_Z11tidl_printfiPKcz")
	.dwattr $C$DW$23, DW_AT_TI_call


           CALL    .B1     ||_Z11tidl_printfiPKcz|| ; [A_B] |95| 
||         STD     .D1     D0,*SP(16)        ; [A_D1] |95| 
||         VMV     .L2     VB0,VB14          ; [B_L2] |88| 
||         MV      .S1     A5,A10            ; [A_S1] |88| 
||         MVKU32  .L1     0,A4              ; [A_L1] |95| 
||         STD     .D2X    A10,*SP(168)      ; [A_D2] 
||         MV      .M1     A6,A9             ; [A_M1] |88| 

	.dwcfi	save_reg_to_mem, 10, 168
	.dwpsn	file "src/printv.c",line 95,column 5,is_stmt,isa 0
$C$RL0:    ; CALL OCCURS (||_Z11tidl_printfiPKcz||) arg:{A4} ret:{}  ; [] |95| 
	.dwpsn	file "src/printv.c",line 103,column 22,is_stmt,isa 0
           MVKU32  .L1     0x40,A1           ; [A_L1] |103| 
	.dwpsn	file "src/printv.c",line 100,column 5,is_stmt,isa 0
           CMPEQW  .L1     A10,0,A0          ; [A_L1] |100| 

   [!A0]   B       .B1     ||$C$L4||         ; [A_B] |100| 
||         MV      .L2X    A1,B15            ; [B_L2] |100| 

           ; BRANCHCC OCCURS {||$C$L4||}     ; [] |100| 
;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*      Disqualified loop: Loop contains control code
;*----------------------------------------------------------------------------*
||$C$L1||:    
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "src/printv.c",line 105,column 27,is_stmt,isa 0

           VSHR    .L2     VB14,0x8,VB14     ; [B_L2] |106| 
||         CMPEQW  .L1     A9,0,A0           ; [A_L1] |108| 
||         VMV     .S2     VB14,VBL0         ; [B_S2] |105| 

	.dwpsn	file "src/printv.c",line 108,column 13,is_stmt,isa 0
   [!A0]   B       .B1     ||$C$L2||         ; [A_B] |108| 
           ; BRANCHCC OCCURS {||$C$L2||}     ; [] |108| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 3
	.dwpsn	file "src/printv.c",line 110,column 15,is_stmt,isa 0

           ADDKPC  .D1     $PCR_OFFSET(($C$SL2+0)),D0 ; [A_D1] |110| 
||         ANDW    .S2     BL0,0xff,B0       ; [B_S2] |110| 

$C$DW$24	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$24, DW_AT_low_pc(0x00)
	.dwattr $C$DW$24, DW_AT_name("_Z11tidl_printfiPKcz")
	.dwattr $C$DW$24, DW_AT_TI_call


           CALL    .B1     ||_Z11tidl_printfiPKcz|| ; [A_B] |110| 
||         STD     .D1     D0,*SP(16)        ; [A_D1] |110| 
||         STW     .D2     B0,*SP(24)        ; [A_D2] |110| 
||         MVKU32  .L1     0,A4              ; [A_L1] |110| 

$C$RL1:    ; CALL OCCURS (||_Z11tidl_printfiPKcz||) arg:{A4} ret:{}  ; [] |110| 
           B       .B1     ||$C$L3||         ; [A_B] 
           ; BRANCH OCCURS {||$C$L3||}       ; [] 
;** --------------------------------------------------------------------------*
||$C$L2||:    
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "src/printv.c",line 114,column 15,is_stmt,isa 0

           ADDKPC  .D1     $PCR_OFFSET(($C$SL3+0)),D0 ; [A_D1] |114| 
||         ANDW    .S2     BL0,0xff,B0       ; [B_S2] |114| 

$C$DW$25	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$25, DW_AT_low_pc(0x00)
	.dwattr $C$DW$25, DW_AT_name("_Z11tidl_printfiPKcz")
	.dwattr $C$DW$25, DW_AT_TI_call


           CALL    .B1     ||_Z11tidl_printfiPKcz|| ; [A_B] |114| 
||         STD     .D1     D0,*SP(16)        ; [A_D1] |114| 
||         STW     .D2     B0,*SP(24)        ; [A_D2] |114| 
||         MVKU32  .L1     0,A4              ; [A_L1] |114| 

$C$RL2:    ; CALL OCCURS (||_Z11tidl_printfiPKcz||) arg:{A4} ret:{}  ; [] |114| 
;** --------------------------------------------------------------------------*
||$C$L3||:    
;          EXCLUSIVE CPU CYCLES: 4
           MV      .L1X    B15,A0            ; [A_L1] 
	.dwpsn	file "src/printv.c",line 103,column 22,is_stmt,isa 0
           ADDW    .D1     A0,0xffffffff,A0  ; [A_D1] |103| 

   [ A0]   B       .B1     ||$C$L1||         ; [A_B] |103| 
||         MV      .L2X    A0,B15            ; [B_L2] |103| 

           ; BRANCHCC OCCURS {||$C$L1||}     ; [] |103| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "src/printv.c",line 117,column 9,is_stmt,isa 0
           ADDKPC  .D1     $PCR_OFFSET(($C$SL4+0)),D0 ; [A_D1] |117| 
$C$DW$26	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$26, DW_AT_low_pc(0x00)
	.dwattr $C$DW$26, DW_AT_name("_Z11tidl_printfiPKcz")
	.dwattr $C$DW$26, DW_AT_TI_call


           CALL    .B1     ||_Z11tidl_printfiPKcz|| ; [A_B] |117| 
||         STD     .D1     D0,*SP(16)        ; [A_D1] |117| 
||         MVKU32  .L1     0,A4              ; [A_L1] |117| 

$C$RL3:    ; CALL OCCURS (||_Z11tidl_printfiPKcz||) arg:{A4} ret:{}  ; [] |117| 
;** --------------------------------------------------------------------------*
||$C$L4||:    
;          EXCLUSIVE CPU CYCLES: 3
	.dwpsn	file "src/printv.c",line 126,column 22,is_stmt,isa 0
           MVKU32  .L1     0x20,A1           ; [A_L1] |126| 
	.dwpsn	file "src/printv.c",line 123,column 5,is_stmt,isa 0
           CMPEQW  .L1     A10,0x1,A0        ; [A_L1] |123| 

   [!A0]   B       .B1     ||$C$L8||         ; [A_B] |123| 
||         MV      .L2X    A1,B15            ; [B_L2] |126| 

           ; BRANCHCC OCCURS {||$C$L8||}     ; [] |123| 
;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*      Disqualified loop: Loop contains control code
;*----------------------------------------------------------------------------*
||$C$L5||:    
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "src/printv.c",line 128,column 29,is_stmt,isa 0

           VSHR    .L2     VB14,0x10,VB14    ; [B_L2] |129| 
||         CMPEQW  .L1     A9,0,A0           ; [A_L1] |130| 
||         VMV     .S2     VB14,VBL0         ; [B_S2] |128| 

	.dwpsn	file "src/printv.c",line 130,column 13,is_stmt,isa 0
   [!A0]   B       .B1     ||$C$L6||         ; [A_B] |130| 
           ; BRANCHCC OCCURS {||$C$L6||}     ; [] |130| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 3
	.dwpsn	file "src/printv.c",line 132,column 15,is_stmt,isa 0

           ADDKPC  .D1     $PCR_OFFSET(($C$SL2+0)),D0 ; [A_D1] |132| 
||         ANDW    .S2     BL0,0xffff,B0     ; [B_S2] |132| 

$C$DW$27	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$27, DW_AT_low_pc(0x00)
	.dwattr $C$DW$27, DW_AT_name("_Z11tidl_printfiPKcz")
	.dwattr $C$DW$27, DW_AT_TI_call


           CALL    .B1     ||_Z11tidl_printfiPKcz|| ; [A_B] |132| 
||         STD     .D1     D0,*SP(16)        ; [A_D1] |132| 
||         STW     .D2     B0,*SP(24)        ; [A_D2] |132| 
||         MVKU32  .L1     0,A4              ; [A_L1] |132| 

$C$RL4:    ; CALL OCCURS (||_Z11tidl_printfiPKcz||) arg:{A4} ret:{}  ; [] |132| 
           B       .B1     ||$C$L7||         ; [A_B] 
           ; BRANCH OCCURS {||$C$L7||}       ; [] 
;** --------------------------------------------------------------------------*
||$C$L6||:    
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "src/printv.c",line 136,column 15,is_stmt,isa 0

           ADDKPC  .D1     $PCR_OFFSET(($C$SL5+0)),D0 ; [A_D1] |136| 
||         ANDW    .S2     BL0,0xffff,B0     ; [B_S2] |136| 

$C$DW$28	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$28, DW_AT_low_pc(0x00)
	.dwattr $C$DW$28, DW_AT_name("_Z11tidl_printfiPKcz")
	.dwattr $C$DW$28, DW_AT_TI_call


           CALL    .B1     ||_Z11tidl_printfiPKcz|| ; [A_B] |136| 
||         STD     .D1     D0,*SP(16)        ; [A_D1] |136| 
||         STW     .D2     B0,*SP(24)        ; [A_D2] |136| 
||         MVKU32  .L1     0,A4              ; [A_L1] |136| 

$C$RL5:    ; CALL OCCURS (||_Z11tidl_printfiPKcz||) arg:{A4} ret:{}  ; [] |136| 
;** --------------------------------------------------------------------------*
||$C$L7||:    
;          EXCLUSIVE CPU CYCLES: 4
           MV      .L1X    B15,A0            ; [A_L1] 
	.dwpsn	file "src/printv.c",line 126,column 22,is_stmt,isa 0
           ADDW    .D1     A0,0xffffffff,A0  ; [A_D1] |126| 

   [ A0]   B       .B1     ||$C$L5||         ; [A_B] |126| 
||         MV      .L2X    A0,B15            ; [B_L2] |126| 

           ; BRANCHCC OCCURS {||$C$L5||}     ; [] |126| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "src/printv.c",line 139,column 9,is_stmt,isa 0
           ADDKPC  .D1     $PCR_OFFSET(($C$SL4+0)),D0 ; [A_D1] |139| 
$C$DW$29	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$29, DW_AT_low_pc(0x00)
	.dwattr $C$DW$29, DW_AT_name("_Z11tidl_printfiPKcz")
	.dwattr $C$DW$29, DW_AT_TI_call


           CALL    .B1     ||_Z11tidl_printfiPKcz|| ; [A_B] |139| 
||         STD     .D1     D0,*SP(16)        ; [A_D1] |139| 
||         MVKU32  .L1     0,A4              ; [A_L1] |139| 

$C$RL6:    ; CALL OCCURS (||_Z11tidl_printfiPKcz||) arg:{A4} ret:{}  ; [] |139| 
;** --------------------------------------------------------------------------*
||$C$L8||:    
;          EXCLUSIVE CPU CYCLES: 3
	.dwpsn	file "src/printv.c",line 148,column 22,is_stmt,isa 0
           MVKU32  .L1     0x10,A1           ; [A_L1] |148| 
	.dwpsn	file "src/printv.c",line 145,column 5,is_stmt,isa 0
           CMPEQW  .L1     A10,0x2,A0        ; [A_L1] |145| 

   [!A0]   B       .B1     ||$C$L12||        ; [A_B] |145| 
||         MV      .L2X    A1,B15            ; [B_L2] |148| 

           ; BRANCHCC OCCURS {||$C$L12||}    ; [] |145| 
;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*      Disqualified loop: Loop contains control code
;*----------------------------------------------------------------------------*
||$C$L9||:    
;          EXCLUSIVE CPU CYCLES: 6
	.dwpsn	file "src/printv.c",line 151,column 13,is_stmt,isa 0
           VSHR    .L2     VB14,0x10,VBL0    ; [B_L2] |151| 
	.dwpsn	file "src/printv.c",line 150,column 27,is_stmt,isa 0

           VSHR    .L2     VBL0,0x10,VB14    ; [B_L2] |152| 
||         CMPEQW  .L1     A9,0,A0           ; [A_L1] |153| 
||         VMV     .S2     VB14,VB0          ; [B_S2] |150| 

	.dwpsn	file "src/printv.c",line 153,column 13,is_stmt,isa 0
   [!A0]   B       .B1     ||$C$L10||        ; [A_B] |153| 
           ; BRANCHCC OCCURS {||$C$L10||}    ; [] |153| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 3
	.dwpsn	file "src/printv.c",line 155,column 15,is_stmt,isa 0
           ADDKPC  .D1     $PCR_OFFSET(($C$SL6+0)),D0 ; [A_D1] |155| 
$C$DW$30	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$30, DW_AT_low_pc(0x00)
	.dwattr $C$DW$30, DW_AT_name("_Z11tidl_printfiPKcz")
	.dwattr $C$DW$30, DW_AT_TI_call


           CALL    .B1     ||_Z11tidl_printfiPKcz|| ; [A_B] |155| 
||         STD     .D1     D0,*SP(16)        ; [A_D1] |155| 
||         STW     .D2     B0,*SP(24)        ; [A_D2] |155| 
||         MVKU32  .L1     0,A4              ; [A_L1] |155| 

$C$RL7:    ; CALL OCCURS (||_Z11tidl_printfiPKcz||) arg:{A4} ret:{}  ; [] |155| 
           B       .B1     ||$C$L11||        ; [A_B] 
           ; BRANCH OCCURS {||$C$L11||}      ; [] 
;** --------------------------------------------------------------------------*
||$C$L10||:    
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "src/printv.c",line 159,column 15,is_stmt,isa 0
           ADDKPC  .D1     $PCR_OFFSET(($C$SL7+0)),D0 ; [A_D1] |159| 
$C$DW$31	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$31, DW_AT_low_pc(0x00)
	.dwattr $C$DW$31, DW_AT_name("_Z11tidl_printfiPKcz")
	.dwattr $C$DW$31, DW_AT_TI_call


           CALL    .B1     ||_Z11tidl_printfiPKcz|| ; [A_B] |159| 
||         STD     .D1     D0,*SP(16)        ; [A_D1] |159| 
||         STW     .D2     B0,*SP(24)        ; [A_D2] |159| 
||         MVKU32  .L1     0,A4              ; [A_L1] |159| 

$C$RL8:    ; CALL OCCURS (||_Z11tidl_printfiPKcz||) arg:{A4} ret:{}  ; [] |159| 
;** --------------------------------------------------------------------------*
||$C$L11||:    
;          EXCLUSIVE CPU CYCLES: 4
           MV      .L1X    B15,A0            ; [A_L1] 
	.dwpsn	file "src/printv.c",line 148,column 22,is_stmt,isa 0
           ADDW    .D1     A0,0xffffffff,A0  ; [A_D1] |148| 

   [ A0]   B       .B1     ||$C$L9||         ; [A_B] |148| 
||         MV      .L2X    A0,B15            ; [B_L2] |148| 

           ; BRANCHCC OCCURS {||$C$L9||}     ; [] |148| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "src/printv.c",line 163,column 9,is_stmt,isa 0
           ADDKPC  .D1     $PCR_OFFSET(($C$SL4+0)),D0 ; [A_D1] |163| 
$C$DW$32	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$32, DW_AT_low_pc(0x00)
	.dwattr $C$DW$32, DW_AT_name("_Z11tidl_printfiPKcz")
	.dwattr $C$DW$32, DW_AT_TI_call


           CALL    .B1     ||_Z11tidl_printfiPKcz|| ; [A_B] |163| 
||         STD     .D1     D0,*SP(16)        ; [A_D1] |163| 
||         MVKU32  .L1     0,A4              ; [A_L1] |163| 

$C$RL9:    ; CALL OCCURS (||_Z11tidl_printfiPKcz||) arg:{A4} ret:{}  ; [] |163| 
;** --------------------------------------------------------------------------*
||$C$L12||:    
;          EXCLUSIVE CPU CYCLES: 3
	.dwpsn	file "src/printv.c",line 172,column 22,is_stmt,isa 0
           MVKU32  .L1     0x8,A1            ; [A_L1] |172| 
	.dwpsn	file "src/printv.c",line 169,column 5,is_stmt,isa 0
           CMPEQW  .L1     A10,0x3,A0        ; [A_L1] |169| 

   [!A0]   B       .B1     ||$C$L16||        ; [A_B] |169| 
||         MV      .L2X    A1,B15            ; [B_L2] |172| 

           ; BRANCHCC OCCURS {||$C$L16||}    ; [] |169| 
;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*      Disqualified loop: Loop contains control code
;*----------------------------------------------------------------------------*
||$C$L13||:    
;          EXCLUSIVE CPU CYCLES: 14
	.dwpsn	file "src/printv.c",line 175,column 13,is_stmt,isa 0
           VSHR    .L2     VB14,0x10,VBL0    ; [B_L2] |175| 
	.dwpsn	file "src/printv.c",line 176,column 13,is_stmt,isa 0
           VSHR    .L2     VBL0,0x10,VBL0    ; [B_L2] |176| 
	.dwpsn	file "src/printv.c",line 177,column 13,is_stmt,isa 0
           VSHR    .L2     VBL0,0x10,VBL1    ; [B_L2] |177| 
	.dwpsn	file "src/printv.c",line 174,column 27,is_stmt,isa 0

           VSHR    .L2     VBL1,0x10,VB14    ; [B_L2] |178| 
||         VMV     .S2     VB14,VBL0         ; [B_S2] |174| 
||         CMPEQW  .L1     A9,0,A0           ; [A_L1] |179| 

	.dwpsn	file "src/printv.c",line 179,column 13,is_stmt,isa 0
   [!A0]   B       .B1     ||$C$L14||        ; [A_B] |179| 
           ; BRANCHCC OCCURS {||$C$L14||}    ; [] |179| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 3
	.dwpsn	file "src/printv.c",line 181,column 15,is_stmt,isa 0

           ADDKPC  .D1     $PCR_OFFSET(($C$SL8+0)),D0 ; [A_D1] |181| 
||         MV      .L2     BL0,B0            ; [B_L2] |181| 

$C$DW$33	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$33, DW_AT_low_pc(0x00)
	.dwattr $C$DW$33, DW_AT_name("_Z11tidl_printfiPKcz")
	.dwattr $C$DW$33, DW_AT_TI_call


           CALL    .B1     ||_Z11tidl_printfiPKcz|| ; [A_B] |181| 
||         STD     .D1     D0,*SP(16)        ; [A_D1] |181| 
||         STD     .D2     B0,*SP(24)        ; [A_D2] |181| 
||         MVKU32  .L1     0,A4              ; [A_L1] |181| 

$C$RL10:   ; CALL OCCURS (||_Z11tidl_printfiPKcz||) arg:{A4} ret:{}  ; [] |181| 
           B       .B1     ||$C$L15||        ; [A_B] 
           ; BRANCH OCCURS {||$C$L15||}      ; [] 
;** --------------------------------------------------------------------------*
||$C$L14||:    
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "src/printv.c",line 185,column 15,is_stmt,isa 0

           ADDKPC  .D1     $PCR_OFFSET(($C$SL9+0)),D0 ; [A_D1] |185| 
||         MV      .L2     BL0,B0            ; [B_L2] |185| 

$C$DW$34	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$34, DW_AT_low_pc(0x00)
	.dwattr $C$DW$34, DW_AT_name("_Z11tidl_printfiPKcz")
	.dwattr $C$DW$34, DW_AT_TI_call


           CALL    .B1     ||_Z11tidl_printfiPKcz|| ; [A_B] |185| 
||         STD     .D1     D0,*SP(16)        ; [A_D1] |185| 
||         STD     .D2     B0,*SP(24)        ; [A_D2] |185| 
||         MVKU32  .L1     0,A4              ; [A_L1] |185| 

$C$RL11:   ; CALL OCCURS (||_Z11tidl_printfiPKcz||) arg:{A4} ret:{}  ; [] |185| 
;** --------------------------------------------------------------------------*
||$C$L15||:    
;          EXCLUSIVE CPU CYCLES: 4
           MV      .L1X    B15,A0            ; [A_L1] 
	.dwpsn	file "src/printv.c",line 172,column 22,is_stmt,isa 0
           ADDW    .D1     A0,0xffffffff,A0  ; [A_D1] |172| 

   [ A0]   B       .B1     ||$C$L13||        ; [A_B] |172| 
||         MV      .L2X    A0,B15            ; [B_L2] |172| 

           ; BRANCHCC OCCURS {||$C$L13||}    ; [] |172| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "src/printv.c",line 189,column 9,is_stmt,isa 0
           ADDKPC  .D1     $PCR_OFFSET(($C$SL4+0)),D0 ; [A_D1] |189| 
$C$DW$35	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$35, DW_AT_low_pc(0x00)
	.dwattr $C$DW$35, DW_AT_name("_Z11tidl_printfiPKcz")
	.dwattr $C$DW$35, DW_AT_TI_call


           CALL    .B1     ||_Z11tidl_printfiPKcz|| ; [A_B] |189| 
||         STD     .D1     D0,*SP(16)        ; [A_D1] |189| 
||         MVKU32  .L1     0,A4              ; [A_L1] |189| 

$C$RL12:   ; CALL OCCURS (||_Z11tidl_printfiPKcz||) arg:{A4} ret:{}  ; [] |189| 
;** --------------------------------------------------------------------------*
||$C$L16||:    
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "src/printv.c",line 195,column 5,is_stmt,isa 0
           CMPEQW  .L1     A10,0x4,A0        ; [A_L1] |195| 

   [!A0]   B       .B1     ||$C$L20||        ; [A_B] |195| 
||         MVKU32  .L2     0xff,B15          ; [B_L2] 
||         MVKU32  .L1     0x40,A8           ; [A_L1] |198| 

           ; BRANCHCC OCCURS {||$C$L20||}    ; [] |195| 
;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*      Disqualified loop: Loop contains control code
;*----------------------------------------------------------------------------*
||$C$L17||:    
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "src/printv.c",line 200,column 27,is_stmt,isa 0

           VSHR    .S2     VB14,0x8,VB14     ; [B_S2] |201| 
||         CMPEQW  .L1     A9,0,A0           ; [A_L1] |202| 
||         VMV     .L2     VB14,VBL0         ; [B_L2] |200| 

	.dwpsn	file "src/printv.c",line 202,column 13,is_stmt,isa 0
   [!A0]   B       .B1     ||$C$L18||        ; [A_B] |202| 
           ; BRANCHCC OCCURS {||$C$L18||}    ; [] |202| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 3
	.dwpsn	file "src/printv.c",line 204,column 15,is_stmt,isa 0

           ADDKPC  .D1     $PCR_OFFSET(($C$SL2+0)),D0 ; [A_D1] |204| 
||         EXT     .S2     BL0,0x38,0x38,B0  ; [B_S2] |204| 

$C$DW$36	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$36, DW_AT_low_pc(0x00)
	.dwattr $C$DW$36, DW_AT_name("_Z11tidl_printfiPKcz")
	.dwattr $C$DW$36, DW_AT_TI_call


           CALL    .B1     ||_Z11tidl_printfiPKcz|| ; [A_B] |204| 
||         STD     .D1     D0,*SP(16)        ; [A_D1] |204| 
||         STW     .D2     B0,*SP(24)        ; [A_D2] |204| 
||         MVKU32  .L1     0,A4              ; [A_L1] |204| 

$C$RL13:   ; CALL OCCURS (||_Z11tidl_printfiPKcz||) arg:{A4} ret:{}  ; [] |204| 
           B       .B1     ||$C$L19||        ; [A_B] 
           ; BRANCH OCCURS {||$C$L19||}      ; [] 
;** --------------------------------------------------------------------------*
||$C$L18||:    
;          EXCLUSIVE CPU CYCLES: 3
	.dwpsn	file "src/printv.c",line 208,column 15,is_stmt,isa 0
           EXT     .L2     BL0,0x38,0x38,BL0 ; [B_L2] |208| 

           ANDW    .L2     B15,BL0,B0        ; [B_L2] |208| 
||         ADDKPC  .D1     $PCR_OFFSET(($C$SL3+0)),A0 ; [A_D1] |208| 

$C$DW$37	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$37, DW_AT_low_pc(0x00)
	.dwattr $C$DW$37, DW_AT_name("_Z11tidl_printfiPKcz")
	.dwattr $C$DW$37, DW_AT_TI_call


           CALL    .B1     ||_Z11tidl_printfiPKcz|| ; [A_B] |208| 
||         STW     .D1X    B0,*SP(24)        ; [A_D1] |208| 
||         STD     .D2X    A0,*SP(16)        ; [A_D2] |208| 
||         MVKU32  .L1     0,A4              ; [A_L1] |208| 

$C$RL14:   ; CALL OCCURS (||_Z11tidl_printfiPKcz||) arg:{A4} ret:{}  ; [] |208| 
;** --------------------------------------------------------------------------*
||$C$L19||:    
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "src/printv.c",line 198,column 22,is_stmt,isa 0
           ADDW    .D1     A8,0xffffffff,A0  ; [A_D1] |198| 

   [ A0]   B       .B1     ||$C$L17||        ; [A_B] |198| 
||         MV      .D1     A0,A8             ; [A_D1] |198| 

           ; BRANCHCC OCCURS {||$C$L17||}    ; [] |198| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "src/printv.c",line 211,column 9,is_stmt,isa 0
           ADDKPC  .D1     $PCR_OFFSET(($C$SL4+0)),D0 ; [A_D1] |211| 
$C$DW$38	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$38, DW_AT_low_pc(0x00)
	.dwattr $C$DW$38, DW_AT_name("_Z11tidl_printfiPKcz")
	.dwattr $C$DW$38, DW_AT_TI_call


           CALL    .B1     ||_Z11tidl_printfiPKcz|| ; [A_B] |211| 
||         STD     .D1     D0,*SP(16)        ; [A_D1] |211| 
||         MVKU32  .L1     0,A4              ; [A_L1] |211| 

$C$RL15:   ; CALL OCCURS (||_Z11tidl_printfiPKcz||) arg:{A4} ret:{}  ; [] |211| 
;** --------------------------------------------------------------------------*
||$C$L20||:    
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "src/printv.c",line 217,column 5,is_stmt,isa 0
           CMPEQW  .L1     A10,0x5,A0        ; [A_L1] |217| 

   [!A0]   B       .B1     ||$C$L24||        ; [A_B] |217| 
||         MVKU32  .L2     0xffff,B15        ; [B_L2] 
||         MVKU32  .L1     0x20,A8           ; [A_L1] |220| 

           ; BRANCHCC OCCURS {||$C$L24||}    ; [] |217| 
;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*      Disqualified loop: Loop contains control code
;*----------------------------------------------------------------------------*
||$C$L21||:    
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "src/printv.c",line 222,column 28,is_stmt,isa 0

           VSHR    .S2     VB14,0x10,VB14    ; [B_S2] |223| 
||         CMPEQW  .L1     A9,0,A0           ; [A_L1] |225| 
||         VMV     .L2     VB14,VBL0         ; [B_L2] |222| 

	.dwpsn	file "src/printv.c",line 225,column 13,is_stmt,isa 0
   [!A0]   B       .B1     ||$C$L22||        ; [A_B] |225| 
           ; BRANCHCC OCCURS {||$C$L22||}    ; [] |225| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 3
	.dwpsn	file "src/printv.c",line 227,column 15,is_stmt,isa 0

           ADDKPC  .D1     $PCR_OFFSET(($C$SL2+0)),D0 ; [A_D1] |227| 
||         EXT     .S2     BL0,0x30,0x30,B0  ; [B_S2] |227| 

$C$DW$39	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$39, DW_AT_low_pc(0x00)
	.dwattr $C$DW$39, DW_AT_name("_Z11tidl_printfiPKcz")
	.dwattr $C$DW$39, DW_AT_TI_call


           CALL    .B1     ||_Z11tidl_printfiPKcz|| ; [A_B] |227| 
||         STD     .D1     D0,*SP(16)        ; [A_D1] |227| 
||         STW     .D2     B0,*SP(24)        ; [A_D2] |227| 
||         MVKU32  .L1     0,A4              ; [A_L1] |227| 

$C$RL16:   ; CALL OCCURS (||_Z11tidl_printfiPKcz||) arg:{A4} ret:{}  ; [] |227| 
           B       .B1     ||$C$L23||        ; [A_B] 
           ; BRANCH OCCURS {||$C$L23||}      ; [] 
;** --------------------------------------------------------------------------*
||$C$L22||:    
;          EXCLUSIVE CPU CYCLES: 3
	.dwpsn	file "src/printv.c",line 231,column 15,is_stmt,isa 0
           EXT     .L2     BL0,0x30,0x30,BL0 ; [B_L2] |231| 

           ANDW    .L2     B15,BL0,B0        ; [B_L2] |231| 
||         ADDKPC  .D1     $PCR_OFFSET(($C$SL5+0)),A0 ; [A_D1] |231| 

$C$DW$40	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$40, DW_AT_low_pc(0x00)
	.dwattr $C$DW$40, DW_AT_name("_Z11tidl_printfiPKcz")
	.dwattr $C$DW$40, DW_AT_TI_call


           CALL    .B1     ||_Z11tidl_printfiPKcz|| ; [A_B] |231| 
||         STW     .D1X    B0,*SP(24)        ; [A_D1] |231| 
||         STD     .D2X    A0,*SP(16)        ; [A_D2] |231| 
||         MVKU32  .L1     0,A4              ; [A_L1] |231| 

$C$RL17:   ; CALL OCCURS (||_Z11tidl_printfiPKcz||) arg:{A4} ret:{}  ; [] |231| 
;** --------------------------------------------------------------------------*
||$C$L23||:    
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "src/printv.c",line 220,column 22,is_stmt,isa 0
           ADDW    .D1     A8,0xffffffff,A0  ; [A_D1] |220| 

   [ A0]   B       .B1     ||$C$L21||        ; [A_B] |220| 
||         MV      .D1     A0,A8             ; [A_D1] |220| 

           ; BRANCHCC OCCURS {||$C$L21||}    ; [] |220| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "src/printv.c",line 235,column 9,is_stmt,isa 0
           ADDKPC  .D1     $PCR_OFFSET(($C$SL4+0)),D0 ; [A_D1] |235| 
$C$DW$41	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$41, DW_AT_low_pc(0x00)
	.dwattr $C$DW$41, DW_AT_name("_Z11tidl_printfiPKcz")
	.dwattr $C$DW$41, DW_AT_TI_call


           CALL    .B1     ||_Z11tidl_printfiPKcz|| ; [A_B] |235| 
||         STD     .D1     D0,*SP(16)        ; [A_D1] |235| 
||         MVKU32  .L1     0,A4              ; [A_L1] |235| 

$C$RL18:   ; CALL OCCURS (||_Z11tidl_printfiPKcz||) arg:{A4} ret:{}  ; [] |235| 
;** --------------------------------------------------------------------------*
||$C$L24||:    
;          EXCLUSIVE CPU CYCLES: 3
	.dwpsn	file "src/printv.c",line 244,column 22,is_stmt,isa 0
           MVKU32  .L1     0x10,A1           ; [A_L1] |244| 
	.dwpsn	file "src/printv.c",line 241,column 5,is_stmt,isa 0
           CMPEQW  .L1     A10,0x6,A0        ; [A_L1] |241| 

   [!A0]   B       .B1     ||$C$L28||        ; [A_B] |241| 
||         MV      .L2X    A1,B15            ; [B_L2] |244| 

           ; BRANCHCC OCCURS {||$C$L28||}    ; [] |241| 
;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*      Disqualified loop: Loop contains control code
;*----------------------------------------------------------------------------*
||$C$L25||:    
;          EXCLUSIVE CPU CYCLES: 6
	.dwpsn	file "src/printv.c",line 247,column 13,is_stmt,isa 0
           VSHR    .L2     VB14,0x10,VBL0    ; [B_L2] |247| 
	.dwpsn	file "src/printv.c",line 246,column 26,is_stmt,isa 0

           VSHR    .L2     VBL0,0x10,VB14    ; [B_L2] |248| 
||         CMPEQW  .L1     A9,0,A0           ; [A_L1] |249| 
||         VMV     .S2     VB14,VB0          ; [B_S2] |246| 

	.dwpsn	file "src/printv.c",line 249,column 13,is_stmt,isa 0
   [!A0]   B       .B1     ||$C$L26||        ; [A_B] |249| 
           ; BRANCHCC OCCURS {||$C$L26||}    ; [] |249| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 3
	.dwpsn	file "src/printv.c",line 251,column 15,is_stmt,isa 0
           ADDKPC  .D1     $PCR_OFFSET(($C$SL6+0)),D0 ; [A_D1] |251| 
$C$DW$42	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$42, DW_AT_low_pc(0x00)
	.dwattr $C$DW$42, DW_AT_name("_Z11tidl_printfiPKcz")
	.dwattr $C$DW$42, DW_AT_TI_call


           CALL    .B1     ||_Z11tidl_printfiPKcz|| ; [A_B] |251| 
||         STD     .D1     D0,*SP(16)        ; [A_D1] |251| 
||         STW     .D2     B0,*SP(24)        ; [A_D2] |251| 
||         MVKU32  .L1     0,A4              ; [A_L1] |251| 

$C$RL19:   ; CALL OCCURS (||_Z11tidl_printfiPKcz||) arg:{A4} ret:{}  ; [] |251| 
           B       .B1     ||$C$L27||        ; [A_B] 
           ; BRANCH OCCURS {||$C$L27||}      ; [] 
;** --------------------------------------------------------------------------*
||$C$L26||:    
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "src/printv.c",line 255,column 15,is_stmt,isa 0
           ADDKPC  .D1     $PCR_OFFSET(($C$SL7+0)),D0 ; [A_D1] |255| 
$C$DW$43	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$43, DW_AT_low_pc(0x00)
	.dwattr $C$DW$43, DW_AT_name("_Z11tidl_printfiPKcz")
	.dwattr $C$DW$43, DW_AT_TI_call


           CALL    .B1     ||_Z11tidl_printfiPKcz|| ; [A_B] |255| 
||         STD     .D1     D0,*SP(16)        ; [A_D1] |255| 
||         STW     .D2     B0,*SP(24)        ; [A_D2] |255| 
||         MVKU32  .L1     0,A4              ; [A_L1] |255| 

$C$RL20:   ; CALL OCCURS (||_Z11tidl_printfiPKcz||) arg:{A4} ret:{}  ; [] |255| 
;** --------------------------------------------------------------------------*
||$C$L27||:    
;          EXCLUSIVE CPU CYCLES: 4
           MV      .L1X    B15,A0            ; [A_L1] 
	.dwpsn	file "src/printv.c",line 244,column 22,is_stmt,isa 0
           ADDW    .D1     A0,0xffffffff,A0  ; [A_D1] |244| 

   [ A0]   B       .B1     ||$C$L25||        ; [A_B] |244| 
||         MV      .L2X    A0,B15            ; [B_L2] |244| 

           ; BRANCHCC OCCURS {||$C$L25||}    ; [] |244| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "src/printv.c",line 258,column 9,is_stmt,isa 0
           ADDKPC  .D1     $PCR_OFFSET(($C$SL4+0)),D0 ; [A_D1] |258| 
$C$DW$44	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$44, DW_AT_low_pc(0x00)
	.dwattr $C$DW$44, DW_AT_name("_Z11tidl_printfiPKcz")
	.dwattr $C$DW$44, DW_AT_TI_call


           CALL    .B1     ||_Z11tidl_printfiPKcz|| ; [A_B] |258| 
||         STD     .D1     D0,*SP(16)        ; [A_D1] |258| 
||         MVKU32  .L1     0,A4              ; [A_L1] |258| 

$C$RL21:   ; CALL OCCURS (||_Z11tidl_printfiPKcz||) arg:{A4} ret:{}  ; [] |258| 
;** --------------------------------------------------------------------------*
||$C$L28||:    
;          EXCLUSIVE CPU CYCLES: 3
	.dwpsn	file "src/printv.c",line 267,column 22,is_stmt,isa 0
           MVKU32  .L1     0x8,A1            ; [A_L1] |267| 
	.dwpsn	file "src/printv.c",line 264,column 5,is_stmt,isa 0
           CMPEQW  .L1     A10,0x7,A0        ; [A_L1] |264| 

   [!A0]   B       .B1     ||$C$L32||        ; [A_B] |264| 
||         MV      .L2X    A1,B15            ; [B_L2] |267| 

           ; BRANCHCC OCCURS {||$C$L32||}    ; [] |264| 
;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*      Disqualified loop: Loop contains control code
;*----------------------------------------------------------------------------*
||$C$L29||:    
;          EXCLUSIVE CPU CYCLES: 15
	.dwpsn	file "src/printv.c",line 269,column 26,is_stmt,isa 0
           VMV     .L2     VB14,VBL0         ; [B_L2] |269| 
	.dwpsn	file "src/printv.c",line 270,column 13,is_stmt,isa 0
           VSHR    .L2     VBL0,0x10,VBL1    ; [B_L2] |270| 
	.dwpsn	file "src/printv.c",line 271,column 13,is_stmt,isa 0
           VSHR    .L2     VBL1,0x10,VBL1    ; [B_L2] |271| 
	.dwpsn	file "src/printv.c",line 272,column 13,is_stmt,isa 0
           VSHR    .L2     VBL1,0x10,VBL1    ; [B_L2] |272| 
	.dwpsn	file "src/printv.c",line 273,column 13,is_stmt,isa 0

           VSHR    .L2     VBL1,0x10,VB14    ; [B_L2] |273| 
||         CMPEQW  .L1     A9,0,A0           ; [A_L1] |274| 

	.dwpsn	file "src/printv.c",line 274,column 13,is_stmt,isa 0
   [!A0]   B       .B1     ||$C$L30||        ; [A_B] |274| 
           ; BRANCHCC OCCURS {||$C$L30||}    ; [] |274| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 3
	.dwpsn	file "src/printv.c",line 276,column 15,is_stmt,isa 0

           ADDKPC  .D1     $PCR_OFFSET(($C$SL8+0)),D0 ; [A_D1] |276| 
||         MV      .L2     BL0,B0            ; [B_L2] |276| 

$C$DW$45	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$45, DW_AT_low_pc(0x00)
	.dwattr $C$DW$45, DW_AT_name("_Z11tidl_printfiPKcz")
	.dwattr $C$DW$45, DW_AT_TI_call


           CALL    .B1     ||_Z11tidl_printfiPKcz|| ; [A_B] |276| 
||         STD     .D1     D0,*SP(16)        ; [A_D1] |276| 
||         STD     .D2     B0,*SP(24)        ; [A_D2] |276| 
||         MVKU32  .L1     0,A4              ; [A_L1] |276| 

$C$RL22:   ; CALL OCCURS (||_Z11tidl_printfiPKcz||) arg:{A4} ret:{}  ; [] |276| 
           B       .B1     ||$C$L31||        ; [A_B] 
           ; BRANCH OCCURS {||$C$L31||}      ; [] 
;** --------------------------------------------------------------------------*
||$C$L30||:    
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "src/printv.c",line 280,column 15,is_stmt,isa 0

           ADDKPC  .D1     $PCR_OFFSET(($C$SL9+0)),D0 ; [A_D1] |280| 
||         MV      .L2     BL0,B0            ; [B_L2] |280| 

$C$DW$46	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$46, DW_AT_low_pc(0x00)
	.dwattr $C$DW$46, DW_AT_name("_Z11tidl_printfiPKcz")
	.dwattr $C$DW$46, DW_AT_TI_call


           CALL    .B1     ||_Z11tidl_printfiPKcz|| ; [A_B] |280| 
||         STD     .D1     D0,*SP(16)        ; [A_D1] |280| 
||         STD     .D2     B0,*SP(24)        ; [A_D2] |280| 
||         MVKU32  .L1     0,A4              ; [A_L1] |280| 

$C$RL23:   ; CALL OCCURS (||_Z11tidl_printfiPKcz||) arg:{A4} ret:{}  ; [] |280| 
;** --------------------------------------------------------------------------*
||$C$L31||:    
;          EXCLUSIVE CPU CYCLES: 4
           MV      .L1X    B15,A0            ; [A_L1] 
	.dwpsn	file "src/printv.c",line 267,column 22,is_stmt,isa 0
           ADDW    .D1     A0,0xffffffff,A0  ; [A_D1] |267| 

   [ A0]   B       .B1     ||$C$L29||        ; [A_B] |267| 
||         MV      .L2X    A0,B15            ; [B_L2] |267| 

           ; BRANCHCC OCCURS {||$C$L29||}    ; [] |267| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "src/printv.c",line 284,column 9,is_stmt,isa 0
           ADDKPC  .D1     $PCR_OFFSET(($C$SL4+0)),D0 ; [A_D1] |284| 
$C$DW$47	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$47, DW_AT_low_pc(0x00)
	.dwattr $C$DW$47, DW_AT_name("_Z11tidl_printfiPKcz")
	.dwattr $C$DW$47, DW_AT_TI_call


           CALL    .B1     ||_Z11tidl_printfiPKcz|| ; [A_B] |284| 
||         STD     .D1     D0,*SP(16)        ; [A_D1] |284| 
||         MVKU32  .L1     0,A4              ; [A_L1] |284| 

$C$RL24:   ; CALL OCCURS (||_Z11tidl_printfiPKcz||) arg:{A4} ret:{}  ; [] |284| 
;** --------------------------------------------------------------------------*
||$C$L32||:    
;          EXCLUSIVE CPU CYCLES: 3
	.dwpsn	file "src/printv.c",line 293,column 22,is_stmt,isa 0
           MVKU32  .L1     0x10,A1           ; [A_L1] |293| 
	.dwpsn	file "src/printv.c",line 290,column 5,is_stmt,isa 0
           CMPEQW  .L1     A10,0x8,A0        ; [A_L1] |290| 

   [!A0]   B       .B1     ||$C$L34||        ; [A_B] |290| 
||         MV      .L2X    A1,B15            ; [B_L2] |293| 

           ; BRANCHCC OCCURS {||$C$L34||}    ; [] |290| 
;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*      Disqualified loop: Loop contains a call
;*      Disqualified loop: Loop contains non-pipelinable instructions
;*----------------------------------------------------------------------------*
||$C$L33||:    
;          EXCLUSIVE CPU CYCLES: 11
	.dwpsn	file "src/printv.c",line 296,column 35,is_stmt,isa 0
           MV      .L2     B14,BL0           ; [B_L2] |296| 

           VSPDPL  .L2     BL0,B0            ; [B_L2] |296| 
||         ADDKPC  .D1     $PCR_OFFSET(($C$SL10+0)),A0 ; [A_D1] |296| 

$C$DW$48	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$48, DW_AT_low_pc(0x00)
	.dwattr $C$DW$48, DW_AT_name("_Z11tidl_printfiPKcz")
	.dwattr $C$DW$48, DW_AT_TI_call


           CALL    .B1     ||_Z11tidl_printfiPKcz|| ; [A_B] |296| 
||         STD     .D1X    B0,*SP(24)        ; [A_D1] |296| 
||         STD     .D2X    A0,*SP(16)        ; [A_D2] |296| 
||         MVKU32  .L1     0,A4              ; [A_L1] |296| 

$C$RL25:   ; CALL OCCURS (||_Z11tidl_printfiPKcz||) arg:{A4} ret:{}  ; [] |296| 
	.dwpsn	file "src/printv.c",line 297,column 13,is_stmt,isa 0
           VSHR    .L2     VB14,0x10,VBL0    ; [B_L2] |297| 
	.dwpsn	file "src/printv.c",line 298,column 13,is_stmt,isa 0

           VSHR    .S2     VBL0,0x10,VB14    ; [B_S2] |298| 
||         MV      .L1X    B15,A0            ; [A_L1] 

	.dwpsn	file "src/printv.c",line 293,column 22,is_stmt,isa 0
           ADDW    .D1     A0,0xffffffff,A0  ; [A_D1] |293| 

   [ A0]   B       .B1     ||$C$L33||        ; [A_B] |293| 
||         MV      .L2X    A0,B15            ; [B_L2] |293| 

           ; BRANCHCC OCCURS {||$C$L33||}    ; [] |293| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "src/printv.c",line 301,column 9,is_stmt,isa 0
           ADDKPC  .D1     $PCR_OFFSET(($C$SL4+0)),D0 ; [A_D1] |301| 
$C$DW$49	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$49, DW_AT_low_pc(0x00)
	.dwattr $C$DW$49, DW_AT_name("_Z11tidl_printfiPKcz")
	.dwattr $C$DW$49, DW_AT_TI_call


           CALL    .B1     ||_Z11tidl_printfiPKcz|| ; [A_B] |301| 
||         STD     .D1     D0,*SP(16)        ; [A_D1] |301| 
||         MVKU32  .L1     0,A4              ; [A_L1] |301| 

$C$RL26:   ; CALL OCCURS (||_Z11tidl_printfiPKcz||) arg:{A4} ret:{}  ; [] |301| 
;** --------------------------------------------------------------------------*
||$C$L34||:    
;          EXCLUSIVE CPU CYCLES: 10
           MVC     .S1     A11,RP            ; [A_S1] 
	.dwcfi	restore_reg, 4101
           VLD64B  .D1     *SP(32),VB14      ; [A_D1] 
	.dwcfi	restore_reg, 62
           VLD64B  .D1     *SP(96),VB15      ; [A_D1] 
	.dwcfi	restore_reg, 63

           LDD     .D1     *SP(176),A9       ; [A_D1] 
||         LDD     .D2     *SP(168),A10      ; [A_D2] 
	.dwcfi	restore_reg, 9
	.dwcfi	restore_reg, 10

           LDD     .D1     *SP(160),A11      ; [A_D1] 
||         LDD     .D2     *SP(184),A8       ; [A_D2] 

	.dwcfi	restore_reg, 11
	.dwcfi	restore_reg, 8
$C$DW$50	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$50, DW_AT_low_pc(0x00)
	.dwattr $C$DW$50, DW_AT_TI_return


           RET     .B1     ; [A_B] 
||         ADDD    .D1     SP,0xb0,SP        ; [A_D1] 

	.dwcfi	cfa_offset, 0
           ; RETURN OCCURS {RP}              ; [] 
	.dwattr $C$DW$5, DW_AT_TI_end_file("src/printv.c")
	.dwattr $C$DW$5, DW_AT_TI_end_line(0x132)
	.dwattr $C$DW$5, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$5

;******************************************************************************
;* STRINGS                                                                    *
;******************************************************************************
	.sect	".const:.string"
||$C$SL1||:	.string	"%s",0
||$C$SL2||:	.string	" %4d,",0
||$C$SL3||:	.string	" 0x%02x,",0
||$C$SL4||:	.string	10,0
||$C$SL5||:	.string	" 0x%04x,",0
||$C$SL6||:	.string	" %8d,",0
||$C$SL7||:	.string	" 0x%08x,",0
||$C$SL8||:	.string	" %16lld,",0
||$C$SL9||:	.string	" 0x%016llx,",0
||$C$SL10||:	.string	" %8.2F,",0
;*****************************************************************************
;* UNDEFINED EXTERNAL REFERENCES                                             *
;*****************************************************************************
	.global	||_Z11tidl_printfiPKcz||
	.dwattr $C$DW$CU, DW_AT_language(DW_LANG_C_plus_plus)

;***************************************************************
;* DWARF CIE ENTRIES                                           *
;***************************************************************

$C$DW$CIE	.dwcie 4101
	.dwcfi	cfa_register, 111
	.dwcfi	cfa_offset, 0
	.dwcfi	same_value, 8
	.dwcfi	same_value, 9
	.dwcfi	same_value, 10
	.dwcfi	same_value, 11
	.dwcfi	same_value, 12
	.dwcfi	same_value, 13
	.dwcfi	same_value, 14
	.dwcfi	same_value, 15
	.dwcfi	same_value, 62
	.dwcfi	same_value, 63
	.dwendentry
	.dwendtag $C$DW$CU


;******************************************************************************
;* TYPE INFORMATION                                                           *
;******************************************************************************

$C$DW$TU$2	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$2
$C$DW$T$2	.dwtag  DW_TAG_unspecified_type
	.dwattr $C$DW$T$2, DW_AT_name("void")

	.dwendtag $C$DW$TU$2


$C$DW$TU$4	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$4
$C$DW$T$4	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$4, DW_AT_encoding(DW_ATE_boolean)
	.dwattr $C$DW$T$4, DW_AT_name("bool")
	.dwattr $C$DW$T$4, DW_AT_byte_size(0x01)

	.dwendtag $C$DW$TU$4


$C$DW$TU$5	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$5
$C$DW$T$5	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$5, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$5, DW_AT_name("signed char")
	.dwattr $C$DW$T$5, DW_AT_byte_size(0x01)

	.dwendtag $C$DW$TU$5


$C$DW$TU$45	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$45

$C$DW$T$45	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$45, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$T$45, DW_AT_GNU_vector
	.dwattr $C$DW$T$45, DW_AT_byte_size(0x40)
$C$DW$51	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$51, DW_AT_upper_bound(0x3f)

	.dwendtag $C$DW$T$45

	.dwendtag $C$DW$TU$45


$C$DW$TU$46	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$46
$C$DW$T$46	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$46, DW_AT_name("char64")
	.dwattr $C$DW$T$46, DW_AT_type(*$C$DW$T$45)
	.dwattr $C$DW$T$46, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$T$46, DW_AT_decl_line(0x73)
	.dwattr $C$DW$T$46, DW_AT_decl_column(0x05)

	.dwendtag $C$DW$TU$46


$C$DW$TU$6	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$6
$C$DW$T$6	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$6, DW_AT_encoding(DW_ATE_unsigned_char)
	.dwattr $C$DW$T$6, DW_AT_name("unsigned char")
	.dwattr $C$DW$T$6, DW_AT_byte_size(0x01)

	.dwendtag $C$DW$TU$6


$C$DW$TU$52	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$52

$C$DW$T$52	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$52, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$52, DW_AT_GNU_vector
	.dwattr $C$DW$T$52, DW_AT_byte_size(0x40)
$C$DW$52	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$52, DW_AT_upper_bound(0x3f)

	.dwendtag $C$DW$T$52

	.dwendtag $C$DW$TU$52


$C$DW$TU$53	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$53
$C$DW$T$53	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$53, DW_AT_name("uchar64")
	.dwattr $C$DW$T$53, DW_AT_type(*$C$DW$T$52)
	.dwattr $C$DW$T$53, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$T$53, DW_AT_decl_line(0x73)
	.dwattr $C$DW$T$53, DW_AT_decl_column(0x05)

	.dwendtag $C$DW$TU$53


$C$DW$TU$7	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$7
$C$DW$T$7	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$7, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$7, DW_AT_name("wchar_t")
	.dwattr $C$DW$T$7, DW_AT_byte_size(0x04)

	.dwendtag $C$DW$TU$7


$C$DW$TU$8	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$8
$C$DW$T$8	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$8, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$8, DW_AT_name("short")
	.dwattr $C$DW$T$8, DW_AT_byte_size(0x02)

	.dwendtag $C$DW$TU$8


$C$DW$TU$56	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$56

$C$DW$T$56	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$56, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$T$56, DW_AT_GNU_vector
	.dwattr $C$DW$T$56, DW_AT_byte_size(0x40)
$C$DW$53	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$53, DW_AT_upper_bound(0x1f)

	.dwendtag $C$DW$T$56

	.dwendtag $C$DW$TU$56


$C$DW$TU$57	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$57
$C$DW$T$57	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$57, DW_AT_name("short32")
	.dwattr $C$DW$T$57, DW_AT_type(*$C$DW$T$56)
	.dwattr $C$DW$T$57, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$T$57, DW_AT_decl_line(0x73)
	.dwattr $C$DW$T$57, DW_AT_decl_column(0x05)

	.dwendtag $C$DW$TU$57


$C$DW$TU$9	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$9
$C$DW$T$9	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$9, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$9, DW_AT_name("unsigned short")
	.dwattr $C$DW$T$9, DW_AT_byte_size(0x02)

	.dwendtag $C$DW$TU$9


$C$DW$TU$60	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$60

$C$DW$T$60	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$60, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$T$60, DW_AT_GNU_vector
	.dwattr $C$DW$T$60, DW_AT_byte_size(0x40)
$C$DW$54	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$54, DW_AT_upper_bound(0x1f)

	.dwendtag $C$DW$T$60

	.dwendtag $C$DW$TU$60


$C$DW$TU$61	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$61
$C$DW$T$61	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$61, DW_AT_name("ushort32")
	.dwattr $C$DW$T$61, DW_AT_type(*$C$DW$T$60)
	.dwattr $C$DW$T$61, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$T$61, DW_AT_decl_line(0x73)
	.dwattr $C$DW$T$61, DW_AT_decl_column(0x05)

	.dwendtag $C$DW$TU$61


$C$DW$TU$10	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$10
$C$DW$T$10	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$10, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$10, DW_AT_name("int")
	.dwattr $C$DW$T$10, DW_AT_byte_size(0x04)

	.dwendtag $C$DW$TU$10


$C$DW$TU$64	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$64

$C$DW$T$64	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$64, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$T$64, DW_AT_GNU_vector
	.dwattr $C$DW$T$64, DW_AT_byte_size(0x40)
$C$DW$55	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$55, DW_AT_upper_bound(0x0f)

	.dwendtag $C$DW$T$64

	.dwendtag $C$DW$TU$64


$C$DW$TU$65	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$65
$C$DW$T$65	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$65, DW_AT_name("int16")
	.dwattr $C$DW$T$65, DW_AT_type(*$C$DW$T$64)
	.dwattr $C$DW$T$65, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$T$65, DW_AT_decl_line(0x73)
	.dwattr $C$DW$T$65, DW_AT_decl_column(0x05)

	.dwendtag $C$DW$TU$65


$C$DW$TU$25	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$25
$C$DW$T$25	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$25, DW_AT_name("__int32_t")
	.dwattr $C$DW$T$25, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$T$25, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/machine/_types.h")
	.dwattr $C$DW$T$25, DW_AT_decl_line(0x64)
	.dwattr $C$DW$T$25, DW_AT_decl_column(0x0f)

	.dwendtag $C$DW$TU$25


$C$DW$TU$26	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$26
$C$DW$T$26	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$26, DW_AT_name("int32_t")
	.dwattr $C$DW$T$26, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$T$26, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/sys/_stdint.h")
	.dwattr $C$DW$T$26, DW_AT_decl_line(0x30)
	.dwattr $C$DW$T$26, DW_AT_decl_column(0x14)

	.dwendtag $C$DW$TU$26


$C$DW$TU$11	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$11
$C$DW$T$11	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$11, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$11, DW_AT_name("unsigned int")
	.dwattr $C$DW$T$11, DW_AT_byte_size(0x04)

	.dwendtag $C$DW$TU$11


$C$DW$TU$68	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$68

$C$DW$T$68	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$68, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$T$68, DW_AT_GNU_vector
	.dwattr $C$DW$T$68, DW_AT_byte_size(0x40)
$C$DW$56	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$56, DW_AT_upper_bound(0x0f)

	.dwendtag $C$DW$T$68

	.dwendtag $C$DW$TU$68


$C$DW$TU$69	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$69
$C$DW$T$69	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$69, DW_AT_name("uint16")
	.dwattr $C$DW$T$69, DW_AT_type(*$C$DW$T$68)
	.dwattr $C$DW$T$69, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$T$69, DW_AT_decl_line(0x73)
	.dwattr $C$DW$T$69, DW_AT_decl_column(0x05)

	.dwendtag $C$DW$TU$69


$C$DW$TU$33	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$33
$C$DW$T$33	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$33, DW_AT_name("__uint32_t")
	.dwattr $C$DW$T$33, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$T$33, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/machine/_types.h")
	.dwattr $C$DW$T$33, DW_AT_decl_line(0x65)
	.dwattr $C$DW$T$33, DW_AT_decl_column(0x17)

	.dwendtag $C$DW$TU$33


$C$DW$TU$34	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$34
$C$DW$T$34	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$34, DW_AT_name("uint32_t")
	.dwattr $C$DW$T$34, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$T$34, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/sys/_stdint.h")
	.dwattr $C$DW$T$34, DW_AT_decl_line(0x46)
	.dwattr $C$DW$T$34, DW_AT_decl_column(0x15)

	.dwendtag $C$DW$TU$34


$C$DW$TU$12	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$12
$C$DW$T$12	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$12, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$12, DW_AT_name("__int40_t")
	.dwattr $C$DW$T$12, DW_AT_byte_size(0x08)

	.dwendtag $C$DW$TU$12


$C$DW$TU$31	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$31

$C$DW$T$31	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$31, DW_AT_type(*$C$DW$T$12)
	.dwattr $C$DW$T$31, DW_AT_GNU_vector
	.dwattr $C$DW$T$31, DW_AT_byte_size(0x40)
$C$DW$57	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$57, DW_AT_upper_bound(0x07)

	.dwendtag $C$DW$T$31

	.dwendtag $C$DW$TU$31


$C$DW$TU$32	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$32
$C$DW$T$32	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$32, DW_AT_name("long8")
	.dwattr $C$DW$T$32, DW_AT_type(*$C$DW$T$31)
	.dwattr $C$DW$T$32, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$T$32, DW_AT_decl_line(0x73)
	.dwattr $C$DW$T$32, DW_AT_decl_column(0x05)

	.dwendtag $C$DW$TU$32


$C$DW$TU$13	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$13
$C$DW$T$13	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$13, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$13, DW_AT_name("unsigned __int40_t")
	.dwattr $C$DW$T$13, DW_AT_byte_size(0x08)

	.dwendtag $C$DW$TU$13


$C$DW$TU$76	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$76

$C$DW$T$76	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$76, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$76, DW_AT_GNU_vector
	.dwattr $C$DW$T$76, DW_AT_byte_size(0x40)
$C$DW$58	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$58, DW_AT_upper_bound(0x07)

	.dwendtag $C$DW$T$76

	.dwendtag $C$DW$TU$76


$C$DW$TU$77	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$77
$C$DW$T$77	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$77, DW_AT_name("ulong8")
	.dwattr $C$DW$T$77, DW_AT_type(*$C$DW$T$76)
	.dwattr $C$DW$T$77, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$T$77, DW_AT_decl_line(0x73)
	.dwattr $C$DW$T$77, DW_AT_decl_column(0x05)

	.dwendtag $C$DW$TU$77


$C$DW$TU$14	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$14
$C$DW$T$14	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$14, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$14, DW_AT_name("long long")
	.dwattr $C$DW$T$14, DW_AT_byte_size(0x08)

	.dwendtag $C$DW$TU$14


$C$DW$TU$15	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$15
$C$DW$T$15	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$15, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$15, DW_AT_name("unsigned long long")
	.dwattr $C$DW$T$15, DW_AT_byte_size(0x08)

	.dwendtag $C$DW$TU$15


$C$DW$TU$16	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$16
$C$DW$T$16	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$16, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$16, DW_AT_name("float")
	.dwattr $C$DW$T$16, DW_AT_byte_size(0x04)

	.dwendtag $C$DW$TU$16


$C$DW$TU$80	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$80

$C$DW$T$80	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$80, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$T$80, DW_AT_GNU_vector
	.dwattr $C$DW$T$80, DW_AT_byte_size(0x40)
$C$DW$59	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$59, DW_AT_upper_bound(0x0f)

	.dwendtag $C$DW$T$80

	.dwendtag $C$DW$TU$80


$C$DW$TU$81	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$81
$C$DW$T$81	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$81, DW_AT_name("float16")
	.dwattr $C$DW$T$81, DW_AT_type(*$C$DW$T$80)
	.dwattr $C$DW$T$81, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$T$81, DW_AT_decl_line(0x73)
	.dwattr $C$DW$T$81, DW_AT_decl_column(0x05)

	.dwendtag $C$DW$TU$81


$C$DW$TU$84	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$84
$C$DW$T$84	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$84, DW_AT_name("float32_tidl")
	.dwattr $C$DW$T$84, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$T$84, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$84, DW_AT_decl_line(0x71)
	.dwattr $C$DW$T$84, DW_AT_decl_column(0x0f)

	.dwendtag $C$DW$TU$84


$C$DW$TU$17	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$17
$C$DW$T$17	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$17, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$17, DW_AT_name("double")
	.dwattr $C$DW$T$17, DW_AT_byte_size(0x08)

	.dwendtag $C$DW$TU$17


$C$DW$TU$18	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$18
$C$DW$T$18	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$18, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$18, DW_AT_name("long double")
	.dwattr $C$DW$T$18, DW_AT_byte_size(0x08)

	.dwendtag $C$DW$TU$18


$C$DW$TU$19	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$19
$C$DW$T$19	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$19, DW_AT_encoding(DW_ATE_TI_complex_signed)
	.dwattr $C$DW$T$19, DW_AT_name("cchar")
	.dwattr $C$DW$T$19, DW_AT_byte_size(0x02)

	.dwendtag $C$DW$TU$19


$C$DW$TU$20	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$20
$C$DW$T$20	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$20, DW_AT_encoding(DW_ATE_TI_complex_signed)
	.dwattr $C$DW$T$20, DW_AT_name("cshort")
	.dwattr $C$DW$T$20, DW_AT_byte_size(0x04)

	.dwendtag $C$DW$TU$20


$C$DW$TU$21	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$21
$C$DW$T$21	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$21, DW_AT_encoding(DW_ATE_TI_complex_signed)
	.dwattr $C$DW$T$21, DW_AT_name("cint")
	.dwattr $C$DW$T$21, DW_AT_byte_size(0x08)

	.dwendtag $C$DW$TU$21


$C$DW$TU$22	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$22
$C$DW$T$22	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$22, DW_AT_encoding(DW_ATE_TI_complex_signed)
	.dwattr $C$DW$T$22, DW_AT_name("clong")
	.dwattr $C$DW$T$22, DW_AT_byte_size(0x10)

	.dwendtag $C$DW$TU$22


$C$DW$TU$23	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$23
$C$DW$T$23	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$23, DW_AT_encoding(DW_ATE_TI_complex_float)
	.dwattr $C$DW$T$23, DW_AT_name("cfloat")
	.dwattr $C$DW$T$23, DW_AT_byte_size(0x08)

	.dwendtag $C$DW$TU$23


$C$DW$TU$24	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$24
$C$DW$T$24	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$24, DW_AT_encoding(DW_ATE_TI_complex_float)
	.dwattr $C$DW$T$24, DW_AT_name("cdouble")
	.dwattr $C$DW$T$24, DW_AT_byte_size(0x10)

	.dwendtag $C$DW$TU$24


$C$DW$TU$28	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$28
$C$DW$T$28	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$28, DW_AT_type(*$C$DW$T$5)

	.dwendtag $C$DW$TU$28


$C$DW$TU$29	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$29
$C$DW$T$29	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$29, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$T$29, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$29

