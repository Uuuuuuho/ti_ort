;******************************************************************************
;* C7x G3 C/C++ Codegen                                            Unix v1.4.2.LTS *
;* Date/Time created: Tue Oct  5 15:11:17 2021                                *
;******************************************************************************
	.compiler_opts --abi=eabi --endian=little --hll_source=on --object_format=elf --silicon_errata_i2117 --silicon_version=7100 --symdebug:dwarf --symdebug:dwarf_version=4 

;******************************************************************************
;* GLOBAL FILE PARAMETERS                                                     *
;*                                                                            *
;*   Architecture      : C7100                                                *
;*   Endian            : Little                                               *
;*   Pipelining        : Enabled                                              *
;*   Debug Info        : DWARF Debug                                          *
;*                                                                            *
;******************************************************************************

	.asg	D14, FP
	.asg	D15, SP
	.global	$bss


$C$DW$CU	.dwtag  DW_TAG_compile_unit
	.dwattr $C$DW$CU, DW_AT_name("tidl_custom.c")
	.dwattr $C$DW$CU, DW_AT_producer("TI C7x G3 C/C++ Codegen Unix v1.4.2.LTS Copyright (c) 2014-2019 Texas Instruments Incorporated")
	.dwattr $C$DW$CU, DW_AT_TI_version(0x01)
	.dwattr $C$DW$CU, DW_AT_comp_dir("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/tidl_j7_08_00_00_10/ti_dl/custom")

$C$DW$1	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1, DW_AT_name("TIDL_customMaxPoolingProcess")
	.dwattr $C$DW$1, DW_AT_linkage_name("_Z28TIDL_customMaxPoolingProcessPvP13sTIDL_Layer_tPS_S2_S_S_PK20sTIDL_sysMemHandle_tif")
	.dwattr $C$DW$1, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1, DW_AT_declaration
	.dwattr $C$DW$1, DW_AT_external
	.dwattr $C$DW$1, DW_AT_decl_file("tidl_custom_maxpooling.h")
	.dwattr $C$DW$1, DW_AT_decl_line(0x67)
	.dwattr $C$DW$1, DW_AT_decl_column(0x09)
$C$DW$2	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$2, DW_AT_type(*$C$DW$T$3)

$C$DW$3	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$3, DW_AT_type(*$C$DW$T$285)

$C$DW$4	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$4, DW_AT_type(*$C$DW$T$273)

$C$DW$5	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$5, DW_AT_type(*$C$DW$T$273)

$C$DW$6	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$6, DW_AT_type(*$C$DW$T$3)

$C$DW$7	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$7, DW_AT_type(*$C$DW$T$3)

$C$DW$8	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$8, DW_AT_type(*$C$DW$T$288)

$C$DW$9	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$9, DW_AT_type(*$C$DW$T$26)

$C$DW$10	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$10, DW_AT_type(*$C$DW$T$16)

	.dwendtag $C$DW$1

;	/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/bin/opt7x /tmp/TI4XPIzfP0X /tmp/TI4XP5CVTwl 
	.sect	".text:TIDL_customLayerProcess"
	.clink
	.global	||TIDL_customLayerProcess||

$C$DW$63	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$63, DW_AT_name("TIDL_customLayerProcess")
	.dwattr $C$DW$63, DW_AT_low_pc(||TIDL_customLayerProcess||)
	.dwattr $C$DW$63, DW_AT_high_pc(0x00)
	.dwattr $C$DW$63, DW_AT_linkage_name("TIDL_customLayerProcess")
	.dwattr $C$DW$63, DW_AT_external
	.dwattr $C$DW$63, DW_AT_decl_file("tidl_custom.c")
	.dwattr $C$DW$63, DW_AT_decl_line(0x84)
	.dwattr $C$DW$63, DW_AT_decl_column(0x09)
	.dwattr $C$DW$63, DW_AT_TI_max_frame_size(0x10)
	.dwpsn	file "tidl_custom.c",line 141,column 1,is_stmt,address ||TIDL_customLayerProcess||,isa 0

	.dwfde $C$DW$CIE, ||TIDL_customLayerProcess||
$C$DW$64	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$64, DW_AT_name("tidlHandle")
	.dwattr $C$DW$64, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$64, DW_AT_location[DW_OP_reg4]

$C$DW$65	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$65, DW_AT_name("tidlLayer")
	.dwattr $C$DW$65, DW_AT_location[DW_OP_reg5]

$C$DW$66	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$66, DW_AT_name("inPtrs")
	.dwattr $C$DW$66, DW_AT_type(*$C$DW$T$273)
	.dwattr $C$DW$66, DW_AT_location[DW_OP_reg6]

$C$DW$67	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$67, DW_AT_name("outPtrs")
	.dwattr $C$DW$67, DW_AT_type(*$C$DW$T$273)
	.dwattr $C$DW$67, DW_AT_location[DW_OP_reg7]

$C$DW$68	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$68, DW_AT_name("params")
	.dwattr $C$DW$68, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$68, DW_AT_location[DW_OP_reg8]

$C$DW$69	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$69, DW_AT_name("dmaUtilsContext")
	.dwattr $C$DW$69, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$69, DW_AT_location[DW_OP_reg9]

$C$DW$70	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$70, DW_AT_name("sysMems")
	.dwattr $C$DW$70, DW_AT_location[DW_OP_reg10]

$C$DW$71	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$71, DW_AT_name("execMode")
	.dwattr $C$DW$71, DW_AT_location[DW_OP_reg11]

$C$DW$72	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$72, DW_AT_name("maxTensorScale")
	.dwattr $C$DW$72, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$72, DW_AT_location[DW_OP_reg12]


;******************************************************************************
;* FUNCTION NAME: TIDL_customLayerProcess                                     *
;*                                                                            *
;*   Regs Modified     : A0,A1,A2,A3,A4,A5,A6,A7,A10,A13,VB0,VB1,VB2,VB3,VB4, *
;*                           VB5,VB6,VB7,VB8,VB9,VB10,VB11,VB12,VB13,AL0,AL1, *
;*                           AL2,AL3,AL4,AL5,AL6,AL7,AM0,AM1,AM2,AM3,AM4,AM5, *
;*                           AM6,AM7,D0,D1,D2,D3,D4,D5,D6,D7,D8,D9,D10,D11,   *
;*                           D12,D13,D14,SP,VBL0,VBL1,VBL2,VBL3,VBL4,VBL5,    *
;*                           VBL6,VBL7,VBM0,VBM1,VBM2,VBM3,VBM4,VBM5,VBM6,    *
;*                           VBM7,P0,P1,P2,P3,P4,P5,P6,P7,CUCR0,CUCR1,CUCR2,  *
;*                           CUCR3                                            *
;*   Regs Used         : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,VB0,   *
;*                           VB1,VB2,VB3,VB4,VB5,VB6,VB7,VB8,VB9,VB10,VB11,   *
;*                           VB12,VB13,AL0,AL1,AL2,AL3,AL4,AL5,AL6,AL7,AM0,   *
;*                           AM1,AM2,AM3,AM4,AM5,AM6,AM7,D0,D1,D2,D3,D4,D5,D6,*
;*                           D7,D8,D9,D10,D11,D12,D13,D14,SP,VBL0,VBL1,VBL2,  *
;*                           VBL3,VBL4,VBL5,VBL6,VBL7,VBM0,VBM1,VBM2,VBM3,    *
;*                           VBM4,VBM5,VBM6,VBM7,P0,P1,P2,P3,P4,P5,P6,P7,     *
;*                           CUCR0,CUCR1,CUCR2,CUCR3                          *
;*   Local Frame Size  : 0 Args + 0 Auto + 16 Save = 16 byte                  *
;******************************************************************************
||TIDL_customLayerProcess||:
;** --------------------------------------------------------------------------*
;* VB0   assigned to $O$C1
;* D0    assigned to $O$Lr9$status
;* A5    assigned to tidlLayer
$C$DW$73	.dwtag  DW_TAG_variable
	.dwattr $C$DW$73, DW_AT_name("tidlLayer")
	.dwattr $C$DW$73, DW_AT_type(*$C$DW$T$285)
	.dwattr $C$DW$73, DW_AT_location[DW_OP_reg5]

	.dwcfi	cfa_offset, 0
;          EXCLUSIVE CPU CYCLES: 16
	.dwpsn	file "tidl_custom.c",line 144,column 3,is_stmt,isa 0
           LDW     .D1     *A5(0),B0         ; [A_D1] |144| 

           CMPEQW  .L1X    B0,0x3,AL0        ; [A_L1] 
||         CMPEQW  .L2     B0,0x1,BL0        ; [B_L2] 
||         CMPEQW  .S2     B0,0x2,BL1        ; [B_S2] 

           XORD    .L2     BL1,0x1,BL1       ; [B_L2] 
||         XORD    .S2     BL0,0x1,BL0       ; [B_S2] 
||         XORD    .L1     AL0,0x1,A0        ; [A_L1] 

           ANDW    .L2     BL1,BL0,BL0       ; [B_L2] 
           ANDW    .L2X    A0,BL0,B1         ; [B_L2] 

           MVC     .S1     RP,A13            ; [A_S1] 
||         STD     .D1     A10,*SP(8)        ; [A_D1] 
||         STD     .D2X    A13,*SP++(-16)    ; [A_D2] 

	.dwcfi	save_reg_to_reg, 4101, 13
	.dwcfi	save_reg_to_mem, 10, 8
	.dwcfi	cfa_offset, 16
	.dwcfi	save_reg_to_mem, 13, -16
           CMPEQW  .L1X    B1,0,A1           ; [A_L1] 

           CMPEQW  .L1X    B0,0,A0           ; [A_L1] |144| 
||         MVKU32  .S1     0,D0              ; [A_S1] 

	.dwpsn	file "tidl_custom.c",line 173,column 3,is_stmt,isa 0

   [ A0]   MVKU32  .L1     0,A1              ; [A_L1] |173| 
|| [!A0]   MVK32   .S1     0xffffffff,D1     ; [A_S1] |173| 

   [!A0]   B       .B1     ||$C$L1||         ; [A_B] |173| 
|| [ A1]   MV      .D1     D0,D1             ; [A_D1] |173| 

           ; BRANCHCC OCCURS {||$C$L1||}     ; [] |173| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 3
	.dwpsn	file "tidl_custom.c",line 146,column 5,is_stmt,isa 0
$C$DW$74	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$74, DW_AT_low_pc(0x00)
	.dwattr $C$DW$74, DW_AT_name("_Z28TIDL_customMaxPoolingProcessPvP13sTIDL_Layer_tPS_S2_S_S_PK20sTIDL_sysMemHandle_tif")
	.dwattr $C$DW$74, DW_AT_TI_call

           CALL    .B1     ||_Z28TIDL_customMaxPoolingProcessPvP13sTIDL_Layer_tPS_S2_S_S_PK20sTIDL_sysMemHandle_tif|| ; [A_B] |146| 
$C$RL0:    ; CALL OCCURS (||_Z28TIDL_customMaxPoolingProcessPvP13sTIDL_Layer_tPS_S2_S_S_PK20sTIDL_sysMemHandle_tif||) arg:{A4,A5,A6,A7,A8,A9,A10,A11,A12} ret:{A4}  ; [] |146| 
           MV      .D1     A4,D0             ; [A_D1] |146| 
	.dwpsn	file "tidl_custom.c",line 173,column 3,is_stmt,isa 0
           MV      .D1     D0,D1             ; [A_D1] |173| 
;** --------------------------------------------------------------------------*
||$C$L1||:    
;          EXCLUSIVE CPU CYCLES: 7

           MVC     .S1     A13,RP            ; [A_S1] |173| 
||         MV      .D1     D1,A4             ; [A_D1] |173| 
	.dwcfi	restore_reg, 4101

           LDD     .D1     *SP(16),A13       ; [A_D1] |173| 
||         LDD     .D2     *SP(24),A10       ; [A_D2] |173| 

	.dwcfi	restore_reg, 13
	.dwcfi	restore_reg, 10
$C$DW$75	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$75, DW_AT_low_pc(0x00)
	.dwattr $C$DW$75, DW_AT_TI_return


           RET     .B1     ; [A_B] |173| 
||         ADDD    .D1     SP,0x10,SP        ; [A_D1] |173| 

	.dwcfi	cfa_offset, 0
           ; RETURN OCCURS {RP}              ; [] |173| 
	.dwattr $C$DW$63, DW_AT_TI_end_file("tidl_custom.c")
	.dwattr $C$DW$63, DW_AT_TI_end_line(0xae)
	.dwattr $C$DW$63, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$63

;*****************************************************************************
;* UNDEFINED EXTERNAL REFERENCES                                             *
;*****************************************************************************
	.global	||_Z28TIDL_customMaxPoolingProcessPvP13sTIDL_Layer_tPS_S2_S_S_PK20sTIDL_sysMemHandle_tif||
	.dwattr $C$DW$CU, DW_AT_language(DW_LANG_C_plus_plus)

;***************************************************************
;* DWARF CIE ENTRIES                                           *
;***************************************************************

$C$DW$CIE	.dwcie 4101
	.dwcfi	cfa_register, 111
	.dwcfi	cfa_offset, 0
	.dwcfi	same_value, 8
	.dwcfi	same_value, 9
	.dwcfi	same_value, 10
	.dwcfi	same_value, 11
	.dwcfi	same_value, 12
	.dwcfi	same_value, 13
	.dwcfi	same_value, 14
	.dwcfi	same_value, 15
	.dwcfi	same_value, 62
	.dwcfi	same_value, 63
	.dwendentry
	.dwendtag $C$DW$CU


;******************************************************************************
;* TYPE INFORMATION                                                           *
;******************************************************************************

$C$DW$TU$2	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$2
$C$DW$T$2	.dwtag  DW_TAG_unspecified_type
	.dwattr $C$DW$T$2, DW_AT_name("void")

	.dwendtag $C$DW$TU$2


$C$DW$TU$3	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$3
$C$DW$T$3	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$3, DW_AT_type(*$C$DW$T$2)
	.dwattr $C$DW$T$3, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$3


$C$DW$TU$273	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$273
$C$DW$T$273	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$273, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$T$273, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$273


$C$DW$TU$4	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$4
$C$DW$T$4	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$4, DW_AT_encoding(DW_ATE_boolean)
	.dwattr $C$DW$T$4, DW_AT_name("bool")
	.dwattr $C$DW$T$4, DW_AT_byte_size(0x01)

	.dwendtag $C$DW$TU$4


$C$DW$TU$5	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$5
$C$DW$T$5	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$5, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$5, DW_AT_name("signed char")
	.dwattr $C$DW$T$5, DW_AT_byte_size(0x01)

	.dwendtag $C$DW$TU$5


$C$DW$TU$6	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$6
$C$DW$T$6	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$6, DW_AT_encoding(DW_ATE_unsigned_char)
	.dwattr $C$DW$T$6, DW_AT_name("unsigned char")
	.dwattr $C$DW$T$6, DW_AT_byte_size(0x01)

	.dwendtag $C$DW$TU$6


$C$DW$TU$7	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$7
$C$DW$T$7	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$7, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$7, DW_AT_name("wchar_t")
	.dwattr $C$DW$T$7, DW_AT_byte_size(0x04)

	.dwendtag $C$DW$TU$7


$C$DW$TU$8	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$8
$C$DW$T$8	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$8, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$8, DW_AT_name("short")
	.dwattr $C$DW$T$8, DW_AT_byte_size(0x02)

	.dwendtag $C$DW$TU$8


$C$DW$TU$9	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$9
$C$DW$T$9	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$9, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$9, DW_AT_name("unsigned short")
	.dwattr $C$DW$T$9, DW_AT_byte_size(0x02)

	.dwendtag $C$DW$TU$9


$C$DW$TU$10	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$10
$C$DW$T$10	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$10, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$10, DW_AT_name("int")
	.dwattr $C$DW$T$10, DW_AT_byte_size(0x04)

	.dwendtag $C$DW$TU$10


$C$DW$TU$25	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$25
$C$DW$T$25	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$25, DW_AT_name("__int32_t")
	.dwattr $C$DW$T$25, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$T$25, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/machine/_types.h")
	.dwattr $C$DW$T$25, DW_AT_decl_line(0x64)
	.dwattr $C$DW$T$25, DW_AT_decl_column(0x0f)

	.dwendtag $C$DW$TU$25


$C$DW$TU$26	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$26
$C$DW$T$26	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$26, DW_AT_name("int32_t")
	.dwattr $C$DW$T$26, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$T$26, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/sys/_stdint.h")
	.dwattr $C$DW$T$26, DW_AT_decl_line(0x30)
	.dwattr $C$DW$T$26, DW_AT_decl_column(0x14)

	.dwendtag $C$DW$TU$26


$C$DW$TU$96	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$96

$C$DW$T$96	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$96, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$T$96, DW_AT_byte_size(0x0c)
$C$DW$76	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$76, DW_AT_upper_bound(0x02)

	.dwendtag $C$DW$T$96

	.dwendtag $C$DW$TU$96


$C$DW$TU$97	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$97

$C$DW$T$97	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$97, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$T$97, DW_AT_byte_size(0x10)
$C$DW$77	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$77, DW_AT_upper_bound(0x03)

	.dwendtag $C$DW$T$97

	.dwendtag $C$DW$TU$97


$C$DW$TU$121	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$121

$C$DW$T$121	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$121, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$T$121, DW_AT_byte_size(0x40)
$C$DW$78	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$78, DW_AT_upper_bound(0x0f)

	.dwendtag $C$DW$T$121

	.dwendtag $C$DW$TU$121


$C$DW$TU$236	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$236

$C$DW$T$236	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$236, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$T$236, DW_AT_byte_size(0x44)
$C$DW$79	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$79, DW_AT_upper_bound(0x10)

	.dwendtag $C$DW$T$236

	.dwendtag $C$DW$TU$236


$C$DW$TU$11	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$11
$C$DW$T$11	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$11, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$11, DW_AT_name("unsigned int")
	.dwattr $C$DW$T$11, DW_AT_byte_size(0x04)

	.dwendtag $C$DW$TU$11


$C$DW$TU$12	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$12
$C$DW$T$12	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$12, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$12, DW_AT_name("__int40_t")
	.dwattr $C$DW$T$12, DW_AT_byte_size(0x08)

	.dwendtag $C$DW$TU$12


$C$DW$TU$13	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$13
$C$DW$T$13	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$13, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$13, DW_AT_name("unsigned __int40_t")
	.dwattr $C$DW$T$13, DW_AT_byte_size(0x08)

	.dwendtag $C$DW$TU$13


$C$DW$TU$14	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$14
$C$DW$T$14	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$14, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$14, DW_AT_name("long long")
	.dwattr $C$DW$T$14, DW_AT_byte_size(0x08)

	.dwendtag $C$DW$TU$14


$C$DW$TU$15	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$15
$C$DW$T$15	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$15, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$15, DW_AT_name("unsigned long long")
	.dwattr $C$DW$T$15, DW_AT_byte_size(0x08)

	.dwendtag $C$DW$TU$15


$C$DW$TU$16	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$16
$C$DW$T$16	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$16, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$16, DW_AT_name("float")
	.dwattr $C$DW$T$16, DW_AT_byte_size(0x04)

	.dwendtag $C$DW$TU$16


$C$DW$TU$27	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$27
$C$DW$T$27	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$27, DW_AT_name("float32_tidl")
	.dwattr $C$DW$T$27, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$T$27, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$27, DW_AT_decl_line(0x71)
	.dwattr $C$DW$T$27, DW_AT_decl_column(0x0f)

	.dwendtag $C$DW$TU$27


$C$DW$TU$213	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$213

$C$DW$T$213	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$213, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$T$213, DW_AT_byte_size(0x10)
$C$DW$80	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$80, DW_AT_upper_bound(0x03)

	.dwendtag $C$DW$T$213

	.dwendtag $C$DW$TU$213


$C$DW$TU$17	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$17
$C$DW$T$17	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$17, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$17, DW_AT_name("double")
	.dwattr $C$DW$T$17, DW_AT_byte_size(0x08)

	.dwendtag $C$DW$TU$17


$C$DW$TU$18	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$18
$C$DW$T$18	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$18, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$18, DW_AT_name("long double")
	.dwattr $C$DW$T$18, DW_AT_byte_size(0x08)

	.dwendtag $C$DW$TU$18


$C$DW$TU$19	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$19
$C$DW$T$19	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$19, DW_AT_encoding(DW_ATE_TI_complex_signed)
	.dwattr $C$DW$T$19, DW_AT_name("cchar")
	.dwattr $C$DW$T$19, DW_AT_byte_size(0x02)

	.dwendtag $C$DW$TU$19


$C$DW$TU$20	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$20
$C$DW$T$20	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$20, DW_AT_encoding(DW_ATE_TI_complex_signed)
	.dwattr $C$DW$T$20, DW_AT_name("cshort")
	.dwattr $C$DW$T$20, DW_AT_byte_size(0x04)

	.dwendtag $C$DW$TU$20


$C$DW$TU$21	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$21
$C$DW$T$21	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$21, DW_AT_encoding(DW_ATE_TI_complex_signed)
	.dwattr $C$DW$T$21, DW_AT_name("cint")
	.dwattr $C$DW$T$21, DW_AT_byte_size(0x08)

	.dwendtag $C$DW$TU$21


$C$DW$TU$22	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$22
$C$DW$T$22	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$22, DW_AT_encoding(DW_ATE_TI_complex_signed)
	.dwattr $C$DW$T$22, DW_AT_name("clong")
	.dwattr $C$DW$T$22, DW_AT_byte_size(0x10)

	.dwendtag $C$DW$TU$22


$C$DW$TU$23	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$23
$C$DW$T$23	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$23, DW_AT_encoding(DW_ATE_TI_complex_float)
	.dwattr $C$DW$T$23, DW_AT_name("cfloat")
	.dwattr $C$DW$T$23, DW_AT_byte_size(0x08)

	.dwendtag $C$DW$TU$23


$C$DW$TU$24	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$24
$C$DW$T$24	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$24, DW_AT_encoding(DW_ATE_TI_complex_float)
	.dwattr $C$DW$T$24, DW_AT_name("cdouble")
	.dwattr $C$DW$T$24, DW_AT_byte_size(0x10)

	.dwendtag $C$DW$TU$24


$C$DW$TU$33	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$33

$C$DW$T$33	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$33, DW_AT_name("sTIDL_ActParams_t")
	.dwattr $C$DW$T$33, DW_AT_byte_size(0x14)
$C$DW$81	.dwtag  DW_TAG_member
	.dwattr $C$DW$81, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$81, DW_AT_name("slope")
	.dwattr $C$DW$81, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$81, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$81, DW_AT_decl_line(0x33c)
	.dwattr $C$DW$81, DW_AT_decl_column(0x0d)

$C$DW$82	.dwtag  DW_TAG_member
	.dwattr $C$DW$82, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$82, DW_AT_name("slopeScale")
	.dwattr $C$DW$82, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$82, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$82, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$82, DW_AT_decl_line(0x33e)
	.dwattr $C$DW$82, DW_AT_decl_column(0x14)

$C$DW$83	.dwtag  DW_TAG_member
	.dwattr $C$DW$83, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$83, DW_AT_name("clipMin")
	.dwattr $C$DW$83, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$83, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$83, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$83, DW_AT_decl_line(0x340)
	.dwattr $C$DW$83, DW_AT_decl_column(0x14)

$C$DW$84	.dwtag  DW_TAG_member
	.dwattr $C$DW$84, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$84, DW_AT_name("clipMax")
	.dwattr $C$DW$84, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$84, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$84, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$84, DW_AT_decl_line(0x342)
	.dwattr $C$DW$84, DW_AT_decl_column(0x14)

$C$DW$85	.dwtag  DW_TAG_member
	.dwattr $C$DW$85, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$85, DW_AT_name("actType")
	.dwattr $C$DW$85, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$85, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$85, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$85, DW_AT_decl_line(0x344)
	.dwattr $C$DW$85, DW_AT_decl_column(0x0d)


$C$DW$86	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$86, DW_AT_name("operator =")
	.dwattr $C$DW$86, DW_AT_declaration
	.dwattr $C$DW$86, DW_AT_linkage_name("_ZN17sTIDL_ActParams_taSERKS_")
	.dwattr $C$DW$86, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$86, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$87	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$87, DW_AT_type(*$C$DW$T$30)

	.dwendtag $C$DW$86


$C$DW$88	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$88, DW_AT_name("operator =")
	.dwattr $C$DW$88, DW_AT_declaration
	.dwattr $C$DW$88, DW_AT_linkage_name("_ZN17sTIDL_ActParams_taSEOS_")
	.dwattr $C$DW$88, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$88, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$89	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$89, DW_AT_type(*$C$DW$T$28)

	.dwendtag $C$DW$88

	.dwattr $C$DW$T$33, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$33, DW_AT_decl_line(0x33a)
	.dwattr $C$DW$T$33, DW_AT_decl_column(0x10)
	.dwendtag $C$DW$T$33

	.dwendtag $C$DW$TU$33


$C$DW$TU$28	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$28
$C$DW$T$28	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$28, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$T$28, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$28


$C$DW$TU$31	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$31

$C$DW$T$31	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$31, DW_AT_type(*$C$DW$T$28)
$C$DW$90	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$90, DW_AT_type(*$C$DW$T$30)

	.dwendtag $C$DW$T$31

	.dwendtag $C$DW$TU$31


$C$DW$TU$32	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$32

$C$DW$T$32	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$32, DW_AT_type(*$C$DW$T$28)
$C$DW$91	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$91, DW_AT_type(*$C$DW$T$28)

	.dwendtag $C$DW$T$32

	.dwendtag $C$DW$TU$32


$C$DW$TU$29	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$29
$C$DW$T$29	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$29, DW_AT_type(*$C$DW$T$33)

	.dwendtag $C$DW$TU$29


$C$DW$TU$30	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$30
$C$DW$T$30	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$30, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$T$30, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$30


$C$DW$TU$185	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$185
$C$DW$T$185	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$185, DW_AT_name("sTIDL_ActParams_t")
	.dwattr $C$DW$T$185, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$T$185, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$185, DW_AT_decl_line(0x345)
	.dwattr $C$DW$T$185, DW_AT_decl_column(0x02)

	.dwendtag $C$DW$TU$185


$C$DW$TU$43	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$43

$C$DW$T$43	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$43, DW_AT_name("sTIDL_ArgMaxParams_t")
	.dwattr $C$DW$T$43, DW_AT_byte_size(0x0c)
$C$DW$92	.dwtag  DW_TAG_member
	.dwattr $C$DW$92, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$92, DW_AT_name("numChannels")
	.dwattr $C$DW$92, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$92, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$92, DW_AT_decl_line(0x379)
	.dwattr $C$DW$92, DW_AT_decl_column(0x0d)

$C$DW$93	.dwtag  DW_TAG_member
	.dwattr $C$DW$93, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$93, DW_AT_name("inDataQ")
	.dwattr $C$DW$93, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$93, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$93, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$93, DW_AT_decl_line(0x37b)
	.dwattr $C$DW$93, DW_AT_decl_column(0x0d)

$C$DW$94	.dwtag  DW_TAG_member
	.dwattr $C$DW$94, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$94, DW_AT_name("outDataQ")
	.dwattr $C$DW$94, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$94, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$94, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$94, DW_AT_decl_line(0x37d)
	.dwattr $C$DW$94, DW_AT_decl_column(0x0d)


$C$DW$95	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$95, DW_AT_name("operator =")
	.dwattr $C$DW$95, DW_AT_declaration
	.dwattr $C$DW$95, DW_AT_linkage_name("_ZN20sTIDL_ArgMaxParams_taSERKS_")
	.dwattr $C$DW$95, DW_AT_type(*$C$DW$T$38)
	.dwattr $C$DW$95, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$96	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$96, DW_AT_type(*$C$DW$T$40)

	.dwendtag $C$DW$95


$C$DW$97	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$97, DW_AT_name("operator =")
	.dwattr $C$DW$97, DW_AT_declaration
	.dwattr $C$DW$97, DW_AT_linkage_name("_ZN20sTIDL_ArgMaxParams_taSEOS_")
	.dwattr $C$DW$97, DW_AT_type(*$C$DW$T$38)
	.dwattr $C$DW$97, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$98	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$98, DW_AT_type(*$C$DW$T$38)

	.dwendtag $C$DW$97

	.dwattr $C$DW$T$43, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$43, DW_AT_decl_line(0x377)
	.dwattr $C$DW$T$43, DW_AT_decl_column(0x10)
	.dwendtag $C$DW$T$43

	.dwendtag $C$DW$TU$43


$C$DW$TU$38	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$38
$C$DW$T$38	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$38, DW_AT_type(*$C$DW$T$43)
	.dwattr $C$DW$T$38, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$38


$C$DW$TU$41	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$41

$C$DW$T$41	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$41, DW_AT_type(*$C$DW$T$38)
$C$DW$99	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$99, DW_AT_type(*$C$DW$T$40)

	.dwendtag $C$DW$T$41

	.dwendtag $C$DW$TU$41


$C$DW$TU$42	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$42

$C$DW$T$42	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$42, DW_AT_type(*$C$DW$T$38)
$C$DW$100	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$100, DW_AT_type(*$C$DW$T$38)

	.dwendtag $C$DW$T$42

	.dwendtag $C$DW$TU$42


$C$DW$TU$39	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$39
$C$DW$T$39	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$39, DW_AT_type(*$C$DW$T$43)

	.dwendtag $C$DW$TU$39


$C$DW$TU$40	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$40
$C$DW$T$40	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$40, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$T$40, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$40


$C$DW$TU$141	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$141
$C$DW$T$141	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$141, DW_AT_name("sTIDL_ArgMaxParams_t")
	.dwattr $C$DW$T$141, DW_AT_type(*$C$DW$T$43)
	.dwattr $C$DW$T$141, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$141, DW_AT_decl_line(0x37e)
	.dwattr $C$DW$T$141, DW_AT_decl_column(0x02)

	.dwendtag $C$DW$TU$141


$C$DW$TU$50	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$50

$C$DW$T$50	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$50, DW_AT_name("sTIDL_BatchNormParams_t")
	.dwattr $C$DW$T$50, DW_AT_byte_size(0x28)
$C$DW$101	.dwtag  DW_TAG_member
	.dwattr $C$DW$101, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$101, DW_AT_name("weights")
	.dwattr $C$DW$101, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$101, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$101, DW_AT_decl_line(0x4bf)
	.dwattr $C$DW$101, DW_AT_decl_column(0x0d)

$C$DW$102	.dwtag  DW_TAG_member
	.dwattr $C$DW$102, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$102, DW_AT_name("bias")
	.dwattr $C$DW$102, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$102, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$102, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$102, DW_AT_decl_line(0x4c1)
	.dwattr $C$DW$102, DW_AT_decl_column(0x0d)

$C$DW$103	.dwtag  DW_TAG_member
	.dwattr $C$DW$103, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$103, DW_AT_name("numChannels")
	.dwattr $C$DW$103, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$103, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$103, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$103, DW_AT_decl_line(0x4c3)
	.dwattr $C$DW$103, DW_AT_decl_column(0x0d)

$C$DW$104	.dwtag  DW_TAG_member
	.dwattr $C$DW$104, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$104, DW_AT_name("biasQ")
	.dwattr $C$DW$104, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$104, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$104, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$104, DW_AT_decl_line(0x4c5)
	.dwattr $C$DW$104, DW_AT_decl_column(0x0d)

$C$DW$105	.dwtag  DW_TAG_member
	.dwattr $C$DW$105, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$105, DW_AT_name("inDataQ")
	.dwattr $C$DW$105, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$105, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$105, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$105, DW_AT_decl_line(0x4c7)
	.dwattr $C$DW$105, DW_AT_decl_column(0x0d)

$C$DW$106	.dwtag  DW_TAG_member
	.dwattr $C$DW$106, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$106, DW_AT_name("outDataQ")
	.dwattr $C$DW$106, DW_AT_data_member_location[DW_OP_plus_uconst 0x14]
	.dwattr $C$DW$106, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$106, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$106, DW_AT_decl_line(0x4c9)
	.dwattr $C$DW$106, DW_AT_decl_column(0x0d)

$C$DW$107	.dwtag  DW_TAG_member
	.dwattr $C$DW$107, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$107, DW_AT_name("weightsQ")
	.dwattr $C$DW$107, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$107, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$107, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$107, DW_AT_decl_line(0x4cb)
	.dwattr $C$DW$107, DW_AT_decl_column(0x0d)

$C$DW$108	.dwtag  DW_TAG_member
	.dwattr $C$DW$108, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$108, DW_AT_name("weightScale")
	.dwattr $C$DW$108, DW_AT_data_member_location[DW_OP_plus_uconst 0x1c]
	.dwattr $C$DW$108, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$108, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$108, DW_AT_decl_line(0x4cd)
	.dwattr $C$DW$108, DW_AT_decl_column(0x10)

$C$DW$109	.dwtag  DW_TAG_member
	.dwattr $C$DW$109, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$109, DW_AT_name("biasScale")
	.dwattr $C$DW$109, DW_AT_data_member_location[DW_OP_plus_uconst 0x20]
	.dwattr $C$DW$109, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$109, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$109, DW_AT_decl_line(0x4cf)
	.dwattr $C$DW$109, DW_AT_decl_column(0x10)

$C$DW$110	.dwtag  DW_TAG_member
	.dwattr $C$DW$110, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$110, DW_AT_name("zeroWeightValue")
	.dwattr $C$DW$110, DW_AT_data_member_location[DW_OP_plus_uconst 0x24]
	.dwattr $C$DW$110, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$110, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$110, DW_AT_decl_line(0x4d1)
	.dwattr $C$DW$110, DW_AT_decl_column(0x0d)


$C$DW$111	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$111, DW_AT_name("operator =")
	.dwattr $C$DW$111, DW_AT_declaration
	.dwattr $C$DW$111, DW_AT_linkage_name("_ZN23sTIDL_BatchNormParams_taSERKS_")
	.dwattr $C$DW$111, DW_AT_type(*$C$DW$T$45)
	.dwattr $C$DW$111, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$112	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$112, DW_AT_type(*$C$DW$T$47)

	.dwendtag $C$DW$111


$C$DW$113	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$113, DW_AT_name("operator =")
	.dwattr $C$DW$113, DW_AT_declaration
	.dwattr $C$DW$113, DW_AT_linkage_name("_ZN23sTIDL_BatchNormParams_taSEOS_")
	.dwattr $C$DW$113, DW_AT_type(*$C$DW$T$45)
	.dwattr $C$DW$113, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$114	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$114, DW_AT_type(*$C$DW$T$45)

	.dwendtag $C$DW$113

	.dwattr $C$DW$T$50, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$50, DW_AT_decl_line(0x4bd)
	.dwattr $C$DW$T$50, DW_AT_decl_column(0x10)
	.dwendtag $C$DW$T$50

	.dwendtag $C$DW$TU$50


$C$DW$TU$45	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$45
$C$DW$T$45	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$45, DW_AT_type(*$C$DW$T$50)
	.dwattr $C$DW$T$45, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$45


$C$DW$TU$48	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$48

$C$DW$T$48	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$48, DW_AT_type(*$C$DW$T$45)
$C$DW$115	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$115, DW_AT_type(*$C$DW$T$47)

	.dwendtag $C$DW$T$48

	.dwendtag $C$DW$TU$48


$C$DW$TU$49	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$49

$C$DW$T$49	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$49, DW_AT_type(*$C$DW$T$45)
$C$DW$116	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$116, DW_AT_type(*$C$DW$T$45)

	.dwendtag $C$DW$T$49

	.dwendtag $C$DW$TU$49


$C$DW$TU$46	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$46
$C$DW$T$46	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$46, DW_AT_type(*$C$DW$T$50)

	.dwendtag $C$DW$TU$46


$C$DW$TU$47	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$47
$C$DW$T$47	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$47, DW_AT_type(*$C$DW$T$46)
	.dwattr $C$DW$T$47, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$47


$C$DW$TU$147	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$147
$C$DW$T$147	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$147, DW_AT_name("sTIDL_BatchNormParams_t")
	.dwattr $C$DW$T$147, DW_AT_type(*$C$DW$T$50)
	.dwattr $C$DW$T$147, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$147, DW_AT_decl_line(0x4d2)
	.dwattr $C$DW$T$147, DW_AT_decl_column(0x02)

	.dwendtag $C$DW$TU$147


$C$DW$TU$57	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$57

$C$DW$T$57	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$57, DW_AT_name("sTIDL_BiasParams_t")
	.dwattr $C$DW$T$57, DW_AT_byte_size(0x14)
$C$DW$117	.dwtag  DW_TAG_member
	.dwattr $C$DW$117, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$117, DW_AT_name("bias")
	.dwattr $C$DW$117, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$117, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$117, DW_AT_decl_line(0x4db)
	.dwattr $C$DW$117, DW_AT_decl_column(0x0d)

$C$DW$118	.dwtag  DW_TAG_member
	.dwattr $C$DW$118, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$118, DW_AT_name("numChannels")
	.dwattr $C$DW$118, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$118, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$118, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$118, DW_AT_decl_line(0x4dd)
	.dwattr $C$DW$118, DW_AT_decl_column(0x0d)

$C$DW$119	.dwtag  DW_TAG_member
	.dwattr $C$DW$119, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$119, DW_AT_name("biasQ")
	.dwattr $C$DW$119, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$119, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$119, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$119, DW_AT_decl_line(0x4df)
	.dwattr $C$DW$119, DW_AT_decl_column(0x0d)

$C$DW$120	.dwtag  DW_TAG_member
	.dwattr $C$DW$120, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$120, DW_AT_name("inDataQ")
	.dwattr $C$DW$120, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$120, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$120, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$120, DW_AT_decl_line(0x4e1)
	.dwattr $C$DW$120, DW_AT_decl_column(0x0d)

$C$DW$121	.dwtag  DW_TAG_member
	.dwattr $C$DW$121, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$121, DW_AT_name("outDataQ")
	.dwattr $C$DW$121, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$121, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$121, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$121, DW_AT_decl_line(0x4e3)
	.dwattr $C$DW$121, DW_AT_decl_column(0x0d)


$C$DW$122	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$122, DW_AT_name("operator =")
	.dwattr $C$DW$122, DW_AT_declaration
	.dwattr $C$DW$122, DW_AT_linkage_name("_ZN18sTIDL_BiasParams_taSERKS_")
	.dwattr $C$DW$122, DW_AT_type(*$C$DW$T$52)
	.dwattr $C$DW$122, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$123	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$123, DW_AT_type(*$C$DW$T$54)

	.dwendtag $C$DW$122


$C$DW$124	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$124, DW_AT_name("operator =")
	.dwattr $C$DW$124, DW_AT_declaration
	.dwattr $C$DW$124, DW_AT_linkage_name("_ZN18sTIDL_BiasParams_taSEOS_")
	.dwattr $C$DW$124, DW_AT_type(*$C$DW$T$52)
	.dwattr $C$DW$124, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$125	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$125, DW_AT_type(*$C$DW$T$52)

	.dwendtag $C$DW$124

	.dwattr $C$DW$T$57, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$57, DW_AT_decl_line(0x4d9)
	.dwattr $C$DW$T$57, DW_AT_decl_column(0x10)
	.dwendtag $C$DW$T$57

	.dwendtag $C$DW$TU$57


$C$DW$TU$52	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$52
$C$DW$T$52	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$52, DW_AT_type(*$C$DW$T$57)
	.dwattr $C$DW$T$52, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$52


$C$DW$TU$55	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$55

$C$DW$T$55	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$55, DW_AT_type(*$C$DW$T$52)
$C$DW$126	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$126, DW_AT_type(*$C$DW$T$54)

	.dwendtag $C$DW$T$55

	.dwendtag $C$DW$TU$55


$C$DW$TU$56	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$56

$C$DW$T$56	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$56, DW_AT_type(*$C$DW$T$52)
$C$DW$127	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$127, DW_AT_type(*$C$DW$T$52)

	.dwendtag $C$DW$T$56

	.dwendtag $C$DW$TU$56


$C$DW$TU$53	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$53
$C$DW$T$53	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$53, DW_AT_type(*$C$DW$T$57)

	.dwendtag $C$DW$TU$53


$C$DW$TU$54	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$54
$C$DW$T$54	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$54, DW_AT_type(*$C$DW$T$53)
	.dwattr $C$DW$T$54, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$54


$C$DW$TU$146	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$146
$C$DW$T$146	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$146, DW_AT_name("sTIDL_BiasParams_t")
	.dwattr $C$DW$T$146, DW_AT_type(*$C$DW$T$57)
	.dwattr $C$DW$T$146, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$146, DW_AT_decl_line(0x4e4)
	.dwattr $C$DW$T$146, DW_AT_decl_column(0x02)

	.dwendtag $C$DW$TU$146


$C$DW$TU$64	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$64

$C$DW$T$64	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$64, DW_AT_name("sTIDL_ConcatParams_t")
	.dwattr $C$DW$T$64, DW_AT_byte_size(0x08)
$C$DW$128	.dwtag  DW_TAG_member
	.dwattr $C$DW$128, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$128, DW_AT_name("axis")
	.dwattr $C$DW$128, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$128, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$128, DW_AT_decl_line(0x4b1)
	.dwattr $C$DW$128, DW_AT_decl_column(0x0c)

$C$DW$129	.dwtag  DW_TAG_member
	.dwattr $C$DW$129, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$129, DW_AT_name("outDataQ")
	.dwattr $C$DW$129, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$129, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$129, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$129, DW_AT_decl_line(0x4b3)
	.dwattr $C$DW$129, DW_AT_decl_column(0x0c)


$C$DW$130	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$130, DW_AT_name("operator =")
	.dwattr $C$DW$130, DW_AT_declaration
	.dwattr $C$DW$130, DW_AT_linkage_name("_ZN20sTIDL_ConcatParams_taSERKS_")
	.dwattr $C$DW$130, DW_AT_type(*$C$DW$T$59)
	.dwattr $C$DW$130, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$131	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$131, DW_AT_type(*$C$DW$T$61)

	.dwendtag $C$DW$130


$C$DW$132	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$132, DW_AT_name("operator =")
	.dwattr $C$DW$132, DW_AT_declaration
	.dwattr $C$DW$132, DW_AT_linkage_name("_ZN20sTIDL_ConcatParams_taSEOS_")
	.dwattr $C$DW$132, DW_AT_type(*$C$DW$T$59)
	.dwattr $C$DW$132, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$133	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$133, DW_AT_type(*$C$DW$T$59)

	.dwendtag $C$DW$132

	.dwattr $C$DW$T$64, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$64, DW_AT_decl_line(0x4af)
	.dwattr $C$DW$T$64, DW_AT_decl_column(0x10)
	.dwendtag $C$DW$T$64

	.dwendtag $C$DW$TU$64


$C$DW$TU$59	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$59
$C$DW$T$59	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$59, DW_AT_type(*$C$DW$T$64)
	.dwattr $C$DW$T$59, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$59


$C$DW$TU$62	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$62

$C$DW$T$62	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$62, DW_AT_type(*$C$DW$T$59)
$C$DW$134	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$134, DW_AT_type(*$C$DW$T$61)

	.dwendtag $C$DW$T$62

	.dwendtag $C$DW$TU$62


$C$DW$TU$63	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$63

$C$DW$T$63	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$63, DW_AT_type(*$C$DW$T$59)
$C$DW$135	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$135, DW_AT_type(*$C$DW$T$59)

	.dwendtag $C$DW$T$63

	.dwendtag $C$DW$TU$63


$C$DW$TU$60	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$60
$C$DW$T$60	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$60, DW_AT_type(*$C$DW$T$64)

	.dwendtag $C$DW$TU$60


$C$DW$TU$61	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$61
$C$DW$T$61	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$61, DW_AT_type(*$C$DW$T$60)
	.dwattr $C$DW$T$61, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$61


$C$DW$TU$144	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$144
$C$DW$T$144	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$144, DW_AT_name("sTIDL_ConcatParams_t")
	.dwattr $C$DW$T$144, DW_AT_type(*$C$DW$T$64)
	.dwattr $C$DW$T$144, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$144, DW_AT_decl_line(0x4b4)
	.dwattr $C$DW$T$144, DW_AT_decl_column(0x02)

	.dwendtag $C$DW$TU$144


$C$DW$TU$72	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$72

$C$DW$T$72	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$72, DW_AT_name("sTIDL_ConvParams_t")
	.dwattr $C$DW$T$72, DW_AT_byte_size(0xa8)
$C$DW$136	.dwtag  DW_TAG_member
	.dwattr $C$DW$136, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$136, DW_AT_name("weights")
	.dwattr $C$DW$136, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$136, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$136, DW_AT_decl_line(0x3dc)
	.dwattr $C$DW$136, DW_AT_decl_column(0x0d)

$C$DW$137	.dwtag  DW_TAG_member
	.dwattr $C$DW$137, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$137, DW_AT_name("bias")
	.dwattr $C$DW$137, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$137, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$137, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$137, DW_AT_decl_line(0x3de)
	.dwattr $C$DW$137, DW_AT_decl_column(0x0d)

$C$DW$138	.dwtag  DW_TAG_member
	.dwattr $C$DW$138, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$138, DW_AT_name("perChannelWeightScaleOffset")
	.dwattr $C$DW$138, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$138, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$138, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$138, DW_AT_decl_line(0x3e1)
	.dwattr $C$DW$138, DW_AT_decl_column(0x0d)

$C$DW$139	.dwtag  DW_TAG_member
	.dwattr $C$DW$139, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$139, DW_AT_name("convolutionType")
	.dwattr $C$DW$139, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$139, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$139, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$139, DW_AT_decl_line(0x3e3)
	.dwattr $C$DW$139, DW_AT_decl_column(0x0d)

$C$DW$140	.dwtag  DW_TAG_member
	.dwattr $C$DW$140, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$140, DW_AT_name("numInChannels")
	.dwattr $C$DW$140, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$140, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$140, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$140, DW_AT_decl_line(0x3e5)
	.dwattr $C$DW$140, DW_AT_decl_column(0x0d)

$C$DW$141	.dwtag  DW_TAG_member
	.dwattr $C$DW$141, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$141, DW_AT_name("numOutChannels")
	.dwattr $C$DW$141, DW_AT_data_member_location[DW_OP_plus_uconst 0x14]
	.dwattr $C$DW$141, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$141, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$141, DW_AT_decl_line(0x3e7)
	.dwattr $C$DW$141, DW_AT_decl_column(0x0d)

$C$DW$142	.dwtag  DW_TAG_member
	.dwattr $C$DW$142, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$142, DW_AT_name("numGroups")
	.dwattr $C$DW$142, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$142, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$142, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$142, DW_AT_decl_line(0x3e9)
	.dwattr $C$DW$142, DW_AT_decl_column(0x0d)

$C$DW$143	.dwtag  DW_TAG_member
	.dwattr $C$DW$143, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$143, DW_AT_name("kernelW")
	.dwattr $C$DW$143, DW_AT_data_member_location[DW_OP_plus_uconst 0x1c]
	.dwattr $C$DW$143, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$143, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$143, DW_AT_decl_line(0x3eb)
	.dwattr $C$DW$143, DW_AT_decl_column(0x0d)

$C$DW$144	.dwtag  DW_TAG_member
	.dwattr $C$DW$144, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$144, DW_AT_name("kernelH")
	.dwattr $C$DW$144, DW_AT_data_member_location[DW_OP_plus_uconst 0x20]
	.dwattr $C$DW$144, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$144, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$144, DW_AT_decl_line(0x3ed)
	.dwattr $C$DW$144, DW_AT_decl_column(0x0d)

$C$DW$145	.dwtag  DW_TAG_member
	.dwattr $C$DW$145, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$145, DW_AT_name("strideW")
	.dwattr $C$DW$145, DW_AT_data_member_location[DW_OP_plus_uconst 0x24]
	.dwattr $C$DW$145, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$145, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$145, DW_AT_decl_line(0x3ef)
	.dwattr $C$DW$145, DW_AT_decl_column(0x0d)

$C$DW$146	.dwtag  DW_TAG_member
	.dwattr $C$DW$146, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$146, DW_AT_name("strideH")
	.dwattr $C$DW$146, DW_AT_data_member_location[DW_OP_plus_uconst 0x28]
	.dwattr $C$DW$146, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$146, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$146, DW_AT_decl_line(0x3f1)
	.dwattr $C$DW$146, DW_AT_decl_column(0x0d)

$C$DW$147	.dwtag  DW_TAG_member
	.dwattr $C$DW$147, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$147, DW_AT_name("dilationW")
	.dwattr $C$DW$147, DW_AT_data_member_location[DW_OP_plus_uconst 0x2c]
	.dwattr $C$DW$147, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$147, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$147, DW_AT_decl_line(0x3f3)
	.dwattr $C$DW$147, DW_AT_decl_column(0x0d)

$C$DW$148	.dwtag  DW_TAG_member
	.dwattr $C$DW$148, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$148, DW_AT_name("dilationH")
	.dwattr $C$DW$148, DW_AT_data_member_location[DW_OP_plus_uconst 0x30]
	.dwattr $C$DW$148, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$148, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$148, DW_AT_decl_line(0x3f5)
	.dwattr $C$DW$148, DW_AT_decl_column(0x0d)

$C$DW$149	.dwtag  DW_TAG_member
	.dwattr $C$DW$149, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$149, DW_AT_name("padW")
	.dwattr $C$DW$149, DW_AT_data_member_location[DW_OP_plus_uconst 0x34]
	.dwattr $C$DW$149, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$149, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$149, DW_AT_decl_line(0x3f7)
	.dwattr $C$DW$149, DW_AT_decl_column(0x0d)

$C$DW$150	.dwtag  DW_TAG_member
	.dwattr $C$DW$150, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$150, DW_AT_name("padH")
	.dwattr $C$DW$150, DW_AT_data_member_location[DW_OP_plus_uconst 0x38]
	.dwattr $C$DW$150, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$150, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$150, DW_AT_decl_line(0x3f9)
	.dwattr $C$DW$150, DW_AT_decl_column(0x0d)

$C$DW$151	.dwtag  DW_TAG_member
	.dwattr $C$DW$151, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$151, DW_AT_name("weightScale")
	.dwattr $C$DW$151, DW_AT_data_member_location[DW_OP_plus_uconst 0x3c]
	.dwattr $C$DW$151, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$151, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$151, DW_AT_decl_line(0x3fb)
	.dwattr $C$DW$151, DW_AT_decl_column(0x14)

$C$DW$152	.dwtag  DW_TAG_member
	.dwattr $C$DW$152, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$152, DW_AT_name("biasScale")
	.dwattr $C$DW$152, DW_AT_data_member_location[DW_OP_plus_uconst 0x40]
	.dwattr $C$DW$152, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$152, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$152, DW_AT_decl_line(0x3fd)
	.dwattr $C$DW$152, DW_AT_decl_column(0x14)

$C$DW$153	.dwtag  DW_TAG_member
	.dwattr $C$DW$153, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$153, DW_AT_name("weightsQ")
	.dwattr $C$DW$153, DW_AT_data_member_location[DW_OP_plus_uconst 0x44]
	.dwattr $C$DW$153, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$153, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$153, DW_AT_decl_line(0x3ff)
	.dwattr $C$DW$153, DW_AT_decl_column(0x0d)

$C$DW$154	.dwtag  DW_TAG_member
	.dwattr $C$DW$154, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$154, DW_AT_name("zeroWeightValue")
	.dwattr $C$DW$154, DW_AT_data_member_location[DW_OP_plus_uconst 0x48]
	.dwattr $C$DW$154, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$154, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$154, DW_AT_decl_line(0x401)
	.dwattr $C$DW$154, DW_AT_decl_column(0x0d)

$C$DW$155	.dwtag  DW_TAG_member
	.dwattr $C$DW$155, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$155, DW_AT_name("biasB")
	.dwattr $C$DW$155, DW_AT_data_member_location[DW_OP_plus_uconst 0x4c]
	.dwattr $C$DW$155, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$155, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$155, DW_AT_decl_line(0x403)
	.dwattr $C$DW$155, DW_AT_decl_column(0x0d)

$C$DW$156	.dwtag  DW_TAG_member
	.dwattr $C$DW$156, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$156, DW_AT_name("biasQ")
	.dwattr $C$DW$156, DW_AT_data_member_location[DW_OP_plus_uconst 0x50]
	.dwattr $C$DW$156, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$156, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$156, DW_AT_decl_line(0x405)
	.dwattr $C$DW$156, DW_AT_decl_column(0x0d)

$C$DW$157	.dwtag  DW_TAG_member
	.dwattr $C$DW$157, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$157, DW_AT_name("inDataQ")
	.dwattr $C$DW$157, DW_AT_data_member_location[DW_OP_plus_uconst 0x54]
	.dwattr $C$DW$157, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$157, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$157, DW_AT_decl_line(0x407)
	.dwattr $C$DW$157, DW_AT_decl_column(0x0d)

$C$DW$158	.dwtag  DW_TAG_member
	.dwattr $C$DW$158, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$158, DW_AT_name("outDataQ")
	.dwattr $C$DW$158, DW_AT_data_member_location[DW_OP_plus_uconst 0x58]
	.dwattr $C$DW$158, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$158, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$158, DW_AT_decl_line(0x409)
	.dwattr $C$DW$158, DW_AT_decl_column(0x0d)

$C$DW$159	.dwtag  DW_TAG_member
	.dwattr $C$DW$159, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$159, DW_AT_name("interDataQ")
	.dwattr $C$DW$159, DW_AT_data_member_location[DW_OP_plus_uconst 0x5c]
	.dwattr $C$DW$159, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$159, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$159, DW_AT_decl_line(0x40b)
	.dwattr $C$DW$159, DW_AT_decl_column(0x0d)

$C$DW$160	.dwtag  DW_TAG_member
	.dwattr $C$DW$160, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$160, DW_AT_name("enableBias")
	.dwattr $C$DW$160, DW_AT_data_member_location[DW_OP_plus_uconst 0x60]
	.dwattr $C$DW$160, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$160, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$160, DW_AT_decl_line(0x40d)
	.dwattr $C$DW$160, DW_AT_decl_column(0x0d)

$C$DW$161	.dwtag  DW_TAG_member
	.dwattr $C$DW$161, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$161, DW_AT_name("enablePooling")
	.dwattr $C$DW$161, DW_AT_data_member_location[DW_OP_plus_uconst 0x64]
	.dwattr $C$DW$161, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$161, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$161, DW_AT_decl_line(0x40f)
	.dwattr $C$DW$161, DW_AT_decl_column(0x0d)

$C$DW$162	.dwtag  DW_TAG_member
	.dwattr $C$DW$162, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$162, DW_AT_name("enableEltWise")
	.dwattr $C$DW$162, DW_AT_data_member_location[DW_OP_plus_uconst 0x68]
	.dwattr $C$DW$162, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$162, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$162, DW_AT_decl_line(0x411)
	.dwattr $C$DW$162, DW_AT_decl_column(0x0d)

$C$DW$163	.dwtag  DW_TAG_member
	.dwattr $C$DW$163, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$163, DW_AT_name("enableEWRelU")
	.dwattr $C$DW$163, DW_AT_data_member_location[DW_OP_plus_uconst 0x6c]
	.dwattr $C$DW$163, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$163, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$163, DW_AT_decl_line(0x413)
	.dwattr $C$DW$163, DW_AT_decl_column(0x0d)

$C$DW$164	.dwtag  DW_TAG_member
	.dwattr $C$DW$164, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$164, DW_AT_name("kernelType")
	.dwattr $C$DW$164, DW_AT_data_member_location[DW_OP_plus_uconst 0x70]
	.dwattr $C$DW$164, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$164, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$164, DW_AT_decl_line(0x415)
	.dwattr $C$DW$164, DW_AT_decl_column(0x0d)

$C$DW$165	.dwtag  DW_TAG_member
	.dwattr $C$DW$165, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$165, DW_AT_name("enableDepthToSpace")
	.dwattr $C$DW$165, DW_AT_data_member_location[DW_OP_plus_uconst 0x74]
	.dwattr $C$DW$165, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$165, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$165, DW_AT_decl_line(0x417)
	.dwattr $C$DW$165, DW_AT_decl_column(0x0d)

$C$DW$166	.dwtag  DW_TAG_member
	.dwattr $C$DW$166, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$166, DW_AT_name("upscaleFactor")
	.dwattr $C$DW$166, DW_AT_data_member_location[DW_OP_plus_uconst 0x78]
	.dwattr $C$DW$166, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$166, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$166, DW_AT_decl_line(0x41a)
	.dwattr $C$DW$166, DW_AT_decl_column(0x0d)

$C$DW$167	.dwtag  DW_TAG_member
	.dwattr $C$DW$167, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$167, DW_AT_name("poolParams")
	.dwattr $C$DW$167, DW_AT_data_member_location[DW_OP_plus_uconst 0x7c]
	.dwattr $C$DW$167, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$167, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$167, DW_AT_decl_line(0x41c)
	.dwattr $C$DW$167, DW_AT_decl_column(0x19)


$C$DW$168	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$168, DW_AT_name("operator =")
	.dwattr $C$DW$168, DW_AT_declaration
	.dwattr $C$DW$168, DW_AT_linkage_name("_ZN18sTIDL_ConvParams_taSERKS_")
	.dwattr $C$DW$168, DW_AT_type(*$C$DW$T$67)
	.dwattr $C$DW$168, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$169	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$169, DW_AT_type(*$C$DW$T$69)

	.dwendtag $C$DW$168


$C$DW$170	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$170, DW_AT_name("operator =")
	.dwattr $C$DW$170, DW_AT_declaration
	.dwattr $C$DW$170, DW_AT_linkage_name("_ZN18sTIDL_ConvParams_taSEOS_")
	.dwattr $C$DW$170, DW_AT_type(*$C$DW$T$67)
	.dwattr $C$DW$170, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$171	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$171, DW_AT_type(*$C$DW$T$67)

	.dwendtag $C$DW$170

	.dwattr $C$DW$T$72, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$72, DW_AT_decl_line(0x3da)
	.dwattr $C$DW$T$72, DW_AT_decl_column(0x10)
	.dwendtag $C$DW$T$72

	.dwendtag $C$DW$TU$72


$C$DW$TU$67	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$67
$C$DW$T$67	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$67, DW_AT_type(*$C$DW$T$72)
	.dwattr $C$DW$T$67, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$67


$C$DW$TU$70	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$70

$C$DW$T$70	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$70, DW_AT_type(*$C$DW$T$67)
$C$DW$172	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$172, DW_AT_type(*$C$DW$T$69)

	.dwendtag $C$DW$T$70

	.dwendtag $C$DW$TU$70


$C$DW$TU$71	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$71

$C$DW$T$71	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$71, DW_AT_type(*$C$DW$T$67)
$C$DW$173	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$173, DW_AT_type(*$C$DW$T$67)

	.dwendtag $C$DW$T$71

	.dwendtag $C$DW$TU$71


$C$DW$TU$68	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$68
$C$DW$T$68	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$68, DW_AT_type(*$C$DW$T$72)

	.dwendtag $C$DW$TU$68


$C$DW$TU$69	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$69
$C$DW$T$69	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$69, DW_AT_type(*$C$DW$T$68)
	.dwattr $C$DW$T$69, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$69


$C$DW$TU$137	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$137
$C$DW$T$137	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$137, DW_AT_name("sTIDL_ConvParams_t")
	.dwattr $C$DW$T$137, DW_AT_type(*$C$DW$T$72)
	.dwattr $C$DW$T$137, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$137, DW_AT_decl_line(0x41d)
	.dwattr $C$DW$T$137, DW_AT_decl_column(0x02)

	.dwendtag $C$DW$TU$137


$C$DW$TU$80	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$80

$C$DW$T$80	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$80, DW_AT_name("sTIDL_CropParams_t")
	.dwattr $C$DW$T$80, DW_AT_byte_size(0x14)
$C$DW$174	.dwtag  DW_TAG_member
	.dwattr $C$DW$174, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$174, DW_AT_name("numChannels")
	.dwattr $C$DW$174, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$174, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$174, DW_AT_decl_line(0x535)
	.dwattr $C$DW$174, DW_AT_decl_column(0x0d)

$C$DW$175	.dwtag  DW_TAG_member
	.dwattr $C$DW$175, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$175, DW_AT_name("inDataQ")
	.dwattr $C$DW$175, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$175, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$175, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$175, DW_AT_decl_line(0x537)
	.dwattr $C$DW$175, DW_AT_decl_column(0x0d)

$C$DW$176	.dwtag  DW_TAG_member
	.dwattr $C$DW$176, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$176, DW_AT_name("outDataQ")
	.dwattr $C$DW$176, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$176, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$176, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$176, DW_AT_decl_line(0x539)
	.dwattr $C$DW$176, DW_AT_decl_column(0x0d)

$C$DW$177	.dwtag  DW_TAG_member
	.dwattr $C$DW$177, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$177, DW_AT_name("offsetW")
	.dwattr $C$DW$177, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$177, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$177, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$177, DW_AT_decl_line(0x53b)
	.dwattr $C$DW$177, DW_AT_decl_column(0x0c)

$C$DW$178	.dwtag  DW_TAG_member
	.dwattr $C$DW$178, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$178, DW_AT_name("offsetH")
	.dwattr $C$DW$178, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$178, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$178, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$178, DW_AT_decl_line(0x53d)
	.dwattr $C$DW$178, DW_AT_decl_column(0x0c)


$C$DW$179	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$179, DW_AT_name("operator =")
	.dwattr $C$DW$179, DW_AT_declaration
	.dwattr $C$DW$179, DW_AT_linkage_name("_ZN18sTIDL_CropParams_taSERKS_")
	.dwattr $C$DW$179, DW_AT_type(*$C$DW$T$75)
	.dwattr $C$DW$179, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$180	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$180, DW_AT_type(*$C$DW$T$77)

	.dwendtag $C$DW$179


$C$DW$181	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$181, DW_AT_name("operator =")
	.dwattr $C$DW$181, DW_AT_declaration
	.dwattr $C$DW$181, DW_AT_linkage_name("_ZN18sTIDL_CropParams_taSEOS_")
	.dwattr $C$DW$181, DW_AT_type(*$C$DW$T$75)
	.dwattr $C$DW$181, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$182	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$182, DW_AT_type(*$C$DW$T$75)

	.dwendtag $C$DW$181

	.dwattr $C$DW$T$80, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$80, DW_AT_decl_line(0x533)
	.dwattr $C$DW$T$80, DW_AT_decl_column(0x10)
	.dwendtag $C$DW$T$80

	.dwendtag $C$DW$TU$80


$C$DW$TU$75	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$75
$C$DW$T$75	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$75, DW_AT_type(*$C$DW$T$80)
	.dwattr $C$DW$T$75, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$75


$C$DW$TU$78	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$78

$C$DW$T$78	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$78, DW_AT_type(*$C$DW$T$75)
$C$DW$183	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$183, DW_AT_type(*$C$DW$T$77)

	.dwendtag $C$DW$T$78

	.dwendtag $C$DW$TU$78


$C$DW$TU$79	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$79

$C$DW$T$79	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$79, DW_AT_type(*$C$DW$T$75)
$C$DW$184	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$184, DW_AT_type(*$C$DW$T$75)

	.dwendtag $C$DW$T$79

	.dwendtag $C$DW$TU$79


$C$DW$TU$76	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$76
$C$DW$T$76	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$76, DW_AT_type(*$C$DW$T$80)

	.dwendtag $C$DW$TU$76


$C$DW$TU$77	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$77
$C$DW$T$77	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$77, DW_AT_type(*$C$DW$T$76)
	.dwattr $C$DW$T$77, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$77


$C$DW$TU$143	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$143
$C$DW$T$143	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$143, DW_AT_name("sTIDL_CropParams_t")
	.dwattr $C$DW$T$143, DW_AT_type(*$C$DW$T$80)
	.dwattr $C$DW$T$143, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$143, DW_AT_decl_line(0x53e)
	.dwattr $C$DW$T$143, DW_AT_decl_column(0x02)

	.dwendtag $C$DW$TU$143


$C$DW$TU$87	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$87

$C$DW$T$87	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$87, DW_AT_name("sTIDL_CustomParams_t")
	.dwattr $C$DW$T$87, DW_AT_byte_size(0x1c)
$C$DW$185	.dwtag  DW_TAG_member
	.dwattr $C$DW$185, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$185, DW_AT_name("customLayerType")
	.dwattr $C$DW$185, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$185, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$185, DW_AT_decl_line(0x3a4)
	.dwattr $C$DW$185, DW_AT_decl_column(0x0d)

$C$DW$186	.dwtag  DW_TAG_member
	.dwattr $C$DW$186, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$186, DW_AT_name("padW")
	.dwattr $C$DW$186, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$186, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$186, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$186, DW_AT_decl_line(0x3a7)
	.dwattr $C$DW$186, DW_AT_decl_column(0x0b)

$C$DW$187	.dwtag  DW_TAG_member
	.dwattr $C$DW$187, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$187, DW_AT_name("padH")
	.dwattr $C$DW$187, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$187, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$187, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$187, DW_AT_decl_line(0x3aa)
	.dwattr $C$DW$187, DW_AT_decl_column(0x0b)

$C$DW$188	.dwtag  DW_TAG_member
	.dwattr $C$DW$188, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$188, DW_AT_name("memOverlapType")
	.dwattr $C$DW$188, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$188, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$188, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$188, DW_AT_decl_line(0x3b0)
	.dwattr $C$DW$188, DW_AT_decl_column(0x0b)

$C$DW$189	.dwtag  DW_TAG_member
	.dwattr $C$DW$189, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$189, DW_AT_name("doesLayerChangePadding")
	.dwattr $C$DW$189, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$189, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$189, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$189, DW_AT_decl_line(0x3b6)
	.dwattr $C$DW$189, DW_AT_decl_column(0x0b)

$C$DW$190	.dwtag  DW_TAG_member
	.dwattr $C$DW$190, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$190, DW_AT_name("doesLayerFillOutXPadding")
	.dwattr $C$DW$190, DW_AT_data_member_location[DW_OP_plus_uconst 0x14]
	.dwattr $C$DW$190, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$190, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$190, DW_AT_decl_line(0x3d0)
	.dwattr $C$DW$190, DW_AT_decl_column(0x0b)

$C$DW$191	.dwtag  DW_TAG_member
	.dwattr $C$DW$191, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$191, DW_AT_name("rsvdPassThrough")
	.dwattr $C$DW$191, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$191, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$191, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$191, DW_AT_decl_line(0x3d2)
	.dwattr $C$DW$191, DW_AT_decl_column(0x0b)


$C$DW$192	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$192, DW_AT_name("operator =")
	.dwattr $C$DW$192, DW_AT_declaration
	.dwattr $C$DW$192, DW_AT_linkage_name("_ZN20sTIDL_CustomParams_taSERKS_")
	.dwattr $C$DW$192, DW_AT_type(*$C$DW$T$82)
	.dwattr $C$DW$192, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$193	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$193, DW_AT_type(*$C$DW$T$84)

	.dwendtag $C$DW$192


$C$DW$194	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$194, DW_AT_name("operator =")
	.dwattr $C$DW$194, DW_AT_declaration
	.dwattr $C$DW$194, DW_AT_linkage_name("_ZN20sTIDL_CustomParams_taSEOS_")
	.dwattr $C$DW$194, DW_AT_type(*$C$DW$T$82)
	.dwattr $C$DW$194, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$195	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$195, DW_AT_type(*$C$DW$T$82)

	.dwendtag $C$DW$194

	.dwattr $C$DW$T$87, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$87, DW_AT_decl_line(0x3a2)
	.dwattr $C$DW$T$87, DW_AT_decl_column(0x10)
	.dwendtag $C$DW$T$87

	.dwendtag $C$DW$TU$87


$C$DW$TU$82	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$82
$C$DW$T$82	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$82, DW_AT_type(*$C$DW$T$87)
	.dwattr $C$DW$T$82, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$82


$C$DW$TU$85	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$85

$C$DW$T$85	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$85, DW_AT_type(*$C$DW$T$82)
$C$DW$196	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$196, DW_AT_type(*$C$DW$T$84)

	.dwendtag $C$DW$T$85

	.dwendtag $C$DW$TU$85


$C$DW$TU$86	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$86

$C$DW$T$86	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$86, DW_AT_type(*$C$DW$T$82)
$C$DW$197	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$197, DW_AT_type(*$C$DW$T$82)

	.dwendtag $C$DW$T$86

	.dwendtag $C$DW$TU$86


$C$DW$TU$83	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$83
$C$DW$T$83	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$83, DW_AT_type(*$C$DW$T$87)

	.dwendtag $C$DW$TU$83


$C$DW$TU$84	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$84
$C$DW$T$84	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$84, DW_AT_type(*$C$DW$T$83)
	.dwattr $C$DW$T$84, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$84


$C$DW$TU$156	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$156
$C$DW$T$156	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$156, DW_AT_name("sTIDL_CustomParams_t")
	.dwattr $C$DW$T$156, DW_AT_type(*$C$DW$T$87)
	.dwattr $C$DW$T$156, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$156, DW_AT_decl_line(0x3d3)
	.dwattr $C$DW$T$156, DW_AT_decl_column(0x02)

	.dwendtag $C$DW$TU$156


$C$DW$TU$94	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$94

$C$DW$T$94	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$94, DW_AT_name("sTIDL_DataLayerParams_t")
	.dwattr $C$DW$T$94, DW_AT_byte_size(0x08)
$C$DW$198	.dwtag  DW_TAG_member
	.dwattr $C$DW$198, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$198, DW_AT_name("numChannels")
	.dwattr $C$DW$198, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$198, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$198, DW_AT_decl_line(0x296)
	.dwattr $C$DW$198, DW_AT_decl_column(0x0d)

$C$DW$199	.dwtag  DW_TAG_member
	.dwattr $C$DW$199, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$199, DW_AT_name("dataQ")
	.dwattr $C$DW$199, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$199, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$199, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$199, DW_AT_decl_line(0x298)
	.dwattr $C$DW$199, DW_AT_decl_column(0x0d)


$C$DW$200	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$200, DW_AT_name("operator =")
	.dwattr $C$DW$200, DW_AT_declaration
	.dwattr $C$DW$200, DW_AT_linkage_name("_ZN23sTIDL_DataLayerParams_taSERKS_")
	.dwattr $C$DW$200, DW_AT_type(*$C$DW$T$89)
	.dwattr $C$DW$200, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$201	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$201, DW_AT_type(*$C$DW$T$91)

	.dwendtag $C$DW$200


$C$DW$202	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$202, DW_AT_name("operator =")
	.dwattr $C$DW$202, DW_AT_declaration
	.dwattr $C$DW$202, DW_AT_linkage_name("_ZN23sTIDL_DataLayerParams_taSEOS_")
	.dwattr $C$DW$202, DW_AT_type(*$C$DW$T$89)
	.dwattr $C$DW$202, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$203	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$203, DW_AT_type(*$C$DW$T$89)

	.dwendtag $C$DW$202

	.dwattr $C$DW$T$94, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$94, DW_AT_decl_line(0x294)
	.dwattr $C$DW$T$94, DW_AT_decl_column(0x10)
	.dwendtag $C$DW$T$94

	.dwendtag $C$DW$TU$94


$C$DW$TU$89	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$89
$C$DW$T$89	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$89, DW_AT_type(*$C$DW$T$94)
	.dwattr $C$DW$T$89, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$89


$C$DW$TU$92	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$92

$C$DW$T$92	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$92, DW_AT_type(*$C$DW$T$89)
$C$DW$204	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$204, DW_AT_type(*$C$DW$T$91)

	.dwendtag $C$DW$T$92

	.dwendtag $C$DW$TU$92


$C$DW$TU$93	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$93

$C$DW$T$93	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$93, DW_AT_type(*$C$DW$T$89)
$C$DW$205	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$205, DW_AT_type(*$C$DW$T$89)

	.dwendtag $C$DW$T$93

	.dwendtag $C$DW$TU$93


$C$DW$TU$90	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$90
$C$DW$T$90	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$90, DW_AT_type(*$C$DW$T$94)

	.dwendtag $C$DW$TU$90


$C$DW$TU$91	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$91
$C$DW$T$91	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$91, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$T$91, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$91


$C$DW$TU$140	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$140
$C$DW$T$140	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$140, DW_AT_name("sTIDL_DataLayerParams_t")
	.dwattr $C$DW$T$140, DW_AT_type(*$C$DW$T$94)
	.dwattr $C$DW$T$140, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$140, DW_AT_decl_line(0x299)
	.dwattr $C$DW$T$140, DW_AT_decl_column(0x02)

	.dwendtag $C$DW$TU$140


$C$DW$TU$103	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$103

$C$DW$T$103	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$103, DW_AT_name("sTIDL_DataParams_t")
	.dwattr $C$DW$T$103, DW_AT_byte_size(0x5c)
$C$DW$206	.dwtag  DW_TAG_member
	.dwattr $C$DW$206, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$206, DW_AT_name("dataId")
	.dwattr $C$DW$206, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$206, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$206, DW_AT_decl_line(0x268)
	.dwattr $C$DW$206, DW_AT_decl_column(0x0b)

$C$DW$207	.dwtag  DW_TAG_member
	.dwattr $C$DW$207, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$207, DW_AT_name("elementType")
	.dwattr $C$DW$207, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$207, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$207, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$207, DW_AT_decl_line(0x26a)
	.dwattr $C$DW$207, DW_AT_decl_column(0x0b)

$C$DW$208	.dwtag  DW_TAG_member
	.dwattr $C$DW$208, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$208, DW_AT_name("numDim")
	.dwattr $C$DW$208, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$208, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$208, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$208, DW_AT_decl_line(0x26c)
	.dwattr $C$DW$208, DW_AT_decl_column(0x0b)

$C$DW$209	.dwtag  DW_TAG_member
	.dwattr $C$DW$209, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$209, DW_AT_name("dataQ")
	.dwattr $C$DW$209, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$209, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$209, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$209, DW_AT_decl_line(0x26e)
	.dwattr $C$DW$209, DW_AT_decl_column(0x0b)

$C$DW$210	.dwtag  DW_TAG_member
	.dwattr $C$DW$210, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$210, DW_AT_name("minValue")
	.dwattr $C$DW$210, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$210, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$210, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$210, DW_AT_decl_line(0x270)
	.dwattr $C$DW$210, DW_AT_decl_column(0x0b)

$C$DW$211	.dwtag  DW_TAG_member
	.dwattr $C$DW$211, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$211, DW_AT_name("maxValue")
	.dwattr $C$DW$211, DW_AT_data_member_location[DW_OP_plus_uconst 0x14]
	.dwattr $C$DW$211, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$211, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$211, DW_AT_decl_line(0x272)
	.dwattr $C$DW$211, DW_AT_decl_column(0x0b)

$C$DW$212	.dwtag  DW_TAG_member
	.dwattr $C$DW$212, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$212, DW_AT_name("minTensorValue")
	.dwattr $C$DW$212, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$212, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$212, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$212, DW_AT_decl_line(0x274)
	.dwattr $C$DW$212, DW_AT_decl_column(0x12)

$C$DW$213	.dwtag  DW_TAG_member
	.dwattr $C$DW$213, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$213, DW_AT_name("maxTensorValue")
	.dwattr $C$DW$213, DW_AT_data_member_location[DW_OP_plus_uconst 0x1c]
	.dwattr $C$DW$213, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$213, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$213, DW_AT_decl_line(0x276)
	.dwattr $C$DW$213, DW_AT_decl_column(0x12)

$C$DW$214	.dwtag  DW_TAG_member
	.dwattr $C$DW$214, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$214, DW_AT_name("tensorScale")
	.dwattr $C$DW$214, DW_AT_data_member_location[DW_OP_plus_uconst 0x20]
	.dwattr $C$DW$214, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$214, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$214, DW_AT_decl_line(0x278)
	.dwattr $C$DW$214, DW_AT_decl_column(0x12)

$C$DW$215	.dwtag  DW_TAG_member
	.dwattr $C$DW$215, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$215, DW_AT_name("padW")
	.dwattr $C$DW$215, DW_AT_data_member_location[DW_OP_plus_uconst 0x24]
	.dwattr $C$DW$215, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$215, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$215, DW_AT_decl_line(0x27a)
	.dwattr $C$DW$215, DW_AT_decl_column(0x0b)

$C$DW$216	.dwtag  DW_TAG_member
	.dwattr $C$DW$216, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$216, DW_AT_name("padH")
	.dwattr $C$DW$216, DW_AT_data_member_location[DW_OP_plus_uconst 0x28]
	.dwattr $C$DW$216, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$216, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$216, DW_AT_decl_line(0x27c)
	.dwattr $C$DW$216, DW_AT_decl_column(0x0b)

$C$DW$217	.dwtag  DW_TAG_member
	.dwattr $C$DW$217, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$217, DW_AT_name("batchPadW")
	.dwattr $C$DW$217, DW_AT_data_member_location[DW_OP_plus_uconst 0x2c]
	.dwattr $C$DW$217, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$217, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$217, DW_AT_decl_line(0x27e)
	.dwattr $C$DW$217, DW_AT_decl_column(0x0b)

$C$DW$218	.dwtag  DW_TAG_member
	.dwattr $C$DW$218, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$218, DW_AT_name("batchPadH")
	.dwattr $C$DW$218, DW_AT_data_member_location[DW_OP_plus_uconst 0x30]
	.dwattr $C$DW$218, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$218, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$218, DW_AT_decl_line(0x280)
	.dwattr $C$DW$218, DW_AT_decl_column(0x0b)

$C$DW$219	.dwtag  DW_TAG_member
	.dwattr $C$DW$219, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$219, DW_AT_name("numBatchW")
	.dwattr $C$DW$219, DW_AT_data_member_location[DW_OP_plus_uconst 0x34]
	.dwattr $C$DW$219, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$219, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$219, DW_AT_decl_line(0x282)
	.dwattr $C$DW$219, DW_AT_decl_column(0x0b)

$C$DW$220	.dwtag  DW_TAG_member
	.dwattr $C$DW$220, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$220, DW_AT_name("numBatchH")
	.dwattr $C$DW$220, DW_AT_data_member_location[DW_OP_plus_uconst 0x38]
	.dwattr $C$DW$220, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$220, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$220, DW_AT_decl_line(0x284)
	.dwattr $C$DW$220, DW_AT_decl_column(0x0b)

$C$DW$221	.dwtag  DW_TAG_member
	.dwattr $C$DW$221, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$221, DW_AT_name("roundBits")
	.dwattr $C$DW$221, DW_AT_data_member_location[DW_OP_plus_uconst 0x3c]
	.dwattr $C$DW$221, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$221, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$221, DW_AT_decl_line(0x286)
	.dwattr $C$DW$221, DW_AT_decl_column(0x0b)

$C$DW$222	.dwtag  DW_TAG_member
	.dwattr $C$DW$222, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$222, DW_AT_name("pitch")
	.dwattr $C$DW$222, DW_AT_data_member_location[DW_OP_plus_uconst 0x40]
	.dwattr $C$DW$222, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$222, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$222, DW_AT_decl_line(0x288)
	.dwattr $C$DW$222, DW_AT_decl_column(0x0b)

$C$DW$223	.dwtag  DW_TAG_member
	.dwattr $C$DW$223, DW_AT_type(*$C$DW$T$97)
	.dwattr $C$DW$223, DW_AT_name("dimValues")
	.dwattr $C$DW$223, DW_AT_data_member_location[DW_OP_plus_uconst 0x4c]
	.dwattr $C$DW$223, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$223, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$223, DW_AT_decl_line(0x28a)
	.dwattr $C$DW$223, DW_AT_decl_column(0x0b)


$C$DW$224	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$224, DW_AT_name("operator =")
	.dwattr $C$DW$224, DW_AT_declaration
	.dwattr $C$DW$224, DW_AT_linkage_name("_ZN18sTIDL_DataParams_taSERKS_")
	.dwattr $C$DW$224, DW_AT_type(*$C$DW$T$98)
	.dwattr $C$DW$224, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$225	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$225, DW_AT_type(*$C$DW$T$100)

	.dwendtag $C$DW$224


$C$DW$226	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$226, DW_AT_name("operator =")
	.dwattr $C$DW$226, DW_AT_declaration
	.dwattr $C$DW$226, DW_AT_linkage_name("_ZN18sTIDL_DataParams_taSEOS_")
	.dwattr $C$DW$226, DW_AT_type(*$C$DW$T$98)
	.dwattr $C$DW$226, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$227	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$227, DW_AT_type(*$C$DW$T$98)

	.dwendtag $C$DW$226

	.dwattr $C$DW$T$103, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$103, DW_AT_decl_line(0x266)
	.dwattr $C$DW$T$103, DW_AT_decl_column(0x10)
	.dwendtag $C$DW$T$103

	.dwendtag $C$DW$TU$103


$C$DW$TU$98	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$98
$C$DW$T$98	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$98, DW_AT_type(*$C$DW$T$103)
	.dwattr $C$DW$T$98, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$98


$C$DW$TU$101	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$101

$C$DW$T$101	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$101, DW_AT_type(*$C$DW$T$98)
$C$DW$228	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$228, DW_AT_type(*$C$DW$T$100)

	.dwendtag $C$DW$T$101

	.dwendtag $C$DW$TU$101


$C$DW$TU$102	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$102

$C$DW$T$102	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$102, DW_AT_type(*$C$DW$T$98)
$C$DW$229	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$229, DW_AT_type(*$C$DW$T$98)

	.dwendtag $C$DW$T$102

	.dwendtag $C$DW$TU$102


$C$DW$TU$99	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$99
$C$DW$T$99	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$99, DW_AT_type(*$C$DW$T$103)

	.dwendtag $C$DW$TU$99


$C$DW$TU$100	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$100
$C$DW$T$100	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$100, DW_AT_type(*$C$DW$T$99)
	.dwattr $C$DW$T$100, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$100


$C$DW$TU$186	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$186
$C$DW$T$186	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$186, DW_AT_name("sTIDL_DataParams_t")
	.dwattr $C$DW$T$186, DW_AT_type(*$C$DW$T$103)
	.dwattr $C$DW$T$186, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$186, DW_AT_decl_line(0x28b)
	.dwattr $C$DW$T$186, DW_AT_decl_column(0x02)

	.dwendtag $C$DW$TU$186


$C$DW$TU$187	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$187

$C$DW$T$187	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$187, DW_AT_type(*$C$DW$T$186)
	.dwattr $C$DW$T$187, DW_AT_byte_size(0x5c0)
$C$DW$230	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$230, DW_AT_upper_bound(0x0f)

	.dwendtag $C$DW$T$187

	.dwendtag $C$DW$TU$187


$C$DW$TU$112	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$112

$C$DW$T$112	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$112, DW_AT_name("sTIDL_DepthToSpaceParams_t")
	.dwattr $C$DW$T$112, DW_AT_byte_size(0x04)
$C$DW$231	.dwtag  DW_TAG_member
	.dwattr $C$DW$231, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$231, DW_AT_name("blockSize")
	.dwattr $C$DW$231, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$231, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$231, DW_AT_decl_line(0x2af)
	.dwattr $C$DW$231, DW_AT_decl_column(0x0d)


$C$DW$232	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$232, DW_AT_name("operator =")
	.dwattr $C$DW$232, DW_AT_declaration
	.dwattr $C$DW$232, DW_AT_linkage_name("_ZN26sTIDL_DepthToSpaceParams_taSERKS_")
	.dwattr $C$DW$232, DW_AT_type(*$C$DW$T$107)
	.dwattr $C$DW$232, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$233	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$233, DW_AT_type(*$C$DW$T$109)

	.dwendtag $C$DW$232


$C$DW$234	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$234, DW_AT_name("operator =")
	.dwattr $C$DW$234, DW_AT_declaration
	.dwattr $C$DW$234, DW_AT_linkage_name("_ZN26sTIDL_DepthToSpaceParams_taSEOS_")
	.dwattr $C$DW$234, DW_AT_type(*$C$DW$T$107)
	.dwattr $C$DW$234, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$235	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$235, DW_AT_type(*$C$DW$T$107)

	.dwendtag $C$DW$234

	.dwattr $C$DW$T$112, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$112, DW_AT_decl_line(0x2ad)
	.dwattr $C$DW$T$112, DW_AT_decl_column(0x10)
	.dwendtag $C$DW$T$112

	.dwendtag $C$DW$TU$112


$C$DW$TU$107	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$107
$C$DW$T$107	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$107, DW_AT_type(*$C$DW$T$112)
	.dwattr $C$DW$T$107, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$107


$C$DW$TU$110	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$110

$C$DW$T$110	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$110, DW_AT_type(*$C$DW$T$107)
$C$DW$236	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$236, DW_AT_type(*$C$DW$T$109)

	.dwendtag $C$DW$T$110

	.dwendtag $C$DW$TU$110


$C$DW$TU$111	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$111

$C$DW$T$111	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$111, DW_AT_type(*$C$DW$T$107)
$C$DW$237	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$237, DW_AT_type(*$C$DW$T$107)

	.dwendtag $C$DW$T$111

	.dwendtag $C$DW$TU$111


$C$DW$TU$108	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$108
$C$DW$T$108	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$108, DW_AT_type(*$C$DW$T$112)

	.dwendtag $C$DW$TU$108


$C$DW$TU$109	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$109
$C$DW$T$109	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$109, DW_AT_type(*$C$DW$T$108)
	.dwattr $C$DW$T$109, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$109


$C$DW$TU$152	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$152
$C$DW$T$152	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$152, DW_AT_name("sTIDL_DepthToSpaceParams_t")
	.dwattr $C$DW$T$152, DW_AT_type(*$C$DW$T$112)
	.dwattr $C$DW$T$152, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$152, DW_AT_decl_line(0x2b0)
	.dwattr $C$DW$T$152, DW_AT_decl_column(0x02)

	.dwendtag $C$DW$TU$152


$C$DW$TU$119	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$119

$C$DW$T$119	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$119, DW_AT_name("sTIDL_DetectOutputParams_t")
	.dwattr $C$DW$T$119, DW_AT_byte_size(0x50)
$C$DW$238	.dwtag  DW_TAG_member
	.dwattr $C$DW$238, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$238, DW_AT_name("processingType")
	.dwattr $C$DW$238, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$238, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$238, DW_AT_decl_line(0x44d)
	.dwattr $C$DW$238, DW_AT_decl_column(0x0d)

$C$DW$239	.dwtag  DW_TAG_member
	.dwattr $C$DW$239, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$239, DW_AT_name("priorBox")
	.dwattr $C$DW$239, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$239, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$239, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$239, DW_AT_decl_line(0x44f)
	.dwattr $C$DW$239, DW_AT_decl_column(0x0c)

$C$DW$240	.dwtag  DW_TAG_member
	.dwattr $C$DW$240, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$240, DW_AT_name("priorBoxSize")
	.dwattr $C$DW$240, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$240, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$240, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$240, DW_AT_decl_line(0x451)
	.dwattr $C$DW$240, DW_AT_decl_column(0x0c)

$C$DW$241	.dwtag  DW_TAG_member
	.dwattr $C$DW$241, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$241, DW_AT_name("numClasses")
	.dwattr $C$DW$241, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$241, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$241, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$241, DW_AT_decl_line(0x453)
	.dwattr $C$DW$241, DW_AT_decl_column(0x0c)

$C$DW$242	.dwtag  DW_TAG_member
	.dwattr $C$DW$242, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$242, DW_AT_name("backgroundLabelId")
	.dwattr $C$DW$242, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$242, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$242, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$242, DW_AT_decl_line(0x455)
	.dwattr $C$DW$242, DW_AT_decl_column(0x0c)

$C$DW$243	.dwtag  DW_TAG_member
	.dwattr $C$DW$243, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$243, DW_AT_name("codeType")
	.dwattr $C$DW$243, DW_AT_data_member_location[DW_OP_plus_uconst 0x14]
	.dwattr $C$DW$243, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$243, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$243, DW_AT_decl_line(0x457)
	.dwattr $C$DW$243, DW_AT_decl_column(0x0c)

$C$DW$244	.dwtag  DW_TAG_member
	.dwattr $C$DW$244, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$244, DW_AT_name("confThreshold")
	.dwattr $C$DW$244, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$244, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$244, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$244, DW_AT_decl_line(0x45a)
	.dwattr $C$DW$244, DW_AT_decl_column(0x11)

$C$DW$245	.dwtag  DW_TAG_member
	.dwattr $C$DW$245, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$245, DW_AT_name("nmsThreshold")
	.dwattr $C$DW$245, DW_AT_data_member_location[DW_OP_plus_uconst 0x1c]
	.dwattr $C$DW$245, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$245, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$245, DW_AT_decl_line(0x45d)
	.dwattr $C$DW$245, DW_AT_decl_column(0x11)

$C$DW$246	.dwtag  DW_TAG_member
	.dwattr $C$DW$246, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$246, DW_AT_name("eta")
	.dwattr $C$DW$246, DW_AT_data_member_location[DW_OP_plus_uconst 0x20]
	.dwattr $C$DW$246, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$246, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$246, DW_AT_decl_line(0x45f)
	.dwattr $C$DW$246, DW_AT_decl_column(0x11)

$C$DW$247	.dwtag  DW_TAG_member
	.dwattr $C$DW$247, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$247, DW_AT_name("topK")
	.dwattr $C$DW$247, DW_AT_data_member_location[DW_OP_plus_uconst 0x24]
	.dwattr $C$DW$247, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$247, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$247, DW_AT_decl_line(0x461)
	.dwattr $C$DW$247, DW_AT_decl_column(0x0c)

$C$DW$248	.dwtag  DW_TAG_member
	.dwattr $C$DW$248, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$248, DW_AT_name("keepTopK")
	.dwattr $C$DW$248, DW_AT_data_member_location[DW_OP_plus_uconst 0x28]
	.dwattr $C$DW$248, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$248, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$248, DW_AT_decl_line(0x463)
	.dwattr $C$DW$248, DW_AT_decl_column(0x0c)

$C$DW$249	.dwtag  DW_TAG_member
	.dwattr $C$DW$249, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$249, DW_AT_name("shareLocation")
	.dwattr $C$DW$249, DW_AT_data_member_location[DW_OP_plus_uconst 0x2c]
	.dwattr $C$DW$249, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$249, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$249, DW_AT_decl_line(0x466)
	.dwattr $C$DW$249, DW_AT_decl_column(0x0c)

$C$DW$250	.dwtag  DW_TAG_member
	.dwattr $C$DW$250, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$250, DW_AT_name("varianceEncoded")
	.dwattr $C$DW$250, DW_AT_data_member_location[DW_OP_plus_uconst 0x30]
	.dwattr $C$DW$250, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$250, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$250, DW_AT_decl_line(0x469)
	.dwattr $C$DW$250, DW_AT_decl_column(0x0c)

$C$DW$251	.dwtag  DW_TAG_member
	.dwattr $C$DW$251, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$251, DW_AT_name("numKeypoints")
	.dwattr $C$DW$251, DW_AT_data_member_location[DW_OP_plus_uconst 0x34]
	.dwattr $C$DW$251, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$251, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$251, DW_AT_decl_line(0x46b)
	.dwattr $C$DW$251, DW_AT_decl_column(0x0c)

$C$DW$252	.dwtag  DW_TAG_member
	.dwattr $C$DW$252, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$252, DW_AT_name("numHeads")
	.dwattr $C$DW$252, DW_AT_data_member_location[DW_OP_plus_uconst 0x38]
	.dwattr $C$DW$252, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$252, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$252, DW_AT_decl_line(0x46e)
	.dwattr $C$DW$252, DW_AT_decl_column(0x0c)

$C$DW$253	.dwtag  DW_TAG_member
	.dwattr $C$DW$253, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$253, DW_AT_name("imWidth")
	.dwattr $C$DW$253, DW_AT_data_member_location[DW_OP_plus_uconst 0x3c]
	.dwattr $C$DW$253, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$253, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$253, DW_AT_decl_line(0x471)
	.dwattr $C$DW$253, DW_AT_decl_column(0x0b)

$C$DW$254	.dwtag  DW_TAG_member
	.dwattr $C$DW$254, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$254, DW_AT_name("imHeight")
	.dwattr $C$DW$254, DW_AT_data_member_location[DW_OP_plus_uconst 0x40]
	.dwattr $C$DW$254, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$254, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$254, DW_AT_decl_line(0x474)
	.dwattr $C$DW$254, DW_AT_decl_column(0x0b)

$C$DW$255	.dwtag  DW_TAG_member
	.dwattr $C$DW$255, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$255, DW_AT_name("scoreConverter")
	.dwattr $C$DW$255, DW_AT_data_member_location[DW_OP_plus_uconst 0x44]
	.dwattr $C$DW$255, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$255, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$255, DW_AT_decl_line(0x477)
	.dwattr $C$DW$255, DW_AT_decl_column(0x0b)

$C$DW$256	.dwtag  DW_TAG_member
	.dwattr $C$DW$256, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$256, DW_AT_name("metaArchType")
	.dwattr $C$DW$256, DW_AT_data_member_location[DW_OP_plus_uconst 0x48]
	.dwattr $C$DW$256, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$256, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$256, DW_AT_decl_line(0x47a)
	.dwattr $C$DW$256, DW_AT_decl_column(0x0b)

$C$DW$257	.dwtag  DW_TAG_member
	.dwattr $C$DW$257, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$257, DW_AT_name("dataLayout")
	.dwattr $C$DW$257, DW_AT_data_member_location[DW_OP_plus_uconst 0x4c]
	.dwattr $C$DW$257, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$257, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$257, DW_AT_decl_line(0x47e)
	.dwattr $C$DW$257, DW_AT_decl_column(0x0b)


$C$DW$258	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$258, DW_AT_name("operator =")
	.dwattr $C$DW$258, DW_AT_declaration
	.dwattr $C$DW$258, DW_AT_linkage_name("_ZN26sTIDL_DetectOutputParams_taSERKS_")
	.dwattr $C$DW$258, DW_AT_type(*$C$DW$T$114)
	.dwattr $C$DW$258, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$259	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$259, DW_AT_type(*$C$DW$T$116)

	.dwendtag $C$DW$258


$C$DW$260	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$260, DW_AT_name("operator =")
	.dwattr $C$DW$260, DW_AT_declaration
	.dwattr $C$DW$260, DW_AT_linkage_name("_ZN26sTIDL_DetectOutputParams_taSEOS_")
	.dwattr $C$DW$260, DW_AT_type(*$C$DW$T$114)
	.dwattr $C$DW$260, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$261	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$261, DW_AT_type(*$C$DW$T$114)

	.dwendtag $C$DW$260

	.dwattr $C$DW$T$119, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$119, DW_AT_decl_line(0x44b)
	.dwattr $C$DW$T$119, DW_AT_decl_column(0x10)
	.dwendtag $C$DW$T$119

	.dwendtag $C$DW$TU$119


$C$DW$TU$114	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$114
$C$DW$T$114	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$114, DW_AT_type(*$C$DW$T$119)
	.dwattr $C$DW$T$114, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$114


$C$DW$TU$117	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$117

$C$DW$T$117	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$117, DW_AT_type(*$C$DW$T$114)
$C$DW$262	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$262, DW_AT_type(*$C$DW$T$116)

	.dwendtag $C$DW$T$117

	.dwendtag $C$DW$TU$117


$C$DW$TU$118	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$118

$C$DW$T$118	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$118, DW_AT_type(*$C$DW$T$114)
$C$DW$263	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$263, DW_AT_type(*$C$DW$T$114)

	.dwendtag $C$DW$T$118

	.dwendtag $C$DW$TU$118


$C$DW$TU$115	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$115
$C$DW$T$115	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$115, DW_AT_type(*$C$DW$T$119)

	.dwendtag $C$DW$TU$115


$C$DW$TU$116	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$116
$C$DW$T$116	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$116, DW_AT_type(*$C$DW$T$115)
	.dwattr $C$DW$T$116, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$116


$C$DW$TU$145	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$145
$C$DW$T$145	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$145, DW_AT_name("sTIDL_DetectOutputParams_t")
	.dwattr $C$DW$T$145, DW_AT_type(*$C$DW$T$119)
	.dwattr $C$DW$T$145, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$145, DW_AT_decl_line(0x480)
	.dwattr $C$DW$T$145, DW_AT_decl_column(0x02)

	.dwendtag $C$DW$TU$145


$C$DW$TU$127	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$127

$C$DW$T$127	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$127, DW_AT_name("sTIDL_EltWiseParams_t")
	.dwattr $C$DW$T$127, DW_AT_byte_size(0x58)
$C$DW$264	.dwtag  DW_TAG_member
	.dwattr $C$DW$264, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$264, DW_AT_name("bias")
	.dwattr $C$DW$264, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$264, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$264, DW_AT_decl_line(0x511)
	.dwattr $C$DW$264, DW_AT_decl_column(0x0b)

$C$DW$265	.dwtag  DW_TAG_member
	.dwattr $C$DW$265, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$265, DW_AT_name("numChannels")
	.dwattr $C$DW$265, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$265, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$265, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$265, DW_AT_decl_line(0x513)
	.dwattr $C$DW$265, DW_AT_decl_column(0x0b)

$C$DW$266	.dwtag  DW_TAG_member
	.dwattr $C$DW$266, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$266, DW_AT_name("eltWiseType")
	.dwattr $C$DW$266, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$266, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$266, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$266, DW_AT_decl_line(0x515)
	.dwattr $C$DW$266, DW_AT_decl_column(0x0b)

$C$DW$267	.dwtag  DW_TAG_member
	.dwattr $C$DW$267, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$267, DW_AT_name("numInData")
	.dwattr $C$DW$267, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$267, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$267, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$267, DW_AT_decl_line(0x517)
	.dwattr $C$DW$267, DW_AT_decl_column(0x0b)

$C$DW$268	.dwtag  DW_TAG_member
	.dwattr $C$DW$268, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$268, DW_AT_name("biasQ")
	.dwattr $C$DW$268, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$268, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$268, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$268, DW_AT_decl_line(0x519)
	.dwattr $C$DW$268, DW_AT_decl_column(0x0b)

$C$DW$269	.dwtag  DW_TAG_member
	.dwattr $C$DW$269, DW_AT_type(*$C$DW$T$121)
	.dwattr $C$DW$269, DW_AT_name("inDataQ")
	.dwattr $C$DW$269, DW_AT_data_member_location[DW_OP_plus_uconst 0x14]
	.dwattr $C$DW$269, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$269, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$269, DW_AT_decl_line(0x51b)
	.dwattr $C$DW$269, DW_AT_decl_column(0x0b)

$C$DW$270	.dwtag  DW_TAG_member
	.dwattr $C$DW$270, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$270, DW_AT_name("outDataQ")
	.dwattr $C$DW$270, DW_AT_data_member_location[DW_OP_plus_uconst 0x54]
	.dwattr $C$DW$270, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$270, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$270, DW_AT_decl_line(0x51d)
	.dwattr $C$DW$270, DW_AT_decl_column(0x0b)


$C$DW$271	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$271, DW_AT_name("operator =")
	.dwattr $C$DW$271, DW_AT_declaration
	.dwattr $C$DW$271, DW_AT_linkage_name("_ZN21sTIDL_EltWiseParams_taSERKS_")
	.dwattr $C$DW$271, DW_AT_type(*$C$DW$T$122)
	.dwattr $C$DW$271, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$272	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$272, DW_AT_type(*$C$DW$T$124)

	.dwendtag $C$DW$271


$C$DW$273	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$273, DW_AT_name("operator =")
	.dwattr $C$DW$273, DW_AT_declaration
	.dwattr $C$DW$273, DW_AT_linkage_name("_ZN21sTIDL_EltWiseParams_taSEOS_")
	.dwattr $C$DW$273, DW_AT_type(*$C$DW$T$122)
	.dwattr $C$DW$273, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$274	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$274, DW_AT_type(*$C$DW$T$122)

	.dwendtag $C$DW$273

	.dwattr $C$DW$T$127, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$127, DW_AT_decl_line(0x50f)
	.dwattr $C$DW$T$127, DW_AT_decl_column(0x10)
	.dwendtag $C$DW$T$127

	.dwendtag $C$DW$TU$127


$C$DW$TU$122	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$122
$C$DW$T$122	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$122, DW_AT_type(*$C$DW$T$127)
	.dwattr $C$DW$T$122, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$122


$C$DW$TU$125	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$125

$C$DW$T$125	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$125, DW_AT_type(*$C$DW$T$122)
$C$DW$275	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$275, DW_AT_type(*$C$DW$T$124)

	.dwendtag $C$DW$T$125

	.dwendtag $C$DW$TU$125


$C$DW$TU$126	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$126

$C$DW$T$126	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$126, DW_AT_type(*$C$DW$T$122)
$C$DW$276	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$276, DW_AT_type(*$C$DW$T$122)

	.dwendtag $C$DW$T$126

	.dwendtag $C$DW$TU$126


$C$DW$TU$123	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$123
$C$DW$T$123	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$123, DW_AT_type(*$C$DW$T$127)

	.dwendtag $C$DW$TU$123


$C$DW$TU$124	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$124
$C$DW$T$124	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$124, DW_AT_type(*$C$DW$T$123)
	.dwattr $C$DW$T$124, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$124


$C$DW$TU$138	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$138
$C$DW$T$138	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$138, DW_AT_name("sTIDL_EltWiseParams_t")
	.dwattr $C$DW$T$138, DW_AT_type(*$C$DW$T$127)
	.dwattr $C$DW$T$138, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$138, DW_AT_decl_line(0x51e)
	.dwattr $C$DW$T$138, DW_AT_decl_column(0x02)

	.dwendtag $C$DW$TU$138


$C$DW$TU$135	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$135

$C$DW$T$135	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$135, DW_AT_name("sTIDL_InnerProductParams_t")
	.dwattr $C$DW$T$135, DW_AT_byte_size(0x38)
$C$DW$277	.dwtag  DW_TAG_member
	.dwattr $C$DW$277, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$277, DW_AT_name("weights")
	.dwattr $C$DW$277, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$277, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$277, DW_AT_decl_line(0x4ed)
	.dwattr $C$DW$277, DW_AT_decl_column(0x0d)

$C$DW$278	.dwtag  DW_TAG_member
	.dwattr $C$DW$278, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$278, DW_AT_name("bias")
	.dwattr $C$DW$278, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$278, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$278, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$278, DW_AT_decl_line(0x4ef)
	.dwattr $C$DW$278, DW_AT_decl_column(0x0d)

$C$DW$279	.dwtag  DW_TAG_member
	.dwattr $C$DW$279, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$279, DW_AT_name("activationType")
	.dwattr $C$DW$279, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$279, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$279, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$279, DW_AT_decl_line(0x4f1)
	.dwattr $C$DW$279, DW_AT_decl_column(0x0d)

$C$DW$280	.dwtag  DW_TAG_member
	.dwattr $C$DW$280, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$280, DW_AT_name("numInNodes")
	.dwattr $C$DW$280, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$280, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$280, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$280, DW_AT_decl_line(0x4f3)
	.dwattr $C$DW$280, DW_AT_decl_column(0x0d)

$C$DW$281	.dwtag  DW_TAG_member
	.dwattr $C$DW$281, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$281, DW_AT_name("numOutNodes")
	.dwattr $C$DW$281, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$281, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$281, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$281, DW_AT_decl_line(0x4f5)
	.dwattr $C$DW$281, DW_AT_decl_column(0x0d)

$C$DW$282	.dwtag  DW_TAG_member
	.dwattr $C$DW$282, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$282, DW_AT_name("weightsQ")
	.dwattr $C$DW$282, DW_AT_data_member_location[DW_OP_plus_uconst 0x14]
	.dwattr $C$DW$282, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$282, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$282, DW_AT_decl_line(0x4f7)
	.dwattr $C$DW$282, DW_AT_decl_column(0x0d)

$C$DW$283	.dwtag  DW_TAG_member
	.dwattr $C$DW$283, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$283, DW_AT_name("weightScale")
	.dwattr $C$DW$283, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$283, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$283, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$283, DW_AT_decl_line(0x4f9)
	.dwattr $C$DW$283, DW_AT_decl_column(0x14)

$C$DW$284	.dwtag  DW_TAG_member
	.dwattr $C$DW$284, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$284, DW_AT_name("biasScale")
	.dwattr $C$DW$284, DW_AT_data_member_location[DW_OP_plus_uconst 0x1c]
	.dwattr $C$DW$284, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$284, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$284, DW_AT_decl_line(0x4fb)
	.dwattr $C$DW$284, DW_AT_decl_column(0x14)

$C$DW$285	.dwtag  DW_TAG_member
	.dwattr $C$DW$285, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$285, DW_AT_name("zeroWeightValue")
	.dwattr $C$DW$285, DW_AT_data_member_location[DW_OP_plus_uconst 0x20]
	.dwattr $C$DW$285, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$285, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$285, DW_AT_decl_line(0x4fd)
	.dwattr $C$DW$285, DW_AT_decl_column(0x0d)

$C$DW$286	.dwtag  DW_TAG_member
	.dwattr $C$DW$286, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$286, DW_AT_name("biasQ")
	.dwattr $C$DW$286, DW_AT_data_member_location[DW_OP_plus_uconst 0x24]
	.dwattr $C$DW$286, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$286, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$286, DW_AT_decl_line(0x4ff)
	.dwattr $C$DW$286, DW_AT_decl_column(0x0d)

$C$DW$287	.dwtag  DW_TAG_member
	.dwattr $C$DW$287, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$287, DW_AT_name("inDataQ")
	.dwattr $C$DW$287, DW_AT_data_member_location[DW_OP_plus_uconst 0x28]
	.dwattr $C$DW$287, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$287, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$287, DW_AT_decl_line(0x501)
	.dwattr $C$DW$287, DW_AT_decl_column(0x0d)

$C$DW$288	.dwtag  DW_TAG_member
	.dwattr $C$DW$288, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$288, DW_AT_name("outDataQ")
	.dwattr $C$DW$288, DW_AT_data_member_location[DW_OP_plus_uconst 0x2c]
	.dwattr $C$DW$288, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$288, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$288, DW_AT_decl_line(0x503)
	.dwattr $C$DW$288, DW_AT_decl_column(0x0d)

$C$DW$289	.dwtag  DW_TAG_member
	.dwattr $C$DW$289, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$289, DW_AT_name("interDataQ")
	.dwattr $C$DW$289, DW_AT_data_member_location[DW_OP_plus_uconst 0x30]
	.dwattr $C$DW$289, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$289, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$289, DW_AT_decl_line(0x505)
	.dwattr $C$DW$289, DW_AT_decl_column(0x0d)

$C$DW$290	.dwtag  DW_TAG_member
	.dwattr $C$DW$290, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$290, DW_AT_name("biasB")
	.dwattr $C$DW$290, DW_AT_data_member_location[DW_OP_plus_uconst 0x34]
	.dwattr $C$DW$290, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$290, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$290, DW_AT_decl_line(0x507)
	.dwattr $C$DW$290, DW_AT_decl_column(0x0d)


$C$DW$291	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$291, DW_AT_name("operator =")
	.dwattr $C$DW$291, DW_AT_declaration
	.dwattr $C$DW$291, DW_AT_linkage_name("_ZN26sTIDL_InnerProductParams_taSERKS_")
	.dwattr $C$DW$291, DW_AT_type(*$C$DW$T$130)
	.dwattr $C$DW$291, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$292	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$292, DW_AT_type(*$C$DW$T$132)

	.dwendtag $C$DW$291


$C$DW$293	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$293, DW_AT_name("operator =")
	.dwattr $C$DW$293, DW_AT_declaration
	.dwattr $C$DW$293, DW_AT_linkage_name("_ZN26sTIDL_InnerProductParams_taSEOS_")
	.dwattr $C$DW$293, DW_AT_type(*$C$DW$T$130)
	.dwattr $C$DW$293, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$294	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$294, DW_AT_type(*$C$DW$T$130)

	.dwendtag $C$DW$293

	.dwattr $C$DW$T$135, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$135, DW_AT_decl_line(0x4eb)
	.dwattr $C$DW$T$135, DW_AT_decl_column(0x10)
	.dwendtag $C$DW$T$135

	.dwendtag $C$DW$TU$135


$C$DW$TU$130	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$130
$C$DW$T$130	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$130, DW_AT_type(*$C$DW$T$135)
	.dwattr $C$DW$T$130, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$130


$C$DW$TU$133	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$133

$C$DW$T$133	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$133, DW_AT_type(*$C$DW$T$130)
$C$DW$295	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$295, DW_AT_type(*$C$DW$T$132)

	.dwendtag $C$DW$T$133

	.dwendtag $C$DW$TU$133


$C$DW$TU$134	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$134

$C$DW$T$134	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$134, DW_AT_type(*$C$DW$T$130)
$C$DW$296	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$296, DW_AT_type(*$C$DW$T$130)

	.dwendtag $C$DW$T$134

	.dwendtag $C$DW$TU$134


$C$DW$TU$131	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$131
$C$DW$T$131	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$131, DW_AT_type(*$C$DW$T$135)

	.dwendtag $C$DW$TU$131


$C$DW$TU$132	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$132
$C$DW$T$132	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$132, DW_AT_type(*$C$DW$T$131)
	.dwattr $C$DW$T$132, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$132


$C$DW$TU$139	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$139
$C$DW$T$139	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$139, DW_AT_name("sTIDL_InnerProductParams_t")
	.dwattr $C$DW$T$139, DW_AT_type(*$C$DW$T$135)
	.dwattr $C$DW$T$139, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$139, DW_AT_decl_line(0x508)
	.dwattr $C$DW$T$139, DW_AT_decl_column(0x02)

	.dwendtag $C$DW$TU$139


$C$DW$TU$162	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$162

$C$DW$T$162	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$162, DW_AT_name("sTIDL_LayerParams_t")
	.dwattr $C$DW$T$162, DW_AT_byte_size(0xa8)
$C$DW$297	.dwtag  DW_TAG_member
	.dwattr $C$DW$297, DW_AT_type(*$C$DW$T$137)
	.dwattr $C$DW$297, DW_AT_name("convParams")
	.dwattr $C$DW$297, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$297, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$297, DW_AT_decl_line(0x554)
	.dwattr $C$DW$297, DW_AT_decl_column(0x29)

$C$DW$298	.dwtag  DW_TAG_member
	.dwattr $C$DW$298, DW_AT_type(*$C$DW$T$138)
	.dwattr $C$DW$298, DW_AT_name("eltWiseParams")
	.dwattr $C$DW$298, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$298, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$298, DW_AT_decl_line(0x555)
	.dwattr $C$DW$298, DW_AT_decl_column(0x29)

$C$DW$299	.dwtag  DW_TAG_member
	.dwattr $C$DW$299, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$299, DW_AT_name("poolParams")
	.dwattr $C$DW$299, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$299, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$299, DW_AT_decl_line(0x556)
	.dwattr $C$DW$299, DW_AT_decl_column(0x29)

$C$DW$300	.dwtag  DW_TAG_member
	.dwattr $C$DW$300, DW_AT_type(*$C$DW$T$139)
	.dwattr $C$DW$300, DW_AT_name("innerProductParams")
	.dwattr $C$DW$300, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$300, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$300, DW_AT_decl_line(0x557)
	.dwattr $C$DW$300, DW_AT_decl_column(0x29)

$C$DW$301	.dwtag  DW_TAG_member
	.dwattr $C$DW$301, DW_AT_type(*$C$DW$T$140)
	.dwattr $C$DW$301, DW_AT_name("dataLayerParams")
	.dwattr $C$DW$301, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$301, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$301, DW_AT_decl_line(0x558)
	.dwattr $C$DW$301, DW_AT_decl_column(0x29)

$C$DW$302	.dwtag  DW_TAG_member
	.dwattr $C$DW$302, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$302, DW_AT_name("argMaxParams")
	.dwattr $C$DW$302, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$302, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$302, DW_AT_decl_line(0x559)
	.dwattr $C$DW$302, DW_AT_decl_column(0x29)

$C$DW$303	.dwtag  DW_TAG_member
	.dwattr $C$DW$303, DW_AT_type(*$C$DW$T$142)
	.dwattr $C$DW$303, DW_AT_name("softMaxParams")
	.dwattr $C$DW$303, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$303, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$303, DW_AT_decl_line(0x55a)
	.dwattr $C$DW$303, DW_AT_decl_column(0x29)

$C$DW$304	.dwtag  DW_TAG_member
	.dwattr $C$DW$304, DW_AT_type(*$C$DW$T$143)
	.dwattr $C$DW$304, DW_AT_name("cropParams")
	.dwattr $C$DW$304, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$304, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$304, DW_AT_decl_line(0x55b)
	.dwattr $C$DW$304, DW_AT_decl_column(0x29)

$C$DW$305	.dwtag  DW_TAG_member
	.dwattr $C$DW$305, DW_AT_type(*$C$DW$T$144)
	.dwattr $C$DW$305, DW_AT_name("concatParams")
	.dwattr $C$DW$305, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$305, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$305, DW_AT_decl_line(0x55c)
	.dwattr $C$DW$305, DW_AT_decl_column(0x29)

$C$DW$306	.dwtag  DW_TAG_member
	.dwattr $C$DW$306, DW_AT_type(*$C$DW$T$145)
	.dwattr $C$DW$306, DW_AT_name("detectOutParams")
	.dwattr $C$DW$306, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$306, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$306, DW_AT_decl_line(0x55d)
	.dwattr $C$DW$306, DW_AT_decl_column(0x29)

$C$DW$307	.dwtag  DW_TAG_member
	.dwattr $C$DW$307, DW_AT_type(*$C$DW$T$146)
	.dwattr $C$DW$307, DW_AT_name("biasParams")
	.dwattr $C$DW$307, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$307, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$307, DW_AT_decl_line(0x55e)
	.dwattr $C$DW$307, DW_AT_decl_column(0x29)

$C$DW$308	.dwtag  DW_TAG_member
	.dwattr $C$DW$308, DW_AT_type(*$C$DW$T$147)
	.dwattr $C$DW$308, DW_AT_name("batchNormParams")
	.dwattr $C$DW$308, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$308, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$308, DW_AT_decl_line(0x55f)
	.dwattr $C$DW$308, DW_AT_decl_column(0x29)

$C$DW$309	.dwtag  DW_TAG_member
	.dwattr $C$DW$309, DW_AT_type(*$C$DW$T$148)
	.dwattr $C$DW$309, DW_AT_name("shuffleLayerParams")
	.dwattr $C$DW$309, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$309, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$309, DW_AT_decl_line(0x560)
	.dwattr $C$DW$309, DW_AT_decl_column(0x29)

$C$DW$310	.dwtag  DW_TAG_member
	.dwattr $C$DW$310, DW_AT_type(*$C$DW$T$149)
	.dwattr $C$DW$310, DW_AT_name("sliceParams")
	.dwattr $C$DW$310, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$310, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$310, DW_AT_decl_line(0x561)
	.dwattr $C$DW$310, DW_AT_decl_column(0x29)

$C$DW$311	.dwtag  DW_TAG_member
	.dwattr $C$DW$311, DW_AT_type(*$C$DW$T$150)
	.dwattr $C$DW$311, DW_AT_name("resizeParams")
	.dwattr $C$DW$311, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$311, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$311, DW_AT_decl_line(0x562)
	.dwattr $C$DW$311, DW_AT_decl_column(0x29)

$C$DW$312	.dwtag  DW_TAG_member
	.dwattr $C$DW$312, DW_AT_type(*$C$DW$T$151)
	.dwattr $C$DW$312, DW_AT_name("roiPoolingParams")
	.dwattr $C$DW$312, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$312, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$312, DW_AT_decl_line(0x563)
	.dwattr $C$DW$312, DW_AT_decl_column(0x29)

$C$DW$313	.dwtag  DW_TAG_member
	.dwattr $C$DW$313, DW_AT_type(*$C$DW$T$152)
	.dwattr $C$DW$313, DW_AT_name("depthToSpaceParams")
	.dwattr $C$DW$313, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$313, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$313, DW_AT_decl_line(0x564)
	.dwattr $C$DW$313, DW_AT_decl_column(0x29)

$C$DW$314	.dwtag  DW_TAG_member
	.dwattr $C$DW$314, DW_AT_type(*$C$DW$T$153)
	.dwattr $C$DW$314, DW_AT_name("padLayerParams")
	.dwattr $C$DW$314, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$314, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$314, DW_AT_decl_line(0x565)
	.dwattr $C$DW$314, DW_AT_decl_column(0x29)

$C$DW$315	.dwtag  DW_TAG_member
	.dwattr $C$DW$315, DW_AT_type(*$C$DW$T$154)
	.dwattr $C$DW$315, DW_AT_name("odOutputReformatLayerParams")
	.dwattr $C$DW$315, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$315, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$315, DW_AT_decl_line(0x566)
	.dwattr $C$DW$315, DW_AT_decl_column(0x29)

$C$DW$316	.dwtag  DW_TAG_member
	.dwattr $C$DW$316, DW_AT_type(*$C$DW$T$155)
	.dwattr $C$DW$316, DW_AT_name("dataConvertParams")
	.dwattr $C$DW$316, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$316, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$316, DW_AT_decl_line(0x567)
	.dwattr $C$DW$316, DW_AT_decl_column(0x29)

$C$DW$317	.dwtag  DW_TAG_member
	.dwattr $C$DW$317, DW_AT_type(*$C$DW$T$156)
	.dwattr $C$DW$317, DW_AT_name("customParams")
	.dwattr $C$DW$317, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$317, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$317, DW_AT_decl_line(0x568)
	.dwattr $C$DW$317, DW_AT_decl_column(0x29)


$C$DW$318	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$318, DW_AT_name("operator =")
	.dwattr $C$DW$318, DW_AT_declaration
	.dwattr $C$DW$318, DW_AT_linkage_name("_ZN19sTIDL_LayerParams_taSERKS_")
	.dwattr $C$DW$318, DW_AT_type(*$C$DW$T$157)
	.dwattr $C$DW$318, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$319	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$319, DW_AT_type(*$C$DW$T$159)

	.dwendtag $C$DW$318


$C$DW$320	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$320, DW_AT_name("operator =")
	.dwattr $C$DW$320, DW_AT_declaration
	.dwattr $C$DW$320, DW_AT_linkage_name("_ZN19sTIDL_LayerParams_taSEOS_")
	.dwattr $C$DW$320, DW_AT_type(*$C$DW$T$157)
	.dwattr $C$DW$320, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$321	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$321, DW_AT_type(*$C$DW$T$157)

	.dwendtag $C$DW$320

	.dwattr $C$DW$T$162, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$162, DW_AT_decl_line(0x553)
	.dwattr $C$DW$T$162, DW_AT_decl_column(0x0f)
	.dwendtag $C$DW$T$162

	.dwendtag $C$DW$TU$162


$C$DW$TU$157	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$157
$C$DW$T$157	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$157, DW_AT_type(*$C$DW$T$162)
	.dwattr $C$DW$T$157, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$157


$C$DW$TU$160	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$160

$C$DW$T$160	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$160, DW_AT_type(*$C$DW$T$157)
$C$DW$322	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$322, DW_AT_type(*$C$DW$T$159)

	.dwendtag $C$DW$T$160

	.dwendtag $C$DW$TU$160


$C$DW$TU$161	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$161

$C$DW$T$161	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$161, DW_AT_type(*$C$DW$T$157)
$C$DW$323	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$323, DW_AT_type(*$C$DW$T$157)

	.dwendtag $C$DW$T$161

	.dwendtag $C$DW$TU$161


$C$DW$TU$158	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$158
$C$DW$T$158	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$158, DW_AT_type(*$C$DW$T$162)

	.dwendtag $C$DW$TU$158


$C$DW$TU$159	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$159
$C$DW$T$159	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$159, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$T$159, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$159


$C$DW$TU$184	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$184
$C$DW$T$184	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$184, DW_AT_name("sTIDL_LayerParams_t")
	.dwattr $C$DW$T$184, DW_AT_type(*$C$DW$T$162)
	.dwattr $C$DW$T$184, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$184, DW_AT_decl_line(0x569)
	.dwattr $C$DW$T$184, DW_AT_decl_column(0x02)

	.dwendtag $C$DW$TU$184


$C$DW$TU$193	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$193

$C$DW$T$193	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$193, DW_AT_name("sTIDL_Layer_t")
	.dwattr $C$DW$T$193, DW_AT_byte_size(0xc58)
$C$DW$324	.dwtag  DW_TAG_member
	.dwattr $C$DW$324, DW_AT_type(*$C$DW$T$184)
	.dwattr $C$DW$324, DW_AT_name("layerParams")
	.dwattr $C$DW$324, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$324, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$324, DW_AT_decl_line(0x572)
	.dwattr $C$DW$324, DW_AT_decl_column(0x17)

$C$DW$325	.dwtag  DW_TAG_member
	.dwattr $C$DW$325, DW_AT_type(*$C$DW$T$185)
	.dwattr $C$DW$325, DW_AT_name("actParams")
	.dwattr $C$DW$325, DW_AT_data_member_location[DW_OP_plus_uconst 0xa8]
	.dwattr $C$DW$325, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$325, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$325, DW_AT_decl_line(0x574)
	.dwattr $C$DW$325, DW_AT_decl_column(0x18)

$C$DW$326	.dwtag  DW_TAG_member
	.dwattr $C$DW$326, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$326, DW_AT_name("layerType")
	.dwattr $C$DW$326, DW_AT_data_member_location[DW_OP_plus_uconst 0xbc]
	.dwattr $C$DW$326, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$326, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$326, DW_AT_decl_line(0x576)
	.dwattr $C$DW$326, DW_AT_decl_column(0x0b)

$C$DW$327	.dwtag  DW_TAG_member
	.dwattr $C$DW$327, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$327, DW_AT_name("numInBufs")
	.dwattr $C$DW$327, DW_AT_data_member_location[DW_OP_plus_uconst 0xc0]
	.dwattr $C$DW$327, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$327, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$327, DW_AT_decl_line(0x578)
	.dwattr $C$DW$327, DW_AT_decl_column(0x0b)

$C$DW$328	.dwtag  DW_TAG_member
	.dwattr $C$DW$328, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$328, DW_AT_name("numOutBufs")
	.dwattr $C$DW$328, DW_AT_data_member_location[DW_OP_plus_uconst 0xc4]
	.dwattr $C$DW$328, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$328, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$328, DW_AT_decl_line(0x57a)
	.dwattr $C$DW$328, DW_AT_decl_column(0x0b)

$C$DW$329	.dwtag  DW_TAG_member
	.dwattr $C$DW$329, DW_AT_type(*$C$DW$T$187)
	.dwattr $C$DW$329, DW_AT_name("inData")
	.dwattr $C$DW$329, DW_AT_data_member_location[DW_OP_plus_uconst 0xc8]
	.dwattr $C$DW$329, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$329, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$329, DW_AT_decl_line(0x57c)
	.dwattr $C$DW$329, DW_AT_decl_column(0x16)

$C$DW$330	.dwtag  DW_TAG_member
	.dwattr $C$DW$330, DW_AT_type(*$C$DW$T$187)
	.dwattr $C$DW$330, DW_AT_name("outData")
	.dwattr $C$DW$330, DW_AT_data_member_location[DW_OP_plus_uconst 0x688]
	.dwattr $C$DW$330, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$330, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$330, DW_AT_decl_line(0x57e)
	.dwattr $C$DW$330, DW_AT_decl_column(0x16)

$C$DW$331	.dwtag  DW_TAG_member
	.dwattr $C$DW$331, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$331, DW_AT_name("coreID")
	.dwattr $C$DW$331, DW_AT_data_member_location[DW_OP_plus_uconst 0xc48]
	.dwattr $C$DW$331, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$331, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$331, DW_AT_decl_line(0x580)
	.dwattr $C$DW$331, DW_AT_decl_column(0x0b)

$C$DW$332	.dwtag  DW_TAG_member
	.dwattr $C$DW$332, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$332, DW_AT_name("layersGroupId")
	.dwattr $C$DW$332, DW_AT_data_member_location[DW_OP_plus_uconst 0xc4c]
	.dwattr $C$DW$332, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$332, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$332, DW_AT_decl_line(0x583)
	.dwattr $C$DW$332, DW_AT_decl_column(0x0b)

$C$DW$333	.dwtag  DW_TAG_member
	.dwattr $C$DW$333, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$333, DW_AT_name("weightsElementSizeInBits")
	.dwattr $C$DW$333, DW_AT_data_member_location[DW_OP_plus_uconst 0xc50]
	.dwattr $C$DW$333, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$333, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$333, DW_AT_decl_line(0x585)
	.dwattr $C$DW$333, DW_AT_decl_column(0x0b)

$C$DW$334	.dwtag  DW_TAG_member
	.dwattr $C$DW$334, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$334, DW_AT_name("strideOffsetMethod")
	.dwattr $C$DW$334, DW_AT_data_member_location[DW_OP_plus_uconst 0xc54]
	.dwattr $C$DW$334, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$334, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$334, DW_AT_decl_line(0x587)
	.dwattr $C$DW$334, DW_AT_decl_column(0x0b)


$C$DW$335	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$335, DW_AT_name("operator =")
	.dwattr $C$DW$335, DW_AT_declaration
	.dwattr $C$DW$335, DW_AT_linkage_name("_ZN13sTIDL_Layer_taSERKS_")
	.dwattr $C$DW$335, DW_AT_type(*$C$DW$T$188)
	.dwattr $C$DW$335, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$336	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$336, DW_AT_type(*$C$DW$T$190)

	.dwendtag $C$DW$335


$C$DW$337	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$337, DW_AT_name("operator =")
	.dwattr $C$DW$337, DW_AT_declaration
	.dwattr $C$DW$337, DW_AT_linkage_name("_ZN13sTIDL_Layer_taSEOS_")
	.dwattr $C$DW$337, DW_AT_type(*$C$DW$T$188)
	.dwattr $C$DW$337, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$338	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$338, DW_AT_type(*$C$DW$T$188)

	.dwendtag $C$DW$337

	.dwattr $C$DW$T$193, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$193, DW_AT_decl_line(0x571)
	.dwattr $C$DW$T$193, DW_AT_decl_column(0x10)
	.dwendtag $C$DW$T$193

	.dwendtag $C$DW$TU$193


$C$DW$TU$188	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$188
$C$DW$T$188	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$188, DW_AT_type(*$C$DW$T$193)
	.dwattr $C$DW$T$188, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$188


$C$DW$TU$191	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$191

$C$DW$T$191	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$191, DW_AT_type(*$C$DW$T$188)
$C$DW$339	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$339, DW_AT_type(*$C$DW$T$190)

	.dwendtag $C$DW$T$191

	.dwendtag $C$DW$TU$191


$C$DW$TU$192	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$192

$C$DW$T$192	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$192, DW_AT_type(*$C$DW$T$188)
$C$DW$340	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$340, DW_AT_type(*$C$DW$T$188)

	.dwendtag $C$DW$T$192

	.dwendtag $C$DW$TU$192


$C$DW$TU$189	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$189
$C$DW$T$189	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$189, DW_AT_type(*$C$DW$T$193)

	.dwendtag $C$DW$TU$189


$C$DW$TU$190	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$190
$C$DW$T$190	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$190, DW_AT_type(*$C$DW$T$189)
	.dwattr $C$DW$T$190, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$190


$C$DW$TU$284	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$284
$C$DW$T$284	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$284, DW_AT_name("sTIDL_Layer_t")
	.dwattr $C$DW$T$284, DW_AT_type(*$C$DW$T$193)
	.dwattr $C$DW$T$284, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$284, DW_AT_decl_line(0x588)
	.dwattr $C$DW$T$284, DW_AT_decl_column(0x02)

	.dwendtag $C$DW$TU$284


$C$DW$TU$285	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$285
$C$DW$T$285	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$285, DW_AT_type(*$C$DW$T$284)
	.dwattr $C$DW$T$285, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$285


$C$DW$TU$204	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$204

$C$DW$T$204	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$204, DW_AT_name("sTIDL_PadLayerParams_t")
	.dwattr $C$DW$T$204, DW_AT_byte_size(0x1c)
$C$DW$341	.dwtag  DW_TAG_member
	.dwattr $C$DW$341, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$341, DW_AT_name("padT")
	.dwattr $C$DW$341, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$341, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$341, DW_AT_decl_line(0x2b9)
	.dwattr $C$DW$341, DW_AT_decl_column(0x0b)

$C$DW$342	.dwtag  DW_TAG_member
	.dwattr $C$DW$342, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$342, DW_AT_name("padB")
	.dwattr $C$DW$342, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$342, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$342, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$342, DW_AT_decl_line(0x2bb)
	.dwattr $C$DW$342, DW_AT_decl_column(0x0b)

$C$DW$343	.dwtag  DW_TAG_member
	.dwattr $C$DW$343, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$343, DW_AT_name("padL")
	.dwattr $C$DW$343, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$343, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$343, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$343, DW_AT_decl_line(0x2bd)
	.dwattr $C$DW$343, DW_AT_decl_column(0x0b)

$C$DW$344	.dwtag  DW_TAG_member
	.dwattr $C$DW$344, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$344, DW_AT_name("padR")
	.dwattr $C$DW$344, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$344, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$344, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$344, DW_AT_decl_line(0x2bf)
	.dwattr $C$DW$344, DW_AT_decl_column(0x0b)

$C$DW$345	.dwtag  DW_TAG_member
	.dwattr $C$DW$345, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$345, DW_AT_name("padConstValue")
	.dwattr $C$DW$345, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$345, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$345, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$345, DW_AT_decl_line(0x2c1)
	.dwattr $C$DW$345, DW_AT_decl_column(0x0d)

$C$DW$346	.dwtag  DW_TAG_member
	.dwattr $C$DW$346, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$346, DW_AT_name("padType")
	.dwattr $C$DW$346, DW_AT_data_member_location[DW_OP_plus_uconst 0x14]
	.dwattr $C$DW$346, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$346, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$346, DW_AT_decl_line(0x2c3)
	.dwattr $C$DW$346, DW_AT_decl_column(0x0d)

$C$DW$347	.dwtag  DW_TAG_member
	.dwattr $C$DW$347, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$347, DW_AT_name("perChannelPadConstTensorOffset")
	.dwattr $C$DW$347, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$347, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$347, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$347, DW_AT_decl_line(0x2c6)
	.dwattr $C$DW$347, DW_AT_decl_column(0x0d)


$C$DW$348	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$348, DW_AT_name("operator =")
	.dwattr $C$DW$348, DW_AT_declaration
	.dwattr $C$DW$348, DW_AT_linkage_name("_ZN22sTIDL_PadLayerParams_taSERKS_")
	.dwattr $C$DW$348, DW_AT_type(*$C$DW$T$199)
	.dwattr $C$DW$348, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$349	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$349, DW_AT_type(*$C$DW$T$201)

	.dwendtag $C$DW$348


$C$DW$350	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$350, DW_AT_name("operator =")
	.dwattr $C$DW$350, DW_AT_declaration
	.dwattr $C$DW$350, DW_AT_linkage_name("_ZN22sTIDL_PadLayerParams_taSEOS_")
	.dwattr $C$DW$350, DW_AT_type(*$C$DW$T$199)
	.dwattr $C$DW$350, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$351	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$351, DW_AT_type(*$C$DW$T$199)

	.dwendtag $C$DW$350

	.dwattr $C$DW$T$204, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$204, DW_AT_decl_line(0x2b7)
	.dwattr $C$DW$T$204, DW_AT_decl_column(0x10)
	.dwendtag $C$DW$T$204

	.dwendtag $C$DW$TU$204


$C$DW$TU$199	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$199
$C$DW$T$199	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$199, DW_AT_type(*$C$DW$T$204)
	.dwattr $C$DW$T$199, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$199


$C$DW$TU$202	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$202

$C$DW$T$202	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$202, DW_AT_type(*$C$DW$T$199)
$C$DW$352	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$352, DW_AT_type(*$C$DW$T$201)

	.dwendtag $C$DW$T$202

	.dwendtag $C$DW$TU$202


$C$DW$TU$203	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$203

$C$DW$T$203	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$203, DW_AT_type(*$C$DW$T$199)
$C$DW$353	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$353, DW_AT_type(*$C$DW$T$199)

	.dwendtag $C$DW$T$203

	.dwendtag $C$DW$TU$203


$C$DW$TU$153	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$153
$C$DW$T$153	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$153, DW_AT_name("sTIDL_PadLayerParams_t")
	.dwattr $C$DW$T$153, DW_AT_type(*$C$DW$T$204)
	.dwattr $C$DW$T$153, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$153, DW_AT_decl_line(0x2c7)
	.dwattr $C$DW$T$153, DW_AT_decl_column(0x02)

	.dwendtag $C$DW$TU$153


$C$DW$TU$200	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$200
$C$DW$T$200	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$200, DW_AT_type(*$C$DW$T$204)

	.dwendtag $C$DW$TU$200


$C$DW$TU$201	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$201
$C$DW$T$201	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$201, DW_AT_type(*$C$DW$T$200)
	.dwattr $C$DW$T$201, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$201


$C$DW$TU$211	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$211

$C$DW$T$211	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$211, DW_AT_name("sTIDL_PoolingParams_t")
	.dwattr $C$DW$T$211, DW_AT_byte_size(0x2c)
$C$DW$354	.dwtag  DW_TAG_member
	.dwattr $C$DW$354, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$354, DW_AT_name("numChannels")
	.dwattr $C$DW$354, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$354, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$354, DW_AT_decl_line(0x387)
	.dwattr $C$DW$354, DW_AT_decl_column(0x0d)

$C$DW$355	.dwtag  DW_TAG_member
	.dwattr $C$DW$355, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$355, DW_AT_name("poolingType")
	.dwattr $C$DW$355, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$355, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$355, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$355, DW_AT_decl_line(0x389)
	.dwattr $C$DW$355, DW_AT_decl_column(0x0d)

$C$DW$356	.dwtag  DW_TAG_member
	.dwattr $C$DW$356, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$356, DW_AT_name("kernelW")
	.dwattr $C$DW$356, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$356, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$356, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$356, DW_AT_decl_line(0x38b)
	.dwattr $C$DW$356, DW_AT_decl_column(0x0d)

$C$DW$357	.dwtag  DW_TAG_member
	.dwattr $C$DW$357, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$357, DW_AT_name("kernelH")
	.dwattr $C$DW$357, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$357, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$357, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$357, DW_AT_decl_line(0x38d)
	.dwattr $C$DW$357, DW_AT_decl_column(0x0d)

$C$DW$358	.dwtag  DW_TAG_member
	.dwattr $C$DW$358, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$358, DW_AT_name("strideW")
	.dwattr $C$DW$358, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$358, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$358, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$358, DW_AT_decl_line(0x38f)
	.dwattr $C$DW$358, DW_AT_decl_column(0x0d)

$C$DW$359	.dwtag  DW_TAG_member
	.dwattr $C$DW$359, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$359, DW_AT_name("strideH")
	.dwattr $C$DW$359, DW_AT_data_member_location[DW_OP_plus_uconst 0x14]
	.dwattr $C$DW$359, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$359, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$359, DW_AT_decl_line(0x391)
	.dwattr $C$DW$359, DW_AT_decl_column(0x0d)

$C$DW$360	.dwtag  DW_TAG_member
	.dwattr $C$DW$360, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$360, DW_AT_name("padW")
	.dwattr $C$DW$360, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$360, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$360, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$360, DW_AT_decl_line(0x393)
	.dwattr $C$DW$360, DW_AT_decl_column(0x0d)

$C$DW$361	.dwtag  DW_TAG_member
	.dwattr $C$DW$361, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$361, DW_AT_name("padH")
	.dwattr $C$DW$361, DW_AT_data_member_location[DW_OP_plus_uconst 0x1c]
	.dwattr $C$DW$361, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$361, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$361, DW_AT_decl_line(0x395)
	.dwattr $C$DW$361, DW_AT_decl_column(0x0d)

$C$DW$362	.dwtag  DW_TAG_member
	.dwattr $C$DW$362, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$362, DW_AT_name("inDataQ")
	.dwattr $C$DW$362, DW_AT_data_member_location[DW_OP_plus_uconst 0x20]
	.dwattr $C$DW$362, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$362, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$362, DW_AT_decl_line(0x397)
	.dwattr $C$DW$362, DW_AT_decl_column(0x0d)

$C$DW$363	.dwtag  DW_TAG_member
	.dwattr $C$DW$363, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$363, DW_AT_name("outDataQ")
	.dwattr $C$DW$363, DW_AT_data_member_location[DW_OP_plus_uconst 0x24]
	.dwattr $C$DW$363, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$363, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$363, DW_AT_decl_line(0x399)
	.dwattr $C$DW$363, DW_AT_decl_column(0x0d)

$C$DW$364	.dwtag  DW_TAG_member
	.dwattr $C$DW$364, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$364, DW_AT_name("useCeil")
	.dwattr $C$DW$364, DW_AT_data_member_location[DW_OP_plus_uconst 0x28]
	.dwattr $C$DW$364, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$364, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$364, DW_AT_decl_line(0x39b)
	.dwattr $C$DW$364, DW_AT_decl_column(0x0d)


$C$DW$365	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$365, DW_AT_name("operator =")
	.dwattr $C$DW$365, DW_AT_declaration
	.dwattr $C$DW$365, DW_AT_linkage_name("_ZN21sTIDL_PoolingParams_taSERKS_")
	.dwattr $C$DW$365, DW_AT_type(*$C$DW$T$206)
	.dwattr $C$DW$365, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$366	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$366, DW_AT_type(*$C$DW$T$208)

	.dwendtag $C$DW$365


$C$DW$367	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$367, DW_AT_name("operator =")
	.dwattr $C$DW$367, DW_AT_declaration
	.dwattr $C$DW$367, DW_AT_linkage_name("_ZN21sTIDL_PoolingParams_taSEOS_")
	.dwattr $C$DW$367, DW_AT_type(*$C$DW$T$206)
	.dwattr $C$DW$367, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$368	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$368, DW_AT_type(*$C$DW$T$206)

	.dwendtag $C$DW$367

	.dwattr $C$DW$T$211, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$211, DW_AT_decl_line(0x385)
	.dwattr $C$DW$T$211, DW_AT_decl_column(0x10)
	.dwendtag $C$DW$T$211

	.dwendtag $C$DW$TU$211


$C$DW$TU$206	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$206
$C$DW$T$206	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$206, DW_AT_type(*$C$DW$T$211)
	.dwattr $C$DW$T$206, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$206


$C$DW$TU$209	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$209

$C$DW$T$209	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$209, DW_AT_type(*$C$DW$T$206)
$C$DW$369	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$369, DW_AT_type(*$C$DW$T$208)

	.dwendtag $C$DW$T$209

	.dwendtag $C$DW$TU$209


$C$DW$TU$210	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$210

$C$DW$T$210	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$210, DW_AT_type(*$C$DW$T$206)
$C$DW$370	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$370, DW_AT_type(*$C$DW$T$206)

	.dwendtag $C$DW$T$210

	.dwendtag $C$DW$TU$210


$C$DW$TU$66	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$66
$C$DW$T$66	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$66, DW_AT_name("sTIDL_PoolingParams_t")
	.dwattr $C$DW$T$66, DW_AT_type(*$C$DW$T$211)
	.dwattr $C$DW$T$66, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$66, DW_AT_decl_line(0x39c)
	.dwattr $C$DW$T$66, DW_AT_decl_column(0x02)

	.dwendtag $C$DW$TU$66


$C$DW$TU$207	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$207
$C$DW$T$207	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$207, DW_AT_type(*$C$DW$T$211)

	.dwendtag $C$DW$TU$207


$C$DW$TU$208	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$208
$C$DW$T$208	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$208, DW_AT_type(*$C$DW$T$207)
	.dwattr $C$DW$T$208, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$208


$C$DW$TU$219	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$219

$C$DW$T$219	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$219, DW_AT_name("sTIDL_ResizeLayerParams_t")
	.dwattr $C$DW$T$219, DW_AT_byte_size(0x14)
$C$DW$371	.dwtag  DW_TAG_member
	.dwattr $C$DW$371, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$371, DW_AT_name("mode")
	.dwattr $C$DW$371, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$371, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$371, DW_AT_decl_line(0x2e8)
	.dwattr $C$DW$371, DW_AT_decl_column(0x0d)

$C$DW$372	.dwtag  DW_TAG_member
	.dwattr $C$DW$372, DW_AT_type(*$C$DW$T$213)
	.dwattr $C$DW$372, DW_AT_name("resizeRatio")
	.dwattr $C$DW$372, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$372, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$372, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$372, DW_AT_decl_line(0x2ea)
	.dwattr $C$DW$372, DW_AT_decl_column(0x14)


$C$DW$373	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$373, DW_AT_name("operator =")
	.dwattr $C$DW$373, DW_AT_declaration
	.dwattr $C$DW$373, DW_AT_linkage_name("_ZN25sTIDL_ResizeLayerParams_taSERKS_")
	.dwattr $C$DW$373, DW_AT_type(*$C$DW$T$214)
	.dwattr $C$DW$373, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$374	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$374, DW_AT_type(*$C$DW$T$216)

	.dwendtag $C$DW$373


$C$DW$375	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$375, DW_AT_name("operator =")
	.dwattr $C$DW$375, DW_AT_declaration
	.dwattr $C$DW$375, DW_AT_linkage_name("_ZN25sTIDL_ResizeLayerParams_taSEOS_")
	.dwattr $C$DW$375, DW_AT_type(*$C$DW$T$214)
	.dwattr $C$DW$375, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$376	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$376, DW_AT_type(*$C$DW$T$214)

	.dwendtag $C$DW$375

	.dwattr $C$DW$T$219, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$219, DW_AT_decl_line(0x2e6)
	.dwattr $C$DW$T$219, DW_AT_decl_column(0x10)
	.dwendtag $C$DW$T$219

	.dwendtag $C$DW$TU$219


$C$DW$TU$214	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$214
$C$DW$T$214	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$214, DW_AT_type(*$C$DW$T$219)
	.dwattr $C$DW$T$214, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$214


$C$DW$TU$217	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$217

$C$DW$T$217	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$217, DW_AT_type(*$C$DW$T$214)
$C$DW$377	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$377, DW_AT_type(*$C$DW$T$216)

	.dwendtag $C$DW$T$217

	.dwendtag $C$DW$TU$217


$C$DW$TU$218	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$218

$C$DW$T$218	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$218, DW_AT_type(*$C$DW$T$214)
$C$DW$378	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$378, DW_AT_type(*$C$DW$T$214)

	.dwendtag $C$DW$T$218

	.dwendtag $C$DW$TU$218


$C$DW$TU$150	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$150
$C$DW$T$150	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$150, DW_AT_name("sTIDL_ResizeLayerParams_t")
	.dwattr $C$DW$T$150, DW_AT_type(*$C$DW$T$219)
	.dwattr $C$DW$T$150, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$150, DW_AT_decl_line(0x2eb)
	.dwattr $C$DW$T$150, DW_AT_decl_column(0x02)

	.dwendtag $C$DW$TU$150


$C$DW$TU$215	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$215
$C$DW$T$215	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$215, DW_AT_type(*$C$DW$T$219)

	.dwendtag $C$DW$TU$215


$C$DW$TU$216	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$216
$C$DW$T$216	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$216, DW_AT_type(*$C$DW$T$215)
	.dwattr $C$DW$T$216, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$216


$C$DW$TU$227	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$227

$C$DW$T$227	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$227, DW_AT_name("sTIDL_RoiPoolingLayerParams_t")
	.dwattr $C$DW$T$227, DW_AT_byte_size(0x0c)
$C$DW$379	.dwtag  DW_TAG_member
	.dwattr $C$DW$379, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$379, DW_AT_name("poolingType")
	.dwattr $C$DW$379, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$379, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$379, DW_AT_decl_line(0x31f)
	.dwattr $C$DW$379, DW_AT_decl_column(0x0d)

$C$DW$380	.dwtag  DW_TAG_member
	.dwattr $C$DW$380, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$380, DW_AT_name("imWidth")
	.dwattr $C$DW$380, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$380, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$380, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$380, DW_AT_decl_line(0x321)
	.dwattr $C$DW$380, DW_AT_decl_column(0x0b)

$C$DW$381	.dwtag  DW_TAG_member
	.dwattr $C$DW$381, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$381, DW_AT_name("imHeight")
	.dwattr $C$DW$381, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$381, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$381, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$381, DW_AT_decl_line(0x323)
	.dwattr $C$DW$381, DW_AT_decl_column(0x0b)


$C$DW$382	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$382, DW_AT_name("operator =")
	.dwattr $C$DW$382, DW_AT_declaration
	.dwattr $C$DW$382, DW_AT_linkage_name("_ZN29sTIDL_RoiPoolingLayerParams_taSERKS_")
	.dwattr $C$DW$382, DW_AT_type(*$C$DW$T$222)
	.dwattr $C$DW$382, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$383	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$383, DW_AT_type(*$C$DW$T$224)

	.dwendtag $C$DW$382


$C$DW$384	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$384, DW_AT_name("operator =")
	.dwattr $C$DW$384, DW_AT_declaration
	.dwattr $C$DW$384, DW_AT_linkage_name("_ZN29sTIDL_RoiPoolingLayerParams_taSEOS_")
	.dwattr $C$DW$384, DW_AT_type(*$C$DW$T$222)
	.dwattr $C$DW$384, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$385	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$385, DW_AT_type(*$C$DW$T$222)

	.dwendtag $C$DW$384

	.dwattr $C$DW$T$227, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$227, DW_AT_decl_line(0x31d)
	.dwattr $C$DW$T$227, DW_AT_decl_column(0x10)
	.dwendtag $C$DW$T$227

	.dwendtag $C$DW$TU$227


$C$DW$TU$222	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$222
$C$DW$T$222	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$222, DW_AT_type(*$C$DW$T$227)
	.dwattr $C$DW$T$222, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$222


$C$DW$TU$225	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$225

$C$DW$T$225	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$225, DW_AT_type(*$C$DW$T$222)
$C$DW$386	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$386, DW_AT_type(*$C$DW$T$224)

	.dwendtag $C$DW$T$225

	.dwendtag $C$DW$TU$225


$C$DW$TU$226	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$226

$C$DW$T$226	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$226, DW_AT_type(*$C$DW$T$222)
$C$DW$387	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$387, DW_AT_type(*$C$DW$T$222)

	.dwendtag $C$DW$T$226

	.dwendtag $C$DW$TU$226


$C$DW$TU$151	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$151
$C$DW$T$151	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$151, DW_AT_name("sTIDL_RoiPoolingLayerParams_t")
	.dwattr $C$DW$T$151, DW_AT_type(*$C$DW$T$227)
	.dwattr $C$DW$T$151, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$151, DW_AT_decl_line(0x324)
	.dwattr $C$DW$T$151, DW_AT_decl_column(0x02)

	.dwendtag $C$DW$TU$151


$C$DW$TU$223	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$223
$C$DW$T$223	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$223, DW_AT_type(*$C$DW$T$227)

	.dwendtag $C$DW$TU$223


$C$DW$TU$224	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$224
$C$DW$T$224	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$224, DW_AT_type(*$C$DW$T$223)
	.dwattr $C$DW$T$224, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$224


$C$DW$TU$234	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$234

$C$DW$T$234	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$234, DW_AT_name("sTIDL_ShuffleLayerParams_t")
	.dwattr $C$DW$T$234, DW_AT_byte_size(0x08)
$C$DW$388	.dwtag  DW_TAG_member
	.dwattr $C$DW$388, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$388, DW_AT_name("numGroups")
	.dwattr $C$DW$388, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$388, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$388, DW_AT_decl_line(0x2a3)
	.dwattr $C$DW$388, DW_AT_decl_column(0x0d)

$C$DW$389	.dwtag  DW_TAG_member
	.dwattr $C$DW$389, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$389, DW_AT_name("resvd")
	.dwattr $C$DW$389, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$389, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$389, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$389, DW_AT_decl_line(0x2a5)
	.dwattr $C$DW$389, DW_AT_decl_column(0x0d)


$C$DW$390	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$390, DW_AT_name("operator =")
	.dwattr $C$DW$390, DW_AT_declaration
	.dwattr $C$DW$390, DW_AT_linkage_name("_ZN26sTIDL_ShuffleLayerParams_taSERKS_")
	.dwattr $C$DW$390, DW_AT_type(*$C$DW$T$229)
	.dwattr $C$DW$390, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$391	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$391, DW_AT_type(*$C$DW$T$231)

	.dwendtag $C$DW$390


$C$DW$392	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$392, DW_AT_name("operator =")
	.dwattr $C$DW$392, DW_AT_declaration
	.dwattr $C$DW$392, DW_AT_linkage_name("_ZN26sTIDL_ShuffleLayerParams_taSEOS_")
	.dwattr $C$DW$392, DW_AT_type(*$C$DW$T$229)
	.dwattr $C$DW$392, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$393	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$393, DW_AT_type(*$C$DW$T$229)

	.dwendtag $C$DW$392

	.dwattr $C$DW$T$234, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$234, DW_AT_decl_line(0x2a1)
	.dwattr $C$DW$T$234, DW_AT_decl_column(0x10)
	.dwendtag $C$DW$T$234

	.dwendtag $C$DW$TU$234


$C$DW$TU$229	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$229
$C$DW$T$229	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$229, DW_AT_type(*$C$DW$T$234)
	.dwattr $C$DW$T$229, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$229


$C$DW$TU$232	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$232

$C$DW$T$232	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$232, DW_AT_type(*$C$DW$T$229)
$C$DW$394	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$394, DW_AT_type(*$C$DW$T$231)

	.dwendtag $C$DW$T$232

	.dwendtag $C$DW$TU$232


$C$DW$TU$233	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$233

$C$DW$T$233	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$233, DW_AT_type(*$C$DW$T$229)
$C$DW$395	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$395, DW_AT_type(*$C$DW$T$229)

	.dwendtag $C$DW$T$233

	.dwendtag $C$DW$TU$233


$C$DW$TU$148	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$148
$C$DW$T$148	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$148, DW_AT_name("sTIDL_ShuffleLayerParams_t")
	.dwattr $C$DW$T$148, DW_AT_type(*$C$DW$T$234)
	.dwattr $C$DW$T$148, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$148, DW_AT_decl_line(0x2a6)
	.dwattr $C$DW$T$148, DW_AT_decl_column(0x02)

	.dwendtag $C$DW$TU$148


$C$DW$TU$230	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$230
$C$DW$T$230	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$230, DW_AT_type(*$C$DW$T$234)

	.dwendtag $C$DW$TU$230


$C$DW$TU$231	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$231
$C$DW$T$231	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$231, DW_AT_type(*$C$DW$T$230)
	.dwattr $C$DW$T$231, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$231


$C$DW$TU$242	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$242

$C$DW$T$242	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$242, DW_AT_name("sTIDL_SliceLayerParams_t")
	.dwattr $C$DW$T$242, DW_AT_byte_size(0x4c)
$C$DW$396	.dwtag  DW_TAG_member
	.dwattr $C$DW$396, DW_AT_type(*$C$DW$T$236)
	.dwattr $C$DW$396, DW_AT_name("slicePoints")
	.dwattr $C$DW$396, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$396, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$396, DW_AT_decl_line(0x32e)
	.dwattr $C$DW$396, DW_AT_decl_column(0x0d)

$C$DW$397	.dwtag  DW_TAG_member
	.dwattr $C$DW$397, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$397, DW_AT_name("axis")
	.dwattr $C$DW$397, DW_AT_data_member_location[DW_OP_plus_uconst 0x44]
	.dwattr $C$DW$397, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$397, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$397, DW_AT_decl_line(0x330)
	.dwattr $C$DW$397, DW_AT_decl_column(0x0c)

$C$DW$398	.dwtag  DW_TAG_member
	.dwattr $C$DW$398, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$398, DW_AT_name("stride")
	.dwattr $C$DW$398, DW_AT_data_member_location[DW_OP_plus_uconst 0x48]
	.dwattr $C$DW$398, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$398, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$398, DW_AT_decl_line(0x332)
	.dwattr $C$DW$398, DW_AT_decl_column(0x0c)


$C$DW$399	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$399, DW_AT_name("operator =")
	.dwattr $C$DW$399, DW_AT_declaration
	.dwattr $C$DW$399, DW_AT_linkage_name("_ZN24sTIDL_SliceLayerParams_taSERKS_")
	.dwattr $C$DW$399, DW_AT_type(*$C$DW$T$237)
	.dwattr $C$DW$399, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$400	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$400, DW_AT_type(*$C$DW$T$239)

	.dwendtag $C$DW$399


$C$DW$401	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$401, DW_AT_name("operator =")
	.dwattr $C$DW$401, DW_AT_declaration
	.dwattr $C$DW$401, DW_AT_linkage_name("_ZN24sTIDL_SliceLayerParams_taSEOS_")
	.dwattr $C$DW$401, DW_AT_type(*$C$DW$T$237)
	.dwattr $C$DW$401, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$402	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$402, DW_AT_type(*$C$DW$T$237)

	.dwendtag $C$DW$401

	.dwattr $C$DW$T$242, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$242, DW_AT_decl_line(0x32c)
	.dwattr $C$DW$T$242, DW_AT_decl_column(0x10)
	.dwendtag $C$DW$T$242

	.dwendtag $C$DW$TU$242


$C$DW$TU$237	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$237
$C$DW$T$237	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$237, DW_AT_type(*$C$DW$T$242)
	.dwattr $C$DW$T$237, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$237


$C$DW$TU$240	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$240

$C$DW$T$240	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$240, DW_AT_type(*$C$DW$T$237)
$C$DW$403	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$403, DW_AT_type(*$C$DW$T$239)

	.dwendtag $C$DW$T$240

	.dwendtag $C$DW$TU$240


$C$DW$TU$241	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$241

$C$DW$T$241	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$241, DW_AT_type(*$C$DW$T$237)
$C$DW$404	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$404, DW_AT_type(*$C$DW$T$237)

	.dwendtag $C$DW$T$241

	.dwendtag $C$DW$TU$241


$C$DW$TU$149	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$149
$C$DW$T$149	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$149, DW_AT_name("sTIDL_SliceLayerParams_t")
	.dwattr $C$DW$T$149, DW_AT_type(*$C$DW$T$242)
	.dwattr $C$DW$T$149, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$149, DW_AT_decl_line(0x333)
	.dwattr $C$DW$T$149, DW_AT_decl_column(0x02)

	.dwendtag $C$DW$TU$149


$C$DW$TU$238	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$238
$C$DW$T$238	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$238, DW_AT_type(*$C$DW$T$242)

	.dwendtag $C$DW$TU$238


$C$DW$TU$239	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$239
$C$DW$T$239	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$239, DW_AT_type(*$C$DW$T$238)
	.dwattr $C$DW$T$239, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$239


$C$DW$TU$250	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$250

$C$DW$T$250	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$250, DW_AT_name("sTIDL_SoftMaxParams_t")
	.dwattr $C$DW$T$250, DW_AT_byte_size(0x0c)
$C$DW$405	.dwtag  DW_TAG_member
	.dwattr $C$DW$405, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$405, DW_AT_name("numChannels")
	.dwattr $C$DW$405, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$405, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$405, DW_AT_decl_line(0x527)
	.dwattr $C$DW$405, DW_AT_decl_column(0x0d)

$C$DW$406	.dwtag  DW_TAG_member
	.dwattr $C$DW$406, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$406, DW_AT_name("inDataQ")
	.dwattr $C$DW$406, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$406, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$406, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$406, DW_AT_decl_line(0x529)
	.dwattr $C$DW$406, DW_AT_decl_column(0x0d)

$C$DW$407	.dwtag  DW_TAG_member
	.dwattr $C$DW$407, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$407, DW_AT_name("outDataQ")
	.dwattr $C$DW$407, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$407, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$407, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$407, DW_AT_decl_line(0x52b)
	.dwattr $C$DW$407, DW_AT_decl_column(0x0d)


$C$DW$408	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$408, DW_AT_name("operator =")
	.dwattr $C$DW$408, DW_AT_declaration
	.dwattr $C$DW$408, DW_AT_linkage_name("_ZN21sTIDL_SoftMaxParams_taSERKS_")
	.dwattr $C$DW$408, DW_AT_type(*$C$DW$T$245)
	.dwattr $C$DW$408, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$409	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$409, DW_AT_type(*$C$DW$T$247)

	.dwendtag $C$DW$408


$C$DW$410	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$410, DW_AT_name("operator =")
	.dwattr $C$DW$410, DW_AT_declaration
	.dwattr $C$DW$410, DW_AT_linkage_name("_ZN21sTIDL_SoftMaxParams_taSEOS_")
	.dwattr $C$DW$410, DW_AT_type(*$C$DW$T$245)
	.dwattr $C$DW$410, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$411	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$411, DW_AT_type(*$C$DW$T$245)

	.dwendtag $C$DW$410

	.dwattr $C$DW$T$250, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$250, DW_AT_decl_line(0x525)
	.dwattr $C$DW$T$250, DW_AT_decl_column(0x10)
	.dwendtag $C$DW$T$250

	.dwendtag $C$DW$TU$250


$C$DW$TU$245	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$245
$C$DW$T$245	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$245, DW_AT_type(*$C$DW$T$250)
	.dwattr $C$DW$T$245, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$245


$C$DW$TU$248	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$248

$C$DW$T$248	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$248, DW_AT_type(*$C$DW$T$245)
$C$DW$412	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$412, DW_AT_type(*$C$DW$T$247)

	.dwendtag $C$DW$T$248

	.dwendtag $C$DW$TU$248


$C$DW$TU$249	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$249

$C$DW$T$249	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$249, DW_AT_type(*$C$DW$T$245)
$C$DW$413	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$413, DW_AT_type(*$C$DW$T$245)

	.dwendtag $C$DW$T$249

	.dwendtag $C$DW$TU$249


$C$DW$TU$142	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$142
$C$DW$T$142	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$142, DW_AT_name("sTIDL_SoftMaxParams_t")
	.dwattr $C$DW$T$142, DW_AT_type(*$C$DW$T$250)
	.dwattr $C$DW$T$142, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$142, DW_AT_decl_line(0x52c)
	.dwattr $C$DW$T$142, DW_AT_decl_column(0x02)

	.dwendtag $C$DW$TU$142


$C$DW$TU$246	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$246
$C$DW$T$246	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$246, DW_AT_type(*$C$DW$T$250)

	.dwendtag $C$DW$TU$246


$C$DW$TU$247	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$247
$C$DW$T$247	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$247, DW_AT_type(*$C$DW$T$246)
	.dwattr $C$DW$T$247, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$247


$C$DW$TU$257	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$257

$C$DW$T$257	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$257, DW_AT_name("sTIDL_dataConvertParams_t")
	.dwattr $C$DW$T$257, DW_AT_byte_size(0x0c)
$C$DW$414	.dwtag  DW_TAG_member
	.dwattr $C$DW$414, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$414, DW_AT_name("type")
	.dwattr $C$DW$414, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$414, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$414, DW_AT_decl_line(0x548)
	.dwattr $C$DW$414, DW_AT_decl_column(0x0b)

$C$DW$415	.dwtag  DW_TAG_member
	.dwattr $C$DW$415, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$415, DW_AT_name("layout")
	.dwattr $C$DW$415, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$415, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$415, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$415, DW_AT_decl_line(0x549)
	.dwattr $C$DW$415, DW_AT_decl_column(0x0b)

$C$DW$416	.dwtag  DW_TAG_member
	.dwattr $C$DW$416, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$416, DW_AT_name("zeroPoint")
	.dwattr $C$DW$416, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$416, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$416, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$416, DW_AT_decl_line(0x54a)
	.dwattr $C$DW$416, DW_AT_decl_column(0x0b)


$C$DW$417	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$417, DW_AT_name("operator =")
	.dwattr $C$DW$417, DW_AT_declaration
	.dwattr $C$DW$417, DW_AT_linkage_name("_ZN25sTIDL_dataConvertParams_taSERKS_")
	.dwattr $C$DW$417, DW_AT_type(*$C$DW$T$252)
	.dwattr $C$DW$417, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$418	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$418, DW_AT_type(*$C$DW$T$254)

	.dwendtag $C$DW$417


$C$DW$419	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$419, DW_AT_name("operator =")
	.dwattr $C$DW$419, DW_AT_declaration
	.dwattr $C$DW$419, DW_AT_linkage_name("_ZN25sTIDL_dataConvertParams_taSEOS_")
	.dwattr $C$DW$419, DW_AT_type(*$C$DW$T$252)
	.dwattr $C$DW$419, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$420	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$420, DW_AT_type(*$C$DW$T$252)

	.dwendtag $C$DW$419

	.dwattr $C$DW$T$257, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$257, DW_AT_decl_line(0x546)
	.dwattr $C$DW$T$257, DW_AT_decl_column(0x10)
	.dwendtag $C$DW$T$257

	.dwendtag $C$DW$TU$257


$C$DW$TU$252	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$252
$C$DW$T$252	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$252, DW_AT_type(*$C$DW$T$257)
	.dwattr $C$DW$T$252, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$252


$C$DW$TU$255	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$255

$C$DW$T$255	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$255, DW_AT_type(*$C$DW$T$252)
$C$DW$421	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$421, DW_AT_type(*$C$DW$T$254)

	.dwendtag $C$DW$T$255

	.dwendtag $C$DW$TU$255


$C$DW$TU$256	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$256

$C$DW$T$256	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$256, DW_AT_type(*$C$DW$T$252)
$C$DW$422	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$422, DW_AT_type(*$C$DW$T$252)

	.dwendtag $C$DW$T$256

	.dwendtag $C$DW$TU$256


$C$DW$TU$155	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$155
$C$DW$T$155	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$155, DW_AT_name("sTIDL_dataConvertParams_t")
	.dwattr $C$DW$T$155, DW_AT_type(*$C$DW$T$257)
	.dwattr $C$DW$T$155, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$155, DW_AT_decl_line(0x54b)
	.dwattr $C$DW$T$155, DW_AT_decl_column(0x02)

	.dwendtag $C$DW$TU$155


$C$DW$TU$253	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$253
$C$DW$T$253	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$253, DW_AT_type(*$C$DW$T$257)

	.dwendtag $C$DW$TU$253


$C$DW$TU$254	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$254
$C$DW$T$254	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$254, DW_AT_type(*$C$DW$T$253)
	.dwattr $C$DW$T$254, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$254


$C$DW$TU$264	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$264

$C$DW$T$264	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$264, DW_AT_name("sTIDL_odOutputReformatLayerParams_t")
	.dwattr $C$DW$T$264, DW_AT_byte_size(0x0c)
$C$DW$423	.dwtag  DW_TAG_member
	.dwattr $C$DW$423, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$423, DW_AT_name("layerType")
	.dwattr $C$DW$423, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$423, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$423, DW_AT_decl_line(0x2d0)
	.dwattr $C$DW$423, DW_AT_decl_column(0x0b)

$C$DW$424	.dwtag  DW_TAG_member
	.dwattr $C$DW$424, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$424, DW_AT_name("inWidthOdNetwork")
	.dwattr $C$DW$424, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$424, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$424, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$424, DW_AT_decl_line(0x2d1)
	.dwattr $C$DW$424, DW_AT_decl_column(0x0b)

$C$DW$425	.dwtag  DW_TAG_member
	.dwattr $C$DW$425, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$425, DW_AT_name("inHeightOdNetwork")
	.dwattr $C$DW$425, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$425, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$425, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$425, DW_AT_decl_line(0x2d2)
	.dwattr $C$DW$425, DW_AT_decl_column(0x0b)


$C$DW$426	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$426, DW_AT_name("operator =")
	.dwattr $C$DW$426, DW_AT_declaration
	.dwattr $C$DW$426, DW_AT_linkage_name("_ZN35sTIDL_odOutputReformatLayerParams_taSERKS_")
	.dwattr $C$DW$426, DW_AT_type(*$C$DW$T$259)
	.dwattr $C$DW$426, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$427	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$427, DW_AT_type(*$C$DW$T$261)

	.dwendtag $C$DW$426


$C$DW$428	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$428, DW_AT_name("operator =")
	.dwattr $C$DW$428, DW_AT_declaration
	.dwattr $C$DW$428, DW_AT_linkage_name("_ZN35sTIDL_odOutputReformatLayerParams_taSEOS_")
	.dwattr $C$DW$428, DW_AT_type(*$C$DW$T$259)
	.dwattr $C$DW$428, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$429	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$429, DW_AT_type(*$C$DW$T$259)

	.dwendtag $C$DW$428

	.dwattr $C$DW$T$264, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$264, DW_AT_decl_line(0x2ce)
	.dwattr $C$DW$T$264, DW_AT_decl_column(0x10)
	.dwendtag $C$DW$T$264

	.dwendtag $C$DW$TU$264


$C$DW$TU$259	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$259
$C$DW$T$259	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$259, DW_AT_type(*$C$DW$T$264)
	.dwattr $C$DW$T$259, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$259


$C$DW$TU$262	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$262

$C$DW$T$262	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$262, DW_AT_type(*$C$DW$T$259)
$C$DW$430	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$430, DW_AT_type(*$C$DW$T$261)

	.dwendtag $C$DW$T$262

	.dwendtag $C$DW$TU$262


$C$DW$TU$263	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$263

$C$DW$T$263	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$263, DW_AT_type(*$C$DW$T$259)
$C$DW$431	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$431, DW_AT_type(*$C$DW$T$259)

	.dwendtag $C$DW$T$263

	.dwendtag $C$DW$TU$263


$C$DW$TU$154	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$154
$C$DW$T$154	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$154, DW_AT_name("sTIDL_odOutputReformatLayerParams_t")
	.dwattr $C$DW$T$154, DW_AT_type(*$C$DW$T$264)
	.dwattr $C$DW$T$154, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$154, DW_AT_decl_line(0x2d3)
	.dwattr $C$DW$T$154, DW_AT_decl_column(0x02)

	.dwendtag $C$DW$TU$154


$C$DW$TU$260	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$260
$C$DW$T$260	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$260, DW_AT_type(*$C$DW$T$264)

	.dwendtag $C$DW$TU$260


$C$DW$TU$261	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$261
$C$DW$T$261	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$261, DW_AT_type(*$C$DW$T$260)
	.dwattr $C$DW$T$261, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$261


$C$DW$TU$271	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$271

$C$DW$T$271	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$271, DW_AT_name("sTIDL_sysMemHandle_t")
	.dwattr $C$DW$T$271, DW_AT_byte_size(0x10)
$C$DW$432	.dwtag  DW_TAG_member
	.dwattr $C$DW$432, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$432, DW_AT_name("base")
	.dwattr $C$DW$432, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$432, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$432, DW_AT_decl_line(0x25c)
	.dwattr $C$DW$432, DW_AT_decl_column(0x0c)

$C$DW$433	.dwtag  DW_TAG_member
	.dwattr $C$DW$433, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$433, DW_AT_name("size")
	.dwattr $C$DW$433, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$433, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$433, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$433, DW_AT_decl_line(0x25d)
	.dwattr $C$DW$433, DW_AT_decl_column(0x0c)

$C$DW$434	.dwtag  DW_TAG_member
	.dwattr $C$DW$434, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$434, DW_AT_name("offset")
	.dwattr $C$DW$434, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$434, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$434, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$434, DW_AT_decl_line(0x25e)
	.dwattr $C$DW$434, DW_AT_decl_column(0x0c)


$C$DW$435	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$435, DW_AT_name("operator =")
	.dwattr $C$DW$435, DW_AT_declaration
	.dwattr $C$DW$435, DW_AT_linkage_name("_ZN20sTIDL_sysMemHandle_taSERKS_")
	.dwattr $C$DW$435, DW_AT_type(*$C$DW$T$266)
	.dwattr $C$DW$435, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$436	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$436, DW_AT_type(*$C$DW$T$268)

	.dwendtag $C$DW$435


$C$DW$437	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$437, DW_AT_name("operator =")
	.dwattr $C$DW$437, DW_AT_declaration
	.dwattr $C$DW$437, DW_AT_linkage_name("_ZN20sTIDL_sysMemHandle_taSEOS_")
	.dwattr $C$DW$437, DW_AT_type(*$C$DW$T$266)
	.dwattr $C$DW$437, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$438	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$438, DW_AT_type(*$C$DW$T$266)

	.dwendtag $C$DW$437

	.dwattr $C$DW$T$271, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$271, DW_AT_decl_line(0x25b)
	.dwattr $C$DW$T$271, DW_AT_decl_column(0x01)
	.dwendtag $C$DW$T$271

	.dwendtag $C$DW$TU$271


$C$DW$TU$266	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$266
$C$DW$T$266	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$266, DW_AT_type(*$C$DW$T$271)
	.dwattr $C$DW$T$266, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$266


$C$DW$TU$269	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$269

$C$DW$T$269	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$269, DW_AT_type(*$C$DW$T$266)
$C$DW$439	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$439, DW_AT_type(*$C$DW$T$268)

	.dwendtag $C$DW$T$269

	.dwendtag $C$DW$TU$269


$C$DW$TU$270	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$270

$C$DW$T$270	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$270, DW_AT_type(*$C$DW$T$266)
$C$DW$440	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$440, DW_AT_type(*$C$DW$T$266)

	.dwendtag $C$DW$T$270

	.dwendtag $C$DW$TU$270


$C$DW$TU$267	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$267
$C$DW$T$267	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$267, DW_AT_type(*$C$DW$T$271)

	.dwendtag $C$DW$TU$267


$C$DW$TU$268	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$268
$C$DW$T$268	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$268, DW_AT_type(*$C$DW$T$267)
	.dwattr $C$DW$T$268, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$268


$C$DW$TU$286	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$286
$C$DW$T$286	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$286, DW_AT_name("sTIDL_sysMemHandle_t")
	.dwattr $C$DW$T$286, DW_AT_type(*$C$DW$T$271)
	.dwattr $C$DW$T$286, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$286, DW_AT_decl_line(0x25f)
	.dwattr $C$DW$T$286, DW_AT_decl_column(0x02)

	.dwendtag $C$DW$TU$286


$C$DW$TU$287	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$287
$C$DW$T$287	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$287, DW_AT_type(*$C$DW$T$286)

	.dwendtag $C$DW$TU$287


$C$DW$TU$288	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$288
$C$DW$T$288	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$288, DW_AT_type(*$C$DW$T$287)
	.dwattr $C$DW$T$288, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$288

