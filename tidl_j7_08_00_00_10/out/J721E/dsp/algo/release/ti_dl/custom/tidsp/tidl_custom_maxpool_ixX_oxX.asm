;******************************************************************************
;* C7x G3 C/C++ Codegen                                            Unix v1.4.2.LTS *
;* Date/Time created: Tue Oct  5 15:11:17 2021                                *
;******************************************************************************
	.compiler_opts --abi=eabi --endian=little --hll_source=on --object_format=elf --silicon_errata_i2117 --silicon_version=7100 --symdebug:dwarf --symdebug:dwarf_version=4 

;******************************************************************************
;* GLOBAL FILE PARAMETERS                                                     *
;*                                                                            *
;*   Architecture      : C7100                                                *
;*   Endian            : Little                                               *
;*   Pipelining        : Enabled                                              *
;*   Debug Info        : DWARF Debug                                          *
;*                                                                            *
;******************************************************************************

	.asg	D14, FP
	.asg	D15, SP
	.global	$bss


$C$DW$CU	.dwtag  DW_TAG_compile_unit
	.dwattr $C$DW$CU, DW_AT_name("tidsp/tidl_custom_maxpool_ixX_oxX.c")
	.dwattr $C$DW$CU, DW_AT_producer("TI C7x G3 C/C++ Codegen Unix v1.4.2.LTS Copyright (c) 2014-2019 Texas Instruments Incorporated")
	.dwattr $C$DW$CU, DW_AT_TI_version(0x01)
	.dwattr $C$DW$CU, DW_AT_comp_dir("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/tidl_j7_08_00_00_10/ti_dl/custom")

$C$DW$1	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1, DW_AT_name("TIDL_customMaxPool_ixX_oxX_exec_cn")
	.dwattr $C$DW$1, DW_AT_linkage_name("_Z34TIDL_customMaxPool_ixX_oxX_exec_cnPvPKvS_")
	.dwattr $C$DW$1, DW_AT_type(*$C$DW$T$59)
	.dwattr $C$DW$1, DW_AT_declaration
	.dwattr $C$DW$1, DW_AT_external
	.dwattr $C$DW$1, DW_AT_decl_file("./tidsp/tidl_custom_maxpool_ixX_oxX_priv.h")
	.dwattr $C$DW$1, DW_AT_decl_line(0x69)
	.dwattr $C$DW$1, DW_AT_decl_column(0x09)
$C$DW$2	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$2, DW_AT_type(*$C$DW$T$3)

$C$DW$3	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$3, DW_AT_type(*$C$DW$T$73)

$C$DW$4	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$4, DW_AT_type(*$C$DW$T$3)

	.dwendtag $C$DW$1


$C$DW$19	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$19, DW_AT_name("TIDL_customMaxPool_3x3_2x2_ixu_oxu_exec_c7x")
	.dwattr $C$DW$19, DW_AT_linkage_name("_Z43TIDL_customMaxPool_3x3_2x2_ixu_oxu_exec_c7xPvPKvS_")
	.dwattr $C$DW$19, DW_AT_type(*$C$DW$T$59)
	.dwattr $C$DW$19, DW_AT_declaration
	.dwattr $C$DW$19, DW_AT_external
	.dwattr $C$DW$19, DW_AT_decl_file("./tidsp/tidl_custom_maxpool_ixX_oxX_priv.h")
	.dwattr $C$DW$19, DW_AT_decl_line(0x6d)
	.dwattr $C$DW$19, DW_AT_decl_column(0x09)
$C$DW$20	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$20, DW_AT_type(*$C$DW$T$3)

$C$DW$21	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$21, DW_AT_type(*$C$DW$T$73)

$C$DW$22	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$22, DW_AT_type(*$C$DW$T$3)

	.dwendtag $C$DW$19


$C$DW$23	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$23, DW_AT_name("TIDL_customMaxPool_ixX_oxX_buffParamsInit_c7x")
	.dwattr $C$DW$23, DW_AT_linkage_name("_Z45TIDL_customMaxPool_ixX_oxX_buffParamsInit_c7xP32TIDL_CustomMaxPoolIxXOxXPrivArgsPK18TIDL_bufParams3D_tS3_PK32TIDL_CustomMaxPoolIxXOxXInitArgs")
	.dwattr $C$DW$23, DW_AT_type(*$C$DW$T$59)
	.dwattr $C$DW$23, DW_AT_declaration
	.dwattr $C$DW$23, DW_AT_external
	.dwattr $C$DW$23, DW_AT_decl_file("./tidsp/tidl_custom_maxpool_ixX_oxX_priv.h")
	.dwattr $C$DW$23, DW_AT_decl_line(0x5e)
	.dwattr $C$DW$23, DW_AT_decl_column(0x09)
$C$DW$24	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$24, DW_AT_type(*$C$DW$T$115)

$C$DW$25	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$25, DW_AT_type(*$C$DW$T$123)

$C$DW$26	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$26, DW_AT_type(*$C$DW$T$123)

$C$DW$27	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$27, DW_AT_type(*$C$DW$T$106)

	.dwendtag $C$DW$23


$C$DW$28	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$28, DW_AT_name("TIDL_customMaxPool_ixX_oxX_init_c7x")
	.dwattr $C$DW$28, DW_AT_linkage_name("_Z35TIDL_customMaxPool_ixX_oxX_init_c7xPK32TIDL_CustomMaxPoolIxXOxXPrivArgsPK18TIDL_bufParams3D_tS4_PK32TIDL_CustomMaxPoolIxXOxXInitArgs")
	.dwattr $C$DW$28, DW_AT_type(*$C$DW$T$59)
	.dwattr $C$DW$28, DW_AT_declaration
	.dwattr $C$DW$28, DW_AT_external
	.dwattr $C$DW$28, DW_AT_decl_file("./tidsp/tidl_custom_maxpool_ixX_oxX_priv.h")
	.dwattr $C$DW$28, DW_AT_decl_line(0x63)
	.dwattr $C$DW$28, DW_AT_decl_column(0x09)
$C$DW$29	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$29, DW_AT_type(*$C$DW$T$114)

$C$DW$30	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$30, DW_AT_type(*$C$DW$T$123)

$C$DW$31	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$31, DW_AT_type(*$C$DW$T$123)

$C$DW$32	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$32, DW_AT_type(*$C$DW$T$106)

	.dwendtag $C$DW$28

;	/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/bin/opt7x /tmp/TI4XWvqXJW1 /tmp/TI4XW1o1vvp 
	.sect	".text:_Z40TIDL_customMaxPool_ixX_oxX_getHandleSizePv"
	.clink
	.global	||_Z40TIDL_customMaxPool_ixX_oxX_getHandleSizePv||

$C$DW$33	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$33, DW_AT_name("TIDL_customMaxPool_ixX_oxX_getHandleSize")
	.dwattr $C$DW$33, DW_AT_low_pc(||_Z40TIDL_customMaxPool_ixX_oxX_getHandleSizePv||)
	.dwattr $C$DW$33, DW_AT_high_pc(0x00)
	.dwattr $C$DW$33, DW_AT_linkage_name("_Z40TIDL_customMaxPool_ixX_oxX_getHandleSizePv")
	.dwattr $C$DW$33, DW_AT_external
	.dwattr $C$DW$33, DW_AT_decl_file("tidsp/tidl_custom_maxpool_ixX_oxX.c")
	.dwattr $C$DW$33, DW_AT_decl_line(0x4f)
	.dwattr $C$DW$33, DW_AT_decl_column(0x09)
	.dwattr $C$DW$33, DW_AT_TI_max_frame_size(0x00)
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX.c",line 80,column 1,is_stmt,address ||_Z40TIDL_customMaxPool_ixX_oxX_getHandleSizePv||,isa 0

	.dwfde $C$DW$CIE, ||_Z40TIDL_customMaxPool_ixX_oxX_getHandleSizePv||
$C$DW$34	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$34, DW_AT_name("kernelInitParams")
	.dwattr $C$DW$34, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$34, DW_AT_location[DW_OP_reg4]


;******************************************************************************
;* FUNCTION NAME: TIDL_customMaxPool_ixX_oxX_getHandleSize(void *)            *
;*                                                                            *
;*   Regs Modified     : A4                                                   *
;*   Regs Used         : A4                                                   *
;*   Local Frame Size  : 0 Args + 0 Auto + 0 Save = 0 byte                    *
;******************************************************************************
||_Z40TIDL_customMaxPool_ixX_oxX_getHandleSizePv||:
;** --------------------------------------------------------------------------*
	.dwcfi	cfa_offset, 0
;          EXCLUSIVE CPU CYCLES: 1
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX.c",line 82,column 3,is_stmt,isa 0
$C$DW$35	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$35, DW_AT_low_pc(0x00)
	.dwattr $C$DW$35, DW_AT_TI_return


           RET     .B1     ; [A_B] 
||         MVKU32  .L1     0x140,A4          ; [A_L1] |82| 

           ; RETURN OCCURS {RP}              ; [] 
	.dwattr $C$DW$33, DW_AT_TI_end_file("tidsp/tidl_custom_maxpool_ixX_oxX.c")
	.dwattr $C$DW$33, DW_AT_TI_end_line(0x53)
	.dwattr $C$DW$33, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$33

	.sect	".text:_Z31TIDL_customMaxPool_ixX_oxX_initPvPK18TIDL_bufParams3D_tS2_S_"
	.clink
	.global	||_Z31TIDL_customMaxPool_ixX_oxX_initPvPK18TIDL_bufParams3D_tS2_S_||

$C$DW$36	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$36, DW_AT_name("TIDL_customMaxPool_ixX_oxX_init")
	.dwattr $C$DW$36, DW_AT_low_pc(||_Z31TIDL_customMaxPool_ixX_oxX_initPvPK18TIDL_bufParams3D_tS2_S_||)
	.dwattr $C$DW$36, DW_AT_high_pc(0x00)
	.dwattr $C$DW$36, DW_AT_linkage_name("_Z31TIDL_customMaxPool_ixX_oxX_initPvPK18TIDL_bufParams3D_tS2_S_")
	.dwattr $C$DW$36, DW_AT_external
	.dwattr $C$DW$36, DW_AT_decl_file("tidsp/tidl_custom_maxpool_ixX_oxX.c")
	.dwattr $C$DW$36, DW_AT_decl_line(0x55)
	.dwattr $C$DW$36, DW_AT_decl_column(0x09)
	.dwattr $C$DW$36, DW_AT_TI_max_frame_size(0x38)
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX.c",line 89,column 1,is_stmt,address ||_Z31TIDL_customMaxPool_ixX_oxX_initPvPK18TIDL_bufParams3D_tS2_S_||,isa 0

	.dwfde $C$DW$CIE, ||_Z31TIDL_customMaxPool_ixX_oxX_initPvPK18TIDL_bufParams3D_tS2_S_||
$C$DW$37	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$37, DW_AT_name("kernelHandle")
	.dwattr $C$DW$37, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$37, DW_AT_location[DW_OP_reg4]

$C$DW$38	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$38, DW_AT_name("srcAddr")
	.dwattr $C$DW$38, DW_AT_location[DW_OP_reg5]

$C$DW$39	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$39, DW_AT_name("dstAddr")
	.dwattr $C$DW$39, DW_AT_location[DW_OP_reg6]

$C$DW$40	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$40, DW_AT_name("pKerInitArgs")
	.dwattr $C$DW$40, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$40, DW_AT_location[DW_OP_reg7]


;******************************************************************************
;* FUNCTION NAME: TIDL_customMaxPool_ixX_oxX_init(void *, const TIDL_bufParams3D_t *, const TIDL_bufParams3D_t *, void *)*
;*                                                                            *
;*   Regs Modified     : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,A14,   *
;*                           VB0,VB1,VB2,VB3,VB4,VB5,VB6,VB7,VB8,VB9,VB10,    *
;*                           VB11,VB12,VB13,AL0,AL1,AL2,AL3,AL4,AL5,AL6,AL7,  *
;*                           AM0,AM1,AM2,AM3,AM4,AM5,AM6,AM7,D0,D1,D2,D3,D4,  *
;*                           D5,D6,D7,D8,D9,D10,D11,D12,D13,D14,SP,VBL0,VBL1, *
;*                           VBL2,VBL3,VBL4,VBL5,VBL6,VBL7,VBM0,VBM1,VBM2,    *
;*                           VBM3,VBM4,VBM5,VBM6,VBM7,P0,P1,P2,P3,P4,P5,P6,P7,*
;*                           CUCR0,CUCR1,CUCR2,CUCR3                          *
;*   Regs Used         : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,A14,   *
;*                           VB0,VB1,VB2,VB3,VB4,VB5,VB6,VB7,VB8,VB9,VB10,    *
;*                           VB11,VB12,VB13,AL0,AL1,AL2,AL3,AL4,AL5,AL6,AL7,  *
;*                           AM0,AM1,AM2,AM3,AM4,AM5,AM6,AM7,D0,D1,D2,D3,D4,  *
;*                           D5,D6,D7,D8,D9,D10,D11,D12,D13,D14,SP,VBL0,VBL1, *
;*                           VBL2,VBL3,VBL4,VBL5,VBL6,VBL7,VBM0,VBM1,VBM2,    *
;*                           VBM3,VBM4,VBM5,VBM6,VBM7,P0,P1,P2,P3,P4,P5,P6,P7,*
;*                           CUCR0,CUCR1,CUCR2,CUCR3                          *
;*   Local Frame Size  : 0 Args + 0 Auto + 56 Save = 56 byte                  *
;******************************************************************************
||_Z31TIDL_customMaxPool_ixX_oxX_initPvPK18TIDL_bufParams3D_tS2_S_||:
;** --------------------------------------------------------------------------*
;* A4    assigned to $O$C31
;* AL0   assigned to $O$U23
;* A8    assigned to $O$Lr31$pKerPrivArgs
;* A9    assigned to $O$Lr20$status
;* A9    assigned to pKerInitArgs
$C$DW$41	.dwtag  DW_TAG_variable
	.dwattr $C$DW$41, DW_AT_name("pKerInitArgs")
	.dwattr $C$DW$41, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$41, DW_AT_location[DW_OP_reg9]

;* A11   assigned to dstAddr
$C$DW$42	.dwtag  DW_TAG_variable
	.dwattr $C$DW$42, DW_AT_name("dstAddr")
	.dwattr $C$DW$42, DW_AT_type(*$C$DW$T$123)
	.dwattr $C$DW$42, DW_AT_location[DW_OP_reg11]

;* A10   assigned to srcAddr
$C$DW$43	.dwtag  DW_TAG_variable
	.dwattr $C$DW$43, DW_AT_name("srcAddr")
	.dwattr $C$DW$43, DW_AT_type(*$C$DW$T$123)
	.dwattr $C$DW$43, DW_AT_location[DW_OP_reg10]

;* A8    assigned to kernelHandle
$C$DW$44	.dwtag  DW_TAG_variable
	.dwattr $C$DW$44, DW_AT_name("kernelHandle")
	.dwattr $C$DW$44, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$44, DW_AT_location[DW_OP_reg8]

	.dwcfi	cfa_offset, 0
;          EXCLUSIVE CPU CYCLES: 18

           STD     .D1     A8,*SP(8)         ; [A_D1] 
||         STD     .D2X    A9,*SP++(-56)     ; [A_D2] 
	.dwcfi	save_reg_to_mem, 8, 8
	.dwcfi	cfa_offset, 56
	.dwcfi	save_reg_to_mem, 9, -56

           MVC     .S1     RP,A14            ; [A_S1] 
||         STD     .D1     A14,*SP(16)       ; [A_D1] 

	.dwcfi	save_reg_to_reg, 4101, 14
	.dwcfi	save_reg_to_mem, 14, 16
           STD     .D1     A11,*SP(40)       ; [A_D1] 
	.dwcfi	save_reg_to_mem, 11, 40

           STD     .D1     A13,*SP(24)       ; [A_D1] 
||         STD     .D2X    A10,*SP(48)       ; [A_D2] 
||         MV      .L1     A4,A8             ; [A_L1] |89| 
||         MV      .S1     A7,A9             ; [A_S1] |89| 

	.dwcfi	save_reg_to_mem, 13, 24
	.dwcfi	save_reg_to_mem, 10, 48
$C$DW$45	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$45, DW_AT_low_pc(0x00)
	.dwattr $C$DW$45, DW_AT_name("__c7xabi_strasg")
	.dwattr $C$DW$45, DW_AT_TI_call


           CALL    .B1     ||__c7xabi_strasg|| ; [A_B] |94| 
||         MV      .D2     A9,A5             ; [A_D2] |94| 
||         MVKU32  .L1     0x2c,A6           ; [A_L1] |94| 
||         MV      .S1     A5,A10            ; [A_S1] |89| 
||         MV      .M1     A6,A11            ; [A_M1] |89| 
||         STD     .D1     A12,*SP(32)       ; [A_D1] 

	.dwcfi	save_reg_to_mem, 12, 32
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX.c",line 94,column 3,is_stmt,isa 0
$C$RL0:    ; CALL OCCURS (||__c7xabi_strasg||) arg:{A4,A5,A6} ret:{A4}  ; [] |94| 
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX.c",line 155,column 3,is_stmt,isa 0
           MVK64   .L1     0,AL1             ; [A_L1] |155| 
           MVK64   .L1     0,AL2             ; [A_L1] |155| 
           MVK64   .L2     0,B0              ; [B_L2] |155| 

           MVKU32  .L1     0,A3              ; [A_L1] |155| 
||         MVK64   .S1     0,AL0             ; [A_S1] |155| 

           CMPEQD  .L1     AL1,A9,A1         ; [A_L1] |155| 
||         MVKU32  .S1     0,A4              ; [A_S1] |155| 

           CMPEQD  .L1X    B0,A10,A0         ; [A_L1] |155| 
||         CMPEQD  .S1     AL2,A8,A2         ; [A_S1] |155| 
||         MVKU32  .L2     0,BL1             ; [B_L2] |155| 
||         MVKU32  .S2     0,BL0             ; [B_S2] |155| 

           CMPEQD  .L1     AL0,A11,A0        ; [A_L1] |155| 
|| [!A0]   MVKU32  .S1     0x1,A3            ; [A_S1] |155| 
|| [!A1]   MVKU32  .L2     0x1,BL0           ; [B_L2] |155| 
|| [!A2]   MVKU32  .S2     0x1,BL1           ; [B_S2] |155| 

   [!A0]   MVKU32  .L1     0x1,A4            ; [A_L1] |155| 
||         ANDW    .L2     BL0,BL1,BL0       ; [B_L2] |155| 

           ANDW    .L2X    A3,BL0,BL0        ; [B_L2] |155| 
           ANDW    .L2X    A4,BL0,B0         ; [B_L2] |155| 
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX.c",line 95,column 3,is_stmt,isa 0

           CMPEQW  .L1X    B0,0,A0           ; [A_L1] |155| 
||         MVK64   .L2     0,B1              ; [B_L2] |95| 

   [ A0]   B       .B1     ||$C$L2||         ; [A_B] |155| 
||         STD     .D1X    B1,*A8(48)        ; [A_D1] |95| 

	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX.c",line 155,column 3,is_stmt,isa 0
           ; BRANCHCC OCCURS {||$C$L2||}     ; [] |155| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 8
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX.c",line 100,column 5,is_stmt,isa 0
           LDW     .D1     *A9(0),AL0        ; [A_D1] |100| 

           CMPEQW  .L1     AL0,0,A1          ; [A_L1] |100| 
||         MVKU32  .S1     0x18,A6           ; [A_S1] |102| 
||         ADDD    .D1     A8,0x108,A4       ; [A_D1] |102| 

   [!A1]   B       .B1     ||$C$L1||         ; [A_B] |100| 
||         CMPEQW  .L1     AL0,0x1,A0        ; [A_L1] |109| 
||         ADDD    .D2     A4,0x18,A13       ; [A_D2] |103| 
||         MVKU32  .S1     0,A9              ; [A_S1] |125| 
||         MV      .M1     A10,A5            ; [A_M1] |102| 
||         ADDKPC  .D1     $PCR_OFFSET((||_Z34TIDL_customMaxPool_ixX_oxX_exec_cnPvPKvS_||+0)),A12 ; [A_D1] |105| 

           ; BRANCHCC OCCURS {||$C$L1||}     ; [] |100| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 3
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX.c",line 102,column 7,is_stmt,isa 0
$C$DW$46	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$46, DW_AT_low_pc(0x00)
	.dwattr $C$DW$46, DW_AT_name("memcpy")
	.dwattr $C$DW$46, DW_AT_TI_call

           CALL    .B1     ||memcpy||        ; [A_B] |102| 
$C$RL1:    ; CALL OCCURS (||memcpy||) arg:{A4,A5,A6} ret:{A4}  ; [] |102| 
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX.c",line 103,column 7,is_stmt,isa 0
$C$DW$47	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$47, DW_AT_low_pc(0x00)
	.dwattr $C$DW$47, DW_AT_name("memcpy")
	.dwattr $C$DW$47, DW_AT_TI_call


           CALL    .B1     ||memcpy||        ; [A_B] |103| 
||         MVKU32  .L1     0x18,A6           ; [A_L1] |103| 
||         MV      .D1     A11,A5            ; [A_D1] |103| 
||         MV      .D2     A13,A4            ; [A_D2] |103| 

$C$RL2:    ; CALL OCCURS (||memcpy||) arg:{A4,A5,A6} ret:{A4}  ; [] |103| 
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX.c",line 105,column 7,is_stmt,isa 0

           B       .B1     ||$C$L3||         ; [A_B] |125| 
||         STD     .D1     A12,*A8(48)       ; [A_D1] |105| 

	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX.c",line 125,column 3,is_stmt,isa 0
           ; BRANCH OCCURS {||$C$L3||}       ; [] |125| 
;** --------------------------------------------------------------------------*
||$C$L1||:    
;          EXCLUSIVE CPU CYCLES: 1
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX.c",line 109,column 10,is_stmt,isa 0

   [!A0]   B       .B1     ||$C$L2||         ; [A_B] |109| 
||         MV      .D2     A8,A7             ; [A_D2] |112| 
||         MV      .L1     A11,A6            ; [A_L1] |112| 
||         MV      .S1     A8,A4             ; [A_S1] |112| 
||         ADDKPC  .D1     $PCR_OFFSET((||_Z43TIDL_customMaxPool_3x3_2x2_ixu_oxu_exec_c7xPvPKvS_||+0)),D0 ; [A_D1] |111| 

           ; BRANCHCC OCCURS {||$C$L2||}     ; [] |109| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 3
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX.c",line 111,column 7,is_stmt,isa 0
$C$DW$48	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$48, DW_AT_low_pc(0x00)
	.dwattr $C$DW$48, DW_AT_name("_Z45TIDL_customMaxPool_ixX_oxX_buffParamsInit_c7xP32TIDL_CustomMaxPoolIxXOxXPrivArgsPK18TIDL_bufParams3D_tS3_PK32TIDL_CustomMaxPoolIxXOxXInitArgs")
	.dwattr $C$DW$48, DW_AT_TI_call


           CALL    .B1     ||_Z45TIDL_customMaxPool_ixX_oxX_buffParamsInit_c7xP32TIDL_CustomMaxPoolIxXOxXPrivArgsPK18TIDL_bufParams3D_tS3_PK32TIDL_CustomMaxPoolIxXOxXInitArgs|| ; [A_B] |112| 
||         STD     .D1     D0,*A8(48)        ; [A_D1] |111| 

	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX.c",line 112,column 7,is_stmt,isa 0
$C$RL3:    ; CALL OCCURS (||_Z45TIDL_customMaxPool_ixX_oxX_buffParamsInit_c7xP32TIDL_CustomMaxPoolIxXOxXPrivArgsPK18TIDL_bufParams3D_tS3_PK32TIDL_CustomMaxPoolIxXOxXInitArgs||) arg:{A4,A5,A6,A7} ret:{A4}  ; [] |112| 
$C$DW$49	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$49, DW_AT_low_pc(0x00)
	.dwattr $C$DW$49, DW_AT_name("_Z35TIDL_customMaxPool_ixX_oxX_init_c7xPK32TIDL_CustomMaxPoolIxXOxXPrivArgsPK18TIDL_bufParams3D_tS4_PK32TIDL_CustomMaxPoolIxXOxXInitArgs")
	.dwattr $C$DW$49, DW_AT_TI_call


           CALL    .B1     ||_Z35TIDL_customMaxPool_ixX_oxX_init_c7xPK32TIDL_CustomMaxPoolIxXOxXPrivArgsPK18TIDL_bufParams3D_tS4_PK32TIDL_CustomMaxPoolIxXOxXInitArgs|| ; [A_B] |125| 
||         MV      .D1     A8,A4             ; [A_D1] |125| 
||         MV      .D2     A4,A9             ; [A_D2] |112| 
||         MV      .L1     A8,A7             ; [A_L1] |125| 
||         MV      .S1     A11,A6            ; [A_S1] |125| 
||         MV      .M1     A10,A5            ; [A_M1] |125| 

	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX.c",line 125,column 3,is_stmt,isa 0
$C$RL4:    ; CALL OCCURS (||_Z35TIDL_customMaxPool_ixX_oxX_init_c7xPK32TIDL_CustomMaxPoolIxXOxXPrivArgsPK18TIDL_bufParams3D_tS4_PK32TIDL_CustomMaxPoolIxXOxXInitArgs||) arg:{A4,A5,A6,A7} ret:{A4}  ; [] |125| 

           B       .B1     ||$C$L3||         ; [A_B] |125| 
||         ADDW    .D1     A9,A4,A9          ; [A_D1] |125| 

           ; BRANCH OCCURS {||$C$L3||}       ; [] |125| 
;** --------------------------------------------------------------------------*
||$C$L2||:    
;          EXCLUSIVE CPU CYCLES: 1
           MVK32   .L1     0xffffffff,A9     ; [A_L1] |125| 
;** --------------------------------------------------------------------------*
||$C$L3||:    
;          EXCLUSIVE CPU CYCLES: 10

           MVC     .S1     A14,RP            ; [A_S1] |125| 
||         MV      .D1     A9,A4             ; [A_D1] |125| 

	.dwcfi	restore_reg, 4101
           LDD     .D1     *SP(24),A13       ; [A_D1] |125| 
	.dwcfi	restore_reg, 13

           LDD     .D1     *SP(40),A11       ; [A_D1] |125| 
||         LDD     .D2     *SP(32),A12       ; [A_D2] |125| 
	.dwcfi	restore_reg, 11
	.dwcfi	restore_reg, 12

           LDD     .D1     *SP(64),A8        ; [A_D1] |125| 
||         LDD     .D2     *SP(48),A10       ; [A_D2] |125| 
	.dwcfi	restore_reg, 8
	.dwcfi	restore_reg, 10

           LDD     .D1     *SP(56),A9        ; [A_D1] |125| 
||         LDD     .D2     *SP(16),A14       ; [A_D2] |125| 

	.dwcfi	restore_reg, 9
	.dwcfi	restore_reg, 14
$C$DW$50	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$50, DW_AT_low_pc(0x00)
	.dwattr $C$DW$50, DW_AT_TI_return


           RET     .B1     ; [A_B] |125| 
||         ADDD    .D1     SP,0x38,SP        ; [A_D1] |125| 

	.dwcfi	cfa_offset, 0
           ; RETURN OCCURS {RP}              ; [] |125| 
	.dwattr $C$DW$36, DW_AT_TI_end_file("tidsp/tidl_custom_maxpool_ixX_oxX.c")
	.dwattr $C$DW$36, DW_AT_TI_end_line(0x7e)
	.dwattr $C$DW$36, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$36

	.sect	".text:_Z31TIDL_customMaxPool_ixX_oxX_execPvPK18TIDL_bufParams3D_tS2_PPKvS_"
	.clink
	.global	||_Z31TIDL_customMaxPool_ixX_oxX_execPvPK18TIDL_bufParams3D_tS2_PPKvS_||

$C$DW$51	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$51, DW_AT_name("TIDL_customMaxPool_ixX_oxX_exec")
	.dwattr $C$DW$51, DW_AT_low_pc(||_Z31TIDL_customMaxPool_ixX_oxX_execPvPK18TIDL_bufParams3D_tS2_PPKvS_||)
	.dwattr $C$DW$51, DW_AT_high_pc(0x00)
	.dwattr $C$DW$51, DW_AT_linkage_name("_Z31TIDL_customMaxPool_ixX_oxX_execPvPK18TIDL_bufParams3D_tS2_PPKvS_")
	.dwattr $C$DW$51, DW_AT_external
	.dwattr $C$DW$51, DW_AT_decl_file("tidsp/tidl_custom_maxpool_ixX_oxX.c")
	.dwattr $C$DW$51, DW_AT_decl_line(0x80)
	.dwattr $C$DW$51, DW_AT_decl_column(0x09)
	.dwattr $C$DW$51, DW_AT_TI_max_frame_size(0x08)
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX.c",line 133,column 1,is_stmt,address ||_Z31TIDL_customMaxPool_ixX_oxX_execPvPK18TIDL_bufParams3D_tS2_PPKvS_||,isa 0

	.dwfde $C$DW$CIE, ||_Z31TIDL_customMaxPool_ixX_oxX_execPvPK18TIDL_bufParams3D_tS2_PPKvS_||
$C$DW$52	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$52, DW_AT_name("kernelHandle")
	.dwattr $C$DW$52, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$52, DW_AT_location[DW_OP_reg4]

$C$DW$53	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$53, DW_AT_name("srcAddr")
	.dwattr $C$DW$53, DW_AT_location[DW_OP_reg5]

$C$DW$54	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$54, DW_AT_name("dstAddr")
	.dwattr $C$DW$54, DW_AT_location[DW_OP_reg6]

$C$DW$55	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$55, DW_AT_name("srcPtr")
	.dwattr $C$DW$55, DW_AT_type(*$C$DW$T$132)
	.dwattr $C$DW$55, DW_AT_location[DW_OP_reg7]

$C$DW$56	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$56, DW_AT_name("dstPtr")
	.dwattr $C$DW$56, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$56, DW_AT_location[DW_OP_reg8]


;******************************************************************************
;* FUNCTION NAME: TIDL_customMaxPool_ixX_oxX_exec(void *, const TIDL_bufParams3D_t *, const TIDL_bufParams3D_t *, const void **, void *)*
;*                                                                            *
;*   Regs Modified     : A0,A1,A2,A3,A4,A5,A6,A7,A9,VB0,VB1,VB2,VB3,VB4,VB5,  *
;*                           VB6,VB7,VB8,VB9,VB10,VB11,VB12,VB13,AL0,AL1,AL2, *
;*                           AL3,AL4,AL5,AL6,AL7,AM0,AM1,AM2,AM3,AM4,AM5,AM6, *
;*                           AM7,D0,D1,D2,D3,D4,D5,D6,D7,D8,D9,D10,D11,D12,   *
;*                           D13,D14,SP,VBL0,VBL1,VBL2,VBL3,VBL4,VBL5,VBL6,   *
;*                           VBL7,VBM0,VBM1,VBM2,VBM3,VBM4,VBM5,VBM6,VBM7,P0, *
;*                           P1,P2,P3,P4,P5,P6,P7,CUCR0,CUCR1,CUCR2,CUCR3     *
;*   Regs Used         : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,VB0,VB1,VB2,VB3,VB4,   *
;*                           VB5,VB6,VB7,VB8,VB9,VB10,VB11,VB12,VB13,AL0,AL1, *
;*                           AL2,AL3,AL4,AL5,AL6,AL7,AM0,AM1,AM2,AM3,AM4,AM5, *
;*                           AM6,AM7,D0,D1,D2,D3,D4,D5,D6,D7,D8,D9,D10,D11,   *
;*                           D12,D13,D14,SP,VBL0,VBL1,VBL2,VBL3,VBL4,VBL5,    *
;*                           VBL6,VBL7,VBM0,VBM1,VBM2,VBM3,VBM4,VBM5,VBM6,    *
;*                           VBM7,P0,P1,P2,P3,P4,P5,P6,P7,CUCR0,CUCR1,CUCR2,  *
;*                           CUCR3                                            *
;*   Local Frame Size  : 0 Args + 0 Auto + 8 Save = 8 byte                    *
;******************************************************************************
||_Z31TIDL_customMaxPool_ixX_oxX_execPvPK18TIDL_bufParams3D_tS2_PPKvS_||:
;** --------------------------------------------------------------------------*
;* A6    assigned to dstPtr
$C$DW$57	.dwtag  DW_TAG_variable
	.dwattr $C$DW$57, DW_AT_name("dstPtr")
	.dwattr $C$DW$57, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$57, DW_AT_location[DW_OP_reg6]

;* A7    assigned to srcPtr
$C$DW$58	.dwtag  DW_TAG_variable
	.dwattr $C$DW$58, DW_AT_name("srcPtr")
	.dwattr $C$DW$58, DW_AT_type(*$C$DW$T$132)
	.dwattr $C$DW$58, DW_AT_location[DW_OP_reg7]

;* A4    assigned to kernelHandle
$C$DW$59	.dwtag  DW_TAG_variable
	.dwattr $C$DW$59, DW_AT_name("kernelHandle")
	.dwattr $C$DW$59, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$59, DW_AT_location[DW_OP_reg4]

	.dwcfi	cfa_offset, 0
;          EXCLUSIVE CPU CYCLES: 14
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX.c",line 142,column 3,is_stmt,isa 0
           LDD     .D1     *A4(48),AL0       ; [A_D1] |142| 
           LDD     .D1     *A7(0),A5         ; [A_D1] |142| 

           MVC     .S1     RP,A9             ; [A_S1] 
||         STD     .D1     A9,*SP(8)         ; [A_D1] 

	.dwcfi	save_reg_to_reg, 4101, 9
	.dwcfi	save_reg_to_mem, 9, 8
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX.c",line 133,column 1,is_stmt,isa 0
$C$DW$60	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$60, DW_AT_low_pc(0x00)
	.dwattr $C$DW$60, DW_AT_TI_call
	.dwattr $C$DW$60, DW_AT_TI_indirect


           CALLA   .S1     AL0               ; [A_S1] |142| 
||         ADDD    .D1     SP,0xfffffff8,SP  ; [A_D1] 
||         MV      .D2     A8,A6             ; [A_D2] |133| 

	.dwcfi	cfa_offset, 8
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX.c",line 142,column 3,is_stmt,isa 0
$C$RL5:    ; CALL OCCURS arg:{A4,A5,A6} ret:{A4}  ; [] |142| 
           MVC     .S1     A9,RP             ; [A_S1] 
	.dwcfi	restore_reg, 4101
           LDD     .D1     *SP(16),A9        ; [A_D1] 
	.dwcfi	restore_reg, 9
$C$DW$61	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$61, DW_AT_low_pc(0x00)
	.dwattr $C$DW$61, DW_AT_TI_return


           RET     .B1     ; [A_B] 
||         ADDD    .D1     SP,0x8,SP         ; [A_D1] 

	.dwcfi	cfa_offset, 0
           ; RETURN OCCURS {RP}              ; [] 
	.dwattr $C$DW$51, DW_AT_TI_end_file("tidsp/tidl_custom_maxpool_ixX_oxX.c")
	.dwattr $C$DW$51, DW_AT_TI_end_line(0x8f)
	.dwattr $C$DW$51, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$51

;*****************************************************************************
;* UNDEFINED EXTERNAL REFERENCES                                             *
;*****************************************************************************
	.global	||_Z34TIDL_customMaxPool_ixX_oxX_exec_cnPvPKvS_||
	.global	||_Z43TIDL_customMaxPool_3x3_2x2_ixu_oxu_exec_c7xPvPKvS_||
	.global	||_Z45TIDL_customMaxPool_ixX_oxX_buffParamsInit_c7xP32TIDL_CustomMaxPoolIxXOxXPrivArgsPK18TIDL_bufParams3D_tS3_PK32TIDL_CustomMaxPoolIxXOxXInitArgs||
	.global	||_Z35TIDL_customMaxPool_ixX_oxX_init_c7xPK32TIDL_CustomMaxPoolIxXOxXPrivArgsPK18TIDL_bufParams3D_tS4_PK32TIDL_CustomMaxPoolIxXOxXInitArgs||
	.global	||__c7xabi_strasg||
	.global	||memcpy||
	.dwattr $C$DW$CU, DW_AT_language(DW_LANG_C_plus_plus)

;***************************************************************
;* DWARF CIE ENTRIES                                           *
;***************************************************************

$C$DW$CIE	.dwcie 4101
	.dwcfi	cfa_register, 111
	.dwcfi	cfa_offset, 0
	.dwcfi	same_value, 8
	.dwcfi	same_value, 9
	.dwcfi	same_value, 10
	.dwcfi	same_value, 11
	.dwcfi	same_value, 12
	.dwcfi	same_value, 13
	.dwcfi	same_value, 14
	.dwcfi	same_value, 15
	.dwcfi	same_value, 62
	.dwcfi	same_value, 63
	.dwendentry
	.dwendtag $C$DW$CU


;******************************************************************************
;* TYPE INFORMATION                                                           *
;******************************************************************************

$C$DW$TU$33	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$33

$C$DW$T$33	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$33, DW_AT_name("TIDL_CustomMaxPoolBuffParamsC7x")
	.dwattr $C$DW$T$33, DW_AT_byte_size(0xd0)
$C$DW$62	.dwtag  DW_TAG_member
	.dwattr $C$DW$62, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$62, DW_AT_name("seTemplate1")
	.dwattr $C$DW$62, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$62, DW_AT_decl_file("./tidsp/tidl_custom_maxpool_ixX_oxX_priv.h")
	.dwattr $C$DW$62, DW_AT_decl_line(0x3b)
	.dwattr $C$DW$62, DW_AT_decl_column(0x0c)

$C$DW$63	.dwtag  DW_TAG_member
	.dwattr $C$DW$63, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$63, DW_AT_name("seTemplate2")
	.dwattr $C$DW$63, DW_AT_data_member_location[DW_OP_plus_uconst 0x40]
	.dwattr $C$DW$63, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$63, DW_AT_decl_file("./tidsp/tidl_custom_maxpool_ixX_oxX_priv.h")
	.dwattr $C$DW$63, DW_AT_decl_line(0x3c)
	.dwattr $C$DW$63, DW_AT_decl_column(0x0c)

$C$DW$64	.dwtag  DW_TAG_member
	.dwattr $C$DW$64, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$64, DW_AT_name("saTemplate")
	.dwattr $C$DW$64, DW_AT_data_member_location[DW_OP_plus_uconst 0x80]
	.dwattr $C$DW$64, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$64, DW_AT_decl_file("./tidsp/tidl_custom_maxpool_ixX_oxX_priv.h")
	.dwattr $C$DW$64, DW_AT_decl_line(0x3d)
	.dwattr $C$DW$64, DW_AT_decl_column(0x0c)

$C$DW$65	.dwtag  DW_TAG_member
	.dwattr $C$DW$65, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$65, DW_AT_name("numCh")
	.dwattr $C$DW$65, DW_AT_data_member_location[DW_OP_plus_uconst 0xc0]
	.dwattr $C$DW$65, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$65, DW_AT_decl_file("./tidsp/tidl_custom_maxpool_ixX_oxX_priv.h")
	.dwattr $C$DW$65, DW_AT_decl_line(0x3e)
	.dwattr $C$DW$65, DW_AT_decl_column(0x0c)

$C$DW$66	.dwtag  DW_TAG_member
	.dwattr $C$DW$66, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$66, DW_AT_name("numLines")
	.dwattr $C$DW$66, DW_AT_data_member_location[DW_OP_plus_uconst 0xc4]
	.dwattr $C$DW$66, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$66, DW_AT_decl_file("./tidsp/tidl_custom_maxpool_ixX_oxX_priv.h")
	.dwattr $C$DW$66, DW_AT_decl_line(0x3f)
	.dwattr $C$DW$66, DW_AT_decl_column(0x0c)

$C$DW$67	.dwtag  DW_TAG_member
	.dwattr $C$DW$67, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$67, DW_AT_name("numTiles")
	.dwattr $C$DW$67, DW_AT_data_member_location[DW_OP_plus_uconst 0xc8]
	.dwattr $C$DW$67, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$67, DW_AT_decl_file("./tidsp/tidl_custom_maxpool_ixX_oxX_priv.h")
	.dwattr $C$DW$67, DW_AT_decl_line(0x40)
	.dwattr $C$DW$67, DW_AT_decl_column(0x0c)

$C$DW$68	.dwtag  DW_TAG_member
	.dwattr $C$DW$68, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$68, DW_AT_name("inStride")
	.dwattr $C$DW$68, DW_AT_data_member_location[DW_OP_plus_uconst 0xcc]
	.dwattr $C$DW$68, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$68, DW_AT_decl_file("./tidsp/tidl_custom_maxpool_ixX_oxX_priv.h")
	.dwattr $C$DW$68, DW_AT_decl_line(0x41)
	.dwattr $C$DW$68, DW_AT_decl_column(0x0c)


$C$DW$69	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$69, DW_AT_name("operator =")
	.dwattr $C$DW$69, DW_AT_declaration
	.dwattr $C$DW$69, DW_AT_linkage_name("_ZN31TIDL_CustomMaxPoolBuffParamsC7xaSERKS_")
	.dwattr $C$DW$69, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$69, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$70	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$70, DW_AT_type(*$C$DW$T$30)

	.dwendtag $C$DW$69


$C$DW$71	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$71, DW_AT_name("operator =")
	.dwattr $C$DW$71, DW_AT_declaration
	.dwattr $C$DW$71, DW_AT_linkage_name("_ZN31TIDL_CustomMaxPoolBuffParamsC7xaSEOS_")
	.dwattr $C$DW$71, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$71, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$72	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$72, DW_AT_type(*$C$DW$T$28)

	.dwendtag $C$DW$71

	.dwattr $C$DW$T$33, DW_AT_decl_file("./tidsp/tidl_custom_maxpool_ixX_oxX_priv.h")
	.dwattr $C$DW$T$33, DW_AT_decl_line(0x3a)
	.dwattr $C$DW$T$33, DW_AT_decl_column(0x01)
	.dwendtag $C$DW$T$33

	.dwendtag $C$DW$TU$33


$C$DW$TU$29	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$29
$C$DW$T$29	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$29, DW_AT_type(*$C$DW$T$33)

	.dwendtag $C$DW$TU$29


$C$DW$TU$30	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$30
$C$DW$T$30	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$30, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$T$30, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$30


$C$DW$TU$47	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$47
$C$DW$T$47	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$47, DW_AT_name("TIDL_CustomMaxPoolBuffParamsC7x")
	.dwattr $C$DW$T$47, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$T$47, DW_AT_decl_file("./tidsp/tidl_custom_maxpool_ixX_oxX_priv.h")
	.dwattr $C$DW$T$47, DW_AT_decl_line(0x42)
	.dwattr $C$DW$T$47, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$47


$C$DW$TU$28	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$28
$C$DW$T$28	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$28, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$T$28, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$28


$C$DW$TU$31	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$31

$C$DW$T$31	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$31, DW_AT_type(*$C$DW$T$28)
$C$DW$73	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$73, DW_AT_type(*$C$DW$T$30)

	.dwendtag $C$DW$T$31

	.dwendtag $C$DW$TU$31


$C$DW$TU$32	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$32

$C$DW$T$32	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$32, DW_AT_type(*$C$DW$T$28)
$C$DW$74	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$74, DW_AT_type(*$C$DW$T$28)

	.dwendtag $C$DW$T$32

	.dwendtag $C$DW$TU$32


$C$DW$TU$44	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$44

$C$DW$T$44	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$44, DW_AT_name("TIDL_CustomMaxPoolBuffParamsNatC")
	.dwattr $C$DW$T$44, DW_AT_byte_size(0x30)
$C$DW$75	.dwtag  DW_TAG_member
	.dwattr $C$DW$75, DW_AT_type(*$C$DW$T$38)
	.dwattr $C$DW$75, DW_AT_name("srcBuf3D")
	.dwattr $C$DW$75, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$75, DW_AT_decl_file("./tidsp/tidl_custom_maxpool_ixX_oxX_priv.h")
	.dwattr $C$DW$75, DW_AT_decl_line(0x46)
	.dwattr $C$DW$75, DW_AT_decl_column(0x16)

$C$DW$76	.dwtag  DW_TAG_member
	.dwattr $C$DW$76, DW_AT_type(*$C$DW$T$38)
	.dwattr $C$DW$76, DW_AT_name("dstBuf3D")
	.dwattr $C$DW$76, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$76, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$76, DW_AT_decl_file("./tidsp/tidl_custom_maxpool_ixX_oxX_priv.h")
	.dwattr $C$DW$76, DW_AT_decl_line(0x47)
	.dwattr $C$DW$76, DW_AT_decl_column(0x16)


$C$DW$77	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$77, DW_AT_name("operator =")
	.dwattr $C$DW$77, DW_AT_declaration
	.dwattr $C$DW$77, DW_AT_linkage_name("_ZN32TIDL_CustomMaxPoolBuffParamsNatCaSERKS_")
	.dwattr $C$DW$77, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$77, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$78	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$78, DW_AT_type(*$C$DW$T$41)

	.dwendtag $C$DW$77


$C$DW$79	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$79, DW_AT_name("operator =")
	.dwattr $C$DW$79, DW_AT_declaration
	.dwattr $C$DW$79, DW_AT_linkage_name("_ZN32TIDL_CustomMaxPoolBuffParamsNatCaSEOS_")
	.dwattr $C$DW$79, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$79, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$80	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$80, DW_AT_type(*$C$DW$T$39)

	.dwendtag $C$DW$79

	.dwattr $C$DW$T$44, DW_AT_decl_file("./tidsp/tidl_custom_maxpool_ixX_oxX_priv.h")
	.dwattr $C$DW$T$44, DW_AT_decl_line(0x45)
	.dwattr $C$DW$T$44, DW_AT_decl_column(0x01)
	.dwendtag $C$DW$T$44

	.dwendtag $C$DW$TU$44


$C$DW$TU$40	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$40
$C$DW$T$40	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$40, DW_AT_type(*$C$DW$T$44)

	.dwendtag $C$DW$TU$40


$C$DW$TU$41	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$41
$C$DW$T$41	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$41, DW_AT_type(*$C$DW$T$40)
	.dwattr $C$DW$T$41, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$41


$C$DW$TU$48	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$48
$C$DW$T$48	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$48, DW_AT_name("TIDL_CustomMaxPoolBuffParamsNatC")
	.dwattr $C$DW$T$48, DW_AT_type(*$C$DW$T$44)
	.dwattr $C$DW$T$48, DW_AT_decl_file("./tidsp/tidl_custom_maxpool_ixX_oxX_priv.h")
	.dwattr $C$DW$T$48, DW_AT_decl_line(0x48)
	.dwattr $C$DW$T$48, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$48


$C$DW$TU$39	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$39
$C$DW$T$39	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$39, DW_AT_type(*$C$DW$T$44)
	.dwattr $C$DW$T$39, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$39


$C$DW$TU$42	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$42

$C$DW$T$42	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$42, DW_AT_type(*$C$DW$T$39)
$C$DW$81	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$81, DW_AT_type(*$C$DW$T$41)

	.dwendtag $C$DW$T$42

	.dwendtag $C$DW$TU$42


$C$DW$TU$43	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$43

$C$DW$T$43	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$43, DW_AT_type(*$C$DW$T$39)
$C$DW$82	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$82, DW_AT_type(*$C$DW$T$39)

	.dwendtag $C$DW$T$43

	.dwendtag $C$DW$TU$43


$C$DW$TU$54	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$54

$C$DW$T$54	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$54, DW_AT_name("TIDL_CustomMaxPoolIxXOxXBuffParams")
	.dwattr $C$DW$T$54, DW_AT_byte_size(0x100)
$C$DW$83	.dwtag  DW_TAG_member
	.dwattr $C$DW$83, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$83, DW_AT_name("c7x")
	.dwattr $C$DW$83, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$83, DW_AT_decl_file("./tidsp/tidl_custom_maxpool_ixX_oxX_priv.h")
	.dwattr $C$DW$83, DW_AT_decl_line(0x4c)
	.dwattr $C$DW$83, DW_AT_decl_column(0x24)

$C$DW$84	.dwtag  DW_TAG_member
	.dwattr $C$DW$84, DW_AT_type(*$C$DW$T$48)
	.dwattr $C$DW$84, DW_AT_name("natc")
	.dwattr $C$DW$84, DW_AT_data_member_location[DW_OP_plus_uconst 0xd0]
	.dwattr $C$DW$84, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$84, DW_AT_decl_file("./tidsp/tidl_custom_maxpool_ixX_oxX_priv.h")
	.dwattr $C$DW$84, DW_AT_decl_line(0x4d)
	.dwattr $C$DW$84, DW_AT_decl_column(0x24)


$C$DW$85	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$85, DW_AT_name("operator =")
	.dwattr $C$DW$85, DW_AT_declaration
	.dwattr $C$DW$85, DW_AT_linkage_name("_ZN34TIDL_CustomMaxPoolIxXOxXBuffParamsaSERKS_")
	.dwattr $C$DW$85, DW_AT_type(*$C$DW$T$49)
	.dwattr $C$DW$85, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$86	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$86, DW_AT_type(*$C$DW$T$51)

	.dwendtag $C$DW$85


$C$DW$87	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$87, DW_AT_name("operator =")
	.dwattr $C$DW$87, DW_AT_declaration
	.dwattr $C$DW$87, DW_AT_linkage_name("_ZN34TIDL_CustomMaxPoolIxXOxXBuffParamsaSEOS_")
	.dwattr $C$DW$87, DW_AT_type(*$C$DW$T$49)
	.dwattr $C$DW$87, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$88	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$88, DW_AT_type(*$C$DW$T$49)

	.dwendtag $C$DW$87

	.dwattr $C$DW$T$54, DW_AT_decl_file("./tidsp/tidl_custom_maxpool_ixX_oxX_priv.h")
	.dwattr $C$DW$T$54, DW_AT_decl_line(0x4b)
	.dwattr $C$DW$T$54, DW_AT_decl_column(0x01)
	.dwendtag $C$DW$T$54

	.dwendtag $C$DW$TU$54


$C$DW$TU$50	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$50
$C$DW$T$50	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$50, DW_AT_type(*$C$DW$T$54)

	.dwendtag $C$DW$TU$50


$C$DW$TU$51	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$51
$C$DW$T$51	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$51, DW_AT_type(*$C$DW$T$50)
	.dwattr $C$DW$T$51, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$51


$C$DW$TU$77	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$77
$C$DW$T$77	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$77, DW_AT_name("TIDL_CustomMaxPoolIxXOxXBuffParams")
	.dwattr $C$DW$T$77, DW_AT_type(*$C$DW$T$54)
	.dwattr $C$DW$T$77, DW_AT_decl_file("./tidsp/tidl_custom_maxpool_ixX_oxX_priv.h")
	.dwattr $C$DW$T$77, DW_AT_decl_line(0x4e)
	.dwattr $C$DW$T$77, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$77


$C$DW$TU$49	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$49
$C$DW$T$49	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$49, DW_AT_type(*$C$DW$T$54)
	.dwattr $C$DW$T$49, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$49


$C$DW$TU$52	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$52

$C$DW$T$52	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$52, DW_AT_type(*$C$DW$T$49)
$C$DW$89	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$89, DW_AT_type(*$C$DW$T$51)

	.dwendtag $C$DW$T$52

	.dwendtag $C$DW$TU$52


$C$DW$TU$53	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$53

$C$DW$T$53	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$53, DW_AT_type(*$C$DW$T$49)
$C$DW$90	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$90, DW_AT_type(*$C$DW$T$49)

	.dwendtag $C$DW$T$53

	.dwendtag $C$DW$TU$53


$C$DW$TU$66	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$66

$C$DW$T$66	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$66, DW_AT_name("TIDL_CustomMaxPoolIxXOxXInitArgs")
	.dwattr $C$DW$T$66, DW_AT_byte_size(0x2c)
$C$DW$91	.dwtag  DW_TAG_member
	.dwattr $C$DW$91, DW_AT_type(*$C$DW$T$59)
	.dwattr $C$DW$91, DW_AT_name("funcStyle")
	.dwattr $C$DW$91, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$91, DW_AT_decl_file("./tidsp/tidl_custom_maxpool_ixX_oxX.h")
	.dwattr $C$DW$91, DW_AT_decl_line(0x33)
	.dwattr $C$DW$91, DW_AT_decl_column(0x0c)

$C$DW$92	.dwtag  DW_TAG_member
	.dwattr $C$DW$92, DW_AT_type(*$C$DW$T$60)
	.dwattr $C$DW$92, DW_AT_name("customMaxPoolParam")
	.dwattr $C$DW$92, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$92, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$92, DW_AT_decl_file("./tidsp/tidl_custom_maxpool_ixX_oxX.h")
	.dwattr $C$DW$92, DW_AT_decl_line(0x34)
	.dwattr $C$DW$92, DW_AT_decl_column(0x18)


$C$DW$93	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$93, DW_AT_name("operator =")
	.dwattr $C$DW$93, DW_AT_declaration
	.dwattr $C$DW$93, DW_AT_linkage_name("_ZN32TIDL_CustomMaxPoolIxXOxXInitArgsaSERKS_")
	.dwattr $C$DW$93, DW_AT_type(*$C$DW$T$61)
	.dwattr $C$DW$93, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$94	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$94, DW_AT_type(*$C$DW$T$63)

	.dwendtag $C$DW$93


$C$DW$95	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$95, DW_AT_name("operator =")
	.dwattr $C$DW$95, DW_AT_declaration
	.dwattr $C$DW$95, DW_AT_linkage_name("_ZN32TIDL_CustomMaxPoolIxXOxXInitArgsaSEOS_")
	.dwattr $C$DW$95, DW_AT_type(*$C$DW$T$61)
	.dwattr $C$DW$95, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$96	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$96, DW_AT_type(*$C$DW$T$61)

	.dwendtag $C$DW$95

	.dwattr $C$DW$T$66, DW_AT_decl_file("./tidsp/tidl_custom_maxpool_ixX_oxX.h")
	.dwattr $C$DW$T$66, DW_AT_decl_line(0x32)
	.dwattr $C$DW$T$66, DW_AT_decl_column(0x01)
	.dwendtag $C$DW$T$66

	.dwendtag $C$DW$TU$66


$C$DW$TU$62	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$62
$C$DW$T$62	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$62, DW_AT_type(*$C$DW$T$66)

	.dwendtag $C$DW$TU$62


$C$DW$TU$63	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$63
$C$DW$T$63	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$63, DW_AT_type(*$C$DW$T$62)
	.dwattr $C$DW$T$63, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$63


$C$DW$TU$71	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$71
$C$DW$T$71	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$71, DW_AT_name("TIDL_CustomMaxPoolIxXOxXInitArgs")
	.dwattr $C$DW$T$71, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$T$71, DW_AT_decl_file("./tidsp/tidl_custom_maxpool_ixX_oxX.h")
	.dwattr $C$DW$T$71, DW_AT_decl_line(0x35)
	.dwattr $C$DW$T$71, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$71


$C$DW$TU$105	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$105
$C$DW$T$105	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$105, DW_AT_type(*$C$DW$T$71)

	.dwendtag $C$DW$TU$105


$C$DW$TU$106	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$106
$C$DW$T$106	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$106, DW_AT_type(*$C$DW$T$105)
	.dwattr $C$DW$T$106, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$106


$C$DW$TU$61	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$61
$C$DW$T$61	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$61, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$T$61, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$61


$C$DW$TU$64	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$64

$C$DW$T$64	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$64, DW_AT_type(*$C$DW$T$61)
$C$DW$97	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$97, DW_AT_type(*$C$DW$T$63)

	.dwendtag $C$DW$T$64

	.dwendtag $C$DW$TU$64


$C$DW$TU$65	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$65

$C$DW$T$65	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$65, DW_AT_type(*$C$DW$T$61)
$C$DW$98	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$98, DW_AT_type(*$C$DW$T$61)

	.dwendtag $C$DW$T$65

	.dwendtag $C$DW$TU$65


$C$DW$TU$83	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$83

$C$DW$T$83	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$83, DW_AT_name("TIDL_CustomMaxPoolIxXOxXPrivArgs")
	.dwattr $C$DW$T$83, DW_AT_byte_size(0x140)
$C$DW$99	.dwtag  DW_TAG_member
	.dwattr $C$DW$99, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$99, DW_AT_name("initArgs")
	.dwattr $C$DW$99, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$99, DW_AT_decl_file("./tidsp/tidl_custom_maxpool_ixX_oxX_priv.h")
	.dwattr $C$DW$99, DW_AT_decl_line(0x52)
	.dwattr $C$DW$99, DW_AT_decl_column(0x24)

$C$DW$100	.dwtag  DW_TAG_member
	.dwattr $C$DW$100, DW_AT_type(*$C$DW$T$76)
	.dwattr $C$DW$100, DW_AT_name("execute")
	.dwattr $C$DW$100, DW_AT_data_member_location[DW_OP_plus_uconst 0x30]
	.dwattr $C$DW$100, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$100, DW_AT_decl_file("./tidsp/tidl_custom_maxpool_ixX_oxX_priv.h")
	.dwattr $C$DW$100, DW_AT_decl_line(0x53)
	.dwattr $C$DW$100, DW_AT_decl_column(0x1e)

$C$DW$101	.dwtag  DW_TAG_member
	.dwattr $C$DW$101, DW_AT_type(*$C$DW$T$77)
	.dwattr $C$DW$101, DW_AT_name("bufParams")
	.dwattr $C$DW$101, DW_AT_data_member_location[DW_OP_plus_uconst 0x38]
	.dwattr $C$DW$101, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$101, DW_AT_decl_file("./tidsp/tidl_custom_maxpool_ixX_oxX_priv.h")
	.dwattr $C$DW$101, DW_AT_decl_line(0x54)
	.dwattr $C$DW$101, DW_AT_decl_column(0x26)

$C$DW$102	.dwtag  DW_TAG_member
	.dwattr $C$DW$102, DW_AT_type(*$C$DW$T$59)
	.dwattr $C$DW$102, DW_AT_name("kernelHandleSize")
	.dwattr $C$DW$102, DW_AT_data_member_location[DW_OP_plus_uconst 0x138]
	.dwattr $C$DW$102, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$102, DW_AT_decl_file("./tidsp/tidl_custom_maxpool_ixX_oxX_priv.h")
	.dwattr $C$DW$102, DW_AT_decl_line(0x55)
	.dwattr $C$DW$102, DW_AT_decl_column(0x0b)


$C$DW$103	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$103, DW_AT_name("operator =")
	.dwattr $C$DW$103, DW_AT_declaration
	.dwattr $C$DW$103, DW_AT_linkage_name("_ZN32TIDL_CustomMaxPoolIxXOxXPrivArgsaSERKS_")
	.dwattr $C$DW$103, DW_AT_type(*$C$DW$T$78)
	.dwattr $C$DW$103, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$104	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$104, DW_AT_type(*$C$DW$T$80)

	.dwendtag $C$DW$103


$C$DW$105	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$105, DW_AT_name("operator =")
	.dwattr $C$DW$105, DW_AT_declaration
	.dwattr $C$DW$105, DW_AT_linkage_name("_ZN32TIDL_CustomMaxPoolIxXOxXPrivArgsaSEOS_")
	.dwattr $C$DW$105, DW_AT_type(*$C$DW$T$78)
	.dwattr $C$DW$105, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$106	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$106, DW_AT_type(*$C$DW$T$78)

	.dwendtag $C$DW$105

	.dwattr $C$DW$T$83, DW_AT_decl_file("./tidsp/tidl_custom_maxpool_ixX_oxX_priv.h")
	.dwattr $C$DW$T$83, DW_AT_decl_line(0x51)
	.dwattr $C$DW$T$83, DW_AT_decl_column(0x01)
	.dwendtag $C$DW$T$83

	.dwendtag $C$DW$TU$83


$C$DW$TU$79	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$79
$C$DW$T$79	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$79, DW_AT_type(*$C$DW$T$83)

	.dwendtag $C$DW$TU$79


$C$DW$TU$80	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$80
$C$DW$T$80	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$80, DW_AT_type(*$C$DW$T$79)
	.dwattr $C$DW$T$80, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$80


$C$DW$TU$112	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$112
$C$DW$T$112	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$112, DW_AT_name("TIDL_CustomMaxPoolIxXOxXPrivArgs")
	.dwattr $C$DW$T$112, DW_AT_type(*$C$DW$T$83)
	.dwattr $C$DW$T$112, DW_AT_decl_file("./tidsp/tidl_custom_maxpool_ixX_oxX_priv.h")
	.dwattr $C$DW$T$112, DW_AT_decl_line(0x56)
	.dwattr $C$DW$T$112, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$112


$C$DW$TU$113	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$113
$C$DW$T$113	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$113, DW_AT_type(*$C$DW$T$112)

	.dwendtag $C$DW$TU$113


$C$DW$TU$114	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$114
$C$DW$T$114	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$114, DW_AT_type(*$C$DW$T$113)
	.dwattr $C$DW$T$114, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$114


$C$DW$TU$115	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$115
$C$DW$T$115	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$115, DW_AT_type(*$C$DW$T$112)
	.dwattr $C$DW$T$115, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$115


$C$DW$TU$78	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$78
$C$DW$T$78	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$78, DW_AT_type(*$C$DW$T$83)
	.dwattr $C$DW$T$78, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$78


$C$DW$TU$81	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$81

$C$DW$T$81	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$81, DW_AT_type(*$C$DW$T$78)
$C$DW$107	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$107, DW_AT_type(*$C$DW$T$80)

	.dwendtag $C$DW$T$81

	.dwendtag $C$DW$TU$81


$C$DW$TU$82	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$82

$C$DW$T$82	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$82, DW_AT_type(*$C$DW$T$78)
$C$DW$108	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$108, DW_AT_type(*$C$DW$T$78)

	.dwendtag $C$DW$T$82

	.dwendtag $C$DW$TU$82


$C$DW$TU$95	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$95

$C$DW$T$95	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$95, DW_AT_name("TIDL_CustomParams0_t")
	.dwattr $C$DW$T$95, DW_AT_byte_size(0x28)
$C$DW$109	.dwtag  DW_TAG_member
	.dwattr $C$DW$109, DW_AT_type(*$C$DW$T$59)
	.dwattr $C$DW$109, DW_AT_name("poolingType")
	.dwattr $C$DW$109, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$109, DW_AT_decl_file("./tidl_custom.h")
	.dwattr $C$DW$109, DW_AT_decl_line(0x70)
	.dwattr $C$DW$109, DW_AT_decl_column(0x0d)

$C$DW$110	.dwtag  DW_TAG_member
	.dwattr $C$DW$110, DW_AT_type(*$C$DW$T$59)
	.dwattr $C$DW$110, DW_AT_name("kernelW")
	.dwattr $C$DW$110, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$110, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$110, DW_AT_decl_file("./tidl_custom.h")
	.dwattr $C$DW$110, DW_AT_decl_line(0x72)
	.dwattr $C$DW$110, DW_AT_decl_column(0x0d)

$C$DW$111	.dwtag  DW_TAG_member
	.dwattr $C$DW$111, DW_AT_type(*$C$DW$T$59)
	.dwattr $C$DW$111, DW_AT_name("kernelH")
	.dwattr $C$DW$111, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$111, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$111, DW_AT_decl_file("./tidl_custom.h")
	.dwattr $C$DW$111, DW_AT_decl_line(0x74)
	.dwattr $C$DW$111, DW_AT_decl_column(0x0d)

$C$DW$112	.dwtag  DW_TAG_member
	.dwattr $C$DW$112, DW_AT_type(*$C$DW$T$59)
	.dwattr $C$DW$112, DW_AT_name("strideW")
	.dwattr $C$DW$112, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$112, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$112, DW_AT_decl_file("./tidl_custom.h")
	.dwattr $C$DW$112, DW_AT_decl_line(0x76)
	.dwattr $C$DW$112, DW_AT_decl_column(0x0d)

$C$DW$113	.dwtag  DW_TAG_member
	.dwattr $C$DW$113, DW_AT_type(*$C$DW$T$59)
	.dwattr $C$DW$113, DW_AT_name("strideH")
	.dwattr $C$DW$113, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$113, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$113, DW_AT_decl_file("./tidl_custom.h")
	.dwattr $C$DW$113, DW_AT_decl_line(0x78)
	.dwattr $C$DW$113, DW_AT_decl_column(0x0d)

$C$DW$114	.dwtag  DW_TAG_member
	.dwattr $C$DW$114, DW_AT_type(*$C$DW$T$59)
	.dwattr $C$DW$114, DW_AT_name("padW")
	.dwattr $C$DW$114, DW_AT_data_member_location[DW_OP_plus_uconst 0x14]
	.dwattr $C$DW$114, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$114, DW_AT_decl_file("./tidl_custom.h")
	.dwattr $C$DW$114, DW_AT_decl_line(0x7a)
	.dwattr $C$DW$114, DW_AT_decl_column(0x0d)

$C$DW$115	.dwtag  DW_TAG_member
	.dwattr $C$DW$115, DW_AT_type(*$C$DW$T$59)
	.dwattr $C$DW$115, DW_AT_name("padH")
	.dwattr $C$DW$115, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$115, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$115, DW_AT_decl_file("./tidl_custom.h")
	.dwattr $C$DW$115, DW_AT_decl_line(0x7c)
	.dwattr $C$DW$115, DW_AT_decl_column(0x0d)

$C$DW$116	.dwtag  DW_TAG_member
	.dwattr $C$DW$116, DW_AT_type(*$C$DW$T$59)
	.dwattr $C$DW$116, DW_AT_name("inDataQ")
	.dwattr $C$DW$116, DW_AT_data_member_location[DW_OP_plus_uconst 0x1c]
	.dwattr $C$DW$116, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$116, DW_AT_decl_file("./tidl_custom.h")
	.dwattr $C$DW$116, DW_AT_decl_line(0x7e)
	.dwattr $C$DW$116, DW_AT_decl_column(0x0d)

$C$DW$117	.dwtag  DW_TAG_member
	.dwattr $C$DW$117, DW_AT_type(*$C$DW$T$59)
	.dwattr $C$DW$117, DW_AT_name("outDataQ")
	.dwattr $C$DW$117, DW_AT_data_member_location[DW_OP_plus_uconst 0x20]
	.dwattr $C$DW$117, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$117, DW_AT_decl_file("./tidl_custom.h")
	.dwattr $C$DW$117, DW_AT_decl_line(0x80)
	.dwattr $C$DW$117, DW_AT_decl_column(0x0d)

$C$DW$118	.dwtag  DW_TAG_member
	.dwattr $C$DW$118, DW_AT_type(*$C$DW$T$59)
	.dwattr $C$DW$118, DW_AT_name("useCeil")
	.dwattr $C$DW$118, DW_AT_data_member_location[DW_OP_plus_uconst 0x24]
	.dwattr $C$DW$118, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$118, DW_AT_decl_file("./tidl_custom.h")
	.dwattr $C$DW$118, DW_AT_decl_line(0x82)
	.dwattr $C$DW$118, DW_AT_decl_column(0x0d)


$C$DW$119	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$119, DW_AT_name("operator =")
	.dwattr $C$DW$119, DW_AT_declaration
	.dwattr $C$DW$119, DW_AT_linkage_name("_ZN20TIDL_CustomParams0_taSERKS_")
	.dwattr $C$DW$119, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$119, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$120	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$120, DW_AT_type(*$C$DW$T$92)

	.dwendtag $C$DW$119


$C$DW$121	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$121, DW_AT_name("operator =")
	.dwattr $C$DW$121, DW_AT_declaration
	.dwattr $C$DW$121, DW_AT_linkage_name("_ZN20TIDL_CustomParams0_taSEOS_")
	.dwattr $C$DW$121, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$121, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$122	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$122, DW_AT_type(*$C$DW$T$90)

	.dwendtag $C$DW$121

	.dwattr $C$DW$T$95, DW_AT_decl_file("./tidl_custom.h")
	.dwattr $C$DW$T$95, DW_AT_decl_line(0x6e)
	.dwattr $C$DW$T$95, DW_AT_decl_column(0x10)
	.dwendtag $C$DW$T$95

	.dwendtag $C$DW$TU$95


$C$DW$TU$60	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$60
$C$DW$T$60	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$60, DW_AT_name("TIDL_CustomParams0_t")
	.dwattr $C$DW$T$60, DW_AT_type(*$C$DW$T$95)
	.dwattr $C$DW$T$60, DW_AT_decl_file("./tidl_custom.h")
	.dwattr $C$DW$T$60, DW_AT_decl_line(0x83)
	.dwattr $C$DW$T$60, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$60


$C$DW$TU$91	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$91
$C$DW$T$91	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$91, DW_AT_type(*$C$DW$T$95)

	.dwendtag $C$DW$TU$91


$C$DW$TU$92	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$92
$C$DW$T$92	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$92, DW_AT_type(*$C$DW$T$91)
	.dwattr $C$DW$T$92, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$92


$C$DW$TU$90	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$90
$C$DW$T$90	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$90, DW_AT_type(*$C$DW$T$95)
	.dwattr $C$DW$T$90, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$90


$C$DW$TU$93	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$93

$C$DW$T$93	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$93, DW_AT_type(*$C$DW$T$90)
$C$DW$123	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$123, DW_AT_type(*$C$DW$T$92)

	.dwendtag $C$DW$T$93

	.dwendtag $C$DW$TU$93


$C$DW$TU$94	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$94

$C$DW$T$94	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$94, DW_AT_type(*$C$DW$T$90)
$C$DW$124	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$124, DW_AT_type(*$C$DW$T$90)

	.dwendtag $C$DW$T$94

	.dwendtag $C$DW$TU$94


$C$DW$TU$102	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$102

$C$DW$T$102	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$102, DW_AT_name("TIDL_bufParams3D_t")
	.dwattr $C$DW$T$102, DW_AT_byte_size(0x18)
$C$DW$125	.dwtag  DW_TAG_member
	.dwattr $C$DW$125, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$125, DW_AT_name("data_type")
	.dwattr $C$DW$125, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$125, DW_AT_decl_file("../inc/tidl_dataflow.h")
	.dwattr $C$DW$125, DW_AT_decl_line(0xc1)
	.dwattr $C$DW$125, DW_AT_decl_column(0x0c)

$C$DW$126	.dwtag  DW_TAG_member
	.dwattr $C$DW$126, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$126, DW_AT_name("dim_x")
	.dwattr $C$DW$126, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$126, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$126, DW_AT_decl_file("../inc/tidl_dataflow.h")
	.dwattr $C$DW$126, DW_AT_decl_line(0xc4)
	.dwattr $C$DW$126, DW_AT_decl_column(0x0c)

$C$DW$127	.dwtag  DW_TAG_member
	.dwattr $C$DW$127, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$127, DW_AT_name("dim_y")
	.dwattr $C$DW$127, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$127, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$127, DW_AT_decl_file("../inc/tidl_dataflow.h")
	.dwattr $C$DW$127, DW_AT_decl_line(0xc7)
	.dwattr $C$DW$127, DW_AT_decl_column(0x0c)

$C$DW$128	.dwtag  DW_TAG_member
	.dwattr $C$DW$128, DW_AT_type(*$C$DW$T$59)
	.dwattr $C$DW$128, DW_AT_name("stride_y")
	.dwattr $C$DW$128, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$128, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$128, DW_AT_decl_file("../inc/tidl_dataflow.h")
	.dwattr $C$DW$128, DW_AT_decl_line(0xc9)
	.dwattr $C$DW$128, DW_AT_decl_column(0x0c)

$C$DW$129	.dwtag  DW_TAG_member
	.dwattr $C$DW$129, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$129, DW_AT_name("dim_z")
	.dwattr $C$DW$129, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$129, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$129, DW_AT_decl_file("../inc/tidl_dataflow.h")
	.dwattr $C$DW$129, DW_AT_decl_line(0xcc)
	.dwattr $C$DW$129, DW_AT_decl_column(0x0c)

$C$DW$130	.dwtag  DW_TAG_member
	.dwattr $C$DW$130, DW_AT_type(*$C$DW$T$59)
	.dwattr $C$DW$130, DW_AT_name("stride_z")
	.dwattr $C$DW$130, DW_AT_data_member_location[DW_OP_plus_uconst 0x14]
	.dwattr $C$DW$130, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$130, DW_AT_decl_file("../inc/tidl_dataflow.h")
	.dwattr $C$DW$130, DW_AT_decl_line(0xce)
	.dwattr $C$DW$130, DW_AT_decl_column(0x0c)


$C$DW$131	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$131, DW_AT_name("operator =")
	.dwattr $C$DW$131, DW_AT_declaration
	.dwattr $C$DW$131, DW_AT_linkage_name("_ZN18TIDL_bufParams3D_taSERKS_")
	.dwattr $C$DW$131, DW_AT_type(*$C$DW$T$97)
	.dwattr $C$DW$131, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$132	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$132, DW_AT_type(*$C$DW$T$99)

	.dwendtag $C$DW$131


$C$DW$133	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$133, DW_AT_name("operator =")
	.dwattr $C$DW$133, DW_AT_declaration
	.dwattr $C$DW$133, DW_AT_linkage_name("_ZN18TIDL_bufParams3D_taSEOS_")
	.dwattr $C$DW$133, DW_AT_type(*$C$DW$T$97)
	.dwattr $C$DW$133, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$134	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$134, DW_AT_type(*$C$DW$T$97)

	.dwendtag $C$DW$133

	.dwattr $C$DW$T$102, DW_AT_decl_file("../inc/tidl_dataflow.h")
	.dwattr $C$DW$T$102, DW_AT_decl_line(0xbf)
	.dwattr $C$DW$T$102, DW_AT_decl_column(0x01)
	.dwendtag $C$DW$T$102

	.dwendtag $C$DW$TU$102


$C$DW$TU$38	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$38
$C$DW$T$38	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$38, DW_AT_name("TIDL_bufParams3D_t")
	.dwattr $C$DW$T$38, DW_AT_type(*$C$DW$T$102)
	.dwattr $C$DW$T$38, DW_AT_decl_file("../inc/tidl_dataflow.h")
	.dwattr $C$DW$T$38, DW_AT_decl_line(0xcf)
	.dwattr $C$DW$T$38, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$38


$C$DW$TU$122	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$122
$C$DW$T$122	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$122, DW_AT_type(*$C$DW$T$38)

	.dwendtag $C$DW$TU$122


$C$DW$TU$123	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$123
$C$DW$T$123	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$123, DW_AT_type(*$C$DW$T$122)
	.dwattr $C$DW$T$123, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$123


$C$DW$TU$98	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$98
$C$DW$T$98	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$98, DW_AT_type(*$C$DW$T$102)

	.dwendtag $C$DW$TU$98


$C$DW$TU$99	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$99
$C$DW$T$99	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$99, DW_AT_type(*$C$DW$T$98)
	.dwattr $C$DW$T$99, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$99


$C$DW$TU$97	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$97
$C$DW$T$97	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$97, DW_AT_type(*$C$DW$T$102)
	.dwattr $C$DW$T$97, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$97


$C$DW$TU$100	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$100

$C$DW$T$100	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$100, DW_AT_type(*$C$DW$T$97)
$C$DW$135	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$135, DW_AT_type(*$C$DW$T$99)

	.dwendtag $C$DW$T$100

	.dwendtag $C$DW$TU$100


$C$DW$TU$101	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$101

$C$DW$T$101	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$101, DW_AT_type(*$C$DW$T$97)
$C$DW$136	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$136, DW_AT_type(*$C$DW$T$97)

	.dwendtag $C$DW$T$101

	.dwendtag $C$DW$TU$101


$C$DW$TU$2	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$2
$C$DW$T$2	.dwtag  DW_TAG_unspecified_type
	.dwattr $C$DW$T$2, DW_AT_name("void")

	.dwendtag $C$DW$TU$2


$C$DW$TU$3	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$3
$C$DW$T$3	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$3, DW_AT_type(*$C$DW$T$2)
	.dwattr $C$DW$T$3, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$3


$C$DW$TU$72	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$72
$C$DW$T$72	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$72, DW_AT_type(*$C$DW$T$2)

	.dwendtag $C$DW$TU$72


$C$DW$TU$73	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$73
$C$DW$T$73	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$73, DW_AT_type(*$C$DW$T$72)
	.dwattr $C$DW$T$73, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$73


$C$DW$TU$132	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$132
$C$DW$T$132	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$132, DW_AT_type(*$C$DW$T$73)
	.dwattr $C$DW$T$132, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$132


$C$DW$TU$4	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$4
$C$DW$T$4	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$4, DW_AT_encoding(DW_ATE_boolean)
	.dwattr $C$DW$T$4, DW_AT_name("bool")
	.dwattr $C$DW$T$4, DW_AT_byte_size(0x01)

	.dwendtag $C$DW$TU$4


$C$DW$TU$5	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$5
$C$DW$T$5	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$5, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$5, DW_AT_name("signed char")
	.dwattr $C$DW$T$5, DW_AT_byte_size(0x01)

	.dwendtag $C$DW$TU$5


$C$DW$TU$6	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$6
$C$DW$T$6	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$6, DW_AT_encoding(DW_ATE_unsigned_char)
	.dwattr $C$DW$T$6, DW_AT_name("unsigned char")
	.dwattr $C$DW$T$6, DW_AT_byte_size(0x01)

	.dwendtag $C$DW$TU$6


$C$DW$TU$7	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$7
$C$DW$T$7	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$7, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$7, DW_AT_name("wchar_t")
	.dwattr $C$DW$T$7, DW_AT_byte_size(0x04)

	.dwendtag $C$DW$TU$7


$C$DW$TU$8	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$8
$C$DW$T$8	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$8, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$8, DW_AT_name("short")
	.dwattr $C$DW$T$8, DW_AT_byte_size(0x02)

	.dwendtag $C$DW$TU$8


$C$DW$TU$9	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$9
$C$DW$T$9	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$9, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$9, DW_AT_name("unsigned short")
	.dwattr $C$DW$T$9, DW_AT_byte_size(0x02)

	.dwendtag $C$DW$TU$9


$C$DW$TU$10	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$10
$C$DW$T$10	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$10, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$10, DW_AT_name("int")
	.dwattr $C$DW$T$10, DW_AT_byte_size(0x04)

	.dwendtag $C$DW$TU$10


$C$DW$TU$58	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$58
$C$DW$T$58	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$58, DW_AT_name("__int32_t")
	.dwattr $C$DW$T$58, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$T$58, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/machine/_types.h")
	.dwattr $C$DW$T$58, DW_AT_decl_line(0x64)
	.dwattr $C$DW$T$58, DW_AT_decl_column(0x0f)

	.dwendtag $C$DW$TU$58


$C$DW$TU$59	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$59
$C$DW$T$59	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$59, DW_AT_name("int32_t")
	.dwattr $C$DW$T$59, DW_AT_type(*$C$DW$T$58)
	.dwattr $C$DW$T$59, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/sys/_stdint.h")
	.dwattr $C$DW$T$59, DW_AT_decl_line(0x30)
	.dwattr $C$DW$T$59, DW_AT_decl_column(0x14)

	.dwendtag $C$DW$TU$59


$C$DW$TU$74	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$74

$C$DW$T$74	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$74, DW_AT_type(*$C$DW$T$59)
$C$DW$137	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$137, DW_AT_type(*$C$DW$T$3)

$C$DW$138	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$138, DW_AT_type(*$C$DW$T$73)

$C$DW$139	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$139, DW_AT_type(*$C$DW$T$3)

	.dwendtag $C$DW$T$74

	.dwendtag $C$DW$TU$74


$C$DW$TU$75	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$75
$C$DW$T$75	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$75, DW_AT_type(*$C$DW$T$74)
	.dwattr $C$DW$T$75, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$75


$C$DW$TU$76	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$76
$C$DW$T$76	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$76, DW_AT_name("TIDL_customMaxPoolExecFunc")
	.dwattr $C$DW$T$76, DW_AT_type(*$C$DW$T$75)
	.dwattr $C$DW$T$76, DW_AT_decl_file("./tidsp/tidl_custom_maxpool_ixX_oxX_priv.h")
	.dwattr $C$DW$T$76, DW_AT_decl_line(0x35)
	.dwattr $C$DW$T$76, DW_AT_decl_column(0x13)

	.dwendtag $C$DW$TU$76


$C$DW$TU$11	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$11
$C$DW$T$11	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$11, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$11, DW_AT_name("unsigned int")
	.dwattr $C$DW$T$11, DW_AT_byte_size(0x04)

	.dwendtag $C$DW$TU$11


$C$DW$TU$25	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$25
$C$DW$T$25	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$25, DW_AT_name("__uint32_t")
	.dwattr $C$DW$T$25, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$T$25, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/machine/_types.h")
	.dwattr $C$DW$T$25, DW_AT_decl_line(0x65)
	.dwattr $C$DW$T$25, DW_AT_decl_column(0x17)

	.dwendtag $C$DW$TU$25


$C$DW$TU$26	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$26
$C$DW$T$26	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$26, DW_AT_name("uint32_t")
	.dwattr $C$DW$T$26, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$T$26, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/sys/_stdint.h")
	.dwattr $C$DW$T$26, DW_AT_decl_line(0x46)
	.dwattr $C$DW$T$26, DW_AT_decl_column(0x15)

	.dwendtag $C$DW$TU$26


$C$DW$TU$27	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$27

$C$DW$T$27	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$27, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$T$27, DW_AT_byte_size(0x40)
$C$DW$140	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$140, DW_AT_upper_bound(0x0f)

	.dwendtag $C$DW$T$27

	.dwendtag $C$DW$TU$27


$C$DW$TU$12	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$12
$C$DW$T$12	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$12, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$12, DW_AT_name("__int40_t")
	.dwattr $C$DW$T$12, DW_AT_byte_size(0x08)

	.dwendtag $C$DW$TU$12


$C$DW$TU$13	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$13
$C$DW$T$13	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$13, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$13, DW_AT_name("unsigned __int40_t")
	.dwattr $C$DW$T$13, DW_AT_byte_size(0x08)

	.dwendtag $C$DW$TU$13


$C$DW$TU$14	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$14
$C$DW$T$14	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$14, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$14, DW_AT_name("long long")
	.dwattr $C$DW$T$14, DW_AT_byte_size(0x08)

	.dwendtag $C$DW$TU$14


$C$DW$TU$15	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$15
$C$DW$T$15	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$15, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$15, DW_AT_name("unsigned long long")
	.dwattr $C$DW$T$15, DW_AT_byte_size(0x08)

	.dwendtag $C$DW$TU$15


$C$DW$TU$16	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$16
$C$DW$T$16	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$16, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$16, DW_AT_name("float")
	.dwattr $C$DW$T$16, DW_AT_byte_size(0x04)

	.dwendtag $C$DW$TU$16


$C$DW$TU$17	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$17
$C$DW$T$17	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$17, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$17, DW_AT_name("double")
	.dwattr $C$DW$T$17, DW_AT_byte_size(0x08)

	.dwendtag $C$DW$TU$17


$C$DW$TU$18	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$18
$C$DW$T$18	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$18, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$18, DW_AT_name("long double")
	.dwattr $C$DW$T$18, DW_AT_byte_size(0x08)

	.dwendtag $C$DW$TU$18


$C$DW$TU$19	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$19
$C$DW$T$19	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$19, DW_AT_encoding(DW_ATE_TI_complex_signed)
	.dwattr $C$DW$T$19, DW_AT_name("cchar")
	.dwattr $C$DW$T$19, DW_AT_byte_size(0x02)

	.dwendtag $C$DW$TU$19


$C$DW$TU$20	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$20
$C$DW$T$20	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$20, DW_AT_encoding(DW_ATE_TI_complex_signed)
	.dwattr $C$DW$T$20, DW_AT_name("cshort")
	.dwattr $C$DW$T$20, DW_AT_byte_size(0x04)

	.dwendtag $C$DW$TU$20


$C$DW$TU$21	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$21
$C$DW$T$21	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$21, DW_AT_encoding(DW_ATE_TI_complex_signed)
	.dwattr $C$DW$T$21, DW_AT_name("cint")
	.dwattr $C$DW$T$21, DW_AT_byte_size(0x08)

	.dwendtag $C$DW$TU$21


$C$DW$TU$22	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$22
$C$DW$T$22	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$22, DW_AT_encoding(DW_ATE_TI_complex_signed)
	.dwattr $C$DW$T$22, DW_AT_name("clong")
	.dwattr $C$DW$T$22, DW_AT_byte_size(0x10)

	.dwendtag $C$DW$TU$22


$C$DW$TU$23	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$23
$C$DW$T$23	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$23, DW_AT_encoding(DW_ATE_TI_complex_float)
	.dwattr $C$DW$T$23, DW_AT_name("cfloat")
	.dwattr $C$DW$T$23, DW_AT_byte_size(0x08)

	.dwendtag $C$DW$TU$23


$C$DW$TU$24	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$24
$C$DW$T$24	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$24, DW_AT_encoding(DW_ATE_TI_complex_float)
	.dwattr $C$DW$T$24, DW_AT_name("cdouble")
	.dwattr $C$DW$T$24, DW_AT_byte_size(0x10)

	.dwendtag $C$DW$TU$24

