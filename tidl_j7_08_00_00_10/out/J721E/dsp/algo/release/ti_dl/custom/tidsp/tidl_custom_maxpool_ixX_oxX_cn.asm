;******************************************************************************
;* C7x G3 C/C++ Codegen                                            Unix v1.4.2.LTS *
;* Date/Time created: Tue Oct  5 15:11:17 2021                                *
;******************************************************************************
	.compiler_opts --abi=eabi --endian=little --hll_source=on --object_format=elf --silicon_errata_i2117 --silicon_version=7100 --symdebug:dwarf --symdebug:dwarf_version=4 

;******************************************************************************
;* GLOBAL FILE PARAMETERS                                                     *
;*                                                                            *
;*   Architecture      : C7100                                                *
;*   Endian            : Little                                               *
;*   Pipelining        : Enabled                                              *
;*   Debug Info        : DWARF Debug                                          *
;*                                                                            *
;******************************************************************************

	.asg	D14, FP
	.asg	D15, SP
	.global	$bss


$C$DW$CU	.dwtag  DW_TAG_compile_unit
	.dwattr $C$DW$CU, DW_AT_name("tidsp/tidl_custom_maxpool_ixX_oxX_cn.c")
	.dwattr $C$DW$CU, DW_AT_producer("TI C7x G3 C/C++ Codegen Unix v1.4.2.LTS Copyright (c) 2014-2019 Texas Instruments Incorporated")
	.dwattr $C$DW$CU, DW_AT_TI_version(0x01)
	.dwattr $C$DW$CU, DW_AT_comp_dir("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/tidl_j7_08_00_00_10/ti_dl/custom")
;	/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/bin/opt7x /tmp/TI4XXDWB4z1 /tmp/TI4XXmK0F6o 
	.sect	".text:_ZN54_INTERNAL_32_tidl_custom_maxpool_ixX_oxX_cn_c_fd8ecb3d39TIDL_customMaxPooling_3x3_skip2_exec_cnEPKhPhP18TIDL_bufParams3D_tS4_i"
	.clink

$C$DW$1	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1, DW_AT_name("TIDL_customMaxPooling_3x3_skip2_exec_cn")
	.dwattr $C$DW$1, DW_AT_low_pc(||_ZN54_INTERNAL_32_tidl_custom_maxpool_ixX_oxX_cn_c_fd8ecb3d39TIDL_customMaxPooling_3x3_skip2_exec_cnEPKhPhP18TIDL_bufParams3D_tS4_i||)
	.dwattr $C$DW$1, DW_AT_high_pc(0x00)
	.dwattr $C$DW$1, DW_AT_linkage_name("_ZN54_INTERNAL_32_tidl_custom_maxpool_ixX_oxX_cn_c_fd8ecb3d39TIDL_customMaxPooling_3x3_skip2_exec_cnEPKhPhP18TIDL_bufParams3D_tS4_i")
	.dwattr $C$DW$1, DW_AT_decl_file("tidsp/tidl_custom_maxpool_ixX_oxX_cn.c")
	.dwattr $C$DW$1, DW_AT_decl_line(0x6c)
	.dwattr $C$DW$1, DW_AT_decl_column(0x10)
	.dwattr $C$DW$1, DW_AT_TI_max_frame_size(0x38)
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_cn.c",line 113,column 1,is_stmt,address ||_ZN54_INTERNAL_32_tidl_custom_maxpool_ixX_oxX_cn_c_fd8ecb3d39TIDL_customMaxPooling_3x3_skip2_exec_cnEPKhPhP18TIDL_bufParams3D_tS4_i||,isa 0

	.dwfde $C$DW$CIE, ||_ZN54_INTERNAL_32_tidl_custom_maxpool_ixX_oxX_cn_c_fd8ecb3d39TIDL_customMaxPooling_3x3_skip2_exec_cnEPKhPhP18TIDL_bufParams3D_tS4_i||
$C$DW$16	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$16, DW_AT_name("src")
	.dwattr $C$DW$16, DW_AT_type(*$C$DW$T$111)
	.dwattr $C$DW$16, DW_AT_location[DW_OP_reg4]

$C$DW$17	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$17, DW_AT_name("dst")
	.dwattr $C$DW$17, DW_AT_type(*$C$DW$T$108)
	.dwattr $C$DW$17, DW_AT_location[DW_OP_reg5]

$C$DW$18	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$18, DW_AT_name("srcAddr")
	.dwattr $C$DW$18, DW_AT_location[DW_OP_reg6]

$C$DW$19	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$19, DW_AT_name("dstAddr")
	.dwattr $C$DW$19, DW_AT_location[DW_OP_reg7]

$C$DW$20	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$20, DW_AT_name("outHeight")
	.dwattr $C$DW$20, DW_AT_location[DW_OP_reg8]


;******************************************************************************
;* FUNCTION NAME: [local to tidl_custom_maxpool_ixX_oxX_cn_c]::TIDL_customMaxPooling_3x3_skip2_exec_cn(const unsigned char *, unsigned char *, TIDL_bufParams3D_t *, TIDL_bufParams3D_t *, int)*
;*                                                                            *
;*   Regs Modified     : A0,A1,A2,A3,A4,A7,A8,A9,A10,A11,A12,A13,A14,VB0,VB1, *
;*                           VB2,VB3,AL0,AL1,AL2,AM0,AM1,AM2,AM3,AM4,AM5,D0,  *
;*                           D1,D2,D3,D4,D5,D6,D14,SP,VBL0,VBL1               *
;*   Regs Used         : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,A14,   *
;*                           VB0,VB1,VB2,VB3,AL0,AL1,AL2,AM0,AM1,AM2,AM3,AM4, *
;*                           AM5,D0,D1,D2,D3,D4,D5,D6,D14,SP,VBL0,VBL1        *
;*   Local Frame Size  : 0 Args + 0 Auto + 56 Save = 56 byte                  *
;******************************************************************************
||_ZN54_INTERNAL_32_tidl_custom_maxpool_ixX_oxX_cn_c_fd8ecb3d39TIDL_customMaxPooling_3x3_skip2_exec_cnEPKhPhP18TIDL_bufParams3D_tS4_i||:
;** --------------------------------------------------------------------------*
;* A2    assigned to $O$C18
;* D3    assigned to $O$H16
;* AM3   assigned to $O$H17
;* A13   assigned to $O$K26
;* A14   assigned to $O$U55
;* D6    assigned to $O$U73
;* D5    assigned to $O$U54
;* AM0   assigned to $O$K50
;* VB3   assigned to $O$U48
;* AM4   assigned to $O$U47
;* D4    assigned to $O$U44
;* A2    assigned to $O$L1
;* A4    assigned to $O$L2
;* A3    assigned to $O$L3
;* AL2   assigned to $O$v1
;* A12   assigned to $O$Lr83$width
;* AM2   assigned to $O$Lr92$inChPitch
;* AM1   assigned to $O$Lr95$outChPitch
;* A10   assigned to $O$Lr9$i1
;* VB1   assigned to $O$Lr28$status
;* A8    assigned to $O$Lr16$i2
;* A7    assigned to $O$Lr24$i3
;* VB2   assigned to $O$Lr68$maxvalueBlock
;* VB0   assigned to $O$Lr42$i4
;* A1    assigned to $O$Lr72$i5
;* A8    assigned to outHeight
$C$DW$21	.dwtag  DW_TAG_variable
	.dwattr $C$DW$21, DW_AT_name("outHeight")
	.dwattr $C$DW$21, DW_AT_type(*$C$DW$T$50)
	.dwattr $C$DW$21, DW_AT_location[DW_OP_reg8]

;* A7    assigned to dstAddr
$C$DW$22	.dwtag  DW_TAG_variable
	.dwattr $C$DW$22, DW_AT_name("dstAddr")
	.dwattr $C$DW$22, DW_AT_type(*$C$DW$T$93)
	.dwattr $C$DW$22, DW_AT_location[DW_OP_reg7]

;* A6    assigned to srcAddr
$C$DW$23	.dwtag  DW_TAG_variable
	.dwattr $C$DW$23, DW_AT_name("srcAddr")
	.dwattr $C$DW$23, DW_AT_type(*$C$DW$T$93)
	.dwattr $C$DW$23, DW_AT_location[DW_OP_reg6]

;* A5    assigned to dst
$C$DW$24	.dwtag  DW_TAG_variable
	.dwattr $C$DW$24, DW_AT_name("dst")
	.dwattr $C$DW$24, DW_AT_type(*$C$DW$T$108)
	.dwattr $C$DW$24, DW_AT_location[DW_OP_reg5]

;* A9    assigned to src
$C$DW$25	.dwtag  DW_TAG_variable
	.dwattr $C$DW$25, DW_AT_name("src")
	.dwattr $C$DW$25, DW_AT_type(*$C$DW$T$111)
	.dwattr $C$DW$25, DW_AT_location[DW_OP_reg9]

	.dwcfi	cfa_offset, 0
;          EXCLUSIVE CPU CYCLES: 8
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_cn.c",line 132,column 13,is_stmt,isa 0
           LDUH    .D1     *A7(16),A2        ; [A_D1] |132| 

           STD     .D1     A8,*SP(8)         ; [A_D1] 
||         STD     .D2X    A9,*SP++(-56)     ; [A_D2] 

	.dwcfi	save_reg_to_mem, 8, 8
	.dwcfi	cfa_offset, 56
	.dwcfi	save_reg_to_mem, 9, -56
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_cn.c",line 127,column 3,is_stmt,isa 0

           LDUW    .D1     *A6(20),AM2       ; [A_D1] |127| 
||         LDUW    .D2     *A7(20),AM1       ; [A_D2] |128| 

           STD     .D1     A12,*SP(32)       ; [A_D1] 
||         STD     .D2X    A10,*SP(48)       ; [A_D2] 

	.dwcfi	save_reg_to_mem, 12, 32
	.dwcfi	save_reg_to_mem, 10, 48
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_cn.c",line 113,column 1,is_stmt,isa 0

           CMPEQW  .L1     A2,0,A0           ; [A_L1] |132| 
||         STD     .D1     A11,*SP(40)       ; [A_D1] 
||         STD     .D2X    A13,*SP(24)       ; [A_D2] 
||         MV      .S1     A4,A9             ; [A_S1] |113| 

	.dwcfi	save_reg_to_mem, 11, 40
	.dwcfi	save_reg_to_mem, 13, 24
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_cn.c",line 123,column 3,is_stmt,isa 0

   [ A0]   B       .B1     ||$C$L11||        ; [A_B] |167| 
||         ANDW    .M1     A8,0xffff,A13     ; [A_M1] 
||         MVKU32  .L1     0,A10             ; [A_L1] |132| 
||         LDUH    .D1     *A7(4),A12        ; [A_D1] |123| 
||         STD     .D2X    A14,*SP(16)       ; [A_D2] 
||         MVKU32  .S1     0,A4              ; [A_S1] |167| 
||         MVKU32  .L2     0,B1              ; [B_L2] 

	.dwcfi	save_reg_to_mem, 14, 16
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_cn.c",line 167,column 3,is_stmt,isa 0
           ; BRANCHCC OCCURS {||$C$L11||}    ; [] |167| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 7
           LDUH    .D1     *A6(12),D3        ; [A_D1] 
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_cn.c",line 152,column 23,is_stmt,isa 0

           MV      .D1     D3,AM0            ; [A_D1] 
||         MVKU32  .L1     0x3,A11           ; [A_L1] |152| 
||         LDUH    .D2     *A7(12),AM3       ; [A_D2] 

;** --------------------------------------------------------------------------*
;**   BEGIN LOOP ||$C$L1||
;** --------------------------------------------------------------------------*
||$C$L1||:    
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_cn.c",line 134,column 15,is_stmt,isa 0
           CMPEQW  .L1     A13,0,A0          ; [A_L1] |134| 
   [ A0]   B       .B1     ||$C$L10||        ; [A_B] |134| 
           ; BRANCHCC OCCURS {||$C$L10||}    ; [] |134| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 1
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_cn.c",line 134,column 9,is_stmt,isa 0

           MPYWW   .N1     AM2,A10,D4        ; [A_N1] 
||         MVKU32  .L1     0,A8              ; [A_L1] |134| 
||         MV      .D1     A13,A4            ; [A_D1] 

;** --------------------------------------------------------------------------*
;**   BEGIN LOOP ||$C$L2||
;** --------------------------------------------------------------------------*
||$C$L2||:    
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_cn.c",line 136,column 17,is_stmt,isa 0
           CMPEQW  .L1     A12,0,A0          ; [A_L1] |136| 
   [ A0]   B       .B1     ||$C$L9||         ; [A_B] |136| 
           ; BRANCHCC OCCURS {||$C$L9||}     ; [] |136| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 6
           LDUW    .D1     *A6(0),AL2        ; [A_D1] 
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_cn.c",line 136,column 11,is_stmt,isa 0

           SHLW    .L1     A8,0x1,AM4        ; [A_L1] 
||         MVKU32  .S1     0,A7              ; [A_S1] |136| 
||         MV      .D1     A12,A3            ; [A_D1] 

;** --------------------------------------------------------------------------*
;**   BEGIN LOOP ||$C$L3||
;** --------------------------------------------------------------------------*
||$C$L3||:    
;          EXCLUSIVE CPU CYCLES: 3
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_cn.c",line 138,column 9,is_stmt,isa 0
           CMPEQW  .L1     AL2,0x4,A0        ; [A_L1] |138| 
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_cn.c",line 144,column 11,is_stmt,isa 0

   [!A0]   MVK32   .L2     0xffffffff,B1     ; [B_L2] |144| 
|| [ A0]   CMPEQW  .L1X    B1,0,A0           ; [A_L1] |147| 
|| [!A0]   MVKU32  .S1     0,A0              ; [A_S1] |147| 

	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_cn.c",line 147,column 9,is_stmt,isa 0
   [!A0]   B       .B1     ||$C$L8||         ; [A_B] |147| 
           ; BRANCHCC OCCURS {||$C$L8||}     ; [] |147| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 5

           MPYWW   .N1     AM1,A10,D0        ; [A_N1] 
||         MPYWW   .M1     AM3,A8,D1         ; [A_M1] 

           MVKU32  .L2     0,B3              ; [B_L2] 
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_cn.c",line 149,column 11,is_stmt,isa 0

           ADDW    .D1     D1,D0,D6          ; [A_D1] 
||         SHLW    .L1     A7,0x1,D5         ; [A_L1] 
||         MVKU32  .L2     0,B2              ; [B_L2] |149| 
||         MVKU32  .S2     0,B0              ; [B_S2] |150| 

;** --------------------------------------------------------------------------*
;**   BEGIN LOOP ||$C$L4||
;** --------------------------------------------------------------------------*
||$C$L4||:    
;          EXCLUSIVE CPU CYCLES: 15
           ADDW    .M1X    B3,AM4,AM5        ; [A_M1] 
           MPYWW   .N1     AM0,AM5,D0        ; [A_N1] 
           ADDW    .D1     D4,D0,D0          ; [A_D1] 
           ADDW    .D1     D5,D0,A14         ; [A_D1] 
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_cn.c",line 152,column 17,is_stmt,isa 0

           MV      .D1     A14,D2            ; [A_D1] 
||         MVKU32  .L1     0,D3              ; [A_L1] |152| 

	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_cn.c",line 154,column 15,is_stmt,isa 0

           ADDW    .D1     D2,D3,D0          ; [A_D1] |154| <0,0>  ^ 
||         MV      .D2     A9,D1             ; [A_D2] 

           ADDD    .D1     D1,D0,D0          ; [A_D1] |154| <0,1>  ^ 
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_cn.c",line 152,column 23,is_stmt,isa 0

           LDUB    .D1     *D0(0),BL0        ; [A_D1] |154| <0,2>  ^ 
||         ADDW    .D2     D3,0x1,AL0        ; [A_D2] |152| <0,0> 

           ANDW    .L1     AL0,0xffff,A1     ; [A_L1] |152| <0,3> [C0]
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_cn.c",line 154,column 15,is_stmt,isa 0

           ADDW    .D1     D2,A1,D14         ; [A_D1] |154| <1,0>  ^ 
||         MV      .D2     A11,AL1           ; [A_D2] 

	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_cn.c",line 152,column 23,is_stmt,isa 0

           ADDD    .D1     D1,D14,D0         ; [A_D1] |154| <1,1>  ^ 
||         ADDW    .D2     A1,0x1,AL0        ; [A_D2] |152| <1,0> 
||         CMPGTUW .L1     AL1,A1,A0         ; [A_L1] |152| <0,5>  ^ 
||         UNPROT          0x1               ; [A_U] 
||         MV      .L2     B2,BL1            ; [B_L2] 

;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*
;*      Loop found in file               : tidsp/tidl_custom_maxpool_ixX_oxX_cn.c
;*      Loop source line                 : 152
;*      Loop opening brace source line   : 153
;*      Loop closing brace source line   : 159
;*      Known Minimum Trip Count         : 1                    
;*      Known Max Trip Count Factor      : 1
;*      Loop Carried Dependency Bound(^) : 4
;*      Unpartitioned Resource Bound     : 1
;*      Partitioned Resource Bound       : 2 (pre-sched)
;*
;*      Searching for software pipeline schedule at ...
;*         ii = 4  Schedule found with 3 iterations in parallel
;*
;*      Partitioned Resource Bound(*)    : 2 (post-sched)
;*
;*      Constant Extension #0 Used [C0]  : 1
;*      Constant Extension #1 Used [C1]  : 1
;*
;*      Resource Partition (may include "post-sched" split/spill moves):
;*
;*                                                A-side   B-side
;*      .L units                                     0        0     
;*      .S units                                     0        0     
;*      .M units                                     0        0     
;*      .N units                                     0        0     
;*      .D units                                     1        -     
;*      .B units                                     1        -     
;*      .C units                                     -        0     
;*      .P units                                     -        0     
;*
;*      .M/.N units                                  0        0     
;*      .L/.S units                                  1        0     
;*      .L/.S/.C units                               0        0     
;*      .L/.S/.C/.M units                            1        2     
;*      .L/.S/.C/.M/.D units                         3        0     
;*
;*      .X cross paths                               0        0     
;*
;*      Bound(.D1 .D2 .D)                            1        -     
;*      Bound(.M .N .MN)                             0        0     
;*      Bound(.L .S .LS)                             1        0     
;*      Bound(.L .S .C .LS .LSC)                     1        0     
;*      Bound(.L .S .C .M .LS .LSC .LSCM)            1        1     
;*      Bound(.L .S .C .M .D .LS .LSC .LSCM .LSCMD)  2*       1     
;*
;*      Register Usage Tables:
;*
;*      +----------------------------------+----------------------------------+
;*      |              ASIDE               |              BSIDE               |
;*      +----------------+--------+--------+----------------+--------+--------+
;*      |      "Axx"     |  "ALx" |  "AMx" |     "VBxx"     | "VBLx" | "VBMx" |
;*      +----------------|--------|--------|----------------|--------|--------+
;*      |0000000000111111|00000000|00000000|0000000000111111|00000000|00000000|
;*      |0123456789012345|01234567|01234567|0123456789012345|01234567|01234567|
;*      +----------------|--------|--------|----------------|--------|--------+
;*   0: |*               |**      |        |                |**      |        |
;*   1: |*               |**      |        |                |**      |        |
;*   2: |**              | *      |        |                |**      |        |
;*   3: |**              |**      |        |                |**      |        |
;*      +----------------+--------+--------+----------------+--------+--------+
;*
;*      +----------------+                 +--------+       +-------+
;*      |      "Dxx"     |                 |  "Px"  |       |"CUCRx"|
;*      +----------------+                 +--------+       +-------+
;*      |0000000000111111|                 |00000000|       |0000   |
;*      |0123456789012345|                 |01234567|       |0123   |
;*      +----------------+                 +--------+       +-------+
;*   0: |***             |                 |        |       |       |
;*   1: | **             |                 |        |       |       |
;*   2: | **             |                 |        |       |       |
;*   3: |***             |                 |        |       |       |
;*      +----------------+                 +--------+       +-------+
;*
;*      Done
;*
;*      Collapsed epilog stages       : 2
;*      Prolog not removed
;*      Collapsed prolog stages       : 0
;*
;*      Max amt of load speculation   : 0 bytes
;*
;*      Minimum safe trip count       : 1
;*      Min. prof. trip count  (est.) : 2
;*
;*      Mem bank conflicts/iter(est.) : { min 0.000, est 0.000, max 0.000 }
;*      Mem bank perf. penalty (est.) : 0.0%
;*
;*
;*      Total cycles (est.)         : 6 + trip_cnt * 4        
;*----------------------------------------------------------------------------*
;*       SETUP CODE
;*
;*                  MVKU32  0x1,A0  ; [] 
;*
;*        SINGLE SCHEDULED ITERATION
;*
;*        ||$C$C51||:
;*   0              ADDW    .D1     D2,A1,D0          ; [A_D1] |154|  ^ 
;*     ||           ADDW    .D2     A1,0x1,AL0        ; [A_D2] |152| 
;*   1              ADDD    .D1     D1,D0,D0          ; [A_D1] |154|  ^ 
;*   2      [ A0]   LDUB    .D1     *D0(0),BL0        ; [A_D1] |154|  ^ 
;*   3              ANDW    .L1     AL0,0xffff,A1     ; [A_L1] |152| [C0]
;*   4              NOP     0x1     ; [A_B] 
;*   5              CMPGTUW .L1     AL1,A1,A0         ; [A_L1] |152|  ^ 
;*   6              NOP     0x2     ; [A_B] 
;*   8              VMAXUB  .L2     BL1,BL0,BL1       ; [B_L2] |154| 
;*   9              ANDW    .L2     BL1,0xff,BL1      ; [B_L2] |154| [C1]
;*     ||   [ A0]   B       .B1     ||$C$C51||        ; [A_B] |152| 
;*  10              ; BRANCHCC OCCURS {||$C$C51||}    ; [] |152| 
;*----------------------------------------------------------------------------*
||$C$L5||:    ; PIPED LOOP PROLOG
;** --------------------------------------------------------------------------*
||$C$L6||:    ; PIPED LOOP KERNEL
;          EXCLUSIVE CPU CYCLES: 4
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_cn.c",line 154,column 15,is_stmt,isa 0
   [ A0]   LDUB    .D1     *D0(0),BL0        ; [A_D1] |154| <1,2>  ^ 
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_cn.c",line 152,column 23,is_stmt,isa 0
           ANDW    .L1     AL0,0xffff,A1     ; [A_L1] |152| <1,3> [C0]

           VMAXUB  .L2     BL1,BL0,BL1       ; [B_L2] |154| <0,8> 
||         ADDW    .D2     A1,0x1,AL0        ; [A_D2] |152| <2,0> 
||         ADDW    .D1     D2,A1,D0          ; [A_D1] |154| <2,0>  ^ 

   [ A0]   B       .B1     ||$C$L6||         ; [A_B] |152| <0,9> 
||         ANDW    .L2     BL1,0xff,BL1      ; [B_L2] |154| <0,9> [C1]
||         CMPGTUW .L1     AL1,A1,A0         ; [A_L1] |152| <1,5>  ^ 
||         ADDD    .D1     D1,D0,D0          ; [A_D1] |154| <2,1>  ^ 

;** --------------------------------------------------------------------------*
||$C$L7||:    ; PIPED LOOP EPILOG
;          EXCLUSIVE CPU CYCLES: 1
           PROTCLR                            ; [A_U] VBL1
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 6
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_cn.c",line 150,column 21,is_stmt,isa 0

           ADDW    .S2     B0,0x1,BL1        ; [B_S2] |150| 
||         MV      .L2     BL1,B2            ; [B_L2] 

           ANDW    .L2     BL1,0xffff,B0     ; [B_L2] |150| 

           MV      .S1X    B0,AL0            ; [A_S1] |150| Define a twin register
||         MVKU32  .L1     0x3,AL1           ; [A_L1] |150| 

           CMPGTUW .L1     AL1,AL0,A0        ; [A_L1] |150| 

   [ A0]   B       .B1     ||$C$L4||         ; [A_B] |150| 
||         MV      .L2     B0,B3             ; [B_L2] |150| 
||         MV      .D1     D1,A9             ; [A_D1] 

           ; BRANCHCC OCCURS {||$C$L4||}     ; [] |150| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 3
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_cn.c",line 161,column 11,is_stmt,isa 0
           ADDW    .D1     D6,A7,D0          ; [A_D1] |161| 
           ADDD    .D1     A5,D0,D0          ; [A_D1] |161| 
           STB     .D1X    B2,*D0(0)         ; [A_D1] |161| 
;** --------------------------------------------------------------------------*
||$C$L8||:    
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_cn.c",line 136,column 17,is_stmt,isa 0

           ADDW    .D1     A3,0xffffffff,A3  ; [A_D1] |136| 
||         ADDW    .D2     A7,0x1,AL0        ; [A_D2] |136| 

   [ A3]   B       .B1     ||$C$L3||         ; [A_B] |136| 
||         ANDW    .L1     AL0,0xffff,A7     ; [A_L1] |136| 

           ; BRANCHCC OCCURS {||$C$L3||}     ; [] |136| 
;** --------------------------------------------------------------------------*
||$C$L9||:    
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_cn.c",line 134,column 15,is_stmt,isa 0

           ADDW    .D1     A4,0xffffffff,A4  ; [A_D1] |134| 
||         ADDW    .D2     A8,0x1,AL0        ; [A_D2] |134| 

   [ A4]   B       .B1     ||$C$L2||         ; [A_B] |134| 
||         ANDW    .L1     AL0,0xffff,A8     ; [A_L1] |134| 

           ; BRANCHCC OCCURS {||$C$L2||}     ; [] |134| 
;** --------------------------------------------------------------------------*
||$C$L10||:    
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_cn.c",line 132,column 13,is_stmt,isa 0

           ADDW    .D1     A2,0xffffffff,A2  ; [A_D1] |132| 
||         ADDW    .D2     A10,0x1,AL0       ; [A_D2] |132| 

   [ A2]   B       .B1     ||$C$L1||         ; [A_B] |132| 
||         ANDW    .L1     AL0,0xffff,A10    ; [A_L1] |132| 

           ; BRANCHCC OCCURS {||$C$L1||}     ; [] |132| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 1
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_cn.c",line 167,column 3,is_stmt,isa 0
           MV      .L1X    B1,A4             ; [A_L1] |167| 
;** --------------------------------------------------------------------------*
||$C$L11||:    
;          EXCLUSIVE CPU CYCLES: 9
           LDD     .D1     *SP(16),A14       ; [A_D1] 
	.dwcfi	restore_reg, 14

           LDD     .D1     *SP(32),A12       ; [A_D1] 
||         LDD     .D2     *SP(24),A13       ; [A_D2] 
	.dwcfi	restore_reg, 12
	.dwcfi	restore_reg, 13

           LDD     .D1     *SP(48),A10       ; [A_D1] 
||         LDD     .D2     *SP(40),A11       ; [A_D2] 
	.dwcfi	restore_reg, 10
	.dwcfi	restore_reg, 11

           LDD     .D1     *SP(64),A8        ; [A_D1] 
||         LDD     .D2     *SP(56),A9        ; [A_D2] 

	.dwcfi	restore_reg, 8
	.dwcfi	restore_reg, 9
$C$DW$26	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$26, DW_AT_low_pc(0x00)
	.dwattr $C$DW$26, DW_AT_TI_return


           RET     .B1     ; [A_B] 
||         ADDD    .D1     SP,0x38,SP        ; [A_D1] 

	.dwcfi	cfa_offset, 0
           ; RETURN OCCURS {RP}              ; [] 
	.dwattr $C$DW$1, DW_AT_TI_end_file("tidsp/tidl_custom_maxpool_ixX_oxX_cn.c")
	.dwattr $C$DW$1, DW_AT_TI_end_line(0xa8)
	.dwattr $C$DW$1, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$1

	.sect	".text:_Z34TIDL_customMaxPool_ixX_oxX_exec_cnPvPKvS_"
	.clink
	.global	||_Z34TIDL_customMaxPool_ixX_oxX_exec_cnPvPKvS_||

$C$DW$27	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$27, DW_AT_name("TIDL_customMaxPool_ixX_oxX_exec_cn")
	.dwattr $C$DW$27, DW_AT_low_pc(||_Z34TIDL_customMaxPool_ixX_oxX_exec_cnPvPKvS_||)
	.dwattr $C$DW$27, DW_AT_high_pc(0x00)
	.dwattr $C$DW$27, DW_AT_linkage_name("_Z34TIDL_customMaxPool_ixX_oxX_exec_cnPvPKvS_")
	.dwattr $C$DW$27, DW_AT_external
	.dwattr $C$DW$27, DW_AT_decl_file("tidsp/tidl_custom_maxpool_ixX_oxX_cn.c")
	.dwattr $C$DW$27, DW_AT_decl_line(0x3d)
	.dwattr $C$DW$27, DW_AT_decl_column(0x09)
	.dwattr $C$DW$27, DW_AT_TI_max_frame_size(0x10)
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_cn.c",line 64,column 1,is_stmt,address ||_Z34TIDL_customMaxPool_ixX_oxX_exec_cnPvPKvS_||,isa 0

	.dwfde $C$DW$CIE, ||_Z34TIDL_customMaxPool_ixX_oxX_exec_cnPvPKvS_||
$C$DW$28	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$28, DW_AT_name("handle")
	.dwattr $C$DW$28, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$28, DW_AT_location[DW_OP_reg4]

$C$DW$29	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$29, DW_AT_name("src")
	.dwattr $C$DW$29, DW_AT_type(*$C$DW$T$64)
	.dwattr $C$DW$29, DW_AT_location[DW_OP_reg5]

$C$DW$30	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$30, DW_AT_name("dst")
	.dwattr $C$DW$30, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$30, DW_AT_location[DW_OP_reg6]


;******************************************************************************
;* FUNCTION NAME: TIDL_customMaxPool_ixX_oxX_exec_cn(void *, const void *, void *)*
;*                                                                            *
;*   Regs Modified     : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,VB0,VB1,VB2,VB3,AL0,   *
;*                           AL1,AL2,AM0,AM1,AM2,AM3,AM4,AM5,D0,D1,D2,D3,D4,  *
;*                           D5,D6,D14,SP,VBL0,VBL1                           *
;*   Regs Used         : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,VB0,VB1,VB2,VB3,AL0,   *
;*                           AL1,AL2,AM0,AM1,AM2,AM3,AM4,AM5,D0,D1,D2,D3,D4,  *
;*                           D5,D6,D14,SP,VBL0,VBL1                           *
;*   Local Frame Size  : 0 Args + 0 Auto + 16 Save = 16 byte                  *
;******************************************************************************
||_Z34TIDL_customMaxPool_ixX_oxX_exec_cnPvPKvS_||:
;** --------------------------------------------------------------------------*
;* D0    assigned to dst
$C$DW$31	.dwtag  DW_TAG_variable
	.dwattr $C$DW$31, DW_AT_name("dst")
	.dwattr $C$DW$31, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$31, DW_AT_location[DW_OP_regx 0x60]

;* A5    assigned to src
$C$DW$32	.dwtag  DW_TAG_variable
	.dwattr $C$DW$32, DW_AT_name("src")
	.dwattr $C$DW$32, DW_AT_type(*$C$DW$T$64)
	.dwattr $C$DW$32, DW_AT_location[DW_OP_reg5]

;* D1    assigned to handle
$C$DW$33	.dwtag  DW_TAG_variable
	.dwattr $C$DW$33, DW_AT_name("handle")
	.dwattr $C$DW$33, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$33, DW_AT_location[DW_OP_regx 0x61]

	.dwcfi	cfa_offset, 0
;          EXCLUSIVE CPU CYCLES: 12
           MV      .D1     A4,D1             ; [A_D1] |64| 
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_cn.c",line 83,column 3,is_stmt,isa 0

           LDW     .D1     *D1(16),BL0       ; [A_D1] |83| 
||         LDW     .D2     *D1(20),BL1       ; [A_D2] |83| 

           CMPEQW  .L2     BL0,0x2,BL0       ; [B_L2] |83| 
||         CMPEQW  .S2     BL1,0x2,BL1       ; [B_S2] |83| 

           ANDW    .L2     BL1,BL0,B0        ; [B_L2] |83| 
           CMPEQW  .L1X    B0,0,A0           ; [A_L1] |83| 
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_cn.c",line 64,column 1,is_stmt,isa 0

   [ A0]   B       .B1     ||$C$L12||        ; [A_B] |83| 
||         MVC     .S1     RP,A9             ; [A_S1] 
||         MV      .L1     A6,D0             ; [A_L1] |64| 
||         STD     .D1     A8,*SP(8)         ; [A_D1] 
||         STD     .D2X    A9,*SP++(-16)     ; [A_D2] 

	.dwcfi	save_reg_to_reg, 4101, 9
	.dwcfi	save_reg_to_mem, 8, 8
	.dwcfi	cfa_offset, 16
	.dwcfi	save_reg_to_mem, 9, -16
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_cn.c",line 83,column 3,is_stmt,isa 0
           ; BRANCHCC OCCURS {||$C$L12||}    ; [] |83| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 11
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_cn.c",line 85,column 5,is_stmt,isa 0

           LDW     .D1     *D1(8),BL0        ; [A_D1] |85| 
||         LDW     .D2     *D1(12),BL1       ; [A_D2] |85| 

           CMPEQW  .L2     BL0,0x3,BL0       ; [B_L2] |85| 
||         CMPEQW  .S2     BL1,0x3,BL1       ; [B_S2] |85| 

           ANDW    .L2     BL1,BL0,B0        ; [B_L2] |85| 

           CMPEQW  .L1X    B0,0,A0           ; [A_L1] |85| 
||         ADDD    .D1     D1,0x120,A7       ; [A_D1] |99| 

   [ A0]   B       .B1     ||$C$L12||        ; [A_B] |85| 
||         MV      .D1     D0,A5             ; [A_D1] |99| 
||         ADDD    .D2     D1,0x108,A6       ; [A_D2] |99| 
||         MV      .L1     A5,A4             ; [A_L1] |99| 

           ; BRANCHCC OCCURS {||$C$L12||}    ; [] |85| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 7
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_cn.c",line 99,column 3,is_stmt,isa 0
           LDW     .D1     *D1(296),A8       ; [A_D1] |99| 
$C$DW$34	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$34, DW_AT_low_pc(0x00)
	.dwattr $C$DW$34, DW_AT_name("_ZN54_INTERNAL_32_tidl_custom_maxpool_ixX_oxX_cn_c_fd8ecb3d39TIDL_customMaxPooling_3x3_skip2_exec_cnEPKhPhP18TIDL_bufParams3D_tS4_i")
	.dwattr $C$DW$34, DW_AT_TI_call

           CALL    .B1     ||_ZN54_INTERNAL_32_tidl_custom_maxpool_ixX_oxX_cn_c_fd8ecb3d39TIDL_customMaxPooling_3x3_skip2_exec_cnEPKhPhP18TIDL_bufParams3D_tS4_i|| ; [A_B] |99| 
$C$RL0:    ; CALL OCCURS (||_ZN54_INTERNAL_32_tidl_custom_maxpool_ixX_oxX_cn_c_fd8ecb3d39TIDL_customMaxPooling_3x3_skip2_exec_cnEPKhPhP18TIDL_bufParams3D_tS4_i||) arg:{A4,A5,A6,A7,A8} ret:{A4}  ; [] |99| 
           B       .B1     ||$C$L13||        ; [A_B] |99| 
           ; BRANCH OCCURS {||$C$L13||}      ; [] |99| 
;** --------------------------------------------------------------------------*
||$C$L12||:    
;          EXCLUSIVE CPU CYCLES: 1
           MVK32   .L1     0xffffffff,A4     ; [A_L1] |99| 
;** --------------------------------------------------------------------------*
||$C$L13||:    
;          EXCLUSIVE CPU CYCLES: 7
           MVC     .S1     A9,RP             ; [A_S1] 
	.dwcfi	restore_reg, 4101

           LDD     .D1     *SP(16),A9        ; [A_D1] 
||         LDD     .D2     *SP(24),A8        ; [A_D2] 

	.dwcfi	restore_reg, 9
	.dwcfi	restore_reg, 8
$C$DW$35	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$35, DW_AT_low_pc(0x00)
	.dwattr $C$DW$35, DW_AT_TI_return


           RET     .B1     ; [A_B] 
||         ADDD    .D1     SP,0x10,SP        ; [A_D1] 

	.dwcfi	cfa_offset, 0
           ; RETURN OCCURS {RP}              ; [] 
	.dwattr $C$DW$27, DW_AT_TI_end_file("tidsp/tidl_custom_maxpool_ixX_oxX_cn.c")
	.dwattr $C$DW$27, DW_AT_TI_end_line(0x64)
	.dwattr $C$DW$27, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$27

	.dwattr $C$DW$CU, DW_AT_language(DW_LANG_C_plus_plus)

;***************************************************************
;* DWARF CIE ENTRIES                                           *
;***************************************************************

$C$DW$CIE	.dwcie 4101
	.dwcfi	cfa_register, 111
	.dwcfi	cfa_offset, 0
	.dwcfi	same_value, 8
	.dwcfi	same_value, 9
	.dwcfi	same_value, 10
	.dwcfi	same_value, 11
	.dwcfi	same_value, 12
	.dwcfi	same_value, 13
	.dwcfi	same_value, 14
	.dwcfi	same_value, 15
	.dwcfi	same_value, 62
	.dwcfi	same_value, 63
	.dwendentry
	.dwendtag $C$DW$CU


;******************************************************************************
;* TYPE INFORMATION                                                           *
;******************************************************************************

$C$DW$TU$33	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$33

$C$DW$T$33	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$33, DW_AT_name("TIDL_CustomMaxPoolBuffParamsC7x")
	.dwattr $C$DW$T$33, DW_AT_byte_size(0xd0)
$C$DW$36	.dwtag  DW_TAG_member
	.dwattr $C$DW$36, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$36, DW_AT_name("seTemplate1")
	.dwattr $C$DW$36, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$36, DW_AT_decl_file("./tidsp/tidl_custom_maxpool_ixX_oxX_priv.h")
	.dwattr $C$DW$36, DW_AT_decl_line(0x3b)
	.dwattr $C$DW$36, DW_AT_decl_column(0x0c)

$C$DW$37	.dwtag  DW_TAG_member
	.dwattr $C$DW$37, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$37, DW_AT_name("seTemplate2")
	.dwattr $C$DW$37, DW_AT_data_member_location[DW_OP_plus_uconst 0x40]
	.dwattr $C$DW$37, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$37, DW_AT_decl_file("./tidsp/tidl_custom_maxpool_ixX_oxX_priv.h")
	.dwattr $C$DW$37, DW_AT_decl_line(0x3c)
	.dwattr $C$DW$37, DW_AT_decl_column(0x0c)

$C$DW$38	.dwtag  DW_TAG_member
	.dwattr $C$DW$38, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$38, DW_AT_name("saTemplate")
	.dwattr $C$DW$38, DW_AT_data_member_location[DW_OP_plus_uconst 0x80]
	.dwattr $C$DW$38, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$38, DW_AT_decl_file("./tidsp/tidl_custom_maxpool_ixX_oxX_priv.h")
	.dwattr $C$DW$38, DW_AT_decl_line(0x3d)
	.dwattr $C$DW$38, DW_AT_decl_column(0x0c)

$C$DW$39	.dwtag  DW_TAG_member
	.dwattr $C$DW$39, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$39, DW_AT_name("numCh")
	.dwattr $C$DW$39, DW_AT_data_member_location[DW_OP_plus_uconst 0xc0]
	.dwattr $C$DW$39, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$39, DW_AT_decl_file("./tidsp/tidl_custom_maxpool_ixX_oxX_priv.h")
	.dwattr $C$DW$39, DW_AT_decl_line(0x3e)
	.dwattr $C$DW$39, DW_AT_decl_column(0x0c)

$C$DW$40	.dwtag  DW_TAG_member
	.dwattr $C$DW$40, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$40, DW_AT_name("numLines")
	.dwattr $C$DW$40, DW_AT_data_member_location[DW_OP_plus_uconst 0xc4]
	.dwattr $C$DW$40, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$40, DW_AT_decl_file("./tidsp/tidl_custom_maxpool_ixX_oxX_priv.h")
	.dwattr $C$DW$40, DW_AT_decl_line(0x3f)
	.dwattr $C$DW$40, DW_AT_decl_column(0x0c)

$C$DW$41	.dwtag  DW_TAG_member
	.dwattr $C$DW$41, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$41, DW_AT_name("numTiles")
	.dwattr $C$DW$41, DW_AT_data_member_location[DW_OP_plus_uconst 0xc8]
	.dwattr $C$DW$41, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$41, DW_AT_decl_file("./tidsp/tidl_custom_maxpool_ixX_oxX_priv.h")
	.dwattr $C$DW$41, DW_AT_decl_line(0x40)
	.dwattr $C$DW$41, DW_AT_decl_column(0x0c)

$C$DW$42	.dwtag  DW_TAG_member
	.dwattr $C$DW$42, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$42, DW_AT_name("inStride")
	.dwattr $C$DW$42, DW_AT_data_member_location[DW_OP_plus_uconst 0xcc]
	.dwattr $C$DW$42, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$42, DW_AT_decl_file("./tidsp/tidl_custom_maxpool_ixX_oxX_priv.h")
	.dwattr $C$DW$42, DW_AT_decl_line(0x41)
	.dwattr $C$DW$42, DW_AT_decl_column(0x0c)


$C$DW$43	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$43, DW_AT_name("operator =")
	.dwattr $C$DW$43, DW_AT_declaration
	.dwattr $C$DW$43, DW_AT_linkage_name("_ZN31TIDL_CustomMaxPoolBuffParamsC7xaSERKS_")
	.dwattr $C$DW$43, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$43, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$44	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$44, DW_AT_type(*$C$DW$T$30)

	.dwendtag $C$DW$43


$C$DW$45	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$45, DW_AT_name("operator =")
	.dwattr $C$DW$45, DW_AT_declaration
	.dwattr $C$DW$45, DW_AT_linkage_name("_ZN31TIDL_CustomMaxPoolBuffParamsC7xaSEOS_")
	.dwattr $C$DW$45, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$45, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$46	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$46, DW_AT_type(*$C$DW$T$28)

	.dwendtag $C$DW$45

	.dwattr $C$DW$T$33, DW_AT_decl_file("./tidsp/tidl_custom_maxpool_ixX_oxX_priv.h")
	.dwattr $C$DW$T$33, DW_AT_decl_line(0x3a)
	.dwattr $C$DW$T$33, DW_AT_decl_column(0x01)
	.dwendtag $C$DW$T$33

	.dwendtag $C$DW$TU$33


$C$DW$TU$29	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$29
$C$DW$T$29	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$29, DW_AT_type(*$C$DW$T$33)

	.dwendtag $C$DW$TU$29


$C$DW$TU$30	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$30
$C$DW$T$30	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$30, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$T$30, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$30


$C$DW$TU$41	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$41
$C$DW$T$41	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$41, DW_AT_name("TIDL_CustomMaxPoolBuffParamsC7x")
	.dwattr $C$DW$T$41, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$T$41, DW_AT_decl_file("./tidsp/tidl_custom_maxpool_ixX_oxX_priv.h")
	.dwattr $C$DW$T$41, DW_AT_decl_line(0x42)
	.dwattr $C$DW$T$41, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$41


$C$DW$TU$28	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$28
$C$DW$T$28	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$28, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$T$28, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$28


$C$DW$TU$31	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$31

$C$DW$T$31	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$31, DW_AT_type(*$C$DW$T$28)
$C$DW$47	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$47, DW_AT_type(*$C$DW$T$30)

	.dwendtag $C$DW$T$31

	.dwendtag $C$DW$TU$31


$C$DW$TU$32	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$32

$C$DW$T$32	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$32, DW_AT_type(*$C$DW$T$28)
$C$DW$48	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$48, DW_AT_type(*$C$DW$T$28)

	.dwendtag $C$DW$T$32

	.dwendtag $C$DW$TU$32


$C$DW$TU$40	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$40

$C$DW$T$40	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$40, DW_AT_name("TIDL_CustomMaxPoolBuffParamsNatC")
	.dwattr $C$DW$T$40, DW_AT_byte_size(0x30)
$C$DW$49	.dwtag  DW_TAG_member
	.dwattr $C$DW$49, DW_AT_type(*$C$DW$T$34)
	.dwattr $C$DW$49, DW_AT_name("srcBuf3D")
	.dwattr $C$DW$49, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$49, DW_AT_decl_file("./tidsp/tidl_custom_maxpool_ixX_oxX_priv.h")
	.dwattr $C$DW$49, DW_AT_decl_line(0x46)
	.dwattr $C$DW$49, DW_AT_decl_column(0x16)

$C$DW$50	.dwtag  DW_TAG_member
	.dwattr $C$DW$50, DW_AT_type(*$C$DW$T$34)
	.dwattr $C$DW$50, DW_AT_name("dstBuf3D")
	.dwattr $C$DW$50, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$50, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$50, DW_AT_decl_file("./tidsp/tidl_custom_maxpool_ixX_oxX_priv.h")
	.dwattr $C$DW$50, DW_AT_decl_line(0x47)
	.dwattr $C$DW$50, DW_AT_decl_column(0x16)


$C$DW$51	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$51, DW_AT_name("operator =")
	.dwattr $C$DW$51, DW_AT_declaration
	.dwattr $C$DW$51, DW_AT_linkage_name("_ZN32TIDL_CustomMaxPoolBuffParamsNatCaSERKS_")
	.dwattr $C$DW$51, DW_AT_type(*$C$DW$T$35)
	.dwattr $C$DW$51, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$52	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$52, DW_AT_type(*$C$DW$T$37)

	.dwendtag $C$DW$51


$C$DW$53	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$53, DW_AT_name("operator =")
	.dwattr $C$DW$53, DW_AT_declaration
	.dwattr $C$DW$53, DW_AT_linkage_name("_ZN32TIDL_CustomMaxPoolBuffParamsNatCaSEOS_")
	.dwattr $C$DW$53, DW_AT_type(*$C$DW$T$35)
	.dwattr $C$DW$53, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$54	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$54, DW_AT_type(*$C$DW$T$35)

	.dwendtag $C$DW$53

	.dwattr $C$DW$T$40, DW_AT_decl_file("./tidsp/tidl_custom_maxpool_ixX_oxX_priv.h")
	.dwattr $C$DW$T$40, DW_AT_decl_line(0x45)
	.dwattr $C$DW$T$40, DW_AT_decl_column(0x01)
	.dwendtag $C$DW$T$40

	.dwendtag $C$DW$TU$40


$C$DW$TU$36	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$36
$C$DW$T$36	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$36, DW_AT_type(*$C$DW$T$40)

	.dwendtag $C$DW$TU$36


$C$DW$TU$37	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$37
$C$DW$T$37	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$37, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$T$37, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$37


$C$DW$TU$42	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$42
$C$DW$T$42	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$42, DW_AT_name("TIDL_CustomMaxPoolBuffParamsNatC")
	.dwattr $C$DW$T$42, DW_AT_type(*$C$DW$T$40)
	.dwattr $C$DW$T$42, DW_AT_decl_file("./tidsp/tidl_custom_maxpool_ixX_oxX_priv.h")
	.dwattr $C$DW$T$42, DW_AT_decl_line(0x48)
	.dwattr $C$DW$T$42, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$42


$C$DW$TU$35	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$35
$C$DW$T$35	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$35, DW_AT_type(*$C$DW$T$40)
	.dwattr $C$DW$T$35, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$35


$C$DW$TU$38	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$38

$C$DW$T$38	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$38, DW_AT_type(*$C$DW$T$35)
$C$DW$55	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$55, DW_AT_type(*$C$DW$T$37)

	.dwendtag $C$DW$T$38

	.dwendtag $C$DW$TU$38


$C$DW$TU$39	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$39

$C$DW$T$39	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$39, DW_AT_type(*$C$DW$T$35)
$C$DW$56	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$56, DW_AT_type(*$C$DW$T$35)

	.dwendtag $C$DW$T$39

	.dwendtag $C$DW$TU$39


$C$DW$TU$48	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$48

$C$DW$T$48	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$48, DW_AT_name("TIDL_CustomMaxPoolIxXOxXBuffParams")
	.dwattr $C$DW$T$48, DW_AT_byte_size(0x100)
$C$DW$57	.dwtag  DW_TAG_member
	.dwattr $C$DW$57, DW_AT_type(*$C$DW$T$41)
	.dwattr $C$DW$57, DW_AT_name("c7x")
	.dwattr $C$DW$57, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$57, DW_AT_decl_file("./tidsp/tidl_custom_maxpool_ixX_oxX_priv.h")
	.dwattr $C$DW$57, DW_AT_decl_line(0x4c)
	.dwattr $C$DW$57, DW_AT_decl_column(0x24)

$C$DW$58	.dwtag  DW_TAG_member
	.dwattr $C$DW$58, DW_AT_type(*$C$DW$T$42)
	.dwattr $C$DW$58, DW_AT_name("natc")
	.dwattr $C$DW$58, DW_AT_data_member_location[DW_OP_plus_uconst 0xd0]
	.dwattr $C$DW$58, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$58, DW_AT_decl_file("./tidsp/tidl_custom_maxpool_ixX_oxX_priv.h")
	.dwattr $C$DW$58, DW_AT_decl_line(0x4d)
	.dwattr $C$DW$58, DW_AT_decl_column(0x24)


$C$DW$59	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$59, DW_AT_name("operator =")
	.dwattr $C$DW$59, DW_AT_declaration
	.dwattr $C$DW$59, DW_AT_linkage_name("_ZN34TIDL_CustomMaxPoolIxXOxXBuffParamsaSERKS_")
	.dwattr $C$DW$59, DW_AT_type(*$C$DW$T$43)
	.dwattr $C$DW$59, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$60	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$60, DW_AT_type(*$C$DW$T$45)

	.dwendtag $C$DW$59


$C$DW$61	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$61, DW_AT_name("operator =")
	.dwattr $C$DW$61, DW_AT_declaration
	.dwattr $C$DW$61, DW_AT_linkage_name("_ZN34TIDL_CustomMaxPoolIxXOxXBuffParamsaSEOS_")
	.dwattr $C$DW$61, DW_AT_type(*$C$DW$T$43)
	.dwattr $C$DW$61, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$62	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$62, DW_AT_type(*$C$DW$T$43)

	.dwendtag $C$DW$61

	.dwattr $C$DW$T$48, DW_AT_decl_file("./tidsp/tidl_custom_maxpool_ixX_oxX_priv.h")
	.dwattr $C$DW$T$48, DW_AT_decl_line(0x4b)
	.dwattr $C$DW$T$48, DW_AT_decl_column(0x01)
	.dwendtag $C$DW$T$48

	.dwendtag $C$DW$TU$48


$C$DW$TU$44	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$44
$C$DW$T$44	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$44, DW_AT_type(*$C$DW$T$48)

	.dwendtag $C$DW$TU$44


$C$DW$TU$45	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$45
$C$DW$T$45	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$45, DW_AT_type(*$C$DW$T$44)
	.dwattr $C$DW$T$45, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$45


$C$DW$TU$68	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$68
$C$DW$T$68	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$68, DW_AT_name("TIDL_CustomMaxPoolIxXOxXBuffParams")
	.dwattr $C$DW$T$68, DW_AT_type(*$C$DW$T$48)
	.dwattr $C$DW$T$68, DW_AT_decl_file("./tidsp/tidl_custom_maxpool_ixX_oxX_priv.h")
	.dwattr $C$DW$T$68, DW_AT_decl_line(0x4e)
	.dwattr $C$DW$T$68, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$68


$C$DW$TU$43	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$43
$C$DW$T$43	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$43, DW_AT_type(*$C$DW$T$48)
	.dwattr $C$DW$T$43, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$43


$C$DW$TU$46	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$46

$C$DW$T$46	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$46, DW_AT_type(*$C$DW$T$43)
$C$DW$63	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$63, DW_AT_type(*$C$DW$T$45)

	.dwendtag $C$DW$T$46

	.dwendtag $C$DW$TU$46


$C$DW$TU$47	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$47

$C$DW$T$47	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$47, DW_AT_type(*$C$DW$T$43)
$C$DW$64	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$64, DW_AT_type(*$C$DW$T$43)

	.dwendtag $C$DW$T$47

	.dwendtag $C$DW$TU$47


$C$DW$TU$57	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$57

$C$DW$T$57	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$57, DW_AT_name("TIDL_CustomMaxPoolIxXOxXInitArgs")
	.dwattr $C$DW$T$57, DW_AT_byte_size(0x2c)
$C$DW$65	.dwtag  DW_TAG_member
	.dwattr $C$DW$65, DW_AT_type(*$C$DW$T$50)
	.dwattr $C$DW$65, DW_AT_name("funcStyle")
	.dwattr $C$DW$65, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$65, DW_AT_decl_file("./tidsp/tidl_custom_maxpool_ixX_oxX.h")
	.dwattr $C$DW$65, DW_AT_decl_line(0x33)
	.dwattr $C$DW$65, DW_AT_decl_column(0x0c)

$C$DW$66	.dwtag  DW_TAG_member
	.dwattr $C$DW$66, DW_AT_type(*$C$DW$T$51)
	.dwattr $C$DW$66, DW_AT_name("customMaxPoolParam")
	.dwattr $C$DW$66, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$66, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$66, DW_AT_decl_file("./tidsp/tidl_custom_maxpool_ixX_oxX.h")
	.dwattr $C$DW$66, DW_AT_decl_line(0x34)
	.dwattr $C$DW$66, DW_AT_decl_column(0x18)


$C$DW$67	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$67, DW_AT_name("operator =")
	.dwattr $C$DW$67, DW_AT_declaration
	.dwattr $C$DW$67, DW_AT_linkage_name("_ZN32TIDL_CustomMaxPoolIxXOxXInitArgsaSERKS_")
	.dwattr $C$DW$67, DW_AT_type(*$C$DW$T$52)
	.dwattr $C$DW$67, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$68	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$68, DW_AT_type(*$C$DW$T$54)

	.dwendtag $C$DW$67


$C$DW$69	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$69, DW_AT_name("operator =")
	.dwattr $C$DW$69, DW_AT_declaration
	.dwattr $C$DW$69, DW_AT_linkage_name("_ZN32TIDL_CustomMaxPoolIxXOxXInitArgsaSEOS_")
	.dwattr $C$DW$69, DW_AT_type(*$C$DW$T$52)
	.dwattr $C$DW$69, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$70	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$70, DW_AT_type(*$C$DW$T$52)

	.dwendtag $C$DW$69

	.dwattr $C$DW$T$57, DW_AT_decl_file("./tidsp/tidl_custom_maxpool_ixX_oxX.h")
	.dwattr $C$DW$T$57, DW_AT_decl_line(0x32)
	.dwattr $C$DW$T$57, DW_AT_decl_column(0x01)
	.dwendtag $C$DW$T$57

	.dwendtag $C$DW$TU$57


$C$DW$TU$53	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$53
$C$DW$T$53	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$53, DW_AT_type(*$C$DW$T$57)

	.dwendtag $C$DW$TU$53


$C$DW$TU$54	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$54
$C$DW$T$54	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$54, DW_AT_type(*$C$DW$T$53)
	.dwattr $C$DW$T$54, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$54


$C$DW$TU$62	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$62
$C$DW$T$62	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$62, DW_AT_name("TIDL_CustomMaxPoolIxXOxXInitArgs")
	.dwattr $C$DW$T$62, DW_AT_type(*$C$DW$T$57)
	.dwattr $C$DW$T$62, DW_AT_decl_file("./tidsp/tidl_custom_maxpool_ixX_oxX.h")
	.dwattr $C$DW$T$62, DW_AT_decl_line(0x35)
	.dwattr $C$DW$T$62, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$62


$C$DW$TU$52	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$52
$C$DW$T$52	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$52, DW_AT_type(*$C$DW$T$57)
	.dwattr $C$DW$T$52, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$52


$C$DW$TU$55	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$55

$C$DW$T$55	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$55, DW_AT_type(*$C$DW$T$52)
$C$DW$71	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$71, DW_AT_type(*$C$DW$T$54)

	.dwendtag $C$DW$T$55

	.dwendtag $C$DW$TU$55


$C$DW$TU$56	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$56

$C$DW$T$56	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$56, DW_AT_type(*$C$DW$T$52)
$C$DW$72	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$72, DW_AT_type(*$C$DW$T$52)

	.dwendtag $C$DW$T$56

	.dwendtag $C$DW$TU$56


$C$DW$TU$74	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$74

$C$DW$T$74	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$74, DW_AT_name("TIDL_CustomMaxPoolIxXOxXPrivArgs")
	.dwattr $C$DW$T$74, DW_AT_byte_size(0x140)
$C$DW$73	.dwtag  DW_TAG_member
	.dwattr $C$DW$73, DW_AT_type(*$C$DW$T$62)
	.dwattr $C$DW$73, DW_AT_name("initArgs")
	.dwattr $C$DW$73, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$73, DW_AT_decl_file("./tidsp/tidl_custom_maxpool_ixX_oxX_priv.h")
	.dwattr $C$DW$73, DW_AT_decl_line(0x52)
	.dwattr $C$DW$73, DW_AT_decl_column(0x24)

$C$DW$74	.dwtag  DW_TAG_member
	.dwattr $C$DW$74, DW_AT_type(*$C$DW$T$67)
	.dwattr $C$DW$74, DW_AT_name("execute")
	.dwattr $C$DW$74, DW_AT_data_member_location[DW_OP_plus_uconst 0x30]
	.dwattr $C$DW$74, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$74, DW_AT_decl_file("./tidsp/tidl_custom_maxpool_ixX_oxX_priv.h")
	.dwattr $C$DW$74, DW_AT_decl_line(0x53)
	.dwattr $C$DW$74, DW_AT_decl_column(0x1e)

$C$DW$75	.dwtag  DW_TAG_member
	.dwattr $C$DW$75, DW_AT_type(*$C$DW$T$68)
	.dwattr $C$DW$75, DW_AT_name("bufParams")
	.dwattr $C$DW$75, DW_AT_data_member_location[DW_OP_plus_uconst 0x38]
	.dwattr $C$DW$75, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$75, DW_AT_decl_file("./tidsp/tidl_custom_maxpool_ixX_oxX_priv.h")
	.dwattr $C$DW$75, DW_AT_decl_line(0x54)
	.dwattr $C$DW$75, DW_AT_decl_column(0x26)

$C$DW$76	.dwtag  DW_TAG_member
	.dwattr $C$DW$76, DW_AT_type(*$C$DW$T$50)
	.dwattr $C$DW$76, DW_AT_name("kernelHandleSize")
	.dwattr $C$DW$76, DW_AT_data_member_location[DW_OP_plus_uconst 0x138]
	.dwattr $C$DW$76, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$76, DW_AT_decl_file("./tidsp/tidl_custom_maxpool_ixX_oxX_priv.h")
	.dwattr $C$DW$76, DW_AT_decl_line(0x55)
	.dwattr $C$DW$76, DW_AT_decl_column(0x0b)


$C$DW$77	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$77, DW_AT_name("operator =")
	.dwattr $C$DW$77, DW_AT_declaration
	.dwattr $C$DW$77, DW_AT_linkage_name("_ZN32TIDL_CustomMaxPoolIxXOxXPrivArgsaSERKS_")
	.dwattr $C$DW$77, DW_AT_type(*$C$DW$T$69)
	.dwattr $C$DW$77, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$78	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$78, DW_AT_type(*$C$DW$T$71)

	.dwendtag $C$DW$77


$C$DW$79	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$79, DW_AT_name("operator =")
	.dwattr $C$DW$79, DW_AT_declaration
	.dwattr $C$DW$79, DW_AT_linkage_name("_ZN32TIDL_CustomMaxPoolIxXOxXPrivArgsaSEOS_")
	.dwattr $C$DW$79, DW_AT_type(*$C$DW$T$69)
	.dwattr $C$DW$79, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$80	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$80, DW_AT_type(*$C$DW$T$69)

	.dwendtag $C$DW$79

	.dwattr $C$DW$T$74, DW_AT_decl_file("./tidsp/tidl_custom_maxpool_ixX_oxX_priv.h")
	.dwattr $C$DW$T$74, DW_AT_decl_line(0x51)
	.dwattr $C$DW$T$74, DW_AT_decl_column(0x01)
	.dwendtag $C$DW$T$74

	.dwendtag $C$DW$TU$74


$C$DW$TU$70	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$70
$C$DW$T$70	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$70, DW_AT_type(*$C$DW$T$74)

	.dwendtag $C$DW$TU$70


$C$DW$TU$71	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$71
$C$DW$T$71	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$71, DW_AT_type(*$C$DW$T$70)
	.dwattr $C$DW$T$71, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$71


$C$DW$TU$92	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$92
$C$DW$T$92	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$92, DW_AT_name("TIDL_CustomMaxPoolIxXOxXPrivArgs")
	.dwattr $C$DW$T$92, DW_AT_type(*$C$DW$T$74)
	.dwattr $C$DW$T$92, DW_AT_decl_file("./tidsp/tidl_custom_maxpool_ixX_oxX_priv.h")
	.dwattr $C$DW$T$92, DW_AT_decl_line(0x56)
	.dwattr $C$DW$T$92, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$92


$C$DW$TU$69	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$69
$C$DW$T$69	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$69, DW_AT_type(*$C$DW$T$74)
	.dwattr $C$DW$T$69, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$69


$C$DW$TU$72	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$72

$C$DW$T$72	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$72, DW_AT_type(*$C$DW$T$69)
$C$DW$81	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$81, DW_AT_type(*$C$DW$T$71)

	.dwendtag $C$DW$T$72

	.dwendtag $C$DW$TU$72


$C$DW$TU$73	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$73

$C$DW$T$73	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$73, DW_AT_type(*$C$DW$T$69)
$C$DW$82	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$82, DW_AT_type(*$C$DW$T$69)

	.dwendtag $C$DW$T$73

	.dwendtag $C$DW$TU$73


$C$DW$TU$80	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$80

$C$DW$T$80	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$80, DW_AT_name("TIDL_CustomParams0_t")
	.dwattr $C$DW$T$80, DW_AT_byte_size(0x28)
$C$DW$83	.dwtag  DW_TAG_member
	.dwattr $C$DW$83, DW_AT_type(*$C$DW$T$50)
	.dwattr $C$DW$83, DW_AT_name("poolingType")
	.dwattr $C$DW$83, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$83, DW_AT_decl_file("./tidl_custom.h")
	.dwattr $C$DW$83, DW_AT_decl_line(0x70)
	.dwattr $C$DW$83, DW_AT_decl_column(0x0d)

$C$DW$84	.dwtag  DW_TAG_member
	.dwattr $C$DW$84, DW_AT_type(*$C$DW$T$50)
	.dwattr $C$DW$84, DW_AT_name("kernelW")
	.dwattr $C$DW$84, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$84, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$84, DW_AT_decl_file("./tidl_custom.h")
	.dwattr $C$DW$84, DW_AT_decl_line(0x72)
	.dwattr $C$DW$84, DW_AT_decl_column(0x0d)

$C$DW$85	.dwtag  DW_TAG_member
	.dwattr $C$DW$85, DW_AT_type(*$C$DW$T$50)
	.dwattr $C$DW$85, DW_AT_name("kernelH")
	.dwattr $C$DW$85, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$85, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$85, DW_AT_decl_file("./tidl_custom.h")
	.dwattr $C$DW$85, DW_AT_decl_line(0x74)
	.dwattr $C$DW$85, DW_AT_decl_column(0x0d)

$C$DW$86	.dwtag  DW_TAG_member
	.dwattr $C$DW$86, DW_AT_type(*$C$DW$T$50)
	.dwattr $C$DW$86, DW_AT_name("strideW")
	.dwattr $C$DW$86, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$86, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$86, DW_AT_decl_file("./tidl_custom.h")
	.dwattr $C$DW$86, DW_AT_decl_line(0x76)
	.dwattr $C$DW$86, DW_AT_decl_column(0x0d)

$C$DW$87	.dwtag  DW_TAG_member
	.dwattr $C$DW$87, DW_AT_type(*$C$DW$T$50)
	.dwattr $C$DW$87, DW_AT_name("strideH")
	.dwattr $C$DW$87, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$87, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$87, DW_AT_decl_file("./tidl_custom.h")
	.dwattr $C$DW$87, DW_AT_decl_line(0x78)
	.dwattr $C$DW$87, DW_AT_decl_column(0x0d)

$C$DW$88	.dwtag  DW_TAG_member
	.dwattr $C$DW$88, DW_AT_type(*$C$DW$T$50)
	.dwattr $C$DW$88, DW_AT_name("padW")
	.dwattr $C$DW$88, DW_AT_data_member_location[DW_OP_plus_uconst 0x14]
	.dwattr $C$DW$88, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$88, DW_AT_decl_file("./tidl_custom.h")
	.dwattr $C$DW$88, DW_AT_decl_line(0x7a)
	.dwattr $C$DW$88, DW_AT_decl_column(0x0d)

$C$DW$89	.dwtag  DW_TAG_member
	.dwattr $C$DW$89, DW_AT_type(*$C$DW$T$50)
	.dwattr $C$DW$89, DW_AT_name("padH")
	.dwattr $C$DW$89, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$89, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$89, DW_AT_decl_file("./tidl_custom.h")
	.dwattr $C$DW$89, DW_AT_decl_line(0x7c)
	.dwattr $C$DW$89, DW_AT_decl_column(0x0d)

$C$DW$90	.dwtag  DW_TAG_member
	.dwattr $C$DW$90, DW_AT_type(*$C$DW$T$50)
	.dwattr $C$DW$90, DW_AT_name("inDataQ")
	.dwattr $C$DW$90, DW_AT_data_member_location[DW_OP_plus_uconst 0x1c]
	.dwattr $C$DW$90, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$90, DW_AT_decl_file("./tidl_custom.h")
	.dwattr $C$DW$90, DW_AT_decl_line(0x7e)
	.dwattr $C$DW$90, DW_AT_decl_column(0x0d)

$C$DW$91	.dwtag  DW_TAG_member
	.dwattr $C$DW$91, DW_AT_type(*$C$DW$T$50)
	.dwattr $C$DW$91, DW_AT_name("outDataQ")
	.dwattr $C$DW$91, DW_AT_data_member_location[DW_OP_plus_uconst 0x20]
	.dwattr $C$DW$91, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$91, DW_AT_decl_file("./tidl_custom.h")
	.dwattr $C$DW$91, DW_AT_decl_line(0x80)
	.dwattr $C$DW$91, DW_AT_decl_column(0x0d)

$C$DW$92	.dwtag  DW_TAG_member
	.dwattr $C$DW$92, DW_AT_type(*$C$DW$T$50)
	.dwattr $C$DW$92, DW_AT_name("useCeil")
	.dwattr $C$DW$92, DW_AT_data_member_location[DW_OP_plus_uconst 0x24]
	.dwattr $C$DW$92, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$92, DW_AT_decl_file("./tidl_custom.h")
	.dwattr $C$DW$92, DW_AT_decl_line(0x82)
	.dwattr $C$DW$92, DW_AT_decl_column(0x0d)


$C$DW$93	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$93, DW_AT_name("operator =")
	.dwattr $C$DW$93, DW_AT_declaration
	.dwattr $C$DW$93, DW_AT_linkage_name("_ZN20TIDL_CustomParams0_taSERKS_")
	.dwattr $C$DW$93, DW_AT_type(*$C$DW$T$75)
	.dwattr $C$DW$93, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$94	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$94, DW_AT_type(*$C$DW$T$77)

	.dwendtag $C$DW$93


$C$DW$95	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$95, DW_AT_name("operator =")
	.dwattr $C$DW$95, DW_AT_declaration
	.dwattr $C$DW$95, DW_AT_linkage_name("_ZN20TIDL_CustomParams0_taSEOS_")
	.dwattr $C$DW$95, DW_AT_type(*$C$DW$T$75)
	.dwattr $C$DW$95, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$96	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$96, DW_AT_type(*$C$DW$T$75)

	.dwendtag $C$DW$95

	.dwattr $C$DW$T$80, DW_AT_decl_file("./tidl_custom.h")
	.dwattr $C$DW$T$80, DW_AT_decl_line(0x6e)
	.dwattr $C$DW$T$80, DW_AT_decl_column(0x10)
	.dwendtag $C$DW$T$80

	.dwendtag $C$DW$TU$80


$C$DW$TU$51	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$51
$C$DW$T$51	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$51, DW_AT_name("TIDL_CustomParams0_t")
	.dwattr $C$DW$T$51, DW_AT_type(*$C$DW$T$80)
	.dwattr $C$DW$T$51, DW_AT_decl_file("./tidl_custom.h")
	.dwattr $C$DW$T$51, DW_AT_decl_line(0x83)
	.dwattr $C$DW$T$51, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$51


$C$DW$TU$76	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$76
$C$DW$T$76	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$76, DW_AT_type(*$C$DW$T$80)

	.dwendtag $C$DW$TU$76


$C$DW$TU$77	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$77
$C$DW$T$77	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$77, DW_AT_type(*$C$DW$T$76)
	.dwattr $C$DW$T$77, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$77


$C$DW$TU$75	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$75
$C$DW$T$75	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$75, DW_AT_type(*$C$DW$T$80)
	.dwattr $C$DW$T$75, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$75


$C$DW$TU$78	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$78

$C$DW$T$78	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$78, DW_AT_type(*$C$DW$T$75)
$C$DW$97	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$97, DW_AT_type(*$C$DW$T$77)

	.dwendtag $C$DW$T$78

	.dwendtag $C$DW$TU$78


$C$DW$TU$79	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$79

$C$DW$T$79	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$79, DW_AT_type(*$C$DW$T$75)
$C$DW$98	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$98, DW_AT_type(*$C$DW$T$75)

	.dwendtag $C$DW$T$79

	.dwendtag $C$DW$TU$79


$C$DW$TU$87	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$87

$C$DW$T$87	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$87, DW_AT_name("TIDL_bufParams3D_t")
	.dwattr $C$DW$T$87, DW_AT_byte_size(0x18)
$C$DW$99	.dwtag  DW_TAG_member
	.dwattr $C$DW$99, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$99, DW_AT_name("data_type")
	.dwattr $C$DW$99, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$99, DW_AT_decl_file("../inc/tidl_dataflow.h")
	.dwattr $C$DW$99, DW_AT_decl_line(0xc1)
	.dwattr $C$DW$99, DW_AT_decl_column(0x0c)

$C$DW$100	.dwtag  DW_TAG_member
	.dwattr $C$DW$100, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$100, DW_AT_name("dim_x")
	.dwattr $C$DW$100, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$100, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$100, DW_AT_decl_file("../inc/tidl_dataflow.h")
	.dwattr $C$DW$100, DW_AT_decl_line(0xc4)
	.dwattr $C$DW$100, DW_AT_decl_column(0x0c)

$C$DW$101	.dwtag  DW_TAG_member
	.dwattr $C$DW$101, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$101, DW_AT_name("dim_y")
	.dwattr $C$DW$101, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$101, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$101, DW_AT_decl_file("../inc/tidl_dataflow.h")
	.dwattr $C$DW$101, DW_AT_decl_line(0xc7)
	.dwattr $C$DW$101, DW_AT_decl_column(0x0c)

$C$DW$102	.dwtag  DW_TAG_member
	.dwattr $C$DW$102, DW_AT_type(*$C$DW$T$50)
	.dwattr $C$DW$102, DW_AT_name("stride_y")
	.dwattr $C$DW$102, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$102, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$102, DW_AT_decl_file("../inc/tidl_dataflow.h")
	.dwattr $C$DW$102, DW_AT_decl_line(0xc9)
	.dwattr $C$DW$102, DW_AT_decl_column(0x0c)

$C$DW$103	.dwtag  DW_TAG_member
	.dwattr $C$DW$103, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$103, DW_AT_name("dim_z")
	.dwattr $C$DW$103, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$103, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$103, DW_AT_decl_file("../inc/tidl_dataflow.h")
	.dwattr $C$DW$103, DW_AT_decl_line(0xcc)
	.dwattr $C$DW$103, DW_AT_decl_column(0x0c)

$C$DW$104	.dwtag  DW_TAG_member
	.dwattr $C$DW$104, DW_AT_type(*$C$DW$T$50)
	.dwattr $C$DW$104, DW_AT_name("stride_z")
	.dwattr $C$DW$104, DW_AT_data_member_location[DW_OP_plus_uconst 0x14]
	.dwattr $C$DW$104, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$104, DW_AT_decl_file("../inc/tidl_dataflow.h")
	.dwattr $C$DW$104, DW_AT_decl_line(0xce)
	.dwattr $C$DW$104, DW_AT_decl_column(0x0c)


$C$DW$105	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$105, DW_AT_name("operator =")
	.dwattr $C$DW$105, DW_AT_declaration
	.dwattr $C$DW$105, DW_AT_linkage_name("_ZN18TIDL_bufParams3D_taSERKS_")
	.dwattr $C$DW$105, DW_AT_type(*$C$DW$T$82)
	.dwattr $C$DW$105, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$106	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$106, DW_AT_type(*$C$DW$T$84)

	.dwendtag $C$DW$105


$C$DW$107	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$107, DW_AT_name("operator =")
	.dwattr $C$DW$107, DW_AT_declaration
	.dwattr $C$DW$107, DW_AT_linkage_name("_ZN18TIDL_bufParams3D_taSEOS_")
	.dwattr $C$DW$107, DW_AT_type(*$C$DW$T$82)
	.dwattr $C$DW$107, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$108	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$108, DW_AT_type(*$C$DW$T$82)

	.dwendtag $C$DW$107

	.dwattr $C$DW$T$87, DW_AT_decl_file("../inc/tidl_dataflow.h")
	.dwattr $C$DW$T$87, DW_AT_decl_line(0xbf)
	.dwattr $C$DW$T$87, DW_AT_decl_column(0x01)
	.dwendtag $C$DW$T$87

	.dwendtag $C$DW$TU$87


$C$DW$TU$34	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$34
$C$DW$T$34	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$34, DW_AT_name("TIDL_bufParams3D_t")
	.dwattr $C$DW$T$34, DW_AT_type(*$C$DW$T$87)
	.dwattr $C$DW$T$34, DW_AT_decl_file("../inc/tidl_dataflow.h")
	.dwattr $C$DW$T$34, DW_AT_decl_line(0xcf)
	.dwattr $C$DW$T$34, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$34


$C$DW$TU$93	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$93
$C$DW$T$93	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$93, DW_AT_type(*$C$DW$T$34)
	.dwattr $C$DW$T$93, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$93


$C$DW$TU$83	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$83
$C$DW$T$83	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$83, DW_AT_type(*$C$DW$T$87)

	.dwendtag $C$DW$TU$83


$C$DW$TU$84	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$84
$C$DW$T$84	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$84, DW_AT_type(*$C$DW$T$83)
	.dwattr $C$DW$T$84, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$84


$C$DW$TU$82	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$82
$C$DW$T$82	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$82, DW_AT_type(*$C$DW$T$87)
	.dwattr $C$DW$T$82, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$82


$C$DW$TU$85	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$85

$C$DW$T$85	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$85, DW_AT_type(*$C$DW$T$82)
$C$DW$109	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$109, DW_AT_type(*$C$DW$T$84)

	.dwendtag $C$DW$T$85

	.dwendtag $C$DW$TU$85


$C$DW$TU$86	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$86

$C$DW$T$86	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$86, DW_AT_type(*$C$DW$T$82)
$C$DW$110	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$110, DW_AT_type(*$C$DW$T$82)

	.dwendtag $C$DW$T$86

	.dwendtag $C$DW$TU$86


$C$DW$TU$2	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$2
$C$DW$T$2	.dwtag  DW_TAG_unspecified_type
	.dwattr $C$DW$T$2, DW_AT_name("void")

	.dwendtag $C$DW$TU$2


$C$DW$TU$3	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$3
$C$DW$T$3	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$3, DW_AT_type(*$C$DW$T$2)
	.dwattr $C$DW$T$3, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$3


$C$DW$TU$63	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$63
$C$DW$T$63	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$63, DW_AT_type(*$C$DW$T$2)

	.dwendtag $C$DW$TU$63


$C$DW$TU$64	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$64
$C$DW$T$64	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$64, DW_AT_type(*$C$DW$T$63)
	.dwattr $C$DW$T$64, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$64


$C$DW$TU$4	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$4
$C$DW$T$4	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$4, DW_AT_encoding(DW_ATE_boolean)
	.dwattr $C$DW$T$4, DW_AT_name("bool")
	.dwattr $C$DW$T$4, DW_AT_byte_size(0x01)

	.dwendtag $C$DW$TU$4


$C$DW$TU$5	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$5
$C$DW$T$5	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$5, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$5, DW_AT_name("signed char")
	.dwattr $C$DW$T$5, DW_AT_byte_size(0x01)

	.dwendtag $C$DW$TU$5


$C$DW$TU$6	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$6
$C$DW$T$6	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$6, DW_AT_encoding(DW_ATE_unsigned_char)
	.dwattr $C$DW$T$6, DW_AT_name("unsigned char")
	.dwattr $C$DW$T$6, DW_AT_byte_size(0x01)

	.dwendtag $C$DW$TU$6


$C$DW$TU$106	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$106
$C$DW$T$106	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$106, DW_AT_name("__uint8_t")
	.dwattr $C$DW$T$106, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$106, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/machine/_types.h")
	.dwattr $C$DW$T$106, DW_AT_decl_line(0x61)
	.dwattr $C$DW$T$106, DW_AT_decl_column(0x18)

	.dwendtag $C$DW$TU$106


$C$DW$TU$107	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$107
$C$DW$T$107	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$107, DW_AT_name("uint8_t")
	.dwattr $C$DW$T$107, DW_AT_type(*$C$DW$T$106)
	.dwattr $C$DW$T$107, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/sys/_stdint.h")
	.dwattr $C$DW$T$107, DW_AT_decl_line(0x3b)
	.dwattr $C$DW$T$107, DW_AT_decl_column(0x14)

	.dwendtag $C$DW$TU$107


$C$DW$TU$108	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$108
$C$DW$T$108	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$108, DW_AT_type(*$C$DW$T$107)
	.dwattr $C$DW$T$108, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$108


$C$DW$TU$110	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$110
$C$DW$T$110	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$110, DW_AT_type(*$C$DW$T$107)

	.dwendtag $C$DW$TU$110


$C$DW$TU$111	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$111
$C$DW$T$111	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$111, DW_AT_type(*$C$DW$T$110)
	.dwattr $C$DW$T$111, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$111


$C$DW$TU$7	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$7
$C$DW$T$7	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$7, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$7, DW_AT_name("wchar_t")
	.dwattr $C$DW$T$7, DW_AT_byte_size(0x04)

	.dwendtag $C$DW$TU$7


$C$DW$TU$8	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$8
$C$DW$T$8	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$8, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$8, DW_AT_name("short")
	.dwattr $C$DW$T$8, DW_AT_byte_size(0x02)

	.dwendtag $C$DW$TU$8


$C$DW$TU$9	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$9
$C$DW$T$9	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$9, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$9, DW_AT_name("unsigned short")
	.dwattr $C$DW$T$9, DW_AT_byte_size(0x02)

	.dwendtag $C$DW$TU$9


$C$DW$TU$114	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$114
$C$DW$T$114	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$114, DW_AT_name("__uint16_t")
	.dwattr $C$DW$T$114, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$T$114, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/machine/_types.h")
	.dwattr $C$DW$T$114, DW_AT_decl_line(0x63)
	.dwattr $C$DW$T$114, DW_AT_decl_column(0x19)

	.dwendtag $C$DW$TU$114


$C$DW$TU$115	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$115
$C$DW$T$115	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$115, DW_AT_name("uint16_t")
	.dwattr $C$DW$T$115, DW_AT_type(*$C$DW$T$114)
	.dwattr $C$DW$T$115, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/sys/_stdint.h")
	.dwattr $C$DW$T$115, DW_AT_decl_line(0x41)
	.dwattr $C$DW$T$115, DW_AT_decl_column(0x15)

	.dwendtag $C$DW$TU$115


$C$DW$TU$10	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$10
$C$DW$T$10	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$10, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$10, DW_AT_name("int")
	.dwattr $C$DW$T$10, DW_AT_byte_size(0x04)

	.dwendtag $C$DW$TU$10


$C$DW$TU$49	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$49
$C$DW$T$49	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$49, DW_AT_name("__int32_t")
	.dwattr $C$DW$T$49, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$T$49, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/machine/_types.h")
	.dwattr $C$DW$T$49, DW_AT_decl_line(0x64)
	.dwattr $C$DW$T$49, DW_AT_decl_column(0x0f)

	.dwendtag $C$DW$TU$49


$C$DW$TU$50	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$50
$C$DW$T$50	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$50, DW_AT_name("int32_t")
	.dwattr $C$DW$T$50, DW_AT_type(*$C$DW$T$49)
	.dwattr $C$DW$T$50, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/sys/_stdint.h")
	.dwattr $C$DW$T$50, DW_AT_decl_line(0x30)
	.dwattr $C$DW$T$50, DW_AT_decl_column(0x14)

	.dwendtag $C$DW$TU$50


$C$DW$TU$65	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$65

$C$DW$T$65	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$65, DW_AT_type(*$C$DW$T$50)
$C$DW$111	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$111, DW_AT_type(*$C$DW$T$3)

$C$DW$112	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$112, DW_AT_type(*$C$DW$T$64)

$C$DW$113	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$113, DW_AT_type(*$C$DW$T$3)

	.dwendtag $C$DW$T$65

	.dwendtag $C$DW$TU$65


$C$DW$TU$66	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$66
$C$DW$T$66	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$66, DW_AT_type(*$C$DW$T$65)
	.dwattr $C$DW$T$66, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$66


$C$DW$TU$67	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$67
$C$DW$T$67	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$67, DW_AT_name("TIDL_customMaxPoolExecFunc")
	.dwattr $C$DW$T$67, DW_AT_type(*$C$DW$T$66)
	.dwattr $C$DW$T$67, DW_AT_decl_file("./tidsp/tidl_custom_maxpool_ixX_oxX_priv.h")
	.dwattr $C$DW$T$67, DW_AT_decl_line(0x35)
	.dwattr $C$DW$T$67, DW_AT_decl_column(0x13)

	.dwendtag $C$DW$TU$67


$C$DW$TU$11	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$11
$C$DW$T$11	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$11, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$11, DW_AT_name("unsigned int")
	.dwattr $C$DW$T$11, DW_AT_byte_size(0x04)

	.dwendtag $C$DW$TU$11


$C$DW$TU$25	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$25
$C$DW$T$25	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$25, DW_AT_name("__uint32_t")
	.dwattr $C$DW$T$25, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$T$25, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/machine/_types.h")
	.dwattr $C$DW$T$25, DW_AT_decl_line(0x65)
	.dwattr $C$DW$T$25, DW_AT_decl_column(0x17)

	.dwendtag $C$DW$TU$25


$C$DW$TU$26	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$26
$C$DW$T$26	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$26, DW_AT_name("uint32_t")
	.dwattr $C$DW$T$26, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$T$26, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/sys/_stdint.h")
	.dwattr $C$DW$T$26, DW_AT_decl_line(0x46)
	.dwattr $C$DW$T$26, DW_AT_decl_column(0x15)

	.dwendtag $C$DW$TU$26


$C$DW$TU$27	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$27

$C$DW$T$27	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$27, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$T$27, DW_AT_byte_size(0x40)
$C$DW$114	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$114, DW_AT_upper_bound(0x0f)

	.dwendtag $C$DW$T$27

	.dwendtag $C$DW$TU$27


$C$DW$TU$12	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$12
$C$DW$T$12	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$12, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$12, DW_AT_name("__int40_t")
	.dwattr $C$DW$T$12, DW_AT_byte_size(0x08)

	.dwendtag $C$DW$TU$12


$C$DW$TU$13	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$13
$C$DW$T$13	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$13, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$13, DW_AT_name("unsigned __int40_t")
	.dwattr $C$DW$T$13, DW_AT_byte_size(0x08)

	.dwendtag $C$DW$TU$13


$C$DW$TU$14	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$14
$C$DW$T$14	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$14, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$14, DW_AT_name("long long")
	.dwattr $C$DW$T$14, DW_AT_byte_size(0x08)

	.dwendtag $C$DW$TU$14


$C$DW$TU$15	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$15
$C$DW$T$15	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$15, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$15, DW_AT_name("unsigned long long")
	.dwattr $C$DW$T$15, DW_AT_byte_size(0x08)

	.dwendtag $C$DW$TU$15


$C$DW$TU$141	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$141

$C$DW$T$141	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$141, DW_AT_type(*$C$DW$T$15)
	.dwattr $C$DW$T$141, DW_AT_byte_size(0x08)
$C$DW$115	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$115, DW_AT_upper_bound(0x00)

	.dwendtag $C$DW$T$141

	.dwendtag $C$DW$TU$141


$C$DW$TU$16	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$16
$C$DW$T$16	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$16, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$16, DW_AT_name("float")
	.dwattr $C$DW$T$16, DW_AT_byte_size(0x04)

	.dwendtag $C$DW$TU$16


$C$DW$TU$17	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$17
$C$DW$T$17	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$17, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$17, DW_AT_name("double")
	.dwattr $C$DW$T$17, DW_AT_byte_size(0x08)

	.dwendtag $C$DW$TU$17


$C$DW$TU$18	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$18
$C$DW$T$18	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$18, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$18, DW_AT_name("long double")
	.dwattr $C$DW$T$18, DW_AT_byte_size(0x08)

	.dwendtag $C$DW$TU$18


$C$DW$TU$19	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$19
$C$DW$T$19	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$19, DW_AT_encoding(DW_ATE_TI_complex_signed)
	.dwattr $C$DW$T$19, DW_AT_name("cchar")
	.dwattr $C$DW$T$19, DW_AT_byte_size(0x02)

	.dwendtag $C$DW$TU$19


$C$DW$TU$20	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$20
$C$DW$T$20	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$20, DW_AT_encoding(DW_ATE_TI_complex_signed)
	.dwattr $C$DW$T$20, DW_AT_name("cshort")
	.dwattr $C$DW$T$20, DW_AT_byte_size(0x04)

	.dwendtag $C$DW$TU$20


$C$DW$TU$21	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$21
$C$DW$T$21	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$21, DW_AT_encoding(DW_ATE_TI_complex_signed)
	.dwattr $C$DW$T$21, DW_AT_name("cint")
	.dwattr $C$DW$T$21, DW_AT_byte_size(0x08)

	.dwendtag $C$DW$TU$21


$C$DW$TU$22	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$22
$C$DW$T$22	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$22, DW_AT_encoding(DW_ATE_TI_complex_signed)
	.dwattr $C$DW$T$22, DW_AT_name("clong")
	.dwattr $C$DW$T$22, DW_AT_byte_size(0x10)

	.dwendtag $C$DW$TU$22


$C$DW$TU$23	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$23
$C$DW$T$23	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$23, DW_AT_encoding(DW_ATE_TI_complex_float)
	.dwattr $C$DW$T$23, DW_AT_name("cfloat")
	.dwattr $C$DW$T$23, DW_AT_byte_size(0x08)

	.dwendtag $C$DW$TU$23


$C$DW$TU$24	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$24
$C$DW$T$24	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$24, DW_AT_encoding(DW_ATE_TI_complex_float)
	.dwattr $C$DW$T$24, DW_AT_name("cdouble")
	.dwattr $C$DW$T$24, DW_AT_byte_size(0x10)

	.dwendtag $C$DW$TU$24


$C$DW$TU$142	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$142

$C$DW$T$142	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$142, DW_AT_name("__ti_vpred_tp")
	.dwattr $C$DW$T$142, DW_AT_byte_size(0x08)
$C$DW$116	.dwtag  DW_TAG_member
	.dwattr $C$DW$116, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$116, DW_AT_name("__vpred_field")
	.dwattr $C$DW$116, DW_AT_accessibility(DW_ACCESS_private)

	.dwendtag $C$DW$T$142

	.dwendtag $C$DW$TU$142

