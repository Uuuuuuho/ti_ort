;******************************************************************************
;* C7x G3 C/C++ Codegen                                            Unix v1.4.2.LTS *
;* Date/Time created: Tue Oct  5 15:11:17 2021                                *
;******************************************************************************
	.compiler_opts --abi=eabi --endian=little --hll_source=on --object_format=elf --silicon_errata_i2117 --silicon_version=7100 --symdebug:dwarf --symdebug:dwarf_version=4 

;******************************************************************************
;* GLOBAL FILE PARAMETERS                                                     *
;*                                                                            *
;*   Architecture      : C7100                                                *
;*   Endian            : Little                                               *
;*   Pipelining        : Enabled                                              *
;*   Debug Info        : DWARF Debug                                          *
;*                                                                            *
;******************************************************************************

	.asg	D14, FP
	.asg	D15, SP
	.global	$bss


$C$DW$CU	.dwtag  DW_TAG_compile_unit
	.dwattr $C$DW$CU, DW_AT_name("tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c")
	.dwattr $C$DW$CU, DW_AT_producer("TI C7x G3 C/C++ Codegen Unix v1.4.2.LTS Copyright (c) 2014-2019 Texas Instruments Incorporated")
	.dwattr $C$DW$CU, DW_AT_TI_version(0x01)
	.dwattr $C$DW$CU, DW_AT_comp_dir("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/tidl_j7_08_00_00_10/ti_dl/custom")
	.sect	".const:$P$T0$1"
	.align	1
	.elfsym	||$P$T0$1||,SYM_SIZE(64)
||$P$T0$1||:
	.bits		0,32
			; $P$T0$1.ICNT0 @ 0
	.bits		0,32
			; $P$T0$1.ICNT1 @ 32
	.bits		0,32
			; $P$T0$1.ICNT2 @ 64
	.bits		0,32
			; $P$T0$1.ICNT3 @ 96
	.bits		0,32
			; $P$T0$1.ICNT4 @ 128
	.bits		0,32
			; $P$T0$1.ICNT5 @ 160
	.bits		0,32
			; $P$T0$1.DECDIM1_WIDTH @ 192
	.bits		0,32
			; $P$T0$1.DECDIM2_WIDTH @ 224
	.bits		0,32
			; $P$T0$1.DIM1 @ 256
	.bits		0,32
			; $P$T0$1.DIM2 @ 288
	.bits		0,32
			; $P$T0$1.DIM3 @ 320
	.bits		0,32
			; $P$T0$1.DIM4 @ 352
	.bits		0,32
			; $P$T0$1.DIM5 @ 384
	.bits		0,8
			; $P$T0$1.LEZR_CNT @ 416
	.bits		0,24
			; $P$T0$1._reserved1 @ 424
	.bits		0,4
			; $P$T0$1.ELETYPE @ 448
	.bits		0,3
			; $P$T0$1.TRANSPOSE @ 452
	.bits		0,1
			; $P$T0$1._reserved2 @ 455
	.bits		0,3
			; $P$T0$1.PROMOTE @ 456
	.bits		0,1
			; $P$T0$1._reserved3 @ 459
	.bits		0x6,3
			; $P$T0$1.VECLEN @ 460
	.bits		0,1
			; $P$T0$1._reserved4 @ 463
	.bits		0,3
			; $P$T0$1.ELEDUP @ 464
	.bits		0,1
			; $P$T0$1.GRPDUP @ 467
	.bits		0,2
			; $P$T0$1.DECIM @ 468
	.bits		0,2
			; $P$T0$1._reserved5 @ 470
	.bits		0,3
			; $P$T0$1.DIMFMT @ 472
	.bits		0,1
			; $P$T0$1.DIR @ 475
	.bits		0,4
			; $P$T0$1.CBK0 @ 476
	.bits		0,4
			; $P$T0$1.CBK1 @ 480
	.bits		0,2
			; $P$T0$1.AM0 @ 484
	.bits		0,2
			; $P$T0$1.AM1 @ 486
	.bits		0,2
			; $P$T0$1.AM2 @ 488
	.bits		0,2
			; $P$T0$1.AM3 @ 490
	.bits		0,2
			; $P$T0$1.AM4 @ 492
	.bits		0,2
			; $P$T0$1.AM5 @ 494
	.bits		0,3
			; $P$T0$1.DECDIM1 @ 496
	.bits		0,2
			; $P$T0$1.DECDIM1SD @ 499
	.bits		0,3
			; $P$T0$1.DECDIM2 @ 501
	.bits		0,2
			; $P$T0$1.DECDIM2SD @ 504
	.bits		0,3
			; $P$T0$1.LEZR @ 506
	.bits		0,3
			; $P$T0$1.TEMPLATE_FMT @ 509

$C$DW$1	.dwtag  DW_TAG_variable
	.dwattr $C$DW$1, DW_AT_name("$P$T0$1")
	.dwattr $C$DW$1, DW_AT_linkage_name("$P$T0$1")
	.dwattr $C$DW$1, DW_AT_type(*$C$DW$T$275)
	.dwattr $C$DW$1, DW_AT_location[DW_OP_addr ||$P$T0$1||]
	.dwattr $C$DW$1, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$1, DW_AT_decl_line(0x205)
	.dwattr $C$DW$1, DW_AT_decl_column(0x1c)

	.sect	".const:$P$T1$2"
	.align	1
	.elfsym	||$P$T1$2||,SYM_SIZE(64)
||$P$T1$2||:
	.bits		0,32
			; $P$T1$2.ICNT0 @ 0
	.bits		0,32
			; $P$T1$2.ICNT1 @ 32
	.bits		0,32
			; $P$T1$2.ICNT2 @ 64
	.bits		0,32
			; $P$T1$2.ICNT3 @ 96
	.bits		0,32
			; $P$T1$2.ICNT4 @ 128
	.bits		0,32
			; $P$T1$2.ICNT5 @ 160
	.bits		0,32
			; $P$T1$2.DECDIM1_WIDTH @ 192
	.bits		0,32
			; $P$T1$2.DECDIM2_WIDTH @ 224
	.bits		0,32
			; $P$T1$2.DIM1 @ 256
	.bits		0,32
			; $P$T1$2.DIM2 @ 288
	.bits		0,32
			; $P$T1$2.DIM3 @ 320
	.bits		0,32
			; $P$T1$2.DIM4 @ 352
	.bits		0,32
			; $P$T1$2.DIM5 @ 384
	.bits		0,32
			; $P$T1$2._reserved1 @ 416
	.bits		0,12
			; $P$T1$2._reserved2 @ 448
	.bits		0x6,3
			; $P$T1$2.VECLEN @ 460
	.bits		0,7
			; $P$T1$2._reserved3 @ 463
	.bits		0,1
			; $P$T1$2.INV_DD1 @ 470
	.bits		0,1
			; $P$T1$2.INV_DD2 @ 471
	.bits		0,3
			; $P$T1$2.DIMFMT @ 472
	.bits		0,5
			; $P$T1$2._reserved4 @ 475
	.bits		0,16
			; $P$T1$2._reserved5 @ 480
	.bits		0,3
			; $P$T1$2.DECDIM1 @ 496
	.bits		0,2
			; $P$T1$2.DECDIM1SD @ 499
	.bits		0,3
			; $P$T1$2.DECDIM2 @ 501
	.bits		0,2
			; $P$T1$2.DECDIM2SD @ 504
	.bits		0,6
			; $P$T1$2._reserved @ 506

$C$DW$22	.dwtag  DW_TAG_variable
	.dwattr $C$DW$22, DW_AT_name("$P$T1$2")
	.dwattr $C$DW$22, DW_AT_linkage_name("$P$T1$2")
	.dwattr $C$DW$22, DW_AT_type(*$C$DW$T$266)
	.dwattr $C$DW$22, DW_AT_location[DW_OP_addr ||$P$T1$2||]
	.dwattr $C$DW$22, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$22, DW_AT_decl_line(0x271)
	.dwattr $C$DW$22, DW_AT_decl_column(0x1c)

;	/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/bin/opt7x /tmp/TI4Y01zY4S1 /tmp/TI4Y0t3wlrp 
	.sect	".text:_Z45TIDL_customMaxPool_ixX_oxX_buffParamsInit_c7xP32TIDL_CustomMaxPoolIxXOxXPrivArgsPK18TIDL_bufParams3D_tS3_PK32TIDL_CustomMaxPoolIxXOxXInitArgs"
	.clink
	.global	||_Z45TIDL_customMaxPool_ixX_oxX_buffParamsInit_c7xP32TIDL_CustomMaxPoolIxXOxXPrivArgsPK18TIDL_bufParams3D_tS3_PK32TIDL_CustomMaxPoolIxXOxXInitArgs||

$C$DW$23	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$23, DW_AT_name("TIDL_customMaxPool_ixX_oxX_buffParamsInit_c7x")
	.dwattr $C$DW$23, DW_AT_low_pc(||_Z45TIDL_customMaxPool_ixX_oxX_buffParamsInit_c7xP32TIDL_CustomMaxPoolIxXOxXPrivArgsPK18TIDL_bufParams3D_tS3_PK32TIDL_CustomMaxPoolIxXOxXInitArgs||)
	.dwattr $C$DW$23, DW_AT_high_pc(0x00)
	.dwattr $C$DW$23, DW_AT_linkage_name("_Z45TIDL_customMaxPool_ixX_oxX_buffParamsInit_c7xP32TIDL_CustomMaxPoolIxXOxXPrivArgsPK18TIDL_bufParams3D_tS3_PK32TIDL_CustomMaxPoolIxXOxXInitArgs")
	.dwattr $C$DW$23, DW_AT_external
	.dwattr $C$DW$23, DW_AT_decl_file("tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c")
	.dwattr $C$DW$23, DW_AT_decl_line(0x88)
	.dwattr $C$DW$23, DW_AT_decl_column(0x09)
	.dwattr $C$DW$23, DW_AT_TI_max_frame_size(0x00)
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 140,column 1,is_stmt,address ||_Z45TIDL_customMaxPool_ixX_oxX_buffParamsInit_c7xP32TIDL_CustomMaxPoolIxXOxXPrivArgsPK18TIDL_bufParams3D_tS3_PK32TIDL_CustomMaxPoolIxXOxXInitArgs||,isa 0

	.dwfde $C$DW$CIE, ||_Z45TIDL_customMaxPool_ixX_oxX_buffParamsInit_c7xP32TIDL_CustomMaxPoolIxXOxXPrivArgsPK18TIDL_bufParams3D_tS3_PK32TIDL_CustomMaxPoolIxXOxXInitArgs||
$C$DW$24	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$24, DW_AT_name("pKerPrivArgs")
	.dwattr $C$DW$24, DW_AT_location[DW_OP_reg4]

$C$DW$25	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$25, DW_AT_name("srcAddr")
	.dwattr $C$DW$25, DW_AT_location[DW_OP_reg5]

$C$DW$26	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$26, DW_AT_name("dstAddr")
	.dwattr $C$DW$26, DW_AT_location[DW_OP_reg6]

$C$DW$27	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$27, DW_AT_name("kerInitArgs")
	.dwattr $C$DW$27, DW_AT_location[DW_OP_reg7]


;******************************************************************************
;* FUNCTION NAME: TIDL_customMaxPool_ixX_oxX_buffParamsInit_c7x(TIDL_CustomMaxPoolIxXOxXPrivArgs *, const TIDL_bufParams3D_t *, const TIDL_bufParams3D_t *, const TIDL_CustomMaxPoolIxXOxXInitArgs *)*
;*                                                                            *
;*   Regs Modified     : A0,A1,A2,A3,A4,VB0,VB1,VB2,VB3,VB4,VB5,AL0,AL1,AL2,  *
;*                           AM0,D0,D1,D14,VBL0,VBL1,VBL7,VBM0                *
;*   Regs Used         : A0,A1,A2,A3,A4,A5,A6,A7,VB0,VB1,VB2,VB3,VB4,VB5,AL0, *
;*                           AL1,AL2,AM0,D0,D1,D14,VBL0,VBL1,VBL7,VBM0        *
;*   Local Frame Size  : 0 Args + 0 Auto + 0 Save = 0 byte                    *
;******************************************************************************
||_Z45TIDL_customMaxPool_ixX_oxX_buffParamsInit_c7xP32TIDL_CustomMaxPoolIxXOxXPrivArgsPK18TIDL_bufParams3D_tS3_PK32TIDL_CustomMaxPoolIxXOxXInitArgs||:
;** --------------------------------------------------------------------------*
;* A0    assigned to $O$C16
;* A2    assigned to $O$C17
;* AL0   assigned to $O$C18
;* VBM0  assigned to $O$K9
;* VB2   assigned to $O$v2
;* A3    assigned to $O$Lr4$status
;* A2    assigned to $O$Lr5$simdWidthOut
;* VB5   assigned to $O$Lr54$numCh
;* VB4   assigned to $O$Lr66$numTiles
;* VB1   assigned to $O$Lr68$numLines
;* AL0   assigned to $O$Lr6$numBytes
;* A7    assigned to kerInitArgs
$C$DW$28	.dwtag  DW_TAG_variable
	.dwattr $C$DW$28, DW_AT_name("kerInitArgs")
	.dwattr $C$DW$28, DW_AT_type(*$C$DW$T$227)
	.dwattr $C$DW$28, DW_AT_location[DW_OP_reg7]

;* A6    assigned to dstAddr
$C$DW$29	.dwtag  DW_TAG_variable
	.dwattr $C$DW$29, DW_AT_name("dstAddr")
	.dwattr $C$DW$29, DW_AT_type(*$C$DW$T$252)
	.dwattr $C$DW$29, DW_AT_location[DW_OP_reg6]

;* A5    assigned to srcAddr
$C$DW$30	.dwtag  DW_TAG_variable
	.dwattr $C$DW$30, DW_AT_name("srcAddr")
	.dwattr $C$DW$30, DW_AT_type(*$C$DW$T$252)
	.dwattr $C$DW$30, DW_AT_location[DW_OP_reg5]

;* VB3   assigned to pKerPrivArgs
$C$DW$31	.dwtag  DW_TAG_variable
	.dwattr $C$DW$31, DW_AT_name("pKerPrivArgs")
	.dwattr $C$DW$31, DW_AT_type(*$C$DW$T$238)
	.dwattr $C$DW$31, DW_AT_location[DW_OP_regx 0x33]

	.dwcfi	cfa_offset, 0
;          EXCLUSIVE CPU CYCLES: 13
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 87,column 5,is_stmt,isa 0
           LDUW    .D1     *A5(0),B2         ; [A_D1] |87| 
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 92,column 10,is_stmt,isa 0

           CMPEQW  .L1X    B2,0x9,A0         ; [A_L1] |92| 
||         CMPEQW  .L2     B2,0x5,BL1        ; [B_L2] |92| 
||         CMPEQW  .S2     B2,0x1,BL0        ; [B_S2] |92| 

           ORW     .L2     BL1,BL0,BL0       ; [B_L2] |92| 
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 87,column 5,is_stmt,isa 0

           CMPEQW  .L2     B2,0x6,BL1        ; [B_L2] |92| 
||         ORW     .S2X    A0,BL0,BL0        ; [B_S2] |92| 
||         ANDW    .M2     B2,0xfffffffb,B0  ; [B_M2] |87| 

	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 92,column 10,is_stmt,isa 0
           ORW     .L2     BL1,BL0,B1        ; [B_L2] |92| 
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 87,column 5,is_stmt,isa 0

           CMPEQW  .L1X    B0,0,A0           ; [A_L1] |87| 
||         MVKU32  .S1     0,A3              ; [A_S1] |141| 

   [ A0]   B       .B1     ||$C$L2||         ; [A_B] |87| 
||         CMPEQW  .L1X    B1,0,A1           ; [A_L1] |92| 
||         MVKU32  .L2     0xfffffffb,BM0    ; [B_L2] |87| 
||         MVKU32  .S1     0x20,A2           ; [A_S1] |146| 
||         MV      .S2X    A4,B3             ; [B_S2] |140| 

           ; BRANCHCC OCCURS {||$C$L2||}     ; [] |87| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 7
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 92,column 10,is_stmt,isa 0

           CMPEQW  .L1X    B2,0xa,A0         ; [A_L1] |92| 
||         CMPEQW  .L2     B2,0x7,BL1        ; [B_L2] |92| 
||         CMPEQW  .S2     B2,0x2,BL0        ; [B_S2] |92| 

           ORW     .L2     BL1,BL0,BL0       ; [B_L2] |92| 
           ORW     .L2X    A0,BL0,BL0        ; [B_L2] |92| 
           CMPEQW  .L2     BL0,0,BL0         ; [B_L2] |92| 
           XORD    .L2     BL0,0x1,B0        ; [B_L2] |92| 
           MV      .L1X    B1,AL0            ; [A_L1] |92| 

   [!A1]   B       .B1     ||$C$L1||         ; [A_B] |92| 
||         CMPEQW  .L1X    B0,0,A0           ; [A_L1] |92| 

           ; BRANCHCC OCCURS {||$C$L1||}     ; [] |92| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 7

           CMPEQW  .L1X    B2,0xb,A1         ; [A_L1] |92| 
||         CMPEQW  .L2     B2,0x8,BL1        ; [B_L2] |92| 
||         CMPEQW  .S2     B2,0x3,BL0        ; [B_S2] |92| 

           ORW     .L2     BL1,BL0,BL0       ; [B_L2] |92| 
           ORW     .L2X    A1,BL0,BL0        ; [B_L2] |92| 

           CMPEQW  .L2     BL0,0,BL0         ; [B_L2] |92| 
||         MV      .S2     B0,B1             ; [B_S2] |92| 

   [ A0]   XORD    .L2     BL0,0x1,B1        ; [B_L2] |92| 
           MV      .L1X    B1,AL0            ; [A_L1] |92| 
;** --------------------------------------------------------------------------*
||$C$L1||:    
;          EXCLUSIVE CPU CYCLES: 2
           CMPEQW  .L1     AL0,0,A0          ; [A_L1] |92| 
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 157,column 5,is_stmt,isa 0
   [!A0]   MVK32   .L1     0xffffffff,A3     ; [A_L1] |157| 
;** --------------------------------------------------------------------------*
||$C$L2||:    
;          EXCLUSIVE CPU CYCLES: 11
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 161,column 3,is_stmt,isa 0

           LDW     .D1     *A7(8),BL1        ; [A_D1] |161| 
||         LDW     .D2     *A7(12),BL0       ; [A_D2] |161| 

           CMPEQW  .L2     BL1,0x3,BL1       ; [B_L2] |161| 
||         CMPEQW  .S2     BL0,0x3,BL0       ; [B_S2] |161| 

           ANDW    .L2     BL1,BL0,B0        ; [B_L2] |161| 

           CMPEQW  .L1X    B0,0,A0           ; [A_L1] |161| 
||         CMPEQW  .S1     A3,0,A4           ; [A_S1] |173| 

	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 163,column 5,is_stmt,isa 0

   [!A4]   B       .B1     ||$C$L4||         ; [A_B] |173| 
|| [!A0]   MVKU32  .L1     0x1f,A2           ; [A_L1] |163| 

	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 173,column 3,is_stmt,isa 0
           ; BRANCHCC OCCURS {||$C$L4||}     ; [] |173| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 12
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 175,column 5,is_stmt,isa 0

           LDW     .D1     *A7(20),BL0       ; [A_D1] |175| 
||         LDW     .D2     *A7(16),BL1       ; [A_D2] |175| 

           CMPEQW  .L2     BL0,0x2,BL0       ; [B_L2] |175| 
||         CMPEQW  .S2     BL1,0x2,BL1       ; [B_S2] |175| 

           XORD    .L2     BL0,0x1,BL0       ; [B_L2] |175| 
||         XORD    .S2     BL1,0x1,BL1       ; [B_S2] |175| 

           ORW     .L2     BL1,BL0,B1        ; [B_L2] |175| 
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 87,column 5,is_stmt,isa 0
           ANDW    .M2     BM0,B2,B0         ; [B_M2] |87| 
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 175,column 5,is_stmt,isa 0
           CMPEQW  .L1X    B1,0,A1           ; [A_L1] |175| 
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 87,column 5,is_stmt,isa 0

   [!A1]   B       .B1     ||$C$L5||         ; [A_B] |197| 
||         CMPEQW  .L1X    B0,0,A0           ; [A_L1] |87| 
||         MVK32   .S1     0xffffffff,A4     ; [A_S1] |197| 
||         ANDW    .M1     A2,0xffff,AM0     ; [A_M1] |178| 

	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 197,column 3,is_stmt,isa 0
           ; BRANCHCC OCCURS {||$C$L5||}     ; [] |197| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 16
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 178,column 7,is_stmt,isa 0
           LDUW    .D1     *A6(4),AL0        ; [A_D1] |178| 
           DIVUW   .L1     AL0,A2,A2         ; [A_L1] |178| 
           MPYWW   .N1     A2,AM0,AL1        ; [A_N1] |178| 
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 92,column 10,is_stmt,isa 0

           CMPEQW  .L2     B2,0x1,BL0        ; [B_L2] |92| 
||         CMPEQW  .L1X    B2,0x9,AL2        ; [A_L1] |92| 
||         CMPEQW  .S2     B2,0x5,BL1        ; [B_S2] |92| 

           XORD    .L2     BL1,0x1,BL1       ; [B_L2] |92| 
||         XORD    .S2     BL0,0x1,BL0       ; [B_S2] |92| 
||         XORD    .L1     AL2,0x1,A1        ; [A_L1] |92| 

           ANDW    .L2     BL1,BL0,BL0       ; [B_L2] |92| 

           CMPEQW  .L1     AL0,AL1,A1        ; [A_L1] |178| 
||         ANDW    .L2X    A1,BL0,B0         ; [B_L2] |92| 

           CMPEQW  .L1X    B0,0,A1           ; [A_L1] |92| 
||         XORD    .L2X    A1,0x1,BL0        ; [B_L2] |178| 

	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 118,column 5,is_stmt,isa 0

   [ A0]   MVKU32  .L1     0x1,A1            ; [A_L1] |118| 
|| [ A0]   MVKU32  .S1     0x1,AL0           ; [A_S1] |118| 

	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 92,column 10,is_stmt,isa 0

   [ A1]   B       .B1     ||$C$L3||         ; [A_B] |92| 
|| [!A0]   CMPEQW  .L1X    B2,0x6,A0         ; [A_L1] |98| 
||         ADDW    .L2X    A2,BL0,B4         ; [B_L2] |178| 
|| [!A0]   MVKU32  .S1     0x2,AL0           ; [A_S1] |118| 
||         LDUW    .D1     *A6(16),B5        ; [A_D1] |177| 
||         LDUW    .D2     *A6(8),B1         ; [A_D2] |179| 

           ; BRANCHCC OCCURS {||$C$L3||}     ; [] |92| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 6
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 102,column 10,is_stmt,isa 0

           CMPEQW  .L2     B2,0x2,BL0        ; [B_L2] |102| 
||         CMPEQW  .L1X    B2,0xa,AL0        ; [A_L1] |102| 
||         CMPEQW  .S2     B2,0x7,BL1        ; [B_S2] |102| 

           XORD    .L2     BL1,0x1,BL1       ; [B_L2] |102| 
||         XORD    .S2     BL0,0x1,BL0       ; [B_S2] |102| 
||         XORD    .L1     AL0,0x1,A1        ; [A_L1] |102| 

           ANDW    .L2     BL1,BL0,BL0       ; [B_L2] |102| 
           ANDW    .L2X    A1,BL0,B0         ; [B_L2] |102| 
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 98,column 10,is_stmt,isa 0

   [ A0]   B       .B1     ||$C$L3||         ; [A_B] |98| 
||         CMPEQW  .L1X    B0,0,A1           ; [A_L1] |102| 
||         MVKU32  .S1     0x3,AL0           ; [A_S1] |118| 

           ; BRANCHCC OCCURS {||$C$L3||}     ; [] |98| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 7
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 108,column 10,is_stmt,isa 0

           CMPEQW  .L2     B2,0x8,BL1        ; [B_L2] |108| 
||         CMPEQW  .S2     B2,0x3,BL0        ; [B_S2] |108| 

           ORW     .L2     BL1,BL0,BL0       ; [B_L2] |108| 
||         CMPEQW  .S2     B2,0xb,BL7        ; [B_S2] |108| 

           ORW     .L2     BL7,BL0,B0        ; [B_L2] |108| 

           CMPEQW  .L1X    B0,0,A0           ; [A_L1] |108| 
||         MVKU32  .S1     0x4,AL0           ; [A_S1] |118| 

	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 118,column 5,is_stmt,isa 0

   [ A1]   MVKU32  .L1     0,A0              ; [A_L1] |118| 
|| [!A1]   MVKU32  .S1     0x8,AL0           ; [A_S1] |118| 

   [ A0]   MVKU32  .L1     0x1,AL0           ; [A_L1] |118| 
;** --------------------------------------------------------------------------*
||$C$L3||:    
;          EXCLUSIVE CPU CYCLES: 8
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 191,column 5,is_stmt,isa 0
           LDW     .D1     *A5(12),AL1       ; [A_D1] |191| 
           ADDD    .L1X    B3,0x104,A0       ; [A_L1] |191| 
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 192,column 5,is_stmt,isa 0

           ADDD    .D1     A0,0xfffffff8,D0  ; [A_D1] |193| 
||         ADDD    .D2     A0,0xfffffffc,D1  ; [A_D2] |192| 

	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 191,column 5,is_stmt,isa 0

           DIVW    .L1     AL1,AL0,D1        ; [A_L1] |191| 
||         STW     .D1X    B1,*D0(0)         ; [A_D1] |193| 
||         STW     .D2     B4,*D1(0)         ; [A_D2] |192| 
||         ADDD    .S1     A0,0xfffffff4,D14 ; [A_S1] |194| 

           STW     .D1     D1,*A0(0)         ; [A_D1] |191| 
||         STW     .D2     B5,*D14(0)        ; [A_D2] |194| 

;** --------------------------------------------------------------------------*
||$C$L4||:    
;          EXCLUSIVE CPU CYCLES: 1
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 197,column 3,is_stmt,isa 0
           MV      .D1     A3,A4             ; [A_D1] |197| 
;** --------------------------------------------------------------------------*
||$C$L5||:    
;          EXCLUSIVE CPU CYCLES: 1
$C$DW$32	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$32, DW_AT_low_pc(0x00)
	.dwattr $C$DW$32, DW_AT_TI_return

           RET     .B1     ; [A_B] 
           ; RETURN OCCURS {RP}              ; [] 
	.dwattr $C$DW$23, DW_AT_TI_end_file("tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c")
	.dwattr $C$DW$23, DW_AT_TI_end_line(0xc6)
	.dwattr $C$DW$23, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$23

	.sect	".text:_Z43TIDL_customMaxPool_3x3_2x2_ixu_oxu_exec_c7xPvPKvS_"
	.clink
	.global	||_Z43TIDL_customMaxPool_3x3_2x2_ixu_oxu_exec_c7xPvPKvS_||

$C$DW$33	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$33, DW_AT_name("TIDL_customMaxPool_3x3_2x2_ixu_oxu_exec_c7x")
	.dwattr $C$DW$33, DW_AT_low_pc(||_Z43TIDL_customMaxPool_3x3_2x2_ixu_oxu_exec_c7xPvPKvS_||)
	.dwattr $C$DW$33, DW_AT_high_pc(0x00)
	.dwattr $C$DW$33, DW_AT_linkage_name("_Z43TIDL_customMaxPool_3x3_2x2_ixu_oxu_exec_c7xPvPKvS_")
	.dwattr $C$DW$33, DW_AT_external
	.dwattr $C$DW$33, DW_AT_decl_file("tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c")
	.dwattr $C$DW$33, DW_AT_decl_line(0x163)
	.dwattr $C$DW$33, DW_AT_decl_column(0x09)
	.dwattr $C$DW$33, DW_AT_TI_max_frame_size(0x00)
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 358,column 1,is_stmt,address ||_Z43TIDL_customMaxPool_3x3_2x2_ixu_oxu_exec_c7xPvPKvS_||,isa 0

	.dwfde $C$DW$CIE, ||_Z43TIDL_customMaxPool_3x3_2x2_ixu_oxu_exec_c7xPvPKvS_||
$C$DW$34	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$34, DW_AT_name("handle")
	.dwattr $C$DW$34, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$34, DW_AT_location[DW_OP_reg4]

$C$DW$35	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$35, DW_AT_name("src")
	.dwattr $C$DW$35, DW_AT_type(*$C$DW$T$83)
	.dwattr $C$DW$35, DW_AT_location[DW_OP_reg5]

$C$DW$36	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$36, DW_AT_name("dst")
	.dwattr $C$DW$36, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$36, DW_AT_location[DW_OP_reg6]


;******************************************************************************
;* FUNCTION NAME: TIDL_customMaxPool_3x3_2x2_ixu_oxu_exec_c7x(void *, const void *, void *)*
;*                                                                            *
;*   Regs Modified     : A0,A1,A2,A4,VB0,VB1,VB2,VB3,VB4,VB5,VB6,VB7,VB8,VB9, *
;*                           VB10,VB11,VB12,VB13,AL0,AM0,D0,D1,VBL0,VBL1,VBL2,*
;*                           VBL3,VBL4,VBL5,VBL6,VBL7,VBM0,VBM1,VBM2,VBM3,    *
;*                           VBM4,VBM5,VBM6,VBM7                              *
;*   Regs Used         : A0,A1,A2,A4,A5,A6,VB0,VB1,VB2,VB3,VB4,VB5,VB6,VB7,   *
;*                           VB8,VB9,VB10,VB11,VB12,VB13,AL0,AM0,D0,D1,VBL0,  *
;*                           VBL1,VBL2,VBL3,VBL4,VBL5,VBL6,VBL7,VBM0,VBM1,    *
;*                           VBM2,VBM3,VBM4,VBM5,VBM6,VBM7                    *
;*   Local Frame Size  : 0 Args + 0 Auto + 0 Save = 0 byte                    *
;******************************************************************************
||_Z43TIDL_customMaxPool_3x3_2x2_ixu_oxu_exec_c7xPvPKvS_||:
;** --------------------------------------------------------------------------*
;* D1    assigned to $O$C15
;* D0    assigned to $O$C16
;* A1    assigned to $O$U45
;* A0    assigned to $O$L1
;* A1    assigned to $O$L2
;* A5    assigned to src
$C$DW$37	.dwtag  DW_TAG_variable
	.dwattr $C$DW$37, DW_AT_name("src")
	.dwattr $C$DW$37, DW_AT_type(*$C$DW$T$83)
	.dwattr $C$DW$37, DW_AT_location[DW_OP_reg5]

;* A4    assigned to handle
$C$DW$38	.dwtag  DW_TAG_variable
	.dwattr $C$DW$38, DW_AT_name("handle")
	.dwattr $C$DW$38, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$38, DW_AT_location[DW_OP_reg4]

;* D1    assigned to tiles
$C$DW$39	.dwtag  DW_TAG_variable
	.dwattr $C$DW$39, DW_AT_name("tiles")
	.dwattr $C$DW$39, DW_AT_type(*$C$DW$T$69)
	.dwattr $C$DW$39, DW_AT_location[DW_OP_regx 0x61]

;* VB0   assigned to numCh
$C$DW$40	.dwtag  DW_TAG_variable
	.dwattr $C$DW$40, DW_AT_name("numCh")
	.dwattr $C$DW$40, DW_AT_type(*$C$DW$T$69)
	.dwattr $C$DW$40, DW_AT_location[DW_OP_regx 0x30]

;* VBL0  assigned to numLines
$C$DW$41	.dwtag  DW_TAG_variable
	.dwattr $C$DW$41, DW_AT_name("numLines")
	.dwattr $C$DW$41, DW_AT_type(*$C$DW$T$69)
	.dwattr $C$DW$41, DW_AT_location[DW_OP_regx 0x48]

;* VB1   assigned to numTiles
$C$DW$42	.dwtag  DW_TAG_variable
	.dwattr $C$DW$42, DW_AT_name("numTiles")
	.dwattr $C$DW$42, DW_AT_type(*$C$DW$T$69)
	.dwattr $C$DW$42, DW_AT_location[DW_OP_regx 0x31]

;* A5    assigned to pIn0
$C$DW$43	.dwtag  DW_TAG_variable
	.dwattr $C$DW$43, DW_AT_name("pIn0")
	.dwattr $C$DW$43, DW_AT_type(*$C$DW$T$293)
	.dwattr $C$DW$43, DW_AT_location[DW_OP_reg5]

;* D0    assigned to pIn1
$C$DW$44	.dwtag  DW_TAG_variable
	.dwattr $C$DW$44, DW_AT_name("pIn1")
	.dwattr $C$DW$44, DW_AT_type(*$C$DW$T$293)
	.dwattr $C$DW$44, DW_AT_location[DW_OP_regx 0x60]

;* A6    assigned to pOut1
$C$DW$45	.dwtag  DW_TAG_variable
	.dwattr $C$DW$45, DW_AT_name("pOut1")
	.dwattr $C$DW$45, DW_AT_type(*$C$DW$T$290)
	.dwattr $C$DW$45, DW_AT_location[DW_OP_reg6]

;* VB0   assigned to vA
$C$DW$46	.dwtag  DW_TAG_variable
	.dwattr $C$DW$46, DW_AT_name("vA")
	.dwattr $C$DW$46, DW_AT_type(*$C$DW$T$307)
	.dwattr $C$DW$46, DW_AT_location[DW_OP_regx 0x30]

;* VBL1  assigned to vA
$C$DW$47	.dwtag  DW_TAG_variable
	.dwattr $C$DW$47, DW_AT_name("vA")
	.dwattr $C$DW$47, DW_AT_type(*$C$DW$T$307)
	.dwattr $C$DW$47, DW_AT_location[DW_OP_regx 0x49]

;* VBL5  assigned to vA
$C$DW$48	.dwtag  DW_TAG_variable
	.dwattr $C$DW$48, DW_AT_name("vA")
	.dwattr $C$DW$48, DW_AT_type(*$C$DW$T$307)
	.dwattr $C$DW$48, DW_AT_location[DW_OP_regx 0x4d]

;* VBL4  assigned to vA
$C$DW$49	.dwtag  DW_TAG_variable
	.dwattr $C$DW$49, DW_AT_name("vA")
	.dwattr $C$DW$49, DW_AT_type(*$C$DW$T$307)
	.dwattr $C$DW$49, DW_AT_location[DW_OP_regx 0x4c]

;* VBL2  assigned to vA
$C$DW$50	.dwtag  DW_TAG_variable
	.dwattr $C$DW$50, DW_AT_name("vA")
	.dwattr $C$DW$50, DW_AT_type(*$C$DW$T$307)
	.dwattr $C$DW$50, DW_AT_location[DW_OP_regx 0x4a]

;* VB6   assigned to vA
$C$DW$51	.dwtag  DW_TAG_variable
	.dwattr $C$DW$51, DW_AT_name("vA")
	.dwattr $C$DW$51, DW_AT_type(*$C$DW$T$307)
	.dwattr $C$DW$51, DW_AT_location[DW_OP_regx 0x36]

;* VBL0  assigned to vA
$C$DW$52	.dwtag  DW_TAG_variable
	.dwattr $C$DW$52, DW_AT_name("vA")
	.dwattr $C$DW$52, DW_AT_type(*$C$DW$T$307)
	.dwattr $C$DW$52, DW_AT_location[DW_OP_regx 0x48]

;* VB5   assigned to vA
$C$DW$53	.dwtag  DW_TAG_variable
	.dwattr $C$DW$53, DW_AT_name("vA")
	.dwattr $C$DW$53, DW_AT_type(*$C$DW$T$307)
	.dwattr $C$DW$53, DW_AT_location[DW_OP_regx 0x35]

;* VB12  assigned to vA
$C$DW$54	.dwtag  DW_TAG_variable
	.dwattr $C$DW$54, DW_AT_name("vA")
	.dwattr $C$DW$54, DW_AT_type(*$C$DW$T$307)
	.dwattr $C$DW$54, DW_AT_location[DW_OP_regx 0x3c]

;* VBL1  assigned to vB
$C$DW$55	.dwtag  DW_TAG_variable
	.dwattr $C$DW$55, DW_AT_name("vB")
	.dwattr $C$DW$55, DW_AT_type(*$C$DW$T$307)
	.dwattr $C$DW$55, DW_AT_location[DW_OP_regx 0x49]

;* VB10  assigned to vB
$C$DW$56	.dwtag  DW_TAG_variable
	.dwattr $C$DW$56, DW_AT_name("vB")
	.dwattr $C$DW$56, DW_AT_type(*$C$DW$T$307)
	.dwattr $C$DW$56, DW_AT_location[DW_OP_regx 0x3a]

;* VBL0  assigned to vB
$C$DW$57	.dwtag  DW_TAG_variable
	.dwattr $C$DW$57, DW_AT_name("vB")
	.dwattr $C$DW$57, DW_AT_type(*$C$DW$T$307)
	.dwattr $C$DW$57, DW_AT_location[DW_OP_regx 0x48]

;* VB9   assigned to vB
$C$DW$58	.dwtag  DW_TAG_variable
	.dwattr $C$DW$58, DW_AT_name("vB")
	.dwattr $C$DW$58, DW_AT_type(*$C$DW$T$307)
	.dwattr $C$DW$58, DW_AT_location[DW_OP_regx 0x39]

;* VBL3  assigned to vB
$C$DW$59	.dwtag  DW_TAG_variable
	.dwattr $C$DW$59, DW_AT_name("vB")
	.dwattr $C$DW$59, DW_AT_type(*$C$DW$T$307)
	.dwattr $C$DW$59, DW_AT_location[DW_OP_regx 0x4b]

;* VBL6  assigned to vB
$C$DW$60	.dwtag  DW_TAG_variable
	.dwattr $C$DW$60, DW_AT_name("vB")
	.dwattr $C$DW$60, DW_AT_type(*$C$DW$T$307)
	.dwattr $C$DW$60, DW_AT_location[DW_OP_regx 0x4e]

;* VBM1  assigned to vB
$C$DW$61	.dwtag  DW_TAG_variable
	.dwattr $C$DW$61, DW_AT_name("vB")
	.dwattr $C$DW$61, DW_AT_type(*$C$DW$T$307)
	.dwattr $C$DW$61, DW_AT_location[DW_OP_regx 0x59]

;* VBL6  assigned to vB
$C$DW$62	.dwtag  DW_TAG_variable
	.dwattr $C$DW$62, DW_AT_name("vB")
	.dwattr $C$DW$62, DW_AT_type(*$C$DW$T$307)
	.dwattr $C$DW$62, DW_AT_location[DW_OP_regx 0x4e]

;* VBL7  assigned to vB
$C$DW$63	.dwtag  DW_TAG_variable
	.dwattr $C$DW$63, DW_AT_name("vB")
	.dwattr $C$DW$63, DW_AT_type(*$C$DW$T$307)
	.dwattr $C$DW$63, DW_AT_location[DW_OP_regx 0x4f]

;* VBM1  assigned to vMax0
$C$DW$64	.dwtag  DW_TAG_variable
	.dwattr $C$DW$64, DW_AT_name("vMax0")
	.dwattr $C$DW$64, DW_AT_type(*$C$DW$T$307)
	.dwattr $C$DW$64, DW_AT_location[DW_OP_regx 0x59]

;* VB8   assigned to vMax0
$C$DW$65	.dwtag  DW_TAG_variable
	.dwattr $C$DW$65, DW_AT_name("vMax0")
	.dwattr $C$DW$65, DW_AT_type(*$C$DW$T$307)
	.dwattr $C$DW$65, DW_AT_location[DW_OP_regx 0x38]

;* VBM7  assigned to vMax0
$C$DW$66	.dwtag  DW_TAG_variable
	.dwattr $C$DW$66, DW_AT_name("vMax0")
	.dwattr $C$DW$66, DW_AT_type(*$C$DW$T$307)
	.dwattr $C$DW$66, DW_AT_location[DW_OP_regx 0x5f]

;* VBM6  assigned to vMax0
$C$DW$67	.dwtag  DW_TAG_variable
	.dwattr $C$DW$67, DW_AT_name("vMax0")
	.dwattr $C$DW$67, DW_AT_type(*$C$DW$T$307)
	.dwattr $C$DW$67, DW_AT_location[DW_OP_regx 0x5e]

;* VBM5  assigned to vMax0
$C$DW$68	.dwtag  DW_TAG_variable
	.dwattr $C$DW$68, DW_AT_name("vMax0")
	.dwattr $C$DW$68, DW_AT_type(*$C$DW$T$307)
	.dwattr $C$DW$68, DW_AT_location[DW_OP_regx 0x5d]

;* VBM4  assigned to vMax0
$C$DW$69	.dwtag  DW_TAG_variable
	.dwattr $C$DW$69, DW_AT_name("vMax0")
	.dwattr $C$DW$69, DW_AT_type(*$C$DW$T$307)
	.dwattr $C$DW$69, DW_AT_location[DW_OP_regx 0x5c]

;* VB2   assigned to vMax0
$C$DW$70	.dwtag  DW_TAG_variable
	.dwattr $C$DW$70, DW_AT_name("vMax0")
	.dwattr $C$DW$70, DW_AT_type(*$C$DW$T$307)
	.dwattr $C$DW$70, DW_AT_location[DW_OP_regx 0x32]

;* VB13  assigned to vMax0
$C$DW$71	.dwtag  DW_TAG_variable
	.dwattr $C$DW$71, DW_AT_name("vMax0")
	.dwattr $C$DW$71, DW_AT_type(*$C$DW$T$307)
	.dwattr $C$DW$71, DW_AT_location[DW_OP_regx 0x3d]

;* VBM2  assigned to vMax0
$C$DW$72	.dwtag  DW_TAG_variable
	.dwattr $C$DW$72, DW_AT_name("vMax0")
	.dwattr $C$DW$72, DW_AT_type(*$C$DW$T$307)
	.dwattr $C$DW$72, DW_AT_location[DW_OP_regx 0x5a]

;* VBM0  assigned to vMax1
$C$DW$73	.dwtag  DW_TAG_variable
	.dwattr $C$DW$73, DW_AT_name("vMax1")
	.dwattr $C$DW$73, DW_AT_type(*$C$DW$T$307)
	.dwattr $C$DW$73, DW_AT_location[DW_OP_regx 0x58]

;* VB7   assigned to vMax1
$C$DW$74	.dwtag  DW_TAG_variable
	.dwattr $C$DW$74, DW_AT_name("vMax1")
	.dwattr $C$DW$74, DW_AT_type(*$C$DW$T$307)
	.dwattr $C$DW$74, DW_AT_location[DW_OP_regx 0x37]

;* VBM3  assigned to vMax1
$C$DW$75	.dwtag  DW_TAG_variable
	.dwattr $C$DW$75, DW_AT_name("vMax1")
	.dwattr $C$DW$75, DW_AT_type(*$C$DW$T$307)
	.dwattr $C$DW$75, DW_AT_location[DW_OP_regx 0x5b]

;* VBM2  assigned to vMax1
$C$DW$76	.dwtag  DW_TAG_variable
	.dwattr $C$DW$76, DW_AT_name("vMax1")
	.dwattr $C$DW$76, DW_AT_type(*$C$DW$T$307)
	.dwattr $C$DW$76, DW_AT_location[DW_OP_regx 0x5a]

;* VBM1  assigned to vMax1
$C$DW$77	.dwtag  DW_TAG_variable
	.dwattr $C$DW$77, DW_AT_name("vMax1")
	.dwattr $C$DW$77, DW_AT_type(*$C$DW$T$307)
	.dwattr $C$DW$77, DW_AT_location[DW_OP_regx 0x59]

;* VBM0  assigned to vMax1
$C$DW$78	.dwtag  DW_TAG_variable
	.dwattr $C$DW$78, DW_AT_name("vMax1")
	.dwattr $C$DW$78, DW_AT_type(*$C$DW$T$307)
	.dwattr $C$DW$78, DW_AT_location[DW_OP_regx 0x58]

;* VB1   assigned to vMax1
$C$DW$79	.dwtag  DW_TAG_variable
	.dwattr $C$DW$79, DW_AT_name("vMax1")
	.dwattr $C$DW$79, DW_AT_type(*$C$DW$T$307)
	.dwattr $C$DW$79, DW_AT_location[DW_OP_regx 0x31]

;* VB11  assigned to vMax1
$C$DW$80	.dwtag  DW_TAG_variable
	.dwattr $C$DW$80, DW_AT_name("vMax1")
	.dwattr $C$DW$80, DW_AT_type(*$C$DW$T$307)
	.dwattr $C$DW$80, DW_AT_location[DW_OP_regx 0x3b]

;* VBM6  assigned to vMax1
$C$DW$81	.dwtag  DW_TAG_variable
	.dwattr $C$DW$81, DW_AT_name("vMax1")
	.dwattr $C$DW$81, DW_AT_type(*$C$DW$T$307)
	.dwattr $C$DW$81, DW_AT_location[DW_OP_regx 0x5e]

;* VBL2  assigned to v
$C$DW$82	.dwtag  DW_TAG_variable
	.dwattr $C$DW$82, DW_AT_name("v")
	.dwattr $C$DW$82, DW_AT_type(*$C$DW$T$305)
	.dwattr $C$DW$82, DW_AT_location[DW_OP_regx 0x4a]

;* VBL4  assigned to v
$C$DW$83	.dwtag  DW_TAG_variable
	.dwattr $C$DW$83, DW_AT_name("v")
	.dwattr $C$DW$83, DW_AT_type(*$C$DW$T$305)
	.dwattr $C$DW$83, DW_AT_location[DW_OP_regx 0x4c]

;* VBL1  assigned to v
$C$DW$84	.dwtag  DW_TAG_variable
	.dwattr $C$DW$84, DW_AT_name("v")
	.dwattr $C$DW$84, DW_AT_type(*$C$DW$T$305)
	.dwattr $C$DW$84, DW_AT_location[DW_OP_regx 0x49]

;* VB5   assigned to v
$C$DW$85	.dwtag  DW_TAG_variable
	.dwattr $C$DW$85, DW_AT_name("v")
	.dwattr $C$DW$85, DW_AT_type(*$C$DW$T$305)
	.dwattr $C$DW$85, DW_AT_location[DW_OP_regx 0x35]

;* VBL6  assigned to v
$C$DW$86	.dwtag  DW_TAG_variable
	.dwattr $C$DW$86, DW_AT_name("v")
	.dwattr $C$DW$86, DW_AT_type(*$C$DW$T$305)
	.dwattr $C$DW$86, DW_AT_location[DW_OP_regx 0x4e]

;* VBL7  assigned to v
$C$DW$87	.dwtag  DW_TAG_variable
	.dwattr $C$DW$87, DW_AT_name("v")
	.dwattr $C$DW$87, DW_AT_type(*$C$DW$T$305)
	.dwattr $C$DW$87, DW_AT_location[DW_OP_regx 0x4f]

;* VBM5  assigned to v
$C$DW$88	.dwtag  DW_TAG_variable
	.dwattr $C$DW$88, DW_AT_name("v")
	.dwattr $C$DW$88, DW_AT_type(*$C$DW$T$305)
	.dwattr $C$DW$88, DW_AT_location[DW_OP_regx 0x5d]

;* VBL3  assigned to v
$C$DW$89	.dwtag  DW_TAG_variable
	.dwattr $C$DW$89, DW_AT_name("v")
	.dwattr $C$DW$89, DW_AT_type(*$C$DW$T$305)
	.dwattr $C$DW$89, DW_AT_location[DW_OP_regx 0x4b]

;* VBL3  assigned to v
$C$DW$90	.dwtag  DW_TAG_variable
	.dwattr $C$DW$90, DW_AT_name("v")
	.dwattr $C$DW$90, DW_AT_type(*$C$DW$T$305)
	.dwattr $C$DW$90, DW_AT_location[DW_OP_regx 0x4b]

;* VBM7  assigned to v
$C$DW$91	.dwtag  DW_TAG_variable
	.dwattr $C$DW$91, DW_AT_name("v")
	.dwattr $C$DW$91, DW_AT_type(*$C$DW$T$305)
	.dwattr $C$DW$91, DW_AT_location[DW_OP_regx 0x5f]

;* VBM2  assigned to v
$C$DW$92	.dwtag  DW_TAG_variable
	.dwattr $C$DW$92, DW_AT_name("v")
	.dwattr $C$DW$92, DW_AT_type(*$C$DW$T$305)
	.dwattr $C$DW$92, DW_AT_location[DW_OP_regx 0x5a]

;* VBL0  assigned to v
$C$DW$93	.dwtag  DW_TAG_variable
	.dwattr $C$DW$93, DW_AT_name("v")
	.dwattr $C$DW$93, DW_AT_type(*$C$DW$T$305)
	.dwattr $C$DW$93, DW_AT_location[DW_OP_regx 0x48]

;* VBL5  assigned to v
$C$DW$94	.dwtag  DW_TAG_variable
	.dwattr $C$DW$94, DW_AT_name("v")
	.dwattr $C$DW$94, DW_AT_type(*$C$DW$T$305)
	.dwattr $C$DW$94, DW_AT_location[DW_OP_regx 0x4d]

;* VBM0  assigned to v
$C$DW$95	.dwtag  DW_TAG_variable
	.dwattr $C$DW$95, DW_AT_name("v")
	.dwattr $C$DW$95, DW_AT_type(*$C$DW$T$305)
	.dwattr $C$DW$95, DW_AT_location[DW_OP_regx 0x58]

;* VBM5  assigned to v
$C$DW$96	.dwtag  DW_TAG_variable
	.dwattr $C$DW$96, DW_AT_name("v")
	.dwattr $C$DW$96, DW_AT_type(*$C$DW$T$305)
	.dwattr $C$DW$96, DW_AT_location[DW_OP_regx 0x5d]

;* VBL3  assigned to v
$C$DW$97	.dwtag  DW_TAG_variable
	.dwattr $C$DW$97, DW_AT_name("v")
	.dwattr $C$DW$97, DW_AT_type(*$C$DW$T$305)
	.dwattr $C$DW$97, DW_AT_location[DW_OP_regx 0x4b]

;* VBL2  assigned to v
$C$DW$98	.dwtag  DW_TAG_variable
	.dwattr $C$DW$98, DW_AT_name("v")
	.dwattr $C$DW$98, DW_AT_type(*$C$DW$T$305)
	.dwattr $C$DW$98, DW_AT_location[DW_OP_regx 0x4a]

;* VBL6  assigned to v
$C$DW$99	.dwtag  DW_TAG_variable
	.dwattr $C$DW$99, DW_AT_name("v")
	.dwattr $C$DW$99, DW_AT_type(*$C$DW$T$305)
	.dwattr $C$DW$99, DW_AT_location[DW_OP_regx 0x4e]

;* VBM2  assigned to v
$C$DW$100	.dwtag  DW_TAG_variable
	.dwattr $C$DW$100, DW_AT_name("v")
	.dwattr $C$DW$100, DW_AT_type(*$C$DW$T$305)
	.dwattr $C$DW$100, DW_AT_location[DW_OP_regx 0x5a]

;* VBL4  assigned to v
$C$DW$101	.dwtag  DW_TAG_variable
	.dwattr $C$DW$101, DW_AT_name("v")
	.dwattr $C$DW$101, DW_AT_type(*$C$DW$T$305)
	.dwattr $C$DW$101, DW_AT_location[DW_OP_regx 0x4c]

;* VBL1  assigned to v
$C$DW$102	.dwtag  DW_TAG_variable
	.dwattr $C$DW$102, DW_AT_name("v")
	.dwattr $C$DW$102, DW_AT_type(*$C$DW$T$305)
	.dwattr $C$DW$102, DW_AT_location[DW_OP_regx 0x49]

;* VBM5  assigned to v
$C$DW$103	.dwtag  DW_TAG_variable
	.dwattr $C$DW$103, DW_AT_name("v")
	.dwattr $C$DW$103, DW_AT_type(*$C$DW$T$305)
	.dwattr $C$DW$103, DW_AT_location[DW_OP_regx 0x5d]

;* VBL5  assigned to v
$C$DW$104	.dwtag  DW_TAG_variable
	.dwattr $C$DW$104, DW_AT_name("v")
	.dwattr $C$DW$104, DW_AT_type(*$C$DW$T$305)
	.dwattr $C$DW$104, DW_AT_location[DW_OP_regx 0x4d]

;* VBL4  assigned to v
$C$DW$105	.dwtag  DW_TAG_variable
	.dwattr $C$DW$105, DW_AT_name("v")
	.dwattr $C$DW$105, DW_AT_type(*$C$DW$T$305)
	.dwattr $C$DW$105, DW_AT_location[DW_OP_regx 0x4c]

;* VB0   assigned to seTemplate1
$C$DW$106	.dwtag  DW_TAG_variable
	.dwattr $C$DW$106, DW_AT_name("seTemplate1")
	.dwattr $C$DW$106, DW_AT_type(*$C$DW$T$274)
	.dwattr $C$DW$106, DW_AT_location[DW_OP_regx 0x30]

;* VB1   assigned to seTemplate2
$C$DW$107	.dwtag  DW_TAG_variable
	.dwattr $C$DW$107, DW_AT_name("seTemplate2")
	.dwattr $C$DW$107, DW_AT_type(*$C$DW$T$274)
	.dwattr $C$DW$107, DW_AT_location[DW_OP_regx 0x31]

;* VB10  assigned to saTemplate
$C$DW$108	.dwtag  DW_TAG_variable
	.dwattr $C$DW$108, DW_AT_name("saTemplate")
	.dwattr $C$DW$108, DW_AT_type(*$C$DW$T$265)
	.dwattr $C$DW$108, DW_AT_location[DW_OP_regx 0x3a]

;* VB0   assigned to vD
$C$DW$109	.dwtag  DW_TAG_variable
	.dwattr $C$DW$109, DW_AT_name("vD")
	.dwattr $C$DW$109, DW_AT_type(*$C$DW$T$307)
	.dwattr $C$DW$109, DW_AT_location[DW_OP_regx 0x30]

;* VBL0  assigned to v
$C$DW$110	.dwtag  DW_TAG_variable
	.dwattr $C$DW$110, DW_AT_name("v")
	.dwattr $C$DW$110, DW_AT_type(*$C$DW$T$305)
	.dwattr $C$DW$110, DW_AT_location[DW_OP_regx 0x48]

;* VBM2  assigned to v
$C$DW$111	.dwtag  DW_TAG_variable
	.dwattr $C$DW$111, DW_AT_name("v")
	.dwattr $C$DW$111, DW_AT_type(*$C$DW$T$305)
	.dwattr $C$DW$111, DW_AT_location[DW_OP_regx 0x5a]

;* VBL2  assigned to v
$C$DW$112	.dwtag  DW_TAG_variable
	.dwattr $C$DW$112, DW_AT_name("v")
	.dwattr $C$DW$112, DW_AT_type(*$C$DW$T$305)
	.dwattr $C$DW$112, DW_AT_location[DW_OP_regx 0x4a]

	.dwcfi	cfa_offset, 0
;          EXCLUSIVE CPU CYCLES: 42
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 384,column 3,is_stmt,isa 0
           ADDD    .D1     A4,0xb8,D1        ; [A_D1] |384| 
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 388,column 3,is_stmt,isa 0
           LDUW    .D1     *D1(76),B2        ; [A_D1] |388| 
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 381,column 3,is_stmt,isa 0
           VLD8D   .D1     *A4(56),VB0       ; [A_D1] |381| 
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 382,column 3,is_stmt,isa 0
           VLD8D   .D1     *A4(120),VB1      ; [A_D1] |382| 
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 388,column 3,is_stmt,isa 0

           SEOPEN  .D2     A5,0,VB0          ; [A_D2] |390| 
||         SHLW    .L1X    B2,0x1,D0         ; [A_L1] |388| 

           ADDAB   .D1     A5,D0,D0          ; [A_D1] |388| 
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 391,column 3,is_stmt,isa 0
           SEOPEN  .D2     D0,0x1,VB1        ; [A_D2] |391| 
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 375,column 3,is_stmt,isa 0
           ADDD    .D1     A4,0xfc,D0        ; [A_D1] |375| 
           LDW     .D1     *D0(0),BL0        ; [A_D1] |375| 
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 376,column 3,is_stmt,isa 0
           LDW     .D1     *D0(4),B1         ; [A_D1] |376| 
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 53,column 4,is_stmt,isa 0
           VMV     .L2     SE0++,VBL1        ; [B_L2] |53| <0,5>  ^ 
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 377,column 3,is_stmt,isa 0

           VMAXUB  .L2     VBL1,SE0++,VBM0   ; [B_L2] |423| <0,6>  ^ 
||         ADDD    .D1     D0,0xfffffffc,D0  ; [A_D1] |377| 

	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 53,column 4,is_stmt,isa 0

           VMV     .S2     SE1++,VBL0        ; [B_S2] |53| <0,6>  ^ 
||         VMAXUB  .M2     VBM0,SE0++,VBM0   ; [B_M2] |423| <0,7>  ^ 
||         LDW     .D1     *D0(0),B0         ; [A_D1] |377| 
||         ADDW    .L2     BL0,0x1,B2        ; [B_L2] |409| 

           VMAXUB  .L2     VBL0,SE1++,VBL0   ; [B_L2] |429| <0,7>  ^ 
||         VMV     .S2     SE0++,VBM1        ; [B_S2] |53| <0,8>  ^ 

	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 409,column 3,is_stmt,isa 0

           VMAXUB  .M2     VBM1,SE0++,VBL0   ; [B_M2] |423| <0,9>  ^ 
||         VMAXUB  .L2     VBL0,SE1++,VBM1   ; [B_L2] |429| <0,9>  ^ 
||         SHRW    .L1X    B2,0x1,AM0        ; [A_L1] |409| 

	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 53,column 4,is_stmt,isa 0

           VMAXUB  .L2     VBL0,SE0++,VBM6   ; [B_L2] |423| <0,10>  ^ 
||         VMV     .S2     SE1++,VBM3        ; [B_S2] |53| <0,10>  ^ 
||         MPYWW   .N1X    B1,AM0,AM0        ; [A_N1] |409| 

           VMAXUB  .M2     VBM3,SE1++,VBL1   ; [B_M2] |429| <0,11>  ^ 
||         VMV     .L2     SE0++,VBL0        ; [B_L2] |53| <0,11>  ^ 

	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 423,column 5,is_stmt,isa 0

           VMAXUB  .L2     VBL0,SE0++,VBL0   ; [B_L2] |423| <0,12>  ^ 
||         VMAXUB  .S2     VBL1,SE1++,VB11   ; [B_S2] |429| <0,12>  ^ 

	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 53,column 4,is_stmt,isa 0

           VMAXUB  .S2     VBL0,SE0++,VB7    ; [B_S2] |423| <0,13>  ^ 
||         VMV     .M2     SE1++,VBM3        ; [B_M2] |53| <0,13>  ^ 

           VMAXUB  .M2     VBM3,SE1++,VBL1   ; [B_M2] |429| <0,14>  ^ 
||         VMV     .S2     SE0++,VBL0        ; [B_S2] |53| <0,14>  ^ 
||         MPYWW   .N1X    B0,AM0,A1         ; [A_N1] |409| 

	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 423,column 5,is_stmt,isa 0

           VMAXUB  .S2     VBL1,SE1++,VB1    ; [B_S2] |429| <0,15>  ^ 
||         VMAXUB  .L2     VBL0,SE0++,VBL0   ; [B_L2] |423| <0,15>  ^ 

	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 53,column 4,is_stmt,isa 0

           VMV     .M2     SE1++,VBL0        ; [B_M2] |53| <0,16>  ^ 
||         VMAXUB  .S2     VBL0,SE0++,VBM4   ; [B_S2] |423| <0,16>  ^ 

           VMV     .L2     SE0++,VBL0        ; [B_L2] |53| <0,17>  ^ 
||         VMAXUB  .S2     VBL0,SE1++,VBM3   ; [B_S2] |429| <0,17>  ^ 
||         VBPACKH .C2     VBM1,VBM0,VBL4    ; [B_C] |433| <0,12> 

           VMAXUB  .S2     VBL0,SE0++,VBM1   ; [B_S2] |423| <0,18>  ^ 
||         VMAXUB  .M2     VBM3,SE1++,VBM0   ; [B_M2] |429| <0,18>  ^ 
||         VBPACKL .C2     VBM1,VBM0,VB0     ; [B_C] |432| <0,11> 
||         ANDW    .L1     A1,0xfffffff0,D0  ; [A_L1] |53| 

           VMAXUB  .M2     VBM1,SE0++,VB2    ; [B_M2] |423| <0,19>  ^ 
||         VMV     .C2     SE1++,VBL0        ; [B_C] |53| <0,19>  ^ 
||         ADDW    .D1     D0,0x7,AL0        ; [A_D1] |53| 

           VMV     .L2     SE0++,VBL0        ; [B_L2] |53| <0,20>  ^ 
||         VMAXUB  .S2     VBL0,SE1++,VBM1   ; [B_S2] |429| <0,20>  ^ 
||         SHRW    .L1     AL0,0x3,A0        ; [A_L1] |53| 

	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 423,column 5,is_stmt,isa 0

           VMAXUB  .S2     VBL0,SE0++,VB3    ; [B_S2] |423| <0,21>  ^ 
||         VMAXUB  .M2     VBM1,SE1++,VB12   ; [B_M2] |429| <0,21>  ^ 
||         VBPACKL .C2     VB11,VBM6,VB5     ; [B_C] |432| <0,21> 
||         MV      .D1     A0,AL0            ; [A_D1] 
||         UNPROT          0x1               ; [A_U] 

;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*
;*      Loop found in file               : tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c
;*      Loop source line                 : 417
;*      Loop opening brace source line   : 418
;*      Loop closing brace source line   : 447
;*      Loop Unroll Multiple             : 8x
;*      Known Minimum Trip Count         : 2                    
;*      Known Max Trip Count Factor      : 2
;*      Loop Carried Dependency Bound(^) : 24
;*      Unpartitioned Resource Bound     : 24
;*      Partitioned Resource Bound       : 24 (pre-sched)
;*
;*      Searching for software pipeline schedule at ...
;*         ii = 24 Did not find schedule
;*         ii = 25 Schedule found with 3 iterations in parallel
;*
;*      Partitioned Resource Bound(*)    : 24 (post-sched)
;*
;*      Constant Extension #0 Used [C0]  : 0
;*      Constant Extension #1 Used [C1]  : 0
;*
;*      Resource Partition (may include "post-sched" split/spill moves):
;*
;*                                                A-side   B-side
;*      .L units                                     0        0     
;*      .S units                                     0        0     
;*      .M units                                     0        0     
;*      .N units                                     0        0     
;*      .D units                                    16        -     
;*      .B units                                     1        -     
;*      .C units                                     -       16     
;*      .P units                                     -        0     
;*
;*      .M/.N units                                  0        0     
;*      .L/.S units                                  0        8     
;*      .L/.S/.C units                               0        8     
;*      .L/.S/.C/.M units                            0       64     
;*      .L/.S/.C/.M/.D units                         0        0     
;*
;*      .X cross paths                               0        0     
;*
;*      Bound(.D1 .D2 .D)                            8        -     
;*      Bound(.M .N .MN)                             0        0     
;*      Bound(.L .S .LS)                             0        4     
;*      Bound(.L .S .C .LS .LSC)                     0       11     
;*      Bound(.L .S .C .M .LS .LSC .LSCM)            0       24*    
;*      Bound(.L .S .C .M .D .LS .LSC .LSCM .LSCMD)  4       24*    
;*
;*      Register Usage Tables:
;*
;*      +----------------------------------+----------------------------------+
;*      |              ASIDE               |              BSIDE               |
;*      +----------------+--------+--------+----------------+--------+--------+
;*      |      "Axx"     |  "ALx" |  "AMx" |     "VBxx"     | "VBLx" | "VBMx" |
;*      +----------------|--------|--------|----------------|--------|--------+
;*      |0000000000111111|00000000|00000000|0000000000111111|00000000|00000000|
;*      |0123456789012345|01234567|01234567|0123456789012345|01234567|01234567|
;*      +----------------|--------|--------|----------------|--------|--------+
;*   0: |*               |        |        |* *  *********  |******* |** * * *|
;*   1: |*               |        |        |****  ********  |****** *| * * * *|
;*   2: |*               |        |        |****  ********  |********| ***** *|
;*   3: |*               |        |        |**************  |********|* * *   |
;*   4: |*               |        |        |*******  *****  |********|*** *   |
;*   5: |*               |        |        |**** **  *****  |* **** *|*** **  |
;*   6: |*               |        |        |*** **   *****  |****** *|*** **  |
;*   7: |*               |        |        |*****    *****  |********|*** **  |
;*   8: |*               |        |        |*****     ****  |********|*** *** |
;*   9: |*               |        |        | ****     ****  |****** *|*** *** |
;*  10: |*               |        |        | *** *    ****  |****** *|******* |
;*  11: |*               |        |        | *****    ****  |****   *|********|
;*  12: |*               |        |        |*******   ****  |**     *|********|
;*  13: |*               |        |        |*******   ****  |***    *|********|
;*  14: |*               |        |        |*******  *****  |****   *|********|
;*  15: |*               |        |        |******* ******  |**     *|********|
;*  16: |*               |        |        |**************  |****   *|********|
;*  17: |*               |        |        |** ***********  |*****  *|***** **|
;*  18: |*               |        |        |**  ****** ***  |*****  *|*****  *|
;*  19: |*               |        |        |**  ****** ***  |* **** *|****** *|
;*  20: |*               |        |        |**   *****  *   |********|***** **|
;*  21: |*               |        |        |**   *****  **  |******  |********|
;*  22: |*               |        |        |*    ****** **  |********|********|
;*  23: |*               |        |        |*    ******* *  |********|** *** *|
;*  24: |*               |        |        |***  ******* *  |******* |** * * *|
;*      +----------------+--------+--------+----------------+--------+--------+
;*
;*      +----------------+                 +--------+       +-------+
;*      |      "Dxx"     |                 |  "Px"  |       |"CUCRx"|
;*      +----------------+                 +--------+       +-------+
;*      |0000000000111111|                 |00000000|       |0000   |
;*      |0123456789012345|                 |01234567|       |0123   |
;*      +----------------+                 +--------+       +-------+
;*   0: |*               |                 |        |       |       |
;*   1: |*               |                 |        |       |       |
;*   2: |*               |                 |        |       |       |
;*   3: |*               |                 |        |       |       |
;*   4: |*               |                 |        |       |       |
;*   5: |*               |                 |        |       |       |
;*   6: |*               |                 |        |       |       |
;*   7: |*               |                 |        |       |       |
;*   8: |*               |                 |        |       |       |
;*   9: |*               |                 |        |       |       |
;*  10: |*               |                 |        |       |       |
;*  11: |*               |                 |        |       |       |
;*  12: |*               |                 |        |       |       |
;*  13: |*               |                 |        |       |       |
;*  14: |*               |                 |        |       |       |
;*  15: |*               |                 |        |       |       |
;*  16: |*               |                 |        |       |       |
;*  17: |*               |                 |        |       |       |
;*  18: |*               |                 |        |       |       |
;*  19: |*               |                 |        |       |       |
;*  20: |*               |                 |        |       |       |
;*  21: |*               |                 |        |       |       |
;*  22: |*               |                 |        |       |       |
;*  23: |*               |                 |        |       |       |
;*  24: |*               |                 |        |       |       |
;*      +----------------+                 +--------+       +-------+
;*
;*      Done
;*
;*      Epilog not entirely removed
;*      Collapsed epilog stages       : 1
;*
;*      Prolog not removed
;*      Collapsed prolog stages       : 0
;*
;*      Max amt of load speculation   : 0 bytes
;*
;*      Minimum safe trip count       : 2 (after unrolling)
;*      Min. prof. trip count  (est.) : 3 (after unrolling)
;*
;*      Mem bank conflicts/iter(est.) : { min 0.000, est 0.000, max 0.000 }
;*      Mem bank perf. penalty (est.) : 0.0%
;*
;*
;*      Total cycles (est.)         : 40 + trip_cnt * 25        
;*----------------------------------------------------------------------------*
;*        SINGLE SCHEDULED ITERATION
;*
;*        ||$C$C141||:
;*   0              TICK                               ; [A_U] 
;*   1              NOP     0x4     ; [A_B] 
;*   5              VMV     .S2     SE0++,VBL3        ; [B_S2] |53|  ^ 
;*   6              VMAXUB  .S2     VBL3,SE0++,VBM1   ; [B_S2] |423|  ^ 
;*     ||           VMV     .M2     SE1++,VBL3        ; [B_M2] |53|  ^ 
;*   7              VMAXUB  .M2     VBM1,SE0++,VBM2   ; [B_M2] |423|  ^ 
;*     ||           VMAXUB  .S2     VBL3,SE1++,VBL3   ; [B_S2] |429|  ^ 
;*   8              VMV     .M2     SE0++,VBM5        ; [B_M2] |53|  ^ 
;*   9              VMAXUB  .S2     VBL3,SE1++,VBM6   ; [B_S2] |429|  ^ 
;*     ||           VMAXUB  .M2     VBM5,SE0++,VBL6   ; [B_M2] |423|  ^ 
;*  10              VMAXUB  .S2     VBL6,SE0++,VB13   ; [B_S2] |423|  ^ 
;*     ||           VMV     .M2     SE1++,VBM5        ; [B_M2] |53|  ^ 
;*  11              VBPACKL .C2     VBM6,VBM2,VB12    ; [B_C] |432| 
;*     ||           VMAXUB  .M2     VBM5,SE1++,VBL6   ; [B_M2] |429|  ^ 
;*     ||           VMV     .S2     SE0++,VBL7        ; [B_S2] |53|  ^ 
;*  12              VBPACKH .C2     VBM6,VBM2,VBL7    ; [B_C] |433| 
;*     ||           VMAXUB  .S2     VBL6,SE1++,VB11   ; [B_S2] |429|  ^ 
;*     ||           VMAXUB  .L2     VBL7,SE0++,VBL7   ; [B_L2] |423|  ^ 
;*  13              VMAXUB  .S2     VBL7,SE0++,VB2    ; [B_S2] |423|  ^ 
;*     ||           VMV     .M2     SE1++,VBM0        ; [B_M2] |53|  ^ 
;*  14              VMAXUB  .M2     VBM0,SE1++,VBL5   ; [B_M2] |429|  ^ 
;*     ||           VMV     .S2     SE0++,VBL6        ; [B_S2] |53|  ^ 
;*  15              VMAXUB  .S2     VBL5,SE1++,VB1    ; [B_S2] |429|  ^ 
;*     ||           VMAXUB  .L2     VBL6,SE0++,VBL5   ; [B_L2] |423|  ^ 
;*  16              VMAXUB  .S2     VBL5,SE0++,VBM4   ; [B_S2] |423|  ^ 
;*     ||           VMV     .M2     SE1++,VBL5        ; [B_M2] |53|  ^ 
;*  17              VMAXUB  .S2     VBL5,SE1++,VBM0   ; [B_S2] |429|  ^ 
;*     ||           VMV     .L2     SE0++,VB5         ; [B_L2] |53|  ^ 
;*  18              VMAXUB  .M2     VBM0,SE1++,VBM0   ; [B_M2] |429|  ^ 
;*     ||           VMAXUB  .S2     VB5,SE0++,VBM1    ; [B_S2] |423|  ^ 
;*  19              VMAXUB  .M2     VBM1,SE0++,VBM5   ; [B_M2] |423|  ^ 
;*     ||           VMV     .C2     SE1++,VBL0        ; [B_C] |53|  ^ 
;*  20              VMAXUB  .S2     VBL0,SE1++,VBM1   ; [B_S2] |429|  ^ 
;*     ||           VMV     .L2     SE0++,VBL1        ; [B_L2] |53|  ^ 
;*  21              VBPACKL .C2     VB11,VB13,VB5     ; [B_C] |432| 
;*     ||           VMAXUB  .M2     VBM1,SE1++,VBM1   ; [B_M2] |429|  ^ 
;*     ||           VMAXUB  .S2     VBL1,SE0++,VB3    ; [B_S2] |423|  ^ 
;*  22              VBPACKL .C2     VB1,VB2,VBL0      ; [B_C] |432| 
;*     ||           VMAXUB  .L2     VB3,SE0++,VBM6    ; [B_L2] |423|  ^ 
;*     ||           VMV     .S2     SE1++,VBM2        ; [B_S2] |53|  ^ 
;*  23              VMAXUB  .C2     VBM2,SE1++,VBM2   ; [B_C] |429|  ^ 
;*     ||           VMV     .M2     SE0++,VBL4        ; [B_M2] |53|  ^ 
;*  24              VBPACKL .C2     VBM0,VBM4,VB6     ; [B_C] |432| 
;*     ||           VMAXUB  .M2     VBM2,SE1++,VBM2   ; [B_M2] |429|  ^ 
;*     ||           VMAXUB  .S2     VBL4,SE0++,VBM3   ; [B_S2] |423|  ^ 
;*  25              VBPACKH .C2     VBM2,VBM6,VB9     ; [B_C] |433| 
;*     ||           VMV     .M2     SE1++,VBM7        ; [B_M2] |53|  ^ 
;*  26              VMAXUB  .M2     VBM3,SE0++,VBM7   ; [B_M2] |423|  ^ 
;*     ||           VMAXUB  .C2     VBM7,SE1++,VBM3   ; [B_C] |429|  ^ 
;*  27              VMAXUB  .M2     VBM3,SE1++,VBM3   ; [B_M2] |429|  ^ 
;*     ||           VMV     .S2     SE0++,VBL2        ; [B_S2] |53|  ^ 
;*  28              VSHR    .L2     VB12,0x8,VBL4     ; [B_L2] |53| 
;*     ||           VMAXUB  .S2     VBL2,SE0++,VBL2   ; [B_S2] |423|  ^ 
;*     ||           VMV     .M2     SE1++,VBL3        ; [B_M2] |53|  ^ 
;*  29              VBPACKH .C2     VB1,VB2,VBM1      ; [B_C] |433| 
;*     ||           VMAXUB  .S2     VBL2,SE0++,VB8    ; [B_S2] |423|  ^ 
;*     ||           VMAXUB  .L2     VBL3,SE1++,VB2    ; [B_L2] |429|  ^ 
;*  30              VSHR    .L2     VB5,0x8,VBL5      ; [B_L2] |53| 
;*     ||           VBPACKL .C2     VBM1,VBM5,VBL2    ; [B_C] |432| 
;*     ||           VMAXUB  .M2     VB2,SE1++,VB7     ; [B_M2] |429|  ^ 
;*  31              VSHR    .L2     VB6,0x8,VBL1      ; [B_L2] |53| 
;*     ||           VBPACKH .C2     VBM1,VBM5,VBL3    ; [B_C] |433| 
;*  32              VBPACKL .C2     VBM2,VBM6,VBL4    ; [B_C] |432| 
;*  33              VSHR    .L2     VBL0,0x8,VBM5     ; [B_L2] |53| 
;*     ||           VBPACKH .C2     VB7,VB8,VB10      ; [B_C] |433| 
;*  34              VBPACKH .C2     VB11,VB13,VBL6    ; [B_C] |433| 
;*  35              VMAXUB  .L2     VBL7,VBL4,VB0     ; [B_L2] |437| 
;*  36              NOP     0x1     ; [A_B] 
;*  37              VMAXUB  .M2     VB12,VB0,VB0      ; [B_M2] |437| 
;*  38              VBPACKH .C2     VBM0,VBM4,VBL6    ; [B_C] |433| 
;*     ||           VSHR    .L2     VBL4,0x8,VBM2     ; [B_L2] |53| 
;*  39              VMAXUB  .L2     VBL6,VBL5,VBM0    ; [B_L2] |437| 
;*     ||           VBPACKL .C2     VBM3,VBM7,VBL5    ; [B_C] |432| 
;*  40              VMAXUB  .M2     VB5,VBM0,VB3      ; [B_M2] |437| 
;*  41              VHHMV   .L2     VB0,VB0,VB0       ; [B_L2] |445| 
;*     ||           VHHMV   .C2     VB3,VB3,VB5       ; [B_C] |445| 
;*  42              VMAXUB  .M2     VBM1,VBM5,VB4     ; [B_M2] |437| 
;*     ||           VBPACKH .C2     VBM3,VBM7,VBL0    ; [B_C] |433| 
;*  43              VST32B  .D2     VB0,*D0[SA0++]    ; [A_D2] |441| 
;*     ||           VSHR    .L2     VBL5,0x8,VBL6     ; [B_L2] |53| 
;*     ||           VBPACKL .C2     VB7,VB8,VBL1      ; [B_C] |432| 
;*  44              VST32B  .D2     VB0,*D0[SA0++]    ; [A_D2] |445| 
;*     ||           VMAXUB  .S2     VBL0,VB4,VB0      ; [B_S2] |437| 
;*     ||           VMAXUB  .L2     VBL6,VBL1,VBM1    ; [B_L2] |437| 
;*  45              VST32B  .D2     VB3,*D0[SA0++]    ; [A_D2] |441| 
;*     ||           VHHMV   .C2     VB0,VB0,VB0       ; [B_C] |445| 
;*     ||           VMAXUB  .M2     VB6,VBM1,VB4      ; [B_M2] |437| 
;*  46              VST32B  .D2     VB5,*D0[SA0++]    ; [A_D2] |445| 
;*     ||           VSHR    .L2     VBL2,0x8,VBL4     ; [B_L2] |53| 
;*  47              VST32B  .D2     VB0,*D0[SA0++]    ; [A_D2] |441| 
;*     ||           VMAXUB  .M2     VB9,VBM2,VB3      ; [B_M2] |437| 
;*  48              VST32B  .D2     VB0,*D0[SA0++]    ; [A_D2] |445| 
;*     ||           VMAXUB  .L2     VBL4,VB3,VB3      ; [B_L2] |437| 
;*     ||           VMAXUB  .S2     VBL0,VBL6,VBL0    ; [B_S2] |437| 
;*  49              VST32B  .D2     VB4,*D0[SA0++]    ; [A_D2] |441| 
;*     ||           VHHMV   .L2     VB4,VB4,VB6       ; [B_L2] |445| 
;*  50              VMAXUB  .S2     VBL3,VBL4,VBL3    ; [B_S2] |437| 
;*     ||           VMAXUB  .L2     VBL5,VBL0,VB4     ; [B_L2] |437| 
;*  51              VMAXUB  .S2     VBL2,VBL3,VB0     ; [B_S2] |437| 
;*     ||           VHHMV   .L2     VB4,VB4,VB0       ; [B_L2] |445| 
;*  52              VST32B  .D2     VB6,*D0[SA0++]    ; [A_D2] |445| 
;*     ||           VHHMV   .C2     VB3,VB3,VB1       ; [B_C] |445| 
;*     ||           VSHR    .L2     VBL1,0x8,VBL2     ; [B_L2] |53| 
;*  53              VST32B  .D2     VB0,*D0[SA0++]    ; [A_D2] |441| 
;*     ||           VHHMV   .C2     VB0,VB0,VB2       ; [B_C] |445| 
;*  54              NOP     0x2     ; [A_B] 
;*  56              VST32B  .D2     VB2,*D0[SA0++]    ; [A_D2] |445| 
;*  57              VST32B  .D2     VB3,*D0[SA0++]    ; [A_D2] |441| 
;*     ||           VMAXUB  .L2     VB10,VBL2,VBL2    ; [B_L2] |437| 
;*  58              VST32B  .D2     VB1,*D0[SA0++]    ; [A_D2] |445| 
;*     ||           VMAXUB  .S2     VBL1,VBL2,VB1     ; [B_S2] |437| 
;*  59              VST32B  .D2     VB4,*D0[SA0++]    ; [A_D2] |441| 
;*  60              VST32B  .D2     VB0,*D0[SA0++]    ; [A_D2] |445| 
;*  61              VST32B  .D2     VB1,*D0[SA0++]    ; [A_D2] |441| 
;*     ||           VHHMV   .L2     VB1,VB1,VB1       ; [B_L2] |445| 
;*  62              NOP     0x2     ; [A_B] 
;*  64              VST32B  .D2     VB1,*D0[SA0++]    ; [A_D2] |445| 
;*     ||           BNL     .B1     ||$C$C141||       ; [A_B] |417| 
;*  65              ; BRANCHCC OCCURS {||$C$C141||}   ; [] |417| 
;*----------------------------------------------------------------------------*
||$C$L6||:    ; PIPED LOOP PROLOG
;          EXCLUSIVE CPU CYCLES: 18
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 53,column 4,is_stmt,isa 0

           EXT     .L1     AL0,0x20,0x20,AL0 ; [A_L1] 
||         VBPACKL .C2     VB1,VB7,VBL0      ; [B_C] |432| <0,22> 
||         VMAXUB  .L2     VB3,SE0++,VBM1    ; [B_L2] |423| <0,22>  ^ 
||         VMV     .S2     SE1++,VBM3        ; [B_S2] |53| <0,22>  ^ 

           NLCINIT .S1     AL0,0x1,1         ; [A_S1] 
||         VMV     .M2     SE0++,VBL0        ; [B_M2] |53| <0,23>  ^ 
||         VMAXUB  .C2     VBM3,SE1++,VBM3   ; [B_C] |429| <0,23>  ^ 

	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 423,column 5,is_stmt,isa 0

           TICK                               ; [A_U] <0,0> 
||         VBPACKL .C2     VBM0,VBM4,VB6     ; [B_C] |432| <0,24> 
||         VMAXUB  .S2     VBL0,SE0++,VBM7   ; [B_S2] |423| <0,24>  ^ 
||         VMAXUB  .M2     VBM3,SE1++,VBM5   ; [B_M2] |429| <0,24>  ^ 

	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 53,column 4,is_stmt,isa 0

           VBPACKH .C2     VBM5,VBM1,VB9     ; [B_C] |433| <0,25> 
||         VMV     .M2     SE1++,VBM3        ; [B_M2] |53| <0,25>  ^ 
||         TICK                               ; [A_U] <1,0> 

	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 423,column 5,is_stmt,isa 0

           VMAXUB  .M2     VBM7,SE0++,VBM7   ; [B_M2] |423| <0,26>  ^ 
||         VMAXUB  .C2     VBM3,SE1++,VBM3   ; [B_C] |429| <0,26>  ^ 

	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 53,column 4,is_stmt,isa 0

           VMV     .S2     SE0++,VBL1        ; [B_S2] |53| <0,27>  ^ 
||         VMAXUB  .M2     VBM3,SE1++,VBM3   ; [B_M2] |429| <0,27>  ^ 

           VSHR    .L2     VB0,0x8,VBL6      ; [B_L2] |53| <0,28> 
||         VMAXUB  .S2     VBL1,SE0++,VBL2   ; [B_S2] |423| <0,28>  ^ 
||         VMV     .M2     SE1++,VBL1        ; [B_M2] |53| <0,28>  ^ 

	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 384,column 3,is_stmt,isa 0

           VLD8D   .D1     *D1(0),VB10       ; [A_D1] |384| 
||         VMAXUB  .S2     VBL2,SE0++,VB8    ; [B_S2] |423| <0,29>  ^ 
||         VMAXUB  .L2     VBL1,SE1++,VB7    ; [B_L2] |429| <0,29>  ^ 
||         VBPACKH .C2     VB1,VB7,VBM1      ; [B_C] |433| <0,29> 

	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 53,column 4,is_stmt,isa 0

           VMAXUB  .M2     VB7,SE1++,VB7     ; [B_M2] |429| <0,30>  ^ 
||         VBPACKL .C2     VB12,VB2,VBL2     ; [B_C] |432| <0,30> 
||         VSHR    .L2     VB5,0x8,VBL5      ; [B_L2] |53| <0,30> 
||         VMV     .S2     SE0++,VBL1        ; [B_S2] |53| <1,5>  ^ 

           VBPACKH .C2     VB12,VB2,VBL3     ; [B_C] |433| <0,31> 
||         VSHR    .L2     VB6,0x8,VBL1      ; [B_L2] |53| <0,31> 
||         VMAXUB  .S2     VBL1,SE0++,VB2    ; [B_S2] |423| <1,6>  ^ 
||         VMV     .M2     SE1++,VBL1        ; [B_M2] |53| <1,6>  ^ 

	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 423,column 5,is_stmt,isa 0

           VBPACKL .C2     VBM5,VBM1,VBL4    ; [B_C] |432| <0,32> 
||         VMAXUB  .S2     VBL1,SE1++,VBL1   ; [B_S2] |429| <1,7>  ^ 
||         VMAXUB  .M2     VB2,SE0++,VB2     ; [B_M2] |423| <1,7>  ^ 

	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 53,column 4,is_stmt,isa 0

           VBPACKH .C2     VB7,VB8,VB10      ; [B_C] |433| <0,33> 
||         VSHR    .L2     VBL0,0x8,VBM5     ; [B_L2] |53| <0,33> 
||         VMV     .M2     SE0++,VBM5        ; [B_M2] |53| <1,8>  ^ 

	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 423,column 5,is_stmt,isa 0

           VBPACKH .C2     VB11,VBM6,VBL6    ; [B_C] |433| <0,34> 
||         VMAXUB  .S2     VBL1,SE1++,VBM6   ; [B_S2] |429| <1,9>  ^ 
||         VMAXUB  .M2     VBM5,SE0++,VBL7   ; [B_M2] |423| <1,9>  ^ 

	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 53,column 4,is_stmt,isa 0

           SAOPEN  .C2     VB10,0            ; [B_C] |395| 
||         VMAXUB  .L2     VBL4,VBL6,VB11    ; [B_L2] |437| <0,35> 
||         VMAXUB  .S2     VBL7,SE0++,VB13   ; [B_S2] |423| <1,10>  ^ 
||         VMV     .M2     SE1++,VBM5        ; [B_M2] |53| <1,10>  ^ 

           VMV     .S2     SE0++,VBL6        ; [B_S2] |53| <1,11>  ^ 
||         VMAXUB  .M2     VBM5,SE1++,VBL7   ; [B_M2] |429| <1,11>  ^ 
||         VBPACKL .C2     VBM6,VB2,VB12     ; [B_C] |432| <1,11> 

	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 423,column 5,is_stmt,isa 0

           VMAXUB  .M2     VB0,VB11,VB0      ; [B_M2] |437| <0,37> 
||         VMAXUB  .L2     VBL6,SE0++,VBL7   ; [B_L2] |423| <1,12>  ^ 
||         VMAXUB  .S2     VBL7,SE1++,VB11   ; [B_S2] |429| <1,12>  ^ 
||         VBPACKH .C2     VBM6,VB2,VBL7     ; [B_C] |433| <1,12> 

	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 53,column 4,is_stmt,isa 0

           VSHR    .L2     VBL4,0x8,VBM2     ; [B_L2] |53| <0,38> 
||         VBPACKH .C2     VBM0,VBM4,VBL6    ; [B_C] |433| <0,38> 
||         VMAXUB  .S2     VBL7,SE0++,VB2    ; [B_S2] |423| <1,13>  ^ 
||         VMV     .M2     SE1++,VBM0        ; [B_M2] |53| <1,13>  ^ 

           MV      .D2     A6,D0             ; [A_D2] 
||         ADDD    .D1     A0,0xfffffffe,A0  ; [A_D1] init epilog collapse predicate
||         SHLW    .L1     A0,0x3,D1         ; [A_L1] |417| 
||         VMAXUB  .L2     VBL6,VBL5,VBM0    ; [B_L2] |437| <0,39> 
||         VBPACKL .C2     VBM3,VBM7,VBL5    ; [B_C] |432| <0,39> 
||         VMV     .S2     SE0++,VBL6        ; [B_S2] |53| <1,14>  ^ 
||         VMAXUB  .M2     VBM0,SE1++,VBL5   ; [B_M2] |429| <1,14>  ^ 

;** --------------------------------------------------------------------------*
||$C$L7||:    ; PIPED LOOP KERNEL
;          EXCLUSIVE CPU CYCLES: 25
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 423,column 5,is_stmt,isa 0

           VMAXUB  .M2     VB5,VBM0,VB3      ; [B_M2] |437| <0,40> 
||         VMAXUB  .L2     VBL6,SE0++,VBL5   ; [B_L2] |423| <1,15>  ^ 
||         VMAXUB  .S2     VBL5,SE1++,VB1    ; [B_S2] |429| <1,15>  ^ 

	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 53,column 4,is_stmt,isa 0

           VHHMV   .L2     VB0,VB0,VB0       ; [B_L2] |445| <0,41> 
||         VHHMV   .C2     VB3,VB3,VB5       ; [B_C] |445| <0,41> 
||         VMAXUB  .S2     VBL5,SE0++,VBM4   ; [B_S2] |423| <1,16>  ^ 
||         VMV     .M2     SE1++,VBL5        ; [B_M2] |53| <1,16>  ^ 

           VMAXUB  .M2     VBM1,VBM5,VB4     ; [B_M2] |437| <0,42> 
||         VBPACKH .C2     VBM3,VBM7,VBL0    ; [B_C] |433| <0,42> 
||         VMV     .L2     SE0++,VB5         ; [B_L2] |53| <1,17>  ^ 
||         VMAXUB  .S2     VBL5,SE1++,VBM0   ; [B_S2] |429| <1,17>  ^ 

           VST32B  .D2     VB0,*D0[SA0++]    ; [A_D2] |441| <0,43> 
||         VSHR    .L2     VBL5,0x8,VBL6     ; [B_L2] |53| <0,43> 
||         VBPACKL .C2     VB7,VB8,VBL1      ; [B_C] |432| <0,43> 
||         VMAXUB  .S2     VB5,SE0++,VBM1    ; [B_S2] |423| <1,18>  ^ 
||         VMAXUB  .M2     VBM0,SE1++,VBM0   ; [B_M2] |429| <1,18>  ^ 

           VST32B  .D2     VB0,*D0[SA0++]    ; [A_D2] |445| <0,44> 
||         VMAXUB  .L2     VBL6,VBL1,VBM1    ; [B_L2] |437| <0,44> 
||         VMAXUB  .S2     VBL0,VB4,VB0      ; [B_S2] |437| <0,44> 
||         VMAXUB  .M2     VBM1,SE0++,VBM5   ; [B_M2] |423| <1,19>  ^ 
||         VMV     .C2     SE1++,VBL0        ; [B_C] |53| <1,19>  ^ 

           VST32B  .D2     VB3,*D0[SA0++]    ; [A_D2] |441| <0,45> 
||         VHHMV   .C2     VB0,VB0,VB0       ; [B_C] |445| <0,45> 
||         VMAXUB  .M2     VB6,VBM1,VB4      ; [B_M2] |437| <0,45> 
||         VMV     .L2     SE0++,VBL1        ; [B_L2] |53| <1,20>  ^ 
||         VMAXUB  .S2     VBL0,SE1++,VBM1   ; [B_S2] |429| <1,20>  ^ 

           VST32B  .D2     VB5,*D0[SA0++]    ; [A_D2] |445| <0,46> 
||         VSHR    .L2     VBL2,0x8,VBL4     ; [B_L2] |53| <0,46> 
||         VMAXUB  .S2     VBL1,SE0++,VB3    ; [B_S2] |423| <1,21>  ^ 
||         VMAXUB  .M2     VBM1,SE1++,VBM1   ; [B_M2] |429| <1,21>  ^ 
||         VBPACKL .C2     VB11,VB13,VB5     ; [B_C] |432| <1,21> 

           VST32B  .D2     VB0,*D0[SA0++]    ; [A_D2] |441| <0,47> 
||         VMAXUB  .M2     VB9,VBM2,VB3      ; [B_M2] |437| <0,47> 
||         VBPACKL .C2     VB1,VB2,VBL0      ; [B_C] |432| <1,22> 
||         VMAXUB  .L2     VB3,SE0++,VBM6    ; [B_L2] |423| <1,22>  ^ 
||         VMV     .S2     SE1++,VBM2        ; [B_S2] |53| <1,22>  ^ 

           VST32B  .D2     VB0,*D0[SA0++]    ; [A_D2] |445| <0,48> 
||         VMAXUB  .L2     VBL4,VB3,VB3      ; [B_L2] |437| <0,48> 
||         VMAXUB  .S2     VBL0,VBL6,VBL0    ; [B_S2] |437| <0,48> 
||         VMV     .M2     SE0++,VBL4        ; [B_M2] |53| <1,23>  ^ 
||         VMAXUB  .C2     VBM2,SE1++,VBM2   ; [B_C] |429| <1,23>  ^ 

	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 423,column 5,is_stmt,isa 0

           VST32B  .D2     VB4,*D0[SA0++]    ; [A_D2] |441| <0,49> 
||         VHHMV   .L2     VB4,VB4,VB6       ; [B_L2] |445| <0,49> 
||         VBPACKL .C2     VBM0,VBM4,VB6     ; [B_C] |432| <1,24> 
||         VMAXUB  .S2     VBL4,SE0++,VBM3   ; [B_S2] |423| <1,24>  ^ 
||         VMAXUB  .M2     VBM2,SE1++,VBM2   ; [B_M2] |429| <1,24>  ^ 

	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 53,column 4,is_stmt,isa 0

           VMAXUB  .L2     VBL5,VBL0,VB4     ; [B_L2] |437| <0,50> 
||         VMAXUB  .S2     VBL3,VBL4,VBL3    ; [B_S2] |437| <0,50> 
||         VBPACKH .C2     VBM2,VBM6,VB9     ; [B_C] |433| <1,25> 
||         VMV     .M2     SE1++,VBM7        ; [B_M2] |53| <1,25>  ^ 
||         TICK                               ; [A_U] <2,0> 

	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 423,column 5,is_stmt,isa 0

           VHHMV   .L2     VB4,VB4,VB0       ; [B_L2] |445| <0,51> 
||         VMAXUB  .S2     VBL2,VBL3,VB0     ; [B_S2] |437| <0,51> 
||         VMAXUB  .M2     VBM3,SE0++,VBM7   ; [B_M2] |423| <1,26>  ^ 
||         VMAXUB  .C2     VBM7,SE1++,VBM3   ; [B_C] |429| <1,26>  ^ 

	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 53,column 4,is_stmt,isa 0

           VST32B  .D2     VB6,*D0[SA0++]    ; [A_D2] |445| <0,52> 
||         VHHMV   .C2     VB3,VB3,VB1       ; [B_C] |445| <0,52> 
||         VSHR    .L2     VBL1,0x8,VBL2     ; [B_L2] |53| <0,52> 
||         VMV     .S2     SE0++,VBL2        ; [B_S2] |53| <1,27>  ^ 
||         VMAXUB  .M2     VBM3,SE1++,VBM3   ; [B_M2] |429| <1,27>  ^ 

           VST32B  .D2     VB0,*D0[SA0++]    ; [A_D2] |441| <0,53> 
||         VHHMV   .C2     VB0,VB0,VB2       ; [B_C] |445| <0,53> 
||         VSHR    .L2     VB12,0x8,VBL4     ; [B_L2] |53| <1,28> 
||         VMAXUB  .S2     VBL2,SE0++,VBL2   ; [B_S2] |423| <1,28>  ^ 
||         VMV     .M2     SE1++,VBL3        ; [B_M2] |53| <1,28>  ^ 

	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 423,column 5,is_stmt,isa 0

           VMAXUB  .S2     VBL2,SE0++,VB8    ; [B_S2] |423| <1,29>  ^ 
||         VMAXUB  .L2     VBL3,SE1++,VB2    ; [B_L2] |429| <1,29>  ^ 
||         VBPACKH .C2     VB1,VB2,VBM1      ; [B_C] |433| <1,29> 

	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 53,column 4,is_stmt,isa 0

           VMAXUB  .M2     VB2,SE1++,VB7     ; [B_M2] |429| <1,30>  ^ 
||         VBPACKL .C2     VBM1,VBM5,VBL2    ; [B_C] |432| <1,30> 
||         VSHR    .L2     VB5,0x8,VBL5      ; [B_L2] |53| <1,30> 
|| [ A0]   VMV     .S2     SE0++,VBL3        ; [B_S2] |53| <2,5>  ^ 

           VST32B  .D2     VB2,*D0[SA0++]    ; [A_D2] |445| <0,56> 
||         VBPACKH .C2     VBM1,VBM5,VBL3    ; [B_C] |433| <1,31> 
||         VSHR    .L2     VB6,0x8,VBL1      ; [B_L2] |53| <1,31> 
|| [ A0]   VMAXUB  .S2     VBL3,SE0++,VBM1   ; [B_S2] |423| <2,6>  ^ 
|| [ A0]   VMV     .M2     SE1++,VBL3        ; [B_M2] |53| <2,6>  ^ 

	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 423,column 5,is_stmt,isa 0

           VST32B  .D2     VB3,*D0[SA0++]    ; [A_D2] |441| <0,57> 
||         VMAXUB  .L2     VB10,VBL2,VBL2    ; [B_L2] |437| <0,57> 
||         VBPACKL .C2     VBM2,VBM6,VBL4    ; [B_C] |432| <1,32> 
|| [ A0]   VMAXUB  .S2     VBL3,SE1++,VBL3   ; [B_S2] |429| <2,7>  ^ 
|| [ A0]   VMAXUB  .M2     VBM1,SE0++,VBM2   ; [B_M2] |423| <2,7>  ^ 

	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 53,column 4,is_stmt,isa 0

           VST32B  .D2     VB1,*D0[SA0++]    ; [A_D2] |445| <0,58> 
||         VMAXUB  .S2     VBL1,VBL2,VB1     ; [B_S2] |437| <0,58> 
||         VBPACKH .C2     VB7,VB8,VB10      ; [B_C] |433| <1,33> 
||         VSHR    .L2     VBL0,0x8,VBM5     ; [B_L2] |53| <1,33> 
|| [ A0]   VMV     .M2     SE0++,VBM5        ; [B_M2] |53| <2,8>  ^ 

	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 423,column 5,is_stmt,isa 0

           VST32B  .D2     VB4,*D0[SA0++]    ; [A_D2] |441| <0,59> 
||         VBPACKH .C2     VB11,VB13,VBL6    ; [B_C] |433| <1,34> 
|| [ A0]   VMAXUB  .S2     VBL3,SE1++,VBM6   ; [B_S2] |429| <2,9>  ^ 
|| [ A0]   VMAXUB  .M2     VBM5,SE0++,VBL6   ; [B_M2] |423| <2,9>  ^ 

	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 53,column 4,is_stmt,isa 0

           VST32B  .D2     VB0,*D0[SA0++]    ; [A_D2] |445| <0,60> 
||         VMAXUB  .L2     VBL7,VBL4,VB0     ; [B_L2] |437| <1,35> 
|| [ A0]   VMAXUB  .S2     VBL6,SE0++,VB13   ; [B_S2] |423| <2,10>  ^ 
|| [ A0]   VMV     .M2     SE1++,VBM5        ; [B_M2] |53| <2,10>  ^ 

           VST32B  .D2     VB1,*D0[SA0++]    ; [A_D2] |441| <0,61> 
||         VHHMV   .L2     VB1,VB1,VB1       ; [B_L2] |445| <0,61> 
|| [ A0]   VMV     .S2     SE0++,VBL7        ; [B_S2] |53| <2,11>  ^ 
|| [ A0]   VMAXUB  .M2     VBM5,SE1++,VBL6   ; [B_M2] |429| <2,11>  ^ 
||         VBPACKL .C2     VBM6,VBM2,VB12    ; [B_C] |432| <2,11> 

	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 423,column 5,is_stmt,isa 0

           VMAXUB  .M2     VB12,VB0,VB0      ; [B_M2] |437| <1,37> 
|| [ A0]   VMAXUB  .L2     VBL7,SE0++,VBL7   ; [B_L2] |423| <2,12>  ^ 
|| [ A0]   VMAXUB  .S2     VBL6,SE1++,VB11   ; [B_S2] |429| <2,12>  ^ 
||         VBPACKH .C2     VBM6,VBM2,VBL7    ; [B_C] |433| <2,12> 

	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 53,column 4,is_stmt,isa 0

           VSHR    .L2     VBL4,0x8,VBM2     ; [B_L2] |53| <1,38> 
||         VBPACKH .C2     VBM0,VBM4,VBL6    ; [B_C] |433| <1,38> 
|| [ A0]   VMAXUB  .S2     VBL7,SE0++,VB2    ; [B_S2] |423| <2,13>  ^ 
|| [ A0]   VMV     .M2     SE1++,VBM0        ; [B_M2] |53| <2,13>  ^ 

   [ A0]   ADDW    .D1     A0,0xffffffff,A0  ; [A_D1] |417| <0,64> collapsing predicate control
||         BNL     .B1     ||$C$L7||         ; [A_B] |417| <0,64> 
||         VST32B  .D2     VB1,*D0[SA0++]    ; [A_D2] |445| <0,64> 
||         VMAXUB  .L2     VBL6,VBL5,VBM0    ; [B_L2] |437| <1,39> 
||         VBPACKL .C2     VBM3,VBM7,VBL5    ; [B_C] |432| <1,39> 
|| [ A0]   VMV     .S2     SE0++,VBL6        ; [B_S2] |53| <2,14>  ^ 
|| [ A0]   VMAXUB  .M2     VBM0,SE1++,VBL5   ; [B_M2] |429| <2,14>  ^ 

;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*
;*      Loop found in file               : tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c
;*      Loop source line                 : 452
;*      Loop opening brace source line   : 453
;*      Loop closing brace source line   : 477
;*      Known Minimum Trip Count         : 1                    
;*      Known Max Trip Count Factor      : 1
;*      Loop Carried Dependency Bound(^) : 3
;*      Unpartitioned Resource Bound     : 3
;*      Partitioned Resource Bound       : 3 (pre-sched)
;*
;*      Searching for software pipeline schedule at ...
;*         ii = 3  Did not find schedule
;*         ii = 4  Schedule found with 5 iterations in parallel
;*
;*      Partitioned Resource Bound(*)    : 3 (post-sched)
;*
;*      Constant Extension #0 Used [C0]  : 0
;*      Constant Extension #1 Used [C1]  : 0
;*
;*      Resource Partition (may include "post-sched" split/spill moves):
;*
;*                                                A-side   B-side
;*      .L units                                     0        0     
;*      .S units                                     0        0     
;*      .M units                                     0        0     
;*      .N units                                     0        0     
;*      .D units                                     2        -     
;*      .B units                                     1        -     
;*      .C units                                     -        2     
;*      .P units                                     -        0     
;*
;*      .M/.N units                                  0        0     
;*      .L/.S units                                  0        1     
;*      .L/.S/.C units                               0        1     
;*      .L/.S/.C/.M units                            0        8     
;*      .L/.S/.C/.M/.D units                         0        0     
;*
;*      .X cross paths                               0        0     
;*
;*      Bound(.D1 .D2 .D)                            1        -     
;*      Bound(.M .N .MN)                             0        0     
;*      Bound(.L .S .LS)                             0        1     
;*      Bound(.L .S .C .LS .LSC)                     0        2     
;*      Bound(.L .S .C .M .LS .LSC .LSCM)            0        3*    
;*      Bound(.L .S .C .M .D .LS .LSC .LSCM .LSCMD)  1        3*    
;*
;*      Register Usage Tables:
;*
;*      +----------------------------------+----------------------------------+
;*      |              ASIDE               |              BSIDE               |
;*      +----------------+--------+--------+----------------+--------+--------+
;*      |      "Axx"     |  "ALx" |  "AMx" |     "VBxx"     | "VBLx" | "VBMx" |
;*      +----------------|--------|--------|----------------|--------|--------+
;*      |0000000000111111|00000000|00000000|0000000000111111|00000000|00000000|
;*      |0123456789012345|01234567|01234567|0123456789012345|01234567|01234567|
;*      +----------------|--------|--------|----------------|--------|--------+
;*   0: |***             |        |        |*               |**      |***     |
;*   1: |***             |        |        |                |**      |*       |
;*   2: |***             |        |        |*               |***     |**      |
;*   3: |***             |        |        |*               |**      |**      |
;*      +----------------+--------+--------+----------------+--------+--------+
;*
;*      +----------------+                 +--------+       +-------+
;*      |      "Dxx"     |                 |  "Px"  |       |"CUCRx"|
;*      +----------------+                 +--------+       +-------+
;*      |0000000000111111|                 |00000000|       |0000   |
;*      |0123456789012345|                 |01234567|       |0123   |
;*      +----------------+                 +--------+       +-------+
;*   0: |*               |                 |        |       |       |
;*   1: |*               |                 |        |       |       |
;*   2: |*               |                 |        |       |       |
;*   3: |*               |                 |        |       |       |
;*      +----------------+                 +--------+       +-------+
;*
;*      Done
;*
;*      Collapsed epilog stages       : 4
;*      Prolog not entirely removed
;*      Collapsed prolog stages       : 3
;*
;*      Max amt of load speculation   : 0 bytes
;*
;*      Minimum safe trip count       : 1
;*      Min. prof. trip count  (est.) : 2
;*
;*      Mem bank conflicts/iter(est.) : { min 0.000, est 0.000, max 0.000 }
;*      Mem bank perf. penalty (est.) : 0.0%
;*
;*
;*      Total cycles (est.)         : 14 + trip_cnt * 4        
;*----------------------------------------------------------------------------*
;*        SINGLE SCHEDULED ITERATION
;*
;*        ||$C$C58||:
;*   0              TICK                               ; [A_U] 
;*   1              VMV     .S2     SE0++,VBL0        ; [B_S2] |53|  ^ 
;*     ||           VMV     .M2     SE1++,VBM2        ; [B_M2] |53|  ^ 
;*   2              VMAXUB  .S2     VBL0,SE0++,VBL0   ; [B_S2] |457|  ^ 
;*     ||           VMAXUB  .M2     VBM2,SE1++,VBM0   ; [B_M2] |462|  ^ 
;*   3              VMAXUB  .S2     VBL0,SE0++,VBM1   ; [B_S2] |457|  ^ 
;*     ||           VMAXUB  .M2     VBM0,SE1++,VBM0   ; [B_M2] |462|  ^ 
;*   4              VBPACKL .C2     VBM0,VBM1,VB0     ; [B_C] |465| 
;*   5              NOP     0x1     ; [A_B] 
;*   6              VBPACKH .C2     VBM0,VBM1,VBL1    ; [B_C] |466| 
;*   7              NOP     0x1     ; [A_B] 
;*   8              VMVDLY4 .N2     VB0,VBL0          ; [B_N2] |465| Split a long life
;*     ||           VSHR    .L2     VB0,0x8,VBL2      ; [B_L2] |53| 
;*   9              NOP     0x3     ; [A_B] 
;*  12              VMAXUB  .S2     VBL1,VBL2,VBL1    ; [B_S2] |469| 
;*  13              VMAXUB  .L2     VBL0,VBL1,VB0     ; [B_L2] |469| 
;*  14              VST32B  .D2     VB0,*D0[SA0++]    ; [A_D2] |472| 
;*     ||           VHHMV   .L2     VB0,VB0,VB0       ; [B_L2] |475| 
;*  15              NOP     0x2     ; [A_B] 
;*  17              VST32B  .D2     VB0,*D0[SA0++]    ; [A_D2] |475| 
;*     ||           BNL     .B1     ||$C$C58||        ; [A_B] |452| 
;*  18              ; BRANCHCC OCCURS {||$C$C58||}    ; [] |452| 
;*----------------------------------------------------------------------------*
||$C$L8||:    ; PIPED LOOP EPILOG AND PROLOG
;          EXCLUSIVE CPU CYCLES: 26

           MVKU32  .L1     0x3,A2            ; [A_L1] init prolog collapse predicate
||         SUBW    .D1     A1,D1,A1          ; [A_D1] |53| 
||         VMAXUB  .M2     VB5,VBM0,VB1      ; [B_M2] |437| <2,40> 

	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 445,column 5,is_stmt,isa 0

           MV      .D2     A1,D1             ; [A_D2] 
||         MV      .D1     A1,AL0            ; [A_D1] 
||         VHHMV   .L2     VB0,VB0,VB0       ; [B_L2] |445| <2,41> 
||         VHHMV   .C2     VB1,VB1,VB5       ; [B_C] |445| <2,41> 

	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 433,column 5,is_stmt,isa 0

           ADDD    .D1     D1,0xffffffff,A0  ; [A_D1] init epilog collapse predicate
||         EXT     .L1     AL0,0x20,0x20,AL0 ; [A_L1] 
||         VMAXUB  .M2     VBM1,VBM5,VBL7    ; [B_M2] |437| <2,42> 
||         VBPACKH .C2     VBM3,VBM7,VBL6    ; [B_C] |433| <2,42> 

	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 53,column 4,is_stmt,isa 0

           NLCINIT .S1     AL0,0x1,4         ; [A_S1] 
||         VST32B  .D2     VB0,*D0[SA0++]    ; [A_D2] |441| <2,43> 
||         VSHR    .L2     VBL5,0x8,VBL7     ; [B_L2] |53| <2,43> 
||         VBPACKL .C2     VB7,VB8,VBL1      ; [B_C] |432| <2,43> 

           VMV     .M2     SE0++,VBL0        ; [B_M2] |53| <0,1>  ^ 
||         TICK                               ; [A_U] <0,0> 
||         VST32B  .D2     VB0,*D0[SA0++]    ; [A_D2] |445| <2,44> 
||         VMAXUB  .L2     VBL6,VBL1,VBM1    ; [B_L2] |437| <2,44> 
||         VMAXUB  .S2     VBL0,VBL7,VB0     ; [B_S2] |437| <2,44> 

	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 437,column 5,is_stmt,isa 0

           VST32B  .D2     VB1,*D0[SA0++]    ; [A_D2] |441| <2,45> 
||         VHHMV   .C2     VB0,VB0,VB0       ; [B_C] |445| <2,45> 
||         VMAXUB  .M2     VB6,VBM1,VB1      ; [B_M2] |437| <2,45> 

	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 53,column 4,is_stmt,isa 0

           VST32B  .D2     VB5,*D0[SA0++]    ; [A_D2] |445| <2,46> 
||         VSHR    .L2     VBL2,0x8,VBL4     ; [B_L2] |53| <2,46> 

           VMV     .L2     SE1++,VBM2        ; [B_L2] |53| <0,1>  ^ 
||         VST32B  .D2     VB0,*D0[SA0++]    ; [A_D2] |441| <2,47> 
||         VMAXUB  .M2     VB9,VBM2,VB2      ; [B_M2] |437| <2,47> 

	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 437,column 5,is_stmt,isa 0

           VST32B  .D2     VB0,*D0[SA0++]    ; [A_D2] |445| <2,48> 
||         VMAXUB  .L2     VBL4,VB2,VB3      ; [B_L2] |437| <2,48> 
||         VMAXUB  .S2     VBL6,VBL7,VBL6    ; [B_S2] |437| <2,48> 

	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 441,column 5,is_stmt,isa 0

           VST32B  .D2     VB1,*D0[SA0++]    ; [A_D2] |441| <2,49> 
||         VHHMV   .L2     VB1,VB1,VB6       ; [B_L2] |445| <2,49> 

	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 437,column 5,is_stmt,isa 0

           VMAXUB  .L2     VBL5,VBL6,VB1     ; [B_L2] |437| <2,50> 
||         VMAXUB  .S2     VBL3,VBL4,VBL3    ; [B_S2] |437| <2,50> 

           VHHMV   .L2     VB1,VB1,VB0       ; [B_L2] |445| <2,51> 
||         VMAXUB  .S2     VBL2,VBL3,VB0     ; [B_S2] |437| <2,51> 

	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 53,column 4,is_stmt,isa 0

           VST32B  .D2     VB6,*D0[SA0++]    ; [A_D2] |445| <2,52> 
||         VHHMV   .C2     VB3,VB3,VB4       ; [B_C] |445| <2,52> 
||         VSHR    .L2     VBL1,0x8,VBL2     ; [B_L2] |53| <2,52> 

	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 441,column 5,is_stmt,isa 0

           VST32B  .D2     VB0,*D0[SA0++]    ; [A_D2] |441| <2,53> 
||         VHHMV   .C2     VB0,VB0,VB2       ; [B_C] |445| <2,53> 

           NOP             0x2               ; [A_B] 
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 445,column 5,is_stmt,isa 0
           VST32B  .D2     VB2,*D0[SA0++]    ; [A_D2] |445| <2,56> 
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 437,column 5,is_stmt,isa 0

           VST32B  .D2     VB3,*D0[SA0++]    ; [A_D2] |441| <2,57> 
||         VMAXUB  .L2     VB10,VBL2,VBL2    ; [B_L2] |437| <2,57> 

           VST32B  .D2     VB4,*D0[SA0++]    ; [A_D2] |445| <2,58> 
||         VMAXUB  .S2     VBL1,VBL2,VB2     ; [B_S2] |437| <2,58> 

	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 441,column 5,is_stmt,isa 0
           VST32B  .D2     VB1,*D0[SA0++]    ; [A_D2] |441| <2,59> 
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 445,column 5,is_stmt,isa 0
           VST32B  .D2     VB0,*D0[SA0++]    ; [A_D2] |445| <2,60> 
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 441,column 5,is_stmt,isa 0

           VST32B  .D2     VB2,*D0[SA0++]    ; [A_D2] |441| <2,61> 
||         VHHMV   .L2     VB2,VB2,VB1       ; [B_L2] |445| <2,61> 

           NOP             0x2               ; [A_B] 
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 445,column 5,is_stmt,isa 0
           VST32B  .D2     VB1,*D0[SA0++]    ; [A_D2] |445| <2,64> 
           NOP             0x1               ; [A_B] 
;** --------------------------------------------------------------------------*
||$C$L9||:    ; PIPED LOOP KERNEL
;          EXCLUSIVE CPU CYCLES: 4
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 457,column 5,is_stmt,isa 0

   [!A2]   VST32B  .D2     VB0,*D0[SA0++]    ; [A_D2] |472| <0,14> 
||         VHHMV   .L2     VB0,VB0,VB0       ; [B_L2] |475| <0,14> 
||         VBPACKH .C2     VBM0,VBM1,VBL1    ; [B_C] |466| <2,6> 
|| [ A1]   VMAXUB  .S2     VBL0,SE0++,VBL0   ; [B_S2] |457| <3,2>  ^ 
|| [ A1]   VMAXUB  .M2     VBM2,SE1++,VBM0   ; [B_M2] |462| <3,2>  ^ 

   [ A1]   VMAXUB  .M2     VBM0,SE1++,VBM0   ; [B_M2] |462| <3,3>  ^ 
|| [ A1]   VMAXUB  .S2     VBL0,SE0++,VBM1   ; [B_S2] |457| <3,3>  ^ 

	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 53,column 4,is_stmt,isa 0

           VMAXUB  .S2     VBL1,VBL2,VBL1    ; [B_S2] |469| <1,12> 
||         VMVDLY4 .N2     VB0,VBL0          ; [B_N2] |465| <2,8> Split a long life
||         VSHR    .L2     VB0,0x8,VBL2      ; [B_L2] |53| <2,8> 
||         VBPACKL .C2     VBM0,VBM1,VB0     ; [B_C] |465| <3,4> 
||         TICK                               ; [A_U] <4,0> 

   [ A2]   ADDW    .S1     A2,0xffffffff,A2  ; [A_S1] |465| <0,17> collapsing predicate control
|| [ A1]   ADDW    .D1     A1,0xffffffff,A1  ; [A_D1] |465| <0,17> collapsing predicate control
|| [ A0]   ADDW    .L1     A0,0xffffffff,A0  ; [A_L1] |465| <0,17> collapsing predicate control
||         BNL     .B1     ||$C$L9||         ; [A_B] |452| <0,17> 
|| [!A2]   VST32B  .D2     VB0,*D0[SA0++]    ; [A_D2] |475| <0,17> 
||         VMAXUB  .L2     VBL0,VBL1,VB0     ; [B_L2] |469| <1,13> 
|| [ A0]   VMV     .S2     SE0++,VBL0        ; [B_S2] |53| <4,1>  ^ 
|| [ A0]   VMV     .M2     SE1++,VBM2        ; [B_M2] |53| <4,1>  ^ 

;** --------------------------------------------------------------------------*
||$C$L10||:    ; PIPED LOOP EPILOG
;          EXCLUSIVE CPU CYCLES: 1
           PROTCLR                            ; [A_U] SE0
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 479,column 3,is_stmt,isa 0
           SECLOSE .D2     0                 ; [A_D2] |479| 
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 480,column 3,is_stmt,isa 0
$C$DW$113	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$113, DW_AT_low_pc(0x00)
	.dwattr $C$DW$113, DW_AT_TI_return


           RET     .B1     ; [A_B] 
||         SACLOSE .C2     0                 ; [B_C] |481| 
||         SECLOSE .D2     0x1               ; [A_D2] |480| 
||         MVKU32  .L1     0,A4              ; [A_L1] |483| 

           ; RETURN OCCURS {RP}              ; [] 
	.dwattr $C$DW$33, DW_AT_TI_end_file("tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c")
	.dwattr $C$DW$33, DW_AT_TI_end_line(0x1e4)
	.dwattr $C$DW$33, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$33

	.sect	".text:_Z35TIDL_customMaxPool_ixX_oxX_init_c7xPK32TIDL_CustomMaxPoolIxXOxXPrivArgsPK18TIDL_bufParams3D_tS4_PK32TIDL_CustomMaxPoolIxXOxXInitArgs"
	.clink
	.global	||_Z35TIDL_customMaxPool_ixX_oxX_init_c7xPK32TIDL_CustomMaxPoolIxXOxXPrivArgsPK18TIDL_bufParams3D_tS4_PK32TIDL_CustomMaxPoolIxXOxXInitArgs||

$C$DW$114	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$114, DW_AT_name("TIDL_customMaxPool_ixX_oxX_init_c7x")
	.dwattr $C$DW$114, DW_AT_low_pc(||_Z35TIDL_customMaxPool_ixX_oxX_init_c7xPK32TIDL_CustomMaxPoolIxXOxXPrivArgsPK18TIDL_bufParams3D_tS4_PK32TIDL_CustomMaxPoolIxXOxXInitArgs||)
	.dwattr $C$DW$114, DW_AT_high_pc(0x00)
	.dwattr $C$DW$114, DW_AT_linkage_name("_Z35TIDL_customMaxPool_ixX_oxX_init_c7xPK32TIDL_CustomMaxPoolIxXOxXPrivArgsPK18TIDL_bufParams3D_tS4_PK32TIDL_CustomMaxPoolIxXOxXInitArgs")
	.dwattr $C$DW$114, DW_AT_external
	.dwattr $C$DW$114, DW_AT_decl_file("tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c")
	.dwattr $C$DW$114, DW_AT_decl_line(0xc8)
	.dwattr $C$DW$114, DW_AT_decl_column(0x09)
	.dwattr $C$DW$114, DW_AT_TI_max_frame_size(0x58)
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 204,column 1,is_stmt,address ||_Z35TIDL_customMaxPool_ixX_oxX_init_c7xPK32TIDL_CustomMaxPoolIxXOxXPrivArgsPK18TIDL_bufParams3D_tS4_PK32TIDL_CustomMaxPoolIxXOxXInitArgs||,isa 0

	.dwfde $C$DW$CIE, ||_Z35TIDL_customMaxPool_ixX_oxX_init_c7xPK32TIDL_CustomMaxPoolIxXOxXPrivArgsPK18TIDL_bufParams3D_tS4_PK32TIDL_CustomMaxPoolIxXOxXInitArgs||
$C$DW$115	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$115, DW_AT_name("pKerPrivArgs")
	.dwattr $C$DW$115, DW_AT_location[DW_OP_reg4]

$C$DW$116	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$116, DW_AT_name("srcAddr")
	.dwattr $C$DW$116, DW_AT_location[DW_OP_reg5]

$C$DW$117	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$117, DW_AT_name("dstAddr")
	.dwattr $C$DW$117, DW_AT_location[DW_OP_reg6]

$C$DW$118	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$118, DW_AT_name("kerInitArgs")
	.dwattr $C$DW$118, DW_AT_location[DW_OP_reg7]


;******************************************************************************
;* FUNCTION NAME: TIDL_customMaxPool_ixX_oxX_init_c7x(const TIDL_CustomMaxPoolIxXOxXPrivArgs *, const TIDL_bufParams3D_t *, const TIDL_bufParams3D_t *, const TIDL_CustomMaxPoolIxXOxXInitArgs *)*
;*                                                                            *
;*   Regs Modified     : A0,A1,A2,A3,A4,A5,A7,A8,A9,A10,VB0,VB1,VB2,VB3,VB4,  *
;*                           VB5,VB13,AL0,AL1,AL2,AL3,AL7,AM0,AM1,AM2,AM3,AM4,*
;*                           D0,D1,SP,VBL0,VBL1,VBL2,VBL3,VBL7,VBM0,VBM1,VBM2,*
;*                           VBM3,VBM4,VBM6,VBM7                              *
;*   Regs Used         : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,VB0,VB1,VB2,VB3,   *
;*                           VB4,VB5,VB13,AL0,AL1,AL2,AL3,AL7,AM0,AM1,AM2,AM3,*
;*                           AM4,D0,D1,SP,VBL0,VBL1,VBL2,VBL3,VBL7,VBM0,VBM1, *
;*                           VBM2,VBM3,VBM4,VBM6,VBM7                         *
;*   Local Frame Size  : 0 Args + 64 Auto + 24 Save = 88 byte                 *
;******************************************************************************
||_Z35TIDL_customMaxPool_ixX_oxX_init_c7xPK32TIDL_CustomMaxPoolIxXOxXPrivArgsPK18TIDL_bufParams3D_tS4_PK32TIDL_CustomMaxPoolIxXOxXInitArgs||:
;** --------------------------------------------------------------------------*
;* VB3   assigned to $O$C16
;* VB1   assigned to $O$C17
;* VB0   assigned to $O$C18
;* VBL0  assigned to $O$C19
;* VB0   assigned to $O$C20
;* A0    assigned to $O$C21
;* A0    assigned to $O$C22
;* AL0   assigned to $O$C23
;* VB4   assigned to $O$C24
;* AL0   assigned to $O$C25
;* A3    assigned to numBytes
$C$DW$119	.dwtag  DW_TAG_variable
	.dwattr $C$DW$119, DW_AT_name("numBytes")
	.dwattr $C$DW$119, DW_AT_type(*$C$DW$T$69)
	.dwattr $C$DW$119, DW_AT_location[DW_OP_reg3]

;* AL1   assigned to numBytes
$C$DW$120	.dwtag  DW_TAG_variable
	.dwattr $C$DW$120, DW_AT_name("numBytes")
	.dwattr $C$DW$120, DW_AT_type(*$C$DW$T$69)
	.dwattr $C$DW$120, DW_AT_location[DW_OP_reg17]

;* VBM0  assigned to $O$K38
;* A7    assigned to kerInitArgs
$C$DW$121	.dwtag  DW_TAG_variable
	.dwattr $C$DW$121, DW_AT_name("kerInitArgs")
	.dwattr $C$DW$121, DW_AT_type(*$C$DW$T$227)
	.dwattr $C$DW$121, DW_AT_location[DW_OP_reg7]

;* A6    assigned to dstAddr
$C$DW$122	.dwtag  DW_TAG_variable
	.dwattr $C$DW$122, DW_AT_name("dstAddr")
	.dwattr $C$DW$122, DW_AT_type(*$C$DW$T$252)
	.dwattr $C$DW$122, DW_AT_location[DW_OP_reg6]

;* A5    assigned to srcAddr
$C$DW$123	.dwtag  DW_TAG_variable
	.dwattr $C$DW$123, DW_AT_name("srcAddr")
	.dwattr $C$DW$123, DW_AT_type(*$C$DW$T$252)
	.dwattr $C$DW$123, DW_AT_location[DW_OP_reg5]

;* A4    assigned to pKerPrivArgs
$C$DW$124	.dwtag  DW_TAG_variable
	.dwattr $C$DW$124, DW_AT_name("pKerPrivArgs")
	.dwattr $C$DW$124, DW_AT_type(*$C$DW$T$237)
	.dwattr $C$DW$124, DW_AT_location[DW_OP_reg4]

;* VB5   assigned to status
$C$DW$125	.dwtag  DW_TAG_variable
	.dwattr $C$DW$125, DW_AT_name("status")
	.dwattr $C$DW$125, DW_AT_type(*$C$DW$T$69)
	.dwattr $C$DW$125, DW_AT_location[DW_OP_regx 0x35]

;* A2    assigned to stride_y
$C$DW$126	.dwtag  DW_TAG_variable
	.dwattr $C$DW$126, DW_AT_name("stride_y")
	.dwattr $C$DW$126, DW_AT_type(*$C$DW$T$69)
	.dwattr $C$DW$126, DW_AT_location[DW_OP_reg2]

;* AM1   assigned to stride_z
$C$DW$127	.dwtag  DW_TAG_variable
	.dwattr $C$DW$127, DW_AT_name("stride_z")
	.dwattr $C$DW$127, DW_AT_type(*$C$DW$T$69)
	.dwattr $C$DW$127, DW_AT_location[DW_OP_reg25]

;* VB3   assigned to seTemplate1
$C$DW$128	.dwtag  DW_TAG_variable
	.dwattr $C$DW$128, DW_AT_name("seTemplate1")
	.dwattr $C$DW$128, DW_AT_type(*$C$DW$T$274)
	.dwattr $C$DW$128, DW_AT_location[DW_OP_regx 0x33]

;* VB2   assigned to saTemplate
$C$DW$129	.dwtag  DW_TAG_variable
	.dwattr $C$DW$129, DW_AT_name("saTemplate")
	.dwattr $C$DW$129, DW_AT_type(*$C$DW$T$265)
	.dwattr $C$DW$129, DW_AT_location[DW_OP_regx 0x32]

	.dwcfi	cfa_offset, 0
;          EXCLUSIVE CPU CYCLES: 23
	.dwpsn	file "/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h",line 517,column 28,is_stmt,isa 0
           VLD8D   .D1     *PC($PCR_OFFSET(||$P$T0$1||)),VB1 ; [A_D1] |517| 
	.dwpsn	file "/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h",line 625,column 28,is_stmt,isa 0
           VLD8D   .D1     *PC($PCR_OFFSET(||$P$T1$2||)),VB0 ; [A_D1] |625| 

           STD     .D1     A8,*SP(8)         ; [A_D1] 
||         STD     .D2X    A9,*SP++(-88)     ; [A_D2] 

	.dwcfi	save_reg_to_mem, 8, 8
	.dwcfi	cfa_offset, 88
	.dwcfi	save_reg_to_mem, 9, -88
	.dwpsn	file "/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h",line 517,column 28,is_stmt,isa 0
           VST8D   .D2     VB1,*SP(16)       ; [A_D2] |517| 
	.dwpsn	file "/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h",line 617,column 5,is_stmt,isa 0
           VLD8D   .D1     *SP(16),VB3       ; [A_D1] |617| 
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 87,column 5,is_stmt,isa 0
           LDUW    .D1     *A5(0),B4         ; [A_D1] |87| 
	.dwpsn	file "/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h",line 625,column 28,is_stmt,isa 0
           VST8D   .D2     VB0,*SP(16)       ; [A_D2] |625| 
	.dwpsn	file "/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h",line 695,column 5,is_stmt,isa 0
           VLD8D   .D1     *SP(16),VB2       ; [A_D1] |695| 
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 214,column 3,is_stmt,isa 0
           EXTUV   .S2     VB3,7,48,56,VBL0  ; [B_S2] |214| 
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 87,column 5,is_stmt,isa 0

           CMPEQW  .L1X    B4,0x9,A0         ; [A_L1] |87| 
||         CMPEQW  .L2     B4,0x1,BL1        ; [B_L2] |87| 
||         CMPEQW  .S2     B4,0x5,BL2        ; [B_S2] |87| 

           VGETUB  .C2     VB2,57,VBL2       ; [B_C] |216| 
||         ORW     .L2     BL2,BL1,BM0       ; [B_L2] |87| 

           ORW     .M2X    A0,BM0,BL0        ; [B_M2] |87| 
||         ANDW    .L2     BL0,0xffffff8f,BL3 ; [B_L2] |214| 
||         CMPEQW  .S2     B4,0x6,BL1        ; [B_S2] |87| 
||         MVKU32  .L1     0x6,AL0           ; [A_L1] |215| 

           ORW     .L2     BL1,BL0,BL0       ; [B_L2] |87| 
||         ANDW    .S2     B4,0xfffffffb,B0  ; [B_S2] |87| 
||         ANDW    .L1     AL0,0x7,AL1       ; [A_L1] |216| 

           CMPEQW  .L2     BL0,0,BL0         ; [B_L2] |87| 
||         ORW     .S2     BL3,0x60,BM0      ; [B_S2] |214| 
||         SHLW    .L1     AL1,0x4,A0        ; [A_L1] |216| 

           XORD    .L2     BL0,0x1,B1        ; [B_L2] |87| 
||         ANDW    .S2     BL2,0xffffff8f,BL1 ; [B_S2] |216| 
||         MV      .L1X    B0,AL0            ; [A_L1] |87| 

           CMPEQW  .L1X    B0,0,A0           ; [A_L1] |87| 
||         ORW     .L2X    A0,BL1,BM0        ; [B_L2] |216| 
||         VPUTB   .C2     BM0,0x39,VB3      ; [B_C] |214| 

   [ A0]   B       .B1     ||$C$L12||        ; [A_B] |87| 
||         MVKU32  .L2     0xfffffffb,BM0    ; [B_L2] |87| 
||         VPUTB   .C2     BM0,0x39,VB2      ; [B_C] |216| 
||         CMPEQW  .L1X    B1,0,A1           ; [A_L1] |87| 
||         STD     .D2X    A10,*SP(80)       ; [A_D2] 
||         MVKU32  .S2     0,B5              ; [B_S2] |205| 

	.dwcfi	save_reg_to_mem, 10, 80
           ; BRANCHCC OCCURS {||$C$L12||}    ; [] |87| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 7

           CMPEQW  .L1X    B4,0xa,A0         ; [A_L1] |87| 
||         CMPEQW  .L2     B4,0x7,BL1        ; [B_L2] |87| 
||         CMPEQW  .S2     B4,0x2,BL0        ; [B_S2] |87| 

           ORW     .L2     BL1,BL0,BL0       ; [B_L2] |87| 
           ORW     .L2X    A0,BL0,BL0        ; [B_L2] |87| 
           CMPEQW  .L2     BL0,0,BL0         ; [B_L2] |87| 
           XORD    .L2     BL0,0x1,B0        ; [B_L2] |87| 
           MV      .L1X    B1,D0             ; [A_L1] |87| 

   [!A1]   B       .B1     ||$C$L11||        ; [A_B] |87| 
||         CMPEQW  .L1X    B0,0,A0           ; [A_L1] |87| 

           ; BRANCHCC OCCURS {||$C$L11||}    ; [] |87| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 8

           CMPEQW  .L1X    B4,0xb,A1         ; [A_L1] |87| 
||         CMPEQW  .L2     B4,0x8,BL1        ; [B_L2] |87| 
||         CMPEQW  .S2     B4,0x3,BL0        ; [B_S2] |87| 

           ORW     .L2     BL1,BL0,BL0       ; [B_L2] |87| 
           ORW     .L2X    A1,BL0,BL0        ; [B_L2] |87| 
           CMPEQW  .L2     BL0,0,BL0         ; [B_L2] |87| 
   [ A0]   XORD    .L2     BL0,0x1,B0        ; [B_L2] |87| 
           CMPEQW  .L2     B0,0,B0           ; [B_L2] |87| 
           XORD    .L1X    B0,0x1,D0         ; [A_L1] |87| 
;** --------------------------------------------------------------------------*
||$C$L11||:    
;          EXCLUSIVE CPU CYCLES: 1
           MV      .D1     D0,AL0            ; [A_D1] |87| 
;** --------------------------------------------------------------------------*
||$C$L12||:    
;          EXCLUSIVE CPU CYCLES: 2
           CMPEQW  .L1     AL0,0,A0          ; [A_L1] |87| 

   [!A0]   B       .B1     ||$C$L14||        ; [A_B] |87| 
||         MVKU32  .L1     0x1,AL1           ; [A_L1] |118| 

           ; BRANCHCC OCCURS {||$C$L14||}    ; [] |87| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 11
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 243,column 5,is_stmt,isa 0

           LDW     .D1     *A7(16),BL0       ; [A_D1] |243| 
||         LDW     .D2     *A7(20),BL1       ; [A_D2] |243| 

           CMPEQW  .L2     BL0,0x2,BL0       ; [B_L2] |243| 
||         CMPEQW  .S2     BL1,0x2,BL1       ; [B_S2] |243| 

           ANDW    .L2     BL1,BL0,B0        ; [B_L2] |243| 
           CMPEQW  .L1X    B0,0,A0           ; [A_L1] |243| 
   [ A0]   B       .B1     ||$C$L14||        ; [A_B] |243| 
           ; BRANCHCC OCCURS {||$C$L14||}    ; [] |243| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 11
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 245,column 7,is_stmt,isa 0

           LDW     .D1     *A7(12),BL1       ; [A_D1] |245| 
||         LDW     .D2     *A7(8),BL0        ; [A_D2] |245| 

	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 250,column 9,is_stmt,isa 0
           EXTUV   .L2     VB3,7,32,56,VBL2  ; [B_L2] |250| 
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 245,column 7,is_stmt,isa 0

           CMPEQW  .L2     BL1,0x3,BL1       ; [B_L2] |245| 
||         CMPEQW  .S2     BL0,0x3,BL0       ; [B_S2] |245| 

           ANDW    .L2     BL1,BL0,B0        ; [B_L2] |245| 
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 256,column 9,is_stmt,isa 0
           MVKU32  .L1     0x40,A3           ; [A_L1] |256| 
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 245,column 7,is_stmt,isa 0

           CMPEQW  .L1X    B0,0,A0           ; [A_L1] |245| 
||         ANDW    .L2     BL2,0xfffffff8,BL7 ; [B_L2] |250| 
||         MVKU32  .S1     0x3e,AL2          ; [A_S1] |258| 

   [ A0]   B       .B1     ||$C$L14||        ; [A_B] |245| 
||         MVKU32  .L2     0x3e,B0           ; [B_L2] |258| 
||         ORW     .S2     BL7,0x4,BM1       ; [B_S2] |250| 
||         MVKU32  .L1     0x3,A2            ; [A_L1] |257| 
||         MVKU32  .S1     0x3e,A1           ; [A_S1] |262| 

           ; BRANCHCC OCCURS {||$C$L14||}    ; [] |245| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 64
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 250,column 9,is_stmt,isa 0
           VPUTB   .C2     BM1,0x3b,VB3      ; [B_C] |250| 
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 251,column 9,is_stmt,isa 0
           EXTUV   .L2     VB3,7,56,56,VBL0  ; [B_L2] |251| 
           ANDW    .L2     BL0,0xfffffff0,BM1 ; [B_L2] |251| 
           VPUTB   .C2     BM1,0x38,VB3      ; [B_C] |251| 
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 252,column 9,is_stmt,isa 0

           EXTUV   .L2     VB3,7,48,56,VBL0  ; [B_L2] |252| 
||         LDUW    .D1     *A5(4),AL0        ; [A_D1] |258| 

           ANDW    .L2     BL0,0xffffff8f,BL0 ; [B_L2] |252| 
           ORW     .L2     BL0,0x60,BM1      ; [B_L2] |252| 

           VPUTB   .C2     BM1,0x39,VB3      ; [B_C] |252| 
||         DIVUW   .L1     AL0,AL2,A0        ; [A_L1] |258| 

	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 258,column 9,is_stmt,isa 0
           MPYWW   .N1X    B0,A0,AL2         ; [A_N1] |258| 
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 256,column 9,is_stmt,isa 0

           VPUTW   .C2X    A3,0,VB3          ; [B_C] |256| 
||         LDUW    .D1     *A6(8),D0         ; [A_D1] |259| 

	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 258,column 9,is_stmt,isa 0
           CMPEQW  .L1     AL0,AL2,AL0       ; [A_L1] |258| 
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 257,column 9,is_stmt,isa 0

           VPUTW   .C2X    A2,0x1,VB3        ; [B_C] |257| 
||         XORD    .L1     AL0,0x1,D1        ; [A_L1] |258| 

	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 258,column 9,is_stmt,isa 0
           ADDW    .D1     A0,D1,A0          ; [A_D1] |258| 

           VPUTW   .C2X    A0,0x2,VB3        ; [B_C] |258| 
||         ADDW    .D1     D0,0x1,AL7        ; [A_D1] |259| 
||         LDUW    .D2     *A5(16),BM3       ; [A_D2] |260| 

	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 259,column 9,is_stmt,isa 0

           SHRUW   .L1     AL7,0x1,A3        ; [A_L1] |259| 
||         LDW     .D1     *A5(12),AL3       ; [A_D1] |261| 

           VPUTW   .C2X    A3,0x3,VB3        ; [B_C] |259| 
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 260,column 9,is_stmt,isa 0
           VPUTW   .C2     BM3,0x4,VB3       ; [B_C] |260| 
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 261,column 9,is_stmt,isa 0
           DIVW    .L1     AL3,AL1,A0        ; [A_L1] |261| 

           VPUTW   .C2X    A0,0x8,VB3        ; [B_C] |261| 
||         LDW     .D1     *A5(20),B13       ; [A_D1] |264| 

	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 262,column 9,is_stmt,isa 0
           VPUTW   .C2X    A1,0x9,VB3        ; [B_C] |262| 
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 263,column 9,is_stmt,isa 0
           SHLW    .L1     A0,0x2,A2         ; [A_L1] |263| 
           VPUTW   .C2X    A2,0xa,VB3        ; [B_C] |263| 
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 264,column 9,is_stmt,isa 0
           DIVW    .L1X    B13,AL1,A3        ; [A_L1] |264| 
           VPUTW   .C2X    A3,0xb,VB3        ; [B_C] |264| 
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 283,column 5,is_stmt,isa 0
           VST8D   .D2     VB3,*A4(56)       ; [A_D2] |283| 
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 284,column 5,is_stmt,isa 0
           VST8D   .D2     VB3,*A4(120)      ; [A_D2] |284| 
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 87,column 5,is_stmt,isa 0
           LDUW    .D1     *A6(0),B0         ; [A_D1] |87| 
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 92,column 10,is_stmt,isa 0

           CMPEQW  .L1X    B0,0x9,AL0        ; [A_L1] |92| 
||         CMPEQW  .L2     B0,0x1,BL0        ; [B_L2] |92| 
||         CMPEQW  .S2     B0,0x5,BL1        ; [B_S2] |92| 

	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 87,column 5,is_stmt,isa 0

           XORD    .L1     AL0,0x1,A0        ; [A_L1] |92| 
||         XORD    .L2     BL0,0x1,BL0       ; [B_L2] |92| 
||         XORD    .S2     BL1,0x1,BL1       ; [B_S2] |92| 
||         ANDW    .M2     BM0,B0,B1         ; [B_M2] |87| 

	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 92,column 10,is_stmt,isa 0
           ANDW    .L2     BL1,BL0,BL0       ; [B_L2] |92| 
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 87,column 5,is_stmt,isa 0

           ANDW    .L2X    A0,BL0,B1         ; [B_L2] |92| 
||         CMPEQW  .L1X    B1,0,A0           ; [A_L1] |87| 

	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 92,column 10,is_stmt,isa 0
           CMPEQW  .L1X    B1,0,A1           ; [A_L1] |92| 
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 98,column 10,is_stmt,isa 0

   [ A0]   MVKU32  .L1     0x1,A1            ; [A_L1] |118| 
||         CMPEQW  .S1X    B0,0x6,A2         ; [A_S1] |98| 

	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 92,column 10,is_stmt,isa 0

   [ A1]   B       .B1     ||$C$L13||        ; [A_B] |92| 
|| [!A0]   MVKU32  .L1     0x2,A3            ; [A_L1] |118| 
|| [ A0]   MVKU32  .S1     0x1,A3            ; [A_S1] |118| 

           ; BRANCHCC OCCURS {||$C$L13||}    ; [] |92| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 6
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 102,column 10,is_stmt,isa 0

           CMPEQW  .L2     B0,0x2,BL0        ; [B_L2] |102| 
||         CMPEQW  .L1X    B0,0xa,AL0        ; [A_L1] |102| 
||         CMPEQW  .S2     B0,0x7,BL1        ; [B_S2] |102| 

           XORD    .L2     BL1,0x1,BL1       ; [B_L2] |102| 
||         XORD    .S2     BL0,0x1,BL0       ; [B_S2] |102| 
||         XORD    .L1     AL0,0x1,A0        ; [A_L1] |102| 

           ANDW    .L2     BL1,BL0,BL0       ; [B_L2] |102| 
           ANDW    .L2X    A0,BL0,B1         ; [B_L2] |102| 
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 98,column 10,is_stmt,isa 0

   [ A2]   B       .B1     ||$C$L13||        ; [A_B] |98| 
||         CMPEQW  .L1X    B1,0,A0           ; [A_L1] |102| 
||         MVKU32  .S1     0x3,A3            ; [A_S1] |118| 

           ; BRANCHCC OCCURS {||$C$L13||}    ; [] |98| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 7
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 108,column 10,is_stmt,isa 0

           CMPEQW  .L2     B0,0x8,BL1        ; [B_L2] |108| 
||         CMPEQW  .S2     B0,0x3,BL0        ; [B_S2] |108| 

           ORW     .L2     BL1,BL0,BL0       ; [B_L2] |108| 
||         CMPEQW  .S2     B0,0xb,BL7        ; [B_S2] |108| 

           ORW     .L2     BL7,BL0,B0        ; [B_L2] |108| 

           CMPEQW  .L1X    B0,0,A1           ; [A_L1] |108| 
||         MVKU32  .S1     0x4,A3            ; [A_S1] |118| 

	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 118,column 5,is_stmt,isa 0

   [ A0]   MVKU32  .L1     0,A1              ; [A_L1] |118| 
|| [!A0]   MVKU32  .S1     0x8,A3            ; [A_S1] |118| 

   [ A1]   MVKU32  .L1     0x1,A3            ; [A_L1] |118| 
;** --------------------------------------------------------------------------*
||$C$L13||:    
;          EXCLUSIVE CPU CYCLES: 12
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 296,column 5,is_stmt,isa 0
           LDW     .D1     *A7(16),BL0       ; [A_D1] |296| 
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 293,column 5,is_stmt,isa 0

           LDW     .D1     *A6(12),B0        ; [A_D1] |293| 
||         LDW     .D2     *A7(20),BL1       ; [A_D2] |296| 

	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 294,column 5,is_stmt,isa 0
           LDW     .D1     *A6(20),B1        ; [A_D1] |294| 
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 296,column 5,is_stmt,isa 0

           CMPEQW  .L2     BL0,0x2,BL0       ; [B_L2] |296| 
||         CMPEQW  .S2     BL1,0x2,BL1       ; [B_S2] |296| 

	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 293,column 5,is_stmt,isa 0

           ANDW    .L2     BL1,BL0,B0        ; [B_L2] |296| 
||         DIVW    .L1X    B0,A3,A2          ; [A_L1] |293| 

	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 296,column 5,is_stmt,isa 0
           CMPEQW  .L1X    B0,0,A1           ; [A_L1] |296| 
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 294,column 5,is_stmt,isa 0

   [ A1]   B       .B1     ||$C$L14||        ; [A_B] |296| 
||         DIVW    .L1X    B1,A3,AM1         ; [A_L1] |294| 
||         CMPEQW  .S1     A3,0x1,A0         ; [A_S1] |298| 

	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 296,column 5,is_stmt,isa 0
           ; BRANCHCC OCCURS {||$C$L14||}    ; [] |296| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 15
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 300,column 9,is_stmt,isa 0
           EXTUV   .L2     VB2,7,48,56,VBL0  ; [B_L2] |300| 
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 308,column 7,is_stmt,isa 0

           LDW     .D1     *A7(12),BL1       ; [A_D1] |308| 
||         LDW     .D2     *A7(8),BL3        ; [A_D2] |308| 

	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 300,column 9,is_stmt,isa 0
           ANDW    .L2     BL0,0xffffff8f,BL2 ; [B_L2] |300| 
           ORW     .L2     BL2,0x50,BM0      ; [B_L2] |300| 
   [ A0]   VPUTB   .C2     BM0,0x39,VB2      ; [B_C] |300| 
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 323,column 9,is_stmt,isa 0
           EXT     .L2X    A3,0x30,0x30,BM7  ; [B_L2] |323| 
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 308,column 7,is_stmt,isa 0

           VGETUB  .C2     VB2,62,VBL2       ; [B_C] |313| 
||         CMPEQW  .L2     BL3,0x3,BL0       ; [B_L2] |308| 
||         CMPEQW  .S2     BL1,0x3,BL1       ; [B_S2] |308| 

           XORD    .L2     BL0,0x1,BL0       ; [B_L2] |308| 
||         XORD    .S2     BL1,0x1,BL1       ; [B_S2] |308| 
||         MVKU32  .L1     0x1f,AM3          ; [A_L1] |330| 
||         EXT     .S1     A3,0x30,0x30,A1   ; [A_S1] |331| 

           ORW     .L2     BL1,BL0,B0        ; [B_L2] |308| 
||         EXT     .L1     A3,0x30,0x30,AM2  ; [A_L1] |332| 
||         EXT     .S1     A3,0x30,0x30,AM4  ; [A_S1] |329| 
||         SHLW    .S2X    A2,0x1,BM6        ; [B_S2] |331| 

	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 304,column 9,is_stmt,isa 0

           MVKU32  .L1     0x2,A8            ; [A_L1] |324| 
|| [!A0]   MVK32   .L2     0xffffffff,B5     ; [B_L2] |304| 
||         EXT     .S1     A3,0x30,0x30,AM0  ; [A_S1] |320| 
||         MVKU32  .S2     0x1f,BM1          ; [B_S2] |323| 

	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 308,column 7,is_stmt,isa 0

           ANDW    .L2     BL2,0xfffffff8,BL0 ; [B_L2] |313| 
||         CMPEQW  .L1X    B0,0,A1           ; [A_L1] |308| 
||         MPYWW   .N2X    A1,BM6,BM1        ; [B_N2] |331| 
||         MPYWW   .M2     BM1,BM7,BM0       ; [B_M2] |323| 
||         MPYSUHW .M1     A3,AM3,A9         ; [A_M1] |330| 
||         MVKU32  .S1     0x1f,A10          ; [A_S1] |325| 

   [ A1]   B       .B1     ||$C$L15||        ; [A_B] |308| 
||         ORW     .L2     BL0,0x2,BM2       ; [B_L2] |313| 
||         CMPEQW  .L1X    B5,0,A0           ; [A_L1] |346| 
||         MPYWW   .N1     AM1,AM2,A3        ; [A_N1] |332| 
||         MPYWW   .M1     A2,AM4,A5         ; [A_M1] |329| 
||         EXT     .S1     A3,0x30,0x30,A7   ; [A_S1] |321| 
||         MVKU32  .S2     0x8421085,BM4     ; [B_S2] |325| 

           ; BRANCHCC OCCURS {||$C$L15||}    ; [] |308| 
;** --------------------------------------------------------------------------*
||$C$L14||:    
;          EXCLUSIVE CPU CYCLES: 1
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 352,column 3,is_stmt,isa 0

           B       .B1     ||$C$L16||        ; [A_B] |352| 
||         MVK32   .L1     0xffffffff,A4     ; [A_L1] |352| 

           ; BRANCH OCCURS {||$C$L16||}      ; [] |352| 
;** --------------------------------------------------------------------------*
||$C$L15||:    
;          EXCLUSIVE CPU CYCLES: 73
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 313,column 9,is_stmt,isa 0
           VPUTB   .C2     BM2,0x3e,VB2      ; [B_C] |313| 
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 314,column 9,is_stmt,isa 0
           EXTUV   .L2     VB2,7,8,56,VBL0   ; [B_L2] |314| 
           ANDW    .L2     BL0,0xffffff1f,BL0 ; [B_L2] |314| 
           ORW     .L2     BL0,0x60,BM2      ; [B_L2] |314| 
           VPUTB   .C2     BM2,0x3e,VB2      ; [B_C] |314| 
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 315,column 9,is_stmt,isa 0
           EXTUV   .L2     VB2,7,0,56,VBL0   ; [B_L2] |315| 
           ANDW    .L2     BL0,0xfffffffc,BL0 ; [B_L2] |315| 
           ORW     .L2     BL0,0x1,BM2       ; [B_L2] |315| 
           VPUTB   .C2     BM2,0x3f,VB2      ; [B_C] |315| 
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 317,column 9,is_stmt,isa 0
           EXTUV   .L2     VB2,7,32,56,VBL0  ; [B_L2] |317| 
           ANDW    .L2     BL0,0xfffffff8,BL0 ; [B_L2] |317| 
           ORW     .L2     BL0,0x4,BM2       ; [B_L2] |317| 

           VPUTB   .C2     BM2,0x3b,VB2      ; [B_C] |317| 
||         LDUW    .D1     *A6(4),B0         ; [A_D1] |320| 

	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 321,column 9,is_stmt,isa 0
           LDUW    .D1     *A6(8),B3         ; [A_D1] |321| 
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 319,column 9,is_stmt,isa 0
           EXTUV   .L2     VB2,7,48,56,VBL0  ; [B_L2] |319| 

           ANDW    .L2     BL0,0xffffff8f,BL0 ; [B_L2] |319| 
||         MPYWW   .N1X    B0,AM0,A1         ; [A_N1] |320| 
||         MPYWW   .N2X    A2,B3,BM3         ; [B_N2] |321| 
||         ANDW    .S2     B0,0xffffffff,BM7 ; [B_S2] |325| 

           MPYDD   .N2     BM4,BM7,BL0       ; [B_N2] |325| 
||         ORW     .L2     BL0,0x50,BM2      ; [B_L2] |319| 

           VPUTB   .C2     BM2,0x39,VB2      ; [B_C] |319| 
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 321,column 9,is_stmt,isa 0
           MPYWW   .N2X    A7,BM3,BM6        ; [B_N2] |321| 
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 320,column 9,is_stmt,isa 0

           VPUTW   .C2X    A1,0x6,VB2        ; [B_C] |320| 
||         SHRUD   .L2     BL0,0x20,BL0      ; [B_L2] |325| 

	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 325,column 9,is_stmt,isa 0
           SUBW    .L2     B0,BL0,BL1        ; [B_L2] |325| 
           SHRUW   .L2     BL1,0x1,BL1       ; [B_L2] |325| 
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 321,column 9,is_stmt,isa 0

           VPUTW   .C2     BM6,0x7,VB2       ; [B_C] |321| 
||         ADDW    .L2     BL0,BL1,BL0       ; [B_L2] |325| 

	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 325,column 9,is_stmt,isa 0
           SHRUW   .L2     BL0,0x4,B1        ; [B_L2] |325| 
           MPYWW   .N2X    A10,B1,BL0        ; [B_N2] |325| 
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 323,column 9,is_stmt,isa 0
           VPUTW   .C2     BM0,0,VB2         ; [B_C] |323| 
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 324,column 9,is_stmt,isa 0

           CMPEQW  .L2     B0,BL0,BL0        ; [B_L2] |325| 
||         VPUTW   .C2X    A8,0x1,VB2        ; [B_C] |324| 

	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 325,column 9,is_stmt,isa 0
           XORD    .L2     BL0,0x1,BL0       ; [B_L2] |325| 
           ADDW    .L2     B1,BL0,BM0        ; [B_L2] |325| 

           VPUTW   .C2     BM0,0x2,VB2       ; [B_C] |325| 
||         LDUW    .D1     *A6(16),B13       ; [A_D1] |327| 

	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 326,column 9,is_stmt,isa 0
           ADDW    .L2     B3,0x1,BL7        ; [B_L2] |326| 
           SHRUW   .L2     BL7,0x1,BM2       ; [B_L2] |326| 
           VPUTW   .C2     BM2,0x3,VB2       ; [B_C] |326| 
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 327,column 9,is_stmt,isa 0
           VPUTW   .C2     B13,0x4,VB2       ; [B_C] |327| 
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 329,column 9,is_stmt,isa 0
           VPUTW   .C2X    A5,0x8,VB2        ; [B_C] |329| 
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 330,column 9,is_stmt,isa 0
           VPUTW   .C2X    A9,0x9,VB2        ; [B_C] |330| 
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 331,column 9,is_stmt,isa 0
           VPUTW   .C2     BM1,0xa,VB2       ; [B_C] |331| 
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 332,column 9,is_stmt,isa 0
           VPUTW   .C2X    A3,0xb,VB2        ; [B_C] |332| 
	.dwpsn	file "tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c",line 349,column 5,is_stmt,isa 0

           MV      .L1X    B5,A4             ; [A_L1] |352| 
|| [ A0]   VST8D   .D2     VB2,*A4(184)      ; [A_D2] |349| 

;** --------------------------------------------------------------------------*
||$C$L16||:    
;          EXCLUSIVE CPU CYCLES: 7
           LDD     .D1     *SP(80),A10       ; [A_D1] 
	.dwcfi	restore_reg, 10

           LDD     .D1     *SP(96),A8        ; [A_D1] 
||         LDD     .D2     *SP(88),A9        ; [A_D2] 

	.dwcfi	restore_reg, 8
	.dwcfi	restore_reg, 9
$C$DW$130	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$130, DW_AT_low_pc(0x00)
	.dwattr $C$DW$130, DW_AT_TI_return


           RET     .B1     ; [A_B] 
||         ADDD    .D1     SP,0x58,SP        ; [A_D1] 

	.dwcfi	cfa_offset, 0
           ; RETURN OCCURS {RP}              ; [] 
	.dwattr $C$DW$114, DW_AT_TI_end_file("tidsp/tidl_custom_maxpool_ixX_oxX_c7x.c")
	.dwattr $C$DW$114, DW_AT_TI_end_line(0x161)
	.dwattr $C$DW$114, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$114

	.dwattr $C$DW$CU, DW_AT_language(DW_LANG_C_plus_plus)

;***************************************************************
;* DWARF CIE ENTRIES                                           *
;***************************************************************

$C$DW$CIE	.dwcie 4101
	.dwcfi	cfa_register, 111
	.dwcfi	cfa_offset, 0
	.dwcfi	same_value, 8
	.dwcfi	same_value, 9
	.dwcfi	same_value, 10
	.dwcfi	same_value, 11
	.dwcfi	same_value, 12
	.dwcfi	same_value, 13
	.dwcfi	same_value, 14
	.dwcfi	same_value, 15
	.dwcfi	same_value, 62
	.dwcfi	same_value, 63
	.dwendentry
	.dwendtag $C$DW$CU


;******************************************************************************
;* TYPE INFORMATION                                                           *
;******************************************************************************

$C$DW$TU$25	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$25

$C$DW$T$25	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$25, DW_AT_name("S3_1")
	.dwattr $C$DW$T$25, DW_AT_declaration
	.dwendtag $C$DW$T$25

	.dwendtag $C$DW$TU$25


$C$DW$TU$26	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$26

$C$DW$T$26	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$26, DW_AT_name("S3_13")
	.dwattr $C$DW$T$26, DW_AT_declaration
	.dwendtag $C$DW$T$26

	.dwendtag $C$DW$TU$26


$C$DW$TU$27	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$27

$C$DW$T$27	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$27, DW_AT_name("S3_5")
	.dwattr $C$DW$T$27, DW_AT_declaration
	.dwendtag $C$DW$T$27

	.dwendtag $C$DW$TU$27


$C$DW$TU$28	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$28

$C$DW$T$28	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$28, DW_AT_name("S4_15")
	.dwattr $C$DW$T$28, DW_AT_declaration
	.dwendtag $C$DW$T$28

	.dwendtag $C$DW$TU$28


$C$DW$TU$29	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$29

$C$DW$T$29	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$29, DW_AT_name("S4_3")
	.dwattr $C$DW$T$29, DW_AT_declaration
	.dwendtag $C$DW$T$29

	.dwendtag $C$DW$TU$29


$C$DW$TU$30	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$30

$C$DW$T$30	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$30, DW_AT_name("S4_7")
	.dwattr $C$DW$T$30, DW_AT_declaration
	.dwendtag $C$DW$T$30

	.dwendtag $C$DW$TU$30


$C$DW$TU$31	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$31

$C$DW$T$31	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$31, DW_AT_name("S5_17")
	.dwattr $C$DW$T$31, DW_AT_declaration
	.dwendtag $C$DW$T$31

	.dwendtag $C$DW$TU$31


$C$DW$TU$32	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$32

$C$DW$T$32	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$32, DW_AT_name("S5_9")
	.dwattr $C$DW$T$32, DW_AT_declaration
	.dwendtag $C$DW$T$32

	.dwendtag $C$DW$TU$32


$C$DW$TU$33	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$33

$C$DW$T$33	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$33, DW_AT_name("S6_11")
	.dwattr $C$DW$T$33, DW_AT_declaration
	.dwendtag $C$DW$T$33

	.dwendtag $C$DW$TU$33


$C$DW$TU$34	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$34

$C$DW$T$34	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$34, DW_AT_name("S6_19")
	.dwattr $C$DW$T$34, DW_AT_declaration
	.dwendtag $C$DW$T$34

	.dwendtag $C$DW$TU$34


$C$DW$TU$43	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$43

$C$DW$T$43	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$43, DW_AT_name("TIDL_CustomMaxPoolBuffParamsC7x")
	.dwattr $C$DW$T$43, DW_AT_byte_size(0xd0)
$C$DW$131	.dwtag  DW_TAG_member
	.dwattr $C$DW$131, DW_AT_type(*$C$DW$T$37)
	.dwattr $C$DW$131, DW_AT_name("seTemplate1")
	.dwattr $C$DW$131, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$131, DW_AT_decl_file("./tidsp/tidl_custom_maxpool_ixX_oxX_priv.h")
	.dwattr $C$DW$131, DW_AT_decl_line(0x3b)
	.dwattr $C$DW$131, DW_AT_decl_column(0x0c)

$C$DW$132	.dwtag  DW_TAG_member
	.dwattr $C$DW$132, DW_AT_type(*$C$DW$T$37)
	.dwattr $C$DW$132, DW_AT_name("seTemplate2")
	.dwattr $C$DW$132, DW_AT_data_member_location[DW_OP_plus_uconst 0x40]
	.dwattr $C$DW$132, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$132, DW_AT_decl_file("./tidsp/tidl_custom_maxpool_ixX_oxX_priv.h")
	.dwattr $C$DW$132, DW_AT_decl_line(0x3c)
	.dwattr $C$DW$132, DW_AT_decl_column(0x0c)

$C$DW$133	.dwtag  DW_TAG_member
	.dwattr $C$DW$133, DW_AT_type(*$C$DW$T$37)
	.dwattr $C$DW$133, DW_AT_name("saTemplate")
	.dwattr $C$DW$133, DW_AT_data_member_location[DW_OP_plus_uconst 0x80]
	.dwattr $C$DW$133, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$133, DW_AT_decl_file("./tidsp/tidl_custom_maxpool_ixX_oxX_priv.h")
	.dwattr $C$DW$133, DW_AT_decl_line(0x3d)
	.dwattr $C$DW$133, DW_AT_decl_column(0x0c)

$C$DW$134	.dwtag  DW_TAG_member
	.dwattr $C$DW$134, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$134, DW_AT_name("numCh")
	.dwattr $C$DW$134, DW_AT_data_member_location[DW_OP_plus_uconst 0xc0]
	.dwattr $C$DW$134, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$134, DW_AT_decl_file("./tidsp/tidl_custom_maxpool_ixX_oxX_priv.h")
	.dwattr $C$DW$134, DW_AT_decl_line(0x3e)
	.dwattr $C$DW$134, DW_AT_decl_column(0x0c)

$C$DW$135	.dwtag  DW_TAG_member
	.dwattr $C$DW$135, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$135, DW_AT_name("numLines")
	.dwattr $C$DW$135, DW_AT_data_member_location[DW_OP_plus_uconst 0xc4]
	.dwattr $C$DW$135, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$135, DW_AT_decl_file("./tidsp/tidl_custom_maxpool_ixX_oxX_priv.h")
	.dwattr $C$DW$135, DW_AT_decl_line(0x3f)
	.dwattr $C$DW$135, DW_AT_decl_column(0x0c)

$C$DW$136	.dwtag  DW_TAG_member
	.dwattr $C$DW$136, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$136, DW_AT_name("numTiles")
	.dwattr $C$DW$136, DW_AT_data_member_location[DW_OP_plus_uconst 0xc8]
	.dwattr $C$DW$136, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$136, DW_AT_decl_file("./tidsp/tidl_custom_maxpool_ixX_oxX_priv.h")
	.dwattr $C$DW$136, DW_AT_decl_line(0x40)
	.dwattr $C$DW$136, DW_AT_decl_column(0x0c)

$C$DW$137	.dwtag  DW_TAG_member
	.dwattr $C$DW$137, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$137, DW_AT_name("inStride")
	.dwattr $C$DW$137, DW_AT_data_member_location[DW_OP_plus_uconst 0xcc]
	.dwattr $C$DW$137, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$137, DW_AT_decl_file("./tidsp/tidl_custom_maxpool_ixX_oxX_priv.h")
	.dwattr $C$DW$137, DW_AT_decl_line(0x41)
	.dwattr $C$DW$137, DW_AT_decl_column(0x0c)


$C$DW$138	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$138, DW_AT_name("operator =")
	.dwattr $C$DW$138, DW_AT_declaration
	.dwattr $C$DW$138, DW_AT_linkage_name("_ZN31TIDL_CustomMaxPoolBuffParamsC7xaSERKS_")
	.dwattr $C$DW$138, DW_AT_type(*$C$DW$T$38)
	.dwattr $C$DW$138, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$139	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$139, DW_AT_type(*$C$DW$T$40)

	.dwendtag $C$DW$138


$C$DW$140	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$140, DW_AT_name("operator =")
	.dwattr $C$DW$140, DW_AT_declaration
	.dwattr $C$DW$140, DW_AT_linkage_name("_ZN31TIDL_CustomMaxPoolBuffParamsC7xaSEOS_")
	.dwattr $C$DW$140, DW_AT_type(*$C$DW$T$38)
	.dwattr $C$DW$140, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$141	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$141, DW_AT_type(*$C$DW$T$38)

	.dwendtag $C$DW$140

	.dwattr $C$DW$T$43, DW_AT_decl_file("./tidsp/tidl_custom_maxpool_ixX_oxX_priv.h")
	.dwattr $C$DW$T$43, DW_AT_decl_line(0x3a)
	.dwattr $C$DW$T$43, DW_AT_decl_column(0x01)
	.dwendtag $C$DW$T$43

	.dwendtag $C$DW$TU$43


$C$DW$TU$39	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$39
$C$DW$T$39	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$39, DW_AT_type(*$C$DW$T$43)

	.dwendtag $C$DW$TU$39


$C$DW$TU$40	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$40
$C$DW$T$40	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$40, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$T$40, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$40


$C$DW$TU$57	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$57
$C$DW$T$57	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$57, DW_AT_name("TIDL_CustomMaxPoolBuffParamsC7x")
	.dwattr $C$DW$T$57, DW_AT_type(*$C$DW$T$43)
	.dwattr $C$DW$T$57, DW_AT_decl_file("./tidsp/tidl_custom_maxpool_ixX_oxX_priv.h")
	.dwattr $C$DW$T$57, DW_AT_decl_line(0x42)
	.dwattr $C$DW$T$57, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$57


$C$DW$TU$38	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$38
$C$DW$T$38	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$38, DW_AT_type(*$C$DW$T$43)
	.dwattr $C$DW$T$38, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$38


$C$DW$TU$41	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$41

$C$DW$T$41	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$41, DW_AT_type(*$C$DW$T$38)
$C$DW$142	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$142, DW_AT_type(*$C$DW$T$40)

	.dwendtag $C$DW$T$41

	.dwendtag $C$DW$TU$41


$C$DW$TU$42	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$42

$C$DW$T$42	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$42, DW_AT_type(*$C$DW$T$38)
$C$DW$143	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$143, DW_AT_type(*$C$DW$T$38)

	.dwendtag $C$DW$T$42

	.dwendtag $C$DW$TU$42


$C$DW$TU$54	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$54

$C$DW$T$54	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$54, DW_AT_name("TIDL_CustomMaxPoolBuffParamsNatC")
	.dwattr $C$DW$T$54, DW_AT_byte_size(0x30)
$C$DW$144	.dwtag  DW_TAG_member
	.dwattr $C$DW$144, DW_AT_type(*$C$DW$T$48)
	.dwattr $C$DW$144, DW_AT_name("srcBuf3D")
	.dwattr $C$DW$144, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$144, DW_AT_decl_file("./tidsp/tidl_custom_maxpool_ixX_oxX_priv.h")
	.dwattr $C$DW$144, DW_AT_decl_line(0x46)
	.dwattr $C$DW$144, DW_AT_decl_column(0x16)

$C$DW$145	.dwtag  DW_TAG_member
	.dwattr $C$DW$145, DW_AT_type(*$C$DW$T$48)
	.dwattr $C$DW$145, DW_AT_name("dstBuf3D")
	.dwattr $C$DW$145, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$145, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$145, DW_AT_decl_file("./tidsp/tidl_custom_maxpool_ixX_oxX_priv.h")
	.dwattr $C$DW$145, DW_AT_decl_line(0x47)
	.dwattr $C$DW$145, DW_AT_decl_column(0x16)


$C$DW$146	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$146, DW_AT_name("operator =")
	.dwattr $C$DW$146, DW_AT_declaration
	.dwattr $C$DW$146, DW_AT_linkage_name("_ZN32TIDL_CustomMaxPoolBuffParamsNatCaSERKS_")
	.dwattr $C$DW$146, DW_AT_type(*$C$DW$T$49)
	.dwattr $C$DW$146, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$147	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$147, DW_AT_type(*$C$DW$T$51)

	.dwendtag $C$DW$146


$C$DW$148	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$148, DW_AT_name("operator =")
	.dwattr $C$DW$148, DW_AT_declaration
	.dwattr $C$DW$148, DW_AT_linkage_name("_ZN32TIDL_CustomMaxPoolBuffParamsNatCaSEOS_")
	.dwattr $C$DW$148, DW_AT_type(*$C$DW$T$49)
	.dwattr $C$DW$148, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$149	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$149, DW_AT_type(*$C$DW$T$49)

	.dwendtag $C$DW$148

	.dwattr $C$DW$T$54, DW_AT_decl_file("./tidsp/tidl_custom_maxpool_ixX_oxX_priv.h")
	.dwattr $C$DW$T$54, DW_AT_decl_line(0x45)
	.dwattr $C$DW$T$54, DW_AT_decl_column(0x01)
	.dwendtag $C$DW$T$54

	.dwendtag $C$DW$TU$54


$C$DW$TU$50	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$50
$C$DW$T$50	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$50, DW_AT_type(*$C$DW$T$54)

	.dwendtag $C$DW$TU$50


$C$DW$TU$51	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$51
$C$DW$T$51	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$51, DW_AT_type(*$C$DW$T$50)
	.dwattr $C$DW$T$51, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$51


$C$DW$TU$58	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$58
$C$DW$T$58	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$58, DW_AT_name("TIDL_CustomMaxPoolBuffParamsNatC")
	.dwattr $C$DW$T$58, DW_AT_type(*$C$DW$T$54)
	.dwattr $C$DW$T$58, DW_AT_decl_file("./tidsp/tidl_custom_maxpool_ixX_oxX_priv.h")
	.dwattr $C$DW$T$58, DW_AT_decl_line(0x48)
	.dwattr $C$DW$T$58, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$58


$C$DW$TU$49	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$49
$C$DW$T$49	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$49, DW_AT_type(*$C$DW$T$54)
	.dwattr $C$DW$T$49, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$49


$C$DW$TU$52	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$52

$C$DW$T$52	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$52, DW_AT_type(*$C$DW$T$49)
$C$DW$150	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$150, DW_AT_type(*$C$DW$T$51)

	.dwendtag $C$DW$T$52

	.dwendtag $C$DW$TU$52


$C$DW$TU$53	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$53

$C$DW$T$53	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$53, DW_AT_type(*$C$DW$T$49)
$C$DW$151	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$151, DW_AT_type(*$C$DW$T$49)

	.dwendtag $C$DW$T$53

	.dwendtag $C$DW$TU$53


$C$DW$TU$64	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$64

$C$DW$T$64	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$64, DW_AT_name("TIDL_CustomMaxPoolIxXOxXBuffParams")
	.dwattr $C$DW$T$64, DW_AT_byte_size(0x100)
$C$DW$152	.dwtag  DW_TAG_member
	.dwattr $C$DW$152, DW_AT_type(*$C$DW$T$57)
	.dwattr $C$DW$152, DW_AT_name("c7x")
	.dwattr $C$DW$152, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$152, DW_AT_decl_file("./tidsp/tidl_custom_maxpool_ixX_oxX_priv.h")
	.dwattr $C$DW$152, DW_AT_decl_line(0x4c)
	.dwattr $C$DW$152, DW_AT_decl_column(0x24)

$C$DW$153	.dwtag  DW_TAG_member
	.dwattr $C$DW$153, DW_AT_type(*$C$DW$T$58)
	.dwattr $C$DW$153, DW_AT_name("natc")
	.dwattr $C$DW$153, DW_AT_data_member_location[DW_OP_plus_uconst 0xd0]
	.dwattr $C$DW$153, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$153, DW_AT_decl_file("./tidsp/tidl_custom_maxpool_ixX_oxX_priv.h")
	.dwattr $C$DW$153, DW_AT_decl_line(0x4d)
	.dwattr $C$DW$153, DW_AT_decl_column(0x24)


$C$DW$154	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$154, DW_AT_name("operator =")
	.dwattr $C$DW$154, DW_AT_declaration
	.dwattr $C$DW$154, DW_AT_linkage_name("_ZN34TIDL_CustomMaxPoolIxXOxXBuffParamsaSERKS_")
	.dwattr $C$DW$154, DW_AT_type(*$C$DW$T$59)
	.dwattr $C$DW$154, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$155	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$155, DW_AT_type(*$C$DW$T$61)

	.dwendtag $C$DW$154


$C$DW$156	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$156, DW_AT_name("operator =")
	.dwattr $C$DW$156, DW_AT_declaration
	.dwattr $C$DW$156, DW_AT_linkage_name("_ZN34TIDL_CustomMaxPoolIxXOxXBuffParamsaSEOS_")
	.dwattr $C$DW$156, DW_AT_type(*$C$DW$T$59)
	.dwattr $C$DW$156, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$157	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$157, DW_AT_type(*$C$DW$T$59)

	.dwendtag $C$DW$156

	.dwattr $C$DW$T$64, DW_AT_decl_file("./tidsp/tidl_custom_maxpool_ixX_oxX_priv.h")
	.dwattr $C$DW$T$64, DW_AT_decl_line(0x4b)
	.dwattr $C$DW$T$64, DW_AT_decl_column(0x01)
	.dwendtag $C$DW$T$64

	.dwendtag $C$DW$TU$64


$C$DW$TU$60	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$60
$C$DW$T$60	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$60, DW_AT_type(*$C$DW$T$64)

	.dwendtag $C$DW$TU$60


$C$DW$TU$61	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$61
$C$DW$T$61	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$61, DW_AT_type(*$C$DW$T$60)
	.dwattr $C$DW$T$61, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$61


$C$DW$TU$87	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$87
$C$DW$T$87	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$87, DW_AT_name("TIDL_CustomMaxPoolIxXOxXBuffParams")
	.dwattr $C$DW$T$87, DW_AT_type(*$C$DW$T$64)
	.dwattr $C$DW$T$87, DW_AT_decl_file("./tidsp/tidl_custom_maxpool_ixX_oxX_priv.h")
	.dwattr $C$DW$T$87, DW_AT_decl_line(0x4e)
	.dwattr $C$DW$T$87, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$87


$C$DW$TU$59	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$59
$C$DW$T$59	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$59, DW_AT_type(*$C$DW$T$64)
	.dwattr $C$DW$T$59, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$59


$C$DW$TU$62	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$62

$C$DW$T$62	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$62, DW_AT_type(*$C$DW$T$59)
$C$DW$158	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$158, DW_AT_type(*$C$DW$T$61)

	.dwendtag $C$DW$T$62

	.dwendtag $C$DW$TU$62


$C$DW$TU$63	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$63

$C$DW$T$63	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$63, DW_AT_type(*$C$DW$T$59)
$C$DW$159	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$159, DW_AT_type(*$C$DW$T$59)

	.dwendtag $C$DW$T$63

	.dwendtag $C$DW$TU$63


$C$DW$TU$76	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$76

$C$DW$T$76	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$76, DW_AT_name("TIDL_CustomMaxPoolIxXOxXInitArgs")
	.dwattr $C$DW$T$76, DW_AT_byte_size(0x2c)
$C$DW$160	.dwtag  DW_TAG_member
	.dwattr $C$DW$160, DW_AT_type(*$C$DW$T$69)
	.dwattr $C$DW$160, DW_AT_name("funcStyle")
	.dwattr $C$DW$160, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$160, DW_AT_decl_file("./tidsp/tidl_custom_maxpool_ixX_oxX.h")
	.dwattr $C$DW$160, DW_AT_decl_line(0x33)
	.dwattr $C$DW$160, DW_AT_decl_column(0x0c)

$C$DW$161	.dwtag  DW_TAG_member
	.dwattr $C$DW$161, DW_AT_type(*$C$DW$T$70)
	.dwattr $C$DW$161, DW_AT_name("customMaxPoolParam")
	.dwattr $C$DW$161, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$161, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$161, DW_AT_decl_file("./tidsp/tidl_custom_maxpool_ixX_oxX.h")
	.dwattr $C$DW$161, DW_AT_decl_line(0x34)
	.dwattr $C$DW$161, DW_AT_decl_column(0x18)


$C$DW$162	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$162, DW_AT_name("operator =")
	.dwattr $C$DW$162, DW_AT_declaration
	.dwattr $C$DW$162, DW_AT_linkage_name("_ZN32TIDL_CustomMaxPoolIxXOxXInitArgsaSERKS_")
	.dwattr $C$DW$162, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$162, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$163	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$163, DW_AT_type(*$C$DW$T$73)

	.dwendtag $C$DW$162


$C$DW$164	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$164, DW_AT_name("operator =")
	.dwattr $C$DW$164, DW_AT_declaration
	.dwattr $C$DW$164, DW_AT_linkage_name("_ZN32TIDL_CustomMaxPoolIxXOxXInitArgsaSEOS_")
	.dwattr $C$DW$164, DW_AT_type(*$C$DW$T$71)
	.dwattr $C$DW$164, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$165	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$165, DW_AT_type(*$C$DW$T$71)

	.dwendtag $C$DW$164

	.dwattr $C$DW$T$76, DW_AT_decl_file("./tidsp/tidl_custom_maxpool_ixX_oxX.h")
	.dwattr $C$DW$T$76, DW_AT_decl_line(0x32)
	.dwattr $C$DW$T$76, DW_AT_decl_column(0x01)
	.dwendtag $C$DW$T$76

	.dwendtag $C$DW$TU$76


$C$DW$TU$72	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$72
$C$DW$T$72	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$72, DW_AT_type(*$C$DW$T$76)

	.dwendtag $C$DW$TU$72


$C$DW$TU$73	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$73
$C$DW$T$73	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$73, DW_AT_type(*$C$DW$T$72)
	.dwattr $C$DW$T$73, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$73


$C$DW$TU$81	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$81
$C$DW$T$81	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$81, DW_AT_name("TIDL_CustomMaxPoolIxXOxXInitArgs")
	.dwattr $C$DW$T$81, DW_AT_type(*$C$DW$T$76)
	.dwattr $C$DW$T$81, DW_AT_decl_file("./tidsp/tidl_custom_maxpool_ixX_oxX.h")
	.dwattr $C$DW$T$81, DW_AT_decl_line(0x35)
	.dwattr $C$DW$T$81, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$81


$C$DW$TU$226	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$226
$C$DW$T$226	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$226, DW_AT_type(*$C$DW$T$81)

	.dwendtag $C$DW$TU$226


$C$DW$TU$227	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$227
$C$DW$T$227	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$227, DW_AT_type(*$C$DW$T$226)
	.dwattr $C$DW$T$227, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$227


$C$DW$TU$71	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$71
$C$DW$T$71	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$71, DW_AT_type(*$C$DW$T$76)
	.dwattr $C$DW$T$71, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$71


$C$DW$TU$74	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$74

$C$DW$T$74	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$74, DW_AT_type(*$C$DW$T$71)
$C$DW$166	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$166, DW_AT_type(*$C$DW$T$73)

	.dwendtag $C$DW$T$74

	.dwendtag $C$DW$TU$74


$C$DW$TU$75	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$75

$C$DW$T$75	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$75, DW_AT_type(*$C$DW$T$71)
$C$DW$167	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$167, DW_AT_type(*$C$DW$T$71)

	.dwendtag $C$DW$T$75

	.dwendtag $C$DW$TU$75


$C$DW$TU$93	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$93

$C$DW$T$93	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$93, DW_AT_name("TIDL_CustomMaxPoolIxXOxXPrivArgs")
	.dwattr $C$DW$T$93, DW_AT_byte_size(0x140)
$C$DW$168	.dwtag  DW_TAG_member
	.dwattr $C$DW$168, DW_AT_type(*$C$DW$T$81)
	.dwattr $C$DW$168, DW_AT_name("initArgs")
	.dwattr $C$DW$168, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$168, DW_AT_decl_file("./tidsp/tidl_custom_maxpool_ixX_oxX_priv.h")
	.dwattr $C$DW$168, DW_AT_decl_line(0x52)
	.dwattr $C$DW$168, DW_AT_decl_column(0x24)

$C$DW$169	.dwtag  DW_TAG_member
	.dwattr $C$DW$169, DW_AT_type(*$C$DW$T$86)
	.dwattr $C$DW$169, DW_AT_name("execute")
	.dwattr $C$DW$169, DW_AT_data_member_location[DW_OP_plus_uconst 0x30]
	.dwattr $C$DW$169, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$169, DW_AT_decl_file("./tidsp/tidl_custom_maxpool_ixX_oxX_priv.h")
	.dwattr $C$DW$169, DW_AT_decl_line(0x53)
	.dwattr $C$DW$169, DW_AT_decl_column(0x1e)

$C$DW$170	.dwtag  DW_TAG_member
	.dwattr $C$DW$170, DW_AT_type(*$C$DW$T$87)
	.dwattr $C$DW$170, DW_AT_name("bufParams")
	.dwattr $C$DW$170, DW_AT_data_member_location[DW_OP_plus_uconst 0x38]
	.dwattr $C$DW$170, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$170, DW_AT_decl_file("./tidsp/tidl_custom_maxpool_ixX_oxX_priv.h")
	.dwattr $C$DW$170, DW_AT_decl_line(0x54)
	.dwattr $C$DW$170, DW_AT_decl_column(0x26)

$C$DW$171	.dwtag  DW_TAG_member
	.dwattr $C$DW$171, DW_AT_type(*$C$DW$T$69)
	.dwattr $C$DW$171, DW_AT_name("kernelHandleSize")
	.dwattr $C$DW$171, DW_AT_data_member_location[DW_OP_plus_uconst 0x138]
	.dwattr $C$DW$171, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$171, DW_AT_decl_file("./tidsp/tidl_custom_maxpool_ixX_oxX_priv.h")
	.dwattr $C$DW$171, DW_AT_decl_line(0x55)
	.dwattr $C$DW$171, DW_AT_decl_column(0x0b)


$C$DW$172	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$172, DW_AT_name("operator =")
	.dwattr $C$DW$172, DW_AT_declaration
	.dwattr $C$DW$172, DW_AT_linkage_name("_ZN32TIDL_CustomMaxPoolIxXOxXPrivArgsaSERKS_")
	.dwattr $C$DW$172, DW_AT_type(*$C$DW$T$88)
	.dwattr $C$DW$172, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$173	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$173, DW_AT_type(*$C$DW$T$90)

	.dwendtag $C$DW$172


$C$DW$174	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$174, DW_AT_name("operator =")
	.dwattr $C$DW$174, DW_AT_declaration
	.dwattr $C$DW$174, DW_AT_linkage_name("_ZN32TIDL_CustomMaxPoolIxXOxXPrivArgsaSEOS_")
	.dwattr $C$DW$174, DW_AT_type(*$C$DW$T$88)
	.dwattr $C$DW$174, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$175	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$175, DW_AT_type(*$C$DW$T$88)

	.dwendtag $C$DW$174

	.dwattr $C$DW$T$93, DW_AT_decl_file("./tidsp/tidl_custom_maxpool_ixX_oxX_priv.h")
	.dwattr $C$DW$T$93, DW_AT_decl_line(0x51)
	.dwattr $C$DW$T$93, DW_AT_decl_column(0x01)
	.dwendtag $C$DW$T$93

	.dwendtag $C$DW$TU$93


$C$DW$TU$89	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$89
$C$DW$T$89	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$89, DW_AT_type(*$C$DW$T$93)

	.dwendtag $C$DW$TU$89


$C$DW$TU$90	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$90
$C$DW$T$90	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$90, DW_AT_type(*$C$DW$T$89)
	.dwattr $C$DW$T$90, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$90


$C$DW$TU$235	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$235
$C$DW$T$235	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$235, DW_AT_name("TIDL_CustomMaxPoolIxXOxXPrivArgs")
	.dwattr $C$DW$T$235, DW_AT_type(*$C$DW$T$93)
	.dwattr $C$DW$T$235, DW_AT_decl_file("./tidsp/tidl_custom_maxpool_ixX_oxX_priv.h")
	.dwattr $C$DW$T$235, DW_AT_decl_line(0x56)
	.dwattr $C$DW$T$235, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$235


$C$DW$TU$236	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$236
$C$DW$T$236	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$236, DW_AT_type(*$C$DW$T$235)

	.dwendtag $C$DW$TU$236


$C$DW$TU$237	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$237
$C$DW$T$237	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$237, DW_AT_type(*$C$DW$T$236)
	.dwattr $C$DW$T$237, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$237


$C$DW$TU$238	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$238
$C$DW$T$238	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$238, DW_AT_type(*$C$DW$T$235)
	.dwattr $C$DW$T$238, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$238


$C$DW$TU$88	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$88
$C$DW$T$88	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$88, DW_AT_type(*$C$DW$T$93)
	.dwattr $C$DW$T$88, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$88


$C$DW$TU$91	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$91

$C$DW$T$91	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$91, DW_AT_type(*$C$DW$T$88)
$C$DW$176	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$176, DW_AT_type(*$C$DW$T$90)

	.dwendtag $C$DW$T$91

	.dwendtag $C$DW$TU$91


$C$DW$TU$92	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$92

$C$DW$T$92	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$92, DW_AT_type(*$C$DW$T$88)
$C$DW$177	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$177, DW_AT_type(*$C$DW$T$88)

	.dwendtag $C$DW$T$92

	.dwendtag $C$DW$TU$92


$C$DW$TU$105	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$105

$C$DW$T$105	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$105, DW_AT_name("TIDL_CustomParams0_t")
	.dwattr $C$DW$T$105, DW_AT_byte_size(0x28)
$C$DW$178	.dwtag  DW_TAG_member
	.dwattr $C$DW$178, DW_AT_type(*$C$DW$T$69)
	.dwattr $C$DW$178, DW_AT_name("poolingType")
	.dwattr $C$DW$178, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$178, DW_AT_decl_file("./tidl_custom.h")
	.dwattr $C$DW$178, DW_AT_decl_line(0x70)
	.dwattr $C$DW$178, DW_AT_decl_column(0x0d)

$C$DW$179	.dwtag  DW_TAG_member
	.dwattr $C$DW$179, DW_AT_type(*$C$DW$T$69)
	.dwattr $C$DW$179, DW_AT_name("kernelW")
	.dwattr $C$DW$179, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$179, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$179, DW_AT_decl_file("./tidl_custom.h")
	.dwattr $C$DW$179, DW_AT_decl_line(0x72)
	.dwattr $C$DW$179, DW_AT_decl_column(0x0d)

$C$DW$180	.dwtag  DW_TAG_member
	.dwattr $C$DW$180, DW_AT_type(*$C$DW$T$69)
	.dwattr $C$DW$180, DW_AT_name("kernelH")
	.dwattr $C$DW$180, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$180, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$180, DW_AT_decl_file("./tidl_custom.h")
	.dwattr $C$DW$180, DW_AT_decl_line(0x74)
	.dwattr $C$DW$180, DW_AT_decl_column(0x0d)

$C$DW$181	.dwtag  DW_TAG_member
	.dwattr $C$DW$181, DW_AT_type(*$C$DW$T$69)
	.dwattr $C$DW$181, DW_AT_name("strideW")
	.dwattr $C$DW$181, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$181, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$181, DW_AT_decl_file("./tidl_custom.h")
	.dwattr $C$DW$181, DW_AT_decl_line(0x76)
	.dwattr $C$DW$181, DW_AT_decl_column(0x0d)

$C$DW$182	.dwtag  DW_TAG_member
	.dwattr $C$DW$182, DW_AT_type(*$C$DW$T$69)
	.dwattr $C$DW$182, DW_AT_name("strideH")
	.dwattr $C$DW$182, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$182, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$182, DW_AT_decl_file("./tidl_custom.h")
	.dwattr $C$DW$182, DW_AT_decl_line(0x78)
	.dwattr $C$DW$182, DW_AT_decl_column(0x0d)

$C$DW$183	.dwtag  DW_TAG_member
	.dwattr $C$DW$183, DW_AT_type(*$C$DW$T$69)
	.dwattr $C$DW$183, DW_AT_name("padW")
	.dwattr $C$DW$183, DW_AT_data_member_location[DW_OP_plus_uconst 0x14]
	.dwattr $C$DW$183, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$183, DW_AT_decl_file("./tidl_custom.h")
	.dwattr $C$DW$183, DW_AT_decl_line(0x7a)
	.dwattr $C$DW$183, DW_AT_decl_column(0x0d)

$C$DW$184	.dwtag  DW_TAG_member
	.dwattr $C$DW$184, DW_AT_type(*$C$DW$T$69)
	.dwattr $C$DW$184, DW_AT_name("padH")
	.dwattr $C$DW$184, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$184, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$184, DW_AT_decl_file("./tidl_custom.h")
	.dwattr $C$DW$184, DW_AT_decl_line(0x7c)
	.dwattr $C$DW$184, DW_AT_decl_column(0x0d)

$C$DW$185	.dwtag  DW_TAG_member
	.dwattr $C$DW$185, DW_AT_type(*$C$DW$T$69)
	.dwattr $C$DW$185, DW_AT_name("inDataQ")
	.dwattr $C$DW$185, DW_AT_data_member_location[DW_OP_plus_uconst 0x1c]
	.dwattr $C$DW$185, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$185, DW_AT_decl_file("./tidl_custom.h")
	.dwattr $C$DW$185, DW_AT_decl_line(0x7e)
	.dwattr $C$DW$185, DW_AT_decl_column(0x0d)

$C$DW$186	.dwtag  DW_TAG_member
	.dwattr $C$DW$186, DW_AT_type(*$C$DW$T$69)
	.dwattr $C$DW$186, DW_AT_name("outDataQ")
	.dwattr $C$DW$186, DW_AT_data_member_location[DW_OP_plus_uconst 0x20]
	.dwattr $C$DW$186, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$186, DW_AT_decl_file("./tidl_custom.h")
	.dwattr $C$DW$186, DW_AT_decl_line(0x80)
	.dwattr $C$DW$186, DW_AT_decl_column(0x0d)

$C$DW$187	.dwtag  DW_TAG_member
	.dwattr $C$DW$187, DW_AT_type(*$C$DW$T$69)
	.dwattr $C$DW$187, DW_AT_name("useCeil")
	.dwattr $C$DW$187, DW_AT_data_member_location[DW_OP_plus_uconst 0x24]
	.dwattr $C$DW$187, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$187, DW_AT_decl_file("./tidl_custom.h")
	.dwattr $C$DW$187, DW_AT_decl_line(0x82)
	.dwattr $C$DW$187, DW_AT_decl_column(0x0d)


$C$DW$188	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$188, DW_AT_name("operator =")
	.dwattr $C$DW$188, DW_AT_declaration
	.dwattr $C$DW$188, DW_AT_linkage_name("_ZN20TIDL_CustomParams0_taSERKS_")
	.dwattr $C$DW$188, DW_AT_type(*$C$DW$T$100)
	.dwattr $C$DW$188, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$189	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$189, DW_AT_type(*$C$DW$T$102)

	.dwendtag $C$DW$188


$C$DW$190	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$190, DW_AT_name("operator =")
	.dwattr $C$DW$190, DW_AT_declaration
	.dwattr $C$DW$190, DW_AT_linkage_name("_ZN20TIDL_CustomParams0_taSEOS_")
	.dwattr $C$DW$190, DW_AT_type(*$C$DW$T$100)
	.dwattr $C$DW$190, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$191	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$191, DW_AT_type(*$C$DW$T$100)

	.dwendtag $C$DW$190

	.dwattr $C$DW$T$105, DW_AT_decl_file("./tidl_custom.h")
	.dwattr $C$DW$T$105, DW_AT_decl_line(0x6e)
	.dwattr $C$DW$T$105, DW_AT_decl_column(0x10)
	.dwendtag $C$DW$T$105

	.dwendtag $C$DW$TU$105


$C$DW$TU$70	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$70
$C$DW$T$70	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$70, DW_AT_name("TIDL_CustomParams0_t")
	.dwattr $C$DW$T$70, DW_AT_type(*$C$DW$T$105)
	.dwattr $C$DW$T$70, DW_AT_decl_file("./tidl_custom.h")
	.dwattr $C$DW$T$70, DW_AT_decl_line(0x83)
	.dwattr $C$DW$T$70, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$70


$C$DW$TU$101	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$101
$C$DW$T$101	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$101, DW_AT_type(*$C$DW$T$105)

	.dwendtag $C$DW$TU$101


$C$DW$TU$102	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$102
$C$DW$T$102	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$102, DW_AT_type(*$C$DW$T$101)
	.dwattr $C$DW$T$102, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$102


$C$DW$TU$100	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$100
$C$DW$T$100	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$100, DW_AT_type(*$C$DW$T$105)
	.dwattr $C$DW$T$100, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$100


$C$DW$TU$103	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$103

$C$DW$T$103	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$103, DW_AT_type(*$C$DW$T$100)
$C$DW$192	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$192, DW_AT_type(*$C$DW$T$102)

	.dwendtag $C$DW$T$103

	.dwendtag $C$DW$TU$103


$C$DW$TU$104	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$104

$C$DW$T$104	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$104, DW_AT_type(*$C$DW$T$100)
$C$DW$193	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$193, DW_AT_type(*$C$DW$T$100)

	.dwendtag $C$DW$T$104

	.dwendtag $C$DW$TU$104


$C$DW$TU$112	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$112

$C$DW$T$112	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$112, DW_AT_name("TIDL_bufParams3D_t")
	.dwattr $C$DW$T$112, DW_AT_byte_size(0x18)
$C$DW$194	.dwtag  DW_TAG_member
	.dwattr $C$DW$194, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$194, DW_AT_name("data_type")
	.dwattr $C$DW$194, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$194, DW_AT_decl_file("../inc/tidl_dataflow.h")
	.dwattr $C$DW$194, DW_AT_decl_line(0xc1)
	.dwattr $C$DW$194, DW_AT_decl_column(0x0c)

$C$DW$195	.dwtag  DW_TAG_member
	.dwattr $C$DW$195, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$195, DW_AT_name("dim_x")
	.dwattr $C$DW$195, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$195, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$195, DW_AT_decl_file("../inc/tidl_dataflow.h")
	.dwattr $C$DW$195, DW_AT_decl_line(0xc4)
	.dwattr $C$DW$195, DW_AT_decl_column(0x0c)

$C$DW$196	.dwtag  DW_TAG_member
	.dwattr $C$DW$196, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$196, DW_AT_name("dim_y")
	.dwattr $C$DW$196, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$196, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$196, DW_AT_decl_file("../inc/tidl_dataflow.h")
	.dwattr $C$DW$196, DW_AT_decl_line(0xc7)
	.dwattr $C$DW$196, DW_AT_decl_column(0x0c)

$C$DW$197	.dwtag  DW_TAG_member
	.dwattr $C$DW$197, DW_AT_type(*$C$DW$T$69)
	.dwattr $C$DW$197, DW_AT_name("stride_y")
	.dwattr $C$DW$197, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$197, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$197, DW_AT_decl_file("../inc/tidl_dataflow.h")
	.dwattr $C$DW$197, DW_AT_decl_line(0xc9)
	.dwattr $C$DW$197, DW_AT_decl_column(0x0c)

$C$DW$198	.dwtag  DW_TAG_member
	.dwattr $C$DW$198, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$198, DW_AT_name("dim_z")
	.dwattr $C$DW$198, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$198, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$198, DW_AT_decl_file("../inc/tidl_dataflow.h")
	.dwattr $C$DW$198, DW_AT_decl_line(0xcc)
	.dwattr $C$DW$198, DW_AT_decl_column(0x0c)

$C$DW$199	.dwtag  DW_TAG_member
	.dwattr $C$DW$199, DW_AT_type(*$C$DW$T$69)
	.dwattr $C$DW$199, DW_AT_name("stride_z")
	.dwattr $C$DW$199, DW_AT_data_member_location[DW_OP_plus_uconst 0x14]
	.dwattr $C$DW$199, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$199, DW_AT_decl_file("../inc/tidl_dataflow.h")
	.dwattr $C$DW$199, DW_AT_decl_line(0xce)
	.dwattr $C$DW$199, DW_AT_decl_column(0x0c)


$C$DW$200	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$200, DW_AT_name("operator =")
	.dwattr $C$DW$200, DW_AT_declaration
	.dwattr $C$DW$200, DW_AT_linkage_name("_ZN18TIDL_bufParams3D_taSERKS_")
	.dwattr $C$DW$200, DW_AT_type(*$C$DW$T$107)
	.dwattr $C$DW$200, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$201	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$201, DW_AT_type(*$C$DW$T$109)

	.dwendtag $C$DW$200


$C$DW$202	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$202, DW_AT_name("operator =")
	.dwattr $C$DW$202, DW_AT_declaration
	.dwattr $C$DW$202, DW_AT_linkage_name("_ZN18TIDL_bufParams3D_taSEOS_")
	.dwattr $C$DW$202, DW_AT_type(*$C$DW$T$107)
	.dwattr $C$DW$202, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$203	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$203, DW_AT_type(*$C$DW$T$107)

	.dwendtag $C$DW$202

	.dwattr $C$DW$T$112, DW_AT_decl_file("../inc/tidl_dataflow.h")
	.dwattr $C$DW$T$112, DW_AT_decl_line(0xbf)
	.dwattr $C$DW$T$112, DW_AT_decl_column(0x01)
	.dwendtag $C$DW$T$112

	.dwendtag $C$DW$TU$112


$C$DW$TU$48	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$48
$C$DW$T$48	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$48, DW_AT_name("TIDL_bufParams3D_t")
	.dwattr $C$DW$T$48, DW_AT_type(*$C$DW$T$112)
	.dwattr $C$DW$T$48, DW_AT_decl_file("../inc/tidl_dataflow.h")
	.dwattr $C$DW$T$48, DW_AT_decl_line(0xcf)
	.dwattr $C$DW$T$48, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$48


$C$DW$TU$251	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$251
$C$DW$T$251	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$251, DW_AT_type(*$C$DW$T$48)

	.dwendtag $C$DW$TU$251


$C$DW$TU$252	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$252
$C$DW$T$252	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$252, DW_AT_type(*$C$DW$T$251)
	.dwattr $C$DW$T$252, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$252


$C$DW$TU$108	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$108
$C$DW$T$108	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$108, DW_AT_type(*$C$DW$T$112)

	.dwendtag $C$DW$TU$108


$C$DW$TU$109	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$109
$C$DW$T$109	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$109, DW_AT_type(*$C$DW$T$108)
	.dwattr $C$DW$T$109, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$109


$C$DW$TU$107	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$107
$C$DW$T$107	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$107, DW_AT_type(*$C$DW$T$112)
	.dwattr $C$DW$T$107, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$107


$C$DW$TU$110	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$110

$C$DW$T$110	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$110, DW_AT_type(*$C$DW$T$107)
$C$DW$204	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$204, DW_AT_type(*$C$DW$T$109)

	.dwendtag $C$DW$T$110

	.dwendtag $C$DW$TU$110


$C$DW$TU$111	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$111

$C$DW$T$111	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$111, DW_AT_type(*$C$DW$T$107)
$C$DW$205	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$205, DW_AT_type(*$C$DW$T$107)

	.dwendtag $C$DW$T$111

	.dwendtag $C$DW$TU$111


$C$DW$TU$114	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$114

$C$DW$T$114	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$114, DW_AT_name("U3_0")
	.dwattr $C$DW$T$114, DW_AT_declaration
	.dwendtag $C$DW$T$114

	.dwendtag $C$DW$TU$114


$C$DW$TU$115	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$115

$C$DW$T$115	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$115, DW_AT_name("U4_14")
	.dwattr $C$DW$T$115, DW_AT_declaration
	.dwendtag $C$DW$T$115

	.dwendtag $C$DW$TU$115


$C$DW$TU$116	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$116

$C$DW$T$116	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$116, DW_AT_name("U4_2")
	.dwattr $C$DW$T$116, DW_AT_declaration
	.dwendtag $C$DW$T$116

	.dwendtag $C$DW$TU$116


$C$DW$TU$117	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$117

$C$DW$T$117	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$117, DW_AT_name("U4_6")
	.dwattr $C$DW$T$117, DW_AT_declaration
	.dwendtag $C$DW$T$117

	.dwendtag $C$DW$TU$117


$C$DW$TU$118	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$118

$C$DW$T$118	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$118, DW_AT_name("U5_16")
	.dwattr $C$DW$T$118, DW_AT_declaration
	.dwendtag $C$DW$T$118

	.dwendtag $C$DW$TU$118


$C$DW$TU$119	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$119

$C$DW$T$119	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$119, DW_AT_name("U5_8")
	.dwattr $C$DW$T$119, DW_AT_declaration
	.dwendtag $C$DW$T$119

	.dwendtag $C$DW$TU$119


$C$DW$TU$120	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$120

$C$DW$T$120	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$120, DW_AT_name("U6_10")
	.dwattr $C$DW$T$120, DW_AT_declaration
	.dwendtag $C$DW$T$120

	.dwendtag $C$DW$TU$120


$C$DW$TU$121	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$121

$C$DW$T$121	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$121, DW_AT_name("U6_18")
	.dwattr $C$DW$T$121, DW_AT_declaration
	.dwendtag $C$DW$T$121

	.dwendtag $C$DW$TU$121


$C$DW$TU$132	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$132

$C$DW$T$132	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$132, DW_AT_name("__SA_DECDIM")
	.dwattr $C$DW$T$132, DW_AT_byte_size(0x01)
$C$DW$206	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$206, DW_AT_name("__SA_DECDIM_DIM0")
	.dwattr $C$DW$206, DW_AT_const_value(0x00)
	.dwattr $C$DW$206, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$206, DW_AT_decl_line(0x1a2)
	.dwattr $C$DW$206, DW_AT_decl_column(0x05)

$C$DW$207	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$207, DW_AT_name("__SA_DECDIM_DIM1")
	.dwattr $C$DW$207, DW_AT_const_value(0x01)
	.dwattr $C$DW$207, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$207, DW_AT_decl_line(0x1a3)
	.dwattr $C$DW$207, DW_AT_decl_column(0x05)

$C$DW$208	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$208, DW_AT_name("__SA_DECDIM_DIM2")
	.dwattr $C$DW$208, DW_AT_const_value(0x02)
	.dwattr $C$DW$208, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$208, DW_AT_decl_line(0x1a4)
	.dwattr $C$DW$208, DW_AT_decl_column(0x05)

$C$DW$209	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$209, DW_AT_name("__SA_DECDIM_DIM3")
	.dwattr $C$DW$209, DW_AT_const_value(0x03)
	.dwattr $C$DW$209, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$209, DW_AT_decl_line(0x1a5)
	.dwattr $C$DW$209, DW_AT_decl_column(0x05)

$C$DW$210	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$210, DW_AT_name("__SA_DECDIM_DIM4")
	.dwattr $C$DW$210, DW_AT_const_value(0x04)
	.dwattr $C$DW$210, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$210, DW_AT_decl_line(0x1a6)
	.dwattr $C$DW$210, DW_AT_decl_column(0x05)

$C$DW$211	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$211, DW_AT_name("__SA_DECDIM_DIM5")
	.dwattr $C$DW$211, DW_AT_const_value(0x05)
	.dwattr $C$DW$211, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$211, DW_AT_decl_line(0x1a7)
	.dwattr $C$DW$211, DW_AT_decl_column(0x05)

	.dwattr $C$DW$T$132, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$T$132, DW_AT_decl_line(0x1a0)
	.dwattr $C$DW$T$132, DW_AT_decl_column(0x2a)
	.dwendtag $C$DW$T$132

	.dwendtag $C$DW$TU$132


$C$DW$TU$133	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$133
$C$DW$T$133	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$133, DW_AT_name("__SA_DECDIM")
	.dwattr $C$DW$T$133, DW_AT_type(*$C$DW$T$132)
	.dwattr $C$DW$T$133, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$T$133, DW_AT_decl_line(0x1a8)
	.dwattr $C$DW$T$133, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$133


$C$DW$TU$143	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$143

$C$DW$T$143	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$143, DW_AT_name("__SA_DECDIM")
	.dwattr $C$DW$T$143, DW_AT_byte_size(0x01)
$C$DW$212	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$212, DW_AT_name("__SA_DECDIM_DIM0")
	.dwattr $C$DW$212, DW_AT_const_value(0x00)
	.dwattr $C$DW$212, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$212, DW_AT_decl_line(0x1a2)
	.dwattr $C$DW$212, DW_AT_decl_column(0x05)

$C$DW$213	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$213, DW_AT_name("__SA_DECDIM_DIM1")
	.dwattr $C$DW$213, DW_AT_const_value(0x01)
	.dwattr $C$DW$213, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$213, DW_AT_decl_line(0x1a3)
	.dwattr $C$DW$213, DW_AT_decl_column(0x05)

$C$DW$214	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$214, DW_AT_name("__SA_DECDIM_DIM2")
	.dwattr $C$DW$214, DW_AT_const_value(0x02)
	.dwattr $C$DW$214, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$214, DW_AT_decl_line(0x1a4)
	.dwattr $C$DW$214, DW_AT_decl_column(0x05)

$C$DW$215	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$215, DW_AT_name("__SA_DECDIM_DIM3")
	.dwattr $C$DW$215, DW_AT_const_value(0x03)
	.dwattr $C$DW$215, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$215, DW_AT_decl_line(0x1a5)
	.dwattr $C$DW$215, DW_AT_decl_column(0x05)

$C$DW$216	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$216, DW_AT_name("__SA_DECDIM_DIM4")
	.dwattr $C$DW$216, DW_AT_const_value(0x04)
	.dwattr $C$DW$216, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$216, DW_AT_decl_line(0x1a6)
	.dwattr $C$DW$216, DW_AT_decl_column(0x05)

$C$DW$217	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$217, DW_AT_name("__SA_DECDIM_DIM5")
	.dwattr $C$DW$217, DW_AT_const_value(0x05)
	.dwattr $C$DW$217, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$217, DW_AT_decl_line(0x1a7)
	.dwattr $C$DW$217, DW_AT_decl_column(0x05)

	.dwattr $C$DW$T$143, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$T$143, DW_AT_decl_line(0x1a0)
	.dwattr $C$DW$T$143, DW_AT_decl_column(0x2a)
	.dwendtag $C$DW$T$143

	.dwendtag $C$DW$TU$143


$C$DW$TU$144	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$144
$C$DW$T$144	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$144, DW_AT_name("__SA_DECDIM")
	.dwattr $C$DW$T$144, DW_AT_type(*$C$DW$T$143)
	.dwattr $C$DW$T$144, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$T$144, DW_AT_decl_line(0x1a8)
	.dwattr $C$DW$T$144, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$144


$C$DW$TU$134	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$134

$C$DW$T$134	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$134, DW_AT_name("__SA_DECDIMSD")
	.dwattr $C$DW$T$134, DW_AT_byte_size(0x01)
$C$DW$218	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$218, DW_AT_name("__SA_DECDIMSD_DIM0")
	.dwattr $C$DW$218, DW_AT_const_value(0x00)
	.dwattr $C$DW$218, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$218, DW_AT_decl_line(0x1af)
	.dwattr $C$DW$218, DW_AT_decl_column(0x05)

$C$DW$219	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$219, DW_AT_name("__SA_DECDIMSD_DIM1")
	.dwattr $C$DW$219, DW_AT_const_value(0x01)
	.dwattr $C$DW$219, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$219, DW_AT_decl_line(0x1b0)
	.dwattr $C$DW$219, DW_AT_decl_column(0x05)

$C$DW$220	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$220, DW_AT_name("__SA_DECDIMSD_DIM2")
	.dwattr $C$DW$220, DW_AT_const_value(0x02)
	.dwattr $C$DW$220, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$220, DW_AT_decl_line(0x1b1)
	.dwattr $C$DW$220, DW_AT_decl_column(0x05)

$C$DW$221	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$221, DW_AT_name("__SA_DECDIMSD_DIM3")
	.dwattr $C$DW$221, DW_AT_const_value(0x03)
	.dwattr $C$DW$221, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$221, DW_AT_decl_line(0x1b2)
	.dwattr $C$DW$221, DW_AT_decl_column(0x05)

	.dwattr $C$DW$T$134, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$T$134, DW_AT_decl_line(0x1ad)
	.dwattr $C$DW$T$134, DW_AT_decl_column(0x2a)
	.dwendtag $C$DW$T$134

	.dwendtag $C$DW$TU$134


$C$DW$TU$135	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$135
$C$DW$T$135	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$135, DW_AT_name("__SA_DECDIMSD")
	.dwattr $C$DW$T$135, DW_AT_type(*$C$DW$T$134)
	.dwattr $C$DW$T$135, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$T$135, DW_AT_decl_line(0x1b3)
	.dwattr $C$DW$T$135, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$135


$C$DW$TU$145	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$145

$C$DW$T$145	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$145, DW_AT_name("__SA_DECDIMSD")
	.dwattr $C$DW$T$145, DW_AT_byte_size(0x01)
$C$DW$222	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$222, DW_AT_name("__SA_DECDIMSD_DIM0")
	.dwattr $C$DW$222, DW_AT_const_value(0x00)
	.dwattr $C$DW$222, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$222, DW_AT_decl_line(0x1af)
	.dwattr $C$DW$222, DW_AT_decl_column(0x05)

$C$DW$223	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$223, DW_AT_name("__SA_DECDIMSD_DIM1")
	.dwattr $C$DW$223, DW_AT_const_value(0x01)
	.dwattr $C$DW$223, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$223, DW_AT_decl_line(0x1b0)
	.dwattr $C$DW$223, DW_AT_decl_column(0x05)

$C$DW$224	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$224, DW_AT_name("__SA_DECDIMSD_DIM2")
	.dwattr $C$DW$224, DW_AT_const_value(0x02)
	.dwattr $C$DW$224, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$224, DW_AT_decl_line(0x1b1)
	.dwattr $C$DW$224, DW_AT_decl_column(0x05)

$C$DW$225	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$225, DW_AT_name("__SA_DECDIMSD_DIM3")
	.dwattr $C$DW$225, DW_AT_const_value(0x03)
	.dwattr $C$DW$225, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$225, DW_AT_decl_line(0x1b2)
	.dwattr $C$DW$225, DW_AT_decl_column(0x05)

	.dwattr $C$DW$T$145, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$T$145, DW_AT_decl_line(0x1ad)
	.dwattr $C$DW$T$145, DW_AT_decl_column(0x2a)
	.dwendtag $C$DW$T$145

	.dwendtag $C$DW$TU$145


$C$DW$TU$146	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$146
$C$DW$T$146	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$146, DW_AT_name("__SA_DECDIMSD")
	.dwattr $C$DW$T$146, DW_AT_type(*$C$DW$T$145)
	.dwattr $C$DW$T$146, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$T$146, DW_AT_decl_line(0x1b3)
	.dwattr $C$DW$T$146, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$146


$C$DW$TU$130	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$130

$C$DW$T$130	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$130, DW_AT_name("__SA_DIMFMT")
	.dwattr $C$DW$T$130, DW_AT_byte_size(0x01)
$C$DW$226	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$226, DW_AT_name("__SA_DIMFMT_1D")
	.dwattr $C$DW$226, DW_AT_const_value(0x00)
	.dwattr $C$DW$226, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$226, DW_AT_decl_line(0x192)
	.dwattr $C$DW$226, DW_AT_decl_column(0x05)

$C$DW$227	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$227, DW_AT_name("__SA_DIMFMT_2D")
	.dwattr $C$DW$227, DW_AT_const_value(0x01)
	.dwattr $C$DW$227, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$227, DW_AT_decl_line(0x193)
	.dwattr $C$DW$227, DW_AT_decl_column(0x05)

$C$DW$228	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$228, DW_AT_name("__SA_DIMFMT_3D")
	.dwattr $C$DW$228, DW_AT_const_value(0x02)
	.dwattr $C$DW$228, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$228, DW_AT_decl_line(0x194)
	.dwattr $C$DW$228, DW_AT_decl_column(0x05)

$C$DW$229	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$229, DW_AT_name("__SA_DIMFMT_4D")
	.dwattr $C$DW$229, DW_AT_const_value(0x03)
	.dwattr $C$DW$229, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$229, DW_AT_decl_line(0x195)
	.dwattr $C$DW$229, DW_AT_decl_column(0x05)

$C$DW$230	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$230, DW_AT_name("__SA_DIMFMT_5D")
	.dwattr $C$DW$230, DW_AT_const_value(0x04)
	.dwattr $C$DW$230, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$230, DW_AT_decl_line(0x196)
	.dwattr $C$DW$230, DW_AT_decl_column(0x05)

$C$DW$231	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$231, DW_AT_name("__SA_DIMFMT_6D")
	.dwattr $C$DW$231, DW_AT_const_value(0x05)
	.dwattr $C$DW$231, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$231, DW_AT_decl_line(0x197)
	.dwattr $C$DW$231, DW_AT_decl_column(0x05)

	.dwattr $C$DW$T$130, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$T$130, DW_AT_decl_line(0x190)
	.dwattr $C$DW$T$130, DW_AT_decl_column(0x2a)
	.dwendtag $C$DW$T$130

	.dwendtag $C$DW$TU$130


$C$DW$TU$131	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$131
$C$DW$T$131	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$131, DW_AT_name("__SA_DIMFMT")
	.dwattr $C$DW$T$131, DW_AT_type(*$C$DW$T$130)
	.dwattr $C$DW$T$131, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$T$131, DW_AT_decl_line(0x198)
	.dwattr $C$DW$T$131, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$131


$C$DW$TU$141	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$141

$C$DW$T$141	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$141, DW_AT_name("__SA_DIMFMT")
	.dwattr $C$DW$T$141, DW_AT_byte_size(0x01)
$C$DW$232	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$232, DW_AT_name("__SA_DIMFMT_1D")
	.dwattr $C$DW$232, DW_AT_const_value(0x00)
	.dwattr $C$DW$232, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$232, DW_AT_decl_line(0x192)
	.dwattr $C$DW$232, DW_AT_decl_column(0x05)

$C$DW$233	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$233, DW_AT_name("__SA_DIMFMT_2D")
	.dwattr $C$DW$233, DW_AT_const_value(0x01)
	.dwattr $C$DW$233, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$233, DW_AT_decl_line(0x193)
	.dwattr $C$DW$233, DW_AT_decl_column(0x05)

$C$DW$234	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$234, DW_AT_name("__SA_DIMFMT_3D")
	.dwattr $C$DW$234, DW_AT_const_value(0x02)
	.dwattr $C$DW$234, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$234, DW_AT_decl_line(0x194)
	.dwattr $C$DW$234, DW_AT_decl_column(0x05)

$C$DW$235	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$235, DW_AT_name("__SA_DIMFMT_4D")
	.dwattr $C$DW$235, DW_AT_const_value(0x03)
	.dwattr $C$DW$235, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$235, DW_AT_decl_line(0x195)
	.dwattr $C$DW$235, DW_AT_decl_column(0x05)

$C$DW$236	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$236, DW_AT_name("__SA_DIMFMT_5D")
	.dwattr $C$DW$236, DW_AT_const_value(0x04)
	.dwattr $C$DW$236, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$236, DW_AT_decl_line(0x196)
	.dwattr $C$DW$236, DW_AT_decl_column(0x05)

$C$DW$237	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$237, DW_AT_name("__SA_DIMFMT_6D")
	.dwattr $C$DW$237, DW_AT_const_value(0x05)
	.dwattr $C$DW$237, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$237, DW_AT_decl_line(0x197)
	.dwattr $C$DW$237, DW_AT_decl_column(0x05)

	.dwattr $C$DW$T$141, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$T$141, DW_AT_decl_line(0x190)
	.dwattr $C$DW$T$141, DW_AT_decl_column(0x2a)
	.dwendtag $C$DW$T$141

	.dwendtag $C$DW$TU$141


$C$DW$TU$142	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$142
$C$DW$T$142	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$142, DW_AT_name("__SA_DIMFMT")
	.dwattr $C$DW$T$142, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$T$142, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$T$142, DW_AT_decl_line(0x198)
	.dwattr $C$DW$T$142, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$142


$C$DW$TU$128	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$128

$C$DW$T$128	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$128, DW_AT_name("__SA_INV_DD")
	.dwattr $C$DW$T$128, DW_AT_byte_size(0x01)
$C$DW$238	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$238, DW_AT_name("__SA_INV_DD_OFF")
	.dwattr $C$DW$238, DW_AT_const_value(0x00)
	.dwattr $C$DW$238, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$238, DW_AT_decl_line(0x189)
	.dwattr $C$DW$238, DW_AT_decl_column(0x05)

$C$DW$239	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$239, DW_AT_name("__SA_INV_DD_ON")
	.dwattr $C$DW$239, DW_AT_const_value(0x01)
	.dwattr $C$DW$239, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$239, DW_AT_decl_line(0x18a)
	.dwattr $C$DW$239, DW_AT_decl_column(0x05)

	.dwattr $C$DW$T$128, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$T$128, DW_AT_decl_line(0x187)
	.dwattr $C$DW$T$128, DW_AT_decl_column(0x2a)
	.dwendtag $C$DW$T$128

	.dwendtag $C$DW$TU$128


$C$DW$TU$129	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$129
$C$DW$T$129	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$129, DW_AT_name("__SA_INV_DD")
	.dwattr $C$DW$T$129, DW_AT_type(*$C$DW$T$128)
	.dwattr $C$DW$T$129, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$T$129, DW_AT_decl_line(0x18b)
	.dwattr $C$DW$T$129, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$129


$C$DW$TU$139	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$139

$C$DW$T$139	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$139, DW_AT_name("__SA_INV_DD")
	.dwattr $C$DW$T$139, DW_AT_byte_size(0x01)
$C$DW$240	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$240, DW_AT_name("__SA_INV_DD_OFF")
	.dwattr $C$DW$240, DW_AT_const_value(0x00)
	.dwattr $C$DW$240, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$240, DW_AT_decl_line(0x189)
	.dwattr $C$DW$240, DW_AT_decl_column(0x05)

$C$DW$241	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$241, DW_AT_name("__SA_INV_DD_ON")
	.dwattr $C$DW$241, DW_AT_const_value(0x01)
	.dwattr $C$DW$241, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$241, DW_AT_decl_line(0x18a)
	.dwattr $C$DW$241, DW_AT_decl_column(0x05)

	.dwattr $C$DW$T$139, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$T$139, DW_AT_decl_line(0x187)
	.dwattr $C$DW$T$139, DW_AT_decl_column(0x2a)
	.dwendtag $C$DW$T$139

	.dwendtag $C$DW$TU$139


$C$DW$TU$140	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$140
$C$DW$T$140	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$140, DW_AT_name("__SA_INV_DD")
	.dwattr $C$DW$T$140, DW_AT_type(*$C$DW$T$139)
	.dwattr $C$DW$T$140, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$T$140, DW_AT_decl_line(0x18b)
	.dwattr $C$DW$T$140, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$140


$C$DW$TU$136	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$136

$C$DW$T$136	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$136, DW_AT_name("__SA_TEMPLATE_v1")
	.dwattr $C$DW$T$136, DW_AT_byte_size(0x40)
$C$DW$242	.dwtag  DW_TAG_member
	.dwattr $C$DW$242, DW_AT_type(*$C$DW$T$123)
	.dwattr $C$DW$242, DW_AT_name("ICNT0")
	.dwattr $C$DW$242, DW_AT_bit_offset(0x20)
	.dwattr $C$DW$242, DW_AT_bit_size(0x20)
	.dwattr $C$DW$242, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$242, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$242, DW_AT_decl_line(0x1bc)
	.dwattr $C$DW$242, DW_AT_decl_column(0x0e)

$C$DW$243	.dwtag  DW_TAG_member
	.dwattr $C$DW$243, DW_AT_type(*$C$DW$T$123)
	.dwattr $C$DW$243, DW_AT_name("ICNT1")
	.dwattr $C$DW$243, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$243, DW_AT_bit_size(0x20)
	.dwattr $C$DW$243, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$243, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$243, DW_AT_decl_line(0x1bd)
	.dwattr $C$DW$243, DW_AT_decl_column(0x0e)

$C$DW$244	.dwtag  DW_TAG_member
	.dwattr $C$DW$244, DW_AT_type(*$C$DW$T$123)
	.dwattr $C$DW$244, DW_AT_name("ICNT2")
	.dwattr $C$DW$244, DW_AT_bit_offset(0x20)
	.dwattr $C$DW$244, DW_AT_bit_size(0x20)
	.dwattr $C$DW$244, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$244, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$244, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$244, DW_AT_decl_line(0x1be)
	.dwattr $C$DW$244, DW_AT_decl_column(0x0e)

$C$DW$245	.dwtag  DW_TAG_member
	.dwattr $C$DW$245, DW_AT_type(*$C$DW$T$123)
	.dwattr $C$DW$245, DW_AT_name("ICNT3")
	.dwattr $C$DW$245, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$245, DW_AT_bit_size(0x20)
	.dwattr $C$DW$245, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$245, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$245, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$245, DW_AT_decl_line(0x1bf)
	.dwattr $C$DW$245, DW_AT_decl_column(0x0e)

$C$DW$246	.dwtag  DW_TAG_member
	.dwattr $C$DW$246, DW_AT_type(*$C$DW$T$123)
	.dwattr $C$DW$246, DW_AT_name("ICNT4")
	.dwattr $C$DW$246, DW_AT_bit_offset(0x20)
	.dwattr $C$DW$246, DW_AT_bit_size(0x20)
	.dwattr $C$DW$246, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$246, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$246, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$246, DW_AT_decl_line(0x1c0)
	.dwattr $C$DW$246, DW_AT_decl_column(0x0e)

$C$DW$247	.dwtag  DW_TAG_member
	.dwattr $C$DW$247, DW_AT_type(*$C$DW$T$123)
	.dwattr $C$DW$247, DW_AT_name("ICNT5")
	.dwattr $C$DW$247, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$247, DW_AT_bit_size(0x20)
	.dwattr $C$DW$247, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$247, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$247, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$247, DW_AT_decl_line(0x1c1)
	.dwattr $C$DW$247, DW_AT_decl_column(0x0e)

$C$DW$248	.dwtag  DW_TAG_member
	.dwattr $C$DW$248, DW_AT_type(*$C$DW$T$123)
	.dwattr $C$DW$248, DW_AT_name("DECDIM1_WIDTH")
	.dwattr $C$DW$248, DW_AT_bit_offset(0x20)
	.dwattr $C$DW$248, DW_AT_bit_size(0x20)
	.dwattr $C$DW$248, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$248, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$248, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$248, DW_AT_decl_line(0x1c2)
	.dwattr $C$DW$248, DW_AT_decl_column(0x0e)

$C$DW$249	.dwtag  DW_TAG_member
	.dwattr $C$DW$249, DW_AT_type(*$C$DW$T$123)
	.dwattr $C$DW$249, DW_AT_name("DECDIM2_WIDTH")
	.dwattr $C$DW$249, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$249, DW_AT_bit_size(0x20)
	.dwattr $C$DW$249, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$249, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$249, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$249, DW_AT_decl_line(0x1c3)
	.dwattr $C$DW$249, DW_AT_decl_column(0x0e)

$C$DW$250	.dwtag  DW_TAG_member
	.dwattr $C$DW$250, DW_AT_type(*$C$DW$T$125)
	.dwattr $C$DW$250, DW_AT_name("DIM1")
	.dwattr $C$DW$250, DW_AT_bit_offset(0x20)
	.dwattr $C$DW$250, DW_AT_bit_size(0x20)
	.dwattr $C$DW$250, DW_AT_data_member_location[DW_OP_plus_uconst 0x20]
	.dwattr $C$DW$250, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$250, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$250, DW_AT_decl_line(0x1c4)
	.dwattr $C$DW$250, DW_AT_decl_column(0x0e)

$C$DW$251	.dwtag  DW_TAG_member
	.dwattr $C$DW$251, DW_AT_type(*$C$DW$T$125)
	.dwattr $C$DW$251, DW_AT_name("DIM2")
	.dwattr $C$DW$251, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$251, DW_AT_bit_size(0x20)
	.dwattr $C$DW$251, DW_AT_data_member_location[DW_OP_plus_uconst 0x20]
	.dwattr $C$DW$251, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$251, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$251, DW_AT_decl_line(0x1c5)
	.dwattr $C$DW$251, DW_AT_decl_column(0x0e)

$C$DW$252	.dwtag  DW_TAG_member
	.dwattr $C$DW$252, DW_AT_type(*$C$DW$T$125)
	.dwattr $C$DW$252, DW_AT_name("DIM3")
	.dwattr $C$DW$252, DW_AT_bit_offset(0x20)
	.dwattr $C$DW$252, DW_AT_bit_size(0x20)
	.dwattr $C$DW$252, DW_AT_data_member_location[DW_OP_plus_uconst 0x28]
	.dwattr $C$DW$252, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$252, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$252, DW_AT_decl_line(0x1c6)
	.dwattr $C$DW$252, DW_AT_decl_column(0x0e)

$C$DW$253	.dwtag  DW_TAG_member
	.dwattr $C$DW$253, DW_AT_type(*$C$DW$T$125)
	.dwattr $C$DW$253, DW_AT_name("DIM4")
	.dwattr $C$DW$253, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$253, DW_AT_bit_size(0x20)
	.dwattr $C$DW$253, DW_AT_data_member_location[DW_OP_plus_uconst 0x28]
	.dwattr $C$DW$253, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$253, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$253, DW_AT_decl_line(0x1c7)
	.dwattr $C$DW$253, DW_AT_decl_column(0x0e)

$C$DW$254	.dwtag  DW_TAG_member
	.dwattr $C$DW$254, DW_AT_type(*$C$DW$T$125)
	.dwattr $C$DW$254, DW_AT_name("DIM5")
	.dwattr $C$DW$254, DW_AT_bit_offset(0x20)
	.dwattr $C$DW$254, DW_AT_bit_size(0x20)
	.dwattr $C$DW$254, DW_AT_data_member_location[DW_OP_plus_uconst 0x30]
	.dwattr $C$DW$254, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$254, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$254, DW_AT_decl_line(0x1c8)
	.dwattr $C$DW$254, DW_AT_decl_column(0x0e)

$C$DW$255	.dwtag  DW_TAG_member
	.dwattr $C$DW$255, DW_AT_type(*$C$DW$T$123)
	.dwattr $C$DW$255, DW_AT_name("_reserved1")
	.dwattr $C$DW$255, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$255, DW_AT_bit_size(0x20)
	.dwattr $C$DW$255, DW_AT_data_member_location[DW_OP_plus_uconst 0x30]
	.dwattr $C$DW$255, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$255, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$255, DW_AT_decl_line(0x1c9)
	.dwattr $C$DW$255, DW_AT_decl_column(0x0e)

$C$DW$256	.dwtag  DW_TAG_member
	.dwattr $C$DW$256, DW_AT_type(*$C$DW$T$123)
	.dwattr $C$DW$256, DW_AT_name("_reserved2")
	.dwattr $C$DW$256, DW_AT_bit_offset(0x34)
	.dwattr $C$DW$256, DW_AT_bit_size(0x0c)
	.dwattr $C$DW$256, DW_AT_data_member_location[DW_OP_plus_uconst 0x38]
	.dwattr $C$DW$256, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$256, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$256, DW_AT_decl_line(0x1cb)
	.dwattr $C$DW$256, DW_AT_decl_column(0x0e)

$C$DW$257	.dwtag  DW_TAG_member
	.dwattr $C$DW$257, DW_AT_type(*$C$DW$T$127)
	.dwattr $C$DW$257, DW_AT_name("VECLEN")
	.dwattr $C$DW$257, DW_AT_bit_offset(0x01)
	.dwattr $C$DW$257, DW_AT_bit_size(0x03)
	.dwattr $C$DW$257, DW_AT_data_member_location[DW_OP_plus_uconst 0x39]
	.dwattr $C$DW$257, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$257, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$257, DW_AT_decl_line(0x1cc)
	.dwattr $C$DW$257, DW_AT_decl_column(0x11)

$C$DW$258	.dwtag  DW_TAG_member
	.dwattr $C$DW$258, DW_AT_type(*$C$DW$T$123)
	.dwattr $C$DW$258, DW_AT_name("_reserved3")
	.dwattr $C$DW$258, DW_AT_bit_offset(0x2a)
	.dwattr $C$DW$258, DW_AT_bit_size(0x07)
	.dwattr $C$DW$258, DW_AT_data_member_location[DW_OP_plus_uconst 0x38]
	.dwattr $C$DW$258, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$258, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$258, DW_AT_decl_line(0x1cd)
	.dwattr $C$DW$258, DW_AT_decl_column(0x0e)

$C$DW$259	.dwtag  DW_TAG_member
	.dwattr $C$DW$259, DW_AT_type(*$C$DW$T$129)
	.dwattr $C$DW$259, DW_AT_name("INV_DD1")
	.dwattr $C$DW$259, DW_AT_bit_offset(0x01)
	.dwattr $C$DW$259, DW_AT_bit_size(0x01)
	.dwattr $C$DW$259, DW_AT_data_member_location[DW_OP_plus_uconst 0x3a]
	.dwattr $C$DW$259, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$259, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$259, DW_AT_decl_line(0x1ce)
	.dwattr $C$DW$259, DW_AT_decl_column(0x11)

$C$DW$260	.dwtag  DW_TAG_member
	.dwattr $C$DW$260, DW_AT_type(*$C$DW$T$129)
	.dwattr $C$DW$260, DW_AT_name("INV_DD2")
	.dwattr $C$DW$260, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$260, DW_AT_bit_size(0x01)
	.dwattr $C$DW$260, DW_AT_data_member_location[DW_OP_plus_uconst 0x3a]
	.dwattr $C$DW$260, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$260, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$260, DW_AT_decl_line(0x1cf)
	.dwattr $C$DW$260, DW_AT_decl_column(0x11)

$C$DW$261	.dwtag  DW_TAG_member
	.dwattr $C$DW$261, DW_AT_type(*$C$DW$T$131)
	.dwattr $C$DW$261, DW_AT_name("DIMFMT")
	.dwattr $C$DW$261, DW_AT_bit_offset(0x05)
	.dwattr $C$DW$261, DW_AT_bit_size(0x03)
	.dwattr $C$DW$261, DW_AT_data_member_location[DW_OP_plus_uconst 0x3b]
	.dwattr $C$DW$261, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$261, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$261, DW_AT_decl_line(0x1d0)
	.dwattr $C$DW$261, DW_AT_decl_column(0x11)

$C$DW$262	.dwtag  DW_TAG_member
	.dwattr $C$DW$262, DW_AT_type(*$C$DW$T$123)
	.dwattr $C$DW$262, DW_AT_name("_reserved4")
	.dwattr $C$DW$262, DW_AT_bit_offset(0x20)
	.dwattr $C$DW$262, DW_AT_bit_size(0x05)
	.dwattr $C$DW$262, DW_AT_data_member_location[DW_OP_plus_uconst 0x38]
	.dwattr $C$DW$262, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$262, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$262, DW_AT_decl_line(0x1d1)
	.dwattr $C$DW$262, DW_AT_decl_column(0x0e)

$C$DW$263	.dwtag  DW_TAG_member
	.dwattr $C$DW$263, DW_AT_type(*$C$DW$T$123)
	.dwattr $C$DW$263, DW_AT_name("_reserved5")
	.dwattr $C$DW$263, DW_AT_bit_offset(0x10)
	.dwattr $C$DW$263, DW_AT_bit_size(0x10)
	.dwattr $C$DW$263, DW_AT_data_member_location[DW_OP_plus_uconst 0x38]
	.dwattr $C$DW$263, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$263, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$263, DW_AT_decl_line(0x1d2)
	.dwattr $C$DW$263, DW_AT_decl_column(0x0e)

$C$DW$264	.dwtag  DW_TAG_member
	.dwattr $C$DW$264, DW_AT_type(*$C$DW$T$133)
	.dwattr $C$DW$264, DW_AT_name("DECDIM1")
	.dwattr $C$DW$264, DW_AT_bit_offset(0x05)
	.dwattr $C$DW$264, DW_AT_bit_size(0x03)
	.dwattr $C$DW$264, DW_AT_data_member_location[DW_OP_plus_uconst 0x3e]
	.dwattr $C$DW$264, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$264, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$264, DW_AT_decl_line(0x1d3)
	.dwattr $C$DW$264, DW_AT_decl_column(0x11)

$C$DW$265	.dwtag  DW_TAG_member
	.dwattr $C$DW$265, DW_AT_type(*$C$DW$T$135)
	.dwattr $C$DW$265, DW_AT_name("DECDIM1SD")
	.dwattr $C$DW$265, DW_AT_bit_offset(0x03)
	.dwattr $C$DW$265, DW_AT_bit_size(0x02)
	.dwattr $C$DW$265, DW_AT_data_member_location[DW_OP_plus_uconst 0x3e]
	.dwattr $C$DW$265, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$265, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$265, DW_AT_decl_line(0x1d4)
	.dwattr $C$DW$265, DW_AT_decl_column(0x13)

$C$DW$266	.dwtag  DW_TAG_member
	.dwattr $C$DW$266, DW_AT_type(*$C$DW$T$133)
	.dwattr $C$DW$266, DW_AT_name("DECDIM2")
	.dwattr $C$DW$266, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$266, DW_AT_bit_size(0x03)
	.dwattr $C$DW$266, DW_AT_data_member_location[DW_OP_plus_uconst 0x3e]
	.dwattr $C$DW$266, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$266, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$266, DW_AT_decl_line(0x1d5)
	.dwattr $C$DW$266, DW_AT_decl_column(0x11)

$C$DW$267	.dwtag  DW_TAG_member
	.dwattr $C$DW$267, DW_AT_type(*$C$DW$T$135)
	.dwattr $C$DW$267, DW_AT_name("DECDIM2SD")
	.dwattr $C$DW$267, DW_AT_bit_offset(0x06)
	.dwattr $C$DW$267, DW_AT_bit_size(0x02)
	.dwattr $C$DW$267, DW_AT_data_member_location[DW_OP_plus_uconst 0x3f]
	.dwattr $C$DW$267, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$267, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$267, DW_AT_decl_line(0x1d6)
	.dwattr $C$DW$267, DW_AT_decl_column(0x13)

$C$DW$268	.dwtag  DW_TAG_member
	.dwattr $C$DW$268, DW_AT_type(*$C$DW$T$123)
	.dwattr $C$DW$268, DW_AT_name("_reserved")
	.dwattr $C$DW$268, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$268, DW_AT_bit_size(0x06)
	.dwattr $C$DW$268, DW_AT_data_member_location[DW_OP_plus_uconst 0x38]
	.dwattr $C$DW$268, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$268, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$268, DW_AT_decl_line(0x1d7)
	.dwattr $C$DW$268, DW_AT_decl_column(0x0e)

	.dwattr $C$DW$T$136, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$T$136, DW_AT_decl_line(0x1b9)
	.dwattr $C$DW$T$136, DW_AT_decl_column(0x2c)
	.dwendtag $C$DW$T$136

	.dwendtag $C$DW$TU$136


$C$DW$TU$152	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$152

$C$DW$T$152	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$152, DW_AT_name("__SA_TEMPLATE_v1")
	.dwattr $C$DW$T$152, DW_AT_byte_size(0x40)
$C$DW$269	.dwtag  DW_TAG_member
	.dwattr $C$DW$269, DW_AT_type(*$C$DW$T$123)
	.dwattr $C$DW$269, DW_AT_name("ICNT0")
	.dwattr $C$DW$269, DW_AT_bit_offset(0x20)
	.dwattr $C$DW$269, DW_AT_bit_size(0x20)
	.dwattr $C$DW$269, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$269, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$269, DW_AT_decl_line(0x1bc)
	.dwattr $C$DW$269, DW_AT_decl_column(0x0e)

$C$DW$270	.dwtag  DW_TAG_member
	.dwattr $C$DW$270, DW_AT_type(*$C$DW$T$123)
	.dwattr $C$DW$270, DW_AT_name("ICNT1")
	.dwattr $C$DW$270, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$270, DW_AT_bit_size(0x20)
	.dwattr $C$DW$270, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$270, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$270, DW_AT_decl_line(0x1bd)
	.dwattr $C$DW$270, DW_AT_decl_column(0x0e)

$C$DW$271	.dwtag  DW_TAG_member
	.dwattr $C$DW$271, DW_AT_type(*$C$DW$T$123)
	.dwattr $C$DW$271, DW_AT_name("ICNT2")
	.dwattr $C$DW$271, DW_AT_bit_offset(0x20)
	.dwattr $C$DW$271, DW_AT_bit_size(0x20)
	.dwattr $C$DW$271, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$271, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$271, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$271, DW_AT_decl_line(0x1be)
	.dwattr $C$DW$271, DW_AT_decl_column(0x0e)

$C$DW$272	.dwtag  DW_TAG_member
	.dwattr $C$DW$272, DW_AT_type(*$C$DW$T$123)
	.dwattr $C$DW$272, DW_AT_name("ICNT3")
	.dwattr $C$DW$272, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$272, DW_AT_bit_size(0x20)
	.dwattr $C$DW$272, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$272, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$272, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$272, DW_AT_decl_line(0x1bf)
	.dwattr $C$DW$272, DW_AT_decl_column(0x0e)

$C$DW$273	.dwtag  DW_TAG_member
	.dwattr $C$DW$273, DW_AT_type(*$C$DW$T$123)
	.dwattr $C$DW$273, DW_AT_name("ICNT4")
	.dwattr $C$DW$273, DW_AT_bit_offset(0x20)
	.dwattr $C$DW$273, DW_AT_bit_size(0x20)
	.dwattr $C$DW$273, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$273, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$273, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$273, DW_AT_decl_line(0x1c0)
	.dwattr $C$DW$273, DW_AT_decl_column(0x0e)

$C$DW$274	.dwtag  DW_TAG_member
	.dwattr $C$DW$274, DW_AT_type(*$C$DW$T$123)
	.dwattr $C$DW$274, DW_AT_name("ICNT5")
	.dwattr $C$DW$274, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$274, DW_AT_bit_size(0x20)
	.dwattr $C$DW$274, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$274, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$274, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$274, DW_AT_decl_line(0x1c1)
	.dwattr $C$DW$274, DW_AT_decl_column(0x0e)

$C$DW$275	.dwtag  DW_TAG_member
	.dwattr $C$DW$275, DW_AT_type(*$C$DW$T$123)
	.dwattr $C$DW$275, DW_AT_name("DECDIM1_WIDTH")
	.dwattr $C$DW$275, DW_AT_bit_offset(0x20)
	.dwattr $C$DW$275, DW_AT_bit_size(0x20)
	.dwattr $C$DW$275, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$275, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$275, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$275, DW_AT_decl_line(0x1c2)
	.dwattr $C$DW$275, DW_AT_decl_column(0x0e)

$C$DW$276	.dwtag  DW_TAG_member
	.dwattr $C$DW$276, DW_AT_type(*$C$DW$T$123)
	.dwattr $C$DW$276, DW_AT_name("DECDIM2_WIDTH")
	.dwattr $C$DW$276, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$276, DW_AT_bit_size(0x20)
	.dwattr $C$DW$276, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$276, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$276, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$276, DW_AT_decl_line(0x1c3)
	.dwattr $C$DW$276, DW_AT_decl_column(0x0e)

$C$DW$277	.dwtag  DW_TAG_member
	.dwattr $C$DW$277, DW_AT_type(*$C$DW$T$125)
	.dwattr $C$DW$277, DW_AT_name("DIM1")
	.dwattr $C$DW$277, DW_AT_bit_offset(0x20)
	.dwattr $C$DW$277, DW_AT_bit_size(0x20)
	.dwattr $C$DW$277, DW_AT_data_member_location[DW_OP_plus_uconst 0x20]
	.dwattr $C$DW$277, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$277, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$277, DW_AT_decl_line(0x1c4)
	.dwattr $C$DW$277, DW_AT_decl_column(0x0e)

$C$DW$278	.dwtag  DW_TAG_member
	.dwattr $C$DW$278, DW_AT_type(*$C$DW$T$125)
	.dwattr $C$DW$278, DW_AT_name("DIM2")
	.dwattr $C$DW$278, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$278, DW_AT_bit_size(0x20)
	.dwattr $C$DW$278, DW_AT_data_member_location[DW_OP_plus_uconst 0x20]
	.dwattr $C$DW$278, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$278, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$278, DW_AT_decl_line(0x1c5)
	.dwattr $C$DW$278, DW_AT_decl_column(0x0e)

$C$DW$279	.dwtag  DW_TAG_member
	.dwattr $C$DW$279, DW_AT_type(*$C$DW$T$125)
	.dwattr $C$DW$279, DW_AT_name("DIM3")
	.dwattr $C$DW$279, DW_AT_bit_offset(0x20)
	.dwattr $C$DW$279, DW_AT_bit_size(0x20)
	.dwattr $C$DW$279, DW_AT_data_member_location[DW_OP_plus_uconst 0x28]
	.dwattr $C$DW$279, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$279, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$279, DW_AT_decl_line(0x1c6)
	.dwattr $C$DW$279, DW_AT_decl_column(0x0e)

$C$DW$280	.dwtag  DW_TAG_member
	.dwattr $C$DW$280, DW_AT_type(*$C$DW$T$125)
	.dwattr $C$DW$280, DW_AT_name("DIM4")
	.dwattr $C$DW$280, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$280, DW_AT_bit_size(0x20)
	.dwattr $C$DW$280, DW_AT_data_member_location[DW_OP_plus_uconst 0x28]
	.dwattr $C$DW$280, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$280, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$280, DW_AT_decl_line(0x1c7)
	.dwattr $C$DW$280, DW_AT_decl_column(0x0e)

$C$DW$281	.dwtag  DW_TAG_member
	.dwattr $C$DW$281, DW_AT_type(*$C$DW$T$125)
	.dwattr $C$DW$281, DW_AT_name("DIM5")
	.dwattr $C$DW$281, DW_AT_bit_offset(0x20)
	.dwattr $C$DW$281, DW_AT_bit_size(0x20)
	.dwattr $C$DW$281, DW_AT_data_member_location[DW_OP_plus_uconst 0x30]
	.dwattr $C$DW$281, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$281, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$281, DW_AT_decl_line(0x1c8)
	.dwattr $C$DW$281, DW_AT_decl_column(0x0e)

$C$DW$282	.dwtag  DW_TAG_member
	.dwattr $C$DW$282, DW_AT_type(*$C$DW$T$123)
	.dwattr $C$DW$282, DW_AT_name("_reserved1")
	.dwattr $C$DW$282, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$282, DW_AT_bit_size(0x20)
	.dwattr $C$DW$282, DW_AT_data_member_location[DW_OP_plus_uconst 0x30]
	.dwattr $C$DW$282, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$282, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$282, DW_AT_decl_line(0x1c9)
	.dwattr $C$DW$282, DW_AT_decl_column(0x0e)

$C$DW$283	.dwtag  DW_TAG_member
	.dwattr $C$DW$283, DW_AT_type(*$C$DW$T$123)
	.dwattr $C$DW$283, DW_AT_name("_reserved2")
	.dwattr $C$DW$283, DW_AT_bit_offset(0x34)
	.dwattr $C$DW$283, DW_AT_bit_size(0x0c)
	.dwattr $C$DW$283, DW_AT_data_member_location[DW_OP_plus_uconst 0x38]
	.dwattr $C$DW$283, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$283, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$283, DW_AT_decl_line(0x1cb)
	.dwattr $C$DW$283, DW_AT_decl_column(0x0e)

$C$DW$284	.dwtag  DW_TAG_member
	.dwattr $C$DW$284, DW_AT_type(*$C$DW$T$138)
	.dwattr $C$DW$284, DW_AT_name("VECLEN")
	.dwattr $C$DW$284, DW_AT_bit_offset(0x01)
	.dwattr $C$DW$284, DW_AT_bit_size(0x03)
	.dwattr $C$DW$284, DW_AT_data_member_location[DW_OP_plus_uconst 0x39]
	.dwattr $C$DW$284, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$284, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$284, DW_AT_decl_line(0x1cc)
	.dwattr $C$DW$284, DW_AT_decl_column(0x11)

$C$DW$285	.dwtag  DW_TAG_member
	.dwattr $C$DW$285, DW_AT_type(*$C$DW$T$123)
	.dwattr $C$DW$285, DW_AT_name("_reserved3")
	.dwattr $C$DW$285, DW_AT_bit_offset(0x2a)
	.dwattr $C$DW$285, DW_AT_bit_size(0x07)
	.dwattr $C$DW$285, DW_AT_data_member_location[DW_OP_plus_uconst 0x38]
	.dwattr $C$DW$285, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$285, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$285, DW_AT_decl_line(0x1cd)
	.dwattr $C$DW$285, DW_AT_decl_column(0x0e)

$C$DW$286	.dwtag  DW_TAG_member
	.dwattr $C$DW$286, DW_AT_type(*$C$DW$T$140)
	.dwattr $C$DW$286, DW_AT_name("INV_DD1")
	.dwattr $C$DW$286, DW_AT_bit_offset(0x01)
	.dwattr $C$DW$286, DW_AT_bit_size(0x01)
	.dwattr $C$DW$286, DW_AT_data_member_location[DW_OP_plus_uconst 0x3a]
	.dwattr $C$DW$286, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$286, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$286, DW_AT_decl_line(0x1ce)
	.dwattr $C$DW$286, DW_AT_decl_column(0x11)

$C$DW$287	.dwtag  DW_TAG_member
	.dwattr $C$DW$287, DW_AT_type(*$C$DW$T$140)
	.dwattr $C$DW$287, DW_AT_name("INV_DD2")
	.dwattr $C$DW$287, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$287, DW_AT_bit_size(0x01)
	.dwattr $C$DW$287, DW_AT_data_member_location[DW_OP_plus_uconst 0x3a]
	.dwattr $C$DW$287, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$287, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$287, DW_AT_decl_line(0x1cf)
	.dwattr $C$DW$287, DW_AT_decl_column(0x11)

$C$DW$288	.dwtag  DW_TAG_member
	.dwattr $C$DW$288, DW_AT_type(*$C$DW$T$142)
	.dwattr $C$DW$288, DW_AT_name("DIMFMT")
	.dwattr $C$DW$288, DW_AT_bit_offset(0x05)
	.dwattr $C$DW$288, DW_AT_bit_size(0x03)
	.dwattr $C$DW$288, DW_AT_data_member_location[DW_OP_plus_uconst 0x3b]
	.dwattr $C$DW$288, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$288, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$288, DW_AT_decl_line(0x1d0)
	.dwattr $C$DW$288, DW_AT_decl_column(0x11)

$C$DW$289	.dwtag  DW_TAG_member
	.dwattr $C$DW$289, DW_AT_type(*$C$DW$T$123)
	.dwattr $C$DW$289, DW_AT_name("_reserved4")
	.dwattr $C$DW$289, DW_AT_bit_offset(0x20)
	.dwattr $C$DW$289, DW_AT_bit_size(0x05)
	.dwattr $C$DW$289, DW_AT_data_member_location[DW_OP_plus_uconst 0x38]
	.dwattr $C$DW$289, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$289, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$289, DW_AT_decl_line(0x1d1)
	.dwattr $C$DW$289, DW_AT_decl_column(0x0e)

$C$DW$290	.dwtag  DW_TAG_member
	.dwattr $C$DW$290, DW_AT_type(*$C$DW$T$123)
	.dwattr $C$DW$290, DW_AT_name("_reserved5")
	.dwattr $C$DW$290, DW_AT_bit_offset(0x10)
	.dwattr $C$DW$290, DW_AT_bit_size(0x10)
	.dwattr $C$DW$290, DW_AT_data_member_location[DW_OP_plus_uconst 0x38]
	.dwattr $C$DW$290, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$290, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$290, DW_AT_decl_line(0x1d2)
	.dwattr $C$DW$290, DW_AT_decl_column(0x0e)

$C$DW$291	.dwtag  DW_TAG_member
	.dwattr $C$DW$291, DW_AT_type(*$C$DW$T$144)
	.dwattr $C$DW$291, DW_AT_name("DECDIM1")
	.dwattr $C$DW$291, DW_AT_bit_offset(0x05)
	.dwattr $C$DW$291, DW_AT_bit_size(0x03)
	.dwattr $C$DW$291, DW_AT_data_member_location[DW_OP_plus_uconst 0x3e]
	.dwattr $C$DW$291, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$291, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$291, DW_AT_decl_line(0x1d3)
	.dwattr $C$DW$291, DW_AT_decl_column(0x11)

$C$DW$292	.dwtag  DW_TAG_member
	.dwattr $C$DW$292, DW_AT_type(*$C$DW$T$146)
	.dwattr $C$DW$292, DW_AT_name("DECDIM1SD")
	.dwattr $C$DW$292, DW_AT_bit_offset(0x03)
	.dwattr $C$DW$292, DW_AT_bit_size(0x02)
	.dwattr $C$DW$292, DW_AT_data_member_location[DW_OP_plus_uconst 0x3e]
	.dwattr $C$DW$292, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$292, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$292, DW_AT_decl_line(0x1d4)
	.dwattr $C$DW$292, DW_AT_decl_column(0x13)

$C$DW$293	.dwtag  DW_TAG_member
	.dwattr $C$DW$293, DW_AT_type(*$C$DW$T$144)
	.dwattr $C$DW$293, DW_AT_name("DECDIM2")
	.dwattr $C$DW$293, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$293, DW_AT_bit_size(0x03)
	.dwattr $C$DW$293, DW_AT_data_member_location[DW_OP_plus_uconst 0x3e]
	.dwattr $C$DW$293, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$293, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$293, DW_AT_decl_line(0x1d5)
	.dwattr $C$DW$293, DW_AT_decl_column(0x11)

$C$DW$294	.dwtag  DW_TAG_member
	.dwattr $C$DW$294, DW_AT_type(*$C$DW$T$146)
	.dwattr $C$DW$294, DW_AT_name("DECDIM2SD")
	.dwattr $C$DW$294, DW_AT_bit_offset(0x06)
	.dwattr $C$DW$294, DW_AT_bit_size(0x02)
	.dwattr $C$DW$294, DW_AT_data_member_location[DW_OP_plus_uconst 0x3f]
	.dwattr $C$DW$294, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$294, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$294, DW_AT_decl_line(0x1d6)
	.dwattr $C$DW$294, DW_AT_decl_column(0x13)

$C$DW$295	.dwtag  DW_TAG_member
	.dwattr $C$DW$295, DW_AT_type(*$C$DW$T$123)
	.dwattr $C$DW$295, DW_AT_name("_reserved")
	.dwattr $C$DW$295, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$295, DW_AT_bit_size(0x06)
	.dwattr $C$DW$295, DW_AT_data_member_location[DW_OP_plus_uconst 0x38]
	.dwattr $C$DW$295, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$295, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$295, DW_AT_decl_line(0x1d7)
	.dwattr $C$DW$295, DW_AT_decl_column(0x0e)


$C$DW$296	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$296, DW_AT_name("operator =")
	.dwattr $C$DW$296, DW_AT_declaration
	.dwattr $C$DW$296, DW_AT_linkage_name("_ZN16__SA_TEMPLATE_v1aSERKS_")
	.dwattr $C$DW$296, DW_AT_type(*$C$DW$T$147)
	.dwattr $C$DW$296, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$297	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$297, DW_AT_type(*$C$DW$T$149)

	.dwendtag $C$DW$296


$C$DW$298	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$298, DW_AT_name("operator =")
	.dwattr $C$DW$298, DW_AT_declaration
	.dwattr $C$DW$298, DW_AT_linkage_name("_ZN16__SA_TEMPLATE_v1aSEOS_")
	.dwattr $C$DW$298, DW_AT_type(*$C$DW$T$147)
	.dwattr $C$DW$298, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$299	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$299, DW_AT_type(*$C$DW$T$147)

	.dwendtag $C$DW$298

	.dwattr $C$DW$T$152, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$T$152, DW_AT_decl_line(0x1b9)
	.dwattr $C$DW$T$152, DW_AT_decl_column(0x2c)
	.dwendtag $C$DW$T$152

	.dwendtag $C$DW$TU$152


$C$DW$TU$148	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$148
$C$DW$T$148	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$148, DW_AT_type(*$C$DW$T$152)

	.dwendtag $C$DW$TU$148


$C$DW$TU$149	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$149
$C$DW$T$149	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$149, DW_AT_type(*$C$DW$T$148)
	.dwattr $C$DW$T$149, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$149


$C$DW$TU$265	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$265
$C$DW$T$265	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$265, DW_AT_name("__SA_TEMPLATE_v1")
	.dwattr $C$DW$T$265, DW_AT_type(*$C$DW$T$152)
	.dwattr $C$DW$T$265, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$T$265, DW_AT_decl_line(0x1fe)
	.dwattr $C$DW$T$265, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$265


$C$DW$TU$266	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$266
$C$DW$T$266	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$266, DW_AT_type(*$C$DW$T$265)

	.dwendtag $C$DW$TU$266


$C$DW$TU$147	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$147
$C$DW$T$147	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$147, DW_AT_type(*$C$DW$T$152)
	.dwattr $C$DW$T$147, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$147


$C$DW$TU$150	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$150

$C$DW$T$150	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$150, DW_AT_type(*$C$DW$T$147)
$C$DW$300	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$300, DW_AT_type(*$C$DW$T$149)

	.dwendtag $C$DW$T$150

	.dwendtag $C$DW$TU$150


$C$DW$TU$151	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$151

$C$DW$T$151	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$151, DW_AT_type(*$C$DW$T$147)
$C$DW$301	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$301, DW_AT_type(*$C$DW$T$147)

	.dwendtag $C$DW$T$151

	.dwendtag $C$DW$TU$151


$C$DW$TU$126	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$126

$C$DW$T$126	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$126, DW_AT_name("__SA_VECLEN")
	.dwattr $C$DW$T$126, DW_AT_byte_size(0x01)
$C$DW$302	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$302, DW_AT_name("__SA_VECLEN_1ELEM")
	.dwattr $C$DW$302, DW_AT_const_value(0x00)
	.dwattr $C$DW$302, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$302, DW_AT_decl_line(0x17e)
	.dwattr $C$DW$302, DW_AT_decl_column(0x05)

$C$DW$303	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$303, DW_AT_name("__SA_VECLEN_2ELEMS")
	.dwattr $C$DW$303, DW_AT_const_value(0x01)
	.dwattr $C$DW$303, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$303, DW_AT_decl_line(0x17f)
	.dwattr $C$DW$303, DW_AT_decl_column(0x05)

$C$DW$304	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$304, DW_AT_name("__SA_VECLEN_4ELEMS")
	.dwattr $C$DW$304, DW_AT_const_value(0x02)
	.dwattr $C$DW$304, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$304, DW_AT_decl_line(0x180)
	.dwattr $C$DW$304, DW_AT_decl_column(0x05)

$C$DW$305	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$305, DW_AT_name("__SA_VECLEN_8ELEMS")
	.dwattr $C$DW$305, DW_AT_const_value(0x03)
	.dwattr $C$DW$305, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$305, DW_AT_decl_line(0x181)
	.dwattr $C$DW$305, DW_AT_decl_column(0x05)

$C$DW$306	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$306, DW_AT_name("__SA_VECLEN_16ELEMS")
	.dwattr $C$DW$306, DW_AT_const_value(0x04)
	.dwattr $C$DW$306, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$306, DW_AT_decl_line(0x182)
	.dwattr $C$DW$306, DW_AT_decl_column(0x05)

$C$DW$307	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$307, DW_AT_name("__SA_VECLEN_32ELEMS")
	.dwattr $C$DW$307, DW_AT_const_value(0x05)
	.dwattr $C$DW$307, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$307, DW_AT_decl_line(0x183)
	.dwattr $C$DW$307, DW_AT_decl_column(0x05)

$C$DW$308	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$308, DW_AT_name("__SA_VECLEN_64ELEMS")
	.dwattr $C$DW$308, DW_AT_const_value(0x06)
	.dwattr $C$DW$308, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$308, DW_AT_decl_line(0x184)
	.dwattr $C$DW$308, DW_AT_decl_column(0x05)

	.dwattr $C$DW$T$126, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$T$126, DW_AT_decl_line(0x17c)
	.dwattr $C$DW$T$126, DW_AT_decl_column(0x2a)
	.dwendtag $C$DW$T$126

	.dwendtag $C$DW$TU$126


$C$DW$TU$127	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$127
$C$DW$T$127	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$127, DW_AT_name("__SA_VECLEN")
	.dwattr $C$DW$T$127, DW_AT_type(*$C$DW$T$126)
	.dwattr $C$DW$T$127, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$T$127, DW_AT_decl_line(0x185)
	.dwattr $C$DW$T$127, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$127


$C$DW$TU$137	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$137

$C$DW$T$137	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$137, DW_AT_name("__SA_VECLEN")
	.dwattr $C$DW$T$137, DW_AT_byte_size(0x01)
$C$DW$309	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$309, DW_AT_name("__SA_VECLEN_1ELEM")
	.dwattr $C$DW$309, DW_AT_const_value(0x00)
	.dwattr $C$DW$309, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$309, DW_AT_decl_line(0x17e)
	.dwattr $C$DW$309, DW_AT_decl_column(0x05)

$C$DW$310	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$310, DW_AT_name("__SA_VECLEN_2ELEMS")
	.dwattr $C$DW$310, DW_AT_const_value(0x01)
	.dwattr $C$DW$310, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$310, DW_AT_decl_line(0x17f)
	.dwattr $C$DW$310, DW_AT_decl_column(0x05)

$C$DW$311	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$311, DW_AT_name("__SA_VECLEN_4ELEMS")
	.dwattr $C$DW$311, DW_AT_const_value(0x02)
	.dwattr $C$DW$311, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$311, DW_AT_decl_line(0x180)
	.dwattr $C$DW$311, DW_AT_decl_column(0x05)

$C$DW$312	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$312, DW_AT_name("__SA_VECLEN_8ELEMS")
	.dwattr $C$DW$312, DW_AT_const_value(0x03)
	.dwattr $C$DW$312, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$312, DW_AT_decl_line(0x181)
	.dwattr $C$DW$312, DW_AT_decl_column(0x05)

$C$DW$313	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$313, DW_AT_name("__SA_VECLEN_16ELEMS")
	.dwattr $C$DW$313, DW_AT_const_value(0x04)
	.dwattr $C$DW$313, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$313, DW_AT_decl_line(0x182)
	.dwattr $C$DW$313, DW_AT_decl_column(0x05)

$C$DW$314	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$314, DW_AT_name("__SA_VECLEN_32ELEMS")
	.dwattr $C$DW$314, DW_AT_const_value(0x05)
	.dwattr $C$DW$314, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$314, DW_AT_decl_line(0x183)
	.dwattr $C$DW$314, DW_AT_decl_column(0x05)

$C$DW$315	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$315, DW_AT_name("__SA_VECLEN_64ELEMS")
	.dwattr $C$DW$315, DW_AT_const_value(0x06)
	.dwattr $C$DW$315, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$315, DW_AT_decl_line(0x184)
	.dwattr $C$DW$315, DW_AT_decl_column(0x05)

	.dwattr $C$DW$T$137, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$T$137, DW_AT_decl_line(0x17c)
	.dwattr $C$DW$T$137, DW_AT_decl_column(0x2a)
	.dwendtag $C$DW$T$137

	.dwendtag $C$DW$TU$137


$C$DW$TU$138	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$138
$C$DW$T$138	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$138, DW_AT_name("__SA_VECLEN")
	.dwattr $C$DW$T$138, DW_AT_type(*$C$DW$T$137)
	.dwattr $C$DW$T$138, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$T$138, DW_AT_decl_line(0x185)
	.dwattr $C$DW$T$138, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$138


$C$DW$TU$171	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$171

$C$DW$T$171	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$171, DW_AT_name("__SE_AM")
	.dwattr $C$DW$T$171, DW_AT_byte_size(0x01)
$C$DW$316	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$316, DW_AT_name("__SE_AM_LINEAR")
	.dwattr $C$DW$316, DW_AT_const_value(0x00)
	.dwattr $C$DW$316, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$316, DW_AT_decl_line(0xcb)
	.dwattr $C$DW$316, DW_AT_decl_column(0x05)

$C$DW$317	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$317, DW_AT_name("__SE_AM_CIRC_CBK0")
	.dwattr $C$DW$317, DW_AT_const_value(0x01)
	.dwattr $C$DW$317, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$317, DW_AT_decl_line(0xcc)
	.dwattr $C$DW$317, DW_AT_decl_column(0x05)

$C$DW$318	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$318, DW_AT_name("__SE_AM_CIRC_CBK1")
	.dwattr $C$DW$318, DW_AT_const_value(0x02)
	.dwattr $C$DW$318, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$318, DW_AT_decl_line(0xcd)
	.dwattr $C$DW$318, DW_AT_decl_column(0x05)

	.dwattr $C$DW$T$171, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$T$171, DW_AT_decl_line(0xc9)
	.dwattr $C$DW$T$171, DW_AT_decl_column(0x2a)
	.dwendtag $C$DW$T$171

	.dwendtag $C$DW$TU$171


$C$DW$TU$172	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$172
$C$DW$T$172	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$172, DW_AT_name("__SE_AM")
	.dwattr $C$DW$T$172, DW_AT_type(*$C$DW$T$171)
	.dwattr $C$DW$T$172, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$T$172, DW_AT_decl_line(0xce)
	.dwattr $C$DW$T$172, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$172


$C$DW$TU$200	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$200

$C$DW$T$200	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$200, DW_AT_name("__SE_AM")
	.dwattr $C$DW$T$200, DW_AT_byte_size(0x01)
$C$DW$319	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$319, DW_AT_name("__SE_AM_LINEAR")
	.dwattr $C$DW$319, DW_AT_const_value(0x00)
	.dwattr $C$DW$319, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$319, DW_AT_decl_line(0xcb)
	.dwattr $C$DW$319, DW_AT_decl_column(0x05)

$C$DW$320	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$320, DW_AT_name("__SE_AM_CIRC_CBK0")
	.dwattr $C$DW$320, DW_AT_const_value(0x01)
	.dwattr $C$DW$320, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$320, DW_AT_decl_line(0xcc)
	.dwattr $C$DW$320, DW_AT_decl_column(0x05)

$C$DW$321	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$321, DW_AT_name("__SE_AM_CIRC_CBK1")
	.dwattr $C$DW$321, DW_AT_const_value(0x02)
	.dwattr $C$DW$321, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$321, DW_AT_decl_line(0xcd)
	.dwattr $C$DW$321, DW_AT_decl_column(0x05)

	.dwattr $C$DW$T$200, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$T$200, DW_AT_decl_line(0xc9)
	.dwattr $C$DW$T$200, DW_AT_decl_column(0x2a)
	.dwendtag $C$DW$T$200

	.dwendtag $C$DW$TU$200


$C$DW$TU$201	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$201
$C$DW$T$201	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$201, DW_AT_name("__SE_AM")
	.dwattr $C$DW$T$201, DW_AT_type(*$C$DW$T$200)
	.dwattr $C$DW$T$201, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$T$201, DW_AT_decl_line(0xce)
	.dwattr $C$DW$T$201, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$201


$C$DW$TU$173	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$173

$C$DW$T$173	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$173, DW_AT_name("__SE_DECDIM")
	.dwattr $C$DW$T$173, DW_AT_byte_size(0x01)
$C$DW$322	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$322, DW_AT_name("__SE_DECDIM_DIM0")
	.dwattr $C$DW$322, DW_AT_const_value(0x00)
	.dwattr $C$DW$322, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$322, DW_AT_decl_line(0xdb)
	.dwattr $C$DW$322, DW_AT_decl_column(0x05)

$C$DW$323	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$323, DW_AT_name("__SE_DECDIM_DIM1")
	.dwattr $C$DW$323, DW_AT_const_value(0x01)
	.dwattr $C$DW$323, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$323, DW_AT_decl_line(0xdc)
	.dwattr $C$DW$323, DW_AT_decl_column(0x05)

$C$DW$324	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$324, DW_AT_name("__SE_DECDIM_DIM2")
	.dwattr $C$DW$324, DW_AT_const_value(0x02)
	.dwattr $C$DW$324, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$324, DW_AT_decl_line(0xdd)
	.dwattr $C$DW$324, DW_AT_decl_column(0x05)

$C$DW$325	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$325, DW_AT_name("__SE_DECDIM_DIM3")
	.dwattr $C$DW$325, DW_AT_const_value(0x03)
	.dwattr $C$DW$325, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$325, DW_AT_decl_line(0xde)
	.dwattr $C$DW$325, DW_AT_decl_column(0x05)

$C$DW$326	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$326, DW_AT_name("__SE_DECDIM_DIM4")
	.dwattr $C$DW$326, DW_AT_const_value(0x04)
	.dwattr $C$DW$326, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$326, DW_AT_decl_line(0xdf)
	.dwattr $C$DW$326, DW_AT_decl_column(0x05)

$C$DW$327	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$327, DW_AT_name("__SE_DECDIM_DIM5")
	.dwattr $C$DW$327, DW_AT_const_value(0x05)
	.dwattr $C$DW$327, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$327, DW_AT_decl_line(0xe0)
	.dwattr $C$DW$327, DW_AT_decl_column(0x05)

	.dwattr $C$DW$T$173, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$T$173, DW_AT_decl_line(0xd9)
	.dwattr $C$DW$T$173, DW_AT_decl_column(0x2a)
	.dwendtag $C$DW$T$173

	.dwendtag $C$DW$TU$173


$C$DW$TU$174	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$174
$C$DW$T$174	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$174, DW_AT_name("__SE_DECDIM")
	.dwattr $C$DW$T$174, DW_AT_type(*$C$DW$T$173)
	.dwattr $C$DW$T$174, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$T$174, DW_AT_decl_line(0xe1)
	.dwattr $C$DW$T$174, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$174


$C$DW$TU$202	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$202

$C$DW$T$202	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$202, DW_AT_name("__SE_DECDIM")
	.dwattr $C$DW$T$202, DW_AT_byte_size(0x01)
$C$DW$328	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$328, DW_AT_name("__SE_DECDIM_DIM0")
	.dwattr $C$DW$328, DW_AT_const_value(0x00)
	.dwattr $C$DW$328, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$328, DW_AT_decl_line(0xdb)
	.dwattr $C$DW$328, DW_AT_decl_column(0x05)

$C$DW$329	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$329, DW_AT_name("__SE_DECDIM_DIM1")
	.dwattr $C$DW$329, DW_AT_const_value(0x01)
	.dwattr $C$DW$329, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$329, DW_AT_decl_line(0xdc)
	.dwattr $C$DW$329, DW_AT_decl_column(0x05)

$C$DW$330	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$330, DW_AT_name("__SE_DECDIM_DIM2")
	.dwattr $C$DW$330, DW_AT_const_value(0x02)
	.dwattr $C$DW$330, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$330, DW_AT_decl_line(0xdd)
	.dwattr $C$DW$330, DW_AT_decl_column(0x05)

$C$DW$331	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$331, DW_AT_name("__SE_DECDIM_DIM3")
	.dwattr $C$DW$331, DW_AT_const_value(0x03)
	.dwattr $C$DW$331, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$331, DW_AT_decl_line(0xde)
	.dwattr $C$DW$331, DW_AT_decl_column(0x05)

$C$DW$332	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$332, DW_AT_name("__SE_DECDIM_DIM4")
	.dwattr $C$DW$332, DW_AT_const_value(0x04)
	.dwattr $C$DW$332, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$332, DW_AT_decl_line(0xdf)
	.dwattr $C$DW$332, DW_AT_decl_column(0x05)

$C$DW$333	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$333, DW_AT_name("__SE_DECDIM_DIM5")
	.dwattr $C$DW$333, DW_AT_const_value(0x05)
	.dwattr $C$DW$333, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$333, DW_AT_decl_line(0xe0)
	.dwattr $C$DW$333, DW_AT_decl_column(0x05)

	.dwattr $C$DW$T$202, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$T$202, DW_AT_decl_line(0xd9)
	.dwattr $C$DW$T$202, DW_AT_decl_column(0x2a)
	.dwendtag $C$DW$T$202

	.dwendtag $C$DW$TU$202


$C$DW$TU$203	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$203
$C$DW$T$203	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$203, DW_AT_name("__SE_DECDIM")
	.dwattr $C$DW$T$203, DW_AT_type(*$C$DW$T$202)
	.dwattr $C$DW$T$203, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$T$203, DW_AT_decl_line(0xe1)
	.dwattr $C$DW$T$203, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$203


$C$DW$TU$175	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$175

$C$DW$T$175	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$175, DW_AT_name("__SE_DECDIMSD")
	.dwattr $C$DW$T$175, DW_AT_byte_size(0x01)
$C$DW$334	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$334, DW_AT_name("__SE_DECDIMSD_DIM0")
	.dwattr $C$DW$334, DW_AT_const_value(0x00)
	.dwattr $C$DW$334, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$334, DW_AT_decl_line(0xe8)
	.dwattr $C$DW$334, DW_AT_decl_column(0x05)

$C$DW$335	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$335, DW_AT_name("__SE_DECDIMSD_DIM1")
	.dwattr $C$DW$335, DW_AT_const_value(0x01)
	.dwattr $C$DW$335, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$335, DW_AT_decl_line(0xe9)
	.dwattr $C$DW$335, DW_AT_decl_column(0x05)

$C$DW$336	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$336, DW_AT_name("__SE_DECDIMSD_DIM2")
	.dwattr $C$DW$336, DW_AT_const_value(0x02)
	.dwattr $C$DW$336, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$336, DW_AT_decl_line(0xea)
	.dwattr $C$DW$336, DW_AT_decl_column(0x05)

$C$DW$337	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$337, DW_AT_name("__SE_DECDIMSD_DIM3")
	.dwattr $C$DW$337, DW_AT_const_value(0x03)
	.dwattr $C$DW$337, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$337, DW_AT_decl_line(0xeb)
	.dwattr $C$DW$337, DW_AT_decl_column(0x05)

	.dwattr $C$DW$T$175, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$T$175, DW_AT_decl_line(0xe6)
	.dwattr $C$DW$T$175, DW_AT_decl_column(0x2a)
	.dwendtag $C$DW$T$175

	.dwendtag $C$DW$TU$175


$C$DW$TU$176	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$176
$C$DW$T$176	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$176, DW_AT_name("__SE_DECDIMSD")
	.dwattr $C$DW$T$176, DW_AT_type(*$C$DW$T$175)
	.dwattr $C$DW$T$176, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$T$176, DW_AT_decl_line(0xec)
	.dwattr $C$DW$T$176, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$176


$C$DW$TU$204	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$204

$C$DW$T$204	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$204, DW_AT_name("__SE_DECDIMSD")
	.dwattr $C$DW$T$204, DW_AT_byte_size(0x01)
$C$DW$338	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$338, DW_AT_name("__SE_DECDIMSD_DIM0")
	.dwattr $C$DW$338, DW_AT_const_value(0x00)
	.dwattr $C$DW$338, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$338, DW_AT_decl_line(0xe8)
	.dwattr $C$DW$338, DW_AT_decl_column(0x05)

$C$DW$339	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$339, DW_AT_name("__SE_DECDIMSD_DIM1")
	.dwattr $C$DW$339, DW_AT_const_value(0x01)
	.dwattr $C$DW$339, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$339, DW_AT_decl_line(0xe9)
	.dwattr $C$DW$339, DW_AT_decl_column(0x05)

$C$DW$340	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$340, DW_AT_name("__SE_DECDIMSD_DIM2")
	.dwattr $C$DW$340, DW_AT_const_value(0x02)
	.dwattr $C$DW$340, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$340, DW_AT_decl_line(0xea)
	.dwattr $C$DW$340, DW_AT_decl_column(0x05)

$C$DW$341	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$341, DW_AT_name("__SE_DECDIMSD_DIM3")
	.dwattr $C$DW$341, DW_AT_const_value(0x03)
	.dwattr $C$DW$341, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$341, DW_AT_decl_line(0xeb)
	.dwattr $C$DW$341, DW_AT_decl_column(0x05)

	.dwattr $C$DW$T$204, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$T$204, DW_AT_decl_line(0xe6)
	.dwattr $C$DW$T$204, DW_AT_decl_column(0x2a)
	.dwendtag $C$DW$T$204

	.dwendtag $C$DW$TU$204


$C$DW$TU$205	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$205
$C$DW$T$205	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$205, DW_AT_name("__SE_DECDIMSD")
	.dwattr $C$DW$T$205, DW_AT_type(*$C$DW$T$204)
	.dwattr $C$DW$T$205, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$T$205, DW_AT_decl_line(0xec)
	.dwattr $C$DW$T$205, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$205


$C$DW$TU$165	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$165

$C$DW$T$165	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$165, DW_AT_name("__SE_DECIM")
	.dwattr $C$DW$T$165, DW_AT_byte_size(0x01)
$C$DW$342	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$342, DW_AT_name("__SE_DECIM_OFF")
	.dwattr $C$DW$342, DW_AT_const_value(0x00)
	.dwattr $C$DW$342, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$342, DW_AT_decl_line(0xa7)
	.dwattr $C$DW$342, DW_AT_decl_column(0x05)

$C$DW$343	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$343, DW_AT_name("__SE_DECIM_2")
	.dwattr $C$DW$343, DW_AT_const_value(0x01)
	.dwattr $C$DW$343, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$343, DW_AT_decl_line(0xa8)
	.dwattr $C$DW$343, DW_AT_decl_column(0x05)

$C$DW$344	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$344, DW_AT_name("__SE_DECIM_4")
	.dwattr $C$DW$344, DW_AT_const_value(0x02)
	.dwattr $C$DW$344, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$344, DW_AT_decl_line(0xa9)
	.dwattr $C$DW$344, DW_AT_decl_column(0x05)

	.dwattr $C$DW$T$165, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$T$165, DW_AT_decl_line(0xa5)
	.dwattr $C$DW$T$165, DW_AT_decl_column(0x2a)
	.dwendtag $C$DW$T$165

	.dwendtag $C$DW$TU$165


$C$DW$TU$166	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$166
$C$DW$T$166	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$166, DW_AT_name("__SE_DECIM")
	.dwattr $C$DW$T$166, DW_AT_type(*$C$DW$T$165)
	.dwattr $C$DW$T$166, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$T$166, DW_AT_decl_line(0xaa)
	.dwattr $C$DW$T$166, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$166


$C$DW$TU$194	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$194

$C$DW$T$194	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$194, DW_AT_name("__SE_DECIM")
	.dwattr $C$DW$T$194, DW_AT_byte_size(0x01)
$C$DW$345	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$345, DW_AT_name("__SE_DECIM_OFF")
	.dwattr $C$DW$345, DW_AT_const_value(0x00)
	.dwattr $C$DW$345, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$345, DW_AT_decl_line(0xa7)
	.dwattr $C$DW$345, DW_AT_decl_column(0x05)

$C$DW$346	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$346, DW_AT_name("__SE_DECIM_2")
	.dwattr $C$DW$346, DW_AT_const_value(0x01)
	.dwattr $C$DW$346, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$346, DW_AT_decl_line(0xa8)
	.dwattr $C$DW$346, DW_AT_decl_column(0x05)

$C$DW$347	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$347, DW_AT_name("__SE_DECIM_4")
	.dwattr $C$DW$347, DW_AT_const_value(0x02)
	.dwattr $C$DW$347, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$347, DW_AT_decl_line(0xa9)
	.dwattr $C$DW$347, DW_AT_decl_column(0x05)

	.dwattr $C$DW$T$194, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$T$194, DW_AT_decl_line(0xa5)
	.dwattr $C$DW$T$194, DW_AT_decl_column(0x2a)
	.dwendtag $C$DW$T$194

	.dwendtag $C$DW$TU$194


$C$DW$TU$195	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$195
$C$DW$T$195	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$195, DW_AT_name("__SE_DECIM")
	.dwattr $C$DW$T$195, DW_AT_type(*$C$DW$T$194)
	.dwattr $C$DW$T$195, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$T$195, DW_AT_decl_line(0xaa)
	.dwattr $C$DW$T$195, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$195


$C$DW$TU$167	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$167

$C$DW$T$167	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$167, DW_AT_name("__SE_DIMFMT")
	.dwattr $C$DW$T$167, DW_AT_byte_size(0x01)
$C$DW$348	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$348, DW_AT_name("__SE_DIMFMT_1D")
	.dwattr $C$DW$348, DW_AT_const_value(0x00)
	.dwattr $C$DW$348, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$348, DW_AT_decl_line(0xb1)
	.dwattr $C$DW$348, DW_AT_decl_column(0x05)

$C$DW$349	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$349, DW_AT_name("__SE_DIMFMT_2D")
	.dwattr $C$DW$349, DW_AT_const_value(0x01)
	.dwattr $C$DW$349, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$349, DW_AT_decl_line(0xb2)
	.dwattr $C$DW$349, DW_AT_decl_column(0x05)

$C$DW$350	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$350, DW_AT_name("__SE_DIMFMT_3D")
	.dwattr $C$DW$350, DW_AT_const_value(0x02)
	.dwattr $C$DW$350, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$350, DW_AT_decl_line(0xb3)
	.dwattr $C$DW$350, DW_AT_decl_column(0x05)

$C$DW$351	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$351, DW_AT_name("__SE_DIMFMT_4D")
	.dwattr $C$DW$351, DW_AT_const_value(0x03)
	.dwattr $C$DW$351, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$351, DW_AT_decl_line(0xb4)
	.dwattr $C$DW$351, DW_AT_decl_column(0x05)

$C$DW$352	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$352, DW_AT_name("__SE_DIMFMT_5D")
	.dwattr $C$DW$352, DW_AT_const_value(0x04)
	.dwattr $C$DW$352, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$352, DW_AT_decl_line(0xb5)
	.dwattr $C$DW$352, DW_AT_decl_column(0x05)

$C$DW$353	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$353, DW_AT_name("__SE_DIMFMT_6D")
	.dwattr $C$DW$353, DW_AT_const_value(0x05)
	.dwattr $C$DW$353, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$353, DW_AT_decl_line(0xb6)
	.dwattr $C$DW$353, DW_AT_decl_column(0x05)

	.dwattr $C$DW$T$167, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$T$167, DW_AT_decl_line(0xaf)
	.dwattr $C$DW$T$167, DW_AT_decl_column(0x2a)
	.dwendtag $C$DW$T$167

	.dwendtag $C$DW$TU$167


$C$DW$TU$168	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$168
$C$DW$T$168	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$168, DW_AT_name("__SE_DIMFMT")
	.dwattr $C$DW$T$168, DW_AT_type(*$C$DW$T$167)
	.dwattr $C$DW$T$168, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$T$168, DW_AT_decl_line(0xb7)
	.dwattr $C$DW$T$168, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$168


$C$DW$TU$196	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$196

$C$DW$T$196	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$196, DW_AT_name("__SE_DIMFMT")
	.dwattr $C$DW$T$196, DW_AT_byte_size(0x01)
$C$DW$354	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$354, DW_AT_name("__SE_DIMFMT_1D")
	.dwattr $C$DW$354, DW_AT_const_value(0x00)
	.dwattr $C$DW$354, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$354, DW_AT_decl_line(0xb1)
	.dwattr $C$DW$354, DW_AT_decl_column(0x05)

$C$DW$355	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$355, DW_AT_name("__SE_DIMFMT_2D")
	.dwattr $C$DW$355, DW_AT_const_value(0x01)
	.dwattr $C$DW$355, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$355, DW_AT_decl_line(0xb2)
	.dwattr $C$DW$355, DW_AT_decl_column(0x05)

$C$DW$356	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$356, DW_AT_name("__SE_DIMFMT_3D")
	.dwattr $C$DW$356, DW_AT_const_value(0x02)
	.dwattr $C$DW$356, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$356, DW_AT_decl_line(0xb3)
	.dwattr $C$DW$356, DW_AT_decl_column(0x05)

$C$DW$357	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$357, DW_AT_name("__SE_DIMFMT_4D")
	.dwattr $C$DW$357, DW_AT_const_value(0x03)
	.dwattr $C$DW$357, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$357, DW_AT_decl_line(0xb4)
	.dwattr $C$DW$357, DW_AT_decl_column(0x05)

$C$DW$358	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$358, DW_AT_name("__SE_DIMFMT_5D")
	.dwattr $C$DW$358, DW_AT_const_value(0x04)
	.dwattr $C$DW$358, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$358, DW_AT_decl_line(0xb5)
	.dwattr $C$DW$358, DW_AT_decl_column(0x05)

$C$DW$359	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$359, DW_AT_name("__SE_DIMFMT_6D")
	.dwattr $C$DW$359, DW_AT_const_value(0x05)
	.dwattr $C$DW$359, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$359, DW_AT_decl_line(0xb6)
	.dwattr $C$DW$359, DW_AT_decl_column(0x05)

	.dwattr $C$DW$T$196, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$T$196, DW_AT_decl_line(0xaf)
	.dwattr $C$DW$T$196, DW_AT_decl_column(0x2a)
	.dwendtag $C$DW$T$196

	.dwendtag $C$DW$TU$196


$C$DW$TU$197	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$197
$C$DW$T$197	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$197, DW_AT_name("__SE_DIMFMT")
	.dwattr $C$DW$T$197, DW_AT_type(*$C$DW$T$196)
	.dwattr $C$DW$T$197, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$T$197, DW_AT_decl_line(0xb7)
	.dwattr $C$DW$T$197, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$197


$C$DW$TU$169	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$169

$C$DW$T$169	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$169, DW_AT_name("__SE_DIR")
	.dwattr $C$DW$T$169, DW_AT_byte_size(0x01)
$C$DW$360	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$360, DW_AT_name("__SE_DIR_INC")
	.dwattr $C$DW$360, DW_AT_const_value(0x00)
	.dwattr $C$DW$360, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$360, DW_AT_decl_line(0xc1)
	.dwattr $C$DW$360, DW_AT_decl_column(0x05)

$C$DW$361	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$361, DW_AT_name("__SE_DIR_DEC")
	.dwattr $C$DW$361, DW_AT_const_value(0x01)
	.dwattr $C$DW$361, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$361, DW_AT_decl_line(0xc2)
	.dwattr $C$DW$361, DW_AT_decl_column(0x05)

	.dwattr $C$DW$T$169, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$T$169, DW_AT_decl_line(0xbf)
	.dwattr $C$DW$T$169, DW_AT_decl_column(0x2a)
	.dwendtag $C$DW$T$169

	.dwendtag $C$DW$TU$169


$C$DW$TU$170	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$170
$C$DW$T$170	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$170, DW_AT_name("__SE_DIR")
	.dwattr $C$DW$T$170, DW_AT_type(*$C$DW$T$169)
	.dwattr $C$DW$T$170, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$T$170, DW_AT_decl_line(0xc3)
	.dwattr $C$DW$T$170, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$170


$C$DW$TU$198	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$198

$C$DW$T$198	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$198, DW_AT_name("__SE_DIR")
	.dwattr $C$DW$T$198, DW_AT_byte_size(0x01)
$C$DW$362	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$362, DW_AT_name("__SE_DIR_INC")
	.dwattr $C$DW$362, DW_AT_const_value(0x00)
	.dwattr $C$DW$362, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$362, DW_AT_decl_line(0xc1)
	.dwattr $C$DW$362, DW_AT_decl_column(0x05)

$C$DW$363	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$363, DW_AT_name("__SE_DIR_DEC")
	.dwattr $C$DW$363, DW_AT_const_value(0x01)
	.dwattr $C$DW$363, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$363, DW_AT_decl_line(0xc2)
	.dwattr $C$DW$363, DW_AT_decl_column(0x05)

	.dwattr $C$DW$T$198, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$T$198, DW_AT_decl_line(0xbf)
	.dwattr $C$DW$T$198, DW_AT_decl_column(0x2a)
	.dwendtag $C$DW$T$198

	.dwendtag $C$DW$TU$198


$C$DW$TU$199	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$199
$C$DW$T$199	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$199, DW_AT_name("__SE_DIR")
	.dwattr $C$DW$T$199, DW_AT_type(*$C$DW$T$198)
	.dwattr $C$DW$T$199, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$T$199, DW_AT_decl_line(0xc3)
	.dwattr $C$DW$T$199, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$199


$C$DW$TU$161	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$161

$C$DW$T$161	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$161, DW_AT_name("__SE_ELEDUP")
	.dwattr $C$DW$T$161, DW_AT_byte_size(0x01)
$C$DW$364	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$364, DW_AT_name("__SE_ELEDUP_OFF")
	.dwattr $C$DW$364, DW_AT_const_value(0x00)
	.dwattr $C$DW$364, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$364, DW_AT_decl_line(0x88)
	.dwattr $C$DW$364, DW_AT_decl_column(0x05)

$C$DW$365	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$365, DW_AT_name("__SE_ELEDUP_2X")
	.dwattr $C$DW$365, DW_AT_const_value(0x01)
	.dwattr $C$DW$365, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$365, DW_AT_decl_line(0x89)
	.dwattr $C$DW$365, DW_AT_decl_column(0x05)

$C$DW$366	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$366, DW_AT_name("__SE_ELEDUP_4X")
	.dwattr $C$DW$366, DW_AT_const_value(0x02)
	.dwattr $C$DW$366, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$366, DW_AT_decl_line(0x8a)
	.dwattr $C$DW$366, DW_AT_decl_column(0x05)

$C$DW$367	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$367, DW_AT_name("__SE_ELEDUP_8X")
	.dwattr $C$DW$367, DW_AT_const_value(0x03)
	.dwattr $C$DW$367, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$367, DW_AT_decl_line(0x8b)
	.dwattr $C$DW$367, DW_AT_decl_column(0x05)

$C$DW$368	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$368, DW_AT_name("__SE_ELEDUP_16X")
	.dwattr $C$DW$368, DW_AT_const_value(0x04)
	.dwattr $C$DW$368, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$368, DW_AT_decl_line(0x8c)
	.dwattr $C$DW$368, DW_AT_decl_column(0x05)

$C$DW$369	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$369, DW_AT_name("__SE_ELEDUP_32X")
	.dwattr $C$DW$369, DW_AT_const_value(0x05)
	.dwattr $C$DW$369, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$369, DW_AT_decl_line(0x8d)
	.dwattr $C$DW$369, DW_AT_decl_column(0x05)

$C$DW$370	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$370, DW_AT_name("__SE_ELEDUP_64X")
	.dwattr $C$DW$370, DW_AT_const_value(0x06)
	.dwattr $C$DW$370, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$370, DW_AT_decl_line(0x8e)
	.dwattr $C$DW$370, DW_AT_decl_column(0x05)

	.dwattr $C$DW$T$161, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$T$161, DW_AT_decl_line(0x86)
	.dwattr $C$DW$T$161, DW_AT_decl_column(0x2a)
	.dwendtag $C$DW$T$161

	.dwendtag $C$DW$TU$161


$C$DW$TU$162	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$162
$C$DW$T$162	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$162, DW_AT_name("__SE_ELEDUP")
	.dwattr $C$DW$T$162, DW_AT_type(*$C$DW$T$161)
	.dwattr $C$DW$T$162, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$T$162, DW_AT_decl_line(0x8f)
	.dwattr $C$DW$T$162, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$162


$C$DW$TU$190	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$190

$C$DW$T$190	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$190, DW_AT_name("__SE_ELEDUP")
	.dwattr $C$DW$T$190, DW_AT_byte_size(0x01)
$C$DW$371	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$371, DW_AT_name("__SE_ELEDUP_OFF")
	.dwattr $C$DW$371, DW_AT_const_value(0x00)
	.dwattr $C$DW$371, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$371, DW_AT_decl_line(0x88)
	.dwattr $C$DW$371, DW_AT_decl_column(0x05)

$C$DW$372	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$372, DW_AT_name("__SE_ELEDUP_2X")
	.dwattr $C$DW$372, DW_AT_const_value(0x01)
	.dwattr $C$DW$372, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$372, DW_AT_decl_line(0x89)
	.dwattr $C$DW$372, DW_AT_decl_column(0x05)

$C$DW$373	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$373, DW_AT_name("__SE_ELEDUP_4X")
	.dwattr $C$DW$373, DW_AT_const_value(0x02)
	.dwattr $C$DW$373, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$373, DW_AT_decl_line(0x8a)
	.dwattr $C$DW$373, DW_AT_decl_column(0x05)

$C$DW$374	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$374, DW_AT_name("__SE_ELEDUP_8X")
	.dwattr $C$DW$374, DW_AT_const_value(0x03)
	.dwattr $C$DW$374, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$374, DW_AT_decl_line(0x8b)
	.dwattr $C$DW$374, DW_AT_decl_column(0x05)

$C$DW$375	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$375, DW_AT_name("__SE_ELEDUP_16X")
	.dwattr $C$DW$375, DW_AT_const_value(0x04)
	.dwattr $C$DW$375, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$375, DW_AT_decl_line(0x8c)
	.dwattr $C$DW$375, DW_AT_decl_column(0x05)

$C$DW$376	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$376, DW_AT_name("__SE_ELEDUP_32X")
	.dwattr $C$DW$376, DW_AT_const_value(0x05)
	.dwattr $C$DW$376, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$376, DW_AT_decl_line(0x8d)
	.dwattr $C$DW$376, DW_AT_decl_column(0x05)

$C$DW$377	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$377, DW_AT_name("__SE_ELEDUP_64X")
	.dwattr $C$DW$377, DW_AT_const_value(0x06)
	.dwattr $C$DW$377, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$377, DW_AT_decl_line(0x8e)
	.dwattr $C$DW$377, DW_AT_decl_column(0x05)

	.dwattr $C$DW$T$190, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$T$190, DW_AT_decl_line(0x86)
	.dwattr $C$DW$T$190, DW_AT_decl_column(0x2a)
	.dwendtag $C$DW$T$190

	.dwendtag $C$DW$TU$190


$C$DW$TU$191	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$191
$C$DW$T$191	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$191, DW_AT_name("__SE_ELEDUP")
	.dwattr $C$DW$T$191, DW_AT_type(*$C$DW$T$190)
	.dwattr $C$DW$T$191, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$T$191, DW_AT_decl_line(0x8f)
	.dwattr $C$DW$T$191, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$191


$C$DW$TU$153	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$153

$C$DW$T$153	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$153, DW_AT_name("__SE_ELETYPE")
	.dwattr $C$DW$T$153, DW_AT_byte_size(0x01)
$C$DW$378	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$378, DW_AT_name("__SE_ELETYPE_8BIT")
	.dwattr $C$DW$378, DW_AT_const_value(0x00)
	.dwattr $C$DW$378, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$378, DW_AT_decl_line(0x3b)
	.dwattr $C$DW$378, DW_AT_decl_column(0x05)

$C$DW$379	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$379, DW_AT_name("__SE_ELETYPE_16BIT")
	.dwattr $C$DW$379, DW_AT_const_value(0x01)
	.dwattr $C$DW$379, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$379, DW_AT_decl_line(0x3c)
	.dwattr $C$DW$379, DW_AT_decl_column(0x05)

$C$DW$380	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$380, DW_AT_name("__SE_ELETYPE_32BIT")
	.dwattr $C$DW$380, DW_AT_const_value(0x02)
	.dwattr $C$DW$380, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$380, DW_AT_decl_line(0x3d)
	.dwattr $C$DW$380, DW_AT_decl_column(0x05)

$C$DW$381	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$381, DW_AT_name("__SE_ELETYPE_64BIT")
	.dwattr $C$DW$381, DW_AT_const_value(0x03)
	.dwattr $C$DW$381, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$381, DW_AT_decl_line(0x3e)
	.dwattr $C$DW$381, DW_AT_decl_column(0x05)

$C$DW$382	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$382, DW_AT_name("__SE_ELETYPE_8BIT_CMPLX_NOSWAP")
	.dwattr $C$DW$382, DW_AT_const_value(0x08)
	.dwattr $C$DW$382, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$382, DW_AT_decl_line(0x40)
	.dwattr $C$DW$382, DW_AT_decl_column(0x05)

$C$DW$383	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$383, DW_AT_name("__SE_ELETYPE_16BIT_CMPLX_NOSWAP")
	.dwattr $C$DW$383, DW_AT_const_value(0x09)
	.dwattr $C$DW$383, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$383, DW_AT_decl_line(0x41)
	.dwattr $C$DW$383, DW_AT_decl_column(0x05)

$C$DW$384	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$384, DW_AT_name("__SE_ELETYPE_32BIT_CMPLX_NOSWAP")
	.dwattr $C$DW$384, DW_AT_const_value(0x0a)
	.dwattr $C$DW$384, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$384, DW_AT_decl_line(0x42)
	.dwattr $C$DW$384, DW_AT_decl_column(0x05)

$C$DW$385	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$385, DW_AT_name("__SE_ELETYPE_64BIT_CMPLX_NOSWAP")
	.dwattr $C$DW$385, DW_AT_const_value(0x0b)
	.dwattr $C$DW$385, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$385, DW_AT_decl_line(0x43)
	.dwattr $C$DW$385, DW_AT_decl_column(0x05)

$C$DW$386	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$386, DW_AT_name("__SE_ELETYPE_8BIT_CMPLX_SWAP")
	.dwattr $C$DW$386, DW_AT_const_value(0x0c)
	.dwattr $C$DW$386, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$386, DW_AT_decl_line(0x44)
	.dwattr $C$DW$386, DW_AT_decl_column(0x05)

$C$DW$387	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$387, DW_AT_name("__SE_ELETYPE_16BIT_CMPLX_SWAP")
	.dwattr $C$DW$387, DW_AT_const_value(0x0d)
	.dwattr $C$DW$387, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$387, DW_AT_decl_line(0x45)
	.dwattr $C$DW$387, DW_AT_decl_column(0x05)

$C$DW$388	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$388, DW_AT_name("__SE_ELETYPE_32BIT_CMPLX_SWAP")
	.dwattr $C$DW$388, DW_AT_const_value(0x0e)
	.dwattr $C$DW$388, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$388, DW_AT_decl_line(0x46)
	.dwattr $C$DW$388, DW_AT_decl_column(0x05)

$C$DW$389	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$389, DW_AT_name("__SE_ELETYPE_64BIT_CMPLX_SWAP")
	.dwattr $C$DW$389, DW_AT_const_value(0x0f)
	.dwattr $C$DW$389, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$389, DW_AT_decl_line(0x47)
	.dwattr $C$DW$389, DW_AT_decl_column(0x05)

	.dwattr $C$DW$T$153, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$T$153, DW_AT_decl_line(0x39)
	.dwattr $C$DW$T$153, DW_AT_decl_column(0x2a)
	.dwendtag $C$DW$T$153

	.dwendtag $C$DW$TU$153


$C$DW$TU$154	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$154
$C$DW$T$154	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$154, DW_AT_name("__SE_ELETYPE")
	.dwattr $C$DW$T$154, DW_AT_type(*$C$DW$T$153)
	.dwattr $C$DW$T$154, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$T$154, DW_AT_decl_line(0x48)
	.dwattr $C$DW$T$154, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$154


$C$DW$TU$182	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$182

$C$DW$T$182	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$182, DW_AT_name("__SE_ELETYPE")
	.dwattr $C$DW$T$182, DW_AT_byte_size(0x01)
$C$DW$390	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$390, DW_AT_name("__SE_ELETYPE_8BIT")
	.dwattr $C$DW$390, DW_AT_const_value(0x00)
	.dwattr $C$DW$390, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$390, DW_AT_decl_line(0x3b)
	.dwattr $C$DW$390, DW_AT_decl_column(0x05)

$C$DW$391	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$391, DW_AT_name("__SE_ELETYPE_16BIT")
	.dwattr $C$DW$391, DW_AT_const_value(0x01)
	.dwattr $C$DW$391, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$391, DW_AT_decl_line(0x3c)
	.dwattr $C$DW$391, DW_AT_decl_column(0x05)

$C$DW$392	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$392, DW_AT_name("__SE_ELETYPE_32BIT")
	.dwattr $C$DW$392, DW_AT_const_value(0x02)
	.dwattr $C$DW$392, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$392, DW_AT_decl_line(0x3d)
	.dwattr $C$DW$392, DW_AT_decl_column(0x05)

$C$DW$393	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$393, DW_AT_name("__SE_ELETYPE_64BIT")
	.dwattr $C$DW$393, DW_AT_const_value(0x03)
	.dwattr $C$DW$393, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$393, DW_AT_decl_line(0x3e)
	.dwattr $C$DW$393, DW_AT_decl_column(0x05)

$C$DW$394	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$394, DW_AT_name("__SE_ELETYPE_8BIT_CMPLX_NOSWAP")
	.dwattr $C$DW$394, DW_AT_const_value(0x08)
	.dwattr $C$DW$394, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$394, DW_AT_decl_line(0x40)
	.dwattr $C$DW$394, DW_AT_decl_column(0x05)

$C$DW$395	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$395, DW_AT_name("__SE_ELETYPE_16BIT_CMPLX_NOSWAP")
	.dwattr $C$DW$395, DW_AT_const_value(0x09)
	.dwattr $C$DW$395, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$395, DW_AT_decl_line(0x41)
	.dwattr $C$DW$395, DW_AT_decl_column(0x05)

$C$DW$396	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$396, DW_AT_name("__SE_ELETYPE_32BIT_CMPLX_NOSWAP")
	.dwattr $C$DW$396, DW_AT_const_value(0x0a)
	.dwattr $C$DW$396, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$396, DW_AT_decl_line(0x42)
	.dwattr $C$DW$396, DW_AT_decl_column(0x05)

$C$DW$397	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$397, DW_AT_name("__SE_ELETYPE_64BIT_CMPLX_NOSWAP")
	.dwattr $C$DW$397, DW_AT_const_value(0x0b)
	.dwattr $C$DW$397, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$397, DW_AT_decl_line(0x43)
	.dwattr $C$DW$397, DW_AT_decl_column(0x05)

$C$DW$398	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$398, DW_AT_name("__SE_ELETYPE_8BIT_CMPLX_SWAP")
	.dwattr $C$DW$398, DW_AT_const_value(0x0c)
	.dwattr $C$DW$398, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$398, DW_AT_decl_line(0x44)
	.dwattr $C$DW$398, DW_AT_decl_column(0x05)

$C$DW$399	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$399, DW_AT_name("__SE_ELETYPE_16BIT_CMPLX_SWAP")
	.dwattr $C$DW$399, DW_AT_const_value(0x0d)
	.dwattr $C$DW$399, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$399, DW_AT_decl_line(0x45)
	.dwattr $C$DW$399, DW_AT_decl_column(0x05)

$C$DW$400	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$400, DW_AT_name("__SE_ELETYPE_32BIT_CMPLX_SWAP")
	.dwattr $C$DW$400, DW_AT_const_value(0x0e)
	.dwattr $C$DW$400, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$400, DW_AT_decl_line(0x46)
	.dwattr $C$DW$400, DW_AT_decl_column(0x05)

$C$DW$401	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$401, DW_AT_name("__SE_ELETYPE_64BIT_CMPLX_SWAP")
	.dwattr $C$DW$401, DW_AT_const_value(0x0f)
	.dwattr $C$DW$401, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$401, DW_AT_decl_line(0x47)
	.dwattr $C$DW$401, DW_AT_decl_column(0x05)

	.dwattr $C$DW$T$182, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$T$182, DW_AT_decl_line(0x39)
	.dwattr $C$DW$T$182, DW_AT_decl_column(0x2a)
	.dwendtag $C$DW$T$182

	.dwendtag $C$DW$TU$182


$C$DW$TU$183	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$183
$C$DW$T$183	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$183, DW_AT_name("__SE_ELETYPE")
	.dwattr $C$DW$T$183, DW_AT_type(*$C$DW$T$182)
	.dwattr $C$DW$T$183, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$T$183, DW_AT_decl_line(0x48)
	.dwattr $C$DW$T$183, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$183


$C$DW$TU$163	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$163

$C$DW$T$163	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$163, DW_AT_name("__SE_GRPDUP")
	.dwattr $C$DW$T$163, DW_AT_byte_size(0x01)
$C$DW$402	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$402, DW_AT_name("__SE_GRPDUP_OFF")
	.dwattr $C$DW$402, DW_AT_const_value(0x00)
	.dwattr $C$DW$402, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$402, DW_AT_decl_line(0x9b)
	.dwattr $C$DW$402, DW_AT_decl_column(0x05)

$C$DW$403	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$403, DW_AT_name("__SE_GRPDUP_ON")
	.dwattr $C$DW$403, DW_AT_const_value(0x01)
	.dwattr $C$DW$403, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$403, DW_AT_decl_line(0x9c)
	.dwattr $C$DW$403, DW_AT_decl_column(0x05)

	.dwattr $C$DW$T$163, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$T$163, DW_AT_decl_line(0x99)
	.dwattr $C$DW$T$163, DW_AT_decl_column(0x2a)
	.dwendtag $C$DW$T$163

	.dwendtag $C$DW$TU$163


$C$DW$TU$164	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$164
$C$DW$T$164	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$164, DW_AT_name("__SE_GRPDUP")
	.dwattr $C$DW$T$164, DW_AT_type(*$C$DW$T$163)
	.dwattr $C$DW$T$164, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$T$164, DW_AT_decl_line(0x9d)
	.dwattr $C$DW$T$164, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$164


$C$DW$TU$192	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$192

$C$DW$T$192	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$192, DW_AT_name("__SE_GRPDUP")
	.dwattr $C$DW$T$192, DW_AT_byte_size(0x01)
$C$DW$404	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$404, DW_AT_name("__SE_GRPDUP_OFF")
	.dwattr $C$DW$404, DW_AT_const_value(0x00)
	.dwattr $C$DW$404, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$404, DW_AT_decl_line(0x9b)
	.dwattr $C$DW$404, DW_AT_decl_column(0x05)

$C$DW$405	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$405, DW_AT_name("__SE_GRPDUP_ON")
	.dwattr $C$DW$405, DW_AT_const_value(0x01)
	.dwattr $C$DW$405, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$405, DW_AT_decl_line(0x9c)
	.dwattr $C$DW$405, DW_AT_decl_column(0x05)

	.dwattr $C$DW$T$192, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$T$192, DW_AT_decl_line(0x99)
	.dwattr $C$DW$T$192, DW_AT_decl_column(0x2a)
	.dwendtag $C$DW$T$192

	.dwendtag $C$DW$TU$192


$C$DW$TU$193	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$193
$C$DW$T$193	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$193, DW_AT_name("__SE_GRPDUP")
	.dwattr $C$DW$T$193, DW_AT_type(*$C$DW$T$192)
	.dwattr $C$DW$T$193, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$T$193, DW_AT_decl_line(0x9d)
	.dwattr $C$DW$T$193, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$193


$C$DW$TU$177	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$177

$C$DW$T$177	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$177, DW_AT_name("__SE_LEZR")
	.dwattr $C$DW$T$177, DW_AT_byte_size(0x01)
$C$DW$406	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$406, DW_AT_name("__SE_LEZR_OFF")
	.dwattr $C$DW$406, DW_AT_const_value(0x00)
	.dwattr $C$DW$406, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$406, DW_AT_decl_line(0xf5)
	.dwattr $C$DW$406, DW_AT_decl_column(0x05)

$C$DW$407	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$407, DW_AT_name("__SE_LEZR_ICNT0")
	.dwattr $C$DW$407, DW_AT_const_value(0x01)
	.dwattr $C$DW$407, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$407, DW_AT_decl_line(0xf6)
	.dwattr $C$DW$407, DW_AT_decl_column(0x05)

$C$DW$408	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$408, DW_AT_name("__SE_LEZR_ICNT1")
	.dwattr $C$DW$408, DW_AT_const_value(0x02)
	.dwattr $C$DW$408, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$408, DW_AT_decl_line(0xf7)
	.dwattr $C$DW$408, DW_AT_decl_column(0x05)

$C$DW$409	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$409, DW_AT_name("__SE_LEZR_ICNT2")
	.dwattr $C$DW$409, DW_AT_const_value(0x03)
	.dwattr $C$DW$409, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$409, DW_AT_decl_line(0xf8)
	.dwattr $C$DW$409, DW_AT_decl_column(0x05)

$C$DW$410	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$410, DW_AT_name("__SE_LEZR_ICNT3")
	.dwattr $C$DW$410, DW_AT_const_value(0x04)
	.dwattr $C$DW$410, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$410, DW_AT_decl_line(0xf9)
	.dwattr $C$DW$410, DW_AT_decl_column(0x05)

$C$DW$411	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$411, DW_AT_name("__SE_LEZR_ICNT4")
	.dwattr $C$DW$411, DW_AT_const_value(0x05)
	.dwattr $C$DW$411, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$411, DW_AT_decl_line(0xfa)
	.dwattr $C$DW$411, DW_AT_decl_column(0x05)

$C$DW$412	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$412, DW_AT_name("__SE_LEZR_ICNT5")
	.dwattr $C$DW$412, DW_AT_const_value(0x06)
	.dwattr $C$DW$412, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$412, DW_AT_decl_line(0xfb)
	.dwattr $C$DW$412, DW_AT_decl_column(0x05)

	.dwattr $C$DW$T$177, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$T$177, DW_AT_decl_line(0xf3)
	.dwattr $C$DW$T$177, DW_AT_decl_column(0x2a)
	.dwendtag $C$DW$T$177

	.dwendtag $C$DW$TU$177


$C$DW$TU$178	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$178
$C$DW$T$178	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$178, DW_AT_name("__SE_LEZR")
	.dwattr $C$DW$T$178, DW_AT_type(*$C$DW$T$177)
	.dwattr $C$DW$T$178, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$T$178, DW_AT_decl_line(0xfc)
	.dwattr $C$DW$T$178, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$178


$C$DW$TU$206	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$206

$C$DW$T$206	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$206, DW_AT_name("__SE_LEZR")
	.dwattr $C$DW$T$206, DW_AT_byte_size(0x01)
$C$DW$413	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$413, DW_AT_name("__SE_LEZR_OFF")
	.dwattr $C$DW$413, DW_AT_const_value(0x00)
	.dwattr $C$DW$413, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$413, DW_AT_decl_line(0xf5)
	.dwattr $C$DW$413, DW_AT_decl_column(0x05)

$C$DW$414	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$414, DW_AT_name("__SE_LEZR_ICNT0")
	.dwattr $C$DW$414, DW_AT_const_value(0x01)
	.dwattr $C$DW$414, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$414, DW_AT_decl_line(0xf6)
	.dwattr $C$DW$414, DW_AT_decl_column(0x05)

$C$DW$415	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$415, DW_AT_name("__SE_LEZR_ICNT1")
	.dwattr $C$DW$415, DW_AT_const_value(0x02)
	.dwattr $C$DW$415, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$415, DW_AT_decl_line(0xf7)
	.dwattr $C$DW$415, DW_AT_decl_column(0x05)

$C$DW$416	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$416, DW_AT_name("__SE_LEZR_ICNT2")
	.dwattr $C$DW$416, DW_AT_const_value(0x03)
	.dwattr $C$DW$416, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$416, DW_AT_decl_line(0xf8)
	.dwattr $C$DW$416, DW_AT_decl_column(0x05)

$C$DW$417	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$417, DW_AT_name("__SE_LEZR_ICNT3")
	.dwattr $C$DW$417, DW_AT_const_value(0x04)
	.dwattr $C$DW$417, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$417, DW_AT_decl_line(0xf9)
	.dwattr $C$DW$417, DW_AT_decl_column(0x05)

$C$DW$418	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$418, DW_AT_name("__SE_LEZR_ICNT4")
	.dwattr $C$DW$418, DW_AT_const_value(0x05)
	.dwattr $C$DW$418, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$418, DW_AT_decl_line(0xfa)
	.dwattr $C$DW$418, DW_AT_decl_column(0x05)

$C$DW$419	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$419, DW_AT_name("__SE_LEZR_ICNT5")
	.dwattr $C$DW$419, DW_AT_const_value(0x06)
	.dwattr $C$DW$419, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$419, DW_AT_decl_line(0xfb)
	.dwattr $C$DW$419, DW_AT_decl_column(0x05)

	.dwattr $C$DW$T$206, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$T$206, DW_AT_decl_line(0xf3)
	.dwattr $C$DW$T$206, DW_AT_decl_column(0x2a)
	.dwendtag $C$DW$T$206

	.dwendtag $C$DW$TU$206


$C$DW$TU$207	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$207
$C$DW$T$207	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$207, DW_AT_name("__SE_LEZR")
	.dwattr $C$DW$T$207, DW_AT_type(*$C$DW$T$206)
	.dwattr $C$DW$T$207, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$T$207, DW_AT_decl_line(0xfc)
	.dwattr $C$DW$T$207, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$207


$C$DW$TU$157	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$157

$C$DW$T$157	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$157, DW_AT_name("__SE_PROMOTE")
	.dwattr $C$DW$T$157, DW_AT_byte_size(0x01)
$C$DW$420	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$420, DW_AT_name("__SE_PROMOTE_OFF")
	.dwattr $C$DW$420, DW_AT_const_value(0x00)
	.dwattr $C$DW$420, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$420, DW_AT_decl_line(0x62)
	.dwattr $C$DW$420, DW_AT_decl_column(0x05)

$C$DW$421	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$421, DW_AT_name("__SE_PROMOTE_2X_ZEROEXT")
	.dwattr $C$DW$421, DW_AT_const_value(0x01)
	.dwattr $C$DW$421, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$421, DW_AT_decl_line(0x63)
	.dwattr $C$DW$421, DW_AT_decl_column(0x05)

$C$DW$422	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$422, DW_AT_name("__SE_PROMOTE_4X_ZEROEXT")
	.dwattr $C$DW$422, DW_AT_const_value(0x02)
	.dwattr $C$DW$422, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$422, DW_AT_decl_line(0x64)
	.dwattr $C$DW$422, DW_AT_decl_column(0x05)

$C$DW$423	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$423, DW_AT_name("__SE_PROMOTE_8X_ZEROEXT")
	.dwattr $C$DW$423, DW_AT_const_value(0x03)
	.dwattr $C$DW$423, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$423, DW_AT_decl_line(0x65)
	.dwattr $C$DW$423, DW_AT_decl_column(0x05)

$C$DW$424	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$424, DW_AT_name("__SE_PROMOTE_2X_SIGNEXT")
	.dwattr $C$DW$424, DW_AT_const_value(0x05)
	.dwattr $C$DW$424, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$424, DW_AT_decl_line(0x67)
	.dwattr $C$DW$424, DW_AT_decl_column(0x05)

$C$DW$425	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$425, DW_AT_name("__SE_PROMOTE_4X_SIGNEXT")
	.dwattr $C$DW$425, DW_AT_const_value(0x06)
	.dwattr $C$DW$425, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$425, DW_AT_decl_line(0x68)
	.dwattr $C$DW$425, DW_AT_decl_column(0x05)

$C$DW$426	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$426, DW_AT_name("__SE_PROMOTE_8X_SIGNEXT")
	.dwattr $C$DW$426, DW_AT_const_value(0x07)
	.dwattr $C$DW$426, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$426, DW_AT_decl_line(0x69)
	.dwattr $C$DW$426, DW_AT_decl_column(0x05)

	.dwattr $C$DW$T$157, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$T$157, DW_AT_decl_line(0x60)
	.dwattr $C$DW$T$157, DW_AT_decl_column(0x2a)
	.dwendtag $C$DW$T$157

	.dwendtag $C$DW$TU$157


$C$DW$TU$158	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$158
$C$DW$T$158	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$158, DW_AT_name("__SE_PROMOTE")
	.dwattr $C$DW$T$158, DW_AT_type(*$C$DW$T$157)
	.dwattr $C$DW$T$158, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$T$158, DW_AT_decl_line(0x6a)
	.dwattr $C$DW$T$158, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$158


$C$DW$TU$186	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$186

$C$DW$T$186	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$186, DW_AT_name("__SE_PROMOTE")
	.dwattr $C$DW$T$186, DW_AT_byte_size(0x01)
$C$DW$427	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$427, DW_AT_name("__SE_PROMOTE_OFF")
	.dwattr $C$DW$427, DW_AT_const_value(0x00)
	.dwattr $C$DW$427, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$427, DW_AT_decl_line(0x62)
	.dwattr $C$DW$427, DW_AT_decl_column(0x05)

$C$DW$428	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$428, DW_AT_name("__SE_PROMOTE_2X_ZEROEXT")
	.dwattr $C$DW$428, DW_AT_const_value(0x01)
	.dwattr $C$DW$428, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$428, DW_AT_decl_line(0x63)
	.dwattr $C$DW$428, DW_AT_decl_column(0x05)

$C$DW$429	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$429, DW_AT_name("__SE_PROMOTE_4X_ZEROEXT")
	.dwattr $C$DW$429, DW_AT_const_value(0x02)
	.dwattr $C$DW$429, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$429, DW_AT_decl_line(0x64)
	.dwattr $C$DW$429, DW_AT_decl_column(0x05)

$C$DW$430	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$430, DW_AT_name("__SE_PROMOTE_8X_ZEROEXT")
	.dwattr $C$DW$430, DW_AT_const_value(0x03)
	.dwattr $C$DW$430, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$430, DW_AT_decl_line(0x65)
	.dwattr $C$DW$430, DW_AT_decl_column(0x05)

$C$DW$431	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$431, DW_AT_name("__SE_PROMOTE_2X_SIGNEXT")
	.dwattr $C$DW$431, DW_AT_const_value(0x05)
	.dwattr $C$DW$431, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$431, DW_AT_decl_line(0x67)
	.dwattr $C$DW$431, DW_AT_decl_column(0x05)

$C$DW$432	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$432, DW_AT_name("__SE_PROMOTE_4X_SIGNEXT")
	.dwattr $C$DW$432, DW_AT_const_value(0x06)
	.dwattr $C$DW$432, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$432, DW_AT_decl_line(0x68)
	.dwattr $C$DW$432, DW_AT_decl_column(0x05)

$C$DW$433	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$433, DW_AT_name("__SE_PROMOTE_8X_SIGNEXT")
	.dwattr $C$DW$433, DW_AT_const_value(0x07)
	.dwattr $C$DW$433, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$433, DW_AT_decl_line(0x69)
	.dwattr $C$DW$433, DW_AT_decl_column(0x05)

	.dwattr $C$DW$T$186, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$T$186, DW_AT_decl_line(0x60)
	.dwattr $C$DW$T$186, DW_AT_decl_column(0x2a)
	.dwendtag $C$DW$T$186

	.dwendtag $C$DW$TU$186


$C$DW$TU$187	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$187
$C$DW$T$187	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$187, DW_AT_name("__SE_PROMOTE")
	.dwattr $C$DW$T$187, DW_AT_type(*$C$DW$T$186)
	.dwattr $C$DW$T$187, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$T$187, DW_AT_decl_line(0x6a)
	.dwattr $C$DW$T$187, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$187


$C$DW$TU$179	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$179

$C$DW$T$179	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$179, DW_AT_name("__SE_TEMPLATE_FMT")
	.dwattr $C$DW$T$179, DW_AT_byte_size(0x01)
$C$DW$434	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$434, DW_AT_name("__SE_TEMPLATE_FMT_v1")
	.dwattr $C$DW$434, DW_AT_const_value(0x00)
	.dwattr $C$DW$434, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$434, DW_AT_decl_line(0x10c)
	.dwattr $C$DW$434, DW_AT_decl_column(0x05)

	.dwattr $C$DW$T$179, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$T$179, DW_AT_decl_line(0x10a)
	.dwattr $C$DW$T$179, DW_AT_decl_column(0x2a)
	.dwendtag $C$DW$T$179

	.dwendtag $C$DW$TU$179


$C$DW$TU$180	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$180
$C$DW$T$180	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$180, DW_AT_name("__SE_TEMPLATE_FMT")
	.dwattr $C$DW$T$180, DW_AT_type(*$C$DW$T$179)
	.dwattr $C$DW$T$180, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$T$180, DW_AT_decl_line(0x10d)
	.dwattr $C$DW$T$180, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$180


$C$DW$TU$208	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$208

$C$DW$T$208	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$208, DW_AT_name("__SE_TEMPLATE_FMT")
	.dwattr $C$DW$T$208, DW_AT_byte_size(0x01)
$C$DW$435	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$435, DW_AT_name("__SE_TEMPLATE_FMT_v1")
	.dwattr $C$DW$435, DW_AT_const_value(0x00)
	.dwattr $C$DW$435, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$435, DW_AT_decl_line(0x10c)
	.dwattr $C$DW$435, DW_AT_decl_column(0x05)

	.dwattr $C$DW$T$208, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$T$208, DW_AT_decl_line(0x10a)
	.dwattr $C$DW$T$208, DW_AT_decl_column(0x2a)
	.dwendtag $C$DW$T$208

	.dwendtag $C$DW$TU$208


$C$DW$TU$209	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$209
$C$DW$T$209	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$209, DW_AT_name("__SE_TEMPLATE_FMT")
	.dwattr $C$DW$T$209, DW_AT_type(*$C$DW$T$208)
	.dwattr $C$DW$T$209, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$T$209, DW_AT_decl_line(0x10d)
	.dwattr $C$DW$T$209, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$209


$C$DW$TU$181	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$181

$C$DW$T$181	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$181, DW_AT_name("__SE_TEMPLATE_v1")
	.dwattr $C$DW$T$181, DW_AT_byte_size(0x40)
$C$DW$436	.dwtag  DW_TAG_member
	.dwattr $C$DW$436, DW_AT_type(*$C$DW$T$123)
	.dwattr $C$DW$436, DW_AT_name("ICNT0")
	.dwattr $C$DW$436, DW_AT_bit_offset(0x20)
	.dwattr $C$DW$436, DW_AT_bit_size(0x20)
	.dwattr $C$DW$436, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$436, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$436, DW_AT_decl_line(0x116)
	.dwattr $C$DW$436, DW_AT_decl_column(0x0e)

$C$DW$437	.dwtag  DW_TAG_member
	.dwattr $C$DW$437, DW_AT_type(*$C$DW$T$123)
	.dwattr $C$DW$437, DW_AT_name("ICNT1")
	.dwattr $C$DW$437, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$437, DW_AT_bit_size(0x20)
	.dwattr $C$DW$437, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$437, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$437, DW_AT_decl_line(0x117)
	.dwattr $C$DW$437, DW_AT_decl_column(0x0e)

$C$DW$438	.dwtag  DW_TAG_member
	.dwattr $C$DW$438, DW_AT_type(*$C$DW$T$123)
	.dwattr $C$DW$438, DW_AT_name("ICNT2")
	.dwattr $C$DW$438, DW_AT_bit_offset(0x20)
	.dwattr $C$DW$438, DW_AT_bit_size(0x20)
	.dwattr $C$DW$438, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$438, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$438, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$438, DW_AT_decl_line(0x118)
	.dwattr $C$DW$438, DW_AT_decl_column(0x0e)

$C$DW$439	.dwtag  DW_TAG_member
	.dwattr $C$DW$439, DW_AT_type(*$C$DW$T$123)
	.dwattr $C$DW$439, DW_AT_name("ICNT3")
	.dwattr $C$DW$439, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$439, DW_AT_bit_size(0x20)
	.dwattr $C$DW$439, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$439, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$439, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$439, DW_AT_decl_line(0x119)
	.dwattr $C$DW$439, DW_AT_decl_column(0x0e)

$C$DW$440	.dwtag  DW_TAG_member
	.dwattr $C$DW$440, DW_AT_type(*$C$DW$T$123)
	.dwattr $C$DW$440, DW_AT_name("ICNT4")
	.dwattr $C$DW$440, DW_AT_bit_offset(0x20)
	.dwattr $C$DW$440, DW_AT_bit_size(0x20)
	.dwattr $C$DW$440, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$440, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$440, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$440, DW_AT_decl_line(0x11a)
	.dwattr $C$DW$440, DW_AT_decl_column(0x0e)

$C$DW$441	.dwtag  DW_TAG_member
	.dwattr $C$DW$441, DW_AT_type(*$C$DW$T$123)
	.dwattr $C$DW$441, DW_AT_name("ICNT5")
	.dwattr $C$DW$441, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$441, DW_AT_bit_size(0x20)
	.dwattr $C$DW$441, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$441, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$441, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$441, DW_AT_decl_line(0x11b)
	.dwattr $C$DW$441, DW_AT_decl_column(0x0e)

$C$DW$442	.dwtag  DW_TAG_member
	.dwattr $C$DW$442, DW_AT_type(*$C$DW$T$123)
	.dwattr $C$DW$442, DW_AT_name("DECDIM1_WIDTH")
	.dwattr $C$DW$442, DW_AT_bit_offset(0x20)
	.dwattr $C$DW$442, DW_AT_bit_size(0x20)
	.dwattr $C$DW$442, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$442, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$442, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$442, DW_AT_decl_line(0x11c)
	.dwattr $C$DW$442, DW_AT_decl_column(0x0e)

$C$DW$443	.dwtag  DW_TAG_member
	.dwattr $C$DW$443, DW_AT_type(*$C$DW$T$123)
	.dwattr $C$DW$443, DW_AT_name("DECDIM2_WIDTH")
	.dwattr $C$DW$443, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$443, DW_AT_bit_size(0x20)
	.dwattr $C$DW$443, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$443, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$443, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$443, DW_AT_decl_line(0x11d)
	.dwattr $C$DW$443, DW_AT_decl_column(0x0e)

$C$DW$444	.dwtag  DW_TAG_member
	.dwattr $C$DW$444, DW_AT_type(*$C$DW$T$125)
	.dwattr $C$DW$444, DW_AT_name("DIM1")
	.dwattr $C$DW$444, DW_AT_bit_offset(0x20)
	.dwattr $C$DW$444, DW_AT_bit_size(0x20)
	.dwattr $C$DW$444, DW_AT_data_member_location[DW_OP_plus_uconst 0x20]
	.dwattr $C$DW$444, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$444, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$444, DW_AT_decl_line(0x11e)
	.dwattr $C$DW$444, DW_AT_decl_column(0x0e)

$C$DW$445	.dwtag  DW_TAG_member
	.dwattr $C$DW$445, DW_AT_type(*$C$DW$T$125)
	.dwattr $C$DW$445, DW_AT_name("DIM2")
	.dwattr $C$DW$445, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$445, DW_AT_bit_size(0x20)
	.dwattr $C$DW$445, DW_AT_data_member_location[DW_OP_plus_uconst 0x20]
	.dwattr $C$DW$445, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$445, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$445, DW_AT_decl_line(0x11f)
	.dwattr $C$DW$445, DW_AT_decl_column(0x0e)

$C$DW$446	.dwtag  DW_TAG_member
	.dwattr $C$DW$446, DW_AT_type(*$C$DW$T$125)
	.dwattr $C$DW$446, DW_AT_name("DIM3")
	.dwattr $C$DW$446, DW_AT_bit_offset(0x20)
	.dwattr $C$DW$446, DW_AT_bit_size(0x20)
	.dwattr $C$DW$446, DW_AT_data_member_location[DW_OP_plus_uconst 0x28]
	.dwattr $C$DW$446, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$446, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$446, DW_AT_decl_line(0x120)
	.dwattr $C$DW$446, DW_AT_decl_column(0x0e)

$C$DW$447	.dwtag  DW_TAG_member
	.dwattr $C$DW$447, DW_AT_type(*$C$DW$T$125)
	.dwattr $C$DW$447, DW_AT_name("DIM4")
	.dwattr $C$DW$447, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$447, DW_AT_bit_size(0x20)
	.dwattr $C$DW$447, DW_AT_data_member_location[DW_OP_plus_uconst 0x28]
	.dwattr $C$DW$447, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$447, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$447, DW_AT_decl_line(0x121)
	.dwattr $C$DW$447, DW_AT_decl_column(0x0e)

$C$DW$448	.dwtag  DW_TAG_member
	.dwattr $C$DW$448, DW_AT_type(*$C$DW$T$125)
	.dwattr $C$DW$448, DW_AT_name("DIM5")
	.dwattr $C$DW$448, DW_AT_bit_offset(0x20)
	.dwattr $C$DW$448, DW_AT_bit_size(0x20)
	.dwattr $C$DW$448, DW_AT_data_member_location[DW_OP_plus_uconst 0x30]
	.dwattr $C$DW$448, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$448, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$448, DW_AT_decl_line(0x122)
	.dwattr $C$DW$448, DW_AT_decl_column(0x0e)

$C$DW$449	.dwtag  DW_TAG_member
	.dwattr $C$DW$449, DW_AT_type(*$C$DW$T$123)
	.dwattr $C$DW$449, DW_AT_name("LEZR_CNT")
	.dwattr $C$DW$449, DW_AT_bit_offset(0x18)
	.dwattr $C$DW$449, DW_AT_bit_size(0x08)
	.dwattr $C$DW$449, DW_AT_data_member_location[DW_OP_plus_uconst 0x30]
	.dwattr $C$DW$449, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$449, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$449, DW_AT_decl_line(0x123)
	.dwattr $C$DW$449, DW_AT_decl_column(0x0e)

$C$DW$450	.dwtag  DW_TAG_member
	.dwattr $C$DW$450, DW_AT_type(*$C$DW$T$123)
	.dwattr $C$DW$450, DW_AT_name("_reserved1")
	.dwattr $C$DW$450, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$450, DW_AT_bit_size(0x18)
	.dwattr $C$DW$450, DW_AT_data_member_location[DW_OP_plus_uconst 0x30]
	.dwattr $C$DW$450, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$450, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$450, DW_AT_decl_line(0x124)
	.dwattr $C$DW$450, DW_AT_decl_column(0x0e)

$C$DW$451	.dwtag  DW_TAG_member
	.dwattr $C$DW$451, DW_AT_type(*$C$DW$T$154)
	.dwattr $C$DW$451, DW_AT_name("ELETYPE")
	.dwattr $C$DW$451, DW_AT_bit_offset(0x04)
	.dwattr $C$DW$451, DW_AT_bit_size(0x04)
	.dwattr $C$DW$451, DW_AT_data_member_location[DW_OP_plus_uconst 0x38]
	.dwattr $C$DW$451, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$451, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$451, DW_AT_decl_line(0x126)
	.dwattr $C$DW$451, DW_AT_decl_column(0x12)

$C$DW$452	.dwtag  DW_TAG_member
	.dwattr $C$DW$452, DW_AT_type(*$C$DW$T$156)
	.dwattr $C$DW$452, DW_AT_name("TRANSPOSE")
	.dwattr $C$DW$452, DW_AT_bit_offset(0x01)
	.dwattr $C$DW$452, DW_AT_bit_size(0x03)
	.dwattr $C$DW$452, DW_AT_data_member_location[DW_OP_plus_uconst 0x38]
	.dwattr $C$DW$452, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$452, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$452, DW_AT_decl_line(0x127)
	.dwattr $C$DW$452, DW_AT_decl_column(0x14)

$C$DW$453	.dwtag  DW_TAG_member
	.dwattr $C$DW$453, DW_AT_type(*$C$DW$T$123)
	.dwattr $C$DW$453, DW_AT_name("_reserved2")
	.dwattr $C$DW$453, DW_AT_bit_offset(0x38)
	.dwattr $C$DW$453, DW_AT_bit_size(0x01)
	.dwattr $C$DW$453, DW_AT_data_member_location[DW_OP_plus_uconst 0x38]
	.dwattr $C$DW$453, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$453, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$453, DW_AT_decl_line(0x128)
	.dwattr $C$DW$453, DW_AT_decl_column(0x0e)

$C$DW$454	.dwtag  DW_TAG_member
	.dwattr $C$DW$454, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$454, DW_AT_name("PROMOTE")
	.dwattr $C$DW$454, DW_AT_bit_offset(0x05)
	.dwattr $C$DW$454, DW_AT_bit_size(0x03)
	.dwattr $C$DW$454, DW_AT_data_member_location[DW_OP_plus_uconst 0x39]
	.dwattr $C$DW$454, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$454, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$454, DW_AT_decl_line(0x129)
	.dwattr $C$DW$454, DW_AT_decl_column(0x12)

$C$DW$455	.dwtag  DW_TAG_member
	.dwattr $C$DW$455, DW_AT_type(*$C$DW$T$123)
	.dwattr $C$DW$455, DW_AT_name("_reserved3")
	.dwattr $C$DW$455, DW_AT_bit_offset(0x34)
	.dwattr $C$DW$455, DW_AT_bit_size(0x01)
	.dwattr $C$DW$455, DW_AT_data_member_location[DW_OP_plus_uconst 0x38]
	.dwattr $C$DW$455, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$455, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$455, DW_AT_decl_line(0x12a)
	.dwattr $C$DW$455, DW_AT_decl_column(0x0e)

$C$DW$456	.dwtag  DW_TAG_member
	.dwattr $C$DW$456, DW_AT_type(*$C$DW$T$160)
	.dwattr $C$DW$456, DW_AT_name("VECLEN")
	.dwattr $C$DW$456, DW_AT_bit_offset(0x01)
	.dwattr $C$DW$456, DW_AT_bit_size(0x03)
	.dwattr $C$DW$456, DW_AT_data_member_location[DW_OP_plus_uconst 0x39]
	.dwattr $C$DW$456, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$456, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$456, DW_AT_decl_line(0x12b)
	.dwattr $C$DW$456, DW_AT_decl_column(0x11)

$C$DW$457	.dwtag  DW_TAG_member
	.dwattr $C$DW$457, DW_AT_type(*$C$DW$T$123)
	.dwattr $C$DW$457, DW_AT_name("_reserved4")
	.dwattr $C$DW$457, DW_AT_bit_offset(0x30)
	.dwattr $C$DW$457, DW_AT_bit_size(0x01)
	.dwattr $C$DW$457, DW_AT_data_member_location[DW_OP_plus_uconst 0x38]
	.dwattr $C$DW$457, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$457, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$457, DW_AT_decl_line(0x12c)
	.dwattr $C$DW$457, DW_AT_decl_column(0x0e)

$C$DW$458	.dwtag  DW_TAG_member
	.dwattr $C$DW$458, DW_AT_type(*$C$DW$T$162)
	.dwattr $C$DW$458, DW_AT_name("ELEDUP")
	.dwattr $C$DW$458, DW_AT_bit_offset(0x05)
	.dwattr $C$DW$458, DW_AT_bit_size(0x03)
	.dwattr $C$DW$458, DW_AT_data_member_location[DW_OP_plus_uconst 0x3a]
	.dwattr $C$DW$458, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$458, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$458, DW_AT_decl_line(0x12d)
	.dwattr $C$DW$458, DW_AT_decl_column(0x11)

$C$DW$459	.dwtag  DW_TAG_member
	.dwattr $C$DW$459, DW_AT_type(*$C$DW$T$164)
	.dwattr $C$DW$459, DW_AT_name("GRPDUP")
	.dwattr $C$DW$459, DW_AT_bit_offset(0x04)
	.dwattr $C$DW$459, DW_AT_bit_size(0x01)
	.dwattr $C$DW$459, DW_AT_data_member_location[DW_OP_plus_uconst 0x3a]
	.dwattr $C$DW$459, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$459, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$459, DW_AT_decl_line(0x12e)
	.dwattr $C$DW$459, DW_AT_decl_column(0x11)

$C$DW$460	.dwtag  DW_TAG_member
	.dwattr $C$DW$460, DW_AT_type(*$C$DW$T$166)
	.dwattr $C$DW$460, DW_AT_name("DECIM")
	.dwattr $C$DW$460, DW_AT_bit_offset(0x02)
	.dwattr $C$DW$460, DW_AT_bit_size(0x02)
	.dwattr $C$DW$460, DW_AT_data_member_location[DW_OP_plus_uconst 0x3a]
	.dwattr $C$DW$460, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$460, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$460, DW_AT_decl_line(0x12f)
	.dwattr $C$DW$460, DW_AT_decl_column(0x10)

$C$DW$461	.dwtag  DW_TAG_member
	.dwattr $C$DW$461, DW_AT_type(*$C$DW$T$123)
	.dwattr $C$DW$461, DW_AT_name("_reserved5")
	.dwattr $C$DW$461, DW_AT_bit_offset(0x28)
	.dwattr $C$DW$461, DW_AT_bit_size(0x02)
	.dwattr $C$DW$461, DW_AT_data_member_location[DW_OP_plus_uconst 0x38]
	.dwattr $C$DW$461, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$461, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$461, DW_AT_decl_line(0x130)
	.dwattr $C$DW$461, DW_AT_decl_column(0x0e)

$C$DW$462	.dwtag  DW_TAG_member
	.dwattr $C$DW$462, DW_AT_type(*$C$DW$T$168)
	.dwattr $C$DW$462, DW_AT_name("DIMFMT")
	.dwattr $C$DW$462, DW_AT_bit_offset(0x05)
	.dwattr $C$DW$462, DW_AT_bit_size(0x03)
	.dwattr $C$DW$462, DW_AT_data_member_location[DW_OP_plus_uconst 0x3b]
	.dwattr $C$DW$462, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$462, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$462, DW_AT_decl_line(0x131)
	.dwattr $C$DW$462, DW_AT_decl_column(0x11)

$C$DW$463	.dwtag  DW_TAG_member
	.dwattr $C$DW$463, DW_AT_type(*$C$DW$T$170)
	.dwattr $C$DW$463, DW_AT_name("DIR")
	.dwattr $C$DW$463, DW_AT_bit_offset(0x04)
	.dwattr $C$DW$463, DW_AT_bit_size(0x01)
	.dwattr $C$DW$463, DW_AT_data_member_location[DW_OP_plus_uconst 0x3b]
	.dwattr $C$DW$463, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$463, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$463, DW_AT_decl_line(0x132)
	.dwattr $C$DW$463, DW_AT_decl_column(0x0e)

$C$DW$464	.dwtag  DW_TAG_member
	.dwattr $C$DW$464, DW_AT_type(*$C$DW$T$123)
	.dwattr $C$DW$464, DW_AT_name("CBK0")
	.dwattr $C$DW$464, DW_AT_bit_offset(0x20)
	.dwattr $C$DW$464, DW_AT_bit_size(0x04)
	.dwattr $C$DW$464, DW_AT_data_member_location[DW_OP_plus_uconst 0x38]
	.dwattr $C$DW$464, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$464, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$464, DW_AT_decl_line(0x133)
	.dwattr $C$DW$464, DW_AT_decl_column(0x0e)

$C$DW$465	.dwtag  DW_TAG_member
	.dwattr $C$DW$465, DW_AT_type(*$C$DW$T$123)
	.dwattr $C$DW$465, DW_AT_name("CBK1")
	.dwattr $C$DW$465, DW_AT_bit_offset(0x1c)
	.dwattr $C$DW$465, DW_AT_bit_size(0x04)
	.dwattr $C$DW$465, DW_AT_data_member_location[DW_OP_plus_uconst 0x38]
	.dwattr $C$DW$465, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$465, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$465, DW_AT_decl_line(0x134)
	.dwattr $C$DW$465, DW_AT_decl_column(0x0e)

$C$DW$466	.dwtag  DW_TAG_member
	.dwattr $C$DW$466, DW_AT_type(*$C$DW$T$172)
	.dwattr $C$DW$466, DW_AT_name("AM0")
	.dwattr $C$DW$466, DW_AT_bit_offset(0x02)
	.dwattr $C$DW$466, DW_AT_bit_size(0x02)
	.dwattr $C$DW$466, DW_AT_data_member_location[DW_OP_plus_uconst 0x3c]
	.dwattr $C$DW$466, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$466, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$466, DW_AT_decl_line(0x135)
	.dwattr $C$DW$466, DW_AT_decl_column(0x0d)

$C$DW$467	.dwtag  DW_TAG_member
	.dwattr $C$DW$467, DW_AT_type(*$C$DW$T$172)
	.dwattr $C$DW$467, DW_AT_name("AM1")
	.dwattr $C$DW$467, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$467, DW_AT_bit_size(0x02)
	.dwattr $C$DW$467, DW_AT_data_member_location[DW_OP_plus_uconst 0x3c]
	.dwattr $C$DW$467, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$467, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$467, DW_AT_decl_line(0x136)
	.dwattr $C$DW$467, DW_AT_decl_column(0x0d)

$C$DW$468	.dwtag  DW_TAG_member
	.dwattr $C$DW$468, DW_AT_type(*$C$DW$T$172)
	.dwattr $C$DW$468, DW_AT_name("AM2")
	.dwattr $C$DW$468, DW_AT_bit_offset(0x06)
	.dwattr $C$DW$468, DW_AT_bit_size(0x02)
	.dwattr $C$DW$468, DW_AT_data_member_location[DW_OP_plus_uconst 0x3d]
	.dwattr $C$DW$468, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$468, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$468, DW_AT_decl_line(0x137)
	.dwattr $C$DW$468, DW_AT_decl_column(0x0d)

$C$DW$469	.dwtag  DW_TAG_member
	.dwattr $C$DW$469, DW_AT_type(*$C$DW$T$172)
	.dwattr $C$DW$469, DW_AT_name("AM3")
	.dwattr $C$DW$469, DW_AT_bit_offset(0x04)
	.dwattr $C$DW$469, DW_AT_bit_size(0x02)
	.dwattr $C$DW$469, DW_AT_data_member_location[DW_OP_plus_uconst 0x3d]
	.dwattr $C$DW$469, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$469, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$469, DW_AT_decl_line(0x138)
	.dwattr $C$DW$469, DW_AT_decl_column(0x0d)

$C$DW$470	.dwtag  DW_TAG_member
	.dwattr $C$DW$470, DW_AT_type(*$C$DW$T$172)
	.dwattr $C$DW$470, DW_AT_name("AM4")
	.dwattr $C$DW$470, DW_AT_bit_offset(0x02)
	.dwattr $C$DW$470, DW_AT_bit_size(0x02)
	.dwattr $C$DW$470, DW_AT_data_member_location[DW_OP_plus_uconst 0x3d]
	.dwattr $C$DW$470, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$470, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$470, DW_AT_decl_line(0x139)
	.dwattr $C$DW$470, DW_AT_decl_column(0x0d)

$C$DW$471	.dwtag  DW_TAG_member
	.dwattr $C$DW$471, DW_AT_type(*$C$DW$T$172)
	.dwattr $C$DW$471, DW_AT_name("AM5")
	.dwattr $C$DW$471, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$471, DW_AT_bit_size(0x02)
	.dwattr $C$DW$471, DW_AT_data_member_location[DW_OP_plus_uconst 0x3d]
	.dwattr $C$DW$471, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$471, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$471, DW_AT_decl_line(0x13a)
	.dwattr $C$DW$471, DW_AT_decl_column(0x0d)

$C$DW$472	.dwtag  DW_TAG_member
	.dwattr $C$DW$472, DW_AT_type(*$C$DW$T$174)
	.dwattr $C$DW$472, DW_AT_name("DECDIM1")
	.dwattr $C$DW$472, DW_AT_bit_offset(0x05)
	.dwattr $C$DW$472, DW_AT_bit_size(0x03)
	.dwattr $C$DW$472, DW_AT_data_member_location[DW_OP_plus_uconst 0x3e]
	.dwattr $C$DW$472, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$472, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$472, DW_AT_decl_line(0x13b)
	.dwattr $C$DW$472, DW_AT_decl_column(0x11)

$C$DW$473	.dwtag  DW_TAG_member
	.dwattr $C$DW$473, DW_AT_type(*$C$DW$T$176)
	.dwattr $C$DW$473, DW_AT_name("DECDIM1SD")
	.dwattr $C$DW$473, DW_AT_bit_offset(0x03)
	.dwattr $C$DW$473, DW_AT_bit_size(0x02)
	.dwattr $C$DW$473, DW_AT_data_member_location[DW_OP_plus_uconst 0x3e]
	.dwattr $C$DW$473, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$473, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$473, DW_AT_decl_line(0x13c)
	.dwattr $C$DW$473, DW_AT_decl_column(0x13)

$C$DW$474	.dwtag  DW_TAG_member
	.dwattr $C$DW$474, DW_AT_type(*$C$DW$T$174)
	.dwattr $C$DW$474, DW_AT_name("DECDIM2")
	.dwattr $C$DW$474, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$474, DW_AT_bit_size(0x03)
	.dwattr $C$DW$474, DW_AT_data_member_location[DW_OP_plus_uconst 0x3e]
	.dwattr $C$DW$474, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$474, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$474, DW_AT_decl_line(0x13d)
	.dwattr $C$DW$474, DW_AT_decl_column(0x11)

$C$DW$475	.dwtag  DW_TAG_member
	.dwattr $C$DW$475, DW_AT_type(*$C$DW$T$176)
	.dwattr $C$DW$475, DW_AT_name("DECDIM2SD")
	.dwattr $C$DW$475, DW_AT_bit_offset(0x06)
	.dwattr $C$DW$475, DW_AT_bit_size(0x02)
	.dwattr $C$DW$475, DW_AT_data_member_location[DW_OP_plus_uconst 0x3f]
	.dwattr $C$DW$475, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$475, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$475, DW_AT_decl_line(0x13e)
	.dwattr $C$DW$475, DW_AT_decl_column(0x13)

$C$DW$476	.dwtag  DW_TAG_member
	.dwattr $C$DW$476, DW_AT_type(*$C$DW$T$178)
	.dwattr $C$DW$476, DW_AT_name("LEZR")
	.dwattr $C$DW$476, DW_AT_bit_offset(0x03)
	.dwattr $C$DW$476, DW_AT_bit_size(0x03)
	.dwattr $C$DW$476, DW_AT_data_member_location[DW_OP_plus_uconst 0x3f]
	.dwattr $C$DW$476, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$476, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$476, DW_AT_decl_line(0x13f)
	.dwattr $C$DW$476, DW_AT_decl_column(0x0f)

$C$DW$477	.dwtag  DW_TAG_member
	.dwattr $C$DW$477, DW_AT_type(*$C$DW$T$180)
	.dwattr $C$DW$477, DW_AT_name("TEMPLATE_FMT")
	.dwattr $C$DW$477, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$477, DW_AT_bit_size(0x03)
	.dwattr $C$DW$477, DW_AT_data_member_location[DW_OP_plus_uconst 0x3f]
	.dwattr $C$DW$477, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$477, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$477, DW_AT_decl_line(0x140)
	.dwattr $C$DW$477, DW_AT_decl_column(0x17)

	.dwattr $C$DW$T$181, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$T$181, DW_AT_decl_line(0x113)
	.dwattr $C$DW$T$181, DW_AT_decl_column(0x2c)
	.dwendtag $C$DW$T$181

	.dwendtag $C$DW$TU$181


$C$DW$TU$215	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$215

$C$DW$T$215	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$215, DW_AT_name("__SE_TEMPLATE_v1")
	.dwattr $C$DW$T$215, DW_AT_byte_size(0x40)
$C$DW$478	.dwtag  DW_TAG_member
	.dwattr $C$DW$478, DW_AT_type(*$C$DW$T$123)
	.dwattr $C$DW$478, DW_AT_name("ICNT0")
	.dwattr $C$DW$478, DW_AT_bit_offset(0x20)
	.dwattr $C$DW$478, DW_AT_bit_size(0x20)
	.dwattr $C$DW$478, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$478, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$478, DW_AT_decl_line(0x116)
	.dwattr $C$DW$478, DW_AT_decl_column(0x0e)

$C$DW$479	.dwtag  DW_TAG_member
	.dwattr $C$DW$479, DW_AT_type(*$C$DW$T$123)
	.dwattr $C$DW$479, DW_AT_name("ICNT1")
	.dwattr $C$DW$479, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$479, DW_AT_bit_size(0x20)
	.dwattr $C$DW$479, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$479, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$479, DW_AT_decl_line(0x117)
	.dwattr $C$DW$479, DW_AT_decl_column(0x0e)

$C$DW$480	.dwtag  DW_TAG_member
	.dwattr $C$DW$480, DW_AT_type(*$C$DW$T$123)
	.dwattr $C$DW$480, DW_AT_name("ICNT2")
	.dwattr $C$DW$480, DW_AT_bit_offset(0x20)
	.dwattr $C$DW$480, DW_AT_bit_size(0x20)
	.dwattr $C$DW$480, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$480, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$480, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$480, DW_AT_decl_line(0x118)
	.dwattr $C$DW$480, DW_AT_decl_column(0x0e)

$C$DW$481	.dwtag  DW_TAG_member
	.dwattr $C$DW$481, DW_AT_type(*$C$DW$T$123)
	.dwattr $C$DW$481, DW_AT_name("ICNT3")
	.dwattr $C$DW$481, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$481, DW_AT_bit_size(0x20)
	.dwattr $C$DW$481, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$481, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$481, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$481, DW_AT_decl_line(0x119)
	.dwattr $C$DW$481, DW_AT_decl_column(0x0e)

$C$DW$482	.dwtag  DW_TAG_member
	.dwattr $C$DW$482, DW_AT_type(*$C$DW$T$123)
	.dwattr $C$DW$482, DW_AT_name("ICNT4")
	.dwattr $C$DW$482, DW_AT_bit_offset(0x20)
	.dwattr $C$DW$482, DW_AT_bit_size(0x20)
	.dwattr $C$DW$482, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$482, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$482, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$482, DW_AT_decl_line(0x11a)
	.dwattr $C$DW$482, DW_AT_decl_column(0x0e)

$C$DW$483	.dwtag  DW_TAG_member
	.dwattr $C$DW$483, DW_AT_type(*$C$DW$T$123)
	.dwattr $C$DW$483, DW_AT_name("ICNT5")
	.dwattr $C$DW$483, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$483, DW_AT_bit_size(0x20)
	.dwattr $C$DW$483, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$483, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$483, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$483, DW_AT_decl_line(0x11b)
	.dwattr $C$DW$483, DW_AT_decl_column(0x0e)

$C$DW$484	.dwtag  DW_TAG_member
	.dwattr $C$DW$484, DW_AT_type(*$C$DW$T$123)
	.dwattr $C$DW$484, DW_AT_name("DECDIM1_WIDTH")
	.dwattr $C$DW$484, DW_AT_bit_offset(0x20)
	.dwattr $C$DW$484, DW_AT_bit_size(0x20)
	.dwattr $C$DW$484, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$484, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$484, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$484, DW_AT_decl_line(0x11c)
	.dwattr $C$DW$484, DW_AT_decl_column(0x0e)

$C$DW$485	.dwtag  DW_TAG_member
	.dwattr $C$DW$485, DW_AT_type(*$C$DW$T$123)
	.dwattr $C$DW$485, DW_AT_name("DECDIM2_WIDTH")
	.dwattr $C$DW$485, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$485, DW_AT_bit_size(0x20)
	.dwattr $C$DW$485, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$485, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$485, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$485, DW_AT_decl_line(0x11d)
	.dwattr $C$DW$485, DW_AT_decl_column(0x0e)

$C$DW$486	.dwtag  DW_TAG_member
	.dwattr $C$DW$486, DW_AT_type(*$C$DW$T$125)
	.dwattr $C$DW$486, DW_AT_name("DIM1")
	.dwattr $C$DW$486, DW_AT_bit_offset(0x20)
	.dwattr $C$DW$486, DW_AT_bit_size(0x20)
	.dwattr $C$DW$486, DW_AT_data_member_location[DW_OP_plus_uconst 0x20]
	.dwattr $C$DW$486, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$486, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$486, DW_AT_decl_line(0x11e)
	.dwattr $C$DW$486, DW_AT_decl_column(0x0e)

$C$DW$487	.dwtag  DW_TAG_member
	.dwattr $C$DW$487, DW_AT_type(*$C$DW$T$125)
	.dwattr $C$DW$487, DW_AT_name("DIM2")
	.dwattr $C$DW$487, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$487, DW_AT_bit_size(0x20)
	.dwattr $C$DW$487, DW_AT_data_member_location[DW_OP_plus_uconst 0x20]
	.dwattr $C$DW$487, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$487, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$487, DW_AT_decl_line(0x11f)
	.dwattr $C$DW$487, DW_AT_decl_column(0x0e)

$C$DW$488	.dwtag  DW_TAG_member
	.dwattr $C$DW$488, DW_AT_type(*$C$DW$T$125)
	.dwattr $C$DW$488, DW_AT_name("DIM3")
	.dwattr $C$DW$488, DW_AT_bit_offset(0x20)
	.dwattr $C$DW$488, DW_AT_bit_size(0x20)
	.dwattr $C$DW$488, DW_AT_data_member_location[DW_OP_plus_uconst 0x28]
	.dwattr $C$DW$488, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$488, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$488, DW_AT_decl_line(0x120)
	.dwattr $C$DW$488, DW_AT_decl_column(0x0e)

$C$DW$489	.dwtag  DW_TAG_member
	.dwattr $C$DW$489, DW_AT_type(*$C$DW$T$125)
	.dwattr $C$DW$489, DW_AT_name("DIM4")
	.dwattr $C$DW$489, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$489, DW_AT_bit_size(0x20)
	.dwattr $C$DW$489, DW_AT_data_member_location[DW_OP_plus_uconst 0x28]
	.dwattr $C$DW$489, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$489, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$489, DW_AT_decl_line(0x121)
	.dwattr $C$DW$489, DW_AT_decl_column(0x0e)

$C$DW$490	.dwtag  DW_TAG_member
	.dwattr $C$DW$490, DW_AT_type(*$C$DW$T$125)
	.dwattr $C$DW$490, DW_AT_name("DIM5")
	.dwattr $C$DW$490, DW_AT_bit_offset(0x20)
	.dwattr $C$DW$490, DW_AT_bit_size(0x20)
	.dwattr $C$DW$490, DW_AT_data_member_location[DW_OP_plus_uconst 0x30]
	.dwattr $C$DW$490, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$490, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$490, DW_AT_decl_line(0x122)
	.dwattr $C$DW$490, DW_AT_decl_column(0x0e)

$C$DW$491	.dwtag  DW_TAG_member
	.dwattr $C$DW$491, DW_AT_type(*$C$DW$T$123)
	.dwattr $C$DW$491, DW_AT_name("LEZR_CNT")
	.dwattr $C$DW$491, DW_AT_bit_offset(0x18)
	.dwattr $C$DW$491, DW_AT_bit_size(0x08)
	.dwattr $C$DW$491, DW_AT_data_member_location[DW_OP_plus_uconst 0x30]
	.dwattr $C$DW$491, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$491, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$491, DW_AT_decl_line(0x123)
	.dwattr $C$DW$491, DW_AT_decl_column(0x0e)

$C$DW$492	.dwtag  DW_TAG_member
	.dwattr $C$DW$492, DW_AT_type(*$C$DW$T$123)
	.dwattr $C$DW$492, DW_AT_name("_reserved1")
	.dwattr $C$DW$492, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$492, DW_AT_bit_size(0x18)
	.dwattr $C$DW$492, DW_AT_data_member_location[DW_OP_plus_uconst 0x30]
	.dwattr $C$DW$492, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$492, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$492, DW_AT_decl_line(0x124)
	.dwattr $C$DW$492, DW_AT_decl_column(0x0e)

$C$DW$493	.dwtag  DW_TAG_member
	.dwattr $C$DW$493, DW_AT_type(*$C$DW$T$183)
	.dwattr $C$DW$493, DW_AT_name("ELETYPE")
	.dwattr $C$DW$493, DW_AT_bit_offset(0x04)
	.dwattr $C$DW$493, DW_AT_bit_size(0x04)
	.dwattr $C$DW$493, DW_AT_data_member_location[DW_OP_plus_uconst 0x38]
	.dwattr $C$DW$493, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$493, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$493, DW_AT_decl_line(0x126)
	.dwattr $C$DW$493, DW_AT_decl_column(0x12)

$C$DW$494	.dwtag  DW_TAG_member
	.dwattr $C$DW$494, DW_AT_type(*$C$DW$T$185)
	.dwattr $C$DW$494, DW_AT_name("TRANSPOSE")
	.dwattr $C$DW$494, DW_AT_bit_offset(0x01)
	.dwattr $C$DW$494, DW_AT_bit_size(0x03)
	.dwattr $C$DW$494, DW_AT_data_member_location[DW_OP_plus_uconst 0x38]
	.dwattr $C$DW$494, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$494, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$494, DW_AT_decl_line(0x127)
	.dwattr $C$DW$494, DW_AT_decl_column(0x14)

$C$DW$495	.dwtag  DW_TAG_member
	.dwattr $C$DW$495, DW_AT_type(*$C$DW$T$123)
	.dwattr $C$DW$495, DW_AT_name("_reserved2")
	.dwattr $C$DW$495, DW_AT_bit_offset(0x38)
	.dwattr $C$DW$495, DW_AT_bit_size(0x01)
	.dwattr $C$DW$495, DW_AT_data_member_location[DW_OP_plus_uconst 0x38]
	.dwattr $C$DW$495, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$495, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$495, DW_AT_decl_line(0x128)
	.dwattr $C$DW$495, DW_AT_decl_column(0x0e)

$C$DW$496	.dwtag  DW_TAG_member
	.dwattr $C$DW$496, DW_AT_type(*$C$DW$T$187)
	.dwattr $C$DW$496, DW_AT_name("PROMOTE")
	.dwattr $C$DW$496, DW_AT_bit_offset(0x05)
	.dwattr $C$DW$496, DW_AT_bit_size(0x03)
	.dwattr $C$DW$496, DW_AT_data_member_location[DW_OP_plus_uconst 0x39]
	.dwattr $C$DW$496, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$496, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$496, DW_AT_decl_line(0x129)
	.dwattr $C$DW$496, DW_AT_decl_column(0x12)

$C$DW$497	.dwtag  DW_TAG_member
	.dwattr $C$DW$497, DW_AT_type(*$C$DW$T$123)
	.dwattr $C$DW$497, DW_AT_name("_reserved3")
	.dwattr $C$DW$497, DW_AT_bit_offset(0x34)
	.dwattr $C$DW$497, DW_AT_bit_size(0x01)
	.dwattr $C$DW$497, DW_AT_data_member_location[DW_OP_plus_uconst 0x38]
	.dwattr $C$DW$497, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$497, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$497, DW_AT_decl_line(0x12a)
	.dwattr $C$DW$497, DW_AT_decl_column(0x0e)

$C$DW$498	.dwtag  DW_TAG_member
	.dwattr $C$DW$498, DW_AT_type(*$C$DW$T$189)
	.dwattr $C$DW$498, DW_AT_name("VECLEN")
	.dwattr $C$DW$498, DW_AT_bit_offset(0x01)
	.dwattr $C$DW$498, DW_AT_bit_size(0x03)
	.dwattr $C$DW$498, DW_AT_data_member_location[DW_OP_plus_uconst 0x39]
	.dwattr $C$DW$498, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$498, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$498, DW_AT_decl_line(0x12b)
	.dwattr $C$DW$498, DW_AT_decl_column(0x11)

$C$DW$499	.dwtag  DW_TAG_member
	.dwattr $C$DW$499, DW_AT_type(*$C$DW$T$123)
	.dwattr $C$DW$499, DW_AT_name("_reserved4")
	.dwattr $C$DW$499, DW_AT_bit_offset(0x30)
	.dwattr $C$DW$499, DW_AT_bit_size(0x01)
	.dwattr $C$DW$499, DW_AT_data_member_location[DW_OP_plus_uconst 0x38]
	.dwattr $C$DW$499, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$499, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$499, DW_AT_decl_line(0x12c)
	.dwattr $C$DW$499, DW_AT_decl_column(0x0e)

$C$DW$500	.dwtag  DW_TAG_member
	.dwattr $C$DW$500, DW_AT_type(*$C$DW$T$191)
	.dwattr $C$DW$500, DW_AT_name("ELEDUP")
	.dwattr $C$DW$500, DW_AT_bit_offset(0x05)
	.dwattr $C$DW$500, DW_AT_bit_size(0x03)
	.dwattr $C$DW$500, DW_AT_data_member_location[DW_OP_plus_uconst 0x3a]
	.dwattr $C$DW$500, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$500, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$500, DW_AT_decl_line(0x12d)
	.dwattr $C$DW$500, DW_AT_decl_column(0x11)

$C$DW$501	.dwtag  DW_TAG_member
	.dwattr $C$DW$501, DW_AT_type(*$C$DW$T$193)
	.dwattr $C$DW$501, DW_AT_name("GRPDUP")
	.dwattr $C$DW$501, DW_AT_bit_offset(0x04)
	.dwattr $C$DW$501, DW_AT_bit_size(0x01)
	.dwattr $C$DW$501, DW_AT_data_member_location[DW_OP_plus_uconst 0x3a]
	.dwattr $C$DW$501, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$501, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$501, DW_AT_decl_line(0x12e)
	.dwattr $C$DW$501, DW_AT_decl_column(0x11)

$C$DW$502	.dwtag  DW_TAG_member
	.dwattr $C$DW$502, DW_AT_type(*$C$DW$T$195)
	.dwattr $C$DW$502, DW_AT_name("DECIM")
	.dwattr $C$DW$502, DW_AT_bit_offset(0x02)
	.dwattr $C$DW$502, DW_AT_bit_size(0x02)
	.dwattr $C$DW$502, DW_AT_data_member_location[DW_OP_plus_uconst 0x3a]
	.dwattr $C$DW$502, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$502, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$502, DW_AT_decl_line(0x12f)
	.dwattr $C$DW$502, DW_AT_decl_column(0x10)

$C$DW$503	.dwtag  DW_TAG_member
	.dwattr $C$DW$503, DW_AT_type(*$C$DW$T$123)
	.dwattr $C$DW$503, DW_AT_name("_reserved5")
	.dwattr $C$DW$503, DW_AT_bit_offset(0x28)
	.dwattr $C$DW$503, DW_AT_bit_size(0x02)
	.dwattr $C$DW$503, DW_AT_data_member_location[DW_OP_plus_uconst 0x38]
	.dwattr $C$DW$503, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$503, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$503, DW_AT_decl_line(0x130)
	.dwattr $C$DW$503, DW_AT_decl_column(0x0e)

$C$DW$504	.dwtag  DW_TAG_member
	.dwattr $C$DW$504, DW_AT_type(*$C$DW$T$197)
	.dwattr $C$DW$504, DW_AT_name("DIMFMT")
	.dwattr $C$DW$504, DW_AT_bit_offset(0x05)
	.dwattr $C$DW$504, DW_AT_bit_size(0x03)
	.dwattr $C$DW$504, DW_AT_data_member_location[DW_OP_plus_uconst 0x3b]
	.dwattr $C$DW$504, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$504, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$504, DW_AT_decl_line(0x131)
	.dwattr $C$DW$504, DW_AT_decl_column(0x11)

$C$DW$505	.dwtag  DW_TAG_member
	.dwattr $C$DW$505, DW_AT_type(*$C$DW$T$199)
	.dwattr $C$DW$505, DW_AT_name("DIR")
	.dwattr $C$DW$505, DW_AT_bit_offset(0x04)
	.dwattr $C$DW$505, DW_AT_bit_size(0x01)
	.dwattr $C$DW$505, DW_AT_data_member_location[DW_OP_plus_uconst 0x3b]
	.dwattr $C$DW$505, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$505, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$505, DW_AT_decl_line(0x132)
	.dwattr $C$DW$505, DW_AT_decl_column(0x0e)

$C$DW$506	.dwtag  DW_TAG_member
	.dwattr $C$DW$506, DW_AT_type(*$C$DW$T$123)
	.dwattr $C$DW$506, DW_AT_name("CBK0")
	.dwattr $C$DW$506, DW_AT_bit_offset(0x20)
	.dwattr $C$DW$506, DW_AT_bit_size(0x04)
	.dwattr $C$DW$506, DW_AT_data_member_location[DW_OP_plus_uconst 0x38]
	.dwattr $C$DW$506, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$506, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$506, DW_AT_decl_line(0x133)
	.dwattr $C$DW$506, DW_AT_decl_column(0x0e)

$C$DW$507	.dwtag  DW_TAG_member
	.dwattr $C$DW$507, DW_AT_type(*$C$DW$T$123)
	.dwattr $C$DW$507, DW_AT_name("CBK1")
	.dwattr $C$DW$507, DW_AT_bit_offset(0x1c)
	.dwattr $C$DW$507, DW_AT_bit_size(0x04)
	.dwattr $C$DW$507, DW_AT_data_member_location[DW_OP_plus_uconst 0x38]
	.dwattr $C$DW$507, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$507, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$507, DW_AT_decl_line(0x134)
	.dwattr $C$DW$507, DW_AT_decl_column(0x0e)

$C$DW$508	.dwtag  DW_TAG_member
	.dwattr $C$DW$508, DW_AT_type(*$C$DW$T$201)
	.dwattr $C$DW$508, DW_AT_name("AM0")
	.dwattr $C$DW$508, DW_AT_bit_offset(0x02)
	.dwattr $C$DW$508, DW_AT_bit_size(0x02)
	.dwattr $C$DW$508, DW_AT_data_member_location[DW_OP_plus_uconst 0x3c]
	.dwattr $C$DW$508, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$508, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$508, DW_AT_decl_line(0x135)
	.dwattr $C$DW$508, DW_AT_decl_column(0x0d)

$C$DW$509	.dwtag  DW_TAG_member
	.dwattr $C$DW$509, DW_AT_type(*$C$DW$T$201)
	.dwattr $C$DW$509, DW_AT_name("AM1")
	.dwattr $C$DW$509, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$509, DW_AT_bit_size(0x02)
	.dwattr $C$DW$509, DW_AT_data_member_location[DW_OP_plus_uconst 0x3c]
	.dwattr $C$DW$509, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$509, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$509, DW_AT_decl_line(0x136)
	.dwattr $C$DW$509, DW_AT_decl_column(0x0d)

$C$DW$510	.dwtag  DW_TAG_member
	.dwattr $C$DW$510, DW_AT_type(*$C$DW$T$201)
	.dwattr $C$DW$510, DW_AT_name("AM2")
	.dwattr $C$DW$510, DW_AT_bit_offset(0x06)
	.dwattr $C$DW$510, DW_AT_bit_size(0x02)
	.dwattr $C$DW$510, DW_AT_data_member_location[DW_OP_plus_uconst 0x3d]
	.dwattr $C$DW$510, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$510, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$510, DW_AT_decl_line(0x137)
	.dwattr $C$DW$510, DW_AT_decl_column(0x0d)

$C$DW$511	.dwtag  DW_TAG_member
	.dwattr $C$DW$511, DW_AT_type(*$C$DW$T$201)
	.dwattr $C$DW$511, DW_AT_name("AM3")
	.dwattr $C$DW$511, DW_AT_bit_offset(0x04)
	.dwattr $C$DW$511, DW_AT_bit_size(0x02)
	.dwattr $C$DW$511, DW_AT_data_member_location[DW_OP_plus_uconst 0x3d]
	.dwattr $C$DW$511, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$511, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$511, DW_AT_decl_line(0x138)
	.dwattr $C$DW$511, DW_AT_decl_column(0x0d)

$C$DW$512	.dwtag  DW_TAG_member
	.dwattr $C$DW$512, DW_AT_type(*$C$DW$T$201)
	.dwattr $C$DW$512, DW_AT_name("AM4")
	.dwattr $C$DW$512, DW_AT_bit_offset(0x02)
	.dwattr $C$DW$512, DW_AT_bit_size(0x02)
	.dwattr $C$DW$512, DW_AT_data_member_location[DW_OP_plus_uconst 0x3d]
	.dwattr $C$DW$512, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$512, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$512, DW_AT_decl_line(0x139)
	.dwattr $C$DW$512, DW_AT_decl_column(0x0d)

$C$DW$513	.dwtag  DW_TAG_member
	.dwattr $C$DW$513, DW_AT_type(*$C$DW$T$201)
	.dwattr $C$DW$513, DW_AT_name("AM5")
	.dwattr $C$DW$513, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$513, DW_AT_bit_size(0x02)
	.dwattr $C$DW$513, DW_AT_data_member_location[DW_OP_plus_uconst 0x3d]
	.dwattr $C$DW$513, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$513, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$513, DW_AT_decl_line(0x13a)
	.dwattr $C$DW$513, DW_AT_decl_column(0x0d)

$C$DW$514	.dwtag  DW_TAG_member
	.dwattr $C$DW$514, DW_AT_type(*$C$DW$T$203)
	.dwattr $C$DW$514, DW_AT_name("DECDIM1")
	.dwattr $C$DW$514, DW_AT_bit_offset(0x05)
	.dwattr $C$DW$514, DW_AT_bit_size(0x03)
	.dwattr $C$DW$514, DW_AT_data_member_location[DW_OP_plus_uconst 0x3e]
	.dwattr $C$DW$514, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$514, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$514, DW_AT_decl_line(0x13b)
	.dwattr $C$DW$514, DW_AT_decl_column(0x11)

$C$DW$515	.dwtag  DW_TAG_member
	.dwattr $C$DW$515, DW_AT_type(*$C$DW$T$205)
	.dwattr $C$DW$515, DW_AT_name("DECDIM1SD")
	.dwattr $C$DW$515, DW_AT_bit_offset(0x03)
	.dwattr $C$DW$515, DW_AT_bit_size(0x02)
	.dwattr $C$DW$515, DW_AT_data_member_location[DW_OP_plus_uconst 0x3e]
	.dwattr $C$DW$515, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$515, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$515, DW_AT_decl_line(0x13c)
	.dwattr $C$DW$515, DW_AT_decl_column(0x13)

$C$DW$516	.dwtag  DW_TAG_member
	.dwattr $C$DW$516, DW_AT_type(*$C$DW$T$203)
	.dwattr $C$DW$516, DW_AT_name("DECDIM2")
	.dwattr $C$DW$516, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$516, DW_AT_bit_size(0x03)
	.dwattr $C$DW$516, DW_AT_data_member_location[DW_OP_plus_uconst 0x3e]
	.dwattr $C$DW$516, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$516, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$516, DW_AT_decl_line(0x13d)
	.dwattr $C$DW$516, DW_AT_decl_column(0x11)

$C$DW$517	.dwtag  DW_TAG_member
	.dwattr $C$DW$517, DW_AT_type(*$C$DW$T$205)
	.dwattr $C$DW$517, DW_AT_name("DECDIM2SD")
	.dwattr $C$DW$517, DW_AT_bit_offset(0x06)
	.dwattr $C$DW$517, DW_AT_bit_size(0x02)
	.dwattr $C$DW$517, DW_AT_data_member_location[DW_OP_plus_uconst 0x3f]
	.dwattr $C$DW$517, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$517, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$517, DW_AT_decl_line(0x13e)
	.dwattr $C$DW$517, DW_AT_decl_column(0x13)

$C$DW$518	.dwtag  DW_TAG_member
	.dwattr $C$DW$518, DW_AT_type(*$C$DW$T$207)
	.dwattr $C$DW$518, DW_AT_name("LEZR")
	.dwattr $C$DW$518, DW_AT_bit_offset(0x03)
	.dwattr $C$DW$518, DW_AT_bit_size(0x03)
	.dwattr $C$DW$518, DW_AT_data_member_location[DW_OP_plus_uconst 0x3f]
	.dwattr $C$DW$518, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$518, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$518, DW_AT_decl_line(0x13f)
	.dwattr $C$DW$518, DW_AT_decl_column(0x0f)

$C$DW$519	.dwtag  DW_TAG_member
	.dwattr $C$DW$519, DW_AT_type(*$C$DW$T$209)
	.dwattr $C$DW$519, DW_AT_name("TEMPLATE_FMT")
	.dwattr $C$DW$519, DW_AT_bit_offset(0x00)
	.dwattr $C$DW$519, DW_AT_bit_size(0x03)
	.dwattr $C$DW$519, DW_AT_data_member_location[DW_OP_plus_uconst 0x3f]
	.dwattr $C$DW$519, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$519, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$519, DW_AT_decl_line(0x140)
	.dwattr $C$DW$519, DW_AT_decl_column(0x17)


$C$DW$520	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$520, DW_AT_name("operator =")
	.dwattr $C$DW$520, DW_AT_declaration
	.dwattr $C$DW$520, DW_AT_linkage_name("_ZN16__SE_TEMPLATE_v1aSERKS_")
	.dwattr $C$DW$520, DW_AT_type(*$C$DW$T$210)
	.dwattr $C$DW$520, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$521	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$521, DW_AT_type(*$C$DW$T$212)

	.dwendtag $C$DW$520


$C$DW$522	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$522, DW_AT_name("operator =")
	.dwattr $C$DW$522, DW_AT_declaration
	.dwattr $C$DW$522, DW_AT_linkage_name("_ZN16__SE_TEMPLATE_v1aSEOS_")
	.dwattr $C$DW$522, DW_AT_type(*$C$DW$T$210)
	.dwattr $C$DW$522, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$523	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$523, DW_AT_type(*$C$DW$T$210)

	.dwendtag $C$DW$522

	.dwattr $C$DW$T$215, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$T$215, DW_AT_decl_line(0x113)
	.dwattr $C$DW$T$215, DW_AT_decl_column(0x2c)
	.dwendtag $C$DW$T$215

	.dwendtag $C$DW$TU$215


$C$DW$TU$211	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$211
$C$DW$T$211	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$211, DW_AT_type(*$C$DW$T$215)

	.dwendtag $C$DW$TU$211


$C$DW$TU$212	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$212
$C$DW$T$212	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$212, DW_AT_type(*$C$DW$T$211)
	.dwattr $C$DW$T$212, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$212


$C$DW$TU$274	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$274
$C$DW$T$274	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$274, DW_AT_name("__SE_TEMPLATE_v1")
	.dwattr $C$DW$T$274, DW_AT_type(*$C$DW$T$215)
	.dwattr $C$DW$T$274, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$T$274, DW_AT_decl_line(0x176)
	.dwattr $C$DW$T$274, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$274


$C$DW$TU$275	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$275
$C$DW$T$275	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$275, DW_AT_type(*$C$DW$T$274)

	.dwendtag $C$DW$TU$275


$C$DW$TU$210	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$210
$C$DW$T$210	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$210, DW_AT_type(*$C$DW$T$215)
	.dwattr $C$DW$T$210, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$210


$C$DW$TU$213	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$213

$C$DW$T$213	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$213, DW_AT_type(*$C$DW$T$210)
$C$DW$524	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$524, DW_AT_type(*$C$DW$T$212)

	.dwendtag $C$DW$T$213

	.dwendtag $C$DW$TU$213


$C$DW$TU$214	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$214

$C$DW$T$214	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$214, DW_AT_type(*$C$DW$T$210)
$C$DW$525	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$525, DW_AT_type(*$C$DW$T$210)

	.dwendtag $C$DW$T$214

	.dwendtag $C$DW$TU$214


$C$DW$TU$155	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$155

$C$DW$T$155	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$155, DW_AT_name("__SE_TRANSPOSE")
	.dwattr $C$DW$T$155, DW_AT_byte_size(0x01)
$C$DW$526	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$526, DW_AT_name("__SE_TRANSPOSE_OFF")
	.dwattr $C$DW$526, DW_AT_const_value(0x00)
	.dwattr $C$DW$526, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$526, DW_AT_decl_line(0x51)
	.dwattr $C$DW$526, DW_AT_decl_column(0x05)

$C$DW$527	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$527, DW_AT_name("__SE_TRANSPOSE_8BIT")
	.dwattr $C$DW$527, DW_AT_const_value(0x01)
	.dwattr $C$DW$527, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$527, DW_AT_decl_line(0x52)
	.dwattr $C$DW$527, DW_AT_decl_column(0x05)

$C$DW$528	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$528, DW_AT_name("__SE_TRANSPOSE_16BIT")
	.dwattr $C$DW$528, DW_AT_const_value(0x02)
	.dwattr $C$DW$528, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$528, DW_AT_decl_line(0x53)
	.dwattr $C$DW$528, DW_AT_decl_column(0x05)

$C$DW$529	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$529, DW_AT_name("__SE_TRANSPOSE_32BIT")
	.dwattr $C$DW$529, DW_AT_const_value(0x03)
	.dwattr $C$DW$529, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$529, DW_AT_decl_line(0x54)
	.dwattr $C$DW$529, DW_AT_decl_column(0x05)

$C$DW$530	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$530, DW_AT_name("__SE_TRANSPOSE_64BIT")
	.dwattr $C$DW$530, DW_AT_const_value(0x04)
	.dwattr $C$DW$530, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$530, DW_AT_decl_line(0x55)
	.dwattr $C$DW$530, DW_AT_decl_column(0x05)

$C$DW$531	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$531, DW_AT_name("__SE_TRANSPOSE_128BIT")
	.dwattr $C$DW$531, DW_AT_const_value(0x05)
	.dwattr $C$DW$531, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$531, DW_AT_decl_line(0x56)
	.dwattr $C$DW$531, DW_AT_decl_column(0x05)

$C$DW$532	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$532, DW_AT_name("__SE_TRANSPOSE_256BIT")
	.dwattr $C$DW$532, DW_AT_const_value(0x06)
	.dwattr $C$DW$532, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$532, DW_AT_decl_line(0x57)
	.dwattr $C$DW$532, DW_AT_decl_column(0x05)

	.dwattr $C$DW$T$155, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$T$155, DW_AT_decl_line(0x4f)
	.dwattr $C$DW$T$155, DW_AT_decl_column(0x2a)
	.dwendtag $C$DW$T$155

	.dwendtag $C$DW$TU$155


$C$DW$TU$156	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$156
$C$DW$T$156	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$156, DW_AT_name("__SE_TRANSPOSE")
	.dwattr $C$DW$T$156, DW_AT_type(*$C$DW$T$155)
	.dwattr $C$DW$T$156, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$T$156, DW_AT_decl_line(0x58)
	.dwattr $C$DW$T$156, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$156


$C$DW$TU$184	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$184

$C$DW$T$184	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$184, DW_AT_name("__SE_TRANSPOSE")
	.dwattr $C$DW$T$184, DW_AT_byte_size(0x01)
$C$DW$533	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$533, DW_AT_name("__SE_TRANSPOSE_OFF")
	.dwattr $C$DW$533, DW_AT_const_value(0x00)
	.dwattr $C$DW$533, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$533, DW_AT_decl_line(0x51)
	.dwattr $C$DW$533, DW_AT_decl_column(0x05)

$C$DW$534	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$534, DW_AT_name("__SE_TRANSPOSE_8BIT")
	.dwattr $C$DW$534, DW_AT_const_value(0x01)
	.dwattr $C$DW$534, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$534, DW_AT_decl_line(0x52)
	.dwattr $C$DW$534, DW_AT_decl_column(0x05)

$C$DW$535	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$535, DW_AT_name("__SE_TRANSPOSE_16BIT")
	.dwattr $C$DW$535, DW_AT_const_value(0x02)
	.dwattr $C$DW$535, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$535, DW_AT_decl_line(0x53)
	.dwattr $C$DW$535, DW_AT_decl_column(0x05)

$C$DW$536	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$536, DW_AT_name("__SE_TRANSPOSE_32BIT")
	.dwattr $C$DW$536, DW_AT_const_value(0x03)
	.dwattr $C$DW$536, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$536, DW_AT_decl_line(0x54)
	.dwattr $C$DW$536, DW_AT_decl_column(0x05)

$C$DW$537	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$537, DW_AT_name("__SE_TRANSPOSE_64BIT")
	.dwattr $C$DW$537, DW_AT_const_value(0x04)
	.dwattr $C$DW$537, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$537, DW_AT_decl_line(0x55)
	.dwattr $C$DW$537, DW_AT_decl_column(0x05)

$C$DW$538	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$538, DW_AT_name("__SE_TRANSPOSE_128BIT")
	.dwattr $C$DW$538, DW_AT_const_value(0x05)
	.dwattr $C$DW$538, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$538, DW_AT_decl_line(0x56)
	.dwattr $C$DW$538, DW_AT_decl_column(0x05)

$C$DW$539	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$539, DW_AT_name("__SE_TRANSPOSE_256BIT")
	.dwattr $C$DW$539, DW_AT_const_value(0x06)
	.dwattr $C$DW$539, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$539, DW_AT_decl_line(0x57)
	.dwattr $C$DW$539, DW_AT_decl_column(0x05)

	.dwattr $C$DW$T$184, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$T$184, DW_AT_decl_line(0x4f)
	.dwattr $C$DW$T$184, DW_AT_decl_column(0x2a)
	.dwendtag $C$DW$T$184

	.dwendtag $C$DW$TU$184


$C$DW$TU$185	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$185
$C$DW$T$185	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$185, DW_AT_name("__SE_TRANSPOSE")
	.dwattr $C$DW$T$185, DW_AT_type(*$C$DW$T$184)
	.dwattr $C$DW$T$185, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$T$185, DW_AT_decl_line(0x58)
	.dwattr $C$DW$T$185, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$185


$C$DW$TU$159	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$159

$C$DW$T$159	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$159, DW_AT_name("__SE_VECLEN")
	.dwattr $C$DW$T$159, DW_AT_byte_size(0x01)
$C$DW$540	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$540, DW_AT_name("__SE_VECLEN_1ELEM")
	.dwattr $C$DW$540, DW_AT_const_value(0x00)
	.dwattr $C$DW$540, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$540, DW_AT_decl_line(0x76)
	.dwattr $C$DW$540, DW_AT_decl_column(0x05)

$C$DW$541	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$541, DW_AT_name("__SE_VECLEN_2ELEMS")
	.dwattr $C$DW$541, DW_AT_const_value(0x01)
	.dwattr $C$DW$541, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$541, DW_AT_decl_line(0x77)
	.dwattr $C$DW$541, DW_AT_decl_column(0x05)

$C$DW$542	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$542, DW_AT_name("__SE_VECLEN_4ELEMS")
	.dwattr $C$DW$542, DW_AT_const_value(0x02)
	.dwattr $C$DW$542, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$542, DW_AT_decl_line(0x78)
	.dwattr $C$DW$542, DW_AT_decl_column(0x05)

$C$DW$543	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$543, DW_AT_name("__SE_VECLEN_8ELEMS")
	.dwattr $C$DW$543, DW_AT_const_value(0x03)
	.dwattr $C$DW$543, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$543, DW_AT_decl_line(0x79)
	.dwattr $C$DW$543, DW_AT_decl_column(0x05)

$C$DW$544	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$544, DW_AT_name("__SE_VECLEN_16ELEMS")
	.dwattr $C$DW$544, DW_AT_const_value(0x04)
	.dwattr $C$DW$544, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$544, DW_AT_decl_line(0x7a)
	.dwattr $C$DW$544, DW_AT_decl_column(0x05)

$C$DW$545	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$545, DW_AT_name("__SE_VECLEN_32ELEMS")
	.dwattr $C$DW$545, DW_AT_const_value(0x05)
	.dwattr $C$DW$545, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$545, DW_AT_decl_line(0x7b)
	.dwattr $C$DW$545, DW_AT_decl_column(0x05)

$C$DW$546	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$546, DW_AT_name("__SE_VECLEN_64ELEMS")
	.dwattr $C$DW$546, DW_AT_const_value(0x06)
	.dwattr $C$DW$546, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$546, DW_AT_decl_line(0x7c)
	.dwattr $C$DW$546, DW_AT_decl_column(0x05)

	.dwattr $C$DW$T$159, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$T$159, DW_AT_decl_line(0x74)
	.dwattr $C$DW$T$159, DW_AT_decl_column(0x2a)
	.dwendtag $C$DW$T$159

	.dwendtag $C$DW$TU$159


$C$DW$TU$160	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$160
$C$DW$T$160	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$160, DW_AT_name("__SE_VECLEN")
	.dwattr $C$DW$T$160, DW_AT_type(*$C$DW$T$159)
	.dwattr $C$DW$T$160, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$T$160, DW_AT_decl_line(0x7d)
	.dwattr $C$DW$T$160, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$160


$C$DW$TU$188	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$188

$C$DW$T$188	.dwtag  DW_TAG_enumeration_type
	.dwattr $C$DW$T$188, DW_AT_name("__SE_VECLEN")
	.dwattr $C$DW$T$188, DW_AT_byte_size(0x01)
$C$DW$547	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$547, DW_AT_name("__SE_VECLEN_1ELEM")
	.dwattr $C$DW$547, DW_AT_const_value(0x00)
	.dwattr $C$DW$547, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$547, DW_AT_decl_line(0x76)
	.dwattr $C$DW$547, DW_AT_decl_column(0x05)

$C$DW$548	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$548, DW_AT_name("__SE_VECLEN_2ELEMS")
	.dwattr $C$DW$548, DW_AT_const_value(0x01)
	.dwattr $C$DW$548, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$548, DW_AT_decl_line(0x77)
	.dwattr $C$DW$548, DW_AT_decl_column(0x05)

$C$DW$549	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$549, DW_AT_name("__SE_VECLEN_4ELEMS")
	.dwattr $C$DW$549, DW_AT_const_value(0x02)
	.dwattr $C$DW$549, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$549, DW_AT_decl_line(0x78)
	.dwattr $C$DW$549, DW_AT_decl_column(0x05)

$C$DW$550	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$550, DW_AT_name("__SE_VECLEN_8ELEMS")
	.dwattr $C$DW$550, DW_AT_const_value(0x03)
	.dwattr $C$DW$550, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$550, DW_AT_decl_line(0x79)
	.dwattr $C$DW$550, DW_AT_decl_column(0x05)

$C$DW$551	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$551, DW_AT_name("__SE_VECLEN_16ELEMS")
	.dwattr $C$DW$551, DW_AT_const_value(0x04)
	.dwattr $C$DW$551, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$551, DW_AT_decl_line(0x7a)
	.dwattr $C$DW$551, DW_AT_decl_column(0x05)

$C$DW$552	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$552, DW_AT_name("__SE_VECLEN_32ELEMS")
	.dwattr $C$DW$552, DW_AT_const_value(0x05)
	.dwattr $C$DW$552, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$552, DW_AT_decl_line(0x7b)
	.dwattr $C$DW$552, DW_AT_decl_column(0x05)

$C$DW$553	.dwtag  DW_TAG_enumerator
	.dwattr $C$DW$553, DW_AT_name("__SE_VECLEN_64ELEMS")
	.dwattr $C$DW$553, DW_AT_const_value(0x06)
	.dwattr $C$DW$553, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$553, DW_AT_decl_line(0x7c)
	.dwattr $C$DW$553, DW_AT_decl_column(0x05)

	.dwattr $C$DW$T$188, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$T$188, DW_AT_decl_line(0x74)
	.dwattr $C$DW$T$188, DW_AT_decl_column(0x2a)
	.dwendtag $C$DW$T$188

	.dwendtag $C$DW$TU$188


$C$DW$TU$189	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$189
$C$DW$T$189	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$189, DW_AT_name("__SE_VECLEN")
	.dwattr $C$DW$T$189, DW_AT_type(*$C$DW$T$188)
	.dwattr $C$DW$T$189, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_strm.h")
	.dwattr $C$DW$T$189, DW_AT_decl_line(0x7d)
	.dwattr $C$DW$T$189, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$189


$C$DW$TU$2	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$2
$C$DW$T$2	.dwtag  DW_TAG_unspecified_type
	.dwattr $C$DW$T$2, DW_AT_name("void")

	.dwendtag $C$DW$TU$2


$C$DW$TU$3	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$3
$C$DW$T$3	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$3, DW_AT_type(*$C$DW$T$2)
	.dwattr $C$DW$T$3, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$3


$C$DW$TU$82	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$82
$C$DW$T$82	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$82, DW_AT_type(*$C$DW$T$2)

	.dwendtag $C$DW$TU$82


$C$DW$TU$83	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$83
$C$DW$T$83	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$83, DW_AT_type(*$C$DW$T$82)
	.dwattr $C$DW$T$83, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$83


$C$DW$TU$4	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$4
$C$DW$T$4	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$4, DW_AT_encoding(DW_ATE_boolean)
	.dwattr $C$DW$T$4, DW_AT_name("bool")
	.dwattr $C$DW$T$4, DW_AT_byte_size(0x01)

	.dwendtag $C$DW$TU$4


$C$DW$TU$5	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$5
$C$DW$T$5	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$5, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$5, DW_AT_name("signed char")
	.dwattr $C$DW$T$5, DW_AT_byte_size(0x01)

	.dwendtag $C$DW$TU$5


$C$DW$TU$287	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$287
$C$DW$T$287	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$287, DW_AT_name("__int8_t")
	.dwattr $C$DW$T$287, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$T$287, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/machine/_types.h")
	.dwattr $C$DW$T$287, DW_AT_decl_line(0x60)
	.dwattr $C$DW$T$287, DW_AT_decl_column(0x16)

	.dwendtag $C$DW$TU$287


$C$DW$TU$288	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$288
$C$DW$T$288	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$288, DW_AT_name("int8_t")
	.dwattr $C$DW$T$288, DW_AT_type(*$C$DW$T$287)
	.dwattr $C$DW$T$288, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/sys/_stdint.h")
	.dwattr $C$DW$T$288, DW_AT_decl_line(0x25)
	.dwattr $C$DW$T$288, DW_AT_decl_column(0x13)

	.dwendtag $C$DW$TU$288


$C$DW$TU$289	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$289
$C$DW$T$289	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$289, DW_AT_type(*$C$DW$T$288)
	.dwattr $C$DW$T$289, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$289


$C$DW$TU$290	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$290
$C$DW$T$290	.dwtag  DW_TAG_restrict_type
	.dwattr $C$DW$T$290, DW_AT_type(*$C$DW$T$289)

	.dwendtag $C$DW$TU$290


$C$DW$TU$291	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$291
$C$DW$T$291	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$291, DW_AT_type(*$C$DW$T$288)

	.dwendtag $C$DW$TU$291


$C$DW$TU$292	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$292
$C$DW$T$292	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$292, DW_AT_type(*$C$DW$T$291)
	.dwattr $C$DW$T$292, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$292


$C$DW$TU$293	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$293
$C$DW$T$293	.dwtag  DW_TAG_restrict_type
	.dwattr $C$DW$T$293, DW_AT_type(*$C$DW$T$292)

	.dwendtag $C$DW$TU$293


$C$DW$TU$6	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$6
$C$DW$T$6	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$6, DW_AT_encoding(DW_ATE_unsigned_char)
	.dwattr $C$DW$T$6, DW_AT_name("unsigned char")
	.dwattr $C$DW$T$6, DW_AT_byte_size(0x01)

	.dwendtag $C$DW$TU$6


$C$DW$TU$301	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$301

$C$DW$T$301	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$301, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$301, DW_AT_GNU_vector
	.dwattr $C$DW$T$301, DW_AT_byte_size(0x20)
$C$DW$554	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$554, DW_AT_upper_bound(0x1f)

	.dwendtag $C$DW$T$301

	.dwendtag $C$DW$TU$301


$C$DW$TU$303	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$303
$C$DW$T$303	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$303, DW_AT_name("uchar32")
	.dwattr $C$DW$T$303, DW_AT_type(*$C$DW$T$301)
	.dwattr $C$DW$T$303, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$T$303, DW_AT_decl_line(0x73)
	.dwattr $C$DW$T$303, DW_AT_decl_column(0x05)

	.dwendtag $C$DW$TU$303


$C$DW$TU$304	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$304

$C$DW$T$304	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$304, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$304, DW_AT_GNU_vector
	.dwattr $C$DW$T$304, DW_AT_byte_size(0x40)
$C$DW$555	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$555, DW_AT_upper_bound(0x3f)

	.dwendtag $C$DW$T$304

	.dwendtag $C$DW$TU$304


$C$DW$TU$305	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$305
$C$DW$T$305	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$305, DW_AT_name("__uchar64")
	.dwattr $C$DW$T$305, DW_AT_type(*$C$DW$T$304)
	.dwattr $C$DW$T$305, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$T$305, DW_AT_decl_line(0x73)
	.dwattr $C$DW$T$305, DW_AT_decl_column(0x05)

	.dwendtag $C$DW$TU$305


$C$DW$TU$307	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$307
$C$DW$T$307	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$307, DW_AT_name("uchar64")
	.dwattr $C$DW$T$307, DW_AT_type(*$C$DW$T$304)
	.dwattr $C$DW$T$307, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$T$307, DW_AT_decl_line(0x73)
	.dwattr $C$DW$T$307, DW_AT_decl_column(0x05)

	.dwendtag $C$DW$TU$307


$C$DW$TU$7	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$7
$C$DW$T$7	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$7, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$7, DW_AT_name("wchar_t")
	.dwattr $C$DW$T$7, DW_AT_byte_size(0x04)

	.dwendtag $C$DW$TU$7


$C$DW$TU$8	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$8
$C$DW$T$8	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$8, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$8, DW_AT_name("short")
	.dwattr $C$DW$T$8, DW_AT_byte_size(0x02)

	.dwendtag $C$DW$TU$8


$C$DW$TU$9	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$9
$C$DW$T$9	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$9, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$9, DW_AT_name("unsigned short")
	.dwattr $C$DW$T$9, DW_AT_byte_size(0x02)

	.dwendtag $C$DW$TU$9


$C$DW$TU$10	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$10
$C$DW$T$10	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$10, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$10, DW_AT_name("int")
	.dwattr $C$DW$T$10, DW_AT_byte_size(0x04)

	.dwendtag $C$DW$TU$10


$C$DW$TU$68	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$68
$C$DW$T$68	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$68, DW_AT_name("__int32_t")
	.dwattr $C$DW$T$68, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$T$68, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/machine/_types.h")
	.dwattr $C$DW$T$68, DW_AT_decl_line(0x64)
	.dwattr $C$DW$T$68, DW_AT_decl_column(0x0f)

	.dwendtag $C$DW$TU$68


$C$DW$TU$69	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$69
$C$DW$T$69	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$69, DW_AT_name("int32_t")
	.dwattr $C$DW$T$69, DW_AT_type(*$C$DW$T$68)
	.dwattr $C$DW$T$69, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/sys/_stdint.h")
	.dwattr $C$DW$T$69, DW_AT_decl_line(0x30)
	.dwattr $C$DW$T$69, DW_AT_decl_column(0x14)

	.dwendtag $C$DW$TU$69


$C$DW$TU$84	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$84

$C$DW$T$84	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$84, DW_AT_type(*$C$DW$T$69)
$C$DW$556	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$556, DW_AT_type(*$C$DW$T$3)

$C$DW$557	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$557, DW_AT_type(*$C$DW$T$83)

$C$DW$558	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$558, DW_AT_type(*$C$DW$T$3)

	.dwendtag $C$DW$T$84

	.dwendtag $C$DW$TU$84


$C$DW$TU$85	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$85
$C$DW$T$85	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$85, DW_AT_type(*$C$DW$T$84)
	.dwattr $C$DW$T$85, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$85


$C$DW$TU$86	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$86
$C$DW$T$86	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$86, DW_AT_name("TIDL_customMaxPoolExecFunc")
	.dwattr $C$DW$T$86, DW_AT_type(*$C$DW$T$85)
	.dwattr $C$DW$T$86, DW_AT_decl_file("./tidsp/tidl_custom_maxpool_ixX_oxX_priv.h")
	.dwattr $C$DW$T$86, DW_AT_decl_line(0x35)
	.dwattr $C$DW$T$86, DW_AT_decl_column(0x13)

	.dwendtag $C$DW$TU$86


$C$DW$TU$11	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$11
$C$DW$T$11	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$11, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$11, DW_AT_name("unsigned int")
	.dwattr $C$DW$T$11, DW_AT_byte_size(0x04)

	.dwendtag $C$DW$TU$11


$C$DW$TU$35	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$35
$C$DW$T$35	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$35, DW_AT_name("__uint32_t")
	.dwattr $C$DW$T$35, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$T$35, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/machine/_types.h")
	.dwattr $C$DW$T$35, DW_AT_decl_line(0x65)
	.dwattr $C$DW$T$35, DW_AT_decl_column(0x17)

	.dwendtag $C$DW$TU$35


$C$DW$TU$36	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$36
$C$DW$T$36	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$36, DW_AT_name("uint32_t")
	.dwattr $C$DW$T$36, DW_AT_type(*$C$DW$T$35)
	.dwattr $C$DW$T$36, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/sys/_stdint.h")
	.dwattr $C$DW$T$36, DW_AT_decl_line(0x46)
	.dwattr $C$DW$T$36, DW_AT_decl_column(0x15)

	.dwendtag $C$DW$TU$36


$C$DW$TU$37	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$37

$C$DW$T$37	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$37, DW_AT_type(*$C$DW$T$36)
	.dwattr $C$DW$T$37, DW_AT_byte_size(0x40)
$C$DW$559	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$559, DW_AT_upper_bound(0x0f)

	.dwendtag $C$DW$T$37

	.dwendtag $C$DW$TU$37


$C$DW$TU$12	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$12
$C$DW$T$12	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$12, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$12, DW_AT_name("__int40_t")
	.dwattr $C$DW$T$12, DW_AT_byte_size(0x08)

	.dwendtag $C$DW$TU$12


$C$DW$TU$124	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$124
$C$DW$T$124	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$124, DW_AT_name("__int64_t")
	.dwattr $C$DW$T$124, DW_AT_type(*$C$DW$T$12)
	.dwattr $C$DW$T$124, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/machine/_types.h")
	.dwattr $C$DW$T$124, DW_AT_decl_line(0x6a)
	.dwattr $C$DW$T$124, DW_AT_decl_column(0x17)

	.dwendtag $C$DW$TU$124


$C$DW$TU$125	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$125
$C$DW$T$125	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$125, DW_AT_name("int64_t")
	.dwattr $C$DW$T$125, DW_AT_type(*$C$DW$T$124)
	.dwattr $C$DW$T$125, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/sys/_stdint.h")
	.dwattr $C$DW$T$125, DW_AT_decl_line(0x35)
	.dwattr $C$DW$T$125, DW_AT_decl_column(0x14)

	.dwendtag $C$DW$TU$125


$C$DW$TU$13	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$13
$C$DW$T$13	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$13, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$13, DW_AT_name("unsigned __int40_t")
	.dwattr $C$DW$T$13, DW_AT_byte_size(0x08)

	.dwendtag $C$DW$TU$13


$C$DW$TU$218	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$218

$C$DW$T$218	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$218, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$218, DW_AT_byte_size(0x08)
$C$DW$560	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$560, DW_AT_upper_bound(0x00)

	.dwendtag $C$DW$T$218

	.dwendtag $C$DW$TU$218


$C$DW$TU$122	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$122
$C$DW$T$122	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$122, DW_AT_name("__uint64_t")
	.dwattr $C$DW$T$122, DW_AT_type(*$C$DW$T$13)
	.dwattr $C$DW$T$122, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/machine/_types.h")
	.dwattr $C$DW$T$122, DW_AT_decl_line(0x6f)
	.dwattr $C$DW$T$122, DW_AT_decl_column(0x1f)

	.dwendtag $C$DW$TU$122


$C$DW$TU$123	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$123
$C$DW$T$123	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$123, DW_AT_name("uint64_t")
	.dwattr $C$DW$T$123, DW_AT_type(*$C$DW$T$122)
	.dwattr $C$DW$T$123, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/sys/_stdint.h")
	.dwattr $C$DW$T$123, DW_AT_decl_line(0x4b)
	.dwattr $C$DW$T$123, DW_AT_decl_column(0x15)

	.dwendtag $C$DW$TU$123


$C$DW$TU$14	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$14
$C$DW$T$14	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$14, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$14, DW_AT_name("long long")
	.dwattr $C$DW$T$14, DW_AT_byte_size(0x08)

	.dwendtag $C$DW$TU$14


$C$DW$TU$15	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$15
$C$DW$T$15	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$15, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$15, DW_AT_name("unsigned long long")
	.dwattr $C$DW$T$15, DW_AT_byte_size(0x08)

	.dwendtag $C$DW$TU$15


$C$DW$TU$216	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$216

$C$DW$T$216	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$216, DW_AT_type(*$C$DW$T$15)
	.dwattr $C$DW$T$216, DW_AT_byte_size(0x08)
$C$DW$561	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$561, DW_AT_upper_bound(0x00)

	.dwendtag $C$DW$T$216

	.dwendtag $C$DW$TU$216


$C$DW$TU$16	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$16
$C$DW$T$16	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$16, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$16, DW_AT_name("float")
	.dwattr $C$DW$T$16, DW_AT_byte_size(0x04)

	.dwendtag $C$DW$TU$16


$C$DW$TU$17	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$17
$C$DW$T$17	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$17, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$17, DW_AT_name("double")
	.dwattr $C$DW$T$17, DW_AT_byte_size(0x08)

	.dwendtag $C$DW$TU$17


$C$DW$TU$18	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$18
$C$DW$T$18	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$18, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$18, DW_AT_name("long double")
	.dwattr $C$DW$T$18, DW_AT_byte_size(0x08)

	.dwendtag $C$DW$TU$18


$C$DW$TU$19	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$19
$C$DW$T$19	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$19, DW_AT_encoding(DW_ATE_TI_complex_signed)
	.dwattr $C$DW$T$19, DW_AT_name("cchar")
	.dwattr $C$DW$T$19, DW_AT_byte_size(0x02)

	.dwendtag $C$DW$TU$19


$C$DW$TU$20	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$20
$C$DW$T$20	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$20, DW_AT_encoding(DW_ATE_TI_complex_signed)
	.dwattr $C$DW$T$20, DW_AT_name("cshort")
	.dwattr $C$DW$T$20, DW_AT_byte_size(0x04)

	.dwendtag $C$DW$TU$20


$C$DW$TU$21	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$21
$C$DW$T$21	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$21, DW_AT_encoding(DW_ATE_TI_complex_signed)
	.dwattr $C$DW$T$21, DW_AT_name("cint")
	.dwattr $C$DW$T$21, DW_AT_byte_size(0x08)

	.dwendtag $C$DW$TU$21


$C$DW$TU$22	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$22
$C$DW$T$22	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$22, DW_AT_encoding(DW_ATE_TI_complex_signed)
	.dwattr $C$DW$T$22, DW_AT_name("clong")
	.dwattr $C$DW$T$22, DW_AT_byte_size(0x10)

	.dwendtag $C$DW$TU$22


$C$DW$TU$23	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$23
$C$DW$T$23	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$23, DW_AT_encoding(DW_ATE_TI_complex_float)
	.dwattr $C$DW$T$23, DW_AT_name("cfloat")
	.dwattr $C$DW$T$23, DW_AT_byte_size(0x08)

	.dwendtag $C$DW$TU$23


$C$DW$TU$24	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$24
$C$DW$T$24	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$24, DW_AT_encoding(DW_ATE_TI_complex_float)
	.dwattr $C$DW$T$24, DW_AT_name("cdouble")
	.dwattr $C$DW$T$24, DW_AT_byte_size(0x10)

	.dwendtag $C$DW$TU$24


$C$DW$TU$217	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$217

$C$DW$T$217	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$217, DW_AT_name("__ti_vpred_tp")
	.dwattr $C$DW$T$217, DW_AT_byte_size(0x08)
$C$DW$562	.dwtag  DW_TAG_member
	.dwattr $C$DW$562, DW_AT_type(*$C$DW$T$216)
	.dwattr $C$DW$562, DW_AT_name("__vpred_field")
	.dwattr $C$DW$562, DW_AT_accessibility(DW_ACCESS_private)

	.dwendtag $C$DW$T$217

	.dwendtag $C$DW$TU$217


$C$DW$TU$346	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$346

$C$DW$T$346	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$346, DW_AT_name("__ti_vpred_tp")
	.dwattr $C$DW$T$346, DW_AT_byte_size(0x08)
$C$DW$563	.dwtag  DW_TAG_member
	.dwattr $C$DW$563, DW_AT_type(*$C$DW$T$216)
	.dwattr $C$DW$563, DW_AT_name("__vpred_field")
	.dwattr $C$DW$563, DW_AT_accessibility(DW_ACCESS_private)

	.dwendtag $C$DW$T$346

	.dwendtag $C$DW$TU$346


$C$DW$TU$224	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$224

$C$DW$T$224	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$224, DW_AT_name("__vpred_tp")
	.dwattr $C$DW$T$224, DW_AT_byte_size(0x08)
$C$DW$564	.dwtag  DW_TAG_member
	.dwattr $C$DW$564, DW_AT_type(*$C$DW$T$218)
	.dwattr $C$DW$564, DW_AT_name("_v")
	.dwattr $C$DW$564, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$564, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$564, DW_AT_decl_line(0x73)
	.dwattr $C$DW$564, DW_AT_decl_column(0x05)


$C$DW$565	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$565, DW_AT_name("operator =")
	.dwattr $C$DW$565, DW_AT_declaration
	.dwattr $C$DW$565, DW_AT_linkage_name("_ZN10__vpred_tpaSERKS_")
	.dwattr $C$DW$565, DW_AT_type(*$C$DW$T$219)
	.dwattr $C$DW$565, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$566	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$566, DW_AT_type(*$C$DW$T$221)

	.dwendtag $C$DW$565


$C$DW$567	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$567, DW_AT_name("operator =")
	.dwattr $C$DW$567, DW_AT_declaration
	.dwattr $C$DW$567, DW_AT_linkage_name("_ZN10__vpred_tpaSEOS_")
	.dwattr $C$DW$567, DW_AT_type(*$C$DW$T$219)
	.dwattr $C$DW$567, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$568	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$568, DW_AT_type(*$C$DW$T$219)

	.dwendtag $C$DW$567

	.dwendtag $C$DW$T$224

	.dwendtag $C$DW$TU$224


$C$DW$TU$219	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$219
$C$DW$T$219	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$219, DW_AT_type(*$C$DW$T$224)
	.dwattr $C$DW$T$219, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$219


$C$DW$TU$222	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$222

$C$DW$T$222	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$222, DW_AT_type(*$C$DW$T$219)
$C$DW$569	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$569, DW_AT_type(*$C$DW$T$221)

	.dwendtag $C$DW$T$222

	.dwendtag $C$DW$TU$222


$C$DW$TU$223	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$223

$C$DW$T$223	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$223, DW_AT_type(*$C$DW$T$219)
$C$DW$570	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$570, DW_AT_type(*$C$DW$T$219)

	.dwendtag $C$DW$T$223

	.dwendtag $C$DW$TU$223


$C$DW$TU$325	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$325
$C$DW$T$325	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$325, DW_AT_name("__vpred")
	.dwattr $C$DW$T$325, DW_AT_type(*$C$DW$T$224)
	.dwattr $C$DW$T$325, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/c7x_mma.h")
	.dwattr $C$DW$T$325, DW_AT_decl_line(0x73)
	.dwattr $C$DW$T$325, DW_AT_decl_column(0x05)

	.dwendtag $C$DW$TU$325


$C$DW$TU$220	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$220
$C$DW$T$220	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$220, DW_AT_type(*$C$DW$T$224)

	.dwendtag $C$DW$TU$220


$C$DW$TU$221	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$221
$C$DW$T$221	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$221, DW_AT_type(*$C$DW$T$220)
	.dwattr $C$DW$T$221, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$221

