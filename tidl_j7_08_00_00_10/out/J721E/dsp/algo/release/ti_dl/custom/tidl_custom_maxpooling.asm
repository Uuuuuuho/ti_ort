;******************************************************************************
;* C7x G3 C/C++ Codegen                                            Unix v1.4.2.LTS *
;* Date/Time created: Tue Oct  5 15:11:17 2021                                *
;******************************************************************************
	.compiler_opts --abi=eabi --endian=little --hll_source=on --object_format=elf --silicon_errata_i2117 --silicon_version=7100 --symdebug:dwarf --symdebug:dwarf_version=4 

;******************************************************************************
;* GLOBAL FILE PARAMETERS                                                     *
;*                                                                            *
;*   Architecture      : C7100                                                *
;*   Endian            : Little                                               *
;*   Pipelining        : Enabled                                              *
;*   Debug Info        : DWARF Debug                                          *
;*                                                                            *
;******************************************************************************

	.asg	D14, FP
	.asg	D15, SP
	.global	$bss


$C$DW$CU	.dwtag  DW_TAG_compile_unit
	.dwattr $C$DW$CU, DW_AT_name("tidl_custom_maxpooling.c")
	.dwattr $C$DW$CU, DW_AT_producer("TI C7x G3 C/C++ Codegen Unix v1.4.2.LTS Copyright (c) 2014-2019 Texas Instruments Incorporated")
	.dwattr $C$DW$CU, DW_AT_TI_version(0x01)
	.dwattr $C$DW$CU, DW_AT_comp_dir("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/tidl_j7_08_00_00_10/ti_dl/custom")

$C$DW$1	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$1, DW_AT_name("TIDL_customMaxPool_ixX_oxX_getHandleSize")
	.dwattr $C$DW$1, DW_AT_linkage_name("_Z40TIDL_customMaxPool_ixX_oxX_getHandleSizePv")
	.dwattr $C$DW$1, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$1, DW_AT_declaration
	.dwattr $C$DW$1, DW_AT_external
	.dwattr $C$DW$1, DW_AT_decl_file("tidsp/tidl_custom_maxpool_ixX_oxX.h")
	.dwattr $C$DW$1, DW_AT_decl_line(0x46)
	.dwattr $C$DW$1, DW_AT_decl_column(0x09)
$C$DW$2	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$2, DW_AT_type(*$C$DW$T$3)

	.dwendtag $C$DW$1


$C$DW$63	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$63, DW_AT_name("TIDL_customMaxPool_ixX_oxX_init")
	.dwattr $C$DW$63, DW_AT_linkage_name("_Z31TIDL_customMaxPool_ixX_oxX_initPvPK18TIDL_bufParams3D_tS2_S_")
	.dwattr $C$DW$63, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$63, DW_AT_declaration
	.dwattr $C$DW$63, DW_AT_external
	.dwattr $C$DW$63, DW_AT_decl_file("tidsp/tidl_custom_maxpool_ixX_oxX.h")
	.dwattr $C$DW$63, DW_AT_decl_line(0x48)
	.dwattr $C$DW$63, DW_AT_decl_column(0x09)
$C$DW$64	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$64, DW_AT_type(*$C$DW$T$3)

$C$DW$65	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$65, DW_AT_type(*$C$DW$T$50)

$C$DW$66	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$66, DW_AT_type(*$C$DW$T$50)

$C$DW$67	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$67, DW_AT_type(*$C$DW$T$3)

	.dwendtag $C$DW$63


$C$DW$68	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$68, DW_AT_name("TIDL_customMaxPool_ixX_oxX_exec")
	.dwattr $C$DW$68, DW_AT_linkage_name("_Z31TIDL_customMaxPool_ixX_oxX_execPvPK18TIDL_bufParams3D_tS2_PPKvS_")
	.dwattr $C$DW$68, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$68, DW_AT_declaration
	.dwattr $C$DW$68, DW_AT_external
	.dwattr $C$DW$68, DW_AT_decl_file("tidsp/tidl_custom_maxpool_ixX_oxX.h")
	.dwattr $C$DW$68, DW_AT_decl_line(0x4d)
	.dwattr $C$DW$68, DW_AT_decl_column(0x09)
$C$DW$69	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$69, DW_AT_type(*$C$DW$T$3)

$C$DW$70	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$70, DW_AT_type(*$C$DW$T$50)

$C$DW$71	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$71, DW_AT_type(*$C$DW$T$50)

$C$DW$72	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$72, DW_AT_type(*$C$DW$T$56)

$C$DW$73	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$73, DW_AT_type(*$C$DW$T$3)

	.dwendtag $C$DW$68


$C$DW$74	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$74, DW_AT_name("TIDL_DataflowInit")
	.dwattr $C$DW$74, DW_AT_linkage_name("_Z17TIDL_DataflowInitPvP23TIDL_DataflowInitParams")
	.dwattr $C$DW$74, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$74, DW_AT_declaration
	.dwattr $C$DW$74, DW_AT_external
	.dwattr $C$DW$74, DW_AT_decl_file("../inc/tidl_dataflow.h")
	.dwattr $C$DW$74, DW_AT_decl_line(0x165)
	.dwattr $C$DW$74, DW_AT_decl_column(0x09)
$C$DW$75	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$75, DW_AT_type(*$C$DW$T$3)

$C$DW$76	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$76, DW_AT_type(*$C$DW$T$343)

	.dwendtag $C$DW$74


$C$DW$77	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$77, DW_AT_name("TIDL_DataflowProcess")
	.dwattr $C$DW$77, DW_AT_linkage_name("_Z20TIDL_DataflowProcessPvPS_S0_")
	.dwattr $C$DW$77, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$77, DW_AT_declaration
	.dwattr $C$DW$77, DW_AT_external
	.dwattr $C$DW$77, DW_AT_decl_file("../inc/tidl_dataflow.h")
	.dwattr $C$DW$77, DW_AT_decl_line(0x171)
	.dwattr $C$DW$77, DW_AT_decl_column(0x09)
$C$DW$78	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$78, DW_AT_type(*$C$DW$T$3)

$C$DW$79	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$79, DW_AT_type(*$C$DW$T$347)

$C$DW$80	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$80, DW_AT_type(*$C$DW$T$347)

	.dwendtag $C$DW$77

;	/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/bin/opt7x /tmp/TI4XUokCr3X /tmp/TI4XUv2EKzl 
	.sect	".text:_Z28TIDL_refCustomMaxPoolingCoreIsEvPT_iiiiiiiiiiiiiS0_S1_P13sTIDL_Layer_t"
	.clink
	.global	||_Z28TIDL_refCustomMaxPoolingCoreIsEvPT_iiiiiiiiiiiiiS0_S1_P13sTIDL_Layer_t||

$C$DW$81	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$81, DW_AT_name("void TIDL_refCustomMaxPoolingCore")
	.dwattr $C$DW$81, DW_AT_low_pc(||_Z28TIDL_refCustomMaxPoolingCoreIsEvPT_iiiiiiiiiiiiiS0_S1_P13sTIDL_Layer_t||)
	.dwattr $C$DW$81, DW_AT_high_pc(0x00)
	.dwattr $C$DW$81, DW_AT_linkage_name("_Z28TIDL_refCustomMaxPoolingCoreIsEvPT_iiiiiiiiiiiiiS0_S1_P13sTIDL_Layer_t")
	.dwattr $C$DW$81, DW_AT_external
	.dwattr $C$DW$81, DW_AT_decl_file("tidl_custom_maxpooling.c")
	.dwattr $C$DW$81, DW_AT_decl_line(0x1a9)
	.dwattr $C$DW$81, DW_AT_decl_column(0x06)
	.dwattr $C$DW$81, DW_AT_TI_max_frame_size(0x18)
	.dwpsn	file "tidl_custom_maxpooling.c",line 442,column 1,is_stmt,address ||_Z28TIDL_refCustomMaxPoolingCoreIsEvPT_iiiiiiiiiiiiiS0_S1_P13sTIDL_Layer_t||,isa 0

	.dwfde $C$DW$CIE, ||_Z28TIDL_refCustomMaxPoolingCoreIsEvPT_iiiiiiiiiiiiiS0_S1_P13sTIDL_Layer_t||
$C$DW$82	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$82, DW_AT_name("pInChannel")
	.dwattr $C$DW$82, DW_AT_location[DW_OP_reg4]

$C$DW$83	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$83, DW_AT_name("width")
	.dwattr $C$DW$83, DW_AT_location[DW_OP_reg5]

$C$DW$84	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$84, DW_AT_name("height")
	.dwattr $C$DW$84, DW_AT_location[DW_OP_reg6]

$C$DW$85	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$85, DW_AT_name("inPitch")
	.dwattr $C$DW$85, DW_AT_location[DW_OP_reg7]

$C$DW$86	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$86, DW_AT_name("inChPitch")
	.dwattr $C$DW$86, DW_AT_location[DW_OP_reg8]

$C$DW$87	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$87, DW_AT_name("outPitch")
	.dwattr $C$DW$87, DW_AT_location[DW_OP_reg9]

$C$DW$88	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$88, DW_AT_name("outChPitch")
	.dwattr $C$DW$88, DW_AT_location[DW_OP_reg10]

$C$DW$89	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$89, DW_AT_name("numOutChannels")
	.dwattr $C$DW$89, DW_AT_location[DW_OP_reg11]

$C$DW$90	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$90, DW_AT_name("kernelW")
	.dwattr $C$DW$90, DW_AT_location[DW_OP_reg12]

$C$DW$91	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$91, DW_AT_name("kernelH")
	.dwattr $C$DW$91, DW_AT_location[DW_OP_bregx 0x6f 40]

$C$DW$92	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$92, DW_AT_name("strideW")
	.dwattr $C$DW$92, DW_AT_location[DW_OP_bregx 0x6f 44]

$C$DW$93	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$93, DW_AT_name("strideH")
	.dwattr $C$DW$93, DW_AT_location[DW_OP_bregx 0x6f 48]

$C$DW$94	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$94, DW_AT_name("padW")
	.dwattr $C$DW$94, DW_AT_location[DW_OP_bregx 0x6f 52]

$C$DW$95	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$95, DW_AT_name("padH")
	.dwattr $C$DW$95, DW_AT_location[DW_OP_bregx 0x6f 56]

$C$DW$96	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$96, DW_AT_name("initValue")
	.dwattr $C$DW$96, DW_AT_location[DW_OP_bregx 0x6f 60]

$C$DW$97	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$97, DW_AT_name("pOutChannel")
	.dwattr $C$DW$97, DW_AT_location[DW_OP_bregx 0x6f 64]

$C$DW$98	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$98, DW_AT_name("tidlLayer")
	.dwattr $C$DW$98, DW_AT_location[DW_OP_bregx 0x6f 72]


;******************************************************************************
;* FUNCTION NAME: void TIDL_refCustomMaxPoolingCore<short>(T1 *, int, int, int, int, int, int, int, int, int, int, int, int, int, T1, T1 *, sTIDL_Layer_t *)*
;*                                                                            *
;*   Regs Modified     : A0,A1,A2,A3,A4,A5,A6,A7,A9,A11,A13,VB0,VB1,VB2,VB3,  *
;*                           VB4,VB5,VB6,VB7,VB8,VB9,VB10,VB11,VB12,VB13,AL0, *
;*                           AL1,AM0,AM1,D0,D1,D2,D3,D4,D5,D6,SP,VBL0,VBL1,   *
;*                           VBL2,VBL3,VBL4,VBL5,VBL6,VBL7,VBM0,VBM1,VBM2,    *
;*                           VBM3,VBM4,VBM5,VBM6,VBM7                         *
;*   Regs Used         : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,VB0,   *
;*                           VB1,VB2,VB3,VB4,VB5,VB6,VB7,VB8,VB9,VB10,VB11,   *
;*                           VB12,VB13,AL0,AL1,AM0,AM1,D0,D1,D2,D3,D4,D5,D6,  *
;*                           SP,VBL0,VBL1,VBL2,VBL3,VBL4,VBL5,VBL6,VBL7,VBM0, *
;*                           VBM1,VBM2,VBM3,VBM4,VBM5,VBM6,VBM7               *
;*   Local Frame Size  : 0 Args + 0 Auto + 24 Save = 24 byte                  *
;******************************************************************************
||_Z28TIDL_refCustomMaxPoolingCoreIsEvPT_iiiiiiiiiiiiiS0_S1_P13sTIDL_Layer_t||:
;** --------------------------------------------------------------------------*
;* VB0   assigned to $O$C3
;* A0    assigned to $O$C4
;* A1    assigned to $O$C5
;* D0    assigned to $O$U95
;* A13   assigned to $O$U95
;* D1    assigned to $O$K58
;* A5    assigned to $O$K168
;* D3    assigned to $O$U188
;* A2    assigned to $O$L1
;* A1    assigned to $O$L2
;* A4    assigned to $O$L3
;* A3    assigned to $O$L4
;* AL0   assigned to $O$L5
;* A5    assigned to $O$L6
;* D2    assigned to $O$Lr91$inData
;* D4    assigned to $O$Lr111$outData
;* A7    assigned to $O$Lr17$i1
;* A11   assigned to $O$Lr74$numCols
;* VB1   assigned to $O$Lr20$i2
;* A6    assigned to $O$Lr33$i3
;* VBL3  assigned to $O$Lr37$i4
;* VB0   assigned to $O$Lr55$maxValue
;* VB3   assigned to $O$Lr15$maxValue
;* D5    assigned to $O$Lr57$i5
;* VB0   assigned to tidlLayer
$C$DW$99	.dwtag  DW_TAG_variable
	.dwattr $C$DW$99, DW_AT_name("tidlLayer")
	.dwattr $C$DW$99, DW_AT_type(*$C$DW$T$354)
	.dwattr $C$DW$99, DW_AT_location[DW_OP_regx 0x30]

;* VB3   assigned to pOutChannel
$C$DW$100	.dwtag  DW_TAG_variable
	.dwattr $C$DW$100, DW_AT_name("pOutChannel")
	.dwattr $C$DW$100, DW_AT_type(*$C$DW$T$366)
	.dwattr $C$DW$100, DW_AT_location[DW_OP_regx 0x33]

;* VBL2  assigned to initValue
$C$DW$101	.dwtag  DW_TAG_variable
	.dwattr $C$DW$101, DW_AT_name("initValue")
	.dwattr $C$DW$101, DW_AT_type(*$C$DW$T$365)
	.dwattr $C$DW$101, DW_AT_location[DW_OP_regx 0x4a]

;* AM0   assigned to padH
$C$DW$102	.dwtag  DW_TAG_variable
	.dwattr $C$DW$102, DW_AT_name("padH")
	.dwattr $C$DW$102, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$102, DW_AT_location[DW_OP_reg24]

;* VBL1  assigned to padW
$C$DW$103	.dwtag  DW_TAG_variable
	.dwattr $C$DW$103, DW_AT_name("padW")
	.dwattr $C$DW$103, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$103, DW_AT_location[DW_OP_regx 0x49]

;* VBM0  assigned to strideH
$C$DW$104	.dwtag  DW_TAG_variable
	.dwattr $C$DW$104, DW_AT_name("strideH")
	.dwattr $C$DW$104, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$104, DW_AT_location[DW_OP_regx 0x58]

;* AM0   assigned to strideW
$C$DW$105	.dwtag  DW_TAG_variable
	.dwattr $C$DW$105, DW_AT_name("strideW")
	.dwattr $C$DW$105, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$105, DW_AT_location[DW_OP_reg24]

;* A9    assigned to kernelH
$C$DW$106	.dwtag  DW_TAG_variable
	.dwattr $C$DW$106, DW_AT_name("kernelH")
	.dwattr $C$DW$106, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$106, DW_AT_location[DW_OP_reg9]

;* A12   assigned to kernelW
$C$DW$107	.dwtag  DW_TAG_variable
	.dwattr $C$DW$107, DW_AT_name("kernelW")
	.dwattr $C$DW$107, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$107, DW_AT_location[DW_OP_reg12]

;* A2    assigned to numOutChannels
$C$DW$108	.dwtag  DW_TAG_variable
	.dwattr $C$DW$108, DW_AT_name("numOutChannels")
	.dwattr $C$DW$108, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$108, DW_AT_location[DW_OP_reg2]

;* A10   assigned to outChPitch
$C$DW$109	.dwtag  DW_TAG_variable
	.dwattr $C$DW$109, DW_AT_name("outChPitch")
	.dwattr $C$DW$109, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$109, DW_AT_location[DW_OP_reg10]

;* VBM1  assigned to outPitch
$C$DW$110	.dwtag  DW_TAG_variable
	.dwattr $C$DW$110, DW_AT_name("outPitch")
	.dwattr $C$DW$110, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$110, DW_AT_location[DW_OP_regx 0x59]

;* A8    assigned to inChPitch
$C$DW$111	.dwtag  DW_TAG_variable
	.dwattr $C$DW$111, DW_AT_name("inChPitch")
	.dwattr $C$DW$111, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$111, DW_AT_location[DW_OP_reg8]

;* VB2   assigned to inPitch
$C$DW$112	.dwtag  DW_TAG_variable
	.dwattr $C$DW$112, DW_AT_name("inPitch")
	.dwattr $C$DW$112, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$112, DW_AT_location[DW_OP_regx 0x32]

;* A4    assigned to pInChannel
$C$DW$113	.dwtag  DW_TAG_variable
	.dwattr $C$DW$113, DW_AT_name("pInChannel")
	.dwattr $C$DW$113, DW_AT_type(*$C$DW$T$366)
	.dwattr $C$DW$113, DW_AT_location[DW_OP_reg4]

;* VB0   assigned to $O$CSE$s32x4$012
;* VB7   assigned to $O$CSE$s32x2$013
;* VB4   assigned to $O$CSE$s32x2$014
;* VBM3  assigned to $O$CSE$s32x4$015
;* VB6   assigned to $O$CSE$s32x2$016
;* VBL1  assigned to $O$CSE$s32x2$017
;* VB5   assigned to $O$CSE$s32x4$019
;* VB5   assigned to $O$CSE$s32x2$020
;* VB3   assigned to $O$CSE$s32x2$021
;* VBM2  assigned to $O$CSE$s32x4$022
;* VB0   assigned to $O$CSE$s32x2$023
;* VBL0  assigned to $O$CSE$s32x2$024
;* VB0   assigned to $O$VEC$s32x16$010
	.dwcfi	cfa_offset, 0
;          EXCLUSIVE CPU CYCLES: 25

           STD     .D1     A9,*SP(8)         ; [A_D1] 
||         STD     .D2X    A11,*SP++(-24)    ; [A_D2] 

	.dwcfi	save_reg_to_mem, 9, 8
	.dwcfi	cfa_offset, 24
	.dwcfi	save_reg_to_mem, 11, -24
           LDD     .D1     *SP(72),B0        ; [A_D1] |442| 
	.dwpsn	file "tidl_custom_maxpooling.c",line 447,column 15,is_stmt,isa 0
           ADDD    .L1X    B0,0xf0,A1        ; [A_L1] |447| 

           LDW     .D1     *A1(0),B4         ; [A_D1] |447| 
||         ADDD    .L1X    B0,0x6d0,A0       ; [A_L1] |448| 

	.dwpsn	file "tidl_custom_maxpooling.c",line 442,column 1,is_stmt,isa 0

           LDW     .D1     *A0(0),B1         ; [A_D1] |448| 
||         ADDD    .L1     A0,0xffffffe0,D0  ; [A_L1] |448| 
||         LDW     .D2     *SP(56),AM0       ; [A_D2] |442| 

	.dwpsn	file "tidl_custom_maxpooling.c",line 448,column 16,is_stmt,isa 0
           LDW     .D1     *D0(0),AM1        ; [A_D1] |448| 
	.dwpsn	file "tidl_custom_maxpooling.c",line 447,column 15,is_stmt,isa 0
           ADDD    .D1     A1,0xfffffffc,D0  ; [A_D1] |447| 
	.dwpsn	file "tidl_custom_maxpooling.c",line 442,column 1,is_stmt,isa 0

           LDW     .D1     *D0(0),BL0        ; [A_D1] |447| 
||         LDW     .D2     *SP(52),BL1       ; [A_D2] |442| 

	.dwpsn	file "tidl_custom_maxpooling.c",line 448,column 16,is_stmt,isa 0
           ADDD    .D1     A0,0xffffffdc,D1  ; [A_D1] |448| 
           LDW     .D1     *D1(0),D2         ; [A_D1] |448| 
	.dwpsn	file "tidl_custom_maxpooling.c",line 442,column 1,is_stmt,isa 0

           SUBW    .M1X    B4,AM0,AM0        ; [A_M1] |447| 
||         LDD     .D1     *SP(64),B3        ; [A_D1] |442| 
||         MV      .L2X    A7,B2             ; [B_L2] |442| 

	.dwpsn	file "tidl_custom_maxpooling.c",line 448,column 16,is_stmt,isa 0
           MPYWW   .N1X    B1,AM1,D0         ; [A_N1] |448| 
	.dwpsn	file "tidl_custom_maxpooling.c",line 447,column 15,is_stmt,isa 0
           MPYWW   .N1X    B2,AM0,D1         ; [A_N1] |447| 
           SUBW    .L2     BL0,BL1,B1        ; [B_L2] |447| 
	.dwpsn	file "tidl_custom_maxpooling.c",line 442,column 1,is_stmt,isa 0

           EXT     .L1X    B1,0x20,0x20,D3   ; [A_L1] |447| 
||         ADDAB   .D1     D2,D0,AL0         ; [A_D1] |448| 
||         LDH     .D2     *SP(60),BL2       ; [A_D2] |442| 

           ADDAB   .D1     D3,D1,AL1         ; [A_D1] |447| 
||         SHLD    .L1     AL0,0x1,AM1       ; [A_L1] |448| 
||         LDW     .D2     *SP(48),BM0       ; [A_D2] |442| 
||         MV      .L2X    A9,BM1            ; [B_L2] |442| 
||         MV      .S1     A11,A2            ; [A_S1] |442| 

           SHLD    .L1     AL1,0x1,D0        ; [A_L1] |447| 
||         ADDD    .M1X    B3,AM1,D4         ; [A_M1] |448| 
||         CMPGTW  .S1     A2,0,A0           ; [A_S1] |450| 
||         LDW     .D1     *SP(40),A9        ; [A_D1] |442| 
||         STD     .D2X    A13,*SP(16)       ; [A_D2] 
	.dwcfi	save_reg_to_mem, 13, 16

   [!A0]   B       .B1     ||$C$L16||        ; [A_B] |450| 
||         ADDD    .D1     A4,D0,D2          ; [A_D1] |447| 
||         ADDD    .S1X    B0,0x6dc,D1       ; [A_S1] 
||         LDW     .D2     *SP(44),AM0       ; [A_D2] |442| 
||         MVKU32  .L1     0,A7              ; [A_L1] |450| 

	.dwpsn	file "tidl_custom_maxpooling.c",line 450,column 16,is_stmt,isa 0
           ; BRANCHCC OCCURS {||$C$L16||}    ; [] |450| 
;** --------------------------------------------------------------------------*
;**   BEGIN LOOP ||$C$L1||
;** --------------------------------------------------------------------------*
||$C$L1||:    
;          EXCLUSIVE CPU CYCLES: 9
	.dwpsn	file "tidl_custom_maxpooling.c",line 456,column 18,is_stmt,isa 0
           LDW     .D1     *D1(0),B0         ; [A_D1] |456| 
	.dwpsn	file "tidl_custom_maxpooling.c",line 455,column 5,is_stmt,isa 0
           LDUW    .D1     *D1(4),A11        ; [A_D1] |455| 
	.dwpsn	file "tidl_custom_maxpooling.c",line 456,column 18,is_stmt,isa 0
           CMPEQW  .L1X    B0,0,A0           ; [A_L1] |456| 
   [ A0]   B       .B1     ||$C$L15||        ; [A_B] |456| 
           ; BRANCHCC OCCURS {||$C$L15||}    ; [] |456| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 1
	.dwpsn	file "tidl_custom_maxpooling.c",line 456,column 10,is_stmt,isa 0

           MVKU32  .L2     0,B1              ; [B_L2] |456| 
||         MV      .L1X    B0,A1             ; [A_L1] 

;** --------------------------------------------------------------------------*
;**   BEGIN LOOP ||$C$L2||
;** --------------------------------------------------------------------------*
||$C$L2||:    
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "tidl_custom_maxpooling.c",line 458,column 20,is_stmt,isa 0
           CMPEQW  .L1     A11,0,A0          ; [A_L1] |458| 
   [ A0]   B       .B1     ||$C$L14||        ; [A_B] |458| 
           ; BRANCHCC OCCURS {||$C$L14||}    ; [] |458| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 7
           MPYWW   .N2     BM1,B1,B0         ; [B_N2] 
           MPYWW   .N1     A10,A7,AM1        ; [A_N1] 
           ADDW    .M1X    B0,AM1,D0         ; [A_M1] 
	.dwpsn	file "tidl_custom_maxpooling.c",line 458,column 12,is_stmt,isa 0

           ADDAH   .D1     D4,D0,D3          ; [A_D1] 
||         MVKU32  .L1     0,A6              ; [A_L1] |458| 
||         MV      .D2     A11,A4            ; [A_D2] 

;** --------------------------------------------------------------------------*
;**   BEGIN LOOP ||$C$L3||
;** --------------------------------------------------------------------------*
||$C$L3||:    
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "tidl_custom_maxpooling.c",line 461,column 22,is_stmt,isa 0
           CMPGTW  .L1     A9,0,A0           ; [A_L1] |461| 

   [!A0]   B       .B1     ||$C$L13||        ; [A_B] |461| 
||         MV      .L2     BL2,B3            ; [B_L2] 
||         MV      .S2     BL2,B0            ; [B_S2] 

           ; BRANCHCC OCCURS {||$C$L13||}    ; [] |461| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 1
	.dwpsn	file "tidl_custom_maxpooling.c",line 461,column 14,is_stmt,isa 0

           MVKU32  .L2     0,BL3             ; [B_L2] |461| 
||         MV      .D1     A9,A3             ; [A_D1] 

;** --------------------------------------------------------------------------*
;**   BEGIN LOOP ||$C$L4||
;** --------------------------------------------------------------------------*
||$C$L4||:    
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "tidl_custom_maxpooling.c",line 463,column 24,is_stmt,isa 0
           CMPGTW  .L1     A12,0,A0          ; [A_L1] |463| 
   [!A0]   B       .B1     ||$C$L12||        ; [A_B] |463| 
           ; BRANCHCC OCCURS {||$C$L12||}    ; [] |463| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 3
	.dwpsn	file "tidl_custom_maxpooling.c",line 465,column 13,is_stmt,isa 0
           VDUPW   .L2     B0,VB0            ; [B_L2] |465| 
           CMPGEW  .L1     A12,0x10,A0       ; [A_L1] 
	.dwpsn	file "tidl_custom_maxpooling.c",line 463,column 16,is_stmt,isa 0

   [!A0]   B       .B1     ||$C$L8||         ; [A_B] 
|| [!A0]   MVKU32  .L1     0,D5              ; [A_L1] |463| 
||         ANDW    .S1     A12,0xf,A5        ; [A_S1] 

           ; BRANCHCC OCCURS {||$C$L8||}     ; [] 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 15
           MPYWW   .N2     BM0,B1,BL0        ; [B_N2] 
           ADDW    .L2     BL3,BL0,BM2       ; [B_L2] 
           MPYWW   .N2     B2,BM2,B3         ; [B_N2] 
           MPYWW   .N1     A8,A7,AM1         ; [A_N1] 
           MPYWW   .N1     AM0,A6,D5         ; [A_N1] 
           ADDW    .M1X    B3,AM1,D0         ; [A_M1] 
           ADDW    .D1     D5,D0,D0          ; [A_D1] 

           ADDAH   .D1     D2,D0,A13         ; [A_D1] 
||         SHRW    .L1     A12,0x4,AL0       ; [A_L1] 

           MV      .D1     A13,D0            ; [A_D1] 
||         EXT     .L1     AL0,0x20,0x20,AL0 ; [A_L1] 

	.dwpsn	file "tidl_custom_maxpooling.c",line 463,column 24,is_stmt,isa 0

           ADDD    .D1     D0,0xffffffe0,D0  ; [A_D1] undo prolog elim. side-effects
||         ANDW    .M1     A12,0xfffffff0,D5 ; [A_M1] |463| 
||         NLCINIT .S1     AL0,0x1,6         ; [A_S1] 
||         UNPROT          0x1               ; [A_U] 
||         VMV     .L2     VB0,VBL1          ; [B_L2] 
||         MVKU32  .L1     0x7,A0            ; [A_L1] init prolog collapse predicate

;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*
;*      Loop found in file               : tidl_custom_maxpooling.c
;*      Loop source line                 : 463
;*      Loop opening brace source line   : 464
;*      Loop closing brace source line   : 470
;*      Loop Unroll Multiple             : 16x
;*      Known Minimum Trip Count         : 1                    
;*      Known Max Trip Count Factor      : 1
;*      Loop Carried Dependency Bound(^) : 0
;*      Unpartitioned Resource Bound     : 1
;*      Partitioned Resource Bound       : 1 (pre-sched)
;*
;*      Searching for software pipeline schedule at ...
;*         ii = 1  Schedule found with 8 iterations in parallel
;*
;*      Partitioned Resource Bound(*)    : 1 (post-sched)
;*
;*      Constant Extension #0 Used [C0]  : 0
;*      Constant Extension #1 Used [C1]  : 0
;*
;*      Resource Partition (may include "post-sched" split/spill moves):
;*
;*                                                A-side   B-side
;*      .L units                                     0        0     
;*      .S units                                     0        0     
;*      .M units                                     0        0     
;*      .N units                                     0        0     
;*      .D units                                     1        -     
;*      .B units                                     1*       -     
;*      .C units                                     -        0     
;*      .P units                                     -        0     
;*
;*      .M/.N units                                  0        0     
;*      .L/.S units                                  0        0     
;*      .L/.S/.C units                               0        0     
;*      .L/.S/.C/.M units                            0        1     
;*      .L/.S/.C/.M/.D units                         0        0     
;*
;*      .X cross paths                               0        0     
;*
;*      Bound(.D1 .D2 .D)                            1*       -     
;*      Bound(.M .N .MN)                             0        0     
;*      Bound(.L .S .LS)                             0        0     
;*      Bound(.L .S .C .LS .LSC)                     0        0     
;*      Bound(.L .S .C .M .LS .LSC .LSCM)            0        1*    
;*      Bound(.L .S .C .M .D .LS .LSC .LSCM .LSCMD)  1*       1*    
;*
;*      Register Usage Tables:
;*
;*      +----------------------------------+----------------------------------+
;*      |              ASIDE               |              BSIDE               |
;*      +----------------+--------+--------+----------------+--------+--------+
;*      |      "Axx"     |  "ALx" |  "AMx" |     "VBxx"     | "VBLx" | "VBMx" |
;*      +----------------|--------|--------|----------------|--------|--------+
;*      |0000000000111111|00000000|00000000|0000000000111111|00000000|00000000|
;*      |0123456789012345|01234567|01234567|0123456789012345|01234567|01234567|
;*      +----------------|--------|--------|----------------|--------|--------+
;*   0: |*               |        |        |                |**      |        |
;*      +----------------+--------+--------+----------------+--------+--------+
;*
;*      +----------------+                 +--------+       +-------+
;*      |      "Dxx"     |                 |  "Px"  |       |"CUCRx"|
;*      +----------------+                 +--------+       +-------+
;*      |0000000000111111|                 |00000000|       |0000   |
;*      |0123456789012345|                 |01234567|       |0123   |
;*      +----------------+                 +--------+       +-------+
;*   0: |*               |                 |        |       |       |
;*      +----------------+                 +--------+       +-------+
;*
;*      Done
;*
;*      Collapsed epilog stages       : 7
;*      Collapsed prolog stages       : 7
;*      Max amt of load speculation   : 192 bytes
;*
;*      Minimum safe trip count       : 1 (after unrolling)
;*      Min. prof. trip count  (est.) : 2 (after unrolling)
;*
;*      Mem bank conflicts/iter(est.) : { min 0.000, est 0.000, max 0.000 }
;*      Mem bank perf. penalty (est.) : 0.0%
;*
;*
;*      Total cycles (est.)         : 7 + trip_cnt * 1        
;*----------------------------------------------------------------------------*
;*        SINGLE SCHEDULED ITERATION
;*
;*        ||$C$C100||:
;*   0              TICK                               ; [A_U] 
;*   1              VLDHUNPKW .D1   *D0++(32),VBL0    ; [A_D1] |465| 
;*   2              NOP     0x5     ; [A_B] 
;*   7              VMAXW   .L2     VBL1,VBL0,VBL1    ; [B_L2] |465| 
;*     ||           BNL     .B1     ||$C$C100||       ; [A_B] |463| 
;*   8              ; BRANCHCC OCCURS {||$C$C100||}   ; [] |463| 
;*----------------------------------------------------------------------------*
||$C$L5||:    ; PIPED LOOP PROLOG
;** --------------------------------------------------------------------------*
||$C$L6||:    ; PIPED LOOP KERNEL
;          EXCLUSIVE CPU CYCLES: 1

   [ A0]   ADDW    .D2     A0,0xffffffff,A0  ; [A_D2] |463| <0,7> collapsing predicate control
||         BNL     .B1     ||$C$L6||         ; [A_B] |463| <0,7> 
|| [!A0]   VMAXW   .L2     VBL1,VBL0,VBL1    ; [B_L2] |465| <0,7> 
||         VLDHUNPKW .D1   *D0++(32),VBL0    ; [A_D1] |465| <6,1> 
||         TICK                               ; [A_U] <7,0> 

;** --------------------------------------------------------------------------*
||$C$L7||:    ; PIPED LOOP EPILOG
;          EXCLUSIVE CPU CYCLES: 1
           PROTCLR                            ; [A_U] VBL1
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 1
           VMV     .L2     VBL1,VB0          ; [B_L2] 
;** --------------------------------------------------------------------------*
||$C$L8||:    
;          EXCLUSIVE CPU CYCLES: 23
	.dwpsn	file "tidl_custom_maxpooling.c",line 465,column 13,is_stmt,isa 0
           VHHMV   .C2     VB0,VB0,VB5       ; [B_C] |465| 
           VGETD   .C2     VB0,0x2,BM5       ; [B_C] |465| 
           VGETD   .C2     VB0,0x3,BM3       ; [B_C] |465| 
           VGETD   .C2     VB5,0x2,BM4       ; [B_C] |465| 
           VGETD   .C2     VB5,0x3,BM2       ; [B_C] |465| 
           VSHFL2DLL .C2   VBM3,VBM5,VBM3    ; [B_C] |465| 
           VSHFL2DLL .C2   VBM2,VBM4,VBM2    ; [B_C] |465| 

           EXTV    .L2     VB5,1,0,0,VB3     ; [B_L2] |465| 
||         EXTV    .S2     VB0,1,0,0,VB4     ; [B_S2] |465| 

           VGETD   .C2     VBM3,1,VBL1       ; [B_C] |465| 
           VGETD   .C2     VBM2,1,VBL0       ; [B_C] |465| 
           MV      .L2     B0,B7             ; [B_L2] |465| 

           MV      .M2     BM3,B6            ; [B_M2] |465| 
||         SHRD    .L2     B4,0x20,B10       ; [B_L2] |465| 
||         SHRD    .S2     B7,0x20,B12       ; [B_S2] |465| 

           SHRD    .L2     B6,0x20,BM6       ; [B_L2] |465| 
||         MV      .C2     B5,B5             ; [B_C] |465| 
||         SHRD    .S2     B3,0x20,B11       ; [B_S2] |465| 
||         VMAXW   .M2     B7,B12,BM7        ; [B_M2] |465| 

           MV      .M2     BM2,B0            ; [B_M2] |465| 
||         VMAXW   .C2     B6,BM6,B9         ; [B_C] |465| 
||         SHRD    .L2     BL1,0x20,BL5      ; [B_L2] |465| 
||         SHRD    .S2     B5,0x20,B13       ; [B_S2] |465| 

           SHRD    .L2     B0,0x20,BM4       ; [B_L2] |465| 
||         VMAXW   .M2     B4,B10,BM3        ; [B_M2] |465| 
||         SHRD    .S2     BL0,0x20,BL4      ; [B_S2] |465| 
||         VMAXW   .C2     B5,B13,BM5        ; [B_C] |465| 

           VMAXW   .M2     B3,B11,BM2        ; [B_M2] |465| 
||         VMAXW   .L2     BL0,BL4,BL0       ; [B_L2] |465| 
||         VMAXW   .C2     B0,BM4,BL6        ; [B_C] |465| 
||         VMAXW   .S2     BL1,BL5,BL7       ; [B_S2] |465| 

           VMAXW   .L2     B9,BL7,BL4        ; [B_L2] |465| 
||         VMAXW   .S2     BL6,BL0,BL0       ; [B_S2] |465| 
||         VMAXW   .M2     BM5,BM2,B8        ; [B_M2] |465| 
||         VMAXW   .C2     BM7,BM3,B12       ; [B_C] |465| 

           VMAXW   .L2     B12,BL4,BL1       ; [B_L2] |465| 
||         VMAXW   .S2     B8,BL0,BL0        ; [B_S2] |465| 
||         CMPEQW  .L1     A5,0,A0           ; [A_L1] 

   [ A0]   B       .B1     ||$C$L12||        ; [A_B] 
||         VMAXW   .L2     BL1,BL0,B0        ; [B_L2] |465| 

           ; BRANCHCC OCCURS {||$C$L12||}    ; [] 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 15
; Peeled loop iterations for unrolled loop:
           MPYWW   .N2     BM0,B1,BL0        ; [B_N2] 
           ADDW    .L2     BL3,BL0,BM2       ; [B_L2] 
           MPYWW   .N2     B2,BM2,B3         ; [B_N2] 
           MPYWW   .N1     A8,A7,AM1         ; [A_N1] 
           MPYWW   .N1     AM0,A6,D0         ; [A_N1] 
           ADDW    .M1X    B3,AM1,D6         ; [A_M1] 
           ADDW    .D1     D0,D6,D0          ; [A_D1] 
           ADDW    .D1     D5,D0,D0          ; [A_D1] 

           ADDAH   .D1     D2,D0,D0          ; [A_D1] 
||         EXT     .L1     A5,0x20,0x20,A5   ; [A_L1] 

           ADDD    .D1     D0,0xfffffffe,D0  ; [A_D1] undo prolog elim. side-effects
||         NLCINIT .S1     A5,0x1,6          ; [A_S1] 
||         UNPROT          0x1               ; [A_U] 
||         MV      .L2     B0,BL1            ; [B_L2] 
||         MVKU32  .L1     0x7,A0            ; [A_L1] init prolog collapse predicate

;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*
;*      Loop found in file               : tidl_custom_maxpooling.c
;*      Loop source line                 : 463
;*      Loop opening brace source line   : 464
;*      Loop closing brace source line   : 470
;*      Known Minimum Trip Count         : 1                    
;*      Known Maximum Trip Count         : 15                    
;*      Known Max Trip Count Factor      : 1
;*      Loop Carried Dependency Bound(^) : 0
;*      Unpartitioned Resource Bound     : 1
;*      Partitioned Resource Bound       : 1 (pre-sched)
;*
;*      Searching for software pipeline schedule at ...
;*         ii = 1  Schedule found with 8 iterations in parallel
;*
;*      Partitioned Resource Bound(*)    : 1 (post-sched)
;*
;*      Constant Extension #0 Used [C0]  : 0
;*      Constant Extension #1 Used [C1]  : 0
;*
;*      Resource Partition (may include "post-sched" split/spill moves):
;*
;*                                                A-side   B-side
;*      .L units                                     0        0     
;*      .S units                                     0        0     
;*      .M units                                     0        0     
;*      .N units                                     0        0     
;*      .D units                                     1        -     
;*      .B units                                     1*       -     
;*      .C units                                     -        0     
;*      .P units                                     -        0     
;*
;*      .M/.N units                                  0        0     
;*      .L/.S units                                  0        0     
;*      .L/.S/.C units                               0        0     
;*      .L/.S/.C/.M units                            0        1     
;*      .L/.S/.C/.M/.D units                         0        0     
;*
;*      .X cross paths                               0        0     
;*
;*      Bound(.D1 .D2 .D)                            1*       -     
;*      Bound(.M .N .MN)                             0        0     
;*      Bound(.L .S .LS)                             0        0     
;*      Bound(.L .S .C .LS .LSC)                     0        0     
;*      Bound(.L .S .C .M .LS .LSC .LSCM)            0        1*    
;*      Bound(.L .S .C .M .D .LS .LSC .LSCM .LSCMD)  1*       1*    
;*
;*      Register Usage Tables:
;*
;*      +----------------------------------+----------------------------------+
;*      |              ASIDE               |              BSIDE               |
;*      +----------------+--------+--------+----------------+--------+--------+
;*      |      "Axx"     |  "ALx" |  "AMx" |     "VBxx"     | "VBLx" | "VBMx" |
;*      +----------------|--------|--------|----------------|--------|--------+
;*      |0000000000111111|00000000|00000000|0000000000111111|00000000|00000000|
;*      |0123456789012345|01234567|01234567|0123456789012345|01234567|01234567|
;*      +----------------|--------|--------|----------------|--------|--------+
;*   0: |*               |        |        |                |**      |        |
;*      +----------------+--------+--------+----------------+--------+--------+
;*
;*      +----------------+                 +--------+       +-------+
;*      |      "Dxx"     |                 |  "Px"  |       |"CUCRx"|
;*      +----------------+                 +--------+       +-------+
;*      |0000000000111111|                 |00000000|       |0000   |
;*      |0123456789012345|                 |01234567|       |0123   |
;*      +----------------+                 +--------+       +-------+
;*   0: |*               |                 |        |       |       |
;*      +----------------+                 +--------+       +-------+
;*
;*      Done
;*
;*      Collapsed epilog stages       : 7
;*      Collapsed prolog stages       : 7
;*      Max amt of load speculation   : 12 bytes
;*
;*      Minimum safe trip count       : 1
;*      Min. prof. trip count  (est.) : 2
;*
;*      Mem bank conflicts/iter(est.) : { min 0.000, est 0.000, max 0.000 }
;*      Mem bank perf. penalty (est.) : 0.0%
;*
;*
;*      Total cycles (est.)         : 7 + trip_cnt * 1 = Between 8 and 22        
;*----------------------------------------------------------------------------*
;*        SINGLE SCHEDULED ITERATION
;*
;*        ||$C$C41||:
;*   0              TICK                               ; [A_U] 
;*   1              SLDH    .D1     *D0++(2),BL0      ; [A_D1] |465| 
;*   2              NOP     0x5     ; [A_B] 
;*   7              VMAXW   .L2     BL1,BL0,BL1       ; [B_L2] |465| 
;*     ||           BNL     .B1     ||$C$C41||        ; [A_B] |463| 
;*   8              ; BRANCHCC OCCURS {||$C$C41||}    ; [] |463| 
;*----------------------------------------------------------------------------*
||$C$L9||:    ; PIPED LOOP PROLOG
;** --------------------------------------------------------------------------*
||$C$L10||:    ; PIPED LOOP KERNEL
;          EXCLUSIVE CPU CYCLES: 1
	.dwpsn	file "tidl_custom_maxpooling.c",line 463,column 24,is_stmt,isa 0

   [ A0]   ADDW    .D2     A0,0xffffffff,A0  ; [A_D2] |463| <0,7> collapsing predicate control
||         BNL     .B1     ||$C$L10||        ; [A_B] |463| <0,7> 
|| [!A0]   VMAXW   .L2     BL1,BL0,BL1       ; [B_L2] |465| <0,7> 
||         SLDH    .D1     *D0++(2),BL0      ; [A_D1] |465| <6,1> 
||         TICK                               ; [A_U] <7,0> 

;** --------------------------------------------------------------------------*
||$C$L11||:    ; PIPED LOOP EPILOG
;          EXCLUSIVE CPU CYCLES: 1
           PROTCLR                            ; [A_U] VBL1
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 1
           MV      .L2     BL1,B0            ; [B_L2] 
;** --------------------------------------------------------------------------*
||$C$L12||:    
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "tidl_custom_maxpooling.c",line 461,column 22,is_stmt,isa 0
           ADDW    .D1     A3,0xffffffff,A3  ; [A_D1] |461| 

   [ A3]   B       .B1     ||$C$L4||         ; [A_B] |461| 
||         ADDW    .L2     BL3,0x1,BL3       ; [B_L2] |461| 

           ; BRANCHCC OCCURS {||$C$L4||}     ; [] |461| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 1
           MV      .L2     B0,B3             ; [B_L2] 
;** --------------------------------------------------------------------------*
||$C$L13||:    
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "tidl_custom_maxpooling.c",line 458,column 20,is_stmt,isa 0
           ADDW    .D1     A4,0xffffffff,A4  ; [A_D1] |458| 

   [ A4]   B       .B1     ||$C$L3||         ; [A_B] |458| 
||         STH     .D1X    B3,*D3++(2)       ; [A_D1] |472| 
||         ADDW    .D2     A6,0x1,A6         ; [A_D2] |458| 

           ; BRANCHCC OCCURS {||$C$L3||}     ; [] |458| 
;** --------------------------------------------------------------------------*
||$C$L14||:    
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "tidl_custom_maxpooling.c",line 456,column 18,is_stmt,isa 0
           ADDW    .D1     A1,0xffffffff,A1  ; [A_D1] |456| 

   [ A1]   B       .B1     ||$C$L2||         ; [A_B] |456| 
||         ADDW    .L2     B1,0x1,B1         ; [B_L2] |456| 

           ; BRANCHCC OCCURS {||$C$L2||}     ; [] |456| 
;** --------------------------------------------------------------------------*
||$C$L15||:    
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "tidl_custom_maxpooling.c",line 450,column 16,is_stmt,isa 0
           ADDW    .D1     A2,0xffffffff,A2  ; [A_D1] |450| 

   [ A2]   B       .B1     ||$C$L1||         ; [A_B] |450| 
||         ADDW    .D1     A7,0x1,A7         ; [A_D1] |450| 

           ; BRANCHCC OCCURS {||$C$L1||}     ; [] |450| 
;** --------------------------------------------------------------------------*
||$C$L16||:    
;          EXCLUSIVE CPU CYCLES: 7
           LDD     .D1     *SP(16),A13       ; [A_D1] 
	.dwcfi	restore_reg, 13

           LDD     .D1     *SP(32),A9        ; [A_D1] 
||         LDD     .D2     *SP(24),A11       ; [A_D2] 

	.dwcfi	restore_reg, 9
	.dwcfi	restore_reg, 11
$C$DW$114	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$114, DW_AT_low_pc(0x00)
	.dwattr $C$DW$114, DW_AT_TI_return


           RET     .B1     ; [A_B] 
||         ADDD    .D1     SP,0x18,SP        ; [A_D1] 

	.dwcfi	cfa_offset, 0
           ; RETURN OCCURS {RP}              ; [] 
	.dwattr $C$DW$81, DW_AT_TI_end_file("tidl_custom_maxpooling.c")
	.dwattr $C$DW$81, DW_AT_TI_end_line(0x1dc)
	.dwattr $C$DW$81, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$81

	.sect	".text:_Z28TIDL_refCustomMaxPoolingCoreItEvPT_iiiiiiiiiiiiiS0_S1_P13sTIDL_Layer_t"
	.clink
	.global	||_Z28TIDL_refCustomMaxPoolingCoreItEvPT_iiiiiiiiiiiiiS0_S1_P13sTIDL_Layer_t||

$C$DW$115	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$115, DW_AT_name("void TIDL_refCustomMaxPoolingCore")
	.dwattr $C$DW$115, DW_AT_low_pc(||_Z28TIDL_refCustomMaxPoolingCoreItEvPT_iiiiiiiiiiiiiS0_S1_P13sTIDL_Layer_t||)
	.dwattr $C$DW$115, DW_AT_high_pc(0x00)
	.dwattr $C$DW$115, DW_AT_linkage_name("_Z28TIDL_refCustomMaxPoolingCoreItEvPT_iiiiiiiiiiiiiS0_S1_P13sTIDL_Layer_t")
	.dwattr $C$DW$115, DW_AT_external
	.dwattr $C$DW$115, DW_AT_decl_file("tidl_custom_maxpooling.c")
	.dwattr $C$DW$115, DW_AT_decl_line(0x1a9)
	.dwattr $C$DW$115, DW_AT_decl_column(0x06)
	.dwattr $C$DW$115, DW_AT_TI_max_frame_size(0x18)
	.dwpsn	file "tidl_custom_maxpooling.c",line 442,column 1,is_stmt,address ||_Z28TIDL_refCustomMaxPoolingCoreItEvPT_iiiiiiiiiiiiiS0_S1_P13sTIDL_Layer_t||,isa 0

	.dwfde $C$DW$CIE, ||_Z28TIDL_refCustomMaxPoolingCoreItEvPT_iiiiiiiiiiiiiS0_S1_P13sTIDL_Layer_t||
$C$DW$116	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$116, DW_AT_name("pInChannel")
	.dwattr $C$DW$116, DW_AT_location[DW_OP_reg4]

$C$DW$117	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$117, DW_AT_name("width")
	.dwattr $C$DW$117, DW_AT_location[DW_OP_reg5]

$C$DW$118	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$118, DW_AT_name("height")
	.dwattr $C$DW$118, DW_AT_location[DW_OP_reg6]

$C$DW$119	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$119, DW_AT_name("inPitch")
	.dwattr $C$DW$119, DW_AT_location[DW_OP_reg7]

$C$DW$120	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$120, DW_AT_name("inChPitch")
	.dwattr $C$DW$120, DW_AT_location[DW_OP_reg8]

$C$DW$121	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$121, DW_AT_name("outPitch")
	.dwattr $C$DW$121, DW_AT_location[DW_OP_reg9]

$C$DW$122	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$122, DW_AT_name("outChPitch")
	.dwattr $C$DW$122, DW_AT_location[DW_OP_reg10]

$C$DW$123	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$123, DW_AT_name("numOutChannels")
	.dwattr $C$DW$123, DW_AT_location[DW_OP_reg11]

$C$DW$124	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$124, DW_AT_name("kernelW")
	.dwattr $C$DW$124, DW_AT_location[DW_OP_reg12]

$C$DW$125	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$125, DW_AT_name("kernelH")
	.dwattr $C$DW$125, DW_AT_location[DW_OP_bregx 0x6f 40]

$C$DW$126	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$126, DW_AT_name("strideW")
	.dwattr $C$DW$126, DW_AT_location[DW_OP_bregx 0x6f 44]

$C$DW$127	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$127, DW_AT_name("strideH")
	.dwattr $C$DW$127, DW_AT_location[DW_OP_bregx 0x6f 48]

$C$DW$128	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$128, DW_AT_name("padW")
	.dwattr $C$DW$128, DW_AT_location[DW_OP_bregx 0x6f 52]

$C$DW$129	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$129, DW_AT_name("padH")
	.dwattr $C$DW$129, DW_AT_location[DW_OP_bregx 0x6f 56]

$C$DW$130	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$130, DW_AT_name("initValue")
	.dwattr $C$DW$130, DW_AT_location[DW_OP_bregx 0x6f 60]

$C$DW$131	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$131, DW_AT_name("pOutChannel")
	.dwattr $C$DW$131, DW_AT_location[DW_OP_bregx 0x6f 64]

$C$DW$132	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$132, DW_AT_name("tidlLayer")
	.dwattr $C$DW$132, DW_AT_location[DW_OP_bregx 0x6f 72]


;******************************************************************************
;* FUNCTION NAME: void TIDL_refCustomMaxPoolingCore<unsigned short>(T1 *, int, int, int, int, int, int, int, int, int, int, int, int, int, T1, T1 *, sTIDL_Layer_t *)*
;*                                                                            *
;*   Regs Modified     : A0,A1,A2,A3,A4,A5,A6,A7,A9,A11,A13,VB0,VB1,VB2,VB3,  *
;*                           VB4,VB5,VB6,VB7,VB8,VB9,VB10,VB11,VB12,VB13,AL0, *
;*                           AL1,AM0,AM1,D0,D1,D2,D3,D4,D5,D6,SP,VBL0,VBL1,   *
;*                           VBL2,VBL3,VBL4,VBL5,VBL6,VBL7,VBM0,VBM1,VBM2,    *
;*                           VBM3,VBM4,VBM5,VBM6,VBM7                         *
;*   Regs Used         : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,VB0,   *
;*                           VB1,VB2,VB3,VB4,VB5,VB6,VB7,VB8,VB9,VB10,VB11,   *
;*                           VB12,VB13,AL0,AL1,AM0,AM1,D0,D1,D2,D3,D4,D5,D6,  *
;*                           SP,VBL0,VBL1,VBL2,VBL3,VBL4,VBL5,VBL6,VBL7,VBM0, *
;*                           VBM1,VBM2,VBM3,VBM4,VBM5,VBM6,VBM7               *
;*   Local Frame Size  : 0 Args + 0 Auto + 24 Save = 24 byte                  *
;******************************************************************************
||_Z28TIDL_refCustomMaxPoolingCoreItEvPT_iiiiiiiiiiiiiS0_S1_P13sTIDL_Layer_t||:
;** --------------------------------------------------------------------------*
;* VB0   assigned to $O$C3
;* A0    assigned to $O$C4
;* A1    assigned to $O$C5
;* D0    assigned to $O$U95
;* A13   assigned to $O$U95
;* D1    assigned to $O$K58
;* A5    assigned to $O$K168
;* D3    assigned to $O$U188
;* A2    assigned to $O$L1
;* A1    assigned to $O$L2
;* A4    assigned to $O$L3
;* A3    assigned to $O$L4
;* AL0   assigned to $O$L5
;* A5    assigned to $O$L6
;* D2    assigned to $O$Lr91$inData
;* D4    assigned to $O$Lr111$outData
;* A7    assigned to $O$Lr17$i1
;* A11   assigned to $O$Lr74$numCols
;* VB1   assigned to $O$Lr20$i2
;* A6    assigned to $O$Lr33$i3
;* VBL3  assigned to $O$Lr37$i4
;* VB0   assigned to $O$Lr55$maxValue
;* VB3   assigned to $O$Lr15$maxValue
;* D5    assigned to $O$Lr57$i5
;* VB0   assigned to tidlLayer
$C$DW$133	.dwtag  DW_TAG_variable
	.dwattr $C$DW$133, DW_AT_name("tidlLayer")
	.dwattr $C$DW$133, DW_AT_type(*$C$DW$T$354)
	.dwattr $C$DW$133, DW_AT_location[DW_OP_regx 0x30]

;* VB3   assigned to pOutChannel
$C$DW$134	.dwtag  DW_TAG_variable
	.dwattr $C$DW$134, DW_AT_name("pOutChannel")
	.dwattr $C$DW$134, DW_AT_type(*$C$DW$T$362)
	.dwattr $C$DW$134, DW_AT_location[DW_OP_regx 0x33]

;* VBL2  assigned to initValue
$C$DW$135	.dwtag  DW_TAG_variable
	.dwattr $C$DW$135, DW_AT_name("initValue")
	.dwattr $C$DW$135, DW_AT_type(*$C$DW$T$361)
	.dwattr $C$DW$135, DW_AT_location[DW_OP_regx 0x4a]

;* AM0   assigned to padH
$C$DW$136	.dwtag  DW_TAG_variable
	.dwattr $C$DW$136, DW_AT_name("padH")
	.dwattr $C$DW$136, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$136, DW_AT_location[DW_OP_reg24]

;* VBL1  assigned to padW
$C$DW$137	.dwtag  DW_TAG_variable
	.dwattr $C$DW$137, DW_AT_name("padW")
	.dwattr $C$DW$137, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$137, DW_AT_location[DW_OP_regx 0x49]

;* VBM1  assigned to strideH
$C$DW$138	.dwtag  DW_TAG_variable
	.dwattr $C$DW$138, DW_AT_name("strideH")
	.dwattr $C$DW$138, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$138, DW_AT_location[DW_OP_regx 0x59]

;* AM0   assigned to strideW
$C$DW$139	.dwtag  DW_TAG_variable
	.dwattr $C$DW$139, DW_AT_name("strideW")
	.dwattr $C$DW$139, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$139, DW_AT_location[DW_OP_reg24]

;* A9    assigned to kernelH
$C$DW$140	.dwtag  DW_TAG_variable
	.dwattr $C$DW$140, DW_AT_name("kernelH")
	.dwattr $C$DW$140, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$140, DW_AT_location[DW_OP_reg9]

;* A12   assigned to kernelW
$C$DW$141	.dwtag  DW_TAG_variable
	.dwattr $C$DW$141, DW_AT_name("kernelW")
	.dwattr $C$DW$141, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$141, DW_AT_location[DW_OP_reg12]

;* A2    assigned to numOutChannels
$C$DW$142	.dwtag  DW_TAG_variable
	.dwattr $C$DW$142, DW_AT_name("numOutChannels")
	.dwattr $C$DW$142, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$142, DW_AT_location[DW_OP_reg2]

;* A10   assigned to outChPitch
$C$DW$143	.dwtag  DW_TAG_variable
	.dwattr $C$DW$143, DW_AT_name("outChPitch")
	.dwattr $C$DW$143, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$143, DW_AT_location[DW_OP_reg10]

;* VBM2  assigned to outPitch
$C$DW$144	.dwtag  DW_TAG_variable
	.dwattr $C$DW$144, DW_AT_name("outPitch")
	.dwattr $C$DW$144, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$144, DW_AT_location[DW_OP_regx 0x5a]

;* A8    assigned to inChPitch
$C$DW$145	.dwtag  DW_TAG_variable
	.dwattr $C$DW$145, DW_AT_name("inChPitch")
	.dwattr $C$DW$145, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$145, DW_AT_location[DW_OP_reg8]

;* VB2   assigned to inPitch
$C$DW$146	.dwtag  DW_TAG_variable
	.dwattr $C$DW$146, DW_AT_name("inPitch")
	.dwattr $C$DW$146, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$146, DW_AT_location[DW_OP_regx 0x32]

;* A4    assigned to pInChannel
$C$DW$147	.dwtag  DW_TAG_variable
	.dwattr $C$DW$147, DW_AT_name("pInChannel")
	.dwattr $C$DW$147, DW_AT_type(*$C$DW$T$362)
	.dwattr $C$DW$147, DW_AT_location[DW_OP_reg4]

;* VB0   assigned to $O$CSE$s32x4$012
;* VB7   assigned to $O$CSE$s32x2$013
;* VB4   assigned to $O$CSE$s32x2$014
;* VBM3  assigned to $O$CSE$s32x4$015
;* VB6   assigned to $O$CSE$s32x2$016
;* VBL1  assigned to $O$CSE$s32x2$017
;* VB5   assigned to $O$CSE$s32x4$019
;* VB5   assigned to $O$CSE$s32x2$020
;* VB3   assigned to $O$CSE$s32x2$021
;* VBM0  assigned to $O$CSE$s32x4$022
;* VB0   assigned to $O$CSE$s32x2$023
;* VBL0  assigned to $O$CSE$s32x2$024
;* VB0   assigned to $O$VEC$s32x16$010
	.dwcfi	cfa_offset, 0
;          EXCLUSIVE CPU CYCLES: 25

           STD     .D1     A9,*SP(8)         ; [A_D1] 
||         STD     .D2X    A11,*SP++(-24)    ; [A_D2] 

	.dwcfi	save_reg_to_mem, 9, 8
	.dwcfi	cfa_offset, 24
	.dwcfi	save_reg_to_mem, 11, -24
           LDD     .D1     *SP(72),B0        ; [A_D1] |442| 
	.dwpsn	file "tidl_custom_maxpooling.c",line 447,column 15,is_stmt,isa 0
           ADDD    .L1X    B0,0xf0,A1        ; [A_L1] |447| 

           LDW     .D1     *A1(0),B4         ; [A_D1] |447| 
||         ADDD    .L1X    B0,0x6d0,A0       ; [A_L1] |448| 

	.dwpsn	file "tidl_custom_maxpooling.c",line 442,column 1,is_stmt,isa 0

           LDW     .D1     *A0(0),B1         ; [A_D1] |448| 
||         ADDD    .L1     A0,0xffffffe0,D0  ; [A_L1] |448| 
||         LDW     .D2     *SP(56),AM0       ; [A_D2] |442| 

	.dwpsn	file "tidl_custom_maxpooling.c",line 448,column 16,is_stmt,isa 0
           LDW     .D1     *D0(0),AM1        ; [A_D1] |448| 
	.dwpsn	file "tidl_custom_maxpooling.c",line 447,column 15,is_stmt,isa 0
           ADDD    .D1     A1,0xfffffffc,D0  ; [A_D1] |447| 
	.dwpsn	file "tidl_custom_maxpooling.c",line 442,column 1,is_stmt,isa 0

           LDW     .D1     *D0(0),BL0        ; [A_D1] |447| 
||         LDW     .D2     *SP(52),BL1       ; [A_D2] |442| 

	.dwpsn	file "tidl_custom_maxpooling.c",line 448,column 16,is_stmt,isa 0
           ADDD    .D1     A0,0xffffffdc,D1  ; [A_D1] |448| 
           LDW     .D1     *D1(0),D2         ; [A_D1] |448| 
	.dwpsn	file "tidl_custom_maxpooling.c",line 442,column 1,is_stmt,isa 0

           SUBW    .M1X    B4,AM0,AM0        ; [A_M1] |447| 
||         LDD     .D1     *SP(64),B3        ; [A_D1] |442| 
||         MV      .L2X    A7,B2             ; [B_L2] |442| 

	.dwpsn	file "tidl_custom_maxpooling.c",line 448,column 16,is_stmt,isa 0
           MPYWW   .N1X    B1,AM1,D0         ; [A_N1] |448| 
	.dwpsn	file "tidl_custom_maxpooling.c",line 447,column 15,is_stmt,isa 0
           MPYWW   .N1X    B2,AM0,D1         ; [A_N1] |447| 
           SUBW    .L2     BL0,BL1,B1        ; [B_L2] |447| 
	.dwpsn	file "tidl_custom_maxpooling.c",line 442,column 1,is_stmt,isa 0

           EXT     .L1X    B1,0x20,0x20,D3   ; [A_L1] |447| 
||         ADDAB   .D1     D2,D0,AL0         ; [A_D1] |448| 
||         LDUH    .D2     *SP(60),BL2       ; [A_D2] |442| 

           ADDAB   .D1     D3,D1,AL1         ; [A_D1] |447| 
||         SHLD    .L1     AL0,0x1,AM1       ; [A_L1] |448| 
||         LDW     .D2     *SP(48),BM1       ; [A_D2] |442| 
||         MV      .L2X    A9,BM2            ; [B_L2] |442| 
||         MV      .S1     A11,A2            ; [A_S1] |442| 

           SHLD    .L1     AL1,0x1,D0        ; [A_L1] |447| 
||         ADDD    .M1X    B3,AM1,D4         ; [A_M1] |448| 
||         CMPGTW  .S1     A2,0,A0           ; [A_S1] |450| 
||         LDW     .D1     *SP(40),A9        ; [A_D1] |442| 
||         STD     .D2X    A13,*SP(16)       ; [A_D2] 
	.dwcfi	save_reg_to_mem, 13, 16

   [!A0]   B       .B1     ||$C$L32||        ; [A_B] |450| 
||         ADDD    .D1     A4,D0,D2          ; [A_D1] |447| 
||         ADDD    .S1X    B0,0x6dc,D1       ; [A_S1] 
||         LDW     .D2     *SP(44),AM0       ; [A_D2] |442| 
||         MVKU32  .L1     0,A7              ; [A_L1] |450| 

	.dwpsn	file "tidl_custom_maxpooling.c",line 450,column 16,is_stmt,isa 0
           ; BRANCHCC OCCURS {||$C$L32||}    ; [] |450| 
;** --------------------------------------------------------------------------*
;**   BEGIN LOOP ||$C$L17||
;** --------------------------------------------------------------------------*
||$C$L17||:    
;          EXCLUSIVE CPU CYCLES: 9
	.dwpsn	file "tidl_custom_maxpooling.c",line 456,column 18,is_stmt,isa 0
           LDW     .D1     *D1(0),B0         ; [A_D1] |456| 
	.dwpsn	file "tidl_custom_maxpooling.c",line 455,column 5,is_stmt,isa 0
           LDUW    .D1     *D1(4),A11        ; [A_D1] |455| 
	.dwpsn	file "tidl_custom_maxpooling.c",line 456,column 18,is_stmt,isa 0
           CMPEQW  .L1X    B0,0,A0           ; [A_L1] |456| 
   [ A0]   B       .B1     ||$C$L31||        ; [A_B] |456| 
           ; BRANCHCC OCCURS {||$C$L31||}    ; [] |456| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 1
	.dwpsn	file "tidl_custom_maxpooling.c",line 456,column 10,is_stmt,isa 0

           MVKU32  .L2     0,B1              ; [B_L2] |456| 
||         MV      .L1X    B0,A1             ; [A_L1] 

;** --------------------------------------------------------------------------*
;**   BEGIN LOOP ||$C$L18||
;** --------------------------------------------------------------------------*
||$C$L18||:    
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "tidl_custom_maxpooling.c",line 458,column 20,is_stmt,isa 0
           CMPEQW  .L1     A11,0,A0          ; [A_L1] |458| 
   [ A0]   B       .B1     ||$C$L30||        ; [A_B] |458| 
           ; BRANCHCC OCCURS {||$C$L30||}    ; [] |458| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 7
           MPYWW   .N2     BM2,B1,B0         ; [B_N2] 
           MPYWW   .N1     A10,A7,AM1        ; [A_N1] 
           ADDW    .M1X    B0,AM1,D0         ; [A_M1] 
	.dwpsn	file "tidl_custom_maxpooling.c",line 458,column 12,is_stmt,isa 0

           ADDAH   .D1     D4,D0,D3          ; [A_D1] 
||         MVKU32  .L1     0,A6              ; [A_L1] |458| 
||         MV      .D2     A11,A4            ; [A_D2] 

;** --------------------------------------------------------------------------*
;**   BEGIN LOOP ||$C$L19||
;** --------------------------------------------------------------------------*
||$C$L19||:    
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "tidl_custom_maxpooling.c",line 461,column 22,is_stmt,isa 0
           CMPGTW  .L1     A9,0,A0           ; [A_L1] |461| 

   [!A0]   B       .B1     ||$C$L29||        ; [A_B] |461| 
||         MV      .L2     BL2,B3            ; [B_L2] 
||         MV      .S2     BL2,B0            ; [B_S2] 

           ; BRANCHCC OCCURS {||$C$L29||}    ; [] |461| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 1
	.dwpsn	file "tidl_custom_maxpooling.c",line 461,column 14,is_stmt,isa 0

           MVKU32  .L2     0,BL3             ; [B_L2] |461| 
||         MV      .D1     A9,A3             ; [A_D1] 

;** --------------------------------------------------------------------------*
;**   BEGIN LOOP ||$C$L20||
;** --------------------------------------------------------------------------*
||$C$L20||:    
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "tidl_custom_maxpooling.c",line 463,column 24,is_stmt,isa 0
           CMPGTW  .L1     A12,0,A0          ; [A_L1] |463| 
   [!A0]   B       .B1     ||$C$L28||        ; [A_B] |463| 
           ; BRANCHCC OCCURS {||$C$L28||}    ; [] |463| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 3
	.dwpsn	file "tidl_custom_maxpooling.c",line 465,column 13,is_stmt,isa 0
           VDUPW   .L2     B0,VB0            ; [B_L2] |465| 
           CMPGEW  .L1     A12,0x10,A0       ; [A_L1] 
	.dwpsn	file "tidl_custom_maxpooling.c",line 463,column 16,is_stmt,isa 0

   [!A0]   B       .B1     ||$C$L24||        ; [A_B] 
|| [!A0]   MVKU32  .L1     0,D5              ; [A_L1] |463| 
||         ANDW    .S1     A12,0xf,A5        ; [A_S1] 

           ; BRANCHCC OCCURS {||$C$L24||}    ; [] 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 15
           MPYWW   .N2     BM1,B1,BL0        ; [B_N2] 
           ADDW    .L2     BL3,BL0,BM0       ; [B_L2] 
           MPYWW   .N2     B2,BM0,B3         ; [B_N2] 
           MPYWW   .N1     A8,A7,AM1         ; [A_N1] 
           MPYWW   .N1     AM0,A6,D5         ; [A_N1] 
           ADDW    .M1X    B3,AM1,D0         ; [A_M1] 
           ADDW    .D1     D5,D0,D0          ; [A_D1] 

           ADDAH   .D1     D2,D0,A13         ; [A_D1] 
||         SHRW    .L1     A12,0x4,AL0       ; [A_L1] 

           MV      .D1     A13,D0            ; [A_D1] 
||         EXT     .L1     AL0,0x20,0x20,AL0 ; [A_L1] 

	.dwpsn	file "tidl_custom_maxpooling.c",line 463,column 24,is_stmt,isa 0

           ADDD    .D1     D0,0xffffffe0,D0  ; [A_D1] undo prolog elim. side-effects
||         ANDW    .M1     A12,0xfffffff0,D5 ; [A_M1] |463| 
||         NLCINIT .S1     AL0,0x1,10        ; [A_S1] 
||         UNPROT          0x1               ; [A_U] 
||         VMV     .L2     VB0,VBL1          ; [B_L2] 
||         MVKU32  .L1     0xb,A0            ; [A_L1] init prolog collapse predicate

;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*
;*      Loop found in file               : tidl_custom_maxpooling.c
;*      Loop source line                 : 463
;*      Loop opening brace source line   : 464
;*      Loop closing brace source line   : 470
;*      Loop Unroll Multiple             : 16x
;*      Known Minimum Trip Count         : 1                    
;*      Known Max Trip Count Factor      : 1
;*      Loop Carried Dependency Bound(^) : 0
;*      Unpartitioned Resource Bound     : 1
;*      Partitioned Resource Bound       : 1 (pre-sched)
;*
;*      Searching for software pipeline schedule at ...
;*         ii = 1  Schedule found with 12 iterations in parallel
;*
;*      Partitioned Resource Bound(*)    : 1 (post-sched)
;*
;*      Constant Extension #0 Used [C0]  : 0
;*      Constant Extension #1 Used [C1]  : 0
;*
;*      Resource Partition (may include "post-sched" split/spill moves):
;*
;*                                                A-side   B-side
;*      .L units                                     0        0     
;*      .S units                                     0        0     
;*      .M units                                     0        0     
;*      .N units                                     0        0     
;*      .D units                                     1        -     
;*      .B units                                     1*       -     
;*      .C units                                     -        1*    
;*      .P units                                     -        0     
;*
;*      .M/.N units                                  0        0     
;*      .L/.S units                                  0        0     
;*      .L/.S/.C units                               0        0     
;*      .L/.S/.C/.M units                            0        1     
;*      .L/.S/.C/.M/.D units                         0        0     
;*
;*      .X cross paths                               0        0     
;*
;*      Bound(.D1 .D2 .D)                            1*       -     
;*      Bound(.M .N .MN)                             0        0     
;*      Bound(.L .S .LS)                             0        0     
;*      Bound(.L .S .C .LS .LSC)                     0        1*    
;*      Bound(.L .S .C .M .LS .LSC .LSCM)            0        1*    
;*      Bound(.L .S .C .M .D .LS .LSC .LSCM .LSCMD)  1*       1*    
;*
;*      Register Usage Tables:
;*
;*      +----------------------------------+----------------------------------+
;*      |              ASIDE               |              BSIDE               |
;*      +----------------+--------+--------+----------------+--------+--------+
;*      |      "Axx"     |  "ALx" |  "AMx" |     "VBxx"     | "VBLx" | "VBMx" |
;*      +----------------|--------|--------|----------------|--------|--------+
;*      |0000000000111111|00000000|00000000|0000000000111111|00000000|00000000|
;*      |0123456789012345|01234567|01234567|0123456789012345|01234567|01234567|
;*      +----------------|--------|--------|----------------|--------|--------+
;*   0: |*               |        |        |                |**      |*       |
;*      +----------------+--------+--------+----------------+--------+--------+
;*
;*      +----------------+                 +--------+       +-------+
;*      |      "Dxx"     |                 |  "Px"  |       |"CUCRx"|
;*      +----------------+                 +--------+       +-------+
;*      |0000000000111111|                 |00000000|       |0000   |
;*      |0123456789012345|                 |01234567|       |0123   |
;*      +----------------+                 +--------+       +-------+
;*   0: |*               |                 |        |       |       |
;*      +----------------+                 +--------+       +-------+
;*
;*      Done
;*
;*      Collapsed epilog stages       : 11
;*      Collapsed prolog stages       : 11
;*      Max amt of load speculation   : 320 bytes
;*
;*      Minimum safe trip count       : 1 (after unrolling)
;*      Min. prof. trip count  (est.) : 2 (after unrolling)
;*
;*      Mem bank conflicts/iter(est.) : { min 0.000, est 0.000, max 0.000 }
;*      Mem bank perf. penalty (est.) : 0.0%
;*
;*
;*      Total cycles (est.)         : 11 + trip_cnt * 1        
;*----------------------------------------------------------------------------*
;*        SINGLE SCHEDULED ITERATION
;*
;*        ||$C$C258||:
;*   0              TICK                               ; [A_U] 
;*   1              VLD16H  .D1     *D0++(32),VBM0    ; [A_D1] |465| 
;*   2              NOP     0x5     ; [A_B] 
;*   7              VHUNPKWU .C2    VBM0,VBL0         ; [B_C] |465| 
;*   8              NOP     0x3     ; [A_B] 
;*  11              VMAXW   .L2     VBL1,VBL0,VBL1    ; [B_L2] |465| 
;*     ||           BNL     .B1     ||$C$C258||       ; [A_B] |463| 
;*  12              ; BRANCHCC OCCURS {||$C$C258||}   ; [] |463| 
;*----------------------------------------------------------------------------*
||$C$L21||:    ; PIPED LOOP PROLOG
;** --------------------------------------------------------------------------*
||$C$L22||:    ; PIPED LOOP KERNEL
;          EXCLUSIVE CPU CYCLES: 1

   [ A0]   ADDW    .D2     A0,0xffffffff,A0  ; [A_D2] |463| <0,11> collapsing predicate control
||         BNL     .B1     ||$C$L22||        ; [A_B] |463| <0,11> 
|| [!A0]   VMAXW   .L2     VBL1,VBL0,VBL1    ; [B_L2] |465| <0,11> 
||         VHUNPKWU .C2    VBM0,VBL0         ; [B_C] |465| <4,7> 
||         VLD16H  .D1     *D0++(32),VBM0    ; [A_D1] |465| <10,1> 
||         TICK                               ; [A_U] <11,0> 

;** --------------------------------------------------------------------------*
||$C$L23||:    ; PIPED LOOP EPILOG
;          EXCLUSIVE CPU CYCLES: 1
           PROTCLR                            ; [A_U] VBL1
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 1
           VMV     .L2     VBL1,VB0          ; [B_L2] 
;** --------------------------------------------------------------------------*
||$C$L24||:    
;          EXCLUSIVE CPU CYCLES: 23
	.dwpsn	file "tidl_custom_maxpooling.c",line 465,column 13,is_stmt,isa 0
           VHHMV   .C2     VB0,VB0,VB5       ; [B_C] |465| 
           VGETD   .C2     VB0,0x2,BM5       ; [B_C] |465| 
           VGETD   .C2     VB0,0x3,BM3       ; [B_C] |465| 
           VGETD   .C2     VB5,0x2,BM4       ; [B_C] |465| 
           VGETD   .C2     VB5,0x3,BM0       ; [B_C] |465| 
           VSHFL2DLL .C2   VBM3,VBM5,VBM3    ; [B_C] |465| 
           VSHFL2DLL .C2   VBM0,VBM4,VBM0    ; [B_C] |465| 

           EXTV    .L2     VB5,1,0,0,VB3     ; [B_L2] |465| 
||         EXTV    .S2     VB0,1,0,0,VB4     ; [B_S2] |465| 

           VGETD   .C2     VBM3,1,VBL1       ; [B_C] |465| 
           VGETD   .C2     VBM0,1,VBL0       ; [B_C] |465| 
           MV      .L2     B0,B7             ; [B_L2] |465| 

           MV      .M2     BM3,B6            ; [B_M2] |465| 
||         SHRD    .L2     B4,0x20,B10       ; [B_L2] |465| 
||         SHRD    .S2     B7,0x20,B12       ; [B_S2] |465| 

           SHRD    .L2     B6,0x20,BM6       ; [B_L2] |465| 
||         MV      .C2     B5,B5             ; [B_C] |465| 
||         SHRD    .S2     B3,0x20,B11       ; [B_S2] |465| 
||         VMAXW   .M2     B7,B12,BM7        ; [B_M2] |465| 

           MV      .M2     BM0,B0            ; [B_M2] |465| 
||         VMAXW   .C2     B6,BM6,B9         ; [B_C] |465| 
||         SHRD    .L2     BL1,0x20,BL5      ; [B_L2] |465| 
||         SHRD    .S2     B5,0x20,B13       ; [B_S2] |465| 

           SHRD    .L2     B0,0x20,BM4       ; [B_L2] |465| 
||         VMAXW   .M2     B4,B10,BM3        ; [B_M2] |465| 
||         SHRD    .S2     BL0,0x20,BL4      ; [B_S2] |465| 
||         VMAXW   .C2     B5,B13,BM5        ; [B_C] |465| 

           VMAXW   .M2     B3,B11,BM0        ; [B_M2] |465| 
||         VMAXW   .L2     BL0,BL4,BL0       ; [B_L2] |465| 
||         VMAXW   .C2     B0,BM4,BL6        ; [B_C] |465| 
||         VMAXW   .S2     BL1,BL5,BL7       ; [B_S2] |465| 

           VMAXW   .L2     B9,BL7,BL4        ; [B_L2] |465| 
||         VMAXW   .S2     BL6,BL0,BL0       ; [B_S2] |465| 
||         VMAXW   .M2     BM5,BM0,B8        ; [B_M2] |465| 
||         VMAXW   .C2     BM7,BM3,B12       ; [B_C] |465| 

           VMAXW   .L2     B12,BL4,BL1       ; [B_L2] |465| 
||         VMAXW   .S2     B8,BL0,BL0        ; [B_S2] |465| 
||         CMPEQW  .L1     A5,0,A0           ; [A_L1] 

   [ A0]   B       .B1     ||$C$L28||        ; [A_B] 
||         VMAXW   .L2     BL1,BL0,B0        ; [B_L2] |465| 

           ; BRANCHCC OCCURS {||$C$L28||}    ; [] 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 15
; Peeled loop iterations for unrolled loop:
           MPYWW   .N2     BM1,B1,BL0        ; [B_N2] 
           ADDW    .L2     BL3,BL0,BM0       ; [B_L2] 
           MPYWW   .N2     B2,BM0,B3         ; [B_N2] 
           MPYWW   .N1     A8,A7,AM1         ; [A_N1] 
           MPYWW   .N1     AM0,A6,D0         ; [A_N1] 
           ADDW    .M1X    B3,AM1,D6         ; [A_M1] 
           ADDW    .D1     D0,D6,D0          ; [A_D1] 
           ADDW    .D1     D5,D0,D0          ; [A_D1] 

           ADDAH   .D1     D2,D0,D0          ; [A_D1] 
||         EXT     .L1     A5,0x20,0x20,A5   ; [A_L1] 

           ADDD    .D1     D0,0xfffffffe,D0  ; [A_D1] undo prolog elim. side-effects
||         NLCINIT .S1     A5,0x1,6          ; [A_S1] 
||         UNPROT          0x1               ; [A_U] 
||         MV      .L2     B0,BL1            ; [B_L2] 
||         MVKU32  .L1     0x7,A0            ; [A_L1] init prolog collapse predicate

;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*
;*      Loop found in file               : tidl_custom_maxpooling.c
;*      Loop source line                 : 463
;*      Loop opening brace source line   : 464
;*      Loop closing brace source line   : 470
;*      Known Minimum Trip Count         : 1                    
;*      Known Maximum Trip Count         : 15                    
;*      Known Max Trip Count Factor      : 1
;*      Loop Carried Dependency Bound(^) : 0
;*      Unpartitioned Resource Bound     : 1
;*      Partitioned Resource Bound       : 1 (pre-sched)
;*
;*      Searching for software pipeline schedule at ...
;*         ii = 1  Schedule found with 8 iterations in parallel
;*
;*      Partitioned Resource Bound(*)    : 1 (post-sched)
;*
;*      Constant Extension #0 Used [C0]  : 0
;*      Constant Extension #1 Used [C1]  : 0
;*
;*      Resource Partition (may include "post-sched" split/spill moves):
;*
;*                                                A-side   B-side
;*      .L units                                     0        0     
;*      .S units                                     0        0     
;*      .M units                                     0        0     
;*      .N units                                     0        0     
;*      .D units                                     1        -     
;*      .B units                                     1*       -     
;*      .C units                                     -        0     
;*      .P units                                     -        0     
;*
;*      .M/.N units                                  0        0     
;*      .L/.S units                                  0        0     
;*      .L/.S/.C units                               0        0     
;*      .L/.S/.C/.M units                            0        1     
;*      .L/.S/.C/.M/.D units                         0        0     
;*
;*      .X cross paths                               0        0     
;*
;*      Bound(.D1 .D2 .D)                            1*       -     
;*      Bound(.M .N .MN)                             0        0     
;*      Bound(.L .S .LS)                             0        0     
;*      Bound(.L .S .C .LS .LSC)                     0        0     
;*      Bound(.L .S .C .M .LS .LSC .LSCM)            0        1*    
;*      Bound(.L .S .C .M .D .LS .LSC .LSCM .LSCMD)  1*       1*    
;*
;*      Register Usage Tables:
;*
;*      +----------------------------------+----------------------------------+
;*      |              ASIDE               |              BSIDE               |
;*      +----------------+--------+--------+----------------+--------+--------+
;*      |      "Axx"     |  "ALx" |  "AMx" |     "VBxx"     | "VBLx" | "VBMx" |
;*      +----------------|--------|--------|----------------|--------|--------+
;*      |0000000000111111|00000000|00000000|0000000000111111|00000000|00000000|
;*      |0123456789012345|01234567|01234567|0123456789012345|01234567|01234567|
;*      +----------------|--------|--------|----------------|--------|--------+
;*   0: |*               |        |        |                |**      |        |
;*      +----------------+--------+--------+----------------+--------+--------+
;*
;*      +----------------+                 +--------+       +-------+
;*      |      "Dxx"     |                 |  "Px"  |       |"CUCRx"|
;*      +----------------+                 +--------+       +-------+
;*      |0000000000111111|                 |00000000|       |0000   |
;*      |0123456789012345|                 |01234567|       |0123   |
;*      +----------------+                 +--------+       +-------+
;*   0: |*               |                 |        |       |       |
;*      +----------------+                 +--------+       +-------+
;*
;*      Done
;*
;*      Collapsed epilog stages       : 7
;*      Collapsed prolog stages       : 7
;*      Max amt of load speculation   : 12 bytes
;*
;*      Minimum safe trip count       : 1
;*      Min. prof. trip count  (est.) : 2
;*
;*      Mem bank conflicts/iter(est.) : { min 0.000, est 0.000, max 0.000 }
;*      Mem bank perf. penalty (est.) : 0.0%
;*
;*
;*      Total cycles (est.)         : 7 + trip_cnt * 1 = Between 8 and 22        
;*----------------------------------------------------------------------------*
;*        SINGLE SCHEDULED ITERATION
;*
;*        ||$C$C199||:
;*   0              TICK                               ; [A_U] 
;*   1              SLDUH   .D1     *D0++(2),BL0      ; [A_D1] |465| 
;*   2              NOP     0x5     ; [A_B] 
;*   7              VMAXW   .L2     BL1,BL0,BL1       ; [B_L2] |465| 
;*     ||           BNL     .B1     ||$C$C199||       ; [A_B] |463| 
;*   8              ; BRANCHCC OCCURS {||$C$C199||}   ; [] |463| 
;*----------------------------------------------------------------------------*
||$C$L25||:    ; PIPED LOOP PROLOG
;** --------------------------------------------------------------------------*
||$C$L26||:    ; PIPED LOOP KERNEL
;          EXCLUSIVE CPU CYCLES: 1
	.dwpsn	file "tidl_custom_maxpooling.c",line 463,column 24,is_stmt,isa 0

   [ A0]   ADDW    .D2     A0,0xffffffff,A0  ; [A_D2] |463| <0,7> collapsing predicate control
||         BNL     .B1     ||$C$L26||        ; [A_B] |463| <0,7> 
|| [!A0]   VMAXW   .L2     BL1,BL0,BL1       ; [B_L2] |465| <0,7> 
||         SLDUH   .D1     *D0++(2),BL0      ; [A_D1] |465| <6,1> 
||         TICK                               ; [A_U] <7,0> 

;** --------------------------------------------------------------------------*
||$C$L27||:    ; PIPED LOOP EPILOG
;          EXCLUSIVE CPU CYCLES: 1
           PROTCLR                            ; [A_U] VBL1
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 1
           MV      .L2     BL1,B0            ; [B_L2] 
;** --------------------------------------------------------------------------*
||$C$L28||:    
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "tidl_custom_maxpooling.c",line 461,column 22,is_stmt,isa 0
           ADDW    .D1     A3,0xffffffff,A3  ; [A_D1] |461| 

   [ A3]   B       .B1     ||$C$L20||        ; [A_B] |461| 
||         ADDW    .L2     BL3,0x1,BL3       ; [B_L2] |461| 

           ; BRANCHCC OCCURS {||$C$L20||}    ; [] |461| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 1
           MV      .L2     B0,B3             ; [B_L2] 
;** --------------------------------------------------------------------------*
||$C$L29||:    
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "tidl_custom_maxpooling.c",line 458,column 20,is_stmt,isa 0
           ADDW    .D1     A4,0xffffffff,A4  ; [A_D1] |458| 

   [ A4]   B       .B1     ||$C$L19||        ; [A_B] |458| 
||         STH     .D1X    B3,*D3++(2)       ; [A_D1] |472| 
||         ADDW    .D2     A6,0x1,A6         ; [A_D2] |458| 

           ; BRANCHCC OCCURS {||$C$L19||}    ; [] |458| 
;** --------------------------------------------------------------------------*
||$C$L30||:    
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "tidl_custom_maxpooling.c",line 456,column 18,is_stmt,isa 0
           ADDW    .D1     A1,0xffffffff,A1  ; [A_D1] |456| 

   [ A1]   B       .B1     ||$C$L18||        ; [A_B] |456| 
||         ADDW    .L2     B1,0x1,B1         ; [B_L2] |456| 

           ; BRANCHCC OCCURS {||$C$L18||}    ; [] |456| 
;** --------------------------------------------------------------------------*
||$C$L31||:    
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "tidl_custom_maxpooling.c",line 450,column 16,is_stmt,isa 0
           ADDW    .D1     A2,0xffffffff,A2  ; [A_D1] |450| 

   [ A2]   B       .B1     ||$C$L17||        ; [A_B] |450| 
||         ADDW    .D1     A7,0x1,A7         ; [A_D1] |450| 

           ; BRANCHCC OCCURS {||$C$L17||}    ; [] |450| 
;** --------------------------------------------------------------------------*
||$C$L32||:    
;          EXCLUSIVE CPU CYCLES: 7
           LDD     .D1     *SP(16),A13       ; [A_D1] 
	.dwcfi	restore_reg, 13

           LDD     .D1     *SP(32),A9        ; [A_D1] 
||         LDD     .D2     *SP(24),A11       ; [A_D2] 

	.dwcfi	restore_reg, 9
	.dwcfi	restore_reg, 11
$C$DW$148	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$148, DW_AT_low_pc(0x00)
	.dwattr $C$DW$148, DW_AT_TI_return


           RET     .B1     ; [A_B] 
||         ADDD    .D1     SP,0x18,SP        ; [A_D1] 

	.dwcfi	cfa_offset, 0
           ; RETURN OCCURS {RP}              ; [] 
	.dwattr $C$DW$115, DW_AT_TI_end_file("tidl_custom_maxpooling.c")
	.dwattr $C$DW$115, DW_AT_TI_end_line(0x1dc)
	.dwattr $C$DW$115, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$115

	.sect	".text:_Z28TIDL_refCustomMaxPoolingCoreIaEvPT_iiiiiiiiiiiiiS0_S1_P13sTIDL_Layer_t"
	.clink
	.global	||_Z28TIDL_refCustomMaxPoolingCoreIaEvPT_iiiiiiiiiiiiiS0_S1_P13sTIDL_Layer_t||

$C$DW$149	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$149, DW_AT_name("void TIDL_refCustomMaxPoolingCore")
	.dwattr $C$DW$149, DW_AT_low_pc(||_Z28TIDL_refCustomMaxPoolingCoreIaEvPT_iiiiiiiiiiiiiS0_S1_P13sTIDL_Layer_t||)
	.dwattr $C$DW$149, DW_AT_high_pc(0x00)
	.dwattr $C$DW$149, DW_AT_linkage_name("_Z28TIDL_refCustomMaxPoolingCoreIaEvPT_iiiiiiiiiiiiiS0_S1_P13sTIDL_Layer_t")
	.dwattr $C$DW$149, DW_AT_external
	.dwattr $C$DW$149, DW_AT_decl_file("tidl_custom_maxpooling.c")
	.dwattr $C$DW$149, DW_AT_decl_line(0x1a9)
	.dwattr $C$DW$149, DW_AT_decl_column(0x06)
	.dwattr $C$DW$149, DW_AT_TI_max_frame_size(0x18)
	.dwpsn	file "tidl_custom_maxpooling.c",line 442,column 1,is_stmt,address ||_Z28TIDL_refCustomMaxPoolingCoreIaEvPT_iiiiiiiiiiiiiS0_S1_P13sTIDL_Layer_t||,isa 0

	.dwfde $C$DW$CIE, ||_Z28TIDL_refCustomMaxPoolingCoreIaEvPT_iiiiiiiiiiiiiS0_S1_P13sTIDL_Layer_t||
$C$DW$150	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$150, DW_AT_name("pInChannel")
	.dwattr $C$DW$150, DW_AT_location[DW_OP_reg4]

$C$DW$151	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$151, DW_AT_name("width")
	.dwattr $C$DW$151, DW_AT_location[DW_OP_reg5]

$C$DW$152	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$152, DW_AT_name("height")
	.dwattr $C$DW$152, DW_AT_location[DW_OP_reg6]

$C$DW$153	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$153, DW_AT_name("inPitch")
	.dwattr $C$DW$153, DW_AT_location[DW_OP_reg7]

$C$DW$154	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$154, DW_AT_name("inChPitch")
	.dwattr $C$DW$154, DW_AT_location[DW_OP_reg8]

$C$DW$155	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$155, DW_AT_name("outPitch")
	.dwattr $C$DW$155, DW_AT_location[DW_OP_reg9]

$C$DW$156	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$156, DW_AT_name("outChPitch")
	.dwattr $C$DW$156, DW_AT_location[DW_OP_reg10]

$C$DW$157	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$157, DW_AT_name("numOutChannels")
	.dwattr $C$DW$157, DW_AT_location[DW_OP_reg11]

$C$DW$158	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$158, DW_AT_name("kernelW")
	.dwattr $C$DW$158, DW_AT_location[DW_OP_reg12]

$C$DW$159	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$159, DW_AT_name("kernelH")
	.dwattr $C$DW$159, DW_AT_location[DW_OP_bregx 0x6f 40]

$C$DW$160	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$160, DW_AT_name("strideW")
	.dwattr $C$DW$160, DW_AT_location[DW_OP_bregx 0x6f 44]

$C$DW$161	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$161, DW_AT_name("strideH")
	.dwattr $C$DW$161, DW_AT_location[DW_OP_bregx 0x6f 48]

$C$DW$162	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$162, DW_AT_name("padW")
	.dwattr $C$DW$162, DW_AT_location[DW_OP_bregx 0x6f 52]

$C$DW$163	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$163, DW_AT_name("padH")
	.dwattr $C$DW$163, DW_AT_location[DW_OP_bregx 0x6f 56]

$C$DW$164	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$164, DW_AT_name("initValue")
	.dwattr $C$DW$164, DW_AT_location[DW_OP_bregx 0x6f 60]

$C$DW$165	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$165, DW_AT_name("pOutChannel")
	.dwattr $C$DW$165, DW_AT_location[DW_OP_bregx 0x6f 64]

$C$DW$166	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$166, DW_AT_name("tidlLayer")
	.dwattr $C$DW$166, DW_AT_location[DW_OP_bregx 0x6f 72]


;******************************************************************************
;* FUNCTION NAME: void TIDL_refCustomMaxPoolingCore<signed char>(T1 *, int, int, int, int, int, int, int, int, int, int, int, int, int, T1, T1 *, sTIDL_Layer_t *)*
;*                                                                            *
;*   Regs Modified     : A0,A1,A2,A3,A4,A5,A6,A7,A9,A11,A13,VB0,VB1,VB2,VB3,  *
;*                           VB4,VB5,VB6,VB7,VB8,VB9,VB10,VB11,VB12,VB13,AL0, *
;*                           AM0,AM1,AM2,D0,D1,D2,D3,D4,D5,D6,SP,VBL0,VBL1,   *
;*                           VBL2,VBL3,VBL4,VBL5,VBL6,VBL7,VBM0,VBM1,VBM2,    *
;*                           VBM3,VBM4,VBM5,VBM6,VBM7                         *
;*   Regs Used         : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,VB0,   *
;*                           VB1,VB2,VB3,VB4,VB5,VB6,VB7,VB8,VB9,VB10,VB11,   *
;*                           VB12,VB13,AL0,AM0,AM1,AM2,D0,D1,D2,D3,D4,D5,D6,  *
;*                           SP,VBL0,VBL1,VBL2,VBL3,VBL4,VBL5,VBL6,VBL7,VBM0, *
;*                           VBM1,VBM2,VBM3,VBM4,VBM5,VBM6,VBM7               *
;*   Local Frame Size  : 0 Args + 0 Auto + 24 Save = 24 byte                  *
;******************************************************************************
||_Z28TIDL_refCustomMaxPoolingCoreIaEvPT_iiiiiiiiiiiiiS0_S1_P13sTIDL_Layer_t||:
;** --------------------------------------------------------------------------*
;* VB0   assigned to $O$C3
;* A0    assigned to $O$C4
;* D0    assigned to $O$C5
;* D0    assigned to $O$U84
;* A13   assigned to $O$U84
;* D3    assigned to $O$K49
;* A5    assigned to $O$K157
;* D4    assigned to $O$U175
;* A2    assigned to $O$L1
;* A1    assigned to $O$L2
;* A4    assigned to $O$L3
;* A3    assigned to $O$L4
;* AL0   assigned to $O$L5
;* A5    assigned to $O$L6
;* D1    assigned to $O$Lr81$inData
;* D2    assigned to $O$Lr97$outData
;* A7    assigned to $O$Lr17$i1
;* A11   assigned to $O$Lr68$numCols
;* VB1   assigned to $O$Lr20$i2
;* A6    assigned to $O$Lr30$i3
;* VBL3  assigned to $O$Lr34$i4
;* VB0   assigned to $O$Lr49$maxValue
;* VB3   assigned to $O$Lr15$maxValue
;* D5    assigned to $O$Lr51$i5
;* VB0   assigned to tidlLayer
$C$DW$167	.dwtag  DW_TAG_variable
	.dwattr $C$DW$167, DW_AT_name("tidlLayer")
	.dwattr $C$DW$167, DW_AT_type(*$C$DW$T$354)
	.dwattr $C$DW$167, DW_AT_location[DW_OP_regx 0x30]

;* D2    assigned to pOutChannel
$C$DW$168	.dwtag  DW_TAG_variable
	.dwattr $C$DW$168, DW_AT_name("pOutChannel")
	.dwattr $C$DW$168, DW_AT_type(*$C$DW$T$358)
	.dwattr $C$DW$168, DW_AT_location[DW_OP_regx 0x62]

;* VBL2  assigned to initValue
$C$DW$169	.dwtag  DW_TAG_variable
	.dwattr $C$DW$169, DW_AT_name("initValue")
	.dwattr $C$DW$169, DW_AT_type(*$C$DW$T$357)
	.dwattr $C$DW$169, DW_AT_location[DW_OP_regx 0x4a]

;* AM0   assigned to padH
$C$DW$170	.dwtag  DW_TAG_variable
	.dwattr $C$DW$170, DW_AT_name("padH")
	.dwattr $C$DW$170, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$170, DW_AT_location[DW_OP_reg24]

;* AM1   assigned to padW
$C$DW$171	.dwtag  DW_TAG_variable
	.dwattr $C$DW$171, DW_AT_name("padW")
	.dwattr $C$DW$171, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$171, DW_AT_location[DW_OP_reg25]

;* VBM0  assigned to strideH
$C$DW$172	.dwtag  DW_TAG_variable
	.dwattr $C$DW$172, DW_AT_name("strideH")
	.dwattr $C$DW$172, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$172, DW_AT_location[DW_OP_regx 0x58]

;* AM0   assigned to strideW
$C$DW$173	.dwtag  DW_TAG_variable
	.dwattr $C$DW$173, DW_AT_name("strideW")
	.dwattr $C$DW$173, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$173, DW_AT_location[DW_OP_reg24]

;* A9    assigned to kernelH
$C$DW$174	.dwtag  DW_TAG_variable
	.dwattr $C$DW$174, DW_AT_name("kernelH")
	.dwattr $C$DW$174, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$174, DW_AT_location[DW_OP_reg9]

;* A12   assigned to kernelW
$C$DW$175	.dwtag  DW_TAG_variable
	.dwattr $C$DW$175, DW_AT_name("kernelW")
	.dwattr $C$DW$175, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$175, DW_AT_location[DW_OP_reg12]

;* A2    assigned to numOutChannels
$C$DW$176	.dwtag  DW_TAG_variable
	.dwattr $C$DW$176, DW_AT_name("numOutChannels")
	.dwattr $C$DW$176, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$176, DW_AT_location[DW_OP_reg2]

;* A10   assigned to outChPitch
$C$DW$177	.dwtag  DW_TAG_variable
	.dwattr $C$DW$177, DW_AT_name("outChPitch")
	.dwattr $C$DW$177, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$177, DW_AT_location[DW_OP_reg10]

;* VBM1  assigned to outPitch
$C$DW$178	.dwtag  DW_TAG_variable
	.dwattr $C$DW$178, DW_AT_name("outPitch")
	.dwattr $C$DW$178, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$178, DW_AT_location[DW_OP_regx 0x59]

;* A8    assigned to inChPitch
$C$DW$179	.dwtag  DW_TAG_variable
	.dwattr $C$DW$179, DW_AT_name("inChPitch")
	.dwattr $C$DW$179, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$179, DW_AT_location[DW_OP_reg8]

;* VB2   assigned to inPitch
$C$DW$180	.dwtag  DW_TAG_variable
	.dwattr $C$DW$180, DW_AT_name("inPitch")
	.dwattr $C$DW$180, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$180, DW_AT_location[DW_OP_regx 0x32]

;* A4    assigned to pInChannel
$C$DW$181	.dwtag  DW_TAG_variable
	.dwattr $C$DW$181, DW_AT_name("pInChannel")
	.dwattr $C$DW$181, DW_AT_type(*$C$DW$T$358)
	.dwattr $C$DW$181, DW_AT_location[DW_OP_reg4]

;* VB0   assigned to $O$CSE$s32x4$010
;* VB7   assigned to $O$CSE$s32x2$011
;* VB4   assigned to $O$CSE$s32x2$012
;* VBM3  assigned to $O$CSE$s32x4$013
;* VB6   assigned to $O$CSE$s32x2$014
;* VBL1  assigned to $O$CSE$s32x2$015
;* VB5   assigned to $O$CSE$s32x4$017
;* VB5   assigned to $O$CSE$s32x2$018
;* VB3   assigned to $O$CSE$s32x2$019
;* VBM2  assigned to $O$CSE$s32x4$020
;* VB0   assigned to $O$CSE$s32x2$021
;* VBL0  assigned to $O$CSE$s32x2$022
;* VB0   assigned to $O$VEC$s32x16$008
	.dwcfi	cfa_offset, 0
;          EXCLUSIVE CPU CYCLES: 24

           STD     .D1     A9,*SP(8)         ; [A_D1] 
||         STD     .D2X    A11,*SP++(-24)    ; [A_D2] 

	.dwcfi	save_reg_to_mem, 9, 8
	.dwcfi	cfa_offset, 24
	.dwcfi	save_reg_to_mem, 11, -24
           LDD     .D1     *SP(72),B0        ; [A_D1] |442| 
	.dwpsn	file "tidl_custom_maxpooling.c",line 447,column 15,is_stmt,isa 0
           ADDD    .L1X    B0,0xf0,D0        ; [A_L1] |447| 

           LDW     .D1     *D0(0),B1         ; [A_D1] |447| 
||         ADDD    .L1X    B0,0x6d0,A0       ; [A_L1] |448| 

	.dwpsn	file "tidl_custom_maxpooling.c",line 442,column 1,is_stmt,isa 0

           ADDD    .D2     A0,0xffffffe0,D1  ; [A_D2] |448| 
||         LDW     .D1     *SP(56),AM0       ; [A_D1] |442| 

	.dwpsn	file "tidl_custom_maxpooling.c",line 448,column 16,is_stmt,isa 0

           LDW     .D1     *A0(0),BM2        ; [A_D1] |448| 
||         LDW     .D2     *D1(0),BM0        ; [A_D2] |448| 

	.dwpsn	file "tidl_custom_maxpooling.c",line 447,column 15,is_stmt,isa 0
           ADDD    .D1     D0,0xfffffffc,D1  ; [A_D1] |447| 
           LDW     .D1     *D1(0),B3         ; [A_D1] |447| 
	.dwpsn	file "tidl_custom_maxpooling.c",line 442,column 1,is_stmt,isa 0
           LDW     .D1     *SP(52),AM1       ; [A_D1] |442| 

           ADDD    .D1     A0,0xffffffdc,D0  ; [A_D1] |448| 
||         MV      .L2X    A7,B2             ; [B_L2] |442| 

	.dwpsn	file "tidl_custom_maxpooling.c",line 447,column 15,is_stmt,isa 0

           SUBW    .M1X    B1,AM0,AM0        ; [A_M1] |447| 
||         LDW     .D1     *D0(0),AM2        ; [A_D1] |448| 

	.dwpsn	file "tidl_custom_maxpooling.c",line 442,column 1,is_stmt,isa 0

           MPYWW   .N2     BM2,BM0,B1        ; [B_N2] |448| 
||         MPYWW   .N1X    B2,AM0,D0         ; [A_N1] |447| 
||         LDD     .D1     *SP(64),D2        ; [A_D1] |442| 

           SUBW    .M1X    B3,AM1,D1         ; [A_M1] |447| 
||         STD     .D1     A13,*SP(16)       ; [A_D1] 
||         LDB     .D2     *SP(60),BL2       ; [A_D2] |442| 
	.dwcfi	save_reg_to_mem, 13, 16

           ADDW    .D1     D0,D1,D1          ; [A_D1] |447| 
||         LDW     .D2     *SP(40),A9        ; [A_D2] |442| 
||         MV      .L1     A11,A2            ; [A_L1] |442| 
||         MV      .L2X    A9,BM1            ; [B_L2] |442| 

           ADDW    .M1X    B1,AM2,D0         ; [A_M1] |448| 
||         ADDAB   .D1     A4,D1,D1          ; [A_D1] |447| 
||         LDW     .D2     *SP(44),AM0       ; [A_D2] |442| 
||         CMPGTW  .L1     A2,0,A0           ; [A_L1] |450| 

   [!A0]   B       .B1     ||$C$L48||        ; [A_B] |450| 
||         ADDAB   .D1     D2,D0,D2          ; [A_D1] |448| 
||         LDW     .D2     *SP(48),BM0       ; [A_D2] |442| 
||         ADDD    .S1X    B0,0x6dc,D3       ; [A_S1] 
||         MVKU32  .L1     0,A7              ; [A_L1] |450| 

	.dwpsn	file "tidl_custom_maxpooling.c",line 450,column 16,is_stmt,isa 0
           ; BRANCHCC OCCURS {||$C$L48||}    ; [] |450| 
;** --------------------------------------------------------------------------*
;**   BEGIN LOOP ||$C$L33||
;** --------------------------------------------------------------------------*
||$C$L33||:    
;          EXCLUSIVE CPU CYCLES: 9
	.dwpsn	file "tidl_custom_maxpooling.c",line 456,column 18,is_stmt,isa 0
           LDW     .D1     *D3(0),B0         ; [A_D1] |456| 
	.dwpsn	file "tidl_custom_maxpooling.c",line 455,column 5,is_stmt,isa 0
           LDUW    .D1     *D3(4),A11        ; [A_D1] |455| 
	.dwpsn	file "tidl_custom_maxpooling.c",line 456,column 18,is_stmt,isa 0
           CMPEQW  .L1X    B0,0,A0           ; [A_L1] |456| 
   [ A0]   B       .B1     ||$C$L47||        ; [A_B] |456| 
           ; BRANCHCC OCCURS {||$C$L47||}    ; [] |456| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 1
	.dwpsn	file "tidl_custom_maxpooling.c",line 456,column 10,is_stmt,isa 0

           MVKU32  .L2     0,B1              ; [B_L2] |456| 
||         MV      .L1X    B0,A1             ; [A_L1] 

;** --------------------------------------------------------------------------*
;**   BEGIN LOOP ||$C$L34||
;** --------------------------------------------------------------------------*
||$C$L34||:    
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "tidl_custom_maxpooling.c",line 458,column 20,is_stmt,isa 0
           CMPEQW  .L1     A11,0,A0          ; [A_L1] |458| 
   [ A0]   B       .B1     ||$C$L46||        ; [A_B] |458| 
           ; BRANCHCC OCCURS {||$C$L46||}    ; [] |458| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 8
           MPYWW   .N2     BM1,B1,B0         ; [B_N2] 
           MPYWW   .N1     A10,A7,D4         ; [A_N1] 
           MV      .L1X    B0,D0             ; [A_L1] Define a twin register
           ADDW    .D1     D4,D0,D0          ; [A_D1] 
	.dwpsn	file "tidl_custom_maxpooling.c",line 458,column 12,is_stmt,isa 0

           ADDAB   .D1     D2,D0,D4          ; [A_D1] 
||         MVKU32  .L1     0,A6              ; [A_L1] |458| 
||         MV      .D2     A11,A4            ; [A_D2] 

;** --------------------------------------------------------------------------*
;**   BEGIN LOOP ||$C$L35||
;** --------------------------------------------------------------------------*
||$C$L35||:    
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "tidl_custom_maxpooling.c",line 461,column 22,is_stmt,isa 0
           CMPGTW  .L1     A9,0,A0           ; [A_L1] |461| 

   [!A0]   B       .B1     ||$C$L45||        ; [A_B] |461| 
||         MV      .L2     BL2,B3            ; [B_L2] 
||         MV      .S2     BL2,B0            ; [B_S2] 

           ; BRANCHCC OCCURS {||$C$L45||}    ; [] |461| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 1
	.dwpsn	file "tidl_custom_maxpooling.c",line 461,column 14,is_stmt,isa 0

           MVKU32  .L2     0,BL3             ; [B_L2] |461| 
||         MV      .D1     A9,A3             ; [A_D1] 

;** --------------------------------------------------------------------------*
;**   BEGIN LOOP ||$C$L36||
;** --------------------------------------------------------------------------*
||$C$L36||:    
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "tidl_custom_maxpooling.c",line 463,column 24,is_stmt,isa 0
           CMPGTW  .L1     A12,0,A0          ; [A_L1] |463| 
   [!A0]   B       .B1     ||$C$L44||        ; [A_B] |463| 
           ; BRANCHCC OCCURS {||$C$L44||}    ; [] |463| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 3
	.dwpsn	file "tidl_custom_maxpooling.c",line 465,column 13,is_stmt,isa 0
           VDUPW   .L2     B0,VB0            ; [B_L2] |465| 
           CMPGEW  .L1     A12,0x10,A0       ; [A_L1] 
	.dwpsn	file "tidl_custom_maxpooling.c",line 463,column 16,is_stmt,isa 0

   [!A0]   B       .B1     ||$C$L40||        ; [A_B] 
|| [!A0]   MVKU32  .L1     0,D5              ; [A_L1] |463| 
||         ANDW    .S1     A12,0xf,A5        ; [A_S1] 

           ; BRANCHCC OCCURS {||$C$L40||}    ; [] 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 15
           MPYWW   .N2     BM0,B1,BL0        ; [B_N2] 
           ADDW    .L2     BL3,BL0,BM2       ; [B_L2] 
           MPYWW   .N2     B2,BM2,B3         ; [B_N2] 
           MPYWW   .N1     AM0,A6,AM1        ; [A_N1] 
           MPYWW   .N1     A8,A7,D5          ; [A_N1] 
           ADDW    .M1X    B3,AM1,D0         ; [A_M1] 
           ADDW    .D1     D5,D0,D0          ; [A_D1] 

           ADDAB   .D1     D1,D0,A13         ; [A_D1] 
||         SHRW    .L1     A12,0x4,AL0       ; [A_L1] 

           MV      .D1     A13,D0            ; [A_D1] 
||         EXT     .L1     AL0,0x20,0x20,AL0 ; [A_L1] 

	.dwpsn	file "tidl_custom_maxpooling.c",line 463,column 24,is_stmt,isa 0

           ADDD    .D1     D0,0xfffffff0,D0  ; [A_D1] undo prolog elim. side-effects
||         ANDW    .M1     A12,0xfffffff0,D5 ; [A_M1] |463| 
||         NLCINIT .S1     AL0,0x1,6         ; [A_S1] 
||         UNPROT          0x1               ; [A_U] 
||         VMV     .L2     VB0,VBL1          ; [B_L2] 
||         MVKU32  .L1     0x7,A0            ; [A_L1] init prolog collapse predicate

;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*
;*      Loop found in file               : tidl_custom_maxpooling.c
;*      Loop source line                 : 463
;*      Loop opening brace source line   : 464
;*      Loop closing brace source line   : 470
;*      Loop Unroll Multiple             : 16x
;*      Known Minimum Trip Count         : 1                    
;*      Known Max Trip Count Factor      : 1
;*      Loop Carried Dependency Bound(^) : 0
;*      Unpartitioned Resource Bound     : 1
;*      Partitioned Resource Bound       : 1 (pre-sched)
;*
;*      Searching for software pipeline schedule at ...
;*         ii = 1  Schedule found with 8 iterations in parallel
;*
;*      Partitioned Resource Bound(*)    : 1 (post-sched)
;*
;*      Constant Extension #0 Used [C0]  : 0
;*      Constant Extension #1 Used [C1]  : 0
;*
;*      Resource Partition (may include "post-sched" split/spill moves):
;*
;*                                                A-side   B-side
;*      .L units                                     0        0     
;*      .S units                                     0        0     
;*      .M units                                     0        0     
;*      .N units                                     0        0     
;*      .D units                                     1        -     
;*      .B units                                     1*       -     
;*      .C units                                     -        0     
;*      .P units                                     -        0     
;*
;*      .M/.N units                                  0        0     
;*      .L/.S units                                  0        0     
;*      .L/.S/.C units                               0        0     
;*      .L/.S/.C/.M units                            0        1     
;*      .L/.S/.C/.M/.D units                         0        0     
;*
;*      .X cross paths                               0        0     
;*
;*      Bound(.D1 .D2 .D)                            1*       -     
;*      Bound(.M .N .MN)                             0        0     
;*      Bound(.L .S .LS)                             0        0     
;*      Bound(.L .S .C .LS .LSC)                     0        0     
;*      Bound(.L .S .C .M .LS .LSC .LSCM)            0        1*    
;*      Bound(.L .S .C .M .D .LS .LSC .LSCM .LSCMD)  1*       1*    
;*
;*      Register Usage Tables:
;*
;*      +----------------------------------+----------------------------------+
;*      |              ASIDE               |              BSIDE               |
;*      +----------------+--------+--------+----------------+--------+--------+
;*      |      "Axx"     |  "ALx" |  "AMx" |     "VBxx"     | "VBLx" | "VBMx" |
;*      +----------------|--------|--------|----------------|--------|--------+
;*      |0000000000111111|00000000|00000000|0000000000111111|00000000|00000000|
;*      |0123456789012345|01234567|01234567|0123456789012345|01234567|01234567|
;*      +----------------|--------|--------|----------------|--------|--------+
;*   0: |*               |        |        |                |**      |        |
;*      +----------------+--------+--------+----------------+--------+--------+
;*
;*      +----------------+                 +--------+       +-------+
;*      |      "Dxx"     |                 |  "Px"  |       |"CUCRx"|
;*      +----------------+                 +--------+       +-------+
;*      |0000000000111111|                 |00000000|       |0000   |
;*      |0123456789012345|                 |01234567|       |0123   |
;*      +----------------+                 +--------+       +-------+
;*   0: |*               |                 |        |       |       |
;*      +----------------+                 +--------+       +-------+
;*
;*      Done
;*
;*      Collapsed epilog stages       : 7
;*      Collapsed prolog stages       : 7
;*      Max amt of load speculation   : 96 bytes
;*
;*      Minimum safe trip count       : 1 (after unrolling)
;*      Min. prof. trip count  (est.) : 2 (after unrolling)
;*
;*      Mem bank conflicts/iter(est.) : { min 0.000, est 0.000, max 0.000 }
;*      Mem bank perf. penalty (est.) : 0.0%
;*
;*
;*      Total cycles (est.)         : 7 + trip_cnt * 1        
;*----------------------------------------------------------------------------*
;*        SINGLE SCHEDULED ITERATION
;*
;*        ||$C$C416||:
;*   0              TICK                               ; [A_U] 
;*   1              VLDBUNPKW .D1   *D0++(16),VBL0    ; [A_D1] |465| 
;*   2              NOP     0x5     ; [A_B] 
;*   7              VMAXW   .L2     VBL1,VBL0,VBL1    ; [B_L2] |465| 
;*     ||           BNL     .B1     ||$C$C416||       ; [A_B] |463| 
;*   8              ; BRANCHCC OCCURS {||$C$C416||}   ; [] |463| 
;*----------------------------------------------------------------------------*
||$C$L37||:    ; PIPED LOOP PROLOG
;** --------------------------------------------------------------------------*
||$C$L38||:    ; PIPED LOOP KERNEL
;          EXCLUSIVE CPU CYCLES: 1

   [ A0]   ADDW    .D2     A0,0xffffffff,A0  ; [A_D2] |463| <0,7> collapsing predicate control
||         BNL     .B1     ||$C$L38||        ; [A_B] |463| <0,7> 
|| [!A0]   VMAXW   .L2     VBL1,VBL0,VBL1    ; [B_L2] |465| <0,7> 
||         VLDBUNPKW .D1   *D0++(16),VBL0    ; [A_D1] |465| <6,1> 
||         TICK                               ; [A_U] <7,0> 

;** --------------------------------------------------------------------------*
||$C$L39||:    ; PIPED LOOP EPILOG
;          EXCLUSIVE CPU CYCLES: 1
           PROTCLR                            ; [A_U] VBL1
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 1
           VMV     .L2     VBL1,VB0          ; [B_L2] 
;** --------------------------------------------------------------------------*
||$C$L40||:    
;          EXCLUSIVE CPU CYCLES: 23
	.dwpsn	file "tidl_custom_maxpooling.c",line 465,column 13,is_stmt,isa 0
           VHHMV   .C2     VB0,VB0,VB5       ; [B_C] |465| 
           VGETD   .C2     VB0,0x2,BM5       ; [B_C] |465| 
           VGETD   .C2     VB0,0x3,BM3       ; [B_C] |465| 
           VGETD   .C2     VB5,0x2,BM4       ; [B_C] |465| 
           VGETD   .C2     VB5,0x3,BM2       ; [B_C] |465| 
           VSHFL2DLL .C2   VBM3,VBM5,VBM3    ; [B_C] |465| 
           VSHFL2DLL .C2   VBM2,VBM4,VBM2    ; [B_C] |465| 

           EXTV    .L2     VB5,1,0,0,VB3     ; [B_L2] |465| 
||         EXTV    .S2     VB0,1,0,0,VB4     ; [B_S2] |465| 

           VGETD   .C2     VBM3,1,VBL1       ; [B_C] |465| 
           VGETD   .C2     VBM2,1,VBL0       ; [B_C] |465| 
           MV      .L2     B0,B7             ; [B_L2] |465| 

           MV      .M2     BM3,B6            ; [B_M2] |465| 
||         SHRD    .L2     B4,0x20,B10       ; [B_L2] |465| 
||         SHRD    .S2     B7,0x20,B12       ; [B_S2] |465| 

           SHRD    .L2     B6,0x20,BM6       ; [B_L2] |465| 
||         MV      .C2     B5,B5             ; [B_C] |465| 
||         SHRD    .S2     B3,0x20,B11       ; [B_S2] |465| 
||         VMAXW   .M2     B7,B12,BM7        ; [B_M2] |465| 

           MV      .M2     BM2,B0            ; [B_M2] |465| 
||         VMAXW   .C2     B6,BM6,B9         ; [B_C] |465| 
||         SHRD    .L2     BL1,0x20,BL5      ; [B_L2] |465| 
||         SHRD    .S2     B5,0x20,B13       ; [B_S2] |465| 

           SHRD    .L2     B0,0x20,BM4       ; [B_L2] |465| 
||         VMAXW   .M2     B4,B10,BM3        ; [B_M2] |465| 
||         SHRD    .S2     BL0,0x20,BL4      ; [B_S2] |465| 
||         VMAXW   .C2     B5,B13,BM5        ; [B_C] |465| 

           VMAXW   .M2     B3,B11,BM2        ; [B_M2] |465| 
||         VMAXW   .L2     BL0,BL4,BL0       ; [B_L2] |465| 
||         VMAXW   .C2     B0,BM4,BL6        ; [B_C] |465| 
||         VMAXW   .S2     BL1,BL5,BL7       ; [B_S2] |465| 

           VMAXW   .L2     B9,BL7,BL4        ; [B_L2] |465| 
||         VMAXW   .S2     BL6,BL0,BL0       ; [B_S2] |465| 
||         VMAXW   .M2     BM5,BM2,B8        ; [B_M2] |465| 
||         VMAXW   .C2     BM7,BM3,B12       ; [B_C] |465| 

           VMAXW   .L2     B12,BL4,BL1       ; [B_L2] |465| 
||         VMAXW   .S2     B8,BL0,BL0        ; [B_S2] |465| 
||         CMPEQW  .L1     A5,0,A0           ; [A_L1] 

   [ A0]   B       .B1     ||$C$L44||        ; [A_B] 
||         VMAXW   .L2     BL1,BL0,B0        ; [B_L2] |465| 

           ; BRANCHCC OCCURS {||$C$L44||}    ; [] 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 15
; Peeled loop iterations for unrolled loop:
           MPYWW   .N2     BM0,B1,BL0        ; [B_N2] 
           ADDW    .L2     BL3,BL0,BM2       ; [B_L2] 
           MPYWW   .N2     B2,BM2,B3         ; [B_N2] 
           MPYWW   .N1     AM0,A6,AM1        ; [A_N1] 
           MPYWW   .N1     A8,A7,D0          ; [A_N1] 
           ADDW    .M1X    B3,AM1,D6         ; [A_M1] 
           ADDW    .D1     D0,D6,D0          ; [A_D1] 
           ADDW    .D1     D5,D0,D0          ; [A_D1] 

           ADDAB   .D1     D1,D0,D0          ; [A_D1] 
||         EXT     .L1     A5,0x20,0x20,A5   ; [A_L1] 

           ADDD    .D1     D0,0xffffffff,D0  ; [A_D1] undo prolog elim. side-effects
||         NLCINIT .S1     A5,0x1,6          ; [A_S1] 
||         UNPROT          0x1               ; [A_U] 
||         MV      .L2     B0,BL1            ; [B_L2] 
||         MVKU32  .L1     0x7,A0            ; [A_L1] init prolog collapse predicate

;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*
;*      Loop found in file               : tidl_custom_maxpooling.c
;*      Loop source line                 : 463
;*      Loop opening brace source line   : 464
;*      Loop closing brace source line   : 470
;*      Known Minimum Trip Count         : 1                    
;*      Known Maximum Trip Count         : 15                    
;*      Known Max Trip Count Factor      : 1
;*      Loop Carried Dependency Bound(^) : 0
;*      Unpartitioned Resource Bound     : 1
;*      Partitioned Resource Bound       : 1 (pre-sched)
;*
;*      Searching for software pipeline schedule at ...
;*         ii = 1  Schedule found with 8 iterations in parallel
;*
;*      Partitioned Resource Bound(*)    : 1 (post-sched)
;*
;*      Constant Extension #0 Used [C0]  : 0
;*      Constant Extension #1 Used [C1]  : 0
;*
;*      Resource Partition (may include "post-sched" split/spill moves):
;*
;*                                                A-side   B-side
;*      .L units                                     0        0     
;*      .S units                                     0        0     
;*      .M units                                     0        0     
;*      .N units                                     0        0     
;*      .D units                                     1        -     
;*      .B units                                     1*       -     
;*      .C units                                     -        0     
;*      .P units                                     -        0     
;*
;*      .M/.N units                                  0        0     
;*      .L/.S units                                  0        0     
;*      .L/.S/.C units                               0        0     
;*      .L/.S/.C/.M units                            0        1     
;*      .L/.S/.C/.M/.D units                         0        0     
;*
;*      .X cross paths                               0        0     
;*
;*      Bound(.D1 .D2 .D)                            1*       -     
;*      Bound(.M .N .MN)                             0        0     
;*      Bound(.L .S .LS)                             0        0     
;*      Bound(.L .S .C .LS .LSC)                     0        0     
;*      Bound(.L .S .C .M .LS .LSC .LSCM)            0        1*    
;*      Bound(.L .S .C .M .D .LS .LSC .LSCM .LSCMD)  1*       1*    
;*
;*      Register Usage Tables:
;*
;*      +----------------------------------+----------------------------------+
;*      |              ASIDE               |              BSIDE               |
;*      +----------------+--------+--------+----------------+--------+--------+
;*      |      "Axx"     |  "ALx" |  "AMx" |     "VBxx"     | "VBLx" | "VBMx" |
;*      +----------------|--------|--------|----------------|--------|--------+
;*      |0000000000111111|00000000|00000000|0000000000111111|00000000|00000000|
;*      |0123456789012345|01234567|01234567|0123456789012345|01234567|01234567|
;*      +----------------|--------|--------|----------------|--------|--------+
;*   0: |*               |        |        |                |**      |        |
;*      +----------------+--------+--------+----------------+--------+--------+
;*
;*      +----------------+                 +--------+       +-------+
;*      |      "Dxx"     |                 |  "Px"  |       |"CUCRx"|
;*      +----------------+                 +--------+       +-------+
;*      |0000000000111111|                 |00000000|       |0000   |
;*      |0123456789012345|                 |01234567|       |0123   |
;*      +----------------+                 +--------+       +-------+
;*   0: |*               |                 |        |       |       |
;*      +----------------+                 +--------+       +-------+
;*
;*      Done
;*
;*      Collapsed epilog stages       : 7
;*      Collapsed prolog stages       : 7
;*      Max amt of load speculation   : 6 bytes
;*
;*      Minimum safe trip count       : 1
;*      Min. prof. trip count  (est.) : 2
;*
;*      Mem bank conflicts/iter(est.) : { min 0.000, est 0.000, max 0.000 }
;*      Mem bank perf. penalty (est.) : 0.0%
;*
;*
;*      Total cycles (est.)         : 7 + trip_cnt * 1 = Between 8 and 22        
;*----------------------------------------------------------------------------*
;*        SINGLE SCHEDULED ITERATION
;*
;*        ||$C$C357||:
;*   0              TICK                               ; [A_U] 
;*   1              SLDB    .D1     *D0++(1),BL0      ; [A_D1] |465| 
;*   2              NOP     0x5     ; [A_B] 
;*   7              VMAXW   .L2     BL1,BL0,BL1       ; [B_L2] |465| 
;*     ||           BNL     .B1     ||$C$C357||       ; [A_B] |463| 
;*   8              ; BRANCHCC OCCURS {||$C$C357||}   ; [] |463| 
;*----------------------------------------------------------------------------*
||$C$L41||:    ; PIPED LOOP PROLOG
;** --------------------------------------------------------------------------*
||$C$L42||:    ; PIPED LOOP KERNEL
;          EXCLUSIVE CPU CYCLES: 1
	.dwpsn	file "tidl_custom_maxpooling.c",line 463,column 24,is_stmt,isa 0

   [ A0]   ADDW    .D2     A0,0xffffffff,A0  ; [A_D2] |463| <0,7> collapsing predicate control
||         BNL     .B1     ||$C$L42||        ; [A_B] |463| <0,7> 
|| [!A0]   VMAXW   .L2     BL1,BL0,BL1       ; [B_L2] |465| <0,7> 
||         SLDB    .D1     *D0++(1),BL0      ; [A_D1] |465| <6,1> 
||         TICK                               ; [A_U] <7,0> 

;** --------------------------------------------------------------------------*
||$C$L43||:    ; PIPED LOOP EPILOG
;          EXCLUSIVE CPU CYCLES: 1
           PROTCLR                            ; [A_U] VBL1
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 1
           MV      .L2     BL1,B0            ; [B_L2] 
;** --------------------------------------------------------------------------*
||$C$L44||:    
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "tidl_custom_maxpooling.c",line 461,column 22,is_stmt,isa 0
           ADDW    .D1     A3,0xffffffff,A3  ; [A_D1] |461| 

   [ A3]   B       .B1     ||$C$L36||        ; [A_B] |461| 
||         ADDW    .L2     BL3,0x1,BL3       ; [B_L2] |461| 

           ; BRANCHCC OCCURS {||$C$L36||}    ; [] |461| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 1
           MV      .L2     B0,B3             ; [B_L2] 
;** --------------------------------------------------------------------------*
||$C$L45||:    
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "tidl_custom_maxpooling.c",line 458,column 20,is_stmt,isa 0
           ADDW    .D1     A4,0xffffffff,A4  ; [A_D1] |458| 

   [ A4]   B       .B1     ||$C$L35||        ; [A_B] |458| 
||         STB     .D1X    B3,*D4++(1)       ; [A_D1] |472| 
||         ADDW    .D2     A6,0x1,A6         ; [A_D2] |458| 

           ; BRANCHCC OCCURS {||$C$L35||}    ; [] |458| 
;** --------------------------------------------------------------------------*
||$C$L46||:    
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "tidl_custom_maxpooling.c",line 456,column 18,is_stmt,isa 0
           ADDW    .D1     A1,0xffffffff,A1  ; [A_D1] |456| 

   [ A1]   B       .B1     ||$C$L34||        ; [A_B] |456| 
||         ADDW    .L2     B1,0x1,B1         ; [B_L2] |456| 

           ; BRANCHCC OCCURS {||$C$L34||}    ; [] |456| 
;** --------------------------------------------------------------------------*
||$C$L47||:    
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "tidl_custom_maxpooling.c",line 450,column 16,is_stmt,isa 0
           ADDW    .D1     A2,0xffffffff,A2  ; [A_D1] |450| 

   [ A2]   B       .B1     ||$C$L33||        ; [A_B] |450| 
||         ADDW    .D1     A7,0x1,A7         ; [A_D1] |450| 

           ; BRANCHCC OCCURS {||$C$L33||}    ; [] |450| 
;** --------------------------------------------------------------------------*
||$C$L48||:    
;          EXCLUSIVE CPU CYCLES: 7
           LDD     .D1     *SP(16),A13       ; [A_D1] 
	.dwcfi	restore_reg, 13

           LDD     .D1     *SP(32),A9        ; [A_D1] 
||         LDD     .D2     *SP(24),A11       ; [A_D2] 

	.dwcfi	restore_reg, 9
	.dwcfi	restore_reg, 11
$C$DW$182	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$182, DW_AT_low_pc(0x00)
	.dwattr $C$DW$182, DW_AT_TI_return


           RET     .B1     ; [A_B] 
||         ADDD    .D1     SP,0x18,SP        ; [A_D1] 

	.dwcfi	cfa_offset, 0
           ; RETURN OCCURS {RP}              ; [] 
	.dwattr $C$DW$149, DW_AT_TI_end_file("tidl_custom_maxpooling.c")
	.dwattr $C$DW$149, DW_AT_TI_end_line(0x1dc)
	.dwattr $C$DW$149, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$149

	.sect	".text:_Z28TIDL_refCustomMaxPoolingCoreIhEvPT_iiiiiiiiiiiiiS0_S1_P13sTIDL_Layer_t"
	.clink
	.global	||_Z28TIDL_refCustomMaxPoolingCoreIhEvPT_iiiiiiiiiiiiiS0_S1_P13sTIDL_Layer_t||

$C$DW$183	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$183, DW_AT_name("void TIDL_refCustomMaxPoolingCore")
	.dwattr $C$DW$183, DW_AT_low_pc(||_Z28TIDL_refCustomMaxPoolingCoreIhEvPT_iiiiiiiiiiiiiS0_S1_P13sTIDL_Layer_t||)
	.dwattr $C$DW$183, DW_AT_high_pc(0x00)
	.dwattr $C$DW$183, DW_AT_linkage_name("_Z28TIDL_refCustomMaxPoolingCoreIhEvPT_iiiiiiiiiiiiiS0_S1_P13sTIDL_Layer_t")
	.dwattr $C$DW$183, DW_AT_external
	.dwattr $C$DW$183, DW_AT_decl_file("tidl_custom_maxpooling.c")
	.dwattr $C$DW$183, DW_AT_decl_line(0x1a9)
	.dwattr $C$DW$183, DW_AT_decl_column(0x06)
	.dwattr $C$DW$183, DW_AT_TI_max_frame_size(0x18)
	.dwpsn	file "tidl_custom_maxpooling.c",line 442,column 1,is_stmt,address ||_Z28TIDL_refCustomMaxPoolingCoreIhEvPT_iiiiiiiiiiiiiS0_S1_P13sTIDL_Layer_t||,isa 0

	.dwfde $C$DW$CIE, ||_Z28TIDL_refCustomMaxPoolingCoreIhEvPT_iiiiiiiiiiiiiS0_S1_P13sTIDL_Layer_t||
$C$DW$184	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$184, DW_AT_name("pInChannel")
	.dwattr $C$DW$184, DW_AT_type(*$C$DW$T$352)
	.dwattr $C$DW$184, DW_AT_location[DW_OP_reg4]

$C$DW$185	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$185, DW_AT_name("width")
	.dwattr $C$DW$185, DW_AT_location[DW_OP_reg5]

$C$DW$186	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$186, DW_AT_name("height")
	.dwattr $C$DW$186, DW_AT_location[DW_OP_reg6]

$C$DW$187	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$187, DW_AT_name("inPitch")
	.dwattr $C$DW$187, DW_AT_location[DW_OP_reg7]

$C$DW$188	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$188, DW_AT_name("inChPitch")
	.dwattr $C$DW$188, DW_AT_location[DW_OP_reg8]

$C$DW$189	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$189, DW_AT_name("outPitch")
	.dwattr $C$DW$189, DW_AT_location[DW_OP_reg9]

$C$DW$190	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$190, DW_AT_name("outChPitch")
	.dwattr $C$DW$190, DW_AT_location[DW_OP_reg10]

$C$DW$191	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$191, DW_AT_name("numOutChannels")
	.dwattr $C$DW$191, DW_AT_location[DW_OP_reg11]

$C$DW$192	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$192, DW_AT_name("kernelW")
	.dwattr $C$DW$192, DW_AT_location[DW_OP_reg12]

$C$DW$193	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$193, DW_AT_name("kernelH")
	.dwattr $C$DW$193, DW_AT_location[DW_OP_bregx 0x6f 40]

$C$DW$194	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$194, DW_AT_name("strideW")
	.dwattr $C$DW$194, DW_AT_location[DW_OP_bregx 0x6f 44]

$C$DW$195	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$195, DW_AT_name("strideH")
	.dwattr $C$DW$195, DW_AT_location[DW_OP_bregx 0x6f 48]

$C$DW$196	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$196, DW_AT_name("padW")
	.dwattr $C$DW$196, DW_AT_location[DW_OP_bregx 0x6f 52]

$C$DW$197	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$197, DW_AT_name("padH")
	.dwattr $C$DW$197, DW_AT_location[DW_OP_bregx 0x6f 56]

$C$DW$198	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$198, DW_AT_name("initValue")
	.dwattr $C$DW$198, DW_AT_type(*$C$DW$T$351)
	.dwattr $C$DW$198, DW_AT_location[DW_OP_bregx 0x6f 60]

$C$DW$199	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$199, DW_AT_name("pOutChannel")
	.dwattr $C$DW$199, DW_AT_type(*$C$DW$T$352)
	.dwattr $C$DW$199, DW_AT_location[DW_OP_bregx 0x6f 64]

$C$DW$200	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$200, DW_AT_name("tidlLayer")
	.dwattr $C$DW$200, DW_AT_location[DW_OP_bregx 0x6f 72]


;******************************************************************************
;* FUNCTION NAME: void TIDL_refCustomMaxPoolingCore<unsigned char>(T1 *, int, int, int, int, int, int, int, int, int, int, int, int, int, T1, T1 *, sTIDL_Layer_t *)*
;*                                                                            *
;*   Regs Modified     : A0,A1,A2,A3,A4,A5,A6,A7,A9,A11,A13,VB0,VB1,VB2,VB3,  *
;*                           VB4,VB5,VB6,VB7,VB8,VB9,VB10,VB11,VB12,VB13,AL0, *
;*                           AM0,AM1,AM2,D0,D1,D2,D3,D4,D5,D6,SP,VBL0,VBL1,   *
;*                           VBL2,VBL3,VBL4,VBL5,VBL6,VBL7,VBM0,VBM1,VBM2,    *
;*                           VBM3,VBM4,VBM5,VBM6,VBM7                         *
;*   Regs Used         : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,VB0,   *
;*                           VB1,VB2,VB3,VB4,VB5,VB6,VB7,VB8,VB9,VB10,VB11,   *
;*                           VB12,VB13,AL0,AM0,AM1,AM2,D0,D1,D2,D3,D4,D5,D6,  *
;*                           SP,VBL0,VBL1,VBL2,VBL3,VBL4,VBL5,VBL6,VBL7,VBM0, *
;*                           VBM1,VBM2,VBM3,VBM4,VBM5,VBM6,VBM7               *
;*   Local Frame Size  : 0 Args + 0 Auto + 24 Save = 24 byte                  *
;******************************************************************************
||_Z28TIDL_refCustomMaxPoolingCoreIhEvPT_iiiiiiiiiiiiiS0_S1_P13sTIDL_Layer_t||:
;** --------------------------------------------------------------------------*
;* VB0   assigned to $O$C3
;* A0    assigned to $O$C4
;* D0    assigned to $O$C5
;* D0    assigned to $O$U84
;* A13   assigned to $O$U84
;* D3    assigned to $O$K49
;* A5    assigned to $O$K157
;* D4    assigned to $O$U175
;* A2    assigned to $O$L1
;* A1    assigned to $O$L2
;* A4    assigned to $O$L3
;* A3    assigned to $O$L4
;* AL0   assigned to $O$L5
;* A5    assigned to $O$L6
;* D1    assigned to $O$Lr81$inData
;* D2    assigned to $O$Lr97$outData
;* A7    assigned to $O$Lr17$i1
;* A11   assigned to $O$Lr68$numCols
;* VB1   assigned to $O$Lr20$i2
;* A6    assigned to $O$Lr30$i3
;* VBL3  assigned to $O$Lr34$i4
;* VB0   assigned to $O$Lr49$maxValue
;* VB3   assigned to $O$Lr15$maxValue
;* D5    assigned to $O$Lr51$i5
;* VB0   assigned to tidlLayer
$C$DW$201	.dwtag  DW_TAG_variable
	.dwattr $C$DW$201, DW_AT_name("tidlLayer")
	.dwattr $C$DW$201, DW_AT_type(*$C$DW$T$354)
	.dwattr $C$DW$201, DW_AT_location[DW_OP_regx 0x30]

;* D2    assigned to pOutChannel
$C$DW$202	.dwtag  DW_TAG_variable
	.dwattr $C$DW$202, DW_AT_name("pOutChannel")
	.dwattr $C$DW$202, DW_AT_type(*$C$DW$T$352)
	.dwattr $C$DW$202, DW_AT_location[DW_OP_regx 0x62]

;* VBL2  assigned to initValue
$C$DW$203	.dwtag  DW_TAG_variable
	.dwattr $C$DW$203, DW_AT_name("initValue")
	.dwattr $C$DW$203, DW_AT_type(*$C$DW$T$351)
	.dwattr $C$DW$203, DW_AT_location[DW_OP_regx 0x4a]

;* AM0   assigned to padH
$C$DW$204	.dwtag  DW_TAG_variable
	.dwattr $C$DW$204, DW_AT_name("padH")
	.dwattr $C$DW$204, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$204, DW_AT_location[DW_OP_reg24]

;* AM1   assigned to padW
$C$DW$205	.dwtag  DW_TAG_variable
	.dwattr $C$DW$205, DW_AT_name("padW")
	.dwattr $C$DW$205, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$205, DW_AT_location[DW_OP_reg25]

;* VBM1  assigned to strideH
$C$DW$206	.dwtag  DW_TAG_variable
	.dwattr $C$DW$206, DW_AT_name("strideH")
	.dwattr $C$DW$206, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$206, DW_AT_location[DW_OP_regx 0x59]

;* AM0   assigned to strideW
$C$DW$207	.dwtag  DW_TAG_variable
	.dwattr $C$DW$207, DW_AT_name("strideW")
	.dwattr $C$DW$207, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$207, DW_AT_location[DW_OP_reg24]

;* A9    assigned to kernelH
$C$DW$208	.dwtag  DW_TAG_variable
	.dwattr $C$DW$208, DW_AT_name("kernelH")
	.dwattr $C$DW$208, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$208, DW_AT_location[DW_OP_reg9]

;* A12   assigned to kernelW
$C$DW$209	.dwtag  DW_TAG_variable
	.dwattr $C$DW$209, DW_AT_name("kernelW")
	.dwattr $C$DW$209, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$209, DW_AT_location[DW_OP_reg12]

;* A2    assigned to numOutChannels
$C$DW$210	.dwtag  DW_TAG_variable
	.dwattr $C$DW$210, DW_AT_name("numOutChannels")
	.dwattr $C$DW$210, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$210, DW_AT_location[DW_OP_reg2]

;* A10   assigned to outChPitch
$C$DW$211	.dwtag  DW_TAG_variable
	.dwattr $C$DW$211, DW_AT_name("outChPitch")
	.dwattr $C$DW$211, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$211, DW_AT_location[DW_OP_reg10]

;* VBM2  assigned to outPitch
$C$DW$212	.dwtag  DW_TAG_variable
	.dwattr $C$DW$212, DW_AT_name("outPitch")
	.dwattr $C$DW$212, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$212, DW_AT_location[DW_OP_regx 0x5a]

;* A8    assigned to inChPitch
$C$DW$213	.dwtag  DW_TAG_variable
	.dwattr $C$DW$213, DW_AT_name("inChPitch")
	.dwattr $C$DW$213, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$213, DW_AT_location[DW_OP_reg8]

;* VB2   assigned to inPitch
$C$DW$214	.dwtag  DW_TAG_variable
	.dwattr $C$DW$214, DW_AT_name("inPitch")
	.dwattr $C$DW$214, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$214, DW_AT_location[DW_OP_regx 0x32]

;* A4    assigned to pInChannel
$C$DW$215	.dwtag  DW_TAG_variable
	.dwattr $C$DW$215, DW_AT_name("pInChannel")
	.dwattr $C$DW$215, DW_AT_type(*$C$DW$T$352)
	.dwattr $C$DW$215, DW_AT_location[DW_OP_reg4]

;* VB0   assigned to $O$CSE$s32x4$010
;* VB7   assigned to $O$CSE$s32x2$011
;* VB4   assigned to $O$CSE$s32x2$012
;* VBM3  assigned to $O$CSE$s32x4$013
;* VB6   assigned to $O$CSE$s32x2$014
;* VBL1  assigned to $O$CSE$s32x2$015
;* VB5   assigned to $O$CSE$s32x4$017
;* VB5   assigned to $O$CSE$s32x2$018
;* VB3   assigned to $O$CSE$s32x2$019
;* VBM0  assigned to $O$CSE$s32x4$020
;* VB0   assigned to $O$CSE$s32x2$021
;* VBL0  assigned to $O$CSE$s32x2$022
;* VB0   assigned to $O$VEC$s32x16$008
	.dwcfi	cfa_offset, 0
;          EXCLUSIVE CPU CYCLES: 24

           STD     .D1     A9,*SP(8)         ; [A_D1] 
||         STD     .D2X    A11,*SP++(-24)    ; [A_D2] 

	.dwcfi	save_reg_to_mem, 9, 8
	.dwcfi	cfa_offset, 24
	.dwcfi	save_reg_to_mem, 11, -24
           LDD     .D1     *SP(72),B0        ; [A_D1] |442| 
	.dwpsn	file "tidl_custom_maxpooling.c",line 447,column 15,is_stmt,isa 0
           ADDD    .L1X    B0,0xf0,D0        ; [A_L1] |447| 

           LDW     .D1     *D0(0),B1         ; [A_D1] |447| 
||         ADDD    .L1X    B0,0x6d0,A0       ; [A_L1] |448| 

	.dwpsn	file "tidl_custom_maxpooling.c",line 442,column 1,is_stmt,isa 0

           ADDD    .D2     A0,0xffffffe0,D1  ; [A_D2] |448| 
||         LDW     .D1     *SP(56),AM0       ; [A_D1] |442| 

	.dwpsn	file "tidl_custom_maxpooling.c",line 448,column 16,is_stmt,isa 0

           LDW     .D1     *A0(0),BM1        ; [A_D1] |448| 
||         LDW     .D2     *D1(0),BM0        ; [A_D2] |448| 

	.dwpsn	file "tidl_custom_maxpooling.c",line 447,column 15,is_stmt,isa 0
           ADDD    .D1     D0,0xfffffffc,D1  ; [A_D1] |447| 
           LDW     .D1     *D1(0),B3         ; [A_D1] |447| 
	.dwpsn	file "tidl_custom_maxpooling.c",line 442,column 1,is_stmt,isa 0
           LDW     .D1     *SP(52),AM1       ; [A_D1] |442| 

           ADDD    .D1     A0,0xffffffdc,D0  ; [A_D1] |448| 
||         MV      .L2X    A7,B2             ; [B_L2] |442| 

	.dwpsn	file "tidl_custom_maxpooling.c",line 447,column 15,is_stmt,isa 0

           SUBW    .M1X    B1,AM0,AM0        ; [A_M1] |447| 
||         LDW     .D1     *D0(0),AM2        ; [A_D1] |448| 

	.dwpsn	file "tidl_custom_maxpooling.c",line 442,column 1,is_stmt,isa 0

           MPYWW   .N2     BM1,BM0,B1        ; [B_N2] |448| 
||         MPYWW   .N1X    B2,AM0,D0         ; [A_N1] |447| 
||         LDD     .D1     *SP(64),D2        ; [A_D1] |442| 

           SUBW    .M1X    B3,AM1,D1         ; [A_M1] |447| 
||         STD     .D1     A13,*SP(16)       ; [A_D1] 
||         LDUB    .D2     *SP(60),BL2       ; [A_D2] |442| 
	.dwcfi	save_reg_to_mem, 13, 16

           ADDW    .D1     D0,D1,D1          ; [A_D1] |447| 
||         LDW     .D2     *SP(40),A9        ; [A_D2] |442| 
||         MV      .L1     A11,A2            ; [A_L1] |442| 
||         MV      .L2X    A9,BM2            ; [B_L2] |442| 

           ADDW    .M1X    B1,AM2,D0         ; [A_M1] |448| 
||         ADDAB   .D1     A4,D1,D1          ; [A_D1] |447| 
||         LDW     .D2     *SP(44),AM0       ; [A_D2] |442| 
||         CMPGTW  .L1     A2,0,A0           ; [A_L1] |450| 

   [!A0]   B       .B1     ||$C$L64||        ; [A_B] |450| 
||         ADDAB   .D1     D2,D0,D2          ; [A_D1] |448| 
||         LDW     .D2     *SP(48),BM1       ; [A_D2] |442| 
||         ADDD    .S1X    B0,0x6dc,D3       ; [A_S1] 
||         MVKU32  .L1     0,A7              ; [A_L1] |450| 

	.dwpsn	file "tidl_custom_maxpooling.c",line 450,column 16,is_stmt,isa 0
           ; BRANCHCC OCCURS {||$C$L64||}    ; [] |450| 
;** --------------------------------------------------------------------------*
;**   BEGIN LOOP ||$C$L49||
;** --------------------------------------------------------------------------*
||$C$L49||:    
;          EXCLUSIVE CPU CYCLES: 9
	.dwpsn	file "tidl_custom_maxpooling.c",line 456,column 18,is_stmt,isa 0
           LDW     .D1     *D3(0),B0         ; [A_D1] |456| 
	.dwpsn	file "tidl_custom_maxpooling.c",line 455,column 5,is_stmt,isa 0
           LDUW    .D1     *D3(4),A11        ; [A_D1] |455| 
	.dwpsn	file "tidl_custom_maxpooling.c",line 456,column 18,is_stmt,isa 0
           CMPEQW  .L1X    B0,0,A0           ; [A_L1] |456| 
   [ A0]   B       .B1     ||$C$L63||        ; [A_B] |456| 
           ; BRANCHCC OCCURS {||$C$L63||}    ; [] |456| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 1
	.dwpsn	file "tidl_custom_maxpooling.c",line 456,column 10,is_stmt,isa 0

           MVKU32  .L2     0,B1              ; [B_L2] |456| 
||         MV      .L1X    B0,A1             ; [A_L1] 

;** --------------------------------------------------------------------------*
;**   BEGIN LOOP ||$C$L50||
;** --------------------------------------------------------------------------*
||$C$L50||:    
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "tidl_custom_maxpooling.c",line 458,column 20,is_stmt,isa 0
           CMPEQW  .L1     A11,0,A0          ; [A_L1] |458| 
   [ A0]   B       .B1     ||$C$L62||        ; [A_B] |458| 
           ; BRANCHCC OCCURS {||$C$L62||}    ; [] |458| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 8
           MPYWW   .N2     BM2,B1,B0         ; [B_N2] 
           MPYWW   .N1     A10,A7,D4         ; [A_N1] 
           MV      .L1X    B0,D0             ; [A_L1] Define a twin register
           ADDW    .D1     D4,D0,D0          ; [A_D1] 
	.dwpsn	file "tidl_custom_maxpooling.c",line 458,column 12,is_stmt,isa 0

           ADDAB   .D1     D2,D0,D4          ; [A_D1] 
||         MVKU32  .L1     0,A6              ; [A_L1] |458| 
||         MV      .D2     A11,A4            ; [A_D2] 

;** --------------------------------------------------------------------------*
;**   BEGIN LOOP ||$C$L51||
;** --------------------------------------------------------------------------*
||$C$L51||:    
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "tidl_custom_maxpooling.c",line 461,column 22,is_stmt,isa 0
           CMPGTW  .L1     A9,0,A0           ; [A_L1] |461| 

   [!A0]   B       .B1     ||$C$L61||        ; [A_B] |461| 
||         MV      .L2     BL2,B3            ; [B_L2] 
||         MV      .S2     BL2,B0            ; [B_S2] 

           ; BRANCHCC OCCURS {||$C$L61||}    ; [] |461| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 1
	.dwpsn	file "tidl_custom_maxpooling.c",line 461,column 14,is_stmt,isa 0

           MVKU32  .L2     0,BL3             ; [B_L2] |461| 
||         MV      .D1     A9,A3             ; [A_D1] 

;** --------------------------------------------------------------------------*
;**   BEGIN LOOP ||$C$L52||
;** --------------------------------------------------------------------------*
||$C$L52||:    
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "tidl_custom_maxpooling.c",line 463,column 24,is_stmt,isa 0
           CMPGTW  .L1     A12,0,A0          ; [A_L1] |463| 
   [!A0]   B       .B1     ||$C$L60||        ; [A_B] |463| 
           ; BRANCHCC OCCURS {||$C$L60||}    ; [] |463| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 3
	.dwpsn	file "tidl_custom_maxpooling.c",line 465,column 13,is_stmt,isa 0
           VDUPW   .L2     B0,VB0            ; [B_L2] |465| 
           CMPGEW  .L1     A12,0x10,A0       ; [A_L1] 
	.dwpsn	file "tidl_custom_maxpooling.c",line 463,column 16,is_stmt,isa 0

   [!A0]   B       .B1     ||$C$L56||        ; [A_B] 
|| [!A0]   MVKU32  .L1     0,D5              ; [A_L1] |463| 
||         ANDW    .S1     A12,0xf,A5        ; [A_S1] 

           ; BRANCHCC OCCURS {||$C$L56||}    ; [] 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 15
           MPYWW   .N2     BM1,B1,BL0        ; [B_N2] 
           ADDW    .L2     BL3,BL0,BM0       ; [B_L2] 
           MPYWW   .N2     B2,BM0,B3         ; [B_N2] 
           MPYWW   .N1     AM0,A6,AM1        ; [A_N1] 
           MPYWW   .N1     A8,A7,D5          ; [A_N1] 
           ADDW    .M1X    B3,AM1,D0         ; [A_M1] 
           ADDW    .D1     D5,D0,D0          ; [A_D1] 

           ADDAB   .D1     D1,D0,A13         ; [A_D1] 
||         SHRW    .L1     A12,0x4,AL0       ; [A_L1] 

           MV      .D1     A13,D0            ; [A_D1] 
||         EXT     .L1     AL0,0x20,0x20,AL0 ; [A_L1] 

	.dwpsn	file "tidl_custom_maxpooling.c",line 463,column 24,is_stmt,isa 0

           ADDD    .D1     D0,0xfffffff0,D0  ; [A_D1] undo prolog elim. side-effects
||         ANDW    .M1     A12,0xfffffff0,D5 ; [A_M1] |463| 
||         NLCINIT .S1     AL0,0x1,10        ; [A_S1] 
||         UNPROT          0x1               ; [A_U] 
||         VMV     .L2     VB0,VBL1          ; [B_L2] 
||         MVKU32  .L1     0xb,A0            ; [A_L1] init prolog collapse predicate

;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*
;*      Loop found in file               : tidl_custom_maxpooling.c
;*      Loop source line                 : 463
;*      Loop opening brace source line   : 464
;*      Loop closing brace source line   : 470
;*      Loop Unroll Multiple             : 16x
;*      Known Minimum Trip Count         : 1                    
;*      Known Max Trip Count Factor      : 1
;*      Loop Carried Dependency Bound(^) : 0
;*      Unpartitioned Resource Bound     : 1
;*      Partitioned Resource Bound       : 1 (pre-sched)
;*
;*      Searching for software pipeline schedule at ...
;*         ii = 1  Schedule found with 12 iterations in parallel
;*
;*      Partitioned Resource Bound(*)    : 1 (post-sched)
;*
;*      Constant Extension #0 Used [C0]  : 0
;*      Constant Extension #1 Used [C1]  : 0
;*
;*      Resource Partition (may include "post-sched" split/spill moves):
;*
;*                                                A-side   B-side
;*      .L units                                     0        0     
;*      .S units                                     0        0     
;*      .M units                                     0        0     
;*      .N units                                     0        0     
;*      .D units                                     1        -     
;*      .B units                                     1*       -     
;*      .C units                                     -        1*    
;*      .P units                                     -        0     
;*
;*      .M/.N units                                  0        0     
;*      .L/.S units                                  0        0     
;*      .L/.S/.C units                               0        0     
;*      .L/.S/.C/.M units                            0        1     
;*      .L/.S/.C/.M/.D units                         0        0     
;*
;*      .X cross paths                               0        0     
;*
;*      Bound(.D1 .D2 .D)                            1*       -     
;*      Bound(.M .N .MN)                             0        0     
;*      Bound(.L .S .LS)                             0        0     
;*      Bound(.L .S .C .LS .LSC)                     0        1*    
;*      Bound(.L .S .C .M .LS .LSC .LSCM)            0        1*    
;*      Bound(.L .S .C .M .D .LS .LSC .LSCM .LSCMD)  1*       1*    
;*
;*      Register Usage Tables:
;*
;*      +----------------------------------+----------------------------------+
;*      |              ASIDE               |              BSIDE               |
;*      +----------------+--------+--------+----------------+--------+--------+
;*      |      "Axx"     |  "ALx" |  "AMx" |     "VBxx"     | "VBLx" | "VBMx" |
;*      +----------------|--------|--------|----------------|--------|--------+
;*      |0000000000111111|00000000|00000000|0000000000111111|00000000|00000000|
;*      |0123456789012345|01234567|01234567|0123456789012345|01234567|01234567|
;*      +----------------|--------|--------|----------------|--------|--------+
;*   0: |*               |        |        |                |**      |*       |
;*      +----------------+--------+--------+----------------+--------+--------+
;*
;*      +----------------+                 +--------+       +-------+
;*      |      "Dxx"     |                 |  "Px"  |       |"CUCRx"|
;*      +----------------+                 +--------+       +-------+
;*      |0000000000111111|                 |00000000|       |0000   |
;*      |0123456789012345|                 |01234567|       |0123   |
;*      +----------------+                 +--------+       +-------+
;*   0: |*               |                 |        |       |       |
;*      +----------------+                 +--------+       +-------+
;*
;*      Done
;*
;*      Collapsed epilog stages       : 11
;*      Collapsed prolog stages       : 11
;*      Max amt of load speculation   : 160 bytes
;*
;*      Minimum safe trip count       : 1 (after unrolling)
;*      Min. prof. trip count  (est.) : 2 (after unrolling)
;*
;*      Mem bank conflicts/iter(est.) : { min 0.000, est 0.000, max 0.000 }
;*      Mem bank perf. penalty (est.) : 0.0%
;*
;*
;*      Total cycles (est.)         : 11 + trip_cnt * 1        
;*----------------------------------------------------------------------------*
;*        SINGLE SCHEDULED ITERATION
;*
;*        ||$C$C574||:
;*   0              TICK                               ; [A_U] 
;*   1              VLD16B  .D1     *D0++(16),VBM0    ; [A_D1] |465| 
;*   2              NOP     0x5     ; [A_B] 
;*   7              VBUNPKWU .C2    VBM0,VBL0         ; [B_C] |465| 
;*   8              NOP     0x3     ; [A_B] 
;*  11              VMAXW   .L2     VBL1,VBL0,VBL1    ; [B_L2] |465| 
;*     ||           BNL     .B1     ||$C$C574||       ; [A_B] |463| 
;*  12              ; BRANCHCC OCCURS {||$C$C574||}   ; [] |463| 
;*----------------------------------------------------------------------------*
||$C$L53||:    ; PIPED LOOP PROLOG
;** --------------------------------------------------------------------------*
||$C$L54||:    ; PIPED LOOP KERNEL
;          EXCLUSIVE CPU CYCLES: 1

   [ A0]   ADDW    .D2     A0,0xffffffff,A0  ; [A_D2] |463| <0,11> collapsing predicate control
||         BNL     .B1     ||$C$L54||        ; [A_B] |463| <0,11> 
|| [!A0]   VMAXW   .L2     VBL1,VBL0,VBL1    ; [B_L2] |465| <0,11> 
||         VBUNPKWU .C2    VBM0,VBL0         ; [B_C] |465| <4,7> 
||         VLD16B  .D1     *D0++(16),VBM0    ; [A_D1] |465| <10,1> 
||         TICK                               ; [A_U] <11,0> 

;** --------------------------------------------------------------------------*
||$C$L55||:    ; PIPED LOOP EPILOG
;          EXCLUSIVE CPU CYCLES: 1
           PROTCLR                            ; [A_U] VBL1
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 1
           VMV     .L2     VBL1,VB0          ; [B_L2] 
;** --------------------------------------------------------------------------*
||$C$L56||:    
;          EXCLUSIVE CPU CYCLES: 23
	.dwpsn	file "tidl_custom_maxpooling.c",line 465,column 13,is_stmt,isa 0
           VHHMV   .C2     VB0,VB0,VB5       ; [B_C] |465| 
           VGETD   .C2     VB0,0x2,BM5       ; [B_C] |465| 
           VGETD   .C2     VB0,0x3,BM3       ; [B_C] |465| 
           VGETD   .C2     VB5,0x2,BM4       ; [B_C] |465| 
           VGETD   .C2     VB5,0x3,BM0       ; [B_C] |465| 
           VSHFL2DLL .C2   VBM3,VBM5,VBM3    ; [B_C] |465| 
           VSHFL2DLL .C2   VBM0,VBM4,VBM0    ; [B_C] |465| 

           EXTV    .L2     VB5,1,0,0,VB3     ; [B_L2] |465| 
||         EXTV    .S2     VB0,1,0,0,VB4     ; [B_S2] |465| 

           VGETD   .C2     VBM3,1,VBL1       ; [B_C] |465| 
           VGETD   .C2     VBM0,1,VBL0       ; [B_C] |465| 
           MV      .L2     B0,B7             ; [B_L2] |465| 

           MV      .M2     BM3,B6            ; [B_M2] |465| 
||         SHRD    .L2     B4,0x20,B10       ; [B_L2] |465| 
||         SHRD    .S2     B7,0x20,B12       ; [B_S2] |465| 

           SHRD    .L2     B6,0x20,BM6       ; [B_L2] |465| 
||         MV      .C2     B5,B5             ; [B_C] |465| 
||         SHRD    .S2     B3,0x20,B11       ; [B_S2] |465| 
||         VMAXW   .M2     B7,B12,BM7        ; [B_M2] |465| 

           MV      .M2     BM0,B0            ; [B_M2] |465| 
||         VMAXW   .C2     B6,BM6,B9         ; [B_C] |465| 
||         SHRD    .L2     BL1,0x20,BL5      ; [B_L2] |465| 
||         SHRD    .S2     B5,0x20,B13       ; [B_S2] |465| 

           SHRD    .L2     B0,0x20,BM4       ; [B_L2] |465| 
||         VMAXW   .M2     B4,B10,BM3        ; [B_M2] |465| 
||         SHRD    .S2     BL0,0x20,BL4      ; [B_S2] |465| 
||         VMAXW   .C2     B5,B13,BM5        ; [B_C] |465| 

           VMAXW   .M2     B3,B11,BM0        ; [B_M2] |465| 
||         VMAXW   .L2     BL0,BL4,BL0       ; [B_L2] |465| 
||         VMAXW   .C2     B0,BM4,BL6        ; [B_C] |465| 
||         VMAXW   .S2     BL1,BL5,BL7       ; [B_S2] |465| 

           VMAXW   .L2     B9,BL7,BL4        ; [B_L2] |465| 
||         VMAXW   .S2     BL6,BL0,BL0       ; [B_S2] |465| 
||         VMAXW   .M2     BM5,BM0,B8        ; [B_M2] |465| 
||         VMAXW   .C2     BM7,BM3,B12       ; [B_C] |465| 

           VMAXW   .L2     B12,BL4,BL1       ; [B_L2] |465| 
||         VMAXW   .S2     B8,BL0,BL0        ; [B_S2] |465| 
||         CMPEQW  .L1     A5,0,A0           ; [A_L1] 

   [ A0]   B       .B1     ||$C$L60||        ; [A_B] 
||         VMAXW   .L2     BL1,BL0,B0        ; [B_L2] |465| 

           ; BRANCHCC OCCURS {||$C$L60||}    ; [] 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 15
; Peeled loop iterations for unrolled loop:
           MPYWW   .N2     BM1,B1,BL0        ; [B_N2] 
           ADDW    .L2     BL3,BL0,BM0       ; [B_L2] 
           MPYWW   .N2     B2,BM0,B3         ; [B_N2] 
           MPYWW   .N1     AM0,A6,AM1        ; [A_N1] 
           MPYWW   .N1     A8,A7,D0          ; [A_N1] 
           ADDW    .M1X    B3,AM1,D6         ; [A_M1] 
           ADDW    .D1     D0,D6,D0          ; [A_D1] 
           ADDW    .D1     D5,D0,D0          ; [A_D1] 

           ADDAB   .D1     D1,D0,D0          ; [A_D1] 
||         EXT     .L1     A5,0x20,0x20,A5   ; [A_L1] 

           ADDD    .D1     D0,0xffffffff,D0  ; [A_D1] undo prolog elim. side-effects
||         NLCINIT .S1     A5,0x1,6          ; [A_S1] 
||         UNPROT          0x1               ; [A_U] 
||         MV      .L2     B0,BL1            ; [B_L2] 
||         MVKU32  .L1     0x7,A0            ; [A_L1] init prolog collapse predicate

;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*
;*      Loop found in file               : tidl_custom_maxpooling.c
;*      Loop source line                 : 463
;*      Loop opening brace source line   : 464
;*      Loop closing brace source line   : 470
;*      Known Minimum Trip Count         : 1                    
;*      Known Maximum Trip Count         : 15                    
;*      Known Max Trip Count Factor      : 1
;*      Loop Carried Dependency Bound(^) : 0
;*      Unpartitioned Resource Bound     : 1
;*      Partitioned Resource Bound       : 1 (pre-sched)
;*
;*      Searching for software pipeline schedule at ...
;*         ii = 1  Schedule found with 8 iterations in parallel
;*
;*      Partitioned Resource Bound(*)    : 1 (post-sched)
;*
;*      Constant Extension #0 Used [C0]  : 0
;*      Constant Extension #1 Used [C1]  : 0
;*
;*      Resource Partition (may include "post-sched" split/spill moves):
;*
;*                                                A-side   B-side
;*      .L units                                     0        0     
;*      .S units                                     0        0     
;*      .M units                                     0        0     
;*      .N units                                     0        0     
;*      .D units                                     1        -     
;*      .B units                                     1*       -     
;*      .C units                                     -        0     
;*      .P units                                     -        0     
;*
;*      .M/.N units                                  0        0     
;*      .L/.S units                                  0        0     
;*      .L/.S/.C units                               0        0     
;*      .L/.S/.C/.M units                            0        1     
;*      .L/.S/.C/.M/.D units                         0        0     
;*
;*      .X cross paths                               0        0     
;*
;*      Bound(.D1 .D2 .D)                            1*       -     
;*      Bound(.M .N .MN)                             0        0     
;*      Bound(.L .S .LS)                             0        0     
;*      Bound(.L .S .C .LS .LSC)                     0        0     
;*      Bound(.L .S .C .M .LS .LSC .LSCM)            0        1*    
;*      Bound(.L .S .C .M .D .LS .LSC .LSCM .LSCMD)  1*       1*    
;*
;*      Register Usage Tables:
;*
;*      +----------------------------------+----------------------------------+
;*      |              ASIDE               |              BSIDE               |
;*      +----------------+--------+--------+----------------+--------+--------+
;*      |      "Axx"     |  "ALx" |  "AMx" |     "VBxx"     | "VBLx" | "VBMx" |
;*      +----------------|--------|--------|----------------|--------|--------+
;*      |0000000000111111|00000000|00000000|0000000000111111|00000000|00000000|
;*      |0123456789012345|01234567|01234567|0123456789012345|01234567|01234567|
;*      +----------------|--------|--------|----------------|--------|--------+
;*   0: |*               |        |        |                |**      |        |
;*      +----------------+--------+--------+----------------+--------+--------+
;*
;*      +----------------+                 +--------+       +-------+
;*      |      "Dxx"     |                 |  "Px"  |       |"CUCRx"|
;*      +----------------+                 +--------+       +-------+
;*      |0000000000111111|                 |00000000|       |0000   |
;*      |0123456789012345|                 |01234567|       |0123   |
;*      +----------------+                 +--------+       +-------+
;*   0: |*               |                 |        |       |       |
;*      +----------------+                 +--------+       +-------+
;*
;*      Done
;*
;*      Collapsed epilog stages       : 7
;*      Collapsed prolog stages       : 7
;*      Max amt of load speculation   : 6 bytes
;*
;*      Minimum safe trip count       : 1
;*      Min. prof. trip count  (est.) : 2
;*
;*      Mem bank conflicts/iter(est.) : { min 0.000, est 0.000, max 0.000 }
;*      Mem bank perf. penalty (est.) : 0.0%
;*
;*
;*      Total cycles (est.)         : 7 + trip_cnt * 1 = Between 8 and 22        
;*----------------------------------------------------------------------------*
;*        SINGLE SCHEDULED ITERATION
;*
;*        ||$C$C515||:
;*   0              TICK                               ; [A_U] 
;*   1              SLDUB   .D1     *D0++(1),BL0      ; [A_D1] |465| 
;*   2              NOP     0x5     ; [A_B] 
;*   7              VMAXW   .L2     BL1,BL0,BL1       ; [B_L2] |465| 
;*     ||           BNL     .B1     ||$C$C515||       ; [A_B] |463| 
;*   8              ; BRANCHCC OCCURS {||$C$C515||}   ; [] |463| 
;*----------------------------------------------------------------------------*
||$C$L57||:    ; PIPED LOOP PROLOG
;** --------------------------------------------------------------------------*
||$C$L58||:    ; PIPED LOOP KERNEL
;          EXCLUSIVE CPU CYCLES: 1
	.dwpsn	file "tidl_custom_maxpooling.c",line 463,column 24,is_stmt,isa 0

   [ A0]   ADDW    .D2     A0,0xffffffff,A0  ; [A_D2] |463| <0,7> collapsing predicate control
||         BNL     .B1     ||$C$L58||        ; [A_B] |463| <0,7> 
|| [!A0]   VMAXW   .L2     BL1,BL0,BL1       ; [B_L2] |465| <0,7> 
||         SLDUB   .D1     *D0++(1),BL0      ; [A_D1] |465| <6,1> 
||         TICK                               ; [A_U] <7,0> 

;** --------------------------------------------------------------------------*
||$C$L59||:    ; PIPED LOOP EPILOG
;          EXCLUSIVE CPU CYCLES: 1
           PROTCLR                            ; [A_U] VBL1
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 1
           MV      .L2     BL1,B0            ; [B_L2] 
;** --------------------------------------------------------------------------*
||$C$L60||:    
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "tidl_custom_maxpooling.c",line 461,column 22,is_stmt,isa 0
           ADDW    .D1     A3,0xffffffff,A3  ; [A_D1] |461| 

   [ A3]   B       .B1     ||$C$L52||        ; [A_B] |461| 
||         ADDW    .L2     BL3,0x1,BL3       ; [B_L2] |461| 

           ; BRANCHCC OCCURS {||$C$L52||}    ; [] |461| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 1
           MV      .L2     B0,B3             ; [B_L2] 
;** --------------------------------------------------------------------------*
||$C$L61||:    
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "tidl_custom_maxpooling.c",line 458,column 20,is_stmt,isa 0
           ADDW    .D1     A4,0xffffffff,A4  ; [A_D1] |458| 

   [ A4]   B       .B1     ||$C$L51||        ; [A_B] |458| 
||         STB     .D1X    B3,*D4++(1)       ; [A_D1] |472| 
||         ADDW    .D2     A6,0x1,A6         ; [A_D2] |458| 

           ; BRANCHCC OCCURS {||$C$L51||}    ; [] |458| 
;** --------------------------------------------------------------------------*
||$C$L62||:    
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "tidl_custom_maxpooling.c",line 456,column 18,is_stmt,isa 0
           ADDW    .D1     A1,0xffffffff,A1  ; [A_D1] |456| 

   [ A1]   B       .B1     ||$C$L50||        ; [A_B] |456| 
||         ADDW    .L2     B1,0x1,B1         ; [B_L2] |456| 

           ; BRANCHCC OCCURS {||$C$L50||}    ; [] |456| 
;** --------------------------------------------------------------------------*
||$C$L63||:    
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "tidl_custom_maxpooling.c",line 450,column 16,is_stmt,isa 0
           ADDW    .D1     A2,0xffffffff,A2  ; [A_D1] |450| 

   [ A2]   B       .B1     ||$C$L49||        ; [A_B] |450| 
||         ADDW    .D1     A7,0x1,A7         ; [A_D1] |450| 

           ; BRANCHCC OCCURS {||$C$L49||}    ; [] |450| 
;** --------------------------------------------------------------------------*
||$C$L64||:    
;          EXCLUSIVE CPU CYCLES: 7
           LDD     .D1     *SP(16),A13       ; [A_D1] 
	.dwcfi	restore_reg, 13

           LDD     .D1     *SP(32),A9        ; [A_D1] 
||         LDD     .D2     *SP(24),A11       ; [A_D2] 

	.dwcfi	restore_reg, 9
	.dwcfi	restore_reg, 11
$C$DW$216	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$216, DW_AT_low_pc(0x00)
	.dwattr $C$DW$216, DW_AT_TI_return


           RET     .B1     ; [A_B] 
||         ADDD    .D1     SP,0x18,SP        ; [A_D1] 

	.dwcfi	cfa_offset, 0
           ; RETURN OCCURS {RP}              ; [] 
	.dwattr $C$DW$183, DW_AT_TI_end_file("tidl_custom_maxpooling.c")
	.dwattr $C$DW$183, DW_AT_TI_end_line(0x1dc)
	.dwattr $C$DW$183, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$183

	.sect	".text:_Z31TIDL_refCustomMaxPoolingProcessP13sTIDL_Layer_tP20TIDL_CustomParams0_tPPvS4_PfS5_"
	.clink
	.global	||_Z31TIDL_refCustomMaxPoolingProcessP13sTIDL_Layer_tP20TIDL_CustomParams0_tPPvS4_PfS5_||

$C$DW$217	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$217, DW_AT_name("TIDL_refCustomMaxPoolingProcess")
	.dwattr $C$DW$217, DW_AT_low_pc(||_Z31TIDL_refCustomMaxPoolingProcessP13sTIDL_Layer_tP20TIDL_CustomParams0_tPPvS4_PfS5_||)
	.dwattr $C$DW$217, DW_AT_high_pc(0x00)
	.dwattr $C$DW$217, DW_AT_linkage_name("_Z31TIDL_refCustomMaxPoolingProcessP13sTIDL_Layer_tP20TIDL_CustomParams0_tPPvS4_PfS5_")
	.dwattr $C$DW$217, DW_AT_external
	.dwattr $C$DW$217, DW_AT_decl_file("tidl_custom_maxpooling.c")
	.dwattr $C$DW$217, DW_AT_decl_line(0x99)
	.dwattr $C$DW$217, DW_AT_decl_column(0x09)
	.dwattr $C$DW$217, DW_AT_TI_max_frame_size(0x138)
	.dwpsn	file "tidl_custom_maxpooling.c",line 159,column 1,is_stmt,address ||_Z31TIDL_refCustomMaxPoolingProcessP13sTIDL_Layer_tP20TIDL_CustomParams0_tPPvS4_PfS5_||,isa 0

	.dwfde $C$DW$CIE, ||_Z31TIDL_refCustomMaxPoolingProcessP13sTIDL_Layer_tP20TIDL_CustomParams0_tPPvS4_PfS5_||
$C$DW$218	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$218, DW_AT_name("tidlLayer")
	.dwattr $C$DW$218, DW_AT_location[DW_OP_reg4]

$C$DW$219	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$219, DW_AT_name("customMaxPoolingParams")
	.dwattr $C$DW$219, DW_AT_location[DW_OP_reg5]

$C$DW$220	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$220, DW_AT_name("inPtrs")
	.dwattr $C$DW$220, DW_AT_type(*$C$DW$T$347)
	.dwattr $C$DW$220, DW_AT_location[DW_OP_reg6]

$C$DW$221	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$221, DW_AT_name("outPtrs")
	.dwattr $C$DW$221, DW_AT_type(*$C$DW$T$347)
	.dwattr $C$DW$221, DW_AT_location[DW_OP_reg7]

$C$DW$222	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$222, DW_AT_name("currMin")
	.dwattr $C$DW$222, DW_AT_location[DW_OP_reg8]

$C$DW$223	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$223, DW_AT_name("currMax")
	.dwattr $C$DW$223, DW_AT_location[DW_OP_reg9]


;******************************************************************************
;* FUNCTION NAME: TIDL_refCustomMaxPoolingProcess(sTIDL_Layer_t *, TIDL_CustomParams0_t *, void **, void **, float *, float *)*
;*                                                                            *
;*   Regs Modified     : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,A14,   *
;*                           A15,VB0,VB1,VB2,VB3,VB4,VB5,VB6,VB7,VB8,VB9,VB10,*
;*                           VB11,VB12,VB13,VB14,VB15,AL0,AL1,AL2,AL3,AL4,AL5,*
;*                           AL6,AL7,AM0,AM1,AM2,AM3,AM4,AM5,AM6,AM7,D0,D1,D2,*
;*                           D3,D4,D5,D6,D7,D8,D9,D10,D11,D12,D13,D14,SP,VBL0,*
;*                           VBL1,VBL2,VBL3,VBL4,VBL5,VBL6,VBL7,VBM0,VBM1,    *
;*                           VBM2,VBM3,VBM4,VBM5,VBM6,VBM7,P0,P1,P2,P3,P4,P5, *
;*                           P6,P7,CUCR0,CUCR1,CUCR2,CUCR3                    *
;*   Regs Used         : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,A13,A14,   *
;*                           A15,VB0,VB1,VB2,VB3,VB4,VB5,VB6,VB7,VB8,VB9,VB10,*
;*                           VB11,VB12,VB13,VB14,VB15,AL0,AL1,AL2,AL3,AL4,AL5,*
;*                           AL6,AL7,AM0,AM1,AM2,AM3,AM4,AM5,AM6,AM7,D0,D1,D2,*
;*                           D3,D4,D5,D6,D7,D8,D9,D10,D11,D12,D13,D14,SP,VBL0,*
;*                           VBL1,VBL2,VBL3,VBL4,VBL5,VBL6,VBL7,VBM0,VBM1,    *
;*                           VBM2,VBM3,VBM4,VBM5,VBM6,VBM7,P0,P1,P2,P3,P4,P5, *
;*                           P6,P7,CUCR0,CUCR1,CUCR2,CUCR3                    *
;*   Local Frame Size  : 40 Args + 72 Auto + 200 Save = 312 byte              *
;******************************************************************************
||_Z31TIDL_refCustomMaxPoolingProcessP13sTIDL_Layer_tP20TIDL_CustomParams0_tPPvS4_PfS5_||:
;** --------------------------------------------------------------------------*
;* VB0   assigned to $O$C1
;* A1    assigned to $O$C2
;* A2    assigned to $O$C3
;* A8    assigned to $O$Lr52$inChPitch
;* A9    assigned to $O$Lr56$outPitch
;* A10   assigned to $O$Lr60$outChPitch
;* A11   assigned to $O$Lr64$numOutChannels
;* A12   assigned to $O$Lr66$kernelW
;* VB15  assigned to $O$Lr76$padH
;* A14   assigned to $O$Lr78$elementType
;* A15   assigned to $O$Lr84$inPtr
;* VB14  assigned to $O$Lr86$outPtr
;* A7    assigned to outPtrs
$C$DW$224	.dwtag  DW_TAG_variable
	.dwattr $C$DW$224, DW_AT_name("outPtrs")
	.dwattr $C$DW$224, DW_AT_type(*$C$DW$T$347)
	.dwattr $C$DW$224, DW_AT_location[DW_OP_reg7]

;* A6    assigned to inPtrs
$C$DW$225	.dwtag  DW_TAG_variable
	.dwattr $C$DW$225, DW_AT_name("inPtrs")
	.dwattr $C$DW$225, DW_AT_type(*$C$DW$T$347)
	.dwattr $C$DW$225, DW_AT_location[DW_OP_reg6]

;* A5    assigned to customMaxPoolingParams
$C$DW$226	.dwtag  DW_TAG_variable
	.dwattr $C$DW$226, DW_AT_name("customMaxPoolingParams")
	.dwattr $C$DW$226, DW_AT_type(*$C$DW$T$337)
	.dwattr $C$DW$226, DW_AT_location[DW_OP_reg5]

;* A13   assigned to tidlLayer
$C$DW$227	.dwtag  DW_TAG_variable
	.dwattr $C$DW$227, DW_AT_name("tidlLayer")
	.dwattr $C$DW$227, DW_AT_type(*$C$DW$T$354)
	.dwattr $C$DW$227, DW_AT_location[DW_OP_reg13]

	.dwcfi	cfa_offset, 0
;          EXCLUSIVE CPU CYCLES: 22
	.dwpsn	file "tidl_custom_maxpooling.c",line 174,column 19,is_stmt,isa 0
           LDW     .D1     *A5(8),B1         ; [A_D1] |174| 

           STD     .D1     A8,*SP(8)         ; [A_D1] 
||         STD     .D2X    A9,*SP++(-312)    ; [A_D2] 
	.dwcfi	save_reg_to_mem, 8, 8
	.dwcfi	cfa_offset, 312
	.dwcfi	save_reg_to_mem, 9, -312

           MVC     .S1     RP,A13            ; [A_S1] 
||         STD     .D1     A13,*SP(280)      ; [A_D1] 

	.dwcfi	save_reg_to_mem, 13, 280
	.dwpsn	file "tidl_custom_maxpooling.c",line 159,column 1,is_stmt,isa 0

           MV      .D2     A4,A13            ; [A_D2] |159| 
||         STD     .D1     A13,*SP(256)      ; [A_D1] 

	.dwcfi	save_reg_to_mem, 4101, 256
	.dwpsn	file "tidl_custom_maxpooling.c",line 166,column 17,is_stmt,isa 0

           LDW     .D1     *A5(16),B1        ; [A_D1] |176| 
||         ADDD    .L1     A13,0x120,A1      ; [A_L1] |166| 
||         STD     .D2     B1,*SP(88)        ; [A_D2] |174| 

           LDW     .D1     *A1(0),A0         ; [A_D1] |166| 
	.dwpsn	file "tidl_custom_maxpooling.c",line 175,column 19,is_stmt,isa 0

           STD     .D1     A12,*SP(288)      ; [A_D1] 
||         LDW     .D2     *A5(12),B2        ; [A_D2] |175| 
	.dwcfi	save_reg_to_mem, 12, 288

           STD     .D1     A15,*SP(264)      ; [A_D1] 
||         VST64B  .D2     VB15,*SP(192)     ; [A_D2] 
	.dwcfi	save_reg_to_mem, 15, 264
	.dwcfi	save_reg_to_mem, 63, 192

           VST64B  .D2     VB14,*SP(128)     ; [A_D2] 
||         STD     .D1     A14,*SP(272)      ; [A_D1] 

	.dwcfi	save_reg_to_mem, 62, 128
	.dwcfi	save_reg_to_mem, 14, 272
	.dwpsn	file "tidl_custom_maxpooling.c",line 187,column 12,is_stmt,isa 0

           ADDD    .L1     A1,0xfffffff4,D0  ; [A_L1] |187| 
||         STD     .D1     A10,*SP(304)      ; [A_D1] 
||         STD     .D2X    A11,*SP(296)      ; [A_D2] 

	.dwcfi	save_reg_to_mem, 10, 304
	.dwcfi	save_reg_to_mem, 11, 296
	.dwpsn	file "tidl_custom_maxpooling.c",line 168,column 19,is_stmt,isa 0

           ADDD    .L1     A1,0xfffffff0,D0  ; [A_L1] |168| 
||         STD     .D1X    B1,*SP(104)       ; [A_D1] |176| 
||         LDW     .D2     *D0(0),B0         ; [A_D2] |187| 

	.dwpsn	file "tidl_custom_maxpooling.c",line 166,column 17,is_stmt,isa 0

           LDW     .D1     *D0(0),A0         ; [A_D1] |168| 
||         STD     .D2X    A0,*SP(64)        ; [A_D2] |166| 
||         ADDD    .L1     A1,0xfffffffc,D1  ; [A_L1] |167| 

	.dwpsn	file "tidl_custom_maxpooling.c",line 167,column 18,is_stmt,isa 0

           LDW     .D2     *D1(0),A1         ; [A_D2] |167| 
||         ADDD    .L1     A1,0xffffffec,D1  ; [A_L1] |169| 
||         LDW     .D1     *A5(20),B1        ; [A_D1] |177| 

	.dwpsn	file "tidl_custom_maxpooling.c",line 173,column 19,is_stmt,isa 0

           LDW     .D1     *A5(24),B15       ; [A_D1] |178| 
||         LDW     .D2     *A5(4),A12        ; [A_D2] |173| 

	.dwpsn	file "tidl_custom_maxpooling.c",line 179,column 23,is_stmt,isa 0

           LDW     .D1     *A13(204),A14     ; [A_D1] |179| 
||         LDD     .D2     *A6(0),A15        ; [A_D2] |184| 

	.dwpsn	file "tidl_custom_maxpooling.c",line 170,column 20,is_stmt,isa 0

           ADDD    .L1     A13,0x6d0,A2      ; [A_L1] |170| 
||         STD     .D1X    B2,*SP(96)        ; [A_D1] |176| 
||         LDD     .D2     *A7(0),B14        ; [A_D2] |185| 

           LDW     .D1     *A2(0),A9         ; [A_D1] |170| 
||         LDW     .D2     *A2(8),A11        ; [A_D2] |172| 

	.dwpsn	file "tidl_custom_maxpooling.c",line 168,column 19,is_stmt,isa 0

           CMPGTW  .L1X    B0,0,A0           ; [A_L1] |187| 
||         STD     .D1     A0,*SP(80)        ; [A_D1] |168| 
||         LDW     .D2     *D1(0),A8         ; [A_D2] |169| 

           MV      .L1X    B0,A1             ; [A_L1] 
||         STD     .D1     A1,*SP(72)        ; [A_D1] |168| 
||         ADDD    .S1     A2,0xfffffffc,D0  ; [A_S1] |171| 
||         STD     .D2     B1,*SP(112)       ; [A_D2] |178| 

	.dwpsn	file "tidl_custom_maxpooling.c",line 171,column 22,is_stmt,isa 0

   [!A0]   B       .B1     ||$C$L70||        ; [A_B] |187| 
||         STD     .D1     A1,*SP(56)        ; [A_D1] 
||         LDW     .D2     *D0(0),A10        ; [A_D2] |171| 

	.dwpsn	file "tidl_custom_maxpooling.c",line 187,column 12,is_stmt,isa 0
           ; BRANCHCC OCCURS {||$C$L70||}    ; [] |187| 
;*----------------------------------------------------------------------------*
;*   SOFTWARE PIPELINE INFORMATION
;*      Disqualified loop: Loop contains control code
;*----------------------------------------------------------------------------*
||$C$L65||:    
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "tidl_custom_maxpooling.c",line 189,column 5,is_stmt,isa 0
           CMPEQW  .L1     A14,0,A0          ; [A_L1] |189| 
   [ A0]   B       .B1     ||$C$L68||        ; [A_B] |189| 
           ; BRANCHCC OCCURS {||$C$L68||}    ; [] |189| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "tidl_custom_maxpooling.c",line 209,column 10,is_stmt,isa 0
           CMPEQW  .L1     A14,0x1,A0        ; [A_L1] |209| 
   [ A0]   B       .B1     ||$C$L67||        ; [A_B] |209| 
           ; BRANCHCC OCCURS {||$C$L67||}    ; [] |209| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "tidl_custom_maxpooling.c",line 229,column 10,is_stmt,isa 0
           CMPEQW  .L1     A14,0x2,A0        ; [A_L1] |229| 
   [ A0]   B       .B1     ||$C$L66||        ; [A_B] |229| 
           ; BRANCHCC OCCURS {||$C$L66||}    ; [] |229| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "tidl_custom_maxpooling.c",line 249,column 10,is_stmt,isa 0

           MVK32   .L1     0xffffffff,A1     ; [A_L1] |276| 
||         CMPEQW  .S1     A14,0x3,A0        ; [A_S1] |249| 

	.dwpsn	file "tidl_custom_maxpooling.c",line 276,column 3,is_stmt,isa 0

   [!A0]   B       .B1     ||$C$L71||        ; [A_B] |276| 
|| [!A0]   STD     .D1     A1,*SP(120)       ; [A_D1] |276| 

           ; BRANCHCC OCCURS {||$C$L71||}    ; [] |276| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 14
           LDD     .D1     *SP(112),B0       ; [A_D1] 
	.dwpsn	file "tidl_custom_maxpooling.c",line 251,column 7,is_stmt,isa 0
           LDD     .D1     *SP(96),B12       ; [A_D1] |251| 

           LDD     .D1     *SP(64),A5        ; [A_D1] |251| 
||         LDD     .D2     *SP(104),B13      ; [A_D2] |251| 

           LDD     .D1     *SP(88),B0        ; [A_D1] |251| 
||         STW     .D2     B0,*SP(28)        ; [A_D2] |251| 

           LDD     .D1     *SP(72),A6        ; [A_D1] |251| 
||         LDD     .D2     *SP(80),A7        ; [A_D2] |251| 

           STD     .D1     A13,*SP(48)       ; [A_D1] |251| 
||         MVK32   .S2     0xffff8000,B1     ; [B_S2] |251| 

           STW     .D1X    B15,*SP(32)       ; [A_D1] |251| 
||         STD     .D2     B14,*SP(40)       ; [A_D2] |251| 

           STW     .D1X    B12,*SP(20)       ; [A_D1] |251| 
||         STH     .D2     B1,*SP(36)        ; [A_D2] |251| 

$C$DW$228	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$228, DW_AT_low_pc(0x00)
	.dwattr $C$DW$228, DW_AT_name("_Z28TIDL_refCustomMaxPoolingCoreIsEvPT_iiiiiiiiiiiiiS0_S1_P13sTIDL_Layer_t")
	.dwattr $C$DW$228, DW_AT_TI_call


           CALL    .B1     ||_Z28TIDL_refCustomMaxPoolingCoreIsEvPT_iiiiiiiiiiiiiS0_S1_P13sTIDL_Layer_t|| ; [A_B] |251| 
||         STW     .D1X    B0,*SP(16)        ; [A_D1] |251| 
||         STW     .D2     B13,*SP(24)       ; [A_D2] |251| 
||         MV      .L1     A15,A4            ; [A_L1] |251| 

$C$RL0:    ; CALL OCCURS (||_Z28TIDL_refCustomMaxPoolingCoreIsEvPT_iiiiiiiiiiiiiS0_S1_P13sTIDL_Layer_t||) arg:{A4,A5,A6,A7,A8,A9,A10,A11,A12} ret:{}  ; [] |251| 
           B       .B1     ||$C$L69||        ; [A_B] 
           ; BRANCH OCCURS {||$C$L69||}      ; [] 
;** --------------------------------------------------------------------------*
||$C$L66||:    
;          EXCLUSIVE CPU CYCLES: 14
           LDD     .D1     *SP(112),B0       ; [A_D1] 
	.dwpsn	file "tidl_custom_maxpooling.c",line 231,column 7,is_stmt,isa 0
           LDD     .D1     *SP(96),B12       ; [A_D1] |231| 

           LDD     .D1     *SP(64),A5        ; [A_D1] |231| 
||         LDD     .D2     *SP(104),B13      ; [A_D2] |231| 

           LDD     .D1     *SP(88),B0        ; [A_D1] |231| 
||         STW     .D2     B0,*SP(28)        ; [A_D2] |231| 

           LDD     .D1     *SP(72),A6        ; [A_D1] |231| 
||         LDD     .D2     *SP(80),A7        ; [A_D2] |231| 

           STD     .D1     A13,*SP(48)       ; [A_D1] |231| 

           STW     .D1X    B15,*SP(32)       ; [A_D1] |231| 
||         STD     .D2     B14,*SP(40)       ; [A_D2] |231| 
||         MVKU32  .L2     0,B1              ; [B_L2] |231| 

           STW     .D1X    B12,*SP(20)       ; [A_D1] |231| 
||         STH     .D2     B1,*SP(36)        ; [A_D2] |231| 

$C$DW$229	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$229, DW_AT_low_pc(0x00)
	.dwattr $C$DW$229, DW_AT_name("_Z28TIDL_refCustomMaxPoolingCoreItEvPT_iiiiiiiiiiiiiS0_S1_P13sTIDL_Layer_t")
	.dwattr $C$DW$229, DW_AT_TI_call


           CALL    .B1     ||_Z28TIDL_refCustomMaxPoolingCoreItEvPT_iiiiiiiiiiiiiS0_S1_P13sTIDL_Layer_t|| ; [A_B] |231| 
||         STW     .D1X    B0,*SP(16)        ; [A_D1] |231| 
||         STW     .D2     B13,*SP(24)       ; [A_D2] |231| 
||         MV      .L1     A15,A4            ; [A_L1] |231| 

$C$RL1:    ; CALL OCCURS (||_Z28TIDL_refCustomMaxPoolingCoreItEvPT_iiiiiiiiiiiiiS0_S1_P13sTIDL_Layer_t||) arg:{A4,A5,A6,A7,A8,A9,A10,A11,A12} ret:{}  ; [] |231| 
           B       .B1     ||$C$L69||        ; [A_B] 
           ; BRANCH OCCURS {||$C$L69||}      ; [] 
;** --------------------------------------------------------------------------*
||$C$L67||:    
;          EXCLUSIVE CPU CYCLES: 14
           LDD     .D1     *SP(112),B0       ; [A_D1] 
	.dwpsn	file "tidl_custom_maxpooling.c",line 211,column 7,is_stmt,isa 0
           LDD     .D1     *SP(96),B12       ; [A_D1] |211| 

           LDD     .D1     *SP(64),A5        ; [A_D1] |211| 
||         LDD     .D2     *SP(104),B13      ; [A_D2] |211| 

           LDD     .D1     *SP(88),B0        ; [A_D1] |211| 
||         STW     .D2     B0,*SP(28)        ; [A_D2] |211| 

           LDD     .D1     *SP(72),A6        ; [A_D1] |211| 
||         LDD     .D2     *SP(80),A7        ; [A_D2] |211| 

           STD     .D1     A13,*SP(48)       ; [A_D1] |211| 
||         MVK32   .S2     0xffffff80,B1     ; [B_S2] |211| 

           STW     .D1X    B15,*SP(32)       ; [A_D1] |211| 
||         STD     .D2     B14,*SP(40)       ; [A_D2] |211| 

           STW     .D1X    B12,*SP(20)       ; [A_D1] |211| 
||         STB     .D2     B1,*SP(36)        ; [A_D2] |211| 

$C$DW$230	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$230, DW_AT_low_pc(0x00)
	.dwattr $C$DW$230, DW_AT_name("_Z28TIDL_refCustomMaxPoolingCoreIaEvPT_iiiiiiiiiiiiiS0_S1_P13sTIDL_Layer_t")
	.dwattr $C$DW$230, DW_AT_TI_call


           CALL    .B1     ||_Z28TIDL_refCustomMaxPoolingCoreIaEvPT_iiiiiiiiiiiiiS0_S1_P13sTIDL_Layer_t|| ; [A_B] |211| 
||         STW     .D1X    B0,*SP(16)        ; [A_D1] |211| 
||         STW     .D2     B13,*SP(24)       ; [A_D2] |211| 
||         MV      .L1     A15,A4            ; [A_L1] |211| 

$C$RL2:    ; CALL OCCURS (||_Z28TIDL_refCustomMaxPoolingCoreIaEvPT_iiiiiiiiiiiiiS0_S1_P13sTIDL_Layer_t||) arg:{A4,A5,A6,A7,A8,A9,A10,A11,A12} ret:{}  ; [] |211| 
           B       .B1     ||$C$L69||        ; [A_B] 
           ; BRANCH OCCURS {||$C$L69||}      ; [] 
;** --------------------------------------------------------------------------*
||$C$L68||:    
;          EXCLUSIVE CPU CYCLES: 13
           LDD     .D1     *SP(112),B0       ; [A_D1] 
	.dwpsn	file "tidl_custom_maxpooling.c",line 191,column 7,is_stmt,isa 0
           LDD     .D1     *SP(96),B12       ; [A_D1] |191| 

           LDD     .D1     *SP(64),A5        ; [A_D1] |191| 
||         LDD     .D2     *SP(104),B13      ; [A_D2] |191| 

           LDD     .D1     *SP(88),B0        ; [A_D1] |191| 
||         STW     .D2     B0,*SP(28)        ; [A_D2] |191| 

           LDD     .D1     *SP(72),A6        ; [A_D1] |191| 
||         LDD     .D2     *SP(80),A7        ; [A_D2] |191| 

           STD     .D1     A13,*SP(48)       ; [A_D1] |191| 

           STW     .D1X    B15,*SP(32)       ; [A_D1] |191| 
||         STD     .D2     B14,*SP(40)       ; [A_D2] |191| 
||         MVKU32  .L2     0,B1              ; [B_L2] |191| 

           STW     .D1X    B12,*SP(20)       ; [A_D1] |191| 
||         STB     .D2     B1,*SP(36)        ; [A_D2] |191| 

$C$DW$231	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$231, DW_AT_low_pc(0x00)
	.dwattr $C$DW$231, DW_AT_name("_Z28TIDL_refCustomMaxPoolingCoreIhEvPT_iiiiiiiiiiiiiS0_S1_P13sTIDL_Layer_t")
	.dwattr $C$DW$231, DW_AT_TI_call


           CALL    .B1     ||_Z28TIDL_refCustomMaxPoolingCoreIhEvPT_iiiiiiiiiiiiiS0_S1_P13sTIDL_Layer_t|| ; [A_B] |191| 
||         STW     .D1X    B0,*SP(16)        ; [A_D1] |191| 
||         STW     .D2     B13,*SP(24)       ; [A_D2] |191| 
||         MV      .L1     A15,A4            ; [A_L1] |191| 

$C$RL3:    ; CALL OCCURS (||_Z28TIDL_refCustomMaxPoolingCoreIhEvPT_iiiiiiiiiiiiiS0_S1_P13sTIDL_Layer_t||) arg:{A4,A5,A6,A7,A8,A9,A10,A11,A12} ret:{}  ; [] |191| 
;** --------------------------------------------------------------------------*
||$C$L69||:    
;          EXCLUSIVE CPU CYCLES: 8
           LDD     .D1     *SP(56),A0        ; [A_D1] 
	.dwpsn	file "tidl_custom_maxpooling.c",line 187,column 12,is_stmt,isa 0
           ADDW    .D1     A0,0xffffffff,A0  ; [A_D1] |187| 

   [ A0]   B       .B1     ||$C$L65||        ; [A_B] |187| 
||         STD     .D1     A0,*SP(56)        ; [A_D1] |187| 

           ; BRANCHCC OCCURS {||$C$L65||}    ; [] |187| 
;** --------------------------------------------------------------------------*
||$C$L70||:    
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "tidl_custom_maxpooling.c",line 276,column 3,is_stmt,isa 0
           MVKU32  .L1     0,A0              ; [A_L1] |276| 
           STD     .D1     A0,*SP(120)       ; [A_D1] |276| 
;** --------------------------------------------------------------------------*
||$C$L71||:    
;          EXCLUSIVE CPU CYCLES: 16

           LDD     .D2     *SP(256),A13      ; [A_D2] 
||         VLD64B  .D1     *SP(128),VB14     ; [A_D1] 
	.dwcfi	restore_reg, 62

           VLD64B  .D1     *SP(192),VB15     ; [A_D1] 
||         LDD     .D2     *SP(120),A4       ; [A_D2] 

	.dwcfi	restore_reg, 63
           MVC     .S1     A13,RP            ; [A_S1] 
	.dwcfi	restore_reg, 4101

           LDD     .D1     *SP(272),A14      ; [A_D1] 
||         LDD     .D2     *SP(264),A15      ; [A_D2] 
	.dwcfi	restore_reg, 14
	.dwcfi	restore_reg, 15

           LDD     .D1     *SP(296),A11      ; [A_D1] 
||         LDD     .D2     *SP(288),A12      ; [A_D2] 
	.dwcfi	restore_reg, 11
	.dwcfi	restore_reg, 12

           LDD     .D1     *SP(312),A9       ; [A_D1] 
||         LDD     .D2     *SP(304),A10      ; [A_D2] 
	.dwcfi	restore_reg, 9
	.dwcfi	restore_reg, 10

           LDD     .D1     *SP(280),A13      ; [A_D1] 
||         LDD     .D2     *SP(320),A8       ; [A_D2] 

	.dwcfi	restore_reg, 13
	.dwcfi	restore_reg, 8
$C$DW$232	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$232, DW_AT_low_pc(0x00)
	.dwattr $C$DW$232, DW_AT_TI_return


           RET     .B1     ; [A_B] 
||         ADDD    .D1     SP,0x138,SP       ; [A_D1] 

	.dwcfi	cfa_offset, 0
           ; RETURN OCCURS {RP}              ; [] 
	.dwattr $C$DW$217, DW_AT_TI_end_file("tidl_custom_maxpooling.c")
	.dwattr $C$DW$217, DW_AT_TI_end_line(0x115)
	.dwattr $C$DW$217, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$217

	.sect	".text:_Z28TIDL_customMaxPoolDspProcessPvP13sTIDL_Layer_tP20TIDL_CustomParams0_tPS_S4_"
	.clink
	.global	||_Z28TIDL_customMaxPoolDspProcessPvP13sTIDL_Layer_tP20TIDL_CustomParams0_tPS_S4_||

$C$DW$233	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$233, DW_AT_name("TIDL_customMaxPoolDspProcess")
	.dwattr $C$DW$233, DW_AT_low_pc(||_Z28TIDL_customMaxPoolDspProcessPvP13sTIDL_Layer_tP20TIDL_CustomParams0_tPS_S4_||)
	.dwattr $C$DW$233, DW_AT_high_pc(0x00)
	.dwattr $C$DW$233, DW_AT_linkage_name("_Z28TIDL_customMaxPoolDspProcessPvP13sTIDL_Layer_tP20TIDL_CustomParams0_tPS_S4_")
	.dwattr $C$DW$233, DW_AT_external
	.dwattr $C$DW$233, DW_AT_decl_file("tidl_custom_maxpooling.c")
	.dwattr $C$DW$233, DW_AT_decl_line(0x11a)
	.dwattr $C$DW$233, DW_AT_decl_column(0x09)
	.dwattr $C$DW$233, DW_AT_TI_max_frame_size(0x98)
	.dwpsn	file "tidl_custom_maxpooling.c",line 287,column 1,is_stmt,address ||_Z28TIDL_customMaxPoolDspProcessPvP13sTIDL_Layer_tP20TIDL_CustomParams0_tPS_S4_||,isa 0

	.dwfde $C$DW$CIE, ||_Z28TIDL_customMaxPoolDspProcessPvP13sTIDL_Layer_tP20TIDL_CustomParams0_tPS_S4_||
$C$DW$234	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$234, DW_AT_name("tidlHandle")
	.dwattr $C$DW$234, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$234, DW_AT_location[DW_OP_reg4]

$C$DW$235	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$235, DW_AT_name("tidlLayer")
	.dwattr $C$DW$235, DW_AT_location[DW_OP_reg5]

$C$DW$236	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$236, DW_AT_name("customMaxPoolingParams")
	.dwattr $C$DW$236, DW_AT_location[DW_OP_reg6]

$C$DW$237	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$237, DW_AT_name("inPtrs")
	.dwattr $C$DW$237, DW_AT_type(*$C$DW$T$347)
	.dwattr $C$DW$237, DW_AT_location[DW_OP_reg7]

$C$DW$238	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$238, DW_AT_name("outPtrs")
	.dwattr $C$DW$238, DW_AT_type(*$C$DW$T$347)
	.dwattr $C$DW$238, DW_AT_location[DW_OP_reg8]


;******************************************************************************
;* FUNCTION NAME: TIDL_customMaxPoolDspProcess(void *, sTIDL_Layer_t *, TIDL_CustomParams0_t *, void **, void **)*
;*                                                                            *
;*   Regs Modified     : A0,A1,A2,A3,A4,A5,A6,A7,A9,A10,A11,VB0,VB1,VB2,VB3,  *
;*                           VB4,VB5,VB6,VB7,VB8,VB9,VB10,VB11,VB12,VB13,AL0, *
;*                           AL1,AL2,AL3,AL4,AL5,AL6,AL7,AM0,AM1,AM2,AM3,AM4, *
;*                           AM5,AM6,AM7,D0,D1,D2,D3,D4,D5,D6,D7,D8,D9,D10,   *
;*                           D11,D12,D13,D14,SP,VBL0,VBL1,VBL2,VBL3,VBL4,VBL5,*
;*                           VBL6,VBL7,VBM0,VBM1,VBM2,VBM3,VBM4,VBM5,VBM6,    *
;*                           VBM7,P0,P1,P2,P3,P4,P5,P6,P7,CUCR0,CUCR1,CUCR2,  *
;*                           CUCR3                                            *
;*   Regs Used         : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,VB0,VB1,VB2,   *
;*                           VB3,VB4,VB5,VB6,VB7,VB8,VB9,VB10,VB11,VB12,VB13, *
;*                           AL0,AL1,AL2,AL3,AL4,AL5,AL6,AL7,AM0,AM1,AM2,AM3, *
;*                           AM4,AM5,AM6,AM7,D0,D1,D2,D3,D4,D5,D6,D7,D8,D9,   *
;*                           D10,D11,D12,D13,D14,SP,VBL0,VBL1,VBL2,VBL3,VBL4, *
;*                           VBL5,VBL6,VBL7,VBM0,VBM1,VBM2,VBM3,VBM4,VBM5,    *
;*                           VBM6,VBM7,P0,P1,P2,P3,P4,P5,P6,P7,CUCR0,CUCR1,   *
;*                           CUCR2,CUCR3                                      *
;*   Local Frame Size  : 0 Args + 128 Auto + 24 Save = 152 byte               *
;******************************************************************************
||_Z28TIDL_customMaxPoolDspProcessPvP13sTIDL_Layer_tP20TIDL_CustomParams0_tPS_S4_||:
;** --------------------------------------------------------------------------*
$C$DW$239	.dwtag  DW_TAG_variable
	.dwattr $C$DW$239, DW_AT_name("initParams")
	.dwattr $C$DW$239, DW_AT_type(*$C$DW$T$342)
	.dwattr $C$DW$239, DW_AT_location[DW_OP_bregx 0x6f 16]

$C$DW$240	.dwtag  DW_TAG_variable
	.dwattr $C$DW$240, DW_AT_name("kernelInitArgs")
	.dwattr $C$DW$240, DW_AT_type(*$C$DW$T$334)
	.dwattr $C$DW$240, DW_AT_location[DW_OP_bregx 0x6f 80]

;* D0    assigned to $O$Lr4$status
;* A8    assigned to outPtrs
$C$DW$241	.dwtag  DW_TAG_variable
	.dwattr $C$DW$241, DW_AT_name("outPtrs")
	.dwattr $C$DW$241, DW_AT_type(*$C$DW$T$347)
	.dwattr $C$DW$241, DW_AT_location[DW_OP_reg8]

;* A10   assigned to inPtrs
$C$DW$242	.dwtag  DW_TAG_variable
	.dwattr $C$DW$242, DW_AT_name("inPtrs")
	.dwattr $C$DW$242, DW_AT_type(*$C$DW$T$347)
	.dwattr $C$DW$242, DW_AT_location[DW_OP_reg10]

;* A5    assigned to customMaxPoolingParams
$C$DW$243	.dwtag  DW_TAG_variable
	.dwattr $C$DW$243, DW_AT_name("customMaxPoolingParams")
	.dwattr $C$DW$243, DW_AT_type(*$C$DW$T$337)
	.dwattr $C$DW$243, DW_AT_location[DW_OP_reg5]

;* A9    assigned to tidlHandle
$C$DW$244	.dwtag  DW_TAG_variable
	.dwattr $C$DW$244, DW_AT_name("tidlHandle")
	.dwattr $C$DW$244, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$244, DW_AT_location[DW_OP_reg9]

	.dwcfi	cfa_offset, 0
;          EXCLUSIVE CPU CYCLES: 12

           STD     .D1     A9,*SP(8)         ; [A_D1] 
||         STD     .D2X    A10,*SP++(-152)   ; [A_D2] 
	.dwcfi	save_reg_to_mem, 9, 8
	.dwcfi	cfa_offset, 152
	.dwcfi	save_reg_to_mem, 10, -152

           MVC     .S1     RP,A11            ; [A_S1] 
||         STD     .D1     A11,*SP(144)      ; [A_D1] 

	.dwcfi	save_reg_to_reg, 4101, 11
	.dwcfi	save_reg_to_mem, 11, 144
	.dwpsn	file "tidl_custom_maxpooling.c",line 296,column 3,is_stmt,isa 0
           STKW    .D1     0x1,*SP(80)       ; [A_D1] |296| 
	.dwpsn	file "tidl_custom_maxpooling.c",line 287,column 1,is_stmt,isa 0
$C$DW$245	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$245, DW_AT_low_pc(0x00)
	.dwattr $C$DW$245, DW_AT_name("__c7xabi_strasg")
	.dwattr $C$DW$245, DW_AT_TI_call


           CALL    .B1     ||__c7xabi_strasg|| ; [A_B] |297| 
||         ADDD    .D1     SP,0x54,A4        ; [A_D1] |297| 
||         MV      .D2     A7,A10            ; [A_D2] |287| 
||         MVKU32  .L1     0x28,A6           ; [A_L1] |297| 
||         MV      .S1     A4,A9             ; [A_S1] |287| 
||         MV      .M1     A6,A5             ; [A_M1] |287| 

	.dwpsn	file "tidl_custom_maxpooling.c",line 297,column 3,is_stmt,isa 0
$C$RL4:    ; CALL OCCURS (||__c7xabi_strasg||) arg:{A4,A5,A6} ret:{A4}  ; [] |297| 
	.dwpsn	file "tidl_custom_maxpooling.c",line 307,column 3,is_stmt,isa 0
           ADDKPC  .D1     $PCR_OFFSET((||_Z31TIDL_customMaxPool_ixX_oxX_initPvPK18TIDL_bufParams3D_tS2_S_||+0)),A0 ; [A_D1] |307| 
	.dwpsn	file "tidl_custom_maxpooling.c",line 303,column 3,is_stmt,isa 0

           ADDKPC  .D1     $PCR_OFFSET((||_Z40TIDL_customMaxPool_ixX_oxX_getHandleSizePv||+0)),A1 ; [A_D1] |303| 
||         ADDKPC  .D2     $PCR_OFFSET((||_Z31TIDL_customMaxPool_ixX_oxX_execPvPK18TIDL_bufParams3D_tS2_PPKvS_||+0)),D1 ; [A_D2] |312| 

	.dwpsn	file "tidl_custom_maxpooling.c",line 315,column 3,is_stmt,isa 0

           ADDD    .D1     SP,0x10,A5        ; [A_D1] |320| 
||         ADDD    .D2     SP,0x50,D0        ; [A_D2] |315| 

	.dwpsn	file "tidl_custom_maxpooling.c",line 301,column 3,is_stmt,isa 0
           STKW    .D1     0,*SP(16)         ; [A_D1] |301| 
	.dwpsn	file "tidl_custom_maxpooling.c",line 307,column 3,is_stmt,isa 0

           STD     .D2X    A0,*SP(32)        ; [A_D2] |307| 
||         STD     .D1     D1,*SP(40)        ; [A_D1] |312| 

	.dwpsn	file "tidl_custom_maxpooling.c",line 303,column 3,is_stmt,isa 0
$C$DW$246	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$246, DW_AT_low_pc(0x00)
	.dwattr $C$DW$246, DW_AT_name("_Z17TIDL_DataflowInitPvP23TIDL_DataflowInitParams")
	.dwattr $C$DW$246, DW_AT_TI_call


           CALL    .B1     ||_Z17TIDL_DataflowInitPvP23TIDL_DataflowInitParams|| ; [A_B] |320| 
||         STD     .D1     D0,*SP(48)        ; [A_D1] |315| 
||         STD     .D2X    A1,*SP(24)        ; [A_D2] |303| 
||         MV      .L1     A9,A4             ; [A_L1] |320| 

	.dwpsn	file "tidl_custom_maxpooling.c",line 320,column 3,is_stmt,isa 0
$C$RL5:    ; CALL OCCURS (||_Z17TIDL_DataflowInitPvP23TIDL_DataflowInitParams||) arg:{A4,A5} ret:{A4}  ; [] |320| 
           CMPEQW  .L1     A4,0,A0           ; [A_L1] |320| 

   [!A0]   B       .B1     ||$C$L72||        ; [A_B] |320| 
||         MV      .D1     A9,A4             ; [A_D1] |327| 
||         MV      .D2     A10,A5            ; [A_D2] |327| 
||         MV      .S1     A8,A6             ; [A_S1] |327| 
||         MVK32   .L1     0xffffffff,D0     ; [A_L1] |322| 

           ; BRANCHCC OCCURS {||$C$L72||}    ; [] |320| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "tidl_custom_maxpooling.c",line 327,column 5,is_stmt,isa 0
$C$DW$247	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$247, DW_AT_low_pc(0x00)
	.dwattr $C$DW$247, DW_AT_name("_Z20TIDL_DataflowProcessPvPS_S0_")
	.dwattr $C$DW$247, DW_AT_TI_call

           CALL    .B1     ||_Z20TIDL_DataflowProcessPvPS_S0_|| ; [A_B] |327| 
$C$RL6:    ; CALL OCCURS (||_Z20TIDL_DataflowProcessPvPS_S0_||) arg:{A4,A5,A6} ret:{A4}  ; [] |327| 
           MV      .D1     A4,D0             ; [A_D1] |327| 
;** --------------------------------------------------------------------------*
||$C$L72||:    
;          EXCLUSIVE CPU CYCLES: 8
	.dwpsn	file "tidl_custom_maxpooling.c",line 332,column 3,is_stmt,isa 0

           MVC     .S1     A11,RP            ; [A_S1] 
||         MV      .D1     D0,A4             ; [A_D1] |332| 

	.dwcfi	restore_reg, 4101
           LDD     .D1     *SP(152),A10      ; [A_D1] 
	.dwcfi	restore_reg, 10

           LDD     .D1     *SP(144),A11      ; [A_D1] 
||         LDD     .D2     *SP(160),A9       ; [A_D2] 

	.dwcfi	restore_reg, 11
	.dwcfi	restore_reg, 9
$C$DW$248	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$248, DW_AT_low_pc(0x00)
	.dwattr $C$DW$248, DW_AT_TI_return


           RET     .B1     ; [A_B] 
||         ADDD    .D1     SP,0x98,SP        ; [A_D1] 

	.dwcfi	cfa_offset, 0
           ; RETURN OCCURS {RP}              ; [] 
	.dwattr $C$DW$233, DW_AT_TI_end_file("tidl_custom_maxpooling.c")
	.dwattr $C$DW$233, DW_AT_TI_end_line(0x14e)
	.dwattr $C$DW$233, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$233

	.sect	".text:_Z28TIDL_customMaxPoolingProcessPvP13sTIDL_Layer_tPS_S2_S_S_PK20sTIDL_sysMemHandle_tif"
	.clink
	.global	||_Z28TIDL_customMaxPoolingProcessPvP13sTIDL_Layer_tPS_S2_S_S_PK20sTIDL_sysMemHandle_tif||

$C$DW$249	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$249, DW_AT_name("TIDL_customMaxPoolingProcess")
	.dwattr $C$DW$249, DW_AT_low_pc(||_Z28TIDL_customMaxPoolingProcessPvP13sTIDL_Layer_tPS_S2_S_S_PK20sTIDL_sysMemHandle_tif||)
	.dwattr $C$DW$249, DW_AT_high_pc(0x00)
	.dwattr $C$DW$249, DW_AT_linkage_name("_Z28TIDL_customMaxPoolingProcessPvP13sTIDL_Layer_tPS_S2_S_S_PK20sTIDL_sysMemHandle_tif")
	.dwattr $C$DW$249, DW_AT_external
	.dwattr $C$DW$249, DW_AT_decl_file("tidl_custom_maxpooling.c")
	.dwattr $C$DW$249, DW_AT_decl_line(0x226)
	.dwattr $C$DW$249, DW_AT_decl_column(0x09)
	.dwattr $C$DW$249, DW_AT_TI_max_frame_size(0x28)
	.dwpsn	file "tidl_custom_maxpooling.c",line 559,column 1,is_stmt,address ||_Z28TIDL_customMaxPoolingProcessPvP13sTIDL_Layer_tPS_S2_S_S_PK20sTIDL_sysMemHandle_tif||,isa 0

	.dwfde $C$DW$CIE, ||_Z28TIDL_customMaxPoolingProcessPvP13sTIDL_Layer_tPS_S2_S_S_PK20sTIDL_sysMemHandle_tif||
$C$DW$250	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$250, DW_AT_name("tidlHandle")
	.dwattr $C$DW$250, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$250, DW_AT_location[DW_OP_reg4]

$C$DW$251	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$251, DW_AT_name("tidlLayer")
	.dwattr $C$DW$251, DW_AT_location[DW_OP_reg5]

$C$DW$252	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$252, DW_AT_name("inPtrs")
	.dwattr $C$DW$252, DW_AT_type(*$C$DW$T$347)
	.dwattr $C$DW$252, DW_AT_location[DW_OP_reg6]

$C$DW$253	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$253, DW_AT_name("outPtrs")
	.dwattr $C$DW$253, DW_AT_type(*$C$DW$T$347)
	.dwattr $C$DW$253, DW_AT_location[DW_OP_reg7]

$C$DW$254	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$254, DW_AT_name("params")
	.dwattr $C$DW$254, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$254, DW_AT_location[DW_OP_reg8]

$C$DW$255	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$255, DW_AT_name("dmaUtilsContext")
	.dwattr $C$DW$255, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$255, DW_AT_location[DW_OP_reg9]

$C$DW$256	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$256, DW_AT_name("sysMems")
	.dwattr $C$DW$256, DW_AT_location[DW_OP_reg10]

$C$DW$257	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$257, DW_AT_name("execMode")
	.dwattr $C$DW$257, DW_AT_location[DW_OP_reg11]

$C$DW$258	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$258, DW_AT_name("maxTensorScale")
	.dwattr $C$DW$258, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$258, DW_AT_location[DW_OP_reg12]


;******************************************************************************
;* FUNCTION NAME: TIDL_customMaxPoolingProcess(void *, sTIDL_Layer_t *, void **, void **, void *, void *, const sTIDL_sysMemHandle_t *, int, float)*
;*                                                                            *
;*   Regs Modified     : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A12,VB0,VB1,VB2,   *
;*                           VB3,VB4,VB5,VB6,VB7,VB8,VB9,VB10,VB11,VB12,VB13, *
;*                           AL0,AL1,AL2,AL3,AL4,AL5,AL6,AL7,AM0,AM1,AM2,AM3, *
;*                           AM4,AM5,AM6,AM7,D0,D1,D2,D3,D4,D5,D6,D7,D8,D9,   *
;*                           D10,D11,D12,D13,D14,SP,VBL0,VBL1,VBL2,VBL3,VBL4, *
;*                           VBL5,VBL6,VBL7,VBM0,VBM1,VBM2,VBM3,VBM4,VBM5,    *
;*                           VBM6,VBM7,P0,P1,P2,P3,P4,P5,P6,P7,CUCR0,CUCR1,   *
;*                           CUCR2,CUCR3                                      *
;*   Regs Used         : A0,A1,A2,A3,A4,A5,A6,A7,A8,A9,A10,A11,A12,VB0,VB1,   *
;*                           VB2,VB3,VB4,VB5,VB6,VB7,VB8,VB9,VB10,VB11,VB12,  *
;*                           VB13,AL0,AL1,AL2,AL3,AL4,AL5,AL6,AL7,AM0,AM1,AM2,*
;*                           AM3,AM4,AM5,AM6,AM7,D0,D1,D2,D3,D4,D5,D6,D7,D8,  *
;*                           D9,D10,D11,D12,D13,D14,SP,VBL0,VBL1,VBL2,VBL3,   *
;*                           VBL4,VBL5,VBL6,VBL7,VBM0,VBM1,VBM2,VBM3,VBM4,    *
;*                           VBM5,VBM6,VBM7,P0,P1,P2,P3,P4,P5,P6,P7,CUCR0,    *
;*                           CUCR1,CUCR2,CUCR3                                *
;*   Local Frame Size  : 0 Args + 8 Auto + 32 Save = 40 byte                  *
;******************************************************************************
||_Z28TIDL_customMaxPoolingProcessPvP13sTIDL_Layer_tPS_S2_S_S_PK20sTIDL_sysMemHandle_tif||:
;** --------------------------------------------------------------------------*
$C$DW$259	.dwtag  DW_TAG_variable
	.dwattr $C$DW$259, DW_AT_name("currMin")
	.dwattr $C$DW$259, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$259, DW_AT_location[DW_OP_bregx 0x6f 16]

$C$DW$260	.dwtag  DW_TAG_variable
	.dwattr $C$DW$260, DW_AT_name("currMax")
	.dwattr $C$DW$260, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$260, DW_AT_location[DW_OP_bregx 0x6f 20]

;* A11   assigned to execMode
$C$DW$261	.dwtag  DW_TAG_variable
	.dwattr $C$DW$261, DW_AT_name("execMode")
	.dwattr $C$DW$261, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$261, DW_AT_location[DW_OP_reg11]

;* D0    assigned to params
$C$DW$262	.dwtag  DW_TAG_variable
	.dwattr $C$DW$262, DW_AT_name("params")
	.dwattr $C$DW$262, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$262, DW_AT_location[DW_OP_regx 0x60]

;* VB1   assigned to outPtrs
$C$DW$263	.dwtag  DW_TAG_variable
	.dwattr $C$DW$263, DW_AT_name("outPtrs")
	.dwattr $C$DW$263, DW_AT_type(*$C$DW$T$347)
	.dwattr $C$DW$263, DW_AT_location[DW_OP_regx 0x31]

;* A7    assigned to inPtrs
$C$DW$264	.dwtag  DW_TAG_variable
	.dwattr $C$DW$264, DW_AT_name("inPtrs")
	.dwattr $C$DW$264, DW_AT_type(*$C$DW$T$347)
	.dwattr $C$DW$264, DW_AT_location[DW_OP_reg7]

;* A10   assigned to tidlLayer
$C$DW$265	.dwtag  DW_TAG_variable
	.dwattr $C$DW$265, DW_AT_name("tidlLayer")
	.dwattr $C$DW$265, DW_AT_type(*$C$DW$T$354)
	.dwattr $C$DW$265, DW_AT_location[DW_OP_reg10]

	.dwcfi	cfa_offset, 0
;          EXCLUSIVE CPU CYCLES: 9

           STD     .D1     A8,*SP(8)         ; [A_D1] 
||         STD     .D2X    A9,*SP++(-40)     ; [A_D2] 

	.dwcfi	save_reg_to_mem, 8, 8
	.dwcfi	cfa_offset, 40
	.dwcfi	save_reg_to_mem, 9, -40
	.dwpsn	file "tidl_custom_maxpooling.c",line 561,column 24,is_stmt,isa 0
           STKW    .D1     0,*SP(16)         ; [A_D1] |561| 
	.dwpsn	file "tidl_custom_maxpooling.c",line 561,column 37,is_stmt,isa 0
           LDUW    .D1     *SP(16),B0        ; [A_D1] |561| 
	.dwpsn	file "tidl_custom_maxpooling.c",line 576,column 3,is_stmt,isa 0

           CMPEQW  .L1     A11,0,AL0         ; [A_L1] |576| 
||         CMPEQW  .S1     A11,0x3,AL1       ; [A_S1] |576| 

           XORD    .L1     AL0,0x1,D1        ; [A_L1] |576| 
||         XORD    .S1     AL1,0x1,D2        ; [A_S1] |576| 

           ANDW    .D1     D2,D1,AL0         ; [A_D1] |576| 
	.dwpsn	file "tidl_custom_maxpooling.c",line 559,column 1,is_stmt,isa 0

           CMPEQW  .L1     AL0,0,A0          ; [A_L1] |576| 
||         STD     .D1     A10,*SP(32)       ; [A_D1] 
||         MV      .D2     A8,D0             ; [A_D2] |559| 
||         CMPEQW  .S1     A11,0x1,A1        ; [A_S1] |617| 
||         MV      .L2X    A7,B1             ; [B_L2] |559| 
	.dwcfi	save_reg_to_mem, 10, 32

   [ A0]   B       .B1     ||$C$L74||        ; [A_B] |576| 
||         STW     .D1X    B0,*SP(20)        ; [A_D1] |561| 
||         MVC     .S1     RP,A12            ; [A_S1] 
||         MV      .L1     A5,A10            ; [A_L1] |559| 
||         STD     .D2X    A12,*SP(24)       ; [A_D2] 
||         MV      .M1     A6,A7             ; [A_M1] |559| 

	.dwcfi	save_reg_to_reg, 4101, 12
	.dwcfi	save_reg_to_mem, 12, 24
	.dwpsn	file "tidl_custom_maxpooling.c",line 576,column 3,is_stmt,isa 0
           ; BRANCHCC OCCURS {||$C$L74||}    ; [] |576| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 7
	.dwpsn	file "tidl_custom_maxpooling.c",line 624,column 5,is_stmt,isa 0
   [ A1]   LDUW    .D1     *A10(232),B0      ; [A_D1] |624| 

   [ A1]   B       .B1     ||$C$L76||        ; [A_B] |643| 
|| [ A1]   STW     .D1X    B0,*A10(1704)     ; [A_D1] |624| 
||         CMPEQW  .L1     A11,0x2,A0        ; [A_L1] |628| 
||         MVKU32  .S1     0,A9              ; [A_S1] |643| 

	.dwpsn	file "tidl_custom_maxpooling.c",line 643,column 3,is_stmt,isa 0
           ; BRANCHCC OCCURS {||$C$L76||}    ; [] |643| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 1
	.dwpsn	file "tidl_custom_maxpooling.c",line 628,column 8,is_stmt,isa 0

   [!A0]   B       .B1     ||$C$L73||        ; [A_B] |628| 
||         MV      .L1X    B1,A8             ; [A_L1] |632| 
||         MV      .D1     D0,A6             ; [A_D1] |632| 

           ; BRANCHCC OCCURS {||$C$L73||}    ; [] |628| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 2
	.dwpsn	file "tidl_custom_maxpooling.c",line 632,column 5,is_stmt,isa 0
$C$DW$266	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$266, DW_AT_low_pc(0x00)
	.dwattr $C$DW$266, DW_AT_name("_Z28TIDL_customMaxPoolDspProcessPvP13sTIDL_Layer_tP20TIDL_CustomParams0_tPS_S4_")
	.dwattr $C$DW$266, DW_AT_TI_call

           CALL    .B1     ||_Z28TIDL_customMaxPoolDspProcessPvP13sTIDL_Layer_tP20TIDL_CustomParams0_tPS_S4_|| ; [A_B] |632| 
$C$RL7:    ; CALL OCCURS (||_Z28TIDL_customMaxPoolDspProcessPvP13sTIDL_Layer_tP20TIDL_CustomParams0_tPS_S4_||) arg:{A4,A6,A7,A8} ret:{}  ; [] |632| 
	.dwpsn	file "tidl_custom_maxpooling.c",line 643,column 3,is_stmt,isa 0
           B       .B1     ||$C$L76||        ; [A_B] |643| 
           ; BRANCH OCCURS {||$C$L76||}      ; [] |643| 
;** --------------------------------------------------------------------------*
||$C$L73||:    
;          EXCLUSIVE CPU CYCLES: 1

           B       .B1     ||$C$L76||        ; [A_B] |643| 
||         MVK32   .L1     0xffffffff,A9     ; [A_L1] |643| 

           ; BRANCH OCCURS {||$C$L76||}      ; [] |643| 
;** --------------------------------------------------------------------------*
||$C$L74||:    
;          EXCLUSIVE CPU CYCLES: 9
	.dwpsn	file "tidl_custom_maxpooling.c",line 585,column 5,is_stmt,isa 0
           LDW     .D1     *A10(1676),B0     ; [A_D1] |585| 

           CMPEQW  .L1X    B0,0x6,A0         ; [A_L1] |585| 
||         ADDD    .D1     SP,0x14,A9        ; [A_D1] |588| 

   [ A0]   B       .B1     ||$C$L75||        ; [A_B] |585| 
||         MV      .L1X    B1,A7             ; [A_L1] |588| 
||         MV      .D1     D0,A5             ; [A_D1] |588| 
||         MV      .S1     A10,A4            ; [A_S1] |588| 
||         ADDD    .D2     SP,0x10,A8        ; [A_D2] |588| 

           ; BRANCHCC OCCURS {||$C$L75||}    ; [] |585| 
;** --------------------------------------------------------------------------*
;          EXCLUSIVE CPU CYCLES: 1
	.dwpsn	file "tidl_custom_maxpooling.c",line 588,column 7,is_stmt,isa 0
$C$DW$267	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$267, DW_AT_low_pc(0x00)
	.dwattr $C$DW$267, DW_AT_name("_Z31TIDL_refCustomMaxPoolingProcessP13sTIDL_Layer_tP20TIDL_CustomParams0_tPPvS4_PfS5_")
	.dwattr $C$DW$267, DW_AT_TI_call

           CALL    .B1     ||_Z31TIDL_refCustomMaxPoolingProcessP13sTIDL_Layer_tP20TIDL_CustomParams0_tPPvS4_PfS5_|| ; [A_B] |588| 
$C$RL8:    ; CALL OCCURS (||_Z31TIDL_refCustomMaxPoolingProcessP13sTIDL_Layer_tP20TIDL_CustomParams0_tPPvS4_PfS5_||) arg:{A4,A5,A6,A7,A8,A9} ret:{}  ; [] |588| 
;** --------------------------------------------------------------------------*
||$C$L75||:    
;          EXCLUSIVE CPU CYCLES: 8
	.dwpsn	file "tidl_custom_maxpooling.c",line 609,column 5,is_stmt,isa 0
           CMPEQW  .L1     A11,0,A0          ; [A_L1] |609| 
	.dwpsn	file "tidl_custom_maxpooling.c",line 611,column 7,is_stmt,isa 0
   [ A0]   LDUW    .D1     *A10(232),B0      ; [A_D1] |611| 

   [ A0]   STW     .D1X    B0,*A10(1704)     ; [A_D1] |611| 
||         MVKU32  .L1     0,A9              ; [A_L1] |643| 

;** --------------------------------------------------------------------------*
||$C$L76||:    
;          EXCLUSIVE CPU CYCLES: 8
	.dwpsn	file "tidl_custom_maxpooling.c",line 643,column 3,is_stmt,isa 0

           MVC     .S1     A12,RP            ; [A_S1] |643| 
||         MV      .D1     A9,A4             ; [A_D1] |643| 
	.dwcfi	restore_reg, 4101

           LDD     .D1     *SP(48),A8        ; [A_D1] |643| 
||         LDD     .D2     *SP(32),A10       ; [A_D2] |643| 
	.dwcfi	restore_reg, 8
	.dwcfi	restore_reg, 10

           LDD     .D1     *SP(40),A9        ; [A_D1] |643| 
||         LDD     .D2     *SP(24),A12       ; [A_D2] |643| 

	.dwcfi	restore_reg, 9
	.dwcfi	restore_reg, 12
$C$DW$268	.dwtag  DW_TAG_TI_branch
	.dwattr $C$DW$268, DW_AT_low_pc(0x00)
	.dwattr $C$DW$268, DW_AT_TI_return


           RET     .B1     ; [A_B] |643| 
||         ADDD    .D1     SP,0x28,SP        ; [A_D1] |643| 

	.dwcfi	cfa_offset, 0
           ; RETURN OCCURS {RP}              ; [] |643| 
	.dwattr $C$DW$249, DW_AT_TI_end_file("tidl_custom_maxpooling.c")
	.dwattr $C$DW$249, DW_AT_TI_end_line(0x284)
	.dwattr $C$DW$249, DW_AT_TI_end_column(0x01)
	.dwendentry
	.dwendtag $C$DW$249

;*****************************************************************************
;* UNDEFINED EXTERNAL REFERENCES                                             *
;*****************************************************************************
	.global	||_Z40TIDL_customMaxPool_ixX_oxX_getHandleSizePv||
	.global	||_Z31TIDL_customMaxPool_ixX_oxX_initPvPK18TIDL_bufParams3D_tS2_S_||
	.global	||_Z31TIDL_customMaxPool_ixX_oxX_execPvPK18TIDL_bufParams3D_tS2_PPKvS_||
	.global	||_Z17TIDL_DataflowInitPvP23TIDL_DataflowInitParams||
	.global	||_Z20TIDL_DataflowProcessPvPS_S0_||
	.global	||__c7xabi_strasg||
;*****************************************************************************
;* SECTION GROUPS                                                            *
;*****************************************************************************
	.group    "_Z28TIDL_refCustomMaxPoolingCoreIaEvPT_iiiiiiiiiiiiiS0_S1_P13sTIDL_Layer_t", 1
	.gmember  ".text:_Z28TIDL_refCustomMaxPoolingCoreIaEvPT_iiiiiiiiiiiiiS0_S1_P13sTIDL_Layer_t"
	.endgroup
	.group    "_Z28TIDL_refCustomMaxPoolingCoreIhEvPT_iiiiiiiiiiiiiS0_S1_P13sTIDL_Layer_t", 1
	.gmember  ".text:_Z28TIDL_refCustomMaxPoolingCoreIhEvPT_iiiiiiiiiiiiiS0_S1_P13sTIDL_Layer_t"
	.endgroup
	.group    "_Z28TIDL_refCustomMaxPoolingCoreIsEvPT_iiiiiiiiiiiiiS0_S1_P13sTIDL_Layer_t", 1
	.gmember  ".text:_Z28TIDL_refCustomMaxPoolingCoreIsEvPT_iiiiiiiiiiiiiS0_S1_P13sTIDL_Layer_t"
	.endgroup
	.group    "_Z28TIDL_refCustomMaxPoolingCoreItEvPT_iiiiiiiiiiiiiS0_S1_P13sTIDL_Layer_t", 1
	.gmember  ".text:_Z28TIDL_refCustomMaxPoolingCoreItEvPT_iiiiiiiiiiiiiS0_S1_P13sTIDL_Layer_t"
	.endgroup

	.dwattr $C$DW$CU, DW_AT_language(DW_LANG_C_plus_plus)

;***************************************************************
;* DWARF CIE ENTRIES                                           *
;***************************************************************

$C$DW$CIE	.dwcie 4101
	.dwcfi	cfa_register, 111
	.dwcfi	cfa_offset, 0
	.dwcfi	same_value, 8
	.dwcfi	same_value, 9
	.dwcfi	same_value, 10
	.dwcfi	same_value, 11
	.dwcfi	same_value, 12
	.dwcfi	same_value, 13
	.dwcfi	same_value, 14
	.dwcfi	same_value, 15
	.dwcfi	same_value, 62
	.dwcfi	same_value, 63
	.dwendentry
	.dwendtag $C$DW$CU


;******************************************************************************
;* TYPE INFORMATION                                                           *
;******************************************************************************

$C$DW$TU$33	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$33

$C$DW$T$33	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$33, DW_AT_name("TIDL_CustomMaxPoolIxXOxXInitArgs")
	.dwattr $C$DW$T$33, DW_AT_byte_size(0x2c)
$C$DW$269	.dwtag  DW_TAG_member
	.dwattr $C$DW$269, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$269, DW_AT_name("funcStyle")
	.dwattr $C$DW$269, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$269, DW_AT_decl_file("tidsp/tidl_custom_maxpool_ixX_oxX.h")
	.dwattr $C$DW$269, DW_AT_decl_line(0x33)
	.dwattr $C$DW$269, DW_AT_decl_column(0x0c)

$C$DW$270	.dwtag  DW_TAG_member
	.dwattr $C$DW$270, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$270, DW_AT_name("customMaxPoolParam")
	.dwattr $C$DW$270, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$270, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$270, DW_AT_decl_file("tidsp/tidl_custom_maxpool_ixX_oxX.h")
	.dwattr $C$DW$270, DW_AT_decl_line(0x34)
	.dwattr $C$DW$270, DW_AT_decl_column(0x18)


$C$DW$271	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$271, DW_AT_name("operator =")
	.dwattr $C$DW$271, DW_AT_declaration
	.dwattr $C$DW$271, DW_AT_linkage_name("_ZN32TIDL_CustomMaxPoolIxXOxXInitArgsaSERKS_")
	.dwattr $C$DW$271, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$271, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$272	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$272, DW_AT_type(*$C$DW$T$30)

	.dwendtag $C$DW$271


$C$DW$273	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$273, DW_AT_name("operator =")
	.dwattr $C$DW$273, DW_AT_declaration
	.dwattr $C$DW$273, DW_AT_linkage_name("_ZN32TIDL_CustomMaxPoolIxXOxXInitArgsaSEOS_")
	.dwattr $C$DW$273, DW_AT_type(*$C$DW$T$28)
	.dwattr $C$DW$273, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$274	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$274, DW_AT_type(*$C$DW$T$28)

	.dwendtag $C$DW$273

	.dwattr $C$DW$T$33, DW_AT_decl_file("tidsp/tidl_custom_maxpool_ixX_oxX.h")
	.dwattr $C$DW$T$33, DW_AT_decl_line(0x32)
	.dwattr $C$DW$T$33, DW_AT_decl_column(0x01)
	.dwendtag $C$DW$T$33

	.dwendtag $C$DW$TU$33


$C$DW$TU$29	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$29
$C$DW$T$29	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$29, DW_AT_type(*$C$DW$T$33)

	.dwendtag $C$DW$TU$29


$C$DW$TU$30	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$30
$C$DW$T$30	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$30, DW_AT_type(*$C$DW$T$29)
	.dwattr $C$DW$T$30, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$30


$C$DW$TU$334	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$334
$C$DW$T$334	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$334, DW_AT_name("TIDL_CustomMaxPoolIxXOxXInitArgs")
	.dwattr $C$DW$T$334, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$T$334, DW_AT_decl_file("tidsp/tidl_custom_maxpool_ixX_oxX.h")
	.dwattr $C$DW$T$334, DW_AT_decl_line(0x35)
	.dwattr $C$DW$T$334, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$334


$C$DW$TU$28	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$28
$C$DW$T$28	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$28, DW_AT_type(*$C$DW$T$33)
	.dwattr $C$DW$T$28, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$28


$C$DW$TU$31	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$31

$C$DW$T$31	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$31, DW_AT_type(*$C$DW$T$28)
$C$DW$275	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$275, DW_AT_type(*$C$DW$T$30)

	.dwendtag $C$DW$T$31

	.dwendtag $C$DW$TU$31


$C$DW$TU$32	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$32

$C$DW$T$32	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$32, DW_AT_type(*$C$DW$T$28)
$C$DW$276	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$276, DW_AT_type(*$C$DW$T$28)

	.dwendtag $C$DW$T$32

	.dwendtag $C$DW$TU$32


$C$DW$TU$43	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$43

$C$DW$T$43	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$43, DW_AT_name("TIDL_CustomParams0_t")
	.dwattr $C$DW$T$43, DW_AT_byte_size(0x28)
$C$DW$277	.dwtag  DW_TAG_member
	.dwattr $C$DW$277, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$277, DW_AT_name("poolingType")
	.dwattr $C$DW$277, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$277, DW_AT_decl_file("tidl_custom.h")
	.dwattr $C$DW$277, DW_AT_decl_line(0x70)
	.dwattr $C$DW$277, DW_AT_decl_column(0x0d)

$C$DW$278	.dwtag  DW_TAG_member
	.dwattr $C$DW$278, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$278, DW_AT_name("kernelW")
	.dwattr $C$DW$278, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$278, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$278, DW_AT_decl_file("tidl_custom.h")
	.dwattr $C$DW$278, DW_AT_decl_line(0x72)
	.dwattr $C$DW$278, DW_AT_decl_column(0x0d)

$C$DW$279	.dwtag  DW_TAG_member
	.dwattr $C$DW$279, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$279, DW_AT_name("kernelH")
	.dwattr $C$DW$279, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$279, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$279, DW_AT_decl_file("tidl_custom.h")
	.dwattr $C$DW$279, DW_AT_decl_line(0x74)
	.dwattr $C$DW$279, DW_AT_decl_column(0x0d)

$C$DW$280	.dwtag  DW_TAG_member
	.dwattr $C$DW$280, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$280, DW_AT_name("strideW")
	.dwattr $C$DW$280, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$280, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$280, DW_AT_decl_file("tidl_custom.h")
	.dwattr $C$DW$280, DW_AT_decl_line(0x76)
	.dwattr $C$DW$280, DW_AT_decl_column(0x0d)

$C$DW$281	.dwtag  DW_TAG_member
	.dwattr $C$DW$281, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$281, DW_AT_name("strideH")
	.dwattr $C$DW$281, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$281, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$281, DW_AT_decl_file("tidl_custom.h")
	.dwattr $C$DW$281, DW_AT_decl_line(0x78)
	.dwattr $C$DW$281, DW_AT_decl_column(0x0d)

$C$DW$282	.dwtag  DW_TAG_member
	.dwattr $C$DW$282, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$282, DW_AT_name("padW")
	.dwattr $C$DW$282, DW_AT_data_member_location[DW_OP_plus_uconst 0x14]
	.dwattr $C$DW$282, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$282, DW_AT_decl_file("tidl_custom.h")
	.dwattr $C$DW$282, DW_AT_decl_line(0x7a)
	.dwattr $C$DW$282, DW_AT_decl_column(0x0d)

$C$DW$283	.dwtag  DW_TAG_member
	.dwattr $C$DW$283, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$283, DW_AT_name("padH")
	.dwattr $C$DW$283, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$283, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$283, DW_AT_decl_file("tidl_custom.h")
	.dwattr $C$DW$283, DW_AT_decl_line(0x7c)
	.dwattr $C$DW$283, DW_AT_decl_column(0x0d)

$C$DW$284	.dwtag  DW_TAG_member
	.dwattr $C$DW$284, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$284, DW_AT_name("inDataQ")
	.dwattr $C$DW$284, DW_AT_data_member_location[DW_OP_plus_uconst 0x1c]
	.dwattr $C$DW$284, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$284, DW_AT_decl_file("tidl_custom.h")
	.dwattr $C$DW$284, DW_AT_decl_line(0x7e)
	.dwattr $C$DW$284, DW_AT_decl_column(0x0d)

$C$DW$285	.dwtag  DW_TAG_member
	.dwattr $C$DW$285, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$285, DW_AT_name("outDataQ")
	.dwattr $C$DW$285, DW_AT_data_member_location[DW_OP_plus_uconst 0x20]
	.dwattr $C$DW$285, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$285, DW_AT_decl_file("tidl_custom.h")
	.dwattr $C$DW$285, DW_AT_decl_line(0x80)
	.dwattr $C$DW$285, DW_AT_decl_column(0x0d)

$C$DW$286	.dwtag  DW_TAG_member
	.dwattr $C$DW$286, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$286, DW_AT_name("useCeil")
	.dwattr $C$DW$286, DW_AT_data_member_location[DW_OP_plus_uconst 0x24]
	.dwattr $C$DW$286, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$286, DW_AT_decl_file("tidl_custom.h")
	.dwattr $C$DW$286, DW_AT_decl_line(0x82)
	.dwattr $C$DW$286, DW_AT_decl_column(0x0d)


$C$DW$287	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$287, DW_AT_name("operator =")
	.dwattr $C$DW$287, DW_AT_declaration
	.dwattr $C$DW$287, DW_AT_linkage_name("_ZN20TIDL_CustomParams0_taSERKS_")
	.dwattr $C$DW$287, DW_AT_type(*$C$DW$T$38)
	.dwattr $C$DW$287, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$288	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$288, DW_AT_type(*$C$DW$T$40)

	.dwendtag $C$DW$287


$C$DW$289	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$289, DW_AT_name("operator =")
	.dwattr $C$DW$289, DW_AT_declaration
	.dwattr $C$DW$289, DW_AT_linkage_name("_ZN20TIDL_CustomParams0_taSEOS_")
	.dwattr $C$DW$289, DW_AT_type(*$C$DW$T$38)
	.dwattr $C$DW$289, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$290	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$290, DW_AT_type(*$C$DW$T$38)

	.dwendtag $C$DW$289

	.dwattr $C$DW$T$43, DW_AT_decl_file("tidl_custom.h")
	.dwattr $C$DW$T$43, DW_AT_decl_line(0x6e)
	.dwattr $C$DW$T$43, DW_AT_decl_column(0x10)
	.dwendtag $C$DW$T$43

	.dwendtag $C$DW$TU$43


$C$DW$TU$27	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$27
$C$DW$T$27	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$27, DW_AT_name("TIDL_CustomParams0_t")
	.dwattr $C$DW$T$27, DW_AT_type(*$C$DW$T$43)
	.dwattr $C$DW$T$27, DW_AT_decl_file("tidl_custom.h")
	.dwattr $C$DW$T$27, DW_AT_decl_line(0x83)
	.dwattr $C$DW$T$27, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$27


$C$DW$TU$337	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$337
$C$DW$T$337	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$337, DW_AT_type(*$C$DW$T$27)
	.dwattr $C$DW$T$337, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$337


$C$DW$TU$39	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$39
$C$DW$T$39	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$39, DW_AT_type(*$C$DW$T$43)

	.dwendtag $C$DW$TU$39


$C$DW$TU$40	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$40
$C$DW$T$40	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$40, DW_AT_type(*$C$DW$T$39)
	.dwattr $C$DW$T$40, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$40


$C$DW$TU$38	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$38
$C$DW$T$38	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$38, DW_AT_type(*$C$DW$T$43)
	.dwattr $C$DW$T$38, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$38


$C$DW$TU$41	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$41

$C$DW$T$41	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$41, DW_AT_type(*$C$DW$T$38)
$C$DW$291	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$291, DW_AT_type(*$C$DW$T$40)

	.dwendtag $C$DW$T$41

	.dwendtag $C$DW$TU$41


$C$DW$TU$42	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$42

$C$DW$T$42	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$42, DW_AT_type(*$C$DW$T$38)
$C$DW$292	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$292, DW_AT_type(*$C$DW$T$38)

	.dwendtag $C$DW$T$42

	.dwendtag $C$DW$TU$42


$C$DW$TU$65	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$65

$C$DW$T$65	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$65, DW_AT_name("TIDL_DataflowInitParams")
	.dwattr $C$DW$T$65, DW_AT_byte_size(0x28)
$C$DW$293	.dwtag  DW_TAG_member
	.dwattr $C$DW$293, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$293, DW_AT_name("dataFlowType")
	.dwattr $C$DW$293, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$293, DW_AT_decl_file("../inc/tidl_dataflow.h")
	.dwattr $C$DW$293, DW_AT_decl_line(0x135)
	.dwattr $C$DW$293, DW_AT_decl_column(0x0b)

$C$DW$294	.dwtag  DW_TAG_member
	.dwattr $C$DW$294, DW_AT_type(*$C$DW$T$47)
	.dwattr $C$DW$294, DW_AT_name("getHandleSize")
	.dwattr $C$DW$294, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$294, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$294, DW_AT_decl_file("../inc/tidl_dataflow.h")
	.dwattr $C$DW$294, DW_AT_decl_line(0x138)
	.dwattr $C$DW$294, DW_AT_decl_column(0x24)

$C$DW$295	.dwtag  DW_TAG_member
	.dwattr $C$DW$295, DW_AT_type(*$C$DW$T$53)
	.dwattr $C$DW$295, DW_AT_name("initFuncPtr")
	.dwattr $C$DW$295, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$295, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$295, DW_AT_decl_file("../inc/tidl_dataflow.h")
	.dwattr $C$DW$295, DW_AT_decl_line(0x142)
	.dwattr $C$DW$295, DW_AT_decl_column(0x22)

$C$DW$296	.dwtag  DW_TAG_member
	.dwattr $C$DW$296, DW_AT_type(*$C$DW$T$59)
	.dwattr $C$DW$296, DW_AT_name("execFuncPtr")
	.dwattr $C$DW$296, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$296, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$296, DW_AT_decl_file("../inc/tidl_dataflow.h")
	.dwattr $C$DW$296, DW_AT_decl_line(0x147)
	.dwattr $C$DW$296, DW_AT_decl_column(0x22)

$C$DW$297	.dwtag  DW_TAG_member
	.dwattr $C$DW$297, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$297, DW_AT_name("kernelInitArgs")
	.dwattr $C$DW$297, DW_AT_data_member_location[DW_OP_plus_uconst 0x20]
	.dwattr $C$DW$297, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$297, DW_AT_decl_file("../inc/tidl_dataflow.h")
	.dwattr $C$DW$297, DW_AT_decl_line(0x14c)
	.dwattr $C$DW$297, DW_AT_decl_column(0x0a)


$C$DW$298	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$298, DW_AT_name("operator =")
	.dwattr $C$DW$298, DW_AT_declaration
	.dwattr $C$DW$298, DW_AT_linkage_name("_ZN23TIDL_DataflowInitParamsaSERKS_")
	.dwattr $C$DW$298, DW_AT_type(*$C$DW$T$60)
	.dwattr $C$DW$298, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$299	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$299, DW_AT_type(*$C$DW$T$62)

	.dwendtag $C$DW$298


$C$DW$300	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$300, DW_AT_name("operator =")
	.dwattr $C$DW$300, DW_AT_declaration
	.dwattr $C$DW$300, DW_AT_linkage_name("_ZN23TIDL_DataflowInitParamsaSEOS_")
	.dwattr $C$DW$300, DW_AT_type(*$C$DW$T$60)
	.dwattr $C$DW$300, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$301	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$301, DW_AT_type(*$C$DW$T$60)

	.dwendtag $C$DW$300

	.dwattr $C$DW$T$65, DW_AT_decl_file("../inc/tidl_dataflow.h")
	.dwattr $C$DW$T$65, DW_AT_decl_line(0x133)
	.dwattr $C$DW$T$65, DW_AT_decl_column(0x01)
	.dwendtag $C$DW$T$65

	.dwendtag $C$DW$TU$65


$C$DW$TU$61	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$61
$C$DW$T$61	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$61, DW_AT_type(*$C$DW$T$65)

	.dwendtag $C$DW$TU$61


$C$DW$TU$62	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$62
$C$DW$T$62	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$62, DW_AT_type(*$C$DW$T$61)
	.dwattr $C$DW$T$62, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$62


$C$DW$TU$342	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$342
$C$DW$T$342	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$342, DW_AT_name("TIDL_DataflowInitParams")
	.dwattr $C$DW$T$342, DW_AT_type(*$C$DW$T$65)
	.dwattr $C$DW$T$342, DW_AT_decl_file("../inc/tidl_dataflow.h")
	.dwattr $C$DW$T$342, DW_AT_decl_line(0x14d)
	.dwattr $C$DW$T$342, DW_AT_decl_column(0x04)

	.dwendtag $C$DW$TU$342


$C$DW$TU$343	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$343
$C$DW$T$343	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$343, DW_AT_type(*$C$DW$T$342)
	.dwattr $C$DW$T$343, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$343


$C$DW$TU$60	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$60
$C$DW$T$60	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$60, DW_AT_type(*$C$DW$T$65)
	.dwattr $C$DW$T$60, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$60


$C$DW$TU$63	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$63

$C$DW$T$63	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$63, DW_AT_type(*$C$DW$T$60)
$C$DW$302	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$302, DW_AT_type(*$C$DW$T$62)

	.dwendtag $C$DW$T$63

	.dwendtag $C$DW$TU$63


$C$DW$TU$64	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$64

$C$DW$T$64	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$64, DW_AT_type(*$C$DW$T$60)
$C$DW$303	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$303, DW_AT_type(*$C$DW$T$60)

	.dwendtag $C$DW$T$64

	.dwendtag $C$DW$TU$64


$C$DW$TU$86	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$86

$C$DW$T$86	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$86, DW_AT_name("TIDL_bufParams3D_t")
	.dwattr $C$DW$T$86, DW_AT_byte_size(0x18)
$C$DW$304	.dwtag  DW_TAG_member
	.dwattr $C$DW$304, DW_AT_type(*$C$DW$T$80)
	.dwattr $C$DW$304, DW_AT_name("data_type")
	.dwattr $C$DW$304, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$304, DW_AT_decl_file("../inc/tidl_dataflow.h")
	.dwattr $C$DW$304, DW_AT_decl_line(0xc1)
	.dwattr $C$DW$304, DW_AT_decl_column(0x0c)

$C$DW$305	.dwtag  DW_TAG_member
	.dwattr $C$DW$305, DW_AT_type(*$C$DW$T$80)
	.dwattr $C$DW$305, DW_AT_name("dim_x")
	.dwattr $C$DW$305, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$305, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$305, DW_AT_decl_file("../inc/tidl_dataflow.h")
	.dwattr $C$DW$305, DW_AT_decl_line(0xc4)
	.dwattr $C$DW$305, DW_AT_decl_column(0x0c)

$C$DW$306	.dwtag  DW_TAG_member
	.dwattr $C$DW$306, DW_AT_type(*$C$DW$T$80)
	.dwattr $C$DW$306, DW_AT_name("dim_y")
	.dwattr $C$DW$306, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$306, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$306, DW_AT_decl_file("../inc/tidl_dataflow.h")
	.dwattr $C$DW$306, DW_AT_decl_line(0xc7)
	.dwattr $C$DW$306, DW_AT_decl_column(0x0c)

$C$DW$307	.dwtag  DW_TAG_member
	.dwattr $C$DW$307, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$307, DW_AT_name("stride_y")
	.dwattr $C$DW$307, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$307, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$307, DW_AT_decl_file("../inc/tidl_dataflow.h")
	.dwattr $C$DW$307, DW_AT_decl_line(0xc9)
	.dwattr $C$DW$307, DW_AT_decl_column(0x0c)

$C$DW$308	.dwtag  DW_TAG_member
	.dwattr $C$DW$308, DW_AT_type(*$C$DW$T$80)
	.dwattr $C$DW$308, DW_AT_name("dim_z")
	.dwattr $C$DW$308, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$308, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$308, DW_AT_decl_file("../inc/tidl_dataflow.h")
	.dwattr $C$DW$308, DW_AT_decl_line(0xcc)
	.dwattr $C$DW$308, DW_AT_decl_column(0x0c)

$C$DW$309	.dwtag  DW_TAG_member
	.dwattr $C$DW$309, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$309, DW_AT_name("stride_z")
	.dwattr $C$DW$309, DW_AT_data_member_location[DW_OP_plus_uconst 0x14]
	.dwattr $C$DW$309, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$309, DW_AT_decl_file("../inc/tidl_dataflow.h")
	.dwattr $C$DW$309, DW_AT_decl_line(0xce)
	.dwattr $C$DW$309, DW_AT_decl_column(0x0c)


$C$DW$310	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$310, DW_AT_name("operator =")
	.dwattr $C$DW$310, DW_AT_declaration
	.dwattr $C$DW$310, DW_AT_linkage_name("_ZN18TIDL_bufParams3D_taSERKS_")
	.dwattr $C$DW$310, DW_AT_type(*$C$DW$T$81)
	.dwattr $C$DW$310, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$311	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$311, DW_AT_type(*$C$DW$T$83)

	.dwendtag $C$DW$310


$C$DW$312	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$312, DW_AT_name("operator =")
	.dwattr $C$DW$312, DW_AT_declaration
	.dwattr $C$DW$312, DW_AT_linkage_name("_ZN18TIDL_bufParams3D_taSEOS_")
	.dwattr $C$DW$312, DW_AT_type(*$C$DW$T$81)
	.dwattr $C$DW$312, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$313	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$313, DW_AT_type(*$C$DW$T$81)

	.dwendtag $C$DW$312

	.dwattr $C$DW$T$86, DW_AT_decl_file("../inc/tidl_dataflow.h")
	.dwattr $C$DW$T$86, DW_AT_decl_line(0xbf)
	.dwattr $C$DW$T$86, DW_AT_decl_column(0x01)
	.dwendtag $C$DW$T$86

	.dwendtag $C$DW$TU$86


$C$DW$TU$48	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$48
$C$DW$T$48	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$48, DW_AT_name("TIDL_bufParams3D_t")
	.dwattr $C$DW$T$48, DW_AT_type(*$C$DW$T$86)
	.dwattr $C$DW$T$48, DW_AT_decl_file("../inc/tidl_dataflow.h")
	.dwattr $C$DW$T$48, DW_AT_decl_line(0xcf)
	.dwattr $C$DW$T$48, DW_AT_decl_column(0x03)

	.dwendtag $C$DW$TU$48


$C$DW$TU$49	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$49
$C$DW$T$49	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$49, DW_AT_type(*$C$DW$T$48)

	.dwendtag $C$DW$TU$49


$C$DW$TU$50	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$50
$C$DW$T$50	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$50, DW_AT_type(*$C$DW$T$49)
	.dwattr $C$DW$T$50, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$50


$C$DW$TU$82	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$82
$C$DW$T$82	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$82, DW_AT_type(*$C$DW$T$86)

	.dwendtag $C$DW$TU$82


$C$DW$TU$83	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$83
$C$DW$T$83	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$83, DW_AT_type(*$C$DW$T$82)
	.dwattr $C$DW$T$83, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$83


$C$DW$TU$81	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$81
$C$DW$T$81	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$81, DW_AT_type(*$C$DW$T$86)
	.dwattr $C$DW$T$81, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$81


$C$DW$TU$84	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$84

$C$DW$T$84	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$84, DW_AT_type(*$C$DW$T$81)
$C$DW$314	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$314, DW_AT_type(*$C$DW$T$83)

	.dwendtag $C$DW$T$84

	.dwendtag $C$DW$TU$84


$C$DW$TU$85	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$85

$C$DW$T$85	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$85, DW_AT_type(*$C$DW$T$81)
$C$DW$315	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$315, DW_AT_type(*$C$DW$T$81)

	.dwendtag $C$DW$T$85

	.dwendtag $C$DW$TU$85


$C$DW$TU$2	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$2
$C$DW$T$2	.dwtag  DW_TAG_unspecified_type
	.dwattr $C$DW$T$2, DW_AT_name("void")

	.dwendtag $C$DW$TU$2


$C$DW$TU$3	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$3
$C$DW$T$3	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$3, DW_AT_type(*$C$DW$T$2)
	.dwattr $C$DW$T$3, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$3


$C$DW$TU$347	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$347
$C$DW$T$347	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$347, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$T$347, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$347


$C$DW$TU$54	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$54
$C$DW$T$54	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$54, DW_AT_type(*$C$DW$T$2)

	.dwendtag $C$DW$TU$54


$C$DW$TU$55	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$55
$C$DW$T$55	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$55, DW_AT_type(*$C$DW$T$54)
	.dwattr $C$DW$T$55, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$55


$C$DW$TU$56	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$56
$C$DW$T$56	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$56, DW_AT_type(*$C$DW$T$55)
	.dwattr $C$DW$T$56, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$56


$C$DW$TU$4	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$4
$C$DW$T$4	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$4, DW_AT_encoding(DW_ATE_boolean)
	.dwattr $C$DW$T$4, DW_AT_name("bool")
	.dwattr $C$DW$T$4, DW_AT_byte_size(0x01)

	.dwendtag $C$DW$TU$4


$C$DW$TU$5	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$5
$C$DW$T$5	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$5, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$5, DW_AT_name("signed char")
	.dwattr $C$DW$T$5, DW_AT_byte_size(0x01)

	.dwendtag $C$DW$TU$5


$C$DW$TU$356	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$356
$C$DW$T$356	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$356, DW_AT_name("__int8_t")
	.dwattr $C$DW$T$356, DW_AT_type(*$C$DW$T$5)
	.dwattr $C$DW$T$356, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/machine/_types.h")
	.dwattr $C$DW$T$356, DW_AT_decl_line(0x60)
	.dwattr $C$DW$T$356, DW_AT_decl_column(0x16)

	.dwendtag $C$DW$TU$356


$C$DW$TU$357	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$357
$C$DW$T$357	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$357, DW_AT_name("int8_t")
	.dwattr $C$DW$T$357, DW_AT_type(*$C$DW$T$356)
	.dwattr $C$DW$T$357, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/sys/_stdint.h")
	.dwattr $C$DW$T$357, DW_AT_decl_line(0x25)
	.dwattr $C$DW$T$357, DW_AT_decl_column(0x13)

	.dwendtag $C$DW$TU$357


$C$DW$TU$358	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$358
$C$DW$T$358	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$358, DW_AT_type(*$C$DW$T$357)
	.dwattr $C$DW$T$358, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$358


$C$DW$TU$6	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$6
$C$DW$T$6	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$6, DW_AT_encoding(DW_ATE_unsigned_char)
	.dwattr $C$DW$T$6, DW_AT_name("unsigned char")
	.dwattr $C$DW$T$6, DW_AT_byte_size(0x01)

	.dwendtag $C$DW$TU$6


$C$DW$TU$350	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$350
$C$DW$T$350	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$350, DW_AT_name("__uint8_t")
	.dwattr $C$DW$T$350, DW_AT_type(*$C$DW$T$6)
	.dwattr $C$DW$T$350, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/machine/_types.h")
	.dwattr $C$DW$T$350, DW_AT_decl_line(0x61)
	.dwattr $C$DW$T$350, DW_AT_decl_column(0x18)

	.dwendtag $C$DW$TU$350


$C$DW$TU$351	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$351
$C$DW$T$351	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$351, DW_AT_name("uint8_t")
	.dwattr $C$DW$T$351, DW_AT_type(*$C$DW$T$350)
	.dwattr $C$DW$T$351, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/sys/_stdint.h")
	.dwattr $C$DW$T$351, DW_AT_decl_line(0x3b)
	.dwattr $C$DW$T$351, DW_AT_decl_column(0x14)

	.dwendtag $C$DW$TU$351


$C$DW$TU$352	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$352
$C$DW$T$352	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$352, DW_AT_type(*$C$DW$T$351)
	.dwattr $C$DW$T$352, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$352


$C$DW$TU$7	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$7
$C$DW$T$7	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$7, DW_AT_encoding(DW_ATE_signed_char)
	.dwattr $C$DW$T$7, DW_AT_name("wchar_t")
	.dwattr $C$DW$T$7, DW_AT_byte_size(0x04)

	.dwendtag $C$DW$TU$7


$C$DW$TU$8	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$8
$C$DW$T$8	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$8, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$8, DW_AT_name("short")
	.dwattr $C$DW$T$8, DW_AT_byte_size(0x02)

	.dwendtag $C$DW$TU$8


$C$DW$TU$364	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$364
$C$DW$T$364	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$364, DW_AT_name("__int16_t")
	.dwattr $C$DW$T$364, DW_AT_type(*$C$DW$T$8)
	.dwattr $C$DW$T$364, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/machine/_types.h")
	.dwattr $C$DW$T$364, DW_AT_decl_line(0x62)
	.dwattr $C$DW$T$364, DW_AT_decl_column(0x11)

	.dwendtag $C$DW$TU$364


$C$DW$TU$365	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$365
$C$DW$T$365	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$365, DW_AT_name("int16_t")
	.dwattr $C$DW$T$365, DW_AT_type(*$C$DW$T$364)
	.dwattr $C$DW$T$365, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/sys/_stdint.h")
	.dwattr $C$DW$T$365, DW_AT_decl_line(0x2b)
	.dwattr $C$DW$T$365, DW_AT_decl_column(0x14)

	.dwendtag $C$DW$TU$365


$C$DW$TU$366	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$366
$C$DW$T$366	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$366, DW_AT_type(*$C$DW$T$365)
	.dwattr $C$DW$T$366, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$366


$C$DW$TU$9	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$9
$C$DW$T$9	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$9, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$9, DW_AT_name("unsigned short")
	.dwattr $C$DW$T$9, DW_AT_byte_size(0x02)

	.dwendtag $C$DW$TU$9


$C$DW$TU$360	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$360
$C$DW$T$360	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$360, DW_AT_name("__uint16_t")
	.dwattr $C$DW$T$360, DW_AT_type(*$C$DW$T$9)
	.dwattr $C$DW$T$360, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/machine/_types.h")
	.dwattr $C$DW$T$360, DW_AT_decl_line(0x63)
	.dwattr $C$DW$T$360, DW_AT_decl_column(0x19)

	.dwendtag $C$DW$TU$360


$C$DW$TU$361	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$361
$C$DW$T$361	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$361, DW_AT_name("uint16_t")
	.dwattr $C$DW$T$361, DW_AT_type(*$C$DW$T$360)
	.dwattr $C$DW$T$361, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/sys/_stdint.h")
	.dwattr $C$DW$T$361, DW_AT_decl_line(0x41)
	.dwattr $C$DW$T$361, DW_AT_decl_column(0x15)

	.dwendtag $C$DW$TU$361


$C$DW$TU$362	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$362
$C$DW$T$362	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$362, DW_AT_type(*$C$DW$T$361)
	.dwattr $C$DW$T$362, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$362


$C$DW$TU$10	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$10
$C$DW$T$10	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$10, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$10, DW_AT_name("int")
	.dwattr $C$DW$T$10, DW_AT_byte_size(0x04)

	.dwendtag $C$DW$TU$10


$C$DW$TU$25	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$25
$C$DW$T$25	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$25, DW_AT_name("__int32_t")
	.dwattr $C$DW$T$25, DW_AT_type(*$C$DW$T$10)
	.dwattr $C$DW$T$25, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/machine/_types.h")
	.dwattr $C$DW$T$25, DW_AT_decl_line(0x64)
	.dwattr $C$DW$T$25, DW_AT_decl_column(0x0f)

	.dwendtag $C$DW$TU$25


$C$DW$TU$26	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$26
$C$DW$T$26	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$26, DW_AT_name("int32_t")
	.dwattr $C$DW$T$26, DW_AT_type(*$C$DW$T$25)
	.dwattr $C$DW$T$26, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/sys/_stdint.h")
	.dwattr $C$DW$T$26, DW_AT_decl_line(0x30)
	.dwattr $C$DW$T$26, DW_AT_decl_column(0x14)

	.dwendtag $C$DW$TU$26


$C$DW$TU$45	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$45

$C$DW$T$45	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$45, DW_AT_type(*$C$DW$T$26)
$C$DW$316	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$316, DW_AT_type(*$C$DW$T$3)

	.dwendtag $C$DW$T$45

	.dwendtag $C$DW$TU$45


$C$DW$TU$46	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$46
$C$DW$T$46	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$46, DW_AT_type(*$C$DW$T$45)
	.dwattr $C$DW$T$46, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$46


$C$DW$TU$47	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$47
$C$DW$T$47	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$47, DW_AT_name("TIDL_DataflowKernelGetHandleSize")
	.dwattr $C$DW$T$47, DW_AT_type(*$C$DW$T$46)
	.dwattr $C$DW$T$47, DW_AT_decl_file("../inc/tidl_dataflow.h")
	.dwattr $C$DW$T$47, DW_AT_decl_line(0xde)
	.dwattr $C$DW$T$47, DW_AT_decl_column(0x13)

	.dwendtag $C$DW$TU$47


$C$DW$TU$51	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$51

$C$DW$T$51	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$51, DW_AT_type(*$C$DW$T$26)
$C$DW$317	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$317, DW_AT_type(*$C$DW$T$3)

$C$DW$318	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$318, DW_AT_type(*$C$DW$T$50)

$C$DW$319	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$319, DW_AT_type(*$C$DW$T$50)

$C$DW$320	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$320, DW_AT_type(*$C$DW$T$3)

	.dwendtag $C$DW$T$51

	.dwendtag $C$DW$TU$51


$C$DW$TU$52	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$52
$C$DW$T$52	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$52, DW_AT_type(*$C$DW$T$51)
	.dwattr $C$DW$T$52, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$52


$C$DW$TU$53	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$53
$C$DW$T$53	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$53, DW_AT_name("TIDL_DataflowKernelInitFuncPtr")
	.dwattr $C$DW$T$53, DW_AT_type(*$C$DW$T$52)
	.dwattr $C$DW$T$53, DW_AT_decl_file("../inc/tidl_dataflow.h")
	.dwattr $C$DW$T$53, DW_AT_decl_line(0xf2)
	.dwattr $C$DW$T$53, DW_AT_decl_column(0x13)

	.dwendtag $C$DW$TU$53


$C$DW$TU$57	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$57

$C$DW$T$57	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$57, DW_AT_type(*$C$DW$T$26)
$C$DW$321	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$321, DW_AT_type(*$C$DW$T$3)

$C$DW$322	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$322, DW_AT_type(*$C$DW$T$50)

$C$DW$323	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$323, DW_AT_type(*$C$DW$T$50)

$C$DW$324	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$324, DW_AT_type(*$C$DW$T$56)

$C$DW$325	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$325, DW_AT_type(*$C$DW$T$3)

	.dwendtag $C$DW$T$57

	.dwendtag $C$DW$TU$57


$C$DW$TU$58	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$58
$C$DW$T$58	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$58, DW_AT_type(*$C$DW$T$57)
	.dwattr $C$DW$T$58, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$58


$C$DW$TU$59	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$59
$C$DW$T$59	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$59, DW_AT_name("TIDL_DataflowKernelExecFuncPtr")
	.dwattr $C$DW$T$59, DW_AT_type(*$C$DW$T$58)
	.dwattr $C$DW$T$59, DW_AT_decl_file("../inc/tidl_dataflow.h")
	.dwattr $C$DW$T$59, DW_AT_decl_line(0x107)
	.dwattr $C$DW$T$59, DW_AT_decl_column(0x13)

	.dwendtag $C$DW$TU$59


$C$DW$TU$157	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$157

$C$DW$T$157	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$157, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$T$157, DW_AT_byte_size(0x0c)
$C$DW$326	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$326, DW_AT_upper_bound(0x02)

	.dwendtag $C$DW$T$157

	.dwendtag $C$DW$TU$157


$C$DW$TU$158	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$158

$C$DW$T$158	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$158, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$T$158, DW_AT_byte_size(0x10)
$C$DW$327	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$327, DW_AT_upper_bound(0x03)

	.dwendtag $C$DW$T$158

	.dwendtag $C$DW$TU$158


$C$DW$TU$182	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$182

$C$DW$T$182	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$182, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$T$182, DW_AT_byte_size(0x40)
$C$DW$328	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$328, DW_AT_upper_bound(0x0f)

	.dwendtag $C$DW$T$182

	.dwendtag $C$DW$TU$182


$C$DW$TU$297	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$297

$C$DW$T$297	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$297, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$T$297, DW_AT_byte_size(0x44)
$C$DW$329	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$329, DW_AT_upper_bound(0x10)

	.dwendtag $C$DW$T$297

	.dwendtag $C$DW$TU$297


$C$DW$TU$11	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$11
$C$DW$T$11	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$11, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$11, DW_AT_name("unsigned int")
	.dwattr $C$DW$T$11, DW_AT_byte_size(0x04)

	.dwendtag $C$DW$TU$11


$C$DW$TU$79	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$79
$C$DW$T$79	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$79, DW_AT_name("__uint32_t")
	.dwattr $C$DW$T$79, DW_AT_type(*$C$DW$T$11)
	.dwattr $C$DW$T$79, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/machine/_types.h")
	.dwattr $C$DW$T$79, DW_AT_decl_line(0x65)
	.dwattr $C$DW$T$79, DW_AT_decl_column(0x17)

	.dwendtag $C$DW$TU$79


$C$DW$TU$80	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$80
$C$DW$T$80	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$80, DW_AT_name("uint32_t")
	.dwattr $C$DW$T$80, DW_AT_type(*$C$DW$T$79)
	.dwattr $C$DW$T$80, DW_AT_decl_file("/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/ti-cgt-c7000_1.4.2.LTS/include/sys/_stdint.h")
	.dwattr $C$DW$T$80, DW_AT_decl_line(0x46)
	.dwattr $C$DW$T$80, DW_AT_decl_column(0x15)

	.dwendtag $C$DW$TU$80


$C$DW$TU$12	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$12
$C$DW$T$12	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$12, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$12, DW_AT_name("__int40_t")
	.dwattr $C$DW$T$12, DW_AT_byte_size(0x08)

	.dwendtag $C$DW$TU$12


$C$DW$TU$13	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$13
$C$DW$T$13	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$13, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$13, DW_AT_name("unsigned __int40_t")
	.dwattr $C$DW$T$13, DW_AT_byte_size(0x08)

	.dwendtag $C$DW$TU$13


$C$DW$TU$14	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$14
$C$DW$T$14	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$14, DW_AT_encoding(DW_ATE_signed)
	.dwattr $C$DW$T$14, DW_AT_name("long long")
	.dwattr $C$DW$T$14, DW_AT_byte_size(0x08)

	.dwendtag $C$DW$TU$14


$C$DW$TU$15	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$15
$C$DW$T$15	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$15, DW_AT_encoding(DW_ATE_unsigned)
	.dwattr $C$DW$T$15, DW_AT_name("unsigned long long")
	.dwattr $C$DW$T$15, DW_AT_byte_size(0x08)

	.dwendtag $C$DW$TU$15


$C$DW$TU$481	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$481

$C$DW$T$481	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$481, DW_AT_type(*$C$DW$T$15)
	.dwattr $C$DW$T$481, DW_AT_byte_size(0x08)
$C$DW$330	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$330, DW_AT_upper_bound(0x00)

	.dwendtag $C$DW$T$481

	.dwendtag $C$DW$TU$481


$C$DW$TU$16	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$16
$C$DW$T$16	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$16, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$16, DW_AT_name("float")
	.dwattr $C$DW$T$16, DW_AT_byte_size(0x04)

	.dwendtag $C$DW$TU$16


$C$DW$TU$90	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$90
$C$DW$T$90	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$90, DW_AT_name("float32_tidl")
	.dwattr $C$DW$T$90, DW_AT_type(*$C$DW$T$16)
	.dwattr $C$DW$T$90, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$90, DW_AT_decl_line(0x71)
	.dwattr $C$DW$T$90, DW_AT_decl_column(0x0f)

	.dwendtag $C$DW$TU$90


$C$DW$TU$274	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$274

$C$DW$T$274	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$274, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$T$274, DW_AT_byte_size(0x10)
$C$DW$331	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$331, DW_AT_upper_bound(0x03)

	.dwendtag $C$DW$T$274

	.dwendtag $C$DW$TU$274


$C$DW$TU$17	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$17
$C$DW$T$17	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$17, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$17, DW_AT_name("double")
	.dwattr $C$DW$T$17, DW_AT_byte_size(0x08)

	.dwendtag $C$DW$TU$17


$C$DW$TU$18	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$18
$C$DW$T$18	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$18, DW_AT_encoding(DW_ATE_float)
	.dwattr $C$DW$T$18, DW_AT_name("long double")
	.dwattr $C$DW$T$18, DW_AT_byte_size(0x08)

	.dwendtag $C$DW$TU$18


$C$DW$TU$19	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$19
$C$DW$T$19	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$19, DW_AT_encoding(DW_ATE_TI_complex_signed)
	.dwattr $C$DW$T$19, DW_AT_name("cchar")
	.dwattr $C$DW$T$19, DW_AT_byte_size(0x02)

	.dwendtag $C$DW$TU$19


$C$DW$TU$20	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$20
$C$DW$T$20	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$20, DW_AT_encoding(DW_ATE_TI_complex_signed)
	.dwattr $C$DW$T$20, DW_AT_name("cshort")
	.dwattr $C$DW$T$20, DW_AT_byte_size(0x04)

	.dwendtag $C$DW$TU$20


$C$DW$TU$21	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$21
$C$DW$T$21	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$21, DW_AT_encoding(DW_ATE_TI_complex_signed)
	.dwattr $C$DW$T$21, DW_AT_name("cint")
	.dwattr $C$DW$T$21, DW_AT_byte_size(0x08)

	.dwendtag $C$DW$TU$21


$C$DW$TU$22	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$22
$C$DW$T$22	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$22, DW_AT_encoding(DW_ATE_TI_complex_signed)
	.dwattr $C$DW$T$22, DW_AT_name("clong")
	.dwattr $C$DW$T$22, DW_AT_byte_size(0x10)

	.dwendtag $C$DW$TU$22


$C$DW$TU$23	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$23
$C$DW$T$23	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$23, DW_AT_encoding(DW_ATE_TI_complex_float)
	.dwattr $C$DW$T$23, DW_AT_name("cfloat")
	.dwattr $C$DW$T$23, DW_AT_byte_size(0x08)

	.dwendtag $C$DW$TU$23


$C$DW$TU$24	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$24
$C$DW$T$24	.dwtag  DW_TAG_base_type
	.dwattr $C$DW$T$24, DW_AT_encoding(DW_ATE_TI_complex_float)
	.dwattr $C$DW$T$24, DW_AT_name("cdouble")
	.dwattr $C$DW$T$24, DW_AT_byte_size(0x10)

	.dwendtag $C$DW$TU$24


$C$DW$TU$482	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$482

$C$DW$T$482	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$482, DW_AT_name("__ti_vpred_tp")
	.dwattr $C$DW$T$482, DW_AT_byte_size(0x08)
$C$DW$332	.dwtag  DW_TAG_member
	.dwattr $C$DW$332, DW_AT_type(*$C$DW$T$481)
	.dwattr $C$DW$332, DW_AT_name("__vpred_field")
	.dwattr $C$DW$332, DW_AT_accessibility(DW_ACCESS_private)

	.dwendtag $C$DW$T$482

	.dwendtag $C$DW$TU$482


$C$DW$TU$96	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$96

$C$DW$T$96	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$96, DW_AT_name("sTIDL_ActParams_t")
	.dwattr $C$DW$T$96, DW_AT_byte_size(0x14)
$C$DW$333	.dwtag  DW_TAG_member
	.dwattr $C$DW$333, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$333, DW_AT_name("slope")
	.dwattr $C$DW$333, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$333, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$333, DW_AT_decl_line(0x33c)
	.dwattr $C$DW$333, DW_AT_decl_column(0x0d)

$C$DW$334	.dwtag  DW_TAG_member
	.dwattr $C$DW$334, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$334, DW_AT_name("slopeScale")
	.dwattr $C$DW$334, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$334, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$334, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$334, DW_AT_decl_line(0x33e)
	.dwattr $C$DW$334, DW_AT_decl_column(0x14)

$C$DW$335	.dwtag  DW_TAG_member
	.dwattr $C$DW$335, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$335, DW_AT_name("clipMin")
	.dwattr $C$DW$335, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$335, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$335, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$335, DW_AT_decl_line(0x340)
	.dwattr $C$DW$335, DW_AT_decl_column(0x14)

$C$DW$336	.dwtag  DW_TAG_member
	.dwattr $C$DW$336, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$336, DW_AT_name("clipMax")
	.dwattr $C$DW$336, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$336, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$336, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$336, DW_AT_decl_line(0x342)
	.dwattr $C$DW$336, DW_AT_decl_column(0x14)

$C$DW$337	.dwtag  DW_TAG_member
	.dwattr $C$DW$337, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$337, DW_AT_name("actType")
	.dwattr $C$DW$337, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$337, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$337, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$337, DW_AT_decl_line(0x344)
	.dwattr $C$DW$337, DW_AT_decl_column(0x0d)


$C$DW$338	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$338, DW_AT_name("operator =")
	.dwattr $C$DW$338, DW_AT_declaration
	.dwattr $C$DW$338, DW_AT_linkage_name("_ZN17sTIDL_ActParams_taSERKS_")
	.dwattr $C$DW$338, DW_AT_type(*$C$DW$T$91)
	.dwattr $C$DW$338, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$339	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$339, DW_AT_type(*$C$DW$T$93)

	.dwendtag $C$DW$338


$C$DW$340	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$340, DW_AT_name("operator =")
	.dwattr $C$DW$340, DW_AT_declaration
	.dwattr $C$DW$340, DW_AT_linkage_name("_ZN17sTIDL_ActParams_taSEOS_")
	.dwattr $C$DW$340, DW_AT_type(*$C$DW$T$91)
	.dwattr $C$DW$340, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$341	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$341, DW_AT_type(*$C$DW$T$91)

	.dwendtag $C$DW$340

	.dwattr $C$DW$T$96, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$96, DW_AT_decl_line(0x33a)
	.dwattr $C$DW$T$96, DW_AT_decl_column(0x10)
	.dwendtag $C$DW$T$96

	.dwendtag $C$DW$TU$96


$C$DW$TU$91	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$91
$C$DW$T$91	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$91, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$T$91, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$91


$C$DW$TU$94	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$94

$C$DW$T$94	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$94, DW_AT_type(*$C$DW$T$91)
$C$DW$342	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$342, DW_AT_type(*$C$DW$T$93)

	.dwendtag $C$DW$T$94

	.dwendtag $C$DW$TU$94


$C$DW$TU$95	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$95

$C$DW$T$95	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$95, DW_AT_type(*$C$DW$T$91)
$C$DW$343	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$343, DW_AT_type(*$C$DW$T$91)

	.dwendtag $C$DW$T$95

	.dwendtag $C$DW$TU$95


$C$DW$TU$92	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$92
$C$DW$T$92	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$92, DW_AT_type(*$C$DW$T$96)

	.dwendtag $C$DW$TU$92


$C$DW$TU$93	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$93
$C$DW$T$93	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$93, DW_AT_type(*$C$DW$T$92)
	.dwattr $C$DW$T$93, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$93


$C$DW$TU$246	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$246
$C$DW$T$246	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$246, DW_AT_name("sTIDL_ActParams_t")
	.dwattr $C$DW$T$246, DW_AT_type(*$C$DW$T$96)
	.dwattr $C$DW$T$246, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$246, DW_AT_decl_line(0x345)
	.dwattr $C$DW$T$246, DW_AT_decl_column(0x02)

	.dwendtag $C$DW$TU$246


$C$DW$TU$104	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$104

$C$DW$T$104	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$104, DW_AT_name("sTIDL_ArgMaxParams_t")
	.dwattr $C$DW$T$104, DW_AT_byte_size(0x0c)
$C$DW$344	.dwtag  DW_TAG_member
	.dwattr $C$DW$344, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$344, DW_AT_name("numChannels")
	.dwattr $C$DW$344, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$344, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$344, DW_AT_decl_line(0x379)
	.dwattr $C$DW$344, DW_AT_decl_column(0x0d)

$C$DW$345	.dwtag  DW_TAG_member
	.dwattr $C$DW$345, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$345, DW_AT_name("inDataQ")
	.dwattr $C$DW$345, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$345, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$345, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$345, DW_AT_decl_line(0x37b)
	.dwattr $C$DW$345, DW_AT_decl_column(0x0d)

$C$DW$346	.dwtag  DW_TAG_member
	.dwattr $C$DW$346, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$346, DW_AT_name("outDataQ")
	.dwattr $C$DW$346, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$346, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$346, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$346, DW_AT_decl_line(0x37d)
	.dwattr $C$DW$346, DW_AT_decl_column(0x0d)


$C$DW$347	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$347, DW_AT_name("operator =")
	.dwattr $C$DW$347, DW_AT_declaration
	.dwattr $C$DW$347, DW_AT_linkage_name("_ZN20sTIDL_ArgMaxParams_taSERKS_")
	.dwattr $C$DW$347, DW_AT_type(*$C$DW$T$99)
	.dwattr $C$DW$347, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$348	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$348, DW_AT_type(*$C$DW$T$101)

	.dwendtag $C$DW$347


$C$DW$349	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$349, DW_AT_name("operator =")
	.dwattr $C$DW$349, DW_AT_declaration
	.dwattr $C$DW$349, DW_AT_linkage_name("_ZN20sTIDL_ArgMaxParams_taSEOS_")
	.dwattr $C$DW$349, DW_AT_type(*$C$DW$T$99)
	.dwattr $C$DW$349, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$350	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$350, DW_AT_type(*$C$DW$T$99)

	.dwendtag $C$DW$349

	.dwattr $C$DW$T$104, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$104, DW_AT_decl_line(0x377)
	.dwattr $C$DW$T$104, DW_AT_decl_column(0x10)
	.dwendtag $C$DW$T$104

	.dwendtag $C$DW$TU$104


$C$DW$TU$99	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$99
$C$DW$T$99	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$99, DW_AT_type(*$C$DW$T$104)
	.dwattr $C$DW$T$99, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$99


$C$DW$TU$102	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$102

$C$DW$T$102	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$102, DW_AT_type(*$C$DW$T$99)
$C$DW$351	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$351, DW_AT_type(*$C$DW$T$101)

	.dwendtag $C$DW$T$102

	.dwendtag $C$DW$TU$102


$C$DW$TU$103	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$103

$C$DW$T$103	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$103, DW_AT_type(*$C$DW$T$99)
$C$DW$352	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$352, DW_AT_type(*$C$DW$T$99)

	.dwendtag $C$DW$T$103

	.dwendtag $C$DW$TU$103


$C$DW$TU$100	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$100
$C$DW$T$100	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$100, DW_AT_type(*$C$DW$T$104)

	.dwendtag $C$DW$TU$100


$C$DW$TU$101	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$101
$C$DW$T$101	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$101, DW_AT_type(*$C$DW$T$100)
	.dwattr $C$DW$T$101, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$101


$C$DW$TU$202	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$202
$C$DW$T$202	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$202, DW_AT_name("sTIDL_ArgMaxParams_t")
	.dwattr $C$DW$T$202, DW_AT_type(*$C$DW$T$104)
	.dwattr $C$DW$T$202, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$202, DW_AT_decl_line(0x37e)
	.dwattr $C$DW$T$202, DW_AT_decl_column(0x02)

	.dwendtag $C$DW$TU$202


$C$DW$TU$111	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$111

$C$DW$T$111	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$111, DW_AT_name("sTIDL_BatchNormParams_t")
	.dwattr $C$DW$T$111, DW_AT_byte_size(0x28)
$C$DW$353	.dwtag  DW_TAG_member
	.dwattr $C$DW$353, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$353, DW_AT_name("weights")
	.dwattr $C$DW$353, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$353, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$353, DW_AT_decl_line(0x4bf)
	.dwattr $C$DW$353, DW_AT_decl_column(0x0d)

$C$DW$354	.dwtag  DW_TAG_member
	.dwattr $C$DW$354, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$354, DW_AT_name("bias")
	.dwattr $C$DW$354, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$354, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$354, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$354, DW_AT_decl_line(0x4c1)
	.dwattr $C$DW$354, DW_AT_decl_column(0x0d)

$C$DW$355	.dwtag  DW_TAG_member
	.dwattr $C$DW$355, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$355, DW_AT_name("numChannels")
	.dwattr $C$DW$355, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$355, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$355, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$355, DW_AT_decl_line(0x4c3)
	.dwattr $C$DW$355, DW_AT_decl_column(0x0d)

$C$DW$356	.dwtag  DW_TAG_member
	.dwattr $C$DW$356, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$356, DW_AT_name("biasQ")
	.dwattr $C$DW$356, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$356, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$356, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$356, DW_AT_decl_line(0x4c5)
	.dwattr $C$DW$356, DW_AT_decl_column(0x0d)

$C$DW$357	.dwtag  DW_TAG_member
	.dwattr $C$DW$357, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$357, DW_AT_name("inDataQ")
	.dwattr $C$DW$357, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$357, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$357, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$357, DW_AT_decl_line(0x4c7)
	.dwattr $C$DW$357, DW_AT_decl_column(0x0d)

$C$DW$358	.dwtag  DW_TAG_member
	.dwattr $C$DW$358, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$358, DW_AT_name("outDataQ")
	.dwattr $C$DW$358, DW_AT_data_member_location[DW_OP_plus_uconst 0x14]
	.dwattr $C$DW$358, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$358, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$358, DW_AT_decl_line(0x4c9)
	.dwattr $C$DW$358, DW_AT_decl_column(0x0d)

$C$DW$359	.dwtag  DW_TAG_member
	.dwattr $C$DW$359, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$359, DW_AT_name("weightsQ")
	.dwattr $C$DW$359, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$359, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$359, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$359, DW_AT_decl_line(0x4cb)
	.dwattr $C$DW$359, DW_AT_decl_column(0x0d)

$C$DW$360	.dwtag  DW_TAG_member
	.dwattr $C$DW$360, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$360, DW_AT_name("weightScale")
	.dwattr $C$DW$360, DW_AT_data_member_location[DW_OP_plus_uconst 0x1c]
	.dwattr $C$DW$360, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$360, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$360, DW_AT_decl_line(0x4cd)
	.dwattr $C$DW$360, DW_AT_decl_column(0x10)

$C$DW$361	.dwtag  DW_TAG_member
	.dwattr $C$DW$361, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$361, DW_AT_name("biasScale")
	.dwattr $C$DW$361, DW_AT_data_member_location[DW_OP_plus_uconst 0x20]
	.dwattr $C$DW$361, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$361, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$361, DW_AT_decl_line(0x4cf)
	.dwattr $C$DW$361, DW_AT_decl_column(0x10)

$C$DW$362	.dwtag  DW_TAG_member
	.dwattr $C$DW$362, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$362, DW_AT_name("zeroWeightValue")
	.dwattr $C$DW$362, DW_AT_data_member_location[DW_OP_plus_uconst 0x24]
	.dwattr $C$DW$362, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$362, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$362, DW_AT_decl_line(0x4d1)
	.dwattr $C$DW$362, DW_AT_decl_column(0x0d)


$C$DW$363	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$363, DW_AT_name("operator =")
	.dwattr $C$DW$363, DW_AT_declaration
	.dwattr $C$DW$363, DW_AT_linkage_name("_ZN23sTIDL_BatchNormParams_taSERKS_")
	.dwattr $C$DW$363, DW_AT_type(*$C$DW$T$106)
	.dwattr $C$DW$363, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$364	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$364, DW_AT_type(*$C$DW$T$108)

	.dwendtag $C$DW$363


$C$DW$365	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$365, DW_AT_name("operator =")
	.dwattr $C$DW$365, DW_AT_declaration
	.dwattr $C$DW$365, DW_AT_linkage_name("_ZN23sTIDL_BatchNormParams_taSEOS_")
	.dwattr $C$DW$365, DW_AT_type(*$C$DW$T$106)
	.dwattr $C$DW$365, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$366	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$366, DW_AT_type(*$C$DW$T$106)

	.dwendtag $C$DW$365

	.dwattr $C$DW$T$111, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$111, DW_AT_decl_line(0x4bd)
	.dwattr $C$DW$T$111, DW_AT_decl_column(0x10)
	.dwendtag $C$DW$T$111

	.dwendtag $C$DW$TU$111


$C$DW$TU$106	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$106
$C$DW$T$106	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$106, DW_AT_type(*$C$DW$T$111)
	.dwattr $C$DW$T$106, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$106


$C$DW$TU$109	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$109

$C$DW$T$109	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$109, DW_AT_type(*$C$DW$T$106)
$C$DW$367	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$367, DW_AT_type(*$C$DW$T$108)

	.dwendtag $C$DW$T$109

	.dwendtag $C$DW$TU$109


$C$DW$TU$110	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$110

$C$DW$T$110	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$110, DW_AT_type(*$C$DW$T$106)
$C$DW$368	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$368, DW_AT_type(*$C$DW$T$106)

	.dwendtag $C$DW$T$110

	.dwendtag $C$DW$TU$110


$C$DW$TU$107	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$107
$C$DW$T$107	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$107, DW_AT_type(*$C$DW$T$111)

	.dwendtag $C$DW$TU$107


$C$DW$TU$108	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$108
$C$DW$T$108	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$108, DW_AT_type(*$C$DW$T$107)
	.dwattr $C$DW$T$108, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$108


$C$DW$TU$208	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$208
$C$DW$T$208	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$208, DW_AT_name("sTIDL_BatchNormParams_t")
	.dwattr $C$DW$T$208, DW_AT_type(*$C$DW$T$111)
	.dwattr $C$DW$T$208, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$208, DW_AT_decl_line(0x4d2)
	.dwattr $C$DW$T$208, DW_AT_decl_column(0x02)

	.dwendtag $C$DW$TU$208


$C$DW$TU$118	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$118

$C$DW$T$118	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$118, DW_AT_name("sTIDL_BiasParams_t")
	.dwattr $C$DW$T$118, DW_AT_byte_size(0x14)
$C$DW$369	.dwtag  DW_TAG_member
	.dwattr $C$DW$369, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$369, DW_AT_name("bias")
	.dwattr $C$DW$369, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$369, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$369, DW_AT_decl_line(0x4db)
	.dwattr $C$DW$369, DW_AT_decl_column(0x0d)

$C$DW$370	.dwtag  DW_TAG_member
	.dwattr $C$DW$370, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$370, DW_AT_name("numChannels")
	.dwattr $C$DW$370, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$370, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$370, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$370, DW_AT_decl_line(0x4dd)
	.dwattr $C$DW$370, DW_AT_decl_column(0x0d)

$C$DW$371	.dwtag  DW_TAG_member
	.dwattr $C$DW$371, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$371, DW_AT_name("biasQ")
	.dwattr $C$DW$371, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$371, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$371, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$371, DW_AT_decl_line(0x4df)
	.dwattr $C$DW$371, DW_AT_decl_column(0x0d)

$C$DW$372	.dwtag  DW_TAG_member
	.dwattr $C$DW$372, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$372, DW_AT_name("inDataQ")
	.dwattr $C$DW$372, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$372, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$372, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$372, DW_AT_decl_line(0x4e1)
	.dwattr $C$DW$372, DW_AT_decl_column(0x0d)

$C$DW$373	.dwtag  DW_TAG_member
	.dwattr $C$DW$373, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$373, DW_AT_name("outDataQ")
	.dwattr $C$DW$373, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$373, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$373, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$373, DW_AT_decl_line(0x4e3)
	.dwattr $C$DW$373, DW_AT_decl_column(0x0d)


$C$DW$374	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$374, DW_AT_name("operator =")
	.dwattr $C$DW$374, DW_AT_declaration
	.dwattr $C$DW$374, DW_AT_linkage_name("_ZN18sTIDL_BiasParams_taSERKS_")
	.dwattr $C$DW$374, DW_AT_type(*$C$DW$T$113)
	.dwattr $C$DW$374, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$375	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$375, DW_AT_type(*$C$DW$T$115)

	.dwendtag $C$DW$374


$C$DW$376	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$376, DW_AT_name("operator =")
	.dwattr $C$DW$376, DW_AT_declaration
	.dwattr $C$DW$376, DW_AT_linkage_name("_ZN18sTIDL_BiasParams_taSEOS_")
	.dwattr $C$DW$376, DW_AT_type(*$C$DW$T$113)
	.dwattr $C$DW$376, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$377	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$377, DW_AT_type(*$C$DW$T$113)

	.dwendtag $C$DW$376

	.dwattr $C$DW$T$118, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$118, DW_AT_decl_line(0x4d9)
	.dwattr $C$DW$T$118, DW_AT_decl_column(0x10)
	.dwendtag $C$DW$T$118

	.dwendtag $C$DW$TU$118


$C$DW$TU$113	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$113
$C$DW$T$113	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$113, DW_AT_type(*$C$DW$T$118)
	.dwattr $C$DW$T$113, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$113


$C$DW$TU$116	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$116

$C$DW$T$116	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$116, DW_AT_type(*$C$DW$T$113)
$C$DW$378	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$378, DW_AT_type(*$C$DW$T$115)

	.dwendtag $C$DW$T$116

	.dwendtag $C$DW$TU$116


$C$DW$TU$117	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$117

$C$DW$T$117	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$117, DW_AT_type(*$C$DW$T$113)
$C$DW$379	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$379, DW_AT_type(*$C$DW$T$113)

	.dwendtag $C$DW$T$117

	.dwendtag $C$DW$TU$117


$C$DW$TU$114	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$114
$C$DW$T$114	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$114, DW_AT_type(*$C$DW$T$118)

	.dwendtag $C$DW$TU$114


$C$DW$TU$115	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$115
$C$DW$T$115	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$115, DW_AT_type(*$C$DW$T$114)
	.dwattr $C$DW$T$115, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$115


$C$DW$TU$207	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$207
$C$DW$T$207	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$207, DW_AT_name("sTIDL_BiasParams_t")
	.dwattr $C$DW$T$207, DW_AT_type(*$C$DW$T$118)
	.dwattr $C$DW$T$207, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$207, DW_AT_decl_line(0x4e4)
	.dwattr $C$DW$T$207, DW_AT_decl_column(0x02)

	.dwendtag $C$DW$TU$207


$C$DW$TU$125	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$125

$C$DW$T$125	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$125, DW_AT_name("sTIDL_ConcatParams_t")
	.dwattr $C$DW$T$125, DW_AT_byte_size(0x08)
$C$DW$380	.dwtag  DW_TAG_member
	.dwattr $C$DW$380, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$380, DW_AT_name("axis")
	.dwattr $C$DW$380, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$380, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$380, DW_AT_decl_line(0x4b1)
	.dwattr $C$DW$380, DW_AT_decl_column(0x0c)

$C$DW$381	.dwtag  DW_TAG_member
	.dwattr $C$DW$381, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$381, DW_AT_name("outDataQ")
	.dwattr $C$DW$381, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$381, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$381, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$381, DW_AT_decl_line(0x4b3)
	.dwattr $C$DW$381, DW_AT_decl_column(0x0c)


$C$DW$382	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$382, DW_AT_name("operator =")
	.dwattr $C$DW$382, DW_AT_declaration
	.dwattr $C$DW$382, DW_AT_linkage_name("_ZN20sTIDL_ConcatParams_taSERKS_")
	.dwattr $C$DW$382, DW_AT_type(*$C$DW$T$120)
	.dwattr $C$DW$382, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$383	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$383, DW_AT_type(*$C$DW$T$122)

	.dwendtag $C$DW$382


$C$DW$384	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$384, DW_AT_name("operator =")
	.dwattr $C$DW$384, DW_AT_declaration
	.dwattr $C$DW$384, DW_AT_linkage_name("_ZN20sTIDL_ConcatParams_taSEOS_")
	.dwattr $C$DW$384, DW_AT_type(*$C$DW$T$120)
	.dwattr $C$DW$384, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$385	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$385, DW_AT_type(*$C$DW$T$120)

	.dwendtag $C$DW$384

	.dwattr $C$DW$T$125, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$125, DW_AT_decl_line(0x4af)
	.dwattr $C$DW$T$125, DW_AT_decl_column(0x10)
	.dwendtag $C$DW$T$125

	.dwendtag $C$DW$TU$125


$C$DW$TU$120	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$120
$C$DW$T$120	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$120, DW_AT_type(*$C$DW$T$125)
	.dwattr $C$DW$T$120, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$120


$C$DW$TU$123	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$123

$C$DW$T$123	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$123, DW_AT_type(*$C$DW$T$120)
$C$DW$386	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$386, DW_AT_type(*$C$DW$T$122)

	.dwendtag $C$DW$T$123

	.dwendtag $C$DW$TU$123


$C$DW$TU$124	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$124

$C$DW$T$124	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$124, DW_AT_type(*$C$DW$T$120)
$C$DW$387	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$387, DW_AT_type(*$C$DW$T$120)

	.dwendtag $C$DW$T$124

	.dwendtag $C$DW$TU$124


$C$DW$TU$121	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$121
$C$DW$T$121	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$121, DW_AT_type(*$C$DW$T$125)

	.dwendtag $C$DW$TU$121


$C$DW$TU$122	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$122
$C$DW$T$122	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$122, DW_AT_type(*$C$DW$T$121)
	.dwattr $C$DW$T$122, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$122


$C$DW$TU$205	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$205
$C$DW$T$205	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$205, DW_AT_name("sTIDL_ConcatParams_t")
	.dwattr $C$DW$T$205, DW_AT_type(*$C$DW$T$125)
	.dwattr $C$DW$T$205, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$205, DW_AT_decl_line(0x4b4)
	.dwattr $C$DW$T$205, DW_AT_decl_column(0x02)

	.dwendtag $C$DW$TU$205


$C$DW$TU$133	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$133

$C$DW$T$133	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$133, DW_AT_name("sTIDL_ConvParams_t")
	.dwattr $C$DW$T$133, DW_AT_byte_size(0xa8)
$C$DW$388	.dwtag  DW_TAG_member
	.dwattr $C$DW$388, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$388, DW_AT_name("weights")
	.dwattr $C$DW$388, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$388, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$388, DW_AT_decl_line(0x3dc)
	.dwattr $C$DW$388, DW_AT_decl_column(0x0d)

$C$DW$389	.dwtag  DW_TAG_member
	.dwattr $C$DW$389, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$389, DW_AT_name("bias")
	.dwattr $C$DW$389, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$389, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$389, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$389, DW_AT_decl_line(0x3de)
	.dwattr $C$DW$389, DW_AT_decl_column(0x0d)

$C$DW$390	.dwtag  DW_TAG_member
	.dwattr $C$DW$390, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$390, DW_AT_name("perChannelWeightScaleOffset")
	.dwattr $C$DW$390, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$390, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$390, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$390, DW_AT_decl_line(0x3e1)
	.dwattr $C$DW$390, DW_AT_decl_column(0x0d)

$C$DW$391	.dwtag  DW_TAG_member
	.dwattr $C$DW$391, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$391, DW_AT_name("convolutionType")
	.dwattr $C$DW$391, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$391, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$391, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$391, DW_AT_decl_line(0x3e3)
	.dwattr $C$DW$391, DW_AT_decl_column(0x0d)

$C$DW$392	.dwtag  DW_TAG_member
	.dwattr $C$DW$392, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$392, DW_AT_name("numInChannels")
	.dwattr $C$DW$392, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$392, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$392, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$392, DW_AT_decl_line(0x3e5)
	.dwattr $C$DW$392, DW_AT_decl_column(0x0d)

$C$DW$393	.dwtag  DW_TAG_member
	.dwattr $C$DW$393, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$393, DW_AT_name("numOutChannels")
	.dwattr $C$DW$393, DW_AT_data_member_location[DW_OP_plus_uconst 0x14]
	.dwattr $C$DW$393, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$393, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$393, DW_AT_decl_line(0x3e7)
	.dwattr $C$DW$393, DW_AT_decl_column(0x0d)

$C$DW$394	.dwtag  DW_TAG_member
	.dwattr $C$DW$394, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$394, DW_AT_name("numGroups")
	.dwattr $C$DW$394, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$394, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$394, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$394, DW_AT_decl_line(0x3e9)
	.dwattr $C$DW$394, DW_AT_decl_column(0x0d)

$C$DW$395	.dwtag  DW_TAG_member
	.dwattr $C$DW$395, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$395, DW_AT_name("kernelW")
	.dwattr $C$DW$395, DW_AT_data_member_location[DW_OP_plus_uconst 0x1c]
	.dwattr $C$DW$395, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$395, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$395, DW_AT_decl_line(0x3eb)
	.dwattr $C$DW$395, DW_AT_decl_column(0x0d)

$C$DW$396	.dwtag  DW_TAG_member
	.dwattr $C$DW$396, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$396, DW_AT_name("kernelH")
	.dwattr $C$DW$396, DW_AT_data_member_location[DW_OP_plus_uconst 0x20]
	.dwattr $C$DW$396, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$396, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$396, DW_AT_decl_line(0x3ed)
	.dwattr $C$DW$396, DW_AT_decl_column(0x0d)

$C$DW$397	.dwtag  DW_TAG_member
	.dwattr $C$DW$397, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$397, DW_AT_name("strideW")
	.dwattr $C$DW$397, DW_AT_data_member_location[DW_OP_plus_uconst 0x24]
	.dwattr $C$DW$397, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$397, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$397, DW_AT_decl_line(0x3ef)
	.dwattr $C$DW$397, DW_AT_decl_column(0x0d)

$C$DW$398	.dwtag  DW_TAG_member
	.dwattr $C$DW$398, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$398, DW_AT_name("strideH")
	.dwattr $C$DW$398, DW_AT_data_member_location[DW_OP_plus_uconst 0x28]
	.dwattr $C$DW$398, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$398, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$398, DW_AT_decl_line(0x3f1)
	.dwattr $C$DW$398, DW_AT_decl_column(0x0d)

$C$DW$399	.dwtag  DW_TAG_member
	.dwattr $C$DW$399, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$399, DW_AT_name("dilationW")
	.dwattr $C$DW$399, DW_AT_data_member_location[DW_OP_plus_uconst 0x2c]
	.dwattr $C$DW$399, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$399, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$399, DW_AT_decl_line(0x3f3)
	.dwattr $C$DW$399, DW_AT_decl_column(0x0d)

$C$DW$400	.dwtag  DW_TAG_member
	.dwattr $C$DW$400, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$400, DW_AT_name("dilationH")
	.dwattr $C$DW$400, DW_AT_data_member_location[DW_OP_plus_uconst 0x30]
	.dwattr $C$DW$400, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$400, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$400, DW_AT_decl_line(0x3f5)
	.dwattr $C$DW$400, DW_AT_decl_column(0x0d)

$C$DW$401	.dwtag  DW_TAG_member
	.dwattr $C$DW$401, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$401, DW_AT_name("padW")
	.dwattr $C$DW$401, DW_AT_data_member_location[DW_OP_plus_uconst 0x34]
	.dwattr $C$DW$401, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$401, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$401, DW_AT_decl_line(0x3f7)
	.dwattr $C$DW$401, DW_AT_decl_column(0x0d)

$C$DW$402	.dwtag  DW_TAG_member
	.dwattr $C$DW$402, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$402, DW_AT_name("padH")
	.dwattr $C$DW$402, DW_AT_data_member_location[DW_OP_plus_uconst 0x38]
	.dwattr $C$DW$402, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$402, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$402, DW_AT_decl_line(0x3f9)
	.dwattr $C$DW$402, DW_AT_decl_column(0x0d)

$C$DW$403	.dwtag  DW_TAG_member
	.dwattr $C$DW$403, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$403, DW_AT_name("weightScale")
	.dwattr $C$DW$403, DW_AT_data_member_location[DW_OP_plus_uconst 0x3c]
	.dwattr $C$DW$403, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$403, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$403, DW_AT_decl_line(0x3fb)
	.dwattr $C$DW$403, DW_AT_decl_column(0x14)

$C$DW$404	.dwtag  DW_TAG_member
	.dwattr $C$DW$404, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$404, DW_AT_name("biasScale")
	.dwattr $C$DW$404, DW_AT_data_member_location[DW_OP_plus_uconst 0x40]
	.dwattr $C$DW$404, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$404, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$404, DW_AT_decl_line(0x3fd)
	.dwattr $C$DW$404, DW_AT_decl_column(0x14)

$C$DW$405	.dwtag  DW_TAG_member
	.dwattr $C$DW$405, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$405, DW_AT_name("weightsQ")
	.dwattr $C$DW$405, DW_AT_data_member_location[DW_OP_plus_uconst 0x44]
	.dwattr $C$DW$405, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$405, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$405, DW_AT_decl_line(0x3ff)
	.dwattr $C$DW$405, DW_AT_decl_column(0x0d)

$C$DW$406	.dwtag  DW_TAG_member
	.dwattr $C$DW$406, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$406, DW_AT_name("zeroWeightValue")
	.dwattr $C$DW$406, DW_AT_data_member_location[DW_OP_plus_uconst 0x48]
	.dwattr $C$DW$406, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$406, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$406, DW_AT_decl_line(0x401)
	.dwattr $C$DW$406, DW_AT_decl_column(0x0d)

$C$DW$407	.dwtag  DW_TAG_member
	.dwattr $C$DW$407, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$407, DW_AT_name("biasB")
	.dwattr $C$DW$407, DW_AT_data_member_location[DW_OP_plus_uconst 0x4c]
	.dwattr $C$DW$407, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$407, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$407, DW_AT_decl_line(0x403)
	.dwattr $C$DW$407, DW_AT_decl_column(0x0d)

$C$DW$408	.dwtag  DW_TAG_member
	.dwattr $C$DW$408, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$408, DW_AT_name("biasQ")
	.dwattr $C$DW$408, DW_AT_data_member_location[DW_OP_plus_uconst 0x50]
	.dwattr $C$DW$408, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$408, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$408, DW_AT_decl_line(0x405)
	.dwattr $C$DW$408, DW_AT_decl_column(0x0d)

$C$DW$409	.dwtag  DW_TAG_member
	.dwattr $C$DW$409, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$409, DW_AT_name("inDataQ")
	.dwattr $C$DW$409, DW_AT_data_member_location[DW_OP_plus_uconst 0x54]
	.dwattr $C$DW$409, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$409, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$409, DW_AT_decl_line(0x407)
	.dwattr $C$DW$409, DW_AT_decl_column(0x0d)

$C$DW$410	.dwtag  DW_TAG_member
	.dwattr $C$DW$410, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$410, DW_AT_name("outDataQ")
	.dwattr $C$DW$410, DW_AT_data_member_location[DW_OP_plus_uconst 0x58]
	.dwattr $C$DW$410, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$410, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$410, DW_AT_decl_line(0x409)
	.dwattr $C$DW$410, DW_AT_decl_column(0x0d)

$C$DW$411	.dwtag  DW_TAG_member
	.dwattr $C$DW$411, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$411, DW_AT_name("interDataQ")
	.dwattr $C$DW$411, DW_AT_data_member_location[DW_OP_plus_uconst 0x5c]
	.dwattr $C$DW$411, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$411, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$411, DW_AT_decl_line(0x40b)
	.dwattr $C$DW$411, DW_AT_decl_column(0x0d)

$C$DW$412	.dwtag  DW_TAG_member
	.dwattr $C$DW$412, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$412, DW_AT_name("enableBias")
	.dwattr $C$DW$412, DW_AT_data_member_location[DW_OP_plus_uconst 0x60]
	.dwattr $C$DW$412, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$412, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$412, DW_AT_decl_line(0x40d)
	.dwattr $C$DW$412, DW_AT_decl_column(0x0d)

$C$DW$413	.dwtag  DW_TAG_member
	.dwattr $C$DW$413, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$413, DW_AT_name("enablePooling")
	.dwattr $C$DW$413, DW_AT_data_member_location[DW_OP_plus_uconst 0x64]
	.dwattr $C$DW$413, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$413, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$413, DW_AT_decl_line(0x40f)
	.dwattr $C$DW$413, DW_AT_decl_column(0x0d)

$C$DW$414	.dwtag  DW_TAG_member
	.dwattr $C$DW$414, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$414, DW_AT_name("enableEltWise")
	.dwattr $C$DW$414, DW_AT_data_member_location[DW_OP_plus_uconst 0x68]
	.dwattr $C$DW$414, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$414, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$414, DW_AT_decl_line(0x411)
	.dwattr $C$DW$414, DW_AT_decl_column(0x0d)

$C$DW$415	.dwtag  DW_TAG_member
	.dwattr $C$DW$415, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$415, DW_AT_name("enableEWRelU")
	.dwattr $C$DW$415, DW_AT_data_member_location[DW_OP_plus_uconst 0x6c]
	.dwattr $C$DW$415, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$415, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$415, DW_AT_decl_line(0x413)
	.dwattr $C$DW$415, DW_AT_decl_column(0x0d)

$C$DW$416	.dwtag  DW_TAG_member
	.dwattr $C$DW$416, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$416, DW_AT_name("kernelType")
	.dwattr $C$DW$416, DW_AT_data_member_location[DW_OP_plus_uconst 0x70]
	.dwattr $C$DW$416, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$416, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$416, DW_AT_decl_line(0x415)
	.dwattr $C$DW$416, DW_AT_decl_column(0x0d)

$C$DW$417	.dwtag  DW_TAG_member
	.dwattr $C$DW$417, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$417, DW_AT_name("enableDepthToSpace")
	.dwattr $C$DW$417, DW_AT_data_member_location[DW_OP_plus_uconst 0x74]
	.dwattr $C$DW$417, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$417, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$417, DW_AT_decl_line(0x417)
	.dwattr $C$DW$417, DW_AT_decl_column(0x0d)

$C$DW$418	.dwtag  DW_TAG_member
	.dwattr $C$DW$418, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$418, DW_AT_name("upscaleFactor")
	.dwattr $C$DW$418, DW_AT_data_member_location[DW_OP_plus_uconst 0x78]
	.dwattr $C$DW$418, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$418, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$418, DW_AT_decl_line(0x41a)
	.dwattr $C$DW$418, DW_AT_decl_column(0x0d)

$C$DW$419	.dwtag  DW_TAG_member
	.dwattr $C$DW$419, DW_AT_type(*$C$DW$T$127)
	.dwattr $C$DW$419, DW_AT_name("poolParams")
	.dwattr $C$DW$419, DW_AT_data_member_location[DW_OP_plus_uconst 0x7c]
	.dwattr $C$DW$419, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$419, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$419, DW_AT_decl_line(0x41c)
	.dwattr $C$DW$419, DW_AT_decl_column(0x19)


$C$DW$420	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$420, DW_AT_name("operator =")
	.dwattr $C$DW$420, DW_AT_declaration
	.dwattr $C$DW$420, DW_AT_linkage_name("_ZN18sTIDL_ConvParams_taSERKS_")
	.dwattr $C$DW$420, DW_AT_type(*$C$DW$T$128)
	.dwattr $C$DW$420, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$421	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$421, DW_AT_type(*$C$DW$T$130)

	.dwendtag $C$DW$420


$C$DW$422	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$422, DW_AT_name("operator =")
	.dwattr $C$DW$422, DW_AT_declaration
	.dwattr $C$DW$422, DW_AT_linkage_name("_ZN18sTIDL_ConvParams_taSEOS_")
	.dwattr $C$DW$422, DW_AT_type(*$C$DW$T$128)
	.dwattr $C$DW$422, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$423	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$423, DW_AT_type(*$C$DW$T$128)

	.dwendtag $C$DW$422

	.dwattr $C$DW$T$133, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$133, DW_AT_decl_line(0x3da)
	.dwattr $C$DW$T$133, DW_AT_decl_column(0x10)
	.dwendtag $C$DW$T$133

	.dwendtag $C$DW$TU$133


$C$DW$TU$128	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$128
$C$DW$T$128	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$128, DW_AT_type(*$C$DW$T$133)
	.dwattr $C$DW$T$128, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$128


$C$DW$TU$131	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$131

$C$DW$T$131	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$131, DW_AT_type(*$C$DW$T$128)
$C$DW$424	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$424, DW_AT_type(*$C$DW$T$130)

	.dwendtag $C$DW$T$131

	.dwendtag $C$DW$TU$131


$C$DW$TU$132	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$132

$C$DW$T$132	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$132, DW_AT_type(*$C$DW$T$128)
$C$DW$425	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$425, DW_AT_type(*$C$DW$T$128)

	.dwendtag $C$DW$T$132

	.dwendtag $C$DW$TU$132


$C$DW$TU$129	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$129
$C$DW$T$129	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$129, DW_AT_type(*$C$DW$T$133)

	.dwendtag $C$DW$TU$129


$C$DW$TU$130	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$130
$C$DW$T$130	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$130, DW_AT_type(*$C$DW$T$129)
	.dwattr $C$DW$T$130, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$130


$C$DW$TU$198	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$198
$C$DW$T$198	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$198, DW_AT_name("sTIDL_ConvParams_t")
	.dwattr $C$DW$T$198, DW_AT_type(*$C$DW$T$133)
	.dwattr $C$DW$T$198, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$198, DW_AT_decl_line(0x41d)
	.dwattr $C$DW$T$198, DW_AT_decl_column(0x02)

	.dwendtag $C$DW$TU$198


$C$DW$TU$141	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$141

$C$DW$T$141	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$141, DW_AT_name("sTIDL_CropParams_t")
	.dwattr $C$DW$T$141, DW_AT_byte_size(0x14)
$C$DW$426	.dwtag  DW_TAG_member
	.dwattr $C$DW$426, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$426, DW_AT_name("numChannels")
	.dwattr $C$DW$426, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$426, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$426, DW_AT_decl_line(0x535)
	.dwattr $C$DW$426, DW_AT_decl_column(0x0d)

$C$DW$427	.dwtag  DW_TAG_member
	.dwattr $C$DW$427, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$427, DW_AT_name("inDataQ")
	.dwattr $C$DW$427, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$427, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$427, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$427, DW_AT_decl_line(0x537)
	.dwattr $C$DW$427, DW_AT_decl_column(0x0d)

$C$DW$428	.dwtag  DW_TAG_member
	.dwattr $C$DW$428, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$428, DW_AT_name("outDataQ")
	.dwattr $C$DW$428, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$428, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$428, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$428, DW_AT_decl_line(0x539)
	.dwattr $C$DW$428, DW_AT_decl_column(0x0d)

$C$DW$429	.dwtag  DW_TAG_member
	.dwattr $C$DW$429, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$429, DW_AT_name("offsetW")
	.dwattr $C$DW$429, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$429, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$429, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$429, DW_AT_decl_line(0x53b)
	.dwattr $C$DW$429, DW_AT_decl_column(0x0c)

$C$DW$430	.dwtag  DW_TAG_member
	.dwattr $C$DW$430, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$430, DW_AT_name("offsetH")
	.dwattr $C$DW$430, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$430, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$430, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$430, DW_AT_decl_line(0x53d)
	.dwattr $C$DW$430, DW_AT_decl_column(0x0c)


$C$DW$431	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$431, DW_AT_name("operator =")
	.dwattr $C$DW$431, DW_AT_declaration
	.dwattr $C$DW$431, DW_AT_linkage_name("_ZN18sTIDL_CropParams_taSERKS_")
	.dwattr $C$DW$431, DW_AT_type(*$C$DW$T$136)
	.dwattr $C$DW$431, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$432	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$432, DW_AT_type(*$C$DW$T$138)

	.dwendtag $C$DW$431


$C$DW$433	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$433, DW_AT_name("operator =")
	.dwattr $C$DW$433, DW_AT_declaration
	.dwattr $C$DW$433, DW_AT_linkage_name("_ZN18sTIDL_CropParams_taSEOS_")
	.dwattr $C$DW$433, DW_AT_type(*$C$DW$T$136)
	.dwattr $C$DW$433, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$434	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$434, DW_AT_type(*$C$DW$T$136)

	.dwendtag $C$DW$433

	.dwattr $C$DW$T$141, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$141, DW_AT_decl_line(0x533)
	.dwattr $C$DW$T$141, DW_AT_decl_column(0x10)
	.dwendtag $C$DW$T$141

	.dwendtag $C$DW$TU$141


$C$DW$TU$136	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$136
$C$DW$T$136	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$136, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$T$136, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$136


$C$DW$TU$139	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$139

$C$DW$T$139	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$139, DW_AT_type(*$C$DW$T$136)
$C$DW$435	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$435, DW_AT_type(*$C$DW$T$138)

	.dwendtag $C$DW$T$139

	.dwendtag $C$DW$TU$139


$C$DW$TU$140	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$140

$C$DW$T$140	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$140, DW_AT_type(*$C$DW$T$136)
$C$DW$436	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$436, DW_AT_type(*$C$DW$T$136)

	.dwendtag $C$DW$T$140

	.dwendtag $C$DW$TU$140


$C$DW$TU$137	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$137
$C$DW$T$137	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$137, DW_AT_type(*$C$DW$T$141)

	.dwendtag $C$DW$TU$137


$C$DW$TU$138	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$138
$C$DW$T$138	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$138, DW_AT_type(*$C$DW$T$137)
	.dwattr $C$DW$T$138, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$138


$C$DW$TU$204	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$204
$C$DW$T$204	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$204, DW_AT_name("sTIDL_CropParams_t")
	.dwattr $C$DW$T$204, DW_AT_type(*$C$DW$T$141)
	.dwattr $C$DW$T$204, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$204, DW_AT_decl_line(0x53e)
	.dwattr $C$DW$T$204, DW_AT_decl_column(0x02)

	.dwendtag $C$DW$TU$204


$C$DW$TU$148	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$148

$C$DW$T$148	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$148, DW_AT_name("sTIDL_CustomParams_t")
	.dwattr $C$DW$T$148, DW_AT_byte_size(0x1c)
$C$DW$437	.dwtag  DW_TAG_member
	.dwattr $C$DW$437, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$437, DW_AT_name("customLayerType")
	.dwattr $C$DW$437, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$437, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$437, DW_AT_decl_line(0x3a4)
	.dwattr $C$DW$437, DW_AT_decl_column(0x0d)

$C$DW$438	.dwtag  DW_TAG_member
	.dwattr $C$DW$438, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$438, DW_AT_name("padW")
	.dwattr $C$DW$438, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$438, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$438, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$438, DW_AT_decl_line(0x3a7)
	.dwattr $C$DW$438, DW_AT_decl_column(0x0b)

$C$DW$439	.dwtag  DW_TAG_member
	.dwattr $C$DW$439, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$439, DW_AT_name("padH")
	.dwattr $C$DW$439, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$439, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$439, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$439, DW_AT_decl_line(0x3aa)
	.dwattr $C$DW$439, DW_AT_decl_column(0x0b)

$C$DW$440	.dwtag  DW_TAG_member
	.dwattr $C$DW$440, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$440, DW_AT_name("memOverlapType")
	.dwattr $C$DW$440, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$440, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$440, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$440, DW_AT_decl_line(0x3b0)
	.dwattr $C$DW$440, DW_AT_decl_column(0x0b)

$C$DW$441	.dwtag  DW_TAG_member
	.dwattr $C$DW$441, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$441, DW_AT_name("doesLayerChangePadding")
	.dwattr $C$DW$441, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$441, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$441, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$441, DW_AT_decl_line(0x3b6)
	.dwattr $C$DW$441, DW_AT_decl_column(0x0b)

$C$DW$442	.dwtag  DW_TAG_member
	.dwattr $C$DW$442, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$442, DW_AT_name("doesLayerFillOutXPadding")
	.dwattr $C$DW$442, DW_AT_data_member_location[DW_OP_plus_uconst 0x14]
	.dwattr $C$DW$442, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$442, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$442, DW_AT_decl_line(0x3d0)
	.dwattr $C$DW$442, DW_AT_decl_column(0x0b)

$C$DW$443	.dwtag  DW_TAG_member
	.dwattr $C$DW$443, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$443, DW_AT_name("rsvdPassThrough")
	.dwattr $C$DW$443, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$443, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$443, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$443, DW_AT_decl_line(0x3d2)
	.dwattr $C$DW$443, DW_AT_decl_column(0x0b)


$C$DW$444	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$444, DW_AT_name("operator =")
	.dwattr $C$DW$444, DW_AT_declaration
	.dwattr $C$DW$444, DW_AT_linkage_name("_ZN20sTIDL_CustomParams_taSERKS_")
	.dwattr $C$DW$444, DW_AT_type(*$C$DW$T$143)
	.dwattr $C$DW$444, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$445	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$445, DW_AT_type(*$C$DW$T$145)

	.dwendtag $C$DW$444


$C$DW$446	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$446, DW_AT_name("operator =")
	.dwattr $C$DW$446, DW_AT_declaration
	.dwattr $C$DW$446, DW_AT_linkage_name("_ZN20sTIDL_CustomParams_taSEOS_")
	.dwattr $C$DW$446, DW_AT_type(*$C$DW$T$143)
	.dwattr $C$DW$446, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$447	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$447, DW_AT_type(*$C$DW$T$143)

	.dwendtag $C$DW$446

	.dwattr $C$DW$T$148, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$148, DW_AT_decl_line(0x3a2)
	.dwattr $C$DW$T$148, DW_AT_decl_column(0x10)
	.dwendtag $C$DW$T$148

	.dwendtag $C$DW$TU$148


$C$DW$TU$143	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$143
$C$DW$T$143	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$143, DW_AT_type(*$C$DW$T$148)
	.dwattr $C$DW$T$143, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$143


$C$DW$TU$146	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$146

$C$DW$T$146	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$146, DW_AT_type(*$C$DW$T$143)
$C$DW$448	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$448, DW_AT_type(*$C$DW$T$145)

	.dwendtag $C$DW$T$146

	.dwendtag $C$DW$TU$146


$C$DW$TU$147	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$147

$C$DW$T$147	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$147, DW_AT_type(*$C$DW$T$143)
$C$DW$449	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$449, DW_AT_type(*$C$DW$T$143)

	.dwendtag $C$DW$T$147

	.dwendtag $C$DW$TU$147


$C$DW$TU$144	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$144
$C$DW$T$144	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$144, DW_AT_type(*$C$DW$T$148)

	.dwendtag $C$DW$TU$144


$C$DW$TU$145	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$145
$C$DW$T$145	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$145, DW_AT_type(*$C$DW$T$144)
	.dwattr $C$DW$T$145, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$145


$C$DW$TU$217	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$217
$C$DW$T$217	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$217, DW_AT_name("sTIDL_CustomParams_t")
	.dwattr $C$DW$T$217, DW_AT_type(*$C$DW$T$148)
	.dwattr $C$DW$T$217, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$217, DW_AT_decl_line(0x3d3)
	.dwattr $C$DW$T$217, DW_AT_decl_column(0x02)

	.dwendtag $C$DW$TU$217


$C$DW$TU$155	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$155

$C$DW$T$155	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$155, DW_AT_name("sTIDL_DataLayerParams_t")
	.dwattr $C$DW$T$155, DW_AT_byte_size(0x08)
$C$DW$450	.dwtag  DW_TAG_member
	.dwattr $C$DW$450, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$450, DW_AT_name("numChannels")
	.dwattr $C$DW$450, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$450, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$450, DW_AT_decl_line(0x296)
	.dwattr $C$DW$450, DW_AT_decl_column(0x0d)

$C$DW$451	.dwtag  DW_TAG_member
	.dwattr $C$DW$451, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$451, DW_AT_name("dataQ")
	.dwattr $C$DW$451, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$451, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$451, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$451, DW_AT_decl_line(0x298)
	.dwattr $C$DW$451, DW_AT_decl_column(0x0d)


$C$DW$452	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$452, DW_AT_name("operator =")
	.dwattr $C$DW$452, DW_AT_declaration
	.dwattr $C$DW$452, DW_AT_linkage_name("_ZN23sTIDL_DataLayerParams_taSERKS_")
	.dwattr $C$DW$452, DW_AT_type(*$C$DW$T$150)
	.dwattr $C$DW$452, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$453	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$453, DW_AT_type(*$C$DW$T$152)

	.dwendtag $C$DW$452


$C$DW$454	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$454, DW_AT_name("operator =")
	.dwattr $C$DW$454, DW_AT_declaration
	.dwattr $C$DW$454, DW_AT_linkage_name("_ZN23sTIDL_DataLayerParams_taSEOS_")
	.dwattr $C$DW$454, DW_AT_type(*$C$DW$T$150)
	.dwattr $C$DW$454, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$455	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$455, DW_AT_type(*$C$DW$T$150)

	.dwendtag $C$DW$454

	.dwattr $C$DW$T$155, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$155, DW_AT_decl_line(0x294)
	.dwattr $C$DW$T$155, DW_AT_decl_column(0x10)
	.dwendtag $C$DW$T$155

	.dwendtag $C$DW$TU$155


$C$DW$TU$150	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$150
$C$DW$T$150	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$150, DW_AT_type(*$C$DW$T$155)
	.dwattr $C$DW$T$150, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$150


$C$DW$TU$153	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$153

$C$DW$T$153	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$153, DW_AT_type(*$C$DW$T$150)
$C$DW$456	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$456, DW_AT_type(*$C$DW$T$152)

	.dwendtag $C$DW$T$153

	.dwendtag $C$DW$TU$153


$C$DW$TU$154	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$154

$C$DW$T$154	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$154, DW_AT_type(*$C$DW$T$150)
$C$DW$457	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$457, DW_AT_type(*$C$DW$T$150)

	.dwendtag $C$DW$T$154

	.dwendtag $C$DW$TU$154


$C$DW$TU$151	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$151
$C$DW$T$151	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$151, DW_AT_type(*$C$DW$T$155)

	.dwendtag $C$DW$TU$151


$C$DW$TU$152	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$152
$C$DW$T$152	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$152, DW_AT_type(*$C$DW$T$151)
	.dwattr $C$DW$T$152, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$152


$C$DW$TU$201	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$201
$C$DW$T$201	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$201, DW_AT_name("sTIDL_DataLayerParams_t")
	.dwattr $C$DW$T$201, DW_AT_type(*$C$DW$T$155)
	.dwattr $C$DW$T$201, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$201, DW_AT_decl_line(0x299)
	.dwattr $C$DW$T$201, DW_AT_decl_column(0x02)

	.dwendtag $C$DW$TU$201


$C$DW$TU$164	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$164

$C$DW$T$164	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$164, DW_AT_name("sTIDL_DataParams_t")
	.dwattr $C$DW$T$164, DW_AT_byte_size(0x5c)
$C$DW$458	.dwtag  DW_TAG_member
	.dwattr $C$DW$458, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$458, DW_AT_name("dataId")
	.dwattr $C$DW$458, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$458, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$458, DW_AT_decl_line(0x268)
	.dwattr $C$DW$458, DW_AT_decl_column(0x0b)

$C$DW$459	.dwtag  DW_TAG_member
	.dwattr $C$DW$459, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$459, DW_AT_name("elementType")
	.dwattr $C$DW$459, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$459, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$459, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$459, DW_AT_decl_line(0x26a)
	.dwattr $C$DW$459, DW_AT_decl_column(0x0b)

$C$DW$460	.dwtag  DW_TAG_member
	.dwattr $C$DW$460, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$460, DW_AT_name("numDim")
	.dwattr $C$DW$460, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$460, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$460, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$460, DW_AT_decl_line(0x26c)
	.dwattr $C$DW$460, DW_AT_decl_column(0x0b)

$C$DW$461	.dwtag  DW_TAG_member
	.dwattr $C$DW$461, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$461, DW_AT_name("dataQ")
	.dwattr $C$DW$461, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$461, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$461, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$461, DW_AT_decl_line(0x26e)
	.dwattr $C$DW$461, DW_AT_decl_column(0x0b)

$C$DW$462	.dwtag  DW_TAG_member
	.dwattr $C$DW$462, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$462, DW_AT_name("minValue")
	.dwattr $C$DW$462, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$462, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$462, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$462, DW_AT_decl_line(0x270)
	.dwattr $C$DW$462, DW_AT_decl_column(0x0b)

$C$DW$463	.dwtag  DW_TAG_member
	.dwattr $C$DW$463, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$463, DW_AT_name("maxValue")
	.dwattr $C$DW$463, DW_AT_data_member_location[DW_OP_plus_uconst 0x14]
	.dwattr $C$DW$463, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$463, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$463, DW_AT_decl_line(0x272)
	.dwattr $C$DW$463, DW_AT_decl_column(0x0b)

$C$DW$464	.dwtag  DW_TAG_member
	.dwattr $C$DW$464, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$464, DW_AT_name("minTensorValue")
	.dwattr $C$DW$464, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$464, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$464, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$464, DW_AT_decl_line(0x274)
	.dwattr $C$DW$464, DW_AT_decl_column(0x12)

$C$DW$465	.dwtag  DW_TAG_member
	.dwattr $C$DW$465, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$465, DW_AT_name("maxTensorValue")
	.dwattr $C$DW$465, DW_AT_data_member_location[DW_OP_plus_uconst 0x1c]
	.dwattr $C$DW$465, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$465, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$465, DW_AT_decl_line(0x276)
	.dwattr $C$DW$465, DW_AT_decl_column(0x12)

$C$DW$466	.dwtag  DW_TAG_member
	.dwattr $C$DW$466, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$466, DW_AT_name("tensorScale")
	.dwattr $C$DW$466, DW_AT_data_member_location[DW_OP_plus_uconst 0x20]
	.dwattr $C$DW$466, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$466, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$466, DW_AT_decl_line(0x278)
	.dwattr $C$DW$466, DW_AT_decl_column(0x12)

$C$DW$467	.dwtag  DW_TAG_member
	.dwattr $C$DW$467, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$467, DW_AT_name("padW")
	.dwattr $C$DW$467, DW_AT_data_member_location[DW_OP_plus_uconst 0x24]
	.dwattr $C$DW$467, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$467, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$467, DW_AT_decl_line(0x27a)
	.dwattr $C$DW$467, DW_AT_decl_column(0x0b)

$C$DW$468	.dwtag  DW_TAG_member
	.dwattr $C$DW$468, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$468, DW_AT_name("padH")
	.dwattr $C$DW$468, DW_AT_data_member_location[DW_OP_plus_uconst 0x28]
	.dwattr $C$DW$468, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$468, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$468, DW_AT_decl_line(0x27c)
	.dwattr $C$DW$468, DW_AT_decl_column(0x0b)

$C$DW$469	.dwtag  DW_TAG_member
	.dwattr $C$DW$469, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$469, DW_AT_name("batchPadW")
	.dwattr $C$DW$469, DW_AT_data_member_location[DW_OP_plus_uconst 0x2c]
	.dwattr $C$DW$469, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$469, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$469, DW_AT_decl_line(0x27e)
	.dwattr $C$DW$469, DW_AT_decl_column(0x0b)

$C$DW$470	.dwtag  DW_TAG_member
	.dwattr $C$DW$470, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$470, DW_AT_name("batchPadH")
	.dwattr $C$DW$470, DW_AT_data_member_location[DW_OP_plus_uconst 0x30]
	.dwattr $C$DW$470, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$470, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$470, DW_AT_decl_line(0x280)
	.dwattr $C$DW$470, DW_AT_decl_column(0x0b)

$C$DW$471	.dwtag  DW_TAG_member
	.dwattr $C$DW$471, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$471, DW_AT_name("numBatchW")
	.dwattr $C$DW$471, DW_AT_data_member_location[DW_OP_plus_uconst 0x34]
	.dwattr $C$DW$471, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$471, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$471, DW_AT_decl_line(0x282)
	.dwattr $C$DW$471, DW_AT_decl_column(0x0b)

$C$DW$472	.dwtag  DW_TAG_member
	.dwattr $C$DW$472, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$472, DW_AT_name("numBatchH")
	.dwattr $C$DW$472, DW_AT_data_member_location[DW_OP_plus_uconst 0x38]
	.dwattr $C$DW$472, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$472, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$472, DW_AT_decl_line(0x284)
	.dwattr $C$DW$472, DW_AT_decl_column(0x0b)

$C$DW$473	.dwtag  DW_TAG_member
	.dwattr $C$DW$473, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$473, DW_AT_name("roundBits")
	.dwattr $C$DW$473, DW_AT_data_member_location[DW_OP_plus_uconst 0x3c]
	.dwattr $C$DW$473, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$473, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$473, DW_AT_decl_line(0x286)
	.dwattr $C$DW$473, DW_AT_decl_column(0x0b)

$C$DW$474	.dwtag  DW_TAG_member
	.dwattr $C$DW$474, DW_AT_type(*$C$DW$T$157)
	.dwattr $C$DW$474, DW_AT_name("pitch")
	.dwattr $C$DW$474, DW_AT_data_member_location[DW_OP_plus_uconst 0x40]
	.dwattr $C$DW$474, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$474, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$474, DW_AT_decl_line(0x288)
	.dwattr $C$DW$474, DW_AT_decl_column(0x0b)

$C$DW$475	.dwtag  DW_TAG_member
	.dwattr $C$DW$475, DW_AT_type(*$C$DW$T$158)
	.dwattr $C$DW$475, DW_AT_name("dimValues")
	.dwattr $C$DW$475, DW_AT_data_member_location[DW_OP_plus_uconst 0x4c]
	.dwattr $C$DW$475, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$475, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$475, DW_AT_decl_line(0x28a)
	.dwattr $C$DW$475, DW_AT_decl_column(0x0b)


$C$DW$476	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$476, DW_AT_name("operator =")
	.dwattr $C$DW$476, DW_AT_declaration
	.dwattr $C$DW$476, DW_AT_linkage_name("_ZN18sTIDL_DataParams_taSERKS_")
	.dwattr $C$DW$476, DW_AT_type(*$C$DW$T$159)
	.dwattr $C$DW$476, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$477	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$477, DW_AT_type(*$C$DW$T$161)

	.dwendtag $C$DW$476


$C$DW$478	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$478, DW_AT_name("operator =")
	.dwattr $C$DW$478, DW_AT_declaration
	.dwattr $C$DW$478, DW_AT_linkage_name("_ZN18sTIDL_DataParams_taSEOS_")
	.dwattr $C$DW$478, DW_AT_type(*$C$DW$T$159)
	.dwattr $C$DW$478, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$479	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$479, DW_AT_type(*$C$DW$T$159)

	.dwendtag $C$DW$478

	.dwattr $C$DW$T$164, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$164, DW_AT_decl_line(0x266)
	.dwattr $C$DW$T$164, DW_AT_decl_column(0x10)
	.dwendtag $C$DW$T$164

	.dwendtag $C$DW$TU$164


$C$DW$TU$159	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$159
$C$DW$T$159	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$159, DW_AT_type(*$C$DW$T$164)
	.dwattr $C$DW$T$159, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$159


$C$DW$TU$162	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$162

$C$DW$T$162	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$162, DW_AT_type(*$C$DW$T$159)
$C$DW$480	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$480, DW_AT_type(*$C$DW$T$161)

	.dwendtag $C$DW$T$162

	.dwendtag $C$DW$TU$162


$C$DW$TU$163	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$163

$C$DW$T$163	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$163, DW_AT_type(*$C$DW$T$159)
$C$DW$481	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$481, DW_AT_type(*$C$DW$T$159)

	.dwendtag $C$DW$T$163

	.dwendtag $C$DW$TU$163


$C$DW$TU$160	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$160
$C$DW$T$160	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$160, DW_AT_type(*$C$DW$T$164)

	.dwendtag $C$DW$TU$160


$C$DW$TU$161	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$161
$C$DW$T$161	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$161, DW_AT_type(*$C$DW$T$160)
	.dwattr $C$DW$T$161, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$161


$C$DW$TU$247	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$247
$C$DW$T$247	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$247, DW_AT_name("sTIDL_DataParams_t")
	.dwattr $C$DW$T$247, DW_AT_type(*$C$DW$T$164)
	.dwattr $C$DW$T$247, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$247, DW_AT_decl_line(0x28b)
	.dwattr $C$DW$T$247, DW_AT_decl_column(0x02)

	.dwendtag $C$DW$TU$247


$C$DW$TU$248	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$248

$C$DW$T$248	.dwtag  DW_TAG_array_type
	.dwattr $C$DW$T$248, DW_AT_type(*$C$DW$T$247)
	.dwattr $C$DW$T$248, DW_AT_byte_size(0x5c0)
$C$DW$482	.dwtag  DW_TAG_subrange_type
	.dwattr $C$DW$482, DW_AT_upper_bound(0x0f)

	.dwendtag $C$DW$T$248

	.dwendtag $C$DW$TU$248


$C$DW$TU$173	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$173

$C$DW$T$173	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$173, DW_AT_name("sTIDL_DepthToSpaceParams_t")
	.dwattr $C$DW$T$173, DW_AT_byte_size(0x04)
$C$DW$483	.dwtag  DW_TAG_member
	.dwattr $C$DW$483, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$483, DW_AT_name("blockSize")
	.dwattr $C$DW$483, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$483, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$483, DW_AT_decl_line(0x2af)
	.dwattr $C$DW$483, DW_AT_decl_column(0x0d)


$C$DW$484	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$484, DW_AT_name("operator =")
	.dwattr $C$DW$484, DW_AT_declaration
	.dwattr $C$DW$484, DW_AT_linkage_name("_ZN26sTIDL_DepthToSpaceParams_taSERKS_")
	.dwattr $C$DW$484, DW_AT_type(*$C$DW$T$168)
	.dwattr $C$DW$484, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$485	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$485, DW_AT_type(*$C$DW$T$170)

	.dwendtag $C$DW$484


$C$DW$486	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$486, DW_AT_name("operator =")
	.dwattr $C$DW$486, DW_AT_declaration
	.dwattr $C$DW$486, DW_AT_linkage_name("_ZN26sTIDL_DepthToSpaceParams_taSEOS_")
	.dwattr $C$DW$486, DW_AT_type(*$C$DW$T$168)
	.dwattr $C$DW$486, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$487	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$487, DW_AT_type(*$C$DW$T$168)

	.dwendtag $C$DW$486

	.dwattr $C$DW$T$173, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$173, DW_AT_decl_line(0x2ad)
	.dwattr $C$DW$T$173, DW_AT_decl_column(0x10)
	.dwendtag $C$DW$T$173

	.dwendtag $C$DW$TU$173


$C$DW$TU$168	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$168
$C$DW$T$168	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$168, DW_AT_type(*$C$DW$T$173)
	.dwattr $C$DW$T$168, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$168


$C$DW$TU$171	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$171

$C$DW$T$171	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$171, DW_AT_type(*$C$DW$T$168)
$C$DW$488	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$488, DW_AT_type(*$C$DW$T$170)

	.dwendtag $C$DW$T$171

	.dwendtag $C$DW$TU$171


$C$DW$TU$172	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$172

$C$DW$T$172	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$172, DW_AT_type(*$C$DW$T$168)
$C$DW$489	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$489, DW_AT_type(*$C$DW$T$168)

	.dwendtag $C$DW$T$172

	.dwendtag $C$DW$TU$172


$C$DW$TU$169	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$169
$C$DW$T$169	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$169, DW_AT_type(*$C$DW$T$173)

	.dwendtag $C$DW$TU$169


$C$DW$TU$170	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$170
$C$DW$T$170	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$170, DW_AT_type(*$C$DW$T$169)
	.dwattr $C$DW$T$170, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$170


$C$DW$TU$213	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$213
$C$DW$T$213	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$213, DW_AT_name("sTIDL_DepthToSpaceParams_t")
	.dwattr $C$DW$T$213, DW_AT_type(*$C$DW$T$173)
	.dwattr $C$DW$T$213, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$213, DW_AT_decl_line(0x2b0)
	.dwattr $C$DW$T$213, DW_AT_decl_column(0x02)

	.dwendtag $C$DW$TU$213


$C$DW$TU$180	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$180

$C$DW$T$180	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$180, DW_AT_name("sTIDL_DetectOutputParams_t")
	.dwattr $C$DW$T$180, DW_AT_byte_size(0x50)
$C$DW$490	.dwtag  DW_TAG_member
	.dwattr $C$DW$490, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$490, DW_AT_name("processingType")
	.dwattr $C$DW$490, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$490, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$490, DW_AT_decl_line(0x44d)
	.dwattr $C$DW$490, DW_AT_decl_column(0x0d)

$C$DW$491	.dwtag  DW_TAG_member
	.dwattr $C$DW$491, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$491, DW_AT_name("priorBox")
	.dwattr $C$DW$491, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$491, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$491, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$491, DW_AT_decl_line(0x44f)
	.dwattr $C$DW$491, DW_AT_decl_column(0x0c)

$C$DW$492	.dwtag  DW_TAG_member
	.dwattr $C$DW$492, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$492, DW_AT_name("priorBoxSize")
	.dwattr $C$DW$492, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$492, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$492, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$492, DW_AT_decl_line(0x451)
	.dwattr $C$DW$492, DW_AT_decl_column(0x0c)

$C$DW$493	.dwtag  DW_TAG_member
	.dwattr $C$DW$493, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$493, DW_AT_name("numClasses")
	.dwattr $C$DW$493, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$493, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$493, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$493, DW_AT_decl_line(0x453)
	.dwattr $C$DW$493, DW_AT_decl_column(0x0c)

$C$DW$494	.dwtag  DW_TAG_member
	.dwattr $C$DW$494, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$494, DW_AT_name("backgroundLabelId")
	.dwattr $C$DW$494, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$494, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$494, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$494, DW_AT_decl_line(0x455)
	.dwattr $C$DW$494, DW_AT_decl_column(0x0c)

$C$DW$495	.dwtag  DW_TAG_member
	.dwattr $C$DW$495, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$495, DW_AT_name("codeType")
	.dwattr $C$DW$495, DW_AT_data_member_location[DW_OP_plus_uconst 0x14]
	.dwattr $C$DW$495, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$495, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$495, DW_AT_decl_line(0x457)
	.dwattr $C$DW$495, DW_AT_decl_column(0x0c)

$C$DW$496	.dwtag  DW_TAG_member
	.dwattr $C$DW$496, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$496, DW_AT_name("confThreshold")
	.dwattr $C$DW$496, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$496, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$496, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$496, DW_AT_decl_line(0x45a)
	.dwattr $C$DW$496, DW_AT_decl_column(0x11)

$C$DW$497	.dwtag  DW_TAG_member
	.dwattr $C$DW$497, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$497, DW_AT_name("nmsThreshold")
	.dwattr $C$DW$497, DW_AT_data_member_location[DW_OP_plus_uconst 0x1c]
	.dwattr $C$DW$497, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$497, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$497, DW_AT_decl_line(0x45d)
	.dwattr $C$DW$497, DW_AT_decl_column(0x11)

$C$DW$498	.dwtag  DW_TAG_member
	.dwattr $C$DW$498, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$498, DW_AT_name("eta")
	.dwattr $C$DW$498, DW_AT_data_member_location[DW_OP_plus_uconst 0x20]
	.dwattr $C$DW$498, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$498, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$498, DW_AT_decl_line(0x45f)
	.dwattr $C$DW$498, DW_AT_decl_column(0x11)

$C$DW$499	.dwtag  DW_TAG_member
	.dwattr $C$DW$499, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$499, DW_AT_name("topK")
	.dwattr $C$DW$499, DW_AT_data_member_location[DW_OP_plus_uconst 0x24]
	.dwattr $C$DW$499, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$499, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$499, DW_AT_decl_line(0x461)
	.dwattr $C$DW$499, DW_AT_decl_column(0x0c)

$C$DW$500	.dwtag  DW_TAG_member
	.dwattr $C$DW$500, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$500, DW_AT_name("keepTopK")
	.dwattr $C$DW$500, DW_AT_data_member_location[DW_OP_plus_uconst 0x28]
	.dwattr $C$DW$500, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$500, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$500, DW_AT_decl_line(0x463)
	.dwattr $C$DW$500, DW_AT_decl_column(0x0c)

$C$DW$501	.dwtag  DW_TAG_member
	.dwattr $C$DW$501, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$501, DW_AT_name("shareLocation")
	.dwattr $C$DW$501, DW_AT_data_member_location[DW_OP_plus_uconst 0x2c]
	.dwattr $C$DW$501, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$501, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$501, DW_AT_decl_line(0x466)
	.dwattr $C$DW$501, DW_AT_decl_column(0x0c)

$C$DW$502	.dwtag  DW_TAG_member
	.dwattr $C$DW$502, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$502, DW_AT_name("varianceEncoded")
	.dwattr $C$DW$502, DW_AT_data_member_location[DW_OP_plus_uconst 0x30]
	.dwattr $C$DW$502, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$502, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$502, DW_AT_decl_line(0x469)
	.dwattr $C$DW$502, DW_AT_decl_column(0x0c)

$C$DW$503	.dwtag  DW_TAG_member
	.dwattr $C$DW$503, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$503, DW_AT_name("numKeypoints")
	.dwattr $C$DW$503, DW_AT_data_member_location[DW_OP_plus_uconst 0x34]
	.dwattr $C$DW$503, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$503, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$503, DW_AT_decl_line(0x46b)
	.dwattr $C$DW$503, DW_AT_decl_column(0x0c)

$C$DW$504	.dwtag  DW_TAG_member
	.dwattr $C$DW$504, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$504, DW_AT_name("numHeads")
	.dwattr $C$DW$504, DW_AT_data_member_location[DW_OP_plus_uconst 0x38]
	.dwattr $C$DW$504, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$504, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$504, DW_AT_decl_line(0x46e)
	.dwattr $C$DW$504, DW_AT_decl_column(0x0c)

$C$DW$505	.dwtag  DW_TAG_member
	.dwattr $C$DW$505, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$505, DW_AT_name("imWidth")
	.dwattr $C$DW$505, DW_AT_data_member_location[DW_OP_plus_uconst 0x3c]
	.dwattr $C$DW$505, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$505, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$505, DW_AT_decl_line(0x471)
	.dwattr $C$DW$505, DW_AT_decl_column(0x0b)

$C$DW$506	.dwtag  DW_TAG_member
	.dwattr $C$DW$506, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$506, DW_AT_name("imHeight")
	.dwattr $C$DW$506, DW_AT_data_member_location[DW_OP_plus_uconst 0x40]
	.dwattr $C$DW$506, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$506, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$506, DW_AT_decl_line(0x474)
	.dwattr $C$DW$506, DW_AT_decl_column(0x0b)

$C$DW$507	.dwtag  DW_TAG_member
	.dwattr $C$DW$507, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$507, DW_AT_name("scoreConverter")
	.dwattr $C$DW$507, DW_AT_data_member_location[DW_OP_plus_uconst 0x44]
	.dwattr $C$DW$507, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$507, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$507, DW_AT_decl_line(0x477)
	.dwattr $C$DW$507, DW_AT_decl_column(0x0b)

$C$DW$508	.dwtag  DW_TAG_member
	.dwattr $C$DW$508, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$508, DW_AT_name("metaArchType")
	.dwattr $C$DW$508, DW_AT_data_member_location[DW_OP_plus_uconst 0x48]
	.dwattr $C$DW$508, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$508, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$508, DW_AT_decl_line(0x47a)
	.dwattr $C$DW$508, DW_AT_decl_column(0x0b)

$C$DW$509	.dwtag  DW_TAG_member
	.dwattr $C$DW$509, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$509, DW_AT_name("dataLayout")
	.dwattr $C$DW$509, DW_AT_data_member_location[DW_OP_plus_uconst 0x4c]
	.dwattr $C$DW$509, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$509, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$509, DW_AT_decl_line(0x47e)
	.dwattr $C$DW$509, DW_AT_decl_column(0x0b)


$C$DW$510	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$510, DW_AT_name("operator =")
	.dwattr $C$DW$510, DW_AT_declaration
	.dwattr $C$DW$510, DW_AT_linkage_name("_ZN26sTIDL_DetectOutputParams_taSERKS_")
	.dwattr $C$DW$510, DW_AT_type(*$C$DW$T$175)
	.dwattr $C$DW$510, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$511	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$511, DW_AT_type(*$C$DW$T$177)

	.dwendtag $C$DW$510


$C$DW$512	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$512, DW_AT_name("operator =")
	.dwattr $C$DW$512, DW_AT_declaration
	.dwattr $C$DW$512, DW_AT_linkage_name("_ZN26sTIDL_DetectOutputParams_taSEOS_")
	.dwattr $C$DW$512, DW_AT_type(*$C$DW$T$175)
	.dwattr $C$DW$512, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$513	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$513, DW_AT_type(*$C$DW$T$175)

	.dwendtag $C$DW$512

	.dwattr $C$DW$T$180, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$180, DW_AT_decl_line(0x44b)
	.dwattr $C$DW$T$180, DW_AT_decl_column(0x10)
	.dwendtag $C$DW$T$180

	.dwendtag $C$DW$TU$180


$C$DW$TU$175	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$175
$C$DW$T$175	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$175, DW_AT_type(*$C$DW$T$180)
	.dwattr $C$DW$T$175, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$175


$C$DW$TU$178	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$178

$C$DW$T$178	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$178, DW_AT_type(*$C$DW$T$175)
$C$DW$514	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$514, DW_AT_type(*$C$DW$T$177)

	.dwendtag $C$DW$T$178

	.dwendtag $C$DW$TU$178


$C$DW$TU$179	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$179

$C$DW$T$179	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$179, DW_AT_type(*$C$DW$T$175)
$C$DW$515	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$515, DW_AT_type(*$C$DW$T$175)

	.dwendtag $C$DW$T$179

	.dwendtag $C$DW$TU$179


$C$DW$TU$176	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$176
$C$DW$T$176	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$176, DW_AT_type(*$C$DW$T$180)

	.dwendtag $C$DW$TU$176


$C$DW$TU$177	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$177
$C$DW$T$177	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$177, DW_AT_type(*$C$DW$T$176)
	.dwattr $C$DW$T$177, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$177


$C$DW$TU$206	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$206
$C$DW$T$206	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$206, DW_AT_name("sTIDL_DetectOutputParams_t")
	.dwattr $C$DW$T$206, DW_AT_type(*$C$DW$T$180)
	.dwattr $C$DW$T$206, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$206, DW_AT_decl_line(0x480)
	.dwattr $C$DW$T$206, DW_AT_decl_column(0x02)

	.dwendtag $C$DW$TU$206


$C$DW$TU$188	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$188

$C$DW$T$188	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$188, DW_AT_name("sTIDL_EltWiseParams_t")
	.dwattr $C$DW$T$188, DW_AT_byte_size(0x58)
$C$DW$516	.dwtag  DW_TAG_member
	.dwattr $C$DW$516, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$516, DW_AT_name("bias")
	.dwattr $C$DW$516, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$516, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$516, DW_AT_decl_line(0x511)
	.dwattr $C$DW$516, DW_AT_decl_column(0x0b)

$C$DW$517	.dwtag  DW_TAG_member
	.dwattr $C$DW$517, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$517, DW_AT_name("numChannels")
	.dwattr $C$DW$517, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$517, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$517, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$517, DW_AT_decl_line(0x513)
	.dwattr $C$DW$517, DW_AT_decl_column(0x0b)

$C$DW$518	.dwtag  DW_TAG_member
	.dwattr $C$DW$518, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$518, DW_AT_name("eltWiseType")
	.dwattr $C$DW$518, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$518, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$518, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$518, DW_AT_decl_line(0x515)
	.dwattr $C$DW$518, DW_AT_decl_column(0x0b)

$C$DW$519	.dwtag  DW_TAG_member
	.dwattr $C$DW$519, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$519, DW_AT_name("numInData")
	.dwattr $C$DW$519, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$519, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$519, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$519, DW_AT_decl_line(0x517)
	.dwattr $C$DW$519, DW_AT_decl_column(0x0b)

$C$DW$520	.dwtag  DW_TAG_member
	.dwattr $C$DW$520, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$520, DW_AT_name("biasQ")
	.dwattr $C$DW$520, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$520, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$520, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$520, DW_AT_decl_line(0x519)
	.dwattr $C$DW$520, DW_AT_decl_column(0x0b)

$C$DW$521	.dwtag  DW_TAG_member
	.dwattr $C$DW$521, DW_AT_type(*$C$DW$T$182)
	.dwattr $C$DW$521, DW_AT_name("inDataQ")
	.dwattr $C$DW$521, DW_AT_data_member_location[DW_OP_plus_uconst 0x14]
	.dwattr $C$DW$521, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$521, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$521, DW_AT_decl_line(0x51b)
	.dwattr $C$DW$521, DW_AT_decl_column(0x0b)

$C$DW$522	.dwtag  DW_TAG_member
	.dwattr $C$DW$522, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$522, DW_AT_name("outDataQ")
	.dwattr $C$DW$522, DW_AT_data_member_location[DW_OP_plus_uconst 0x54]
	.dwattr $C$DW$522, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$522, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$522, DW_AT_decl_line(0x51d)
	.dwattr $C$DW$522, DW_AT_decl_column(0x0b)


$C$DW$523	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$523, DW_AT_name("operator =")
	.dwattr $C$DW$523, DW_AT_declaration
	.dwattr $C$DW$523, DW_AT_linkage_name("_ZN21sTIDL_EltWiseParams_taSERKS_")
	.dwattr $C$DW$523, DW_AT_type(*$C$DW$T$183)
	.dwattr $C$DW$523, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$524	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$524, DW_AT_type(*$C$DW$T$185)

	.dwendtag $C$DW$523


$C$DW$525	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$525, DW_AT_name("operator =")
	.dwattr $C$DW$525, DW_AT_declaration
	.dwattr $C$DW$525, DW_AT_linkage_name("_ZN21sTIDL_EltWiseParams_taSEOS_")
	.dwattr $C$DW$525, DW_AT_type(*$C$DW$T$183)
	.dwattr $C$DW$525, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$526	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$526, DW_AT_type(*$C$DW$T$183)

	.dwendtag $C$DW$525

	.dwattr $C$DW$T$188, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$188, DW_AT_decl_line(0x50f)
	.dwattr $C$DW$T$188, DW_AT_decl_column(0x10)
	.dwendtag $C$DW$T$188

	.dwendtag $C$DW$TU$188


$C$DW$TU$183	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$183
$C$DW$T$183	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$183, DW_AT_type(*$C$DW$T$188)
	.dwattr $C$DW$T$183, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$183


$C$DW$TU$186	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$186

$C$DW$T$186	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$186, DW_AT_type(*$C$DW$T$183)
$C$DW$527	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$527, DW_AT_type(*$C$DW$T$185)

	.dwendtag $C$DW$T$186

	.dwendtag $C$DW$TU$186


$C$DW$TU$187	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$187

$C$DW$T$187	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$187, DW_AT_type(*$C$DW$T$183)
$C$DW$528	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$528, DW_AT_type(*$C$DW$T$183)

	.dwendtag $C$DW$T$187

	.dwendtag $C$DW$TU$187


$C$DW$TU$184	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$184
$C$DW$T$184	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$184, DW_AT_type(*$C$DW$T$188)

	.dwendtag $C$DW$TU$184


$C$DW$TU$185	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$185
$C$DW$T$185	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$185, DW_AT_type(*$C$DW$T$184)
	.dwattr $C$DW$T$185, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$185


$C$DW$TU$199	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$199
$C$DW$T$199	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$199, DW_AT_name("sTIDL_EltWiseParams_t")
	.dwattr $C$DW$T$199, DW_AT_type(*$C$DW$T$188)
	.dwattr $C$DW$T$199, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$199, DW_AT_decl_line(0x51e)
	.dwattr $C$DW$T$199, DW_AT_decl_column(0x02)

	.dwendtag $C$DW$TU$199


$C$DW$TU$196	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$196

$C$DW$T$196	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$196, DW_AT_name("sTIDL_InnerProductParams_t")
	.dwattr $C$DW$T$196, DW_AT_byte_size(0x38)
$C$DW$529	.dwtag  DW_TAG_member
	.dwattr $C$DW$529, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$529, DW_AT_name("weights")
	.dwattr $C$DW$529, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$529, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$529, DW_AT_decl_line(0x4ed)
	.dwattr $C$DW$529, DW_AT_decl_column(0x0d)

$C$DW$530	.dwtag  DW_TAG_member
	.dwattr $C$DW$530, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$530, DW_AT_name("bias")
	.dwattr $C$DW$530, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$530, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$530, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$530, DW_AT_decl_line(0x4ef)
	.dwattr $C$DW$530, DW_AT_decl_column(0x0d)

$C$DW$531	.dwtag  DW_TAG_member
	.dwattr $C$DW$531, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$531, DW_AT_name("activationType")
	.dwattr $C$DW$531, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$531, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$531, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$531, DW_AT_decl_line(0x4f1)
	.dwattr $C$DW$531, DW_AT_decl_column(0x0d)

$C$DW$532	.dwtag  DW_TAG_member
	.dwattr $C$DW$532, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$532, DW_AT_name("numInNodes")
	.dwattr $C$DW$532, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$532, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$532, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$532, DW_AT_decl_line(0x4f3)
	.dwattr $C$DW$532, DW_AT_decl_column(0x0d)

$C$DW$533	.dwtag  DW_TAG_member
	.dwattr $C$DW$533, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$533, DW_AT_name("numOutNodes")
	.dwattr $C$DW$533, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$533, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$533, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$533, DW_AT_decl_line(0x4f5)
	.dwattr $C$DW$533, DW_AT_decl_column(0x0d)

$C$DW$534	.dwtag  DW_TAG_member
	.dwattr $C$DW$534, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$534, DW_AT_name("weightsQ")
	.dwattr $C$DW$534, DW_AT_data_member_location[DW_OP_plus_uconst 0x14]
	.dwattr $C$DW$534, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$534, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$534, DW_AT_decl_line(0x4f7)
	.dwattr $C$DW$534, DW_AT_decl_column(0x0d)

$C$DW$535	.dwtag  DW_TAG_member
	.dwattr $C$DW$535, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$535, DW_AT_name("weightScale")
	.dwattr $C$DW$535, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$535, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$535, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$535, DW_AT_decl_line(0x4f9)
	.dwattr $C$DW$535, DW_AT_decl_column(0x14)

$C$DW$536	.dwtag  DW_TAG_member
	.dwattr $C$DW$536, DW_AT_type(*$C$DW$T$90)
	.dwattr $C$DW$536, DW_AT_name("biasScale")
	.dwattr $C$DW$536, DW_AT_data_member_location[DW_OP_plus_uconst 0x1c]
	.dwattr $C$DW$536, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$536, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$536, DW_AT_decl_line(0x4fb)
	.dwattr $C$DW$536, DW_AT_decl_column(0x14)

$C$DW$537	.dwtag  DW_TAG_member
	.dwattr $C$DW$537, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$537, DW_AT_name("zeroWeightValue")
	.dwattr $C$DW$537, DW_AT_data_member_location[DW_OP_plus_uconst 0x20]
	.dwattr $C$DW$537, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$537, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$537, DW_AT_decl_line(0x4fd)
	.dwattr $C$DW$537, DW_AT_decl_column(0x0d)

$C$DW$538	.dwtag  DW_TAG_member
	.dwattr $C$DW$538, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$538, DW_AT_name("biasQ")
	.dwattr $C$DW$538, DW_AT_data_member_location[DW_OP_plus_uconst 0x24]
	.dwattr $C$DW$538, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$538, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$538, DW_AT_decl_line(0x4ff)
	.dwattr $C$DW$538, DW_AT_decl_column(0x0d)

$C$DW$539	.dwtag  DW_TAG_member
	.dwattr $C$DW$539, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$539, DW_AT_name("inDataQ")
	.dwattr $C$DW$539, DW_AT_data_member_location[DW_OP_plus_uconst 0x28]
	.dwattr $C$DW$539, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$539, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$539, DW_AT_decl_line(0x501)
	.dwattr $C$DW$539, DW_AT_decl_column(0x0d)

$C$DW$540	.dwtag  DW_TAG_member
	.dwattr $C$DW$540, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$540, DW_AT_name("outDataQ")
	.dwattr $C$DW$540, DW_AT_data_member_location[DW_OP_plus_uconst 0x2c]
	.dwattr $C$DW$540, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$540, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$540, DW_AT_decl_line(0x503)
	.dwattr $C$DW$540, DW_AT_decl_column(0x0d)

$C$DW$541	.dwtag  DW_TAG_member
	.dwattr $C$DW$541, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$541, DW_AT_name("interDataQ")
	.dwattr $C$DW$541, DW_AT_data_member_location[DW_OP_plus_uconst 0x30]
	.dwattr $C$DW$541, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$541, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$541, DW_AT_decl_line(0x505)
	.dwattr $C$DW$541, DW_AT_decl_column(0x0d)

$C$DW$542	.dwtag  DW_TAG_member
	.dwattr $C$DW$542, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$542, DW_AT_name("biasB")
	.dwattr $C$DW$542, DW_AT_data_member_location[DW_OP_plus_uconst 0x34]
	.dwattr $C$DW$542, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$542, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$542, DW_AT_decl_line(0x507)
	.dwattr $C$DW$542, DW_AT_decl_column(0x0d)


$C$DW$543	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$543, DW_AT_name("operator =")
	.dwattr $C$DW$543, DW_AT_declaration
	.dwattr $C$DW$543, DW_AT_linkage_name("_ZN26sTIDL_InnerProductParams_taSERKS_")
	.dwattr $C$DW$543, DW_AT_type(*$C$DW$T$191)
	.dwattr $C$DW$543, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$544	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$544, DW_AT_type(*$C$DW$T$193)

	.dwendtag $C$DW$543


$C$DW$545	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$545, DW_AT_name("operator =")
	.dwattr $C$DW$545, DW_AT_declaration
	.dwattr $C$DW$545, DW_AT_linkage_name("_ZN26sTIDL_InnerProductParams_taSEOS_")
	.dwattr $C$DW$545, DW_AT_type(*$C$DW$T$191)
	.dwattr $C$DW$545, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$546	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$546, DW_AT_type(*$C$DW$T$191)

	.dwendtag $C$DW$545

	.dwattr $C$DW$T$196, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$196, DW_AT_decl_line(0x4eb)
	.dwattr $C$DW$T$196, DW_AT_decl_column(0x10)
	.dwendtag $C$DW$T$196

	.dwendtag $C$DW$TU$196


$C$DW$TU$191	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$191
$C$DW$T$191	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$191, DW_AT_type(*$C$DW$T$196)
	.dwattr $C$DW$T$191, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$191


$C$DW$TU$194	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$194

$C$DW$T$194	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$194, DW_AT_type(*$C$DW$T$191)
$C$DW$547	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$547, DW_AT_type(*$C$DW$T$193)

	.dwendtag $C$DW$T$194

	.dwendtag $C$DW$TU$194


$C$DW$TU$195	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$195

$C$DW$T$195	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$195, DW_AT_type(*$C$DW$T$191)
$C$DW$548	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$548, DW_AT_type(*$C$DW$T$191)

	.dwendtag $C$DW$T$195

	.dwendtag $C$DW$TU$195


$C$DW$TU$192	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$192
$C$DW$T$192	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$192, DW_AT_type(*$C$DW$T$196)

	.dwendtag $C$DW$TU$192


$C$DW$TU$193	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$193
$C$DW$T$193	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$193, DW_AT_type(*$C$DW$T$192)
	.dwattr $C$DW$T$193, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$193


$C$DW$TU$200	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$200
$C$DW$T$200	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$200, DW_AT_name("sTIDL_InnerProductParams_t")
	.dwattr $C$DW$T$200, DW_AT_type(*$C$DW$T$196)
	.dwattr $C$DW$T$200, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$200, DW_AT_decl_line(0x508)
	.dwattr $C$DW$T$200, DW_AT_decl_column(0x02)

	.dwendtag $C$DW$TU$200


$C$DW$TU$223	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$223

$C$DW$T$223	.dwtag  DW_TAG_union_type
	.dwattr $C$DW$T$223, DW_AT_name("sTIDL_LayerParams_t")
	.dwattr $C$DW$T$223, DW_AT_byte_size(0xa8)
$C$DW$549	.dwtag  DW_TAG_member
	.dwattr $C$DW$549, DW_AT_type(*$C$DW$T$198)
	.dwattr $C$DW$549, DW_AT_name("convParams")
	.dwattr $C$DW$549, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$549, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$549, DW_AT_decl_line(0x554)
	.dwattr $C$DW$549, DW_AT_decl_column(0x29)

$C$DW$550	.dwtag  DW_TAG_member
	.dwattr $C$DW$550, DW_AT_type(*$C$DW$T$199)
	.dwattr $C$DW$550, DW_AT_name("eltWiseParams")
	.dwattr $C$DW$550, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$550, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$550, DW_AT_decl_line(0x555)
	.dwattr $C$DW$550, DW_AT_decl_column(0x29)

$C$DW$551	.dwtag  DW_TAG_member
	.dwattr $C$DW$551, DW_AT_type(*$C$DW$T$127)
	.dwattr $C$DW$551, DW_AT_name("poolParams")
	.dwattr $C$DW$551, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$551, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$551, DW_AT_decl_line(0x556)
	.dwattr $C$DW$551, DW_AT_decl_column(0x29)

$C$DW$552	.dwtag  DW_TAG_member
	.dwattr $C$DW$552, DW_AT_type(*$C$DW$T$200)
	.dwattr $C$DW$552, DW_AT_name("innerProductParams")
	.dwattr $C$DW$552, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$552, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$552, DW_AT_decl_line(0x557)
	.dwattr $C$DW$552, DW_AT_decl_column(0x29)

$C$DW$553	.dwtag  DW_TAG_member
	.dwattr $C$DW$553, DW_AT_type(*$C$DW$T$201)
	.dwattr $C$DW$553, DW_AT_name("dataLayerParams")
	.dwattr $C$DW$553, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$553, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$553, DW_AT_decl_line(0x558)
	.dwattr $C$DW$553, DW_AT_decl_column(0x29)

$C$DW$554	.dwtag  DW_TAG_member
	.dwattr $C$DW$554, DW_AT_type(*$C$DW$T$202)
	.dwattr $C$DW$554, DW_AT_name("argMaxParams")
	.dwattr $C$DW$554, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$554, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$554, DW_AT_decl_line(0x559)
	.dwattr $C$DW$554, DW_AT_decl_column(0x29)

$C$DW$555	.dwtag  DW_TAG_member
	.dwattr $C$DW$555, DW_AT_type(*$C$DW$T$203)
	.dwattr $C$DW$555, DW_AT_name("softMaxParams")
	.dwattr $C$DW$555, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$555, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$555, DW_AT_decl_line(0x55a)
	.dwattr $C$DW$555, DW_AT_decl_column(0x29)

$C$DW$556	.dwtag  DW_TAG_member
	.dwattr $C$DW$556, DW_AT_type(*$C$DW$T$204)
	.dwattr $C$DW$556, DW_AT_name("cropParams")
	.dwattr $C$DW$556, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$556, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$556, DW_AT_decl_line(0x55b)
	.dwattr $C$DW$556, DW_AT_decl_column(0x29)

$C$DW$557	.dwtag  DW_TAG_member
	.dwattr $C$DW$557, DW_AT_type(*$C$DW$T$205)
	.dwattr $C$DW$557, DW_AT_name("concatParams")
	.dwattr $C$DW$557, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$557, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$557, DW_AT_decl_line(0x55c)
	.dwattr $C$DW$557, DW_AT_decl_column(0x29)

$C$DW$558	.dwtag  DW_TAG_member
	.dwattr $C$DW$558, DW_AT_type(*$C$DW$T$206)
	.dwattr $C$DW$558, DW_AT_name("detectOutParams")
	.dwattr $C$DW$558, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$558, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$558, DW_AT_decl_line(0x55d)
	.dwattr $C$DW$558, DW_AT_decl_column(0x29)

$C$DW$559	.dwtag  DW_TAG_member
	.dwattr $C$DW$559, DW_AT_type(*$C$DW$T$207)
	.dwattr $C$DW$559, DW_AT_name("biasParams")
	.dwattr $C$DW$559, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$559, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$559, DW_AT_decl_line(0x55e)
	.dwattr $C$DW$559, DW_AT_decl_column(0x29)

$C$DW$560	.dwtag  DW_TAG_member
	.dwattr $C$DW$560, DW_AT_type(*$C$DW$T$208)
	.dwattr $C$DW$560, DW_AT_name("batchNormParams")
	.dwattr $C$DW$560, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$560, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$560, DW_AT_decl_line(0x55f)
	.dwattr $C$DW$560, DW_AT_decl_column(0x29)

$C$DW$561	.dwtag  DW_TAG_member
	.dwattr $C$DW$561, DW_AT_type(*$C$DW$T$209)
	.dwattr $C$DW$561, DW_AT_name("shuffleLayerParams")
	.dwattr $C$DW$561, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$561, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$561, DW_AT_decl_line(0x560)
	.dwattr $C$DW$561, DW_AT_decl_column(0x29)

$C$DW$562	.dwtag  DW_TAG_member
	.dwattr $C$DW$562, DW_AT_type(*$C$DW$T$210)
	.dwattr $C$DW$562, DW_AT_name("sliceParams")
	.dwattr $C$DW$562, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$562, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$562, DW_AT_decl_line(0x561)
	.dwattr $C$DW$562, DW_AT_decl_column(0x29)

$C$DW$563	.dwtag  DW_TAG_member
	.dwattr $C$DW$563, DW_AT_type(*$C$DW$T$211)
	.dwattr $C$DW$563, DW_AT_name("resizeParams")
	.dwattr $C$DW$563, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$563, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$563, DW_AT_decl_line(0x562)
	.dwattr $C$DW$563, DW_AT_decl_column(0x29)

$C$DW$564	.dwtag  DW_TAG_member
	.dwattr $C$DW$564, DW_AT_type(*$C$DW$T$212)
	.dwattr $C$DW$564, DW_AT_name("roiPoolingParams")
	.dwattr $C$DW$564, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$564, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$564, DW_AT_decl_line(0x563)
	.dwattr $C$DW$564, DW_AT_decl_column(0x29)

$C$DW$565	.dwtag  DW_TAG_member
	.dwattr $C$DW$565, DW_AT_type(*$C$DW$T$213)
	.dwattr $C$DW$565, DW_AT_name("depthToSpaceParams")
	.dwattr $C$DW$565, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$565, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$565, DW_AT_decl_line(0x564)
	.dwattr $C$DW$565, DW_AT_decl_column(0x29)

$C$DW$566	.dwtag  DW_TAG_member
	.dwattr $C$DW$566, DW_AT_type(*$C$DW$T$214)
	.dwattr $C$DW$566, DW_AT_name("padLayerParams")
	.dwattr $C$DW$566, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$566, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$566, DW_AT_decl_line(0x565)
	.dwattr $C$DW$566, DW_AT_decl_column(0x29)

$C$DW$567	.dwtag  DW_TAG_member
	.dwattr $C$DW$567, DW_AT_type(*$C$DW$T$215)
	.dwattr $C$DW$567, DW_AT_name("odOutputReformatLayerParams")
	.dwattr $C$DW$567, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$567, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$567, DW_AT_decl_line(0x566)
	.dwattr $C$DW$567, DW_AT_decl_column(0x29)

$C$DW$568	.dwtag  DW_TAG_member
	.dwattr $C$DW$568, DW_AT_type(*$C$DW$T$216)
	.dwattr $C$DW$568, DW_AT_name("dataConvertParams")
	.dwattr $C$DW$568, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$568, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$568, DW_AT_decl_line(0x567)
	.dwattr $C$DW$568, DW_AT_decl_column(0x29)

$C$DW$569	.dwtag  DW_TAG_member
	.dwattr $C$DW$569, DW_AT_type(*$C$DW$T$217)
	.dwattr $C$DW$569, DW_AT_name("customParams")
	.dwattr $C$DW$569, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$569, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$569, DW_AT_decl_line(0x568)
	.dwattr $C$DW$569, DW_AT_decl_column(0x29)


$C$DW$570	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$570, DW_AT_name("operator =")
	.dwattr $C$DW$570, DW_AT_declaration
	.dwattr $C$DW$570, DW_AT_linkage_name("_ZN19sTIDL_LayerParams_taSERKS_")
	.dwattr $C$DW$570, DW_AT_type(*$C$DW$T$218)
	.dwattr $C$DW$570, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$571	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$571, DW_AT_type(*$C$DW$T$220)

	.dwendtag $C$DW$570


$C$DW$572	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$572, DW_AT_name("operator =")
	.dwattr $C$DW$572, DW_AT_declaration
	.dwattr $C$DW$572, DW_AT_linkage_name("_ZN19sTIDL_LayerParams_taSEOS_")
	.dwattr $C$DW$572, DW_AT_type(*$C$DW$T$218)
	.dwattr $C$DW$572, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$573	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$573, DW_AT_type(*$C$DW$T$218)

	.dwendtag $C$DW$572

	.dwattr $C$DW$T$223, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$223, DW_AT_decl_line(0x553)
	.dwattr $C$DW$T$223, DW_AT_decl_column(0x0f)
	.dwendtag $C$DW$T$223

	.dwendtag $C$DW$TU$223


$C$DW$TU$218	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$218
$C$DW$T$218	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$218, DW_AT_type(*$C$DW$T$223)
	.dwattr $C$DW$T$218, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$218


$C$DW$TU$221	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$221

$C$DW$T$221	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$221, DW_AT_type(*$C$DW$T$218)
$C$DW$574	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$574, DW_AT_type(*$C$DW$T$220)

	.dwendtag $C$DW$T$221

	.dwendtag $C$DW$TU$221


$C$DW$TU$222	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$222

$C$DW$T$222	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$222, DW_AT_type(*$C$DW$T$218)
$C$DW$575	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$575, DW_AT_type(*$C$DW$T$218)

	.dwendtag $C$DW$T$222

	.dwendtag $C$DW$TU$222


$C$DW$TU$219	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$219
$C$DW$T$219	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$219, DW_AT_type(*$C$DW$T$223)

	.dwendtag $C$DW$TU$219


$C$DW$TU$220	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$220
$C$DW$T$220	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$220, DW_AT_type(*$C$DW$T$219)
	.dwattr $C$DW$T$220, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$220


$C$DW$TU$245	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$245
$C$DW$T$245	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$245, DW_AT_name("sTIDL_LayerParams_t")
	.dwattr $C$DW$T$245, DW_AT_type(*$C$DW$T$223)
	.dwattr $C$DW$T$245, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$245, DW_AT_decl_line(0x569)
	.dwattr $C$DW$T$245, DW_AT_decl_column(0x02)

	.dwendtag $C$DW$TU$245


$C$DW$TU$254	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$254

$C$DW$T$254	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$254, DW_AT_name("sTIDL_Layer_t")
	.dwattr $C$DW$T$254, DW_AT_byte_size(0xc58)
$C$DW$576	.dwtag  DW_TAG_member
	.dwattr $C$DW$576, DW_AT_type(*$C$DW$T$245)
	.dwattr $C$DW$576, DW_AT_name("layerParams")
	.dwattr $C$DW$576, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$576, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$576, DW_AT_decl_line(0x572)
	.dwattr $C$DW$576, DW_AT_decl_column(0x17)

$C$DW$577	.dwtag  DW_TAG_member
	.dwattr $C$DW$577, DW_AT_type(*$C$DW$T$246)
	.dwattr $C$DW$577, DW_AT_name("actParams")
	.dwattr $C$DW$577, DW_AT_data_member_location[DW_OP_plus_uconst 0xa8]
	.dwattr $C$DW$577, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$577, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$577, DW_AT_decl_line(0x574)
	.dwattr $C$DW$577, DW_AT_decl_column(0x18)

$C$DW$578	.dwtag  DW_TAG_member
	.dwattr $C$DW$578, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$578, DW_AT_name("layerType")
	.dwattr $C$DW$578, DW_AT_data_member_location[DW_OP_plus_uconst 0xbc]
	.dwattr $C$DW$578, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$578, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$578, DW_AT_decl_line(0x576)
	.dwattr $C$DW$578, DW_AT_decl_column(0x0b)

$C$DW$579	.dwtag  DW_TAG_member
	.dwattr $C$DW$579, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$579, DW_AT_name("numInBufs")
	.dwattr $C$DW$579, DW_AT_data_member_location[DW_OP_plus_uconst 0xc0]
	.dwattr $C$DW$579, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$579, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$579, DW_AT_decl_line(0x578)
	.dwattr $C$DW$579, DW_AT_decl_column(0x0b)

$C$DW$580	.dwtag  DW_TAG_member
	.dwattr $C$DW$580, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$580, DW_AT_name("numOutBufs")
	.dwattr $C$DW$580, DW_AT_data_member_location[DW_OP_plus_uconst 0xc4]
	.dwattr $C$DW$580, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$580, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$580, DW_AT_decl_line(0x57a)
	.dwattr $C$DW$580, DW_AT_decl_column(0x0b)

$C$DW$581	.dwtag  DW_TAG_member
	.dwattr $C$DW$581, DW_AT_type(*$C$DW$T$248)
	.dwattr $C$DW$581, DW_AT_name("inData")
	.dwattr $C$DW$581, DW_AT_data_member_location[DW_OP_plus_uconst 0xc8]
	.dwattr $C$DW$581, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$581, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$581, DW_AT_decl_line(0x57c)
	.dwattr $C$DW$581, DW_AT_decl_column(0x16)

$C$DW$582	.dwtag  DW_TAG_member
	.dwattr $C$DW$582, DW_AT_type(*$C$DW$T$248)
	.dwattr $C$DW$582, DW_AT_name("outData")
	.dwattr $C$DW$582, DW_AT_data_member_location[DW_OP_plus_uconst 0x688]
	.dwattr $C$DW$582, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$582, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$582, DW_AT_decl_line(0x57e)
	.dwattr $C$DW$582, DW_AT_decl_column(0x16)

$C$DW$583	.dwtag  DW_TAG_member
	.dwattr $C$DW$583, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$583, DW_AT_name("coreID")
	.dwattr $C$DW$583, DW_AT_data_member_location[DW_OP_plus_uconst 0xc48]
	.dwattr $C$DW$583, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$583, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$583, DW_AT_decl_line(0x580)
	.dwattr $C$DW$583, DW_AT_decl_column(0x0b)

$C$DW$584	.dwtag  DW_TAG_member
	.dwattr $C$DW$584, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$584, DW_AT_name("layersGroupId")
	.dwattr $C$DW$584, DW_AT_data_member_location[DW_OP_plus_uconst 0xc4c]
	.dwattr $C$DW$584, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$584, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$584, DW_AT_decl_line(0x583)
	.dwattr $C$DW$584, DW_AT_decl_column(0x0b)

$C$DW$585	.dwtag  DW_TAG_member
	.dwattr $C$DW$585, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$585, DW_AT_name("weightsElementSizeInBits")
	.dwattr $C$DW$585, DW_AT_data_member_location[DW_OP_plus_uconst 0xc50]
	.dwattr $C$DW$585, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$585, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$585, DW_AT_decl_line(0x585)
	.dwattr $C$DW$585, DW_AT_decl_column(0x0b)

$C$DW$586	.dwtag  DW_TAG_member
	.dwattr $C$DW$586, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$586, DW_AT_name("strideOffsetMethod")
	.dwattr $C$DW$586, DW_AT_data_member_location[DW_OP_plus_uconst 0xc54]
	.dwattr $C$DW$586, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$586, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$586, DW_AT_decl_line(0x587)
	.dwattr $C$DW$586, DW_AT_decl_column(0x0b)


$C$DW$587	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$587, DW_AT_name("operator =")
	.dwattr $C$DW$587, DW_AT_declaration
	.dwattr $C$DW$587, DW_AT_linkage_name("_ZN13sTIDL_Layer_taSERKS_")
	.dwattr $C$DW$587, DW_AT_type(*$C$DW$T$249)
	.dwattr $C$DW$587, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$588	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$588, DW_AT_type(*$C$DW$T$251)

	.dwendtag $C$DW$587


$C$DW$589	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$589, DW_AT_name("operator =")
	.dwattr $C$DW$589, DW_AT_declaration
	.dwattr $C$DW$589, DW_AT_linkage_name("_ZN13sTIDL_Layer_taSEOS_")
	.dwattr $C$DW$589, DW_AT_type(*$C$DW$T$249)
	.dwattr $C$DW$589, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$590	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$590, DW_AT_type(*$C$DW$T$249)

	.dwendtag $C$DW$589

	.dwattr $C$DW$T$254, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$254, DW_AT_decl_line(0x571)
	.dwattr $C$DW$T$254, DW_AT_decl_column(0x10)
	.dwendtag $C$DW$T$254

	.dwendtag $C$DW$TU$254


$C$DW$TU$249	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$249
$C$DW$T$249	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$249, DW_AT_type(*$C$DW$T$254)
	.dwattr $C$DW$T$249, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$249


$C$DW$TU$252	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$252

$C$DW$T$252	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$252, DW_AT_type(*$C$DW$T$249)
$C$DW$591	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$591, DW_AT_type(*$C$DW$T$251)

	.dwendtag $C$DW$T$252

	.dwendtag $C$DW$TU$252


$C$DW$TU$253	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$253

$C$DW$T$253	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$253, DW_AT_type(*$C$DW$T$249)
$C$DW$592	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$592, DW_AT_type(*$C$DW$T$249)

	.dwendtag $C$DW$T$253

	.dwendtag $C$DW$TU$253


$C$DW$TU$250	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$250
$C$DW$T$250	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$250, DW_AT_type(*$C$DW$T$254)

	.dwendtag $C$DW$TU$250


$C$DW$TU$251	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$251
$C$DW$T$251	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$251, DW_AT_type(*$C$DW$T$250)
	.dwattr $C$DW$T$251, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$251


$C$DW$TU$353	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$353
$C$DW$T$353	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$353, DW_AT_name("sTIDL_Layer_t")
	.dwattr $C$DW$T$353, DW_AT_type(*$C$DW$T$254)
	.dwattr $C$DW$T$353, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$353, DW_AT_decl_line(0x588)
	.dwattr $C$DW$T$353, DW_AT_decl_column(0x02)

	.dwendtag $C$DW$TU$353


$C$DW$TU$354	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$354
$C$DW$T$354	.dwtag  DW_TAG_pointer_type
	.dwattr $C$DW$T$354, DW_AT_type(*$C$DW$T$353)
	.dwattr $C$DW$T$354, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$354


$C$DW$TU$265	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$265

$C$DW$T$265	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$265, DW_AT_name("sTIDL_PadLayerParams_t")
	.dwattr $C$DW$T$265, DW_AT_byte_size(0x1c)
$C$DW$593	.dwtag  DW_TAG_member
	.dwattr $C$DW$593, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$593, DW_AT_name("padT")
	.dwattr $C$DW$593, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$593, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$593, DW_AT_decl_line(0x2b9)
	.dwattr $C$DW$593, DW_AT_decl_column(0x0b)

$C$DW$594	.dwtag  DW_TAG_member
	.dwattr $C$DW$594, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$594, DW_AT_name("padB")
	.dwattr $C$DW$594, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$594, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$594, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$594, DW_AT_decl_line(0x2bb)
	.dwattr $C$DW$594, DW_AT_decl_column(0x0b)

$C$DW$595	.dwtag  DW_TAG_member
	.dwattr $C$DW$595, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$595, DW_AT_name("padL")
	.dwattr $C$DW$595, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$595, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$595, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$595, DW_AT_decl_line(0x2bd)
	.dwattr $C$DW$595, DW_AT_decl_column(0x0b)

$C$DW$596	.dwtag  DW_TAG_member
	.dwattr $C$DW$596, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$596, DW_AT_name("padR")
	.dwattr $C$DW$596, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$596, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$596, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$596, DW_AT_decl_line(0x2bf)
	.dwattr $C$DW$596, DW_AT_decl_column(0x0b)

$C$DW$597	.dwtag  DW_TAG_member
	.dwattr $C$DW$597, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$597, DW_AT_name("padConstValue")
	.dwattr $C$DW$597, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$597, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$597, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$597, DW_AT_decl_line(0x2c1)
	.dwattr $C$DW$597, DW_AT_decl_column(0x0d)

$C$DW$598	.dwtag  DW_TAG_member
	.dwattr $C$DW$598, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$598, DW_AT_name("padType")
	.dwattr $C$DW$598, DW_AT_data_member_location[DW_OP_plus_uconst 0x14]
	.dwattr $C$DW$598, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$598, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$598, DW_AT_decl_line(0x2c3)
	.dwattr $C$DW$598, DW_AT_decl_column(0x0d)

$C$DW$599	.dwtag  DW_TAG_member
	.dwattr $C$DW$599, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$599, DW_AT_name("perChannelPadConstTensorOffset")
	.dwattr $C$DW$599, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$599, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$599, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$599, DW_AT_decl_line(0x2c6)
	.dwattr $C$DW$599, DW_AT_decl_column(0x0d)


$C$DW$600	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$600, DW_AT_name("operator =")
	.dwattr $C$DW$600, DW_AT_declaration
	.dwattr $C$DW$600, DW_AT_linkage_name("_ZN22sTIDL_PadLayerParams_taSERKS_")
	.dwattr $C$DW$600, DW_AT_type(*$C$DW$T$260)
	.dwattr $C$DW$600, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$601	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$601, DW_AT_type(*$C$DW$T$262)

	.dwendtag $C$DW$600


$C$DW$602	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$602, DW_AT_name("operator =")
	.dwattr $C$DW$602, DW_AT_declaration
	.dwattr $C$DW$602, DW_AT_linkage_name("_ZN22sTIDL_PadLayerParams_taSEOS_")
	.dwattr $C$DW$602, DW_AT_type(*$C$DW$T$260)
	.dwattr $C$DW$602, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$603	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$603, DW_AT_type(*$C$DW$T$260)

	.dwendtag $C$DW$602

	.dwattr $C$DW$T$265, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$265, DW_AT_decl_line(0x2b7)
	.dwattr $C$DW$T$265, DW_AT_decl_column(0x10)
	.dwendtag $C$DW$T$265

	.dwendtag $C$DW$TU$265


$C$DW$TU$260	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$260
$C$DW$T$260	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$260, DW_AT_type(*$C$DW$T$265)
	.dwattr $C$DW$T$260, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$260


$C$DW$TU$263	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$263

$C$DW$T$263	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$263, DW_AT_type(*$C$DW$T$260)
$C$DW$604	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$604, DW_AT_type(*$C$DW$T$262)

	.dwendtag $C$DW$T$263

	.dwendtag $C$DW$TU$263


$C$DW$TU$264	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$264

$C$DW$T$264	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$264, DW_AT_type(*$C$DW$T$260)
$C$DW$605	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$605, DW_AT_type(*$C$DW$T$260)

	.dwendtag $C$DW$T$264

	.dwendtag $C$DW$TU$264


$C$DW$TU$214	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$214
$C$DW$T$214	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$214, DW_AT_name("sTIDL_PadLayerParams_t")
	.dwattr $C$DW$T$214, DW_AT_type(*$C$DW$T$265)
	.dwattr $C$DW$T$214, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$214, DW_AT_decl_line(0x2c7)
	.dwattr $C$DW$T$214, DW_AT_decl_column(0x02)

	.dwendtag $C$DW$TU$214


$C$DW$TU$261	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$261
$C$DW$T$261	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$261, DW_AT_type(*$C$DW$T$265)

	.dwendtag $C$DW$TU$261


$C$DW$TU$262	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$262
$C$DW$T$262	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$262, DW_AT_type(*$C$DW$T$261)
	.dwattr $C$DW$T$262, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$262


$C$DW$TU$272	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$272

$C$DW$T$272	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$272, DW_AT_name("sTIDL_PoolingParams_t")
	.dwattr $C$DW$T$272, DW_AT_byte_size(0x2c)
$C$DW$606	.dwtag  DW_TAG_member
	.dwattr $C$DW$606, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$606, DW_AT_name("numChannels")
	.dwattr $C$DW$606, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$606, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$606, DW_AT_decl_line(0x387)
	.dwattr $C$DW$606, DW_AT_decl_column(0x0d)

$C$DW$607	.dwtag  DW_TAG_member
	.dwattr $C$DW$607, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$607, DW_AT_name("poolingType")
	.dwattr $C$DW$607, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$607, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$607, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$607, DW_AT_decl_line(0x389)
	.dwattr $C$DW$607, DW_AT_decl_column(0x0d)

$C$DW$608	.dwtag  DW_TAG_member
	.dwattr $C$DW$608, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$608, DW_AT_name("kernelW")
	.dwattr $C$DW$608, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$608, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$608, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$608, DW_AT_decl_line(0x38b)
	.dwattr $C$DW$608, DW_AT_decl_column(0x0d)

$C$DW$609	.dwtag  DW_TAG_member
	.dwattr $C$DW$609, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$609, DW_AT_name("kernelH")
	.dwattr $C$DW$609, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$609, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$609, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$609, DW_AT_decl_line(0x38d)
	.dwattr $C$DW$609, DW_AT_decl_column(0x0d)

$C$DW$610	.dwtag  DW_TAG_member
	.dwattr $C$DW$610, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$610, DW_AT_name("strideW")
	.dwattr $C$DW$610, DW_AT_data_member_location[DW_OP_plus_uconst 0x10]
	.dwattr $C$DW$610, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$610, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$610, DW_AT_decl_line(0x38f)
	.dwattr $C$DW$610, DW_AT_decl_column(0x0d)

$C$DW$611	.dwtag  DW_TAG_member
	.dwattr $C$DW$611, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$611, DW_AT_name("strideH")
	.dwattr $C$DW$611, DW_AT_data_member_location[DW_OP_plus_uconst 0x14]
	.dwattr $C$DW$611, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$611, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$611, DW_AT_decl_line(0x391)
	.dwattr $C$DW$611, DW_AT_decl_column(0x0d)

$C$DW$612	.dwtag  DW_TAG_member
	.dwattr $C$DW$612, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$612, DW_AT_name("padW")
	.dwattr $C$DW$612, DW_AT_data_member_location[DW_OP_plus_uconst 0x18]
	.dwattr $C$DW$612, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$612, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$612, DW_AT_decl_line(0x393)
	.dwattr $C$DW$612, DW_AT_decl_column(0x0d)

$C$DW$613	.dwtag  DW_TAG_member
	.dwattr $C$DW$613, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$613, DW_AT_name("padH")
	.dwattr $C$DW$613, DW_AT_data_member_location[DW_OP_plus_uconst 0x1c]
	.dwattr $C$DW$613, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$613, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$613, DW_AT_decl_line(0x395)
	.dwattr $C$DW$613, DW_AT_decl_column(0x0d)

$C$DW$614	.dwtag  DW_TAG_member
	.dwattr $C$DW$614, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$614, DW_AT_name("inDataQ")
	.dwattr $C$DW$614, DW_AT_data_member_location[DW_OP_plus_uconst 0x20]
	.dwattr $C$DW$614, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$614, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$614, DW_AT_decl_line(0x397)
	.dwattr $C$DW$614, DW_AT_decl_column(0x0d)

$C$DW$615	.dwtag  DW_TAG_member
	.dwattr $C$DW$615, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$615, DW_AT_name("outDataQ")
	.dwattr $C$DW$615, DW_AT_data_member_location[DW_OP_plus_uconst 0x24]
	.dwattr $C$DW$615, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$615, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$615, DW_AT_decl_line(0x399)
	.dwattr $C$DW$615, DW_AT_decl_column(0x0d)

$C$DW$616	.dwtag  DW_TAG_member
	.dwattr $C$DW$616, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$616, DW_AT_name("useCeil")
	.dwattr $C$DW$616, DW_AT_data_member_location[DW_OP_plus_uconst 0x28]
	.dwattr $C$DW$616, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$616, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$616, DW_AT_decl_line(0x39b)
	.dwattr $C$DW$616, DW_AT_decl_column(0x0d)


$C$DW$617	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$617, DW_AT_name("operator =")
	.dwattr $C$DW$617, DW_AT_declaration
	.dwattr $C$DW$617, DW_AT_linkage_name("_ZN21sTIDL_PoolingParams_taSERKS_")
	.dwattr $C$DW$617, DW_AT_type(*$C$DW$T$267)
	.dwattr $C$DW$617, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$618	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$618, DW_AT_type(*$C$DW$T$269)

	.dwendtag $C$DW$617


$C$DW$619	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$619, DW_AT_name("operator =")
	.dwattr $C$DW$619, DW_AT_declaration
	.dwattr $C$DW$619, DW_AT_linkage_name("_ZN21sTIDL_PoolingParams_taSEOS_")
	.dwattr $C$DW$619, DW_AT_type(*$C$DW$T$267)
	.dwattr $C$DW$619, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$620	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$620, DW_AT_type(*$C$DW$T$267)

	.dwendtag $C$DW$619

	.dwattr $C$DW$T$272, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$272, DW_AT_decl_line(0x385)
	.dwattr $C$DW$T$272, DW_AT_decl_column(0x10)
	.dwendtag $C$DW$T$272

	.dwendtag $C$DW$TU$272


$C$DW$TU$267	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$267
$C$DW$T$267	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$267, DW_AT_type(*$C$DW$T$272)
	.dwattr $C$DW$T$267, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$267


$C$DW$TU$270	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$270

$C$DW$T$270	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$270, DW_AT_type(*$C$DW$T$267)
$C$DW$621	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$621, DW_AT_type(*$C$DW$T$269)

	.dwendtag $C$DW$T$270

	.dwendtag $C$DW$TU$270


$C$DW$TU$271	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$271

$C$DW$T$271	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$271, DW_AT_type(*$C$DW$T$267)
$C$DW$622	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$622, DW_AT_type(*$C$DW$T$267)

	.dwendtag $C$DW$T$271

	.dwendtag $C$DW$TU$271


$C$DW$TU$127	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$127
$C$DW$T$127	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$127, DW_AT_name("sTIDL_PoolingParams_t")
	.dwattr $C$DW$T$127, DW_AT_type(*$C$DW$T$272)
	.dwattr $C$DW$T$127, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$127, DW_AT_decl_line(0x39c)
	.dwattr $C$DW$T$127, DW_AT_decl_column(0x02)

	.dwendtag $C$DW$TU$127


$C$DW$TU$268	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$268
$C$DW$T$268	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$268, DW_AT_type(*$C$DW$T$272)

	.dwendtag $C$DW$TU$268


$C$DW$TU$269	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$269
$C$DW$T$269	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$269, DW_AT_type(*$C$DW$T$268)
	.dwattr $C$DW$T$269, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$269


$C$DW$TU$280	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$280

$C$DW$T$280	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$280, DW_AT_name("sTIDL_ResizeLayerParams_t")
	.dwattr $C$DW$T$280, DW_AT_byte_size(0x14)
$C$DW$623	.dwtag  DW_TAG_member
	.dwattr $C$DW$623, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$623, DW_AT_name("mode")
	.dwattr $C$DW$623, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$623, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$623, DW_AT_decl_line(0x2e8)
	.dwattr $C$DW$623, DW_AT_decl_column(0x0d)

$C$DW$624	.dwtag  DW_TAG_member
	.dwattr $C$DW$624, DW_AT_type(*$C$DW$T$274)
	.dwattr $C$DW$624, DW_AT_name("resizeRatio")
	.dwattr $C$DW$624, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$624, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$624, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$624, DW_AT_decl_line(0x2ea)
	.dwattr $C$DW$624, DW_AT_decl_column(0x14)


$C$DW$625	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$625, DW_AT_name("operator =")
	.dwattr $C$DW$625, DW_AT_declaration
	.dwattr $C$DW$625, DW_AT_linkage_name("_ZN25sTIDL_ResizeLayerParams_taSERKS_")
	.dwattr $C$DW$625, DW_AT_type(*$C$DW$T$275)
	.dwattr $C$DW$625, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$626	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$626, DW_AT_type(*$C$DW$T$277)

	.dwendtag $C$DW$625


$C$DW$627	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$627, DW_AT_name("operator =")
	.dwattr $C$DW$627, DW_AT_declaration
	.dwattr $C$DW$627, DW_AT_linkage_name("_ZN25sTIDL_ResizeLayerParams_taSEOS_")
	.dwattr $C$DW$627, DW_AT_type(*$C$DW$T$275)
	.dwattr $C$DW$627, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$628	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$628, DW_AT_type(*$C$DW$T$275)

	.dwendtag $C$DW$627

	.dwattr $C$DW$T$280, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$280, DW_AT_decl_line(0x2e6)
	.dwattr $C$DW$T$280, DW_AT_decl_column(0x10)
	.dwendtag $C$DW$T$280

	.dwendtag $C$DW$TU$280


$C$DW$TU$275	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$275
$C$DW$T$275	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$275, DW_AT_type(*$C$DW$T$280)
	.dwattr $C$DW$T$275, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$275


$C$DW$TU$278	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$278

$C$DW$T$278	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$278, DW_AT_type(*$C$DW$T$275)
$C$DW$629	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$629, DW_AT_type(*$C$DW$T$277)

	.dwendtag $C$DW$T$278

	.dwendtag $C$DW$TU$278


$C$DW$TU$279	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$279

$C$DW$T$279	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$279, DW_AT_type(*$C$DW$T$275)
$C$DW$630	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$630, DW_AT_type(*$C$DW$T$275)

	.dwendtag $C$DW$T$279

	.dwendtag $C$DW$TU$279


$C$DW$TU$211	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$211
$C$DW$T$211	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$211, DW_AT_name("sTIDL_ResizeLayerParams_t")
	.dwattr $C$DW$T$211, DW_AT_type(*$C$DW$T$280)
	.dwattr $C$DW$T$211, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$211, DW_AT_decl_line(0x2eb)
	.dwattr $C$DW$T$211, DW_AT_decl_column(0x02)

	.dwendtag $C$DW$TU$211


$C$DW$TU$276	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$276
$C$DW$T$276	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$276, DW_AT_type(*$C$DW$T$280)

	.dwendtag $C$DW$TU$276


$C$DW$TU$277	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$277
$C$DW$T$277	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$277, DW_AT_type(*$C$DW$T$276)
	.dwattr $C$DW$T$277, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$277


$C$DW$TU$288	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$288

$C$DW$T$288	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$288, DW_AT_name("sTIDL_RoiPoolingLayerParams_t")
	.dwattr $C$DW$T$288, DW_AT_byte_size(0x0c)
$C$DW$631	.dwtag  DW_TAG_member
	.dwattr $C$DW$631, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$631, DW_AT_name("poolingType")
	.dwattr $C$DW$631, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$631, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$631, DW_AT_decl_line(0x31f)
	.dwattr $C$DW$631, DW_AT_decl_column(0x0d)

$C$DW$632	.dwtag  DW_TAG_member
	.dwattr $C$DW$632, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$632, DW_AT_name("imWidth")
	.dwattr $C$DW$632, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$632, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$632, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$632, DW_AT_decl_line(0x321)
	.dwattr $C$DW$632, DW_AT_decl_column(0x0b)

$C$DW$633	.dwtag  DW_TAG_member
	.dwattr $C$DW$633, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$633, DW_AT_name("imHeight")
	.dwattr $C$DW$633, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$633, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$633, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$633, DW_AT_decl_line(0x323)
	.dwattr $C$DW$633, DW_AT_decl_column(0x0b)


$C$DW$634	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$634, DW_AT_name("operator =")
	.dwattr $C$DW$634, DW_AT_declaration
	.dwattr $C$DW$634, DW_AT_linkage_name("_ZN29sTIDL_RoiPoolingLayerParams_taSERKS_")
	.dwattr $C$DW$634, DW_AT_type(*$C$DW$T$283)
	.dwattr $C$DW$634, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$635	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$635, DW_AT_type(*$C$DW$T$285)

	.dwendtag $C$DW$634


$C$DW$636	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$636, DW_AT_name("operator =")
	.dwattr $C$DW$636, DW_AT_declaration
	.dwattr $C$DW$636, DW_AT_linkage_name("_ZN29sTIDL_RoiPoolingLayerParams_taSEOS_")
	.dwattr $C$DW$636, DW_AT_type(*$C$DW$T$283)
	.dwattr $C$DW$636, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$637	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$637, DW_AT_type(*$C$DW$T$283)

	.dwendtag $C$DW$636

	.dwattr $C$DW$T$288, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$288, DW_AT_decl_line(0x31d)
	.dwattr $C$DW$T$288, DW_AT_decl_column(0x10)
	.dwendtag $C$DW$T$288

	.dwendtag $C$DW$TU$288


$C$DW$TU$283	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$283
$C$DW$T$283	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$283, DW_AT_type(*$C$DW$T$288)
	.dwattr $C$DW$T$283, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$283


$C$DW$TU$286	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$286

$C$DW$T$286	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$286, DW_AT_type(*$C$DW$T$283)
$C$DW$638	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$638, DW_AT_type(*$C$DW$T$285)

	.dwendtag $C$DW$T$286

	.dwendtag $C$DW$TU$286


$C$DW$TU$287	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$287

$C$DW$T$287	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$287, DW_AT_type(*$C$DW$T$283)
$C$DW$639	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$639, DW_AT_type(*$C$DW$T$283)

	.dwendtag $C$DW$T$287

	.dwendtag $C$DW$TU$287


$C$DW$TU$212	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$212
$C$DW$T$212	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$212, DW_AT_name("sTIDL_RoiPoolingLayerParams_t")
	.dwattr $C$DW$T$212, DW_AT_type(*$C$DW$T$288)
	.dwattr $C$DW$T$212, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$212, DW_AT_decl_line(0x324)
	.dwattr $C$DW$T$212, DW_AT_decl_column(0x02)

	.dwendtag $C$DW$TU$212


$C$DW$TU$284	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$284
$C$DW$T$284	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$284, DW_AT_type(*$C$DW$T$288)

	.dwendtag $C$DW$TU$284


$C$DW$TU$285	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$285
$C$DW$T$285	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$285, DW_AT_type(*$C$DW$T$284)
	.dwattr $C$DW$T$285, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$285


$C$DW$TU$295	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$295

$C$DW$T$295	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$295, DW_AT_name("sTIDL_ShuffleLayerParams_t")
	.dwattr $C$DW$T$295, DW_AT_byte_size(0x08)
$C$DW$640	.dwtag  DW_TAG_member
	.dwattr $C$DW$640, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$640, DW_AT_name("numGroups")
	.dwattr $C$DW$640, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$640, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$640, DW_AT_decl_line(0x2a3)
	.dwattr $C$DW$640, DW_AT_decl_column(0x0d)

$C$DW$641	.dwtag  DW_TAG_member
	.dwattr $C$DW$641, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$641, DW_AT_name("resvd")
	.dwattr $C$DW$641, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$641, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$641, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$641, DW_AT_decl_line(0x2a5)
	.dwattr $C$DW$641, DW_AT_decl_column(0x0d)


$C$DW$642	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$642, DW_AT_name("operator =")
	.dwattr $C$DW$642, DW_AT_declaration
	.dwattr $C$DW$642, DW_AT_linkage_name("_ZN26sTIDL_ShuffleLayerParams_taSERKS_")
	.dwattr $C$DW$642, DW_AT_type(*$C$DW$T$290)
	.dwattr $C$DW$642, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$643	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$643, DW_AT_type(*$C$DW$T$292)

	.dwendtag $C$DW$642


$C$DW$644	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$644, DW_AT_name("operator =")
	.dwattr $C$DW$644, DW_AT_declaration
	.dwattr $C$DW$644, DW_AT_linkage_name("_ZN26sTIDL_ShuffleLayerParams_taSEOS_")
	.dwattr $C$DW$644, DW_AT_type(*$C$DW$T$290)
	.dwattr $C$DW$644, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$645	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$645, DW_AT_type(*$C$DW$T$290)

	.dwendtag $C$DW$644

	.dwattr $C$DW$T$295, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$295, DW_AT_decl_line(0x2a1)
	.dwattr $C$DW$T$295, DW_AT_decl_column(0x10)
	.dwendtag $C$DW$T$295

	.dwendtag $C$DW$TU$295


$C$DW$TU$290	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$290
$C$DW$T$290	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$290, DW_AT_type(*$C$DW$T$295)
	.dwattr $C$DW$T$290, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$290


$C$DW$TU$293	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$293

$C$DW$T$293	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$293, DW_AT_type(*$C$DW$T$290)
$C$DW$646	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$646, DW_AT_type(*$C$DW$T$292)

	.dwendtag $C$DW$T$293

	.dwendtag $C$DW$TU$293


$C$DW$TU$294	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$294

$C$DW$T$294	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$294, DW_AT_type(*$C$DW$T$290)
$C$DW$647	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$647, DW_AT_type(*$C$DW$T$290)

	.dwendtag $C$DW$T$294

	.dwendtag $C$DW$TU$294


$C$DW$TU$209	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$209
$C$DW$T$209	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$209, DW_AT_name("sTIDL_ShuffleLayerParams_t")
	.dwattr $C$DW$T$209, DW_AT_type(*$C$DW$T$295)
	.dwattr $C$DW$T$209, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$209, DW_AT_decl_line(0x2a6)
	.dwattr $C$DW$T$209, DW_AT_decl_column(0x02)

	.dwendtag $C$DW$TU$209


$C$DW$TU$291	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$291
$C$DW$T$291	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$291, DW_AT_type(*$C$DW$T$295)

	.dwendtag $C$DW$TU$291


$C$DW$TU$292	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$292
$C$DW$T$292	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$292, DW_AT_type(*$C$DW$T$291)
	.dwattr $C$DW$T$292, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$292


$C$DW$TU$303	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$303

$C$DW$T$303	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$303, DW_AT_name("sTIDL_SliceLayerParams_t")
	.dwattr $C$DW$T$303, DW_AT_byte_size(0x4c)
$C$DW$648	.dwtag  DW_TAG_member
	.dwattr $C$DW$648, DW_AT_type(*$C$DW$T$297)
	.dwattr $C$DW$648, DW_AT_name("slicePoints")
	.dwattr $C$DW$648, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$648, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$648, DW_AT_decl_line(0x32e)
	.dwattr $C$DW$648, DW_AT_decl_column(0x0d)

$C$DW$649	.dwtag  DW_TAG_member
	.dwattr $C$DW$649, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$649, DW_AT_name("axis")
	.dwattr $C$DW$649, DW_AT_data_member_location[DW_OP_plus_uconst 0x44]
	.dwattr $C$DW$649, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$649, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$649, DW_AT_decl_line(0x330)
	.dwattr $C$DW$649, DW_AT_decl_column(0x0c)

$C$DW$650	.dwtag  DW_TAG_member
	.dwattr $C$DW$650, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$650, DW_AT_name("stride")
	.dwattr $C$DW$650, DW_AT_data_member_location[DW_OP_plus_uconst 0x48]
	.dwattr $C$DW$650, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$650, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$650, DW_AT_decl_line(0x332)
	.dwattr $C$DW$650, DW_AT_decl_column(0x0c)


$C$DW$651	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$651, DW_AT_name("operator =")
	.dwattr $C$DW$651, DW_AT_declaration
	.dwattr $C$DW$651, DW_AT_linkage_name("_ZN24sTIDL_SliceLayerParams_taSERKS_")
	.dwattr $C$DW$651, DW_AT_type(*$C$DW$T$298)
	.dwattr $C$DW$651, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$652	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$652, DW_AT_type(*$C$DW$T$300)

	.dwendtag $C$DW$651


$C$DW$653	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$653, DW_AT_name("operator =")
	.dwattr $C$DW$653, DW_AT_declaration
	.dwattr $C$DW$653, DW_AT_linkage_name("_ZN24sTIDL_SliceLayerParams_taSEOS_")
	.dwattr $C$DW$653, DW_AT_type(*$C$DW$T$298)
	.dwattr $C$DW$653, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$654	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$654, DW_AT_type(*$C$DW$T$298)

	.dwendtag $C$DW$653

	.dwattr $C$DW$T$303, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$303, DW_AT_decl_line(0x32c)
	.dwattr $C$DW$T$303, DW_AT_decl_column(0x10)
	.dwendtag $C$DW$T$303

	.dwendtag $C$DW$TU$303


$C$DW$TU$298	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$298
$C$DW$T$298	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$298, DW_AT_type(*$C$DW$T$303)
	.dwattr $C$DW$T$298, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$298


$C$DW$TU$301	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$301

$C$DW$T$301	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$301, DW_AT_type(*$C$DW$T$298)
$C$DW$655	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$655, DW_AT_type(*$C$DW$T$300)

	.dwendtag $C$DW$T$301

	.dwendtag $C$DW$TU$301


$C$DW$TU$302	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$302

$C$DW$T$302	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$302, DW_AT_type(*$C$DW$T$298)
$C$DW$656	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$656, DW_AT_type(*$C$DW$T$298)

	.dwendtag $C$DW$T$302

	.dwendtag $C$DW$TU$302


$C$DW$TU$210	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$210
$C$DW$T$210	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$210, DW_AT_name("sTIDL_SliceLayerParams_t")
	.dwattr $C$DW$T$210, DW_AT_type(*$C$DW$T$303)
	.dwattr $C$DW$T$210, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$210, DW_AT_decl_line(0x333)
	.dwattr $C$DW$T$210, DW_AT_decl_column(0x02)

	.dwendtag $C$DW$TU$210


$C$DW$TU$299	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$299
$C$DW$T$299	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$299, DW_AT_type(*$C$DW$T$303)

	.dwendtag $C$DW$TU$299


$C$DW$TU$300	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$300
$C$DW$T$300	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$300, DW_AT_type(*$C$DW$T$299)
	.dwattr $C$DW$T$300, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$300


$C$DW$TU$311	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$311

$C$DW$T$311	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$311, DW_AT_name("sTIDL_SoftMaxParams_t")
	.dwattr $C$DW$T$311, DW_AT_byte_size(0x0c)
$C$DW$657	.dwtag  DW_TAG_member
	.dwattr $C$DW$657, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$657, DW_AT_name("numChannels")
	.dwattr $C$DW$657, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$657, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$657, DW_AT_decl_line(0x527)
	.dwattr $C$DW$657, DW_AT_decl_column(0x0d)

$C$DW$658	.dwtag  DW_TAG_member
	.dwattr $C$DW$658, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$658, DW_AT_name("inDataQ")
	.dwattr $C$DW$658, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$658, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$658, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$658, DW_AT_decl_line(0x529)
	.dwattr $C$DW$658, DW_AT_decl_column(0x0d)

$C$DW$659	.dwtag  DW_TAG_member
	.dwattr $C$DW$659, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$659, DW_AT_name("outDataQ")
	.dwattr $C$DW$659, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$659, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$659, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$659, DW_AT_decl_line(0x52b)
	.dwattr $C$DW$659, DW_AT_decl_column(0x0d)


$C$DW$660	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$660, DW_AT_name("operator =")
	.dwattr $C$DW$660, DW_AT_declaration
	.dwattr $C$DW$660, DW_AT_linkage_name("_ZN21sTIDL_SoftMaxParams_taSERKS_")
	.dwattr $C$DW$660, DW_AT_type(*$C$DW$T$306)
	.dwattr $C$DW$660, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$661	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$661, DW_AT_type(*$C$DW$T$308)

	.dwendtag $C$DW$660


$C$DW$662	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$662, DW_AT_name("operator =")
	.dwattr $C$DW$662, DW_AT_declaration
	.dwattr $C$DW$662, DW_AT_linkage_name("_ZN21sTIDL_SoftMaxParams_taSEOS_")
	.dwattr $C$DW$662, DW_AT_type(*$C$DW$T$306)
	.dwattr $C$DW$662, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$663	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$663, DW_AT_type(*$C$DW$T$306)

	.dwendtag $C$DW$662

	.dwattr $C$DW$T$311, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$311, DW_AT_decl_line(0x525)
	.dwattr $C$DW$T$311, DW_AT_decl_column(0x10)
	.dwendtag $C$DW$T$311

	.dwendtag $C$DW$TU$311


$C$DW$TU$306	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$306
$C$DW$T$306	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$306, DW_AT_type(*$C$DW$T$311)
	.dwattr $C$DW$T$306, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$306


$C$DW$TU$309	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$309

$C$DW$T$309	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$309, DW_AT_type(*$C$DW$T$306)
$C$DW$664	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$664, DW_AT_type(*$C$DW$T$308)

	.dwendtag $C$DW$T$309

	.dwendtag $C$DW$TU$309


$C$DW$TU$310	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$310

$C$DW$T$310	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$310, DW_AT_type(*$C$DW$T$306)
$C$DW$665	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$665, DW_AT_type(*$C$DW$T$306)

	.dwendtag $C$DW$T$310

	.dwendtag $C$DW$TU$310


$C$DW$TU$203	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$203
$C$DW$T$203	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$203, DW_AT_name("sTIDL_SoftMaxParams_t")
	.dwattr $C$DW$T$203, DW_AT_type(*$C$DW$T$311)
	.dwattr $C$DW$T$203, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$203, DW_AT_decl_line(0x52c)
	.dwattr $C$DW$T$203, DW_AT_decl_column(0x02)

	.dwendtag $C$DW$TU$203


$C$DW$TU$307	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$307
$C$DW$T$307	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$307, DW_AT_type(*$C$DW$T$311)

	.dwendtag $C$DW$TU$307


$C$DW$TU$308	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$308
$C$DW$T$308	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$308, DW_AT_type(*$C$DW$T$307)
	.dwattr $C$DW$T$308, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$308


$C$DW$TU$318	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$318

$C$DW$T$318	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$318, DW_AT_name("sTIDL_dataConvertParams_t")
	.dwattr $C$DW$T$318, DW_AT_byte_size(0x0c)
$C$DW$666	.dwtag  DW_TAG_member
	.dwattr $C$DW$666, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$666, DW_AT_name("type")
	.dwattr $C$DW$666, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$666, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$666, DW_AT_decl_line(0x548)
	.dwattr $C$DW$666, DW_AT_decl_column(0x0b)

$C$DW$667	.dwtag  DW_TAG_member
	.dwattr $C$DW$667, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$667, DW_AT_name("layout")
	.dwattr $C$DW$667, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$667, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$667, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$667, DW_AT_decl_line(0x549)
	.dwattr $C$DW$667, DW_AT_decl_column(0x0b)

$C$DW$668	.dwtag  DW_TAG_member
	.dwattr $C$DW$668, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$668, DW_AT_name("zeroPoint")
	.dwattr $C$DW$668, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$668, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$668, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$668, DW_AT_decl_line(0x54a)
	.dwattr $C$DW$668, DW_AT_decl_column(0x0b)


$C$DW$669	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$669, DW_AT_name("operator =")
	.dwattr $C$DW$669, DW_AT_declaration
	.dwattr $C$DW$669, DW_AT_linkage_name("_ZN25sTIDL_dataConvertParams_taSERKS_")
	.dwattr $C$DW$669, DW_AT_type(*$C$DW$T$313)
	.dwattr $C$DW$669, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$670	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$670, DW_AT_type(*$C$DW$T$315)

	.dwendtag $C$DW$669


$C$DW$671	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$671, DW_AT_name("operator =")
	.dwattr $C$DW$671, DW_AT_declaration
	.dwattr $C$DW$671, DW_AT_linkage_name("_ZN25sTIDL_dataConvertParams_taSEOS_")
	.dwattr $C$DW$671, DW_AT_type(*$C$DW$T$313)
	.dwattr $C$DW$671, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$672	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$672, DW_AT_type(*$C$DW$T$313)

	.dwendtag $C$DW$671

	.dwattr $C$DW$T$318, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$318, DW_AT_decl_line(0x546)
	.dwattr $C$DW$T$318, DW_AT_decl_column(0x10)
	.dwendtag $C$DW$T$318

	.dwendtag $C$DW$TU$318


$C$DW$TU$313	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$313
$C$DW$T$313	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$313, DW_AT_type(*$C$DW$T$318)
	.dwattr $C$DW$T$313, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$313


$C$DW$TU$316	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$316

$C$DW$T$316	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$316, DW_AT_type(*$C$DW$T$313)
$C$DW$673	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$673, DW_AT_type(*$C$DW$T$315)

	.dwendtag $C$DW$T$316

	.dwendtag $C$DW$TU$316


$C$DW$TU$317	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$317

$C$DW$T$317	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$317, DW_AT_type(*$C$DW$T$313)
$C$DW$674	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$674, DW_AT_type(*$C$DW$T$313)

	.dwendtag $C$DW$T$317

	.dwendtag $C$DW$TU$317


$C$DW$TU$216	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$216
$C$DW$T$216	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$216, DW_AT_name("sTIDL_dataConvertParams_t")
	.dwattr $C$DW$T$216, DW_AT_type(*$C$DW$T$318)
	.dwattr $C$DW$T$216, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$216, DW_AT_decl_line(0x54b)
	.dwattr $C$DW$T$216, DW_AT_decl_column(0x02)

	.dwendtag $C$DW$TU$216


$C$DW$TU$314	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$314
$C$DW$T$314	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$314, DW_AT_type(*$C$DW$T$318)

	.dwendtag $C$DW$TU$314


$C$DW$TU$315	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$315
$C$DW$T$315	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$315, DW_AT_type(*$C$DW$T$314)
	.dwattr $C$DW$T$315, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$315


$C$DW$TU$325	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$325

$C$DW$T$325	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$325, DW_AT_name("sTIDL_odOutputReformatLayerParams_t")
	.dwattr $C$DW$T$325, DW_AT_byte_size(0x0c)
$C$DW$675	.dwtag  DW_TAG_member
	.dwattr $C$DW$675, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$675, DW_AT_name("layerType")
	.dwattr $C$DW$675, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$675, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$675, DW_AT_decl_line(0x2d0)
	.dwattr $C$DW$675, DW_AT_decl_column(0x0b)

$C$DW$676	.dwtag  DW_TAG_member
	.dwattr $C$DW$676, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$676, DW_AT_name("inWidthOdNetwork")
	.dwattr $C$DW$676, DW_AT_data_member_location[DW_OP_plus_uconst 0x4]
	.dwattr $C$DW$676, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$676, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$676, DW_AT_decl_line(0x2d1)
	.dwattr $C$DW$676, DW_AT_decl_column(0x0b)

$C$DW$677	.dwtag  DW_TAG_member
	.dwattr $C$DW$677, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$677, DW_AT_name("inHeightOdNetwork")
	.dwattr $C$DW$677, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$677, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$677, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$677, DW_AT_decl_line(0x2d2)
	.dwattr $C$DW$677, DW_AT_decl_column(0x0b)


$C$DW$678	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$678, DW_AT_name("operator =")
	.dwattr $C$DW$678, DW_AT_declaration
	.dwattr $C$DW$678, DW_AT_linkage_name("_ZN35sTIDL_odOutputReformatLayerParams_taSERKS_")
	.dwattr $C$DW$678, DW_AT_type(*$C$DW$T$320)
	.dwattr $C$DW$678, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$679	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$679, DW_AT_type(*$C$DW$T$322)

	.dwendtag $C$DW$678


$C$DW$680	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$680, DW_AT_name("operator =")
	.dwattr $C$DW$680, DW_AT_declaration
	.dwattr $C$DW$680, DW_AT_linkage_name("_ZN35sTIDL_odOutputReformatLayerParams_taSEOS_")
	.dwattr $C$DW$680, DW_AT_type(*$C$DW$T$320)
	.dwattr $C$DW$680, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$681	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$681, DW_AT_type(*$C$DW$T$320)

	.dwendtag $C$DW$680

	.dwattr $C$DW$T$325, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$325, DW_AT_decl_line(0x2ce)
	.dwattr $C$DW$T$325, DW_AT_decl_column(0x10)
	.dwendtag $C$DW$T$325

	.dwendtag $C$DW$TU$325


$C$DW$TU$320	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$320
$C$DW$T$320	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$320, DW_AT_type(*$C$DW$T$325)
	.dwattr $C$DW$T$320, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$320


$C$DW$TU$323	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$323

$C$DW$T$323	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$323, DW_AT_type(*$C$DW$T$320)
$C$DW$682	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$682, DW_AT_type(*$C$DW$T$322)

	.dwendtag $C$DW$T$323

	.dwendtag $C$DW$TU$323


$C$DW$TU$324	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$324

$C$DW$T$324	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$324, DW_AT_type(*$C$DW$T$320)
$C$DW$683	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$683, DW_AT_type(*$C$DW$T$320)

	.dwendtag $C$DW$T$324

	.dwendtag $C$DW$TU$324


$C$DW$TU$215	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$215
$C$DW$T$215	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$215, DW_AT_name("sTIDL_odOutputReformatLayerParams_t")
	.dwattr $C$DW$T$215, DW_AT_type(*$C$DW$T$325)
	.dwattr $C$DW$T$215, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$215, DW_AT_decl_line(0x2d3)
	.dwattr $C$DW$T$215, DW_AT_decl_column(0x02)

	.dwendtag $C$DW$TU$215


$C$DW$TU$321	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$321
$C$DW$T$321	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$321, DW_AT_type(*$C$DW$T$325)

	.dwendtag $C$DW$TU$321


$C$DW$TU$322	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$322
$C$DW$T$322	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$322, DW_AT_type(*$C$DW$T$321)
	.dwattr $C$DW$T$322, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$322


$C$DW$TU$332	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$332

$C$DW$T$332	.dwtag  DW_TAG_structure_type
	.dwattr $C$DW$T$332, DW_AT_name("sTIDL_sysMemHandle_t")
	.dwattr $C$DW$T$332, DW_AT_byte_size(0x10)
$C$DW$684	.dwtag  DW_TAG_member
	.dwattr $C$DW$684, DW_AT_type(*$C$DW$T$3)
	.dwattr $C$DW$684, DW_AT_name("base")
	.dwattr $C$DW$684, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$684, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$684, DW_AT_decl_line(0x25c)
	.dwattr $C$DW$684, DW_AT_decl_column(0x0c)

$C$DW$685	.dwtag  DW_TAG_member
	.dwattr $C$DW$685, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$685, DW_AT_name("size")
	.dwattr $C$DW$685, DW_AT_data_member_location[DW_OP_plus_uconst 0x8]
	.dwattr $C$DW$685, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$685, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$685, DW_AT_decl_line(0x25d)
	.dwattr $C$DW$685, DW_AT_decl_column(0x0c)

$C$DW$686	.dwtag  DW_TAG_member
	.dwattr $C$DW$686, DW_AT_type(*$C$DW$T$26)
	.dwattr $C$DW$686, DW_AT_name("offset")
	.dwattr $C$DW$686, DW_AT_data_member_location[DW_OP_plus_uconst 0xc]
	.dwattr $C$DW$686, DW_AT_accessibility(DW_ACCESS_public)
	.dwattr $C$DW$686, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$686, DW_AT_decl_line(0x25e)
	.dwattr $C$DW$686, DW_AT_decl_column(0x0c)


$C$DW$687	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$687, DW_AT_name("operator =")
	.dwattr $C$DW$687, DW_AT_declaration
	.dwattr $C$DW$687, DW_AT_linkage_name("_ZN20sTIDL_sysMemHandle_taSERKS_")
	.dwattr $C$DW$687, DW_AT_type(*$C$DW$T$327)
	.dwattr $C$DW$687, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$688	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$688, DW_AT_type(*$C$DW$T$329)

	.dwendtag $C$DW$687


$C$DW$689	.dwtag  DW_TAG_subprogram
	.dwattr $C$DW$689, DW_AT_name("operator =")
	.dwattr $C$DW$689, DW_AT_declaration
	.dwattr $C$DW$689, DW_AT_linkage_name("_ZN20sTIDL_sysMemHandle_taSEOS_")
	.dwattr $C$DW$689, DW_AT_type(*$C$DW$T$327)
	.dwattr $C$DW$689, DW_AT_accessibility(DW_ACCESS_public)
$C$DW$690	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$690, DW_AT_type(*$C$DW$T$327)

	.dwendtag $C$DW$689

	.dwattr $C$DW$T$332, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$332, DW_AT_decl_line(0x25b)
	.dwattr $C$DW$T$332, DW_AT_decl_column(0x01)
	.dwendtag $C$DW$T$332

	.dwendtag $C$DW$TU$332


$C$DW$TU$327	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$327
$C$DW$T$327	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$327, DW_AT_type(*$C$DW$T$332)
	.dwattr $C$DW$T$327, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$327


$C$DW$TU$330	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$330

$C$DW$T$330	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$330, DW_AT_type(*$C$DW$T$327)
$C$DW$691	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$691, DW_AT_type(*$C$DW$T$329)

	.dwendtag $C$DW$T$330

	.dwendtag $C$DW$TU$330


$C$DW$TU$331	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$331

$C$DW$T$331	.dwtag  DW_TAG_subroutine_type
	.dwattr $C$DW$T$331, DW_AT_type(*$C$DW$T$327)
$C$DW$692	.dwtag  DW_TAG_formal_parameter
	.dwattr $C$DW$692, DW_AT_type(*$C$DW$T$327)

	.dwendtag $C$DW$T$331

	.dwendtag $C$DW$TU$331


$C$DW$TU$328	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$328
$C$DW$T$328	.dwtag  DW_TAG_const_type
	.dwattr $C$DW$T$328, DW_AT_type(*$C$DW$T$332)

	.dwendtag $C$DW$TU$328


$C$DW$TU$329	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$329
$C$DW$T$329	.dwtag  DW_TAG_reference_type
	.dwattr $C$DW$T$329, DW_AT_type(*$C$DW$T$328)
	.dwattr $C$DW$T$329, DW_AT_address_class(0x40)

	.dwendtag $C$DW$TU$329


$C$DW$TU$446	.dwtag  DW_TAG_type_unit
	.dwmtype  $C$DW$T$446
$C$DW$T$446	.dwtag  DW_TAG_typedef
	.dwattr $C$DW$T$446, DW_AT_name("sTIDL_sysMemHandle_t")
	.dwattr $C$DW$T$446, DW_AT_type(*$C$DW$T$332)
	.dwattr $C$DW$T$446, DW_AT_decl_file("../inc/itidl_ti.h")
	.dwattr $C$DW$T$446, DW_AT_decl_line(0x25f)
	.dwattr $C$DW$T$446, DW_AT_decl_column(0x02)

	.dwendtag $C$DW$TU$446

