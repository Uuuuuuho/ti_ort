TIDL_TB_FILES :=
COMMON_FILE   :=

# This is relative to the plat directory
# This section lists ti_dl/test/src/*.c files
# needed by all platforms
TIDL_TB_FILES += tidl_tb.c
TIDL_TB_FILES += tidl_tb_utils.c
TIDL_TB_FILES += tidl_config.c
TIDL_TB_FILES += tidl_image_postproc.c
TIDL_TB_FILES += tidl_image_preproc.c
TIDL_TB_FILES += tidl_image_read_write.c

# This is relative to the plat directory
# This section lists common/*.c files
# needed by all platforms
COMMON_FILES += ti_draw_utils.c
COMMON_FILES += ti_mem_manager.c
COMMON_FILES += configparser.c

CSOURCES += $(foreach file, $(TIDL_TB_FILES), ../../../test/src/$(file))
CSOURCES += $(foreach file, $(COMMON_FILES), ../../../../common/$(file))

# include search directories needed by all platforms
IDIRS += $(PDK_PATH)
IDIRS += $(IVISION_PATH)
IDIRS += $($(_MODULE)_SDIR)/../../../inc
IDIRS += $($(_MODULE)_SDIR)/../../src
IDIRS += $($(_MODULE)_SDIR)/../../../../common
IDIRS += $($(_MODULE)_SDIR)/../../../custom
IDIRS += $($(_MODULE)_SDIR)/../../../utils/perfsim
IDIRS += $($(_MODULE)_SDIR)/../../../rt/inc

# Note: this is linked as shared library. that means you will need to copy
# the build .so file to the target device (PC or EVM)
LDIRS += $(VISION_APPS_PATH)/out/$(TARGET_PLATFORM)/$(TARGET_CPU)/LINUX/$(TARGET_BUILD)
SHARED_LIBS += vx_tidl_rt

# defs needed by all platforms
DEFS+=SOC_J721E
DEFS+=BUILD_C7X_1
