ifeq ($(TARGET_PLATFORM), J7)

include $(PRELUDE)
TARGET      := TI_DEVICE_a72_test_dl_algo_host_rt
TARGETTYPE  := exe
CSOURCES    := $(call all-c-files)

## OPENCV
#ifeq ($(BUILD_WITH_OPENCV), 1)
#DEFS += BUILD_WITH_OPENCV
#
## search path for opencv includes 
#IDIRS += $(TIDL_OPENCV_PATH)/modules/core/include
#IDIRS += $(TIDL_OPENCV_PATH)/modules/highgui/include
#IDIRS += $(TIDL_OPENCV_PATH)/modules/imgcodecs/include
#IDIRS += $(TIDL_OPENCV_PATH)/modules/videoio/include
#IDIRS += $(TIDL_OPENCV_PATH)/modules/imgproc/include
#
## search path for TIDL RT includes
#IDIRS += $(TIDL_RT_PATH)/src
#
## search path for opencv library
#LDIRS += $(TIDL_OPENCV_PATH)/cmake/lib
#LDIRS += $(TIDL_OPENCV_PATH)/cmake/3rdparty/lib
#
## opencv libraries
#STATIC_LIBS += opencv_imgproc
#STATIC_LIBS += opencv_imgcodecs
#STATIC_LIBS += opencv_core
#STATIC_LIBS += libtiff 
#STATIC_LIBS += libwebp
#STATIC_LIBS += libpng
#STATIC_LIBS += libjpeg
#STATIC_LIBS += IlmImf
#STATIC_LIBS += zlib
#STATIC_LIBS += libjasper
#
## opencv needs libdl.so
#SHARED_LIBS += dl 
#endif

# defines for host emulation
DEFS += _HOST_BUILD
DEFS += _A72_BUILD

LDIRS       += $(LINUX_FS_PATH)/usr/lib


# This is not needed in HE as tiovx libs in HE include librt.so
# to emulate multiple cores
SHARED_LIBS += rt 
SHARED_LIBS += ti_rpmsg_char

# get the common make flags from test/src/<plat>/../concerto_common.mak
include $($(_MODULE)_SDIR)/../concerto_common.mak

# This compiler keeps screaming about warnings
CFLAGS += -Wno-unknown-pragmas \
	  -Wno-format-overflow \
	  -Wno-maybe-uninitialized \
	  -Wno-unused-variable \
	  -Wno-unused-function \
	  -Wno-sign-compare \
	  -Wno-parentheses

include $(FINALE)

endif
