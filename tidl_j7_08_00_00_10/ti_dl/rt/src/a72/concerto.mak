ifeq ($(TARGET_PLATFORM), J7)

include $(PRELUDE)

CSOURCES    := tidl_rt_linux_arm.c

LDIRS += $(TIOVX_PATH)/lib/J7/$(TARGET_CPU)/$(TARGET_OS)/$(TARGET_BUILD)
LDIRS += $(VISION_APPS_PATH)/out/J7/$(TARGET_CPU)/$(TARGET_OS)/$(TARGET_BUILD)
LDIRS += $(LINUX_FS_PATH)/usr/lib


TIOVX_LIBS  =
TIOVX_LIBS += vx_framework
TIOVX_LIBS += vx_platform_psdk_j7_linux
TIOVX_LIBS += vx_kernels_host_utils
TIOVX_LIBS += vx_kernels_tidl
TIOVX_LIBS += vx_kernels_openvx_core
TIOVX_LIBS += vx_utils

VISION_APPS_UTILS_LIBS += app_utils_console_io
VISION_APPS_UTILS_LIBS += app_utils_ipc
VISION_APPS_UTILS_LIBS += app_rtos_linux_mpu1_common
VISION_APPS_UTILS_LIBS += app_utils_remote_service
VISION_APPS_UTILS_LIBS += app_utils_mem
VISION_APPS_UTILS_LIBS += app_utils_perf_stats

STATIC_LIBS += $(TIOVX_LIBS)
STATIC_LIBS += $(VISION_APPS_UTILS_LIBS)

SHARED_LIBS += ti_rpmsg_char

include $($(_MODULE)_SDIR)/../concerto_common.mak

IDIRS += $(VISION_APPS_PATH)

include $(FINALE)

endif
