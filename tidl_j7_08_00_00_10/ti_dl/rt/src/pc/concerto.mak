ifeq ($(TARGET_PLATFORM), PC)

include $(PRELUDE)

CSOURCES    := tidl_rt_x86.c

LDIRS += $(CGT7X_ROOT)/host_emulation
LDIRS += $(MMALIB_PATH)/lib/$(TARGET_BUILD)
LDIRS += $(PDK_PATH)/ti/csl/lib/j721e/c7x-hostemu/$(TARGET_BUILD)
LDIRS += $(PDK_PATH)/ti/osal/lib/nonos/j721e/c7x-hostemu/$(TARGET_BUILD)
LDIRS += $(PDK_PATH)/ti/drv/sciclient/lib/j721e_hostemu/c7x-hostemu/$(TARGET_BUILD)
LDIRS += $(PDK_PATH)/ti/drv/udma/lib/j721e_hostemu/c7x-hostemu/$(TARGET_BUILD)
LDIRS += $($(_MODULE)_SDIR)/../../../lib/PC/dsp/algo/$(TARGET_BUILD)
LDIRS += $(TIOVX_PATH)/lib/PC/$(TARGET_CPU)/$(TARGET_OS)/$(TARGET_BUILD)

PDK_LIBS =
PDK_LIBS += dmautils.lib
PDK_LIBS += udma.lib
PDK_LIBS += sciclient.lib
PDK_LIBS += ti.csl.lib
PDK_LIBS += ti.osal.lib

MMA_LIBS =
MMA_LIBS += mmalib_cn_x86_64
MMA_LIBS += mmalib_x86_64
MMA_LIBS += common_x86_64

TIDL_LIBS =
TIDL_LIBS += tidl_algo
TIDL_LIBS += tidl_obj_algo
TIDL_LIBS += tidl_priv_algo
TIDL_LIBS += tidl_custom

TIOVX_LIBS  =
TIOVX_LIBS += vx_framework
TIOVX_LIBS += vx_platform_pc
TIOVX_LIBS += vx_kernels_host_utils vx_kernels_target_utils
TIOVX_LIBS += vx_kernels_tidl
TIOVX_LIBS += vx_kernels_openvx_core vx_target_kernels_openvx_core
TIOVX_LIBS += vx_utils
TIOVX_LIBS += vx_target_kernels_tidl
TIOVX_LIBS += vx_target_kernels_ivision_common
TIOVX_LIBS += vx_target_kernels_c66
TIOVX_LIBS += vx_target_kernels_source_sink
TIOVX_LIBS += vx_target_kernels_tutorial

ADDITIONAL_STATIC_LIBS += $(PDK_LIBS)

STATIC_LIBS += $(TIDL_LIBS)
STATIC_LIBS += $(MMA_LIBS)
STATIC_LIBS += $(TIOVX_LIBS)
STATIC_LIBS += vxlib_$(TARGET_CPU) c6xsim_$(TARGET_CPU)_C66
STATIC_LIBS += C7100-host-emulation

include $($(_MODULE)_SDIR)/../concerto_common.mak

include $(FINALE)

endif
