TARGET      := vx_tidl_rt
TARGETTYPE  := dsmo
CFLAGS += -fPIC -Wno-int-to-pointer-cast
CPPFLAGS += -fPIC --std=c++11

CSOURCES    += ../tidl_rt_ovx.c
CPPSOURCES    += ../tidl_rt_ovx_datamove.cpp

IDIRS :=
IDIRS += $(IVISION_PATH)
IDIRS += $(TIOVX_PATH)/include
IDIRS += $(TIOVX_PATH)/kernels/include
IDIRS += $(TIOVX_PATH)/kernels_j7/include
IDIRS += $(TIOVX_PATH)/kernels_j7/include
IDIRS += $(TIOVX_PATH)/utils/include
IDIRS += $($(_MODULE)_SDIR)/../../../inc
IDIRS += $($(_MODULE)_SDIR)/../../../rt/inc

