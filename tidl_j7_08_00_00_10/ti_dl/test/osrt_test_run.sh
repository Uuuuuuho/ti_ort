#!/bin/bash

# Copyright (c) {2015 - 2021} Texas Instruments Incorporated
#
# All rights reserved not granted herein.
#
# Limited License.
#
# Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
# license under copyrights and patents it now or hereafter owns or controls to make,
# have made, use, import, offer to sell and sell ("Utilize") this software subject to the
# terms herein.  With respect to the foregoing patent license, such license is granted
# solely to the extent that any such patent is necessary to Utilize the software alone.
# The patent license shall not apply to any combinations which include this software,
# other than combinations with devices manufactured by or for TI ("TI Devices").
# No hardware patent is licensed hereunder.
#
# Redistributions must preserve existing copyright notices and reproduce this license
# (including the above copyright notice and the disclaimer and (if applicable) source
# code license limitations below) in the documentation and/or other materials provided
# with the distribution
#
# Redistribution and use in binary form, without modification, are permitted provided
# that the following conditions are met:
#
# *       No reverse engineering, decompilation, or disassembly of this software is
# permitted with respect to any software provided in binary form.
#
# *       any redistribution and use are licensed by TI for use only with TI Devices.
#
# *       Nothing shall obligate TI to provide you with source code for the software
# licensed and provided to you in object code.
#
# If software source code is provided to you, modification and redistribution of the
# source code are permitted provided that the following conditions are met:
#
# *       any redistribution and use of the source code, including any resulting derivative
# works, are licensed by TI for use only with TI Devices.
#
# *       any redistribution and use of any object code compiled from the source code
# and any resulting derivative works, are licensed by TI for use only with TI Devices.
#
# Neither the name of Texas Instruments Incorporated nor the names of its suppliers
#
# may be used to endorse or promote products derived from this software without
# specific prior written permission.
#
# DISCLAIMER.
#
# THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
# OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
# OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
# IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
# OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
# OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
# OF THE POSSIBILITY OF SUCH DAMAGE.

if [ $# -eq "0" ]
then
install_pip=1
download_models=1
compile_models=1
else
install_pip=$1
download_models=$2
compile_models=$3
fi

if [ -z ${TIDL_BASE_PATH+x} ]; then cd ../../  && export TIDL_BASE_PATH=$(pwd) && cd - && echo "Setting TIDL_BASE_PATH - '$TIDL_BASE_PATH'" ;  fi

if [ $compile_models -eq "1" ]
then

cd $TIDL_BASE_PATH/ti_dl/test/tflrt
source ./prepare_model_compliation_env.sh $install_pip $download_models
python3 tflrt_delegate.py -c


cd $TIDL_BASE_PATH/ti_dl/test/onnxrt
source ./prepare_model_compliation_env.sh $install_pip $download_models
python3 onnxrt_ep.py -c


else

tidl_tools_path=$TIDL_BASE_PATH
import_path=":${tidl_tools_path}/ti_dl/utils/tidlModelImport/out"
rt_path=":${tidl_tools_path}/ti_dl/rt/out/PC/x86_64/LINUX/release"
tfl_delegate_path=":${tidl_tools_path}/ti_dl/tfl_delegate/out/PC/x86_64/LINUX/release"
onnx_ep_path=":${tidl_tools_path}/ti_dl/onnxrt_EP/out/PC/x86_64/LINUX/release"

echo "Setting LD_LIBRARY_PATH"
unset LD_LIBRARY_PATH
ld_library_path="${LD_LIBRARY_PATH}${import_path}${rt_path}${tfl_delegate_path}${onnx_ep_path}"
export LD_LIBRARY_PATH=$ld_library_path
echo LD_LIBRARY_PATH $LD_LIBRARY_PATH

cd $TIDL_BASE_PATH/ti_dl/rt/out/PC/x86_64/LINUX/release
rm -f libvx_tidl_rt.so
ln -s libvx_tidl_rt.so.1.0 libvx_tidl_rt.so
cd $TIDL_BASE_PATH/ti_dl/test

#checking all the tidl_tools are available
echo "Checking TIDL tools .... "
if [ ! -e $TIDL_BASE_PATH/tidl_tools/device_config.cfg ]; then echo "ERROR : device_config.cfg not found in tidl_tools ... Exiting" && return; fi 
if [ ! -e $TIDL_BASE_PATH/tidl_tools/PC_dsp_test_dl_algo.out ]; then echo "ERROR : PC_dsp_test_dl_algo.out not found in tidl_tools ... Exiting" && return; fi 
if [ ! -e $TIDL_BASE_PATH/tidl_tools/ti_cnnperfsim.out ]; then echo "ERROR : ti_cnnperfsim.out not found in tidl_tools ... Exiting" && return; fi 
echo "TIDL tools ok !"
echo "Model environment set successfully"


fi

cd $TIDL_BASE_PATH/ti_dl/test
python3 osrt_run_gen_report.py
