import argparse
import csv
import os

parser = argparse.ArgumentParser()

parser.add_argument('-d', '--log_dir',  default='./consoles')
parser.add_argument('-o', '--outFileName',  default='infer_out.csv')
parser.add_argument('-n','--numFrames', default = 1000 )
#parser.add_argument('-l', '--list_file',  default='testvecs/config/config_accuracy_list.txt')
args = parser.parse_args()
newConfigFound = 0
currenConfigCycles = 0



outCsv  = open(args.outFileName, 'w', newline="");
csvWriter = csv.writer(outCsv);
csvWriter.writerow(["Network Config Name","Float","16bit","calib0","calib7"]);
#csvWriter.writerow(["Network Config Name","Float","16bit","calib0","calib7","calib8","calib13","calib15" ]);

configFileList = []
dictMain = {}


configOptions = ["float","16bit","calib0","calib7"]
#configOptions = ["float","16bit","calib0","calib7","calib8","calib13","calib15"]

infer_logs_name = "./all_infer_logs.txt"
fw = open(infer_logs_name,'w')

for item in os.listdir(args.log_dir):
    f = open(os.path.join(args.log_dir, item),'r')
    contents = f.read()
    f.close()
    fw.write(contents)
fw.close()

with open(infer_logs_name, 'r') as infer_logs:
    for line in infer_logs.readlines():
        if "Processing config file"  in line:
            if newConfigFound == 0:
                split_lines = line.rstrip().split(":");
                configFileName = split_lines[1];
                configFileName = configFileName.split(".");
                configFileName = configFileName[0]
                for baseOptionName in configOptions:
                    if configFileName.rfind(baseOptionName) != -1:
                        baseConfigName= configFileName[:-len(baseOptionName)]

                        if baseConfigName not in dictMain.keys():
                            dictList = {key: None for key in configOptions}
                            dictMain[baseConfigName] = dictList
                        break

                newConfigFound = 1;
            else:
                #csvWriter.writerow([configFileName, currenConfigCycles]);
                split_lines = line.rstrip().split(":");
                configFileName = split_lines[1];
                currenConfigCycles = 0;
                newConfigFound = 0


        if newConfigFound == 1:
            if "#  999 . .. T" in line :
                split_lines = line.rstrip().split(":");
                split_lines = split_lines[1].split(",");
                top1_accuracy = split_lines[1]
                top5_accuracy = split_lines[2]
                dictMain[baseConfigName][baseOptionName] = top1_accuracy
                newConfigFound = 0;

    for line in dictMain:
        print(line, dictMain[line])
    for key in dictMain.keys():
        dictList = dictMain[key]
        #csvWriter.writerow([key,dictList["float"],dictList["16bit"],dictList["calib0"],dictList["calib7"],dictList["calib8"],dictList["calib13"],dictList["calib15"]])
        #csvWriter.writerow([key,dictList["float"],dictList["16bit"]])
        csvWriter.writerow([key,dictList["float"],dictList["16bit"],dictList["calib0"],dictList["calib7"]])







