/****************************************************************************/
/*  lnk.cmd   v#.##                                                         */
/*  Copyright (c) 2014-%%%%  Texas Instruments Incorporated                 */
/*                                                                          */
/*  Usage: cl7x  <src files...> -z -o <out file> -m <map file> lnk.cmd      */
/*                                                                          */
/*    Description: THIS FILE IS A SAMPLE linker command file that can be    */
/*                 used for linking programs built with the C compiler and  */
/*                 running the resulting .out file on a C7100 simulator.    */
/*                 Use it as a guideline.  You will want to change the      */
/*                 memory layout to match your specific C7xxx target        */
/*                 system.  You may want to change the allocation scheme    */
/*                 according to the size of your program.                   */
/*                                                                          */
/*    Notes: (1)   You must specify a directory in which your library is    */
/*                 located.  either add a -i"<directory>" line to this      */
/*                 file or use the system environment variable C_DIR to     */
/*                 specify a search path for the libraries.                 */
/*                                                                          */
/*           (2)   You may need to specify your library name with           */
/*                 -llibrary.lib if the correct library is not found        */
/*                 automatically.                                           */
/*                                                                          */
/****************************************************************************/
-c
-heap 0x3C000000
-stack 0x7FE0
--args 0x1000
--diag_suppress=10068 // "no matching section"
//--cinit_compression=off

MEMORY
{
  L2SRAM   (RWX): org = 0x64800000, len = 0x070000 /* Assuming 448KB of SRAM */
  //L2SRAM   (RWX): org = 0x64800000, len = 0x078000 /* Assuming 448KB + 32KBof SRAM */
  MSMCSRAM (RWX): org = 0x70001000, len =  0x7E8000
  L1DSRAM       : org = 0x64E00000, len = 0x4000
#if defined(TIDL_BUILD_FOR_LOKI)
  EXTMEM (RWX)  : org = 0x83000000, len = 0x4F000000
#else
  EXTMEM (RWX)  : org = 0x83000000, len = 0x3D000000
#endif
  APP_LOG_MEM   : org = 0x81000000 , len = 0x00040000
  APP_FILIO_MEM : org = 0x82000000 , len = 0x00040000
  VECTOR_MEM    : org = 0x80000000 , len = 0x800
}

SECTIONS
{
	.ss_vectors > VECTOR_MEM
    .text       >       EXTMEM
    .bss:app_log_mem        (NOLOAD) : {} > APP_LOG_MEM
    .bss        >       EXTMEM  /* Zero-initialized data */
    .data       >       EXTMEM  /* Initialized data */

    .cinit      >       EXTMEM  /* could be part of const */
    .init_array >       EXTMEM  /* C++ initializations */
    .stack      align = 0x100 >       MSMCSRAM
    //.stack      align = 0x100 >       L2SRAM
    .args       >       EXTMEM
    .cio        >       EXTMEM
    .const      >       EXTMEM
    .switch     >       EXTMEM /* For exception handling. */
                         /* Not a default ELF section -- remove?  */
                         /* could be part of const */
//    .dmaUtilsContext > EXTMEM
//    .mmaHandle > EXTMEM

    .sysmem        > EXTMEM /* heap */

    .iobuf        :> EXTMEM
    .createParams :> EXTMEM
    .tempiobuf    :> EXTMEM

    .fileio       :> EXTMEM       /*Reserved memory section*/
    .hostdata     :> EXTMEM       /*Reserved memory section*/
    .dmem1Sect    :> EXTMEM 
    .gIOParams      :> EXTMEM   
    .tidl_tb_io_dec     :> EXTMEM 
    .tidl_tb_output_tensor    :> EXTMEM 
    .tidl_tb_input_tensor    :> EXTMEM 
    .tidl_tb_net              :> EXTMEM
    .dmem0Sect    :> EXTMEM 
	.l1ScratchBuf align = 0x100 > L1DSRAM
	.l2ScratchBuf align = 0x8000 > L2SRAM
	.l3ScratchBuf align = 0x100  > MSMCSRAM
  .bss:app_fileio_msg_mem align = 0x4000  (NOLOAD) > APP_FILIO_MEM
}

/* Move frequently accessedd sections to MSMC */

SECTIONS
{
    .mmalib_col_flow
    {
        *mmalib_C71.lib<MMALIB_CNN_convolve_col_smallNo_ixX_ixX_oxX_ci.obj> (.text:*exec_ci*)
        *mmalib_C71.lib<MMALIB_CNN_convolve_col_smallNo_ixX_ixX_oxX_ci.obj> (.text:*init_ci*)
    } align = 0x80 > EXTMEM
    .mmalib_row_flow
    {
        *mmalib_C71.lib<MMALIB_CNN_convolve_row_ixX_ixX_oxX_ci.obj> (.text:*exec_ci*)
    } align = 0x80 > EXTMEM

    GROUP (frequenty_accessed_functions)
    {
        .text:tidl_printf
        .text:DmaUtilsAutoInc3d_prepareTr
        .text:DmaUtilsAutoInc3d_configure
        .text:DmaUtilsAutoInc3d_trigger
        .text:DmaUtilsAutoInc3d_wait
        .text:DmaUtilsAutoInc3d_getTrMemReq
	.text:DmaUtilsAutoInc3d_setupTr
	.text:DmaUtilsAutoInc3d_setupTr
        .text:DmaUtilsAutoInc3d_getTotalBlockCount
	.text:Udma_chDruSubmitTr
	.text:Udma_chEnable
        .text:MMALIB_CNN_convolve_row_ixX_ixX_oxX_exec
        .text:MMALIB_CNN_convolve_col_smallNo_ixX_ixX_oxX_exec
        .text:MMALIB_CNN_convolve_col_smallNo_ixX_ixX_oxX_init
        .text:MMALIB_CNN_convolve_col_smallNo_ixX_ixX_oxX_init_checkParams
        .text:MMALIB_CNN_convolve_col_smallNo_ixX_ixX_oxX_getHandleSize
    } align = 0x80 > EXTMEM
}

