ifeq ($(TARGET_PLATFORM), PC)
ifeq ($(TARGET_OS), WINDOWS)

include $(PRELUDE)

TARGET      := PC_dsp_test_dl_algo.out
TARGETTYPE  := exe
CSOURCES := main.c
CSOURCES += ..\..\..\..\common\getopt\getopt.c

# host emulation needs HE includes
IDIRS += $(CGT7X_ROOT)/host_emulation/include/C7100

# library search dirs are always platform specific
LDIRS += $(CGT7X_ROOT)/host_emulation
LDIRS += $(PDK_PATH)/ti/csl/lib/j721e/c7x-hostemu/$(TARGET_BUILD)
LDIRS += $(PDK_PATH)/ti/osal/lib/nonos/j721e/c7x-hostemu/$(TARGET_BUILD)
LDIRS += $(PDK_PATH)/ti/drv/sciclient/lib/j721e_hostemu/c7x-hostemu/$(TARGET_BUILD)
LDIRS += $(PDK_PATH)/ti/drv/udma/lib/j721e_hostemu/c7x-hostemu/$(TARGET_BUILD)
LDIRS += $(MMALIB_PATH)/lib/$(TARGET_BUILD)

# path to tidl_algo and tidl_priv_algo 
LDIRS += $($(_MODULE)_SDIR)/../../../lib/PC/dsp/algo/$(TARGET_BUILD)

# External libraries: The order in which they are defined ins important
STATIC_LIBS += dmautils
STATIC_LIBS += udma
STATIC_LIBS += sciclient
STATIC_LIBS += ti.csl
STATIC_LIBS += ti.osal

# internal libraries 
STATIC_LIBS += tidl_algo
STATIC_LIBS += tidl_obj_algo
STATIC_LIBS += tidl_priv_algo

# Custom Library
STATIC_LIBS += tidl_custom

# External libraries: The order in which they are defined ins important
# Also, must be defined after the internal libraries
STATIC_LIBS += mmalib_cn_x86_64
STATIC_LIBS += mmalib_x86_64
STATIC_LIBS += common_x86_64
STATIC_LIBS += C7100-host-emulation

# CUDA
# TODO may have LDIRS, IDIRS, {STATIC, SHARED}_LIBS
ifeq ($(BUILD_WITH_CUDA), 1)
DEFS += BUILD_WITH_CUDA
endif

# OPENCV
ifeq ($(BUILD_WITH_OPENCV), 1)
DEFS += BUILD_WITH_OPENCV

# search path for opencv includes 
IDIRS += $(TIDL_OPENCV_PATH)/modules/core/include
IDIRS += $(TIDL_OPENCV_PATH)/modules/highgui/include
IDIRS += $(TIDL_OPENCV_PATH)/modules/imgcodecs/include
IDIRS += $(TIDL_OPENCV_PATH)/modules/videoio/include
IDIRS += $(TIDL_OPENCV_PATH)/modules/imgproc/include

# search path for opencv library
LDIRS += $(TIDL_OPENCV_PATH)/../build/lib/Release
LDIRS += $(TIDL_OPENCV_PATH)/../build/3rdparty/lib/Release

# opencv libraries
STATIC_LIBS += opencv_imgproc310
STATIC_LIBS += opencv_imgcodecs310
STATIC_LIBS += opencv_core310
STATIC_LIBS += libtiff 
STATIC_LIBS += libwebp
STATIC_LIBS += libpng
STATIC_LIBS += libjpeg
STATIC_LIBS += IlmImf
STATIC_LIBS += zlib
STATIC_LIBS += libjasper
STATIC_LIBS += User32


endif

# AVX
# TODO may have LDIRS, IDIRS, {STATIC, SHARED}_LIBS
ifeq ($(TIDL_BUILD_WITH_AVX), 1)
DEFS += TIDL_BUILD_WITH_AVX
endif

# defines for host emulation
DEFS += HOST_EMULATION
DEFS += _HOST_BUILD

IDIRS += $($(_MODULE)_SDIR)/../../../../common/getopt
CFLAGS+= /W0 

# get the common make flags from test/src/<plat>/../concerto_common.mak
include $($(_MODULE)_SDIR)/../concerto_common.mak

include $(FINALE)

endif
endif
