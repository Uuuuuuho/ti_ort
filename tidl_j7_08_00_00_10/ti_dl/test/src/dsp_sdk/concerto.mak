ifeq ($(TARGET_PLATFORM), J7_SDK)

include $(PRELUDE)

TARGET      := j7-c71_0-fw
override BIN_EXT :=
TARGETTYPE  := exe
CSOURCES    := $(call all-c-files)

# DSP build needs extenal getopt() support
CSOURCES    += ../../../../common/getopt/getopt.c
IDIRS += $($(_MODULE)_SDIR)/../../../../common/getopt

# DSP build needs XDC support
XDC_BLD_FILE = $($(_MODULE)_SDIR)/config_c71.bld
XDC_IDIRS    = $($(_MODULE)_SDIR)
XDC_CFG_FILE = $($(_MODULE)_SDIR)/c7x_1.cfg
XDC_PLATFORM = "ti.platforms.tms320C7x:J7ES"

# DSP build needs linker command files for memory maps
LINKER_CMD_FILES +=  $($(_MODULE)_SDIR)/linker_mem_map.cmd
LINKER_CMD_FILES +=  $($(_MODULE)_SDIR)/linker.cmd

# DSP build needs CGT, BIOS, and XDC include files
IDIRS += $(CGT7X_ROOT)/include
IDIRS += $(BIOS_PATH)/packages
IDIRS += $(XDCTOOLS_PATH)/packages

# library search dirs are always platform specific
LDIRS += $(CGT7X_ROOT)/lib
LDIRS += $(PDK_PATH)/ti/csl/lib/j721e/c7x/$(TARGET_BUILD)
LDIRS += $(PDK_PATH)/ti/drv/ipc/lib/j721e/c7x_1/$(TARGET_BUILD)
LDIRS += $(PDK_PATH)/ti/osal/lib/tirtos/j721e/c7x/$(TARGET_BUILD)
LDIRS += $(PDK_PATH)/ti/drv/sciclient/lib/j721e/c7x_1/$(TARGET_BUILD)
LDIRS += $(PDK_PATH)/ti/drv/udma/lib/j721e/c7x_1/$(TARGET_BUILD)
LDIRS += $(MMALIB_PATH)/lib/$(TARGET_BUILD)

# path to tidl_algo and tidl_priv_algo
LDIRS += $($(_MODULE)_SDIR)/../../../lib/dsp/algo/$(TARGET_BUILD)

# External libraries
ADDITIONAL_STATIC_LIBS += ti.csl.ae71
ADDITIONAL_STATIC_LIBS += ipc.ae71
ADDITIONAL_STATIC_LIBS += ti.osal.ae71
ADDITIONAL_STATIC_LIBS += sciclient.ae71
ADDITIONAL_STATIC_LIBS += udma.ae71
ADDITIONAL_STATIC_LIBS += dmautils.ae71
ADDITIONAL_STATIC_LIBS += common_C7100.lib
ADDITIONAL_STATIC_LIBS += mmalib_C7100.lib
ADDITIONAL_STATIC_LIBS += mmalib_cn_C7100.lib

# internal libraries
ADDITIONAL_STATIC_LIBS += tidl_algo.lib
ADDITIONAL_STATIC_LIBS += tidl_obj_algo.lib
ADDITIONAL_STATIC_LIBS += tidl_priv_algo.lib

# Custom Library
ADDITIONAL_STATIC_LIBS += tidl_custom.lib


#
# Suppress this warning, 10063-D: entry-point symbol other than "_c_int00" specified
# c7x boots in secure mode and to switch to non-secure mode we need to start at a special entry point '_c_int00_secure'
# and later after switching to non-secure mode, sysbios jumps to usual entry point of _c_int00
# Hence we need to suppress this warning
CFLAGS+=--diag_suppress=10063

DEFS += DDR_BW_STATS

# get the common make flags from test/src/<plat>/../concerto_common.mak
include $($(_MODULE)_SDIR)/../concerto_common.mak

include $(FINALE)

endif
