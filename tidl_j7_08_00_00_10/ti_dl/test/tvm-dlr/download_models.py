import os
import requests

models = {
    'inception_v3.tflite' : {'url':'https://tfhub.dev/tensorflow/lite-model/inception_v3/1/default/1?lite-format=tflite', 'dir':'../testvecs/models/public/tflite/'},
    'mobilenetv2-1.0.onnx' : {'url':'https://github.com/vinitra-zz/models/raw/7301ce1e16891ed5f75dd15a6a53a643001288f0/vision/classification/mobilenet/model/mobilenetv2-7.onnx', 'dir':'../testvecs/models/public/onnx/'}
}


for model_name in models:
    model_path = models[model_name]['dir'] + model_name
    if(not os.path.isfile(model_path)):
        print("Downloading  ", model_name)
        url = models[model_name]['url']
        r = requests.get(url, allow_redirects=True)
        open(model_path, 'wb').write(r.content)
