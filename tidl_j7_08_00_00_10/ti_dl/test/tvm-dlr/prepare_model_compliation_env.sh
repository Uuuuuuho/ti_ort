#!/bin/bash

if [ -z ${TIDL_BASE_PATH+x} ]; then cd ../../../  && export TIDL_BASE_PATH=$(pwd) && cd ti_dl/test/tvm-dlr && echo "Setting TIDL_BASE_PATH - '$TIDL_BASE_PATH'" ;  fi
if [ -z ${ARM64_GCC_PATH+x} ]; then cd ../../../../gcc-arm-9.2-2019.12-x86_64-aarch64-none-linux-gnu  && export ARM64_GCC_PATH=$(pwd) && cd - && echo "Setting ARM64_GCC_PATH - '$ARM64_GCC_PATH'" ;  fi

#Install required python packages
echo "Installing requests"
pip3 install --upgrade requests

mkdir -p '../testvecs/models/public/tflite/' 
mkdir -p '../testvecs/models/public/onnx/' 

echo "Downloading File ..."
# Call download models script
cd $TIDL_BASE_PATH/ti_dl/test/tvm-dlr/ > /dev/null
python3 download_models.py
cd -   > /dev/null

echo "Models Files done"

echo "Installing DLR"
pip3 install --upgrade --force-reinstall dlr-1.8.0-py3-none-any.whl 
echo "Installing TVM"
pip3 install --upgrade --force-reinstall tvm-0.8.dev0-cp36-cp36m-linux_x86_64.whl
echo "Installing tflite onnx"
pip3 install --upgrade tflite onnx
echo "Installing opencv pillow"
pip3 install opencv-python==4.2.0.34 pillow
echo "Installing pytest"
pip3 install --upgrade pytest
echo "Installing graphviz"
pip3 install --upgrade graphviz

cd $TIDL_BASE_PATH/ti_dl/rt/out/PC/x86_64/LINUX/release
rm -f libvx_tidl_rt.so
ln -s libvx_tidl_rt.so.1.0 libvx_tidl_rt.so
cd -

#set LD_LIBRARY_PATHS
tidl_tools_path=$TIDL_BASE_PATH

import_path=":${tidl_tools_path}/ti_dl/utils/tidlModelImport/out"
rt_path=":${tidl_tools_path}/ti_dl/rt/out/PC/x86_64/LINUX/release"

echo "Setting LD_LIBRARY_PATH"
unset LD_LIBRARY_PATH
ld_library_path="${LD_LIBRARY_PATH}${import_path}${rt_path}"
export LD_LIBRARY_PATH=$ld_library_path




#checking all the tidl_tools are available
echo "Checking TIDL tools .... "
if [ ! -e $TIDL_BASE_PATH/tidl_tools/device_config.cfg ]; then echo "ERROR : device_config.cfg not found in tidl_tools ... Exiting" && return; fi 
if [ ! -e $TIDL_BASE_PATH/tidl_tools/PC_dsp_test_dl_algo.out ]; then echo "ERROR : PC_dsp_test_dl_algo.out not found in tidl_tools ... Exiting" && return; fi 
if [ ! -e $TIDL_BASE_PATH/tidl_tools/ti_cnnperfsim.out ]; then echo "ERROR : ti_cnnperfsim.out not found in tidl_tools ... Exiting" && return; fi 

echo "TIDL tools ok !"

echo "Model environment set successfully"
