/*
 *  Do not modify this file; it is automatically 
 *  generated and any modifications will be overwritten.
 *
 * @(#) xdc-J06
 */

#define __nested__
#define __config__

#include <xdc/std.h>

/*
 * ======== GENERATED SECTIONS ========
 *
 *     MODULE INCLUDES
 *
 *     <module-name> INTERNALS
 *     <module-name> INHERITS
 *     <module-name> VTABLE
 *     <module-name> PATCH TABLE
 *     <module-name> DECLARATIONS
 *     <module-name> OBJECT OFFSETS
 *     <module-name> TEMPLATES
 *     <module-name> INITIALIZERS
 *     <module-name> FUNCTION STUBS
 *     <module-name> PROXY BODY
 *     <module-name> OBJECT DESCRIPTOR
 *     <module-name> VIRTUAL FUNCTIONS
 *     <module-name> SYSTEM FUNCTIONS
 *     <module-name> PRAGMAS
 *
 *     INITIALIZATION ENTRY POINT
 *     PROGRAM GLOBALS
 *     CLINK DIRECTIVES
 */


/*
 * ======== MODULE INCLUDES ========
 */

#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/family/c64p/tci6488/TimerSupport.h>
#include <ti/sysbios/family/c7x/Cache.h>
#include <ti/sysbios/family/c7x/Exception.h>
#include <ti/sysbios/family/c7x/Hwi.h>
#include <ti/sysbios/family/c7x/IntrinsicsSupport.h>
#include <ti/sysbios/family/c7x/Mmu.h>
#include <ti/sysbios/family/c7x/TaskSupport.h>
#include <ti/sysbios/family/c7x/TimestampProvider.h>
#include <ti/sysbios/gates/GateHwi.h>
#include <ti/sysbios/gates/GateMutex.h>
#include <ti/sysbios/gates/GateMutexPri.h>
#include <ti/sysbios/gates/GateSwi.h>
#include <ti/sysbios/gates/GateTask.h>
#include <ti/sysbios/hal/Cache.h>
#include <ti/sysbios/hal/Core.h>
#include <ti/sysbios/hal/CoreNull.h>
#include <ti/sysbios/hal/Hwi.h>
#include <ti/sysbios/heaps/HeapBuf.h>
#include <ti/sysbios/heaps/HeapMem.h>
#include <ti/sysbios/knl/Clock.h>
#include <ti/sysbios/knl/Event.h>
#include <ti/sysbios/knl/Idle.h>
#include <ti/sysbios/knl/Intrinsics.h>
#include <ti/sysbios/knl/Queue.h>
#include <ti/sysbios/knl/Semaphore.h>
#include <ti/sysbios/knl/Swi.h>
#include <ti/sysbios/knl/Task.h>
#include <ti/sysbios/syncs/SyncSem.h>
#include <ti/sysbios/timers/dmtimer/Timer.h>
#include <ti/sysbios/utils/Load.h>
#include <ti/sysbios/xdcruntime/GateThreadSupport.h>
#include <xdc/runtime/Assert.h>
#include <xdc/runtime/Core.h>
#include <xdc/runtime/Defaults.h>
#include <xdc/runtime/Diags.h>
#include <xdc/runtime/Error.h>
#include <xdc/runtime/Gate.h>
#include <xdc/runtime/Log.h>
#include <xdc/runtime/Main.h>
#include <xdc/runtime/Memory.h>
#include <xdc/runtime/Registry.h>
#include <xdc/runtime/Startup.h>
#include <xdc/runtime/SysMin.h>
#include <xdc/runtime/SysStd.h>
#include <xdc/runtime/System.h>
#include <xdc/runtime/Text.h>
#include <xdc/runtime/Timestamp.h>
#include <xdc/runtime/TimestampNull.h>
#include <xdc/runtime/Types.h>
#include <xdc/runtime/knl/GateH.h>
#include <xdc/runtime/knl/GateThread.h>

/* suppress 'type qualifier is meaningless on cast type' warning */
#if defined(__ti__) && !defined(__clang__)
#pragma diag_suppress 193
#endif
#ifdef __IAR_SYSTEMS_ICC__
#pragma diag_suppress=Pe191
#endif

#if !(defined(__GNUC__))
#if !(defined(__TI_GNU_ATTRIBUTE_SUPPORT__) || defined(__IAR_SYSTEMS_ICC__)) || defined(__ARP32__)
#define __attribute__(x)
#endif
#endif

/*
 * ======== ti.sysbios.BIOS INTERNALS ========
 */


/*
 * ======== ti.sysbios.BIOS_RtsGateProxy INTERNALS ========
 */

/* Module__ */
typedef struct ti_sysbios_BIOS_RtsGateProxy_Module__ {
    xdc_runtime_Types_Link link;
} ti_sysbios_BIOS_RtsGateProxy_Module__;

/* Module__root__V */
extern ti_sysbios_BIOS_RtsGateProxy_Module__ ti_sysbios_BIOS_RtsGateProxy_Module__root__V;

/* @@@ ti_sysbios_knl_Queue_Object__ */
typedef struct ti_sysbios_knl_Queue_Object__ {
    ti_sysbios_knl_Queue_Elem elem;
} ti_sysbios_knl_Queue_Object__;

/* @@@ ti_sysbios_knl_Queue_Object2__ */
typedef struct {
    xdc_runtime_Types_InstHdr hdr;
    ti_sysbios_knl_Queue_Object__ obj;
} ti_sysbios_knl_Queue_Object2__;

/* @@@ ti_sysbios_knl_Semaphore_Object__ */
typedef struct ti_sysbios_knl_Semaphore_Object__ {
    ti_sysbios_knl_Event_Handle event;
    xdc_UInt eventId;
    ti_sysbios_knl_Semaphore_Mode mode;
    volatile xdc_UInt16 count;
    ti_sysbios_knl_Queue_Object__ Object_field_pendQ;
} ti_sysbios_knl_Semaphore_Object__;

/* @@@ ti_sysbios_knl_Semaphore_Object2__ */
typedef struct {
    xdc_runtime_Types_InstHdr hdr;
    ti_sysbios_knl_Semaphore_Object__ obj;
} ti_sysbios_knl_Semaphore_Object2__;

/* Object__ */
typedef struct ti_sysbios_gates_GateMutex_Object__ {
    const ti_sysbios_gates_GateMutex_Fxns__ *__fxns;
    ti_sysbios_knl_Task_Handle owner;
    ti_sysbios_knl_Semaphore_Object__ Object_field_sem;
} ti_sysbios_gates_GateMutex_Object__;

/* Object2__ */
typedef struct {
    xdc_runtime_Types_InstHdr hdr;
    ti_sysbios_gates_GateMutex_Object__ obj;
} ti_sysbios_gates_GateMutex_Object2__;

/* Object */
typedef ti_sysbios_gates_GateMutex_Object__ ti_sysbios_BIOS_RtsGateProxy_Object__;

/* Object2__ */
typedef struct {
    xdc_runtime_Types_InstHdr hdr;
    ti_sysbios_BIOS_RtsGateProxy_Object__ obj;
} ti_sysbios_BIOS_RtsGateProxy_Object2__;


/*
 * ======== ti.sysbios.family.c64p.tci6488.TimerSupport INTERNALS ========
 */


/*
 * ======== ti.sysbios.family.c7x.Cache INTERNALS ========
 */


/*
 * ======== ti.sysbios.family.c7x.Exception INTERNALS ========
 */


/*
 * ======== ti.sysbios.family.c7x.Hwi INTERNALS ========
 */

/* Module__ */
typedef struct ti_sysbios_family_c7x_Hwi_Module__ {
    xdc_runtime_Types_Link link;
} ti_sysbios_family_c7x_Hwi_Module__;

/* Module__root__V */
extern ti_sysbios_family_c7x_Hwi_Module__ ti_sysbios_family_c7x_Hwi_Module__root__V;

/* Object__ */
typedef struct ti_sysbios_family_c7x_Hwi_Object__ {
    const ti_sysbios_family_c7x_Hwi_Fxns__ *__fxns;
    xdc_ULong disableMask;
    xdc_ULong restoreMask;
    xdc_UArg arg;
    ti_sysbios_family_c7x_Hwi_FuncPtr fxn;
    xdc_Int intNum;
    xdc_Int priority;
    ti_sysbios_family_c7x_Hwi_Irp irp;
    __TA_ti_sysbios_family_c7x_Hwi_Instance_State__hookEnv hookEnv;
} ti_sysbios_family_c7x_Hwi_Object__;

/* Object2__ */
typedef struct {
    xdc_runtime_Types_InstHdr hdr;
    ti_sysbios_family_c7x_Hwi_Object__ obj;
} ti_sysbios_family_c7x_Hwi_Object2__;


/*
 * ======== ti.sysbios.family.c7x.IntrinsicsSupport INTERNALS ========
 */


/*
 * ======== ti.sysbios.family.c7x.Mmu INTERNALS ========
 */


/*
 * ======== ti.sysbios.family.c7x.TaskSupport INTERNALS ========
 */


/*
 * ======== ti.sysbios.family.c7x.TimestampProvider INTERNALS ========
 */


/*
 * ======== ti.sysbios.gates.GateHwi INTERNALS ========
 */

/* Module__ */
typedef struct ti_sysbios_gates_GateHwi_Module__ {
    xdc_runtime_Types_Link link;
} ti_sysbios_gates_GateHwi_Module__;

/* Module__root__V */
extern ti_sysbios_gates_GateHwi_Module__ ti_sysbios_gates_GateHwi_Module__root__V;

/* Object__ */
typedef struct ti_sysbios_gates_GateHwi_Object__ {
    const ti_sysbios_gates_GateHwi_Fxns__ *__fxns;
} ti_sysbios_gates_GateHwi_Object__;

/* Object2__ */
typedef struct {
    xdc_runtime_Types_InstHdr hdr;
    ti_sysbios_gates_GateHwi_Object__ obj;
} ti_sysbios_gates_GateHwi_Object2__;


/*
 * ======== ti.sysbios.gates.GateMutex INTERNALS ========
 */

/* Module__ */
typedef struct ti_sysbios_gates_GateMutex_Module__ {
    xdc_runtime_Types_Link link;
} ti_sysbios_gates_GateMutex_Module__;

/* Module__root__V */
extern ti_sysbios_gates_GateMutex_Module__ ti_sysbios_gates_GateMutex_Module__root__V;

/* <-- ti_sysbios_gates_GateMutex_Object */


/*
 * ======== ti.sysbios.gates.GateMutexPri INTERNALS ========
 */

/* Module__ */
typedef struct ti_sysbios_gates_GateMutexPri_Module__ {
    xdc_runtime_Types_Link link;
} ti_sysbios_gates_GateMutexPri_Module__;

/* Module__root__V */
extern ti_sysbios_gates_GateMutexPri_Module__ ti_sysbios_gates_GateMutexPri_Module__root__V;

/* <-- ti_sysbios_knl_Queue_Object */

/* Object__ */
typedef struct ti_sysbios_gates_GateMutexPri_Object__ {
    const ti_sysbios_gates_GateMutexPri_Fxns__ *__fxns;
    volatile xdc_UInt mutexCnt;
    volatile xdc_Int ownerOrigPri;
    volatile ti_sysbios_knl_Task_Handle owner;
    ti_sysbios_knl_Queue_Object__ Object_field_pendQ;
} ti_sysbios_gates_GateMutexPri_Object__;

/* Object2__ */
typedef struct {
    xdc_runtime_Types_InstHdr hdr;
    ti_sysbios_gates_GateMutexPri_Object__ obj;
} ti_sysbios_gates_GateMutexPri_Object2__;


/*
 * ======== ti.sysbios.gates.GateSwi INTERNALS ========
 */

/* Module__ */
typedef struct ti_sysbios_gates_GateSwi_Module__ {
    xdc_runtime_Types_Link link;
} ti_sysbios_gates_GateSwi_Module__;

/* Module__root__V */
extern ti_sysbios_gates_GateSwi_Module__ ti_sysbios_gates_GateSwi_Module__root__V;

/* Object__ */
typedef struct ti_sysbios_gates_GateSwi_Object__ {
    const ti_sysbios_gates_GateSwi_Fxns__ *__fxns;
} ti_sysbios_gates_GateSwi_Object__;

/* Object2__ */
typedef struct {
    xdc_runtime_Types_InstHdr hdr;
    ti_sysbios_gates_GateSwi_Object__ obj;
} ti_sysbios_gates_GateSwi_Object2__;


/*
 * ======== ti.sysbios.gates.GateTask INTERNALS ========
 */

/* Module__ */
typedef struct ti_sysbios_gates_GateTask_Module__ {
    xdc_runtime_Types_Link link;
} ti_sysbios_gates_GateTask_Module__;

/* Module__root__V */
extern ti_sysbios_gates_GateTask_Module__ ti_sysbios_gates_GateTask_Module__root__V;

/* Object__ */
typedef struct ti_sysbios_gates_GateTask_Object__ {
    const ti_sysbios_gates_GateTask_Fxns__ *__fxns;
} ti_sysbios_gates_GateTask_Object__;

/* Object2__ */
typedef struct {
    xdc_runtime_Types_InstHdr hdr;
    ti_sysbios_gates_GateTask_Object__ obj;
} ti_sysbios_gates_GateTask_Object2__;


/*
 * ======== ti.sysbios.hal.Cache INTERNALS ========
 */


/*
 * ======== ti.sysbios.hal.Cache_CacheProxy INTERNALS ========
 */


/*
 * ======== ti.sysbios.hal.Core INTERNALS ========
 */


/*
 * ======== ti.sysbios.hal.CoreNull INTERNALS ========
 */


/*
 * ======== ti.sysbios.hal.Core_CoreProxy INTERNALS ========
 */


/*
 * ======== ti.sysbios.hal.Hwi INTERNALS ========
 */

/* Module__ */
typedef struct ti_sysbios_hal_Hwi_Module__ {
    xdc_runtime_Types_Link link;
} ti_sysbios_hal_Hwi_Module__;

/* Module__root__V */
extern ti_sysbios_hal_Hwi_Module__ ti_sysbios_hal_Hwi_Module__root__V;

/* Object__ */
typedef struct ti_sysbios_hal_Hwi_Object__ {
    const ti_sysbios_hal_Hwi_Fxns__ *__fxns;
    ti_sysbios_hal_Hwi_HwiProxy_Handle pi;
} ti_sysbios_hal_Hwi_Object__;

/* Object2__ */
typedef struct {
    xdc_runtime_Types_InstHdr hdr;
    ti_sysbios_hal_Hwi_Object__ obj;
} ti_sysbios_hal_Hwi_Object2__;


/*
 * ======== ti.sysbios.hal.Hwi_HwiProxy INTERNALS ========
 */

/* Module__ */
typedef struct ti_sysbios_hal_Hwi_HwiProxy_Module__ {
    xdc_runtime_Types_Link link;
} ti_sysbios_hal_Hwi_HwiProxy_Module__;

/* Module__root__V */
extern ti_sysbios_hal_Hwi_HwiProxy_Module__ ti_sysbios_hal_Hwi_HwiProxy_Module__root__V;

/* <-- ti_sysbios_family_c7x_Hwi_Object */

/* Object */
typedef ti_sysbios_family_c7x_Hwi_Object__ ti_sysbios_hal_Hwi_HwiProxy_Object__;

/* Object2__ */
typedef struct {
    xdc_runtime_Types_InstHdr hdr;
    ti_sysbios_hal_Hwi_HwiProxy_Object__ obj;
} ti_sysbios_hal_Hwi_HwiProxy_Object2__;


/*
 * ======== ti.sysbios.heaps.HeapBuf INTERNALS ========
 */

/* Module__ */
typedef struct ti_sysbios_heaps_HeapBuf_Module__ {
    xdc_runtime_Types_Link link;
} ti_sysbios_heaps_HeapBuf_Module__;

/* Module__root__V */
extern ti_sysbios_heaps_HeapBuf_Module__ ti_sysbios_heaps_HeapBuf_Module__root__V;

/* <-- ti_sysbios_knl_Queue_Object */

/* Object__ */
typedef struct ti_sysbios_heaps_HeapBuf_Object__ {
    const ti_sysbios_heaps_HeapBuf_Fxns__ *__fxns;
    xdc_SizeT blockSize;
    xdc_SizeT align;
    xdc_UInt numBlocks;
    xdc_runtime_Memory_Size bufSize;
    __TA_ti_sysbios_heaps_HeapBuf_Instance_State__buf buf;
    xdc_UInt numFreeBlocks;
    xdc_UInt minFreeBlocks;
    ti_sysbios_knl_Queue_Object__ Object_field_freeList;
} ti_sysbios_heaps_HeapBuf_Object__;

/* Object2__ */
typedef struct {
    xdc_runtime_Types_InstHdr hdr;
    ti_sysbios_heaps_HeapBuf_Object__ obj;
} ti_sysbios_heaps_HeapBuf_Object2__;


/*
 * ======== ti.sysbios.heaps.HeapMem INTERNALS ========
 */

/* Module__ */
typedef struct ti_sysbios_heaps_HeapMem_Module__ {
    xdc_runtime_Types_Link link;
} ti_sysbios_heaps_HeapMem_Module__;

/* Module__root__V */
extern ti_sysbios_heaps_HeapMem_Module__ ti_sysbios_heaps_HeapMem_Module__root__V;

/* Object__ */
typedef struct ti_sysbios_heaps_HeapMem_Object__ {
    const ti_sysbios_heaps_HeapMem_Fxns__ *__fxns;
    xdc_runtime_Memory_Size align;
    __TA_ti_sysbios_heaps_HeapMem_Instance_State__buf buf;
    ti_sysbios_heaps_HeapMem_Header head;
    xdc_SizeT minBlockAlign;
} ti_sysbios_heaps_HeapMem_Object__;

/* Object2__ */
typedef struct {
    xdc_runtime_Types_InstHdr hdr;
    ti_sysbios_heaps_HeapMem_Object__ obj;
} ti_sysbios_heaps_HeapMem_Object2__;


/*
 * ======== ti.sysbios.heaps.HeapMem_Module_GateProxy INTERNALS ========
 */

/* Module__ */
typedef struct ti_sysbios_heaps_HeapMem_Module_GateProxy_Module__ {
    xdc_runtime_Types_Link link;
} ti_sysbios_heaps_HeapMem_Module_GateProxy_Module__;

/* Module__root__V */
extern ti_sysbios_heaps_HeapMem_Module_GateProxy_Module__ ti_sysbios_heaps_HeapMem_Module_GateProxy_Module__root__V;

/* <-- ti_sysbios_gates_GateMutex_Object */

/* Object */
typedef ti_sysbios_gates_GateMutex_Object__ ti_sysbios_heaps_HeapMem_Module_GateProxy_Object__;

/* Object2__ */
typedef struct {
    xdc_runtime_Types_InstHdr hdr;
    ti_sysbios_heaps_HeapMem_Module_GateProxy_Object__ obj;
} ti_sysbios_heaps_HeapMem_Module_GateProxy_Object2__;


/*
 * ======== ti.sysbios.knl.Clock INTERNALS ========
 */

/* Module__ */
typedef struct ti_sysbios_knl_Clock_Module__ {
    xdc_runtime_Types_Link link;
} ti_sysbios_knl_Clock_Module__;

/* Module__root__V */
extern ti_sysbios_knl_Clock_Module__ ti_sysbios_knl_Clock_Module__root__V;

/* <-- ti_sysbios_knl_Queue_Object */

/* Object__ */
typedef struct ti_sysbios_knl_Clock_Object__ {
    ti_sysbios_knl_Queue_Elem elem;
    xdc_UInt32 timeout;
    xdc_UInt32 currTimeout;
    xdc_UInt32 period;
    volatile xdc_Bool active;
    ti_sysbios_knl_Clock_FuncPtr fxn;
    xdc_UArg arg;
    xdc_UInt32 timeoutTicks;
} ti_sysbios_knl_Clock_Object__;

/* Object2__ */
typedef struct {
    xdc_runtime_Types_InstHdr hdr;
    ti_sysbios_knl_Clock_Object__ obj;
} ti_sysbios_knl_Clock_Object2__;


/*
 * ======== ti.sysbios.knl.Clock_TimerProxy INTERNALS ========
 */

/* Module__ */
typedef struct ti_sysbios_knl_Clock_TimerProxy_Module__ {
    xdc_runtime_Types_Link link;
} ti_sysbios_knl_Clock_TimerProxy_Module__;

/* Module__root__V */
extern ti_sysbios_knl_Clock_TimerProxy_Module__ ti_sysbios_knl_Clock_TimerProxy_Module__root__V;

/* Object__ */
typedef struct ti_sysbios_timers_dmtimer_Timer_Object__ {
    const ti_sysbios_timers_dmtimer_Timer_Fxns__ *__fxns;
    xdc_Bool staticInst;
    xdc_Int id;
    xdc_UInt tiocpCfg;
    xdc_UInt tmar;
    xdc_UInt tier;
    xdc_UInt twer;
    xdc_UInt tclr;
    xdc_UInt tsicr;
    ti_sysbios_interfaces_ITimer_RunMode runMode;
    ti_sysbios_interfaces_ITimer_StartMode startMode;
    xdc_UInt period;
    ti_sysbios_interfaces_ITimer_PeriodType periodType;
    xdc_UInt intNum;
    xdc_Int eventId;
    xdc_UArg arg;
    ti_sysbios_hal_Hwi_FuncPtr tickFxn;
    xdc_runtime_Types_FreqHz extFreq;
    ti_sysbios_hal_Hwi_Handle hwi;
    xdc_UInt prevThreshold;
    xdc_UInt rollovers;
    xdc_UInt savedCurrCount;
    xdc_Bool useDefaultEventId;
} ti_sysbios_timers_dmtimer_Timer_Object__;

/* Object2__ */
typedef struct {
    xdc_runtime_Types_InstHdr hdr;
    ti_sysbios_timers_dmtimer_Timer_Object__ obj;
} ti_sysbios_timers_dmtimer_Timer_Object2__;

/* Object */
typedef ti_sysbios_timers_dmtimer_Timer_Object__ ti_sysbios_knl_Clock_TimerProxy_Object__;

/* Object2__ */
typedef struct {
    xdc_runtime_Types_InstHdr hdr;
    ti_sysbios_knl_Clock_TimerProxy_Object__ obj;
} ti_sysbios_knl_Clock_TimerProxy_Object2__;


/*
 * ======== ti.sysbios.knl.Event INTERNALS ========
 */

/* Module__ */
typedef struct ti_sysbios_knl_Event_Module__ {
    xdc_runtime_Types_Link link;
} ti_sysbios_knl_Event_Module__;

/* Module__root__V */
extern ti_sysbios_knl_Event_Module__ ti_sysbios_knl_Event_Module__root__V;

/* <-- ti_sysbios_knl_Queue_Object */

/* Object__ */
typedef struct ti_sysbios_knl_Event_Object__ {
    volatile xdc_UInt postedEvents;
    ti_sysbios_knl_Queue_Object__ Object_field_pendQ;
} ti_sysbios_knl_Event_Object__;

/* Object2__ */
typedef struct {
    xdc_runtime_Types_InstHdr hdr;
    ti_sysbios_knl_Event_Object__ obj;
} ti_sysbios_knl_Event_Object2__;


/*
 * ======== ti.sysbios.knl.Idle INTERNALS ========
 */


/*
 * ======== ti.sysbios.knl.Intrinsics INTERNALS ========
 */


/*
 * ======== ti.sysbios.knl.Intrinsics_SupportProxy INTERNALS ========
 */


/*
 * ======== ti.sysbios.knl.Queue INTERNALS ========
 */

/* Module__ */
typedef struct ti_sysbios_knl_Queue_Module__ {
    xdc_runtime_Types_Link link;
} ti_sysbios_knl_Queue_Module__;

/* Module__root__V */
extern ti_sysbios_knl_Queue_Module__ ti_sysbios_knl_Queue_Module__root__V;

/* <-- ti_sysbios_knl_Queue_Object */


/*
 * ======== ti.sysbios.knl.Semaphore INTERNALS ========
 */

/* Module__ */
typedef struct ti_sysbios_knl_Semaphore_Module__ {
    xdc_runtime_Types_Link link;
} ti_sysbios_knl_Semaphore_Module__;

/* Module__root__V */
extern ti_sysbios_knl_Semaphore_Module__ ti_sysbios_knl_Semaphore_Module__root__V;

/* <-- ti_sysbios_knl_Semaphore_Object */


/*
 * ======== ti.sysbios.knl.Swi INTERNALS ========
 */

/* Module__ */
typedef struct ti_sysbios_knl_Swi_Module__ {
    xdc_runtime_Types_Link link;
} ti_sysbios_knl_Swi_Module__;

/* Module__root__V */
extern ti_sysbios_knl_Swi_Module__ ti_sysbios_knl_Swi_Module__root__V;

/* Object__ */
typedef struct ti_sysbios_knl_Swi_Object__ {
    ti_sysbios_knl_Queue_Elem qElem;
    ti_sysbios_knl_Swi_FuncPtr fxn;
    xdc_UArg arg0;
    xdc_UArg arg1;
    xdc_UInt priority;
    xdc_UInt mask;
    xdc_Bool posted;
    xdc_UInt initTrigger;
    xdc_UInt trigger;
    ti_sysbios_knl_Queue_Handle readyQ;
    __TA_ti_sysbios_knl_Swi_Instance_State__hookEnv hookEnv;
} ti_sysbios_knl_Swi_Object__;

/* Object2__ */
typedef struct {
    xdc_runtime_Types_InstHdr hdr;
    ti_sysbios_knl_Swi_Object__ obj;
} ti_sysbios_knl_Swi_Object2__;


/*
 * ======== ti.sysbios.knl.Task INTERNALS ========
 */

/* Module__ */
typedef struct ti_sysbios_knl_Task_Module__ {
    xdc_runtime_Types_Link link;
} ti_sysbios_knl_Task_Module__;

/* Module__root__V */
extern ti_sysbios_knl_Task_Module__ ti_sysbios_knl_Task_Module__root__V;

/* <-- ti_sysbios_knl_Queue_Object */

/* Object__ */
typedef struct ti_sysbios_knl_Task_Object__ {
    ti_sysbios_knl_Queue_Elem qElem;
    volatile xdc_Int priority;
    xdc_UInt mask;
    xdc_Ptr context;
    ti_sysbios_knl_Task_Mode mode;
    ti_sysbios_knl_Task_PendElem *pendElem;
    xdc_SizeT stackSize;
    __TA_ti_sysbios_knl_Task_Instance_State__stack stack;
    xdc_runtime_IHeap_Handle stackHeap;
    ti_sysbios_knl_Task_FuncPtr fxn;
    xdc_UArg arg0;
    xdc_UArg arg1;
    xdc_Ptr env;
    __TA_ti_sysbios_knl_Task_Instance_State__hookEnv hookEnv;
    xdc_Bool vitalTaskFlag;
    ti_sysbios_knl_Queue_Handle readyQ;
    xdc_UInt curCoreId;
    xdc_UInt affinity;
    xdc_Bool privileged;
    xdc_Ptr domain;
    xdc_UInt32 checkValue;
    xdc_Ptr tls;
} ti_sysbios_knl_Task_Object__;

/* Object2__ */
typedef struct {
    xdc_runtime_Types_InstHdr hdr;
    ti_sysbios_knl_Task_Object__ obj;
} ti_sysbios_knl_Task_Object2__;


/*
 * ======== ti.sysbios.knl.Task_SupportProxy INTERNALS ========
 */


/*
 * ======== ti.sysbios.syncs.SyncSem INTERNALS ========
 */

/* Module__ */
typedef struct ti_sysbios_syncs_SyncSem_Module__ {
    xdc_runtime_Types_Link link;
} ti_sysbios_syncs_SyncSem_Module__;

/* Module__root__V */
extern ti_sysbios_syncs_SyncSem_Module__ ti_sysbios_syncs_SyncSem_Module__root__V;

/* Object__ */
typedef struct ti_sysbios_syncs_SyncSem_Object__ {
    const ti_sysbios_syncs_SyncSem_Fxns__ *__fxns;
    xdc_Bool userSem;
    ti_sysbios_knl_Semaphore_Handle sem;
} ti_sysbios_syncs_SyncSem_Object__;

/* Object2__ */
typedef struct {
    xdc_runtime_Types_InstHdr hdr;
    ti_sysbios_syncs_SyncSem_Object__ obj;
} ti_sysbios_syncs_SyncSem_Object2__;


/*
 * ======== ti.sysbios.timers.dmtimer.Timer INTERNALS ========
 */

/* Module__ */
typedef struct ti_sysbios_timers_dmtimer_Timer_Module__ {
    xdc_runtime_Types_Link link;
} ti_sysbios_timers_dmtimer_Timer_Module__;

/* Module__root__V */
extern ti_sysbios_timers_dmtimer_Timer_Module__ ti_sysbios_timers_dmtimer_Timer_Module__root__V;

/* <-- ti_sysbios_timers_dmtimer_Timer_Object */


/*
 * ======== ti.sysbios.timers.dmtimer.Timer_TimerSupportProxy INTERNALS ========
 */


/*
 * ======== ti.sysbios.utils.Load INTERNALS ========
 */


/*
 * ======== ti.sysbios.xdcruntime.GateThreadSupport INTERNALS ========
 */

/* Module__ */
typedef struct ti_sysbios_xdcruntime_GateThreadSupport_Module__ {
    xdc_runtime_Types_Link link;
} ti_sysbios_xdcruntime_GateThreadSupport_Module__;

/* Module__root__V */
extern ti_sysbios_xdcruntime_GateThreadSupport_Module__ ti_sysbios_xdcruntime_GateThreadSupport_Module__root__V;

/* <-- ti_sysbios_gates_GateMutexPri_Object */

/* Object__ */
typedef struct ti_sysbios_xdcruntime_GateThreadSupport_Object__ {
    const ti_sysbios_xdcruntime_GateThreadSupport_Fxns__ *__fxns;
    ti_sysbios_gates_GateMutexPri_Object__ Object_field_gate;
} ti_sysbios_xdcruntime_GateThreadSupport_Object__;

/* Object2__ */
typedef struct {
    xdc_runtime_Types_InstHdr hdr;
    ti_sysbios_xdcruntime_GateThreadSupport_Object__ obj;
} ti_sysbios_xdcruntime_GateThreadSupport_Object2__;


/*
 * ======== xdc.runtime.Assert INTERNALS ========
 */


/*
 * ======== xdc.runtime.Core INTERNALS ========
 */


/*
 * ======== xdc.runtime.Defaults INTERNALS ========
 */


/*
 * ======== xdc.runtime.Diags INTERNALS ========
 */


/*
 * ======== xdc.runtime.Error INTERNALS ========
 */


/*
 * ======== xdc.runtime.Gate INTERNALS ========
 */


/*
 * ======== xdc.runtime.Log INTERNALS ========
 */


/*
 * ======== xdc.runtime.Main INTERNALS ========
 */


/*
 * ======== xdc.runtime.Main_Module_GateProxy INTERNALS ========
 */

/* Module__ */
typedef struct xdc_runtime_Main_Module_GateProxy_Module__ {
    xdc_runtime_Types_Link link;
} xdc_runtime_Main_Module_GateProxy_Module__;

/* Module__root__V */
extern xdc_runtime_Main_Module_GateProxy_Module__ xdc_runtime_Main_Module_GateProxy_Module__root__V;

/* <-- ti_sysbios_gates_GateHwi_Object */

/* Object */
typedef ti_sysbios_gates_GateHwi_Object__ xdc_runtime_Main_Module_GateProxy_Object__;

/* Object2__ */
typedef struct {
    xdc_runtime_Types_InstHdr hdr;
    xdc_runtime_Main_Module_GateProxy_Object__ obj;
} xdc_runtime_Main_Module_GateProxy_Object2__;


/*
 * ======== xdc.runtime.Memory INTERNALS ========
 */


/*
 * ======== xdc.runtime.Memory_HeapProxy INTERNALS ========
 */

/* Module__ */
typedef struct xdc_runtime_Memory_HeapProxy_Module__ {
    xdc_runtime_Types_Link link;
} xdc_runtime_Memory_HeapProxy_Module__;

/* Module__root__V */
extern xdc_runtime_Memory_HeapProxy_Module__ xdc_runtime_Memory_HeapProxy_Module__root__V;

/* <-- ti_sysbios_heaps_HeapMem_Object */

/* Object */
typedef ti_sysbios_heaps_HeapMem_Object__ xdc_runtime_Memory_HeapProxy_Object__;

/* Object2__ */
typedef struct {
    xdc_runtime_Types_InstHdr hdr;
    xdc_runtime_Memory_HeapProxy_Object__ obj;
} xdc_runtime_Memory_HeapProxy_Object2__;


/*
 * ======== xdc.runtime.Registry INTERNALS ========
 */


/*
 * ======== xdc.runtime.Startup INTERNALS ========
 */


/*
 * ======== xdc.runtime.SysMin INTERNALS ========
 */


/*
 * ======== xdc.runtime.SysStd INTERNALS ========
 */


/*
 * ======== xdc.runtime.System INTERNALS ========
 */


/*
 * ======== xdc.runtime.System_Module_GateProxy INTERNALS ========
 */

/* Module__ */
typedef struct xdc_runtime_System_Module_GateProxy_Module__ {
    xdc_runtime_Types_Link link;
} xdc_runtime_System_Module_GateProxy_Module__;

/* Module__root__V */
extern xdc_runtime_System_Module_GateProxy_Module__ xdc_runtime_System_Module_GateProxy_Module__root__V;

/* <-- ti_sysbios_gates_GateHwi_Object */

/* Object */
typedef ti_sysbios_gates_GateHwi_Object__ xdc_runtime_System_Module_GateProxy_Object__;

/* Object2__ */
typedef struct {
    xdc_runtime_Types_InstHdr hdr;
    xdc_runtime_System_Module_GateProxy_Object__ obj;
} xdc_runtime_System_Module_GateProxy_Object2__;


/*
 * ======== xdc.runtime.System_SupportProxy INTERNALS ========
 */


/*
 * ======== xdc.runtime.Text INTERNALS ========
 */


/*
 * ======== xdc.runtime.Timestamp INTERNALS ========
 */


/*
 * ======== xdc.runtime.TimestampNull INTERNALS ========
 */


/*
 * ======== xdc.runtime.Timestamp_SupportProxy INTERNALS ========
 */


/*
 * ======== xdc.runtime.Types INTERNALS ========
 */


/*
 * ======== xdc.runtime.knl.GateH INTERNALS ========
 */


/*
 * ======== xdc.runtime.knl.GateH_Proxy INTERNALS ========
 */

/* Module__ */
typedef struct xdc_runtime_knl_GateH_Proxy_Module__ {
    xdc_runtime_Types_Link link;
} xdc_runtime_knl_GateH_Proxy_Module__;

/* Module__root__V */
extern xdc_runtime_knl_GateH_Proxy_Module__ xdc_runtime_knl_GateH_Proxy_Module__root__V;

/* Object__ */
typedef struct xdc_runtime_knl_GateThread_Object__ {
    const xdc_runtime_knl_GateThread_Fxns__ *__fxns;
    xdc_runtime_knl_GateThread_Proxy_Handle proxyHandle;
} xdc_runtime_knl_GateThread_Object__;

/* Object2__ */
typedef struct {
    xdc_runtime_Types_InstHdr hdr;
    xdc_runtime_knl_GateThread_Object__ obj;
} xdc_runtime_knl_GateThread_Object2__;

/* Object */
typedef xdc_runtime_knl_GateThread_Object__ xdc_runtime_knl_GateH_Proxy_Object__;

/* Object2__ */
typedef struct {
    xdc_runtime_Types_InstHdr hdr;
    xdc_runtime_knl_GateH_Proxy_Object__ obj;
} xdc_runtime_knl_GateH_Proxy_Object2__;


/*
 * ======== xdc.runtime.knl.GateThread INTERNALS ========
 */

/* Module__ */
typedef struct xdc_runtime_knl_GateThread_Module__ {
    xdc_runtime_Types_Link link;
} xdc_runtime_knl_GateThread_Module__;

/* Module__root__V */
extern xdc_runtime_knl_GateThread_Module__ xdc_runtime_knl_GateThread_Module__root__V;

/* <-- xdc_runtime_knl_GateThread_Object */


/*
 * ======== xdc.runtime.knl.GateThread_Proxy INTERNALS ========
 */

/* Module__ */
typedef struct xdc_runtime_knl_GateThread_Proxy_Module__ {
    xdc_runtime_Types_Link link;
} xdc_runtime_knl_GateThread_Proxy_Module__;

/* Module__root__V */
extern xdc_runtime_knl_GateThread_Proxy_Module__ xdc_runtime_knl_GateThread_Proxy_Module__root__V;

/* <-- ti_sysbios_xdcruntime_GateThreadSupport_Object */

/* Object */
typedef ti_sysbios_xdcruntime_GateThreadSupport_Object__ xdc_runtime_knl_GateThread_Proxy_Object__;

/* Object2__ */
typedef struct {
    xdc_runtime_Types_InstHdr hdr;
    xdc_runtime_knl_GateThread_Proxy_Object__ obj;
} xdc_runtime_knl_GateThread_Proxy_Object2__;


/*
 * ======== INHERITS ========
 */

#pragma DATA_SECTION(ti_sysbios_interfaces_ITimerSupport_Interface__BASE__C, ".const:ti_sysbios_interfaces_ITimerSupport_Interface__BASE__C");
__FAR__ const xdc_runtime_Types_Base ti_sysbios_interfaces_ITimerSupport_Interface__BASE__C = {&xdc_runtime_IModule_Interface__BASE__C};

#pragma DATA_SECTION(xdc_runtime_ITimestampClient_Interface__BASE__C, ".const:xdc_runtime_ITimestampClient_Interface__BASE__C");
__FAR__ const xdc_runtime_Types_Base xdc_runtime_ITimestampClient_Interface__BASE__C = {&xdc_runtime_IModule_Interface__BASE__C};

#pragma DATA_SECTION(xdc_runtime_ITimestampProvider_Interface__BASE__C, ".const:xdc_runtime_ITimestampProvider_Interface__BASE__C");
__FAR__ const xdc_runtime_Types_Base xdc_runtime_ITimestampProvider_Interface__BASE__C = {&xdc_runtime_ITimestampClient_Interface__BASE__C};

#pragma DATA_SECTION(xdc_runtime_ISystemSupport_Interface__BASE__C, ".const:xdc_runtime_ISystemSupport_Interface__BASE__C");
__FAR__ const xdc_runtime_Types_Base xdc_runtime_ISystemSupport_Interface__BASE__C = {&xdc_runtime_IModule_Interface__BASE__C};

#pragma DATA_SECTION(xdc_runtime_knl_ISync_Interface__BASE__C, ".const:xdc_runtime_knl_ISync_Interface__BASE__C");
__FAR__ const xdc_runtime_Types_Base xdc_runtime_knl_ISync_Interface__BASE__C = {&xdc_runtime_IModule_Interface__BASE__C};

#pragma DATA_SECTION(xdc_runtime_IGateProvider_Interface__BASE__C, ".const:xdc_runtime_IGateProvider_Interface__BASE__C");
__FAR__ const xdc_runtime_Types_Base xdc_runtime_IGateProvider_Interface__BASE__C = {&xdc_runtime_IModule_Interface__BASE__C};

#pragma DATA_SECTION(xdc_runtime_IModule_Interface__BASE__C, ".const:xdc_runtime_IModule_Interface__BASE__C");
__FAR__ const xdc_runtime_Types_Base xdc_runtime_IModule_Interface__BASE__C = {NULL};

#pragma DATA_SECTION(ti_sysbios_interfaces_ICore_Interface__BASE__C, ".const:ti_sysbios_interfaces_ICore_Interface__BASE__C");
__FAR__ const xdc_runtime_Types_Base ti_sysbios_interfaces_ICore_Interface__BASE__C = {&xdc_runtime_IModule_Interface__BASE__C};

#pragma DATA_SECTION(xdc_runtime_IHeap_Interface__BASE__C, ".const:xdc_runtime_IHeap_Interface__BASE__C");
__FAR__ const xdc_runtime_Types_Base xdc_runtime_IHeap_Interface__BASE__C = {&xdc_runtime_IModule_Interface__BASE__C};


/*
 * ======== ti.sysbios.family.c64p.tci6488.TimerSupport VTABLE ========
 */

/* Module__FXNS__C */
#pragma DATA_SECTION(ti_sysbios_family_c64p_tci6488_TimerSupport_Module__FXNS__C, ".const:ti_sysbios_family_c64p_tci6488_TimerSupport_Module__FXNS__C");
const ti_sysbios_family_c64p_tci6488_TimerSupport_Fxns__ ti_sysbios_family_c64p_tci6488_TimerSupport_Module__FXNS__C = {
    &ti_sysbios_interfaces_ITimerSupport_Interface__BASE__C, /* __base */
    &ti_sysbios_family_c64p_tci6488_TimerSupport_Module__FXNS__C.__sfxns, /* __sysp */
    ti_sysbios_family_c64p_tci6488_TimerSupport_enable__E,
    {
        NULL, /* __create */
        NULL, /* __delete */
        NULL, /* __label */
        0x8042, /* __mid */
    } /* __sfxns */
};


/*
 * ======== ti.sysbios.gates.GateHwi VTABLE ========
 */

/* Module__FXNS__C */
#pragma DATA_SECTION(ti_sysbios_gates_GateHwi_Module__FXNS__C, ".const:ti_sysbios_gates_GateHwi_Module__FXNS__C");
const ti_sysbios_gates_GateHwi_Fxns__ ti_sysbios_gates_GateHwi_Module__FXNS__C = {
    &xdc_runtime_IGateProvider_Interface__BASE__C, /* __base */
    &ti_sysbios_gates_GateHwi_Module__FXNS__C.__sfxns, /* __sysp */
    ti_sysbios_gates_GateHwi_query__E,
    ti_sysbios_gates_GateHwi_enter__E,
    ti_sysbios_gates_GateHwi_leave__E,
    {
        ti_sysbios_gates_GateHwi_Object__create__S,
        ti_sysbios_gates_GateHwi_Object__delete__S,
        ti_sysbios_gates_GateHwi_Handle__label__S,
        0x8036, /* __mid */
    } /* __sfxns */
};


/*
 * ======== ti.sysbios.gates.GateMutex VTABLE ========
 */

/* Module__FXNS__C */
#pragma DATA_SECTION(ti_sysbios_gates_GateMutex_Module__FXNS__C, ".const:ti_sysbios_gates_GateMutex_Module__FXNS__C");
const ti_sysbios_gates_GateMutex_Fxns__ ti_sysbios_gates_GateMutex_Module__FXNS__C = {
    &xdc_runtime_IGateProvider_Interface__BASE__C, /* __base */
    &ti_sysbios_gates_GateMutex_Module__FXNS__C.__sfxns, /* __sysp */
    ti_sysbios_gates_GateMutex_query__E,
    ti_sysbios_gates_GateMutex_enter__E,
    ti_sysbios_gates_GateMutex_leave__E,
    {
        ti_sysbios_gates_GateMutex_Object__create__S,
        ti_sysbios_gates_GateMutex_Object__delete__S,
        ti_sysbios_gates_GateMutex_Handle__label__S,
        0x803a, /* __mid */
    } /* __sfxns */
};


/*
 * ======== ti.sysbios.gates.GateMutexPri VTABLE ========
 */

/* Module__FXNS__C */
#pragma DATA_SECTION(ti_sysbios_gates_GateMutexPri_Module__FXNS__C, ".const:ti_sysbios_gates_GateMutexPri_Module__FXNS__C");
const ti_sysbios_gates_GateMutexPri_Fxns__ ti_sysbios_gates_GateMutexPri_Module__FXNS__C = {
    &xdc_runtime_IGateProvider_Interface__BASE__C, /* __base */
    &ti_sysbios_gates_GateMutexPri_Module__FXNS__C.__sfxns, /* __sysp */
    ti_sysbios_gates_GateMutexPri_query__E,
    ti_sysbios_gates_GateMutexPri_enter__E,
    ti_sysbios_gates_GateMutexPri_leave__E,
    {
        ti_sysbios_gates_GateMutexPri_Object__create__S,
        ti_sysbios_gates_GateMutexPri_Object__delete__S,
        ti_sysbios_gates_GateMutexPri_Handle__label__S,
        0x8039, /* __mid */
    } /* __sfxns */
};


/*
 * ======== ti.sysbios.gates.GateSwi VTABLE ========
 */

/* Module__FXNS__C */
#pragma DATA_SECTION(ti_sysbios_gates_GateSwi_Module__FXNS__C, ".const:ti_sysbios_gates_GateSwi_Module__FXNS__C");
const ti_sysbios_gates_GateSwi_Fxns__ ti_sysbios_gates_GateSwi_Module__FXNS__C = {
    &xdc_runtime_IGateProvider_Interface__BASE__C, /* __base */
    &ti_sysbios_gates_GateSwi_Module__FXNS__C.__sfxns, /* __sysp */
    ti_sysbios_gates_GateSwi_query__E,
    ti_sysbios_gates_GateSwi_enter__E,
    ti_sysbios_gates_GateSwi_leave__E,
    {
        ti_sysbios_gates_GateSwi_Object__create__S,
        ti_sysbios_gates_GateSwi_Object__delete__S,
        ti_sysbios_gates_GateSwi_Handle__label__S,
        0x8037, /* __mid */
    } /* __sfxns */
};


/*
 * ======== ti.sysbios.gates.GateTask VTABLE ========
 */

/* Module__FXNS__C */
#pragma DATA_SECTION(ti_sysbios_gates_GateTask_Module__FXNS__C, ".const:ti_sysbios_gates_GateTask_Module__FXNS__C");
const ti_sysbios_gates_GateTask_Fxns__ ti_sysbios_gates_GateTask_Module__FXNS__C = {
    &xdc_runtime_IGateProvider_Interface__BASE__C, /* __base */
    &ti_sysbios_gates_GateTask_Module__FXNS__C.__sfxns, /* __sysp */
    ti_sysbios_gates_GateTask_query__E,
    ti_sysbios_gates_GateTask_enter__E,
    ti_sysbios_gates_GateTask_leave__E,
    {
        ti_sysbios_gates_GateTask_Object__create__S,
        ti_sysbios_gates_GateTask_Object__delete__S,
        ti_sysbios_gates_GateTask_Handle__label__S,
        0x8038, /* __mid */
    } /* __sfxns */
};


/*
 * ======== ti.sysbios.hal.CoreNull VTABLE ========
 */

/* Module__FXNS__C */
#pragma DATA_SECTION(ti_sysbios_hal_CoreNull_Module__FXNS__C, ".const:ti_sysbios_hal_CoreNull_Module__FXNS__C");
const ti_sysbios_hal_CoreNull_Fxns__ ti_sysbios_hal_CoreNull_Module__FXNS__C = {
    &ti_sysbios_interfaces_ICore_Interface__BASE__C, /* __base */
    &ti_sysbios_hal_CoreNull_Module__FXNS__C.__sfxns, /* __sysp */
    ti_sysbios_hal_CoreNull_getId__E,
    ti_sysbios_hal_CoreNull_interruptCore__E,
    ti_sysbios_hal_CoreNull_lock__E,
    ti_sysbios_hal_CoreNull_unlock__E,
    ti_sysbios_hal_CoreNull_hwiDisable__E,
    ti_sysbios_hal_CoreNull_hwiEnable__E,
    ti_sysbios_hal_CoreNull_hwiRestore__E,
    {
        NULL, /* __create */
        NULL, /* __delete */
        NULL, /* __label */
        0x802e, /* __mid */
    } /* __sfxns */
};


/*
 * ======== ti.sysbios.heaps.HeapBuf VTABLE ========
 */

/* Module__FXNS__C */
#pragma DATA_SECTION(ti_sysbios_heaps_HeapBuf_Module__FXNS__C, ".const:ti_sysbios_heaps_HeapBuf_Module__FXNS__C");
const ti_sysbios_heaps_HeapBuf_Fxns__ ti_sysbios_heaps_HeapBuf_Module__FXNS__C = {
    &xdc_runtime_IHeap_Interface__BASE__C, /* __base */
    &ti_sysbios_heaps_HeapBuf_Module__FXNS__C.__sfxns, /* __sysp */
    ti_sysbios_heaps_HeapBuf_alloc__E,
    ti_sysbios_heaps_HeapBuf_free__E,
    ti_sysbios_heaps_HeapBuf_isBlocking__E,
    ti_sysbios_heaps_HeapBuf_getStats__E,
    {
        ti_sysbios_heaps_HeapBuf_Object__create__S,
        ti_sysbios_heaps_HeapBuf_Object__delete__S,
        ti_sysbios_heaps_HeapBuf_Handle__label__S,
        0x8031, /* __mid */
    } /* __sfxns */
};


/*
 * ======== ti.sysbios.heaps.HeapMem VTABLE ========
 */

/* Module__FXNS__C */
#pragma DATA_SECTION(ti_sysbios_heaps_HeapMem_Module__FXNS__C, ".const:ti_sysbios_heaps_HeapMem_Module__FXNS__C");
const ti_sysbios_heaps_HeapMem_Fxns__ ti_sysbios_heaps_HeapMem_Module__FXNS__C = {
    &xdc_runtime_IHeap_Interface__BASE__C, /* __base */
    &ti_sysbios_heaps_HeapMem_Module__FXNS__C.__sfxns, /* __sysp */
    ti_sysbios_heaps_HeapMem_alloc__E,
    ti_sysbios_heaps_HeapMem_free__E,
    ti_sysbios_heaps_HeapMem_isBlocking__E,
    ti_sysbios_heaps_HeapMem_getStats__E,
    {
        ti_sysbios_heaps_HeapMem_Object__create__S,
        ti_sysbios_heaps_HeapMem_Object__delete__S,
        ti_sysbios_heaps_HeapMem_Handle__label__S,
        0x8032, /* __mid */
    } /* __sfxns */
};


/*
 * ======== ti.sysbios.syncs.SyncSem VTABLE ========
 */

/* Module__FXNS__C */
#pragma DATA_SECTION(ti_sysbios_syncs_SyncSem_Module__FXNS__C, ".const:ti_sysbios_syncs_SyncSem_Module__FXNS__C");
const ti_sysbios_syncs_SyncSem_Fxns__ ti_sysbios_syncs_SyncSem_Module__FXNS__C = {
    &xdc_runtime_knl_ISync_Interface__BASE__C, /* __base */
    &ti_sysbios_syncs_SyncSem_Module__FXNS__C.__sfxns, /* __sysp */
    ti_sysbios_syncs_SyncSem_query__E,
    ti_sysbios_syncs_SyncSem_signal__E,
    ti_sysbios_syncs_SyncSem_wait__E,
    {
        ti_sysbios_syncs_SyncSem_Object__create__S,
        ti_sysbios_syncs_SyncSem_Object__delete__S,
        ti_sysbios_syncs_SyncSem_Handle__label__S,
        0x8034, /* __mid */
    } /* __sfxns */
};


/*
 * ======== xdc.runtime.SysMin VTABLE ========
 */

/* Module__FXNS__C */
#pragma DATA_SECTION(xdc_runtime_SysMin_Module__FXNS__C, ".const:xdc_runtime_SysMin_Module__FXNS__C");
const xdc_runtime_SysMin_Fxns__ xdc_runtime_SysMin_Module__FXNS__C = {
    &xdc_runtime_ISystemSupport_Interface__BASE__C, /* __base */
    &xdc_runtime_SysMin_Module__FXNS__C.__sfxns, /* __sysp */
    xdc_runtime_SysMin_abort__E,
    xdc_runtime_SysMin_exit__E,
    xdc_runtime_SysMin_flush__E,
    xdc_runtime_SysMin_putch__E,
    xdc_runtime_SysMin_ready__E,
    {
        NULL, /* __create */
        NULL, /* __delete */
        NULL, /* __label */
        0x800e, /* __mid */
    } /* __sfxns */
};


/*
 * ======== xdc.runtime.SysStd VTABLE ========
 */

/* Module__FXNS__C */
#pragma DATA_SECTION(xdc_runtime_SysStd_Module__FXNS__C, ".const:xdc_runtime_SysStd_Module__FXNS__C");
const xdc_runtime_SysStd_Fxns__ xdc_runtime_SysStd_Module__FXNS__C = {
    &xdc_runtime_ISystemSupport_Interface__BASE__C, /* __base */
    &xdc_runtime_SysStd_Module__FXNS__C.__sfxns, /* __sysp */
    xdc_runtime_SysStd_abort__E,
    xdc_runtime_SysStd_exit__E,
    xdc_runtime_SysStd_flush__E,
    xdc_runtime_SysStd_putch__E,
    xdc_runtime_SysStd_ready__E,
    {
        NULL, /* __create */
        NULL, /* __delete */
        NULL, /* __label */
        0x800f, /* __mid */
    } /* __sfxns */
};


/*
 * ======== xdc.runtime.Timestamp VTABLE ========
 */

/* Module__FXNS__C */
#pragma DATA_SECTION(xdc_runtime_Timestamp_Module__FXNS__C, ".const:xdc_runtime_Timestamp_Module__FXNS__C");
const xdc_runtime_Timestamp_Fxns__ xdc_runtime_Timestamp_Module__FXNS__C = {
    &xdc_runtime_ITimestampClient_Interface__BASE__C, /* __base */
    &xdc_runtime_Timestamp_Module__FXNS__C.__sfxns, /* __sysp */
    xdc_runtime_Timestamp_get32__E,
    xdc_runtime_Timestamp_get64__E,
    xdc_runtime_Timestamp_getFreq__E,
    {
        NULL, /* __create */
        NULL, /* __delete */
        NULL, /* __label */
        0x8011, /* __mid */
    } /* __sfxns */
};


/*
 * ======== xdc.runtime.TimestampNull VTABLE ========
 */

/* Module__FXNS__C */
#pragma DATA_SECTION(xdc_runtime_TimestampNull_Module__FXNS__C, ".const:xdc_runtime_TimestampNull_Module__FXNS__C");
const xdc_runtime_TimestampNull_Fxns__ xdc_runtime_TimestampNull_Module__FXNS__C = {
    &xdc_runtime_ITimestampProvider_Interface__BASE__C, /* __base */
    &xdc_runtime_TimestampNull_Module__FXNS__C.__sfxns, /* __sysp */
    xdc_runtime_TimestampNull_get32__E,
    xdc_runtime_TimestampNull_get64__E,
    xdc_runtime_TimestampNull_getFreq__E,
    {
        NULL, /* __create */
        NULL, /* __delete */
        NULL, /* __label */
        0x8012, /* __mid */
    } /* __sfxns */
};


/*
 * ======== xdc.runtime.knl.GateThread VTABLE ========
 */

/* Module__FXNS__C */
#pragma DATA_SECTION(xdc_runtime_knl_GateThread_Module__FXNS__C, ".const:xdc_runtime_knl_GateThread_Module__FXNS__C");
const xdc_runtime_knl_GateThread_Fxns__ xdc_runtime_knl_GateThread_Module__FXNS__C = {
    &xdc_runtime_IGateProvider_Interface__BASE__C, /* __base */
    &xdc_runtime_knl_GateThread_Module__FXNS__C.__sfxns, /* __sysp */
    xdc_runtime_knl_GateThread_query__E,
    xdc_runtime_knl_GateThread_enter__E,
    xdc_runtime_knl_GateThread_leave__E,
    {
        xdc_runtime_knl_GateThread_Object__create__S,
        xdc_runtime_knl_GateThread_Object__delete__S,
        xdc_runtime_knl_GateThread_Handle__label__S,
        0x802a, /* __mid */
    } /* __sfxns */
};


/*
 * ======== ti.sysbios.BIOS DECLARATIONS ========
 */

/* Module_State__ */
typedef struct ti_sysbios_BIOS_Module_State__ {
    xdc_runtime_Types_FreqHz cpuFreq;
    xdc_UInt rtsGateCount;
    xdc_IArg rtsGateKey;
    ti_sysbios_BIOS_RtsGateProxy_Handle rtsGate;
    ti_sysbios_BIOS_ThreadType threadType;
    __TA_ti_sysbios_BIOS_Module_State__smpThreadType smpThreadType;
    volatile ti_sysbios_BIOS_StartFuncPtr startFunc;
    volatile ti_sysbios_BIOS_ExitFuncPtr exitFunc;
} ti_sysbios_BIOS_Module_State__;

/* --> ti_sysbios_BIOS_startFunc */
extern xdc_Void ti_sysbios_BIOS_startFunc(xdc_Void);

/* --> ti_sysbios_BIOS_exitFunc */
extern xdc_Void ti_sysbios_BIOS_exitFunc(xdc_Int f_arg0);

/* Module__state__V */
ti_sysbios_BIOS_Module_State__ ti_sysbios_BIOS_Module__state__V;


/*
 * ======== ti.sysbios.BIOS_RtsGateProxy DECLARATIONS ========
 */


/*
 * ======== ti.sysbios.family.c64p.tci6488.TimerSupport DECLARATIONS ========
 */


/*
 * ======== ti.sysbios.family.c7x.Cache DECLARATIONS ========
 */

/* Module_State__ */
typedef struct ti_sysbios_family_c7x_Cache_Module_State__ {
    xdc_UInt64 L1DCFG;
    xdc_UInt64 L2CFG;
} ti_sysbios_family_c7x_Cache_Module_State__;

/* Module__state__V */
ti_sysbios_family_c7x_Cache_Module_State__ ti_sysbios_family_c7x_Cache_Module__state__V;


/*
 * ======== ti.sysbios.family.c7x.Exception DECLARATIONS ========
 */

/* Module_State__ */
typedef struct ti_sysbios_family_c7x_Exception_Module_State__ {
    xdc_Bits64 nrp;
    xdc_Bits64 ntsr;
    xdc_Bits64 ierr;
    xdc_Bits64 iear;
    xdc_Bits64 iesr;
    ti_sysbios_family_c7x_Exception_FuncPtr returnHook;
    ti_sysbios_family_c7x_Exception_Context *excContext;
    xdc_Char *excPtr;
    __TA_ti_sysbios_family_c7x_Exception_Module_State__contextBuf contextBuf;
} ti_sysbios_family_c7x_Exception_Module_State__;

/* Module__state__V */
ti_sysbios_family_c7x_Exception_Module_State__ ti_sysbios_family_c7x_Exception_Module__state__V;


/*
 * ======== ti.sysbios.family.c7x.Hwi DECLARATIONS ========
 */

/* --> ti_sysbios_family_c7x_Hwi_Instance_State_0_hookEnv__A */
__T1_ti_sysbios_family_c7x_Hwi_Instance_State__hookEnv ti_sysbios_family_c7x_Hwi_Instance_State_0_hookEnv__A[1];

/* Object__table__V */
ti_sysbios_family_c7x_Hwi_Object__ ti_sysbios_family_c7x_Hwi_Object__table__V[1];

/* Module_State__ */
typedef struct ti_sysbios_family_c7x_Hwi_Module_State__ {
    __TA_ti_sysbios_family_c7x_Hwi_Module_State__intEvents intEvents;
    xdc_ULong ierMask;
    volatile xdc_Int intNum;
    xdc_Char *taskSP;
    xdc_Char *isrStack;
    xdc_Int scw;
    __TA_ti_sysbios_family_c7x_Hwi_Module_State__dispatchTable dispatchTable;
} ti_sysbios_family_c7x_Hwi_Module_State__;

/* Module__state__V */
ti_sysbios_family_c7x_Hwi_Module_State__ ti_sysbios_family_c7x_Hwi_Module__state__V;

/* --> soft_reset */
extern void* soft_reset;

/* --> secure_soft_reset */
extern void* secure_soft_reset;

/* --> ti_sysbios_knl_Swi_disable__E */
extern xdc_UInt ti_sysbios_knl_Swi_disable__E(xdc_Void);

/* --> ti_sysbios_knl_Swi_restoreHwi__E */
extern xdc_Void ti_sysbios_knl_Swi_restoreHwi__E(xdc_UInt f_arg0);

/* --> ti_sysbios_knl_Task_disable__E */
extern xdc_UInt ti_sysbios_knl_Task_disable__E(xdc_Void);

/* --> ti_sysbios_knl_Task_restoreHwi__E */
extern xdc_Void ti_sysbios_knl_Task_restoreHwi__E(xdc_UInt f_arg0);

/* --> ti_sysbios_family_c7x_Hwi_hooks__A */
const __T1_ti_sysbios_family_c7x_Hwi_hooks ti_sysbios_family_c7x_Hwi_hooks__A[1];


/*
 * ======== ti.sysbios.family.c7x.IntrinsicsSupport DECLARATIONS ========
 */


/*
 * ======== ti.sysbios.family.c7x.Mmu DECLARATIONS ========
 */

/* Module_State__ */
typedef struct ti_sysbios_family_c7x_Mmu_Module_State__ {
} ti_sysbios_family_c7x_Mmu_Module_State__;

/* Module__state__V */
ti_sysbios_family_c7x_Mmu_Module_State__ ti_sysbios_family_c7x_Mmu_Module__state__V;

/* --> tidlMmuInit */
extern xdc_Void tidlMmuInit(xdc_Void);


/*
 * ======== ti.sysbios.family.c7x.TaskSupport DECLARATIONS ========
 */


/*
 * ======== ti.sysbios.family.c7x.TimestampProvider DECLARATIONS ========
 */


/*
 * ======== ti.sysbios.gates.GateHwi DECLARATIONS ========
 */

/* Object__table__V */
ti_sysbios_gates_GateHwi_Object__ ti_sysbios_gates_GateHwi_Object__table__V[1];


/*
 * ======== ti.sysbios.gates.GateMutex DECLARATIONS ========
 */

/* Object__table__V */
ti_sysbios_gates_GateMutex_Object__ ti_sysbios_gates_GateMutex_Object__table__V[2];


/*
 * ======== ti.sysbios.gates.GateMutexPri DECLARATIONS ========
 */


/*
 * ======== ti.sysbios.gates.GateSwi DECLARATIONS ========
 */


/*
 * ======== ti.sysbios.gates.GateTask DECLARATIONS ========
 */


/*
 * ======== ti.sysbios.hal.Cache DECLARATIONS ========
 */


/*
 * ======== ti.sysbios.hal.Cache_CacheProxy DECLARATIONS ========
 */


/*
 * ======== ti.sysbios.hal.Core DECLARATIONS ========
 */


/*
 * ======== ti.sysbios.hal.CoreNull DECLARATIONS ========
 */


/*
 * ======== ti.sysbios.hal.Core_CoreProxy DECLARATIONS ========
 */


/*
 * ======== ti.sysbios.hal.Hwi DECLARATIONS ========
 */

/* Object__table__V */
ti_sysbios_hal_Hwi_Object__ ti_sysbios_hal_Hwi_Object__table__V[1];


/*
 * ======== ti.sysbios.hal.Hwi_HwiProxy DECLARATIONS ========
 */


/*
 * ======== ti.sysbios.heaps.HeapBuf DECLARATIONS ========
 */

/* Module_State__ */
typedef struct ti_sysbios_heaps_HeapBuf_Module_State__ {
    __TA_ti_sysbios_heaps_HeapBuf_Module_State__constructedHeaps constructedHeaps;
} ti_sysbios_heaps_HeapBuf_Module_State__;

/* Module__state__V */
ti_sysbios_heaps_HeapBuf_Module_State__ ti_sysbios_heaps_HeapBuf_Module__state__V;


/*
 * ======== ti.sysbios.heaps.HeapMem DECLARATIONS ========
 */

/* --> ti_sysbios_heaps_HeapMem_Instance_State_0_buf__A */
__T1_ti_sysbios_heaps_HeapMem_Instance_State__buf ti_sysbios_heaps_HeapMem_Instance_State_0_buf__A[503316480];
#if !(defined(__MACH__) && defined(__APPLE__))
__T1_ti_sysbios_heaps_HeapMem_Instance_State__buf ti_sysbios_heaps_HeapMem_Instance_State_0_buf__A[503316480] __attribute__ ((section(".sysmem"), aligned(16)));
#endif

/* Object__table__V */
ti_sysbios_heaps_HeapMem_Object__ ti_sysbios_heaps_HeapMem_Object__table__V[1];


/*
 * ======== ti.sysbios.heaps.HeapMem_Module_GateProxy DECLARATIONS ========
 */


/*
 * ======== ti.sysbios.knl.Clock DECLARATIONS ========
 */

/* Module_State__ */
typedef struct ti_sysbios_knl_Clock_Module_State__ {
    volatile xdc_UInt32 ticks;
    xdc_UInt swiCount;
    ti_sysbios_knl_Clock_TimerProxy_Handle timer;
    ti_sysbios_knl_Swi_Handle swi;
    volatile xdc_UInt numTickSkip;
    xdc_UInt32 nextScheduledTick;
    xdc_UInt32 maxSkippable;
    xdc_Bool inWorkFunc;
    volatile xdc_Bool startDuringWorkFunc;
    xdc_Bool ticking;
    ti_sysbios_knl_Queue_Object__ Object_field_clockQ;
} ti_sysbios_knl_Clock_Module_State__;

/* Module__state__V */
ti_sysbios_knl_Clock_Module_State__ ti_sysbios_knl_Clock_Module__state__V;

/* --> ti_sysbios_knl_Clock_doTick__I */
extern xdc_Void ti_sysbios_knl_Clock_doTick__I(xdc_UArg f_arg0);


/*
 * ======== ti.sysbios.knl.Clock_TimerProxy DECLARATIONS ========
 */


/*
 * ======== ti.sysbios.knl.Event DECLARATIONS ========
 */


/*
 * ======== ti.sysbios.knl.Idle DECLARATIONS ========
 */

/* --> tidlIdleLoop */
extern xdc_Void tidlIdleLoop(xdc_Void);

/* --> ti_sysbios_knl_Idle_funcList__A */
const __T1_ti_sysbios_knl_Idle_funcList ti_sysbios_knl_Idle_funcList__A[2];

/* --> ti_sysbios_knl_Idle_coreList__A */
const __T1_ti_sysbios_knl_Idle_coreList ti_sysbios_knl_Idle_coreList__A[2];


/*
 * ======== ti.sysbios.knl.Intrinsics DECLARATIONS ========
 */


/*
 * ======== ti.sysbios.knl.Intrinsics_SupportProxy DECLARATIONS ========
 */


/*
 * ======== ti.sysbios.knl.Queue DECLARATIONS ========
 */


/*
 * ======== ti.sysbios.knl.Semaphore DECLARATIONS ========
 */


/*
 * ======== ti.sysbios.knl.Swi DECLARATIONS ========
 */

/* --> ti_sysbios_knl_Swi_Instance_State_0_hookEnv__A */
__T1_ti_sysbios_knl_Swi_Instance_State__hookEnv ti_sysbios_knl_Swi_Instance_State_0_hookEnv__A[1];

/* Object__table__V */
ti_sysbios_knl_Swi_Object__ ti_sysbios_knl_Swi_Object__table__V[1];

/* Module_State__ */
typedef struct ti_sysbios_knl_Swi_Module_State__ {
    volatile xdc_Bool locked;
    xdc_UInt curSet;
    xdc_UInt curTrigger;
    ti_sysbios_knl_Swi_Handle curSwi;
    ti_sysbios_knl_Queue_Handle curQ;
    __TA_ti_sysbios_knl_Swi_Module_State__readyQ readyQ;
    __TA_ti_sysbios_knl_Swi_Module_State__constructedSwis constructedSwis;
} ti_sysbios_knl_Swi_Module_State__;

/* --> ti_sysbios_knl_Swi_Module_State_0_readyQ__A */
__T1_ti_sysbios_knl_Swi_Module_State__readyQ ti_sysbios_knl_Swi_Module_State_0_readyQ__A[16];

/* Module__state__V */
ti_sysbios_knl_Swi_Module_State__ ti_sysbios_knl_Swi_Module__state__V;

/* --> ti_sysbios_knl_Swi_hooks__A */
const __T1_ti_sysbios_knl_Swi_hooks ti_sysbios_knl_Swi_hooks__A[1];

/* --> ti_sysbios_knl_Task_disable__E */
extern xdc_UInt ti_sysbios_knl_Task_disable__E(xdc_Void);

/* --> ti_sysbios_knl_Task_restore__E */
extern xdc_Void ti_sysbios_knl_Task_restore__E(xdc_UInt f_arg0);


/*
 * ======== ti.sysbios.knl.Task DECLARATIONS ========
 */

/* --> ti_sysbios_knl_Task_Instance_State_0_stack__A */
__T1_ti_sysbios_knl_Task_Instance_State__stack ti_sysbios_knl_Task_Instance_State_0_stack__A[16384];
#if !(defined(__MACH__) && defined(__APPLE__))
__T1_ti_sysbios_knl_Task_Instance_State__stack ti_sysbios_knl_Task_Instance_State_0_stack__A[16384] __attribute__ ((section(".bss:taskStackSection"), aligned(8192)));
#endif

/* --> ti_sysbios_knl_Task_Instance_State_0_hookEnv__A */
__T1_ti_sysbios_knl_Task_Instance_State__hookEnv ti_sysbios_knl_Task_Instance_State_0_hookEnv__A[1];

/* Object__table__V */
ti_sysbios_knl_Task_Object__ ti_sysbios_knl_Task_Object__table__V[1];

/* Module_State__ */
typedef struct ti_sysbios_knl_Task_Module_State__ {
    volatile xdc_Bool locked;
    volatile xdc_UInt curSet;
    volatile xdc_Bool workFlag;
    xdc_UInt vitalTasks;
    ti_sysbios_knl_Task_Handle curTask;
    ti_sysbios_knl_Queue_Handle curQ;
    __TA_ti_sysbios_knl_Task_Module_State__readyQ readyQ;
    __TA_ti_sysbios_knl_Task_Module_State__smpCurSet smpCurSet;
    __TA_ti_sysbios_knl_Task_Module_State__smpCurMask smpCurMask;
    __TA_ti_sysbios_knl_Task_Module_State__smpCurTask smpCurTask;
    __TA_ti_sysbios_knl_Task_Module_State__smpReadyQ smpReadyQ;
    __TA_ti_sysbios_knl_Task_Module_State__idleTask idleTask;
    __TA_ti_sysbios_knl_Task_Module_State__constructedTasks constructedTasks;
    xdc_Bool curTaskPrivileged;
    ti_sysbios_knl_Queue_Object__ Object_field_inactiveQ;
    ti_sysbios_knl_Queue_Object__ Object_field_terminatedQ;
} ti_sysbios_knl_Task_Module_State__;

/* --> ti_sysbios_knl_Task_Module_State_0_readyQ__A */
__T1_ti_sysbios_knl_Task_Module_State__readyQ ti_sysbios_knl_Task_Module_State_0_readyQ__A[16];

/* --> ti_sysbios_knl_Task_Module_State_0_idleTask__A */
__T1_ti_sysbios_knl_Task_Module_State__idleTask ti_sysbios_knl_Task_Module_State_0_idleTask__A[1];

/* Module__state__V */
ti_sysbios_knl_Task_Module_State__ ti_sysbios_knl_Task_Module__state__V;

/* --> ti_sysbios_utils_Load_taskRegHook__E */
extern xdc_Void ti_sysbios_utils_Load_taskRegHook__E(xdc_Int f_arg0);

/* --> ti_sysbios_knl_Task_hooks__A */
const __T1_ti_sysbios_knl_Task_hooks ti_sysbios_knl_Task_hooks__A[1];


/*
 * ======== ti.sysbios.knl.Task_SupportProxy DECLARATIONS ========
 */


/*
 * ======== ti.sysbios.syncs.SyncSem DECLARATIONS ========
 */


/*
 * ======== ti.sysbios.timers.dmtimer.Timer DECLARATIONS ========
 */

/* Object__table__V */
ti_sysbios_timers_dmtimer_Timer_Object__ ti_sysbios_timers_dmtimer_Timer_Object__table__V[1];

/* Module_State__ */
typedef struct ti_sysbios_timers_dmtimer_Timer_Module_State__ {
    xdc_Bits32 availMask;
    __TA_ti_sysbios_timers_dmtimer_Timer_Module_State__intFreqs intFreqs;
    __TA_ti_sysbios_timers_dmtimer_Timer_Module_State__device device;
    __TA_ti_sysbios_timers_dmtimer_Timer_Module_State__handles handles;
    xdc_Bool firstInit;
} ti_sysbios_timers_dmtimer_Timer_Module_State__;

/* --> ti_sysbios_timers_dmtimer_Timer_Module_State_0_intFreqs__A */
__T1_ti_sysbios_timers_dmtimer_Timer_Module_State__intFreqs ti_sysbios_timers_dmtimer_Timer_Module_State_0_intFreqs__A[4];

/* --> ti_sysbios_timers_dmtimer_Timer_Module_State_0_device__A */
__T1_ti_sysbios_timers_dmtimer_Timer_Module_State__device ti_sysbios_timers_dmtimer_Timer_Module_State_0_device__A[4];

/* --> ti_sysbios_timers_dmtimer_Timer_Module_State_0_handles__A */
__T1_ti_sysbios_timers_dmtimer_Timer_Module_State__handles ti_sysbios_timers_dmtimer_Timer_Module_State_0_handles__A[4];

/* Module__state__V */
ti_sysbios_timers_dmtimer_Timer_Module_State__ ti_sysbios_timers_dmtimer_Timer_Module__state__V;


/*
 * ======== ti.sysbios.timers.dmtimer.Timer_TimerSupportProxy DECLARATIONS ========
 */


/*
 * ======== ti.sysbios.utils.Load DECLARATIONS ========
 */

/* Module_State__ */
typedef struct ti_sysbios_utils_Load_Module_State__ {
    xdc_Int taskHId;
    __TA_ti_sysbios_utils_Load_Module_State__taskStartTime taskStartTime;
    xdc_UInt32 timeElapsed;
    __TA_ti_sysbios_utils_Load_Module_State__runningTask runningTask;
    xdc_Bool firstSwitchDone;
    xdc_UInt32 swiStartTime;
    ti_sysbios_utils_Load_HookContext swiEnv;
    __TA_ti_sysbios_utils_Load_Module_State__taskEnv taskEnv;
    xdc_UInt32 swiCnt;
    xdc_UInt32 hwiStartTime;
    ti_sysbios_utils_Load_HookContext hwiEnv;
    xdc_UInt32 hwiCnt;
    xdc_UInt32 timeSlotCnt;
    xdc_UInt32 minLoop;
    xdc_UInt32 minIdle;
    xdc_UInt32 t0;
    xdc_UInt32 idleCnt;
    xdc_UInt32 cpuLoad;
    xdc_UInt32 taskEnvLen;
    xdc_UInt32 taskNum;
    xdc_Bool powerEnabled;
    xdc_UInt32 idleStartTime;
    xdc_UInt32 busyStartTime;
    xdc_UInt32 busyTime;
    ti_sysbios_knl_Queue_Object__ Object_field_taskList;
} ti_sysbios_utils_Load_Module_State__;

/* --> ti_sysbios_utils_Load_Module_State_0_taskStartTime__A */
__T1_ti_sysbios_utils_Load_Module_State__taskStartTime ti_sysbios_utils_Load_Module_State_0_taskStartTime__A[1];

/* --> ti_sysbios_utils_Load_Module_State_0_runningTask__A */
__T1_ti_sysbios_utils_Load_Module_State__runningTask ti_sysbios_utils_Load_Module_State_0_runningTask__A[1];

/* --> ti_sysbios_utils_Load_Module_State_0_taskEnv__A */
__T1_ti_sysbios_utils_Load_Module_State__taskEnv ti_sysbios_utils_Load_Module_State_0_taskEnv__A[1];

/* Module__state__V */
ti_sysbios_utils_Load_Module_State__ ti_sysbios_utils_Load_Module__state__V;

/* --> tidlPerfStatsBiosLoadUpdate */
extern xdc_Void tidlPerfStatsBiosLoadUpdate(xdc_Void);


/*
 * ======== ti.sysbios.xdcruntime.GateThreadSupport DECLARATIONS ========
 */


/*
 * ======== xdc.runtime.Assert DECLARATIONS ========
 */


/*
 * ======== xdc.runtime.Core DECLARATIONS ========
 */


/*
 * ======== xdc.runtime.Defaults DECLARATIONS ========
 */


/*
 * ======== xdc.runtime.Diags DECLARATIONS ========
 */


/*
 * ======== xdc.runtime.Error DECLARATIONS ========
 */

/* Module_State__ */
typedef struct xdc_runtime_Error_Module_State__ {
    xdc_UInt16 count;
} xdc_runtime_Error_Module_State__;

/* Module__state__V */
xdc_runtime_Error_Module_State__ xdc_runtime_Error_Module__state__V;


/*
 * ======== xdc.runtime.Gate DECLARATIONS ========
 */


/*
 * ======== xdc.runtime.Log DECLARATIONS ========
 */


/*
 * ======== xdc.runtime.Main DECLARATIONS ========
 */


/*
 * ======== xdc.runtime.Main_Module_GateProxy DECLARATIONS ========
 */


/*
 * ======== xdc.runtime.Memory DECLARATIONS ========
 */

/* Module_State__ */
typedef struct xdc_runtime_Memory_Module_State__ {
    xdc_SizeT maxDefaultTypeAlign;
} xdc_runtime_Memory_Module_State__;

/* Module__state__V */
xdc_runtime_Memory_Module_State__ xdc_runtime_Memory_Module__state__V;


/*
 * ======== xdc.runtime.Memory_HeapProxy DECLARATIONS ========
 */


/*
 * ======== xdc.runtime.Registry DECLARATIONS ========
 */

/* Module_State__ */
typedef struct xdc_runtime_Registry_Module_State__ {
    xdc_runtime_Registry_Desc *listHead;
    xdc_runtime_Types_ModuleId curId;
} xdc_runtime_Registry_Module_State__;

/* Module__state__V */
xdc_runtime_Registry_Module_State__ xdc_runtime_Registry_Module__state__V;


/*
 * ======== xdc.runtime.Startup DECLARATIONS ========
 */

/* Module_State__ */
typedef struct xdc_runtime_Startup_Module_State__ {
    xdc_Int *stateTab;
    xdc_Bool execFlag;
    xdc_Bool rtsDoneFlag;
} xdc_runtime_Startup_Module_State__;

/* Module__state__V */
xdc_runtime_Startup_Module_State__ xdc_runtime_Startup_Module__state__V;

/* --> ti_sysbios_hal_Hwi_initStack */
extern xdc_Void ti_sysbios_hal_Hwi_initStack(xdc_Void);

/* --> xdc_runtime_Startup_firstFxns__A */
const __T1_xdc_runtime_Startup_firstFxns xdc_runtime_Startup_firstFxns__A[2];

/* --> xdc_runtime_System_Module_startup__E */
extern xdc_Int xdc_runtime_System_Module_startup__E(xdc_Int f_arg0);

/* --> xdc_runtime_SysMin_Module_startup__E */
extern xdc_Int xdc_runtime_SysMin_Module_startup__E(xdc_Int f_arg0);

/* --> ti_sysbios_family_c7x_Cache_Module_startup__E */
extern xdc_Int ti_sysbios_family_c7x_Cache_Module_startup__E(xdc_Int f_arg0);

/* --> ti_sysbios_family_c7x_Exception_Module_startup__E */
extern xdc_Int ti_sysbios_family_c7x_Exception_Module_startup__E(xdc_Int f_arg0);

/* --> ti_sysbios_family_c7x_Hwi_Module_startup__E */
extern xdc_Int ti_sysbios_family_c7x_Hwi_Module_startup__E(xdc_Int f_arg0);

/* --> ti_sysbios_family_c7x_TimestampProvider_Module_startup__E */
extern xdc_Int ti_sysbios_family_c7x_TimestampProvider_Module_startup__E(xdc_Int f_arg0);

/* --> ti_sysbios_knl_Clock_Module_startup__E */
extern xdc_Int ti_sysbios_knl_Clock_Module_startup__E(xdc_Int f_arg0);

/* --> ti_sysbios_knl_Swi_Module_startup__E */
extern xdc_Int ti_sysbios_knl_Swi_Module_startup__E(xdc_Int f_arg0);

/* --> ti_sysbios_knl_Task_Module_startup__E */
extern xdc_Int ti_sysbios_knl_Task_Module_startup__E(xdc_Int f_arg0);

/* --> ti_sysbios_hal_Hwi_Module_startup__E */
extern xdc_Int ti_sysbios_hal_Hwi_Module_startup__E(xdc_Int f_arg0);

/* --> ti_sysbios_heaps_HeapBuf_Module_startup__E */
extern xdc_Int ti_sysbios_heaps_HeapBuf_Module_startup__E(xdc_Int f_arg0);

/* --> ti_sysbios_timers_dmtimer_Timer_Module_startup__E */
extern xdc_Int ti_sysbios_timers_dmtimer_Timer_Module_startup__E(xdc_Int f_arg0);

/* --> xdc_runtime_Startup_sfxnTab__A */
const __T1_xdc_runtime_Startup_sfxnTab xdc_runtime_Startup_sfxnTab__A[12];

/* --> xdc_runtime_Startup_sfxnRts__A */
const __T1_xdc_runtime_Startup_sfxnRts xdc_runtime_Startup_sfxnRts__A[12];


/*
 * ======== xdc.runtime.SysMin DECLARATIONS ========
 */

/* Module_State__ */
typedef struct xdc_runtime_SysMin_Module_State__ {
    __TA_xdc_runtime_SysMin_Module_State__outbuf outbuf;
    xdc_UInt outidx;
    xdc_Bool wrapped;
} xdc_runtime_SysMin_Module_State__;

/* --> xdc_runtime_SysMin_Module_State_0_outbuf__A */
__T1_xdc_runtime_SysMin_Module_State__outbuf xdc_runtime_SysMin_Module_State_0_outbuf__A[524288];

/* Module__state__V */
xdc_runtime_SysMin_Module_State__ xdc_runtime_SysMin_Module__state__V;

/* --> xdc_runtime_SysMin_output__I */
extern xdc_Void xdc_runtime_SysMin_output__I(xdc_Char* f_arg0,xdc_UInt f_arg1);


/*
 * ======== xdc.runtime.SysStd DECLARATIONS ========
 */


/*
 * ======== xdc.runtime.System DECLARATIONS ========
 */

/* Module_State__ */
typedef struct xdc_runtime_System_Module_State__ {
    __TA_xdc_runtime_System_Module_State__atexitHandlers atexitHandlers;
    xdc_Int numAtexitHandlers;
} xdc_runtime_System_Module_State__;

/* --> xdc_runtime_System_Module_State_0_atexitHandlers__A */
__T1_xdc_runtime_System_Module_State__atexitHandlers xdc_runtime_System_Module_State_0_atexitHandlers__A[8];

/* Module__state__V */
xdc_runtime_System_Module_State__ xdc_runtime_System_Module__state__V;

/* --> xdc_runtime_System_printfExtend__I */
extern xdc_Int xdc_runtime_System_printfExtend__I(xdc_Char** f_arg0,xdc_CString* f_arg1,xdc_VaList* f_arg2,xdc_runtime_System_ParseData* f_arg3);


/*
 * ======== xdc.runtime.System_Module_GateProxy DECLARATIONS ========
 */


/*
 * ======== xdc.runtime.System_SupportProxy DECLARATIONS ========
 */


/*
 * ======== xdc.runtime.Text DECLARATIONS ========
 */

/* Module_State__ */
typedef struct xdc_runtime_Text_Module_State__ {
    xdc_CPtr charBase;
    xdc_CPtr nodeBase;
} xdc_runtime_Text_Module_State__;

/* Module__state__V */
xdc_runtime_Text_Module_State__ xdc_runtime_Text_Module__state__V;

/* --> xdc_runtime_Text_charTab__A */
const __T1_xdc_runtime_Text_charTab xdc_runtime_Text_charTab__A[7434];

/* --> xdc_runtime_Text_nodeTab__A */
const __T1_xdc_runtime_Text_nodeTab xdc_runtime_Text_nodeTab__A[69];


/*
 * ======== xdc.runtime.Timestamp DECLARATIONS ========
 */


/*
 * ======== xdc.runtime.TimestampNull DECLARATIONS ========
 */


/*
 * ======== xdc.runtime.Timestamp_SupportProxy DECLARATIONS ========
 */


/*
 * ======== xdc.runtime.Types DECLARATIONS ========
 */


/*
 * ======== xdc.runtime.knl.GateH DECLARATIONS ========
 */


/*
 * ======== xdc.runtime.knl.GateH_Proxy DECLARATIONS ========
 */


/*
 * ======== xdc.runtime.knl.GateThread DECLARATIONS ========
 */


/*
 * ======== xdc.runtime.knl.GateThread_Proxy DECLARATIONS ========
 */


/*
 * ======== OBJECT OFFSETS ========
 */

#pragma DATA_SECTION(ti_sysbios_gates_GateMutex_Instance_State_sem__O, ".const:ti_sysbios_gates_GateMutex_Instance_State_sem__O");
__FAR__ const xdc_SizeT ti_sysbios_gates_GateMutex_Instance_State_sem__O = offsetof(ti_sysbios_gates_GateMutex_Object__,Object_field_sem);


/*
 * ======== OBJECT OFFSETS ========
 */

#pragma DATA_SECTION(ti_sysbios_gates_GateMutexPri_Instance_State_pendQ__O, ".const:ti_sysbios_gates_GateMutexPri_Instance_State_pendQ__O");
__FAR__ const xdc_SizeT ti_sysbios_gates_GateMutexPri_Instance_State_pendQ__O = offsetof(ti_sysbios_gates_GateMutexPri_Object__,Object_field_pendQ);


/*
 * ======== OBJECT OFFSETS ========
 */

#pragma DATA_SECTION(ti_sysbios_heaps_HeapBuf_Instance_State_freeList__O, ".const:ti_sysbios_heaps_HeapBuf_Instance_State_freeList__O");
__FAR__ const xdc_SizeT ti_sysbios_heaps_HeapBuf_Instance_State_freeList__O = offsetof(ti_sysbios_heaps_HeapBuf_Object__,Object_field_freeList);


/*
 * ======== OBJECT OFFSETS ========
 */

#pragma DATA_SECTION(ti_sysbios_knl_Clock_Module_State_clockQ__O, ".const:ti_sysbios_knl_Clock_Module_State_clockQ__O");
__FAR__ const xdc_SizeT ti_sysbios_knl_Clock_Module_State_clockQ__O = offsetof(ti_sysbios_knl_Clock_Module_State__,Object_field_clockQ);


/*
 * ======== OBJECT OFFSETS ========
 */

#pragma DATA_SECTION(ti_sysbios_knl_Event_Instance_State_pendQ__O, ".const:ti_sysbios_knl_Event_Instance_State_pendQ__O");
__FAR__ const xdc_SizeT ti_sysbios_knl_Event_Instance_State_pendQ__O = offsetof(ti_sysbios_knl_Event_Object__,Object_field_pendQ);


/*
 * ======== OBJECT OFFSETS ========
 */

#pragma DATA_SECTION(ti_sysbios_knl_Semaphore_Instance_State_pendQ__O, ".const:ti_sysbios_knl_Semaphore_Instance_State_pendQ__O");
__FAR__ const xdc_SizeT ti_sysbios_knl_Semaphore_Instance_State_pendQ__O = offsetof(ti_sysbios_knl_Semaphore_Object__,Object_field_pendQ);


/*
 * ======== OBJECT OFFSETS ========
 */

#pragma DATA_SECTION(ti_sysbios_knl_Task_Module_State_inactiveQ__O, ".const:ti_sysbios_knl_Task_Module_State_inactiveQ__O");
__FAR__ const xdc_SizeT ti_sysbios_knl_Task_Module_State_inactiveQ__O = offsetof(ti_sysbios_knl_Task_Module_State__,Object_field_inactiveQ);
#pragma DATA_SECTION(ti_sysbios_knl_Task_Module_State_terminatedQ__O, ".const:ti_sysbios_knl_Task_Module_State_terminatedQ__O");
__FAR__ const xdc_SizeT ti_sysbios_knl_Task_Module_State_terminatedQ__O = offsetof(ti_sysbios_knl_Task_Module_State__,Object_field_terminatedQ);


/*
 * ======== OBJECT OFFSETS ========
 */

#pragma DATA_SECTION(ti_sysbios_utils_Load_Module_State_taskList__O, ".const:ti_sysbios_utils_Load_Module_State_taskList__O");
__FAR__ const xdc_SizeT ti_sysbios_utils_Load_Module_State_taskList__O = offsetof(ti_sysbios_utils_Load_Module_State__,Object_field_taskList);


/*
 * ======== OBJECT OFFSETS ========
 */

#pragma DATA_SECTION(ti_sysbios_xdcruntime_GateThreadSupport_Instance_State_gate__O, ".const:ti_sysbios_xdcruntime_GateThreadSupport_Instance_State_gate__O");
__FAR__ const xdc_SizeT ti_sysbios_xdcruntime_GateThreadSupport_Instance_State_gate__O = offsetof(ti_sysbios_xdcruntime_GateThreadSupport_Object__,Object_field_gate);


/*
 * ======== xdc.cfg.Program TEMPLATE ========
 */

/*
 *  ======== __ASM__ ========
 *  Define absolute path prefix for this executable's
 *  configuration generated files.
 */
xdc__META(__ASM__, "@(#)__ASM__ = /home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/tidl_j7_08_00_00_10/ti_dl/test/out/J7_SDK/C71/SYSBIOS/release/module/src.dsp_sdk/configuro/package/cfg/c7x_1_pe71");

/*
 *  ======== __ISA__ ========
 *  Define the ISA of this executable.  This symbol is used by platform
 *  specific "exec" commands that support more than one ISA; e.g., gdb
 */
xdc__META(__ISA__, "@(#)__ISA__ = 71");

/*
 *  ======== __PLAT__ ========
 *  Define the name of the platform that can run this executable.  This
 *  symbol is used by platform independent "exec" commands
 */
xdc__META(__PLAT__, "@(#)__PLAT__ = ti.platforms.tms320C7x");

/*
 *  ======== __TARG__ ========
 *  Define the name of the target used to build this executable.
 */
xdc__META(__TARG__, "@(#)__TARG__ = ti.targets.elf.C71");

/*
 *  ======== __TRDR__ ========
 *  Define the name of the class that can read/parse this executable.
 */
xdc__META(__TRDR__, "@(#)__TRDR__ = xdc.targets.omf.Elf");


/*
 * ======== xdc.cfg.SourceDir TEMPLATE ========
 */



/*
 * ======== xdc.runtime.Diags TEMPLATE ========
 */



/*
 * ======== xdc.runtime.Error TEMPLATE ========
 */


xdc_runtime_Error_Block xdc_runtime_Error_IgnoreBlock = {
    (unsigned short)(~0U), /* unused */
    {
        {0, 0} /* data */
    },
    0,  /* id */
    NULL,  /* msg */
    {
        0,  /* mod */
        NULL,  /* file */
        0   /* line */
    }
};

/*
 *  ======== Error_policyLog ========
 */
Void xdc_runtime_Error_policyLog__I(xdc_runtime_Types_ModuleId mod,
    CString file, Int line, CString msg, IArg arg1, IArg arg2)
{
    /*
     * Log the error, now that we've retrieved the error message.
     *
     * We call Log_put here instead of Log_write so that we can log the
     * caller's module id instead of the Error module's id.
     *
     * In logging this event, we'll use the Error module's mask and logger. We
     * don't have a way to reliably access the caller's diags mask and logger.
     * The caller isn't guaranteed to have a mask on the target, even if they
     * are performing logging.
     */
#undef Module__DGSINCL
#define Module__DGSINCL xdc_runtime_Error_Module__diagsIncluded__C
#undef Module__DGSENAB
#define Module__DGSENAB xdc_runtime_Error_Module__diagsEnabled__C
#undef Module__DGSMASK
#define Module__DGSMASK xdc_runtime_Error_Module__diagsMask__C
#undef Module__LOGOBJ
#define Module__LOGOBJ xdc_runtime_Error_Module__loggerObj__C
#undef Module__LOGFXN8
#define Module__LOGFXN8 xdc_runtime_Error_Module__loggerFxn8__C

    if (xdc_runtime_Diags_query(xdc_runtime_Log_L_error)) {
        xdc_runtime_Log_put8(xdc_runtime_Log_L_error, mod, (IArg) file, line,
                 (IArg) msg, arg1, arg2, 0, 0, 0);
    }
}

/*
 * ======== xdc.runtime.Startup TEMPLATE ========
 */

/*
 *  ======== MODULE STARTUP DONE FUNCTIONS ========
 */
/* REQ_TAG(SYSBIOS-953) */
xdc_Bool xdc_runtime_System_Module__startupDone__F(void); /* keep GCC prototype warning quiet */
xdc_Bool xdc_runtime_System_Module__startupDone__F(void) {
    return (xdc_Bool)((&xdc_runtime_Startup_Module__state__V)->stateTab == NULL || (&xdc_runtime_Startup_Module__state__V)->stateTab[0] < 0);
}
xdc_Bool xdc_runtime_SysMin_Module__startupDone__F(void); /* keep GCC prototype warning quiet */
xdc_Bool xdc_runtime_SysMin_Module__startupDone__F(void) {
    return (xdc_Bool)((&xdc_runtime_Startup_Module__state__V)->stateTab == NULL || (&xdc_runtime_Startup_Module__state__V)->stateTab[1] < 0);
}
xdc_Bool ti_sysbios_family_c7x_Cache_Module__startupDone__F(void); /* keep GCC prototype warning quiet */
xdc_Bool ti_sysbios_family_c7x_Cache_Module__startupDone__F(void) {
    return (xdc_Bool)((&xdc_runtime_Startup_Module__state__V)->stateTab == NULL || (&xdc_runtime_Startup_Module__state__V)->stateTab[2] < 0);
}
xdc_Bool ti_sysbios_family_c7x_Exception_Module__startupDone__F(void); /* keep GCC prototype warning quiet */
xdc_Bool ti_sysbios_family_c7x_Exception_Module__startupDone__F(void) {
    return (xdc_Bool)((&xdc_runtime_Startup_Module__state__V)->stateTab == NULL || (&xdc_runtime_Startup_Module__state__V)->stateTab[3] < 0);
}
xdc_Bool ti_sysbios_family_c7x_Hwi_Module__startupDone__F(void); /* keep GCC prototype warning quiet */
xdc_Bool ti_sysbios_family_c7x_Hwi_Module__startupDone__F(void) {
    return (xdc_Bool)((&xdc_runtime_Startup_Module__state__V)->stateTab == NULL || (&xdc_runtime_Startup_Module__state__V)->stateTab[4] < 0);
}
xdc_Bool ti_sysbios_family_c7x_TimestampProvider_Module__startupDone__F(void); /* keep GCC prototype warning quiet */
xdc_Bool ti_sysbios_family_c7x_TimestampProvider_Module__startupDone__F(void) {
    return (xdc_Bool)((&xdc_runtime_Startup_Module__state__V)->stateTab == NULL || (&xdc_runtime_Startup_Module__state__V)->stateTab[5] < 0);
}
xdc_Bool ti_sysbios_knl_Clock_Module__startupDone__F(void); /* keep GCC prototype warning quiet */
xdc_Bool ti_sysbios_knl_Clock_Module__startupDone__F(void) {
    return (xdc_Bool)((&xdc_runtime_Startup_Module__state__V)->stateTab == NULL || (&xdc_runtime_Startup_Module__state__V)->stateTab[6] < 0);
}
xdc_Bool ti_sysbios_knl_Swi_Module__startupDone__F(void); /* keep GCC prototype warning quiet */
xdc_Bool ti_sysbios_knl_Swi_Module__startupDone__F(void) {
    return (xdc_Bool)((&xdc_runtime_Startup_Module__state__V)->stateTab == NULL || (&xdc_runtime_Startup_Module__state__V)->stateTab[7] < 0);
}
xdc_Bool ti_sysbios_knl_Task_Module__startupDone__F(void); /* keep GCC prototype warning quiet */
xdc_Bool ti_sysbios_knl_Task_Module__startupDone__F(void) {
    return (xdc_Bool)((&xdc_runtime_Startup_Module__state__V)->stateTab == NULL || (&xdc_runtime_Startup_Module__state__V)->stateTab[8] < 0);
}
xdc_Bool ti_sysbios_hal_Hwi_Module__startupDone__F(void); /* keep GCC prototype warning quiet */
xdc_Bool ti_sysbios_hal_Hwi_Module__startupDone__F(void) {
    return (xdc_Bool)((&xdc_runtime_Startup_Module__state__V)->stateTab == NULL || (&xdc_runtime_Startup_Module__state__V)->stateTab[9] < 0);
}
xdc_Bool ti_sysbios_heaps_HeapBuf_Module__startupDone__F(void); /* keep GCC prototype warning quiet */
xdc_Bool ti_sysbios_heaps_HeapBuf_Module__startupDone__F(void) {
    return (xdc_Bool)((&xdc_runtime_Startup_Module__state__V)->stateTab == NULL || (&xdc_runtime_Startup_Module__state__V)->stateTab[10] < 0);
}
xdc_Bool ti_sysbios_timers_dmtimer_Timer_Module__startupDone__F(void); /* keep GCC prototype warning quiet */
xdc_Bool ti_sysbios_timers_dmtimer_Timer_Module__startupDone__F(void) {
    return (xdc_Bool)((&xdc_runtime_Startup_Module__state__V)->stateTab == NULL || (&xdc_runtime_Startup_Module__state__V)->stateTab[11] < 0);
}


/*
 * Startup_exec__I is an internal entry point called by target/platform boot
 * code. Boot code is not brought into a partial-link assembly. So, without this
 * attribute, whole program optimizers would otherwise optimize-out this
 * function.
 */
xdc_Void xdc_runtime_Startup_exec__I(void) __attribute__ ((used));

/*
 *  ======== xdc_runtime_Startup_exec__I ========
 *  Initialize all used modules that have startup functions
 */
/* REQ_TAG(SYSBIOS-949) */
xdc_Void xdc_runtime_Startup_exec__I(void)
{
    xdc_Int state[12];
    xdc_runtime_Startup_startModsFxn__C(state, 12);
}

/*
 * ======== xdc.runtime.Reset TEMPLATE ========
 */

/*
 * Startup_reset__I is an internal entry point called by target/platform boot
 * code. Boot code is not brought into a partial-link assembly. So, without this
 * pragma, whole program optimizers would otherwise optimize-out this function.
 */
xdc_Void xdc_runtime_Startup_reset__I(void) __attribute__ ((used));


/*
 *  ======== xdc_runtime_Startup_reset__I ========
 *  This function is called by bootstrap initialization code as early as
 *  possible in the startup process.  This function calls all functions in
 *  the Reset.fxns array _as well as_ Startup.resetFxn (if it's non-NULL)
 */
xdc_Void xdc_runtime_Startup_reset__I(void)
{
}

/*
 * ======== xdc.runtime.System TEMPLATE ========
 */


#include <xdc/std.h>
#include <limits.h>
#include <xdc/runtime/Types.h>

#include <xdc/runtime/System.h>
#include <xdc/runtime/System__internal.h>

/*
 *  ======== System_printfExtend__I ========
 *  This function processes optional extended formats of printf.
 *
 *  It returns the number of characters added to the result.
 *
 *  Precision (maximum string length) is not supported for %$S.
 *
 *  Right-justified (which is default) minimum width is not supported
 *  for %$L, %$S, or %$F.
 */
xdc_Int xdc_runtime_System_printfExtend__I(xdc_Char **pbuf, xdc_CString *pfmt,
    xdc_VaList *pva, xdc_runtime_System_ParseData *parse)
{
    xdc_Int     res;
    xdc_Char    c;
    xdc_Bool    found = FALSE;

    /*
     * Create a local variable 'va' to ensure that the dereference of
     * pva only occurs once.
     */
    va_list va = *pva;

    res = 0;

    c = **pfmt;
    *pfmt = *pfmt + 1;


    if (c == '$') {
        c = **pfmt;
        *pfmt = *pfmt + 1;

        if (c == 'L') {
            xdc_runtime_Types_Label *lab = (parse->aFlag == TRUE) ?
                (xdc_runtime_Types_Label *)xdc_iargToPtr(va_arg(va, xdc_IArg)) :
                (xdc_runtime_Types_Label *)va_arg(va, void *);

            /*
             * Call Text_putLab to write out the label, taking the precision
             * into account.
             */
            res = xdc_runtime_Text_putLab(lab, pbuf, parse->precis);

            /*
             * Set the length to 0 to indicate to 'doPrint' that nothing should
             * be copied from parse.ptr.
             */
            parse->len = 0;

            /* Update the minimum width field. */
            parse->width -= res;

            found = TRUE;
        }

        if (c == 'F') {
            xdc_runtime_Types_Site site;

            /* Retrieve the file name string from the argument list */
            site.file = (parse->aFlag == TRUE) ?
                (xdc_Char *) xdc_iargToPtr(va_arg(va, xdc_IArg)) :
                (xdc_Char *) va_arg(va, xdc_Char *);

            /* Retrieve the line number from the argument list. */
            site.line = (parse->aFlag == TRUE) ?
                (xdc_Int) va_arg(va, xdc_IArg) :
                (xdc_Int) va_arg(va, xdc_Int);

            /*
             * Omit the 'mod' field, set it to 0.
             * '0' is a safe sentinel value - the IDs for named modules are
             * 0x8000 and higher, and the IDs for unnamed modules go from 0x1
             * to 0x7fff.
             */
            site.mod = 0;

            /*
             * Call putSite to format the file and line number.
             * If a precision was specified, it will be used as the maximum
             * string length.
             */
            res = xdc_runtime_Text_putSite(&site, pbuf, parse->precis);

            /*
             * Set the length to 0 to indicate to 'doPrint' that nothing should
             * be copied from parse.ptr.
             */
            parse->len = 0;

            /* Update the minimum width field */
            parse->width -= res;

            found = TRUE;
        }

        if (c == 'S') {
            /* Retrieve the format string from the argument list */
            parse->ptr = (parse->aFlag == TRUE) ?
                (xdc_Char *) xdc_iargToPtr(va_arg(va, xdc_IArg)) :
                (xdc_Char *) va_arg(va, xdc_Char *);

            /* Update pva before passing it to doPrint. */
            *pva = va;

            /* Perform the recursive format. System_doPrint does not advance
             * the buffer pointer, so it has to be done explicitly.
             * System_doPrint guarantees that parse->precis is positive.
             */
            res = xdc_runtime_System_doPrint__I(*pbuf, (xdc_SizeT)parse->precis,
                                                parse->ptr, pva, parse->aFlag);

            if (*pbuf != NULL) {
                if (res >= parse->precis) {
                    /* Not enough space for all characters, only
                     * (parse->precis - 1) and '\0' were printed.
                     */
                    res = parse->precis - 1;
                }
                *pbuf += res;
            }

            /* Update the temporary variable with any changes to *pva */
            va = *pva;

            /*
             * Set the length to 0 to indicate to 'doPrint' that nothing should
             * be copied from parse.ptr
             */
            parse->len = 0;

            /* Update the minimum width field */
            parse->width -= res;

            /* Indicate that we were able to interpret the specifier */
            found = TRUE;
        }

    }

    if (c == 'f') {
        /* support arguments _after_ optional float support */
        if (parse->aFlag == TRUE) {
            (void)va_arg(va, xdc_IArg);
        }
        else {
            (void)va_arg(va, double);
        }
    }

    if (found == FALSE) {
        /* other character (like %) copy to output */
        *(parse->ptr) = c;
        parse->len = 1;
    }

    /*
     * Before returning, we must update the value of pva. We use a label here
     * so that all return points will go through this update.
     * The 'goto end' is here to ensure that there is always a reference to the
     * label (to avoid the compiler complaining).
     */
    goto end;
end:
    *pva = va;
    return (res);
}

/*
 * ======== xdc.runtime.SysMin TEMPLATE ========
 */


#if defined(__ti__)
extern int HOSTwrite(int out, const char *buffer, unsigned count);
#elif (defined(gnu_targets_arm_STD_) && defined(xdc_target__os_undefined))
extern int _write(int out, char *buffer, int count);
#define HOSTwrite(x,y,z) _write((int)(x),(char *)(y),(int)(z))
#elif defined(__IAR_SYSTEMS_ICC__)
#include <yfuns.h>
#define HOSTwrite(x,y,z) __write((x),(unsigned char *)(y),(z))
#else
#include <stdio.h>
#endif

/*
 *  ======== SysMin_output__I ========
 *  HOSTWrite only writes a max of N chars at a time. The amount it writes
 *  is returned. This function loops until the entire buffer is written.
 *  Being a static function allows it to conditionally compile out.
 */
xdc_Void xdc_runtime_SysMin_output__I(xdc_Char *buf, xdc_UInt size)
{
#if defined(__ti__) || (defined(gnu_targets_arm_STD_) && defined(xdc_target__os_undefined)) || defined (__IAR_SYSTEMS_ICC__)
    xdc_Int printCount;

    while (size != 0U) {
        printCount = HOSTwrite(1, buf, size);
        if ((printCount <= 0) || ((xdc_UInt)printCount > size)) {
            break;  /* ensure we never get stuck in an infinite loop */
        }
        size -= (xdc_UInt)printCount;
        buf = buf + printCount;
    }
#else
    fwrite(buf, 1, size, stdout);
#endif
}

/*
 * ======== xdc.runtime.Text TEMPLATE ========
 */

/*
 *  ======== xdc_runtime_Text_visitRope__I ========
 *  This function is indirectly called within Text.c through
 *  the visitRopeFxn configuration parameter of xdc.runtime.Text.
 */
void xdc_runtime_Text_visitRope__I(xdc_runtime_Text_RopeId rope,
    xdc_runtime_Text_RopeVisitor visFxn, xdc_Ptr visState)
{
    xdc_CString stack[6];
    xdc_runtime_Text_visitRope2__I(rope, visFxn, visState, stack);
}

/*
 * ======== ti.sysbios.family.c7x.Hwi TEMPLATE ========
 */



/*
 * ======== ti.sysbios.family.c7x.Mmu TEMPLATE ========
 */



#if defined(__GNUC__) && !defined(__ti__)
#error "Unsupported compiler"
#else

#pragma DATA_SECTION(ti_sysbios_family_c7x_Mmu_tableArray, ".data.ti_sysbios_family_c7x_Mmu_tableArray");
#pragma DATA_ALIGN(ti_sysbios_family_c7x_Mmu_tableArray, 4096);

#pragma DATA_SECTION(ti_sysbios_family_c7x_Mmu_tableArray_NS, ".data.ti_sysbios_family_c7x_Mmu_tableArray_NS");
#pragma DATA_ALIGN(ti_sysbios_family_c7x_Mmu_tableArray_NS, 4096);

UInt64 ti_sysbios_family_c7x_Mmu_tableArray[16][512];
UInt64 ti_sysbios_family_c7x_Mmu_tableArray_NS[16][512];

#pragma DATA_SECTION(ti_sysbios_family_c7x_Mmu_tableArraySlot, ".data.ti_sysbios_family_c7x_Mmu_tableArraySlot");
#pragma DATA_ALIGN(ti_sysbios_family_c7x_Mmu_tableArraySlot, 4096);
UInt64 ti_sysbios_family_c7x_Mmu_tableArraySlot;
#pragma DATA_SECTION(ti_sysbios_family_c7x_Mmu_tableArraySlot_NS, ".data.ti_sysbios_family_c7x_Mmu_tableArraySlot_NS");
#pragma DATA_ALIGN(ti_sysbios_family_c7x_Mmu_tableArraySlot_NS, 4096);
UInt64 ti_sysbios_family_c7x_Mmu_tableArraySlot_NS;

#pragma DATA_SECTION(ti_sysbios_family_c7x_Mmu_level1Table, ".data.ti_sysbios_family_c7x_Mmu_level1Table");
#pragma DATA_ALIGN(ti_sysbios_family_c7x_Mmu_level1Table, 4096);
UInt64* ti_sysbios_family_c7x_Mmu_level1Table;
#pragma DATA_SECTION(ti_sysbios_family_c7x_Mmu_level1Table_NS, ".data.ti_sysbios_family_c7x_Mmu_level1Table_NS");
#pragma DATA_ALIGN(ti_sysbios_family_c7x_Mmu_level1Table_NS, 4096);
UInt64* ti_sysbios_family_c7x_Mmu_level1Table_NS;

#endif

/*
 * ======== ti.sysbios.BIOS TEMPLATE ========
 */


Void ti_sysbios_BIOS_atExitFunc__I(Int stat);

extern Void ti_sysbios_BIOS_registerRTSLock(Void);
extern Void ti_sysbios_timers_dmtimer_Timer_startup__E(Void);

Void ti_sysbios_BIOS_startFunc__I(Void)
{
    /*
     *  Check if XDC runtime startup functions have been called. If not, then
     *  call BIOS_linkedWithIncorrectBootLibrary() function. This function
     *  will spin forever.
     */
    if (xdc_runtime_Startup_rtsDone() != TRUE) {
        ti_sysbios_BIOS_linkedWithIncorrectBootLibrary();
    }

    (void)xdc_runtime_System_atexit(
        (xdc_runtime_System_AtexitHandler)ti_sysbios_BIOS_atExitFunc__I);
    ti_sysbios_BIOS_registerRTSLock();
    ti_sysbios_timers_dmtimer_Timer_startup__E();
    ti_sysbios_hal_Hwi_startup();
    ti_sysbios_knl_Swi_startup();
    ti_sysbios_utils_Load_startup();
    ti_sysbios_knl_Task_startup();
}

#include <_lock.h>

Void ti_sysbios_BIOS_atExitFunc__I(Int notused)
{
    (Void)ti_sysbios_knl_Swi_disable();
    (Void)ti_sysbios_knl_Task_disable();
    (Void)ti_sysbios_BIOS_setThreadType(ti_sysbios_BIOS_ThreadType_Main);

    if ((&ti_sysbios_BIOS_Module__state__V)->rtsGate != NULL) {
        _register_lock(_nop);
        _register_unlock(_nop);
    }
}


/*
 *  ======== BIOS_rtsLock ========
 *  Called by rts _lock() function
 */
Void ti_sysbios_BIOS_rtsLock(Void)
{
    IArg key;

    key = ti_sysbios_BIOS_RtsGateProxy_enter((&ti_sysbios_BIOS_Module__state__V)->rtsGate);
    if ((&ti_sysbios_BIOS_Module__state__V)->rtsGateCount == 0U) {
        (&ti_sysbios_BIOS_Module__state__V)->rtsGateKey = key;
    }
    /* Increment need not be atomic */
    (&ti_sysbios_BIOS_Module__state__V)->rtsGateCount++;
}

/*
 *  ======== BIOS_rtsUnLock ========
 *  Called by rts _unlock() function
 */
Void ti_sysbios_BIOS_rtsUnlock(Void)
{
    xdc_runtime_Assert_isTrue(((&ti_sysbios_BIOS_Module__state__V)->rtsGateCount), NULL);

    (&ti_sysbios_BIOS_Module__state__V)->rtsGateCount--;

    if ((&ti_sysbios_BIOS_Module__state__V)->rtsGateCount == 0U) {
        ti_sysbios_BIOS_RtsGateProxy_leave((&ti_sysbios_BIOS_Module__state__V)->rtsGate, (&ti_sysbios_BIOS_Module__state__V)->rtsGateKey);
    }
}

/*
 *  ======== BIOS_nullFunc ========
 */
Void ti_sysbios_BIOS_nullFunc__I(Void)
{
}

/*
 *  ======== BIOS_registerRTSLock ========
 */
Void ti_sysbios_BIOS_registerRTSLock(Void)
{
    if ((&ti_sysbios_BIOS_Module__state__V)->rtsGate != NULL) {
        _register_lock(ti_sysbios_BIOS_rtsLock);
        _register_unlock(ti_sysbios_BIOS_rtsUnlock);
    }
}

/*
 *  ======== BIOS_removeRTSLock ========
 */
Void ti_sysbios_BIOS_removeRTSLock(Void)
{
    if ((&ti_sysbios_BIOS_Module__state__V)->rtsGate != NULL) {
        _register_lock(ti_sysbios_BIOS_nullFunc);
        _register_unlock(ti_sysbios_BIOS_nullFunc);
    }
}

/*
 *  ======== BIOS_exitFunc ========
 */
Void ti_sysbios_BIOS_exitFunc(Int stat)
{
    /* remove the RTS lock */
    ti_sysbios_BIOS_removeRTSLock();

    /* force thread type to 'Main' */
    (void)ti_sysbios_BIOS_setThreadType(ti_sysbios_BIOS_ThreadType_Main);

    xdc_runtime_System_exit(stat);
}

/*
 *  ======== BIOS_errorRaiseHook ========
 */
Void ti_sysbios_BIOS_errorRaiseHook(xdc_runtime_Error_Block *eb)
{
    /*
     * If this is an Assert thread, defang Gate threadtype check
     */
    if (eb->id == xdc_runtime_Assert_E_assertFailed) {
        /* remove the RTS lock */
        ti_sysbios_BIOS_removeRTSLock();
        /* force thread type to 'Main' */
        ti_sysbios_BIOS_setThreadType(ti_sysbios_BIOS_ThreadType_Main);
    }
    /* Call the installed Error.raiseHook */
    ti_sysbios_BIOS_installedErrorHook(eb);
}

/*
 * ======== ti.sysbios.Build TEMPLATE ========
 */


/*
 * ======== ti.sysbios.knl.Clock TEMPLATE ========
 */

Void ti_sysbios_knl_Clock_doTick__I(UArg arg)
{
    /* update system time */
    (&ti_sysbios_knl_Clock_Module__state__V)->ticks++;

    ti_sysbios_knl_Clock_logTick__E();

    if (ti_sysbios_knl_Queue_empty(ti_sysbios_knl_Clock_Module_State_clockQ())
        == 0U) {
        (&ti_sysbios_knl_Clock_Module__state__V)->swiCount++;

        ti_sysbios_knl_Swi_post((&ti_sysbios_knl_Clock_Module__state__V)->swi);
    }
}

/*
 * ======== ti.sysbios.knl.Task TEMPLATE ========
 */




/*
 * ======== ti.sysbios.utils.Load TEMPLATE ========
 */

#include <ti/sysbios/utils/Load.h>
#include <ti/sysbios/hal/Core.h>

/*
 *  ======== Load_update ========
 */
Void ti_sysbios_utils_Load_update__E()
{
    /* Update and Log CPU load, Task, Swi, and Hwi loads (if enabled). */
    ti_sysbios_utils_Load_updateLoads();
}

/*
 *  ======== Load_hwiBeginHook ========
 */
Void ti_sysbios_utils_Load_hwiBeginHook__E(ti_sysbios_interfaces_IHwi_Handle hwi)
{
    ti_sysbios_utils_Load_HookContext *pTaskEnv;
    UInt32 delta;
    UInt coreId;
    UInt key;

    coreId = 0;

    key = ti_sysbios_hal_Hwi_disable();

    if (++(ti_sysbios_utils_Load_Module__state__V.hwiCnt) == 1) {   /* If first Hwi */

        /* Get the interrupt time */
        UInt32 intrTime = xdc_runtime_Timestamp_get32();

        if (ti_sysbios_utils_Load_Module__state__V.swiCnt == 0) {  /* we were in a Task */
            /* Update the total time the task has run */
            delta = intrTime - ti_sysbios_utils_Load_Module__state__V.taskStartTime[coreId];

            /*
             * may be NULL if interrupt happened early before first task
             * switch, or if task monitoring is disabled.
             */
            if (ti_sysbios_utils_Load_Module__state__V.runningTask[coreId] != NULL) {
                pTaskEnv = (ti_sysbios_utils_Load_HookContext *)ti_sysbios_knl_Task_getHookContext(
                    ti_sysbios_utils_Load_Module__state__V.runningTask[coreId],
                    ti_sysbios_utils_Load_Module__state__V.taskHId);

                /* record time, if task has been registered */
                if (pTaskEnv != NULL) {
                    pTaskEnv->nextTotalTime += delta;
                }
            }
        }
        else {  /* we were in a Swi */
            /* Update the total time the task has run */
            delta = intrTime - ti_sysbios_utils_Load_Module__state__V.swiStartTime;

            /* record it */
            ti_sysbios_utils_Load_Module__state__V.swiEnv.nextTotalTime += delta;
        }

        /* Update the start time for next hwi */
        ti_sysbios_utils_Load_Module__state__V.hwiStartTime = intrTime;
    }

    ti_sysbios_hal_Hwi_restore(key);
}

/*
 * ======== ti.sysbios.rts.MemAlloc TEMPLATE ========
 */



#if defined(__ti__) && !defined(__clang__)

#pragma FUNC_EXT_CALLED(malloc);
#pragma FUNC_EXT_CALLED(memalign);
#pragma FUNC_EXT_CALLED(free);
#pragma FUNC_EXT_CALLED(calloc);
#pragma FUNC_EXT_CALLED(realloc);
#pragma FUNC_EXT_CALLED(aligned_alloc);

#define ATTRIBUTE

#elif defined(__IAR_SYSTEMS_ICC__)

#define ATTRIBUTE

#else

#define ATTRIBUTE __attribute__ ((used))

#endif


#include <xdc/std.h>

#include <xdc/runtime/Memory.h>
#include <xdc/runtime/Error.h>

#include <string.h>

#if defined(__GNUC__) && !defined(__ti__)

#include <reent.h>

#endif


/*
 * Header is a union to make sure that the size is a power of 2.
 */
typedef union Header {
    struct {
        Ptr   actualBuf;
        SizeT size;
    } header;
    UArg pad[2];	/* 4 words on 28L, 8 bytes on most others */
} Header;



/*
 *  ======== ti_sysbios_rts_MemAlloc_alloc ========
 */
static Void *ti_sysbios_rts_MemAlloc_alloc(SizeT size)
{
    Header *packet;
    xdc_runtime_Error_Block eb;

    if (size == 0) {
        return (NULL);
    }

    xdc_runtime_Error_init(&eb);

    packet = (Header *)xdc_runtime_Memory_alloc(NULL,
        (SizeT)(size + sizeof(Header)), 0, &eb);

    if (packet == NULL) {
        return (NULL);
    }

    packet->header.actualBuf = (Ptr)packet;
    packet->header.size = size + sizeof(Header);

    return (packet + 1);
}

/*
 *  ======== malloc ========
 */
Void ATTRIBUTE *malloc(SizeT size)
{
    return (ti_sysbios_rts_MemAlloc_alloc(size));
}

/*
 *  ======== memalign ========
 *  mirrors the memalign() function from the TI run-time library
 */
Void ATTRIBUTE *memalign(SizeT alignment, SizeT size)
{
    Header                      *packet;
    Void                        *tmp;
    xdc_runtime_Error_Block     eb;

    /* return NULL if size is 0, or alignment is not a power-of-2 */
    if (size == 0 || (alignment & (alignment - 1))) {
        return (NULL);
    }

    if (alignment < sizeof(Header)) {
        alignment = sizeof(Header);
    }

    xdc_runtime_Error_init(&eb);

    /*
     * Allocate 'align + size' so that we have room for the 'packet'
     * and can return an aligned buffer.
     */
    tmp = xdc_runtime_Memory_alloc(NULL, alignment + size, alignment, &eb);

    if (tmp == NULL) {
        return (NULL);
    }

    packet = (Header *)((char *)tmp + alignment - sizeof(Header));

    packet->header.actualBuf = tmp;
    packet->header.size = size + sizeof(Header);

    return (packet + 1);
}

/*
 *  ======== calloc ========
 */
Void ATTRIBUTE *calloc(SizeT nmemb, SizeT size)
{
    SizeT       nbytes;
    Ptr         retval;

    nbytes = nmemb * size;

    /* return NULL if there's an overflow */
    if (nmemb && size != (nbytes / nmemb)) {
        return (NULL);
    }

    retval = ti_sysbios_rts_MemAlloc_alloc(nbytes);
    if (retval != NULL) {
        (Void)memset(retval, (Int)'\0', nbytes);
    }

    return (retval);
}

/*
 *  ======== free ========
 */
Void ATTRIBUTE free(Void *ptr)
{
    Header      *packet;

    if (ptr != NULL) {
        packet = ((Header *)ptr) - 1;

        xdc_runtime_Memory_free(NULL, (Ptr)packet->header.actualBuf,
            (packet->header.size +
            ((char*)packet - (char*)packet->header.actualBuf)));
    }
}

/*
 *  ======== realloc ========
 */
Void ATTRIBUTE *realloc(Void *ptr, SizeT size)
{
    Ptr         retval;
    Header      *packet;
    SizeT       oldSize;

    if (ptr == NULL) {
        retval = malloc(size);
    }
    else if (size == 0) {
        free(ptr);
        retval = NULL;
    }
    else {
        packet = (Header *)ptr - 1;
        retval = malloc(size);
        if (retval != NULL) {
            oldSize = packet->header.size - sizeof(Header);
            (Void)memcpy(retval, ptr, (size < oldSize) ? size : oldSize);
            free(ptr);
        }
    }

    return (retval);
}

/*
 *  ======== aligned_alloc ========
 */
Void ATTRIBUTE *aligned_alloc(SizeT alignment, SizeT size)
{
    Void *retval;

    retval = memalign(alignment, size);

    return (retval);
}

#if defined(__GNUC__) && !defined(__ti__)

/*
 *  ======== _malloc_r ========
 */
Void ATTRIBUTE *_malloc_r(struct _reent *rptr, SizeT size)
{
    return malloc(size);
}

/*
 *  ======== _calloc_r ========
 */
Void ATTRIBUTE *_calloc_r(struct _reent *rptr, SizeT nmemb, SizeT size)
{
    return calloc(nmemb, size);
}

/*
 *  ======== _free_r ========
 */
Void ATTRIBUTE _free_r(struct _reent *rptr, Void *ptr)
{
    free(ptr);
}

/*
 *  ======== _realloc_r ========
 */
Void ATTRIBUTE *_realloc_r(struct _reent *rptr, Void *ptr, SizeT size)
{
    return realloc(ptr, size);
}

#endif


/*
 * ======== ti.sysbios.BIOS INITIALIZERS ========
 */

/* Module__state__V */
#ifdef __ti__
ti_sysbios_BIOS_Module_State__ ti_sysbios_BIOS_Module__state__V __attribute__ ((section(".data:ti_sysbios_BIOS_Module__state__V")));
#elif !(defined(__MACH__) && defined(__APPLE__))
ti_sysbios_BIOS_Module_State__ ti_sysbios_BIOS_Module__state__V __attribute__ ((section(".data_ti_sysbios_BIOS_Module__state__V")));
#endif
ti_sysbios_BIOS_Module_State__ ti_sysbios_BIOS_Module__state__V = {
    {
        (xdc_Bits32)0x0U,  /* hi */
        (xdc_Bits32)0x3b9aca00U,  /* lo */
    },  /* cpuFreq */
    (xdc_UInt)0x0U,  /* rtsGateCount */
    ((xdc_IArg)(0x0)),  /* rtsGateKey */
    (ti_sysbios_BIOS_RtsGateProxy_Handle)&ti_sysbios_gates_GateMutex_Object__table__V[1],  /* rtsGate */
    ti_sysbios_BIOS_ThreadType_Main,  /* threadType */
    ((void*)0),  /* smpThreadType */
    ((xdc_Void(*)(xdc_Void))(ti_sysbios_BIOS_startFunc)),  /* startFunc */
    ((xdc_Void(*)(xdc_Int f_arg0))(ti_sysbios_BIOS_exitFunc)),  /* exitFunc */
};

/* Module__diagsEnabled__C */
#pragma DATA_SECTION(ti_sysbios_BIOS_Module__diagsEnabled__C, ".const:ti_sysbios_BIOS_Module__diagsEnabled__C");
__FAR__ const CT__ti_sysbios_BIOS_Module__diagsEnabled ti_sysbios_BIOS_Module__diagsEnabled__C = (xdc_Bits32)0x90U;

/* Module__diagsIncluded__C */
#pragma DATA_SECTION(ti_sysbios_BIOS_Module__diagsIncluded__C, ".const:ti_sysbios_BIOS_Module__diagsIncluded__C");
__FAR__ const CT__ti_sysbios_BIOS_Module__diagsIncluded ti_sysbios_BIOS_Module__diagsIncluded__C = (xdc_Bits32)0x90U;

/* Module__diagsMask__C */
#pragma DATA_SECTION(ti_sysbios_BIOS_Module__diagsMask__C, ".const:ti_sysbios_BIOS_Module__diagsMask__C");
__FAR__ const CT__ti_sysbios_BIOS_Module__diagsMask ti_sysbios_BIOS_Module__diagsMask__C = ((const CT__ti_sysbios_BIOS_Module__diagsMask)NULL);

/* Module__gateObj__C */
#pragma DATA_SECTION(ti_sysbios_BIOS_Module__gateObj__C, ".const:ti_sysbios_BIOS_Module__gateObj__C");
__FAR__ const CT__ti_sysbios_BIOS_Module__gateObj ti_sysbios_BIOS_Module__gateObj__C = ((const CT__ti_sysbios_BIOS_Module__gateObj)NULL);

/* Module__gatePrms__C */
#pragma DATA_SECTION(ti_sysbios_BIOS_Module__gatePrms__C, ".const:ti_sysbios_BIOS_Module__gatePrms__C");
__FAR__ const CT__ti_sysbios_BIOS_Module__gatePrms ti_sysbios_BIOS_Module__gatePrms__C = ((const CT__ti_sysbios_BIOS_Module__gatePrms)NULL);

/* Module__id__C */
#pragma DATA_SECTION(ti_sysbios_BIOS_Module__id__C, ".const:ti_sysbios_BIOS_Module__id__C");
__FAR__ const CT__ti_sysbios_BIOS_Module__id ti_sysbios_BIOS_Module__id__C = (xdc_Bits16)0x801eU;

/* Module__loggerDefined__C */
#pragma DATA_SECTION(ti_sysbios_BIOS_Module__loggerDefined__C, ".const:ti_sysbios_BIOS_Module__loggerDefined__C");
__FAR__ const CT__ti_sysbios_BIOS_Module__loggerDefined ti_sysbios_BIOS_Module__loggerDefined__C = 0;

/* Module__loggerObj__C */
#pragma DATA_SECTION(ti_sysbios_BIOS_Module__loggerObj__C, ".const:ti_sysbios_BIOS_Module__loggerObj__C");
__FAR__ const CT__ti_sysbios_BIOS_Module__loggerObj ti_sysbios_BIOS_Module__loggerObj__C = ((const CT__ti_sysbios_BIOS_Module__loggerObj)NULL);

/* Module__loggerFxn0__C */
#pragma DATA_SECTION(ti_sysbios_BIOS_Module__loggerFxn0__C, ".const:ti_sysbios_BIOS_Module__loggerFxn0__C");
__FAR__ const CT__ti_sysbios_BIOS_Module__loggerFxn0 ti_sysbios_BIOS_Module__loggerFxn0__C = ((const CT__ti_sysbios_BIOS_Module__loggerFxn0)NULL);

/* Module__loggerFxn1__C */
#pragma DATA_SECTION(ti_sysbios_BIOS_Module__loggerFxn1__C, ".const:ti_sysbios_BIOS_Module__loggerFxn1__C");
__FAR__ const CT__ti_sysbios_BIOS_Module__loggerFxn1 ti_sysbios_BIOS_Module__loggerFxn1__C = ((const CT__ti_sysbios_BIOS_Module__loggerFxn1)NULL);

/* Module__loggerFxn2__C */
#pragma DATA_SECTION(ti_sysbios_BIOS_Module__loggerFxn2__C, ".const:ti_sysbios_BIOS_Module__loggerFxn2__C");
__FAR__ const CT__ti_sysbios_BIOS_Module__loggerFxn2 ti_sysbios_BIOS_Module__loggerFxn2__C = ((const CT__ti_sysbios_BIOS_Module__loggerFxn2)NULL);

/* Module__loggerFxn4__C */
#pragma DATA_SECTION(ti_sysbios_BIOS_Module__loggerFxn4__C, ".const:ti_sysbios_BIOS_Module__loggerFxn4__C");
__FAR__ const CT__ti_sysbios_BIOS_Module__loggerFxn4 ti_sysbios_BIOS_Module__loggerFxn4__C = ((const CT__ti_sysbios_BIOS_Module__loggerFxn4)NULL);

/* Module__loggerFxn8__C */
#pragma DATA_SECTION(ti_sysbios_BIOS_Module__loggerFxn8__C, ".const:ti_sysbios_BIOS_Module__loggerFxn8__C");
__FAR__ const CT__ti_sysbios_BIOS_Module__loggerFxn8 ti_sysbios_BIOS_Module__loggerFxn8__C = ((const CT__ti_sysbios_BIOS_Module__loggerFxn8)NULL);

/* Object__count__C */
#pragma DATA_SECTION(ti_sysbios_BIOS_Object__count__C, ".const:ti_sysbios_BIOS_Object__count__C");
__FAR__ const CT__ti_sysbios_BIOS_Object__count ti_sysbios_BIOS_Object__count__C = 0;

/* Object__heap__C */
#pragma DATA_SECTION(ti_sysbios_BIOS_Object__heap__C, ".const:ti_sysbios_BIOS_Object__heap__C");
__FAR__ const CT__ti_sysbios_BIOS_Object__heap ti_sysbios_BIOS_Object__heap__C = NULL;

/* Object__sizeof__C */
#pragma DATA_SECTION(ti_sysbios_BIOS_Object__sizeof__C, ".const:ti_sysbios_BIOS_Object__sizeof__C");
__FAR__ const CT__ti_sysbios_BIOS_Object__sizeof ti_sysbios_BIOS_Object__sizeof__C = 0;

/* Object__table__C */
#pragma DATA_SECTION(ti_sysbios_BIOS_Object__table__C, ".const:ti_sysbios_BIOS_Object__table__C");
__FAR__ const CT__ti_sysbios_BIOS_Object__table ti_sysbios_BIOS_Object__table__C = NULL;

/* smpEnabled__C */
#pragma DATA_SECTION(ti_sysbios_BIOS_smpEnabled__C, ".const:ti_sysbios_BIOS_smpEnabled__C");
__FAR__ const CT__ti_sysbios_BIOS_smpEnabled ti_sysbios_BIOS_smpEnabled__C = 0;

/* mpeEnabled__C */
#pragma DATA_SECTION(ti_sysbios_BIOS_mpeEnabled__C, ".const:ti_sysbios_BIOS_mpeEnabled__C");
__FAR__ const CT__ti_sysbios_BIOS_mpeEnabled ti_sysbios_BIOS_mpeEnabled__C = 0;

/* cpuFreq__C */
#pragma DATA_SECTION(ti_sysbios_BIOS_cpuFreq__C, ".const:ti_sysbios_BIOS_cpuFreq__C");
__FAR__ const CT__ti_sysbios_BIOS_cpuFreq ti_sysbios_BIOS_cpuFreq__C = {
    (xdc_Bits32)0x0U,  /* hi */
    (xdc_Bits32)0x3b9aca00U,  /* lo */
};

/* runtimeCreatesEnabled__C */
#pragma DATA_SECTION(ti_sysbios_BIOS_runtimeCreatesEnabled__C, ".const:ti_sysbios_BIOS_runtimeCreatesEnabled__C");
__FAR__ const CT__ti_sysbios_BIOS_runtimeCreatesEnabled ti_sysbios_BIOS_runtimeCreatesEnabled__C = 1;

/* taskEnabled__C */
#pragma DATA_SECTION(ti_sysbios_BIOS_taskEnabled__C, ".const:ti_sysbios_BIOS_taskEnabled__C");
__FAR__ const CT__ti_sysbios_BIOS_taskEnabled ti_sysbios_BIOS_taskEnabled__C = 1;

/* swiEnabled__C */
#pragma DATA_SECTION(ti_sysbios_BIOS_swiEnabled__C, ".const:ti_sysbios_BIOS_swiEnabled__C");
__FAR__ const CT__ti_sysbios_BIOS_swiEnabled ti_sysbios_BIOS_swiEnabled__C = 1;

/* clockEnabled__C */
#pragma DATA_SECTION(ti_sysbios_BIOS_clockEnabled__C, ".const:ti_sysbios_BIOS_clockEnabled__C");
__FAR__ const CT__ti_sysbios_BIOS_clockEnabled ti_sysbios_BIOS_clockEnabled__C = 1;

/* defaultKernelHeapInstance__C */
#pragma DATA_SECTION(ti_sysbios_BIOS_defaultKernelHeapInstance__C, ".const:ti_sysbios_BIOS_defaultKernelHeapInstance__C");
__FAR__ const CT__ti_sysbios_BIOS_defaultKernelHeapInstance ti_sysbios_BIOS_defaultKernelHeapInstance__C = 0;

/* kernelHeapSize__C */
#pragma DATA_SECTION(ti_sysbios_BIOS_kernelHeapSize__C, ".const:ti_sysbios_BIOS_kernelHeapSize__C");
__FAR__ const CT__ti_sysbios_BIOS_kernelHeapSize ti_sysbios_BIOS_kernelHeapSize__C = (xdc_SizeT)0x1000;

/* kernelHeapSection__C */
#pragma DATA_SECTION(ti_sysbios_BIOS_kernelHeapSection__C, ".const:ti_sysbios_BIOS_kernelHeapSection__C");
__FAR__ const CT__ti_sysbios_BIOS_kernelHeapSection ti_sysbios_BIOS_kernelHeapSection__C = ".kernel_heap";

/* heapSize__C */
#pragma DATA_SECTION(ti_sysbios_BIOS_heapSize__C, ".const:ti_sysbios_BIOS_heapSize__C");
__FAR__ const CT__ti_sysbios_BIOS_heapSize ti_sysbios_BIOS_heapSize__C = (xdc_SizeT)0x1000;

/* heapSection__C */
#pragma DATA_SECTION(ti_sysbios_BIOS_heapSection__C, ".const:ti_sysbios_BIOS_heapSection__C");
__FAR__ const CT__ti_sysbios_BIOS_heapSection ti_sysbios_BIOS_heapSection__C = 0;

/* heapTrackEnabled__C */
#pragma DATA_SECTION(ti_sysbios_BIOS_heapTrackEnabled__C, ".const:ti_sysbios_BIOS_heapTrackEnabled__C");
__FAR__ const CT__ti_sysbios_BIOS_heapTrackEnabled ti_sysbios_BIOS_heapTrackEnabled__C = 0;

/* setupSecureContext__C */
#pragma DATA_SECTION(ti_sysbios_BIOS_setupSecureContext__C, ".const:ti_sysbios_BIOS_setupSecureContext__C");
__FAR__ const CT__ti_sysbios_BIOS_setupSecureContext ti_sysbios_BIOS_setupSecureContext__C = 0;

/* useSK__C */
#pragma DATA_SECTION(ti_sysbios_BIOS_useSK__C, ".const:ti_sysbios_BIOS_useSK__C");
__FAR__ const CT__ti_sysbios_BIOS_useSK ti_sysbios_BIOS_useSK__C = 0;

/* installedErrorHook__C */
#pragma DATA_SECTION(ti_sysbios_BIOS_installedErrorHook__C, ".const:ti_sysbios_BIOS_installedErrorHook__C");
__FAR__ const CT__ti_sysbios_BIOS_installedErrorHook ti_sysbios_BIOS_installedErrorHook__C = ((const CT__ti_sysbios_BIOS_installedErrorHook)(xdc_runtime_Error_print__E));


/*
 * ======== ti.sysbios.BIOS_RtsGateProxy INITIALIZERS ========
 */


/*
 * ======== ti.sysbios.family.c64p.tci6488.TimerSupport INITIALIZERS ========
 */

/* Module__diagsEnabled__C */
#pragma DATA_SECTION(ti_sysbios_family_c64p_tci6488_TimerSupport_Module__diagsEnabled__C, ".const:ti_sysbios_family_c64p_tci6488_TimerSupport_Module__diagsEnabled__C");
__FAR__ const CT__ti_sysbios_family_c64p_tci6488_TimerSupport_Module__diagsEnabled ti_sysbios_family_c64p_tci6488_TimerSupport_Module__diagsEnabled__C = (xdc_Bits32)0x90U;

/* Module__diagsIncluded__C */
#pragma DATA_SECTION(ti_sysbios_family_c64p_tci6488_TimerSupport_Module__diagsIncluded__C, ".const:ti_sysbios_family_c64p_tci6488_TimerSupport_Module__diagsIncluded__C");
__FAR__ const CT__ti_sysbios_family_c64p_tci6488_TimerSupport_Module__diagsIncluded ti_sysbios_family_c64p_tci6488_TimerSupport_Module__diagsIncluded__C = (xdc_Bits32)0x90U;

/* Module__diagsMask__C */
#pragma DATA_SECTION(ti_sysbios_family_c64p_tci6488_TimerSupport_Module__diagsMask__C, ".const:ti_sysbios_family_c64p_tci6488_TimerSupport_Module__diagsMask__C");
__FAR__ const CT__ti_sysbios_family_c64p_tci6488_TimerSupport_Module__diagsMask ti_sysbios_family_c64p_tci6488_TimerSupport_Module__diagsMask__C = ((const CT__ti_sysbios_family_c64p_tci6488_TimerSupport_Module__diagsMask)NULL);

/* Module__gateObj__C */
#pragma DATA_SECTION(ti_sysbios_family_c64p_tci6488_TimerSupport_Module__gateObj__C, ".const:ti_sysbios_family_c64p_tci6488_TimerSupport_Module__gateObj__C");
__FAR__ const CT__ti_sysbios_family_c64p_tci6488_TimerSupport_Module__gateObj ti_sysbios_family_c64p_tci6488_TimerSupport_Module__gateObj__C = ((const CT__ti_sysbios_family_c64p_tci6488_TimerSupport_Module__gateObj)NULL);

/* Module__gatePrms__C */
#pragma DATA_SECTION(ti_sysbios_family_c64p_tci6488_TimerSupport_Module__gatePrms__C, ".const:ti_sysbios_family_c64p_tci6488_TimerSupport_Module__gatePrms__C");
__FAR__ const CT__ti_sysbios_family_c64p_tci6488_TimerSupport_Module__gatePrms ti_sysbios_family_c64p_tci6488_TimerSupport_Module__gatePrms__C = ((const CT__ti_sysbios_family_c64p_tci6488_TimerSupport_Module__gatePrms)NULL);

/* Module__id__C */
#pragma DATA_SECTION(ti_sysbios_family_c64p_tci6488_TimerSupport_Module__id__C, ".const:ti_sysbios_family_c64p_tci6488_TimerSupport_Module__id__C");
__FAR__ const CT__ti_sysbios_family_c64p_tci6488_TimerSupport_Module__id ti_sysbios_family_c64p_tci6488_TimerSupport_Module__id__C = (xdc_Bits16)0x8042U;

/* Module__loggerDefined__C */
#pragma DATA_SECTION(ti_sysbios_family_c64p_tci6488_TimerSupport_Module__loggerDefined__C, ".const:ti_sysbios_family_c64p_tci6488_TimerSupport_Module__loggerDefined__C");
__FAR__ const CT__ti_sysbios_family_c64p_tci6488_TimerSupport_Module__loggerDefined ti_sysbios_family_c64p_tci6488_TimerSupport_Module__loggerDefined__C = 0;

/* Module__loggerObj__C */
#pragma DATA_SECTION(ti_sysbios_family_c64p_tci6488_TimerSupport_Module__loggerObj__C, ".const:ti_sysbios_family_c64p_tci6488_TimerSupport_Module__loggerObj__C");
__FAR__ const CT__ti_sysbios_family_c64p_tci6488_TimerSupport_Module__loggerObj ti_sysbios_family_c64p_tci6488_TimerSupport_Module__loggerObj__C = ((const CT__ti_sysbios_family_c64p_tci6488_TimerSupport_Module__loggerObj)NULL);

/* Module__loggerFxn0__C */
#pragma DATA_SECTION(ti_sysbios_family_c64p_tci6488_TimerSupport_Module__loggerFxn0__C, ".const:ti_sysbios_family_c64p_tci6488_TimerSupport_Module__loggerFxn0__C");
__FAR__ const CT__ti_sysbios_family_c64p_tci6488_TimerSupport_Module__loggerFxn0 ti_sysbios_family_c64p_tci6488_TimerSupport_Module__loggerFxn0__C = ((const CT__ti_sysbios_family_c64p_tci6488_TimerSupport_Module__loggerFxn0)NULL);

/* Module__loggerFxn1__C */
#pragma DATA_SECTION(ti_sysbios_family_c64p_tci6488_TimerSupport_Module__loggerFxn1__C, ".const:ti_sysbios_family_c64p_tci6488_TimerSupport_Module__loggerFxn1__C");
__FAR__ const CT__ti_sysbios_family_c64p_tci6488_TimerSupport_Module__loggerFxn1 ti_sysbios_family_c64p_tci6488_TimerSupport_Module__loggerFxn1__C = ((const CT__ti_sysbios_family_c64p_tci6488_TimerSupport_Module__loggerFxn1)NULL);

/* Module__loggerFxn2__C */
#pragma DATA_SECTION(ti_sysbios_family_c64p_tci6488_TimerSupport_Module__loggerFxn2__C, ".const:ti_sysbios_family_c64p_tci6488_TimerSupport_Module__loggerFxn2__C");
__FAR__ const CT__ti_sysbios_family_c64p_tci6488_TimerSupport_Module__loggerFxn2 ti_sysbios_family_c64p_tci6488_TimerSupport_Module__loggerFxn2__C = ((const CT__ti_sysbios_family_c64p_tci6488_TimerSupport_Module__loggerFxn2)NULL);

/* Module__loggerFxn4__C */
#pragma DATA_SECTION(ti_sysbios_family_c64p_tci6488_TimerSupport_Module__loggerFxn4__C, ".const:ti_sysbios_family_c64p_tci6488_TimerSupport_Module__loggerFxn4__C");
__FAR__ const CT__ti_sysbios_family_c64p_tci6488_TimerSupport_Module__loggerFxn4 ti_sysbios_family_c64p_tci6488_TimerSupport_Module__loggerFxn4__C = ((const CT__ti_sysbios_family_c64p_tci6488_TimerSupport_Module__loggerFxn4)NULL);

/* Module__loggerFxn8__C */
#pragma DATA_SECTION(ti_sysbios_family_c64p_tci6488_TimerSupport_Module__loggerFxn8__C, ".const:ti_sysbios_family_c64p_tci6488_TimerSupport_Module__loggerFxn8__C");
__FAR__ const CT__ti_sysbios_family_c64p_tci6488_TimerSupport_Module__loggerFxn8 ti_sysbios_family_c64p_tci6488_TimerSupport_Module__loggerFxn8__C = ((const CT__ti_sysbios_family_c64p_tci6488_TimerSupport_Module__loggerFxn8)NULL);

/* Object__count__C */
#pragma DATA_SECTION(ti_sysbios_family_c64p_tci6488_TimerSupport_Object__count__C, ".const:ti_sysbios_family_c64p_tci6488_TimerSupport_Object__count__C");
__FAR__ const CT__ti_sysbios_family_c64p_tci6488_TimerSupport_Object__count ti_sysbios_family_c64p_tci6488_TimerSupport_Object__count__C = 0;

/* Object__heap__C */
#pragma DATA_SECTION(ti_sysbios_family_c64p_tci6488_TimerSupport_Object__heap__C, ".const:ti_sysbios_family_c64p_tci6488_TimerSupport_Object__heap__C");
__FAR__ const CT__ti_sysbios_family_c64p_tci6488_TimerSupport_Object__heap ti_sysbios_family_c64p_tci6488_TimerSupport_Object__heap__C = NULL;

/* Object__sizeof__C */
#pragma DATA_SECTION(ti_sysbios_family_c64p_tci6488_TimerSupport_Object__sizeof__C, ".const:ti_sysbios_family_c64p_tci6488_TimerSupport_Object__sizeof__C");
__FAR__ const CT__ti_sysbios_family_c64p_tci6488_TimerSupport_Object__sizeof ti_sysbios_family_c64p_tci6488_TimerSupport_Object__sizeof__C = 0;

/* Object__table__C */
#pragma DATA_SECTION(ti_sysbios_family_c64p_tci6488_TimerSupport_Object__table__C, ".const:ti_sysbios_family_c64p_tci6488_TimerSupport_Object__table__C");
__FAR__ const CT__ti_sysbios_family_c64p_tci6488_TimerSupport_Object__table ti_sysbios_family_c64p_tci6488_TimerSupport_Object__table__C = NULL;


/*
 * ======== ti.sysbios.family.c7x.Cache INITIALIZERS ========
 */

/* Module__state__V */
#ifdef __ti__
ti_sysbios_family_c7x_Cache_Module_State__ ti_sysbios_family_c7x_Cache_Module__state__V __attribute__ ((section(".data:ti_sysbios_family_c7x_Cache_Module__state__V")));
#elif !(defined(__MACH__) && defined(__APPLE__))
ti_sysbios_family_c7x_Cache_Module_State__ ti_sysbios_family_c7x_Cache_Module__state__V __attribute__ ((section(".data_ti_sysbios_family_c7x_Cache_Module__state__V")));
#endif
ti_sysbios_family_c7x_Cache_Module_State__ ti_sysbios_family_c7x_Cache_Module__state__V = {
    (xdc_UInt64)0x4U,  /* L1DCFG */
    (xdc_UInt64)0x0U,  /* L2CFG */
};

/* Module__diagsEnabled__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Cache_Module__diagsEnabled__C, ".const:ti_sysbios_family_c7x_Cache_Module__diagsEnabled__C");
__FAR__ const CT__ti_sysbios_family_c7x_Cache_Module__diagsEnabled ti_sysbios_family_c7x_Cache_Module__diagsEnabled__C = (xdc_Bits32)0x90U;

/* Module__diagsIncluded__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Cache_Module__diagsIncluded__C, ".const:ti_sysbios_family_c7x_Cache_Module__diagsIncluded__C");
__FAR__ const CT__ti_sysbios_family_c7x_Cache_Module__diagsIncluded ti_sysbios_family_c7x_Cache_Module__diagsIncluded__C = (xdc_Bits32)0x90U;

/* Module__diagsMask__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Cache_Module__diagsMask__C, ".const:ti_sysbios_family_c7x_Cache_Module__diagsMask__C");
__FAR__ const CT__ti_sysbios_family_c7x_Cache_Module__diagsMask ti_sysbios_family_c7x_Cache_Module__diagsMask__C = ((const CT__ti_sysbios_family_c7x_Cache_Module__diagsMask)NULL);

/* Module__gateObj__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Cache_Module__gateObj__C, ".const:ti_sysbios_family_c7x_Cache_Module__gateObj__C");
__FAR__ const CT__ti_sysbios_family_c7x_Cache_Module__gateObj ti_sysbios_family_c7x_Cache_Module__gateObj__C = ((const CT__ti_sysbios_family_c7x_Cache_Module__gateObj)NULL);

/* Module__gatePrms__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Cache_Module__gatePrms__C, ".const:ti_sysbios_family_c7x_Cache_Module__gatePrms__C");
__FAR__ const CT__ti_sysbios_family_c7x_Cache_Module__gatePrms ti_sysbios_family_c7x_Cache_Module__gatePrms__C = ((const CT__ti_sysbios_family_c7x_Cache_Module__gatePrms)NULL);

/* Module__id__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Cache_Module__id__C, ".const:ti_sysbios_family_c7x_Cache_Module__id__C");
__FAR__ const CT__ti_sysbios_family_c7x_Cache_Module__id ti_sysbios_family_c7x_Cache_Module__id__C = (xdc_Bits16)0x8017U;

/* Module__loggerDefined__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Cache_Module__loggerDefined__C, ".const:ti_sysbios_family_c7x_Cache_Module__loggerDefined__C");
__FAR__ const CT__ti_sysbios_family_c7x_Cache_Module__loggerDefined ti_sysbios_family_c7x_Cache_Module__loggerDefined__C = 0;

/* Module__loggerObj__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Cache_Module__loggerObj__C, ".const:ti_sysbios_family_c7x_Cache_Module__loggerObj__C");
__FAR__ const CT__ti_sysbios_family_c7x_Cache_Module__loggerObj ti_sysbios_family_c7x_Cache_Module__loggerObj__C = ((const CT__ti_sysbios_family_c7x_Cache_Module__loggerObj)NULL);

/* Module__loggerFxn0__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Cache_Module__loggerFxn0__C, ".const:ti_sysbios_family_c7x_Cache_Module__loggerFxn0__C");
__FAR__ const CT__ti_sysbios_family_c7x_Cache_Module__loggerFxn0 ti_sysbios_family_c7x_Cache_Module__loggerFxn0__C = ((const CT__ti_sysbios_family_c7x_Cache_Module__loggerFxn0)NULL);

/* Module__loggerFxn1__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Cache_Module__loggerFxn1__C, ".const:ti_sysbios_family_c7x_Cache_Module__loggerFxn1__C");
__FAR__ const CT__ti_sysbios_family_c7x_Cache_Module__loggerFxn1 ti_sysbios_family_c7x_Cache_Module__loggerFxn1__C = ((const CT__ti_sysbios_family_c7x_Cache_Module__loggerFxn1)NULL);

/* Module__loggerFxn2__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Cache_Module__loggerFxn2__C, ".const:ti_sysbios_family_c7x_Cache_Module__loggerFxn2__C");
__FAR__ const CT__ti_sysbios_family_c7x_Cache_Module__loggerFxn2 ti_sysbios_family_c7x_Cache_Module__loggerFxn2__C = ((const CT__ti_sysbios_family_c7x_Cache_Module__loggerFxn2)NULL);

/* Module__loggerFxn4__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Cache_Module__loggerFxn4__C, ".const:ti_sysbios_family_c7x_Cache_Module__loggerFxn4__C");
__FAR__ const CT__ti_sysbios_family_c7x_Cache_Module__loggerFxn4 ti_sysbios_family_c7x_Cache_Module__loggerFxn4__C = ((const CT__ti_sysbios_family_c7x_Cache_Module__loggerFxn4)NULL);

/* Module__loggerFxn8__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Cache_Module__loggerFxn8__C, ".const:ti_sysbios_family_c7x_Cache_Module__loggerFxn8__C");
__FAR__ const CT__ti_sysbios_family_c7x_Cache_Module__loggerFxn8 ti_sysbios_family_c7x_Cache_Module__loggerFxn8__C = ((const CT__ti_sysbios_family_c7x_Cache_Module__loggerFxn8)NULL);

/* Object__count__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Cache_Object__count__C, ".const:ti_sysbios_family_c7x_Cache_Object__count__C");
__FAR__ const CT__ti_sysbios_family_c7x_Cache_Object__count ti_sysbios_family_c7x_Cache_Object__count__C = 0;

/* Object__heap__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Cache_Object__heap__C, ".const:ti_sysbios_family_c7x_Cache_Object__heap__C");
__FAR__ const CT__ti_sysbios_family_c7x_Cache_Object__heap ti_sysbios_family_c7x_Cache_Object__heap__C = NULL;

/* Object__sizeof__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Cache_Object__sizeof__C, ".const:ti_sysbios_family_c7x_Cache_Object__sizeof__C");
__FAR__ const CT__ti_sysbios_family_c7x_Cache_Object__sizeof ti_sysbios_family_c7x_Cache_Object__sizeof__C = 0;

/* Object__table__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Cache_Object__table__C, ".const:ti_sysbios_family_c7x_Cache_Object__table__C");
__FAR__ const CT__ti_sysbios_family_c7x_Cache_Object__table ti_sysbios_family_c7x_Cache_Object__table__C = NULL;

/* enableCache__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Cache_enableCache__C, ".const:ti_sysbios_family_c7x_Cache_enableCache__C");
__FAR__ const CT__ti_sysbios_family_c7x_Cache_enableCache ti_sysbios_family_c7x_Cache_enableCache__C = 1;

/* initSize__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Cache_initSize__C, ".const:ti_sysbios_family_c7x_Cache_initSize__C");
__FAR__ const CT__ti_sysbios_family_c7x_Cache_initSize ti_sysbios_family_c7x_Cache_initSize__C = {
    ti_sysbios_family_c7x_Cache_L1Size_32K,  /* l1pSize */
    ti_sysbios_family_c7x_Cache_L1Size_32K,  /* l1dSize */
    ti_sysbios_family_c7x_Cache_L2Size_0K,  /* l2Size */
};

/* atomicBlockSize__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Cache_atomicBlockSize__C, ".const:ti_sysbios_family_c7x_Cache_atomicBlockSize__C");
__FAR__ const CT__ti_sysbios_family_c7x_Cache_atomicBlockSize ti_sysbios_family_c7x_Cache_atomicBlockSize__C = (xdc_UInt32)0x400U;


/*
 * ======== ti.sysbios.family.c7x.Exception INITIALIZERS ========
 */

/* Module__state__V */
#ifdef __ti__
ti_sysbios_family_c7x_Exception_Module_State__ ti_sysbios_family_c7x_Exception_Module__state__V __attribute__ ((section(".data:ti_sysbios_family_c7x_Exception_Module__state__V")));
#elif !(defined(__MACH__) && defined(__APPLE__))
ti_sysbios_family_c7x_Exception_Module_State__ ti_sysbios_family_c7x_Exception_Module__state__V __attribute__ ((section(".data_ti_sysbios_family_c7x_Exception_Module__state__V")));
#endif
ti_sysbios_family_c7x_Exception_Module_State__ ti_sysbios_family_c7x_Exception_Module__state__V = {
    (xdc_Bits64)0x0U,  /* nrp */
    (xdc_Bits64)0x0U,  /* ntsr */
    (xdc_Bits64)0x0U,  /* ierr */
    (xdc_Bits64)0x0U,  /* iear */
    (xdc_Bits64)0x0U,  /* iesr */
    ((xdc_Void(*)(xdc_Void))NULL),  /* returnHook */
    ((ti_sysbios_family_c7x_Exception_Context*)NULL),  /* excContext */
    ((xdc_Char*)NULL),  /* excPtr */
    ((void*)0),  /* contextBuf */
};

/* Module__diagsEnabled__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Exception_Module__diagsEnabled__C, ".const:ti_sysbios_family_c7x_Exception_Module__diagsEnabled__C");
__FAR__ const CT__ti_sysbios_family_c7x_Exception_Module__diagsEnabled ti_sysbios_family_c7x_Exception_Module__diagsEnabled__C = (xdc_Bits32)0x190U;

/* Module__diagsIncluded__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Exception_Module__diagsIncluded__C, ".const:ti_sysbios_family_c7x_Exception_Module__diagsIncluded__C");
__FAR__ const CT__ti_sysbios_family_c7x_Exception_Module__diagsIncluded ti_sysbios_family_c7x_Exception_Module__diagsIncluded__C = (xdc_Bits32)0x190U;

/* Module__diagsMask__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Exception_Module__diagsMask__C, ".const:ti_sysbios_family_c7x_Exception_Module__diagsMask__C");
__FAR__ const CT__ti_sysbios_family_c7x_Exception_Module__diagsMask ti_sysbios_family_c7x_Exception_Module__diagsMask__C = ((const CT__ti_sysbios_family_c7x_Exception_Module__diagsMask)NULL);

/* Module__gateObj__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Exception_Module__gateObj__C, ".const:ti_sysbios_family_c7x_Exception_Module__gateObj__C");
__FAR__ const CT__ti_sysbios_family_c7x_Exception_Module__gateObj ti_sysbios_family_c7x_Exception_Module__gateObj__C = ((const CT__ti_sysbios_family_c7x_Exception_Module__gateObj)NULL);

/* Module__gatePrms__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Exception_Module__gatePrms__C, ".const:ti_sysbios_family_c7x_Exception_Module__gatePrms__C");
__FAR__ const CT__ti_sysbios_family_c7x_Exception_Module__gatePrms ti_sysbios_family_c7x_Exception_Module__gatePrms__C = ((const CT__ti_sysbios_family_c7x_Exception_Module__gatePrms)NULL);

/* Module__id__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Exception_Module__id__C, ".const:ti_sysbios_family_c7x_Exception_Module__id__C");
__FAR__ const CT__ti_sysbios_family_c7x_Exception_Module__id ti_sysbios_family_c7x_Exception_Module__id__C = (xdc_Bits16)0x8018U;

/* Module__loggerDefined__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Exception_Module__loggerDefined__C, ".const:ti_sysbios_family_c7x_Exception_Module__loggerDefined__C");
__FAR__ const CT__ti_sysbios_family_c7x_Exception_Module__loggerDefined ti_sysbios_family_c7x_Exception_Module__loggerDefined__C = 0;

/* Module__loggerObj__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Exception_Module__loggerObj__C, ".const:ti_sysbios_family_c7x_Exception_Module__loggerObj__C");
__FAR__ const CT__ti_sysbios_family_c7x_Exception_Module__loggerObj ti_sysbios_family_c7x_Exception_Module__loggerObj__C = ((const CT__ti_sysbios_family_c7x_Exception_Module__loggerObj)NULL);

/* Module__loggerFxn0__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Exception_Module__loggerFxn0__C, ".const:ti_sysbios_family_c7x_Exception_Module__loggerFxn0__C");
__FAR__ const CT__ti_sysbios_family_c7x_Exception_Module__loggerFxn0 ti_sysbios_family_c7x_Exception_Module__loggerFxn0__C = ((const CT__ti_sysbios_family_c7x_Exception_Module__loggerFxn0)NULL);

/* Module__loggerFxn1__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Exception_Module__loggerFxn1__C, ".const:ti_sysbios_family_c7x_Exception_Module__loggerFxn1__C");
__FAR__ const CT__ti_sysbios_family_c7x_Exception_Module__loggerFxn1 ti_sysbios_family_c7x_Exception_Module__loggerFxn1__C = ((const CT__ti_sysbios_family_c7x_Exception_Module__loggerFxn1)NULL);

/* Module__loggerFxn2__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Exception_Module__loggerFxn2__C, ".const:ti_sysbios_family_c7x_Exception_Module__loggerFxn2__C");
__FAR__ const CT__ti_sysbios_family_c7x_Exception_Module__loggerFxn2 ti_sysbios_family_c7x_Exception_Module__loggerFxn2__C = ((const CT__ti_sysbios_family_c7x_Exception_Module__loggerFxn2)NULL);

/* Module__loggerFxn4__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Exception_Module__loggerFxn4__C, ".const:ti_sysbios_family_c7x_Exception_Module__loggerFxn4__C");
__FAR__ const CT__ti_sysbios_family_c7x_Exception_Module__loggerFxn4 ti_sysbios_family_c7x_Exception_Module__loggerFxn4__C = ((const CT__ti_sysbios_family_c7x_Exception_Module__loggerFxn4)NULL);

/* Module__loggerFxn8__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Exception_Module__loggerFxn8__C, ".const:ti_sysbios_family_c7x_Exception_Module__loggerFxn8__C");
__FAR__ const CT__ti_sysbios_family_c7x_Exception_Module__loggerFxn8 ti_sysbios_family_c7x_Exception_Module__loggerFxn8__C = ((const CT__ti_sysbios_family_c7x_Exception_Module__loggerFxn8)NULL);

/* Object__count__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Exception_Object__count__C, ".const:ti_sysbios_family_c7x_Exception_Object__count__C");
__FAR__ const CT__ti_sysbios_family_c7x_Exception_Object__count ti_sysbios_family_c7x_Exception_Object__count__C = 0;

/* Object__heap__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Exception_Object__heap__C, ".const:ti_sysbios_family_c7x_Exception_Object__heap__C");
__FAR__ const CT__ti_sysbios_family_c7x_Exception_Object__heap ti_sysbios_family_c7x_Exception_Object__heap__C = NULL;

/* Object__sizeof__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Exception_Object__sizeof__C, ".const:ti_sysbios_family_c7x_Exception_Object__sizeof__C");
__FAR__ const CT__ti_sysbios_family_c7x_Exception_Object__sizeof ti_sysbios_family_c7x_Exception_Object__sizeof__C = 0;

/* Object__table__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Exception_Object__table__C, ".const:ti_sysbios_family_c7x_Exception_Object__table__C");
__FAR__ const CT__ti_sysbios_family_c7x_Exception_Object__table ti_sysbios_family_c7x_Exception_Object__table__C = NULL;

/* E_exceptionMin__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Exception_E_exceptionMin__C, ".const:ti_sysbios_family_c7x_Exception_E_exceptionMin__C");
__FAR__ const CT__ti_sysbios_family_c7x_Exception_E_exceptionMin ti_sysbios_family_c7x_Exception_E_exceptionMin__C = (((xdc_runtime_Error_Id)4046) << 16 | 0U);

/* E_exceptionMax__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Exception_E_exceptionMax__C, ".const:ti_sysbios_family_c7x_Exception_E_exceptionMax__C");
__FAR__ const CT__ti_sysbios_family_c7x_Exception_E_exceptionMax ti_sysbios_family_c7x_Exception_E_exceptionMax__C = (((xdc_runtime_Error_Id)4188) << 16 | 0U);

/* useInternalBuffer__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Exception_useInternalBuffer__C, ".const:ti_sysbios_family_c7x_Exception_useInternalBuffer__C");
__FAR__ const CT__ti_sysbios_family_c7x_Exception_useInternalBuffer ti_sysbios_family_c7x_Exception_useInternalBuffer__C = 0;

/* enablePrint__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Exception_enablePrint__C, ".const:ti_sysbios_family_c7x_Exception_enablePrint__C");
__FAR__ const CT__ti_sysbios_family_c7x_Exception_enablePrint ti_sysbios_family_c7x_Exception_enablePrint__C = 1;

/* exceptionHook__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Exception_exceptionHook__C, ".const:ti_sysbios_family_c7x_Exception_exceptionHook__C");
__FAR__ const CT__ti_sysbios_family_c7x_Exception_exceptionHook ti_sysbios_family_c7x_Exception_exceptionHook__C = ((const CT__ti_sysbios_family_c7x_Exception_exceptionHook)NULL);

/* internalHook__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Exception_internalHook__C, ".const:ti_sysbios_family_c7x_Exception_internalHook__C");
__FAR__ const CT__ti_sysbios_family_c7x_Exception_internalHook ti_sysbios_family_c7x_Exception_internalHook__C = ((const CT__ti_sysbios_family_c7x_Exception_internalHook)NULL);

/* returnHook__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Exception_returnHook__C, ".const:ti_sysbios_family_c7x_Exception_returnHook__C");
__FAR__ const CT__ti_sysbios_family_c7x_Exception_returnHook ti_sysbios_family_c7x_Exception_returnHook__C = ((const CT__ti_sysbios_family_c7x_Exception_returnHook)NULL);


/*
 * ======== ti.sysbios.family.c7x.Hwi INITIALIZERS ========
 */

/* Object__DESC__C */
__FAR__ const xdc_runtime_Core_ObjDesc ti_sysbios_family_c7x_Hwi_Object__DESC__C;

/* Object__PARAMS__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Hwi_Object__PARAMS__C, ".const:ti_sysbios_family_c7x_Hwi_Object__PARAMS__C");
__FAR__ const ti_sysbios_family_c7x_Hwi_Params ti_sysbios_family_c7x_Hwi_Object__PARAMS__C = {
    sizeof (ti_sysbios_family_c7x_Hwi_Params), /* __size */
    NULL, /* __self */
    NULL, /* __fxns */
    (xdc_runtime_IInstance_Params*)&ti_sysbios_family_c7x_Hwi_Object__PARAMS__C.__iprms, /* instance */
    ti_sysbios_interfaces_IHwi_MaskingOption_SELF,  /* maskSetting */
    ((xdc_UArg)(0x0)),  /* arg */
    1,  /* enableInt */
    (xdc_Int)(-0x0 - 1),  /* eventId */
    (xdc_Int)(-0x0 - 1),  /* priority */
    (xdc_ULong)0x0UL,  /* disableMask */
    (xdc_ULong)0x0UL,  /* restoreMask */
    {
        sizeof (xdc_runtime_IInstance_Params), /* __size */
        0,  /* name */
    },  /* instance */
};

/* --> ti_sysbios_family_c7x_Hwi_Instance_State_0_hookEnv__A */
__T1_ti_sysbios_family_c7x_Hwi_Instance_State__hookEnv ti_sysbios_family_c7x_Hwi_Instance_State_0_hookEnv__A[1];

/* Module__root__V */
ti_sysbios_family_c7x_Hwi_Module__ ti_sysbios_family_c7x_Hwi_Module__root__V = {
    {&ti_sysbios_family_c7x_Hwi_Module__root__V.link,  /* link.next */
    &ti_sysbios_family_c7x_Hwi_Module__root__V.link},  /* link.prev */
};

/* Object__table__V */
ti_sysbios_family_c7x_Hwi_Object__ ti_sysbios_family_c7x_Hwi_Object__table__V[1] = {
    {/* instance#0 */
        0,
        (xdc_ULong)0x8000UL,  /* disableMask */
        (xdc_ULong)0x8000UL,  /* restoreMask */
        ((xdc_UArg)((void*)(ti_sysbios_timers_dmtimer_Timer_Handle)&ti_sysbios_timers_dmtimer_Timer_Object__table__V[0])),  /* arg */
        ((xdc_Void(*)(xdc_UArg f_arg0))(ti_sysbios_timers_dmtimer_Timer_stub__E)),  /* fxn */
        (xdc_Int)0xf,  /* intNum */
        (xdc_Int)0x1,  /* priority */
        ((xdc_UArg)NULL),  /* irp */
        ((void*)ti_sysbios_family_c7x_Hwi_Instance_State_0_hookEnv__A),  /* hookEnv */
    },
};

/* Module__state__V */
#ifdef __ti__
ti_sysbios_family_c7x_Hwi_Module_State__ ti_sysbios_family_c7x_Hwi_Module__state__V __attribute__ ((section(".data:ti_sysbios_family_c7x_Hwi_Module__state__V")));
#elif !(defined(__MACH__) && defined(__APPLE__))
ti_sysbios_family_c7x_Hwi_Module_State__ ti_sysbios_family_c7x_Hwi_Module__state__V __attribute__ ((section(".data_ti_sysbios_family_c7x_Hwi_Module__state__V")));
#endif
ti_sysbios_family_c7x_Hwi_Module_State__ ti_sysbios_family_c7x_Hwi_Module__state__V = {
    {
        (xdc_Char)(-0x0 - 1),  /* [0] */
        (xdc_Char)(-0x0 - 1),  /* [1] */
        (xdc_Char)(-0x0 - 1),  /* [2] */
        (xdc_Char)(-0x0 - 1),  /* [3] */
        (xdc_Char)(-0x0 - 1),  /* [4] */
        (xdc_Char)(-0x0 - 1),  /* [5] */
        (xdc_Char)(-0x0 - 1),  /* [6] */
        (xdc_Char)(-0x0 - 1),  /* [7] */
        (xdc_Char)(-0x0 - 1),  /* [8] */
        (xdc_Char)(-0x0 - 1),  /* [9] */
        (xdc_Char)(-0x0 - 1),  /* [10] */
        (xdc_Char)(-0x0 - 1),  /* [11] */
        (xdc_Char)(-0x0 - 1),  /* [12] */
        (xdc_Char)(-0x0 - 1),  /* [13] */
        (xdc_Char)(-0x0 - 1),  /* [14] */
        (xdc_Char)0x4e1,  /* [15] */
        (xdc_Char)(-0x0 - 1),  /* [16] */
        (xdc_Char)(-0x0 - 1),  /* [17] */
        (xdc_Char)(-0x0 - 1),  /* [18] */
        (xdc_Char)(-0x0 - 1),  /* [19] */
        (xdc_Char)(-0x0 - 1),  /* [20] */
        (xdc_Char)(-0x0 - 1),  /* [21] */
        (xdc_Char)(-0x0 - 1),  /* [22] */
        (xdc_Char)(-0x0 - 1),  /* [23] */
        (xdc_Char)(-0x0 - 1),  /* [24] */
        (xdc_Char)(-0x0 - 1),  /* [25] */
        (xdc_Char)(-0x0 - 1),  /* [26] */
        (xdc_Char)(-0x0 - 1),  /* [27] */
        (xdc_Char)(-0x0 - 1),  /* [28] */
        (xdc_Char)(-0x0 - 1),  /* [29] */
        (xdc_Char)(-0x0 - 1),  /* [30] */
        (xdc_Char)(-0x0 - 1),  /* [31] */
        (xdc_Char)(-0x0 - 1),  /* [32] */
        (xdc_Char)(-0x0 - 1),  /* [33] */
        (xdc_Char)(-0x0 - 1),  /* [34] */
        (xdc_Char)(-0x0 - 1),  /* [35] */
        (xdc_Char)(-0x0 - 1),  /* [36] */
        (xdc_Char)(-0x0 - 1),  /* [37] */
        (xdc_Char)(-0x0 - 1),  /* [38] */
        (xdc_Char)(-0x0 - 1),  /* [39] */
        (xdc_Char)(-0x0 - 1),  /* [40] */
        (xdc_Char)(-0x0 - 1),  /* [41] */
        (xdc_Char)(-0x0 - 1),  /* [42] */
        (xdc_Char)(-0x0 - 1),  /* [43] */
        (xdc_Char)(-0x0 - 1),  /* [44] */
        (xdc_Char)(-0x0 - 1),  /* [45] */
        (xdc_Char)(-0x0 - 1),  /* [46] */
        (xdc_Char)(-0x0 - 1),  /* [47] */
        (xdc_Char)(-0x0 - 1),  /* [48] */
        (xdc_Char)(-0x0 - 1),  /* [49] */
        (xdc_Char)(-0x0 - 1),  /* [50] */
        (xdc_Char)(-0x0 - 1),  /* [51] */
        (xdc_Char)(-0x0 - 1),  /* [52] */
        (xdc_Char)(-0x0 - 1),  /* [53] */
        (xdc_Char)(-0x0 - 1),  /* [54] */
        (xdc_Char)(-0x0 - 1),  /* [55] */
        (xdc_Char)(-0x0 - 1),  /* [56] */
        (xdc_Char)(-0x0 - 1),  /* [57] */
        (xdc_Char)(-0x0 - 1),  /* [58] */
        (xdc_Char)(-0x0 - 1),  /* [59] */
        (xdc_Char)(-0x0 - 1),  /* [60] */
        (xdc_Char)(-0x0 - 1),  /* [61] */
        (xdc_Char)(-0x0 - 1),  /* [62] */
        (xdc_Char)(-0x0 - 1),  /* [63] */
    },  /* intEvents */
    (xdc_ULong)0x8003UL,  /* ierMask */
    (xdc_Int)0x0,  /* intNum */
    ((xdc_Char*)NULL),  /* taskSP */
    ((xdc_Char*)NULL),  /* isrStack */
    (xdc_Int)0x0,  /* scw */
    {
        0,  /* [0] */
        0,  /* [1] */
        0,  /* [2] */
        0,  /* [3] */
        0,  /* [4] */
        0,  /* [5] */
        0,  /* [6] */
        0,  /* [7] */
        0,  /* [8] */
        0,  /* [9] */
        0,  /* [10] */
        0,  /* [11] */
        0,  /* [12] */
        0,  /* [13] */
        0,  /* [14] */
        (ti_sysbios_family_c7x_Hwi_Handle)&ti_sysbios_family_c7x_Hwi_Object__table__V[0],  /* [15] */
        0,  /* [16] */
        0,  /* [17] */
        0,  /* [18] */
        0,  /* [19] */
        0,  /* [20] */
        0,  /* [21] */
        0,  /* [22] */
        0,  /* [23] */
        0,  /* [24] */
        0,  /* [25] */
        0,  /* [26] */
        0,  /* [27] */
        0,  /* [28] */
        0,  /* [29] */
        0,  /* [30] */
        0,  /* [31] */
        0,  /* [32] */
        0,  /* [33] */
        0,  /* [34] */
        0,  /* [35] */
        0,  /* [36] */
        0,  /* [37] */
        0,  /* [38] */
        0,  /* [39] */
        0,  /* [40] */
        0,  /* [41] */
        0,  /* [42] */
        0,  /* [43] */
        0,  /* [44] */
        0,  /* [45] */
        0,  /* [46] */
        0,  /* [47] */
        0,  /* [48] */
        0,  /* [49] */
        0,  /* [50] */
        0,  /* [51] */
        0,  /* [52] */
        0,  /* [53] */
        0,  /* [54] */
        0,  /* [55] */
        0,  /* [56] */
        0,  /* [57] */
        0,  /* [58] */
        0,  /* [59] */
        0,  /* [60] */
        0,  /* [61] */
        0,  /* [62] */
        0,  /* [63] */
    },  /* dispatchTable */
};

/* --> ti_sysbios_family_c7x_Hwi_hooks__A */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Hwi_hooks__A, ".const:ti_sysbios_family_c7x_Hwi_hooks__A");
const __T1_ti_sysbios_family_c7x_Hwi_hooks ti_sysbios_family_c7x_Hwi_hooks__A[1] = {
    {
        ((xdc_Void(*)(xdc_Int f_arg0))NULL),  /* registerFxn */
        ((xdc_Void(*)(ti_sysbios_interfaces_IHwi_Handle f_arg0,xdc_runtime_Error_Block* f_arg1))NULL),  /* createFxn */
        ((xdc_Void(*)(ti_sysbios_interfaces_IHwi_Handle f_arg0))(ti_sysbios_utils_Load_hwiBeginHook__E)),  /* beginFxn */
        ((xdc_Void(*)(ti_sysbios_interfaces_IHwi_Handle f_arg0))(ti_sysbios_utils_Load_hwiEndHook__E)),  /* endFxn */
        ((xdc_Void(*)(ti_sysbios_interfaces_IHwi_Handle f_arg0))NULL),  /* deleteFxn */
    },  /* [0] */
};

/* Module__diagsEnabled__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Hwi_Module__diagsEnabled__C, ".const:ti_sysbios_family_c7x_Hwi_Module__diagsEnabled__C");
__FAR__ const CT__ti_sysbios_family_c7x_Hwi_Module__diagsEnabled ti_sysbios_family_c7x_Hwi_Module__diagsEnabled__C = (xdc_Bits32)0x90U;

/* Module__diagsIncluded__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Hwi_Module__diagsIncluded__C, ".const:ti_sysbios_family_c7x_Hwi_Module__diagsIncluded__C");
__FAR__ const CT__ti_sysbios_family_c7x_Hwi_Module__diagsIncluded ti_sysbios_family_c7x_Hwi_Module__diagsIncluded__C = (xdc_Bits32)0x90U;

/* Module__diagsMask__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Hwi_Module__diagsMask__C, ".const:ti_sysbios_family_c7x_Hwi_Module__diagsMask__C");
__FAR__ const CT__ti_sysbios_family_c7x_Hwi_Module__diagsMask ti_sysbios_family_c7x_Hwi_Module__diagsMask__C = ((const CT__ti_sysbios_family_c7x_Hwi_Module__diagsMask)NULL);

/* Module__gateObj__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Hwi_Module__gateObj__C, ".const:ti_sysbios_family_c7x_Hwi_Module__gateObj__C");
__FAR__ const CT__ti_sysbios_family_c7x_Hwi_Module__gateObj ti_sysbios_family_c7x_Hwi_Module__gateObj__C = ((const CT__ti_sysbios_family_c7x_Hwi_Module__gateObj)NULL);

/* Module__gatePrms__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Hwi_Module__gatePrms__C, ".const:ti_sysbios_family_c7x_Hwi_Module__gatePrms__C");
__FAR__ const CT__ti_sysbios_family_c7x_Hwi_Module__gatePrms ti_sysbios_family_c7x_Hwi_Module__gatePrms__C = ((const CT__ti_sysbios_family_c7x_Hwi_Module__gatePrms)NULL);

/* Module__id__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Hwi_Module__id__C, ".const:ti_sysbios_family_c7x_Hwi_Module__id__C");
__FAR__ const CT__ti_sysbios_family_c7x_Hwi_Module__id ti_sysbios_family_c7x_Hwi_Module__id__C = (xdc_Bits16)0x8019U;

/* Module__loggerDefined__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Hwi_Module__loggerDefined__C, ".const:ti_sysbios_family_c7x_Hwi_Module__loggerDefined__C");
__FAR__ const CT__ti_sysbios_family_c7x_Hwi_Module__loggerDefined ti_sysbios_family_c7x_Hwi_Module__loggerDefined__C = 0;

/* Module__loggerObj__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Hwi_Module__loggerObj__C, ".const:ti_sysbios_family_c7x_Hwi_Module__loggerObj__C");
__FAR__ const CT__ti_sysbios_family_c7x_Hwi_Module__loggerObj ti_sysbios_family_c7x_Hwi_Module__loggerObj__C = ((const CT__ti_sysbios_family_c7x_Hwi_Module__loggerObj)NULL);

/* Module__loggerFxn0__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Hwi_Module__loggerFxn0__C, ".const:ti_sysbios_family_c7x_Hwi_Module__loggerFxn0__C");
__FAR__ const CT__ti_sysbios_family_c7x_Hwi_Module__loggerFxn0 ti_sysbios_family_c7x_Hwi_Module__loggerFxn0__C = ((const CT__ti_sysbios_family_c7x_Hwi_Module__loggerFxn0)NULL);

/* Module__loggerFxn1__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Hwi_Module__loggerFxn1__C, ".const:ti_sysbios_family_c7x_Hwi_Module__loggerFxn1__C");
__FAR__ const CT__ti_sysbios_family_c7x_Hwi_Module__loggerFxn1 ti_sysbios_family_c7x_Hwi_Module__loggerFxn1__C = ((const CT__ti_sysbios_family_c7x_Hwi_Module__loggerFxn1)NULL);

/* Module__loggerFxn2__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Hwi_Module__loggerFxn2__C, ".const:ti_sysbios_family_c7x_Hwi_Module__loggerFxn2__C");
__FAR__ const CT__ti_sysbios_family_c7x_Hwi_Module__loggerFxn2 ti_sysbios_family_c7x_Hwi_Module__loggerFxn2__C = ((const CT__ti_sysbios_family_c7x_Hwi_Module__loggerFxn2)NULL);

/* Module__loggerFxn4__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Hwi_Module__loggerFxn4__C, ".const:ti_sysbios_family_c7x_Hwi_Module__loggerFxn4__C");
__FAR__ const CT__ti_sysbios_family_c7x_Hwi_Module__loggerFxn4 ti_sysbios_family_c7x_Hwi_Module__loggerFxn4__C = ((const CT__ti_sysbios_family_c7x_Hwi_Module__loggerFxn4)NULL);

/* Module__loggerFxn8__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Hwi_Module__loggerFxn8__C, ".const:ti_sysbios_family_c7x_Hwi_Module__loggerFxn8__C");
__FAR__ const CT__ti_sysbios_family_c7x_Hwi_Module__loggerFxn8 ti_sysbios_family_c7x_Hwi_Module__loggerFxn8__C = ((const CT__ti_sysbios_family_c7x_Hwi_Module__loggerFxn8)NULL);

/* Object__count__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Hwi_Object__count__C, ".const:ti_sysbios_family_c7x_Hwi_Object__count__C");
__FAR__ const CT__ti_sysbios_family_c7x_Hwi_Object__count ti_sysbios_family_c7x_Hwi_Object__count__C = 1;

/* Object__heap__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Hwi_Object__heap__C, ".const:ti_sysbios_family_c7x_Hwi_Object__heap__C");
__FAR__ const CT__ti_sysbios_family_c7x_Hwi_Object__heap ti_sysbios_family_c7x_Hwi_Object__heap__C = NULL;

/* Object__sizeof__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Hwi_Object__sizeof__C, ".const:ti_sysbios_family_c7x_Hwi_Object__sizeof__C");
__FAR__ const CT__ti_sysbios_family_c7x_Hwi_Object__sizeof ti_sysbios_family_c7x_Hwi_Object__sizeof__C = sizeof(ti_sysbios_family_c7x_Hwi_Object__);

/* Object__table__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Hwi_Object__table__C, ".const:ti_sysbios_family_c7x_Hwi_Object__table__C");
__FAR__ const CT__ti_sysbios_family_c7x_Hwi_Object__table ti_sysbios_family_c7x_Hwi_Object__table__C = ti_sysbios_family_c7x_Hwi_Object__table__V;

/* dispatcherAutoNestingSupport__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Hwi_dispatcherAutoNestingSupport__C, ".const:ti_sysbios_family_c7x_Hwi_dispatcherAutoNestingSupport__C");
__FAR__ const CT__ti_sysbios_family_c7x_Hwi_dispatcherAutoNestingSupport ti_sysbios_family_c7x_Hwi_dispatcherAutoNestingSupport__C = 1;

/* dispatcherSwiSupport__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Hwi_dispatcherSwiSupport__C, ".const:ti_sysbios_family_c7x_Hwi_dispatcherSwiSupport__C");
__FAR__ const CT__ti_sysbios_family_c7x_Hwi_dispatcherSwiSupport ti_sysbios_family_c7x_Hwi_dispatcherSwiSupport__C = 1;

/* dispatcherTaskSupport__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Hwi_dispatcherTaskSupport__C, ".const:ti_sysbios_family_c7x_Hwi_dispatcherTaskSupport__C");
__FAR__ const CT__ti_sysbios_family_c7x_Hwi_dispatcherTaskSupport ti_sysbios_family_c7x_Hwi_dispatcherTaskSupport__C = 1;

/* dispatcherIrpTrackingSupport__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Hwi_dispatcherIrpTrackingSupport__C, ".const:ti_sysbios_family_c7x_Hwi_dispatcherIrpTrackingSupport__C");
__FAR__ const CT__ti_sysbios_family_c7x_Hwi_dispatcherIrpTrackingSupport ti_sysbios_family_c7x_Hwi_dispatcherIrpTrackingSupport__C = 1;

/* DEFAULT_INT_PRIORITY__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Hwi_DEFAULT_INT_PRIORITY__C, ".const:ti_sysbios_family_c7x_Hwi_DEFAULT_INT_PRIORITY__C");
__FAR__ const CT__ti_sysbios_family_c7x_Hwi_DEFAULT_INT_PRIORITY ti_sysbios_family_c7x_Hwi_DEFAULT_INT_PRIORITY__C = (xdc_UInt)0x6U;

/* bootToNonSecure__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Hwi_bootToNonSecure__C, ".const:ti_sysbios_family_c7x_Hwi_bootToNonSecure__C");
__FAR__ const CT__ti_sysbios_family_c7x_Hwi_bootToNonSecure ti_sysbios_family_c7x_Hwi_bootToNonSecure__C = 1;

/* A_invalidPriority__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Hwi_A_invalidPriority__C, ".const:ti_sysbios_family_c7x_Hwi_A_invalidPriority__C");
__FAR__ const CT__ti_sysbios_family_c7x_Hwi_A_invalidPriority ti_sysbios_family_c7x_Hwi_A_invalidPriority__C = (((xdc_runtime_Assert_Id)400) << 16 | 16);

/* E_alreadyDefined__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Hwi_E_alreadyDefined__C, ".const:ti_sysbios_family_c7x_Hwi_E_alreadyDefined__C");
__FAR__ const CT__ti_sysbios_family_c7x_Hwi_E_alreadyDefined ti_sysbios_family_c7x_Hwi_E_alreadyDefined__C = (((xdc_runtime_Error_Id)4230) << 16 | 0U);

/* E_handleNotFound__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Hwi_E_handleNotFound__C, ".const:ti_sysbios_family_c7x_Hwi_E_handleNotFound__C");
__FAR__ const CT__ti_sysbios_family_c7x_Hwi_E_handleNotFound ti_sysbios_family_c7x_Hwi_E_handleNotFound__C = (((xdc_runtime_Error_Id)4278) << 16 | 0U);

/* E_allocSCFailed__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Hwi_E_allocSCFailed__C, ".const:ti_sysbios_family_c7x_Hwi_E_allocSCFailed__C");
__FAR__ const CT__ti_sysbios_family_c7x_Hwi_E_allocSCFailed ti_sysbios_family_c7x_Hwi_E_allocSCFailed__C = (((xdc_runtime_Error_Id)4323) << 16 | 0U);

/* E_registerSCFailed__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Hwi_E_registerSCFailed__C, ".const:ti_sysbios_family_c7x_Hwi_E_registerSCFailed__C");
__FAR__ const CT__ti_sysbios_family_c7x_Hwi_E_registerSCFailed ti_sysbios_family_c7x_Hwi_E_registerSCFailed__C = (((xdc_runtime_Error_Id)4368) << 16 | 0U);

/* E_invalidIntNum__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Hwi_E_invalidIntNum__C, ".const:ti_sysbios_family_c7x_Hwi_E_invalidIntNum__C");
__FAR__ const CT__ti_sysbios_family_c7x_Hwi_E_invalidIntNum ti_sysbios_family_c7x_Hwi_E_invalidIntNum__C = (((xdc_runtime_Error_Id)4419) << 16 | 0U);

/* E_invalidPriority__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Hwi_E_invalidPriority__C, ".const:ti_sysbios_family_c7x_Hwi_E_invalidPriority__C");
__FAR__ const CT__ti_sysbios_family_c7x_Hwi_E_invalidPriority ti_sysbios_family_c7x_Hwi_E_invalidPriority__C = (((xdc_runtime_Error_Id)4471) << 16 | 0U);

/* LM_begin__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Hwi_LM_begin__C, ".const:ti_sysbios_family_c7x_Hwi_LM_begin__C");
__FAR__ const CT__ti_sysbios_family_c7x_Hwi_LM_begin ti_sysbios_family_c7x_Hwi_LM_begin__C = (((xdc_runtime_Log_Event)5771) << 16 | 768);

/* LD_end__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Hwi_LD_end__C, ".const:ti_sysbios_family_c7x_Hwi_LD_end__C");
__FAR__ const CT__ti_sysbios_family_c7x_Hwi_LD_end ti_sysbios_family_c7x_Hwi_LD_end__C = (((xdc_runtime_Log_Event)5841) << 16 | 512);

/* enableException__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Hwi_enableException__C, ".const:ti_sysbios_family_c7x_Hwi_enableException__C");
__FAR__ const CT__ti_sysbios_family_c7x_Hwi_enableException ti_sysbios_family_c7x_Hwi_enableException__C = 1;

/* vectorTableBase__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Hwi_vectorTableBase__C, ".const:ti_sysbios_family_c7x_Hwi_vectorTableBase__C");
__FAR__ const CT__ti_sysbios_family_c7x_Hwi_vectorTableBase ti_sysbios_family_c7x_Hwi_vectorTableBase__C = ((const CT__ti_sysbios_family_c7x_Hwi_vectorTableBase)((void*)&soft_reset));

/* vectorTableBase_SS__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Hwi_vectorTableBase_SS__C, ".const:ti_sysbios_family_c7x_Hwi_vectorTableBase_SS__C");
__FAR__ const CT__ti_sysbios_family_c7x_Hwi_vectorTableBase_SS ti_sysbios_family_c7x_Hwi_vectorTableBase_SS__C = ((const CT__ti_sysbios_family_c7x_Hwi_vectorTableBase_SS)((void*)&secure_soft_reset));

/* swiDisable__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Hwi_swiDisable__C, ".const:ti_sysbios_family_c7x_Hwi_swiDisable__C");
__FAR__ const CT__ti_sysbios_family_c7x_Hwi_swiDisable ti_sysbios_family_c7x_Hwi_swiDisable__C = ((const CT__ti_sysbios_family_c7x_Hwi_swiDisable)(ti_sysbios_knl_Swi_disable__E));

/* swiRestoreHwi__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Hwi_swiRestoreHwi__C, ".const:ti_sysbios_family_c7x_Hwi_swiRestoreHwi__C");
__FAR__ const CT__ti_sysbios_family_c7x_Hwi_swiRestoreHwi ti_sysbios_family_c7x_Hwi_swiRestoreHwi__C = ((const CT__ti_sysbios_family_c7x_Hwi_swiRestoreHwi)(ti_sysbios_knl_Swi_restoreHwi__E));

/* taskDisable__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Hwi_taskDisable__C, ".const:ti_sysbios_family_c7x_Hwi_taskDisable__C");
__FAR__ const CT__ti_sysbios_family_c7x_Hwi_taskDisable ti_sysbios_family_c7x_Hwi_taskDisable__C = ((const CT__ti_sysbios_family_c7x_Hwi_taskDisable)(ti_sysbios_knl_Task_disable__E));

/* taskRestoreHwi__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Hwi_taskRestoreHwi__C, ".const:ti_sysbios_family_c7x_Hwi_taskRestoreHwi__C");
__FAR__ const CT__ti_sysbios_family_c7x_Hwi_taskRestoreHwi ti_sysbios_family_c7x_Hwi_taskRestoreHwi__C = ((const CT__ti_sysbios_family_c7x_Hwi_taskRestoreHwi)(ti_sysbios_knl_Task_restoreHwi__E));

/* hooks__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Hwi_hooks__C, ".const:ti_sysbios_family_c7x_Hwi_hooks__C");
__FAR__ const CT__ti_sysbios_family_c7x_Hwi_hooks ti_sysbios_family_c7x_Hwi_hooks__C = {1, ((__T1_ti_sysbios_family_c7x_Hwi_hooks const  *)ti_sysbios_family_c7x_Hwi_hooks__A)};


/*
 * ======== ti.sysbios.family.c7x.IntrinsicsSupport INITIALIZERS ========
 */

/* Module__diagsEnabled__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_IntrinsicsSupport_Module__diagsEnabled__C, ".const:ti_sysbios_family_c7x_IntrinsicsSupport_Module__diagsEnabled__C");
__FAR__ const CT__ti_sysbios_family_c7x_IntrinsicsSupport_Module__diagsEnabled ti_sysbios_family_c7x_IntrinsicsSupport_Module__diagsEnabled__C = (xdc_Bits32)0x90U;

/* Module__diagsIncluded__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_IntrinsicsSupport_Module__diagsIncluded__C, ".const:ti_sysbios_family_c7x_IntrinsicsSupport_Module__diagsIncluded__C");
__FAR__ const CT__ti_sysbios_family_c7x_IntrinsicsSupport_Module__diagsIncluded ti_sysbios_family_c7x_IntrinsicsSupport_Module__diagsIncluded__C = (xdc_Bits32)0x90U;

/* Module__diagsMask__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_IntrinsicsSupport_Module__diagsMask__C, ".const:ti_sysbios_family_c7x_IntrinsicsSupport_Module__diagsMask__C");
__FAR__ const CT__ti_sysbios_family_c7x_IntrinsicsSupport_Module__diagsMask ti_sysbios_family_c7x_IntrinsicsSupport_Module__diagsMask__C = ((const CT__ti_sysbios_family_c7x_IntrinsicsSupport_Module__diagsMask)NULL);

/* Module__gateObj__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_IntrinsicsSupport_Module__gateObj__C, ".const:ti_sysbios_family_c7x_IntrinsicsSupport_Module__gateObj__C");
__FAR__ const CT__ti_sysbios_family_c7x_IntrinsicsSupport_Module__gateObj ti_sysbios_family_c7x_IntrinsicsSupport_Module__gateObj__C = ((const CT__ti_sysbios_family_c7x_IntrinsicsSupport_Module__gateObj)NULL);

/* Module__gatePrms__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_IntrinsicsSupport_Module__gatePrms__C, ".const:ti_sysbios_family_c7x_IntrinsicsSupport_Module__gatePrms__C");
__FAR__ const CT__ti_sysbios_family_c7x_IntrinsicsSupport_Module__gatePrms ti_sysbios_family_c7x_IntrinsicsSupport_Module__gatePrms__C = ((const CT__ti_sysbios_family_c7x_IntrinsicsSupport_Module__gatePrms)NULL);

/* Module__id__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_IntrinsicsSupport_Module__id__C, ".const:ti_sysbios_family_c7x_IntrinsicsSupport_Module__id__C");
__FAR__ const CT__ti_sysbios_family_c7x_IntrinsicsSupport_Module__id ti_sysbios_family_c7x_IntrinsicsSupport_Module__id__C = (xdc_Bits16)0x801aU;

/* Module__loggerDefined__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_IntrinsicsSupport_Module__loggerDefined__C, ".const:ti_sysbios_family_c7x_IntrinsicsSupport_Module__loggerDefined__C");
__FAR__ const CT__ti_sysbios_family_c7x_IntrinsicsSupport_Module__loggerDefined ti_sysbios_family_c7x_IntrinsicsSupport_Module__loggerDefined__C = 0;

/* Module__loggerObj__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_IntrinsicsSupport_Module__loggerObj__C, ".const:ti_sysbios_family_c7x_IntrinsicsSupport_Module__loggerObj__C");
__FAR__ const CT__ti_sysbios_family_c7x_IntrinsicsSupport_Module__loggerObj ti_sysbios_family_c7x_IntrinsicsSupport_Module__loggerObj__C = ((const CT__ti_sysbios_family_c7x_IntrinsicsSupport_Module__loggerObj)NULL);

/* Module__loggerFxn0__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_IntrinsicsSupport_Module__loggerFxn0__C, ".const:ti_sysbios_family_c7x_IntrinsicsSupport_Module__loggerFxn0__C");
__FAR__ const CT__ti_sysbios_family_c7x_IntrinsicsSupport_Module__loggerFxn0 ti_sysbios_family_c7x_IntrinsicsSupport_Module__loggerFxn0__C = ((const CT__ti_sysbios_family_c7x_IntrinsicsSupport_Module__loggerFxn0)NULL);

/* Module__loggerFxn1__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_IntrinsicsSupport_Module__loggerFxn1__C, ".const:ti_sysbios_family_c7x_IntrinsicsSupport_Module__loggerFxn1__C");
__FAR__ const CT__ti_sysbios_family_c7x_IntrinsicsSupport_Module__loggerFxn1 ti_sysbios_family_c7x_IntrinsicsSupport_Module__loggerFxn1__C = ((const CT__ti_sysbios_family_c7x_IntrinsicsSupport_Module__loggerFxn1)NULL);

/* Module__loggerFxn2__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_IntrinsicsSupport_Module__loggerFxn2__C, ".const:ti_sysbios_family_c7x_IntrinsicsSupport_Module__loggerFxn2__C");
__FAR__ const CT__ti_sysbios_family_c7x_IntrinsicsSupport_Module__loggerFxn2 ti_sysbios_family_c7x_IntrinsicsSupport_Module__loggerFxn2__C = ((const CT__ti_sysbios_family_c7x_IntrinsicsSupport_Module__loggerFxn2)NULL);

/* Module__loggerFxn4__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_IntrinsicsSupport_Module__loggerFxn4__C, ".const:ti_sysbios_family_c7x_IntrinsicsSupport_Module__loggerFxn4__C");
__FAR__ const CT__ti_sysbios_family_c7x_IntrinsicsSupport_Module__loggerFxn4 ti_sysbios_family_c7x_IntrinsicsSupport_Module__loggerFxn4__C = ((const CT__ti_sysbios_family_c7x_IntrinsicsSupport_Module__loggerFxn4)NULL);

/* Module__loggerFxn8__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_IntrinsicsSupport_Module__loggerFxn8__C, ".const:ti_sysbios_family_c7x_IntrinsicsSupport_Module__loggerFxn8__C");
__FAR__ const CT__ti_sysbios_family_c7x_IntrinsicsSupport_Module__loggerFxn8 ti_sysbios_family_c7x_IntrinsicsSupport_Module__loggerFxn8__C = ((const CT__ti_sysbios_family_c7x_IntrinsicsSupport_Module__loggerFxn8)NULL);

/* Object__count__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_IntrinsicsSupport_Object__count__C, ".const:ti_sysbios_family_c7x_IntrinsicsSupport_Object__count__C");
__FAR__ const CT__ti_sysbios_family_c7x_IntrinsicsSupport_Object__count ti_sysbios_family_c7x_IntrinsicsSupport_Object__count__C = 0;

/* Object__heap__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_IntrinsicsSupport_Object__heap__C, ".const:ti_sysbios_family_c7x_IntrinsicsSupport_Object__heap__C");
__FAR__ const CT__ti_sysbios_family_c7x_IntrinsicsSupport_Object__heap ti_sysbios_family_c7x_IntrinsicsSupport_Object__heap__C = NULL;

/* Object__sizeof__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_IntrinsicsSupport_Object__sizeof__C, ".const:ti_sysbios_family_c7x_IntrinsicsSupport_Object__sizeof__C");
__FAR__ const CT__ti_sysbios_family_c7x_IntrinsicsSupport_Object__sizeof ti_sysbios_family_c7x_IntrinsicsSupport_Object__sizeof__C = 0;

/* Object__table__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_IntrinsicsSupport_Object__table__C, ".const:ti_sysbios_family_c7x_IntrinsicsSupport_Object__table__C");
__FAR__ const CT__ti_sysbios_family_c7x_IntrinsicsSupport_Object__table ti_sysbios_family_c7x_IntrinsicsSupport_Object__table__C = NULL;


/*
 * ======== ti.sysbios.family.c7x.Mmu INITIALIZERS ========
 */

/* Module__state__V */
#ifdef __ti__
ti_sysbios_family_c7x_Mmu_Module_State__ ti_sysbios_family_c7x_Mmu_Module__state__V __attribute__ ((section(".data:ti_sysbios_family_c7x_Mmu_Module__state__V")));
#elif !(defined(__MACH__) && defined(__APPLE__))
ti_sysbios_family_c7x_Mmu_Module_State__ ti_sysbios_family_c7x_Mmu_Module__state__V __attribute__ ((section(".data_ti_sysbios_family_c7x_Mmu_Module__state__V")));
#endif
ti_sysbios_family_c7x_Mmu_Module_State__ ti_sysbios_family_c7x_Mmu_Module__state__V = {
};

/* Module__diagsEnabled__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Mmu_Module__diagsEnabled__C, ".const:ti_sysbios_family_c7x_Mmu_Module__diagsEnabled__C");
__FAR__ const CT__ti_sysbios_family_c7x_Mmu_Module__diagsEnabled ti_sysbios_family_c7x_Mmu_Module__diagsEnabled__C = (xdc_Bits32)0x90U;

/* Module__diagsIncluded__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Mmu_Module__diagsIncluded__C, ".const:ti_sysbios_family_c7x_Mmu_Module__diagsIncluded__C");
__FAR__ const CT__ti_sysbios_family_c7x_Mmu_Module__diagsIncluded ti_sysbios_family_c7x_Mmu_Module__diagsIncluded__C = (xdc_Bits32)0x90U;

/* Module__diagsMask__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Mmu_Module__diagsMask__C, ".const:ti_sysbios_family_c7x_Mmu_Module__diagsMask__C");
__FAR__ const CT__ti_sysbios_family_c7x_Mmu_Module__diagsMask ti_sysbios_family_c7x_Mmu_Module__diagsMask__C = ((const CT__ti_sysbios_family_c7x_Mmu_Module__diagsMask)NULL);

/* Module__gateObj__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Mmu_Module__gateObj__C, ".const:ti_sysbios_family_c7x_Mmu_Module__gateObj__C");
__FAR__ const CT__ti_sysbios_family_c7x_Mmu_Module__gateObj ti_sysbios_family_c7x_Mmu_Module__gateObj__C = ((const CT__ti_sysbios_family_c7x_Mmu_Module__gateObj)NULL);

/* Module__gatePrms__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Mmu_Module__gatePrms__C, ".const:ti_sysbios_family_c7x_Mmu_Module__gatePrms__C");
__FAR__ const CT__ti_sysbios_family_c7x_Mmu_Module__gatePrms ti_sysbios_family_c7x_Mmu_Module__gatePrms__C = ((const CT__ti_sysbios_family_c7x_Mmu_Module__gatePrms)NULL);

/* Module__id__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Mmu_Module__id__C, ".const:ti_sysbios_family_c7x_Mmu_Module__id__C");
__FAR__ const CT__ti_sysbios_family_c7x_Mmu_Module__id ti_sysbios_family_c7x_Mmu_Module__id__C = (xdc_Bits16)0x801bU;

/* Module__loggerDefined__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Mmu_Module__loggerDefined__C, ".const:ti_sysbios_family_c7x_Mmu_Module__loggerDefined__C");
__FAR__ const CT__ti_sysbios_family_c7x_Mmu_Module__loggerDefined ti_sysbios_family_c7x_Mmu_Module__loggerDefined__C = 0;

/* Module__loggerObj__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Mmu_Module__loggerObj__C, ".const:ti_sysbios_family_c7x_Mmu_Module__loggerObj__C");
__FAR__ const CT__ti_sysbios_family_c7x_Mmu_Module__loggerObj ti_sysbios_family_c7x_Mmu_Module__loggerObj__C = ((const CT__ti_sysbios_family_c7x_Mmu_Module__loggerObj)NULL);

/* Module__loggerFxn0__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Mmu_Module__loggerFxn0__C, ".const:ti_sysbios_family_c7x_Mmu_Module__loggerFxn0__C");
__FAR__ const CT__ti_sysbios_family_c7x_Mmu_Module__loggerFxn0 ti_sysbios_family_c7x_Mmu_Module__loggerFxn0__C = ((const CT__ti_sysbios_family_c7x_Mmu_Module__loggerFxn0)NULL);

/* Module__loggerFxn1__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Mmu_Module__loggerFxn1__C, ".const:ti_sysbios_family_c7x_Mmu_Module__loggerFxn1__C");
__FAR__ const CT__ti_sysbios_family_c7x_Mmu_Module__loggerFxn1 ti_sysbios_family_c7x_Mmu_Module__loggerFxn1__C = ((const CT__ti_sysbios_family_c7x_Mmu_Module__loggerFxn1)NULL);

/* Module__loggerFxn2__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Mmu_Module__loggerFxn2__C, ".const:ti_sysbios_family_c7x_Mmu_Module__loggerFxn2__C");
__FAR__ const CT__ti_sysbios_family_c7x_Mmu_Module__loggerFxn2 ti_sysbios_family_c7x_Mmu_Module__loggerFxn2__C = ((const CT__ti_sysbios_family_c7x_Mmu_Module__loggerFxn2)NULL);

/* Module__loggerFxn4__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Mmu_Module__loggerFxn4__C, ".const:ti_sysbios_family_c7x_Mmu_Module__loggerFxn4__C");
__FAR__ const CT__ti_sysbios_family_c7x_Mmu_Module__loggerFxn4 ti_sysbios_family_c7x_Mmu_Module__loggerFxn4__C = ((const CT__ti_sysbios_family_c7x_Mmu_Module__loggerFxn4)NULL);

/* Module__loggerFxn8__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Mmu_Module__loggerFxn8__C, ".const:ti_sysbios_family_c7x_Mmu_Module__loggerFxn8__C");
__FAR__ const CT__ti_sysbios_family_c7x_Mmu_Module__loggerFxn8 ti_sysbios_family_c7x_Mmu_Module__loggerFxn8__C = ((const CT__ti_sysbios_family_c7x_Mmu_Module__loggerFxn8)NULL);

/* Object__count__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Mmu_Object__count__C, ".const:ti_sysbios_family_c7x_Mmu_Object__count__C");
__FAR__ const CT__ti_sysbios_family_c7x_Mmu_Object__count ti_sysbios_family_c7x_Mmu_Object__count__C = 0;

/* Object__heap__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Mmu_Object__heap__C, ".const:ti_sysbios_family_c7x_Mmu_Object__heap__C");
__FAR__ const CT__ti_sysbios_family_c7x_Mmu_Object__heap ti_sysbios_family_c7x_Mmu_Object__heap__C = NULL;

/* Object__sizeof__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Mmu_Object__sizeof__C, ".const:ti_sysbios_family_c7x_Mmu_Object__sizeof__C");
__FAR__ const CT__ti_sysbios_family_c7x_Mmu_Object__sizeof ti_sysbios_family_c7x_Mmu_Object__sizeof__C = 0;

/* Object__table__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Mmu_Object__table__C, ".const:ti_sysbios_family_c7x_Mmu_Object__table__C");
__FAR__ const CT__ti_sysbios_family_c7x_Mmu_Object__table ti_sysbios_family_c7x_Mmu_Object__table__C = NULL;

/* A_nullPointer__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Mmu_A_nullPointer__C, ".const:ti_sysbios_family_c7x_Mmu_A_nullPointer__C");
__FAR__ const CT__ti_sysbios_family_c7x_Mmu_A_nullPointer ti_sysbios_family_c7x_Mmu_A_nullPointer__C = (((xdc_runtime_Assert_Id)448) << 16 | 16);

/* A_vaddrOutOfRange__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Mmu_A_vaddrOutOfRange__C, ".const:ti_sysbios_family_c7x_Mmu_A_vaddrOutOfRange__C");
__FAR__ const CT__ti_sysbios_family_c7x_Mmu_A_vaddrOutOfRange ti_sysbios_family_c7x_Mmu_A_vaddrOutOfRange__C = (((xdc_runtime_Assert_Id)479) << 16 | 16);

/* A_paddrOutOfRange__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Mmu_A_paddrOutOfRange__C, ".const:ti_sysbios_family_c7x_Mmu_A_paddrOutOfRange__C");
__FAR__ const CT__ti_sysbios_family_c7x_Mmu_A_paddrOutOfRange ti_sysbios_family_c7x_Mmu_A_paddrOutOfRange__C = (((xdc_runtime_Assert_Id)530) << 16 | 16);

/* A_unalignedVaddr__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Mmu_A_unalignedVaddr__C, ".const:ti_sysbios_family_c7x_Mmu_A_unalignedVaddr__C");
__FAR__ const CT__ti_sysbios_family_c7x_Mmu_A_unalignedVaddr ti_sysbios_family_c7x_Mmu_A_unalignedVaddr__C = (((xdc_runtime_Assert_Id)582) << 16 | 16);

/* A_unalignedPaddr__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Mmu_A_unalignedPaddr__C, ".const:ti_sysbios_family_c7x_Mmu_A_unalignedPaddr__C");
__FAR__ const CT__ti_sysbios_family_c7x_Mmu_A_unalignedPaddr ti_sysbios_family_c7x_Mmu_A_unalignedPaddr__C = (((xdc_runtime_Assert_Id)633) << 16 | 16);

/* A_unalignedSize__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Mmu_A_unalignedSize__C, ".const:ti_sysbios_family_c7x_Mmu_A_unalignedSize__C");
__FAR__ const CT__ti_sysbios_family_c7x_Mmu_A_unalignedSize ti_sysbios_family_c7x_Mmu_A_unalignedSize__C = (((xdc_runtime_Assert_Id)685) << 16 | 16);

/* defaultMapAttrs__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Mmu_defaultMapAttrs__C, ".const:ti_sysbios_family_c7x_Mmu_defaultMapAttrs__C");
__FAR__ const CT__ti_sysbios_family_c7x_Mmu_defaultMapAttrs ti_sysbios_family_c7x_Mmu_defaultMapAttrs__C = {
    1,  /* ns */
    ti_sysbios_family_c7x_Mmu_AccessPerm_PRIV_RW_USER_NONE,  /* accessPerm */
    1,  /* privExecute */
    0,  /* userExecute */
    ti_sysbios_family_c7x_Mmu_Shareable_OUTER,  /* shareable */
    ti_sysbios_family_c7x_Mmu_AttrIndx_MAIR0,  /* attrIndx */
    1,  /* global */
};

/* enableMMU__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Mmu_enableMMU__C, ".const:ti_sysbios_family_c7x_Mmu_enableMMU__C");
__FAR__ const CT__ti_sysbios_family_c7x_Mmu_enableMMU ti_sysbios_family_c7x_Mmu_enableMMU__C = 1;

/* granuleSize__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Mmu_granuleSize__C, ".const:ti_sysbios_family_c7x_Mmu_granuleSize__C");
__FAR__ const CT__ti_sysbios_family_c7x_Mmu_granuleSize ti_sysbios_family_c7x_Mmu_granuleSize__C = ti_sysbios_family_c7x_Mmu_GranuleSize_4KB;

/* MAIR0__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Mmu_MAIR0__C, ".const:ti_sysbios_family_c7x_Mmu_MAIR0__C");
__FAR__ const CT__ti_sysbios_family_c7x_Mmu_MAIR0 ti_sysbios_family_c7x_Mmu_MAIR0__C = (xdc_UInt8)0x0U;

/* MAIR1__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Mmu_MAIR1__C, ".const:ti_sysbios_family_c7x_Mmu_MAIR1__C");
__FAR__ const CT__ti_sysbios_family_c7x_Mmu_MAIR1 ti_sysbios_family_c7x_Mmu_MAIR1__C = (xdc_UInt8)0x4U;

/* MAIR2__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Mmu_MAIR2__C, ".const:ti_sysbios_family_c7x_Mmu_MAIR2__C");
__FAR__ const CT__ti_sysbios_family_c7x_Mmu_MAIR2 ti_sysbios_family_c7x_Mmu_MAIR2__C = (xdc_UInt8)0x8U;

/* MAIR3__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Mmu_MAIR3__C, ".const:ti_sysbios_family_c7x_Mmu_MAIR3__C");
__FAR__ const CT__ti_sysbios_family_c7x_Mmu_MAIR3 ti_sysbios_family_c7x_Mmu_MAIR3__C = (xdc_UInt8)0xcU;

/* MAIR4__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Mmu_MAIR4__C, ".const:ti_sysbios_family_c7x_Mmu_MAIR4__C");
__FAR__ const CT__ti_sysbios_family_c7x_Mmu_MAIR4 ti_sysbios_family_c7x_Mmu_MAIR4__C = (xdc_UInt8)0x44U;

/* MAIR5__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Mmu_MAIR5__C, ".const:ti_sysbios_family_c7x_Mmu_MAIR5__C");
__FAR__ const CT__ti_sysbios_family_c7x_Mmu_MAIR5 ti_sysbios_family_c7x_Mmu_MAIR5__C = (xdc_UInt8)0x4fU;

/* MAIR6__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Mmu_MAIR6__C, ".const:ti_sysbios_family_c7x_Mmu_MAIR6__C");
__FAR__ const CT__ti_sysbios_family_c7x_Mmu_MAIR6 ti_sysbios_family_c7x_Mmu_MAIR6__C = (xdc_UInt8)0xbbU;

/* MAIR7__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Mmu_MAIR7__C, ".const:ti_sysbios_family_c7x_Mmu_MAIR7__C");
__FAR__ const CT__ti_sysbios_family_c7x_Mmu_MAIR7 ti_sysbios_family_c7x_Mmu_MAIR7__C = (xdc_UInt8)0x7dU;

/* initFunc__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Mmu_initFunc__C, ".const:ti_sysbios_family_c7x_Mmu_initFunc__C");
__FAR__ const CT__ti_sysbios_family_c7x_Mmu_initFunc ti_sysbios_family_c7x_Mmu_initFunc__C = ((const CT__ti_sysbios_family_c7x_Mmu_initFunc)(tidlMmuInit));

/* tableMemory__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Mmu_tableMemory__C, ".const:ti_sysbios_family_c7x_Mmu_tableMemory__C");
__FAR__ const CT__ti_sysbios_family_c7x_Mmu_tableMemory ti_sysbios_family_c7x_Mmu_tableMemory__C = "";

/* tableMemory_NS__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Mmu_tableMemory_NS__C, ".const:ti_sysbios_family_c7x_Mmu_tableMemory_NS__C");
__FAR__ const CT__ti_sysbios_family_c7x_Mmu_tableMemory_NS ti_sysbios_family_c7x_Mmu_tableMemory_NS__C = "MSMC";

/* tableArrayLen__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Mmu_tableArrayLen__C, ".const:ti_sysbios_family_c7x_Mmu_tableArrayLen__C");
__FAR__ const CT__ti_sysbios_family_c7x_Mmu_tableArrayLen ti_sysbios_family_c7x_Mmu_tableArrayLen__C = (xdc_UInt)0x10U;

/* configInfo__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_Mmu_configInfo__C, ".const:ti_sysbios_family_c7x_Mmu_configInfo__C");
__FAR__ const CT__ti_sysbios_family_c7x_Mmu_configInfo ti_sysbios_family_c7x_Mmu_configInfo__C = {
    (xdc_UInt64)0x1ffU,  /* indexMask */
    (xdc_UInt32)0x200U,  /* tableLength */
    {
        (xdc_UInt8)0x27U,  /* [0] */
        (xdc_UInt8)0x1eU,  /* [1] */
        (xdc_UInt8)0x15U,  /* [2] */
        (xdc_UInt8)0xcU,  /* [3] */
    },  /* tableOffset */
    (xdc_UInt8)0xcU,  /* granuleSizeBits */
    (xdc_UInt8)0x9U,  /* indexBits */
    0,  /* noLevel0Table */
};


/*
 * ======== ti.sysbios.family.c7x.TaskSupport INITIALIZERS ========
 */

/* Module__diagsEnabled__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_TaskSupport_Module__diagsEnabled__C, ".const:ti_sysbios_family_c7x_TaskSupport_Module__diagsEnabled__C");
__FAR__ const CT__ti_sysbios_family_c7x_TaskSupport_Module__diagsEnabled ti_sysbios_family_c7x_TaskSupport_Module__diagsEnabled__C = (xdc_Bits32)0x90U;

/* Module__diagsIncluded__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_TaskSupport_Module__diagsIncluded__C, ".const:ti_sysbios_family_c7x_TaskSupport_Module__diagsIncluded__C");
__FAR__ const CT__ti_sysbios_family_c7x_TaskSupport_Module__diagsIncluded ti_sysbios_family_c7x_TaskSupport_Module__diagsIncluded__C = (xdc_Bits32)0x90U;

/* Module__diagsMask__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_TaskSupport_Module__diagsMask__C, ".const:ti_sysbios_family_c7x_TaskSupport_Module__diagsMask__C");
__FAR__ const CT__ti_sysbios_family_c7x_TaskSupport_Module__diagsMask ti_sysbios_family_c7x_TaskSupport_Module__diagsMask__C = ((const CT__ti_sysbios_family_c7x_TaskSupport_Module__diagsMask)NULL);

/* Module__gateObj__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_TaskSupport_Module__gateObj__C, ".const:ti_sysbios_family_c7x_TaskSupport_Module__gateObj__C");
__FAR__ const CT__ti_sysbios_family_c7x_TaskSupport_Module__gateObj ti_sysbios_family_c7x_TaskSupport_Module__gateObj__C = ((const CT__ti_sysbios_family_c7x_TaskSupport_Module__gateObj)NULL);

/* Module__gatePrms__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_TaskSupport_Module__gatePrms__C, ".const:ti_sysbios_family_c7x_TaskSupport_Module__gatePrms__C");
__FAR__ const CT__ti_sysbios_family_c7x_TaskSupport_Module__gatePrms ti_sysbios_family_c7x_TaskSupport_Module__gatePrms__C = ((const CT__ti_sysbios_family_c7x_TaskSupport_Module__gatePrms)NULL);

/* Module__id__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_TaskSupport_Module__id__C, ".const:ti_sysbios_family_c7x_TaskSupport_Module__id__C");
__FAR__ const CT__ti_sysbios_family_c7x_TaskSupport_Module__id ti_sysbios_family_c7x_TaskSupport_Module__id__C = (xdc_Bits16)0x801cU;

/* Module__loggerDefined__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_TaskSupport_Module__loggerDefined__C, ".const:ti_sysbios_family_c7x_TaskSupport_Module__loggerDefined__C");
__FAR__ const CT__ti_sysbios_family_c7x_TaskSupport_Module__loggerDefined ti_sysbios_family_c7x_TaskSupport_Module__loggerDefined__C = 0;

/* Module__loggerObj__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_TaskSupport_Module__loggerObj__C, ".const:ti_sysbios_family_c7x_TaskSupport_Module__loggerObj__C");
__FAR__ const CT__ti_sysbios_family_c7x_TaskSupport_Module__loggerObj ti_sysbios_family_c7x_TaskSupport_Module__loggerObj__C = ((const CT__ti_sysbios_family_c7x_TaskSupport_Module__loggerObj)NULL);

/* Module__loggerFxn0__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_TaskSupport_Module__loggerFxn0__C, ".const:ti_sysbios_family_c7x_TaskSupport_Module__loggerFxn0__C");
__FAR__ const CT__ti_sysbios_family_c7x_TaskSupport_Module__loggerFxn0 ti_sysbios_family_c7x_TaskSupport_Module__loggerFxn0__C = ((const CT__ti_sysbios_family_c7x_TaskSupport_Module__loggerFxn0)NULL);

/* Module__loggerFxn1__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_TaskSupport_Module__loggerFxn1__C, ".const:ti_sysbios_family_c7x_TaskSupport_Module__loggerFxn1__C");
__FAR__ const CT__ti_sysbios_family_c7x_TaskSupport_Module__loggerFxn1 ti_sysbios_family_c7x_TaskSupport_Module__loggerFxn1__C = ((const CT__ti_sysbios_family_c7x_TaskSupport_Module__loggerFxn1)NULL);

/* Module__loggerFxn2__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_TaskSupport_Module__loggerFxn2__C, ".const:ti_sysbios_family_c7x_TaskSupport_Module__loggerFxn2__C");
__FAR__ const CT__ti_sysbios_family_c7x_TaskSupport_Module__loggerFxn2 ti_sysbios_family_c7x_TaskSupport_Module__loggerFxn2__C = ((const CT__ti_sysbios_family_c7x_TaskSupport_Module__loggerFxn2)NULL);

/* Module__loggerFxn4__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_TaskSupport_Module__loggerFxn4__C, ".const:ti_sysbios_family_c7x_TaskSupport_Module__loggerFxn4__C");
__FAR__ const CT__ti_sysbios_family_c7x_TaskSupport_Module__loggerFxn4 ti_sysbios_family_c7x_TaskSupport_Module__loggerFxn4__C = ((const CT__ti_sysbios_family_c7x_TaskSupport_Module__loggerFxn4)NULL);

/* Module__loggerFxn8__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_TaskSupport_Module__loggerFxn8__C, ".const:ti_sysbios_family_c7x_TaskSupport_Module__loggerFxn8__C");
__FAR__ const CT__ti_sysbios_family_c7x_TaskSupport_Module__loggerFxn8 ti_sysbios_family_c7x_TaskSupport_Module__loggerFxn8__C = ((const CT__ti_sysbios_family_c7x_TaskSupport_Module__loggerFxn8)NULL);

/* Object__count__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_TaskSupport_Object__count__C, ".const:ti_sysbios_family_c7x_TaskSupport_Object__count__C");
__FAR__ const CT__ti_sysbios_family_c7x_TaskSupport_Object__count ti_sysbios_family_c7x_TaskSupport_Object__count__C = 0;

/* Object__heap__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_TaskSupport_Object__heap__C, ".const:ti_sysbios_family_c7x_TaskSupport_Object__heap__C");
__FAR__ const CT__ti_sysbios_family_c7x_TaskSupport_Object__heap ti_sysbios_family_c7x_TaskSupport_Object__heap__C = NULL;

/* Object__sizeof__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_TaskSupport_Object__sizeof__C, ".const:ti_sysbios_family_c7x_TaskSupport_Object__sizeof__C");
__FAR__ const CT__ti_sysbios_family_c7x_TaskSupport_Object__sizeof ti_sysbios_family_c7x_TaskSupport_Object__sizeof__C = 0;

/* Object__table__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_TaskSupport_Object__table__C, ".const:ti_sysbios_family_c7x_TaskSupport_Object__table__C");
__FAR__ const CT__ti_sysbios_family_c7x_TaskSupport_Object__table ti_sysbios_family_c7x_TaskSupport_Object__table__C = NULL;

/* defaultStackSize__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_TaskSupport_defaultStackSize__C, ".const:ti_sysbios_family_c7x_TaskSupport_defaultStackSize__C");
__FAR__ const CT__ti_sysbios_family_c7x_TaskSupport_defaultStackSize ti_sysbios_family_c7x_TaskSupport_defaultStackSize__C = (xdc_SizeT)0x4000;

/* stackAlignment__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_TaskSupport_stackAlignment__C, ".const:ti_sysbios_family_c7x_TaskSupport_stackAlignment__C");
__FAR__ const CT__ti_sysbios_family_c7x_TaskSupport_stackAlignment ti_sysbios_family_c7x_TaskSupport_stackAlignment__C = (xdc_UInt)0x2000U;

/* A_stackSizeTooSmall__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_TaskSupport_A_stackSizeTooSmall__C, ".const:ti_sysbios_family_c7x_TaskSupport_A_stackSizeTooSmall__C");
__FAR__ const CT__ti_sysbios_family_c7x_TaskSupport_A_stackSizeTooSmall ti_sysbios_family_c7x_TaskSupport_A_stackSizeTooSmall__C = (((xdc_runtime_Assert_Id)736) << 16 | 16);


/*
 * ======== ti.sysbios.family.c7x.TimestampProvider INITIALIZERS ========
 */

/* Module__diagsEnabled__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_TimestampProvider_Module__diagsEnabled__C, ".const:ti_sysbios_family_c7x_TimestampProvider_Module__diagsEnabled__C");
__FAR__ const CT__ti_sysbios_family_c7x_TimestampProvider_Module__diagsEnabled ti_sysbios_family_c7x_TimestampProvider_Module__diagsEnabled__C = (xdc_Bits32)0x10U;

/* Module__diagsIncluded__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_TimestampProvider_Module__diagsIncluded__C, ".const:ti_sysbios_family_c7x_TimestampProvider_Module__diagsIncluded__C");
__FAR__ const CT__ti_sysbios_family_c7x_TimestampProvider_Module__diagsIncluded ti_sysbios_family_c7x_TimestampProvider_Module__diagsIncluded__C = (xdc_Bits32)0x10U;

/* Module__diagsMask__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_TimestampProvider_Module__diagsMask__C, ".const:ti_sysbios_family_c7x_TimestampProvider_Module__diagsMask__C");
__FAR__ const CT__ti_sysbios_family_c7x_TimestampProvider_Module__diagsMask ti_sysbios_family_c7x_TimestampProvider_Module__diagsMask__C = ((const CT__ti_sysbios_family_c7x_TimestampProvider_Module__diagsMask)NULL);

/* Module__gateObj__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_TimestampProvider_Module__gateObj__C, ".const:ti_sysbios_family_c7x_TimestampProvider_Module__gateObj__C");
__FAR__ const CT__ti_sysbios_family_c7x_TimestampProvider_Module__gateObj ti_sysbios_family_c7x_TimestampProvider_Module__gateObj__C = ((const CT__ti_sysbios_family_c7x_TimestampProvider_Module__gateObj)NULL);

/* Module__gatePrms__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_TimestampProvider_Module__gatePrms__C, ".const:ti_sysbios_family_c7x_TimestampProvider_Module__gatePrms__C");
__FAR__ const CT__ti_sysbios_family_c7x_TimestampProvider_Module__gatePrms ti_sysbios_family_c7x_TimestampProvider_Module__gatePrms__C = ((const CT__ti_sysbios_family_c7x_TimestampProvider_Module__gatePrms)NULL);

/* Module__id__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_TimestampProvider_Module__id__C, ".const:ti_sysbios_family_c7x_TimestampProvider_Module__id__C");
__FAR__ const CT__ti_sysbios_family_c7x_TimestampProvider_Module__id ti_sysbios_family_c7x_TimestampProvider_Module__id__C = (xdc_Bits16)0x801dU;

/* Module__loggerDefined__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_TimestampProvider_Module__loggerDefined__C, ".const:ti_sysbios_family_c7x_TimestampProvider_Module__loggerDefined__C");
__FAR__ const CT__ti_sysbios_family_c7x_TimestampProvider_Module__loggerDefined ti_sysbios_family_c7x_TimestampProvider_Module__loggerDefined__C = 0;

/* Module__loggerObj__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_TimestampProvider_Module__loggerObj__C, ".const:ti_sysbios_family_c7x_TimestampProvider_Module__loggerObj__C");
__FAR__ const CT__ti_sysbios_family_c7x_TimestampProvider_Module__loggerObj ti_sysbios_family_c7x_TimestampProvider_Module__loggerObj__C = ((const CT__ti_sysbios_family_c7x_TimestampProvider_Module__loggerObj)NULL);

/* Module__loggerFxn0__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_TimestampProvider_Module__loggerFxn0__C, ".const:ti_sysbios_family_c7x_TimestampProvider_Module__loggerFxn0__C");
__FAR__ const CT__ti_sysbios_family_c7x_TimestampProvider_Module__loggerFxn0 ti_sysbios_family_c7x_TimestampProvider_Module__loggerFxn0__C = ((const CT__ti_sysbios_family_c7x_TimestampProvider_Module__loggerFxn0)NULL);

/* Module__loggerFxn1__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_TimestampProvider_Module__loggerFxn1__C, ".const:ti_sysbios_family_c7x_TimestampProvider_Module__loggerFxn1__C");
__FAR__ const CT__ti_sysbios_family_c7x_TimestampProvider_Module__loggerFxn1 ti_sysbios_family_c7x_TimestampProvider_Module__loggerFxn1__C = ((const CT__ti_sysbios_family_c7x_TimestampProvider_Module__loggerFxn1)NULL);

/* Module__loggerFxn2__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_TimestampProvider_Module__loggerFxn2__C, ".const:ti_sysbios_family_c7x_TimestampProvider_Module__loggerFxn2__C");
__FAR__ const CT__ti_sysbios_family_c7x_TimestampProvider_Module__loggerFxn2 ti_sysbios_family_c7x_TimestampProvider_Module__loggerFxn2__C = ((const CT__ti_sysbios_family_c7x_TimestampProvider_Module__loggerFxn2)NULL);

/* Module__loggerFxn4__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_TimestampProvider_Module__loggerFxn4__C, ".const:ti_sysbios_family_c7x_TimestampProvider_Module__loggerFxn4__C");
__FAR__ const CT__ti_sysbios_family_c7x_TimestampProvider_Module__loggerFxn4 ti_sysbios_family_c7x_TimestampProvider_Module__loggerFxn4__C = ((const CT__ti_sysbios_family_c7x_TimestampProvider_Module__loggerFxn4)NULL);

/* Module__loggerFxn8__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_TimestampProvider_Module__loggerFxn8__C, ".const:ti_sysbios_family_c7x_TimestampProvider_Module__loggerFxn8__C");
__FAR__ const CT__ti_sysbios_family_c7x_TimestampProvider_Module__loggerFxn8 ti_sysbios_family_c7x_TimestampProvider_Module__loggerFxn8__C = ((const CT__ti_sysbios_family_c7x_TimestampProvider_Module__loggerFxn8)NULL);

/* Object__count__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_TimestampProvider_Object__count__C, ".const:ti_sysbios_family_c7x_TimestampProvider_Object__count__C");
__FAR__ const CT__ti_sysbios_family_c7x_TimestampProvider_Object__count ti_sysbios_family_c7x_TimestampProvider_Object__count__C = 0;

/* Object__heap__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_TimestampProvider_Object__heap__C, ".const:ti_sysbios_family_c7x_TimestampProvider_Object__heap__C");
__FAR__ const CT__ti_sysbios_family_c7x_TimestampProvider_Object__heap ti_sysbios_family_c7x_TimestampProvider_Object__heap__C = NULL;

/* Object__sizeof__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_TimestampProvider_Object__sizeof__C, ".const:ti_sysbios_family_c7x_TimestampProvider_Object__sizeof__C");
__FAR__ const CT__ti_sysbios_family_c7x_TimestampProvider_Object__sizeof ti_sysbios_family_c7x_TimestampProvider_Object__sizeof__C = 0;

/* Object__table__C */
#pragma DATA_SECTION(ti_sysbios_family_c7x_TimestampProvider_Object__table__C, ".const:ti_sysbios_family_c7x_TimestampProvider_Object__table__C");
__FAR__ const CT__ti_sysbios_family_c7x_TimestampProvider_Object__table ti_sysbios_family_c7x_TimestampProvider_Object__table__C = NULL;


/*
 * ======== ti.sysbios.gates.GateHwi INITIALIZERS ========
 */

/* Object__DESC__C */
__FAR__ const xdc_runtime_Core_ObjDesc ti_sysbios_gates_GateHwi_Object__DESC__C;

/* Object__PARAMS__C */
#pragma DATA_SECTION(ti_sysbios_gates_GateHwi_Object__PARAMS__C, ".const:ti_sysbios_gates_GateHwi_Object__PARAMS__C");
__FAR__ const ti_sysbios_gates_GateHwi_Params ti_sysbios_gates_GateHwi_Object__PARAMS__C = {
    sizeof (ti_sysbios_gates_GateHwi_Params), /* __size */
    NULL, /* __self */
    NULL, /* __fxns */
    (xdc_runtime_IInstance_Params*)&ti_sysbios_gates_GateHwi_Object__PARAMS__C.__iprms, /* instance */
    {
        sizeof (xdc_runtime_IInstance_Params), /* __size */
        0,  /* name */
    },  /* instance */
};

/* Module__root__V */
ti_sysbios_gates_GateHwi_Module__ ti_sysbios_gates_GateHwi_Module__root__V = {
    {&ti_sysbios_gates_GateHwi_Module__root__V.link,  /* link.next */
    &ti_sysbios_gates_GateHwi_Module__root__V.link},  /* link.prev */
};

/* Object__table__V */
ti_sysbios_gates_GateHwi_Object__ ti_sysbios_gates_GateHwi_Object__table__V[1] = {
    {/* instance#0 */
        &ti_sysbios_gates_GateHwi_Module__FXNS__C,
    },
};

/* Module__diagsEnabled__C */
#pragma DATA_SECTION(ti_sysbios_gates_GateHwi_Module__diagsEnabled__C, ".const:ti_sysbios_gates_GateHwi_Module__diagsEnabled__C");
__FAR__ const CT__ti_sysbios_gates_GateHwi_Module__diagsEnabled ti_sysbios_gates_GateHwi_Module__diagsEnabled__C = (xdc_Bits32)0x90U;

/* Module__diagsIncluded__C */
#pragma DATA_SECTION(ti_sysbios_gates_GateHwi_Module__diagsIncluded__C, ".const:ti_sysbios_gates_GateHwi_Module__diagsIncluded__C");
__FAR__ const CT__ti_sysbios_gates_GateHwi_Module__diagsIncluded ti_sysbios_gates_GateHwi_Module__diagsIncluded__C = (xdc_Bits32)0x90U;

/* Module__diagsMask__C */
#pragma DATA_SECTION(ti_sysbios_gates_GateHwi_Module__diagsMask__C, ".const:ti_sysbios_gates_GateHwi_Module__diagsMask__C");
__FAR__ const CT__ti_sysbios_gates_GateHwi_Module__diagsMask ti_sysbios_gates_GateHwi_Module__diagsMask__C = ((const CT__ti_sysbios_gates_GateHwi_Module__diagsMask)NULL);

/* Module__gateObj__C */
#pragma DATA_SECTION(ti_sysbios_gates_GateHwi_Module__gateObj__C, ".const:ti_sysbios_gates_GateHwi_Module__gateObj__C");
__FAR__ const CT__ti_sysbios_gates_GateHwi_Module__gateObj ti_sysbios_gates_GateHwi_Module__gateObj__C = ((const CT__ti_sysbios_gates_GateHwi_Module__gateObj)NULL);

/* Module__gatePrms__C */
#pragma DATA_SECTION(ti_sysbios_gates_GateHwi_Module__gatePrms__C, ".const:ti_sysbios_gates_GateHwi_Module__gatePrms__C");
__FAR__ const CT__ti_sysbios_gates_GateHwi_Module__gatePrms ti_sysbios_gates_GateHwi_Module__gatePrms__C = ((const CT__ti_sysbios_gates_GateHwi_Module__gatePrms)NULL);

/* Module__id__C */
#pragma DATA_SECTION(ti_sysbios_gates_GateHwi_Module__id__C, ".const:ti_sysbios_gates_GateHwi_Module__id__C");
__FAR__ const CT__ti_sysbios_gates_GateHwi_Module__id ti_sysbios_gates_GateHwi_Module__id__C = (xdc_Bits16)0x8036U;

/* Module__loggerDefined__C */
#pragma DATA_SECTION(ti_sysbios_gates_GateHwi_Module__loggerDefined__C, ".const:ti_sysbios_gates_GateHwi_Module__loggerDefined__C");
__FAR__ const CT__ti_sysbios_gates_GateHwi_Module__loggerDefined ti_sysbios_gates_GateHwi_Module__loggerDefined__C = 0;

/* Module__loggerObj__C */
#pragma DATA_SECTION(ti_sysbios_gates_GateHwi_Module__loggerObj__C, ".const:ti_sysbios_gates_GateHwi_Module__loggerObj__C");
__FAR__ const CT__ti_sysbios_gates_GateHwi_Module__loggerObj ti_sysbios_gates_GateHwi_Module__loggerObj__C = ((const CT__ti_sysbios_gates_GateHwi_Module__loggerObj)NULL);

/* Module__loggerFxn0__C */
#pragma DATA_SECTION(ti_sysbios_gates_GateHwi_Module__loggerFxn0__C, ".const:ti_sysbios_gates_GateHwi_Module__loggerFxn0__C");
__FAR__ const CT__ti_sysbios_gates_GateHwi_Module__loggerFxn0 ti_sysbios_gates_GateHwi_Module__loggerFxn0__C = ((const CT__ti_sysbios_gates_GateHwi_Module__loggerFxn0)NULL);

/* Module__loggerFxn1__C */
#pragma DATA_SECTION(ti_sysbios_gates_GateHwi_Module__loggerFxn1__C, ".const:ti_sysbios_gates_GateHwi_Module__loggerFxn1__C");
__FAR__ const CT__ti_sysbios_gates_GateHwi_Module__loggerFxn1 ti_sysbios_gates_GateHwi_Module__loggerFxn1__C = ((const CT__ti_sysbios_gates_GateHwi_Module__loggerFxn1)NULL);

/* Module__loggerFxn2__C */
#pragma DATA_SECTION(ti_sysbios_gates_GateHwi_Module__loggerFxn2__C, ".const:ti_sysbios_gates_GateHwi_Module__loggerFxn2__C");
__FAR__ const CT__ti_sysbios_gates_GateHwi_Module__loggerFxn2 ti_sysbios_gates_GateHwi_Module__loggerFxn2__C = ((const CT__ti_sysbios_gates_GateHwi_Module__loggerFxn2)NULL);

/* Module__loggerFxn4__C */
#pragma DATA_SECTION(ti_sysbios_gates_GateHwi_Module__loggerFxn4__C, ".const:ti_sysbios_gates_GateHwi_Module__loggerFxn4__C");
__FAR__ const CT__ti_sysbios_gates_GateHwi_Module__loggerFxn4 ti_sysbios_gates_GateHwi_Module__loggerFxn4__C = ((const CT__ti_sysbios_gates_GateHwi_Module__loggerFxn4)NULL);

/* Module__loggerFxn8__C */
#pragma DATA_SECTION(ti_sysbios_gates_GateHwi_Module__loggerFxn8__C, ".const:ti_sysbios_gates_GateHwi_Module__loggerFxn8__C");
__FAR__ const CT__ti_sysbios_gates_GateHwi_Module__loggerFxn8 ti_sysbios_gates_GateHwi_Module__loggerFxn8__C = ((const CT__ti_sysbios_gates_GateHwi_Module__loggerFxn8)NULL);

/* Object__count__C */
#pragma DATA_SECTION(ti_sysbios_gates_GateHwi_Object__count__C, ".const:ti_sysbios_gates_GateHwi_Object__count__C");
__FAR__ const CT__ti_sysbios_gates_GateHwi_Object__count ti_sysbios_gates_GateHwi_Object__count__C = 1;

/* Object__heap__C */
#pragma DATA_SECTION(ti_sysbios_gates_GateHwi_Object__heap__C, ".const:ti_sysbios_gates_GateHwi_Object__heap__C");
__FAR__ const CT__ti_sysbios_gates_GateHwi_Object__heap ti_sysbios_gates_GateHwi_Object__heap__C = NULL;

/* Object__sizeof__C */
#pragma DATA_SECTION(ti_sysbios_gates_GateHwi_Object__sizeof__C, ".const:ti_sysbios_gates_GateHwi_Object__sizeof__C");
__FAR__ const CT__ti_sysbios_gates_GateHwi_Object__sizeof ti_sysbios_gates_GateHwi_Object__sizeof__C = sizeof(ti_sysbios_gates_GateHwi_Object__);

/* Object__table__C */
#pragma DATA_SECTION(ti_sysbios_gates_GateHwi_Object__table__C, ".const:ti_sysbios_gates_GateHwi_Object__table__C");
__FAR__ const CT__ti_sysbios_gates_GateHwi_Object__table ti_sysbios_gates_GateHwi_Object__table__C = ti_sysbios_gates_GateHwi_Object__table__V;


/*
 * ======== ti.sysbios.gates.GateMutex INITIALIZERS ========
 */

/* Object__DESC__C */
__FAR__ const xdc_runtime_Core_ObjDesc ti_sysbios_gates_GateMutex_Object__DESC__C;

/* Object__PARAMS__C */
#pragma DATA_SECTION(ti_sysbios_gates_GateMutex_Object__PARAMS__C, ".const:ti_sysbios_gates_GateMutex_Object__PARAMS__C");
__FAR__ const ti_sysbios_gates_GateMutex_Params ti_sysbios_gates_GateMutex_Object__PARAMS__C = {
    sizeof (ti_sysbios_gates_GateMutex_Params), /* __size */
    NULL, /* __self */
    NULL, /* __fxns */
    (xdc_runtime_IInstance_Params*)&ti_sysbios_gates_GateMutex_Object__PARAMS__C.__iprms, /* instance */
    {
        sizeof (xdc_runtime_IInstance_Params), /* __size */
        0,  /* name */
    },  /* instance */
};

/* Module__root__V */
ti_sysbios_gates_GateMutex_Module__ ti_sysbios_gates_GateMutex_Module__root__V = {
    {&ti_sysbios_gates_GateMutex_Module__root__V.link,  /* link.next */
    &ti_sysbios_gates_GateMutex_Module__root__V.link},  /* link.prev */
};

/* Object__table__V */
ti_sysbios_gates_GateMutex_Object__ ti_sysbios_gates_GateMutex_Object__table__V[2] = {
    {/* instance#0 */
        &ti_sysbios_gates_GateMutex_Module__FXNS__C,
        0,  /* owner */
        {
            0,  /* event */
            (xdc_UInt)0x1U,  /* eventId */
            ti_sysbios_knl_Semaphore_Mode_COUNTING,  /* mode */
            (xdc_UInt16)0x1U,  /* count */
            {
                {
                    ((ti_sysbios_knl_Queue_Elem*)((void*)&ti_sysbios_gates_GateMutex_Object__table__V[0].Object_field_sem.Object_field_pendQ.elem)),  /* next */
                    ((ti_sysbios_knl_Queue_Elem*)((void*)&ti_sysbios_gates_GateMutex_Object__table__V[0].Object_field_sem.Object_field_pendQ.elem)),  /* prev */
                },  /* elem */
            },  /* Object_field_pendQ */
        },  /* Object_field_sem */
    },
    {/* instance#1 */
        &ti_sysbios_gates_GateMutex_Module__FXNS__C,
        0,  /* owner */
        {
            0,  /* event */
            (xdc_UInt)0x1U,  /* eventId */
            ti_sysbios_knl_Semaphore_Mode_COUNTING,  /* mode */
            (xdc_UInt16)0x1U,  /* count */
            {
                {
                    ((ti_sysbios_knl_Queue_Elem*)((void*)&ti_sysbios_gates_GateMutex_Object__table__V[1].Object_field_sem.Object_field_pendQ.elem)),  /* next */
                    ((ti_sysbios_knl_Queue_Elem*)((void*)&ti_sysbios_gates_GateMutex_Object__table__V[1].Object_field_sem.Object_field_pendQ.elem)),  /* prev */
                },  /* elem */
            },  /* Object_field_pendQ */
        },  /* Object_field_sem */
    },
};

/* Module__diagsEnabled__C */
#pragma DATA_SECTION(ti_sysbios_gates_GateMutex_Module__diagsEnabled__C, ".const:ti_sysbios_gates_GateMutex_Module__diagsEnabled__C");
__FAR__ const CT__ti_sysbios_gates_GateMutex_Module__diagsEnabled ti_sysbios_gates_GateMutex_Module__diagsEnabled__C = (xdc_Bits32)0x90U;

/* Module__diagsIncluded__C */
#pragma DATA_SECTION(ti_sysbios_gates_GateMutex_Module__diagsIncluded__C, ".const:ti_sysbios_gates_GateMutex_Module__diagsIncluded__C");
__FAR__ const CT__ti_sysbios_gates_GateMutex_Module__diagsIncluded ti_sysbios_gates_GateMutex_Module__diagsIncluded__C = (xdc_Bits32)0x90U;

/* Module__diagsMask__C */
#pragma DATA_SECTION(ti_sysbios_gates_GateMutex_Module__diagsMask__C, ".const:ti_sysbios_gates_GateMutex_Module__diagsMask__C");
__FAR__ const CT__ti_sysbios_gates_GateMutex_Module__diagsMask ti_sysbios_gates_GateMutex_Module__diagsMask__C = ((const CT__ti_sysbios_gates_GateMutex_Module__diagsMask)NULL);

/* Module__gateObj__C */
#pragma DATA_SECTION(ti_sysbios_gates_GateMutex_Module__gateObj__C, ".const:ti_sysbios_gates_GateMutex_Module__gateObj__C");
__FAR__ const CT__ti_sysbios_gates_GateMutex_Module__gateObj ti_sysbios_gates_GateMutex_Module__gateObj__C = ((const CT__ti_sysbios_gates_GateMutex_Module__gateObj)NULL);

/* Module__gatePrms__C */
#pragma DATA_SECTION(ti_sysbios_gates_GateMutex_Module__gatePrms__C, ".const:ti_sysbios_gates_GateMutex_Module__gatePrms__C");
__FAR__ const CT__ti_sysbios_gates_GateMutex_Module__gatePrms ti_sysbios_gates_GateMutex_Module__gatePrms__C = ((const CT__ti_sysbios_gates_GateMutex_Module__gatePrms)NULL);

/* Module__id__C */
#pragma DATA_SECTION(ti_sysbios_gates_GateMutex_Module__id__C, ".const:ti_sysbios_gates_GateMutex_Module__id__C");
__FAR__ const CT__ti_sysbios_gates_GateMutex_Module__id ti_sysbios_gates_GateMutex_Module__id__C = (xdc_Bits16)0x803aU;

/* Module__loggerDefined__C */
#pragma DATA_SECTION(ti_sysbios_gates_GateMutex_Module__loggerDefined__C, ".const:ti_sysbios_gates_GateMutex_Module__loggerDefined__C");
__FAR__ const CT__ti_sysbios_gates_GateMutex_Module__loggerDefined ti_sysbios_gates_GateMutex_Module__loggerDefined__C = 0;

/* Module__loggerObj__C */
#pragma DATA_SECTION(ti_sysbios_gates_GateMutex_Module__loggerObj__C, ".const:ti_sysbios_gates_GateMutex_Module__loggerObj__C");
__FAR__ const CT__ti_sysbios_gates_GateMutex_Module__loggerObj ti_sysbios_gates_GateMutex_Module__loggerObj__C = ((const CT__ti_sysbios_gates_GateMutex_Module__loggerObj)NULL);

/* Module__loggerFxn0__C */
#pragma DATA_SECTION(ti_sysbios_gates_GateMutex_Module__loggerFxn0__C, ".const:ti_sysbios_gates_GateMutex_Module__loggerFxn0__C");
__FAR__ const CT__ti_sysbios_gates_GateMutex_Module__loggerFxn0 ti_sysbios_gates_GateMutex_Module__loggerFxn0__C = ((const CT__ti_sysbios_gates_GateMutex_Module__loggerFxn0)NULL);

/* Module__loggerFxn1__C */
#pragma DATA_SECTION(ti_sysbios_gates_GateMutex_Module__loggerFxn1__C, ".const:ti_sysbios_gates_GateMutex_Module__loggerFxn1__C");
__FAR__ const CT__ti_sysbios_gates_GateMutex_Module__loggerFxn1 ti_sysbios_gates_GateMutex_Module__loggerFxn1__C = ((const CT__ti_sysbios_gates_GateMutex_Module__loggerFxn1)NULL);

/* Module__loggerFxn2__C */
#pragma DATA_SECTION(ti_sysbios_gates_GateMutex_Module__loggerFxn2__C, ".const:ti_sysbios_gates_GateMutex_Module__loggerFxn2__C");
__FAR__ const CT__ti_sysbios_gates_GateMutex_Module__loggerFxn2 ti_sysbios_gates_GateMutex_Module__loggerFxn2__C = ((const CT__ti_sysbios_gates_GateMutex_Module__loggerFxn2)NULL);

/* Module__loggerFxn4__C */
#pragma DATA_SECTION(ti_sysbios_gates_GateMutex_Module__loggerFxn4__C, ".const:ti_sysbios_gates_GateMutex_Module__loggerFxn4__C");
__FAR__ const CT__ti_sysbios_gates_GateMutex_Module__loggerFxn4 ti_sysbios_gates_GateMutex_Module__loggerFxn4__C = ((const CT__ti_sysbios_gates_GateMutex_Module__loggerFxn4)NULL);

/* Module__loggerFxn8__C */
#pragma DATA_SECTION(ti_sysbios_gates_GateMutex_Module__loggerFxn8__C, ".const:ti_sysbios_gates_GateMutex_Module__loggerFxn8__C");
__FAR__ const CT__ti_sysbios_gates_GateMutex_Module__loggerFxn8 ti_sysbios_gates_GateMutex_Module__loggerFxn8__C = ((const CT__ti_sysbios_gates_GateMutex_Module__loggerFxn8)NULL);

/* Object__count__C */
#pragma DATA_SECTION(ti_sysbios_gates_GateMutex_Object__count__C, ".const:ti_sysbios_gates_GateMutex_Object__count__C");
__FAR__ const CT__ti_sysbios_gates_GateMutex_Object__count ti_sysbios_gates_GateMutex_Object__count__C = 2;

/* Object__heap__C */
#pragma DATA_SECTION(ti_sysbios_gates_GateMutex_Object__heap__C, ".const:ti_sysbios_gates_GateMutex_Object__heap__C");
__FAR__ const CT__ti_sysbios_gates_GateMutex_Object__heap ti_sysbios_gates_GateMutex_Object__heap__C = NULL;

/* Object__sizeof__C */
#pragma DATA_SECTION(ti_sysbios_gates_GateMutex_Object__sizeof__C, ".const:ti_sysbios_gates_GateMutex_Object__sizeof__C");
__FAR__ const CT__ti_sysbios_gates_GateMutex_Object__sizeof ti_sysbios_gates_GateMutex_Object__sizeof__C = sizeof(ti_sysbios_gates_GateMutex_Object__);

/* Object__table__C */
#pragma DATA_SECTION(ti_sysbios_gates_GateMutex_Object__table__C, ".const:ti_sysbios_gates_GateMutex_Object__table__C");
__FAR__ const CT__ti_sysbios_gates_GateMutex_Object__table ti_sysbios_gates_GateMutex_Object__table__C = ti_sysbios_gates_GateMutex_Object__table__V;

/* A_badContext__C */
#pragma DATA_SECTION(ti_sysbios_gates_GateMutex_A_badContext__C, ".const:ti_sysbios_gates_GateMutex_A_badContext__C");
__FAR__ const CT__ti_sysbios_gates_GateMutex_A_badContext ti_sysbios_gates_GateMutex_A_badContext__C = (((xdc_runtime_Assert_Id)3552) << 16 | 16);


/*
 * ======== ti.sysbios.gates.GateMutexPri INITIALIZERS ========
 */

/* Object__DESC__C */
__FAR__ const xdc_runtime_Core_ObjDesc ti_sysbios_gates_GateMutexPri_Object__DESC__C;

/* Object__PARAMS__C */
#pragma DATA_SECTION(ti_sysbios_gates_GateMutexPri_Object__PARAMS__C, ".const:ti_sysbios_gates_GateMutexPri_Object__PARAMS__C");
__FAR__ const ti_sysbios_gates_GateMutexPri_Params ti_sysbios_gates_GateMutexPri_Object__PARAMS__C = {
    sizeof (ti_sysbios_gates_GateMutexPri_Params), /* __size */
    NULL, /* __self */
    NULL, /* __fxns */
    (xdc_runtime_IInstance_Params*)&ti_sysbios_gates_GateMutexPri_Object__PARAMS__C.__iprms, /* instance */
    {
        sizeof (xdc_runtime_IInstance_Params), /* __size */
        0,  /* name */
    },  /* instance */
};

/* Module__root__V */
ti_sysbios_gates_GateMutexPri_Module__ ti_sysbios_gates_GateMutexPri_Module__root__V = {
    {&ti_sysbios_gates_GateMutexPri_Module__root__V.link,  /* link.next */
    &ti_sysbios_gates_GateMutexPri_Module__root__V.link},  /* link.prev */
};

/* Module__diagsEnabled__C */
#pragma DATA_SECTION(ti_sysbios_gates_GateMutexPri_Module__diagsEnabled__C, ".const:ti_sysbios_gates_GateMutexPri_Module__diagsEnabled__C");
__FAR__ const CT__ti_sysbios_gates_GateMutexPri_Module__diagsEnabled ti_sysbios_gates_GateMutexPri_Module__diagsEnabled__C = (xdc_Bits32)0x90U;

/* Module__diagsIncluded__C */
#pragma DATA_SECTION(ti_sysbios_gates_GateMutexPri_Module__diagsIncluded__C, ".const:ti_sysbios_gates_GateMutexPri_Module__diagsIncluded__C");
__FAR__ const CT__ti_sysbios_gates_GateMutexPri_Module__diagsIncluded ti_sysbios_gates_GateMutexPri_Module__diagsIncluded__C = (xdc_Bits32)0x90U;

/* Module__diagsMask__C */
#pragma DATA_SECTION(ti_sysbios_gates_GateMutexPri_Module__diagsMask__C, ".const:ti_sysbios_gates_GateMutexPri_Module__diagsMask__C");
__FAR__ const CT__ti_sysbios_gates_GateMutexPri_Module__diagsMask ti_sysbios_gates_GateMutexPri_Module__diagsMask__C = ((const CT__ti_sysbios_gates_GateMutexPri_Module__diagsMask)NULL);

/* Module__gateObj__C */
#pragma DATA_SECTION(ti_sysbios_gates_GateMutexPri_Module__gateObj__C, ".const:ti_sysbios_gates_GateMutexPri_Module__gateObj__C");
__FAR__ const CT__ti_sysbios_gates_GateMutexPri_Module__gateObj ti_sysbios_gates_GateMutexPri_Module__gateObj__C = ((const CT__ti_sysbios_gates_GateMutexPri_Module__gateObj)NULL);

/* Module__gatePrms__C */
#pragma DATA_SECTION(ti_sysbios_gates_GateMutexPri_Module__gatePrms__C, ".const:ti_sysbios_gates_GateMutexPri_Module__gatePrms__C");
__FAR__ const CT__ti_sysbios_gates_GateMutexPri_Module__gatePrms ti_sysbios_gates_GateMutexPri_Module__gatePrms__C = ((const CT__ti_sysbios_gates_GateMutexPri_Module__gatePrms)NULL);

/* Module__id__C */
#pragma DATA_SECTION(ti_sysbios_gates_GateMutexPri_Module__id__C, ".const:ti_sysbios_gates_GateMutexPri_Module__id__C");
__FAR__ const CT__ti_sysbios_gates_GateMutexPri_Module__id ti_sysbios_gates_GateMutexPri_Module__id__C = (xdc_Bits16)0x8039U;

/* Module__loggerDefined__C */
#pragma DATA_SECTION(ti_sysbios_gates_GateMutexPri_Module__loggerDefined__C, ".const:ti_sysbios_gates_GateMutexPri_Module__loggerDefined__C");
__FAR__ const CT__ti_sysbios_gates_GateMutexPri_Module__loggerDefined ti_sysbios_gates_GateMutexPri_Module__loggerDefined__C = 0;

/* Module__loggerObj__C */
#pragma DATA_SECTION(ti_sysbios_gates_GateMutexPri_Module__loggerObj__C, ".const:ti_sysbios_gates_GateMutexPri_Module__loggerObj__C");
__FAR__ const CT__ti_sysbios_gates_GateMutexPri_Module__loggerObj ti_sysbios_gates_GateMutexPri_Module__loggerObj__C = ((const CT__ti_sysbios_gates_GateMutexPri_Module__loggerObj)NULL);

/* Module__loggerFxn0__C */
#pragma DATA_SECTION(ti_sysbios_gates_GateMutexPri_Module__loggerFxn0__C, ".const:ti_sysbios_gates_GateMutexPri_Module__loggerFxn0__C");
__FAR__ const CT__ti_sysbios_gates_GateMutexPri_Module__loggerFxn0 ti_sysbios_gates_GateMutexPri_Module__loggerFxn0__C = ((const CT__ti_sysbios_gates_GateMutexPri_Module__loggerFxn0)NULL);

/* Module__loggerFxn1__C */
#pragma DATA_SECTION(ti_sysbios_gates_GateMutexPri_Module__loggerFxn1__C, ".const:ti_sysbios_gates_GateMutexPri_Module__loggerFxn1__C");
__FAR__ const CT__ti_sysbios_gates_GateMutexPri_Module__loggerFxn1 ti_sysbios_gates_GateMutexPri_Module__loggerFxn1__C = ((const CT__ti_sysbios_gates_GateMutexPri_Module__loggerFxn1)NULL);

/* Module__loggerFxn2__C */
#pragma DATA_SECTION(ti_sysbios_gates_GateMutexPri_Module__loggerFxn2__C, ".const:ti_sysbios_gates_GateMutexPri_Module__loggerFxn2__C");
__FAR__ const CT__ti_sysbios_gates_GateMutexPri_Module__loggerFxn2 ti_sysbios_gates_GateMutexPri_Module__loggerFxn2__C = ((const CT__ti_sysbios_gates_GateMutexPri_Module__loggerFxn2)NULL);

/* Module__loggerFxn4__C */
#pragma DATA_SECTION(ti_sysbios_gates_GateMutexPri_Module__loggerFxn4__C, ".const:ti_sysbios_gates_GateMutexPri_Module__loggerFxn4__C");
__FAR__ const CT__ti_sysbios_gates_GateMutexPri_Module__loggerFxn4 ti_sysbios_gates_GateMutexPri_Module__loggerFxn4__C = ((const CT__ti_sysbios_gates_GateMutexPri_Module__loggerFxn4)NULL);

/* Module__loggerFxn8__C */
#pragma DATA_SECTION(ti_sysbios_gates_GateMutexPri_Module__loggerFxn8__C, ".const:ti_sysbios_gates_GateMutexPri_Module__loggerFxn8__C");
__FAR__ const CT__ti_sysbios_gates_GateMutexPri_Module__loggerFxn8 ti_sysbios_gates_GateMutexPri_Module__loggerFxn8__C = ((const CT__ti_sysbios_gates_GateMutexPri_Module__loggerFxn8)NULL);

/* Object__count__C */
#pragma DATA_SECTION(ti_sysbios_gates_GateMutexPri_Object__count__C, ".const:ti_sysbios_gates_GateMutexPri_Object__count__C");
__FAR__ const CT__ti_sysbios_gates_GateMutexPri_Object__count ti_sysbios_gates_GateMutexPri_Object__count__C = 0;

/* Object__heap__C */
#pragma DATA_SECTION(ti_sysbios_gates_GateMutexPri_Object__heap__C, ".const:ti_sysbios_gates_GateMutexPri_Object__heap__C");
__FAR__ const CT__ti_sysbios_gates_GateMutexPri_Object__heap ti_sysbios_gates_GateMutexPri_Object__heap__C = NULL;

/* Object__sizeof__C */
#pragma DATA_SECTION(ti_sysbios_gates_GateMutexPri_Object__sizeof__C, ".const:ti_sysbios_gates_GateMutexPri_Object__sizeof__C");
__FAR__ const CT__ti_sysbios_gates_GateMutexPri_Object__sizeof ti_sysbios_gates_GateMutexPri_Object__sizeof__C = sizeof(ti_sysbios_gates_GateMutexPri_Object__);

/* Object__table__C */
#pragma DATA_SECTION(ti_sysbios_gates_GateMutexPri_Object__table__C, ".const:ti_sysbios_gates_GateMutexPri_Object__table__C");
__FAR__ const CT__ti_sysbios_gates_GateMutexPri_Object__table ti_sysbios_gates_GateMutexPri_Object__table__C = NULL;

/* A_badContext__C */
#pragma DATA_SECTION(ti_sysbios_gates_GateMutexPri_A_badContext__C, ".const:ti_sysbios_gates_GateMutexPri_A_badContext__C");
__FAR__ const CT__ti_sysbios_gates_GateMutexPri_A_badContext ti_sysbios_gates_GateMutexPri_A_badContext__C = (((xdc_runtime_Assert_Id)3380) << 16 | 16);

/* A_enterTaskDisabled__C */
#pragma DATA_SECTION(ti_sysbios_gates_GateMutexPri_A_enterTaskDisabled__C, ".const:ti_sysbios_gates_GateMutexPri_A_enterTaskDisabled__C");
__FAR__ const CT__ti_sysbios_gates_GateMutexPri_A_enterTaskDisabled ti_sysbios_gates_GateMutexPri_A_enterTaskDisabled__C = (((xdc_runtime_Assert_Id)3453) << 16 | 16);


/*
 * ======== ti.sysbios.gates.GateSwi INITIALIZERS ========
 */

/* Object__DESC__C */
__FAR__ const xdc_runtime_Core_ObjDesc ti_sysbios_gates_GateSwi_Object__DESC__C;

/* Object__PARAMS__C */
#pragma DATA_SECTION(ti_sysbios_gates_GateSwi_Object__PARAMS__C, ".const:ti_sysbios_gates_GateSwi_Object__PARAMS__C");
__FAR__ const ti_sysbios_gates_GateSwi_Params ti_sysbios_gates_GateSwi_Object__PARAMS__C = {
    sizeof (ti_sysbios_gates_GateSwi_Params), /* __size */
    NULL, /* __self */
    NULL, /* __fxns */
    (xdc_runtime_IInstance_Params*)&ti_sysbios_gates_GateSwi_Object__PARAMS__C.__iprms, /* instance */
    {
        sizeof (xdc_runtime_IInstance_Params), /* __size */
        0,  /* name */
    },  /* instance */
};

/* Module__root__V */
ti_sysbios_gates_GateSwi_Module__ ti_sysbios_gates_GateSwi_Module__root__V = {
    {&ti_sysbios_gates_GateSwi_Module__root__V.link,  /* link.next */
    &ti_sysbios_gates_GateSwi_Module__root__V.link},  /* link.prev */
};

/* Module__diagsEnabled__C */
#pragma DATA_SECTION(ti_sysbios_gates_GateSwi_Module__diagsEnabled__C, ".const:ti_sysbios_gates_GateSwi_Module__diagsEnabled__C");
__FAR__ const CT__ti_sysbios_gates_GateSwi_Module__diagsEnabled ti_sysbios_gates_GateSwi_Module__diagsEnabled__C = (xdc_Bits32)0x90U;

/* Module__diagsIncluded__C */
#pragma DATA_SECTION(ti_sysbios_gates_GateSwi_Module__diagsIncluded__C, ".const:ti_sysbios_gates_GateSwi_Module__diagsIncluded__C");
__FAR__ const CT__ti_sysbios_gates_GateSwi_Module__diagsIncluded ti_sysbios_gates_GateSwi_Module__diagsIncluded__C = (xdc_Bits32)0x90U;

/* Module__diagsMask__C */
#pragma DATA_SECTION(ti_sysbios_gates_GateSwi_Module__diagsMask__C, ".const:ti_sysbios_gates_GateSwi_Module__diagsMask__C");
__FAR__ const CT__ti_sysbios_gates_GateSwi_Module__diagsMask ti_sysbios_gates_GateSwi_Module__diagsMask__C = ((const CT__ti_sysbios_gates_GateSwi_Module__diagsMask)NULL);

/* Module__gateObj__C */
#pragma DATA_SECTION(ti_sysbios_gates_GateSwi_Module__gateObj__C, ".const:ti_sysbios_gates_GateSwi_Module__gateObj__C");
__FAR__ const CT__ti_sysbios_gates_GateSwi_Module__gateObj ti_sysbios_gates_GateSwi_Module__gateObj__C = ((const CT__ti_sysbios_gates_GateSwi_Module__gateObj)NULL);

/* Module__gatePrms__C */
#pragma DATA_SECTION(ti_sysbios_gates_GateSwi_Module__gatePrms__C, ".const:ti_sysbios_gates_GateSwi_Module__gatePrms__C");
__FAR__ const CT__ti_sysbios_gates_GateSwi_Module__gatePrms ti_sysbios_gates_GateSwi_Module__gatePrms__C = ((const CT__ti_sysbios_gates_GateSwi_Module__gatePrms)NULL);

/* Module__id__C */
#pragma DATA_SECTION(ti_sysbios_gates_GateSwi_Module__id__C, ".const:ti_sysbios_gates_GateSwi_Module__id__C");
__FAR__ const CT__ti_sysbios_gates_GateSwi_Module__id ti_sysbios_gates_GateSwi_Module__id__C = (xdc_Bits16)0x8037U;

/* Module__loggerDefined__C */
#pragma DATA_SECTION(ti_sysbios_gates_GateSwi_Module__loggerDefined__C, ".const:ti_sysbios_gates_GateSwi_Module__loggerDefined__C");
__FAR__ const CT__ti_sysbios_gates_GateSwi_Module__loggerDefined ti_sysbios_gates_GateSwi_Module__loggerDefined__C = 0;

/* Module__loggerObj__C */
#pragma DATA_SECTION(ti_sysbios_gates_GateSwi_Module__loggerObj__C, ".const:ti_sysbios_gates_GateSwi_Module__loggerObj__C");
__FAR__ const CT__ti_sysbios_gates_GateSwi_Module__loggerObj ti_sysbios_gates_GateSwi_Module__loggerObj__C = ((const CT__ti_sysbios_gates_GateSwi_Module__loggerObj)NULL);

/* Module__loggerFxn0__C */
#pragma DATA_SECTION(ti_sysbios_gates_GateSwi_Module__loggerFxn0__C, ".const:ti_sysbios_gates_GateSwi_Module__loggerFxn0__C");
__FAR__ const CT__ti_sysbios_gates_GateSwi_Module__loggerFxn0 ti_sysbios_gates_GateSwi_Module__loggerFxn0__C = ((const CT__ti_sysbios_gates_GateSwi_Module__loggerFxn0)NULL);

/* Module__loggerFxn1__C */
#pragma DATA_SECTION(ti_sysbios_gates_GateSwi_Module__loggerFxn1__C, ".const:ti_sysbios_gates_GateSwi_Module__loggerFxn1__C");
__FAR__ const CT__ti_sysbios_gates_GateSwi_Module__loggerFxn1 ti_sysbios_gates_GateSwi_Module__loggerFxn1__C = ((const CT__ti_sysbios_gates_GateSwi_Module__loggerFxn1)NULL);

/* Module__loggerFxn2__C */
#pragma DATA_SECTION(ti_sysbios_gates_GateSwi_Module__loggerFxn2__C, ".const:ti_sysbios_gates_GateSwi_Module__loggerFxn2__C");
__FAR__ const CT__ti_sysbios_gates_GateSwi_Module__loggerFxn2 ti_sysbios_gates_GateSwi_Module__loggerFxn2__C = ((const CT__ti_sysbios_gates_GateSwi_Module__loggerFxn2)NULL);

/* Module__loggerFxn4__C */
#pragma DATA_SECTION(ti_sysbios_gates_GateSwi_Module__loggerFxn4__C, ".const:ti_sysbios_gates_GateSwi_Module__loggerFxn4__C");
__FAR__ const CT__ti_sysbios_gates_GateSwi_Module__loggerFxn4 ti_sysbios_gates_GateSwi_Module__loggerFxn4__C = ((const CT__ti_sysbios_gates_GateSwi_Module__loggerFxn4)NULL);

/* Module__loggerFxn8__C */
#pragma DATA_SECTION(ti_sysbios_gates_GateSwi_Module__loggerFxn8__C, ".const:ti_sysbios_gates_GateSwi_Module__loggerFxn8__C");
__FAR__ const CT__ti_sysbios_gates_GateSwi_Module__loggerFxn8 ti_sysbios_gates_GateSwi_Module__loggerFxn8__C = ((const CT__ti_sysbios_gates_GateSwi_Module__loggerFxn8)NULL);

/* Object__count__C */
#pragma DATA_SECTION(ti_sysbios_gates_GateSwi_Object__count__C, ".const:ti_sysbios_gates_GateSwi_Object__count__C");
__FAR__ const CT__ti_sysbios_gates_GateSwi_Object__count ti_sysbios_gates_GateSwi_Object__count__C = 0;

/* Object__heap__C */
#pragma DATA_SECTION(ti_sysbios_gates_GateSwi_Object__heap__C, ".const:ti_sysbios_gates_GateSwi_Object__heap__C");
__FAR__ const CT__ti_sysbios_gates_GateSwi_Object__heap ti_sysbios_gates_GateSwi_Object__heap__C = NULL;

/* Object__sizeof__C */
#pragma DATA_SECTION(ti_sysbios_gates_GateSwi_Object__sizeof__C, ".const:ti_sysbios_gates_GateSwi_Object__sizeof__C");
__FAR__ const CT__ti_sysbios_gates_GateSwi_Object__sizeof ti_sysbios_gates_GateSwi_Object__sizeof__C = sizeof(ti_sysbios_gates_GateSwi_Object__);

/* Object__table__C */
#pragma DATA_SECTION(ti_sysbios_gates_GateSwi_Object__table__C, ".const:ti_sysbios_gates_GateSwi_Object__table__C");
__FAR__ const CT__ti_sysbios_gates_GateSwi_Object__table ti_sysbios_gates_GateSwi_Object__table__C = NULL;

/* A_badContext__C */
#pragma DATA_SECTION(ti_sysbios_gates_GateSwi_A_badContext__C, ".const:ti_sysbios_gates_GateSwi_A_badContext__C");
__FAR__ const CT__ti_sysbios_gates_GateSwi_A_badContext ti_sysbios_gates_GateSwi_A_badContext__C = (((xdc_runtime_Assert_Id)3192) << 16 | 16);


/*
 * ======== ti.sysbios.gates.GateTask INITIALIZERS ========
 */

/* Object__DESC__C */
__FAR__ const xdc_runtime_Core_ObjDesc ti_sysbios_gates_GateTask_Object__DESC__C;

/* Object__PARAMS__C */
#pragma DATA_SECTION(ti_sysbios_gates_GateTask_Object__PARAMS__C, ".const:ti_sysbios_gates_GateTask_Object__PARAMS__C");
__FAR__ const ti_sysbios_gates_GateTask_Params ti_sysbios_gates_GateTask_Object__PARAMS__C = {
    sizeof (ti_sysbios_gates_GateTask_Params), /* __size */
    NULL, /* __self */
    NULL, /* __fxns */
    (xdc_runtime_IInstance_Params*)&ti_sysbios_gates_GateTask_Object__PARAMS__C.__iprms, /* instance */
    {
        sizeof (xdc_runtime_IInstance_Params), /* __size */
        0,  /* name */
    },  /* instance */
};

/* Module__root__V */
ti_sysbios_gates_GateTask_Module__ ti_sysbios_gates_GateTask_Module__root__V = {
    {&ti_sysbios_gates_GateTask_Module__root__V.link,  /* link.next */
    &ti_sysbios_gates_GateTask_Module__root__V.link},  /* link.prev */
};

/* Module__diagsEnabled__C */
#pragma DATA_SECTION(ti_sysbios_gates_GateTask_Module__diagsEnabled__C, ".const:ti_sysbios_gates_GateTask_Module__diagsEnabled__C");
__FAR__ const CT__ti_sysbios_gates_GateTask_Module__diagsEnabled ti_sysbios_gates_GateTask_Module__diagsEnabled__C = (xdc_Bits32)0x90U;

/* Module__diagsIncluded__C */
#pragma DATA_SECTION(ti_sysbios_gates_GateTask_Module__diagsIncluded__C, ".const:ti_sysbios_gates_GateTask_Module__diagsIncluded__C");
__FAR__ const CT__ti_sysbios_gates_GateTask_Module__diagsIncluded ti_sysbios_gates_GateTask_Module__diagsIncluded__C = (xdc_Bits32)0x90U;

/* Module__diagsMask__C */
#pragma DATA_SECTION(ti_sysbios_gates_GateTask_Module__diagsMask__C, ".const:ti_sysbios_gates_GateTask_Module__diagsMask__C");
__FAR__ const CT__ti_sysbios_gates_GateTask_Module__diagsMask ti_sysbios_gates_GateTask_Module__diagsMask__C = ((const CT__ti_sysbios_gates_GateTask_Module__diagsMask)NULL);

/* Module__gateObj__C */
#pragma DATA_SECTION(ti_sysbios_gates_GateTask_Module__gateObj__C, ".const:ti_sysbios_gates_GateTask_Module__gateObj__C");
__FAR__ const CT__ti_sysbios_gates_GateTask_Module__gateObj ti_sysbios_gates_GateTask_Module__gateObj__C = ((const CT__ti_sysbios_gates_GateTask_Module__gateObj)NULL);

/* Module__gatePrms__C */
#pragma DATA_SECTION(ti_sysbios_gates_GateTask_Module__gatePrms__C, ".const:ti_sysbios_gates_GateTask_Module__gatePrms__C");
__FAR__ const CT__ti_sysbios_gates_GateTask_Module__gatePrms ti_sysbios_gates_GateTask_Module__gatePrms__C = ((const CT__ti_sysbios_gates_GateTask_Module__gatePrms)NULL);

/* Module__id__C */
#pragma DATA_SECTION(ti_sysbios_gates_GateTask_Module__id__C, ".const:ti_sysbios_gates_GateTask_Module__id__C");
__FAR__ const CT__ti_sysbios_gates_GateTask_Module__id ti_sysbios_gates_GateTask_Module__id__C = (xdc_Bits16)0x8038U;

/* Module__loggerDefined__C */
#pragma DATA_SECTION(ti_sysbios_gates_GateTask_Module__loggerDefined__C, ".const:ti_sysbios_gates_GateTask_Module__loggerDefined__C");
__FAR__ const CT__ti_sysbios_gates_GateTask_Module__loggerDefined ti_sysbios_gates_GateTask_Module__loggerDefined__C = 0;

/* Module__loggerObj__C */
#pragma DATA_SECTION(ti_sysbios_gates_GateTask_Module__loggerObj__C, ".const:ti_sysbios_gates_GateTask_Module__loggerObj__C");
__FAR__ const CT__ti_sysbios_gates_GateTask_Module__loggerObj ti_sysbios_gates_GateTask_Module__loggerObj__C = ((const CT__ti_sysbios_gates_GateTask_Module__loggerObj)NULL);

/* Module__loggerFxn0__C */
#pragma DATA_SECTION(ti_sysbios_gates_GateTask_Module__loggerFxn0__C, ".const:ti_sysbios_gates_GateTask_Module__loggerFxn0__C");
__FAR__ const CT__ti_sysbios_gates_GateTask_Module__loggerFxn0 ti_sysbios_gates_GateTask_Module__loggerFxn0__C = ((const CT__ti_sysbios_gates_GateTask_Module__loggerFxn0)NULL);

/* Module__loggerFxn1__C */
#pragma DATA_SECTION(ti_sysbios_gates_GateTask_Module__loggerFxn1__C, ".const:ti_sysbios_gates_GateTask_Module__loggerFxn1__C");
__FAR__ const CT__ti_sysbios_gates_GateTask_Module__loggerFxn1 ti_sysbios_gates_GateTask_Module__loggerFxn1__C = ((const CT__ti_sysbios_gates_GateTask_Module__loggerFxn1)NULL);

/* Module__loggerFxn2__C */
#pragma DATA_SECTION(ti_sysbios_gates_GateTask_Module__loggerFxn2__C, ".const:ti_sysbios_gates_GateTask_Module__loggerFxn2__C");
__FAR__ const CT__ti_sysbios_gates_GateTask_Module__loggerFxn2 ti_sysbios_gates_GateTask_Module__loggerFxn2__C = ((const CT__ti_sysbios_gates_GateTask_Module__loggerFxn2)NULL);

/* Module__loggerFxn4__C */
#pragma DATA_SECTION(ti_sysbios_gates_GateTask_Module__loggerFxn4__C, ".const:ti_sysbios_gates_GateTask_Module__loggerFxn4__C");
__FAR__ const CT__ti_sysbios_gates_GateTask_Module__loggerFxn4 ti_sysbios_gates_GateTask_Module__loggerFxn4__C = ((const CT__ti_sysbios_gates_GateTask_Module__loggerFxn4)NULL);

/* Module__loggerFxn8__C */
#pragma DATA_SECTION(ti_sysbios_gates_GateTask_Module__loggerFxn8__C, ".const:ti_sysbios_gates_GateTask_Module__loggerFxn8__C");
__FAR__ const CT__ti_sysbios_gates_GateTask_Module__loggerFxn8 ti_sysbios_gates_GateTask_Module__loggerFxn8__C = ((const CT__ti_sysbios_gates_GateTask_Module__loggerFxn8)NULL);

/* Object__count__C */
#pragma DATA_SECTION(ti_sysbios_gates_GateTask_Object__count__C, ".const:ti_sysbios_gates_GateTask_Object__count__C");
__FAR__ const CT__ti_sysbios_gates_GateTask_Object__count ti_sysbios_gates_GateTask_Object__count__C = 0;

/* Object__heap__C */
#pragma DATA_SECTION(ti_sysbios_gates_GateTask_Object__heap__C, ".const:ti_sysbios_gates_GateTask_Object__heap__C");
__FAR__ const CT__ti_sysbios_gates_GateTask_Object__heap ti_sysbios_gates_GateTask_Object__heap__C = NULL;

/* Object__sizeof__C */
#pragma DATA_SECTION(ti_sysbios_gates_GateTask_Object__sizeof__C, ".const:ti_sysbios_gates_GateTask_Object__sizeof__C");
__FAR__ const CT__ti_sysbios_gates_GateTask_Object__sizeof ti_sysbios_gates_GateTask_Object__sizeof__C = sizeof(ti_sysbios_gates_GateTask_Object__);

/* Object__table__C */
#pragma DATA_SECTION(ti_sysbios_gates_GateTask_Object__table__C, ".const:ti_sysbios_gates_GateTask_Object__table__C");
__FAR__ const CT__ti_sysbios_gates_GateTask_Object__table ti_sysbios_gates_GateTask_Object__table__C = NULL;

/* A_badContext__C */
#pragma DATA_SECTION(ti_sysbios_gates_GateTask_A_badContext__C, ".const:ti_sysbios_gates_GateTask_A_badContext__C");
__FAR__ const CT__ti_sysbios_gates_GateTask_A_badContext ti_sysbios_gates_GateTask_A_badContext__C = (((xdc_runtime_Assert_Id)3280) << 16 | 16);


/*
 * ======== ti.sysbios.hal.Cache INITIALIZERS ========
 */

/* Module__diagsEnabled__C */
#pragma DATA_SECTION(ti_sysbios_hal_Cache_Module__diagsEnabled__C, ".const:ti_sysbios_hal_Cache_Module__diagsEnabled__C");
__FAR__ const CT__ti_sysbios_hal_Cache_Module__diagsEnabled ti_sysbios_hal_Cache_Module__diagsEnabled__C = (xdc_Bits32)0x90U;

/* Module__diagsIncluded__C */
#pragma DATA_SECTION(ti_sysbios_hal_Cache_Module__diagsIncluded__C, ".const:ti_sysbios_hal_Cache_Module__diagsIncluded__C");
__FAR__ const CT__ti_sysbios_hal_Cache_Module__diagsIncluded ti_sysbios_hal_Cache_Module__diagsIncluded__C = (xdc_Bits32)0x90U;

/* Module__diagsMask__C */
#pragma DATA_SECTION(ti_sysbios_hal_Cache_Module__diagsMask__C, ".const:ti_sysbios_hal_Cache_Module__diagsMask__C");
__FAR__ const CT__ti_sysbios_hal_Cache_Module__diagsMask ti_sysbios_hal_Cache_Module__diagsMask__C = ((const CT__ti_sysbios_hal_Cache_Module__diagsMask)NULL);

/* Module__gateObj__C */
#pragma DATA_SECTION(ti_sysbios_hal_Cache_Module__gateObj__C, ".const:ti_sysbios_hal_Cache_Module__gateObj__C");
__FAR__ const CT__ti_sysbios_hal_Cache_Module__gateObj ti_sysbios_hal_Cache_Module__gateObj__C = ((const CT__ti_sysbios_hal_Cache_Module__gateObj)NULL);

/* Module__gatePrms__C */
#pragma DATA_SECTION(ti_sysbios_hal_Cache_Module__gatePrms__C, ".const:ti_sysbios_hal_Cache_Module__gatePrms__C");
__FAR__ const CT__ti_sysbios_hal_Cache_Module__gatePrms ti_sysbios_hal_Cache_Module__gatePrms__C = ((const CT__ti_sysbios_hal_Cache_Module__gatePrms)NULL);

/* Module__id__C */
#pragma DATA_SECTION(ti_sysbios_hal_Cache_Module__id__C, ".const:ti_sysbios_hal_Cache_Module__id__C");
__FAR__ const CT__ti_sysbios_hal_Cache_Module__id ti_sysbios_hal_Cache_Module__id__C = (xdc_Bits16)0x802cU;

/* Module__loggerDefined__C */
#pragma DATA_SECTION(ti_sysbios_hal_Cache_Module__loggerDefined__C, ".const:ti_sysbios_hal_Cache_Module__loggerDefined__C");
__FAR__ const CT__ti_sysbios_hal_Cache_Module__loggerDefined ti_sysbios_hal_Cache_Module__loggerDefined__C = 0;

/* Module__loggerObj__C */
#pragma DATA_SECTION(ti_sysbios_hal_Cache_Module__loggerObj__C, ".const:ti_sysbios_hal_Cache_Module__loggerObj__C");
__FAR__ const CT__ti_sysbios_hal_Cache_Module__loggerObj ti_sysbios_hal_Cache_Module__loggerObj__C = ((const CT__ti_sysbios_hal_Cache_Module__loggerObj)NULL);

/* Module__loggerFxn0__C */
#pragma DATA_SECTION(ti_sysbios_hal_Cache_Module__loggerFxn0__C, ".const:ti_sysbios_hal_Cache_Module__loggerFxn0__C");
__FAR__ const CT__ti_sysbios_hal_Cache_Module__loggerFxn0 ti_sysbios_hal_Cache_Module__loggerFxn0__C = ((const CT__ti_sysbios_hal_Cache_Module__loggerFxn0)NULL);

/* Module__loggerFxn1__C */
#pragma DATA_SECTION(ti_sysbios_hal_Cache_Module__loggerFxn1__C, ".const:ti_sysbios_hal_Cache_Module__loggerFxn1__C");
__FAR__ const CT__ti_sysbios_hal_Cache_Module__loggerFxn1 ti_sysbios_hal_Cache_Module__loggerFxn1__C = ((const CT__ti_sysbios_hal_Cache_Module__loggerFxn1)NULL);

/* Module__loggerFxn2__C */
#pragma DATA_SECTION(ti_sysbios_hal_Cache_Module__loggerFxn2__C, ".const:ti_sysbios_hal_Cache_Module__loggerFxn2__C");
__FAR__ const CT__ti_sysbios_hal_Cache_Module__loggerFxn2 ti_sysbios_hal_Cache_Module__loggerFxn2__C = ((const CT__ti_sysbios_hal_Cache_Module__loggerFxn2)NULL);

/* Module__loggerFxn4__C */
#pragma DATA_SECTION(ti_sysbios_hal_Cache_Module__loggerFxn4__C, ".const:ti_sysbios_hal_Cache_Module__loggerFxn4__C");
__FAR__ const CT__ti_sysbios_hal_Cache_Module__loggerFxn4 ti_sysbios_hal_Cache_Module__loggerFxn4__C = ((const CT__ti_sysbios_hal_Cache_Module__loggerFxn4)NULL);

/* Module__loggerFxn8__C */
#pragma DATA_SECTION(ti_sysbios_hal_Cache_Module__loggerFxn8__C, ".const:ti_sysbios_hal_Cache_Module__loggerFxn8__C");
__FAR__ const CT__ti_sysbios_hal_Cache_Module__loggerFxn8 ti_sysbios_hal_Cache_Module__loggerFxn8__C = ((const CT__ti_sysbios_hal_Cache_Module__loggerFxn8)NULL);

/* Object__count__C */
#pragma DATA_SECTION(ti_sysbios_hal_Cache_Object__count__C, ".const:ti_sysbios_hal_Cache_Object__count__C");
__FAR__ const CT__ti_sysbios_hal_Cache_Object__count ti_sysbios_hal_Cache_Object__count__C = 0;

/* Object__heap__C */
#pragma DATA_SECTION(ti_sysbios_hal_Cache_Object__heap__C, ".const:ti_sysbios_hal_Cache_Object__heap__C");
__FAR__ const CT__ti_sysbios_hal_Cache_Object__heap ti_sysbios_hal_Cache_Object__heap__C = NULL;

/* Object__sizeof__C */
#pragma DATA_SECTION(ti_sysbios_hal_Cache_Object__sizeof__C, ".const:ti_sysbios_hal_Cache_Object__sizeof__C");
__FAR__ const CT__ti_sysbios_hal_Cache_Object__sizeof ti_sysbios_hal_Cache_Object__sizeof__C = 0;

/* Object__table__C */
#pragma DATA_SECTION(ti_sysbios_hal_Cache_Object__table__C, ".const:ti_sysbios_hal_Cache_Object__table__C");
__FAR__ const CT__ti_sysbios_hal_Cache_Object__table ti_sysbios_hal_Cache_Object__table__C = NULL;

/* enableCache__C */
#pragma DATA_SECTION(ti_sysbios_hal_Cache_enableCache__C, ".const:ti_sysbios_hal_Cache_enableCache__C");
__FAR__ const CT__ti_sysbios_hal_Cache_enableCache ti_sysbios_hal_Cache_enableCache__C = 1;


/*
 * ======== ti.sysbios.hal.Cache_CacheProxy INITIALIZERS ========
 */


/*
 * ======== ti.sysbios.hal.Core INITIALIZERS ========
 */

/* Module__diagsEnabled__C */
#pragma DATA_SECTION(ti_sysbios_hal_Core_Module__diagsEnabled__C, ".const:ti_sysbios_hal_Core_Module__diagsEnabled__C");
__FAR__ const CT__ti_sysbios_hal_Core_Module__diagsEnabled ti_sysbios_hal_Core_Module__diagsEnabled__C = (xdc_Bits32)0x90U;

/* Module__diagsIncluded__C */
#pragma DATA_SECTION(ti_sysbios_hal_Core_Module__diagsIncluded__C, ".const:ti_sysbios_hal_Core_Module__diagsIncluded__C");
__FAR__ const CT__ti_sysbios_hal_Core_Module__diagsIncluded ti_sysbios_hal_Core_Module__diagsIncluded__C = (xdc_Bits32)0x90U;

/* Module__diagsMask__C */
#pragma DATA_SECTION(ti_sysbios_hal_Core_Module__diagsMask__C, ".const:ti_sysbios_hal_Core_Module__diagsMask__C");
__FAR__ const CT__ti_sysbios_hal_Core_Module__diagsMask ti_sysbios_hal_Core_Module__diagsMask__C = ((const CT__ti_sysbios_hal_Core_Module__diagsMask)NULL);

/* Module__gateObj__C */
#pragma DATA_SECTION(ti_sysbios_hal_Core_Module__gateObj__C, ".const:ti_sysbios_hal_Core_Module__gateObj__C");
__FAR__ const CT__ti_sysbios_hal_Core_Module__gateObj ti_sysbios_hal_Core_Module__gateObj__C = ((const CT__ti_sysbios_hal_Core_Module__gateObj)NULL);

/* Module__gatePrms__C */
#pragma DATA_SECTION(ti_sysbios_hal_Core_Module__gatePrms__C, ".const:ti_sysbios_hal_Core_Module__gatePrms__C");
__FAR__ const CT__ti_sysbios_hal_Core_Module__gatePrms ti_sysbios_hal_Core_Module__gatePrms__C = ((const CT__ti_sysbios_hal_Core_Module__gatePrms)NULL);

/* Module__id__C */
#pragma DATA_SECTION(ti_sysbios_hal_Core_Module__id__C, ".const:ti_sysbios_hal_Core_Module__id__C");
__FAR__ const CT__ti_sysbios_hal_Core_Module__id ti_sysbios_hal_Core_Module__id__C = (xdc_Bits16)0x802dU;

/* Module__loggerDefined__C */
#pragma DATA_SECTION(ti_sysbios_hal_Core_Module__loggerDefined__C, ".const:ti_sysbios_hal_Core_Module__loggerDefined__C");
__FAR__ const CT__ti_sysbios_hal_Core_Module__loggerDefined ti_sysbios_hal_Core_Module__loggerDefined__C = 0;

/* Module__loggerObj__C */
#pragma DATA_SECTION(ti_sysbios_hal_Core_Module__loggerObj__C, ".const:ti_sysbios_hal_Core_Module__loggerObj__C");
__FAR__ const CT__ti_sysbios_hal_Core_Module__loggerObj ti_sysbios_hal_Core_Module__loggerObj__C = ((const CT__ti_sysbios_hal_Core_Module__loggerObj)NULL);

/* Module__loggerFxn0__C */
#pragma DATA_SECTION(ti_sysbios_hal_Core_Module__loggerFxn0__C, ".const:ti_sysbios_hal_Core_Module__loggerFxn0__C");
__FAR__ const CT__ti_sysbios_hal_Core_Module__loggerFxn0 ti_sysbios_hal_Core_Module__loggerFxn0__C = ((const CT__ti_sysbios_hal_Core_Module__loggerFxn0)NULL);

/* Module__loggerFxn1__C */
#pragma DATA_SECTION(ti_sysbios_hal_Core_Module__loggerFxn1__C, ".const:ti_sysbios_hal_Core_Module__loggerFxn1__C");
__FAR__ const CT__ti_sysbios_hal_Core_Module__loggerFxn1 ti_sysbios_hal_Core_Module__loggerFxn1__C = ((const CT__ti_sysbios_hal_Core_Module__loggerFxn1)NULL);

/* Module__loggerFxn2__C */
#pragma DATA_SECTION(ti_sysbios_hal_Core_Module__loggerFxn2__C, ".const:ti_sysbios_hal_Core_Module__loggerFxn2__C");
__FAR__ const CT__ti_sysbios_hal_Core_Module__loggerFxn2 ti_sysbios_hal_Core_Module__loggerFxn2__C = ((const CT__ti_sysbios_hal_Core_Module__loggerFxn2)NULL);

/* Module__loggerFxn4__C */
#pragma DATA_SECTION(ti_sysbios_hal_Core_Module__loggerFxn4__C, ".const:ti_sysbios_hal_Core_Module__loggerFxn4__C");
__FAR__ const CT__ti_sysbios_hal_Core_Module__loggerFxn4 ti_sysbios_hal_Core_Module__loggerFxn4__C = ((const CT__ti_sysbios_hal_Core_Module__loggerFxn4)NULL);

/* Module__loggerFxn8__C */
#pragma DATA_SECTION(ti_sysbios_hal_Core_Module__loggerFxn8__C, ".const:ti_sysbios_hal_Core_Module__loggerFxn8__C");
__FAR__ const CT__ti_sysbios_hal_Core_Module__loggerFxn8 ti_sysbios_hal_Core_Module__loggerFxn8__C = ((const CT__ti_sysbios_hal_Core_Module__loggerFxn8)NULL);

/* Object__count__C */
#pragma DATA_SECTION(ti_sysbios_hal_Core_Object__count__C, ".const:ti_sysbios_hal_Core_Object__count__C");
__FAR__ const CT__ti_sysbios_hal_Core_Object__count ti_sysbios_hal_Core_Object__count__C = 0;

/* Object__heap__C */
#pragma DATA_SECTION(ti_sysbios_hal_Core_Object__heap__C, ".const:ti_sysbios_hal_Core_Object__heap__C");
__FAR__ const CT__ti_sysbios_hal_Core_Object__heap ti_sysbios_hal_Core_Object__heap__C = NULL;

/* Object__sizeof__C */
#pragma DATA_SECTION(ti_sysbios_hal_Core_Object__sizeof__C, ".const:ti_sysbios_hal_Core_Object__sizeof__C");
__FAR__ const CT__ti_sysbios_hal_Core_Object__sizeof ti_sysbios_hal_Core_Object__sizeof__C = 0;

/* Object__table__C */
#pragma DATA_SECTION(ti_sysbios_hal_Core_Object__table__C, ".const:ti_sysbios_hal_Core_Object__table__C");
__FAR__ const CT__ti_sysbios_hal_Core_Object__table ti_sysbios_hal_Core_Object__table__C = NULL;

/* numCores__C */
#pragma DATA_SECTION(ti_sysbios_hal_Core_numCores__C, ".const:ti_sysbios_hal_Core_numCores__C");
__FAR__ const CT__ti_sysbios_hal_Core_numCores ti_sysbios_hal_Core_numCores__C = (xdc_UInt)0x1U;


/*
 * ======== ti.sysbios.hal.CoreNull INITIALIZERS ========
 */

/* Module__diagsEnabled__C */
#pragma DATA_SECTION(ti_sysbios_hal_CoreNull_Module__diagsEnabled__C, ".const:ti_sysbios_hal_CoreNull_Module__diagsEnabled__C");
__FAR__ const CT__ti_sysbios_hal_CoreNull_Module__diagsEnabled ti_sysbios_hal_CoreNull_Module__diagsEnabled__C = (xdc_Bits32)0x90U;

/* Module__diagsIncluded__C */
#pragma DATA_SECTION(ti_sysbios_hal_CoreNull_Module__diagsIncluded__C, ".const:ti_sysbios_hal_CoreNull_Module__diagsIncluded__C");
__FAR__ const CT__ti_sysbios_hal_CoreNull_Module__diagsIncluded ti_sysbios_hal_CoreNull_Module__diagsIncluded__C = (xdc_Bits32)0x90U;

/* Module__diagsMask__C */
#pragma DATA_SECTION(ti_sysbios_hal_CoreNull_Module__diagsMask__C, ".const:ti_sysbios_hal_CoreNull_Module__diagsMask__C");
__FAR__ const CT__ti_sysbios_hal_CoreNull_Module__diagsMask ti_sysbios_hal_CoreNull_Module__diagsMask__C = ((const CT__ti_sysbios_hal_CoreNull_Module__diagsMask)NULL);

/* Module__gateObj__C */
#pragma DATA_SECTION(ti_sysbios_hal_CoreNull_Module__gateObj__C, ".const:ti_sysbios_hal_CoreNull_Module__gateObj__C");
__FAR__ const CT__ti_sysbios_hal_CoreNull_Module__gateObj ti_sysbios_hal_CoreNull_Module__gateObj__C = ((const CT__ti_sysbios_hal_CoreNull_Module__gateObj)NULL);

/* Module__gatePrms__C */
#pragma DATA_SECTION(ti_sysbios_hal_CoreNull_Module__gatePrms__C, ".const:ti_sysbios_hal_CoreNull_Module__gatePrms__C");
__FAR__ const CT__ti_sysbios_hal_CoreNull_Module__gatePrms ti_sysbios_hal_CoreNull_Module__gatePrms__C = ((const CT__ti_sysbios_hal_CoreNull_Module__gatePrms)NULL);

/* Module__id__C */
#pragma DATA_SECTION(ti_sysbios_hal_CoreNull_Module__id__C, ".const:ti_sysbios_hal_CoreNull_Module__id__C");
__FAR__ const CT__ti_sysbios_hal_CoreNull_Module__id ti_sysbios_hal_CoreNull_Module__id__C = (xdc_Bits16)0x802eU;

/* Module__loggerDefined__C */
#pragma DATA_SECTION(ti_sysbios_hal_CoreNull_Module__loggerDefined__C, ".const:ti_sysbios_hal_CoreNull_Module__loggerDefined__C");
__FAR__ const CT__ti_sysbios_hal_CoreNull_Module__loggerDefined ti_sysbios_hal_CoreNull_Module__loggerDefined__C = 0;

/* Module__loggerObj__C */
#pragma DATA_SECTION(ti_sysbios_hal_CoreNull_Module__loggerObj__C, ".const:ti_sysbios_hal_CoreNull_Module__loggerObj__C");
__FAR__ const CT__ti_sysbios_hal_CoreNull_Module__loggerObj ti_sysbios_hal_CoreNull_Module__loggerObj__C = ((const CT__ti_sysbios_hal_CoreNull_Module__loggerObj)NULL);

/* Module__loggerFxn0__C */
#pragma DATA_SECTION(ti_sysbios_hal_CoreNull_Module__loggerFxn0__C, ".const:ti_sysbios_hal_CoreNull_Module__loggerFxn0__C");
__FAR__ const CT__ti_sysbios_hal_CoreNull_Module__loggerFxn0 ti_sysbios_hal_CoreNull_Module__loggerFxn0__C = ((const CT__ti_sysbios_hal_CoreNull_Module__loggerFxn0)NULL);

/* Module__loggerFxn1__C */
#pragma DATA_SECTION(ti_sysbios_hal_CoreNull_Module__loggerFxn1__C, ".const:ti_sysbios_hal_CoreNull_Module__loggerFxn1__C");
__FAR__ const CT__ti_sysbios_hal_CoreNull_Module__loggerFxn1 ti_sysbios_hal_CoreNull_Module__loggerFxn1__C = ((const CT__ti_sysbios_hal_CoreNull_Module__loggerFxn1)NULL);

/* Module__loggerFxn2__C */
#pragma DATA_SECTION(ti_sysbios_hal_CoreNull_Module__loggerFxn2__C, ".const:ti_sysbios_hal_CoreNull_Module__loggerFxn2__C");
__FAR__ const CT__ti_sysbios_hal_CoreNull_Module__loggerFxn2 ti_sysbios_hal_CoreNull_Module__loggerFxn2__C = ((const CT__ti_sysbios_hal_CoreNull_Module__loggerFxn2)NULL);

/* Module__loggerFxn4__C */
#pragma DATA_SECTION(ti_sysbios_hal_CoreNull_Module__loggerFxn4__C, ".const:ti_sysbios_hal_CoreNull_Module__loggerFxn4__C");
__FAR__ const CT__ti_sysbios_hal_CoreNull_Module__loggerFxn4 ti_sysbios_hal_CoreNull_Module__loggerFxn4__C = ((const CT__ti_sysbios_hal_CoreNull_Module__loggerFxn4)NULL);

/* Module__loggerFxn8__C */
#pragma DATA_SECTION(ti_sysbios_hal_CoreNull_Module__loggerFxn8__C, ".const:ti_sysbios_hal_CoreNull_Module__loggerFxn8__C");
__FAR__ const CT__ti_sysbios_hal_CoreNull_Module__loggerFxn8 ti_sysbios_hal_CoreNull_Module__loggerFxn8__C = ((const CT__ti_sysbios_hal_CoreNull_Module__loggerFxn8)NULL);

/* Object__count__C */
#pragma DATA_SECTION(ti_sysbios_hal_CoreNull_Object__count__C, ".const:ti_sysbios_hal_CoreNull_Object__count__C");
__FAR__ const CT__ti_sysbios_hal_CoreNull_Object__count ti_sysbios_hal_CoreNull_Object__count__C = 0;

/* Object__heap__C */
#pragma DATA_SECTION(ti_sysbios_hal_CoreNull_Object__heap__C, ".const:ti_sysbios_hal_CoreNull_Object__heap__C");
__FAR__ const CT__ti_sysbios_hal_CoreNull_Object__heap ti_sysbios_hal_CoreNull_Object__heap__C = NULL;

/* Object__sizeof__C */
#pragma DATA_SECTION(ti_sysbios_hal_CoreNull_Object__sizeof__C, ".const:ti_sysbios_hal_CoreNull_Object__sizeof__C");
__FAR__ const CT__ti_sysbios_hal_CoreNull_Object__sizeof ti_sysbios_hal_CoreNull_Object__sizeof__C = 0;

/* Object__table__C */
#pragma DATA_SECTION(ti_sysbios_hal_CoreNull_Object__table__C, ".const:ti_sysbios_hal_CoreNull_Object__table__C");
__FAR__ const CT__ti_sysbios_hal_CoreNull_Object__table ti_sysbios_hal_CoreNull_Object__table__C = NULL;

/* numCores__C */
#pragma DATA_SECTION(ti_sysbios_hal_CoreNull_numCores__C, ".const:ti_sysbios_hal_CoreNull_numCores__C");
__FAR__ const CT__ti_sysbios_hal_CoreNull_numCores ti_sysbios_hal_CoreNull_numCores__C = (xdc_UInt)0x1U;

/* id__C */
#pragma DATA_SECTION(ti_sysbios_hal_CoreNull_id__C, ".const:ti_sysbios_hal_CoreNull_id__C");
__FAR__ const CT__ti_sysbios_hal_CoreNull_id ti_sysbios_hal_CoreNull_id__C = (xdc_UInt)0x0U;


/*
 * ======== ti.sysbios.hal.Core_CoreProxy INITIALIZERS ========
 */


/*
 * ======== ti.sysbios.hal.Hwi INITIALIZERS ========
 */

/* Object__DESC__C */
__FAR__ const xdc_runtime_Core_ObjDesc ti_sysbios_hal_Hwi_Object__DESC__C;

/* Object__PARAMS__C */
#pragma DATA_SECTION(ti_sysbios_hal_Hwi_Object__PARAMS__C, ".const:ti_sysbios_hal_Hwi_Object__PARAMS__C");
__FAR__ const ti_sysbios_hal_Hwi_Params ti_sysbios_hal_Hwi_Object__PARAMS__C = {
    sizeof (ti_sysbios_hal_Hwi_Params), /* __size */
    NULL, /* __self */
    NULL, /* __fxns */
    (xdc_runtime_IInstance_Params*)&ti_sysbios_hal_Hwi_Object__PARAMS__C.__iprms, /* instance */
    ti_sysbios_interfaces_IHwi_MaskingOption_SELF,  /* maskSetting */
    ((xdc_UArg)(0x0)),  /* arg */
    1,  /* enableInt */
    (xdc_Int)(-0x0 - 1),  /* eventId */
    (xdc_Int)(-0x0 - 1),  /* priority */
    {
        sizeof (xdc_runtime_IInstance_Params), /* __size */
        0,  /* name */
    },  /* instance */
};

/* Module__root__V */
ti_sysbios_hal_Hwi_Module__ ti_sysbios_hal_Hwi_Module__root__V = {
    {&ti_sysbios_hal_Hwi_Module__root__V.link,  /* link.next */
    &ti_sysbios_hal_Hwi_Module__root__V.link},  /* link.prev */
};

/* Object__table__V */
ti_sysbios_hal_Hwi_Object__ ti_sysbios_hal_Hwi_Object__table__V[1] = {
    {/* instance#0 */
        0,
        (ti_sysbios_hal_Hwi_HwiProxy_Handle)&ti_sysbios_family_c7x_Hwi_Object__table__V[0],  /* pi */
    },
};

/* Module__diagsEnabled__C */
#pragma DATA_SECTION(ti_sysbios_hal_Hwi_Module__diagsEnabled__C, ".const:ti_sysbios_hal_Hwi_Module__diagsEnabled__C");
__FAR__ const CT__ti_sysbios_hal_Hwi_Module__diagsEnabled ti_sysbios_hal_Hwi_Module__diagsEnabled__C = (xdc_Bits32)0x90U;

/* Module__diagsIncluded__C */
#pragma DATA_SECTION(ti_sysbios_hal_Hwi_Module__diagsIncluded__C, ".const:ti_sysbios_hal_Hwi_Module__diagsIncluded__C");
__FAR__ const CT__ti_sysbios_hal_Hwi_Module__diagsIncluded ti_sysbios_hal_Hwi_Module__diagsIncluded__C = (xdc_Bits32)0x90U;

/* Module__diagsMask__C */
#pragma DATA_SECTION(ti_sysbios_hal_Hwi_Module__diagsMask__C, ".const:ti_sysbios_hal_Hwi_Module__diagsMask__C");
__FAR__ const CT__ti_sysbios_hal_Hwi_Module__diagsMask ti_sysbios_hal_Hwi_Module__diagsMask__C = ((const CT__ti_sysbios_hal_Hwi_Module__diagsMask)NULL);

/* Module__gateObj__C */
#pragma DATA_SECTION(ti_sysbios_hal_Hwi_Module__gateObj__C, ".const:ti_sysbios_hal_Hwi_Module__gateObj__C");
__FAR__ const CT__ti_sysbios_hal_Hwi_Module__gateObj ti_sysbios_hal_Hwi_Module__gateObj__C = ((const CT__ti_sysbios_hal_Hwi_Module__gateObj)NULL);

/* Module__gatePrms__C */
#pragma DATA_SECTION(ti_sysbios_hal_Hwi_Module__gatePrms__C, ".const:ti_sysbios_hal_Hwi_Module__gatePrms__C");
__FAR__ const CT__ti_sysbios_hal_Hwi_Module__gatePrms ti_sysbios_hal_Hwi_Module__gatePrms__C = ((const CT__ti_sysbios_hal_Hwi_Module__gatePrms)NULL);

/* Module__id__C */
#pragma DATA_SECTION(ti_sysbios_hal_Hwi_Module__id__C, ".const:ti_sysbios_hal_Hwi_Module__id__C");
__FAR__ const CT__ti_sysbios_hal_Hwi_Module__id ti_sysbios_hal_Hwi_Module__id__C = (xdc_Bits16)0x802fU;

/* Module__loggerDefined__C */
#pragma DATA_SECTION(ti_sysbios_hal_Hwi_Module__loggerDefined__C, ".const:ti_sysbios_hal_Hwi_Module__loggerDefined__C");
__FAR__ const CT__ti_sysbios_hal_Hwi_Module__loggerDefined ti_sysbios_hal_Hwi_Module__loggerDefined__C = 0;

/* Module__loggerObj__C */
#pragma DATA_SECTION(ti_sysbios_hal_Hwi_Module__loggerObj__C, ".const:ti_sysbios_hal_Hwi_Module__loggerObj__C");
__FAR__ const CT__ti_sysbios_hal_Hwi_Module__loggerObj ti_sysbios_hal_Hwi_Module__loggerObj__C = ((const CT__ti_sysbios_hal_Hwi_Module__loggerObj)NULL);

/* Module__loggerFxn0__C */
#pragma DATA_SECTION(ti_sysbios_hal_Hwi_Module__loggerFxn0__C, ".const:ti_sysbios_hal_Hwi_Module__loggerFxn0__C");
__FAR__ const CT__ti_sysbios_hal_Hwi_Module__loggerFxn0 ti_sysbios_hal_Hwi_Module__loggerFxn0__C = ((const CT__ti_sysbios_hal_Hwi_Module__loggerFxn0)NULL);

/* Module__loggerFxn1__C */
#pragma DATA_SECTION(ti_sysbios_hal_Hwi_Module__loggerFxn1__C, ".const:ti_sysbios_hal_Hwi_Module__loggerFxn1__C");
__FAR__ const CT__ti_sysbios_hal_Hwi_Module__loggerFxn1 ti_sysbios_hal_Hwi_Module__loggerFxn1__C = ((const CT__ti_sysbios_hal_Hwi_Module__loggerFxn1)NULL);

/* Module__loggerFxn2__C */
#pragma DATA_SECTION(ti_sysbios_hal_Hwi_Module__loggerFxn2__C, ".const:ti_sysbios_hal_Hwi_Module__loggerFxn2__C");
__FAR__ const CT__ti_sysbios_hal_Hwi_Module__loggerFxn2 ti_sysbios_hal_Hwi_Module__loggerFxn2__C = ((const CT__ti_sysbios_hal_Hwi_Module__loggerFxn2)NULL);

/* Module__loggerFxn4__C */
#pragma DATA_SECTION(ti_sysbios_hal_Hwi_Module__loggerFxn4__C, ".const:ti_sysbios_hal_Hwi_Module__loggerFxn4__C");
__FAR__ const CT__ti_sysbios_hal_Hwi_Module__loggerFxn4 ti_sysbios_hal_Hwi_Module__loggerFxn4__C = ((const CT__ti_sysbios_hal_Hwi_Module__loggerFxn4)NULL);

/* Module__loggerFxn8__C */
#pragma DATA_SECTION(ti_sysbios_hal_Hwi_Module__loggerFxn8__C, ".const:ti_sysbios_hal_Hwi_Module__loggerFxn8__C");
__FAR__ const CT__ti_sysbios_hal_Hwi_Module__loggerFxn8 ti_sysbios_hal_Hwi_Module__loggerFxn8__C = ((const CT__ti_sysbios_hal_Hwi_Module__loggerFxn8)NULL);

/* Object__count__C */
#pragma DATA_SECTION(ti_sysbios_hal_Hwi_Object__count__C, ".const:ti_sysbios_hal_Hwi_Object__count__C");
__FAR__ const CT__ti_sysbios_hal_Hwi_Object__count ti_sysbios_hal_Hwi_Object__count__C = 1;

/* Object__heap__C */
#pragma DATA_SECTION(ti_sysbios_hal_Hwi_Object__heap__C, ".const:ti_sysbios_hal_Hwi_Object__heap__C");
__FAR__ const CT__ti_sysbios_hal_Hwi_Object__heap ti_sysbios_hal_Hwi_Object__heap__C = NULL;

/* Object__sizeof__C */
#pragma DATA_SECTION(ti_sysbios_hal_Hwi_Object__sizeof__C, ".const:ti_sysbios_hal_Hwi_Object__sizeof__C");
__FAR__ const CT__ti_sysbios_hal_Hwi_Object__sizeof ti_sysbios_hal_Hwi_Object__sizeof__C = sizeof(ti_sysbios_hal_Hwi_Object__);

/* Object__table__C */
#pragma DATA_SECTION(ti_sysbios_hal_Hwi_Object__table__C, ".const:ti_sysbios_hal_Hwi_Object__table__C");
__FAR__ const CT__ti_sysbios_hal_Hwi_Object__table ti_sysbios_hal_Hwi_Object__table__C = ti_sysbios_hal_Hwi_Object__table__V;

/* dispatcherAutoNestingSupport__C */
#pragma DATA_SECTION(ti_sysbios_hal_Hwi_dispatcherAutoNestingSupport__C, ".const:ti_sysbios_hal_Hwi_dispatcherAutoNestingSupport__C");
__FAR__ const CT__ti_sysbios_hal_Hwi_dispatcherAutoNestingSupport ti_sysbios_hal_Hwi_dispatcherAutoNestingSupport__C = 1;

/* dispatcherSwiSupport__C */
#pragma DATA_SECTION(ti_sysbios_hal_Hwi_dispatcherSwiSupport__C, ".const:ti_sysbios_hal_Hwi_dispatcherSwiSupport__C");
__FAR__ const CT__ti_sysbios_hal_Hwi_dispatcherSwiSupport ti_sysbios_hal_Hwi_dispatcherSwiSupport__C = 1;

/* dispatcherTaskSupport__C */
#pragma DATA_SECTION(ti_sysbios_hal_Hwi_dispatcherTaskSupport__C, ".const:ti_sysbios_hal_Hwi_dispatcherTaskSupport__C");
__FAR__ const CT__ti_sysbios_hal_Hwi_dispatcherTaskSupport ti_sysbios_hal_Hwi_dispatcherTaskSupport__C = 1;

/* dispatcherIrpTrackingSupport__C */
#pragma DATA_SECTION(ti_sysbios_hal_Hwi_dispatcherIrpTrackingSupport__C, ".const:ti_sysbios_hal_Hwi_dispatcherIrpTrackingSupport__C");
__FAR__ const CT__ti_sysbios_hal_Hwi_dispatcherIrpTrackingSupport ti_sysbios_hal_Hwi_dispatcherIrpTrackingSupport__C = 1;

/* E_stackOverflow__C */
#pragma DATA_SECTION(ti_sysbios_hal_Hwi_E_stackOverflow__C, ".const:ti_sysbios_hal_Hwi_E_stackOverflow__C");
__FAR__ const CT__ti_sysbios_hal_Hwi_E_stackOverflow ti_sysbios_hal_Hwi_E_stackOverflow__C = (((xdc_runtime_Error_Id)5016) << 16 | 0U);


/*
 * ======== ti.sysbios.hal.Hwi_HwiProxy INITIALIZERS ========
 */


/*
 * ======== ti.sysbios.heaps.HeapBuf INITIALIZERS ========
 */

/* Object__DESC__C */
__FAR__ const xdc_runtime_Core_ObjDesc ti_sysbios_heaps_HeapBuf_Object__DESC__C;

/* Object__PARAMS__C */
#pragma DATA_SECTION(ti_sysbios_heaps_HeapBuf_Object__PARAMS__C, ".const:ti_sysbios_heaps_HeapBuf_Object__PARAMS__C");
__FAR__ const ti_sysbios_heaps_HeapBuf_Params ti_sysbios_heaps_HeapBuf_Object__PARAMS__C = {
    sizeof (ti_sysbios_heaps_HeapBuf_Params), /* __size */
    NULL, /* __self */
    NULL, /* __fxns */
    (xdc_runtime_IInstance_Params*)&ti_sysbios_heaps_HeapBuf_Object__PARAMS__C.__iprms, /* instance */
    (xdc_SizeT)0x0,  /* align */
    (xdc_UInt)0x0U,  /* numBlocks */
    (xdc_SizeT)0x0,  /* blockSize */
    ((xdc_UArg)(0x0)),  /* bufSize */
    ((xdc_Ptr)(0x0)),  /* buf */
    {
        sizeof (xdc_runtime_IInstance_Params), /* __size */
        0,  /* name */
    },  /* instance */
};

/* Module__root__V */
ti_sysbios_heaps_HeapBuf_Module__ ti_sysbios_heaps_HeapBuf_Module__root__V = {
    {&ti_sysbios_heaps_HeapBuf_Module__root__V.link,  /* link.next */
    &ti_sysbios_heaps_HeapBuf_Module__root__V.link},  /* link.prev */
};

/* Module__state__V */
#ifdef __ti__
ti_sysbios_heaps_HeapBuf_Module_State__ ti_sysbios_heaps_HeapBuf_Module__state__V __attribute__ ((section(".data:ti_sysbios_heaps_HeapBuf_Module__state__V")));
#elif !(defined(__MACH__) && defined(__APPLE__))
ti_sysbios_heaps_HeapBuf_Module_State__ ti_sysbios_heaps_HeapBuf_Module__state__V __attribute__ ((section(".data_ti_sysbios_heaps_HeapBuf_Module__state__V")));
#endif
ti_sysbios_heaps_HeapBuf_Module_State__ ti_sysbios_heaps_HeapBuf_Module__state__V = {
    ((void*)0),  /* constructedHeaps */
};

/* Module__diagsEnabled__C */
#pragma DATA_SECTION(ti_sysbios_heaps_HeapBuf_Module__diagsEnabled__C, ".const:ti_sysbios_heaps_HeapBuf_Module__diagsEnabled__C");
__FAR__ const CT__ti_sysbios_heaps_HeapBuf_Module__diagsEnabled ti_sysbios_heaps_HeapBuf_Module__diagsEnabled__C = (xdc_Bits32)0x90U;

/* Module__diagsIncluded__C */
#pragma DATA_SECTION(ti_sysbios_heaps_HeapBuf_Module__diagsIncluded__C, ".const:ti_sysbios_heaps_HeapBuf_Module__diagsIncluded__C");
__FAR__ const CT__ti_sysbios_heaps_HeapBuf_Module__diagsIncluded ti_sysbios_heaps_HeapBuf_Module__diagsIncluded__C = (xdc_Bits32)0x90U;

/* Module__diagsMask__C */
#pragma DATA_SECTION(ti_sysbios_heaps_HeapBuf_Module__diagsMask__C, ".const:ti_sysbios_heaps_HeapBuf_Module__diagsMask__C");
__FAR__ const CT__ti_sysbios_heaps_HeapBuf_Module__diagsMask ti_sysbios_heaps_HeapBuf_Module__diagsMask__C = ((const CT__ti_sysbios_heaps_HeapBuf_Module__diagsMask)NULL);

/* Module__gateObj__C */
#pragma DATA_SECTION(ti_sysbios_heaps_HeapBuf_Module__gateObj__C, ".const:ti_sysbios_heaps_HeapBuf_Module__gateObj__C");
__FAR__ const CT__ti_sysbios_heaps_HeapBuf_Module__gateObj ti_sysbios_heaps_HeapBuf_Module__gateObj__C = ((const CT__ti_sysbios_heaps_HeapBuf_Module__gateObj)NULL);

/* Module__gatePrms__C */
#pragma DATA_SECTION(ti_sysbios_heaps_HeapBuf_Module__gatePrms__C, ".const:ti_sysbios_heaps_HeapBuf_Module__gatePrms__C");
__FAR__ const CT__ti_sysbios_heaps_HeapBuf_Module__gatePrms ti_sysbios_heaps_HeapBuf_Module__gatePrms__C = ((const CT__ti_sysbios_heaps_HeapBuf_Module__gatePrms)NULL);

/* Module__id__C */
#pragma DATA_SECTION(ti_sysbios_heaps_HeapBuf_Module__id__C, ".const:ti_sysbios_heaps_HeapBuf_Module__id__C");
__FAR__ const CT__ti_sysbios_heaps_HeapBuf_Module__id ti_sysbios_heaps_HeapBuf_Module__id__C = (xdc_Bits16)0x8031U;

/* Module__loggerDefined__C */
#pragma DATA_SECTION(ti_sysbios_heaps_HeapBuf_Module__loggerDefined__C, ".const:ti_sysbios_heaps_HeapBuf_Module__loggerDefined__C");
__FAR__ const CT__ti_sysbios_heaps_HeapBuf_Module__loggerDefined ti_sysbios_heaps_HeapBuf_Module__loggerDefined__C = 0;

/* Module__loggerObj__C */
#pragma DATA_SECTION(ti_sysbios_heaps_HeapBuf_Module__loggerObj__C, ".const:ti_sysbios_heaps_HeapBuf_Module__loggerObj__C");
__FAR__ const CT__ti_sysbios_heaps_HeapBuf_Module__loggerObj ti_sysbios_heaps_HeapBuf_Module__loggerObj__C = ((const CT__ti_sysbios_heaps_HeapBuf_Module__loggerObj)NULL);

/* Module__loggerFxn0__C */
#pragma DATA_SECTION(ti_sysbios_heaps_HeapBuf_Module__loggerFxn0__C, ".const:ti_sysbios_heaps_HeapBuf_Module__loggerFxn0__C");
__FAR__ const CT__ti_sysbios_heaps_HeapBuf_Module__loggerFxn0 ti_sysbios_heaps_HeapBuf_Module__loggerFxn0__C = ((const CT__ti_sysbios_heaps_HeapBuf_Module__loggerFxn0)NULL);

/* Module__loggerFxn1__C */
#pragma DATA_SECTION(ti_sysbios_heaps_HeapBuf_Module__loggerFxn1__C, ".const:ti_sysbios_heaps_HeapBuf_Module__loggerFxn1__C");
__FAR__ const CT__ti_sysbios_heaps_HeapBuf_Module__loggerFxn1 ti_sysbios_heaps_HeapBuf_Module__loggerFxn1__C = ((const CT__ti_sysbios_heaps_HeapBuf_Module__loggerFxn1)NULL);

/* Module__loggerFxn2__C */
#pragma DATA_SECTION(ti_sysbios_heaps_HeapBuf_Module__loggerFxn2__C, ".const:ti_sysbios_heaps_HeapBuf_Module__loggerFxn2__C");
__FAR__ const CT__ti_sysbios_heaps_HeapBuf_Module__loggerFxn2 ti_sysbios_heaps_HeapBuf_Module__loggerFxn2__C = ((const CT__ti_sysbios_heaps_HeapBuf_Module__loggerFxn2)NULL);

/* Module__loggerFxn4__C */
#pragma DATA_SECTION(ti_sysbios_heaps_HeapBuf_Module__loggerFxn4__C, ".const:ti_sysbios_heaps_HeapBuf_Module__loggerFxn4__C");
__FAR__ const CT__ti_sysbios_heaps_HeapBuf_Module__loggerFxn4 ti_sysbios_heaps_HeapBuf_Module__loggerFxn4__C = ((const CT__ti_sysbios_heaps_HeapBuf_Module__loggerFxn4)NULL);

/* Module__loggerFxn8__C */
#pragma DATA_SECTION(ti_sysbios_heaps_HeapBuf_Module__loggerFxn8__C, ".const:ti_sysbios_heaps_HeapBuf_Module__loggerFxn8__C");
__FAR__ const CT__ti_sysbios_heaps_HeapBuf_Module__loggerFxn8 ti_sysbios_heaps_HeapBuf_Module__loggerFxn8__C = ((const CT__ti_sysbios_heaps_HeapBuf_Module__loggerFxn8)NULL);

/* Object__count__C */
#pragma DATA_SECTION(ti_sysbios_heaps_HeapBuf_Object__count__C, ".const:ti_sysbios_heaps_HeapBuf_Object__count__C");
__FAR__ const CT__ti_sysbios_heaps_HeapBuf_Object__count ti_sysbios_heaps_HeapBuf_Object__count__C = 0;

/* Object__heap__C */
#pragma DATA_SECTION(ti_sysbios_heaps_HeapBuf_Object__heap__C, ".const:ti_sysbios_heaps_HeapBuf_Object__heap__C");
__FAR__ const CT__ti_sysbios_heaps_HeapBuf_Object__heap ti_sysbios_heaps_HeapBuf_Object__heap__C = NULL;

/* Object__sizeof__C */
#pragma DATA_SECTION(ti_sysbios_heaps_HeapBuf_Object__sizeof__C, ".const:ti_sysbios_heaps_HeapBuf_Object__sizeof__C");
__FAR__ const CT__ti_sysbios_heaps_HeapBuf_Object__sizeof ti_sysbios_heaps_HeapBuf_Object__sizeof__C = sizeof(ti_sysbios_heaps_HeapBuf_Object__);

/* Object__table__C */
#pragma DATA_SECTION(ti_sysbios_heaps_HeapBuf_Object__table__C, ".const:ti_sysbios_heaps_HeapBuf_Object__table__C");
__FAR__ const CT__ti_sysbios_heaps_HeapBuf_Object__table ti_sysbios_heaps_HeapBuf_Object__table__C = NULL;

/* A_nullBuf__C */
#pragma DATA_SECTION(ti_sysbios_heaps_HeapBuf_A_nullBuf__C, ".const:ti_sysbios_heaps_HeapBuf_A_nullBuf__C");
__FAR__ const CT__ti_sysbios_heaps_HeapBuf_A_nullBuf ti_sysbios_heaps_HeapBuf_A_nullBuf__C = (((xdc_runtime_Assert_Id)2319) << 16 | 16);

/* A_bufAlign__C */
#pragma DATA_SECTION(ti_sysbios_heaps_HeapBuf_A_bufAlign__C, ".const:ti_sysbios_heaps_HeapBuf_A_bufAlign__C");
__FAR__ const CT__ti_sysbios_heaps_HeapBuf_A_bufAlign ti_sysbios_heaps_HeapBuf_A_bufAlign__C = (((xdc_runtime_Assert_Id)2348) << 16 | 16);

/* A_invalidAlign__C */
#pragma DATA_SECTION(ti_sysbios_heaps_HeapBuf_A_invalidAlign__C, ".const:ti_sysbios_heaps_HeapBuf_A_invalidAlign__C");
__FAR__ const CT__ti_sysbios_heaps_HeapBuf_A_invalidAlign ti_sysbios_heaps_HeapBuf_A_invalidAlign__C = (((xdc_runtime_Assert_Id)2373) << 16 | 16);

/* A_invalidRequestedAlign__C */
#pragma DATA_SECTION(ti_sysbios_heaps_HeapBuf_A_invalidRequestedAlign__C, ".const:ti_sysbios_heaps_HeapBuf_A_invalidRequestedAlign__C");
__FAR__ const CT__ti_sysbios_heaps_HeapBuf_A_invalidRequestedAlign ti_sysbios_heaps_HeapBuf_A_invalidRequestedAlign__C = (((xdc_runtime_Assert_Id)2463) << 16 | 16);

/* A_invalidBlockSize__C */
#pragma DATA_SECTION(ti_sysbios_heaps_HeapBuf_A_invalidBlockSize__C, ".const:ti_sysbios_heaps_HeapBuf_A_invalidBlockSize__C");
__FAR__ const CT__ti_sysbios_heaps_HeapBuf_A_invalidBlockSize ti_sysbios_heaps_HeapBuf_A_invalidBlockSize__C = (((xdc_runtime_Assert_Id)2552) << 16 | 16);

/* A_zeroBlocks__C */
#pragma DATA_SECTION(ti_sysbios_heaps_HeapBuf_A_zeroBlocks__C, ".const:ti_sysbios_heaps_HeapBuf_A_zeroBlocks__C");
__FAR__ const CT__ti_sysbios_heaps_HeapBuf_A_zeroBlocks ti_sysbios_heaps_HeapBuf_A_zeroBlocks__C = (((xdc_runtime_Assert_Id)2612) << 16 | 16);

/* A_zeroBufSize__C */
#pragma DATA_SECTION(ti_sysbios_heaps_HeapBuf_A_zeroBufSize__C, ".const:ti_sysbios_heaps_HeapBuf_A_zeroBufSize__C");
__FAR__ const CT__ti_sysbios_heaps_HeapBuf_A_zeroBufSize ti_sysbios_heaps_HeapBuf_A_zeroBufSize__C = (((xdc_runtime_Assert_Id)2637) << 16 | 16);

/* A_invalidBufSize__C */
#pragma DATA_SECTION(ti_sysbios_heaps_HeapBuf_A_invalidBufSize__C, ".const:ti_sysbios_heaps_HeapBuf_A_invalidBufSize__C");
__FAR__ const CT__ti_sysbios_heaps_HeapBuf_A_invalidBufSize ti_sysbios_heaps_HeapBuf_A_invalidBufSize__C = (((xdc_runtime_Assert_Id)2660) << 16 | 16);

/* A_noBlocksToFree__C */
#pragma DATA_SECTION(ti_sysbios_heaps_HeapBuf_A_noBlocksToFree__C, ".const:ti_sysbios_heaps_HeapBuf_A_noBlocksToFree__C");
__FAR__ const CT__ti_sysbios_heaps_HeapBuf_A_noBlocksToFree ti_sysbios_heaps_HeapBuf_A_noBlocksToFree__C = (((xdc_runtime_Assert_Id)2718) << 16 | 16);

/* A_invalidFree__C */
#pragma DATA_SECTION(ti_sysbios_heaps_HeapBuf_A_invalidFree__C, ".const:ti_sysbios_heaps_HeapBuf_A_invalidFree__C");
__FAR__ const CT__ti_sysbios_heaps_HeapBuf_A_invalidFree ti_sysbios_heaps_HeapBuf_A_invalidFree__C = (((xdc_runtime_Assert_Id)2778) << 16 | 16);

/* E_size__C */
#pragma DATA_SECTION(ti_sysbios_heaps_HeapBuf_E_size__C, ".const:ti_sysbios_heaps_HeapBuf_E_size__C");
__FAR__ const CT__ti_sysbios_heaps_HeapBuf_E_size ti_sysbios_heaps_HeapBuf_E_size__C = (((xdc_runtime_Error_Id)5053) << 16 | 0U);

/* trackMaxAllocs__C */
#pragma DATA_SECTION(ti_sysbios_heaps_HeapBuf_trackMaxAllocs__C, ".const:ti_sysbios_heaps_HeapBuf_trackMaxAllocs__C");
__FAR__ const CT__ti_sysbios_heaps_HeapBuf_trackMaxAllocs ti_sysbios_heaps_HeapBuf_trackMaxAllocs__C = 0;

/* numConstructedHeaps__C */
#pragma DATA_SECTION(ti_sysbios_heaps_HeapBuf_numConstructedHeaps__C, ".const:ti_sysbios_heaps_HeapBuf_numConstructedHeaps__C");
__FAR__ const CT__ti_sysbios_heaps_HeapBuf_numConstructedHeaps ti_sysbios_heaps_HeapBuf_numConstructedHeaps__C = (xdc_UInt)0x0U;


/*
 * ======== ti.sysbios.heaps.HeapMem INITIALIZERS ========
 */

/* Object__DESC__C */
__FAR__ const xdc_runtime_Core_ObjDesc ti_sysbios_heaps_HeapMem_Object__DESC__C;

/* Object__PARAMS__C */
#pragma DATA_SECTION(ti_sysbios_heaps_HeapMem_Object__PARAMS__C, ".const:ti_sysbios_heaps_HeapMem_Object__PARAMS__C");
__FAR__ const ti_sysbios_heaps_HeapMem_Params ti_sysbios_heaps_HeapMem_Object__PARAMS__C = {
    sizeof (ti_sysbios_heaps_HeapMem_Params), /* __size */
    NULL, /* __self */
    NULL, /* __fxns */
    (xdc_runtime_IInstance_Params*)&ti_sysbios_heaps_HeapMem_Object__PARAMS__C.__iprms, /* instance */
    (xdc_SizeT)0x0,  /* minBlockAlign */
    ((xdc_Ptr)(0x0)),  /* buf */
    ((xdc_UArg)(0x0)),  /* size */
    {
        sizeof (xdc_runtime_IInstance_Params), /* __size */
        0,  /* name */
    },  /* instance */
};

/* --> ti_sysbios_heaps_HeapMem_Instance_State_0_buf__A */
__T1_ti_sysbios_heaps_HeapMem_Instance_State__buf ti_sysbios_heaps_HeapMem_Instance_State_0_buf__A[503316480];

/* Module__root__V */
ti_sysbios_heaps_HeapMem_Module__ ti_sysbios_heaps_HeapMem_Module__root__V = {
    {&ti_sysbios_heaps_HeapMem_Module__root__V.link,  /* link.next */
    &ti_sysbios_heaps_HeapMem_Module__root__V.link},  /* link.prev */
};

/* Object__table__V */
ti_sysbios_heaps_HeapMem_Object__ ti_sysbios_heaps_HeapMem_Object__table__V[1] = {
    {/* instance#0 */
        &ti_sysbios_heaps_HeapMem_Module__FXNS__C,
        ((xdc_UArg)(0x10)),  /* align */
        ((void*)ti_sysbios_heaps_HeapMem_Instance_State_0_buf__A),  /* buf */
        {
            ((ti_sysbios_heaps_HeapMem_Header*)NULL),  /* next */
            ((xdc_UArg)(0x1e000000)),  /* size */
        },  /* head */
        (xdc_SizeT)0x10,  /* minBlockAlign */
    },
};

/* Module__diagsEnabled__C */
#pragma DATA_SECTION(ti_sysbios_heaps_HeapMem_Module__diagsEnabled__C, ".const:ti_sysbios_heaps_HeapMem_Module__diagsEnabled__C");
__FAR__ const CT__ti_sysbios_heaps_HeapMem_Module__diagsEnabled ti_sysbios_heaps_HeapMem_Module__diagsEnabled__C = (xdc_Bits32)0x90U;

/* Module__diagsIncluded__C */
#pragma DATA_SECTION(ti_sysbios_heaps_HeapMem_Module__diagsIncluded__C, ".const:ti_sysbios_heaps_HeapMem_Module__diagsIncluded__C");
__FAR__ const CT__ti_sysbios_heaps_HeapMem_Module__diagsIncluded ti_sysbios_heaps_HeapMem_Module__diagsIncluded__C = (xdc_Bits32)0x90U;

/* Module__diagsMask__C */
#pragma DATA_SECTION(ti_sysbios_heaps_HeapMem_Module__diagsMask__C, ".const:ti_sysbios_heaps_HeapMem_Module__diagsMask__C");
__FAR__ const CT__ti_sysbios_heaps_HeapMem_Module__diagsMask ti_sysbios_heaps_HeapMem_Module__diagsMask__C = ((const CT__ti_sysbios_heaps_HeapMem_Module__diagsMask)NULL);

/* Module__gateObj__C */
#pragma DATA_SECTION(ti_sysbios_heaps_HeapMem_Module__gateObj__C, ".const:ti_sysbios_heaps_HeapMem_Module__gateObj__C");
__FAR__ const CT__ti_sysbios_heaps_HeapMem_Module__gateObj ti_sysbios_heaps_HeapMem_Module__gateObj__C = ((const CT__ti_sysbios_heaps_HeapMem_Module__gateObj)((void*)(xdc_runtime_IGateProvider_Handle)&ti_sysbios_gates_GateMutex_Object__table__V[0]));

/* Module__gatePrms__C */
#pragma DATA_SECTION(ti_sysbios_heaps_HeapMem_Module__gatePrms__C, ".const:ti_sysbios_heaps_HeapMem_Module__gatePrms__C");
__FAR__ const CT__ti_sysbios_heaps_HeapMem_Module__gatePrms ti_sysbios_heaps_HeapMem_Module__gatePrms__C = ((const CT__ti_sysbios_heaps_HeapMem_Module__gatePrms)NULL);

/* Module__id__C */
#pragma DATA_SECTION(ti_sysbios_heaps_HeapMem_Module__id__C, ".const:ti_sysbios_heaps_HeapMem_Module__id__C");
__FAR__ const CT__ti_sysbios_heaps_HeapMem_Module__id ti_sysbios_heaps_HeapMem_Module__id__C = (xdc_Bits16)0x8032U;

/* Module__loggerDefined__C */
#pragma DATA_SECTION(ti_sysbios_heaps_HeapMem_Module__loggerDefined__C, ".const:ti_sysbios_heaps_HeapMem_Module__loggerDefined__C");
__FAR__ const CT__ti_sysbios_heaps_HeapMem_Module__loggerDefined ti_sysbios_heaps_HeapMem_Module__loggerDefined__C = 0;

/* Module__loggerObj__C */
#pragma DATA_SECTION(ti_sysbios_heaps_HeapMem_Module__loggerObj__C, ".const:ti_sysbios_heaps_HeapMem_Module__loggerObj__C");
__FAR__ const CT__ti_sysbios_heaps_HeapMem_Module__loggerObj ti_sysbios_heaps_HeapMem_Module__loggerObj__C = ((const CT__ti_sysbios_heaps_HeapMem_Module__loggerObj)NULL);

/* Module__loggerFxn0__C */
#pragma DATA_SECTION(ti_sysbios_heaps_HeapMem_Module__loggerFxn0__C, ".const:ti_sysbios_heaps_HeapMem_Module__loggerFxn0__C");
__FAR__ const CT__ti_sysbios_heaps_HeapMem_Module__loggerFxn0 ti_sysbios_heaps_HeapMem_Module__loggerFxn0__C = ((const CT__ti_sysbios_heaps_HeapMem_Module__loggerFxn0)NULL);

/* Module__loggerFxn1__C */
#pragma DATA_SECTION(ti_sysbios_heaps_HeapMem_Module__loggerFxn1__C, ".const:ti_sysbios_heaps_HeapMem_Module__loggerFxn1__C");
__FAR__ const CT__ti_sysbios_heaps_HeapMem_Module__loggerFxn1 ti_sysbios_heaps_HeapMem_Module__loggerFxn1__C = ((const CT__ti_sysbios_heaps_HeapMem_Module__loggerFxn1)NULL);

/* Module__loggerFxn2__C */
#pragma DATA_SECTION(ti_sysbios_heaps_HeapMem_Module__loggerFxn2__C, ".const:ti_sysbios_heaps_HeapMem_Module__loggerFxn2__C");
__FAR__ const CT__ti_sysbios_heaps_HeapMem_Module__loggerFxn2 ti_sysbios_heaps_HeapMem_Module__loggerFxn2__C = ((const CT__ti_sysbios_heaps_HeapMem_Module__loggerFxn2)NULL);

/* Module__loggerFxn4__C */
#pragma DATA_SECTION(ti_sysbios_heaps_HeapMem_Module__loggerFxn4__C, ".const:ti_sysbios_heaps_HeapMem_Module__loggerFxn4__C");
__FAR__ const CT__ti_sysbios_heaps_HeapMem_Module__loggerFxn4 ti_sysbios_heaps_HeapMem_Module__loggerFxn4__C = ((const CT__ti_sysbios_heaps_HeapMem_Module__loggerFxn4)NULL);

/* Module__loggerFxn8__C */
#pragma DATA_SECTION(ti_sysbios_heaps_HeapMem_Module__loggerFxn8__C, ".const:ti_sysbios_heaps_HeapMem_Module__loggerFxn8__C");
__FAR__ const CT__ti_sysbios_heaps_HeapMem_Module__loggerFxn8 ti_sysbios_heaps_HeapMem_Module__loggerFxn8__C = ((const CT__ti_sysbios_heaps_HeapMem_Module__loggerFxn8)NULL);

/* Object__count__C */
#pragma DATA_SECTION(ti_sysbios_heaps_HeapMem_Object__count__C, ".const:ti_sysbios_heaps_HeapMem_Object__count__C");
__FAR__ const CT__ti_sysbios_heaps_HeapMem_Object__count ti_sysbios_heaps_HeapMem_Object__count__C = 1;

/* Object__heap__C */
#pragma DATA_SECTION(ti_sysbios_heaps_HeapMem_Object__heap__C, ".const:ti_sysbios_heaps_HeapMem_Object__heap__C");
__FAR__ const CT__ti_sysbios_heaps_HeapMem_Object__heap ti_sysbios_heaps_HeapMem_Object__heap__C = NULL;

/* Object__sizeof__C */
#pragma DATA_SECTION(ti_sysbios_heaps_HeapMem_Object__sizeof__C, ".const:ti_sysbios_heaps_HeapMem_Object__sizeof__C");
__FAR__ const CT__ti_sysbios_heaps_HeapMem_Object__sizeof ti_sysbios_heaps_HeapMem_Object__sizeof__C = sizeof(ti_sysbios_heaps_HeapMem_Object__);

/* Object__table__C */
#pragma DATA_SECTION(ti_sysbios_heaps_HeapMem_Object__table__C, ".const:ti_sysbios_heaps_HeapMem_Object__table__C");
__FAR__ const CT__ti_sysbios_heaps_HeapMem_Object__table ti_sysbios_heaps_HeapMem_Object__table__C = ti_sysbios_heaps_HeapMem_Object__table__V;

/* A_zeroBlock__C */
#pragma DATA_SECTION(ti_sysbios_heaps_HeapMem_A_zeroBlock__C, ".const:ti_sysbios_heaps_HeapMem_A_zeroBlock__C");
__FAR__ const CT__ti_sysbios_heaps_HeapMem_A_zeroBlock ti_sysbios_heaps_HeapMem_A_zeroBlock__C = (((xdc_runtime_Assert_Id)2806) << 16 | 16);

/* A_heapSize__C */
#pragma DATA_SECTION(ti_sysbios_heaps_HeapMem_A_heapSize__C, ".const:ti_sysbios_heaps_HeapMem_A_heapSize__C");
__FAR__ const CT__ti_sysbios_heaps_HeapMem_A_heapSize ti_sysbios_heaps_HeapMem_A_heapSize__C = (((xdc_runtime_Assert_Id)2842) << 16 | 16);

/* A_align__C */
#pragma DATA_SECTION(ti_sysbios_heaps_HeapMem_A_align__C, ".const:ti_sysbios_heaps_HeapMem_A_align__C");
__FAR__ const CT__ti_sysbios_heaps_HeapMem_A_align ti_sysbios_heaps_HeapMem_A_align__C = (((xdc_runtime_Assert_Id)2887) << 16 | 16);

/* E_memory__C */
#pragma DATA_SECTION(ti_sysbios_heaps_HeapMem_E_memory__C, ".const:ti_sysbios_heaps_HeapMem_E_memory__C");
__FAR__ const CT__ti_sysbios_heaps_HeapMem_E_memory ti_sysbios_heaps_HeapMem_E_memory__C = (((xdc_runtime_Error_Id)5101) << 16 | 0U);

/* A_invalidFree__C */
#pragma DATA_SECTION(ti_sysbios_heaps_HeapMem_A_invalidFree__C, ".const:ti_sysbios_heaps_HeapMem_A_invalidFree__C");
__FAR__ const CT__ti_sysbios_heaps_HeapMem_A_invalidFree ti_sysbios_heaps_HeapMem_A_invalidFree__C = (((xdc_runtime_Assert_Id)2778) << 16 | 16);

/* primaryHeapBaseAddr__C */
#pragma DATA_SECTION(ti_sysbios_heaps_HeapMem_primaryHeapBaseAddr__C, ".const:ti_sysbios_heaps_HeapMem_primaryHeapBaseAddr__C");
__FAR__ const CT__ti_sysbios_heaps_HeapMem_primaryHeapBaseAddr ti_sysbios_heaps_HeapMem_primaryHeapBaseAddr__C = ((const CT__ti_sysbios_heaps_HeapMem_primaryHeapBaseAddr)NULL);

/* primaryHeapEndAddr__C */
#pragma DATA_SECTION(ti_sysbios_heaps_HeapMem_primaryHeapEndAddr__C, ".const:ti_sysbios_heaps_HeapMem_primaryHeapEndAddr__C");
__FAR__ const CT__ti_sysbios_heaps_HeapMem_primaryHeapEndAddr ti_sysbios_heaps_HeapMem_primaryHeapEndAddr__C = ((const CT__ti_sysbios_heaps_HeapMem_primaryHeapEndAddr)NULL);

/* reqAlign__C */
#pragma DATA_SECTION(ti_sysbios_heaps_HeapMem_reqAlign__C, ".const:ti_sysbios_heaps_HeapMem_reqAlign__C");
__FAR__ const CT__ti_sysbios_heaps_HeapMem_reqAlign ti_sysbios_heaps_HeapMem_reqAlign__C = (xdc_SizeT)0x10;


/*
 * ======== ti.sysbios.heaps.HeapMem_Module_GateProxy INITIALIZERS ========
 */


/*
 * ======== ti.sysbios.knl.Clock INITIALIZERS ========
 */

/* Object__DESC__C */
__FAR__ const xdc_runtime_Core_ObjDesc ti_sysbios_knl_Clock_Object__DESC__C;

/* Object__PARAMS__C */
#pragma DATA_SECTION(ti_sysbios_knl_Clock_Object__PARAMS__C, ".const:ti_sysbios_knl_Clock_Object__PARAMS__C");
__FAR__ const ti_sysbios_knl_Clock_Params ti_sysbios_knl_Clock_Object__PARAMS__C = {
    sizeof (ti_sysbios_knl_Clock_Params), /* __size */
    NULL, /* __self */
    NULL, /* __fxns */
    (xdc_runtime_IInstance_Params*)&ti_sysbios_knl_Clock_Object__PARAMS__C.__iprms, /* instance */
    0,  /* startFlag */
    (xdc_UInt32)0x0U,  /* period */
    ((xdc_UArg)NULL),  /* arg */
    {
        sizeof (xdc_runtime_IInstance_Params), /* __size */
        0,  /* name */
    },  /* instance */
};

/* Module__root__V */
ti_sysbios_knl_Clock_Module__ ti_sysbios_knl_Clock_Module__root__V = {
    {&ti_sysbios_knl_Clock_Module__root__V.link,  /* link.next */
    &ti_sysbios_knl_Clock_Module__root__V.link},  /* link.prev */
};

/* Module__state__V */
#ifdef __ti__
ti_sysbios_knl_Clock_Module_State__ ti_sysbios_knl_Clock_Module__state__V __attribute__ ((section(".data:ti_sysbios_knl_Clock_Module__state__V")));
#elif !(defined(__MACH__) && defined(__APPLE__))
ti_sysbios_knl_Clock_Module_State__ ti_sysbios_knl_Clock_Module__state__V __attribute__ ((section(".data_ti_sysbios_knl_Clock_Module__state__V")));
#endif
ti_sysbios_knl_Clock_Module_State__ ti_sysbios_knl_Clock_Module__state__V = {
    (xdc_UInt32)0x0U,  /* ticks */
    (xdc_UInt)0x0U,  /* swiCount */
    (ti_sysbios_knl_Clock_TimerProxy_Handle)&ti_sysbios_timers_dmtimer_Timer_Object__table__V[0],  /* timer */
    (ti_sysbios_knl_Swi_Handle)&ti_sysbios_knl_Swi_Object__table__V[0],  /* swi */
    (xdc_UInt)0x1U,  /* numTickSkip */
    (xdc_UInt32)0x1U,  /* nextScheduledTick */
    (xdc_UInt32)0x0U,  /* maxSkippable */
    0,  /* inWorkFunc */
    0,  /* startDuringWorkFunc */
    0,  /* ticking */
    {
        {
            ((ti_sysbios_knl_Queue_Elem*)((void*)&ti_sysbios_knl_Clock_Module__state__V.Object_field_clockQ.elem)),  /* next */
            ((ti_sysbios_knl_Queue_Elem*)((void*)&ti_sysbios_knl_Clock_Module__state__V.Object_field_clockQ.elem)),  /* prev */
        },  /* elem */
    },  /* Object_field_clockQ */
};

/* Module__diagsEnabled__C */
#pragma DATA_SECTION(ti_sysbios_knl_Clock_Module__diagsEnabled__C, ".const:ti_sysbios_knl_Clock_Module__diagsEnabled__C");
__FAR__ const CT__ti_sysbios_knl_Clock_Module__diagsEnabled ti_sysbios_knl_Clock_Module__diagsEnabled__C = (xdc_Bits32)0x90U;

/* Module__diagsIncluded__C */
#pragma DATA_SECTION(ti_sysbios_knl_Clock_Module__diagsIncluded__C, ".const:ti_sysbios_knl_Clock_Module__diagsIncluded__C");
__FAR__ const CT__ti_sysbios_knl_Clock_Module__diagsIncluded ti_sysbios_knl_Clock_Module__diagsIncluded__C = (xdc_Bits32)0x90U;

/* Module__diagsMask__C */
#pragma DATA_SECTION(ti_sysbios_knl_Clock_Module__diagsMask__C, ".const:ti_sysbios_knl_Clock_Module__diagsMask__C");
__FAR__ const CT__ti_sysbios_knl_Clock_Module__diagsMask ti_sysbios_knl_Clock_Module__diagsMask__C = ((const CT__ti_sysbios_knl_Clock_Module__diagsMask)NULL);

/* Module__gateObj__C */
#pragma DATA_SECTION(ti_sysbios_knl_Clock_Module__gateObj__C, ".const:ti_sysbios_knl_Clock_Module__gateObj__C");
__FAR__ const CT__ti_sysbios_knl_Clock_Module__gateObj ti_sysbios_knl_Clock_Module__gateObj__C = ((const CT__ti_sysbios_knl_Clock_Module__gateObj)NULL);

/* Module__gatePrms__C */
#pragma DATA_SECTION(ti_sysbios_knl_Clock_Module__gatePrms__C, ".const:ti_sysbios_knl_Clock_Module__gatePrms__C");
__FAR__ const CT__ti_sysbios_knl_Clock_Module__gatePrms ti_sysbios_knl_Clock_Module__gatePrms__C = ((const CT__ti_sysbios_knl_Clock_Module__gatePrms)NULL);

/* Module__id__C */
#pragma DATA_SECTION(ti_sysbios_knl_Clock_Module__id__C, ".const:ti_sysbios_knl_Clock_Module__id__C");
__FAR__ const CT__ti_sysbios_knl_Clock_Module__id ti_sysbios_knl_Clock_Module__id__C = (xdc_Bits16)0x8020U;

/* Module__loggerDefined__C */
#pragma DATA_SECTION(ti_sysbios_knl_Clock_Module__loggerDefined__C, ".const:ti_sysbios_knl_Clock_Module__loggerDefined__C");
__FAR__ const CT__ti_sysbios_knl_Clock_Module__loggerDefined ti_sysbios_knl_Clock_Module__loggerDefined__C = 0;

/* Module__loggerObj__C */
#pragma DATA_SECTION(ti_sysbios_knl_Clock_Module__loggerObj__C, ".const:ti_sysbios_knl_Clock_Module__loggerObj__C");
__FAR__ const CT__ti_sysbios_knl_Clock_Module__loggerObj ti_sysbios_knl_Clock_Module__loggerObj__C = ((const CT__ti_sysbios_knl_Clock_Module__loggerObj)NULL);

/* Module__loggerFxn0__C */
#pragma DATA_SECTION(ti_sysbios_knl_Clock_Module__loggerFxn0__C, ".const:ti_sysbios_knl_Clock_Module__loggerFxn0__C");
__FAR__ const CT__ti_sysbios_knl_Clock_Module__loggerFxn0 ti_sysbios_knl_Clock_Module__loggerFxn0__C = ((const CT__ti_sysbios_knl_Clock_Module__loggerFxn0)NULL);

/* Module__loggerFxn1__C */
#pragma DATA_SECTION(ti_sysbios_knl_Clock_Module__loggerFxn1__C, ".const:ti_sysbios_knl_Clock_Module__loggerFxn1__C");
__FAR__ const CT__ti_sysbios_knl_Clock_Module__loggerFxn1 ti_sysbios_knl_Clock_Module__loggerFxn1__C = ((const CT__ti_sysbios_knl_Clock_Module__loggerFxn1)NULL);

/* Module__loggerFxn2__C */
#pragma DATA_SECTION(ti_sysbios_knl_Clock_Module__loggerFxn2__C, ".const:ti_sysbios_knl_Clock_Module__loggerFxn2__C");
__FAR__ const CT__ti_sysbios_knl_Clock_Module__loggerFxn2 ti_sysbios_knl_Clock_Module__loggerFxn2__C = ((const CT__ti_sysbios_knl_Clock_Module__loggerFxn2)NULL);

/* Module__loggerFxn4__C */
#pragma DATA_SECTION(ti_sysbios_knl_Clock_Module__loggerFxn4__C, ".const:ti_sysbios_knl_Clock_Module__loggerFxn4__C");
__FAR__ const CT__ti_sysbios_knl_Clock_Module__loggerFxn4 ti_sysbios_knl_Clock_Module__loggerFxn4__C = ((const CT__ti_sysbios_knl_Clock_Module__loggerFxn4)NULL);

/* Module__loggerFxn8__C */
#pragma DATA_SECTION(ti_sysbios_knl_Clock_Module__loggerFxn8__C, ".const:ti_sysbios_knl_Clock_Module__loggerFxn8__C");
__FAR__ const CT__ti_sysbios_knl_Clock_Module__loggerFxn8 ti_sysbios_knl_Clock_Module__loggerFxn8__C = ((const CT__ti_sysbios_knl_Clock_Module__loggerFxn8)NULL);

/* Object__count__C */
#pragma DATA_SECTION(ti_sysbios_knl_Clock_Object__count__C, ".const:ti_sysbios_knl_Clock_Object__count__C");
__FAR__ const CT__ti_sysbios_knl_Clock_Object__count ti_sysbios_knl_Clock_Object__count__C = 0;

/* Object__heap__C */
#pragma DATA_SECTION(ti_sysbios_knl_Clock_Object__heap__C, ".const:ti_sysbios_knl_Clock_Object__heap__C");
__FAR__ const CT__ti_sysbios_knl_Clock_Object__heap ti_sysbios_knl_Clock_Object__heap__C = NULL;

/* Object__sizeof__C */
#pragma DATA_SECTION(ti_sysbios_knl_Clock_Object__sizeof__C, ".const:ti_sysbios_knl_Clock_Object__sizeof__C");
__FAR__ const CT__ti_sysbios_knl_Clock_Object__sizeof ti_sysbios_knl_Clock_Object__sizeof__C = sizeof(ti_sysbios_knl_Clock_Object__);

/* Object__table__C */
#pragma DATA_SECTION(ti_sysbios_knl_Clock_Object__table__C, ".const:ti_sysbios_knl_Clock_Object__table__C");
__FAR__ const CT__ti_sysbios_knl_Clock_Object__table ti_sysbios_knl_Clock_Object__table__C = NULL;

/* LW_delayed__C */
#pragma DATA_SECTION(ti_sysbios_knl_Clock_LW_delayed__C, ".const:ti_sysbios_knl_Clock_LW_delayed__C");
__FAR__ const CT__ti_sysbios_knl_Clock_LW_delayed ti_sysbios_knl_Clock_LW_delayed__C = (((xdc_runtime_Log_Event)5859) << 16 | 1024);

/* LM_tick__C */
#pragma DATA_SECTION(ti_sysbios_knl_Clock_LM_tick__C, ".const:ti_sysbios_knl_Clock_LM_tick__C");
__FAR__ const CT__ti_sysbios_knl_Clock_LM_tick ti_sysbios_knl_Clock_LM_tick__C = (((xdc_runtime_Log_Event)5881) << 16 | 768);

/* LM_begin__C */
#pragma DATA_SECTION(ti_sysbios_knl_Clock_LM_begin__C, ".const:ti_sysbios_knl_Clock_LM_begin__C");
__FAR__ const CT__ti_sysbios_knl_Clock_LM_begin ti_sysbios_knl_Clock_LM_begin__C = (((xdc_runtime_Log_Event)5899) << 16 | 768);

/* A_clockDisabled__C */
#pragma DATA_SECTION(ti_sysbios_knl_Clock_A_clockDisabled__C, ".const:ti_sysbios_knl_Clock_A_clockDisabled__C");
__FAR__ const CT__ti_sysbios_knl_Clock_A_clockDisabled ti_sysbios_knl_Clock_A_clockDisabled__C = (((xdc_runtime_Assert_Id)790) << 16 | 16);

/* A_badThreadType__C */
#pragma DATA_SECTION(ti_sysbios_knl_Clock_A_badThreadType__C, ".const:ti_sysbios_knl_Clock_A_badThreadType__C");
__FAR__ const CT__ti_sysbios_knl_Clock_A_badThreadType ti_sysbios_knl_Clock_A_badThreadType__C = (((xdc_runtime_Assert_Id)871) << 16 | 16);

/* serviceMargin__C */
#pragma DATA_SECTION(ti_sysbios_knl_Clock_serviceMargin__C, ".const:ti_sysbios_knl_Clock_serviceMargin__C");
__FAR__ const CT__ti_sysbios_knl_Clock_serviceMargin ti_sysbios_knl_Clock_serviceMargin__C = (xdc_UInt32)0x0U;

/* tickSource__C */
#pragma DATA_SECTION(ti_sysbios_knl_Clock_tickSource__C, ".const:ti_sysbios_knl_Clock_tickSource__C");
__FAR__ const CT__ti_sysbios_knl_Clock_tickSource ti_sysbios_knl_Clock_tickSource__C = ti_sysbios_knl_Clock_TickSource_TIMER;

/* tickMode__C */
#pragma DATA_SECTION(ti_sysbios_knl_Clock_tickMode__C, ".const:ti_sysbios_knl_Clock_tickMode__C");
__FAR__ const CT__ti_sysbios_knl_Clock_tickMode ti_sysbios_knl_Clock_tickMode__C = ti_sysbios_knl_Clock_TickMode_PERIODIC;

/* timerId__C */
#pragma DATA_SECTION(ti_sysbios_knl_Clock_timerId__C, ".const:ti_sysbios_knl_Clock_timerId__C");
__FAR__ const CT__ti_sysbios_knl_Clock_timerId ti_sysbios_knl_Clock_timerId__C = (xdc_UInt)0x0U;

/* tickPeriod__C */
#pragma DATA_SECTION(ti_sysbios_knl_Clock_tickPeriod__C, ".const:ti_sysbios_knl_Clock_tickPeriod__C");
__FAR__ const CT__ti_sysbios_knl_Clock_tickPeriod ti_sysbios_knl_Clock_tickPeriod__C = (xdc_UInt32)0x3e8U;

/* doTickFunc__C */
#pragma DATA_SECTION(ti_sysbios_knl_Clock_doTickFunc__C, ".const:ti_sysbios_knl_Clock_doTickFunc__C");
__FAR__ const CT__ti_sysbios_knl_Clock_doTickFunc ti_sysbios_knl_Clock_doTickFunc__C = ((const CT__ti_sysbios_knl_Clock_doTickFunc)(ti_sysbios_knl_Clock_doTick__I));

/* triggerClock__C */
#pragma DATA_SECTION(ti_sysbios_knl_Clock_triggerClock__C, ".const:ti_sysbios_knl_Clock_triggerClock__C");
__FAR__ const CT__ti_sysbios_knl_Clock_triggerClock ti_sysbios_knl_Clock_triggerClock__C = 0;


/*
 * ======== ti.sysbios.knl.Clock_TimerProxy INITIALIZERS ========
 */


/*
 * ======== ti.sysbios.knl.Event INITIALIZERS ========
 */

/* Object__DESC__C */
__FAR__ const xdc_runtime_Core_ObjDesc ti_sysbios_knl_Event_Object__DESC__C;

/* Object__PARAMS__C */
#pragma DATA_SECTION(ti_sysbios_knl_Event_Object__PARAMS__C, ".const:ti_sysbios_knl_Event_Object__PARAMS__C");
__FAR__ const ti_sysbios_knl_Event_Params ti_sysbios_knl_Event_Object__PARAMS__C = {
    sizeof (ti_sysbios_knl_Event_Params), /* __size */
    NULL, /* __self */
    NULL, /* __fxns */
    (xdc_runtime_IInstance_Params*)&ti_sysbios_knl_Event_Object__PARAMS__C.__iprms, /* instance */
    {
        sizeof (xdc_runtime_IInstance_Params), /* __size */
        0,  /* name */
    },  /* instance */
};

/* Module__root__V */
ti_sysbios_knl_Event_Module__ ti_sysbios_knl_Event_Module__root__V = {
    {&ti_sysbios_knl_Event_Module__root__V.link,  /* link.next */
    &ti_sysbios_knl_Event_Module__root__V.link},  /* link.prev */
};

/* Module__diagsEnabled__C */
#pragma DATA_SECTION(ti_sysbios_knl_Event_Module__diagsEnabled__C, ".const:ti_sysbios_knl_Event_Module__diagsEnabled__C");
__FAR__ const CT__ti_sysbios_knl_Event_Module__diagsEnabled ti_sysbios_knl_Event_Module__diagsEnabled__C = (xdc_Bits32)0x90U;

/* Module__diagsIncluded__C */
#pragma DATA_SECTION(ti_sysbios_knl_Event_Module__diagsIncluded__C, ".const:ti_sysbios_knl_Event_Module__diagsIncluded__C");
__FAR__ const CT__ti_sysbios_knl_Event_Module__diagsIncluded ti_sysbios_knl_Event_Module__diagsIncluded__C = (xdc_Bits32)0x90U;

/* Module__diagsMask__C */
#pragma DATA_SECTION(ti_sysbios_knl_Event_Module__diagsMask__C, ".const:ti_sysbios_knl_Event_Module__diagsMask__C");
__FAR__ const CT__ti_sysbios_knl_Event_Module__diagsMask ti_sysbios_knl_Event_Module__diagsMask__C = ((const CT__ti_sysbios_knl_Event_Module__diagsMask)NULL);

/* Module__gateObj__C */
#pragma DATA_SECTION(ti_sysbios_knl_Event_Module__gateObj__C, ".const:ti_sysbios_knl_Event_Module__gateObj__C");
__FAR__ const CT__ti_sysbios_knl_Event_Module__gateObj ti_sysbios_knl_Event_Module__gateObj__C = ((const CT__ti_sysbios_knl_Event_Module__gateObj)NULL);

/* Module__gatePrms__C */
#pragma DATA_SECTION(ti_sysbios_knl_Event_Module__gatePrms__C, ".const:ti_sysbios_knl_Event_Module__gatePrms__C");
__FAR__ const CT__ti_sysbios_knl_Event_Module__gatePrms ti_sysbios_knl_Event_Module__gatePrms__C = ((const CT__ti_sysbios_knl_Event_Module__gatePrms)NULL);

/* Module__id__C */
#pragma DATA_SECTION(ti_sysbios_knl_Event_Module__id__C, ".const:ti_sysbios_knl_Event_Module__id__C");
__FAR__ const CT__ti_sysbios_knl_Event_Module__id ti_sysbios_knl_Event_Module__id__C = (xdc_Bits16)0x8023U;

/* Module__loggerDefined__C */
#pragma DATA_SECTION(ti_sysbios_knl_Event_Module__loggerDefined__C, ".const:ti_sysbios_knl_Event_Module__loggerDefined__C");
__FAR__ const CT__ti_sysbios_knl_Event_Module__loggerDefined ti_sysbios_knl_Event_Module__loggerDefined__C = 0;

/* Module__loggerObj__C */
#pragma DATA_SECTION(ti_sysbios_knl_Event_Module__loggerObj__C, ".const:ti_sysbios_knl_Event_Module__loggerObj__C");
__FAR__ const CT__ti_sysbios_knl_Event_Module__loggerObj ti_sysbios_knl_Event_Module__loggerObj__C = ((const CT__ti_sysbios_knl_Event_Module__loggerObj)NULL);

/* Module__loggerFxn0__C */
#pragma DATA_SECTION(ti_sysbios_knl_Event_Module__loggerFxn0__C, ".const:ti_sysbios_knl_Event_Module__loggerFxn0__C");
__FAR__ const CT__ti_sysbios_knl_Event_Module__loggerFxn0 ti_sysbios_knl_Event_Module__loggerFxn0__C = ((const CT__ti_sysbios_knl_Event_Module__loggerFxn0)NULL);

/* Module__loggerFxn1__C */
#pragma DATA_SECTION(ti_sysbios_knl_Event_Module__loggerFxn1__C, ".const:ti_sysbios_knl_Event_Module__loggerFxn1__C");
__FAR__ const CT__ti_sysbios_knl_Event_Module__loggerFxn1 ti_sysbios_knl_Event_Module__loggerFxn1__C = ((const CT__ti_sysbios_knl_Event_Module__loggerFxn1)NULL);

/* Module__loggerFxn2__C */
#pragma DATA_SECTION(ti_sysbios_knl_Event_Module__loggerFxn2__C, ".const:ti_sysbios_knl_Event_Module__loggerFxn2__C");
__FAR__ const CT__ti_sysbios_knl_Event_Module__loggerFxn2 ti_sysbios_knl_Event_Module__loggerFxn2__C = ((const CT__ti_sysbios_knl_Event_Module__loggerFxn2)NULL);

/* Module__loggerFxn4__C */
#pragma DATA_SECTION(ti_sysbios_knl_Event_Module__loggerFxn4__C, ".const:ti_sysbios_knl_Event_Module__loggerFxn4__C");
__FAR__ const CT__ti_sysbios_knl_Event_Module__loggerFxn4 ti_sysbios_knl_Event_Module__loggerFxn4__C = ((const CT__ti_sysbios_knl_Event_Module__loggerFxn4)NULL);

/* Module__loggerFxn8__C */
#pragma DATA_SECTION(ti_sysbios_knl_Event_Module__loggerFxn8__C, ".const:ti_sysbios_knl_Event_Module__loggerFxn8__C");
__FAR__ const CT__ti_sysbios_knl_Event_Module__loggerFxn8 ti_sysbios_knl_Event_Module__loggerFxn8__C = ((const CT__ti_sysbios_knl_Event_Module__loggerFxn8)NULL);

/* Object__count__C */
#pragma DATA_SECTION(ti_sysbios_knl_Event_Object__count__C, ".const:ti_sysbios_knl_Event_Object__count__C");
__FAR__ const CT__ti_sysbios_knl_Event_Object__count ti_sysbios_knl_Event_Object__count__C = 0;

/* Object__heap__C */
#pragma DATA_SECTION(ti_sysbios_knl_Event_Object__heap__C, ".const:ti_sysbios_knl_Event_Object__heap__C");
__FAR__ const CT__ti_sysbios_knl_Event_Object__heap ti_sysbios_knl_Event_Object__heap__C = NULL;

/* Object__sizeof__C */
#pragma DATA_SECTION(ti_sysbios_knl_Event_Object__sizeof__C, ".const:ti_sysbios_knl_Event_Object__sizeof__C");
__FAR__ const CT__ti_sysbios_knl_Event_Object__sizeof ti_sysbios_knl_Event_Object__sizeof__C = sizeof(ti_sysbios_knl_Event_Object__);

/* Object__table__C */
#pragma DATA_SECTION(ti_sysbios_knl_Event_Object__table__C, ".const:ti_sysbios_knl_Event_Object__table__C");
__FAR__ const CT__ti_sysbios_knl_Event_Object__table ti_sysbios_knl_Event_Object__table__C = NULL;

/* LM_post__C */
#pragma DATA_SECTION(ti_sysbios_knl_Event_LM_post__C, ".const:ti_sysbios_knl_Event_LM_post__C");
__FAR__ const CT__ti_sysbios_knl_Event_LM_post ti_sysbios_knl_Event_LM_post__C = (((xdc_runtime_Log_Event)5931) << 16 | 768);

/* LM_pend__C */
#pragma DATA_SECTION(ti_sysbios_knl_Event_LM_pend__C, ".const:ti_sysbios_knl_Event_LM_pend__C");
__FAR__ const CT__ti_sysbios_knl_Event_LM_pend ti_sysbios_knl_Event_LM_pend__C = (((xdc_runtime_Log_Event)5985) << 16 | 768);

/* A_nullEventMasks__C */
#pragma DATA_SECTION(ti_sysbios_knl_Event_A_nullEventMasks__C, ".const:ti_sysbios_knl_Event_A_nullEventMasks__C");
__FAR__ const CT__ti_sysbios_knl_Event_A_nullEventMasks ti_sysbios_knl_Event_A_nullEventMasks__C = (((xdc_runtime_Assert_Id)941) << 16 | 16);

/* A_nullEventId__C */
#pragma DATA_SECTION(ti_sysbios_knl_Event_A_nullEventId__C, ".const:ti_sysbios_knl_Event_A_nullEventId__C");
__FAR__ const CT__ti_sysbios_knl_Event_A_nullEventId ti_sysbios_knl_Event_A_nullEventId__C = (((xdc_runtime_Assert_Id)988) << 16 | 16);

/* A_eventInUse__C */
#pragma DATA_SECTION(ti_sysbios_knl_Event_A_eventInUse__C, ".const:ti_sysbios_knl_Event_A_eventInUse__C");
__FAR__ const CT__ti_sysbios_knl_Event_A_eventInUse ti_sysbios_knl_Event_A_eventInUse__C = (((xdc_runtime_Assert_Id)1027) << 16 | 16);

/* A_badContext__C */
#pragma DATA_SECTION(ti_sysbios_knl_Event_A_badContext__C, ".const:ti_sysbios_knl_Event_A_badContext__C");
__FAR__ const CT__ti_sysbios_knl_Event_A_badContext ti_sysbios_knl_Event_A_badContext__C = (((xdc_runtime_Assert_Id)1070) << 16 | 16);

/* A_pendTaskDisabled__C */
#pragma DATA_SECTION(ti_sysbios_knl_Event_A_pendTaskDisabled__C, ".const:ti_sysbios_knl_Event_A_pendTaskDisabled__C");
__FAR__ const CT__ti_sysbios_knl_Event_A_pendTaskDisabled ti_sysbios_knl_Event_A_pendTaskDisabled__C = (((xdc_runtime_Assert_Id)1133) << 16 | 16);


/*
 * ======== ti.sysbios.knl.Idle INITIALIZERS ========
 */

/* --> ti_sysbios_knl_Idle_funcList__A */
#pragma DATA_SECTION(ti_sysbios_knl_Idle_funcList__A, ".const:ti_sysbios_knl_Idle_funcList__A");
const __T1_ti_sysbios_knl_Idle_funcList ti_sysbios_knl_Idle_funcList__A[2] = {
    ((xdc_Void(*)(xdc_Void))(tidlIdleLoop)),  /* [0] */
    ((xdc_Void(*)(xdc_Void))(ti_sysbios_utils_Load_idleFxn__E)),  /* [1] */
};

/* --> ti_sysbios_knl_Idle_coreList__A */
#pragma DATA_SECTION(ti_sysbios_knl_Idle_coreList__A, ".const:ti_sysbios_knl_Idle_coreList__A");
const __T1_ti_sysbios_knl_Idle_coreList ti_sysbios_knl_Idle_coreList__A[2] = {
    (xdc_UInt)0x0U,  /* [0] */
    (xdc_UInt)0x0U,  /* [1] */
};

/* Module__diagsEnabled__C */
#pragma DATA_SECTION(ti_sysbios_knl_Idle_Module__diagsEnabled__C, ".const:ti_sysbios_knl_Idle_Module__diagsEnabled__C");
__FAR__ const CT__ti_sysbios_knl_Idle_Module__diagsEnabled ti_sysbios_knl_Idle_Module__diagsEnabled__C = (xdc_Bits32)0x90U;

/* Module__diagsIncluded__C */
#pragma DATA_SECTION(ti_sysbios_knl_Idle_Module__diagsIncluded__C, ".const:ti_sysbios_knl_Idle_Module__diagsIncluded__C");
__FAR__ const CT__ti_sysbios_knl_Idle_Module__diagsIncluded ti_sysbios_knl_Idle_Module__diagsIncluded__C = (xdc_Bits32)0x90U;

/* Module__diagsMask__C */
#pragma DATA_SECTION(ti_sysbios_knl_Idle_Module__diagsMask__C, ".const:ti_sysbios_knl_Idle_Module__diagsMask__C");
__FAR__ const CT__ti_sysbios_knl_Idle_Module__diagsMask ti_sysbios_knl_Idle_Module__diagsMask__C = ((const CT__ti_sysbios_knl_Idle_Module__diagsMask)NULL);

/* Module__gateObj__C */
#pragma DATA_SECTION(ti_sysbios_knl_Idle_Module__gateObj__C, ".const:ti_sysbios_knl_Idle_Module__gateObj__C");
__FAR__ const CT__ti_sysbios_knl_Idle_Module__gateObj ti_sysbios_knl_Idle_Module__gateObj__C = ((const CT__ti_sysbios_knl_Idle_Module__gateObj)NULL);

/* Module__gatePrms__C */
#pragma DATA_SECTION(ti_sysbios_knl_Idle_Module__gatePrms__C, ".const:ti_sysbios_knl_Idle_Module__gatePrms__C");
__FAR__ const CT__ti_sysbios_knl_Idle_Module__gatePrms ti_sysbios_knl_Idle_Module__gatePrms__C = ((const CT__ti_sysbios_knl_Idle_Module__gatePrms)NULL);

/* Module__id__C */
#pragma DATA_SECTION(ti_sysbios_knl_Idle_Module__id__C, ".const:ti_sysbios_knl_Idle_Module__id__C");
__FAR__ const CT__ti_sysbios_knl_Idle_Module__id ti_sysbios_knl_Idle_Module__id__C = (xdc_Bits16)0x8021U;

/* Module__loggerDefined__C */
#pragma DATA_SECTION(ti_sysbios_knl_Idle_Module__loggerDefined__C, ".const:ti_sysbios_knl_Idle_Module__loggerDefined__C");
__FAR__ const CT__ti_sysbios_knl_Idle_Module__loggerDefined ti_sysbios_knl_Idle_Module__loggerDefined__C = 0;

/* Module__loggerObj__C */
#pragma DATA_SECTION(ti_sysbios_knl_Idle_Module__loggerObj__C, ".const:ti_sysbios_knl_Idle_Module__loggerObj__C");
__FAR__ const CT__ti_sysbios_knl_Idle_Module__loggerObj ti_sysbios_knl_Idle_Module__loggerObj__C = ((const CT__ti_sysbios_knl_Idle_Module__loggerObj)NULL);

/* Module__loggerFxn0__C */
#pragma DATA_SECTION(ti_sysbios_knl_Idle_Module__loggerFxn0__C, ".const:ti_sysbios_knl_Idle_Module__loggerFxn0__C");
__FAR__ const CT__ti_sysbios_knl_Idle_Module__loggerFxn0 ti_sysbios_knl_Idle_Module__loggerFxn0__C = ((const CT__ti_sysbios_knl_Idle_Module__loggerFxn0)NULL);

/* Module__loggerFxn1__C */
#pragma DATA_SECTION(ti_sysbios_knl_Idle_Module__loggerFxn1__C, ".const:ti_sysbios_knl_Idle_Module__loggerFxn1__C");
__FAR__ const CT__ti_sysbios_knl_Idle_Module__loggerFxn1 ti_sysbios_knl_Idle_Module__loggerFxn1__C = ((const CT__ti_sysbios_knl_Idle_Module__loggerFxn1)NULL);

/* Module__loggerFxn2__C */
#pragma DATA_SECTION(ti_sysbios_knl_Idle_Module__loggerFxn2__C, ".const:ti_sysbios_knl_Idle_Module__loggerFxn2__C");
__FAR__ const CT__ti_sysbios_knl_Idle_Module__loggerFxn2 ti_sysbios_knl_Idle_Module__loggerFxn2__C = ((const CT__ti_sysbios_knl_Idle_Module__loggerFxn2)NULL);

/* Module__loggerFxn4__C */
#pragma DATA_SECTION(ti_sysbios_knl_Idle_Module__loggerFxn4__C, ".const:ti_sysbios_knl_Idle_Module__loggerFxn4__C");
__FAR__ const CT__ti_sysbios_knl_Idle_Module__loggerFxn4 ti_sysbios_knl_Idle_Module__loggerFxn4__C = ((const CT__ti_sysbios_knl_Idle_Module__loggerFxn4)NULL);

/* Module__loggerFxn8__C */
#pragma DATA_SECTION(ti_sysbios_knl_Idle_Module__loggerFxn8__C, ".const:ti_sysbios_knl_Idle_Module__loggerFxn8__C");
__FAR__ const CT__ti_sysbios_knl_Idle_Module__loggerFxn8 ti_sysbios_knl_Idle_Module__loggerFxn8__C = ((const CT__ti_sysbios_knl_Idle_Module__loggerFxn8)NULL);

/* Object__count__C */
#pragma DATA_SECTION(ti_sysbios_knl_Idle_Object__count__C, ".const:ti_sysbios_knl_Idle_Object__count__C");
__FAR__ const CT__ti_sysbios_knl_Idle_Object__count ti_sysbios_knl_Idle_Object__count__C = 0;

/* Object__heap__C */
#pragma DATA_SECTION(ti_sysbios_knl_Idle_Object__heap__C, ".const:ti_sysbios_knl_Idle_Object__heap__C");
__FAR__ const CT__ti_sysbios_knl_Idle_Object__heap ti_sysbios_knl_Idle_Object__heap__C = NULL;

/* Object__sizeof__C */
#pragma DATA_SECTION(ti_sysbios_knl_Idle_Object__sizeof__C, ".const:ti_sysbios_knl_Idle_Object__sizeof__C");
__FAR__ const CT__ti_sysbios_knl_Idle_Object__sizeof ti_sysbios_knl_Idle_Object__sizeof__C = 0;

/* Object__table__C */
#pragma DATA_SECTION(ti_sysbios_knl_Idle_Object__table__C, ".const:ti_sysbios_knl_Idle_Object__table__C");
__FAR__ const CT__ti_sysbios_knl_Idle_Object__table ti_sysbios_knl_Idle_Object__table__C = NULL;

/* funcList__C */
#pragma DATA_SECTION(ti_sysbios_knl_Idle_funcList__C, ".const:ti_sysbios_knl_Idle_funcList__C");
__FAR__ const CT__ti_sysbios_knl_Idle_funcList ti_sysbios_knl_Idle_funcList__C = {2, ((__T1_ti_sysbios_knl_Idle_funcList const  *)ti_sysbios_knl_Idle_funcList__A)};

/* coreList__C */
#pragma DATA_SECTION(ti_sysbios_knl_Idle_coreList__C, ".const:ti_sysbios_knl_Idle_coreList__C");
__FAR__ const CT__ti_sysbios_knl_Idle_coreList ti_sysbios_knl_Idle_coreList__C = {2, ((__T1_ti_sysbios_knl_Idle_coreList const  *)ti_sysbios_knl_Idle_coreList__A)};


/*
 * ======== ti.sysbios.knl.Intrinsics INITIALIZERS ========
 */

/* Module__diagsEnabled__C */
#pragma DATA_SECTION(ti_sysbios_knl_Intrinsics_Module__diagsEnabled__C, ".const:ti_sysbios_knl_Intrinsics_Module__diagsEnabled__C");
__FAR__ const CT__ti_sysbios_knl_Intrinsics_Module__diagsEnabled ti_sysbios_knl_Intrinsics_Module__diagsEnabled__C = (xdc_Bits32)0x90U;

/* Module__diagsIncluded__C */
#pragma DATA_SECTION(ti_sysbios_knl_Intrinsics_Module__diagsIncluded__C, ".const:ti_sysbios_knl_Intrinsics_Module__diagsIncluded__C");
__FAR__ const CT__ti_sysbios_knl_Intrinsics_Module__diagsIncluded ti_sysbios_knl_Intrinsics_Module__diagsIncluded__C = (xdc_Bits32)0x90U;

/* Module__diagsMask__C */
#pragma DATA_SECTION(ti_sysbios_knl_Intrinsics_Module__diagsMask__C, ".const:ti_sysbios_knl_Intrinsics_Module__diagsMask__C");
__FAR__ const CT__ti_sysbios_knl_Intrinsics_Module__diagsMask ti_sysbios_knl_Intrinsics_Module__diagsMask__C = ((const CT__ti_sysbios_knl_Intrinsics_Module__diagsMask)NULL);

/* Module__gateObj__C */
#pragma DATA_SECTION(ti_sysbios_knl_Intrinsics_Module__gateObj__C, ".const:ti_sysbios_knl_Intrinsics_Module__gateObj__C");
__FAR__ const CT__ti_sysbios_knl_Intrinsics_Module__gateObj ti_sysbios_knl_Intrinsics_Module__gateObj__C = ((const CT__ti_sysbios_knl_Intrinsics_Module__gateObj)NULL);

/* Module__gatePrms__C */
#pragma DATA_SECTION(ti_sysbios_knl_Intrinsics_Module__gatePrms__C, ".const:ti_sysbios_knl_Intrinsics_Module__gatePrms__C");
__FAR__ const CT__ti_sysbios_knl_Intrinsics_Module__gatePrms ti_sysbios_knl_Intrinsics_Module__gatePrms__C = ((const CT__ti_sysbios_knl_Intrinsics_Module__gatePrms)NULL);

/* Module__id__C */
#pragma DATA_SECTION(ti_sysbios_knl_Intrinsics_Module__id__C, ".const:ti_sysbios_knl_Intrinsics_Module__id__C");
__FAR__ const CT__ti_sysbios_knl_Intrinsics_Module__id ti_sysbios_knl_Intrinsics_Module__id__C = (xdc_Bits16)0x8022U;

/* Module__loggerDefined__C */
#pragma DATA_SECTION(ti_sysbios_knl_Intrinsics_Module__loggerDefined__C, ".const:ti_sysbios_knl_Intrinsics_Module__loggerDefined__C");
__FAR__ const CT__ti_sysbios_knl_Intrinsics_Module__loggerDefined ti_sysbios_knl_Intrinsics_Module__loggerDefined__C = 0;

/* Module__loggerObj__C */
#pragma DATA_SECTION(ti_sysbios_knl_Intrinsics_Module__loggerObj__C, ".const:ti_sysbios_knl_Intrinsics_Module__loggerObj__C");
__FAR__ const CT__ti_sysbios_knl_Intrinsics_Module__loggerObj ti_sysbios_knl_Intrinsics_Module__loggerObj__C = ((const CT__ti_sysbios_knl_Intrinsics_Module__loggerObj)NULL);

/* Module__loggerFxn0__C */
#pragma DATA_SECTION(ti_sysbios_knl_Intrinsics_Module__loggerFxn0__C, ".const:ti_sysbios_knl_Intrinsics_Module__loggerFxn0__C");
__FAR__ const CT__ti_sysbios_knl_Intrinsics_Module__loggerFxn0 ti_sysbios_knl_Intrinsics_Module__loggerFxn0__C = ((const CT__ti_sysbios_knl_Intrinsics_Module__loggerFxn0)NULL);

/* Module__loggerFxn1__C */
#pragma DATA_SECTION(ti_sysbios_knl_Intrinsics_Module__loggerFxn1__C, ".const:ti_sysbios_knl_Intrinsics_Module__loggerFxn1__C");
__FAR__ const CT__ti_sysbios_knl_Intrinsics_Module__loggerFxn1 ti_sysbios_knl_Intrinsics_Module__loggerFxn1__C = ((const CT__ti_sysbios_knl_Intrinsics_Module__loggerFxn1)NULL);

/* Module__loggerFxn2__C */
#pragma DATA_SECTION(ti_sysbios_knl_Intrinsics_Module__loggerFxn2__C, ".const:ti_sysbios_knl_Intrinsics_Module__loggerFxn2__C");
__FAR__ const CT__ti_sysbios_knl_Intrinsics_Module__loggerFxn2 ti_sysbios_knl_Intrinsics_Module__loggerFxn2__C = ((const CT__ti_sysbios_knl_Intrinsics_Module__loggerFxn2)NULL);

/* Module__loggerFxn4__C */
#pragma DATA_SECTION(ti_sysbios_knl_Intrinsics_Module__loggerFxn4__C, ".const:ti_sysbios_knl_Intrinsics_Module__loggerFxn4__C");
__FAR__ const CT__ti_sysbios_knl_Intrinsics_Module__loggerFxn4 ti_sysbios_knl_Intrinsics_Module__loggerFxn4__C = ((const CT__ti_sysbios_knl_Intrinsics_Module__loggerFxn4)NULL);

/* Module__loggerFxn8__C */
#pragma DATA_SECTION(ti_sysbios_knl_Intrinsics_Module__loggerFxn8__C, ".const:ti_sysbios_knl_Intrinsics_Module__loggerFxn8__C");
__FAR__ const CT__ti_sysbios_knl_Intrinsics_Module__loggerFxn8 ti_sysbios_knl_Intrinsics_Module__loggerFxn8__C = ((const CT__ti_sysbios_knl_Intrinsics_Module__loggerFxn8)NULL);

/* Object__count__C */
#pragma DATA_SECTION(ti_sysbios_knl_Intrinsics_Object__count__C, ".const:ti_sysbios_knl_Intrinsics_Object__count__C");
__FAR__ const CT__ti_sysbios_knl_Intrinsics_Object__count ti_sysbios_knl_Intrinsics_Object__count__C = 0;

/* Object__heap__C */
#pragma DATA_SECTION(ti_sysbios_knl_Intrinsics_Object__heap__C, ".const:ti_sysbios_knl_Intrinsics_Object__heap__C");
__FAR__ const CT__ti_sysbios_knl_Intrinsics_Object__heap ti_sysbios_knl_Intrinsics_Object__heap__C = NULL;

/* Object__sizeof__C */
#pragma DATA_SECTION(ti_sysbios_knl_Intrinsics_Object__sizeof__C, ".const:ti_sysbios_knl_Intrinsics_Object__sizeof__C");
__FAR__ const CT__ti_sysbios_knl_Intrinsics_Object__sizeof ti_sysbios_knl_Intrinsics_Object__sizeof__C = 0;

/* Object__table__C */
#pragma DATA_SECTION(ti_sysbios_knl_Intrinsics_Object__table__C, ".const:ti_sysbios_knl_Intrinsics_Object__table__C");
__FAR__ const CT__ti_sysbios_knl_Intrinsics_Object__table ti_sysbios_knl_Intrinsics_Object__table__C = NULL;


/*
 * ======== ti.sysbios.knl.Intrinsics_SupportProxy INITIALIZERS ========
 */


/*
 * ======== ti.sysbios.knl.Queue INITIALIZERS ========
 */

/* Object__DESC__C */
__FAR__ const xdc_runtime_Core_ObjDesc ti_sysbios_knl_Queue_Object__DESC__C;

/* Object__PARAMS__C */
#pragma DATA_SECTION(ti_sysbios_knl_Queue_Object__PARAMS__C, ".const:ti_sysbios_knl_Queue_Object__PARAMS__C");
__FAR__ const ti_sysbios_knl_Queue_Params ti_sysbios_knl_Queue_Object__PARAMS__C = {
    sizeof (ti_sysbios_knl_Queue_Params), /* __size */
    NULL, /* __self */
    NULL, /* __fxns */
    (xdc_runtime_IInstance_Params*)&ti_sysbios_knl_Queue_Object__PARAMS__C.__iprms, /* instance */
    {
        sizeof (xdc_runtime_IInstance_Params), /* __size */
        0,  /* name */
    },  /* instance */
};

/* Module__root__V */
ti_sysbios_knl_Queue_Module__ ti_sysbios_knl_Queue_Module__root__V = {
    {&ti_sysbios_knl_Queue_Module__root__V.link,  /* link.next */
    &ti_sysbios_knl_Queue_Module__root__V.link},  /* link.prev */
};

/* Module__diagsEnabled__C */
#pragma DATA_SECTION(ti_sysbios_knl_Queue_Module__diagsEnabled__C, ".const:ti_sysbios_knl_Queue_Module__diagsEnabled__C");
__FAR__ const CT__ti_sysbios_knl_Queue_Module__diagsEnabled ti_sysbios_knl_Queue_Module__diagsEnabled__C = (xdc_Bits32)0x90U;

/* Module__diagsIncluded__C */
#pragma DATA_SECTION(ti_sysbios_knl_Queue_Module__diagsIncluded__C, ".const:ti_sysbios_knl_Queue_Module__diagsIncluded__C");
__FAR__ const CT__ti_sysbios_knl_Queue_Module__diagsIncluded ti_sysbios_knl_Queue_Module__diagsIncluded__C = (xdc_Bits32)0x90U;

/* Module__diagsMask__C */
#pragma DATA_SECTION(ti_sysbios_knl_Queue_Module__diagsMask__C, ".const:ti_sysbios_knl_Queue_Module__diagsMask__C");
__FAR__ const CT__ti_sysbios_knl_Queue_Module__diagsMask ti_sysbios_knl_Queue_Module__diagsMask__C = ((const CT__ti_sysbios_knl_Queue_Module__diagsMask)NULL);

/* Module__gateObj__C */
#pragma DATA_SECTION(ti_sysbios_knl_Queue_Module__gateObj__C, ".const:ti_sysbios_knl_Queue_Module__gateObj__C");
__FAR__ const CT__ti_sysbios_knl_Queue_Module__gateObj ti_sysbios_knl_Queue_Module__gateObj__C = ((const CT__ti_sysbios_knl_Queue_Module__gateObj)NULL);

/* Module__gatePrms__C */
#pragma DATA_SECTION(ti_sysbios_knl_Queue_Module__gatePrms__C, ".const:ti_sysbios_knl_Queue_Module__gatePrms__C");
__FAR__ const CT__ti_sysbios_knl_Queue_Module__gatePrms ti_sysbios_knl_Queue_Module__gatePrms__C = ((const CT__ti_sysbios_knl_Queue_Module__gatePrms)NULL);

/* Module__id__C */
#pragma DATA_SECTION(ti_sysbios_knl_Queue_Module__id__C, ".const:ti_sysbios_knl_Queue_Module__id__C");
__FAR__ const CT__ti_sysbios_knl_Queue_Module__id ti_sysbios_knl_Queue_Module__id__C = (xdc_Bits16)0x8024U;

/* Module__loggerDefined__C */
#pragma DATA_SECTION(ti_sysbios_knl_Queue_Module__loggerDefined__C, ".const:ti_sysbios_knl_Queue_Module__loggerDefined__C");
__FAR__ const CT__ti_sysbios_knl_Queue_Module__loggerDefined ti_sysbios_knl_Queue_Module__loggerDefined__C = 0;

/* Module__loggerObj__C */
#pragma DATA_SECTION(ti_sysbios_knl_Queue_Module__loggerObj__C, ".const:ti_sysbios_knl_Queue_Module__loggerObj__C");
__FAR__ const CT__ti_sysbios_knl_Queue_Module__loggerObj ti_sysbios_knl_Queue_Module__loggerObj__C = ((const CT__ti_sysbios_knl_Queue_Module__loggerObj)NULL);

/* Module__loggerFxn0__C */
#pragma DATA_SECTION(ti_sysbios_knl_Queue_Module__loggerFxn0__C, ".const:ti_sysbios_knl_Queue_Module__loggerFxn0__C");
__FAR__ const CT__ti_sysbios_knl_Queue_Module__loggerFxn0 ti_sysbios_knl_Queue_Module__loggerFxn0__C = ((const CT__ti_sysbios_knl_Queue_Module__loggerFxn0)NULL);

/* Module__loggerFxn1__C */
#pragma DATA_SECTION(ti_sysbios_knl_Queue_Module__loggerFxn1__C, ".const:ti_sysbios_knl_Queue_Module__loggerFxn1__C");
__FAR__ const CT__ti_sysbios_knl_Queue_Module__loggerFxn1 ti_sysbios_knl_Queue_Module__loggerFxn1__C = ((const CT__ti_sysbios_knl_Queue_Module__loggerFxn1)NULL);

/* Module__loggerFxn2__C */
#pragma DATA_SECTION(ti_sysbios_knl_Queue_Module__loggerFxn2__C, ".const:ti_sysbios_knl_Queue_Module__loggerFxn2__C");
__FAR__ const CT__ti_sysbios_knl_Queue_Module__loggerFxn2 ti_sysbios_knl_Queue_Module__loggerFxn2__C = ((const CT__ti_sysbios_knl_Queue_Module__loggerFxn2)NULL);

/* Module__loggerFxn4__C */
#pragma DATA_SECTION(ti_sysbios_knl_Queue_Module__loggerFxn4__C, ".const:ti_sysbios_knl_Queue_Module__loggerFxn4__C");
__FAR__ const CT__ti_sysbios_knl_Queue_Module__loggerFxn4 ti_sysbios_knl_Queue_Module__loggerFxn4__C = ((const CT__ti_sysbios_knl_Queue_Module__loggerFxn4)NULL);

/* Module__loggerFxn8__C */
#pragma DATA_SECTION(ti_sysbios_knl_Queue_Module__loggerFxn8__C, ".const:ti_sysbios_knl_Queue_Module__loggerFxn8__C");
__FAR__ const CT__ti_sysbios_knl_Queue_Module__loggerFxn8 ti_sysbios_knl_Queue_Module__loggerFxn8__C = ((const CT__ti_sysbios_knl_Queue_Module__loggerFxn8)NULL);

/* Object__count__C */
#pragma DATA_SECTION(ti_sysbios_knl_Queue_Object__count__C, ".const:ti_sysbios_knl_Queue_Object__count__C");
__FAR__ const CT__ti_sysbios_knl_Queue_Object__count ti_sysbios_knl_Queue_Object__count__C = 0;

/* Object__heap__C */
#pragma DATA_SECTION(ti_sysbios_knl_Queue_Object__heap__C, ".const:ti_sysbios_knl_Queue_Object__heap__C");
__FAR__ const CT__ti_sysbios_knl_Queue_Object__heap ti_sysbios_knl_Queue_Object__heap__C = NULL;

/* Object__sizeof__C */
#pragma DATA_SECTION(ti_sysbios_knl_Queue_Object__sizeof__C, ".const:ti_sysbios_knl_Queue_Object__sizeof__C");
__FAR__ const CT__ti_sysbios_knl_Queue_Object__sizeof ti_sysbios_knl_Queue_Object__sizeof__C = sizeof(ti_sysbios_knl_Queue_Object__);

/* Object__table__C */
#pragma DATA_SECTION(ti_sysbios_knl_Queue_Object__table__C, ".const:ti_sysbios_knl_Queue_Object__table__C");
__FAR__ const CT__ti_sysbios_knl_Queue_Object__table ti_sysbios_knl_Queue_Object__table__C = NULL;


/*
 * ======== ti.sysbios.knl.Semaphore INITIALIZERS ========
 */

/* Object__DESC__C */
__FAR__ const xdc_runtime_Core_ObjDesc ti_sysbios_knl_Semaphore_Object__DESC__C;

/* Object__PARAMS__C */
#pragma DATA_SECTION(ti_sysbios_knl_Semaphore_Object__PARAMS__C, ".const:ti_sysbios_knl_Semaphore_Object__PARAMS__C");
__FAR__ const ti_sysbios_knl_Semaphore_Params ti_sysbios_knl_Semaphore_Object__PARAMS__C = {
    sizeof (ti_sysbios_knl_Semaphore_Params), /* __size */
    NULL, /* __self */
    NULL, /* __fxns */
    (xdc_runtime_IInstance_Params*)&ti_sysbios_knl_Semaphore_Object__PARAMS__C.__iprms, /* instance */
    0,  /* event */
    (xdc_UInt)0x1U,  /* eventId */
    ti_sysbios_knl_Semaphore_Mode_COUNTING,  /* mode */
    {
        sizeof (xdc_runtime_IInstance_Params), /* __size */
        0,  /* name */
    },  /* instance */
};

/* Module__root__V */
ti_sysbios_knl_Semaphore_Module__ ti_sysbios_knl_Semaphore_Module__root__V = {
    {&ti_sysbios_knl_Semaphore_Module__root__V.link,  /* link.next */
    &ti_sysbios_knl_Semaphore_Module__root__V.link},  /* link.prev */
};

/* Module__diagsEnabled__C */
#pragma DATA_SECTION(ti_sysbios_knl_Semaphore_Module__diagsEnabled__C, ".const:ti_sysbios_knl_Semaphore_Module__diagsEnabled__C");
__FAR__ const CT__ti_sysbios_knl_Semaphore_Module__diagsEnabled ti_sysbios_knl_Semaphore_Module__diagsEnabled__C = (xdc_Bits32)0x90U;

/* Module__diagsIncluded__C */
#pragma DATA_SECTION(ti_sysbios_knl_Semaphore_Module__diagsIncluded__C, ".const:ti_sysbios_knl_Semaphore_Module__diagsIncluded__C");
__FAR__ const CT__ti_sysbios_knl_Semaphore_Module__diagsIncluded ti_sysbios_knl_Semaphore_Module__diagsIncluded__C = (xdc_Bits32)0x90U;

/* Module__diagsMask__C */
#pragma DATA_SECTION(ti_sysbios_knl_Semaphore_Module__diagsMask__C, ".const:ti_sysbios_knl_Semaphore_Module__diagsMask__C");
__FAR__ const CT__ti_sysbios_knl_Semaphore_Module__diagsMask ti_sysbios_knl_Semaphore_Module__diagsMask__C = ((const CT__ti_sysbios_knl_Semaphore_Module__diagsMask)NULL);

/* Module__gateObj__C */
#pragma DATA_SECTION(ti_sysbios_knl_Semaphore_Module__gateObj__C, ".const:ti_sysbios_knl_Semaphore_Module__gateObj__C");
__FAR__ const CT__ti_sysbios_knl_Semaphore_Module__gateObj ti_sysbios_knl_Semaphore_Module__gateObj__C = ((const CT__ti_sysbios_knl_Semaphore_Module__gateObj)NULL);

/* Module__gatePrms__C */
#pragma DATA_SECTION(ti_sysbios_knl_Semaphore_Module__gatePrms__C, ".const:ti_sysbios_knl_Semaphore_Module__gatePrms__C");
__FAR__ const CT__ti_sysbios_knl_Semaphore_Module__gatePrms ti_sysbios_knl_Semaphore_Module__gatePrms__C = ((const CT__ti_sysbios_knl_Semaphore_Module__gatePrms)NULL);

/* Module__id__C */
#pragma DATA_SECTION(ti_sysbios_knl_Semaphore_Module__id__C, ".const:ti_sysbios_knl_Semaphore_Module__id__C");
__FAR__ const CT__ti_sysbios_knl_Semaphore_Module__id ti_sysbios_knl_Semaphore_Module__id__C = (xdc_Bits16)0x8025U;

/* Module__loggerDefined__C */
#pragma DATA_SECTION(ti_sysbios_knl_Semaphore_Module__loggerDefined__C, ".const:ti_sysbios_knl_Semaphore_Module__loggerDefined__C");
__FAR__ const CT__ti_sysbios_knl_Semaphore_Module__loggerDefined ti_sysbios_knl_Semaphore_Module__loggerDefined__C = 0;

/* Module__loggerObj__C */
#pragma DATA_SECTION(ti_sysbios_knl_Semaphore_Module__loggerObj__C, ".const:ti_sysbios_knl_Semaphore_Module__loggerObj__C");
__FAR__ const CT__ti_sysbios_knl_Semaphore_Module__loggerObj ti_sysbios_knl_Semaphore_Module__loggerObj__C = ((const CT__ti_sysbios_knl_Semaphore_Module__loggerObj)NULL);

/* Module__loggerFxn0__C */
#pragma DATA_SECTION(ti_sysbios_knl_Semaphore_Module__loggerFxn0__C, ".const:ti_sysbios_knl_Semaphore_Module__loggerFxn0__C");
__FAR__ const CT__ti_sysbios_knl_Semaphore_Module__loggerFxn0 ti_sysbios_knl_Semaphore_Module__loggerFxn0__C = ((const CT__ti_sysbios_knl_Semaphore_Module__loggerFxn0)NULL);

/* Module__loggerFxn1__C */
#pragma DATA_SECTION(ti_sysbios_knl_Semaphore_Module__loggerFxn1__C, ".const:ti_sysbios_knl_Semaphore_Module__loggerFxn1__C");
__FAR__ const CT__ti_sysbios_knl_Semaphore_Module__loggerFxn1 ti_sysbios_knl_Semaphore_Module__loggerFxn1__C = ((const CT__ti_sysbios_knl_Semaphore_Module__loggerFxn1)NULL);

/* Module__loggerFxn2__C */
#pragma DATA_SECTION(ti_sysbios_knl_Semaphore_Module__loggerFxn2__C, ".const:ti_sysbios_knl_Semaphore_Module__loggerFxn2__C");
__FAR__ const CT__ti_sysbios_knl_Semaphore_Module__loggerFxn2 ti_sysbios_knl_Semaphore_Module__loggerFxn2__C = ((const CT__ti_sysbios_knl_Semaphore_Module__loggerFxn2)NULL);

/* Module__loggerFxn4__C */
#pragma DATA_SECTION(ti_sysbios_knl_Semaphore_Module__loggerFxn4__C, ".const:ti_sysbios_knl_Semaphore_Module__loggerFxn4__C");
__FAR__ const CT__ti_sysbios_knl_Semaphore_Module__loggerFxn4 ti_sysbios_knl_Semaphore_Module__loggerFxn4__C = ((const CT__ti_sysbios_knl_Semaphore_Module__loggerFxn4)NULL);

/* Module__loggerFxn8__C */
#pragma DATA_SECTION(ti_sysbios_knl_Semaphore_Module__loggerFxn8__C, ".const:ti_sysbios_knl_Semaphore_Module__loggerFxn8__C");
__FAR__ const CT__ti_sysbios_knl_Semaphore_Module__loggerFxn8 ti_sysbios_knl_Semaphore_Module__loggerFxn8__C = ((const CT__ti_sysbios_knl_Semaphore_Module__loggerFxn8)NULL);

/* Object__count__C */
#pragma DATA_SECTION(ti_sysbios_knl_Semaphore_Object__count__C, ".const:ti_sysbios_knl_Semaphore_Object__count__C");
__FAR__ const CT__ti_sysbios_knl_Semaphore_Object__count ti_sysbios_knl_Semaphore_Object__count__C = 0;

/* Object__heap__C */
#pragma DATA_SECTION(ti_sysbios_knl_Semaphore_Object__heap__C, ".const:ti_sysbios_knl_Semaphore_Object__heap__C");
__FAR__ const CT__ti_sysbios_knl_Semaphore_Object__heap ti_sysbios_knl_Semaphore_Object__heap__C = NULL;

/* Object__sizeof__C */
#pragma DATA_SECTION(ti_sysbios_knl_Semaphore_Object__sizeof__C, ".const:ti_sysbios_knl_Semaphore_Object__sizeof__C");
__FAR__ const CT__ti_sysbios_knl_Semaphore_Object__sizeof ti_sysbios_knl_Semaphore_Object__sizeof__C = sizeof(ti_sysbios_knl_Semaphore_Object__);

/* Object__table__C */
#pragma DATA_SECTION(ti_sysbios_knl_Semaphore_Object__table__C, ".const:ti_sysbios_knl_Semaphore_Object__table__C");
__FAR__ const CT__ti_sysbios_knl_Semaphore_Object__table ti_sysbios_knl_Semaphore_Object__table__C = NULL;

/* LM_post__C */
#pragma DATA_SECTION(ti_sysbios_knl_Semaphore_LM_post__C, ".const:ti_sysbios_knl_Semaphore_LM_post__C");
__FAR__ const CT__ti_sysbios_knl_Semaphore_LM_post ti_sysbios_knl_Semaphore_LM_post__C = (((xdc_runtime_Log_Event)6066) << 16 | 768);

/* LM_pend__C */
#pragma DATA_SECTION(ti_sysbios_knl_Semaphore_LM_pend__C, ".const:ti_sysbios_knl_Semaphore_LM_pend__C");
__FAR__ const CT__ti_sysbios_knl_Semaphore_LM_pend ti_sysbios_knl_Semaphore_LM_pend__C = (((xdc_runtime_Log_Event)6096) << 16 | 768);

/* A_noEvents__C */
#pragma DATA_SECTION(ti_sysbios_knl_Semaphore_A_noEvents__C, ".const:ti_sysbios_knl_Semaphore_A_noEvents__C");
__FAR__ const CT__ti_sysbios_knl_Semaphore_A_noEvents ti_sysbios_knl_Semaphore_A_noEvents__C = (((xdc_runtime_Assert_Id)1281) << 16 | 16);

/* A_invTimeout__C */
#pragma DATA_SECTION(ti_sysbios_knl_Semaphore_A_invTimeout__C, ".const:ti_sysbios_knl_Semaphore_A_invTimeout__C");
__FAR__ const CT__ti_sysbios_knl_Semaphore_A_invTimeout ti_sysbios_knl_Semaphore_A_invTimeout__C = (((xdc_runtime_Assert_Id)1336) << 16 | 16);

/* A_badContext__C */
#pragma DATA_SECTION(ti_sysbios_knl_Semaphore_A_badContext__C, ".const:ti_sysbios_knl_Semaphore_A_badContext__C");
__FAR__ const CT__ti_sysbios_knl_Semaphore_A_badContext ti_sysbios_knl_Semaphore_A_badContext__C = (((xdc_runtime_Assert_Id)1070) << 16 | 16);

/* A_overflow__C */
#pragma DATA_SECTION(ti_sysbios_knl_Semaphore_A_overflow__C, ".const:ti_sysbios_knl_Semaphore_A_overflow__C");
__FAR__ const CT__ti_sysbios_knl_Semaphore_A_overflow ti_sysbios_knl_Semaphore_A_overflow__C = (((xdc_runtime_Assert_Id)1401) << 16 | 16);

/* A_pendTaskDisabled__C */
#pragma DATA_SECTION(ti_sysbios_knl_Semaphore_A_pendTaskDisabled__C, ".const:ti_sysbios_knl_Semaphore_A_pendTaskDisabled__C");
__FAR__ const CT__ti_sysbios_knl_Semaphore_A_pendTaskDisabled ti_sysbios_knl_Semaphore_A_pendTaskDisabled__C = (((xdc_runtime_Assert_Id)1455) << 16 | 16);

/* E_objectNotInKernelSpace__C */
#pragma DATA_SECTION(ti_sysbios_knl_Semaphore_E_objectNotInKernelSpace__C, ".const:ti_sysbios_knl_Semaphore_E_objectNotInKernelSpace__C");
__FAR__ const CT__ti_sysbios_knl_Semaphore_E_objectNotInKernelSpace ti_sysbios_knl_Semaphore_E_objectNotInKernelSpace__C = (((xdc_runtime_Error_Id)4598) << 16 | 0U);

/* supportsEvents__C */
#pragma DATA_SECTION(ti_sysbios_knl_Semaphore_supportsEvents__C, ".const:ti_sysbios_knl_Semaphore_supportsEvents__C");
__FAR__ const CT__ti_sysbios_knl_Semaphore_supportsEvents ti_sysbios_knl_Semaphore_supportsEvents__C = 0;

/* supportsPriority__C */
#pragma DATA_SECTION(ti_sysbios_knl_Semaphore_supportsPriority__C, ".const:ti_sysbios_knl_Semaphore_supportsPriority__C");
__FAR__ const CT__ti_sysbios_knl_Semaphore_supportsPriority ti_sysbios_knl_Semaphore_supportsPriority__C = 1;

/* eventPost__C */
#pragma DATA_SECTION(ti_sysbios_knl_Semaphore_eventPost__C, ".const:ti_sysbios_knl_Semaphore_eventPost__C");
__FAR__ const CT__ti_sysbios_knl_Semaphore_eventPost ti_sysbios_knl_Semaphore_eventPost__C = ((const CT__ti_sysbios_knl_Semaphore_eventPost)NULL);

/* eventSync__C */
#pragma DATA_SECTION(ti_sysbios_knl_Semaphore_eventSync__C, ".const:ti_sysbios_knl_Semaphore_eventSync__C");
__FAR__ const CT__ti_sysbios_knl_Semaphore_eventSync ti_sysbios_knl_Semaphore_eventSync__C = ((const CT__ti_sysbios_knl_Semaphore_eventSync)NULL);


/*
 * ======== ti.sysbios.knl.Swi INITIALIZERS ========
 */

/* Object__DESC__C */
__FAR__ const xdc_runtime_Core_ObjDesc ti_sysbios_knl_Swi_Object__DESC__C;

/* Object__PARAMS__C */
#pragma DATA_SECTION(ti_sysbios_knl_Swi_Object__PARAMS__C, ".const:ti_sysbios_knl_Swi_Object__PARAMS__C");
__FAR__ const ti_sysbios_knl_Swi_Params ti_sysbios_knl_Swi_Object__PARAMS__C = {
    sizeof (ti_sysbios_knl_Swi_Params), /* __size */
    NULL, /* __self */
    NULL, /* __fxns */
    (xdc_runtime_IInstance_Params*)&ti_sysbios_knl_Swi_Object__PARAMS__C.__iprms, /* instance */
    ((xdc_UArg)(0x0)),  /* arg0 */
    ((xdc_UArg)(0x0)),  /* arg1 */
    (xdc_UInt)(-0x0 - 1),  /* priority */
    (xdc_UInt)0x0U,  /* trigger */
    {
        sizeof (xdc_runtime_IInstance_Params), /* __size */
        0,  /* name */
    },  /* instance */
};

/* --> ti_sysbios_knl_Swi_Instance_State_0_hookEnv__A */
__T1_ti_sysbios_knl_Swi_Instance_State__hookEnv ti_sysbios_knl_Swi_Instance_State_0_hookEnv__A[1];

/* Module__root__V */
ti_sysbios_knl_Swi_Module__ ti_sysbios_knl_Swi_Module__root__V = {
    {&ti_sysbios_knl_Swi_Module__root__V.link,  /* link.next */
    &ti_sysbios_knl_Swi_Module__root__V.link},  /* link.prev */
};

/* Object__table__V */
ti_sysbios_knl_Swi_Object__ ti_sysbios_knl_Swi_Object__table__V[1] = {
    {/* instance#0 */
        {
            ((ti_sysbios_knl_Queue_Elem*)((void*)&ti_sysbios_knl_Swi_Object__table__V[0].qElem)),  /* next */
            ((ti_sysbios_knl_Queue_Elem*)((void*)&ti_sysbios_knl_Swi_Object__table__V[0].qElem)),  /* prev */
        },  /* qElem */
        ((xdc_Void(*)(xdc_UArg f_arg0,xdc_UArg f_arg1))(ti_sysbios_knl_Clock_workFunc__E)),  /* fxn */
        ((xdc_UArg)(0x0)),  /* arg0 */
        ((xdc_UArg)(0x0)),  /* arg1 */
        (xdc_UInt)0xfU,  /* priority */
        (xdc_UInt)0x8000U,  /* mask */
        0,  /* posted */
        (xdc_UInt)0x0U,  /* initTrigger */
        (xdc_UInt)0x0U,  /* trigger */
        (ti_sysbios_knl_Queue_Handle)&ti_sysbios_knl_Swi_Module_State_0_readyQ__A[15],  /* readyQ */
        ((void*)ti_sysbios_knl_Swi_Instance_State_0_hookEnv__A),  /* hookEnv */
    },
};

/* --> ti_sysbios_knl_Swi_Module_State_0_readyQ__A */
__T1_ti_sysbios_knl_Swi_Module_State__readyQ ti_sysbios_knl_Swi_Module_State_0_readyQ__A[16] = {
    {
        {
            ((ti_sysbios_knl_Queue_Elem*)((void*)&ti_sysbios_knl_Swi_Module_State_0_readyQ__A[0].elem)),  /* next */
            ((ti_sysbios_knl_Queue_Elem*)((void*)&ti_sysbios_knl_Swi_Module_State_0_readyQ__A[0].elem)),  /* prev */
        },  /* elem */
    },  /* [0] */
    {
        {
            ((ti_sysbios_knl_Queue_Elem*)((void*)&ti_sysbios_knl_Swi_Module_State_0_readyQ__A[1].elem)),  /* next */
            ((ti_sysbios_knl_Queue_Elem*)((void*)&ti_sysbios_knl_Swi_Module_State_0_readyQ__A[1].elem)),  /* prev */
        },  /* elem */
    },  /* [1] */
    {
        {
            ((ti_sysbios_knl_Queue_Elem*)((void*)&ti_sysbios_knl_Swi_Module_State_0_readyQ__A[2].elem)),  /* next */
            ((ti_sysbios_knl_Queue_Elem*)((void*)&ti_sysbios_knl_Swi_Module_State_0_readyQ__A[2].elem)),  /* prev */
        },  /* elem */
    },  /* [2] */
    {
        {
            ((ti_sysbios_knl_Queue_Elem*)((void*)&ti_sysbios_knl_Swi_Module_State_0_readyQ__A[3].elem)),  /* next */
            ((ti_sysbios_knl_Queue_Elem*)((void*)&ti_sysbios_knl_Swi_Module_State_0_readyQ__A[3].elem)),  /* prev */
        },  /* elem */
    },  /* [3] */
    {
        {
            ((ti_sysbios_knl_Queue_Elem*)((void*)&ti_sysbios_knl_Swi_Module_State_0_readyQ__A[4].elem)),  /* next */
            ((ti_sysbios_knl_Queue_Elem*)((void*)&ti_sysbios_knl_Swi_Module_State_0_readyQ__A[4].elem)),  /* prev */
        },  /* elem */
    },  /* [4] */
    {
        {
            ((ti_sysbios_knl_Queue_Elem*)((void*)&ti_sysbios_knl_Swi_Module_State_0_readyQ__A[5].elem)),  /* next */
            ((ti_sysbios_knl_Queue_Elem*)((void*)&ti_sysbios_knl_Swi_Module_State_0_readyQ__A[5].elem)),  /* prev */
        },  /* elem */
    },  /* [5] */
    {
        {
            ((ti_sysbios_knl_Queue_Elem*)((void*)&ti_sysbios_knl_Swi_Module_State_0_readyQ__A[6].elem)),  /* next */
            ((ti_sysbios_knl_Queue_Elem*)((void*)&ti_sysbios_knl_Swi_Module_State_0_readyQ__A[6].elem)),  /* prev */
        },  /* elem */
    },  /* [6] */
    {
        {
            ((ti_sysbios_knl_Queue_Elem*)((void*)&ti_sysbios_knl_Swi_Module_State_0_readyQ__A[7].elem)),  /* next */
            ((ti_sysbios_knl_Queue_Elem*)((void*)&ti_sysbios_knl_Swi_Module_State_0_readyQ__A[7].elem)),  /* prev */
        },  /* elem */
    },  /* [7] */
    {
        {
            ((ti_sysbios_knl_Queue_Elem*)((void*)&ti_sysbios_knl_Swi_Module_State_0_readyQ__A[8].elem)),  /* next */
            ((ti_sysbios_knl_Queue_Elem*)((void*)&ti_sysbios_knl_Swi_Module_State_0_readyQ__A[8].elem)),  /* prev */
        },  /* elem */
    },  /* [8] */
    {
        {
            ((ti_sysbios_knl_Queue_Elem*)((void*)&ti_sysbios_knl_Swi_Module_State_0_readyQ__A[9].elem)),  /* next */
            ((ti_sysbios_knl_Queue_Elem*)((void*)&ti_sysbios_knl_Swi_Module_State_0_readyQ__A[9].elem)),  /* prev */
        },  /* elem */
    },  /* [9] */
    {
        {
            ((ti_sysbios_knl_Queue_Elem*)((void*)&ti_sysbios_knl_Swi_Module_State_0_readyQ__A[10].elem)),  /* next */
            ((ti_sysbios_knl_Queue_Elem*)((void*)&ti_sysbios_knl_Swi_Module_State_0_readyQ__A[10].elem)),  /* prev */
        },  /* elem */
    },  /* [10] */
    {
        {
            ((ti_sysbios_knl_Queue_Elem*)((void*)&ti_sysbios_knl_Swi_Module_State_0_readyQ__A[11].elem)),  /* next */
            ((ti_sysbios_knl_Queue_Elem*)((void*)&ti_sysbios_knl_Swi_Module_State_0_readyQ__A[11].elem)),  /* prev */
        },  /* elem */
    },  /* [11] */
    {
        {
            ((ti_sysbios_knl_Queue_Elem*)((void*)&ti_sysbios_knl_Swi_Module_State_0_readyQ__A[12].elem)),  /* next */
            ((ti_sysbios_knl_Queue_Elem*)((void*)&ti_sysbios_knl_Swi_Module_State_0_readyQ__A[12].elem)),  /* prev */
        },  /* elem */
    },  /* [12] */
    {
        {
            ((ti_sysbios_knl_Queue_Elem*)((void*)&ti_sysbios_knl_Swi_Module_State_0_readyQ__A[13].elem)),  /* next */
            ((ti_sysbios_knl_Queue_Elem*)((void*)&ti_sysbios_knl_Swi_Module_State_0_readyQ__A[13].elem)),  /* prev */
        },  /* elem */
    },  /* [13] */
    {
        {
            ((ti_sysbios_knl_Queue_Elem*)((void*)&ti_sysbios_knl_Swi_Module_State_0_readyQ__A[14].elem)),  /* next */
            ((ti_sysbios_knl_Queue_Elem*)((void*)&ti_sysbios_knl_Swi_Module_State_0_readyQ__A[14].elem)),  /* prev */
        },  /* elem */
    },  /* [14] */
    {
        {
            ((ti_sysbios_knl_Queue_Elem*)((void*)&ti_sysbios_knl_Swi_Module_State_0_readyQ__A[15].elem)),  /* next */
            ((ti_sysbios_knl_Queue_Elem*)((void*)&ti_sysbios_knl_Swi_Module_State_0_readyQ__A[15].elem)),  /* prev */
        },  /* elem */
    },  /* [15] */
};

/* Module__state__V */
#ifdef __ti__
ti_sysbios_knl_Swi_Module_State__ ti_sysbios_knl_Swi_Module__state__V __attribute__ ((section(".data:ti_sysbios_knl_Swi_Module__state__V")));
#elif !(defined(__MACH__) && defined(__APPLE__))
ti_sysbios_knl_Swi_Module_State__ ti_sysbios_knl_Swi_Module__state__V __attribute__ ((section(".data_ti_sysbios_knl_Swi_Module__state__V")));
#endif
ti_sysbios_knl_Swi_Module_State__ ti_sysbios_knl_Swi_Module__state__V = {
    1,  /* locked */
    (xdc_UInt)0x0U,  /* curSet */
    (xdc_UInt)0x0U,  /* curTrigger */
    0,  /* curSwi */
    0,  /* curQ */
    ((void*)ti_sysbios_knl_Swi_Module_State_0_readyQ__A),  /* readyQ */
    ((void*)0),  /* constructedSwis */
};

/* --> ti_sysbios_knl_Swi_hooks__A */
#pragma DATA_SECTION(ti_sysbios_knl_Swi_hooks__A, ".const:ti_sysbios_knl_Swi_hooks__A");
const __T1_ti_sysbios_knl_Swi_hooks ti_sysbios_knl_Swi_hooks__A[1] = {
    {
        ((xdc_Void(*)(xdc_Int f_arg0))NULL),  /* registerFxn */
        ((xdc_Void(*)(ti_sysbios_knl_Swi_Handle f_arg0,xdc_runtime_Error_Block* f_arg1))NULL),  /* createFxn */
        ((xdc_Void(*)(ti_sysbios_knl_Swi_Handle f_arg0))NULL),  /* readyFxn */
        ((xdc_Void(*)(ti_sysbios_knl_Swi_Handle f_arg0))(ti_sysbios_utils_Load_swiBeginHook__E)),  /* beginFxn */
        ((xdc_Void(*)(ti_sysbios_knl_Swi_Handle f_arg0))(ti_sysbios_utils_Load_swiEndHook__E)),  /* endFxn */
        ((xdc_Void(*)(ti_sysbios_knl_Swi_Handle f_arg0))NULL),  /* deleteFxn */
    },  /* [0] */
};

/* Module__diagsEnabled__C */
#pragma DATA_SECTION(ti_sysbios_knl_Swi_Module__diagsEnabled__C, ".const:ti_sysbios_knl_Swi_Module__diagsEnabled__C");
__FAR__ const CT__ti_sysbios_knl_Swi_Module__diagsEnabled ti_sysbios_knl_Swi_Module__diagsEnabled__C = (xdc_Bits32)0x90U;

/* Module__diagsIncluded__C */
#pragma DATA_SECTION(ti_sysbios_knl_Swi_Module__diagsIncluded__C, ".const:ti_sysbios_knl_Swi_Module__diagsIncluded__C");
__FAR__ const CT__ti_sysbios_knl_Swi_Module__diagsIncluded ti_sysbios_knl_Swi_Module__diagsIncluded__C = (xdc_Bits32)0x90U;

/* Module__diagsMask__C */
#pragma DATA_SECTION(ti_sysbios_knl_Swi_Module__diagsMask__C, ".const:ti_sysbios_knl_Swi_Module__diagsMask__C");
__FAR__ const CT__ti_sysbios_knl_Swi_Module__diagsMask ti_sysbios_knl_Swi_Module__diagsMask__C = ((const CT__ti_sysbios_knl_Swi_Module__diagsMask)NULL);

/* Module__gateObj__C */
#pragma DATA_SECTION(ti_sysbios_knl_Swi_Module__gateObj__C, ".const:ti_sysbios_knl_Swi_Module__gateObj__C");
__FAR__ const CT__ti_sysbios_knl_Swi_Module__gateObj ti_sysbios_knl_Swi_Module__gateObj__C = ((const CT__ti_sysbios_knl_Swi_Module__gateObj)NULL);

/* Module__gatePrms__C */
#pragma DATA_SECTION(ti_sysbios_knl_Swi_Module__gatePrms__C, ".const:ti_sysbios_knl_Swi_Module__gatePrms__C");
__FAR__ const CT__ti_sysbios_knl_Swi_Module__gatePrms ti_sysbios_knl_Swi_Module__gatePrms__C = ((const CT__ti_sysbios_knl_Swi_Module__gatePrms)NULL);

/* Module__id__C */
#pragma DATA_SECTION(ti_sysbios_knl_Swi_Module__id__C, ".const:ti_sysbios_knl_Swi_Module__id__C");
__FAR__ const CT__ti_sysbios_knl_Swi_Module__id ti_sysbios_knl_Swi_Module__id__C = (xdc_Bits16)0x8026U;

/* Module__loggerDefined__C */
#pragma DATA_SECTION(ti_sysbios_knl_Swi_Module__loggerDefined__C, ".const:ti_sysbios_knl_Swi_Module__loggerDefined__C");
__FAR__ const CT__ti_sysbios_knl_Swi_Module__loggerDefined ti_sysbios_knl_Swi_Module__loggerDefined__C = 0;

/* Module__loggerObj__C */
#pragma DATA_SECTION(ti_sysbios_knl_Swi_Module__loggerObj__C, ".const:ti_sysbios_knl_Swi_Module__loggerObj__C");
__FAR__ const CT__ti_sysbios_knl_Swi_Module__loggerObj ti_sysbios_knl_Swi_Module__loggerObj__C = ((const CT__ti_sysbios_knl_Swi_Module__loggerObj)NULL);

/* Module__loggerFxn0__C */
#pragma DATA_SECTION(ti_sysbios_knl_Swi_Module__loggerFxn0__C, ".const:ti_sysbios_knl_Swi_Module__loggerFxn0__C");
__FAR__ const CT__ti_sysbios_knl_Swi_Module__loggerFxn0 ti_sysbios_knl_Swi_Module__loggerFxn0__C = ((const CT__ti_sysbios_knl_Swi_Module__loggerFxn0)NULL);

/* Module__loggerFxn1__C */
#pragma DATA_SECTION(ti_sysbios_knl_Swi_Module__loggerFxn1__C, ".const:ti_sysbios_knl_Swi_Module__loggerFxn1__C");
__FAR__ const CT__ti_sysbios_knl_Swi_Module__loggerFxn1 ti_sysbios_knl_Swi_Module__loggerFxn1__C = ((const CT__ti_sysbios_knl_Swi_Module__loggerFxn1)NULL);

/* Module__loggerFxn2__C */
#pragma DATA_SECTION(ti_sysbios_knl_Swi_Module__loggerFxn2__C, ".const:ti_sysbios_knl_Swi_Module__loggerFxn2__C");
__FAR__ const CT__ti_sysbios_knl_Swi_Module__loggerFxn2 ti_sysbios_knl_Swi_Module__loggerFxn2__C = ((const CT__ti_sysbios_knl_Swi_Module__loggerFxn2)NULL);

/* Module__loggerFxn4__C */
#pragma DATA_SECTION(ti_sysbios_knl_Swi_Module__loggerFxn4__C, ".const:ti_sysbios_knl_Swi_Module__loggerFxn4__C");
__FAR__ const CT__ti_sysbios_knl_Swi_Module__loggerFxn4 ti_sysbios_knl_Swi_Module__loggerFxn4__C = ((const CT__ti_sysbios_knl_Swi_Module__loggerFxn4)NULL);

/* Module__loggerFxn8__C */
#pragma DATA_SECTION(ti_sysbios_knl_Swi_Module__loggerFxn8__C, ".const:ti_sysbios_knl_Swi_Module__loggerFxn8__C");
__FAR__ const CT__ti_sysbios_knl_Swi_Module__loggerFxn8 ti_sysbios_knl_Swi_Module__loggerFxn8__C = ((const CT__ti_sysbios_knl_Swi_Module__loggerFxn8)NULL);

/* Object__count__C */
#pragma DATA_SECTION(ti_sysbios_knl_Swi_Object__count__C, ".const:ti_sysbios_knl_Swi_Object__count__C");
__FAR__ const CT__ti_sysbios_knl_Swi_Object__count ti_sysbios_knl_Swi_Object__count__C = 1;

/* Object__heap__C */
#pragma DATA_SECTION(ti_sysbios_knl_Swi_Object__heap__C, ".const:ti_sysbios_knl_Swi_Object__heap__C");
__FAR__ const CT__ti_sysbios_knl_Swi_Object__heap ti_sysbios_knl_Swi_Object__heap__C = NULL;

/* Object__sizeof__C */
#pragma DATA_SECTION(ti_sysbios_knl_Swi_Object__sizeof__C, ".const:ti_sysbios_knl_Swi_Object__sizeof__C");
__FAR__ const CT__ti_sysbios_knl_Swi_Object__sizeof ti_sysbios_knl_Swi_Object__sizeof__C = sizeof(ti_sysbios_knl_Swi_Object__);

/* Object__table__C */
#pragma DATA_SECTION(ti_sysbios_knl_Swi_Object__table__C, ".const:ti_sysbios_knl_Swi_Object__table__C");
__FAR__ const CT__ti_sysbios_knl_Swi_Object__table ti_sysbios_knl_Swi_Object__table__C = ti_sysbios_knl_Swi_Object__table__V;

/* LM_begin__C */
#pragma DATA_SECTION(ti_sysbios_knl_Swi_LM_begin__C, ".const:ti_sysbios_knl_Swi_LM_begin__C");
__FAR__ const CT__ti_sysbios_knl_Swi_LM_begin ti_sysbios_knl_Swi_LM_begin__C = (((xdc_runtime_Log_Event)6139) << 16 | 768);

/* LD_end__C */
#pragma DATA_SECTION(ti_sysbios_knl_Swi_LD_end__C, ".const:ti_sysbios_knl_Swi_LD_end__C");
__FAR__ const CT__ti_sysbios_knl_Swi_LD_end ti_sysbios_knl_Swi_LD_end__C = (((xdc_runtime_Log_Event)6186) << 16 | 512);

/* LM_post__C */
#pragma DATA_SECTION(ti_sysbios_knl_Swi_LM_post__C, ".const:ti_sysbios_knl_Swi_LM_post__C");
__FAR__ const CT__ti_sysbios_knl_Swi_LM_post ti_sysbios_knl_Swi_LM_post__C = (((xdc_runtime_Log_Event)6204) << 16 | 768);

/* A_swiDisabled__C */
#pragma DATA_SECTION(ti_sysbios_knl_Swi_A_swiDisabled__C, ".const:ti_sysbios_knl_Swi_A_swiDisabled__C");
__FAR__ const CT__ti_sysbios_knl_Swi_A_swiDisabled ti_sysbios_knl_Swi_A_swiDisabled__C = (((xdc_runtime_Assert_Id)1549) << 16 | 16);

/* A_badPriority__C */
#pragma DATA_SECTION(ti_sysbios_knl_Swi_A_badPriority__C, ".const:ti_sysbios_knl_Swi_A_badPriority__C");
__FAR__ const CT__ti_sysbios_knl_Swi_A_badPriority ti_sysbios_knl_Swi_A_badPriority__C = (((xdc_runtime_Assert_Id)1606) << 16 | 16);

/* A_badContextId__C */
#pragma DATA_SECTION(ti_sysbios_knl_Swi_A_badContextId__C, ".const:ti_sysbios_knl_Swi_A_badContextId__C");
__FAR__ const CT__ti_sysbios_knl_Swi_A_badContextId ti_sysbios_knl_Swi_A_badContextId__C = (((xdc_runtime_Assert_Id)1655) << 16 | 16);

/* numPriorities__C */
#pragma DATA_SECTION(ti_sysbios_knl_Swi_numPriorities__C, ".const:ti_sysbios_knl_Swi_numPriorities__C");
__FAR__ const CT__ti_sysbios_knl_Swi_numPriorities ti_sysbios_knl_Swi_numPriorities__C = (xdc_UInt)0x10U;

/* hooks__C */
#pragma DATA_SECTION(ti_sysbios_knl_Swi_hooks__C, ".const:ti_sysbios_knl_Swi_hooks__C");
__FAR__ const CT__ti_sysbios_knl_Swi_hooks ti_sysbios_knl_Swi_hooks__C = {1, ((__T1_ti_sysbios_knl_Swi_hooks const  *)ti_sysbios_knl_Swi_hooks__A)};

/* taskDisable__C */
#pragma DATA_SECTION(ti_sysbios_knl_Swi_taskDisable__C, ".const:ti_sysbios_knl_Swi_taskDisable__C");
__FAR__ const CT__ti_sysbios_knl_Swi_taskDisable ti_sysbios_knl_Swi_taskDisable__C = ((const CT__ti_sysbios_knl_Swi_taskDisable)(ti_sysbios_knl_Task_disable__E));

/* taskRestore__C */
#pragma DATA_SECTION(ti_sysbios_knl_Swi_taskRestore__C, ".const:ti_sysbios_knl_Swi_taskRestore__C");
__FAR__ const CT__ti_sysbios_knl_Swi_taskRestore ti_sysbios_knl_Swi_taskRestore__C = ((const CT__ti_sysbios_knl_Swi_taskRestore)(ti_sysbios_knl_Task_restore__E));

/* numConstructedSwis__C */
#pragma DATA_SECTION(ti_sysbios_knl_Swi_numConstructedSwis__C, ".const:ti_sysbios_knl_Swi_numConstructedSwis__C");
__FAR__ const CT__ti_sysbios_knl_Swi_numConstructedSwis ti_sysbios_knl_Swi_numConstructedSwis__C = (xdc_UInt)0x0U;


/*
 * ======== ti.sysbios.knl.Task INITIALIZERS ========
 */

/* Object__DESC__C */
__FAR__ const xdc_runtime_Core_ObjDesc ti_sysbios_knl_Task_Object__DESC__C;

/* Object__PARAMS__C */
#pragma DATA_SECTION(ti_sysbios_knl_Task_Object__PARAMS__C, ".const:ti_sysbios_knl_Task_Object__PARAMS__C");
__FAR__ const ti_sysbios_knl_Task_Params ti_sysbios_knl_Task_Object__PARAMS__C = {
    sizeof (ti_sysbios_knl_Task_Params), /* __size */
    NULL, /* __self */
    NULL, /* __fxns */
    (xdc_runtime_IInstance_Params*)&ti_sysbios_knl_Task_Object__PARAMS__C.__iprms, /* instance */
    ((xdc_UArg)(0x0)),  /* arg0 */
    ((xdc_UArg)(0x0)),  /* arg1 */
    (xdc_Int)0x1,  /* priority */
    ((xdc_Ptr)NULL),  /* stack */
    (xdc_SizeT)0x0,  /* stackSize */
    0,  /* stackHeap */
    ((xdc_Ptr)NULL),  /* env */
    1,  /* vitalTaskFlag */
    (xdc_UInt)0x0U,  /* affinity */
    1,  /* privileged */
    ((xdc_Ptr)NULL),  /* domain */
    {
        sizeof (xdc_runtime_IInstance_Params), /* __size */
        0,  /* name */
    },  /* instance */
};

/* --> ti_sysbios_knl_Task_Instance_State_0_stack__A */
__T1_ti_sysbios_knl_Task_Instance_State__stack ti_sysbios_knl_Task_Instance_State_0_stack__A[16384];

/* --> ti_sysbios_knl_Task_Instance_State_0_hookEnv__A */
__T1_ti_sysbios_knl_Task_Instance_State__hookEnv ti_sysbios_knl_Task_Instance_State_0_hookEnv__A[1];

/* Module__root__V */
ti_sysbios_knl_Task_Module__ ti_sysbios_knl_Task_Module__root__V = {
    {&ti_sysbios_knl_Task_Module__root__V.link,  /* link.next */
    &ti_sysbios_knl_Task_Module__root__V.link},  /* link.prev */
};

/* Object__table__V */
ti_sysbios_knl_Task_Object__ ti_sysbios_knl_Task_Object__table__V[1] = {
    {/* instance#0 */
        {
            ((ti_sysbios_knl_Queue_Elem*)((void*)&ti_sysbios_knl_Task_Object__table__V[0].qElem)),  /* next */
            ((ti_sysbios_knl_Queue_Elem*)((void*)&ti_sysbios_knl_Task_Object__table__V[0].qElem)),  /* prev */
        },  /* qElem */
        (xdc_Int)0x0,  /* priority */
        (xdc_UInt)0x1U,  /* mask */
        ((xdc_Ptr)NULL),  /* context */
        ti_sysbios_knl_Task_Mode_INACTIVE,  /* mode */
        ((ti_sysbios_knl_Task_PendElem*)NULL),  /* pendElem */
        (xdc_SizeT)0x4000,  /* stackSize */
        ((void*)ti_sysbios_knl_Task_Instance_State_0_stack__A),  /* stack */
        0,  /* stackHeap */
        ((xdc_Void(*)(xdc_UArg f_arg0,xdc_UArg f_arg1))(ti_sysbios_knl_Idle_loop__E)),  /* fxn */
        ((xdc_UArg)(0x0)),  /* arg0 */
        ((xdc_UArg)(0x0)),  /* arg1 */
        ((xdc_Ptr)NULL),  /* env */
        ((void*)ti_sysbios_knl_Task_Instance_State_0_hookEnv__A),  /* hookEnv */
        1,  /* vitalTaskFlag */
        0,  /* readyQ */
        (xdc_UInt)0x0U,  /* curCoreId */
        (xdc_UInt)0x0U,  /* affinity */
        1,  /* privileged */
        ((xdc_Ptr)NULL),  /* domain */
        (xdc_UInt32)0x0U,  /* checkValue */
        ((xdc_Ptr)NULL),  /* tls */
    },
};

/* --> ti_sysbios_knl_Task_Module_State_0_readyQ__A */
__T1_ti_sysbios_knl_Task_Module_State__readyQ ti_sysbios_knl_Task_Module_State_0_readyQ__A[16] = {
    {
        {
            ((ti_sysbios_knl_Queue_Elem*)((void*)&ti_sysbios_knl_Task_Module_State_0_readyQ__A[0].elem)),  /* next */
            ((ti_sysbios_knl_Queue_Elem*)((void*)&ti_sysbios_knl_Task_Module_State_0_readyQ__A[0].elem)),  /* prev */
        },  /* elem */
    },  /* [0] */
    {
        {
            ((ti_sysbios_knl_Queue_Elem*)((void*)&ti_sysbios_knl_Task_Module_State_0_readyQ__A[1].elem)),  /* next */
            ((ti_sysbios_knl_Queue_Elem*)((void*)&ti_sysbios_knl_Task_Module_State_0_readyQ__A[1].elem)),  /* prev */
        },  /* elem */
    },  /* [1] */
    {
        {
            ((ti_sysbios_knl_Queue_Elem*)((void*)&ti_sysbios_knl_Task_Module_State_0_readyQ__A[2].elem)),  /* next */
            ((ti_sysbios_knl_Queue_Elem*)((void*)&ti_sysbios_knl_Task_Module_State_0_readyQ__A[2].elem)),  /* prev */
        },  /* elem */
    },  /* [2] */
    {
        {
            ((ti_sysbios_knl_Queue_Elem*)((void*)&ti_sysbios_knl_Task_Module_State_0_readyQ__A[3].elem)),  /* next */
            ((ti_sysbios_knl_Queue_Elem*)((void*)&ti_sysbios_knl_Task_Module_State_0_readyQ__A[3].elem)),  /* prev */
        },  /* elem */
    },  /* [3] */
    {
        {
            ((ti_sysbios_knl_Queue_Elem*)((void*)&ti_sysbios_knl_Task_Module_State_0_readyQ__A[4].elem)),  /* next */
            ((ti_sysbios_knl_Queue_Elem*)((void*)&ti_sysbios_knl_Task_Module_State_0_readyQ__A[4].elem)),  /* prev */
        },  /* elem */
    },  /* [4] */
    {
        {
            ((ti_sysbios_knl_Queue_Elem*)((void*)&ti_sysbios_knl_Task_Module_State_0_readyQ__A[5].elem)),  /* next */
            ((ti_sysbios_knl_Queue_Elem*)((void*)&ti_sysbios_knl_Task_Module_State_0_readyQ__A[5].elem)),  /* prev */
        },  /* elem */
    },  /* [5] */
    {
        {
            ((ti_sysbios_knl_Queue_Elem*)((void*)&ti_sysbios_knl_Task_Module_State_0_readyQ__A[6].elem)),  /* next */
            ((ti_sysbios_knl_Queue_Elem*)((void*)&ti_sysbios_knl_Task_Module_State_0_readyQ__A[6].elem)),  /* prev */
        },  /* elem */
    },  /* [6] */
    {
        {
            ((ti_sysbios_knl_Queue_Elem*)((void*)&ti_sysbios_knl_Task_Module_State_0_readyQ__A[7].elem)),  /* next */
            ((ti_sysbios_knl_Queue_Elem*)((void*)&ti_sysbios_knl_Task_Module_State_0_readyQ__A[7].elem)),  /* prev */
        },  /* elem */
    },  /* [7] */
    {
        {
            ((ti_sysbios_knl_Queue_Elem*)((void*)&ti_sysbios_knl_Task_Module_State_0_readyQ__A[8].elem)),  /* next */
            ((ti_sysbios_knl_Queue_Elem*)((void*)&ti_sysbios_knl_Task_Module_State_0_readyQ__A[8].elem)),  /* prev */
        },  /* elem */
    },  /* [8] */
    {
        {
            ((ti_sysbios_knl_Queue_Elem*)((void*)&ti_sysbios_knl_Task_Module_State_0_readyQ__A[9].elem)),  /* next */
            ((ti_sysbios_knl_Queue_Elem*)((void*)&ti_sysbios_knl_Task_Module_State_0_readyQ__A[9].elem)),  /* prev */
        },  /* elem */
    },  /* [9] */
    {
        {
            ((ti_sysbios_knl_Queue_Elem*)((void*)&ti_sysbios_knl_Task_Module_State_0_readyQ__A[10].elem)),  /* next */
            ((ti_sysbios_knl_Queue_Elem*)((void*)&ti_sysbios_knl_Task_Module_State_0_readyQ__A[10].elem)),  /* prev */
        },  /* elem */
    },  /* [10] */
    {
        {
            ((ti_sysbios_knl_Queue_Elem*)((void*)&ti_sysbios_knl_Task_Module_State_0_readyQ__A[11].elem)),  /* next */
            ((ti_sysbios_knl_Queue_Elem*)((void*)&ti_sysbios_knl_Task_Module_State_0_readyQ__A[11].elem)),  /* prev */
        },  /* elem */
    },  /* [11] */
    {
        {
            ((ti_sysbios_knl_Queue_Elem*)((void*)&ti_sysbios_knl_Task_Module_State_0_readyQ__A[12].elem)),  /* next */
            ((ti_sysbios_knl_Queue_Elem*)((void*)&ti_sysbios_knl_Task_Module_State_0_readyQ__A[12].elem)),  /* prev */
        },  /* elem */
    },  /* [12] */
    {
        {
            ((ti_sysbios_knl_Queue_Elem*)((void*)&ti_sysbios_knl_Task_Module_State_0_readyQ__A[13].elem)),  /* next */
            ((ti_sysbios_knl_Queue_Elem*)((void*)&ti_sysbios_knl_Task_Module_State_0_readyQ__A[13].elem)),  /* prev */
        },  /* elem */
    },  /* [13] */
    {
        {
            ((ti_sysbios_knl_Queue_Elem*)((void*)&ti_sysbios_knl_Task_Module_State_0_readyQ__A[14].elem)),  /* next */
            ((ti_sysbios_knl_Queue_Elem*)((void*)&ti_sysbios_knl_Task_Module_State_0_readyQ__A[14].elem)),  /* prev */
        },  /* elem */
    },  /* [14] */
    {
        {
            ((ti_sysbios_knl_Queue_Elem*)((void*)&ti_sysbios_knl_Task_Module_State_0_readyQ__A[15].elem)),  /* next */
            ((ti_sysbios_knl_Queue_Elem*)((void*)&ti_sysbios_knl_Task_Module_State_0_readyQ__A[15].elem)),  /* prev */
        },  /* elem */
    },  /* [15] */
};

/* --> ti_sysbios_knl_Task_Module_State_0_idleTask__A */
__T1_ti_sysbios_knl_Task_Module_State__idleTask ti_sysbios_knl_Task_Module_State_0_idleTask__A[1] = {
    (ti_sysbios_knl_Task_Handle)&ti_sysbios_knl_Task_Object__table__V[0],  /* [0] */
};

/* Module__state__V */
#ifdef __ti__
ti_sysbios_knl_Task_Module_State__ ti_sysbios_knl_Task_Module__state__V __attribute__ ((section(".data:ti_sysbios_knl_Task_Module__state__V")));
#elif !(defined(__MACH__) && defined(__APPLE__))
ti_sysbios_knl_Task_Module_State__ ti_sysbios_knl_Task_Module__state__V __attribute__ ((section(".data_ti_sysbios_knl_Task_Module__state__V")));
#endif
ti_sysbios_knl_Task_Module_State__ ti_sysbios_knl_Task_Module__state__V = {
    1,  /* locked */
    (xdc_UInt)0x0U,  /* curSet */
    0,  /* workFlag */
    (xdc_UInt)0x1U,  /* vitalTasks */
    0,  /* curTask */
    0,  /* curQ */
    ((void*)ti_sysbios_knl_Task_Module_State_0_readyQ__A),  /* readyQ */
    ((void*)0),  /* smpCurSet */
    ((void*)0),  /* smpCurMask */
    ((void*)0),  /* smpCurTask */
    ((void*)0),  /* smpReadyQ */
    ((void*)ti_sysbios_knl_Task_Module_State_0_idleTask__A),  /* idleTask */
    ((void*)0),  /* constructedTasks */
    1,  /* curTaskPrivileged */
    {
        {
            ((ti_sysbios_knl_Queue_Elem*)((void*)&ti_sysbios_knl_Task_Module__state__V.Object_field_inactiveQ.elem)),  /* next */
            ((ti_sysbios_knl_Queue_Elem*)((void*)&ti_sysbios_knl_Task_Module__state__V.Object_field_inactiveQ.elem)),  /* prev */
        },  /* elem */
    },  /* Object_field_inactiveQ */
    {
        {
            ((ti_sysbios_knl_Queue_Elem*)((void*)&ti_sysbios_knl_Task_Module__state__V.Object_field_terminatedQ.elem)),  /* next */
            ((ti_sysbios_knl_Queue_Elem*)((void*)&ti_sysbios_knl_Task_Module__state__V.Object_field_terminatedQ.elem)),  /* prev */
        },  /* elem */
    },  /* Object_field_terminatedQ */
};

/* --> ti_sysbios_knl_Task_hooks__A */
#pragma DATA_SECTION(ti_sysbios_knl_Task_hooks__A, ".const:ti_sysbios_knl_Task_hooks__A");
const __T1_ti_sysbios_knl_Task_hooks ti_sysbios_knl_Task_hooks__A[1] = {
    {
        ((xdc_Void(*)(xdc_Int f_arg0))(ti_sysbios_utils_Load_taskRegHook__E)),  /* registerFxn */
        ((xdc_Void(*)(ti_sysbios_knl_Task_Handle f_arg0,xdc_runtime_Error_Block* f_arg1))(ti_sysbios_utils_Load_taskCreateHook__E)),  /* createFxn */
        ((xdc_Void(*)(ti_sysbios_knl_Task_Handle f_arg0))NULL),  /* readyFxn */
        ((xdc_Void(*)(ti_sysbios_knl_Task_Handle f_arg0,ti_sysbios_knl_Task_Handle f_arg1))(ti_sysbios_utils_Load_taskSwitchHook__E)),  /* switchFxn */
        ((xdc_Void(*)(ti_sysbios_knl_Task_Handle f_arg0))NULL),  /* exitFxn */
        ((xdc_Void(*)(ti_sysbios_knl_Task_Handle f_arg0))(ti_sysbios_utils_Load_taskDeleteHook__E)),  /* deleteFxn */
    },  /* [0] */
};

/* Module__diagsEnabled__C */
#pragma DATA_SECTION(ti_sysbios_knl_Task_Module__diagsEnabled__C, ".const:ti_sysbios_knl_Task_Module__diagsEnabled__C");
__FAR__ const CT__ti_sysbios_knl_Task_Module__diagsEnabled ti_sysbios_knl_Task_Module__diagsEnabled__C = (xdc_Bits32)0x90U;

/* Module__diagsIncluded__C */
#pragma DATA_SECTION(ti_sysbios_knl_Task_Module__diagsIncluded__C, ".const:ti_sysbios_knl_Task_Module__diagsIncluded__C");
__FAR__ const CT__ti_sysbios_knl_Task_Module__diagsIncluded ti_sysbios_knl_Task_Module__diagsIncluded__C = (xdc_Bits32)0x90U;

/* Module__diagsMask__C */
#pragma DATA_SECTION(ti_sysbios_knl_Task_Module__diagsMask__C, ".const:ti_sysbios_knl_Task_Module__diagsMask__C");
__FAR__ const CT__ti_sysbios_knl_Task_Module__diagsMask ti_sysbios_knl_Task_Module__diagsMask__C = ((const CT__ti_sysbios_knl_Task_Module__diagsMask)NULL);

/* Module__gateObj__C */
#pragma DATA_SECTION(ti_sysbios_knl_Task_Module__gateObj__C, ".const:ti_sysbios_knl_Task_Module__gateObj__C");
__FAR__ const CT__ti_sysbios_knl_Task_Module__gateObj ti_sysbios_knl_Task_Module__gateObj__C = ((const CT__ti_sysbios_knl_Task_Module__gateObj)NULL);

/* Module__gatePrms__C */
#pragma DATA_SECTION(ti_sysbios_knl_Task_Module__gatePrms__C, ".const:ti_sysbios_knl_Task_Module__gatePrms__C");
__FAR__ const CT__ti_sysbios_knl_Task_Module__gatePrms ti_sysbios_knl_Task_Module__gatePrms__C = ((const CT__ti_sysbios_knl_Task_Module__gatePrms)NULL);

/* Module__id__C */
#pragma DATA_SECTION(ti_sysbios_knl_Task_Module__id__C, ".const:ti_sysbios_knl_Task_Module__id__C");
__FAR__ const CT__ti_sysbios_knl_Task_Module__id ti_sysbios_knl_Task_Module__id__C = (xdc_Bits16)0x8027U;

/* Module__loggerDefined__C */
#pragma DATA_SECTION(ti_sysbios_knl_Task_Module__loggerDefined__C, ".const:ti_sysbios_knl_Task_Module__loggerDefined__C");
__FAR__ const CT__ti_sysbios_knl_Task_Module__loggerDefined ti_sysbios_knl_Task_Module__loggerDefined__C = 0;

/* Module__loggerObj__C */
#pragma DATA_SECTION(ti_sysbios_knl_Task_Module__loggerObj__C, ".const:ti_sysbios_knl_Task_Module__loggerObj__C");
__FAR__ const CT__ti_sysbios_knl_Task_Module__loggerObj ti_sysbios_knl_Task_Module__loggerObj__C = ((const CT__ti_sysbios_knl_Task_Module__loggerObj)NULL);

/* Module__loggerFxn0__C */
#pragma DATA_SECTION(ti_sysbios_knl_Task_Module__loggerFxn0__C, ".const:ti_sysbios_knl_Task_Module__loggerFxn0__C");
__FAR__ const CT__ti_sysbios_knl_Task_Module__loggerFxn0 ti_sysbios_knl_Task_Module__loggerFxn0__C = ((const CT__ti_sysbios_knl_Task_Module__loggerFxn0)NULL);

/* Module__loggerFxn1__C */
#pragma DATA_SECTION(ti_sysbios_knl_Task_Module__loggerFxn1__C, ".const:ti_sysbios_knl_Task_Module__loggerFxn1__C");
__FAR__ const CT__ti_sysbios_knl_Task_Module__loggerFxn1 ti_sysbios_knl_Task_Module__loggerFxn1__C = ((const CT__ti_sysbios_knl_Task_Module__loggerFxn1)NULL);

/* Module__loggerFxn2__C */
#pragma DATA_SECTION(ti_sysbios_knl_Task_Module__loggerFxn2__C, ".const:ti_sysbios_knl_Task_Module__loggerFxn2__C");
__FAR__ const CT__ti_sysbios_knl_Task_Module__loggerFxn2 ti_sysbios_knl_Task_Module__loggerFxn2__C = ((const CT__ti_sysbios_knl_Task_Module__loggerFxn2)NULL);

/* Module__loggerFxn4__C */
#pragma DATA_SECTION(ti_sysbios_knl_Task_Module__loggerFxn4__C, ".const:ti_sysbios_knl_Task_Module__loggerFxn4__C");
__FAR__ const CT__ti_sysbios_knl_Task_Module__loggerFxn4 ti_sysbios_knl_Task_Module__loggerFxn4__C = ((const CT__ti_sysbios_knl_Task_Module__loggerFxn4)NULL);

/* Module__loggerFxn8__C */
#pragma DATA_SECTION(ti_sysbios_knl_Task_Module__loggerFxn8__C, ".const:ti_sysbios_knl_Task_Module__loggerFxn8__C");
__FAR__ const CT__ti_sysbios_knl_Task_Module__loggerFxn8 ti_sysbios_knl_Task_Module__loggerFxn8__C = ((const CT__ti_sysbios_knl_Task_Module__loggerFxn8)NULL);

/* Object__count__C */
#pragma DATA_SECTION(ti_sysbios_knl_Task_Object__count__C, ".const:ti_sysbios_knl_Task_Object__count__C");
__FAR__ const CT__ti_sysbios_knl_Task_Object__count ti_sysbios_knl_Task_Object__count__C = 1;

/* Object__heap__C */
#pragma DATA_SECTION(ti_sysbios_knl_Task_Object__heap__C, ".const:ti_sysbios_knl_Task_Object__heap__C");
__FAR__ const CT__ti_sysbios_knl_Task_Object__heap ti_sysbios_knl_Task_Object__heap__C = NULL;

/* Object__sizeof__C */
#pragma DATA_SECTION(ti_sysbios_knl_Task_Object__sizeof__C, ".const:ti_sysbios_knl_Task_Object__sizeof__C");
__FAR__ const CT__ti_sysbios_knl_Task_Object__sizeof ti_sysbios_knl_Task_Object__sizeof__C = sizeof(ti_sysbios_knl_Task_Object__);

/* Object__table__C */
#pragma DATA_SECTION(ti_sysbios_knl_Task_Object__table__C, ".const:ti_sysbios_knl_Task_Object__table__C");
__FAR__ const CT__ti_sysbios_knl_Task_Object__table ti_sysbios_knl_Task_Object__table__C = ti_sysbios_knl_Task_Object__table__V;

/* LM_switch__C */
#pragma DATA_SECTION(ti_sysbios_knl_Task_LM_switch__C, ".const:ti_sysbios_knl_Task_LM_switch__C");
__FAR__ const CT__ti_sysbios_knl_Task_LM_switch ti_sysbios_knl_Task_LM_switch__C = (((xdc_runtime_Log_Event)6244) << 16 | 768);

/* LM_sleep__C */
#pragma DATA_SECTION(ti_sysbios_knl_Task_LM_sleep__C, ".const:ti_sysbios_knl_Task_LM_sleep__C");
__FAR__ const CT__ti_sysbios_knl_Task_LM_sleep ti_sysbios_knl_Task_LM_sleep__C = (((xdc_runtime_Log_Event)6312) << 16 | 768);

/* LD_ready__C */
#pragma DATA_SECTION(ti_sysbios_knl_Task_LD_ready__C, ".const:ti_sysbios_knl_Task_LD_ready__C");
__FAR__ const CT__ti_sysbios_knl_Task_LD_ready ti_sysbios_knl_Task_LD_ready__C = (((xdc_runtime_Log_Event)6357) << 16 | 512);

/* LD_block__C */
#pragma DATA_SECTION(ti_sysbios_knl_Task_LD_block__C, ".const:ti_sysbios_knl_Task_LD_block__C");
__FAR__ const CT__ti_sysbios_knl_Task_LD_block ti_sysbios_knl_Task_LD_block__C = (((xdc_runtime_Log_Event)6398) << 16 | 512);

/* LM_yield__C */
#pragma DATA_SECTION(ti_sysbios_knl_Task_LM_yield__C, ".const:ti_sysbios_knl_Task_LM_yield__C");
__FAR__ const CT__ti_sysbios_knl_Task_LM_yield ti_sysbios_knl_Task_LM_yield__C = (((xdc_runtime_Log_Event)6430) << 16 | 768);

/* LM_setPri__C */
#pragma DATA_SECTION(ti_sysbios_knl_Task_LM_setPri__C, ".const:ti_sysbios_knl_Task_LM_setPri__C");
__FAR__ const CT__ti_sysbios_knl_Task_LM_setPri ti_sysbios_knl_Task_LM_setPri__C = (((xdc_runtime_Log_Event)6478) << 16 | 768);

/* LD_exit__C */
#pragma DATA_SECTION(ti_sysbios_knl_Task_LD_exit__C, ".const:ti_sysbios_knl_Task_LD_exit__C");
__FAR__ const CT__ti_sysbios_knl_Task_LD_exit ti_sysbios_knl_Task_LD_exit__C = (((xdc_runtime_Log_Event)6534) << 16 | 512);

/* LM_setAffinity__C */
#pragma DATA_SECTION(ti_sysbios_knl_Task_LM_setAffinity__C, ".const:ti_sysbios_knl_Task_LM_setAffinity__C");
__FAR__ const CT__ti_sysbios_knl_Task_LM_setAffinity ti_sysbios_knl_Task_LM_setAffinity__C = (((xdc_runtime_Log_Event)6565) << 16 | 768);

/* LM_schedule__C */
#pragma DATA_SECTION(ti_sysbios_knl_Task_LM_schedule__C, ".const:ti_sysbios_knl_Task_LM_schedule__C");
__FAR__ const CT__ti_sysbios_knl_Task_LM_schedule ti_sysbios_knl_Task_LM_schedule__C = (((xdc_runtime_Log_Event)6648) << 16 | 1024);

/* LM_noWork__C */
#pragma DATA_SECTION(ti_sysbios_knl_Task_LM_noWork__C, ".const:ti_sysbios_knl_Task_LM_noWork__C");
__FAR__ const CT__ti_sysbios_knl_Task_LM_noWork ti_sysbios_knl_Task_LM_noWork__C = (((xdc_runtime_Log_Event)6734) << 16 | 1024);

/* E_stackOverflow__C */
#pragma DATA_SECTION(ti_sysbios_knl_Task_E_stackOverflow__C, ".const:ti_sysbios_knl_Task_E_stackOverflow__C");
__FAR__ const CT__ti_sysbios_knl_Task_E_stackOverflow ti_sysbios_knl_Task_E_stackOverflow__C = (((xdc_runtime_Error_Id)4677) << 16 | 0U);

/* E_spOutOfBounds__C */
#pragma DATA_SECTION(ti_sysbios_knl_Task_E_spOutOfBounds__C, ".const:ti_sysbios_knl_Task_E_spOutOfBounds__C");
__FAR__ const CT__ti_sysbios_knl_Task_E_spOutOfBounds ti_sysbios_knl_Task_E_spOutOfBounds__C = (((xdc_runtime_Error_Id)4720) << 16 | 0U);

/* E_deleteNotAllowed__C */
#pragma DATA_SECTION(ti_sysbios_knl_Task_E_deleteNotAllowed__C, ".const:ti_sysbios_knl_Task_E_deleteNotAllowed__C");
__FAR__ const CT__ti_sysbios_knl_Task_E_deleteNotAllowed ti_sysbios_knl_Task_E_deleteNotAllowed__C = (((xdc_runtime_Error_Id)4771) << 16 | 0U);

/* E_moduleStateCheckFailed__C */
#pragma DATA_SECTION(ti_sysbios_knl_Task_E_moduleStateCheckFailed__C, ".const:ti_sysbios_knl_Task_E_moduleStateCheckFailed__C");
__FAR__ const CT__ti_sysbios_knl_Task_E_moduleStateCheckFailed ti_sysbios_knl_Task_E_moduleStateCheckFailed__C = (((xdc_runtime_Error_Id)4802) << 16 | 0U);

/* E_objectCheckFailed__C */
#pragma DATA_SECTION(ti_sysbios_knl_Task_E_objectCheckFailed__C, ".const:ti_sysbios_knl_Task_E_objectCheckFailed__C");
__FAR__ const CT__ti_sysbios_knl_Task_E_objectCheckFailed ti_sysbios_knl_Task_E_objectCheckFailed__C = (((xdc_runtime_Error_Id)4875) << 16 | 0U);

/* E_objectNotInKernelSpace__C */
#pragma DATA_SECTION(ti_sysbios_knl_Task_E_objectNotInKernelSpace__C, ".const:ti_sysbios_knl_Task_E_objectNotInKernelSpace__C");
__FAR__ const CT__ti_sysbios_knl_Task_E_objectNotInKernelSpace ti_sysbios_knl_Task_E_objectNotInKernelSpace__C = (((xdc_runtime_Error_Id)4942) << 16 | 0U);

/* A_badThreadType__C */
#pragma DATA_SECTION(ti_sysbios_knl_Task_A_badThreadType__C, ".const:ti_sysbios_knl_Task_A_badThreadType__C");
__FAR__ const CT__ti_sysbios_knl_Task_A_badThreadType ti_sysbios_knl_Task_A_badThreadType__C = (((xdc_runtime_Assert_Id)1704) << 16 | 16);

/* A_badTaskState__C */
#pragma DATA_SECTION(ti_sysbios_knl_Task_A_badTaskState__C, ".const:ti_sysbios_knl_Task_A_badTaskState__C");
__FAR__ const CT__ti_sysbios_knl_Task_A_badTaskState ti_sysbios_knl_Task_A_badTaskState__C = (((xdc_runtime_Assert_Id)1773) << 16 | 16);

/* A_noPendElem__C */
#pragma DATA_SECTION(ti_sysbios_knl_Task_A_noPendElem__C, ".const:ti_sysbios_knl_Task_A_noPendElem__C");
__FAR__ const CT__ti_sysbios_knl_Task_A_noPendElem ti_sysbios_knl_Task_A_noPendElem__C = (((xdc_runtime_Assert_Id)1827) << 16 | 16);

/* A_taskDisabled__C */
#pragma DATA_SECTION(ti_sysbios_knl_Task_A_taskDisabled__C, ".const:ti_sysbios_knl_Task_A_taskDisabled__C");
__FAR__ const CT__ti_sysbios_knl_Task_A_taskDisabled ti_sysbios_knl_Task_A_taskDisabled__C = (((xdc_runtime_Assert_Id)1881) << 16 | 16);

/* A_badPriority__C */
#pragma DATA_SECTION(ti_sysbios_knl_Task_A_badPriority__C, ".const:ti_sysbios_knl_Task_A_badPriority__C");
__FAR__ const CT__ti_sysbios_knl_Task_A_badPriority ti_sysbios_knl_Task_A_badPriority__C = (((xdc_runtime_Assert_Id)1944) << 16 | 16);

/* A_badTimeout__C */
#pragma DATA_SECTION(ti_sysbios_knl_Task_A_badTimeout__C, ".const:ti_sysbios_knl_Task_A_badTimeout__C");
__FAR__ const CT__ti_sysbios_knl_Task_A_badTimeout ti_sysbios_knl_Task_A_badTimeout__C = (((xdc_runtime_Assert_Id)1994) << 16 | 16);

/* A_badAffinity__C */
#pragma DATA_SECTION(ti_sysbios_knl_Task_A_badAffinity__C, ".const:ti_sysbios_knl_Task_A_badAffinity__C");
__FAR__ const CT__ti_sysbios_knl_Task_A_badAffinity ti_sysbios_knl_Task_A_badAffinity__C = (((xdc_runtime_Assert_Id)2029) << 16 | 16);

/* A_sleepTaskDisabled__C */
#pragma DATA_SECTION(ti_sysbios_knl_Task_A_sleepTaskDisabled__C, ".const:ti_sysbios_knl_Task_A_sleepTaskDisabled__C");
__FAR__ const CT__ti_sysbios_knl_Task_A_sleepTaskDisabled ti_sysbios_knl_Task_A_sleepTaskDisabled__C = (((xdc_runtime_Assert_Id)2062) << 16 | 16);

/* A_invalidCoreId__C */
#pragma DATA_SECTION(ti_sysbios_knl_Task_A_invalidCoreId__C, ".const:ti_sysbios_knl_Task_A_invalidCoreId__C");
__FAR__ const CT__ti_sysbios_knl_Task_A_invalidCoreId ti_sysbios_knl_Task_A_invalidCoreId__C = (((xdc_runtime_Assert_Id)2146) << 16 | 16);

/* A_badContextId__C */
#pragma DATA_SECTION(ti_sysbios_knl_Task_A_badContextId__C, ".const:ti_sysbios_knl_Task_A_badContextId__C");
__FAR__ const CT__ti_sysbios_knl_Task_A_badContextId ti_sysbios_knl_Task_A_badContextId__C = (((xdc_runtime_Assert_Id)1655) << 16 | 16);

/* numPriorities__C */
#pragma DATA_SECTION(ti_sysbios_knl_Task_numPriorities__C, ".const:ti_sysbios_knl_Task_numPriorities__C");
__FAR__ const CT__ti_sysbios_knl_Task_numPriorities ti_sysbios_knl_Task_numPriorities__C = (xdc_UInt)0x10U;

/* defaultStackSize__C */
#pragma DATA_SECTION(ti_sysbios_knl_Task_defaultStackSize__C, ".const:ti_sysbios_knl_Task_defaultStackSize__C");
__FAR__ const CT__ti_sysbios_knl_Task_defaultStackSize ti_sysbios_knl_Task_defaultStackSize__C = (xdc_SizeT)0x4000;

/* defaultStackHeap__C */
#pragma DATA_SECTION(ti_sysbios_knl_Task_defaultStackHeap__C, ".const:ti_sysbios_knl_Task_defaultStackHeap__C");
__FAR__ const CT__ti_sysbios_knl_Task_defaultStackHeap ti_sysbios_knl_Task_defaultStackHeap__C = 0;

/* allBlockedFunc__C */
#pragma DATA_SECTION(ti_sysbios_knl_Task_allBlockedFunc__C, ".const:ti_sysbios_knl_Task_allBlockedFunc__C");
__FAR__ const CT__ti_sysbios_knl_Task_allBlockedFunc ti_sysbios_knl_Task_allBlockedFunc__C = ((const CT__ti_sysbios_knl_Task_allBlockedFunc)NULL);

/* initStackFlag__C */
#pragma DATA_SECTION(ti_sysbios_knl_Task_initStackFlag__C, ".const:ti_sysbios_knl_Task_initStackFlag__C");
__FAR__ const CT__ti_sysbios_knl_Task_initStackFlag ti_sysbios_knl_Task_initStackFlag__C = 1;

/* checkStackFlag__C */
#pragma DATA_SECTION(ti_sysbios_knl_Task_checkStackFlag__C, ".const:ti_sysbios_knl_Task_checkStackFlag__C");
__FAR__ const CT__ti_sysbios_knl_Task_checkStackFlag ti_sysbios_knl_Task_checkStackFlag__C = 1;

/* deleteTerminatedTasks__C */
#pragma DATA_SECTION(ti_sysbios_knl_Task_deleteTerminatedTasks__C, ".const:ti_sysbios_knl_Task_deleteTerminatedTasks__C");
__FAR__ const CT__ti_sysbios_knl_Task_deleteTerminatedTasks ti_sysbios_knl_Task_deleteTerminatedTasks__C = 0;

/* hooks__C */
#pragma DATA_SECTION(ti_sysbios_knl_Task_hooks__C, ".const:ti_sysbios_knl_Task_hooks__C");
__FAR__ const CT__ti_sysbios_knl_Task_hooks ti_sysbios_knl_Task_hooks__C = {1, ((__T1_ti_sysbios_knl_Task_hooks const  *)ti_sysbios_knl_Task_hooks__A)};

/* moduleStateCheckFxn__C */
#pragma DATA_SECTION(ti_sysbios_knl_Task_moduleStateCheckFxn__C, ".const:ti_sysbios_knl_Task_moduleStateCheckFxn__C");
__FAR__ const CT__ti_sysbios_knl_Task_moduleStateCheckFxn ti_sysbios_knl_Task_moduleStateCheckFxn__C = ((const CT__ti_sysbios_knl_Task_moduleStateCheckFxn)(ti_sysbios_knl_Task_moduleStateCheck__I));

/* moduleStateCheckValueFxn__C */
#pragma DATA_SECTION(ti_sysbios_knl_Task_moduleStateCheckValueFxn__C, ".const:ti_sysbios_knl_Task_moduleStateCheckValueFxn__C");
__FAR__ const CT__ti_sysbios_knl_Task_moduleStateCheckValueFxn ti_sysbios_knl_Task_moduleStateCheckValueFxn__C = ((const CT__ti_sysbios_knl_Task_moduleStateCheckValueFxn)(ti_sysbios_knl_Task_getModuleStateCheckValue__I));

/* moduleStateCheckFlag__C */
#pragma DATA_SECTION(ti_sysbios_knl_Task_moduleStateCheckFlag__C, ".const:ti_sysbios_knl_Task_moduleStateCheckFlag__C");
__FAR__ const CT__ti_sysbios_knl_Task_moduleStateCheckFlag ti_sysbios_knl_Task_moduleStateCheckFlag__C = 0;

/* objectCheckFxn__C */
#pragma DATA_SECTION(ti_sysbios_knl_Task_objectCheckFxn__C, ".const:ti_sysbios_knl_Task_objectCheckFxn__C");
__FAR__ const CT__ti_sysbios_knl_Task_objectCheckFxn ti_sysbios_knl_Task_objectCheckFxn__C = ((const CT__ti_sysbios_knl_Task_objectCheckFxn)(ti_sysbios_knl_Task_objectCheck__I));

/* objectCheckValueFxn__C */
#pragma DATA_SECTION(ti_sysbios_knl_Task_objectCheckValueFxn__C, ".const:ti_sysbios_knl_Task_objectCheckValueFxn__C");
__FAR__ const CT__ti_sysbios_knl_Task_objectCheckValueFxn ti_sysbios_knl_Task_objectCheckValueFxn__C = ((const CT__ti_sysbios_knl_Task_objectCheckValueFxn)(ti_sysbios_knl_Task_getObjectCheckValue__I));

/* objectCheckFlag__C */
#pragma DATA_SECTION(ti_sysbios_knl_Task_objectCheckFlag__C, ".const:ti_sysbios_knl_Task_objectCheckFlag__C");
__FAR__ const CT__ti_sysbios_knl_Task_objectCheckFlag ti_sysbios_knl_Task_objectCheckFlag__C = 0;

/* numConstructedTasks__C */
#pragma DATA_SECTION(ti_sysbios_knl_Task_numConstructedTasks__C, ".const:ti_sysbios_knl_Task_numConstructedTasks__C");
__FAR__ const CT__ti_sysbios_knl_Task_numConstructedTasks ti_sysbios_knl_Task_numConstructedTasks__C = (xdc_UInt)0x0U;

/* startupHookFunc__C */
#pragma DATA_SECTION(ti_sysbios_knl_Task_startupHookFunc__C, ".const:ti_sysbios_knl_Task_startupHookFunc__C");
__FAR__ const CT__ti_sysbios_knl_Task_startupHookFunc ti_sysbios_knl_Task_startupHookFunc__C = ((const CT__ti_sysbios_knl_Task_startupHookFunc)NULL);


/*
 * ======== ti.sysbios.knl.Task_SupportProxy INITIALIZERS ========
 */


/*
 * ======== ti.sysbios.syncs.SyncSem INITIALIZERS ========
 */

/* Object__DESC__C */
__FAR__ const xdc_runtime_Core_ObjDesc ti_sysbios_syncs_SyncSem_Object__DESC__C;

/* Object__PARAMS__C */
#pragma DATA_SECTION(ti_sysbios_syncs_SyncSem_Object__PARAMS__C, ".const:ti_sysbios_syncs_SyncSem_Object__PARAMS__C");
__FAR__ const ti_sysbios_syncs_SyncSem_Params ti_sysbios_syncs_SyncSem_Object__PARAMS__C = {
    sizeof (ti_sysbios_syncs_SyncSem_Params), /* __size */
    NULL, /* __self */
    NULL, /* __fxns */
    (xdc_runtime_IInstance_Params*)&ti_sysbios_syncs_SyncSem_Object__PARAMS__C.__iprms, /* instance */
    0,  /* sem */
    {
        sizeof (xdc_runtime_IInstance_Params), /* __size */
        0,  /* name */
    },  /* instance */
};

/* Module__root__V */
ti_sysbios_syncs_SyncSem_Module__ ti_sysbios_syncs_SyncSem_Module__root__V = {
    {&ti_sysbios_syncs_SyncSem_Module__root__V.link,  /* link.next */
    &ti_sysbios_syncs_SyncSem_Module__root__V.link},  /* link.prev */
};

/* Module__diagsEnabled__C */
#pragma DATA_SECTION(ti_sysbios_syncs_SyncSem_Module__diagsEnabled__C, ".const:ti_sysbios_syncs_SyncSem_Module__diagsEnabled__C");
__FAR__ const CT__ti_sysbios_syncs_SyncSem_Module__diagsEnabled ti_sysbios_syncs_SyncSem_Module__diagsEnabled__C = (xdc_Bits32)0x90U;

/* Module__diagsIncluded__C */
#pragma DATA_SECTION(ti_sysbios_syncs_SyncSem_Module__diagsIncluded__C, ".const:ti_sysbios_syncs_SyncSem_Module__diagsIncluded__C");
__FAR__ const CT__ti_sysbios_syncs_SyncSem_Module__diagsIncluded ti_sysbios_syncs_SyncSem_Module__diagsIncluded__C = (xdc_Bits32)0x90U;

/* Module__diagsMask__C */
#pragma DATA_SECTION(ti_sysbios_syncs_SyncSem_Module__diagsMask__C, ".const:ti_sysbios_syncs_SyncSem_Module__diagsMask__C");
__FAR__ const CT__ti_sysbios_syncs_SyncSem_Module__diagsMask ti_sysbios_syncs_SyncSem_Module__diagsMask__C = ((const CT__ti_sysbios_syncs_SyncSem_Module__diagsMask)NULL);

/* Module__gateObj__C */
#pragma DATA_SECTION(ti_sysbios_syncs_SyncSem_Module__gateObj__C, ".const:ti_sysbios_syncs_SyncSem_Module__gateObj__C");
__FAR__ const CT__ti_sysbios_syncs_SyncSem_Module__gateObj ti_sysbios_syncs_SyncSem_Module__gateObj__C = ((const CT__ti_sysbios_syncs_SyncSem_Module__gateObj)NULL);

/* Module__gatePrms__C */
#pragma DATA_SECTION(ti_sysbios_syncs_SyncSem_Module__gatePrms__C, ".const:ti_sysbios_syncs_SyncSem_Module__gatePrms__C");
__FAR__ const CT__ti_sysbios_syncs_SyncSem_Module__gatePrms ti_sysbios_syncs_SyncSem_Module__gatePrms__C = ((const CT__ti_sysbios_syncs_SyncSem_Module__gatePrms)NULL);

/* Module__id__C */
#pragma DATA_SECTION(ti_sysbios_syncs_SyncSem_Module__id__C, ".const:ti_sysbios_syncs_SyncSem_Module__id__C");
__FAR__ const CT__ti_sysbios_syncs_SyncSem_Module__id ti_sysbios_syncs_SyncSem_Module__id__C = (xdc_Bits16)0x8034U;

/* Module__loggerDefined__C */
#pragma DATA_SECTION(ti_sysbios_syncs_SyncSem_Module__loggerDefined__C, ".const:ti_sysbios_syncs_SyncSem_Module__loggerDefined__C");
__FAR__ const CT__ti_sysbios_syncs_SyncSem_Module__loggerDefined ti_sysbios_syncs_SyncSem_Module__loggerDefined__C = 0;

/* Module__loggerObj__C */
#pragma DATA_SECTION(ti_sysbios_syncs_SyncSem_Module__loggerObj__C, ".const:ti_sysbios_syncs_SyncSem_Module__loggerObj__C");
__FAR__ const CT__ti_sysbios_syncs_SyncSem_Module__loggerObj ti_sysbios_syncs_SyncSem_Module__loggerObj__C = ((const CT__ti_sysbios_syncs_SyncSem_Module__loggerObj)NULL);

/* Module__loggerFxn0__C */
#pragma DATA_SECTION(ti_sysbios_syncs_SyncSem_Module__loggerFxn0__C, ".const:ti_sysbios_syncs_SyncSem_Module__loggerFxn0__C");
__FAR__ const CT__ti_sysbios_syncs_SyncSem_Module__loggerFxn0 ti_sysbios_syncs_SyncSem_Module__loggerFxn0__C = ((const CT__ti_sysbios_syncs_SyncSem_Module__loggerFxn0)NULL);

/* Module__loggerFxn1__C */
#pragma DATA_SECTION(ti_sysbios_syncs_SyncSem_Module__loggerFxn1__C, ".const:ti_sysbios_syncs_SyncSem_Module__loggerFxn1__C");
__FAR__ const CT__ti_sysbios_syncs_SyncSem_Module__loggerFxn1 ti_sysbios_syncs_SyncSem_Module__loggerFxn1__C = ((const CT__ti_sysbios_syncs_SyncSem_Module__loggerFxn1)NULL);

/* Module__loggerFxn2__C */
#pragma DATA_SECTION(ti_sysbios_syncs_SyncSem_Module__loggerFxn2__C, ".const:ti_sysbios_syncs_SyncSem_Module__loggerFxn2__C");
__FAR__ const CT__ti_sysbios_syncs_SyncSem_Module__loggerFxn2 ti_sysbios_syncs_SyncSem_Module__loggerFxn2__C = ((const CT__ti_sysbios_syncs_SyncSem_Module__loggerFxn2)NULL);

/* Module__loggerFxn4__C */
#pragma DATA_SECTION(ti_sysbios_syncs_SyncSem_Module__loggerFxn4__C, ".const:ti_sysbios_syncs_SyncSem_Module__loggerFxn4__C");
__FAR__ const CT__ti_sysbios_syncs_SyncSem_Module__loggerFxn4 ti_sysbios_syncs_SyncSem_Module__loggerFxn4__C = ((const CT__ti_sysbios_syncs_SyncSem_Module__loggerFxn4)NULL);

/* Module__loggerFxn8__C */
#pragma DATA_SECTION(ti_sysbios_syncs_SyncSem_Module__loggerFxn8__C, ".const:ti_sysbios_syncs_SyncSem_Module__loggerFxn8__C");
__FAR__ const CT__ti_sysbios_syncs_SyncSem_Module__loggerFxn8 ti_sysbios_syncs_SyncSem_Module__loggerFxn8__C = ((const CT__ti_sysbios_syncs_SyncSem_Module__loggerFxn8)NULL);

/* Object__count__C */
#pragma DATA_SECTION(ti_sysbios_syncs_SyncSem_Object__count__C, ".const:ti_sysbios_syncs_SyncSem_Object__count__C");
__FAR__ const CT__ti_sysbios_syncs_SyncSem_Object__count ti_sysbios_syncs_SyncSem_Object__count__C = 0;

/* Object__heap__C */
#pragma DATA_SECTION(ti_sysbios_syncs_SyncSem_Object__heap__C, ".const:ti_sysbios_syncs_SyncSem_Object__heap__C");
__FAR__ const CT__ti_sysbios_syncs_SyncSem_Object__heap ti_sysbios_syncs_SyncSem_Object__heap__C = NULL;

/* Object__sizeof__C */
#pragma DATA_SECTION(ti_sysbios_syncs_SyncSem_Object__sizeof__C, ".const:ti_sysbios_syncs_SyncSem_Object__sizeof__C");
__FAR__ const CT__ti_sysbios_syncs_SyncSem_Object__sizeof ti_sysbios_syncs_SyncSem_Object__sizeof__C = sizeof(ti_sysbios_syncs_SyncSem_Object__);

/* Object__table__C */
#pragma DATA_SECTION(ti_sysbios_syncs_SyncSem_Object__table__C, ".const:ti_sysbios_syncs_SyncSem_Object__table__C");
__FAR__ const CT__ti_sysbios_syncs_SyncSem_Object__table ti_sysbios_syncs_SyncSem_Object__table__C = NULL;


/*
 * ======== ti.sysbios.timers.dmtimer.Timer INITIALIZERS ========
 */

/* Object__DESC__C */
__FAR__ const xdc_runtime_Core_ObjDesc ti_sysbios_timers_dmtimer_Timer_Object__DESC__C;

/* Object__PARAMS__C */
#pragma DATA_SECTION(ti_sysbios_timers_dmtimer_Timer_Object__PARAMS__C, ".const:ti_sysbios_timers_dmtimer_Timer_Object__PARAMS__C");
__FAR__ const ti_sysbios_timers_dmtimer_Timer_Params ti_sysbios_timers_dmtimer_Timer_Object__PARAMS__C = {
    sizeof (ti_sysbios_timers_dmtimer_Timer_Params), /* __size */
    NULL, /* __self */
    NULL, /* __fxns */
    (xdc_runtime_IInstance_Params*)&ti_sysbios_timers_dmtimer_Timer_Object__PARAMS__C.__iprms, /* instance */
    ti_sysbios_interfaces_ITimer_RunMode_CONTINUOUS,  /* runMode */
    ti_sysbios_interfaces_ITimer_StartMode_AUTO,  /* startMode */
    ((xdc_UArg)NULL),  /* arg */
    (xdc_UInt32)0x0U,  /* period */
    ti_sysbios_interfaces_ITimer_PeriodType_MICROSECS,  /* periodType */
    {
        (xdc_Bits32)0x0U,  /* hi */
        (xdc_Bits32)0x0U,  /* lo */
    },  /* extFreq */
    {
        (xdc_Bits8)0x0U,  /* idlemode */
        (xdc_Bits8)0x0U,  /* emufree */
        (xdc_Bits8)0x1U,  /* softreset */
    },  /* tiocpCfg */
    {
        (xdc_Bits8)0x0U,  /* mat_it_ena */
        (xdc_Bits8)0x1U,  /* ovf_it_ena */
        (xdc_Bits8)0x0U,  /* tcar_it_ena */
    },  /* tier */
    {
        (xdc_Bits8)0x0U,  /* mat_wup_ena */
        (xdc_Bits8)0x0U,  /* ovf_wup_ena */
        (xdc_Bits8)0x0U,  /* tcar_wup_ena */
    },  /* twer */
    {
        (xdc_Bits32)0x0U,  /* ptv */
        (xdc_Bits8)0x0U,  /* pre */
        (xdc_Bits8)0x0U,  /* ce */
        (xdc_Bits8)0x0U,  /* scpwm */
        (xdc_Bits16)0x0U,  /* tcm */
        (xdc_Bits16)0x0U,  /* trg */
        (xdc_Bits8)0x0U,  /* pt */
        (xdc_Bits8)0x0U,  /* captmode */
        (xdc_Bits8)0x0U,  /* gpocfg */
    },  /* tclr */
    {
        (xdc_Bits8)0x0U,  /* sft */
        (xdc_Bits8)0x0U,  /* posted */
        (xdc_Bits8)0x0U,  /* readmode */
    },  /* tsicr */
    (xdc_UInt32)0x0U,  /* tmar */
    (xdc_Int)(-0x0 - 1),  /* intNum */
    (xdc_Int)(-0x0 - 1),  /* eventId */
    ((ti_sysbios_hal_Hwi_Params*)NULL),  /* hwiParams */
    {
        sizeof (xdc_runtime_IInstance_Params), /* __size */
        0,  /* name */
    },  /* instance */
};

/* Module__root__V */
ti_sysbios_timers_dmtimer_Timer_Module__ ti_sysbios_timers_dmtimer_Timer_Module__root__V = {
    {&ti_sysbios_timers_dmtimer_Timer_Module__root__V.link,  /* link.next */
    &ti_sysbios_timers_dmtimer_Timer_Module__root__V.link},  /* link.prev */
};

/* Object__table__V */
ti_sysbios_timers_dmtimer_Timer_Object__ ti_sysbios_timers_dmtimer_Timer_Object__table__V[1] = {
    {/* instance#0 */
        0,
        1,  /* staticInst */
        (xdc_Int)0x0,  /* id */
        (xdc_UInt)0x1U,  /* tiocpCfg */
        (xdc_UInt)0x0U,  /* tmar */
        (xdc_UInt)0x2U,  /* tier */
        (xdc_UInt)0x0U,  /* twer */
        (xdc_UInt)0x0U,  /* tclr */
        (xdc_UInt)0x0U,  /* tsicr */
        ti_sysbios_interfaces_ITimer_RunMode_CONTINUOUS,  /* runMode */
        ti_sysbios_interfaces_ITimer_StartMode_AUTO,  /* startMode */
        (xdc_UInt)0x3e8U,  /* period */
        ti_sysbios_interfaces_ITimer_PeriodType_MICROSECS,  /* periodType */
        (xdc_UInt)0xfU,  /* intNum */
        (xdc_Int)0x4e1,  /* eventId */
        ((xdc_UArg)NULL),  /* arg */
        ((xdc_Void(*)(xdc_UArg f_arg0))(ti_sysbios_knl_Clock_doTick__I)),  /* tickFxn */
        {
            (xdc_Bits32)0x0U,  /* hi */
            (xdc_Bits32)0x0U,  /* lo */
        },  /* extFreq */
        (ti_sysbios_hal_Hwi_Handle)&ti_sysbios_hal_Hwi_Object__table__V[0],  /* hwi */
        (xdc_UInt)0x0U,  /* prevThreshold */
        (xdc_UInt)0x0U,  /* rollovers */
        (xdc_UInt)0x0U,  /* savedCurrCount */
        1,  /* useDefaultEventId */
    },
};

/* --> ti_sysbios_timers_dmtimer_Timer_Module_State_0_intFreqs__A */
__T1_ti_sysbios_timers_dmtimer_Timer_Module_State__intFreqs ti_sysbios_timers_dmtimer_Timer_Module_State_0_intFreqs__A[4] = {
    {
        (xdc_Bits32)0x0U,  /* hi */
        (xdc_Bits32)0x124f800U,  /* lo */
    },  /* [0] */
    {
        (xdc_Bits32)0x0U,  /* hi */
        (xdc_Bits32)0x124f800U,  /* lo */
    },  /* [1] */
    {
        (xdc_Bits32)0x0U,  /* hi */
        (xdc_Bits32)0x124f800U,  /* lo */
    },  /* [2] */
    {
        (xdc_Bits32)0x0U,  /* hi */
        (xdc_Bits32)0x124f800U,  /* lo */
    },  /* [3] */
};

/* --> ti_sysbios_timers_dmtimer_Timer_Module_State_0_device__A */
__T1_ti_sysbios_timers_dmtimer_Timer_Module_State__device ti_sysbios_timers_dmtimer_Timer_Module_State_0_device__A[4] = {
    {
        (xdc_Int)0xf,  /* intNum */
        (xdc_Int)0x4e1,  /* eventId */
        ((xdc_Ptr)(0x2410000)),  /* baseAddr */
    },  /* [0] */
    {
        (xdc_Int)0xf,  /* intNum */
        (xdc_Int)0x4e1,  /* eventId */
        ((xdc_Ptr)((void*)0x2410000)),  /* baseAddr */
    },  /* [1] */
    {
        (xdc_Int)0x10,  /* intNum */
        (xdc_Int)0x4e2,  /* eventId */
        ((xdc_Ptr)((void*)0x2420000)),  /* baseAddr */
    },  /* [2] */
    {
        (xdc_Int)0x11,  /* intNum */
        (xdc_Int)0x4e3,  /* eventId */
        ((xdc_Ptr)((void*)0x2430000)),  /* baseAddr */
    },  /* [3] */
};

/* --> ti_sysbios_timers_dmtimer_Timer_Module_State_0_handles__A */
__T1_ti_sysbios_timers_dmtimer_Timer_Module_State__handles ti_sysbios_timers_dmtimer_Timer_Module_State_0_handles__A[4] = {
    (ti_sysbios_timers_dmtimer_Timer_Handle)&ti_sysbios_timers_dmtimer_Timer_Object__table__V[0],  /* [0] */
    0,  /* [1] */
    0,  /* [2] */
    0,  /* [3] */
};

/* Module__state__V */
#ifdef __ti__
ti_sysbios_timers_dmtimer_Timer_Module_State__ ti_sysbios_timers_dmtimer_Timer_Module__state__V __attribute__ ((section(".data:ti_sysbios_timers_dmtimer_Timer_Module__state__V")));
#elif !(defined(__MACH__) && defined(__APPLE__))
ti_sysbios_timers_dmtimer_Timer_Module_State__ ti_sysbios_timers_dmtimer_Timer_Module__state__V __attribute__ ((section(".data_ti_sysbios_timers_dmtimer_Timer_Module__state__V")));
#endif
ti_sysbios_timers_dmtimer_Timer_Module_State__ ti_sysbios_timers_dmtimer_Timer_Module__state__V = {
    (xdc_Bits32)0xeU,  /* availMask */
    ((void*)ti_sysbios_timers_dmtimer_Timer_Module_State_0_intFreqs__A),  /* intFreqs */
    ((void*)ti_sysbios_timers_dmtimer_Timer_Module_State_0_device__A),  /* device */
    ((void*)ti_sysbios_timers_dmtimer_Timer_Module_State_0_handles__A),  /* handles */
    1,  /* firstInit */
};

/* Module__diagsEnabled__C */
#pragma DATA_SECTION(ti_sysbios_timers_dmtimer_Timer_Module__diagsEnabled__C, ".const:ti_sysbios_timers_dmtimer_Timer_Module__diagsEnabled__C");
__FAR__ const CT__ti_sysbios_timers_dmtimer_Timer_Module__diagsEnabled ti_sysbios_timers_dmtimer_Timer_Module__diagsEnabled__C = (xdc_Bits32)0x90U;

/* Module__diagsIncluded__C */
#pragma DATA_SECTION(ti_sysbios_timers_dmtimer_Timer_Module__diagsIncluded__C, ".const:ti_sysbios_timers_dmtimer_Timer_Module__diagsIncluded__C");
__FAR__ const CT__ti_sysbios_timers_dmtimer_Timer_Module__diagsIncluded ti_sysbios_timers_dmtimer_Timer_Module__diagsIncluded__C = (xdc_Bits32)0x90U;

/* Module__diagsMask__C */
#pragma DATA_SECTION(ti_sysbios_timers_dmtimer_Timer_Module__diagsMask__C, ".const:ti_sysbios_timers_dmtimer_Timer_Module__diagsMask__C");
__FAR__ const CT__ti_sysbios_timers_dmtimer_Timer_Module__diagsMask ti_sysbios_timers_dmtimer_Timer_Module__diagsMask__C = ((const CT__ti_sysbios_timers_dmtimer_Timer_Module__diagsMask)NULL);

/* Module__gateObj__C */
#pragma DATA_SECTION(ti_sysbios_timers_dmtimer_Timer_Module__gateObj__C, ".const:ti_sysbios_timers_dmtimer_Timer_Module__gateObj__C");
__FAR__ const CT__ti_sysbios_timers_dmtimer_Timer_Module__gateObj ti_sysbios_timers_dmtimer_Timer_Module__gateObj__C = ((const CT__ti_sysbios_timers_dmtimer_Timer_Module__gateObj)NULL);

/* Module__gatePrms__C */
#pragma DATA_SECTION(ti_sysbios_timers_dmtimer_Timer_Module__gatePrms__C, ".const:ti_sysbios_timers_dmtimer_Timer_Module__gatePrms__C");
__FAR__ const CT__ti_sysbios_timers_dmtimer_Timer_Module__gatePrms ti_sysbios_timers_dmtimer_Timer_Module__gatePrms__C = ((const CT__ti_sysbios_timers_dmtimer_Timer_Module__gatePrms)NULL);

/* Module__id__C */
#pragma DATA_SECTION(ti_sysbios_timers_dmtimer_Timer_Module__id__C, ".const:ti_sysbios_timers_dmtimer_Timer_Module__id__C");
__FAR__ const CT__ti_sysbios_timers_dmtimer_Timer_Module__id ti_sysbios_timers_dmtimer_Timer_Module__id__C = (xdc_Bits16)0x803fU;

/* Module__loggerDefined__C */
#pragma DATA_SECTION(ti_sysbios_timers_dmtimer_Timer_Module__loggerDefined__C, ".const:ti_sysbios_timers_dmtimer_Timer_Module__loggerDefined__C");
__FAR__ const CT__ti_sysbios_timers_dmtimer_Timer_Module__loggerDefined ti_sysbios_timers_dmtimer_Timer_Module__loggerDefined__C = 0;

/* Module__loggerObj__C */
#pragma DATA_SECTION(ti_sysbios_timers_dmtimer_Timer_Module__loggerObj__C, ".const:ti_sysbios_timers_dmtimer_Timer_Module__loggerObj__C");
__FAR__ const CT__ti_sysbios_timers_dmtimer_Timer_Module__loggerObj ti_sysbios_timers_dmtimer_Timer_Module__loggerObj__C = ((const CT__ti_sysbios_timers_dmtimer_Timer_Module__loggerObj)NULL);

/* Module__loggerFxn0__C */
#pragma DATA_SECTION(ti_sysbios_timers_dmtimer_Timer_Module__loggerFxn0__C, ".const:ti_sysbios_timers_dmtimer_Timer_Module__loggerFxn0__C");
__FAR__ const CT__ti_sysbios_timers_dmtimer_Timer_Module__loggerFxn0 ti_sysbios_timers_dmtimer_Timer_Module__loggerFxn0__C = ((const CT__ti_sysbios_timers_dmtimer_Timer_Module__loggerFxn0)NULL);

/* Module__loggerFxn1__C */
#pragma DATA_SECTION(ti_sysbios_timers_dmtimer_Timer_Module__loggerFxn1__C, ".const:ti_sysbios_timers_dmtimer_Timer_Module__loggerFxn1__C");
__FAR__ const CT__ti_sysbios_timers_dmtimer_Timer_Module__loggerFxn1 ti_sysbios_timers_dmtimer_Timer_Module__loggerFxn1__C = ((const CT__ti_sysbios_timers_dmtimer_Timer_Module__loggerFxn1)NULL);

/* Module__loggerFxn2__C */
#pragma DATA_SECTION(ti_sysbios_timers_dmtimer_Timer_Module__loggerFxn2__C, ".const:ti_sysbios_timers_dmtimer_Timer_Module__loggerFxn2__C");
__FAR__ const CT__ti_sysbios_timers_dmtimer_Timer_Module__loggerFxn2 ti_sysbios_timers_dmtimer_Timer_Module__loggerFxn2__C = ((const CT__ti_sysbios_timers_dmtimer_Timer_Module__loggerFxn2)NULL);

/* Module__loggerFxn4__C */
#pragma DATA_SECTION(ti_sysbios_timers_dmtimer_Timer_Module__loggerFxn4__C, ".const:ti_sysbios_timers_dmtimer_Timer_Module__loggerFxn4__C");
__FAR__ const CT__ti_sysbios_timers_dmtimer_Timer_Module__loggerFxn4 ti_sysbios_timers_dmtimer_Timer_Module__loggerFxn4__C = ((const CT__ti_sysbios_timers_dmtimer_Timer_Module__loggerFxn4)NULL);

/* Module__loggerFxn8__C */
#pragma DATA_SECTION(ti_sysbios_timers_dmtimer_Timer_Module__loggerFxn8__C, ".const:ti_sysbios_timers_dmtimer_Timer_Module__loggerFxn8__C");
__FAR__ const CT__ti_sysbios_timers_dmtimer_Timer_Module__loggerFxn8 ti_sysbios_timers_dmtimer_Timer_Module__loggerFxn8__C = ((const CT__ti_sysbios_timers_dmtimer_Timer_Module__loggerFxn8)NULL);

/* Object__count__C */
#pragma DATA_SECTION(ti_sysbios_timers_dmtimer_Timer_Object__count__C, ".const:ti_sysbios_timers_dmtimer_Timer_Object__count__C");
__FAR__ const CT__ti_sysbios_timers_dmtimer_Timer_Object__count ti_sysbios_timers_dmtimer_Timer_Object__count__C = 1;

/* Object__heap__C */
#pragma DATA_SECTION(ti_sysbios_timers_dmtimer_Timer_Object__heap__C, ".const:ti_sysbios_timers_dmtimer_Timer_Object__heap__C");
__FAR__ const CT__ti_sysbios_timers_dmtimer_Timer_Object__heap ti_sysbios_timers_dmtimer_Timer_Object__heap__C = NULL;

/* Object__sizeof__C */
#pragma DATA_SECTION(ti_sysbios_timers_dmtimer_Timer_Object__sizeof__C, ".const:ti_sysbios_timers_dmtimer_Timer_Object__sizeof__C");
__FAR__ const CT__ti_sysbios_timers_dmtimer_Timer_Object__sizeof ti_sysbios_timers_dmtimer_Timer_Object__sizeof__C = sizeof(ti_sysbios_timers_dmtimer_Timer_Object__);

/* Object__table__C */
#pragma DATA_SECTION(ti_sysbios_timers_dmtimer_Timer_Object__table__C, ".const:ti_sysbios_timers_dmtimer_Timer_Object__table__C");
__FAR__ const CT__ti_sysbios_timers_dmtimer_Timer_Object__table ti_sysbios_timers_dmtimer_Timer_Object__table__C = ti_sysbios_timers_dmtimer_Timer_Object__table__V;

/* A_notAvailable__C */
#pragma DATA_SECTION(ti_sysbios_timers_dmtimer_Timer_A_notAvailable__C, ".const:ti_sysbios_timers_dmtimer_Timer_A_notAvailable__C");
__FAR__ const CT__ti_sysbios_timers_dmtimer_Timer_A_notAvailable ti_sysbios_timers_dmtimer_Timer_A_notAvailable__C = (((xdc_runtime_Assert_Id)3751) << 16 | 16);

/* E_invalidTimer__C */
#pragma DATA_SECTION(ti_sysbios_timers_dmtimer_Timer_E_invalidTimer__C, ".const:ti_sysbios_timers_dmtimer_Timer_E_invalidTimer__C");
__FAR__ const CT__ti_sysbios_timers_dmtimer_Timer_E_invalidTimer ti_sysbios_timers_dmtimer_Timer_E_invalidTimer__C = (((xdc_runtime_Error_Id)5137) << 16 | 0U);

/* E_notAvailable__C */
#pragma DATA_SECTION(ti_sysbios_timers_dmtimer_Timer_E_notAvailable__C, ".const:ti_sysbios_timers_dmtimer_Timer_E_notAvailable__C");
__FAR__ const CT__ti_sysbios_timers_dmtimer_Timer_E_notAvailable ti_sysbios_timers_dmtimer_Timer_E_notAvailable__C = (((xdc_runtime_Error_Id)5173) << 16 | 0U);

/* E_cannotSupport__C */
#pragma DATA_SECTION(ti_sysbios_timers_dmtimer_Timer_E_cannotSupport__C, ".const:ti_sysbios_timers_dmtimer_Timer_E_cannotSupport__C");
__FAR__ const CT__ti_sysbios_timers_dmtimer_Timer_E_cannotSupport ti_sysbios_timers_dmtimer_Timer_E_cannotSupport__C = (((xdc_runtime_Error_Id)5212) << 16 | 0U);

/* E_freqMismatch__C */
#pragma DATA_SECTION(ti_sysbios_timers_dmtimer_Timer_E_freqMismatch__C, ".const:ti_sysbios_timers_dmtimer_Timer_E_freqMismatch__C");
__FAR__ const CT__ti_sysbios_timers_dmtimer_Timer_E_freqMismatch ti_sysbios_timers_dmtimer_Timer_E_freqMismatch__C = (((xdc_runtime_Error_Id)5270) << 16 | 0U);

/* E_badIntNum__C */
#pragma DATA_SECTION(ti_sysbios_timers_dmtimer_Timer_E_badIntNum__C, ".const:ti_sysbios_timers_dmtimer_Timer_E_badIntNum__C");
__FAR__ const CT__ti_sysbios_timers_dmtimer_Timer_E_badIntNum ti_sysbios_timers_dmtimer_Timer_E_badIntNum__C = (((xdc_runtime_Error_Id)5405) << 16 | 0U);

/* anyMask__C */
#pragma DATA_SECTION(ti_sysbios_timers_dmtimer_Timer_anyMask__C, ".const:ti_sysbios_timers_dmtimer_Timer_anyMask__C");
__FAR__ const CT__ti_sysbios_timers_dmtimer_Timer_anyMask ti_sysbios_timers_dmtimer_Timer_anyMask__C = (xdc_Bits32)0xfU;

/* checkFrequency__C */
#pragma DATA_SECTION(ti_sysbios_timers_dmtimer_Timer_checkFrequency__C, ".const:ti_sysbios_timers_dmtimer_Timer_checkFrequency__C");
__FAR__ const CT__ti_sysbios_timers_dmtimer_Timer_checkFrequency ti_sysbios_timers_dmtimer_Timer_checkFrequency__C = 0;

/* startupNeeded__C */
#pragma DATA_SECTION(ti_sysbios_timers_dmtimer_Timer_startupNeeded__C, ".const:ti_sysbios_timers_dmtimer_Timer_startupNeeded__C");
__FAR__ const CT__ti_sysbios_timers_dmtimer_Timer_startupNeeded ti_sysbios_timers_dmtimer_Timer_startupNeeded__C = 1;

/* numTimerDevices__C */
#pragma DATA_SECTION(ti_sysbios_timers_dmtimer_Timer_numTimerDevices__C, ".const:ti_sysbios_timers_dmtimer_Timer_numTimerDevices__C");
__FAR__ const CT__ti_sysbios_timers_dmtimer_Timer_numTimerDevices ti_sysbios_timers_dmtimer_Timer_numTimerDevices__C = (xdc_Int)0x4;


/*
 * ======== ti.sysbios.timers.dmtimer.Timer_TimerSupportProxy INITIALIZERS ========
 */


/*
 * ======== ti.sysbios.utils.Load INITIALIZERS ========
 */

/* --> ti_sysbios_utils_Load_Module_State_0_taskStartTime__A */
__T1_ti_sysbios_utils_Load_Module_State__taskStartTime ti_sysbios_utils_Load_Module_State_0_taskStartTime__A[1] = {
    (xdc_UInt32)0x0U,  /* [0] */
};

/* --> ti_sysbios_utils_Load_Module_State_0_runningTask__A */
__T1_ti_sysbios_utils_Load_Module_State__runningTask ti_sysbios_utils_Load_Module_State_0_runningTask__A[1] = {
    0,  /* [0] */
};

/* --> ti_sysbios_utils_Load_Module_State_0_taskEnv__A */
__T1_ti_sysbios_utils_Load_Module_State__taskEnv ti_sysbios_utils_Load_Module_State_0_taskEnv__A[1] = {
    {
        {
            ((ti_sysbios_knl_Queue_Elem*)((void*)&ti_sysbios_utils_Load_Module_State_0_taskEnv__A[0].qElem)),  /* next */
            ((ti_sysbios_knl_Queue_Elem*)((void*)&ti_sysbios_utils_Load_Module_State_0_taskEnv__A[0].qElem)),  /* prev */
        },  /* qElem */
        (xdc_UInt32)0x0U,  /* totalTimeElapsed */
        (xdc_UInt32)0x0U,  /* totalTime */
        (xdc_UInt32)0x0U,  /* nextTotalTime */
        (xdc_UInt32)0x0U,  /* timeOfLastUpdate */
        ((xdc_Ptr)NULL),  /* threadHandle */
    },  /* [0] */
};

/* Module__state__V */
#ifdef __ti__
ti_sysbios_utils_Load_Module_State__ ti_sysbios_utils_Load_Module__state__V __attribute__ ((section(".data:ti_sysbios_utils_Load_Module__state__V")));
#elif !(defined(__MACH__) && defined(__APPLE__))
ti_sysbios_utils_Load_Module_State__ ti_sysbios_utils_Load_Module__state__V __attribute__ ((section(".data_ti_sysbios_utils_Load_Module__state__V")));
#endif
ti_sysbios_utils_Load_Module_State__ ti_sysbios_utils_Load_Module__state__V = {
    (xdc_Int)0x0,  /* taskHId */
    ((void*)ti_sysbios_utils_Load_Module_State_0_taskStartTime__A),  /* taskStartTime */
    (xdc_UInt32)0x0U,  /* timeElapsed */
    ((void*)ti_sysbios_utils_Load_Module_State_0_runningTask__A),  /* runningTask */
    0,  /* firstSwitchDone */
    (xdc_UInt32)0x0U,  /* swiStartTime */
    {
        {
            ((ti_sysbios_knl_Queue_Elem*)((void*)&ti_sysbios_utils_Load_Module__state__V.swiEnv.qElem)),  /* next */
            ((ti_sysbios_knl_Queue_Elem*)((void*)&ti_sysbios_utils_Load_Module__state__V.swiEnv.qElem)),  /* prev */
        },  /* qElem */
        (xdc_UInt32)0x0U,  /* totalTimeElapsed */
        (xdc_UInt32)0x0U,  /* totalTime */
        (xdc_UInt32)0x0U,  /* nextTotalTime */
        (xdc_UInt32)0x0U,  /* timeOfLastUpdate */
        ((xdc_Ptr)NULL),  /* threadHandle */
    },  /* swiEnv */
    ((void*)ti_sysbios_utils_Load_Module_State_0_taskEnv__A),  /* taskEnv */
    (xdc_UInt32)0x0U,  /* swiCnt */
    (xdc_UInt32)0x0U,  /* hwiStartTime */
    {
        {
            ((ti_sysbios_knl_Queue_Elem*)((void*)&ti_sysbios_utils_Load_Module__state__V.hwiEnv.qElem)),  /* next */
            ((ti_sysbios_knl_Queue_Elem*)((void*)&ti_sysbios_utils_Load_Module__state__V.hwiEnv.qElem)),  /* prev */
        },  /* qElem */
        (xdc_UInt32)0x0U,  /* totalTimeElapsed */
        (xdc_UInt32)0x0U,  /* totalTime */
        (xdc_UInt32)0x0U,  /* nextTotalTime */
        (xdc_UInt32)0x0U,  /* timeOfLastUpdate */
        ((xdc_Ptr)NULL),  /* threadHandle */
    },  /* hwiEnv */
    (xdc_UInt32)0x0U,  /* hwiCnt */
    (xdc_UInt32)0x0U,  /* timeSlotCnt */
    (xdc_UInt32)0xffffffffU,  /* minLoop */
    (xdc_UInt32)0x0U,  /* minIdle */
    (xdc_UInt32)0x0U,  /* t0 */
    (xdc_UInt32)0x0U,  /* idleCnt */
    (xdc_UInt32)0x0U,  /* cpuLoad */
    (xdc_UInt32)0x1U,  /* taskEnvLen */
    (xdc_UInt32)0x0U,  /* taskNum */
    0,  /* powerEnabled */
    (xdc_UInt32)0x0U,  /* idleStartTime */
    (xdc_UInt32)0x0U,  /* busyStartTime */
    (xdc_UInt32)0x0U,  /* busyTime */
    {
        {
            ((ti_sysbios_knl_Queue_Elem*)((void*)&ti_sysbios_utils_Load_Module__state__V.Object_field_taskList.elem)),  /* next */
            ((ti_sysbios_knl_Queue_Elem*)((void*)&ti_sysbios_utils_Load_Module__state__V.Object_field_taskList.elem)),  /* prev */
        },  /* elem */
    },  /* Object_field_taskList */
};

/* Module__diagsEnabled__C */
#pragma DATA_SECTION(ti_sysbios_utils_Load_Module__diagsEnabled__C, ".const:ti_sysbios_utils_Load_Module__diagsEnabled__C");
__FAR__ const CT__ti_sysbios_utils_Load_Module__diagsEnabled ti_sysbios_utils_Load_Module__diagsEnabled__C = (xdc_Bits32)0x90U;

/* Module__diagsIncluded__C */
#pragma DATA_SECTION(ti_sysbios_utils_Load_Module__diagsIncluded__C, ".const:ti_sysbios_utils_Load_Module__diagsIncluded__C");
__FAR__ const CT__ti_sysbios_utils_Load_Module__diagsIncluded ti_sysbios_utils_Load_Module__diagsIncluded__C = (xdc_Bits32)0x90U;

/* Module__diagsMask__C */
#pragma DATA_SECTION(ti_sysbios_utils_Load_Module__diagsMask__C, ".const:ti_sysbios_utils_Load_Module__diagsMask__C");
__FAR__ const CT__ti_sysbios_utils_Load_Module__diagsMask ti_sysbios_utils_Load_Module__diagsMask__C = ((const CT__ti_sysbios_utils_Load_Module__diagsMask)NULL);

/* Module__gateObj__C */
#pragma DATA_SECTION(ti_sysbios_utils_Load_Module__gateObj__C, ".const:ti_sysbios_utils_Load_Module__gateObj__C");
__FAR__ const CT__ti_sysbios_utils_Load_Module__gateObj ti_sysbios_utils_Load_Module__gateObj__C = ((const CT__ti_sysbios_utils_Load_Module__gateObj)NULL);

/* Module__gatePrms__C */
#pragma DATA_SECTION(ti_sysbios_utils_Load_Module__gatePrms__C, ".const:ti_sysbios_utils_Load_Module__gatePrms__C");
__FAR__ const CT__ti_sysbios_utils_Load_Module__gatePrms ti_sysbios_utils_Load_Module__gatePrms__C = ((const CT__ti_sysbios_utils_Load_Module__gatePrms)NULL);

/* Module__id__C */
#pragma DATA_SECTION(ti_sysbios_utils_Load_Module__id__C, ".const:ti_sysbios_utils_Load_Module__id__C");
__FAR__ const CT__ti_sysbios_utils_Load_Module__id ti_sysbios_utils_Load_Module__id__C = (xdc_Bits16)0x803cU;

/* Module__loggerDefined__C */
#pragma DATA_SECTION(ti_sysbios_utils_Load_Module__loggerDefined__C, ".const:ti_sysbios_utils_Load_Module__loggerDefined__C");
__FAR__ const CT__ti_sysbios_utils_Load_Module__loggerDefined ti_sysbios_utils_Load_Module__loggerDefined__C = 0;

/* Module__loggerObj__C */
#pragma DATA_SECTION(ti_sysbios_utils_Load_Module__loggerObj__C, ".const:ti_sysbios_utils_Load_Module__loggerObj__C");
__FAR__ const CT__ti_sysbios_utils_Load_Module__loggerObj ti_sysbios_utils_Load_Module__loggerObj__C = ((const CT__ti_sysbios_utils_Load_Module__loggerObj)NULL);

/* Module__loggerFxn0__C */
#pragma DATA_SECTION(ti_sysbios_utils_Load_Module__loggerFxn0__C, ".const:ti_sysbios_utils_Load_Module__loggerFxn0__C");
__FAR__ const CT__ti_sysbios_utils_Load_Module__loggerFxn0 ti_sysbios_utils_Load_Module__loggerFxn0__C = ((const CT__ti_sysbios_utils_Load_Module__loggerFxn0)NULL);

/* Module__loggerFxn1__C */
#pragma DATA_SECTION(ti_sysbios_utils_Load_Module__loggerFxn1__C, ".const:ti_sysbios_utils_Load_Module__loggerFxn1__C");
__FAR__ const CT__ti_sysbios_utils_Load_Module__loggerFxn1 ti_sysbios_utils_Load_Module__loggerFxn1__C = ((const CT__ti_sysbios_utils_Load_Module__loggerFxn1)NULL);

/* Module__loggerFxn2__C */
#pragma DATA_SECTION(ti_sysbios_utils_Load_Module__loggerFxn2__C, ".const:ti_sysbios_utils_Load_Module__loggerFxn2__C");
__FAR__ const CT__ti_sysbios_utils_Load_Module__loggerFxn2 ti_sysbios_utils_Load_Module__loggerFxn2__C = ((const CT__ti_sysbios_utils_Load_Module__loggerFxn2)NULL);

/* Module__loggerFxn4__C */
#pragma DATA_SECTION(ti_sysbios_utils_Load_Module__loggerFxn4__C, ".const:ti_sysbios_utils_Load_Module__loggerFxn4__C");
__FAR__ const CT__ti_sysbios_utils_Load_Module__loggerFxn4 ti_sysbios_utils_Load_Module__loggerFxn4__C = ((const CT__ti_sysbios_utils_Load_Module__loggerFxn4)NULL);

/* Module__loggerFxn8__C */
#pragma DATA_SECTION(ti_sysbios_utils_Load_Module__loggerFxn8__C, ".const:ti_sysbios_utils_Load_Module__loggerFxn8__C");
__FAR__ const CT__ti_sysbios_utils_Load_Module__loggerFxn8 ti_sysbios_utils_Load_Module__loggerFxn8__C = ((const CT__ti_sysbios_utils_Load_Module__loggerFxn8)NULL);

/* Object__count__C */
#pragma DATA_SECTION(ti_sysbios_utils_Load_Object__count__C, ".const:ti_sysbios_utils_Load_Object__count__C");
__FAR__ const CT__ti_sysbios_utils_Load_Object__count ti_sysbios_utils_Load_Object__count__C = 0;

/* Object__heap__C */
#pragma DATA_SECTION(ti_sysbios_utils_Load_Object__heap__C, ".const:ti_sysbios_utils_Load_Object__heap__C");
__FAR__ const CT__ti_sysbios_utils_Load_Object__heap ti_sysbios_utils_Load_Object__heap__C = NULL;

/* Object__sizeof__C */
#pragma DATA_SECTION(ti_sysbios_utils_Load_Object__sizeof__C, ".const:ti_sysbios_utils_Load_Object__sizeof__C");
__FAR__ const CT__ti_sysbios_utils_Load_Object__sizeof ti_sysbios_utils_Load_Object__sizeof__C = 0;

/* Object__table__C */
#pragma DATA_SECTION(ti_sysbios_utils_Load_Object__table__C, ".const:ti_sysbios_utils_Load_Object__table__C");
__FAR__ const CT__ti_sysbios_utils_Load_Object__table ti_sysbios_utils_Load_Object__table__C = NULL;

/* LS_cpuLoad__C */
#pragma DATA_SECTION(ti_sysbios_utils_Load_LS_cpuLoad__C, ".const:ti_sysbios_utils_Load_LS_cpuLoad__C");
__FAR__ const CT__ti_sysbios_utils_Load_LS_cpuLoad ti_sysbios_utils_Load_LS_cpuLoad__C = (((xdc_runtime_Log_Event)6804) << 16 | 2048);

/* LS_hwiLoad__C */
#pragma DATA_SECTION(ti_sysbios_utils_Load_LS_hwiLoad__C, ".const:ti_sysbios_utils_Load_LS_hwiLoad__C");
__FAR__ const CT__ti_sysbios_utils_Load_LS_hwiLoad ti_sysbios_utils_Load_LS_hwiLoad__C = (((xdc_runtime_Log_Event)6821) << 16 | 2048);

/* LS_swiLoad__C */
#pragma DATA_SECTION(ti_sysbios_utils_Load_LS_swiLoad__C, ".const:ti_sysbios_utils_Load_LS_swiLoad__C");
__FAR__ const CT__ti_sysbios_utils_Load_LS_swiLoad ti_sysbios_utils_Load_LS_swiLoad__C = (((xdc_runtime_Log_Event)6839) << 16 | 2048);

/* LS_taskLoad__C */
#pragma DATA_SECTION(ti_sysbios_utils_Load_LS_taskLoad__C, ".const:ti_sysbios_utils_Load_LS_taskLoad__C");
__FAR__ const CT__ti_sysbios_utils_Load_LS_taskLoad ti_sysbios_utils_Load_LS_taskLoad__C = (((xdc_runtime_Log_Event)6857) << 16 | 2048);

/* postUpdate__C */
#pragma DATA_SECTION(ti_sysbios_utils_Load_postUpdate__C, ".const:ti_sysbios_utils_Load_postUpdate__C");
__FAR__ const CT__ti_sysbios_utils_Load_postUpdate ti_sysbios_utils_Load_postUpdate__C = ((const CT__ti_sysbios_utils_Load_postUpdate)(tidlPerfStatsBiosLoadUpdate));

/* updateInIdle__C */
#pragma DATA_SECTION(ti_sysbios_utils_Load_updateInIdle__C, ".const:ti_sysbios_utils_Load_updateInIdle__C");
__FAR__ const CT__ti_sysbios_utils_Load_updateInIdle ti_sysbios_utils_Load_updateInIdle__C = 1;

/* windowInMs__C */
#pragma DATA_SECTION(ti_sysbios_utils_Load_windowInMs__C, ".const:ti_sysbios_utils_Load_windowInMs__C");
__FAR__ const CT__ti_sysbios_utils_Load_windowInMs ti_sysbios_utils_Load_windowInMs__C = (xdc_UInt)0x1f4U;

/* hwiEnabled__C */
#pragma DATA_SECTION(ti_sysbios_utils_Load_hwiEnabled__C, ".const:ti_sysbios_utils_Load_hwiEnabled__C");
__FAR__ const CT__ti_sysbios_utils_Load_hwiEnabled ti_sysbios_utils_Load_hwiEnabled__C = 1;

/* swiEnabled__C */
#pragma DATA_SECTION(ti_sysbios_utils_Load_swiEnabled__C, ".const:ti_sysbios_utils_Load_swiEnabled__C");
__FAR__ const CT__ti_sysbios_utils_Load_swiEnabled ti_sysbios_utils_Load_swiEnabled__C = 1;

/* taskEnabled__C */
#pragma DATA_SECTION(ti_sysbios_utils_Load_taskEnabled__C, ".const:ti_sysbios_utils_Load_taskEnabled__C");
__FAR__ const CT__ti_sysbios_utils_Load_taskEnabled ti_sysbios_utils_Load_taskEnabled__C = 1;

/* autoAddTasks__C */
#pragma DATA_SECTION(ti_sysbios_utils_Load_autoAddTasks__C, ".const:ti_sysbios_utils_Load_autoAddTasks__C");
__FAR__ const CT__ti_sysbios_utils_Load_autoAddTasks ti_sysbios_utils_Load_autoAddTasks__C = 1;


/*
 * ======== ti.sysbios.xdcruntime.GateThreadSupport INITIALIZERS ========
 */

/* Object__DESC__C */
__FAR__ const xdc_runtime_Core_ObjDesc ti_sysbios_xdcruntime_GateThreadSupport_Object__DESC__C;

/* Object__PARAMS__C */
#pragma DATA_SECTION(ti_sysbios_xdcruntime_GateThreadSupport_Object__PARAMS__C, ".const:ti_sysbios_xdcruntime_GateThreadSupport_Object__PARAMS__C");
__FAR__ const ti_sysbios_xdcruntime_GateThreadSupport_Params ti_sysbios_xdcruntime_GateThreadSupport_Object__PARAMS__C = {
    sizeof (ti_sysbios_xdcruntime_GateThreadSupport_Params), /* __size */
    NULL, /* __self */
    NULL, /* __fxns */
    (xdc_runtime_IInstance_Params*)&ti_sysbios_xdcruntime_GateThreadSupport_Object__PARAMS__C.__iprms, /* instance */
    {
        sizeof (xdc_runtime_IInstance_Params), /* __size */
        0,  /* name */
    },  /* instance */
};

/* Module__root__V */
ti_sysbios_xdcruntime_GateThreadSupport_Module__ ti_sysbios_xdcruntime_GateThreadSupport_Module__root__V = {
    {&ti_sysbios_xdcruntime_GateThreadSupport_Module__root__V.link,  /* link.next */
    &ti_sysbios_xdcruntime_GateThreadSupport_Module__root__V.link},  /* link.prev */
};

/* Module__diagsEnabled__C */
#pragma DATA_SECTION(ti_sysbios_xdcruntime_GateThreadSupport_Module__diagsEnabled__C, ".const:ti_sysbios_xdcruntime_GateThreadSupport_Module__diagsEnabled__C");
__FAR__ const CT__ti_sysbios_xdcruntime_GateThreadSupport_Module__diagsEnabled ti_sysbios_xdcruntime_GateThreadSupport_Module__diagsEnabled__C = (xdc_Bits32)0x90U;

/* Module__diagsIncluded__C */
#pragma DATA_SECTION(ti_sysbios_xdcruntime_GateThreadSupport_Module__diagsIncluded__C, ".const:ti_sysbios_xdcruntime_GateThreadSupport_Module__diagsIncluded__C");
__FAR__ const CT__ti_sysbios_xdcruntime_GateThreadSupport_Module__diagsIncluded ti_sysbios_xdcruntime_GateThreadSupport_Module__diagsIncluded__C = (xdc_Bits32)0x90U;

/* Module__diagsMask__C */
#pragma DATA_SECTION(ti_sysbios_xdcruntime_GateThreadSupport_Module__diagsMask__C, ".const:ti_sysbios_xdcruntime_GateThreadSupport_Module__diagsMask__C");
__FAR__ const CT__ti_sysbios_xdcruntime_GateThreadSupport_Module__diagsMask ti_sysbios_xdcruntime_GateThreadSupport_Module__diagsMask__C = ((const CT__ti_sysbios_xdcruntime_GateThreadSupport_Module__diagsMask)NULL);

/* Module__gateObj__C */
#pragma DATA_SECTION(ti_sysbios_xdcruntime_GateThreadSupport_Module__gateObj__C, ".const:ti_sysbios_xdcruntime_GateThreadSupport_Module__gateObj__C");
__FAR__ const CT__ti_sysbios_xdcruntime_GateThreadSupport_Module__gateObj ti_sysbios_xdcruntime_GateThreadSupport_Module__gateObj__C = ((const CT__ti_sysbios_xdcruntime_GateThreadSupport_Module__gateObj)NULL);

/* Module__gatePrms__C */
#pragma DATA_SECTION(ti_sysbios_xdcruntime_GateThreadSupport_Module__gatePrms__C, ".const:ti_sysbios_xdcruntime_GateThreadSupport_Module__gatePrms__C");
__FAR__ const CT__ti_sysbios_xdcruntime_GateThreadSupport_Module__gatePrms ti_sysbios_xdcruntime_GateThreadSupport_Module__gatePrms__C = ((const CT__ti_sysbios_xdcruntime_GateThreadSupport_Module__gatePrms)NULL);

/* Module__id__C */
#pragma DATA_SECTION(ti_sysbios_xdcruntime_GateThreadSupport_Module__id__C, ".const:ti_sysbios_xdcruntime_GateThreadSupport_Module__id__C");
__FAR__ const CT__ti_sysbios_xdcruntime_GateThreadSupport_Module__id ti_sysbios_xdcruntime_GateThreadSupport_Module__id__C = (xdc_Bits16)0x8044U;

/* Module__loggerDefined__C */
#pragma DATA_SECTION(ti_sysbios_xdcruntime_GateThreadSupport_Module__loggerDefined__C, ".const:ti_sysbios_xdcruntime_GateThreadSupport_Module__loggerDefined__C");
__FAR__ const CT__ti_sysbios_xdcruntime_GateThreadSupport_Module__loggerDefined ti_sysbios_xdcruntime_GateThreadSupport_Module__loggerDefined__C = 0;

/* Module__loggerObj__C */
#pragma DATA_SECTION(ti_sysbios_xdcruntime_GateThreadSupport_Module__loggerObj__C, ".const:ti_sysbios_xdcruntime_GateThreadSupport_Module__loggerObj__C");
__FAR__ const CT__ti_sysbios_xdcruntime_GateThreadSupport_Module__loggerObj ti_sysbios_xdcruntime_GateThreadSupport_Module__loggerObj__C = ((const CT__ti_sysbios_xdcruntime_GateThreadSupport_Module__loggerObj)NULL);

/* Module__loggerFxn0__C */
#pragma DATA_SECTION(ti_sysbios_xdcruntime_GateThreadSupport_Module__loggerFxn0__C, ".const:ti_sysbios_xdcruntime_GateThreadSupport_Module__loggerFxn0__C");
__FAR__ const CT__ti_sysbios_xdcruntime_GateThreadSupport_Module__loggerFxn0 ti_sysbios_xdcruntime_GateThreadSupport_Module__loggerFxn0__C = ((const CT__ti_sysbios_xdcruntime_GateThreadSupport_Module__loggerFxn0)NULL);

/* Module__loggerFxn1__C */
#pragma DATA_SECTION(ti_sysbios_xdcruntime_GateThreadSupport_Module__loggerFxn1__C, ".const:ti_sysbios_xdcruntime_GateThreadSupport_Module__loggerFxn1__C");
__FAR__ const CT__ti_sysbios_xdcruntime_GateThreadSupport_Module__loggerFxn1 ti_sysbios_xdcruntime_GateThreadSupport_Module__loggerFxn1__C = ((const CT__ti_sysbios_xdcruntime_GateThreadSupport_Module__loggerFxn1)NULL);

/* Module__loggerFxn2__C */
#pragma DATA_SECTION(ti_sysbios_xdcruntime_GateThreadSupport_Module__loggerFxn2__C, ".const:ti_sysbios_xdcruntime_GateThreadSupport_Module__loggerFxn2__C");
__FAR__ const CT__ti_sysbios_xdcruntime_GateThreadSupport_Module__loggerFxn2 ti_sysbios_xdcruntime_GateThreadSupport_Module__loggerFxn2__C = ((const CT__ti_sysbios_xdcruntime_GateThreadSupport_Module__loggerFxn2)NULL);

/* Module__loggerFxn4__C */
#pragma DATA_SECTION(ti_sysbios_xdcruntime_GateThreadSupport_Module__loggerFxn4__C, ".const:ti_sysbios_xdcruntime_GateThreadSupport_Module__loggerFxn4__C");
__FAR__ const CT__ti_sysbios_xdcruntime_GateThreadSupport_Module__loggerFxn4 ti_sysbios_xdcruntime_GateThreadSupport_Module__loggerFxn4__C = ((const CT__ti_sysbios_xdcruntime_GateThreadSupport_Module__loggerFxn4)NULL);

/* Module__loggerFxn8__C */
#pragma DATA_SECTION(ti_sysbios_xdcruntime_GateThreadSupport_Module__loggerFxn8__C, ".const:ti_sysbios_xdcruntime_GateThreadSupport_Module__loggerFxn8__C");
__FAR__ const CT__ti_sysbios_xdcruntime_GateThreadSupport_Module__loggerFxn8 ti_sysbios_xdcruntime_GateThreadSupport_Module__loggerFxn8__C = ((const CT__ti_sysbios_xdcruntime_GateThreadSupport_Module__loggerFxn8)NULL);

/* Object__count__C */
#pragma DATA_SECTION(ti_sysbios_xdcruntime_GateThreadSupport_Object__count__C, ".const:ti_sysbios_xdcruntime_GateThreadSupport_Object__count__C");
__FAR__ const CT__ti_sysbios_xdcruntime_GateThreadSupport_Object__count ti_sysbios_xdcruntime_GateThreadSupport_Object__count__C = 0;

/* Object__heap__C */
#pragma DATA_SECTION(ti_sysbios_xdcruntime_GateThreadSupport_Object__heap__C, ".const:ti_sysbios_xdcruntime_GateThreadSupport_Object__heap__C");
__FAR__ const CT__ti_sysbios_xdcruntime_GateThreadSupport_Object__heap ti_sysbios_xdcruntime_GateThreadSupport_Object__heap__C = NULL;

/* Object__sizeof__C */
#pragma DATA_SECTION(ti_sysbios_xdcruntime_GateThreadSupport_Object__sizeof__C, ".const:ti_sysbios_xdcruntime_GateThreadSupport_Object__sizeof__C");
__FAR__ const CT__ti_sysbios_xdcruntime_GateThreadSupport_Object__sizeof ti_sysbios_xdcruntime_GateThreadSupport_Object__sizeof__C = sizeof(ti_sysbios_xdcruntime_GateThreadSupport_Object__);

/* Object__table__C */
#pragma DATA_SECTION(ti_sysbios_xdcruntime_GateThreadSupport_Object__table__C, ".const:ti_sysbios_xdcruntime_GateThreadSupport_Object__table__C");
__FAR__ const CT__ti_sysbios_xdcruntime_GateThreadSupport_Object__table ti_sysbios_xdcruntime_GateThreadSupport_Object__table__C = NULL;


/*
 * ======== xdc.runtime.Assert INITIALIZERS ========
 */

/* Module__diagsEnabled__C */
#pragma DATA_SECTION(xdc_runtime_Assert_Module__diagsEnabled__C, ".const:xdc_runtime_Assert_Module__diagsEnabled__C");
__FAR__ const CT__xdc_runtime_Assert_Module__diagsEnabled xdc_runtime_Assert_Module__diagsEnabled__C = (xdc_Bits32)0x10U;

/* Module__diagsIncluded__C */
#pragma DATA_SECTION(xdc_runtime_Assert_Module__diagsIncluded__C, ".const:xdc_runtime_Assert_Module__diagsIncluded__C");
__FAR__ const CT__xdc_runtime_Assert_Module__diagsIncluded xdc_runtime_Assert_Module__diagsIncluded__C = (xdc_Bits32)0x10U;

/* Module__diagsMask__C */
#pragma DATA_SECTION(xdc_runtime_Assert_Module__diagsMask__C, ".const:xdc_runtime_Assert_Module__diagsMask__C");
__FAR__ const CT__xdc_runtime_Assert_Module__diagsMask xdc_runtime_Assert_Module__diagsMask__C = ((const CT__xdc_runtime_Assert_Module__diagsMask)NULL);

/* Module__gateObj__C */
#pragma DATA_SECTION(xdc_runtime_Assert_Module__gateObj__C, ".const:xdc_runtime_Assert_Module__gateObj__C");
__FAR__ const CT__xdc_runtime_Assert_Module__gateObj xdc_runtime_Assert_Module__gateObj__C = ((const CT__xdc_runtime_Assert_Module__gateObj)NULL);

/* Module__gatePrms__C */
#pragma DATA_SECTION(xdc_runtime_Assert_Module__gatePrms__C, ".const:xdc_runtime_Assert_Module__gatePrms__C");
__FAR__ const CT__xdc_runtime_Assert_Module__gatePrms xdc_runtime_Assert_Module__gatePrms__C = ((const CT__xdc_runtime_Assert_Module__gatePrms)NULL);

/* Module__id__C */
#pragma DATA_SECTION(xdc_runtime_Assert_Module__id__C, ".const:xdc_runtime_Assert_Module__id__C");
__FAR__ const CT__xdc_runtime_Assert_Module__id xdc_runtime_Assert_Module__id__C = (xdc_Bits16)0x8002U;

/* Module__loggerDefined__C */
#pragma DATA_SECTION(xdc_runtime_Assert_Module__loggerDefined__C, ".const:xdc_runtime_Assert_Module__loggerDefined__C");
__FAR__ const CT__xdc_runtime_Assert_Module__loggerDefined xdc_runtime_Assert_Module__loggerDefined__C = 0;

/* Module__loggerObj__C */
#pragma DATA_SECTION(xdc_runtime_Assert_Module__loggerObj__C, ".const:xdc_runtime_Assert_Module__loggerObj__C");
__FAR__ const CT__xdc_runtime_Assert_Module__loggerObj xdc_runtime_Assert_Module__loggerObj__C = ((const CT__xdc_runtime_Assert_Module__loggerObj)NULL);

/* Module__loggerFxn0__C */
#pragma DATA_SECTION(xdc_runtime_Assert_Module__loggerFxn0__C, ".const:xdc_runtime_Assert_Module__loggerFxn0__C");
__FAR__ const CT__xdc_runtime_Assert_Module__loggerFxn0 xdc_runtime_Assert_Module__loggerFxn0__C = ((const CT__xdc_runtime_Assert_Module__loggerFxn0)NULL);

/* Module__loggerFxn1__C */
#pragma DATA_SECTION(xdc_runtime_Assert_Module__loggerFxn1__C, ".const:xdc_runtime_Assert_Module__loggerFxn1__C");
__FAR__ const CT__xdc_runtime_Assert_Module__loggerFxn1 xdc_runtime_Assert_Module__loggerFxn1__C = ((const CT__xdc_runtime_Assert_Module__loggerFxn1)NULL);

/* Module__loggerFxn2__C */
#pragma DATA_SECTION(xdc_runtime_Assert_Module__loggerFxn2__C, ".const:xdc_runtime_Assert_Module__loggerFxn2__C");
__FAR__ const CT__xdc_runtime_Assert_Module__loggerFxn2 xdc_runtime_Assert_Module__loggerFxn2__C = ((const CT__xdc_runtime_Assert_Module__loggerFxn2)NULL);

/* Module__loggerFxn4__C */
#pragma DATA_SECTION(xdc_runtime_Assert_Module__loggerFxn4__C, ".const:xdc_runtime_Assert_Module__loggerFxn4__C");
__FAR__ const CT__xdc_runtime_Assert_Module__loggerFxn4 xdc_runtime_Assert_Module__loggerFxn4__C = ((const CT__xdc_runtime_Assert_Module__loggerFxn4)NULL);

/* Module__loggerFxn8__C */
#pragma DATA_SECTION(xdc_runtime_Assert_Module__loggerFxn8__C, ".const:xdc_runtime_Assert_Module__loggerFxn8__C");
__FAR__ const CT__xdc_runtime_Assert_Module__loggerFxn8 xdc_runtime_Assert_Module__loggerFxn8__C = ((const CT__xdc_runtime_Assert_Module__loggerFxn8)NULL);

/* Object__count__C */
#pragma DATA_SECTION(xdc_runtime_Assert_Object__count__C, ".const:xdc_runtime_Assert_Object__count__C");
__FAR__ const CT__xdc_runtime_Assert_Object__count xdc_runtime_Assert_Object__count__C = 0;

/* Object__heap__C */
#pragma DATA_SECTION(xdc_runtime_Assert_Object__heap__C, ".const:xdc_runtime_Assert_Object__heap__C");
__FAR__ const CT__xdc_runtime_Assert_Object__heap xdc_runtime_Assert_Object__heap__C = NULL;

/* Object__sizeof__C */
#pragma DATA_SECTION(xdc_runtime_Assert_Object__sizeof__C, ".const:xdc_runtime_Assert_Object__sizeof__C");
__FAR__ const CT__xdc_runtime_Assert_Object__sizeof xdc_runtime_Assert_Object__sizeof__C = 0;

/* Object__table__C */
#pragma DATA_SECTION(xdc_runtime_Assert_Object__table__C, ".const:xdc_runtime_Assert_Object__table__C");
__FAR__ const CT__xdc_runtime_Assert_Object__table xdc_runtime_Assert_Object__table__C = NULL;

/* E_assertFailed__C */
#pragma DATA_SECTION(xdc_runtime_Assert_E_assertFailed__C, ".const:xdc_runtime_Assert_E_assertFailed__C");
__FAR__ const CT__xdc_runtime_Assert_E_assertFailed xdc_runtime_Assert_E_assertFailed__C = (((xdc_runtime_Error_Id)3806) << 16 | 0U);


/*
 * ======== xdc.runtime.Core INITIALIZERS ========
 */

/* Module__diagsEnabled__C */
#pragma DATA_SECTION(xdc_runtime_Core_Module__diagsEnabled__C, ".const:xdc_runtime_Core_Module__diagsEnabled__C");
__FAR__ const CT__xdc_runtime_Core_Module__diagsEnabled xdc_runtime_Core_Module__diagsEnabled__C = (xdc_Bits32)0x10U;

/* Module__diagsIncluded__C */
#pragma DATA_SECTION(xdc_runtime_Core_Module__diagsIncluded__C, ".const:xdc_runtime_Core_Module__diagsIncluded__C");
__FAR__ const CT__xdc_runtime_Core_Module__diagsIncluded xdc_runtime_Core_Module__diagsIncluded__C = (xdc_Bits32)0x10U;

/* Module__diagsMask__C */
#pragma DATA_SECTION(xdc_runtime_Core_Module__diagsMask__C, ".const:xdc_runtime_Core_Module__diagsMask__C");
__FAR__ const CT__xdc_runtime_Core_Module__diagsMask xdc_runtime_Core_Module__diagsMask__C = ((const CT__xdc_runtime_Core_Module__diagsMask)NULL);

/* Module__gateObj__C */
#pragma DATA_SECTION(xdc_runtime_Core_Module__gateObj__C, ".const:xdc_runtime_Core_Module__gateObj__C");
__FAR__ const CT__xdc_runtime_Core_Module__gateObj xdc_runtime_Core_Module__gateObj__C = ((const CT__xdc_runtime_Core_Module__gateObj)NULL);

/* Module__gatePrms__C */
#pragma DATA_SECTION(xdc_runtime_Core_Module__gatePrms__C, ".const:xdc_runtime_Core_Module__gatePrms__C");
__FAR__ const CT__xdc_runtime_Core_Module__gatePrms xdc_runtime_Core_Module__gatePrms__C = ((const CT__xdc_runtime_Core_Module__gatePrms)NULL);

/* Module__id__C */
#pragma DATA_SECTION(xdc_runtime_Core_Module__id__C, ".const:xdc_runtime_Core_Module__id__C");
__FAR__ const CT__xdc_runtime_Core_Module__id xdc_runtime_Core_Module__id__C = (xdc_Bits16)0x8003U;

/* Module__loggerDefined__C */
#pragma DATA_SECTION(xdc_runtime_Core_Module__loggerDefined__C, ".const:xdc_runtime_Core_Module__loggerDefined__C");
__FAR__ const CT__xdc_runtime_Core_Module__loggerDefined xdc_runtime_Core_Module__loggerDefined__C = 0;

/* Module__loggerObj__C */
#pragma DATA_SECTION(xdc_runtime_Core_Module__loggerObj__C, ".const:xdc_runtime_Core_Module__loggerObj__C");
__FAR__ const CT__xdc_runtime_Core_Module__loggerObj xdc_runtime_Core_Module__loggerObj__C = ((const CT__xdc_runtime_Core_Module__loggerObj)NULL);

/* Module__loggerFxn0__C */
#pragma DATA_SECTION(xdc_runtime_Core_Module__loggerFxn0__C, ".const:xdc_runtime_Core_Module__loggerFxn0__C");
__FAR__ const CT__xdc_runtime_Core_Module__loggerFxn0 xdc_runtime_Core_Module__loggerFxn0__C = ((const CT__xdc_runtime_Core_Module__loggerFxn0)NULL);

/* Module__loggerFxn1__C */
#pragma DATA_SECTION(xdc_runtime_Core_Module__loggerFxn1__C, ".const:xdc_runtime_Core_Module__loggerFxn1__C");
__FAR__ const CT__xdc_runtime_Core_Module__loggerFxn1 xdc_runtime_Core_Module__loggerFxn1__C = ((const CT__xdc_runtime_Core_Module__loggerFxn1)NULL);

/* Module__loggerFxn2__C */
#pragma DATA_SECTION(xdc_runtime_Core_Module__loggerFxn2__C, ".const:xdc_runtime_Core_Module__loggerFxn2__C");
__FAR__ const CT__xdc_runtime_Core_Module__loggerFxn2 xdc_runtime_Core_Module__loggerFxn2__C = ((const CT__xdc_runtime_Core_Module__loggerFxn2)NULL);

/* Module__loggerFxn4__C */
#pragma DATA_SECTION(xdc_runtime_Core_Module__loggerFxn4__C, ".const:xdc_runtime_Core_Module__loggerFxn4__C");
__FAR__ const CT__xdc_runtime_Core_Module__loggerFxn4 xdc_runtime_Core_Module__loggerFxn4__C = ((const CT__xdc_runtime_Core_Module__loggerFxn4)NULL);

/* Module__loggerFxn8__C */
#pragma DATA_SECTION(xdc_runtime_Core_Module__loggerFxn8__C, ".const:xdc_runtime_Core_Module__loggerFxn8__C");
__FAR__ const CT__xdc_runtime_Core_Module__loggerFxn8 xdc_runtime_Core_Module__loggerFxn8__C = ((const CT__xdc_runtime_Core_Module__loggerFxn8)NULL);

/* Object__count__C */
#pragma DATA_SECTION(xdc_runtime_Core_Object__count__C, ".const:xdc_runtime_Core_Object__count__C");
__FAR__ const CT__xdc_runtime_Core_Object__count xdc_runtime_Core_Object__count__C = 0;

/* Object__heap__C */
#pragma DATA_SECTION(xdc_runtime_Core_Object__heap__C, ".const:xdc_runtime_Core_Object__heap__C");
__FAR__ const CT__xdc_runtime_Core_Object__heap xdc_runtime_Core_Object__heap__C = NULL;

/* Object__sizeof__C */
#pragma DATA_SECTION(xdc_runtime_Core_Object__sizeof__C, ".const:xdc_runtime_Core_Object__sizeof__C");
__FAR__ const CT__xdc_runtime_Core_Object__sizeof xdc_runtime_Core_Object__sizeof__C = 0;

/* Object__table__C */
#pragma DATA_SECTION(xdc_runtime_Core_Object__table__C, ".const:xdc_runtime_Core_Object__table__C");
__FAR__ const CT__xdc_runtime_Core_Object__table xdc_runtime_Core_Object__table__C = NULL;

/* A_initializedParams__C */
#pragma DATA_SECTION(xdc_runtime_Core_A_initializedParams__C, ".const:xdc_runtime_Core_A_initializedParams__C");
__FAR__ const CT__xdc_runtime_Core_A_initializedParams xdc_runtime_Core_A_initializedParams__C = (((xdc_runtime_Assert_Id)1) << 16 | 16);


/*
 * ======== xdc.runtime.Defaults INITIALIZERS ========
 */

/* Module__diagsEnabled__C */
#pragma DATA_SECTION(xdc_runtime_Defaults_Module__diagsEnabled__C, ".const:xdc_runtime_Defaults_Module__diagsEnabled__C");
__FAR__ const CT__xdc_runtime_Defaults_Module__diagsEnabled xdc_runtime_Defaults_Module__diagsEnabled__C = (xdc_Bits32)0x90U;

/* Module__diagsIncluded__C */
#pragma DATA_SECTION(xdc_runtime_Defaults_Module__diagsIncluded__C, ".const:xdc_runtime_Defaults_Module__diagsIncluded__C");
__FAR__ const CT__xdc_runtime_Defaults_Module__diagsIncluded xdc_runtime_Defaults_Module__diagsIncluded__C = (xdc_Bits32)0x90U;

/* Module__diagsMask__C */
#pragma DATA_SECTION(xdc_runtime_Defaults_Module__diagsMask__C, ".const:xdc_runtime_Defaults_Module__diagsMask__C");
__FAR__ const CT__xdc_runtime_Defaults_Module__diagsMask xdc_runtime_Defaults_Module__diagsMask__C = ((const CT__xdc_runtime_Defaults_Module__diagsMask)NULL);

/* Module__gateObj__C */
#pragma DATA_SECTION(xdc_runtime_Defaults_Module__gateObj__C, ".const:xdc_runtime_Defaults_Module__gateObj__C");
__FAR__ const CT__xdc_runtime_Defaults_Module__gateObj xdc_runtime_Defaults_Module__gateObj__C = ((const CT__xdc_runtime_Defaults_Module__gateObj)NULL);

/* Module__gatePrms__C */
#pragma DATA_SECTION(xdc_runtime_Defaults_Module__gatePrms__C, ".const:xdc_runtime_Defaults_Module__gatePrms__C");
__FAR__ const CT__xdc_runtime_Defaults_Module__gatePrms xdc_runtime_Defaults_Module__gatePrms__C = ((const CT__xdc_runtime_Defaults_Module__gatePrms)NULL);

/* Module__id__C */
#pragma DATA_SECTION(xdc_runtime_Defaults_Module__id__C, ".const:xdc_runtime_Defaults_Module__id__C");
__FAR__ const CT__xdc_runtime_Defaults_Module__id xdc_runtime_Defaults_Module__id__C = (xdc_Bits16)0x8004U;

/* Module__loggerDefined__C */
#pragma DATA_SECTION(xdc_runtime_Defaults_Module__loggerDefined__C, ".const:xdc_runtime_Defaults_Module__loggerDefined__C");
__FAR__ const CT__xdc_runtime_Defaults_Module__loggerDefined xdc_runtime_Defaults_Module__loggerDefined__C = 0;

/* Module__loggerObj__C */
#pragma DATA_SECTION(xdc_runtime_Defaults_Module__loggerObj__C, ".const:xdc_runtime_Defaults_Module__loggerObj__C");
__FAR__ const CT__xdc_runtime_Defaults_Module__loggerObj xdc_runtime_Defaults_Module__loggerObj__C = ((const CT__xdc_runtime_Defaults_Module__loggerObj)NULL);

/* Module__loggerFxn0__C */
#pragma DATA_SECTION(xdc_runtime_Defaults_Module__loggerFxn0__C, ".const:xdc_runtime_Defaults_Module__loggerFxn0__C");
__FAR__ const CT__xdc_runtime_Defaults_Module__loggerFxn0 xdc_runtime_Defaults_Module__loggerFxn0__C = ((const CT__xdc_runtime_Defaults_Module__loggerFxn0)NULL);

/* Module__loggerFxn1__C */
#pragma DATA_SECTION(xdc_runtime_Defaults_Module__loggerFxn1__C, ".const:xdc_runtime_Defaults_Module__loggerFxn1__C");
__FAR__ const CT__xdc_runtime_Defaults_Module__loggerFxn1 xdc_runtime_Defaults_Module__loggerFxn1__C = ((const CT__xdc_runtime_Defaults_Module__loggerFxn1)NULL);

/* Module__loggerFxn2__C */
#pragma DATA_SECTION(xdc_runtime_Defaults_Module__loggerFxn2__C, ".const:xdc_runtime_Defaults_Module__loggerFxn2__C");
__FAR__ const CT__xdc_runtime_Defaults_Module__loggerFxn2 xdc_runtime_Defaults_Module__loggerFxn2__C = ((const CT__xdc_runtime_Defaults_Module__loggerFxn2)NULL);

/* Module__loggerFxn4__C */
#pragma DATA_SECTION(xdc_runtime_Defaults_Module__loggerFxn4__C, ".const:xdc_runtime_Defaults_Module__loggerFxn4__C");
__FAR__ const CT__xdc_runtime_Defaults_Module__loggerFxn4 xdc_runtime_Defaults_Module__loggerFxn4__C = ((const CT__xdc_runtime_Defaults_Module__loggerFxn4)NULL);

/* Module__loggerFxn8__C */
#pragma DATA_SECTION(xdc_runtime_Defaults_Module__loggerFxn8__C, ".const:xdc_runtime_Defaults_Module__loggerFxn8__C");
__FAR__ const CT__xdc_runtime_Defaults_Module__loggerFxn8 xdc_runtime_Defaults_Module__loggerFxn8__C = ((const CT__xdc_runtime_Defaults_Module__loggerFxn8)NULL);

/* Object__count__C */
#pragma DATA_SECTION(xdc_runtime_Defaults_Object__count__C, ".const:xdc_runtime_Defaults_Object__count__C");
__FAR__ const CT__xdc_runtime_Defaults_Object__count xdc_runtime_Defaults_Object__count__C = 0;

/* Object__heap__C */
#pragma DATA_SECTION(xdc_runtime_Defaults_Object__heap__C, ".const:xdc_runtime_Defaults_Object__heap__C");
__FAR__ const CT__xdc_runtime_Defaults_Object__heap xdc_runtime_Defaults_Object__heap__C = NULL;

/* Object__sizeof__C */
#pragma DATA_SECTION(xdc_runtime_Defaults_Object__sizeof__C, ".const:xdc_runtime_Defaults_Object__sizeof__C");
__FAR__ const CT__xdc_runtime_Defaults_Object__sizeof xdc_runtime_Defaults_Object__sizeof__C = 0;

/* Object__table__C */
#pragma DATA_SECTION(xdc_runtime_Defaults_Object__table__C, ".const:xdc_runtime_Defaults_Object__table__C");
__FAR__ const CT__xdc_runtime_Defaults_Object__table xdc_runtime_Defaults_Object__table__C = NULL;


/*
 * ======== xdc.runtime.Diags INITIALIZERS ========
 */

/* Module__diagsEnabled__C */
#pragma DATA_SECTION(xdc_runtime_Diags_Module__diagsEnabled__C, ".const:xdc_runtime_Diags_Module__diagsEnabled__C");
__FAR__ const CT__xdc_runtime_Diags_Module__diagsEnabled xdc_runtime_Diags_Module__diagsEnabled__C = (xdc_Bits32)0x10U;

/* Module__diagsIncluded__C */
#pragma DATA_SECTION(xdc_runtime_Diags_Module__diagsIncluded__C, ".const:xdc_runtime_Diags_Module__diagsIncluded__C");
__FAR__ const CT__xdc_runtime_Diags_Module__diagsIncluded xdc_runtime_Diags_Module__diagsIncluded__C = (xdc_Bits32)0x10U;

/* Module__diagsMask__C */
#pragma DATA_SECTION(xdc_runtime_Diags_Module__diagsMask__C, ".const:xdc_runtime_Diags_Module__diagsMask__C");
__FAR__ const CT__xdc_runtime_Diags_Module__diagsMask xdc_runtime_Diags_Module__diagsMask__C = ((const CT__xdc_runtime_Diags_Module__diagsMask)NULL);

/* Module__gateObj__C */
#pragma DATA_SECTION(xdc_runtime_Diags_Module__gateObj__C, ".const:xdc_runtime_Diags_Module__gateObj__C");
__FAR__ const CT__xdc_runtime_Diags_Module__gateObj xdc_runtime_Diags_Module__gateObj__C = ((const CT__xdc_runtime_Diags_Module__gateObj)NULL);

/* Module__gatePrms__C */
#pragma DATA_SECTION(xdc_runtime_Diags_Module__gatePrms__C, ".const:xdc_runtime_Diags_Module__gatePrms__C");
__FAR__ const CT__xdc_runtime_Diags_Module__gatePrms xdc_runtime_Diags_Module__gatePrms__C = ((const CT__xdc_runtime_Diags_Module__gatePrms)NULL);

/* Module__id__C */
#pragma DATA_SECTION(xdc_runtime_Diags_Module__id__C, ".const:xdc_runtime_Diags_Module__id__C");
__FAR__ const CT__xdc_runtime_Diags_Module__id xdc_runtime_Diags_Module__id__C = (xdc_Bits16)0x8005U;

/* Module__loggerDefined__C */
#pragma DATA_SECTION(xdc_runtime_Diags_Module__loggerDefined__C, ".const:xdc_runtime_Diags_Module__loggerDefined__C");
__FAR__ const CT__xdc_runtime_Diags_Module__loggerDefined xdc_runtime_Diags_Module__loggerDefined__C = 0;

/* Module__loggerObj__C */
#pragma DATA_SECTION(xdc_runtime_Diags_Module__loggerObj__C, ".const:xdc_runtime_Diags_Module__loggerObj__C");
__FAR__ const CT__xdc_runtime_Diags_Module__loggerObj xdc_runtime_Diags_Module__loggerObj__C = ((const CT__xdc_runtime_Diags_Module__loggerObj)NULL);

/* Module__loggerFxn0__C */
#pragma DATA_SECTION(xdc_runtime_Diags_Module__loggerFxn0__C, ".const:xdc_runtime_Diags_Module__loggerFxn0__C");
__FAR__ const CT__xdc_runtime_Diags_Module__loggerFxn0 xdc_runtime_Diags_Module__loggerFxn0__C = ((const CT__xdc_runtime_Diags_Module__loggerFxn0)NULL);

/* Module__loggerFxn1__C */
#pragma DATA_SECTION(xdc_runtime_Diags_Module__loggerFxn1__C, ".const:xdc_runtime_Diags_Module__loggerFxn1__C");
__FAR__ const CT__xdc_runtime_Diags_Module__loggerFxn1 xdc_runtime_Diags_Module__loggerFxn1__C = ((const CT__xdc_runtime_Diags_Module__loggerFxn1)NULL);

/* Module__loggerFxn2__C */
#pragma DATA_SECTION(xdc_runtime_Diags_Module__loggerFxn2__C, ".const:xdc_runtime_Diags_Module__loggerFxn2__C");
__FAR__ const CT__xdc_runtime_Diags_Module__loggerFxn2 xdc_runtime_Diags_Module__loggerFxn2__C = ((const CT__xdc_runtime_Diags_Module__loggerFxn2)NULL);

/* Module__loggerFxn4__C */
#pragma DATA_SECTION(xdc_runtime_Diags_Module__loggerFxn4__C, ".const:xdc_runtime_Diags_Module__loggerFxn4__C");
__FAR__ const CT__xdc_runtime_Diags_Module__loggerFxn4 xdc_runtime_Diags_Module__loggerFxn4__C = ((const CT__xdc_runtime_Diags_Module__loggerFxn4)NULL);

/* Module__loggerFxn8__C */
#pragma DATA_SECTION(xdc_runtime_Diags_Module__loggerFxn8__C, ".const:xdc_runtime_Diags_Module__loggerFxn8__C");
__FAR__ const CT__xdc_runtime_Diags_Module__loggerFxn8 xdc_runtime_Diags_Module__loggerFxn8__C = ((const CT__xdc_runtime_Diags_Module__loggerFxn8)NULL);

/* Object__count__C */
#pragma DATA_SECTION(xdc_runtime_Diags_Object__count__C, ".const:xdc_runtime_Diags_Object__count__C");
__FAR__ const CT__xdc_runtime_Diags_Object__count xdc_runtime_Diags_Object__count__C = 0;

/* Object__heap__C */
#pragma DATA_SECTION(xdc_runtime_Diags_Object__heap__C, ".const:xdc_runtime_Diags_Object__heap__C");
__FAR__ const CT__xdc_runtime_Diags_Object__heap xdc_runtime_Diags_Object__heap__C = NULL;

/* Object__sizeof__C */
#pragma DATA_SECTION(xdc_runtime_Diags_Object__sizeof__C, ".const:xdc_runtime_Diags_Object__sizeof__C");
__FAR__ const CT__xdc_runtime_Diags_Object__sizeof xdc_runtime_Diags_Object__sizeof__C = 0;

/* Object__table__C */
#pragma DATA_SECTION(xdc_runtime_Diags_Object__table__C, ".const:xdc_runtime_Diags_Object__table__C");
__FAR__ const CT__xdc_runtime_Diags_Object__table xdc_runtime_Diags_Object__table__C = NULL;

/* setMaskEnabled__C */
#pragma DATA_SECTION(xdc_runtime_Diags_setMaskEnabled__C, ".const:xdc_runtime_Diags_setMaskEnabled__C");
__FAR__ const CT__xdc_runtime_Diags_setMaskEnabled xdc_runtime_Diags_setMaskEnabled__C = 0;

/* dictBase__C */
#pragma DATA_SECTION(xdc_runtime_Diags_dictBase__C, ".const:xdc_runtime_Diags_dictBase__C");
__FAR__ const CT__xdc_runtime_Diags_dictBase xdc_runtime_Diags_dictBase__C = ((const CT__xdc_runtime_Diags_dictBase)NULL);


/*
 * ======== xdc.runtime.Error INITIALIZERS ========
 */

/* Module__state__V */
#ifdef __ti__
xdc_runtime_Error_Module_State__ xdc_runtime_Error_Module__state__V __attribute__ ((section(".data:xdc_runtime_Error_Module__state__V")));
#elif !(defined(__MACH__) && defined(__APPLE__))
xdc_runtime_Error_Module_State__ xdc_runtime_Error_Module__state__V __attribute__ ((section(".data_xdc_runtime_Error_Module__state__V")));
#endif
xdc_runtime_Error_Module_State__ xdc_runtime_Error_Module__state__V = {
    (xdc_UInt16)0x0U,  /* count */
};

/* Module__diagsEnabled__C */
#pragma DATA_SECTION(xdc_runtime_Error_Module__diagsEnabled__C, ".const:xdc_runtime_Error_Module__diagsEnabled__C");
__FAR__ const CT__xdc_runtime_Error_Module__diagsEnabled xdc_runtime_Error_Module__diagsEnabled__C = (xdc_Bits32)0x90U;

/* Module__diagsIncluded__C */
#pragma DATA_SECTION(xdc_runtime_Error_Module__diagsIncluded__C, ".const:xdc_runtime_Error_Module__diagsIncluded__C");
__FAR__ const CT__xdc_runtime_Error_Module__diagsIncluded xdc_runtime_Error_Module__diagsIncluded__C = (xdc_Bits32)0x90U;

/* Module__diagsMask__C */
#pragma DATA_SECTION(xdc_runtime_Error_Module__diagsMask__C, ".const:xdc_runtime_Error_Module__diagsMask__C");
__FAR__ const CT__xdc_runtime_Error_Module__diagsMask xdc_runtime_Error_Module__diagsMask__C = ((const CT__xdc_runtime_Error_Module__diagsMask)NULL);

/* Module__gateObj__C */
#pragma DATA_SECTION(xdc_runtime_Error_Module__gateObj__C, ".const:xdc_runtime_Error_Module__gateObj__C");
__FAR__ const CT__xdc_runtime_Error_Module__gateObj xdc_runtime_Error_Module__gateObj__C = ((const CT__xdc_runtime_Error_Module__gateObj)NULL);

/* Module__gatePrms__C */
#pragma DATA_SECTION(xdc_runtime_Error_Module__gatePrms__C, ".const:xdc_runtime_Error_Module__gatePrms__C");
__FAR__ const CT__xdc_runtime_Error_Module__gatePrms xdc_runtime_Error_Module__gatePrms__C = ((const CT__xdc_runtime_Error_Module__gatePrms)NULL);

/* Module__id__C */
#pragma DATA_SECTION(xdc_runtime_Error_Module__id__C, ".const:xdc_runtime_Error_Module__id__C");
__FAR__ const CT__xdc_runtime_Error_Module__id xdc_runtime_Error_Module__id__C = (xdc_Bits16)0x8006U;

/* Module__loggerDefined__C */
#pragma DATA_SECTION(xdc_runtime_Error_Module__loggerDefined__C, ".const:xdc_runtime_Error_Module__loggerDefined__C");
__FAR__ const CT__xdc_runtime_Error_Module__loggerDefined xdc_runtime_Error_Module__loggerDefined__C = 0;

/* Module__loggerObj__C */
#pragma DATA_SECTION(xdc_runtime_Error_Module__loggerObj__C, ".const:xdc_runtime_Error_Module__loggerObj__C");
__FAR__ const CT__xdc_runtime_Error_Module__loggerObj xdc_runtime_Error_Module__loggerObj__C = ((const CT__xdc_runtime_Error_Module__loggerObj)NULL);

/* Module__loggerFxn0__C */
#pragma DATA_SECTION(xdc_runtime_Error_Module__loggerFxn0__C, ".const:xdc_runtime_Error_Module__loggerFxn0__C");
__FAR__ const CT__xdc_runtime_Error_Module__loggerFxn0 xdc_runtime_Error_Module__loggerFxn0__C = ((const CT__xdc_runtime_Error_Module__loggerFxn0)NULL);

/* Module__loggerFxn1__C */
#pragma DATA_SECTION(xdc_runtime_Error_Module__loggerFxn1__C, ".const:xdc_runtime_Error_Module__loggerFxn1__C");
__FAR__ const CT__xdc_runtime_Error_Module__loggerFxn1 xdc_runtime_Error_Module__loggerFxn1__C = ((const CT__xdc_runtime_Error_Module__loggerFxn1)NULL);

/* Module__loggerFxn2__C */
#pragma DATA_SECTION(xdc_runtime_Error_Module__loggerFxn2__C, ".const:xdc_runtime_Error_Module__loggerFxn2__C");
__FAR__ const CT__xdc_runtime_Error_Module__loggerFxn2 xdc_runtime_Error_Module__loggerFxn2__C = ((const CT__xdc_runtime_Error_Module__loggerFxn2)NULL);

/* Module__loggerFxn4__C */
#pragma DATA_SECTION(xdc_runtime_Error_Module__loggerFxn4__C, ".const:xdc_runtime_Error_Module__loggerFxn4__C");
__FAR__ const CT__xdc_runtime_Error_Module__loggerFxn4 xdc_runtime_Error_Module__loggerFxn4__C = ((const CT__xdc_runtime_Error_Module__loggerFxn4)NULL);

/* Module__loggerFxn8__C */
#pragma DATA_SECTION(xdc_runtime_Error_Module__loggerFxn8__C, ".const:xdc_runtime_Error_Module__loggerFxn8__C");
__FAR__ const CT__xdc_runtime_Error_Module__loggerFxn8 xdc_runtime_Error_Module__loggerFxn8__C = ((const CT__xdc_runtime_Error_Module__loggerFxn8)NULL);

/* Object__count__C */
#pragma DATA_SECTION(xdc_runtime_Error_Object__count__C, ".const:xdc_runtime_Error_Object__count__C");
__FAR__ const CT__xdc_runtime_Error_Object__count xdc_runtime_Error_Object__count__C = 0;

/* Object__heap__C */
#pragma DATA_SECTION(xdc_runtime_Error_Object__heap__C, ".const:xdc_runtime_Error_Object__heap__C");
__FAR__ const CT__xdc_runtime_Error_Object__heap xdc_runtime_Error_Object__heap__C = NULL;

/* Object__sizeof__C */
#pragma DATA_SECTION(xdc_runtime_Error_Object__sizeof__C, ".const:xdc_runtime_Error_Object__sizeof__C");
__FAR__ const CT__xdc_runtime_Error_Object__sizeof xdc_runtime_Error_Object__sizeof__C = 0;

/* Object__table__C */
#pragma DATA_SECTION(xdc_runtime_Error_Object__table__C, ".const:xdc_runtime_Error_Object__table__C");
__FAR__ const CT__xdc_runtime_Error_Object__table xdc_runtime_Error_Object__table__C = NULL;

/* policyFxn__C */
#pragma DATA_SECTION(xdc_runtime_Error_policyFxn__C, ".const:xdc_runtime_Error_policyFxn__C");
__FAR__ const CT__xdc_runtime_Error_policyFxn xdc_runtime_Error_policyFxn__C = ((const CT__xdc_runtime_Error_policyFxn)(xdc_runtime_Error_policyDefault__E));

/* E_generic__C */
#pragma DATA_SECTION(xdc_runtime_Error_E_generic__C, ".const:xdc_runtime_Error_E_generic__C");
__FAR__ const CT__xdc_runtime_Error_E_generic xdc_runtime_Error_E_generic__C = (((xdc_runtime_Error_Id)3828) << 16 | 0U);

/* E_memory__C */
#pragma DATA_SECTION(xdc_runtime_Error_E_memory__C, ".const:xdc_runtime_Error_E_memory__C");
__FAR__ const CT__xdc_runtime_Error_E_memory xdc_runtime_Error_E_memory__C = (((xdc_runtime_Error_Id)3832) << 16 | 0U);

/* E_msgCode__C */
#pragma DATA_SECTION(xdc_runtime_Error_E_msgCode__C, ".const:xdc_runtime_Error_E_msgCode__C");
__FAR__ const CT__xdc_runtime_Error_E_msgCode xdc_runtime_Error_E_msgCode__C = (((xdc_runtime_Error_Id)3866) << 16 | 0U);

/* policy__C */
#pragma DATA_SECTION(xdc_runtime_Error_policy__C, ".const:xdc_runtime_Error_policy__C");
__FAR__ const CT__xdc_runtime_Error_policy xdc_runtime_Error_policy__C = xdc_runtime_Error_UNWIND;

/* raiseHook__C */
#pragma DATA_SECTION(xdc_runtime_Error_raiseHook__C, ".const:xdc_runtime_Error_raiseHook__C");
__FAR__ const CT__xdc_runtime_Error_raiseHook xdc_runtime_Error_raiseHook__C = ((const CT__xdc_runtime_Error_raiseHook)(ti_sysbios_BIOS_errorRaiseHook__I));

/* maxDepth__C */
#pragma DATA_SECTION(xdc_runtime_Error_maxDepth__C, ".const:xdc_runtime_Error_maxDepth__C");
__FAR__ const CT__xdc_runtime_Error_maxDepth xdc_runtime_Error_maxDepth__C = (xdc_UInt16)0x10U;


/*
 * ======== xdc.runtime.Gate INITIALIZERS ========
 */

/* Module__diagsEnabled__C */
#pragma DATA_SECTION(xdc_runtime_Gate_Module__diagsEnabled__C, ".const:xdc_runtime_Gate_Module__diagsEnabled__C");
__FAR__ const CT__xdc_runtime_Gate_Module__diagsEnabled xdc_runtime_Gate_Module__diagsEnabled__C = (xdc_Bits32)0x10U;

/* Module__diagsIncluded__C */
#pragma DATA_SECTION(xdc_runtime_Gate_Module__diagsIncluded__C, ".const:xdc_runtime_Gate_Module__diagsIncluded__C");
__FAR__ const CT__xdc_runtime_Gate_Module__diagsIncluded xdc_runtime_Gate_Module__diagsIncluded__C = (xdc_Bits32)0x10U;

/* Module__diagsMask__C */
#pragma DATA_SECTION(xdc_runtime_Gate_Module__diagsMask__C, ".const:xdc_runtime_Gate_Module__diagsMask__C");
__FAR__ const CT__xdc_runtime_Gate_Module__diagsMask xdc_runtime_Gate_Module__diagsMask__C = ((const CT__xdc_runtime_Gate_Module__diagsMask)NULL);

/* Module__gateObj__C */
#pragma DATA_SECTION(xdc_runtime_Gate_Module__gateObj__C, ".const:xdc_runtime_Gate_Module__gateObj__C");
__FAR__ const CT__xdc_runtime_Gate_Module__gateObj xdc_runtime_Gate_Module__gateObj__C = ((const CT__xdc_runtime_Gate_Module__gateObj)NULL);

/* Module__gatePrms__C */
#pragma DATA_SECTION(xdc_runtime_Gate_Module__gatePrms__C, ".const:xdc_runtime_Gate_Module__gatePrms__C");
__FAR__ const CT__xdc_runtime_Gate_Module__gatePrms xdc_runtime_Gate_Module__gatePrms__C = ((const CT__xdc_runtime_Gate_Module__gatePrms)NULL);

/* Module__id__C */
#pragma DATA_SECTION(xdc_runtime_Gate_Module__id__C, ".const:xdc_runtime_Gate_Module__id__C");
__FAR__ const CT__xdc_runtime_Gate_Module__id xdc_runtime_Gate_Module__id__C = (xdc_Bits16)0x8007U;

/* Module__loggerDefined__C */
#pragma DATA_SECTION(xdc_runtime_Gate_Module__loggerDefined__C, ".const:xdc_runtime_Gate_Module__loggerDefined__C");
__FAR__ const CT__xdc_runtime_Gate_Module__loggerDefined xdc_runtime_Gate_Module__loggerDefined__C = 0;

/* Module__loggerObj__C */
#pragma DATA_SECTION(xdc_runtime_Gate_Module__loggerObj__C, ".const:xdc_runtime_Gate_Module__loggerObj__C");
__FAR__ const CT__xdc_runtime_Gate_Module__loggerObj xdc_runtime_Gate_Module__loggerObj__C = ((const CT__xdc_runtime_Gate_Module__loggerObj)NULL);

/* Module__loggerFxn0__C */
#pragma DATA_SECTION(xdc_runtime_Gate_Module__loggerFxn0__C, ".const:xdc_runtime_Gate_Module__loggerFxn0__C");
__FAR__ const CT__xdc_runtime_Gate_Module__loggerFxn0 xdc_runtime_Gate_Module__loggerFxn0__C = ((const CT__xdc_runtime_Gate_Module__loggerFxn0)NULL);

/* Module__loggerFxn1__C */
#pragma DATA_SECTION(xdc_runtime_Gate_Module__loggerFxn1__C, ".const:xdc_runtime_Gate_Module__loggerFxn1__C");
__FAR__ const CT__xdc_runtime_Gate_Module__loggerFxn1 xdc_runtime_Gate_Module__loggerFxn1__C = ((const CT__xdc_runtime_Gate_Module__loggerFxn1)NULL);

/* Module__loggerFxn2__C */
#pragma DATA_SECTION(xdc_runtime_Gate_Module__loggerFxn2__C, ".const:xdc_runtime_Gate_Module__loggerFxn2__C");
__FAR__ const CT__xdc_runtime_Gate_Module__loggerFxn2 xdc_runtime_Gate_Module__loggerFxn2__C = ((const CT__xdc_runtime_Gate_Module__loggerFxn2)NULL);

/* Module__loggerFxn4__C */
#pragma DATA_SECTION(xdc_runtime_Gate_Module__loggerFxn4__C, ".const:xdc_runtime_Gate_Module__loggerFxn4__C");
__FAR__ const CT__xdc_runtime_Gate_Module__loggerFxn4 xdc_runtime_Gate_Module__loggerFxn4__C = ((const CT__xdc_runtime_Gate_Module__loggerFxn4)NULL);

/* Module__loggerFxn8__C */
#pragma DATA_SECTION(xdc_runtime_Gate_Module__loggerFxn8__C, ".const:xdc_runtime_Gate_Module__loggerFxn8__C");
__FAR__ const CT__xdc_runtime_Gate_Module__loggerFxn8 xdc_runtime_Gate_Module__loggerFxn8__C = ((const CT__xdc_runtime_Gate_Module__loggerFxn8)NULL);

/* Object__count__C */
#pragma DATA_SECTION(xdc_runtime_Gate_Object__count__C, ".const:xdc_runtime_Gate_Object__count__C");
__FAR__ const CT__xdc_runtime_Gate_Object__count xdc_runtime_Gate_Object__count__C = 0;

/* Object__heap__C */
#pragma DATA_SECTION(xdc_runtime_Gate_Object__heap__C, ".const:xdc_runtime_Gate_Object__heap__C");
__FAR__ const CT__xdc_runtime_Gate_Object__heap xdc_runtime_Gate_Object__heap__C = NULL;

/* Object__sizeof__C */
#pragma DATA_SECTION(xdc_runtime_Gate_Object__sizeof__C, ".const:xdc_runtime_Gate_Object__sizeof__C");
__FAR__ const CT__xdc_runtime_Gate_Object__sizeof xdc_runtime_Gate_Object__sizeof__C = 0;

/* Object__table__C */
#pragma DATA_SECTION(xdc_runtime_Gate_Object__table__C, ".const:xdc_runtime_Gate_Object__table__C");
__FAR__ const CT__xdc_runtime_Gate_Object__table xdc_runtime_Gate_Object__table__C = NULL;


/*
 * ======== xdc.runtime.Log INITIALIZERS ========
 */

/* Module__diagsEnabled__C */
#pragma DATA_SECTION(xdc_runtime_Log_Module__diagsEnabled__C, ".const:xdc_runtime_Log_Module__diagsEnabled__C");
__FAR__ const CT__xdc_runtime_Log_Module__diagsEnabled xdc_runtime_Log_Module__diagsEnabled__C = (xdc_Bits32)0x10U;

/* Module__diagsIncluded__C */
#pragma DATA_SECTION(xdc_runtime_Log_Module__diagsIncluded__C, ".const:xdc_runtime_Log_Module__diagsIncluded__C");
__FAR__ const CT__xdc_runtime_Log_Module__diagsIncluded xdc_runtime_Log_Module__diagsIncluded__C = (xdc_Bits32)0x10U;

/* Module__diagsMask__C */
#pragma DATA_SECTION(xdc_runtime_Log_Module__diagsMask__C, ".const:xdc_runtime_Log_Module__diagsMask__C");
__FAR__ const CT__xdc_runtime_Log_Module__diagsMask xdc_runtime_Log_Module__diagsMask__C = ((const CT__xdc_runtime_Log_Module__diagsMask)NULL);

/* Module__gateObj__C */
#pragma DATA_SECTION(xdc_runtime_Log_Module__gateObj__C, ".const:xdc_runtime_Log_Module__gateObj__C");
__FAR__ const CT__xdc_runtime_Log_Module__gateObj xdc_runtime_Log_Module__gateObj__C = ((const CT__xdc_runtime_Log_Module__gateObj)NULL);

/* Module__gatePrms__C */
#pragma DATA_SECTION(xdc_runtime_Log_Module__gatePrms__C, ".const:xdc_runtime_Log_Module__gatePrms__C");
__FAR__ const CT__xdc_runtime_Log_Module__gatePrms xdc_runtime_Log_Module__gatePrms__C = ((const CT__xdc_runtime_Log_Module__gatePrms)NULL);

/* Module__id__C */
#pragma DATA_SECTION(xdc_runtime_Log_Module__id__C, ".const:xdc_runtime_Log_Module__id__C");
__FAR__ const CT__xdc_runtime_Log_Module__id xdc_runtime_Log_Module__id__C = (xdc_Bits16)0x8008U;

/* Module__loggerDefined__C */
#pragma DATA_SECTION(xdc_runtime_Log_Module__loggerDefined__C, ".const:xdc_runtime_Log_Module__loggerDefined__C");
__FAR__ const CT__xdc_runtime_Log_Module__loggerDefined xdc_runtime_Log_Module__loggerDefined__C = 0;

/* Module__loggerObj__C */
#pragma DATA_SECTION(xdc_runtime_Log_Module__loggerObj__C, ".const:xdc_runtime_Log_Module__loggerObj__C");
__FAR__ const CT__xdc_runtime_Log_Module__loggerObj xdc_runtime_Log_Module__loggerObj__C = ((const CT__xdc_runtime_Log_Module__loggerObj)NULL);

/* Module__loggerFxn0__C */
#pragma DATA_SECTION(xdc_runtime_Log_Module__loggerFxn0__C, ".const:xdc_runtime_Log_Module__loggerFxn0__C");
__FAR__ const CT__xdc_runtime_Log_Module__loggerFxn0 xdc_runtime_Log_Module__loggerFxn0__C = ((const CT__xdc_runtime_Log_Module__loggerFxn0)NULL);

/* Module__loggerFxn1__C */
#pragma DATA_SECTION(xdc_runtime_Log_Module__loggerFxn1__C, ".const:xdc_runtime_Log_Module__loggerFxn1__C");
__FAR__ const CT__xdc_runtime_Log_Module__loggerFxn1 xdc_runtime_Log_Module__loggerFxn1__C = ((const CT__xdc_runtime_Log_Module__loggerFxn1)NULL);

/* Module__loggerFxn2__C */
#pragma DATA_SECTION(xdc_runtime_Log_Module__loggerFxn2__C, ".const:xdc_runtime_Log_Module__loggerFxn2__C");
__FAR__ const CT__xdc_runtime_Log_Module__loggerFxn2 xdc_runtime_Log_Module__loggerFxn2__C = ((const CT__xdc_runtime_Log_Module__loggerFxn2)NULL);

/* Module__loggerFxn4__C */
#pragma DATA_SECTION(xdc_runtime_Log_Module__loggerFxn4__C, ".const:xdc_runtime_Log_Module__loggerFxn4__C");
__FAR__ const CT__xdc_runtime_Log_Module__loggerFxn4 xdc_runtime_Log_Module__loggerFxn4__C = ((const CT__xdc_runtime_Log_Module__loggerFxn4)NULL);

/* Module__loggerFxn8__C */
#pragma DATA_SECTION(xdc_runtime_Log_Module__loggerFxn8__C, ".const:xdc_runtime_Log_Module__loggerFxn8__C");
__FAR__ const CT__xdc_runtime_Log_Module__loggerFxn8 xdc_runtime_Log_Module__loggerFxn8__C = ((const CT__xdc_runtime_Log_Module__loggerFxn8)NULL);

/* Object__count__C */
#pragma DATA_SECTION(xdc_runtime_Log_Object__count__C, ".const:xdc_runtime_Log_Object__count__C");
__FAR__ const CT__xdc_runtime_Log_Object__count xdc_runtime_Log_Object__count__C = 0;

/* Object__heap__C */
#pragma DATA_SECTION(xdc_runtime_Log_Object__heap__C, ".const:xdc_runtime_Log_Object__heap__C");
__FAR__ const CT__xdc_runtime_Log_Object__heap xdc_runtime_Log_Object__heap__C = NULL;

/* Object__sizeof__C */
#pragma DATA_SECTION(xdc_runtime_Log_Object__sizeof__C, ".const:xdc_runtime_Log_Object__sizeof__C");
__FAR__ const CT__xdc_runtime_Log_Object__sizeof xdc_runtime_Log_Object__sizeof__C = 0;

/* Object__table__C */
#pragma DATA_SECTION(xdc_runtime_Log_Object__table__C, ".const:xdc_runtime_Log_Object__table__C");
__FAR__ const CT__xdc_runtime_Log_Object__table xdc_runtime_Log_Object__table__C = NULL;

/* L_construct__C */
#pragma DATA_SECTION(xdc_runtime_Log_L_construct__C, ".const:xdc_runtime_Log_L_construct__C");
__FAR__ const CT__xdc_runtime_Log_L_construct xdc_runtime_Log_L_construct__C = (((xdc_runtime_Log_Event)5595) << 16 | 4);

/* L_create__C */
#pragma DATA_SECTION(xdc_runtime_Log_L_create__C, ".const:xdc_runtime_Log_L_create__C");
__FAR__ const CT__xdc_runtime_Log_L_create xdc_runtime_Log_L_create__C = (((xdc_runtime_Log_Event)5619) << 16 | 4);

/* L_destruct__C */
#pragma DATA_SECTION(xdc_runtime_Log_L_destruct__C, ".const:xdc_runtime_Log_L_destruct__C");
__FAR__ const CT__xdc_runtime_Log_L_destruct xdc_runtime_Log_L_destruct__C = (((xdc_runtime_Log_Event)5640) << 16 | 4);

/* L_delete__C */
#pragma DATA_SECTION(xdc_runtime_Log_L_delete__C, ".const:xdc_runtime_Log_L_delete__C");
__FAR__ const CT__xdc_runtime_Log_L_delete xdc_runtime_Log_L_delete__C = (((xdc_runtime_Log_Event)5659) << 16 | 4);

/* L_error__C */
#pragma DATA_SECTION(xdc_runtime_Log_L_error__C, ".const:xdc_runtime_Log_L_error__C");
__FAR__ const CT__xdc_runtime_Log_L_error xdc_runtime_Log_L_error__C = (((xdc_runtime_Log_Event)5676) << 16 | 192);

/* L_warning__C */
#pragma DATA_SECTION(xdc_runtime_Log_L_warning__C, ".const:xdc_runtime_Log_L_warning__C");
__FAR__ const CT__xdc_runtime_Log_L_warning xdc_runtime_Log_L_warning__C = (((xdc_runtime_Log_Event)5690) << 16 | 224);

/* L_info__C */
#pragma DATA_SECTION(xdc_runtime_Log_L_info__C, ".const:xdc_runtime_Log_L_info__C");
__FAR__ const CT__xdc_runtime_Log_L_info xdc_runtime_Log_L_info__C = (((xdc_runtime_Log_Event)5706) << 16 | 16384);

/* L_start__C */
#pragma DATA_SECTION(xdc_runtime_Log_L_start__C, ".const:xdc_runtime_Log_L_start__C");
__FAR__ const CT__xdc_runtime_Log_L_start xdc_runtime_Log_L_start__C = (((xdc_runtime_Log_Event)5713) << 16 | 32768);

/* L_stop__C */
#pragma DATA_SECTION(xdc_runtime_Log_L_stop__C, ".const:xdc_runtime_Log_L_stop__C");
__FAR__ const CT__xdc_runtime_Log_L_stop xdc_runtime_Log_L_stop__C = (((xdc_runtime_Log_Event)5724) << 16 | 32768);

/* L_startInstance__C */
#pragma DATA_SECTION(xdc_runtime_Log_L_startInstance__C, ".const:xdc_runtime_Log_L_startInstance__C");
__FAR__ const CT__xdc_runtime_Log_L_startInstance xdc_runtime_Log_L_startInstance__C = (((xdc_runtime_Log_Event)5734) << 16 | 32768);

/* L_stopInstance__C */
#pragma DATA_SECTION(xdc_runtime_Log_L_stopInstance__C, ".const:xdc_runtime_Log_L_stopInstance__C");
__FAR__ const CT__xdc_runtime_Log_L_stopInstance xdc_runtime_Log_L_stopInstance__C = (((xdc_runtime_Log_Event)5753) << 16 | 32768);


/*
 * ======== xdc.runtime.Main INITIALIZERS ========
 */

/* Module__diagsEnabled__C */
#pragma DATA_SECTION(xdc_runtime_Main_Module__diagsEnabled__C, ".const:xdc_runtime_Main_Module__diagsEnabled__C");
__FAR__ const CT__xdc_runtime_Main_Module__diagsEnabled xdc_runtime_Main_Module__diagsEnabled__C = (xdc_Bits32)0x90U;

/* Module__diagsIncluded__C */
#pragma DATA_SECTION(xdc_runtime_Main_Module__diagsIncluded__C, ".const:xdc_runtime_Main_Module__diagsIncluded__C");
__FAR__ const CT__xdc_runtime_Main_Module__diagsIncluded xdc_runtime_Main_Module__diagsIncluded__C = (xdc_Bits32)0x90U;

/* Module__diagsMask__C */
#pragma DATA_SECTION(xdc_runtime_Main_Module__diagsMask__C, ".const:xdc_runtime_Main_Module__diagsMask__C");
__FAR__ const CT__xdc_runtime_Main_Module__diagsMask xdc_runtime_Main_Module__diagsMask__C = ((const CT__xdc_runtime_Main_Module__diagsMask)NULL);

/* Module__gateObj__C */
#pragma DATA_SECTION(xdc_runtime_Main_Module__gateObj__C, ".const:xdc_runtime_Main_Module__gateObj__C");
__FAR__ const CT__xdc_runtime_Main_Module__gateObj xdc_runtime_Main_Module__gateObj__C = ((const CT__xdc_runtime_Main_Module__gateObj)((void*)(xdc_runtime_IGateProvider_Handle)&ti_sysbios_gates_GateHwi_Object__table__V[0]));

/* Module__gatePrms__C */
#pragma DATA_SECTION(xdc_runtime_Main_Module__gatePrms__C, ".const:xdc_runtime_Main_Module__gatePrms__C");
__FAR__ const CT__xdc_runtime_Main_Module__gatePrms xdc_runtime_Main_Module__gatePrms__C = ((const CT__xdc_runtime_Main_Module__gatePrms)NULL);

/* Module__id__C */
#pragma DATA_SECTION(xdc_runtime_Main_Module__id__C, ".const:xdc_runtime_Main_Module__id__C");
__FAR__ const CT__xdc_runtime_Main_Module__id xdc_runtime_Main_Module__id__C = (xdc_Bits16)0x8009U;

/* Module__loggerDefined__C */
#pragma DATA_SECTION(xdc_runtime_Main_Module__loggerDefined__C, ".const:xdc_runtime_Main_Module__loggerDefined__C");
__FAR__ const CT__xdc_runtime_Main_Module__loggerDefined xdc_runtime_Main_Module__loggerDefined__C = 0;

/* Module__loggerObj__C */
#pragma DATA_SECTION(xdc_runtime_Main_Module__loggerObj__C, ".const:xdc_runtime_Main_Module__loggerObj__C");
__FAR__ const CT__xdc_runtime_Main_Module__loggerObj xdc_runtime_Main_Module__loggerObj__C = ((const CT__xdc_runtime_Main_Module__loggerObj)NULL);

/* Module__loggerFxn0__C */
#pragma DATA_SECTION(xdc_runtime_Main_Module__loggerFxn0__C, ".const:xdc_runtime_Main_Module__loggerFxn0__C");
__FAR__ const CT__xdc_runtime_Main_Module__loggerFxn0 xdc_runtime_Main_Module__loggerFxn0__C = ((const CT__xdc_runtime_Main_Module__loggerFxn0)NULL);

/* Module__loggerFxn1__C */
#pragma DATA_SECTION(xdc_runtime_Main_Module__loggerFxn1__C, ".const:xdc_runtime_Main_Module__loggerFxn1__C");
__FAR__ const CT__xdc_runtime_Main_Module__loggerFxn1 xdc_runtime_Main_Module__loggerFxn1__C = ((const CT__xdc_runtime_Main_Module__loggerFxn1)NULL);

/* Module__loggerFxn2__C */
#pragma DATA_SECTION(xdc_runtime_Main_Module__loggerFxn2__C, ".const:xdc_runtime_Main_Module__loggerFxn2__C");
__FAR__ const CT__xdc_runtime_Main_Module__loggerFxn2 xdc_runtime_Main_Module__loggerFxn2__C = ((const CT__xdc_runtime_Main_Module__loggerFxn2)NULL);

/* Module__loggerFxn4__C */
#pragma DATA_SECTION(xdc_runtime_Main_Module__loggerFxn4__C, ".const:xdc_runtime_Main_Module__loggerFxn4__C");
__FAR__ const CT__xdc_runtime_Main_Module__loggerFxn4 xdc_runtime_Main_Module__loggerFxn4__C = ((const CT__xdc_runtime_Main_Module__loggerFxn4)NULL);

/* Module__loggerFxn8__C */
#pragma DATA_SECTION(xdc_runtime_Main_Module__loggerFxn8__C, ".const:xdc_runtime_Main_Module__loggerFxn8__C");
__FAR__ const CT__xdc_runtime_Main_Module__loggerFxn8 xdc_runtime_Main_Module__loggerFxn8__C = ((const CT__xdc_runtime_Main_Module__loggerFxn8)NULL);

/* Object__count__C */
#pragma DATA_SECTION(xdc_runtime_Main_Object__count__C, ".const:xdc_runtime_Main_Object__count__C");
__FAR__ const CT__xdc_runtime_Main_Object__count xdc_runtime_Main_Object__count__C = 0;

/* Object__heap__C */
#pragma DATA_SECTION(xdc_runtime_Main_Object__heap__C, ".const:xdc_runtime_Main_Object__heap__C");
__FAR__ const CT__xdc_runtime_Main_Object__heap xdc_runtime_Main_Object__heap__C = NULL;

/* Object__sizeof__C */
#pragma DATA_SECTION(xdc_runtime_Main_Object__sizeof__C, ".const:xdc_runtime_Main_Object__sizeof__C");
__FAR__ const CT__xdc_runtime_Main_Object__sizeof xdc_runtime_Main_Object__sizeof__C = 0;

/* Object__table__C */
#pragma DATA_SECTION(xdc_runtime_Main_Object__table__C, ".const:xdc_runtime_Main_Object__table__C");
__FAR__ const CT__xdc_runtime_Main_Object__table xdc_runtime_Main_Object__table__C = NULL;


/*
 * ======== xdc.runtime.Main_Module_GateProxy INITIALIZERS ========
 */


/*
 * ======== xdc.runtime.Memory INITIALIZERS ========
 */

/* Module__state__V */
#ifdef __ti__
xdc_runtime_Memory_Module_State__ xdc_runtime_Memory_Module__state__V __attribute__ ((section(".data:xdc_runtime_Memory_Module__state__V")));
#elif !(defined(__MACH__) && defined(__APPLE__))
xdc_runtime_Memory_Module_State__ xdc_runtime_Memory_Module__state__V __attribute__ ((section(".data_xdc_runtime_Memory_Module__state__V")));
#endif
xdc_runtime_Memory_Module_State__ xdc_runtime_Memory_Module__state__V = {
    (xdc_SizeT)0x8,  /* maxDefaultTypeAlign */
};

/* Module__diagsEnabled__C */
#pragma DATA_SECTION(xdc_runtime_Memory_Module__diagsEnabled__C, ".const:xdc_runtime_Memory_Module__diagsEnabled__C");
__FAR__ const CT__xdc_runtime_Memory_Module__diagsEnabled xdc_runtime_Memory_Module__diagsEnabled__C = (xdc_Bits32)0x10U;

/* Module__diagsIncluded__C */
#pragma DATA_SECTION(xdc_runtime_Memory_Module__diagsIncluded__C, ".const:xdc_runtime_Memory_Module__diagsIncluded__C");
__FAR__ const CT__xdc_runtime_Memory_Module__diagsIncluded xdc_runtime_Memory_Module__diagsIncluded__C = (xdc_Bits32)0x10U;

/* Module__diagsMask__C */
#pragma DATA_SECTION(xdc_runtime_Memory_Module__diagsMask__C, ".const:xdc_runtime_Memory_Module__diagsMask__C");
__FAR__ const CT__xdc_runtime_Memory_Module__diagsMask xdc_runtime_Memory_Module__diagsMask__C = ((const CT__xdc_runtime_Memory_Module__diagsMask)NULL);

/* Module__gateObj__C */
#pragma DATA_SECTION(xdc_runtime_Memory_Module__gateObj__C, ".const:xdc_runtime_Memory_Module__gateObj__C");
__FAR__ const CT__xdc_runtime_Memory_Module__gateObj xdc_runtime_Memory_Module__gateObj__C = ((const CT__xdc_runtime_Memory_Module__gateObj)NULL);

/* Module__gatePrms__C */
#pragma DATA_SECTION(xdc_runtime_Memory_Module__gatePrms__C, ".const:xdc_runtime_Memory_Module__gatePrms__C");
__FAR__ const CT__xdc_runtime_Memory_Module__gatePrms xdc_runtime_Memory_Module__gatePrms__C = ((const CT__xdc_runtime_Memory_Module__gatePrms)NULL);

/* Module__id__C */
#pragma DATA_SECTION(xdc_runtime_Memory_Module__id__C, ".const:xdc_runtime_Memory_Module__id__C");
__FAR__ const CT__xdc_runtime_Memory_Module__id xdc_runtime_Memory_Module__id__C = (xdc_Bits16)0x800aU;

/* Module__loggerDefined__C */
#pragma DATA_SECTION(xdc_runtime_Memory_Module__loggerDefined__C, ".const:xdc_runtime_Memory_Module__loggerDefined__C");
__FAR__ const CT__xdc_runtime_Memory_Module__loggerDefined xdc_runtime_Memory_Module__loggerDefined__C = 0;

/* Module__loggerObj__C */
#pragma DATA_SECTION(xdc_runtime_Memory_Module__loggerObj__C, ".const:xdc_runtime_Memory_Module__loggerObj__C");
__FAR__ const CT__xdc_runtime_Memory_Module__loggerObj xdc_runtime_Memory_Module__loggerObj__C = ((const CT__xdc_runtime_Memory_Module__loggerObj)NULL);

/* Module__loggerFxn0__C */
#pragma DATA_SECTION(xdc_runtime_Memory_Module__loggerFxn0__C, ".const:xdc_runtime_Memory_Module__loggerFxn0__C");
__FAR__ const CT__xdc_runtime_Memory_Module__loggerFxn0 xdc_runtime_Memory_Module__loggerFxn0__C = ((const CT__xdc_runtime_Memory_Module__loggerFxn0)NULL);

/* Module__loggerFxn1__C */
#pragma DATA_SECTION(xdc_runtime_Memory_Module__loggerFxn1__C, ".const:xdc_runtime_Memory_Module__loggerFxn1__C");
__FAR__ const CT__xdc_runtime_Memory_Module__loggerFxn1 xdc_runtime_Memory_Module__loggerFxn1__C = ((const CT__xdc_runtime_Memory_Module__loggerFxn1)NULL);

/* Module__loggerFxn2__C */
#pragma DATA_SECTION(xdc_runtime_Memory_Module__loggerFxn2__C, ".const:xdc_runtime_Memory_Module__loggerFxn2__C");
__FAR__ const CT__xdc_runtime_Memory_Module__loggerFxn2 xdc_runtime_Memory_Module__loggerFxn2__C = ((const CT__xdc_runtime_Memory_Module__loggerFxn2)NULL);

/* Module__loggerFxn4__C */
#pragma DATA_SECTION(xdc_runtime_Memory_Module__loggerFxn4__C, ".const:xdc_runtime_Memory_Module__loggerFxn4__C");
__FAR__ const CT__xdc_runtime_Memory_Module__loggerFxn4 xdc_runtime_Memory_Module__loggerFxn4__C = ((const CT__xdc_runtime_Memory_Module__loggerFxn4)NULL);

/* Module__loggerFxn8__C */
#pragma DATA_SECTION(xdc_runtime_Memory_Module__loggerFxn8__C, ".const:xdc_runtime_Memory_Module__loggerFxn8__C");
__FAR__ const CT__xdc_runtime_Memory_Module__loggerFxn8 xdc_runtime_Memory_Module__loggerFxn8__C = ((const CT__xdc_runtime_Memory_Module__loggerFxn8)NULL);

/* Object__count__C */
#pragma DATA_SECTION(xdc_runtime_Memory_Object__count__C, ".const:xdc_runtime_Memory_Object__count__C");
__FAR__ const CT__xdc_runtime_Memory_Object__count xdc_runtime_Memory_Object__count__C = 0;

/* Object__heap__C */
#pragma DATA_SECTION(xdc_runtime_Memory_Object__heap__C, ".const:xdc_runtime_Memory_Object__heap__C");
__FAR__ const CT__xdc_runtime_Memory_Object__heap xdc_runtime_Memory_Object__heap__C = NULL;

/* Object__sizeof__C */
#pragma DATA_SECTION(xdc_runtime_Memory_Object__sizeof__C, ".const:xdc_runtime_Memory_Object__sizeof__C");
__FAR__ const CT__xdc_runtime_Memory_Object__sizeof xdc_runtime_Memory_Object__sizeof__C = 0;

/* Object__table__C */
#pragma DATA_SECTION(xdc_runtime_Memory_Object__table__C, ".const:xdc_runtime_Memory_Object__table__C");
__FAR__ const CT__xdc_runtime_Memory_Object__table xdc_runtime_Memory_Object__table__C = NULL;

/* defaultHeapInstance__C */
#pragma DATA_SECTION(xdc_runtime_Memory_defaultHeapInstance__C, ".const:xdc_runtime_Memory_defaultHeapInstance__C");
__FAR__ const CT__xdc_runtime_Memory_defaultHeapInstance xdc_runtime_Memory_defaultHeapInstance__C = (xdc_runtime_IHeap_Handle)&ti_sysbios_heaps_HeapMem_Object__table__V[0];


/*
 * ======== xdc.runtime.Memory_HeapProxy INITIALIZERS ========
 */


/*
 * ======== xdc.runtime.Registry INITIALIZERS ========
 */

/* Module__state__V */
#ifdef __ti__
xdc_runtime_Registry_Module_State__ xdc_runtime_Registry_Module__state__V __attribute__ ((section(".data:xdc_runtime_Registry_Module__state__V")));
#elif !(defined(__MACH__) && defined(__APPLE__))
xdc_runtime_Registry_Module_State__ xdc_runtime_Registry_Module__state__V __attribute__ ((section(".data_xdc_runtime_Registry_Module__state__V")));
#endif
xdc_runtime_Registry_Module_State__ xdc_runtime_Registry_Module__state__V = {
    ((xdc_runtime_Types_RegDesc*)NULL),  /* listHead */
    (xdc_Bits16)0x7fffU,  /* curId */
};

/* Module__diagsEnabled__C */
#pragma DATA_SECTION(xdc_runtime_Registry_Module__diagsEnabled__C, ".const:xdc_runtime_Registry_Module__diagsEnabled__C");
__FAR__ const CT__xdc_runtime_Registry_Module__diagsEnabled xdc_runtime_Registry_Module__diagsEnabled__C = (xdc_Bits32)0x90U;

/* Module__diagsIncluded__C */
#pragma DATA_SECTION(xdc_runtime_Registry_Module__diagsIncluded__C, ".const:xdc_runtime_Registry_Module__diagsIncluded__C");
__FAR__ const CT__xdc_runtime_Registry_Module__diagsIncluded xdc_runtime_Registry_Module__diagsIncluded__C = (xdc_Bits32)0x90U;

/* Module__diagsMask__C */
#pragma DATA_SECTION(xdc_runtime_Registry_Module__diagsMask__C, ".const:xdc_runtime_Registry_Module__diagsMask__C");
__FAR__ const CT__xdc_runtime_Registry_Module__diagsMask xdc_runtime_Registry_Module__diagsMask__C = ((const CT__xdc_runtime_Registry_Module__diagsMask)NULL);

/* Module__gateObj__C */
#pragma DATA_SECTION(xdc_runtime_Registry_Module__gateObj__C, ".const:xdc_runtime_Registry_Module__gateObj__C");
__FAR__ const CT__xdc_runtime_Registry_Module__gateObj xdc_runtime_Registry_Module__gateObj__C = ((const CT__xdc_runtime_Registry_Module__gateObj)NULL);

/* Module__gatePrms__C */
#pragma DATA_SECTION(xdc_runtime_Registry_Module__gatePrms__C, ".const:xdc_runtime_Registry_Module__gatePrms__C");
__FAR__ const CT__xdc_runtime_Registry_Module__gatePrms xdc_runtime_Registry_Module__gatePrms__C = ((const CT__xdc_runtime_Registry_Module__gatePrms)NULL);

/* Module__id__C */
#pragma DATA_SECTION(xdc_runtime_Registry_Module__id__C, ".const:xdc_runtime_Registry_Module__id__C");
__FAR__ const CT__xdc_runtime_Registry_Module__id xdc_runtime_Registry_Module__id__C = (xdc_Bits16)0x800bU;

/* Module__loggerDefined__C */
#pragma DATA_SECTION(xdc_runtime_Registry_Module__loggerDefined__C, ".const:xdc_runtime_Registry_Module__loggerDefined__C");
__FAR__ const CT__xdc_runtime_Registry_Module__loggerDefined xdc_runtime_Registry_Module__loggerDefined__C = 0;

/* Module__loggerObj__C */
#pragma DATA_SECTION(xdc_runtime_Registry_Module__loggerObj__C, ".const:xdc_runtime_Registry_Module__loggerObj__C");
__FAR__ const CT__xdc_runtime_Registry_Module__loggerObj xdc_runtime_Registry_Module__loggerObj__C = ((const CT__xdc_runtime_Registry_Module__loggerObj)NULL);

/* Module__loggerFxn0__C */
#pragma DATA_SECTION(xdc_runtime_Registry_Module__loggerFxn0__C, ".const:xdc_runtime_Registry_Module__loggerFxn0__C");
__FAR__ const CT__xdc_runtime_Registry_Module__loggerFxn0 xdc_runtime_Registry_Module__loggerFxn0__C = ((const CT__xdc_runtime_Registry_Module__loggerFxn0)NULL);

/* Module__loggerFxn1__C */
#pragma DATA_SECTION(xdc_runtime_Registry_Module__loggerFxn1__C, ".const:xdc_runtime_Registry_Module__loggerFxn1__C");
__FAR__ const CT__xdc_runtime_Registry_Module__loggerFxn1 xdc_runtime_Registry_Module__loggerFxn1__C = ((const CT__xdc_runtime_Registry_Module__loggerFxn1)NULL);

/* Module__loggerFxn2__C */
#pragma DATA_SECTION(xdc_runtime_Registry_Module__loggerFxn2__C, ".const:xdc_runtime_Registry_Module__loggerFxn2__C");
__FAR__ const CT__xdc_runtime_Registry_Module__loggerFxn2 xdc_runtime_Registry_Module__loggerFxn2__C = ((const CT__xdc_runtime_Registry_Module__loggerFxn2)NULL);

/* Module__loggerFxn4__C */
#pragma DATA_SECTION(xdc_runtime_Registry_Module__loggerFxn4__C, ".const:xdc_runtime_Registry_Module__loggerFxn4__C");
__FAR__ const CT__xdc_runtime_Registry_Module__loggerFxn4 xdc_runtime_Registry_Module__loggerFxn4__C = ((const CT__xdc_runtime_Registry_Module__loggerFxn4)NULL);

/* Module__loggerFxn8__C */
#pragma DATA_SECTION(xdc_runtime_Registry_Module__loggerFxn8__C, ".const:xdc_runtime_Registry_Module__loggerFxn8__C");
__FAR__ const CT__xdc_runtime_Registry_Module__loggerFxn8 xdc_runtime_Registry_Module__loggerFxn8__C = ((const CT__xdc_runtime_Registry_Module__loggerFxn8)NULL);

/* Object__count__C */
#pragma DATA_SECTION(xdc_runtime_Registry_Object__count__C, ".const:xdc_runtime_Registry_Object__count__C");
__FAR__ const CT__xdc_runtime_Registry_Object__count xdc_runtime_Registry_Object__count__C = 0;

/* Object__heap__C */
#pragma DATA_SECTION(xdc_runtime_Registry_Object__heap__C, ".const:xdc_runtime_Registry_Object__heap__C");
__FAR__ const CT__xdc_runtime_Registry_Object__heap xdc_runtime_Registry_Object__heap__C = NULL;

/* Object__sizeof__C */
#pragma DATA_SECTION(xdc_runtime_Registry_Object__sizeof__C, ".const:xdc_runtime_Registry_Object__sizeof__C");
__FAR__ const CT__xdc_runtime_Registry_Object__sizeof xdc_runtime_Registry_Object__sizeof__C = 0;

/* Object__table__C */
#pragma DATA_SECTION(xdc_runtime_Registry_Object__table__C, ".const:xdc_runtime_Registry_Object__table__C");
__FAR__ const CT__xdc_runtime_Registry_Object__table xdc_runtime_Registry_Object__table__C = NULL;


/*
 * ======== xdc.runtime.Startup INITIALIZERS ========
 */

/* Module__state__V */
#ifdef __ti__
xdc_runtime_Startup_Module_State__ xdc_runtime_Startup_Module__state__V __attribute__ ((section(".data:xdc_runtime_Startup_Module__state__V")));
#elif !(defined(__MACH__) && defined(__APPLE__))
xdc_runtime_Startup_Module_State__ xdc_runtime_Startup_Module__state__V __attribute__ ((section(".data_xdc_runtime_Startup_Module__state__V")));
#endif
xdc_runtime_Startup_Module_State__ xdc_runtime_Startup_Module__state__V = {
    ((xdc_Int*)NULL),  /* stateTab */
    0,  /* execFlag */
    0,  /* rtsDoneFlag */
};

/* --> xdc_runtime_Startup_firstFxns__A */
#pragma DATA_SECTION(xdc_runtime_Startup_firstFxns__A, ".const:xdc_runtime_Startup_firstFxns__A");
const __T1_xdc_runtime_Startup_firstFxns xdc_runtime_Startup_firstFxns__A[2] = {
    ((xdc_Void(*)(xdc_Void))(ti_sysbios_heaps_HeapMem_init__I)),  /* [0] */
    ((xdc_Void(*)(xdc_Void))(ti_sysbios_hal_Hwi_initStack)),  /* [1] */
};

/* --> xdc_runtime_Startup_sfxnTab__A */
#pragma DATA_SECTION(xdc_runtime_Startup_sfxnTab__A, ".const:xdc_runtime_Startup_sfxnTab__A");
const __T1_xdc_runtime_Startup_sfxnTab xdc_runtime_Startup_sfxnTab__A[12] = {
    ((xdc_Int(*)(xdc_Int f_arg0))(xdc_runtime_System_Module_startup__E)),  /* [0] */
    ((xdc_Int(*)(xdc_Int f_arg0))(xdc_runtime_SysMin_Module_startup__E)),  /* [1] */
    ((xdc_Int(*)(xdc_Int f_arg0))(ti_sysbios_family_c7x_Cache_Module_startup__E)),  /* [2] */
    ((xdc_Int(*)(xdc_Int f_arg0))(ti_sysbios_family_c7x_Exception_Module_startup__E)),  /* [3] */
    ((xdc_Int(*)(xdc_Int f_arg0))(ti_sysbios_family_c7x_Hwi_Module_startup__E)),  /* [4] */
    ((xdc_Int(*)(xdc_Int f_arg0))(ti_sysbios_family_c7x_TimestampProvider_Module_startup__E)),  /* [5] */
    ((xdc_Int(*)(xdc_Int f_arg0))(ti_sysbios_knl_Clock_Module_startup__E)),  /* [6] */
    ((xdc_Int(*)(xdc_Int f_arg0))(ti_sysbios_knl_Swi_Module_startup__E)),  /* [7] */
    ((xdc_Int(*)(xdc_Int f_arg0))(ti_sysbios_knl_Task_Module_startup__E)),  /* [8] */
    ((xdc_Int(*)(xdc_Int f_arg0))(ti_sysbios_hal_Hwi_Module_startup__E)),  /* [9] */
    ((xdc_Int(*)(xdc_Int f_arg0))(ti_sysbios_heaps_HeapBuf_Module_startup__E)),  /* [10] */
    ((xdc_Int(*)(xdc_Int f_arg0))(ti_sysbios_timers_dmtimer_Timer_Module_startup__E)),  /* [11] */
};

/* --> xdc_runtime_Startup_sfxnRts__A */
#pragma DATA_SECTION(xdc_runtime_Startup_sfxnRts__A, ".const:xdc_runtime_Startup_sfxnRts__A");
const __T1_xdc_runtime_Startup_sfxnRts xdc_runtime_Startup_sfxnRts__A[12] = {
    1,  /* [0] */
    1,  /* [1] */
    0,  /* [2] */
    0,  /* [3] */
    0,  /* [4] */
    1,  /* [5] */
    0,  /* [6] */
    0,  /* [7] */
    0,  /* [8] */
    0,  /* [9] */
    1,  /* [10] */
    0,  /* [11] */
};

/* Module__diagsEnabled__C */
#pragma DATA_SECTION(xdc_runtime_Startup_Module__diagsEnabled__C, ".const:xdc_runtime_Startup_Module__diagsEnabled__C");
__FAR__ const CT__xdc_runtime_Startup_Module__diagsEnabled xdc_runtime_Startup_Module__diagsEnabled__C = (xdc_Bits32)0x10U;

/* Module__diagsIncluded__C */
#pragma DATA_SECTION(xdc_runtime_Startup_Module__diagsIncluded__C, ".const:xdc_runtime_Startup_Module__diagsIncluded__C");
__FAR__ const CT__xdc_runtime_Startup_Module__diagsIncluded xdc_runtime_Startup_Module__diagsIncluded__C = (xdc_Bits32)0x10U;

/* Module__diagsMask__C */
#pragma DATA_SECTION(xdc_runtime_Startup_Module__diagsMask__C, ".const:xdc_runtime_Startup_Module__diagsMask__C");
__FAR__ const CT__xdc_runtime_Startup_Module__diagsMask xdc_runtime_Startup_Module__diagsMask__C = ((const CT__xdc_runtime_Startup_Module__diagsMask)NULL);

/* Module__gateObj__C */
#pragma DATA_SECTION(xdc_runtime_Startup_Module__gateObj__C, ".const:xdc_runtime_Startup_Module__gateObj__C");
__FAR__ const CT__xdc_runtime_Startup_Module__gateObj xdc_runtime_Startup_Module__gateObj__C = ((const CT__xdc_runtime_Startup_Module__gateObj)NULL);

/* Module__gatePrms__C */
#pragma DATA_SECTION(xdc_runtime_Startup_Module__gatePrms__C, ".const:xdc_runtime_Startup_Module__gatePrms__C");
__FAR__ const CT__xdc_runtime_Startup_Module__gatePrms xdc_runtime_Startup_Module__gatePrms__C = ((const CT__xdc_runtime_Startup_Module__gatePrms)NULL);

/* Module__id__C */
#pragma DATA_SECTION(xdc_runtime_Startup_Module__id__C, ".const:xdc_runtime_Startup_Module__id__C");
__FAR__ const CT__xdc_runtime_Startup_Module__id xdc_runtime_Startup_Module__id__C = (xdc_Bits16)0x800cU;

/* Module__loggerDefined__C */
#pragma DATA_SECTION(xdc_runtime_Startup_Module__loggerDefined__C, ".const:xdc_runtime_Startup_Module__loggerDefined__C");
__FAR__ const CT__xdc_runtime_Startup_Module__loggerDefined xdc_runtime_Startup_Module__loggerDefined__C = 0;

/* Module__loggerObj__C */
#pragma DATA_SECTION(xdc_runtime_Startup_Module__loggerObj__C, ".const:xdc_runtime_Startup_Module__loggerObj__C");
__FAR__ const CT__xdc_runtime_Startup_Module__loggerObj xdc_runtime_Startup_Module__loggerObj__C = ((const CT__xdc_runtime_Startup_Module__loggerObj)NULL);

/* Module__loggerFxn0__C */
#pragma DATA_SECTION(xdc_runtime_Startup_Module__loggerFxn0__C, ".const:xdc_runtime_Startup_Module__loggerFxn0__C");
__FAR__ const CT__xdc_runtime_Startup_Module__loggerFxn0 xdc_runtime_Startup_Module__loggerFxn0__C = ((const CT__xdc_runtime_Startup_Module__loggerFxn0)NULL);

/* Module__loggerFxn1__C */
#pragma DATA_SECTION(xdc_runtime_Startup_Module__loggerFxn1__C, ".const:xdc_runtime_Startup_Module__loggerFxn1__C");
__FAR__ const CT__xdc_runtime_Startup_Module__loggerFxn1 xdc_runtime_Startup_Module__loggerFxn1__C = ((const CT__xdc_runtime_Startup_Module__loggerFxn1)NULL);

/* Module__loggerFxn2__C */
#pragma DATA_SECTION(xdc_runtime_Startup_Module__loggerFxn2__C, ".const:xdc_runtime_Startup_Module__loggerFxn2__C");
__FAR__ const CT__xdc_runtime_Startup_Module__loggerFxn2 xdc_runtime_Startup_Module__loggerFxn2__C = ((const CT__xdc_runtime_Startup_Module__loggerFxn2)NULL);

/* Module__loggerFxn4__C */
#pragma DATA_SECTION(xdc_runtime_Startup_Module__loggerFxn4__C, ".const:xdc_runtime_Startup_Module__loggerFxn4__C");
__FAR__ const CT__xdc_runtime_Startup_Module__loggerFxn4 xdc_runtime_Startup_Module__loggerFxn4__C = ((const CT__xdc_runtime_Startup_Module__loggerFxn4)NULL);

/* Module__loggerFxn8__C */
#pragma DATA_SECTION(xdc_runtime_Startup_Module__loggerFxn8__C, ".const:xdc_runtime_Startup_Module__loggerFxn8__C");
__FAR__ const CT__xdc_runtime_Startup_Module__loggerFxn8 xdc_runtime_Startup_Module__loggerFxn8__C = ((const CT__xdc_runtime_Startup_Module__loggerFxn8)NULL);

/* Object__count__C */
#pragma DATA_SECTION(xdc_runtime_Startup_Object__count__C, ".const:xdc_runtime_Startup_Object__count__C");
__FAR__ const CT__xdc_runtime_Startup_Object__count xdc_runtime_Startup_Object__count__C = 0;

/* Object__heap__C */
#pragma DATA_SECTION(xdc_runtime_Startup_Object__heap__C, ".const:xdc_runtime_Startup_Object__heap__C");
__FAR__ const CT__xdc_runtime_Startup_Object__heap xdc_runtime_Startup_Object__heap__C = NULL;

/* Object__sizeof__C */
#pragma DATA_SECTION(xdc_runtime_Startup_Object__sizeof__C, ".const:xdc_runtime_Startup_Object__sizeof__C");
__FAR__ const CT__xdc_runtime_Startup_Object__sizeof xdc_runtime_Startup_Object__sizeof__C = 0;

/* Object__table__C */
#pragma DATA_SECTION(xdc_runtime_Startup_Object__table__C, ".const:xdc_runtime_Startup_Object__table__C");
__FAR__ const CT__xdc_runtime_Startup_Object__table xdc_runtime_Startup_Object__table__C = NULL;

/* maxPasses__C */
#pragma DATA_SECTION(xdc_runtime_Startup_maxPasses__C, ".const:xdc_runtime_Startup_maxPasses__C");
__FAR__ const CT__xdc_runtime_Startup_maxPasses xdc_runtime_Startup_maxPasses__C = (xdc_Int)0x20;

/* firstFxns__C */
#pragma DATA_SECTION(xdc_runtime_Startup_firstFxns__C, ".const:xdc_runtime_Startup_firstFxns__C");
__FAR__ const CT__xdc_runtime_Startup_firstFxns xdc_runtime_Startup_firstFxns__C = {2, ((__T1_xdc_runtime_Startup_firstFxns const  *)xdc_runtime_Startup_firstFxns__A)};

/* lastFxns__C */
#pragma DATA_SECTION(xdc_runtime_Startup_lastFxns__C, ".const:xdc_runtime_Startup_lastFxns__C");
__FAR__ const CT__xdc_runtime_Startup_lastFxns xdc_runtime_Startup_lastFxns__C = {0, 0};

/* startModsFxn__C */
#pragma DATA_SECTION(xdc_runtime_Startup_startModsFxn__C, ".const:xdc_runtime_Startup_startModsFxn__C");
__FAR__ const CT__xdc_runtime_Startup_startModsFxn xdc_runtime_Startup_startModsFxn__C = ((const CT__xdc_runtime_Startup_startModsFxn)(xdc_runtime_Startup_startMods__I));

/* execImpl__C */
#pragma DATA_SECTION(xdc_runtime_Startup_execImpl__C, ".const:xdc_runtime_Startup_execImpl__C");
__FAR__ const CT__xdc_runtime_Startup_execImpl xdc_runtime_Startup_execImpl__C = ((const CT__xdc_runtime_Startup_execImpl)(xdc_runtime_Startup_exec__I));

/* sfxnTab__C */
#pragma DATA_SECTION(xdc_runtime_Startup_sfxnTab__C, ".const:xdc_runtime_Startup_sfxnTab__C");
__FAR__ const CT__xdc_runtime_Startup_sfxnTab xdc_runtime_Startup_sfxnTab__C = ((const CT__xdc_runtime_Startup_sfxnTab)xdc_runtime_Startup_sfxnTab__A);

/* sfxnRts__C */
#pragma DATA_SECTION(xdc_runtime_Startup_sfxnRts__C, ".const:xdc_runtime_Startup_sfxnRts__C");
__FAR__ const CT__xdc_runtime_Startup_sfxnRts xdc_runtime_Startup_sfxnRts__C = ((const CT__xdc_runtime_Startup_sfxnRts)xdc_runtime_Startup_sfxnRts__A);


/*
 * ======== xdc.runtime.SysMin INITIALIZERS ========
 */

/* --> xdc_runtime_SysMin_Module_State_0_outbuf__A */
__T1_xdc_runtime_SysMin_Module_State__outbuf xdc_runtime_SysMin_Module_State_0_outbuf__A[524288];

/* Module__state__V */
#ifdef __ti__
xdc_runtime_SysMin_Module_State__ xdc_runtime_SysMin_Module__state__V __attribute__ ((section(".data:xdc_runtime_SysMin_Module__state__V")));
#elif !(defined(__MACH__) && defined(__APPLE__))
xdc_runtime_SysMin_Module_State__ xdc_runtime_SysMin_Module__state__V __attribute__ ((section(".data_xdc_runtime_SysMin_Module__state__V")));
#endif
xdc_runtime_SysMin_Module_State__ xdc_runtime_SysMin_Module__state__V = {
    ((void*)xdc_runtime_SysMin_Module_State_0_outbuf__A),  /* outbuf */
    (xdc_UInt)0x0U,  /* outidx */
    0,  /* wrapped */
};

/* Module__diagsEnabled__C */
#pragma DATA_SECTION(xdc_runtime_SysMin_Module__diagsEnabled__C, ".const:xdc_runtime_SysMin_Module__diagsEnabled__C");
__FAR__ const CT__xdc_runtime_SysMin_Module__diagsEnabled xdc_runtime_SysMin_Module__diagsEnabled__C = (xdc_Bits32)0x10U;

/* Module__diagsIncluded__C */
#pragma DATA_SECTION(xdc_runtime_SysMin_Module__diagsIncluded__C, ".const:xdc_runtime_SysMin_Module__diagsIncluded__C");
__FAR__ const CT__xdc_runtime_SysMin_Module__diagsIncluded xdc_runtime_SysMin_Module__diagsIncluded__C = (xdc_Bits32)0x10U;

/* Module__diagsMask__C */
#pragma DATA_SECTION(xdc_runtime_SysMin_Module__diagsMask__C, ".const:xdc_runtime_SysMin_Module__diagsMask__C");
__FAR__ const CT__xdc_runtime_SysMin_Module__diagsMask xdc_runtime_SysMin_Module__diagsMask__C = ((const CT__xdc_runtime_SysMin_Module__diagsMask)NULL);

/* Module__gateObj__C */
#pragma DATA_SECTION(xdc_runtime_SysMin_Module__gateObj__C, ".const:xdc_runtime_SysMin_Module__gateObj__C");
__FAR__ const CT__xdc_runtime_SysMin_Module__gateObj xdc_runtime_SysMin_Module__gateObj__C = ((const CT__xdc_runtime_SysMin_Module__gateObj)NULL);

/* Module__gatePrms__C */
#pragma DATA_SECTION(xdc_runtime_SysMin_Module__gatePrms__C, ".const:xdc_runtime_SysMin_Module__gatePrms__C");
__FAR__ const CT__xdc_runtime_SysMin_Module__gatePrms xdc_runtime_SysMin_Module__gatePrms__C = ((const CT__xdc_runtime_SysMin_Module__gatePrms)NULL);

/* Module__id__C */
#pragma DATA_SECTION(xdc_runtime_SysMin_Module__id__C, ".const:xdc_runtime_SysMin_Module__id__C");
__FAR__ const CT__xdc_runtime_SysMin_Module__id xdc_runtime_SysMin_Module__id__C = (xdc_Bits16)0x800eU;

/* Module__loggerDefined__C */
#pragma DATA_SECTION(xdc_runtime_SysMin_Module__loggerDefined__C, ".const:xdc_runtime_SysMin_Module__loggerDefined__C");
__FAR__ const CT__xdc_runtime_SysMin_Module__loggerDefined xdc_runtime_SysMin_Module__loggerDefined__C = 0;

/* Module__loggerObj__C */
#pragma DATA_SECTION(xdc_runtime_SysMin_Module__loggerObj__C, ".const:xdc_runtime_SysMin_Module__loggerObj__C");
__FAR__ const CT__xdc_runtime_SysMin_Module__loggerObj xdc_runtime_SysMin_Module__loggerObj__C = ((const CT__xdc_runtime_SysMin_Module__loggerObj)NULL);

/* Module__loggerFxn0__C */
#pragma DATA_SECTION(xdc_runtime_SysMin_Module__loggerFxn0__C, ".const:xdc_runtime_SysMin_Module__loggerFxn0__C");
__FAR__ const CT__xdc_runtime_SysMin_Module__loggerFxn0 xdc_runtime_SysMin_Module__loggerFxn0__C = ((const CT__xdc_runtime_SysMin_Module__loggerFxn0)NULL);

/* Module__loggerFxn1__C */
#pragma DATA_SECTION(xdc_runtime_SysMin_Module__loggerFxn1__C, ".const:xdc_runtime_SysMin_Module__loggerFxn1__C");
__FAR__ const CT__xdc_runtime_SysMin_Module__loggerFxn1 xdc_runtime_SysMin_Module__loggerFxn1__C = ((const CT__xdc_runtime_SysMin_Module__loggerFxn1)NULL);

/* Module__loggerFxn2__C */
#pragma DATA_SECTION(xdc_runtime_SysMin_Module__loggerFxn2__C, ".const:xdc_runtime_SysMin_Module__loggerFxn2__C");
__FAR__ const CT__xdc_runtime_SysMin_Module__loggerFxn2 xdc_runtime_SysMin_Module__loggerFxn2__C = ((const CT__xdc_runtime_SysMin_Module__loggerFxn2)NULL);

/* Module__loggerFxn4__C */
#pragma DATA_SECTION(xdc_runtime_SysMin_Module__loggerFxn4__C, ".const:xdc_runtime_SysMin_Module__loggerFxn4__C");
__FAR__ const CT__xdc_runtime_SysMin_Module__loggerFxn4 xdc_runtime_SysMin_Module__loggerFxn4__C = ((const CT__xdc_runtime_SysMin_Module__loggerFxn4)NULL);

/* Module__loggerFxn8__C */
#pragma DATA_SECTION(xdc_runtime_SysMin_Module__loggerFxn8__C, ".const:xdc_runtime_SysMin_Module__loggerFxn8__C");
__FAR__ const CT__xdc_runtime_SysMin_Module__loggerFxn8 xdc_runtime_SysMin_Module__loggerFxn8__C = ((const CT__xdc_runtime_SysMin_Module__loggerFxn8)NULL);

/* Object__count__C */
#pragma DATA_SECTION(xdc_runtime_SysMin_Object__count__C, ".const:xdc_runtime_SysMin_Object__count__C");
__FAR__ const CT__xdc_runtime_SysMin_Object__count xdc_runtime_SysMin_Object__count__C = 0;

/* Object__heap__C */
#pragma DATA_SECTION(xdc_runtime_SysMin_Object__heap__C, ".const:xdc_runtime_SysMin_Object__heap__C");
__FAR__ const CT__xdc_runtime_SysMin_Object__heap xdc_runtime_SysMin_Object__heap__C = NULL;

/* Object__sizeof__C */
#pragma DATA_SECTION(xdc_runtime_SysMin_Object__sizeof__C, ".const:xdc_runtime_SysMin_Object__sizeof__C");
__FAR__ const CT__xdc_runtime_SysMin_Object__sizeof xdc_runtime_SysMin_Object__sizeof__C = 0;

/* Object__table__C */
#pragma DATA_SECTION(xdc_runtime_SysMin_Object__table__C, ".const:xdc_runtime_SysMin_Object__table__C");
__FAR__ const CT__xdc_runtime_SysMin_Object__table xdc_runtime_SysMin_Object__table__C = NULL;

/* bufSize__C */
#pragma DATA_SECTION(xdc_runtime_SysMin_bufSize__C, ".const:xdc_runtime_SysMin_bufSize__C");
__FAR__ const CT__xdc_runtime_SysMin_bufSize xdc_runtime_SysMin_bufSize__C = (xdc_SizeT)0x80000;

/* flushAtExit__C */
#pragma DATA_SECTION(xdc_runtime_SysMin_flushAtExit__C, ".const:xdc_runtime_SysMin_flushAtExit__C");
__FAR__ const CT__xdc_runtime_SysMin_flushAtExit xdc_runtime_SysMin_flushAtExit__C = 1;

/* outputFxn__C */
#pragma DATA_SECTION(xdc_runtime_SysMin_outputFxn__C, ".const:xdc_runtime_SysMin_outputFxn__C");
__FAR__ const CT__xdc_runtime_SysMin_outputFxn xdc_runtime_SysMin_outputFxn__C = ((const CT__xdc_runtime_SysMin_outputFxn)NULL);

/* outputFunc__C */
#pragma DATA_SECTION(xdc_runtime_SysMin_outputFunc__C, ".const:xdc_runtime_SysMin_outputFunc__C");
__FAR__ const CT__xdc_runtime_SysMin_outputFunc xdc_runtime_SysMin_outputFunc__C = ((const CT__xdc_runtime_SysMin_outputFunc)(xdc_runtime_SysMin_output__I));


/*
 * ======== xdc.runtime.SysStd INITIALIZERS ========
 */

/* Module__diagsEnabled__C */
#pragma DATA_SECTION(xdc_runtime_SysStd_Module__diagsEnabled__C, ".const:xdc_runtime_SysStd_Module__diagsEnabled__C");
__FAR__ const CT__xdc_runtime_SysStd_Module__diagsEnabled xdc_runtime_SysStd_Module__diagsEnabled__C = (xdc_Bits32)0x10U;

/* Module__diagsIncluded__C */
#pragma DATA_SECTION(xdc_runtime_SysStd_Module__diagsIncluded__C, ".const:xdc_runtime_SysStd_Module__diagsIncluded__C");
__FAR__ const CT__xdc_runtime_SysStd_Module__diagsIncluded xdc_runtime_SysStd_Module__diagsIncluded__C = (xdc_Bits32)0x10U;

/* Module__diagsMask__C */
#pragma DATA_SECTION(xdc_runtime_SysStd_Module__diagsMask__C, ".const:xdc_runtime_SysStd_Module__diagsMask__C");
__FAR__ const CT__xdc_runtime_SysStd_Module__diagsMask xdc_runtime_SysStd_Module__diagsMask__C = ((const CT__xdc_runtime_SysStd_Module__diagsMask)NULL);

/* Module__gateObj__C */
#pragma DATA_SECTION(xdc_runtime_SysStd_Module__gateObj__C, ".const:xdc_runtime_SysStd_Module__gateObj__C");
__FAR__ const CT__xdc_runtime_SysStd_Module__gateObj xdc_runtime_SysStd_Module__gateObj__C = ((const CT__xdc_runtime_SysStd_Module__gateObj)NULL);

/* Module__gatePrms__C */
#pragma DATA_SECTION(xdc_runtime_SysStd_Module__gatePrms__C, ".const:xdc_runtime_SysStd_Module__gatePrms__C");
__FAR__ const CT__xdc_runtime_SysStd_Module__gatePrms xdc_runtime_SysStd_Module__gatePrms__C = ((const CT__xdc_runtime_SysStd_Module__gatePrms)NULL);

/* Module__id__C */
#pragma DATA_SECTION(xdc_runtime_SysStd_Module__id__C, ".const:xdc_runtime_SysStd_Module__id__C");
__FAR__ const CT__xdc_runtime_SysStd_Module__id xdc_runtime_SysStd_Module__id__C = (xdc_Bits16)0x800fU;

/* Module__loggerDefined__C */
#pragma DATA_SECTION(xdc_runtime_SysStd_Module__loggerDefined__C, ".const:xdc_runtime_SysStd_Module__loggerDefined__C");
__FAR__ const CT__xdc_runtime_SysStd_Module__loggerDefined xdc_runtime_SysStd_Module__loggerDefined__C = 0;

/* Module__loggerObj__C */
#pragma DATA_SECTION(xdc_runtime_SysStd_Module__loggerObj__C, ".const:xdc_runtime_SysStd_Module__loggerObj__C");
__FAR__ const CT__xdc_runtime_SysStd_Module__loggerObj xdc_runtime_SysStd_Module__loggerObj__C = ((const CT__xdc_runtime_SysStd_Module__loggerObj)NULL);

/* Module__loggerFxn0__C */
#pragma DATA_SECTION(xdc_runtime_SysStd_Module__loggerFxn0__C, ".const:xdc_runtime_SysStd_Module__loggerFxn0__C");
__FAR__ const CT__xdc_runtime_SysStd_Module__loggerFxn0 xdc_runtime_SysStd_Module__loggerFxn0__C = ((const CT__xdc_runtime_SysStd_Module__loggerFxn0)NULL);

/* Module__loggerFxn1__C */
#pragma DATA_SECTION(xdc_runtime_SysStd_Module__loggerFxn1__C, ".const:xdc_runtime_SysStd_Module__loggerFxn1__C");
__FAR__ const CT__xdc_runtime_SysStd_Module__loggerFxn1 xdc_runtime_SysStd_Module__loggerFxn1__C = ((const CT__xdc_runtime_SysStd_Module__loggerFxn1)NULL);

/* Module__loggerFxn2__C */
#pragma DATA_SECTION(xdc_runtime_SysStd_Module__loggerFxn2__C, ".const:xdc_runtime_SysStd_Module__loggerFxn2__C");
__FAR__ const CT__xdc_runtime_SysStd_Module__loggerFxn2 xdc_runtime_SysStd_Module__loggerFxn2__C = ((const CT__xdc_runtime_SysStd_Module__loggerFxn2)NULL);

/* Module__loggerFxn4__C */
#pragma DATA_SECTION(xdc_runtime_SysStd_Module__loggerFxn4__C, ".const:xdc_runtime_SysStd_Module__loggerFxn4__C");
__FAR__ const CT__xdc_runtime_SysStd_Module__loggerFxn4 xdc_runtime_SysStd_Module__loggerFxn4__C = ((const CT__xdc_runtime_SysStd_Module__loggerFxn4)NULL);

/* Module__loggerFxn8__C */
#pragma DATA_SECTION(xdc_runtime_SysStd_Module__loggerFxn8__C, ".const:xdc_runtime_SysStd_Module__loggerFxn8__C");
__FAR__ const CT__xdc_runtime_SysStd_Module__loggerFxn8 xdc_runtime_SysStd_Module__loggerFxn8__C = ((const CT__xdc_runtime_SysStd_Module__loggerFxn8)NULL);

/* Object__count__C */
#pragma DATA_SECTION(xdc_runtime_SysStd_Object__count__C, ".const:xdc_runtime_SysStd_Object__count__C");
__FAR__ const CT__xdc_runtime_SysStd_Object__count xdc_runtime_SysStd_Object__count__C = 0;

/* Object__heap__C */
#pragma DATA_SECTION(xdc_runtime_SysStd_Object__heap__C, ".const:xdc_runtime_SysStd_Object__heap__C");
__FAR__ const CT__xdc_runtime_SysStd_Object__heap xdc_runtime_SysStd_Object__heap__C = NULL;

/* Object__sizeof__C */
#pragma DATA_SECTION(xdc_runtime_SysStd_Object__sizeof__C, ".const:xdc_runtime_SysStd_Object__sizeof__C");
__FAR__ const CT__xdc_runtime_SysStd_Object__sizeof xdc_runtime_SysStd_Object__sizeof__C = 0;

/* Object__table__C */
#pragma DATA_SECTION(xdc_runtime_SysStd_Object__table__C, ".const:xdc_runtime_SysStd_Object__table__C");
__FAR__ const CT__xdc_runtime_SysStd_Object__table xdc_runtime_SysStd_Object__table__C = NULL;


/*
 * ======== xdc.runtime.System INITIALIZERS ========
 */

/* --> xdc_runtime_System_Module_State_0_atexitHandlers__A */
__T1_xdc_runtime_System_Module_State__atexitHandlers xdc_runtime_System_Module_State_0_atexitHandlers__A[8] = {
    ((xdc_Void(*)(xdc_Int f_arg0))NULL),  /* [0] */
    ((xdc_Void(*)(xdc_Int f_arg0))NULL),  /* [1] */
    ((xdc_Void(*)(xdc_Int f_arg0))NULL),  /* [2] */
    ((xdc_Void(*)(xdc_Int f_arg0))NULL),  /* [3] */
    ((xdc_Void(*)(xdc_Int f_arg0))NULL),  /* [4] */
    ((xdc_Void(*)(xdc_Int f_arg0))NULL),  /* [5] */
    ((xdc_Void(*)(xdc_Int f_arg0))NULL),  /* [6] */
    ((xdc_Void(*)(xdc_Int f_arg0))NULL),  /* [7] */
};

/* Module__state__V */
#ifdef __ti__
xdc_runtime_System_Module_State__ xdc_runtime_System_Module__state__V __attribute__ ((section(".data:xdc_runtime_System_Module__state__V")));
#elif !(defined(__MACH__) && defined(__APPLE__))
xdc_runtime_System_Module_State__ xdc_runtime_System_Module__state__V __attribute__ ((section(".data_xdc_runtime_System_Module__state__V")));
#endif
xdc_runtime_System_Module_State__ xdc_runtime_System_Module__state__V = {
    ((void*)xdc_runtime_System_Module_State_0_atexitHandlers__A),  /* atexitHandlers */
    (xdc_Int)0x0,  /* numAtexitHandlers */
};

/* Module__diagsEnabled__C */
#pragma DATA_SECTION(xdc_runtime_System_Module__diagsEnabled__C, ".const:xdc_runtime_System_Module__diagsEnabled__C");
__FAR__ const CT__xdc_runtime_System_Module__diagsEnabled xdc_runtime_System_Module__diagsEnabled__C = (xdc_Bits32)0x10U;

/* Module__diagsIncluded__C */
#pragma DATA_SECTION(xdc_runtime_System_Module__diagsIncluded__C, ".const:xdc_runtime_System_Module__diagsIncluded__C");
__FAR__ const CT__xdc_runtime_System_Module__diagsIncluded xdc_runtime_System_Module__diagsIncluded__C = (xdc_Bits32)0x10U;

/* Module__diagsMask__C */
#pragma DATA_SECTION(xdc_runtime_System_Module__diagsMask__C, ".const:xdc_runtime_System_Module__diagsMask__C");
__FAR__ const CT__xdc_runtime_System_Module__diagsMask xdc_runtime_System_Module__diagsMask__C = ((const CT__xdc_runtime_System_Module__diagsMask)NULL);

/* Module__gateObj__C */
#pragma DATA_SECTION(xdc_runtime_System_Module__gateObj__C, ".const:xdc_runtime_System_Module__gateObj__C");
__FAR__ const CT__xdc_runtime_System_Module__gateObj xdc_runtime_System_Module__gateObj__C = ((const CT__xdc_runtime_System_Module__gateObj)((void*)(xdc_runtime_IGateProvider_Handle)&ti_sysbios_gates_GateHwi_Object__table__V[0]));

/* Module__gatePrms__C */
#pragma DATA_SECTION(xdc_runtime_System_Module__gatePrms__C, ".const:xdc_runtime_System_Module__gatePrms__C");
__FAR__ const CT__xdc_runtime_System_Module__gatePrms xdc_runtime_System_Module__gatePrms__C = ((const CT__xdc_runtime_System_Module__gatePrms)NULL);

/* Module__id__C */
#pragma DATA_SECTION(xdc_runtime_System_Module__id__C, ".const:xdc_runtime_System_Module__id__C");
__FAR__ const CT__xdc_runtime_System_Module__id xdc_runtime_System_Module__id__C = (xdc_Bits16)0x800dU;

/* Module__loggerDefined__C */
#pragma DATA_SECTION(xdc_runtime_System_Module__loggerDefined__C, ".const:xdc_runtime_System_Module__loggerDefined__C");
__FAR__ const CT__xdc_runtime_System_Module__loggerDefined xdc_runtime_System_Module__loggerDefined__C = 0;

/* Module__loggerObj__C */
#pragma DATA_SECTION(xdc_runtime_System_Module__loggerObj__C, ".const:xdc_runtime_System_Module__loggerObj__C");
__FAR__ const CT__xdc_runtime_System_Module__loggerObj xdc_runtime_System_Module__loggerObj__C = ((const CT__xdc_runtime_System_Module__loggerObj)NULL);

/* Module__loggerFxn0__C */
#pragma DATA_SECTION(xdc_runtime_System_Module__loggerFxn0__C, ".const:xdc_runtime_System_Module__loggerFxn0__C");
__FAR__ const CT__xdc_runtime_System_Module__loggerFxn0 xdc_runtime_System_Module__loggerFxn0__C = ((const CT__xdc_runtime_System_Module__loggerFxn0)NULL);

/* Module__loggerFxn1__C */
#pragma DATA_SECTION(xdc_runtime_System_Module__loggerFxn1__C, ".const:xdc_runtime_System_Module__loggerFxn1__C");
__FAR__ const CT__xdc_runtime_System_Module__loggerFxn1 xdc_runtime_System_Module__loggerFxn1__C = ((const CT__xdc_runtime_System_Module__loggerFxn1)NULL);

/* Module__loggerFxn2__C */
#pragma DATA_SECTION(xdc_runtime_System_Module__loggerFxn2__C, ".const:xdc_runtime_System_Module__loggerFxn2__C");
__FAR__ const CT__xdc_runtime_System_Module__loggerFxn2 xdc_runtime_System_Module__loggerFxn2__C = ((const CT__xdc_runtime_System_Module__loggerFxn2)NULL);

/* Module__loggerFxn4__C */
#pragma DATA_SECTION(xdc_runtime_System_Module__loggerFxn4__C, ".const:xdc_runtime_System_Module__loggerFxn4__C");
__FAR__ const CT__xdc_runtime_System_Module__loggerFxn4 xdc_runtime_System_Module__loggerFxn4__C = ((const CT__xdc_runtime_System_Module__loggerFxn4)NULL);

/* Module__loggerFxn8__C */
#pragma DATA_SECTION(xdc_runtime_System_Module__loggerFxn8__C, ".const:xdc_runtime_System_Module__loggerFxn8__C");
__FAR__ const CT__xdc_runtime_System_Module__loggerFxn8 xdc_runtime_System_Module__loggerFxn8__C = ((const CT__xdc_runtime_System_Module__loggerFxn8)NULL);

/* Object__count__C */
#pragma DATA_SECTION(xdc_runtime_System_Object__count__C, ".const:xdc_runtime_System_Object__count__C");
__FAR__ const CT__xdc_runtime_System_Object__count xdc_runtime_System_Object__count__C = 0;

/* Object__heap__C */
#pragma DATA_SECTION(xdc_runtime_System_Object__heap__C, ".const:xdc_runtime_System_Object__heap__C");
__FAR__ const CT__xdc_runtime_System_Object__heap xdc_runtime_System_Object__heap__C = NULL;

/* Object__sizeof__C */
#pragma DATA_SECTION(xdc_runtime_System_Object__sizeof__C, ".const:xdc_runtime_System_Object__sizeof__C");
__FAR__ const CT__xdc_runtime_System_Object__sizeof xdc_runtime_System_Object__sizeof__C = 0;

/* Object__table__C */
#pragma DATA_SECTION(xdc_runtime_System_Object__table__C, ".const:xdc_runtime_System_Object__table__C");
__FAR__ const CT__xdc_runtime_System_Object__table xdc_runtime_System_Object__table__C = NULL;

/* A_cannotFitIntoArg__C */
#pragma DATA_SECTION(xdc_runtime_System_A_cannotFitIntoArg__C, ".const:xdc_runtime_System_A_cannotFitIntoArg__C");
__FAR__ const CT__xdc_runtime_System_A_cannotFitIntoArg xdc_runtime_System_A_cannotFitIntoArg__C = (((xdc_runtime_Assert_Id)352) << 16 | 16);

/* maxAtexitHandlers__C */
#pragma DATA_SECTION(xdc_runtime_System_maxAtexitHandlers__C, ".const:xdc_runtime_System_maxAtexitHandlers__C");
__FAR__ const CT__xdc_runtime_System_maxAtexitHandlers xdc_runtime_System_maxAtexitHandlers__C = (xdc_Int)0x8;

/* abortFxn__C */
#pragma DATA_SECTION(xdc_runtime_System_abortFxn__C, ".const:xdc_runtime_System_abortFxn__C");
__FAR__ const CT__xdc_runtime_System_abortFxn xdc_runtime_System_abortFxn__C = ((const CT__xdc_runtime_System_abortFxn)(xdc_runtime_System_abortStd__E));

/* exitFxn__C */
#pragma DATA_SECTION(xdc_runtime_System_exitFxn__C, ".const:xdc_runtime_System_exitFxn__C");
__FAR__ const CT__xdc_runtime_System_exitFxn xdc_runtime_System_exitFxn__C = ((const CT__xdc_runtime_System_exitFxn)(xdc_runtime_System_exitStd__E));

/* extendFxn__C */
#pragma DATA_SECTION(xdc_runtime_System_extendFxn__C, ".const:xdc_runtime_System_extendFxn__C");
__FAR__ const CT__xdc_runtime_System_extendFxn xdc_runtime_System_extendFxn__C = ((const CT__xdc_runtime_System_extendFxn)(xdc_runtime_System_printfExtend__I));


/*
 * ======== xdc.runtime.System_Module_GateProxy INITIALIZERS ========
 */


/*
 * ======== xdc.runtime.System_SupportProxy INITIALIZERS ========
 */


/*
 * ======== xdc.runtime.Text INITIALIZERS ========
 */

/* Module__state__V */
#ifdef __ti__
xdc_runtime_Text_Module_State__ xdc_runtime_Text_Module__state__V __attribute__ ((section(".data:xdc_runtime_Text_Module__state__V")));
#elif !(defined(__MACH__) && defined(__APPLE__))
xdc_runtime_Text_Module_State__ xdc_runtime_Text_Module__state__V __attribute__ ((section(".data_xdc_runtime_Text_Module__state__V")));
#endif
xdc_runtime_Text_Module_State__ xdc_runtime_Text_Module__state__V = {
    ((xdc_CPtr)(&xdc_runtime_Text_charTab__A[0])),  /* charBase */
    ((xdc_CPtr)(&xdc_runtime_Text_nodeTab__A[0])),  /* nodeBase */
};

/* --> xdc_runtime_Text_charTab__A */
#pragma DATA_SECTION(xdc_runtime_Text_charTab__A, ".const:xdc_runtime_Text_charTab__A");
const __T1_xdc_runtime_Text_charTab xdc_runtime_Text_charTab__A[7434] = {
    (xdc_Char)0x0,  /* [0] */
    (xdc_Char)0x41,  /* [1] */
    (xdc_Char)0x5f,  /* [2] */
    (xdc_Char)0x69,  /* [3] */
    (xdc_Char)0x6e,  /* [4] */
    (xdc_Char)0x69,  /* [5] */
    (xdc_Char)0x74,  /* [6] */
    (xdc_Char)0x69,  /* [7] */
    (xdc_Char)0x61,  /* [8] */
    (xdc_Char)0x6c,  /* [9] */
    (xdc_Char)0x69,  /* [10] */
    (xdc_Char)0x7a,  /* [11] */
    (xdc_Char)0x65,  /* [12] */
    (xdc_Char)0x64,  /* [13] */
    (xdc_Char)0x50,  /* [14] */
    (xdc_Char)0x61,  /* [15] */
    (xdc_Char)0x72,  /* [16] */
    (xdc_Char)0x61,  /* [17] */
    (xdc_Char)0x6d,  /* [18] */
    (xdc_Char)0x73,  /* [19] */
    (xdc_Char)0x3a,  /* [20] */
    (xdc_Char)0x20,  /* [21] */
    (xdc_Char)0x75,  /* [22] */
    (xdc_Char)0x6e,  /* [23] */
    (xdc_Char)0x69,  /* [24] */
    (xdc_Char)0x6e,  /* [25] */
    (xdc_Char)0x69,  /* [26] */
    (xdc_Char)0x74,  /* [27] */
    (xdc_Char)0x69,  /* [28] */
    (xdc_Char)0x61,  /* [29] */
    (xdc_Char)0x6c,  /* [30] */
    (xdc_Char)0x69,  /* [31] */
    (xdc_Char)0x7a,  /* [32] */
    (xdc_Char)0x65,  /* [33] */
    (xdc_Char)0x64,  /* [34] */
    (xdc_Char)0x20,  /* [35] */
    (xdc_Char)0x50,  /* [36] */
    (xdc_Char)0x61,  /* [37] */
    (xdc_Char)0x72,  /* [38] */
    (xdc_Char)0x61,  /* [39] */
    (xdc_Char)0x6d,  /* [40] */
    (xdc_Char)0x73,  /* [41] */
    (xdc_Char)0x20,  /* [42] */
    (xdc_Char)0x73,  /* [43] */
    (xdc_Char)0x74,  /* [44] */
    (xdc_Char)0x72,  /* [45] */
    (xdc_Char)0x75,  /* [46] */
    (xdc_Char)0x63,  /* [47] */
    (xdc_Char)0x74,  /* [48] */
    (xdc_Char)0x0,  /* [49] */
    (xdc_Char)0x48,  /* [50] */
    (xdc_Char)0x65,  /* [51] */
    (xdc_Char)0x61,  /* [52] */
    (xdc_Char)0x70,  /* [53] */
    (xdc_Char)0x4d,  /* [54] */
    (xdc_Char)0x69,  /* [55] */
    (xdc_Char)0x6e,  /* [56] */
    (xdc_Char)0x5f,  /* [57] */
    (xdc_Char)0x63,  /* [58] */
    (xdc_Char)0x72,  /* [59] */
    (xdc_Char)0x65,  /* [60] */
    (xdc_Char)0x61,  /* [61] */
    (xdc_Char)0x74,  /* [62] */
    (xdc_Char)0x65,  /* [63] */
    (xdc_Char)0x20,  /* [64] */
    (xdc_Char)0x63,  /* [65] */
    (xdc_Char)0x61,  /* [66] */
    (xdc_Char)0x6e,  /* [67] */
    (xdc_Char)0x6e,  /* [68] */
    (xdc_Char)0x6f,  /* [69] */
    (xdc_Char)0x74,  /* [70] */
    (xdc_Char)0x20,  /* [71] */
    (xdc_Char)0x68,  /* [72] */
    (xdc_Char)0x61,  /* [73] */
    (xdc_Char)0x76,  /* [74] */
    (xdc_Char)0x65,  /* [75] */
    (xdc_Char)0x20,  /* [76] */
    (xdc_Char)0x61,  /* [77] */
    (xdc_Char)0x20,  /* [78] */
    (xdc_Char)0x7a,  /* [79] */
    (xdc_Char)0x65,  /* [80] */
    (xdc_Char)0x72,  /* [81] */
    (xdc_Char)0x6f,  /* [82] */
    (xdc_Char)0x20,  /* [83] */
    (xdc_Char)0x73,  /* [84] */
    (xdc_Char)0x69,  /* [85] */
    (xdc_Char)0x7a,  /* [86] */
    (xdc_Char)0x65,  /* [87] */
    (xdc_Char)0x20,  /* [88] */
    (xdc_Char)0x76,  /* [89] */
    (xdc_Char)0x61,  /* [90] */
    (xdc_Char)0x6c,  /* [91] */
    (xdc_Char)0x75,  /* [92] */
    (xdc_Char)0x65,  /* [93] */
    (xdc_Char)0x0,  /* [94] */
    (xdc_Char)0x48,  /* [95] */
    (xdc_Char)0x65,  /* [96] */
    (xdc_Char)0x61,  /* [97] */
    (xdc_Char)0x70,  /* [98] */
    (xdc_Char)0x53,  /* [99] */
    (xdc_Char)0x74,  /* [100] */
    (xdc_Char)0x64,  /* [101] */
    (xdc_Char)0x5f,  /* [102] */
    (xdc_Char)0x63,  /* [103] */
    (xdc_Char)0x72,  /* [104] */
    (xdc_Char)0x65,  /* [105] */
    (xdc_Char)0x61,  /* [106] */
    (xdc_Char)0x74,  /* [107] */
    (xdc_Char)0x65,  /* [108] */
    (xdc_Char)0x20,  /* [109] */
    (xdc_Char)0x63,  /* [110] */
    (xdc_Char)0x61,  /* [111] */
    (xdc_Char)0x6e,  /* [112] */
    (xdc_Char)0x6e,  /* [113] */
    (xdc_Char)0x6f,  /* [114] */
    (xdc_Char)0x74,  /* [115] */
    (xdc_Char)0x20,  /* [116] */
    (xdc_Char)0x68,  /* [117] */
    (xdc_Char)0x61,  /* [118] */
    (xdc_Char)0x76,  /* [119] */
    (xdc_Char)0x65,  /* [120] */
    (xdc_Char)0x20,  /* [121] */
    (xdc_Char)0x61,  /* [122] */
    (xdc_Char)0x20,  /* [123] */
    (xdc_Char)0x7a,  /* [124] */
    (xdc_Char)0x65,  /* [125] */
    (xdc_Char)0x72,  /* [126] */
    (xdc_Char)0x6f,  /* [127] */
    (xdc_Char)0x20,  /* [128] */
    (xdc_Char)0x73,  /* [129] */
    (xdc_Char)0x69,  /* [130] */
    (xdc_Char)0x7a,  /* [131] */
    (xdc_Char)0x65,  /* [132] */
    (xdc_Char)0x20,  /* [133] */
    (xdc_Char)0x76,  /* [134] */
    (xdc_Char)0x61,  /* [135] */
    (xdc_Char)0x6c,  /* [136] */
    (xdc_Char)0x75,  /* [137] */
    (xdc_Char)0x65,  /* [138] */
    (xdc_Char)0x0,  /* [139] */
    (xdc_Char)0x48,  /* [140] */
    (xdc_Char)0x65,  /* [141] */
    (xdc_Char)0x61,  /* [142] */
    (xdc_Char)0x70,  /* [143] */
    (xdc_Char)0x53,  /* [144] */
    (xdc_Char)0x74,  /* [145] */
    (xdc_Char)0x64,  /* [146] */
    (xdc_Char)0x5f,  /* [147] */
    (xdc_Char)0x61,  /* [148] */
    (xdc_Char)0x6c,  /* [149] */
    (xdc_Char)0x6c,  /* [150] */
    (xdc_Char)0x6f,  /* [151] */
    (xdc_Char)0x63,  /* [152] */
    (xdc_Char)0x20,  /* [153] */
    (xdc_Char)0x61,  /* [154] */
    (xdc_Char)0x6c,  /* [155] */
    (xdc_Char)0x69,  /* [156] */
    (xdc_Char)0x67,  /* [157] */
    (xdc_Char)0x6e,  /* [158] */
    (xdc_Char)0x6d,  /* [159] */
    (xdc_Char)0x65,  /* [160] */
    (xdc_Char)0x6e,  /* [161] */
    (xdc_Char)0x74,  /* [162] */
    (xdc_Char)0x20,  /* [163] */
    (xdc_Char)0x6d,  /* [164] */
    (xdc_Char)0x75,  /* [165] */
    (xdc_Char)0x73,  /* [166] */
    (xdc_Char)0x74,  /* [167] */
    (xdc_Char)0x20,  /* [168] */
    (xdc_Char)0x62,  /* [169] */
    (xdc_Char)0x65,  /* [170] */
    (xdc_Char)0x20,  /* [171] */
    (xdc_Char)0x61,  /* [172] */
    (xdc_Char)0x20,  /* [173] */
    (xdc_Char)0x70,  /* [174] */
    (xdc_Char)0x6f,  /* [175] */
    (xdc_Char)0x77,  /* [176] */
    (xdc_Char)0x65,  /* [177] */
    (xdc_Char)0x72,  /* [178] */
    (xdc_Char)0x20,  /* [179] */
    (xdc_Char)0x6f,  /* [180] */
    (xdc_Char)0x66,  /* [181] */
    (xdc_Char)0x20,  /* [182] */
    (xdc_Char)0x32,  /* [183] */
    (xdc_Char)0x0,  /* [184] */
    (xdc_Char)0x48,  /* [185] */
    (xdc_Char)0x65,  /* [186] */
    (xdc_Char)0x61,  /* [187] */
    (xdc_Char)0x70,  /* [188] */
    (xdc_Char)0x53,  /* [189] */
    (xdc_Char)0x74,  /* [190] */
    (xdc_Char)0x64,  /* [191] */
    (xdc_Char)0x20,  /* [192] */
    (xdc_Char)0x69,  /* [193] */
    (xdc_Char)0x6e,  /* [194] */
    (xdc_Char)0x73,  /* [195] */
    (xdc_Char)0x74,  /* [196] */
    (xdc_Char)0x61,  /* [197] */
    (xdc_Char)0x6e,  /* [198] */
    (xdc_Char)0x63,  /* [199] */
    (xdc_Char)0x65,  /* [200] */
    (xdc_Char)0x20,  /* [201] */
    (xdc_Char)0x74,  /* [202] */
    (xdc_Char)0x6f,  /* [203] */
    (xdc_Char)0x74,  /* [204] */
    (xdc_Char)0x61,  /* [205] */
    (xdc_Char)0x6c,  /* [206] */
    (xdc_Char)0x46,  /* [207] */
    (xdc_Char)0x72,  /* [208] */
    (xdc_Char)0x65,  /* [209] */
    (xdc_Char)0x65,  /* [210] */
    (xdc_Char)0x53,  /* [211] */
    (xdc_Char)0x69,  /* [212] */
    (xdc_Char)0x7a,  /* [213] */
    (xdc_Char)0x65,  /* [214] */
    (xdc_Char)0x20,  /* [215] */
    (xdc_Char)0x69,  /* [216] */
    (xdc_Char)0x73,  /* [217] */
    (xdc_Char)0x20,  /* [218] */
    (xdc_Char)0x67,  /* [219] */
    (xdc_Char)0x72,  /* [220] */
    (xdc_Char)0x65,  /* [221] */
    (xdc_Char)0x61,  /* [222] */
    (xdc_Char)0x74,  /* [223] */
    (xdc_Char)0x65,  /* [224] */
    (xdc_Char)0x72,  /* [225] */
    (xdc_Char)0x20,  /* [226] */
    (xdc_Char)0x74,  /* [227] */
    (xdc_Char)0x68,  /* [228] */
    (xdc_Char)0x61,  /* [229] */
    (xdc_Char)0x6e,  /* [230] */
    (xdc_Char)0x20,  /* [231] */
    (xdc_Char)0x73,  /* [232] */
    (xdc_Char)0x74,  /* [233] */
    (xdc_Char)0x61,  /* [234] */
    (xdc_Char)0x72,  /* [235] */
    (xdc_Char)0x74,  /* [236] */
    (xdc_Char)0x69,  /* [237] */
    (xdc_Char)0x6e,  /* [238] */
    (xdc_Char)0x67,  /* [239] */
    (xdc_Char)0x20,  /* [240] */
    (xdc_Char)0x73,  /* [241] */
    (xdc_Char)0x69,  /* [242] */
    (xdc_Char)0x7a,  /* [243] */
    (xdc_Char)0x65,  /* [244] */
    (xdc_Char)0x0,  /* [245] */
    (xdc_Char)0x48,  /* [246] */
    (xdc_Char)0x65,  /* [247] */
    (xdc_Char)0x61,  /* [248] */
    (xdc_Char)0x70,  /* [249] */
    (xdc_Char)0x53,  /* [250] */
    (xdc_Char)0x74,  /* [251] */
    (xdc_Char)0x64,  /* [252] */
    (xdc_Char)0x5f,  /* [253] */
    (xdc_Char)0x61,  /* [254] */
    (xdc_Char)0x6c,  /* [255] */
    (xdc_Char)0x6c,  /* [256] */
    (xdc_Char)0x6f,  /* [257] */
    (xdc_Char)0x63,  /* [258] */
    (xdc_Char)0x20,  /* [259] */
    (xdc_Char)0x2d,  /* [260] */
    (xdc_Char)0x20,  /* [261] */
    (xdc_Char)0x72,  /* [262] */
    (xdc_Char)0x65,  /* [263] */
    (xdc_Char)0x71,  /* [264] */
    (xdc_Char)0x75,  /* [265] */
    (xdc_Char)0x65,  /* [266] */
    (xdc_Char)0x73,  /* [267] */
    (xdc_Char)0x74,  /* [268] */
    (xdc_Char)0x65,  /* [269] */
    (xdc_Char)0x64,  /* [270] */
    (xdc_Char)0x20,  /* [271] */
    (xdc_Char)0x61,  /* [272] */
    (xdc_Char)0x6c,  /* [273] */
    (xdc_Char)0x69,  /* [274] */
    (xdc_Char)0x67,  /* [275] */
    (xdc_Char)0x6e,  /* [276] */
    (xdc_Char)0x6d,  /* [277] */
    (xdc_Char)0x65,  /* [278] */
    (xdc_Char)0x6e,  /* [279] */
    (xdc_Char)0x74,  /* [280] */
    (xdc_Char)0x20,  /* [281] */
    (xdc_Char)0x69,  /* [282] */
    (xdc_Char)0x73,  /* [283] */
    (xdc_Char)0x20,  /* [284] */
    (xdc_Char)0x67,  /* [285] */
    (xdc_Char)0x72,  /* [286] */
    (xdc_Char)0x65,  /* [287] */
    (xdc_Char)0x61,  /* [288] */
    (xdc_Char)0x74,  /* [289] */
    (xdc_Char)0x65,  /* [290] */
    (xdc_Char)0x72,  /* [291] */
    (xdc_Char)0x20,  /* [292] */
    (xdc_Char)0x74,  /* [293] */
    (xdc_Char)0x68,  /* [294] */
    (xdc_Char)0x61,  /* [295] */
    (xdc_Char)0x6e,  /* [296] */
    (xdc_Char)0x20,  /* [297] */
    (xdc_Char)0x61,  /* [298] */
    (xdc_Char)0x6c,  /* [299] */
    (xdc_Char)0x6c,  /* [300] */
    (xdc_Char)0x6f,  /* [301] */
    (xdc_Char)0x77,  /* [302] */
    (xdc_Char)0x65,  /* [303] */
    (xdc_Char)0x64,  /* [304] */
    (xdc_Char)0x0,  /* [305] */
    (xdc_Char)0x41,  /* [306] */
    (xdc_Char)0x5f,  /* [307] */
    (xdc_Char)0x69,  /* [308] */
    (xdc_Char)0x6e,  /* [309] */
    (xdc_Char)0x76,  /* [310] */
    (xdc_Char)0x61,  /* [311] */
    (xdc_Char)0x6c,  /* [312] */
    (xdc_Char)0x69,  /* [313] */
    (xdc_Char)0x64,  /* [314] */
    (xdc_Char)0x4c,  /* [315] */
    (xdc_Char)0x6f,  /* [316] */
    (xdc_Char)0x67,  /* [317] */
    (xdc_Char)0x67,  /* [318] */
    (xdc_Char)0x65,  /* [319] */
    (xdc_Char)0x72,  /* [320] */
    (xdc_Char)0x3a,  /* [321] */
    (xdc_Char)0x20,  /* [322] */
    (xdc_Char)0x54,  /* [323] */
    (xdc_Char)0x68,  /* [324] */
    (xdc_Char)0x65,  /* [325] */
    (xdc_Char)0x20,  /* [326] */
    (xdc_Char)0x6c,  /* [327] */
    (xdc_Char)0x6f,  /* [328] */
    (xdc_Char)0x67,  /* [329] */
    (xdc_Char)0x67,  /* [330] */
    (xdc_Char)0x65,  /* [331] */
    (xdc_Char)0x72,  /* [332] */
    (xdc_Char)0x20,  /* [333] */
    (xdc_Char)0x69,  /* [334] */
    (xdc_Char)0x64,  /* [335] */
    (xdc_Char)0x20,  /* [336] */
    (xdc_Char)0x25,  /* [337] */
    (xdc_Char)0x64,  /* [338] */
    (xdc_Char)0x20,  /* [339] */
    (xdc_Char)0x69,  /* [340] */
    (xdc_Char)0x73,  /* [341] */
    (xdc_Char)0x20,  /* [342] */
    (xdc_Char)0x69,  /* [343] */
    (xdc_Char)0x6e,  /* [344] */
    (xdc_Char)0x76,  /* [345] */
    (xdc_Char)0x61,  /* [346] */
    (xdc_Char)0x6c,  /* [347] */
    (xdc_Char)0x69,  /* [348] */
    (xdc_Char)0x64,  /* [349] */
    (xdc_Char)0x2e,  /* [350] */
    (xdc_Char)0x0,  /* [351] */
    (xdc_Char)0x41,  /* [352] */
    (xdc_Char)0x5f,  /* [353] */
    (xdc_Char)0x63,  /* [354] */
    (xdc_Char)0x61,  /* [355] */
    (xdc_Char)0x6e,  /* [356] */
    (xdc_Char)0x6e,  /* [357] */
    (xdc_Char)0x6f,  /* [358] */
    (xdc_Char)0x74,  /* [359] */
    (xdc_Char)0x46,  /* [360] */
    (xdc_Char)0x69,  /* [361] */
    (xdc_Char)0x74,  /* [362] */
    (xdc_Char)0x49,  /* [363] */
    (xdc_Char)0x6e,  /* [364] */
    (xdc_Char)0x74,  /* [365] */
    (xdc_Char)0x6f,  /* [366] */
    (xdc_Char)0x41,  /* [367] */
    (xdc_Char)0x72,  /* [368] */
    (xdc_Char)0x67,  /* [369] */
    (xdc_Char)0x3a,  /* [370] */
    (xdc_Char)0x20,  /* [371] */
    (xdc_Char)0x73,  /* [372] */
    (xdc_Char)0x69,  /* [373] */
    (xdc_Char)0x7a,  /* [374] */
    (xdc_Char)0x65,  /* [375] */
    (xdc_Char)0x6f,  /* [376] */
    (xdc_Char)0x66,  /* [377] */
    (xdc_Char)0x28,  /* [378] */
    (xdc_Char)0x46,  /* [379] */
    (xdc_Char)0x6c,  /* [380] */
    (xdc_Char)0x6f,  /* [381] */
    (xdc_Char)0x61,  /* [382] */
    (xdc_Char)0x74,  /* [383] */
    (xdc_Char)0x29,  /* [384] */
    (xdc_Char)0x20,  /* [385] */
    (xdc_Char)0x3e,  /* [386] */
    (xdc_Char)0x20,  /* [387] */
    (xdc_Char)0x73,  /* [388] */
    (xdc_Char)0x69,  /* [389] */
    (xdc_Char)0x7a,  /* [390] */
    (xdc_Char)0x65,  /* [391] */
    (xdc_Char)0x6f,  /* [392] */
    (xdc_Char)0x66,  /* [393] */
    (xdc_Char)0x28,  /* [394] */
    (xdc_Char)0x41,  /* [395] */
    (xdc_Char)0x72,  /* [396] */
    (xdc_Char)0x67,  /* [397] */
    (xdc_Char)0x29,  /* [398] */
    (xdc_Char)0x0,  /* [399] */
    (xdc_Char)0x41,  /* [400] */
    (xdc_Char)0x5f,  /* [401] */
    (xdc_Char)0x69,  /* [402] */
    (xdc_Char)0x6e,  /* [403] */
    (xdc_Char)0x76,  /* [404] */
    (xdc_Char)0x61,  /* [405] */
    (xdc_Char)0x6c,  /* [406] */
    (xdc_Char)0x69,  /* [407] */
    (xdc_Char)0x64,  /* [408] */
    (xdc_Char)0x50,  /* [409] */
    (xdc_Char)0x72,  /* [410] */
    (xdc_Char)0x69,  /* [411] */
    (xdc_Char)0x6f,  /* [412] */
    (xdc_Char)0x72,  /* [413] */
    (xdc_Char)0x69,  /* [414] */
    (xdc_Char)0x74,  /* [415] */
    (xdc_Char)0x79,  /* [416] */
    (xdc_Char)0x3a,  /* [417] */
    (xdc_Char)0x20,  /* [418] */
    (xdc_Char)0x70,  /* [419] */
    (xdc_Char)0x72,  /* [420] */
    (xdc_Char)0x6f,  /* [421] */
    (xdc_Char)0x69,  /* [422] */
    (xdc_Char)0x6f,  /* [423] */
    (xdc_Char)0x72,  /* [424] */
    (xdc_Char)0x69,  /* [425] */
    (xdc_Char)0x74,  /* [426] */
    (xdc_Char)0x79,  /* [427] */
    (xdc_Char)0x20,  /* [428] */
    (xdc_Char)0x6f,  /* [429] */
    (xdc_Char)0x75,  /* [430] */
    (xdc_Char)0x74,  /* [431] */
    (xdc_Char)0x20,  /* [432] */
    (xdc_Char)0x6f,  /* [433] */
    (xdc_Char)0x66,  /* [434] */
    (xdc_Char)0x20,  /* [435] */
    (xdc_Char)0x72,  /* [436] */
    (xdc_Char)0x61,  /* [437] */
    (xdc_Char)0x6e,  /* [438] */
    (xdc_Char)0x67,  /* [439] */
    (xdc_Char)0x65,  /* [440] */
    (xdc_Char)0x20,  /* [441] */
    (xdc_Char)0x28,  /* [442] */
    (xdc_Char)0x31,  /* [443] */
    (xdc_Char)0x2d,  /* [444] */
    (xdc_Char)0x37,  /* [445] */
    (xdc_Char)0x29,  /* [446] */
    (xdc_Char)0x0,  /* [447] */
    (xdc_Char)0x41,  /* [448] */
    (xdc_Char)0x5f,  /* [449] */
    (xdc_Char)0x6e,  /* [450] */
    (xdc_Char)0x75,  /* [451] */
    (xdc_Char)0x6c,  /* [452] */
    (xdc_Char)0x6c,  /* [453] */
    (xdc_Char)0x50,  /* [454] */
    (xdc_Char)0x6f,  /* [455] */
    (xdc_Char)0x69,  /* [456] */
    (xdc_Char)0x6e,  /* [457] */
    (xdc_Char)0x74,  /* [458] */
    (xdc_Char)0x65,  /* [459] */
    (xdc_Char)0x72,  /* [460] */
    (xdc_Char)0x3a,  /* [461] */
    (xdc_Char)0x20,  /* [462] */
    (xdc_Char)0x50,  /* [463] */
    (xdc_Char)0x6f,  /* [464] */
    (xdc_Char)0x69,  /* [465] */
    (xdc_Char)0x6e,  /* [466] */
    (xdc_Char)0x74,  /* [467] */
    (xdc_Char)0x65,  /* [468] */
    (xdc_Char)0x72,  /* [469] */
    (xdc_Char)0x20,  /* [470] */
    (xdc_Char)0x69,  /* [471] */
    (xdc_Char)0x73,  /* [472] */
    (xdc_Char)0x20,  /* [473] */
    (xdc_Char)0x6e,  /* [474] */
    (xdc_Char)0x75,  /* [475] */
    (xdc_Char)0x6c,  /* [476] */
    (xdc_Char)0x6c,  /* [477] */
    (xdc_Char)0x0,  /* [478] */
    (xdc_Char)0x41,  /* [479] */
    (xdc_Char)0x5f,  /* [480] */
    (xdc_Char)0x76,  /* [481] */
    (xdc_Char)0x61,  /* [482] */
    (xdc_Char)0x64,  /* [483] */
    (xdc_Char)0x64,  /* [484] */
    (xdc_Char)0x72,  /* [485] */
    (xdc_Char)0x4f,  /* [486] */
    (xdc_Char)0x75,  /* [487] */
    (xdc_Char)0x74,  /* [488] */
    (xdc_Char)0x4f,  /* [489] */
    (xdc_Char)0x66,  /* [490] */
    (xdc_Char)0x52,  /* [491] */
    (xdc_Char)0x61,  /* [492] */
    (xdc_Char)0x6e,  /* [493] */
    (xdc_Char)0x67,  /* [494] */
    (xdc_Char)0x65,  /* [495] */
    (xdc_Char)0x3a,  /* [496] */
    (xdc_Char)0x20,  /* [497] */
    (xdc_Char)0x56,  /* [498] */
    (xdc_Char)0x69,  /* [499] */
    (xdc_Char)0x72,  /* [500] */
    (xdc_Char)0x74,  /* [501] */
    (xdc_Char)0x75,  /* [502] */
    (xdc_Char)0x61,  /* [503] */
    (xdc_Char)0x6c,  /* [504] */
    (xdc_Char)0x20,  /* [505] */
    (xdc_Char)0x61,  /* [506] */
    (xdc_Char)0x64,  /* [507] */
    (xdc_Char)0x64,  /* [508] */
    (xdc_Char)0x72,  /* [509] */
    (xdc_Char)0x65,  /* [510] */
    (xdc_Char)0x73,  /* [511] */
    (xdc_Char)0x73,  /* [512] */
    (xdc_Char)0x20,  /* [513] */
    (xdc_Char)0x69,  /* [514] */
    (xdc_Char)0x73,  /* [515] */
    (xdc_Char)0x20,  /* [516] */
    (xdc_Char)0x6f,  /* [517] */
    (xdc_Char)0x75,  /* [518] */
    (xdc_Char)0x74,  /* [519] */
    (xdc_Char)0x20,  /* [520] */
    (xdc_Char)0x6f,  /* [521] */
    (xdc_Char)0x66,  /* [522] */
    (xdc_Char)0x20,  /* [523] */
    (xdc_Char)0x72,  /* [524] */
    (xdc_Char)0x61,  /* [525] */
    (xdc_Char)0x6e,  /* [526] */
    (xdc_Char)0x67,  /* [527] */
    (xdc_Char)0x65,  /* [528] */
    (xdc_Char)0x0,  /* [529] */
    (xdc_Char)0x41,  /* [530] */
    (xdc_Char)0x5f,  /* [531] */
    (xdc_Char)0x70,  /* [532] */
    (xdc_Char)0x61,  /* [533] */
    (xdc_Char)0x64,  /* [534] */
    (xdc_Char)0x64,  /* [535] */
    (xdc_Char)0x72,  /* [536] */
    (xdc_Char)0x4f,  /* [537] */
    (xdc_Char)0x75,  /* [538] */
    (xdc_Char)0x74,  /* [539] */
    (xdc_Char)0x4f,  /* [540] */
    (xdc_Char)0x66,  /* [541] */
    (xdc_Char)0x52,  /* [542] */
    (xdc_Char)0x61,  /* [543] */
    (xdc_Char)0x6e,  /* [544] */
    (xdc_Char)0x67,  /* [545] */
    (xdc_Char)0x65,  /* [546] */
    (xdc_Char)0x3a,  /* [547] */
    (xdc_Char)0x20,  /* [548] */
    (xdc_Char)0x50,  /* [549] */
    (xdc_Char)0x68,  /* [550] */
    (xdc_Char)0x79,  /* [551] */
    (xdc_Char)0x73,  /* [552] */
    (xdc_Char)0x69,  /* [553] */
    (xdc_Char)0x63,  /* [554] */
    (xdc_Char)0x61,  /* [555] */
    (xdc_Char)0x6c,  /* [556] */
    (xdc_Char)0x20,  /* [557] */
    (xdc_Char)0x61,  /* [558] */
    (xdc_Char)0x64,  /* [559] */
    (xdc_Char)0x64,  /* [560] */
    (xdc_Char)0x72,  /* [561] */
    (xdc_Char)0x65,  /* [562] */
    (xdc_Char)0x73,  /* [563] */
    (xdc_Char)0x73,  /* [564] */
    (xdc_Char)0x20,  /* [565] */
    (xdc_Char)0x69,  /* [566] */
    (xdc_Char)0x73,  /* [567] */
    (xdc_Char)0x20,  /* [568] */
    (xdc_Char)0x6f,  /* [569] */
    (xdc_Char)0x75,  /* [570] */
    (xdc_Char)0x74,  /* [571] */
    (xdc_Char)0x20,  /* [572] */
    (xdc_Char)0x6f,  /* [573] */
    (xdc_Char)0x66,  /* [574] */
    (xdc_Char)0x20,  /* [575] */
    (xdc_Char)0x72,  /* [576] */
    (xdc_Char)0x61,  /* [577] */
    (xdc_Char)0x6e,  /* [578] */
    (xdc_Char)0x67,  /* [579] */
    (xdc_Char)0x65,  /* [580] */
    (xdc_Char)0x0,  /* [581] */
    (xdc_Char)0x41,  /* [582] */
    (xdc_Char)0x5f,  /* [583] */
    (xdc_Char)0x75,  /* [584] */
    (xdc_Char)0x6e,  /* [585] */
    (xdc_Char)0x61,  /* [586] */
    (xdc_Char)0x6c,  /* [587] */
    (xdc_Char)0x69,  /* [588] */
    (xdc_Char)0x67,  /* [589] */
    (xdc_Char)0x6e,  /* [590] */
    (xdc_Char)0x65,  /* [591] */
    (xdc_Char)0x64,  /* [592] */
    (xdc_Char)0x56,  /* [593] */
    (xdc_Char)0x61,  /* [594] */
    (xdc_Char)0x64,  /* [595] */
    (xdc_Char)0x64,  /* [596] */
    (xdc_Char)0x72,  /* [597] */
    (xdc_Char)0x3a,  /* [598] */
    (xdc_Char)0x20,  /* [599] */
    (xdc_Char)0x56,  /* [600] */
    (xdc_Char)0x69,  /* [601] */
    (xdc_Char)0x72,  /* [602] */
    (xdc_Char)0x74,  /* [603] */
    (xdc_Char)0x75,  /* [604] */
    (xdc_Char)0x61,  /* [605] */
    (xdc_Char)0x6c,  /* [606] */
    (xdc_Char)0x20,  /* [607] */
    (xdc_Char)0x61,  /* [608] */
    (xdc_Char)0x64,  /* [609] */
    (xdc_Char)0x64,  /* [610] */
    (xdc_Char)0x72,  /* [611] */
    (xdc_Char)0x65,  /* [612] */
    (xdc_Char)0x73,  /* [613] */
    (xdc_Char)0x73,  /* [614] */
    (xdc_Char)0x20,  /* [615] */
    (xdc_Char)0x6e,  /* [616] */
    (xdc_Char)0x6f,  /* [617] */
    (xdc_Char)0x74,  /* [618] */
    (xdc_Char)0x20,  /* [619] */
    (xdc_Char)0x70,  /* [620] */
    (xdc_Char)0x61,  /* [621] */
    (xdc_Char)0x67,  /* [622] */
    (xdc_Char)0x65,  /* [623] */
    (xdc_Char)0x20,  /* [624] */
    (xdc_Char)0x61,  /* [625] */
    (xdc_Char)0x6c,  /* [626] */
    (xdc_Char)0x69,  /* [627] */
    (xdc_Char)0x67,  /* [628] */
    (xdc_Char)0x6e,  /* [629] */
    (xdc_Char)0x65,  /* [630] */
    (xdc_Char)0x64,  /* [631] */
    (xdc_Char)0x0,  /* [632] */
    (xdc_Char)0x41,  /* [633] */
    (xdc_Char)0x5f,  /* [634] */
    (xdc_Char)0x75,  /* [635] */
    (xdc_Char)0x6e,  /* [636] */
    (xdc_Char)0x61,  /* [637] */
    (xdc_Char)0x6c,  /* [638] */
    (xdc_Char)0x69,  /* [639] */
    (xdc_Char)0x67,  /* [640] */
    (xdc_Char)0x6e,  /* [641] */
    (xdc_Char)0x65,  /* [642] */
    (xdc_Char)0x64,  /* [643] */
    (xdc_Char)0x50,  /* [644] */
    (xdc_Char)0x61,  /* [645] */
    (xdc_Char)0x64,  /* [646] */
    (xdc_Char)0x64,  /* [647] */
    (xdc_Char)0x72,  /* [648] */
    (xdc_Char)0x3a,  /* [649] */
    (xdc_Char)0x20,  /* [650] */
    (xdc_Char)0x50,  /* [651] */
    (xdc_Char)0x68,  /* [652] */
    (xdc_Char)0x79,  /* [653] */
    (xdc_Char)0x73,  /* [654] */
    (xdc_Char)0x69,  /* [655] */
    (xdc_Char)0x63,  /* [656] */
    (xdc_Char)0x61,  /* [657] */
    (xdc_Char)0x6c,  /* [658] */
    (xdc_Char)0x20,  /* [659] */
    (xdc_Char)0x61,  /* [660] */
    (xdc_Char)0x64,  /* [661] */
    (xdc_Char)0x64,  /* [662] */
    (xdc_Char)0x72,  /* [663] */
    (xdc_Char)0x65,  /* [664] */
    (xdc_Char)0x73,  /* [665] */
    (xdc_Char)0x73,  /* [666] */
    (xdc_Char)0x20,  /* [667] */
    (xdc_Char)0x6e,  /* [668] */
    (xdc_Char)0x6f,  /* [669] */
    (xdc_Char)0x74,  /* [670] */
    (xdc_Char)0x20,  /* [671] */
    (xdc_Char)0x70,  /* [672] */
    (xdc_Char)0x61,  /* [673] */
    (xdc_Char)0x67,  /* [674] */
    (xdc_Char)0x65,  /* [675] */
    (xdc_Char)0x20,  /* [676] */
    (xdc_Char)0x61,  /* [677] */
    (xdc_Char)0x6c,  /* [678] */
    (xdc_Char)0x69,  /* [679] */
    (xdc_Char)0x67,  /* [680] */
    (xdc_Char)0x6e,  /* [681] */
    (xdc_Char)0x65,  /* [682] */
    (xdc_Char)0x64,  /* [683] */
    (xdc_Char)0x0,  /* [684] */
    (xdc_Char)0x41,  /* [685] */
    (xdc_Char)0x5f,  /* [686] */
    (xdc_Char)0x75,  /* [687] */
    (xdc_Char)0x6e,  /* [688] */
    (xdc_Char)0x61,  /* [689] */
    (xdc_Char)0x6c,  /* [690] */
    (xdc_Char)0x69,  /* [691] */
    (xdc_Char)0x67,  /* [692] */
    (xdc_Char)0x6e,  /* [693] */
    (xdc_Char)0x65,  /* [694] */
    (xdc_Char)0x64,  /* [695] */
    (xdc_Char)0x53,  /* [696] */
    (xdc_Char)0x69,  /* [697] */
    (xdc_Char)0x7a,  /* [698] */
    (xdc_Char)0x65,  /* [699] */
    (xdc_Char)0x3a,  /* [700] */
    (xdc_Char)0x20,  /* [701] */
    (xdc_Char)0x4d,  /* [702] */
    (xdc_Char)0x6d,  /* [703] */
    (xdc_Char)0x75,  /* [704] */
    (xdc_Char)0x20,  /* [705] */
    (xdc_Char)0x6d,  /* [706] */
    (xdc_Char)0x61,  /* [707] */
    (xdc_Char)0x70,  /* [708] */
    (xdc_Char)0x70,  /* [709] */
    (xdc_Char)0x69,  /* [710] */
    (xdc_Char)0x6e,  /* [711] */
    (xdc_Char)0x67,  /* [712] */
    (xdc_Char)0x20,  /* [713] */
    (xdc_Char)0x73,  /* [714] */
    (xdc_Char)0x69,  /* [715] */
    (xdc_Char)0x7a,  /* [716] */
    (xdc_Char)0x65,  /* [717] */
    (xdc_Char)0x20,  /* [718] */
    (xdc_Char)0x6e,  /* [719] */
    (xdc_Char)0x6f,  /* [720] */
    (xdc_Char)0x74,  /* [721] */
    (xdc_Char)0x20,  /* [722] */
    (xdc_Char)0x70,  /* [723] */
    (xdc_Char)0x61,  /* [724] */
    (xdc_Char)0x67,  /* [725] */
    (xdc_Char)0x65,  /* [726] */
    (xdc_Char)0x20,  /* [727] */
    (xdc_Char)0x61,  /* [728] */
    (xdc_Char)0x6c,  /* [729] */
    (xdc_Char)0x69,  /* [730] */
    (xdc_Char)0x67,  /* [731] */
    (xdc_Char)0x6e,  /* [732] */
    (xdc_Char)0x65,  /* [733] */
    (xdc_Char)0x64,  /* [734] */
    (xdc_Char)0x0,  /* [735] */
    (xdc_Char)0x41,  /* [736] */
    (xdc_Char)0x5f,  /* [737] */
    (xdc_Char)0x73,  /* [738] */
    (xdc_Char)0x74,  /* [739] */
    (xdc_Char)0x61,  /* [740] */
    (xdc_Char)0x63,  /* [741] */
    (xdc_Char)0x6b,  /* [742] */
    (xdc_Char)0x53,  /* [743] */
    (xdc_Char)0x69,  /* [744] */
    (xdc_Char)0x7a,  /* [745] */
    (xdc_Char)0x65,  /* [746] */
    (xdc_Char)0x54,  /* [747] */
    (xdc_Char)0x6f,  /* [748] */
    (xdc_Char)0x6f,  /* [749] */
    (xdc_Char)0x53,  /* [750] */
    (xdc_Char)0x6d,  /* [751] */
    (xdc_Char)0x61,  /* [752] */
    (xdc_Char)0x6c,  /* [753] */
    (xdc_Char)0x6c,  /* [754] */
    (xdc_Char)0x3a,  /* [755] */
    (xdc_Char)0x20,  /* [756] */
    (xdc_Char)0x54,  /* [757] */
    (xdc_Char)0x61,  /* [758] */
    (xdc_Char)0x73,  /* [759] */
    (xdc_Char)0x6b,  /* [760] */
    (xdc_Char)0x20,  /* [761] */
    (xdc_Char)0x73,  /* [762] */
    (xdc_Char)0x74,  /* [763] */
    (xdc_Char)0x61,  /* [764] */
    (xdc_Char)0x63,  /* [765] */
    (xdc_Char)0x6b,  /* [766] */
    (xdc_Char)0x20,  /* [767] */
    (xdc_Char)0x73,  /* [768] */
    (xdc_Char)0x69,  /* [769] */
    (xdc_Char)0x7a,  /* [770] */
    (xdc_Char)0x65,  /* [771] */
    (xdc_Char)0x20,  /* [772] */
    (xdc_Char)0x6d,  /* [773] */
    (xdc_Char)0x75,  /* [774] */
    (xdc_Char)0x73,  /* [775] */
    (xdc_Char)0x74,  /* [776] */
    (xdc_Char)0x20,  /* [777] */
    (xdc_Char)0x62,  /* [778] */
    (xdc_Char)0x65,  /* [779] */
    (xdc_Char)0x20,  /* [780] */
    (xdc_Char)0x3e,  /* [781] */
    (xdc_Char)0x3d,  /* [782] */
    (xdc_Char)0x20,  /* [783] */
    (xdc_Char)0x31,  /* [784] */
    (xdc_Char)0x36,  /* [785] */
    (xdc_Char)0x4b,  /* [786] */
    (xdc_Char)0x42,  /* [787] */
    (xdc_Char)0x2e,  /* [788] */
    (xdc_Char)0x0,  /* [789] */
    (xdc_Char)0x41,  /* [790] */
    (xdc_Char)0x5f,  /* [791] */
    (xdc_Char)0x63,  /* [792] */
    (xdc_Char)0x6c,  /* [793] */
    (xdc_Char)0x6f,  /* [794] */
    (xdc_Char)0x63,  /* [795] */
    (xdc_Char)0x6b,  /* [796] */
    (xdc_Char)0x44,  /* [797] */
    (xdc_Char)0x69,  /* [798] */
    (xdc_Char)0x73,  /* [799] */
    (xdc_Char)0x61,  /* [800] */
    (xdc_Char)0x62,  /* [801] */
    (xdc_Char)0x6c,  /* [802] */
    (xdc_Char)0x65,  /* [803] */
    (xdc_Char)0x64,  /* [804] */
    (xdc_Char)0x3a,  /* [805] */
    (xdc_Char)0x20,  /* [806] */
    (xdc_Char)0x43,  /* [807] */
    (xdc_Char)0x61,  /* [808] */
    (xdc_Char)0x6e,  /* [809] */
    (xdc_Char)0x6e,  /* [810] */
    (xdc_Char)0x6f,  /* [811] */
    (xdc_Char)0x74,  /* [812] */
    (xdc_Char)0x20,  /* [813] */
    (xdc_Char)0x63,  /* [814] */
    (xdc_Char)0x72,  /* [815] */
    (xdc_Char)0x65,  /* [816] */
    (xdc_Char)0x61,  /* [817] */
    (xdc_Char)0x74,  /* [818] */
    (xdc_Char)0x65,  /* [819] */
    (xdc_Char)0x20,  /* [820] */
    (xdc_Char)0x61,  /* [821] */
    (xdc_Char)0x20,  /* [822] */
    (xdc_Char)0x63,  /* [823] */
    (xdc_Char)0x6c,  /* [824] */
    (xdc_Char)0x6f,  /* [825] */
    (xdc_Char)0x63,  /* [826] */
    (xdc_Char)0x6b,  /* [827] */
    (xdc_Char)0x20,  /* [828] */
    (xdc_Char)0x69,  /* [829] */
    (xdc_Char)0x6e,  /* [830] */
    (xdc_Char)0x73,  /* [831] */
    (xdc_Char)0x74,  /* [832] */
    (xdc_Char)0x61,  /* [833] */
    (xdc_Char)0x6e,  /* [834] */
    (xdc_Char)0x63,  /* [835] */
    (xdc_Char)0x65,  /* [836] */
    (xdc_Char)0x20,  /* [837] */
    (xdc_Char)0x77,  /* [838] */
    (xdc_Char)0x68,  /* [839] */
    (xdc_Char)0x65,  /* [840] */
    (xdc_Char)0x6e,  /* [841] */
    (xdc_Char)0x20,  /* [842] */
    (xdc_Char)0x42,  /* [843] */
    (xdc_Char)0x49,  /* [844] */
    (xdc_Char)0x4f,  /* [845] */
    (xdc_Char)0x53,  /* [846] */
    (xdc_Char)0x2e,  /* [847] */
    (xdc_Char)0x63,  /* [848] */
    (xdc_Char)0x6c,  /* [849] */
    (xdc_Char)0x6f,  /* [850] */
    (xdc_Char)0x63,  /* [851] */
    (xdc_Char)0x6b,  /* [852] */
    (xdc_Char)0x45,  /* [853] */
    (xdc_Char)0x6e,  /* [854] */
    (xdc_Char)0x61,  /* [855] */
    (xdc_Char)0x62,  /* [856] */
    (xdc_Char)0x6c,  /* [857] */
    (xdc_Char)0x65,  /* [858] */
    (xdc_Char)0x64,  /* [859] */
    (xdc_Char)0x20,  /* [860] */
    (xdc_Char)0x69,  /* [861] */
    (xdc_Char)0x73,  /* [862] */
    (xdc_Char)0x20,  /* [863] */
    (xdc_Char)0x66,  /* [864] */
    (xdc_Char)0x61,  /* [865] */
    (xdc_Char)0x6c,  /* [866] */
    (xdc_Char)0x73,  /* [867] */
    (xdc_Char)0x65,  /* [868] */
    (xdc_Char)0x2e,  /* [869] */
    (xdc_Char)0x0,  /* [870] */
    (xdc_Char)0x41,  /* [871] */
    (xdc_Char)0x5f,  /* [872] */
    (xdc_Char)0x62,  /* [873] */
    (xdc_Char)0x61,  /* [874] */
    (xdc_Char)0x64,  /* [875] */
    (xdc_Char)0x54,  /* [876] */
    (xdc_Char)0x68,  /* [877] */
    (xdc_Char)0x72,  /* [878] */
    (xdc_Char)0x65,  /* [879] */
    (xdc_Char)0x61,  /* [880] */
    (xdc_Char)0x64,  /* [881] */
    (xdc_Char)0x54,  /* [882] */
    (xdc_Char)0x79,  /* [883] */
    (xdc_Char)0x70,  /* [884] */
    (xdc_Char)0x65,  /* [885] */
    (xdc_Char)0x3a,  /* [886] */
    (xdc_Char)0x20,  /* [887] */
    (xdc_Char)0x43,  /* [888] */
    (xdc_Char)0x61,  /* [889] */
    (xdc_Char)0x6e,  /* [890] */
    (xdc_Char)0x6e,  /* [891] */
    (xdc_Char)0x6f,  /* [892] */
    (xdc_Char)0x74,  /* [893] */
    (xdc_Char)0x20,  /* [894] */
    (xdc_Char)0x63,  /* [895] */
    (xdc_Char)0x72,  /* [896] */
    (xdc_Char)0x65,  /* [897] */
    (xdc_Char)0x61,  /* [898] */
    (xdc_Char)0x74,  /* [899] */
    (xdc_Char)0x65,  /* [900] */
    (xdc_Char)0x2f,  /* [901] */
    (xdc_Char)0x64,  /* [902] */
    (xdc_Char)0x65,  /* [903] */
    (xdc_Char)0x6c,  /* [904] */
    (xdc_Char)0x65,  /* [905] */
    (xdc_Char)0x74,  /* [906] */
    (xdc_Char)0x65,  /* [907] */
    (xdc_Char)0x20,  /* [908] */
    (xdc_Char)0x61,  /* [909] */
    (xdc_Char)0x20,  /* [910] */
    (xdc_Char)0x43,  /* [911] */
    (xdc_Char)0x6c,  /* [912] */
    (xdc_Char)0x6f,  /* [913] */
    (xdc_Char)0x63,  /* [914] */
    (xdc_Char)0x6b,  /* [915] */
    (xdc_Char)0x20,  /* [916] */
    (xdc_Char)0x66,  /* [917] */
    (xdc_Char)0x72,  /* [918] */
    (xdc_Char)0x6f,  /* [919] */
    (xdc_Char)0x6d,  /* [920] */
    (xdc_Char)0x20,  /* [921] */
    (xdc_Char)0x48,  /* [922] */
    (xdc_Char)0x77,  /* [923] */
    (xdc_Char)0x69,  /* [924] */
    (xdc_Char)0x20,  /* [925] */
    (xdc_Char)0x6f,  /* [926] */
    (xdc_Char)0x72,  /* [927] */
    (xdc_Char)0x20,  /* [928] */
    (xdc_Char)0x53,  /* [929] */
    (xdc_Char)0x77,  /* [930] */
    (xdc_Char)0x69,  /* [931] */
    (xdc_Char)0x20,  /* [932] */
    (xdc_Char)0x74,  /* [933] */
    (xdc_Char)0x68,  /* [934] */
    (xdc_Char)0x72,  /* [935] */
    (xdc_Char)0x65,  /* [936] */
    (xdc_Char)0x61,  /* [937] */
    (xdc_Char)0x64,  /* [938] */
    (xdc_Char)0x2e,  /* [939] */
    (xdc_Char)0x0,  /* [940] */
    (xdc_Char)0x41,  /* [941] */
    (xdc_Char)0x5f,  /* [942] */
    (xdc_Char)0x6e,  /* [943] */
    (xdc_Char)0x75,  /* [944] */
    (xdc_Char)0x6c,  /* [945] */
    (xdc_Char)0x6c,  /* [946] */
    (xdc_Char)0x45,  /* [947] */
    (xdc_Char)0x76,  /* [948] */
    (xdc_Char)0x65,  /* [949] */
    (xdc_Char)0x6e,  /* [950] */
    (xdc_Char)0x74,  /* [951] */
    (xdc_Char)0x4d,  /* [952] */
    (xdc_Char)0x61,  /* [953] */
    (xdc_Char)0x73,  /* [954] */
    (xdc_Char)0x6b,  /* [955] */
    (xdc_Char)0x73,  /* [956] */
    (xdc_Char)0x3a,  /* [957] */
    (xdc_Char)0x20,  /* [958] */
    (xdc_Char)0x6f,  /* [959] */
    (xdc_Char)0x72,  /* [960] */
    (xdc_Char)0x4d,  /* [961] */
    (xdc_Char)0x61,  /* [962] */
    (xdc_Char)0x73,  /* [963] */
    (xdc_Char)0x6b,  /* [964] */
    (xdc_Char)0x20,  /* [965] */
    (xdc_Char)0x61,  /* [966] */
    (xdc_Char)0x6e,  /* [967] */
    (xdc_Char)0x64,  /* [968] */
    (xdc_Char)0x20,  /* [969] */
    (xdc_Char)0x61,  /* [970] */
    (xdc_Char)0x6e,  /* [971] */
    (xdc_Char)0x64,  /* [972] */
    (xdc_Char)0x4d,  /* [973] */
    (xdc_Char)0x61,  /* [974] */
    (xdc_Char)0x73,  /* [975] */
    (xdc_Char)0x6b,  /* [976] */
    (xdc_Char)0x20,  /* [977] */
    (xdc_Char)0x61,  /* [978] */
    (xdc_Char)0x72,  /* [979] */
    (xdc_Char)0x65,  /* [980] */
    (xdc_Char)0x20,  /* [981] */
    (xdc_Char)0x6e,  /* [982] */
    (xdc_Char)0x75,  /* [983] */
    (xdc_Char)0x6c,  /* [984] */
    (xdc_Char)0x6c,  /* [985] */
    (xdc_Char)0x2e,  /* [986] */
    (xdc_Char)0x0,  /* [987] */
    (xdc_Char)0x41,  /* [988] */
    (xdc_Char)0x5f,  /* [989] */
    (xdc_Char)0x6e,  /* [990] */
    (xdc_Char)0x75,  /* [991] */
    (xdc_Char)0x6c,  /* [992] */
    (xdc_Char)0x6c,  /* [993] */
    (xdc_Char)0x45,  /* [994] */
    (xdc_Char)0x76,  /* [995] */
    (xdc_Char)0x65,  /* [996] */
    (xdc_Char)0x6e,  /* [997] */
    (xdc_Char)0x74,  /* [998] */
    (xdc_Char)0x49,  /* [999] */
    (xdc_Char)0x64,  /* [1000] */
    (xdc_Char)0x3a,  /* [1001] */
    (xdc_Char)0x20,  /* [1002] */
    (xdc_Char)0x70,  /* [1003] */
    (xdc_Char)0x6f,  /* [1004] */
    (xdc_Char)0x73,  /* [1005] */
    (xdc_Char)0x74,  /* [1006] */
    (xdc_Char)0x65,  /* [1007] */
    (xdc_Char)0x64,  /* [1008] */
    (xdc_Char)0x20,  /* [1009] */
    (xdc_Char)0x65,  /* [1010] */
    (xdc_Char)0x76,  /* [1011] */
    (xdc_Char)0x65,  /* [1012] */
    (xdc_Char)0x6e,  /* [1013] */
    (xdc_Char)0x74,  /* [1014] */
    (xdc_Char)0x49,  /* [1015] */
    (xdc_Char)0x64,  /* [1016] */
    (xdc_Char)0x20,  /* [1017] */
    (xdc_Char)0x69,  /* [1018] */
    (xdc_Char)0x73,  /* [1019] */
    (xdc_Char)0x20,  /* [1020] */
    (xdc_Char)0x6e,  /* [1021] */
    (xdc_Char)0x75,  /* [1022] */
    (xdc_Char)0x6c,  /* [1023] */
    (xdc_Char)0x6c,  /* [1024] */
    (xdc_Char)0x2e,  /* [1025] */
    (xdc_Char)0x0,  /* [1026] */
    (xdc_Char)0x41,  /* [1027] */
    (xdc_Char)0x5f,  /* [1028] */
    (xdc_Char)0x65,  /* [1029] */
    (xdc_Char)0x76,  /* [1030] */
    (xdc_Char)0x65,  /* [1031] */
    (xdc_Char)0x6e,  /* [1032] */
    (xdc_Char)0x74,  /* [1033] */
    (xdc_Char)0x49,  /* [1034] */
    (xdc_Char)0x6e,  /* [1035] */
    (xdc_Char)0x55,  /* [1036] */
    (xdc_Char)0x73,  /* [1037] */
    (xdc_Char)0x65,  /* [1038] */
    (xdc_Char)0x3a,  /* [1039] */
    (xdc_Char)0x20,  /* [1040] */
    (xdc_Char)0x45,  /* [1041] */
    (xdc_Char)0x76,  /* [1042] */
    (xdc_Char)0x65,  /* [1043] */
    (xdc_Char)0x6e,  /* [1044] */
    (xdc_Char)0x74,  /* [1045] */
    (xdc_Char)0x20,  /* [1046] */
    (xdc_Char)0x6f,  /* [1047] */
    (xdc_Char)0x62,  /* [1048] */
    (xdc_Char)0x6a,  /* [1049] */
    (xdc_Char)0x65,  /* [1050] */
    (xdc_Char)0x63,  /* [1051] */
    (xdc_Char)0x74,  /* [1052] */
    (xdc_Char)0x20,  /* [1053] */
    (xdc_Char)0x61,  /* [1054] */
    (xdc_Char)0x6c,  /* [1055] */
    (xdc_Char)0x72,  /* [1056] */
    (xdc_Char)0x65,  /* [1057] */
    (xdc_Char)0x61,  /* [1058] */
    (xdc_Char)0x64,  /* [1059] */
    (xdc_Char)0x79,  /* [1060] */
    (xdc_Char)0x20,  /* [1061] */
    (xdc_Char)0x69,  /* [1062] */
    (xdc_Char)0x6e,  /* [1063] */
    (xdc_Char)0x20,  /* [1064] */
    (xdc_Char)0x75,  /* [1065] */
    (xdc_Char)0x73,  /* [1066] */
    (xdc_Char)0x65,  /* [1067] */
    (xdc_Char)0x2e,  /* [1068] */
    (xdc_Char)0x0,  /* [1069] */
    (xdc_Char)0x41,  /* [1070] */
    (xdc_Char)0x5f,  /* [1071] */
    (xdc_Char)0x62,  /* [1072] */
    (xdc_Char)0x61,  /* [1073] */
    (xdc_Char)0x64,  /* [1074] */
    (xdc_Char)0x43,  /* [1075] */
    (xdc_Char)0x6f,  /* [1076] */
    (xdc_Char)0x6e,  /* [1077] */
    (xdc_Char)0x74,  /* [1078] */
    (xdc_Char)0x65,  /* [1079] */
    (xdc_Char)0x78,  /* [1080] */
    (xdc_Char)0x74,  /* [1081] */
    (xdc_Char)0x3a,  /* [1082] */
    (xdc_Char)0x20,  /* [1083] */
    (xdc_Char)0x62,  /* [1084] */
    (xdc_Char)0x61,  /* [1085] */
    (xdc_Char)0x64,  /* [1086] */
    (xdc_Char)0x20,  /* [1087] */
    (xdc_Char)0x63,  /* [1088] */
    (xdc_Char)0x61,  /* [1089] */
    (xdc_Char)0x6c,  /* [1090] */
    (xdc_Char)0x6c,  /* [1091] */
    (xdc_Char)0x69,  /* [1092] */
    (xdc_Char)0x6e,  /* [1093] */
    (xdc_Char)0x67,  /* [1094] */
    (xdc_Char)0x20,  /* [1095] */
    (xdc_Char)0x63,  /* [1096] */
    (xdc_Char)0x6f,  /* [1097] */
    (xdc_Char)0x6e,  /* [1098] */
    (xdc_Char)0x74,  /* [1099] */
    (xdc_Char)0x65,  /* [1100] */
    (xdc_Char)0x78,  /* [1101] */
    (xdc_Char)0x74,  /* [1102] */
    (xdc_Char)0x2e,  /* [1103] */
    (xdc_Char)0x20,  /* [1104] */
    (xdc_Char)0x4d,  /* [1105] */
    (xdc_Char)0x75,  /* [1106] */
    (xdc_Char)0x73,  /* [1107] */
    (xdc_Char)0x74,  /* [1108] */
    (xdc_Char)0x20,  /* [1109] */
    (xdc_Char)0x62,  /* [1110] */
    (xdc_Char)0x65,  /* [1111] */
    (xdc_Char)0x20,  /* [1112] */
    (xdc_Char)0x63,  /* [1113] */
    (xdc_Char)0x61,  /* [1114] */
    (xdc_Char)0x6c,  /* [1115] */
    (xdc_Char)0x6c,  /* [1116] */
    (xdc_Char)0x65,  /* [1117] */
    (xdc_Char)0x64,  /* [1118] */
    (xdc_Char)0x20,  /* [1119] */
    (xdc_Char)0x66,  /* [1120] */
    (xdc_Char)0x72,  /* [1121] */
    (xdc_Char)0x6f,  /* [1122] */
    (xdc_Char)0x6d,  /* [1123] */
    (xdc_Char)0x20,  /* [1124] */
    (xdc_Char)0x61,  /* [1125] */
    (xdc_Char)0x20,  /* [1126] */
    (xdc_Char)0x54,  /* [1127] */
    (xdc_Char)0x61,  /* [1128] */
    (xdc_Char)0x73,  /* [1129] */
    (xdc_Char)0x6b,  /* [1130] */
    (xdc_Char)0x2e,  /* [1131] */
    (xdc_Char)0x0,  /* [1132] */
    (xdc_Char)0x41,  /* [1133] */
    (xdc_Char)0x5f,  /* [1134] */
    (xdc_Char)0x70,  /* [1135] */
    (xdc_Char)0x65,  /* [1136] */
    (xdc_Char)0x6e,  /* [1137] */
    (xdc_Char)0x64,  /* [1138] */
    (xdc_Char)0x54,  /* [1139] */
    (xdc_Char)0x61,  /* [1140] */
    (xdc_Char)0x73,  /* [1141] */
    (xdc_Char)0x6b,  /* [1142] */
    (xdc_Char)0x44,  /* [1143] */
    (xdc_Char)0x69,  /* [1144] */
    (xdc_Char)0x73,  /* [1145] */
    (xdc_Char)0x61,  /* [1146] */
    (xdc_Char)0x62,  /* [1147] */
    (xdc_Char)0x6c,  /* [1148] */
    (xdc_Char)0x65,  /* [1149] */
    (xdc_Char)0x64,  /* [1150] */
    (xdc_Char)0x3a,  /* [1151] */
    (xdc_Char)0x20,  /* [1152] */
    (xdc_Char)0x43,  /* [1153] */
    (xdc_Char)0x61,  /* [1154] */
    (xdc_Char)0x6e,  /* [1155] */
    (xdc_Char)0x6e,  /* [1156] */
    (xdc_Char)0x6f,  /* [1157] */
    (xdc_Char)0x74,  /* [1158] */
    (xdc_Char)0x20,  /* [1159] */
    (xdc_Char)0x63,  /* [1160] */
    (xdc_Char)0x61,  /* [1161] */
    (xdc_Char)0x6c,  /* [1162] */
    (xdc_Char)0x6c,  /* [1163] */
    (xdc_Char)0x20,  /* [1164] */
    (xdc_Char)0x45,  /* [1165] */
    (xdc_Char)0x76,  /* [1166] */
    (xdc_Char)0x65,  /* [1167] */
    (xdc_Char)0x6e,  /* [1168] */
    (xdc_Char)0x74,  /* [1169] */
    (xdc_Char)0x5f,  /* [1170] */
    (xdc_Char)0x70,  /* [1171] */
    (xdc_Char)0x65,  /* [1172] */
    (xdc_Char)0x6e,  /* [1173] */
    (xdc_Char)0x64,  /* [1174] */
    (xdc_Char)0x28,  /* [1175] */
    (xdc_Char)0x29,  /* [1176] */
    (xdc_Char)0x20,  /* [1177] */
    (xdc_Char)0x77,  /* [1178] */
    (xdc_Char)0x68,  /* [1179] */
    (xdc_Char)0x69,  /* [1180] */
    (xdc_Char)0x6c,  /* [1181] */
    (xdc_Char)0x65,  /* [1182] */
    (xdc_Char)0x20,  /* [1183] */
    (xdc_Char)0x74,  /* [1184] */
    (xdc_Char)0x68,  /* [1185] */
    (xdc_Char)0x65,  /* [1186] */
    (xdc_Char)0x20,  /* [1187] */
    (xdc_Char)0x54,  /* [1188] */
    (xdc_Char)0x61,  /* [1189] */
    (xdc_Char)0x73,  /* [1190] */
    (xdc_Char)0x6b,  /* [1191] */
    (xdc_Char)0x20,  /* [1192] */
    (xdc_Char)0x6f,  /* [1193] */
    (xdc_Char)0x72,  /* [1194] */
    (xdc_Char)0x20,  /* [1195] */
    (xdc_Char)0x53,  /* [1196] */
    (xdc_Char)0x77,  /* [1197] */
    (xdc_Char)0x69,  /* [1198] */
    (xdc_Char)0x20,  /* [1199] */
    (xdc_Char)0x73,  /* [1200] */
    (xdc_Char)0x63,  /* [1201] */
    (xdc_Char)0x68,  /* [1202] */
    (xdc_Char)0x65,  /* [1203] */
    (xdc_Char)0x64,  /* [1204] */
    (xdc_Char)0x75,  /* [1205] */
    (xdc_Char)0x6c,  /* [1206] */
    (xdc_Char)0x65,  /* [1207] */
    (xdc_Char)0x72,  /* [1208] */
    (xdc_Char)0x20,  /* [1209] */
    (xdc_Char)0x69,  /* [1210] */
    (xdc_Char)0x73,  /* [1211] */
    (xdc_Char)0x20,  /* [1212] */
    (xdc_Char)0x64,  /* [1213] */
    (xdc_Char)0x69,  /* [1214] */
    (xdc_Char)0x73,  /* [1215] */
    (xdc_Char)0x61,  /* [1216] */
    (xdc_Char)0x62,  /* [1217] */
    (xdc_Char)0x6c,  /* [1218] */
    (xdc_Char)0x65,  /* [1219] */
    (xdc_Char)0x64,  /* [1220] */
    (xdc_Char)0x2e,  /* [1221] */
    (xdc_Char)0x0,  /* [1222] */
    (xdc_Char)0x4d,  /* [1223] */
    (xdc_Char)0x61,  /* [1224] */
    (xdc_Char)0x69,  /* [1225] */
    (xdc_Char)0x6c,  /* [1226] */
    (xdc_Char)0x62,  /* [1227] */
    (xdc_Char)0x6f,  /* [1228] */
    (xdc_Char)0x78,  /* [1229] */
    (xdc_Char)0x5f,  /* [1230] */
    (xdc_Char)0x63,  /* [1231] */
    (xdc_Char)0x72,  /* [1232] */
    (xdc_Char)0x65,  /* [1233] */
    (xdc_Char)0x61,  /* [1234] */
    (xdc_Char)0x74,  /* [1235] */
    (xdc_Char)0x65,  /* [1236] */
    (xdc_Char)0x27,  /* [1237] */
    (xdc_Char)0x73,  /* [1238] */
    (xdc_Char)0x20,  /* [1239] */
    (xdc_Char)0x62,  /* [1240] */
    (xdc_Char)0x75,  /* [1241] */
    (xdc_Char)0x66,  /* [1242] */
    (xdc_Char)0x53,  /* [1243] */
    (xdc_Char)0x69,  /* [1244] */
    (xdc_Char)0x7a,  /* [1245] */
    (xdc_Char)0x65,  /* [1246] */
    (xdc_Char)0x20,  /* [1247] */
    (xdc_Char)0x70,  /* [1248] */
    (xdc_Char)0x61,  /* [1249] */
    (xdc_Char)0x72,  /* [1250] */
    (xdc_Char)0x61,  /* [1251] */
    (xdc_Char)0x6d,  /* [1252] */
    (xdc_Char)0x65,  /* [1253] */
    (xdc_Char)0x74,  /* [1254] */
    (xdc_Char)0x65,  /* [1255] */
    (xdc_Char)0x72,  /* [1256] */
    (xdc_Char)0x20,  /* [1257] */
    (xdc_Char)0x69,  /* [1258] */
    (xdc_Char)0x73,  /* [1259] */
    (xdc_Char)0x20,  /* [1260] */
    (xdc_Char)0x69,  /* [1261] */
    (xdc_Char)0x6e,  /* [1262] */
    (xdc_Char)0x76,  /* [1263] */
    (xdc_Char)0x61,  /* [1264] */
    (xdc_Char)0x6c,  /* [1265] */
    (xdc_Char)0x69,  /* [1266] */
    (xdc_Char)0x64,  /* [1267] */
    (xdc_Char)0x20,  /* [1268] */
    (xdc_Char)0x28,  /* [1269] */
    (xdc_Char)0x74,  /* [1270] */
    (xdc_Char)0x6f,  /* [1271] */
    (xdc_Char)0x6f,  /* [1272] */
    (xdc_Char)0x20,  /* [1273] */
    (xdc_Char)0x73,  /* [1274] */
    (xdc_Char)0x6d,  /* [1275] */
    (xdc_Char)0x61,  /* [1276] */
    (xdc_Char)0x6c,  /* [1277] */
    (xdc_Char)0x6c,  /* [1278] */
    (xdc_Char)0x29,  /* [1279] */
    (xdc_Char)0x0,  /* [1280] */
    (xdc_Char)0x41,  /* [1281] */
    (xdc_Char)0x5f,  /* [1282] */
    (xdc_Char)0x6e,  /* [1283] */
    (xdc_Char)0x6f,  /* [1284] */
    (xdc_Char)0x45,  /* [1285] */
    (xdc_Char)0x76,  /* [1286] */
    (xdc_Char)0x65,  /* [1287] */
    (xdc_Char)0x6e,  /* [1288] */
    (xdc_Char)0x74,  /* [1289] */
    (xdc_Char)0x73,  /* [1290] */
    (xdc_Char)0x3a,  /* [1291] */
    (xdc_Char)0x20,  /* [1292] */
    (xdc_Char)0x54,  /* [1293] */
    (xdc_Char)0x68,  /* [1294] */
    (xdc_Char)0x65,  /* [1295] */
    (xdc_Char)0x20,  /* [1296] */
    (xdc_Char)0x45,  /* [1297] */
    (xdc_Char)0x76,  /* [1298] */
    (xdc_Char)0x65,  /* [1299] */
    (xdc_Char)0x6e,  /* [1300] */
    (xdc_Char)0x74,  /* [1301] */
    (xdc_Char)0x2e,  /* [1302] */
    (xdc_Char)0x73,  /* [1303] */
    (xdc_Char)0x75,  /* [1304] */
    (xdc_Char)0x70,  /* [1305] */
    (xdc_Char)0x70,  /* [1306] */
    (xdc_Char)0x6f,  /* [1307] */
    (xdc_Char)0x72,  /* [1308] */
    (xdc_Char)0x74,  /* [1309] */
    (xdc_Char)0x73,  /* [1310] */
    (xdc_Char)0x45,  /* [1311] */
    (xdc_Char)0x76,  /* [1312] */
    (xdc_Char)0x65,  /* [1313] */
    (xdc_Char)0x6e,  /* [1314] */
    (xdc_Char)0x74,  /* [1315] */
    (xdc_Char)0x73,  /* [1316] */
    (xdc_Char)0x20,  /* [1317] */
    (xdc_Char)0x66,  /* [1318] */
    (xdc_Char)0x6c,  /* [1319] */
    (xdc_Char)0x61,  /* [1320] */
    (xdc_Char)0x67,  /* [1321] */
    (xdc_Char)0x20,  /* [1322] */
    (xdc_Char)0x69,  /* [1323] */
    (xdc_Char)0x73,  /* [1324] */
    (xdc_Char)0x20,  /* [1325] */
    (xdc_Char)0x64,  /* [1326] */
    (xdc_Char)0x69,  /* [1327] */
    (xdc_Char)0x73,  /* [1328] */
    (xdc_Char)0x61,  /* [1329] */
    (xdc_Char)0x62,  /* [1330] */
    (xdc_Char)0x6c,  /* [1331] */
    (xdc_Char)0x65,  /* [1332] */
    (xdc_Char)0x64,  /* [1333] */
    (xdc_Char)0x2e,  /* [1334] */
    (xdc_Char)0x0,  /* [1335] */
    (xdc_Char)0x41,  /* [1336] */
    (xdc_Char)0x5f,  /* [1337] */
    (xdc_Char)0x69,  /* [1338] */
    (xdc_Char)0x6e,  /* [1339] */
    (xdc_Char)0x76,  /* [1340] */
    (xdc_Char)0x54,  /* [1341] */
    (xdc_Char)0x69,  /* [1342] */
    (xdc_Char)0x6d,  /* [1343] */
    (xdc_Char)0x65,  /* [1344] */
    (xdc_Char)0x6f,  /* [1345] */
    (xdc_Char)0x75,  /* [1346] */
    (xdc_Char)0x74,  /* [1347] */
    (xdc_Char)0x3a,  /* [1348] */
    (xdc_Char)0x20,  /* [1349] */
    (xdc_Char)0x43,  /* [1350] */
    (xdc_Char)0x61,  /* [1351] */
    (xdc_Char)0x6e,  /* [1352] */
    (xdc_Char)0x27,  /* [1353] */
    (xdc_Char)0x74,  /* [1354] */
    (xdc_Char)0x20,  /* [1355] */
    (xdc_Char)0x75,  /* [1356] */
    (xdc_Char)0x73,  /* [1357] */
    (xdc_Char)0x65,  /* [1358] */
    (xdc_Char)0x20,  /* [1359] */
    (xdc_Char)0x42,  /* [1360] */
    (xdc_Char)0x49,  /* [1361] */
    (xdc_Char)0x4f,  /* [1362] */
    (xdc_Char)0x53,  /* [1363] */
    (xdc_Char)0x5f,  /* [1364] */
    (xdc_Char)0x45,  /* [1365] */
    (xdc_Char)0x56,  /* [1366] */
    (xdc_Char)0x45,  /* [1367] */
    (xdc_Char)0x4e,  /* [1368] */
    (xdc_Char)0x54,  /* [1369] */
    (xdc_Char)0x5f,  /* [1370] */
    (xdc_Char)0x41,  /* [1371] */
    (xdc_Char)0x43,  /* [1372] */
    (xdc_Char)0x51,  /* [1373] */
    (xdc_Char)0x55,  /* [1374] */
    (xdc_Char)0x49,  /* [1375] */
    (xdc_Char)0x52,  /* [1376] */
    (xdc_Char)0x45,  /* [1377] */
    (xdc_Char)0x44,  /* [1378] */
    (xdc_Char)0x20,  /* [1379] */
    (xdc_Char)0x77,  /* [1380] */
    (xdc_Char)0x69,  /* [1381] */
    (xdc_Char)0x74,  /* [1382] */
    (xdc_Char)0x68,  /* [1383] */
    (xdc_Char)0x20,  /* [1384] */
    (xdc_Char)0x74,  /* [1385] */
    (xdc_Char)0x68,  /* [1386] */
    (xdc_Char)0x69,  /* [1387] */
    (xdc_Char)0x73,  /* [1388] */
    (xdc_Char)0x20,  /* [1389] */
    (xdc_Char)0x53,  /* [1390] */
    (xdc_Char)0x65,  /* [1391] */
    (xdc_Char)0x6d,  /* [1392] */
    (xdc_Char)0x61,  /* [1393] */
    (xdc_Char)0x70,  /* [1394] */
    (xdc_Char)0x68,  /* [1395] */
    (xdc_Char)0x6f,  /* [1396] */
    (xdc_Char)0x72,  /* [1397] */
    (xdc_Char)0x65,  /* [1398] */
    (xdc_Char)0x2e,  /* [1399] */
    (xdc_Char)0x0,  /* [1400] */
    (xdc_Char)0x41,  /* [1401] */
    (xdc_Char)0x5f,  /* [1402] */
    (xdc_Char)0x6f,  /* [1403] */
    (xdc_Char)0x76,  /* [1404] */
    (xdc_Char)0x65,  /* [1405] */
    (xdc_Char)0x72,  /* [1406] */
    (xdc_Char)0x66,  /* [1407] */
    (xdc_Char)0x6c,  /* [1408] */
    (xdc_Char)0x6f,  /* [1409] */
    (xdc_Char)0x77,  /* [1410] */
    (xdc_Char)0x3a,  /* [1411] */
    (xdc_Char)0x20,  /* [1412] */
    (xdc_Char)0x43,  /* [1413] */
    (xdc_Char)0x6f,  /* [1414] */
    (xdc_Char)0x75,  /* [1415] */
    (xdc_Char)0x6e,  /* [1416] */
    (xdc_Char)0x74,  /* [1417] */
    (xdc_Char)0x20,  /* [1418] */
    (xdc_Char)0x68,  /* [1419] */
    (xdc_Char)0x61,  /* [1420] */
    (xdc_Char)0x73,  /* [1421] */
    (xdc_Char)0x20,  /* [1422] */
    (xdc_Char)0x65,  /* [1423] */
    (xdc_Char)0x78,  /* [1424] */
    (xdc_Char)0x63,  /* [1425] */
    (xdc_Char)0x65,  /* [1426] */
    (xdc_Char)0x65,  /* [1427] */
    (xdc_Char)0x64,  /* [1428] */
    (xdc_Char)0x65,  /* [1429] */
    (xdc_Char)0x64,  /* [1430] */
    (xdc_Char)0x20,  /* [1431] */
    (xdc_Char)0x36,  /* [1432] */
    (xdc_Char)0x35,  /* [1433] */
    (xdc_Char)0x35,  /* [1434] */
    (xdc_Char)0x33,  /* [1435] */
    (xdc_Char)0x35,  /* [1436] */
    (xdc_Char)0x20,  /* [1437] */
    (xdc_Char)0x61,  /* [1438] */
    (xdc_Char)0x6e,  /* [1439] */
    (xdc_Char)0x64,  /* [1440] */
    (xdc_Char)0x20,  /* [1441] */
    (xdc_Char)0x72,  /* [1442] */
    (xdc_Char)0x6f,  /* [1443] */
    (xdc_Char)0x6c,  /* [1444] */
    (xdc_Char)0x6c,  /* [1445] */
    (xdc_Char)0x65,  /* [1446] */
    (xdc_Char)0x64,  /* [1447] */
    (xdc_Char)0x20,  /* [1448] */
    (xdc_Char)0x6f,  /* [1449] */
    (xdc_Char)0x76,  /* [1450] */
    (xdc_Char)0x65,  /* [1451] */
    (xdc_Char)0x72,  /* [1452] */
    (xdc_Char)0x2e,  /* [1453] */
    (xdc_Char)0x0,  /* [1454] */
    (xdc_Char)0x41,  /* [1455] */
    (xdc_Char)0x5f,  /* [1456] */
    (xdc_Char)0x70,  /* [1457] */
    (xdc_Char)0x65,  /* [1458] */
    (xdc_Char)0x6e,  /* [1459] */
    (xdc_Char)0x64,  /* [1460] */
    (xdc_Char)0x54,  /* [1461] */
    (xdc_Char)0x61,  /* [1462] */
    (xdc_Char)0x73,  /* [1463] */
    (xdc_Char)0x6b,  /* [1464] */
    (xdc_Char)0x44,  /* [1465] */
    (xdc_Char)0x69,  /* [1466] */
    (xdc_Char)0x73,  /* [1467] */
    (xdc_Char)0x61,  /* [1468] */
    (xdc_Char)0x62,  /* [1469] */
    (xdc_Char)0x6c,  /* [1470] */
    (xdc_Char)0x65,  /* [1471] */
    (xdc_Char)0x64,  /* [1472] */
    (xdc_Char)0x3a,  /* [1473] */
    (xdc_Char)0x20,  /* [1474] */
    (xdc_Char)0x43,  /* [1475] */
    (xdc_Char)0x61,  /* [1476] */
    (xdc_Char)0x6e,  /* [1477] */
    (xdc_Char)0x6e,  /* [1478] */
    (xdc_Char)0x6f,  /* [1479] */
    (xdc_Char)0x74,  /* [1480] */
    (xdc_Char)0x20,  /* [1481] */
    (xdc_Char)0x63,  /* [1482] */
    (xdc_Char)0x61,  /* [1483] */
    (xdc_Char)0x6c,  /* [1484] */
    (xdc_Char)0x6c,  /* [1485] */
    (xdc_Char)0x20,  /* [1486] */
    (xdc_Char)0x53,  /* [1487] */
    (xdc_Char)0x65,  /* [1488] */
    (xdc_Char)0x6d,  /* [1489] */
    (xdc_Char)0x61,  /* [1490] */
    (xdc_Char)0x70,  /* [1491] */
    (xdc_Char)0x68,  /* [1492] */
    (xdc_Char)0x6f,  /* [1493] */
    (xdc_Char)0x72,  /* [1494] */
    (xdc_Char)0x65,  /* [1495] */
    (xdc_Char)0x5f,  /* [1496] */
    (xdc_Char)0x70,  /* [1497] */
    (xdc_Char)0x65,  /* [1498] */
    (xdc_Char)0x6e,  /* [1499] */
    (xdc_Char)0x64,  /* [1500] */
    (xdc_Char)0x28,  /* [1501] */
    (xdc_Char)0x29,  /* [1502] */
    (xdc_Char)0x20,  /* [1503] */
    (xdc_Char)0x77,  /* [1504] */
    (xdc_Char)0x68,  /* [1505] */
    (xdc_Char)0x69,  /* [1506] */
    (xdc_Char)0x6c,  /* [1507] */
    (xdc_Char)0x65,  /* [1508] */
    (xdc_Char)0x20,  /* [1509] */
    (xdc_Char)0x74,  /* [1510] */
    (xdc_Char)0x68,  /* [1511] */
    (xdc_Char)0x65,  /* [1512] */
    (xdc_Char)0x20,  /* [1513] */
    (xdc_Char)0x54,  /* [1514] */
    (xdc_Char)0x61,  /* [1515] */
    (xdc_Char)0x73,  /* [1516] */
    (xdc_Char)0x6b,  /* [1517] */
    (xdc_Char)0x20,  /* [1518] */
    (xdc_Char)0x6f,  /* [1519] */
    (xdc_Char)0x72,  /* [1520] */
    (xdc_Char)0x20,  /* [1521] */
    (xdc_Char)0x53,  /* [1522] */
    (xdc_Char)0x77,  /* [1523] */
    (xdc_Char)0x69,  /* [1524] */
    (xdc_Char)0x20,  /* [1525] */
    (xdc_Char)0x73,  /* [1526] */
    (xdc_Char)0x63,  /* [1527] */
    (xdc_Char)0x68,  /* [1528] */
    (xdc_Char)0x65,  /* [1529] */
    (xdc_Char)0x64,  /* [1530] */
    (xdc_Char)0x75,  /* [1531] */
    (xdc_Char)0x6c,  /* [1532] */
    (xdc_Char)0x65,  /* [1533] */
    (xdc_Char)0x72,  /* [1534] */
    (xdc_Char)0x20,  /* [1535] */
    (xdc_Char)0x69,  /* [1536] */
    (xdc_Char)0x73,  /* [1537] */
    (xdc_Char)0x20,  /* [1538] */
    (xdc_Char)0x64,  /* [1539] */
    (xdc_Char)0x69,  /* [1540] */
    (xdc_Char)0x73,  /* [1541] */
    (xdc_Char)0x61,  /* [1542] */
    (xdc_Char)0x62,  /* [1543] */
    (xdc_Char)0x6c,  /* [1544] */
    (xdc_Char)0x65,  /* [1545] */
    (xdc_Char)0x64,  /* [1546] */
    (xdc_Char)0x2e,  /* [1547] */
    (xdc_Char)0x0,  /* [1548] */
    (xdc_Char)0x41,  /* [1549] */
    (xdc_Char)0x5f,  /* [1550] */
    (xdc_Char)0x73,  /* [1551] */
    (xdc_Char)0x77,  /* [1552] */
    (xdc_Char)0x69,  /* [1553] */
    (xdc_Char)0x44,  /* [1554] */
    (xdc_Char)0x69,  /* [1555] */
    (xdc_Char)0x73,  /* [1556] */
    (xdc_Char)0x61,  /* [1557] */
    (xdc_Char)0x62,  /* [1558] */
    (xdc_Char)0x6c,  /* [1559] */
    (xdc_Char)0x65,  /* [1560] */
    (xdc_Char)0x64,  /* [1561] */
    (xdc_Char)0x3a,  /* [1562] */
    (xdc_Char)0x20,  /* [1563] */
    (xdc_Char)0x43,  /* [1564] */
    (xdc_Char)0x61,  /* [1565] */
    (xdc_Char)0x6e,  /* [1566] */
    (xdc_Char)0x6e,  /* [1567] */
    (xdc_Char)0x6f,  /* [1568] */
    (xdc_Char)0x74,  /* [1569] */
    (xdc_Char)0x20,  /* [1570] */
    (xdc_Char)0x63,  /* [1571] */
    (xdc_Char)0x72,  /* [1572] */
    (xdc_Char)0x65,  /* [1573] */
    (xdc_Char)0x61,  /* [1574] */
    (xdc_Char)0x74,  /* [1575] */
    (xdc_Char)0x65,  /* [1576] */
    (xdc_Char)0x20,  /* [1577] */
    (xdc_Char)0x61,  /* [1578] */
    (xdc_Char)0x20,  /* [1579] */
    (xdc_Char)0x53,  /* [1580] */
    (xdc_Char)0x77,  /* [1581] */
    (xdc_Char)0x69,  /* [1582] */
    (xdc_Char)0x20,  /* [1583] */
    (xdc_Char)0x77,  /* [1584] */
    (xdc_Char)0x68,  /* [1585] */
    (xdc_Char)0x65,  /* [1586] */
    (xdc_Char)0x6e,  /* [1587] */
    (xdc_Char)0x20,  /* [1588] */
    (xdc_Char)0x53,  /* [1589] */
    (xdc_Char)0x77,  /* [1590] */
    (xdc_Char)0x69,  /* [1591] */
    (xdc_Char)0x20,  /* [1592] */
    (xdc_Char)0x69,  /* [1593] */
    (xdc_Char)0x73,  /* [1594] */
    (xdc_Char)0x20,  /* [1595] */
    (xdc_Char)0x64,  /* [1596] */
    (xdc_Char)0x69,  /* [1597] */
    (xdc_Char)0x73,  /* [1598] */
    (xdc_Char)0x61,  /* [1599] */
    (xdc_Char)0x62,  /* [1600] */
    (xdc_Char)0x6c,  /* [1601] */
    (xdc_Char)0x65,  /* [1602] */
    (xdc_Char)0x64,  /* [1603] */
    (xdc_Char)0x2e,  /* [1604] */
    (xdc_Char)0x0,  /* [1605] */
    (xdc_Char)0x41,  /* [1606] */
    (xdc_Char)0x5f,  /* [1607] */
    (xdc_Char)0x62,  /* [1608] */
    (xdc_Char)0x61,  /* [1609] */
    (xdc_Char)0x64,  /* [1610] */
    (xdc_Char)0x50,  /* [1611] */
    (xdc_Char)0x72,  /* [1612] */
    (xdc_Char)0x69,  /* [1613] */
    (xdc_Char)0x6f,  /* [1614] */
    (xdc_Char)0x72,  /* [1615] */
    (xdc_Char)0x69,  /* [1616] */
    (xdc_Char)0x74,  /* [1617] */
    (xdc_Char)0x79,  /* [1618] */
    (xdc_Char)0x3a,  /* [1619] */
    (xdc_Char)0x20,  /* [1620] */
    (xdc_Char)0x41,  /* [1621] */
    (xdc_Char)0x6e,  /* [1622] */
    (xdc_Char)0x20,  /* [1623] */
    (xdc_Char)0x69,  /* [1624] */
    (xdc_Char)0x6e,  /* [1625] */
    (xdc_Char)0x76,  /* [1626] */
    (xdc_Char)0x61,  /* [1627] */
    (xdc_Char)0x6c,  /* [1628] */
    (xdc_Char)0x69,  /* [1629] */
    (xdc_Char)0x64,  /* [1630] */
    (xdc_Char)0x20,  /* [1631] */
    (xdc_Char)0x53,  /* [1632] */
    (xdc_Char)0x77,  /* [1633] */
    (xdc_Char)0x69,  /* [1634] */
    (xdc_Char)0x20,  /* [1635] */
    (xdc_Char)0x70,  /* [1636] */
    (xdc_Char)0x72,  /* [1637] */
    (xdc_Char)0x69,  /* [1638] */
    (xdc_Char)0x6f,  /* [1639] */
    (xdc_Char)0x72,  /* [1640] */
    (xdc_Char)0x69,  /* [1641] */
    (xdc_Char)0x74,  /* [1642] */
    (xdc_Char)0x79,  /* [1643] */
    (xdc_Char)0x20,  /* [1644] */
    (xdc_Char)0x77,  /* [1645] */
    (xdc_Char)0x61,  /* [1646] */
    (xdc_Char)0x73,  /* [1647] */
    (xdc_Char)0x20,  /* [1648] */
    (xdc_Char)0x75,  /* [1649] */
    (xdc_Char)0x73,  /* [1650] */
    (xdc_Char)0x65,  /* [1651] */
    (xdc_Char)0x64,  /* [1652] */
    (xdc_Char)0x2e,  /* [1653] */
    (xdc_Char)0x0,  /* [1654] */
    (xdc_Char)0x41,  /* [1655] */
    (xdc_Char)0x5f,  /* [1656] */
    (xdc_Char)0x62,  /* [1657] */
    (xdc_Char)0x61,  /* [1658] */
    (xdc_Char)0x64,  /* [1659] */
    (xdc_Char)0x43,  /* [1660] */
    (xdc_Char)0x6f,  /* [1661] */
    (xdc_Char)0x6e,  /* [1662] */
    (xdc_Char)0x74,  /* [1663] */
    (xdc_Char)0x65,  /* [1664] */
    (xdc_Char)0x78,  /* [1665] */
    (xdc_Char)0x74,  /* [1666] */
    (xdc_Char)0x49,  /* [1667] */
    (xdc_Char)0x64,  /* [1668] */
    (xdc_Char)0x3a,  /* [1669] */
    (xdc_Char)0x20,  /* [1670] */
    (xdc_Char)0x48,  /* [1671] */
    (xdc_Char)0x6f,  /* [1672] */
    (xdc_Char)0x6f,  /* [1673] */
    (xdc_Char)0x6b,  /* [1674] */
    (xdc_Char)0x20,  /* [1675] */
    (xdc_Char)0x63,  /* [1676] */
    (xdc_Char)0x6f,  /* [1677] */
    (xdc_Char)0x6e,  /* [1678] */
    (xdc_Char)0x74,  /* [1679] */
    (xdc_Char)0x65,  /* [1680] */
    (xdc_Char)0x78,  /* [1681] */
    (xdc_Char)0x74,  /* [1682] */
    (xdc_Char)0x20,  /* [1683] */
    (xdc_Char)0x69,  /* [1684] */
    (xdc_Char)0x64,  /* [1685] */
    (xdc_Char)0x20,  /* [1686] */
    (xdc_Char)0x69,  /* [1687] */
    (xdc_Char)0x73,  /* [1688] */
    (xdc_Char)0x20,  /* [1689] */
    (xdc_Char)0x6f,  /* [1690] */
    (xdc_Char)0x75,  /* [1691] */
    (xdc_Char)0x74,  /* [1692] */
    (xdc_Char)0x20,  /* [1693] */
    (xdc_Char)0x6f,  /* [1694] */
    (xdc_Char)0x66,  /* [1695] */
    (xdc_Char)0x20,  /* [1696] */
    (xdc_Char)0x72,  /* [1697] */
    (xdc_Char)0x61,  /* [1698] */
    (xdc_Char)0x6e,  /* [1699] */
    (xdc_Char)0x67,  /* [1700] */
    (xdc_Char)0x65,  /* [1701] */
    (xdc_Char)0x2e,  /* [1702] */
    (xdc_Char)0x0,  /* [1703] */
    (xdc_Char)0x41,  /* [1704] */
    (xdc_Char)0x5f,  /* [1705] */
    (xdc_Char)0x62,  /* [1706] */
    (xdc_Char)0x61,  /* [1707] */
    (xdc_Char)0x64,  /* [1708] */
    (xdc_Char)0x54,  /* [1709] */
    (xdc_Char)0x68,  /* [1710] */
    (xdc_Char)0x72,  /* [1711] */
    (xdc_Char)0x65,  /* [1712] */
    (xdc_Char)0x61,  /* [1713] */
    (xdc_Char)0x64,  /* [1714] */
    (xdc_Char)0x54,  /* [1715] */
    (xdc_Char)0x79,  /* [1716] */
    (xdc_Char)0x70,  /* [1717] */
    (xdc_Char)0x65,  /* [1718] */
    (xdc_Char)0x3a,  /* [1719] */
    (xdc_Char)0x20,  /* [1720] */
    (xdc_Char)0x43,  /* [1721] */
    (xdc_Char)0x61,  /* [1722] */
    (xdc_Char)0x6e,  /* [1723] */
    (xdc_Char)0x6e,  /* [1724] */
    (xdc_Char)0x6f,  /* [1725] */
    (xdc_Char)0x74,  /* [1726] */
    (xdc_Char)0x20,  /* [1727] */
    (xdc_Char)0x63,  /* [1728] */
    (xdc_Char)0x72,  /* [1729] */
    (xdc_Char)0x65,  /* [1730] */
    (xdc_Char)0x61,  /* [1731] */
    (xdc_Char)0x74,  /* [1732] */
    (xdc_Char)0x65,  /* [1733] */
    (xdc_Char)0x2f,  /* [1734] */
    (xdc_Char)0x64,  /* [1735] */
    (xdc_Char)0x65,  /* [1736] */
    (xdc_Char)0x6c,  /* [1737] */
    (xdc_Char)0x65,  /* [1738] */
    (xdc_Char)0x74,  /* [1739] */
    (xdc_Char)0x65,  /* [1740] */
    (xdc_Char)0x20,  /* [1741] */
    (xdc_Char)0x61,  /* [1742] */
    (xdc_Char)0x20,  /* [1743] */
    (xdc_Char)0x74,  /* [1744] */
    (xdc_Char)0x61,  /* [1745] */
    (xdc_Char)0x73,  /* [1746] */
    (xdc_Char)0x6b,  /* [1747] */
    (xdc_Char)0x20,  /* [1748] */
    (xdc_Char)0x66,  /* [1749] */
    (xdc_Char)0x72,  /* [1750] */
    (xdc_Char)0x6f,  /* [1751] */
    (xdc_Char)0x6d,  /* [1752] */
    (xdc_Char)0x20,  /* [1753] */
    (xdc_Char)0x48,  /* [1754] */
    (xdc_Char)0x77,  /* [1755] */
    (xdc_Char)0x69,  /* [1756] */
    (xdc_Char)0x20,  /* [1757] */
    (xdc_Char)0x6f,  /* [1758] */
    (xdc_Char)0x72,  /* [1759] */
    (xdc_Char)0x20,  /* [1760] */
    (xdc_Char)0x53,  /* [1761] */
    (xdc_Char)0x77,  /* [1762] */
    (xdc_Char)0x69,  /* [1763] */
    (xdc_Char)0x20,  /* [1764] */
    (xdc_Char)0x74,  /* [1765] */
    (xdc_Char)0x68,  /* [1766] */
    (xdc_Char)0x72,  /* [1767] */
    (xdc_Char)0x65,  /* [1768] */
    (xdc_Char)0x61,  /* [1769] */
    (xdc_Char)0x64,  /* [1770] */
    (xdc_Char)0x2e,  /* [1771] */
    (xdc_Char)0x0,  /* [1772] */
    (xdc_Char)0x41,  /* [1773] */
    (xdc_Char)0x5f,  /* [1774] */
    (xdc_Char)0x62,  /* [1775] */
    (xdc_Char)0x61,  /* [1776] */
    (xdc_Char)0x64,  /* [1777] */
    (xdc_Char)0x54,  /* [1778] */
    (xdc_Char)0x61,  /* [1779] */
    (xdc_Char)0x73,  /* [1780] */
    (xdc_Char)0x6b,  /* [1781] */
    (xdc_Char)0x53,  /* [1782] */
    (xdc_Char)0x74,  /* [1783] */
    (xdc_Char)0x61,  /* [1784] */
    (xdc_Char)0x74,  /* [1785] */
    (xdc_Char)0x65,  /* [1786] */
    (xdc_Char)0x3a,  /* [1787] */
    (xdc_Char)0x20,  /* [1788] */
    (xdc_Char)0x43,  /* [1789] */
    (xdc_Char)0x61,  /* [1790] */
    (xdc_Char)0x6e,  /* [1791] */
    (xdc_Char)0x27,  /* [1792] */
    (xdc_Char)0x74,  /* [1793] */
    (xdc_Char)0x20,  /* [1794] */
    (xdc_Char)0x64,  /* [1795] */
    (xdc_Char)0x65,  /* [1796] */
    (xdc_Char)0x6c,  /* [1797] */
    (xdc_Char)0x65,  /* [1798] */
    (xdc_Char)0x74,  /* [1799] */
    (xdc_Char)0x65,  /* [1800] */
    (xdc_Char)0x20,  /* [1801] */
    (xdc_Char)0x61,  /* [1802] */
    (xdc_Char)0x20,  /* [1803] */
    (xdc_Char)0x74,  /* [1804] */
    (xdc_Char)0x61,  /* [1805] */
    (xdc_Char)0x73,  /* [1806] */
    (xdc_Char)0x6b,  /* [1807] */
    (xdc_Char)0x20,  /* [1808] */
    (xdc_Char)0x69,  /* [1809] */
    (xdc_Char)0x6e,  /* [1810] */
    (xdc_Char)0x20,  /* [1811] */
    (xdc_Char)0x52,  /* [1812] */
    (xdc_Char)0x55,  /* [1813] */
    (xdc_Char)0x4e,  /* [1814] */
    (xdc_Char)0x4e,  /* [1815] */
    (xdc_Char)0x49,  /* [1816] */
    (xdc_Char)0x4e,  /* [1817] */
    (xdc_Char)0x47,  /* [1818] */
    (xdc_Char)0x20,  /* [1819] */
    (xdc_Char)0x73,  /* [1820] */
    (xdc_Char)0x74,  /* [1821] */
    (xdc_Char)0x61,  /* [1822] */
    (xdc_Char)0x74,  /* [1823] */
    (xdc_Char)0x65,  /* [1824] */
    (xdc_Char)0x2e,  /* [1825] */
    (xdc_Char)0x0,  /* [1826] */
    (xdc_Char)0x41,  /* [1827] */
    (xdc_Char)0x5f,  /* [1828] */
    (xdc_Char)0x6e,  /* [1829] */
    (xdc_Char)0x6f,  /* [1830] */
    (xdc_Char)0x50,  /* [1831] */
    (xdc_Char)0x65,  /* [1832] */
    (xdc_Char)0x6e,  /* [1833] */
    (xdc_Char)0x64,  /* [1834] */
    (xdc_Char)0x45,  /* [1835] */
    (xdc_Char)0x6c,  /* [1836] */
    (xdc_Char)0x65,  /* [1837] */
    (xdc_Char)0x6d,  /* [1838] */
    (xdc_Char)0x3a,  /* [1839] */
    (xdc_Char)0x20,  /* [1840] */
    (xdc_Char)0x4e,  /* [1841] */
    (xdc_Char)0x6f,  /* [1842] */
    (xdc_Char)0x74,  /* [1843] */
    (xdc_Char)0x20,  /* [1844] */
    (xdc_Char)0x65,  /* [1845] */
    (xdc_Char)0x6e,  /* [1846] */
    (xdc_Char)0x6f,  /* [1847] */
    (xdc_Char)0x75,  /* [1848] */
    (xdc_Char)0x67,  /* [1849] */
    (xdc_Char)0x68,  /* [1850] */
    (xdc_Char)0x20,  /* [1851] */
    (xdc_Char)0x69,  /* [1852] */
    (xdc_Char)0x6e,  /* [1853] */
    (xdc_Char)0x66,  /* [1854] */
    (xdc_Char)0x6f,  /* [1855] */
    (xdc_Char)0x20,  /* [1856] */
    (xdc_Char)0x74,  /* [1857] */
    (xdc_Char)0x6f,  /* [1858] */
    (xdc_Char)0x20,  /* [1859] */
    (xdc_Char)0x64,  /* [1860] */
    (xdc_Char)0x65,  /* [1861] */
    (xdc_Char)0x6c,  /* [1862] */
    (xdc_Char)0x65,  /* [1863] */
    (xdc_Char)0x74,  /* [1864] */
    (xdc_Char)0x65,  /* [1865] */
    (xdc_Char)0x20,  /* [1866] */
    (xdc_Char)0x42,  /* [1867] */
    (xdc_Char)0x4c,  /* [1868] */
    (xdc_Char)0x4f,  /* [1869] */
    (xdc_Char)0x43,  /* [1870] */
    (xdc_Char)0x4b,  /* [1871] */
    (xdc_Char)0x45,  /* [1872] */
    (xdc_Char)0x44,  /* [1873] */
    (xdc_Char)0x20,  /* [1874] */
    (xdc_Char)0x74,  /* [1875] */
    (xdc_Char)0x61,  /* [1876] */
    (xdc_Char)0x73,  /* [1877] */
    (xdc_Char)0x6b,  /* [1878] */
    (xdc_Char)0x2e,  /* [1879] */
    (xdc_Char)0x0,  /* [1880] */
    (xdc_Char)0x41,  /* [1881] */
    (xdc_Char)0x5f,  /* [1882] */
    (xdc_Char)0x74,  /* [1883] */
    (xdc_Char)0x61,  /* [1884] */
    (xdc_Char)0x73,  /* [1885] */
    (xdc_Char)0x6b,  /* [1886] */
    (xdc_Char)0x44,  /* [1887] */
    (xdc_Char)0x69,  /* [1888] */
    (xdc_Char)0x73,  /* [1889] */
    (xdc_Char)0x61,  /* [1890] */
    (xdc_Char)0x62,  /* [1891] */
    (xdc_Char)0x6c,  /* [1892] */
    (xdc_Char)0x65,  /* [1893] */
    (xdc_Char)0x64,  /* [1894] */
    (xdc_Char)0x3a,  /* [1895] */
    (xdc_Char)0x20,  /* [1896] */
    (xdc_Char)0x43,  /* [1897] */
    (xdc_Char)0x61,  /* [1898] */
    (xdc_Char)0x6e,  /* [1899] */
    (xdc_Char)0x6e,  /* [1900] */
    (xdc_Char)0x6f,  /* [1901] */
    (xdc_Char)0x74,  /* [1902] */
    (xdc_Char)0x20,  /* [1903] */
    (xdc_Char)0x63,  /* [1904] */
    (xdc_Char)0x72,  /* [1905] */
    (xdc_Char)0x65,  /* [1906] */
    (xdc_Char)0x61,  /* [1907] */
    (xdc_Char)0x74,  /* [1908] */
    (xdc_Char)0x65,  /* [1909] */
    (xdc_Char)0x20,  /* [1910] */
    (xdc_Char)0x61,  /* [1911] */
    (xdc_Char)0x20,  /* [1912] */
    (xdc_Char)0x74,  /* [1913] */
    (xdc_Char)0x61,  /* [1914] */
    (xdc_Char)0x73,  /* [1915] */
    (xdc_Char)0x6b,  /* [1916] */
    (xdc_Char)0x20,  /* [1917] */
    (xdc_Char)0x77,  /* [1918] */
    (xdc_Char)0x68,  /* [1919] */
    (xdc_Char)0x65,  /* [1920] */
    (xdc_Char)0x6e,  /* [1921] */
    (xdc_Char)0x20,  /* [1922] */
    (xdc_Char)0x74,  /* [1923] */
    (xdc_Char)0x61,  /* [1924] */
    (xdc_Char)0x73,  /* [1925] */
    (xdc_Char)0x6b,  /* [1926] */
    (xdc_Char)0x69,  /* [1927] */
    (xdc_Char)0x6e,  /* [1928] */
    (xdc_Char)0x67,  /* [1929] */
    (xdc_Char)0x20,  /* [1930] */
    (xdc_Char)0x69,  /* [1931] */
    (xdc_Char)0x73,  /* [1932] */
    (xdc_Char)0x20,  /* [1933] */
    (xdc_Char)0x64,  /* [1934] */
    (xdc_Char)0x69,  /* [1935] */
    (xdc_Char)0x73,  /* [1936] */
    (xdc_Char)0x61,  /* [1937] */
    (xdc_Char)0x62,  /* [1938] */
    (xdc_Char)0x6c,  /* [1939] */
    (xdc_Char)0x65,  /* [1940] */
    (xdc_Char)0x64,  /* [1941] */
    (xdc_Char)0x2e,  /* [1942] */
    (xdc_Char)0x0,  /* [1943] */
    (xdc_Char)0x41,  /* [1944] */
    (xdc_Char)0x5f,  /* [1945] */
    (xdc_Char)0x62,  /* [1946] */
    (xdc_Char)0x61,  /* [1947] */
    (xdc_Char)0x64,  /* [1948] */
    (xdc_Char)0x50,  /* [1949] */
    (xdc_Char)0x72,  /* [1950] */
    (xdc_Char)0x69,  /* [1951] */
    (xdc_Char)0x6f,  /* [1952] */
    (xdc_Char)0x72,  /* [1953] */
    (xdc_Char)0x69,  /* [1954] */
    (xdc_Char)0x74,  /* [1955] */
    (xdc_Char)0x79,  /* [1956] */
    (xdc_Char)0x3a,  /* [1957] */
    (xdc_Char)0x20,  /* [1958] */
    (xdc_Char)0x41,  /* [1959] */
    (xdc_Char)0x6e,  /* [1960] */
    (xdc_Char)0x20,  /* [1961] */
    (xdc_Char)0x69,  /* [1962] */
    (xdc_Char)0x6e,  /* [1963] */
    (xdc_Char)0x76,  /* [1964] */
    (xdc_Char)0x61,  /* [1965] */
    (xdc_Char)0x6c,  /* [1966] */
    (xdc_Char)0x69,  /* [1967] */
    (xdc_Char)0x64,  /* [1968] */
    (xdc_Char)0x20,  /* [1969] */
    (xdc_Char)0x74,  /* [1970] */
    (xdc_Char)0x61,  /* [1971] */
    (xdc_Char)0x73,  /* [1972] */
    (xdc_Char)0x6b,  /* [1973] */
    (xdc_Char)0x20,  /* [1974] */
    (xdc_Char)0x70,  /* [1975] */
    (xdc_Char)0x72,  /* [1976] */
    (xdc_Char)0x69,  /* [1977] */
    (xdc_Char)0x6f,  /* [1978] */
    (xdc_Char)0x72,  /* [1979] */
    (xdc_Char)0x69,  /* [1980] */
    (xdc_Char)0x74,  /* [1981] */
    (xdc_Char)0x79,  /* [1982] */
    (xdc_Char)0x20,  /* [1983] */
    (xdc_Char)0x77,  /* [1984] */
    (xdc_Char)0x61,  /* [1985] */
    (xdc_Char)0x73,  /* [1986] */
    (xdc_Char)0x20,  /* [1987] */
    (xdc_Char)0x75,  /* [1988] */
    (xdc_Char)0x73,  /* [1989] */
    (xdc_Char)0x65,  /* [1990] */
    (xdc_Char)0x64,  /* [1991] */
    (xdc_Char)0x2e,  /* [1992] */
    (xdc_Char)0x0,  /* [1993] */
    (xdc_Char)0x41,  /* [1994] */
    (xdc_Char)0x5f,  /* [1995] */
    (xdc_Char)0x62,  /* [1996] */
    (xdc_Char)0x61,  /* [1997] */
    (xdc_Char)0x64,  /* [1998] */
    (xdc_Char)0x54,  /* [1999] */
    (xdc_Char)0x69,  /* [2000] */
    (xdc_Char)0x6d,  /* [2001] */
    (xdc_Char)0x65,  /* [2002] */
    (xdc_Char)0x6f,  /* [2003] */
    (xdc_Char)0x75,  /* [2004] */
    (xdc_Char)0x74,  /* [2005] */
    (xdc_Char)0x3a,  /* [2006] */
    (xdc_Char)0x20,  /* [2007] */
    (xdc_Char)0x43,  /* [2008] */
    (xdc_Char)0x61,  /* [2009] */
    (xdc_Char)0x6e,  /* [2010] */
    (xdc_Char)0x27,  /* [2011] */
    (xdc_Char)0x74,  /* [2012] */
    (xdc_Char)0x20,  /* [2013] */
    (xdc_Char)0x73,  /* [2014] */
    (xdc_Char)0x6c,  /* [2015] */
    (xdc_Char)0x65,  /* [2016] */
    (xdc_Char)0x65,  /* [2017] */
    (xdc_Char)0x70,  /* [2018] */
    (xdc_Char)0x20,  /* [2019] */
    (xdc_Char)0x46,  /* [2020] */
    (xdc_Char)0x4f,  /* [2021] */
    (xdc_Char)0x52,  /* [2022] */
    (xdc_Char)0x45,  /* [2023] */
    (xdc_Char)0x56,  /* [2024] */
    (xdc_Char)0x45,  /* [2025] */
    (xdc_Char)0x52,  /* [2026] */
    (xdc_Char)0x2e,  /* [2027] */
    (xdc_Char)0x0,  /* [2028] */
    (xdc_Char)0x41,  /* [2029] */
    (xdc_Char)0x5f,  /* [2030] */
    (xdc_Char)0x62,  /* [2031] */
    (xdc_Char)0x61,  /* [2032] */
    (xdc_Char)0x64,  /* [2033] */
    (xdc_Char)0x41,  /* [2034] */
    (xdc_Char)0x66,  /* [2035] */
    (xdc_Char)0x66,  /* [2036] */
    (xdc_Char)0x69,  /* [2037] */
    (xdc_Char)0x6e,  /* [2038] */
    (xdc_Char)0x69,  /* [2039] */
    (xdc_Char)0x74,  /* [2040] */
    (xdc_Char)0x79,  /* [2041] */
    (xdc_Char)0x3a,  /* [2042] */
    (xdc_Char)0x20,  /* [2043] */
    (xdc_Char)0x49,  /* [2044] */
    (xdc_Char)0x6e,  /* [2045] */
    (xdc_Char)0x76,  /* [2046] */
    (xdc_Char)0x61,  /* [2047] */
    (xdc_Char)0x6c,  /* [2048] */
    (xdc_Char)0x69,  /* [2049] */
    (xdc_Char)0x64,  /* [2050] */
    (xdc_Char)0x20,  /* [2051] */
    (xdc_Char)0x61,  /* [2052] */
    (xdc_Char)0x66,  /* [2053] */
    (xdc_Char)0x66,  /* [2054] */
    (xdc_Char)0x69,  /* [2055] */
    (xdc_Char)0x6e,  /* [2056] */
    (xdc_Char)0x69,  /* [2057] */
    (xdc_Char)0x74,  /* [2058] */
    (xdc_Char)0x79,  /* [2059] */
    (xdc_Char)0x2e,  /* [2060] */
    (xdc_Char)0x0,  /* [2061] */
    (xdc_Char)0x41,  /* [2062] */
    (xdc_Char)0x5f,  /* [2063] */
    (xdc_Char)0x73,  /* [2064] */
    (xdc_Char)0x6c,  /* [2065] */
    (xdc_Char)0x65,  /* [2066] */
    (xdc_Char)0x65,  /* [2067] */
    (xdc_Char)0x70,  /* [2068] */
    (xdc_Char)0x54,  /* [2069] */
    (xdc_Char)0x61,  /* [2070] */
    (xdc_Char)0x73,  /* [2071] */
    (xdc_Char)0x6b,  /* [2072] */
    (xdc_Char)0x44,  /* [2073] */
    (xdc_Char)0x69,  /* [2074] */
    (xdc_Char)0x73,  /* [2075] */
    (xdc_Char)0x61,  /* [2076] */
    (xdc_Char)0x62,  /* [2077] */
    (xdc_Char)0x6c,  /* [2078] */
    (xdc_Char)0x65,  /* [2079] */
    (xdc_Char)0x64,  /* [2080] */
    (xdc_Char)0x3a,  /* [2081] */
    (xdc_Char)0x20,  /* [2082] */
    (xdc_Char)0x43,  /* [2083] */
    (xdc_Char)0x61,  /* [2084] */
    (xdc_Char)0x6e,  /* [2085] */
    (xdc_Char)0x6e,  /* [2086] */
    (xdc_Char)0x6f,  /* [2087] */
    (xdc_Char)0x74,  /* [2088] */
    (xdc_Char)0x20,  /* [2089] */
    (xdc_Char)0x63,  /* [2090] */
    (xdc_Char)0x61,  /* [2091] */
    (xdc_Char)0x6c,  /* [2092] */
    (xdc_Char)0x6c,  /* [2093] */
    (xdc_Char)0x20,  /* [2094] */
    (xdc_Char)0x54,  /* [2095] */
    (xdc_Char)0x61,  /* [2096] */
    (xdc_Char)0x73,  /* [2097] */
    (xdc_Char)0x6b,  /* [2098] */
    (xdc_Char)0x5f,  /* [2099] */
    (xdc_Char)0x73,  /* [2100] */
    (xdc_Char)0x6c,  /* [2101] */
    (xdc_Char)0x65,  /* [2102] */
    (xdc_Char)0x65,  /* [2103] */
    (xdc_Char)0x70,  /* [2104] */
    (xdc_Char)0x28,  /* [2105] */
    (xdc_Char)0x29,  /* [2106] */
    (xdc_Char)0x20,  /* [2107] */
    (xdc_Char)0x77,  /* [2108] */
    (xdc_Char)0x68,  /* [2109] */
    (xdc_Char)0x69,  /* [2110] */
    (xdc_Char)0x6c,  /* [2111] */
    (xdc_Char)0x65,  /* [2112] */
    (xdc_Char)0x20,  /* [2113] */
    (xdc_Char)0x74,  /* [2114] */
    (xdc_Char)0x68,  /* [2115] */
    (xdc_Char)0x65,  /* [2116] */
    (xdc_Char)0x20,  /* [2117] */
    (xdc_Char)0x54,  /* [2118] */
    (xdc_Char)0x61,  /* [2119] */
    (xdc_Char)0x73,  /* [2120] */
    (xdc_Char)0x6b,  /* [2121] */
    (xdc_Char)0x20,  /* [2122] */
    (xdc_Char)0x73,  /* [2123] */
    (xdc_Char)0x63,  /* [2124] */
    (xdc_Char)0x68,  /* [2125] */
    (xdc_Char)0x65,  /* [2126] */
    (xdc_Char)0x64,  /* [2127] */
    (xdc_Char)0x75,  /* [2128] */
    (xdc_Char)0x6c,  /* [2129] */
    (xdc_Char)0x65,  /* [2130] */
    (xdc_Char)0x72,  /* [2131] */
    (xdc_Char)0x20,  /* [2132] */
    (xdc_Char)0x69,  /* [2133] */
    (xdc_Char)0x73,  /* [2134] */
    (xdc_Char)0x20,  /* [2135] */
    (xdc_Char)0x64,  /* [2136] */
    (xdc_Char)0x69,  /* [2137] */
    (xdc_Char)0x73,  /* [2138] */
    (xdc_Char)0x61,  /* [2139] */
    (xdc_Char)0x62,  /* [2140] */
    (xdc_Char)0x6c,  /* [2141] */
    (xdc_Char)0x65,  /* [2142] */
    (xdc_Char)0x64,  /* [2143] */
    (xdc_Char)0x2e,  /* [2144] */
    (xdc_Char)0x0,  /* [2145] */
    (xdc_Char)0x41,  /* [2146] */
    (xdc_Char)0x5f,  /* [2147] */
    (xdc_Char)0x69,  /* [2148] */
    (xdc_Char)0x6e,  /* [2149] */
    (xdc_Char)0x76,  /* [2150] */
    (xdc_Char)0x61,  /* [2151] */
    (xdc_Char)0x6c,  /* [2152] */
    (xdc_Char)0x69,  /* [2153] */
    (xdc_Char)0x64,  /* [2154] */
    (xdc_Char)0x43,  /* [2155] */
    (xdc_Char)0x6f,  /* [2156] */
    (xdc_Char)0x72,  /* [2157] */
    (xdc_Char)0x65,  /* [2158] */
    (xdc_Char)0x49,  /* [2159] */
    (xdc_Char)0x64,  /* [2160] */
    (xdc_Char)0x3a,  /* [2161] */
    (xdc_Char)0x20,  /* [2162] */
    (xdc_Char)0x43,  /* [2163] */
    (xdc_Char)0x61,  /* [2164] */
    (xdc_Char)0x6e,  /* [2165] */
    (xdc_Char)0x6e,  /* [2166] */
    (xdc_Char)0x6f,  /* [2167] */
    (xdc_Char)0x74,  /* [2168] */
    (xdc_Char)0x20,  /* [2169] */
    (xdc_Char)0x70,  /* [2170] */
    (xdc_Char)0x61,  /* [2171] */
    (xdc_Char)0x73,  /* [2172] */
    (xdc_Char)0x73,  /* [2173] */
    (xdc_Char)0x20,  /* [2174] */
    (xdc_Char)0x61,  /* [2175] */
    (xdc_Char)0x20,  /* [2176] */
    (xdc_Char)0x6e,  /* [2177] */
    (xdc_Char)0x6f,  /* [2178] */
    (xdc_Char)0x6e,  /* [2179] */
    (xdc_Char)0x2d,  /* [2180] */
    (xdc_Char)0x7a,  /* [2181] */
    (xdc_Char)0x65,  /* [2182] */
    (xdc_Char)0x72,  /* [2183] */
    (xdc_Char)0x6f,  /* [2184] */
    (xdc_Char)0x20,  /* [2185] */
    (xdc_Char)0x43,  /* [2186] */
    (xdc_Char)0x6f,  /* [2187] */
    (xdc_Char)0x72,  /* [2188] */
    (xdc_Char)0x65,  /* [2189] */
    (xdc_Char)0x49,  /* [2190] */
    (xdc_Char)0x64,  /* [2191] */
    (xdc_Char)0x20,  /* [2192] */
    (xdc_Char)0x69,  /* [2193] */
    (xdc_Char)0x6e,  /* [2194] */
    (xdc_Char)0x20,  /* [2195] */
    (xdc_Char)0x61,  /* [2196] */
    (xdc_Char)0x20,  /* [2197] */
    (xdc_Char)0x6e,  /* [2198] */
    (xdc_Char)0x6f,  /* [2199] */
    (xdc_Char)0x6e,  /* [2200] */
    (xdc_Char)0x2d,  /* [2201] */
    (xdc_Char)0x53,  /* [2202] */
    (xdc_Char)0x4d,  /* [2203] */
    (xdc_Char)0x50,  /* [2204] */
    (xdc_Char)0x20,  /* [2205] */
    (xdc_Char)0x61,  /* [2206] */
    (xdc_Char)0x70,  /* [2207] */
    (xdc_Char)0x70,  /* [2208] */
    (xdc_Char)0x6c,  /* [2209] */
    (xdc_Char)0x69,  /* [2210] */
    (xdc_Char)0x63,  /* [2211] */
    (xdc_Char)0x61,  /* [2212] */
    (xdc_Char)0x74,  /* [2213] */
    (xdc_Char)0x69,  /* [2214] */
    (xdc_Char)0x6f,  /* [2215] */
    (xdc_Char)0x6e,  /* [2216] */
    (xdc_Char)0x2e,  /* [2217] */
    (xdc_Char)0x0,  /* [2218] */
    (xdc_Char)0x41,  /* [2219] */
    (xdc_Char)0x5f,  /* [2220] */
    (xdc_Char)0x7a,  /* [2221] */
    (xdc_Char)0x65,  /* [2222] */
    (xdc_Char)0x72,  /* [2223] */
    (xdc_Char)0x6f,  /* [2224] */
    (xdc_Char)0x54,  /* [2225] */
    (xdc_Char)0x69,  /* [2226] */
    (xdc_Char)0x6d,  /* [2227] */
    (xdc_Char)0x65,  /* [2228] */
    (xdc_Char)0x6f,  /* [2229] */
    (xdc_Char)0x75,  /* [2230] */
    (xdc_Char)0x74,  /* [2231] */
    (xdc_Char)0x3a,  /* [2232] */
    (xdc_Char)0x20,  /* [2233] */
    (xdc_Char)0x54,  /* [2234] */
    (xdc_Char)0x69,  /* [2235] */
    (xdc_Char)0x6d,  /* [2236] */
    (xdc_Char)0x65,  /* [2237] */
    (xdc_Char)0x6f,  /* [2238] */
    (xdc_Char)0x75,  /* [2239] */
    (xdc_Char)0x74,  /* [2240] */
    (xdc_Char)0x20,  /* [2241] */
    (xdc_Char)0x76,  /* [2242] */
    (xdc_Char)0x61,  /* [2243] */
    (xdc_Char)0x6c,  /* [2244] */
    (xdc_Char)0x75,  /* [2245] */
    (xdc_Char)0x65,  /* [2246] */
    (xdc_Char)0x20,  /* [2247] */
    (xdc_Char)0x61,  /* [2248] */
    (xdc_Char)0x6e,  /* [2249] */
    (xdc_Char)0x6e,  /* [2250] */
    (xdc_Char)0x6f,  /* [2251] */
    (xdc_Char)0x74,  /* [2252] */
    (xdc_Char)0x20,  /* [2253] */
    (xdc_Char)0x62,  /* [2254] */
    (xdc_Char)0x65,  /* [2255] */
    (xdc_Char)0x20,  /* [2256] */
    (xdc_Char)0x7a,  /* [2257] */
    (xdc_Char)0x65,  /* [2258] */
    (xdc_Char)0x72,  /* [2259] */
    (xdc_Char)0x6f,  /* [2260] */
    (xdc_Char)0x0,  /* [2261] */
    (xdc_Char)0x41,  /* [2262] */
    (xdc_Char)0x5f,  /* [2263] */
    (xdc_Char)0x69,  /* [2264] */
    (xdc_Char)0x6e,  /* [2265] */
    (xdc_Char)0x76,  /* [2266] */
    (xdc_Char)0x61,  /* [2267] */
    (xdc_Char)0x6c,  /* [2268] */
    (xdc_Char)0x69,  /* [2269] */
    (xdc_Char)0x64,  /* [2270] */
    (xdc_Char)0x4b,  /* [2271] */
    (xdc_Char)0x65,  /* [2272] */
    (xdc_Char)0x79,  /* [2273] */
    (xdc_Char)0x3a,  /* [2274] */
    (xdc_Char)0x20,  /* [2275] */
    (xdc_Char)0x74,  /* [2276] */
    (xdc_Char)0x68,  /* [2277] */
    (xdc_Char)0x65,  /* [2278] */
    (xdc_Char)0x20,  /* [2279] */
    (xdc_Char)0x6b,  /* [2280] */
    (xdc_Char)0x65,  /* [2281] */
    (xdc_Char)0x79,  /* [2282] */
    (xdc_Char)0x20,  /* [2283] */
    (xdc_Char)0x6d,  /* [2284] */
    (xdc_Char)0x75,  /* [2285] */
    (xdc_Char)0x73,  /* [2286] */
    (xdc_Char)0x74,  /* [2287] */
    (xdc_Char)0x20,  /* [2288] */
    (xdc_Char)0x62,  /* [2289] */
    (xdc_Char)0x65,  /* [2290] */
    (xdc_Char)0x20,  /* [2291] */
    (xdc_Char)0x73,  /* [2292] */
    (xdc_Char)0x65,  /* [2293] */
    (xdc_Char)0x74,  /* [2294] */
    (xdc_Char)0x20,  /* [2295] */
    (xdc_Char)0x74,  /* [2296] */
    (xdc_Char)0x6f,  /* [2297] */
    (xdc_Char)0x20,  /* [2298] */
    (xdc_Char)0x61,  /* [2299] */
    (xdc_Char)0x20,  /* [2300] */
    (xdc_Char)0x6e,  /* [2301] */
    (xdc_Char)0x6f,  /* [2302] */
    (xdc_Char)0x6e,  /* [2303] */
    (xdc_Char)0x2d,  /* [2304] */
    (xdc_Char)0x64,  /* [2305] */
    (xdc_Char)0x65,  /* [2306] */
    (xdc_Char)0x66,  /* [2307] */
    (xdc_Char)0x61,  /* [2308] */
    (xdc_Char)0x75,  /* [2309] */
    (xdc_Char)0x6c,  /* [2310] */
    (xdc_Char)0x74,  /* [2311] */
    (xdc_Char)0x20,  /* [2312] */
    (xdc_Char)0x76,  /* [2313] */
    (xdc_Char)0x61,  /* [2314] */
    (xdc_Char)0x6c,  /* [2315] */
    (xdc_Char)0x75,  /* [2316] */
    (xdc_Char)0x65,  /* [2317] */
    (xdc_Char)0x0,  /* [2318] */
    (xdc_Char)0x62,  /* [2319] */
    (xdc_Char)0x75,  /* [2320] */
    (xdc_Char)0x66,  /* [2321] */
    (xdc_Char)0x20,  /* [2322] */
    (xdc_Char)0x70,  /* [2323] */
    (xdc_Char)0x61,  /* [2324] */
    (xdc_Char)0x72,  /* [2325] */
    (xdc_Char)0x61,  /* [2326] */
    (xdc_Char)0x6d,  /* [2327] */
    (xdc_Char)0x65,  /* [2328] */
    (xdc_Char)0x74,  /* [2329] */
    (xdc_Char)0x65,  /* [2330] */
    (xdc_Char)0x72,  /* [2331] */
    (xdc_Char)0x20,  /* [2332] */
    (xdc_Char)0x63,  /* [2333] */
    (xdc_Char)0x61,  /* [2334] */
    (xdc_Char)0x6e,  /* [2335] */
    (xdc_Char)0x6e,  /* [2336] */
    (xdc_Char)0x6f,  /* [2337] */
    (xdc_Char)0x74,  /* [2338] */
    (xdc_Char)0x20,  /* [2339] */
    (xdc_Char)0x62,  /* [2340] */
    (xdc_Char)0x65,  /* [2341] */
    (xdc_Char)0x20,  /* [2342] */
    (xdc_Char)0x6e,  /* [2343] */
    (xdc_Char)0x75,  /* [2344] */
    (xdc_Char)0x6c,  /* [2345] */
    (xdc_Char)0x6c,  /* [2346] */
    (xdc_Char)0x0,  /* [2347] */
    (xdc_Char)0x62,  /* [2348] */
    (xdc_Char)0x75,  /* [2349] */
    (xdc_Char)0x66,  /* [2350] */
    (xdc_Char)0x20,  /* [2351] */
    (xdc_Char)0x6e,  /* [2352] */
    (xdc_Char)0x6f,  /* [2353] */
    (xdc_Char)0x74,  /* [2354] */
    (xdc_Char)0x20,  /* [2355] */
    (xdc_Char)0x70,  /* [2356] */
    (xdc_Char)0x72,  /* [2357] */
    (xdc_Char)0x6f,  /* [2358] */
    (xdc_Char)0x70,  /* [2359] */
    (xdc_Char)0x65,  /* [2360] */
    (xdc_Char)0x72,  /* [2361] */
    (xdc_Char)0x6c,  /* [2362] */
    (xdc_Char)0x79,  /* [2363] */
    (xdc_Char)0x20,  /* [2364] */
    (xdc_Char)0x61,  /* [2365] */
    (xdc_Char)0x6c,  /* [2366] */
    (xdc_Char)0x69,  /* [2367] */
    (xdc_Char)0x67,  /* [2368] */
    (xdc_Char)0x6e,  /* [2369] */
    (xdc_Char)0x65,  /* [2370] */
    (xdc_Char)0x64,  /* [2371] */
    (xdc_Char)0x0,  /* [2372] */
    (xdc_Char)0x61,  /* [2373] */
    (xdc_Char)0x6c,  /* [2374] */
    (xdc_Char)0x69,  /* [2375] */
    (xdc_Char)0x67,  /* [2376] */
    (xdc_Char)0x6e,  /* [2377] */
    (xdc_Char)0x20,  /* [2378] */
    (xdc_Char)0x70,  /* [2379] */
    (xdc_Char)0x61,  /* [2380] */
    (xdc_Char)0x72,  /* [2381] */
    (xdc_Char)0x61,  /* [2382] */
    (xdc_Char)0x6d,  /* [2383] */
    (xdc_Char)0x65,  /* [2384] */
    (xdc_Char)0x74,  /* [2385] */
    (xdc_Char)0x65,  /* [2386] */
    (xdc_Char)0x72,  /* [2387] */
    (xdc_Char)0x20,  /* [2388] */
    (xdc_Char)0x6d,  /* [2389] */
    (xdc_Char)0x75,  /* [2390] */
    (xdc_Char)0x73,  /* [2391] */
    (xdc_Char)0x74,  /* [2392] */
    (xdc_Char)0x20,  /* [2393] */
    (xdc_Char)0x62,  /* [2394] */
    (xdc_Char)0x65,  /* [2395] */
    (xdc_Char)0x20,  /* [2396] */
    (xdc_Char)0x30,  /* [2397] */
    (xdc_Char)0x20,  /* [2398] */
    (xdc_Char)0x6f,  /* [2399] */
    (xdc_Char)0x72,  /* [2400] */
    (xdc_Char)0x20,  /* [2401] */
    (xdc_Char)0x61,  /* [2402] */
    (xdc_Char)0x20,  /* [2403] */
    (xdc_Char)0x70,  /* [2404] */
    (xdc_Char)0x6f,  /* [2405] */
    (xdc_Char)0x77,  /* [2406] */
    (xdc_Char)0x65,  /* [2407] */
    (xdc_Char)0x72,  /* [2408] */
    (xdc_Char)0x20,  /* [2409] */
    (xdc_Char)0x6f,  /* [2410] */
    (xdc_Char)0x66,  /* [2411] */
    (xdc_Char)0x20,  /* [2412] */
    (xdc_Char)0x32,  /* [2413] */
    (xdc_Char)0x20,  /* [2414] */
    (xdc_Char)0x3e,  /* [2415] */
    (xdc_Char)0x3d,  /* [2416] */
    (xdc_Char)0x20,  /* [2417] */
    (xdc_Char)0x74,  /* [2418] */
    (xdc_Char)0x68,  /* [2419] */
    (xdc_Char)0x65,  /* [2420] */
    (xdc_Char)0x20,  /* [2421] */
    (xdc_Char)0x76,  /* [2422] */
    (xdc_Char)0x61,  /* [2423] */
    (xdc_Char)0x6c,  /* [2424] */
    (xdc_Char)0x75,  /* [2425] */
    (xdc_Char)0x65,  /* [2426] */
    (xdc_Char)0x20,  /* [2427] */
    (xdc_Char)0x6f,  /* [2428] */
    (xdc_Char)0x66,  /* [2429] */
    (xdc_Char)0x20,  /* [2430] */
    (xdc_Char)0x4d,  /* [2431] */
    (xdc_Char)0x65,  /* [2432] */
    (xdc_Char)0x6d,  /* [2433] */
    (xdc_Char)0x6f,  /* [2434] */
    (xdc_Char)0x72,  /* [2435] */
    (xdc_Char)0x79,  /* [2436] */
    (xdc_Char)0x5f,  /* [2437] */
    (xdc_Char)0x67,  /* [2438] */
    (xdc_Char)0x65,  /* [2439] */
    (xdc_Char)0x74,  /* [2440] */
    (xdc_Char)0x4d,  /* [2441] */
    (xdc_Char)0x61,  /* [2442] */
    (xdc_Char)0x78,  /* [2443] */
    (xdc_Char)0x44,  /* [2444] */
    (xdc_Char)0x65,  /* [2445] */
    (xdc_Char)0x66,  /* [2446] */
    (xdc_Char)0x61,  /* [2447] */
    (xdc_Char)0x75,  /* [2448] */
    (xdc_Char)0x6c,  /* [2449] */
    (xdc_Char)0x74,  /* [2450] */
    (xdc_Char)0x54,  /* [2451] */
    (xdc_Char)0x79,  /* [2452] */
    (xdc_Char)0x70,  /* [2453] */
    (xdc_Char)0x65,  /* [2454] */
    (xdc_Char)0x41,  /* [2455] */
    (xdc_Char)0x6c,  /* [2456] */
    (xdc_Char)0x69,  /* [2457] */
    (xdc_Char)0x67,  /* [2458] */
    (xdc_Char)0x6e,  /* [2459] */
    (xdc_Char)0x28,  /* [2460] */
    (xdc_Char)0x29,  /* [2461] */
    (xdc_Char)0x0,  /* [2462] */
    (xdc_Char)0x61,  /* [2463] */
    (xdc_Char)0x6c,  /* [2464] */
    (xdc_Char)0x69,  /* [2465] */
    (xdc_Char)0x67,  /* [2466] */
    (xdc_Char)0x6e,  /* [2467] */
    (xdc_Char)0x20,  /* [2468] */
    (xdc_Char)0x70,  /* [2469] */
    (xdc_Char)0x61,  /* [2470] */
    (xdc_Char)0x72,  /* [2471] */
    (xdc_Char)0x61,  /* [2472] */
    (xdc_Char)0x6d,  /* [2473] */
    (xdc_Char)0x65,  /* [2474] */
    (xdc_Char)0x74,  /* [2475] */
    (xdc_Char)0x65,  /* [2476] */
    (xdc_Char)0x72,  /* [2477] */
    (xdc_Char)0x20,  /* [2478] */
    (xdc_Char)0x31,  /* [2479] */
    (xdc_Char)0x29,  /* [2480] */
    (xdc_Char)0x20,  /* [2481] */
    (xdc_Char)0x6d,  /* [2482] */
    (xdc_Char)0x75,  /* [2483] */
    (xdc_Char)0x73,  /* [2484] */
    (xdc_Char)0x74,  /* [2485] */
    (xdc_Char)0x20,  /* [2486] */
    (xdc_Char)0x62,  /* [2487] */
    (xdc_Char)0x65,  /* [2488] */
    (xdc_Char)0x20,  /* [2489] */
    (xdc_Char)0x30,  /* [2490] */
    (xdc_Char)0x20,  /* [2491] */
    (xdc_Char)0x6f,  /* [2492] */
    (xdc_Char)0x72,  /* [2493] */
    (xdc_Char)0x20,  /* [2494] */
    (xdc_Char)0x61,  /* [2495] */
    (xdc_Char)0x20,  /* [2496] */
    (xdc_Char)0x70,  /* [2497] */
    (xdc_Char)0x6f,  /* [2498] */
    (xdc_Char)0x77,  /* [2499] */
    (xdc_Char)0x65,  /* [2500] */
    (xdc_Char)0x72,  /* [2501] */
    (xdc_Char)0x20,  /* [2502] */
    (xdc_Char)0x6f,  /* [2503] */
    (xdc_Char)0x66,  /* [2504] */
    (xdc_Char)0x20,  /* [2505] */
    (xdc_Char)0x32,  /* [2506] */
    (xdc_Char)0x20,  /* [2507] */
    (xdc_Char)0x61,  /* [2508] */
    (xdc_Char)0x6e,  /* [2509] */
    (xdc_Char)0x64,  /* [2510] */
    (xdc_Char)0x20,  /* [2511] */
    (xdc_Char)0x32,  /* [2512] */
    (xdc_Char)0x29,  /* [2513] */
    (xdc_Char)0x20,  /* [2514] */
    (xdc_Char)0x6e,  /* [2515] */
    (xdc_Char)0x6f,  /* [2516] */
    (xdc_Char)0x74,  /* [2517] */
    (xdc_Char)0x20,  /* [2518] */
    (xdc_Char)0x67,  /* [2519] */
    (xdc_Char)0x72,  /* [2520] */
    (xdc_Char)0x65,  /* [2521] */
    (xdc_Char)0x61,  /* [2522] */
    (xdc_Char)0x74,  /* [2523] */
    (xdc_Char)0x65,  /* [2524] */
    (xdc_Char)0x72,  /* [2525] */
    (xdc_Char)0x20,  /* [2526] */
    (xdc_Char)0x74,  /* [2527] */
    (xdc_Char)0x68,  /* [2528] */
    (xdc_Char)0x61,  /* [2529] */
    (xdc_Char)0x6e,  /* [2530] */
    (xdc_Char)0x20,  /* [2531] */
    (xdc_Char)0x74,  /* [2532] */
    (xdc_Char)0x68,  /* [2533] */
    (xdc_Char)0x65,  /* [2534] */
    (xdc_Char)0x20,  /* [2535] */
    (xdc_Char)0x68,  /* [2536] */
    (xdc_Char)0x65,  /* [2537] */
    (xdc_Char)0x61,  /* [2538] */
    (xdc_Char)0x70,  /* [2539] */
    (xdc_Char)0x73,  /* [2540] */
    (xdc_Char)0x20,  /* [2541] */
    (xdc_Char)0x61,  /* [2542] */
    (xdc_Char)0x6c,  /* [2543] */
    (xdc_Char)0x69,  /* [2544] */
    (xdc_Char)0x67,  /* [2545] */
    (xdc_Char)0x6e,  /* [2546] */
    (xdc_Char)0x6d,  /* [2547] */
    (xdc_Char)0x65,  /* [2548] */
    (xdc_Char)0x6e,  /* [2549] */
    (xdc_Char)0x74,  /* [2550] */
    (xdc_Char)0x0,  /* [2551] */
    (xdc_Char)0x62,  /* [2552] */
    (xdc_Char)0x6c,  /* [2553] */
    (xdc_Char)0x6f,  /* [2554] */
    (xdc_Char)0x63,  /* [2555] */
    (xdc_Char)0x6b,  /* [2556] */
    (xdc_Char)0x53,  /* [2557] */
    (xdc_Char)0x69,  /* [2558] */
    (xdc_Char)0x7a,  /* [2559] */
    (xdc_Char)0x65,  /* [2560] */
    (xdc_Char)0x20,  /* [2561] */
    (xdc_Char)0x6d,  /* [2562] */
    (xdc_Char)0x75,  /* [2563] */
    (xdc_Char)0x73,  /* [2564] */
    (xdc_Char)0x74,  /* [2565] */
    (xdc_Char)0x20,  /* [2566] */
    (xdc_Char)0x62,  /* [2567] */
    (xdc_Char)0x65,  /* [2568] */
    (xdc_Char)0x20,  /* [2569] */
    (xdc_Char)0x6c,  /* [2570] */
    (xdc_Char)0x61,  /* [2571] */
    (xdc_Char)0x72,  /* [2572] */
    (xdc_Char)0x67,  /* [2573] */
    (xdc_Char)0x65,  /* [2574] */
    (xdc_Char)0x20,  /* [2575] */
    (xdc_Char)0x65,  /* [2576] */
    (xdc_Char)0x6e,  /* [2577] */
    (xdc_Char)0x6f,  /* [2578] */
    (xdc_Char)0x75,  /* [2579] */
    (xdc_Char)0x67,  /* [2580] */
    (xdc_Char)0x68,  /* [2581] */
    (xdc_Char)0x20,  /* [2582] */
    (xdc_Char)0x74,  /* [2583] */
    (xdc_Char)0x6f,  /* [2584] */
    (xdc_Char)0x20,  /* [2585] */
    (xdc_Char)0x68,  /* [2586] */
    (xdc_Char)0x6f,  /* [2587] */
    (xdc_Char)0x6c,  /* [2588] */
    (xdc_Char)0x64,  /* [2589] */
    (xdc_Char)0x20,  /* [2590] */
    (xdc_Char)0x61,  /* [2591] */
    (xdc_Char)0x74,  /* [2592] */
    (xdc_Char)0x6c,  /* [2593] */
    (xdc_Char)0x65,  /* [2594] */
    (xdc_Char)0x61,  /* [2595] */
    (xdc_Char)0x73,  /* [2596] */
    (xdc_Char)0x74,  /* [2597] */
    (xdc_Char)0x20,  /* [2598] */
    (xdc_Char)0x74,  /* [2599] */
    (xdc_Char)0x77,  /* [2600] */
    (xdc_Char)0x6f,  /* [2601] */
    (xdc_Char)0x20,  /* [2602] */
    (xdc_Char)0x70,  /* [2603] */
    (xdc_Char)0x6f,  /* [2604] */
    (xdc_Char)0x69,  /* [2605] */
    (xdc_Char)0x6e,  /* [2606] */
    (xdc_Char)0x74,  /* [2607] */
    (xdc_Char)0x65,  /* [2608] */
    (xdc_Char)0x72,  /* [2609] */
    (xdc_Char)0x73,  /* [2610] */
    (xdc_Char)0x0,  /* [2611] */
    (xdc_Char)0x6e,  /* [2612] */
    (xdc_Char)0x75,  /* [2613] */
    (xdc_Char)0x6d,  /* [2614] */
    (xdc_Char)0x42,  /* [2615] */
    (xdc_Char)0x6c,  /* [2616] */
    (xdc_Char)0x6f,  /* [2617] */
    (xdc_Char)0x63,  /* [2618] */
    (xdc_Char)0x6b,  /* [2619] */
    (xdc_Char)0x73,  /* [2620] */
    (xdc_Char)0x20,  /* [2621] */
    (xdc_Char)0x63,  /* [2622] */
    (xdc_Char)0x61,  /* [2623] */
    (xdc_Char)0x6e,  /* [2624] */
    (xdc_Char)0x6e,  /* [2625] */
    (xdc_Char)0x6f,  /* [2626] */
    (xdc_Char)0x74,  /* [2627] */
    (xdc_Char)0x20,  /* [2628] */
    (xdc_Char)0x62,  /* [2629] */
    (xdc_Char)0x65,  /* [2630] */
    (xdc_Char)0x20,  /* [2631] */
    (xdc_Char)0x7a,  /* [2632] */
    (xdc_Char)0x65,  /* [2633] */
    (xdc_Char)0x72,  /* [2634] */
    (xdc_Char)0x6f,  /* [2635] */
    (xdc_Char)0x0,  /* [2636] */
    (xdc_Char)0x62,  /* [2637] */
    (xdc_Char)0x75,  /* [2638] */
    (xdc_Char)0x66,  /* [2639] */
    (xdc_Char)0x53,  /* [2640] */
    (xdc_Char)0x69,  /* [2641] */
    (xdc_Char)0x7a,  /* [2642] */
    (xdc_Char)0x65,  /* [2643] */
    (xdc_Char)0x20,  /* [2644] */
    (xdc_Char)0x63,  /* [2645] */
    (xdc_Char)0x61,  /* [2646] */
    (xdc_Char)0x6e,  /* [2647] */
    (xdc_Char)0x6e,  /* [2648] */
    (xdc_Char)0x6f,  /* [2649] */
    (xdc_Char)0x74,  /* [2650] */
    (xdc_Char)0x20,  /* [2651] */
    (xdc_Char)0x62,  /* [2652] */
    (xdc_Char)0x65,  /* [2653] */
    (xdc_Char)0x20,  /* [2654] */
    (xdc_Char)0x7a,  /* [2655] */
    (xdc_Char)0x65,  /* [2656] */
    (xdc_Char)0x72,  /* [2657] */
    (xdc_Char)0x6f,  /* [2658] */
    (xdc_Char)0x0,  /* [2659] */
    (xdc_Char)0x48,  /* [2660] */
    (xdc_Char)0x65,  /* [2661] */
    (xdc_Char)0x61,  /* [2662] */
    (xdc_Char)0x70,  /* [2663] */
    (xdc_Char)0x42,  /* [2664] */
    (xdc_Char)0x75,  /* [2665] */
    (xdc_Char)0x66,  /* [2666] */
    (xdc_Char)0x5f,  /* [2667] */
    (xdc_Char)0x63,  /* [2668] */
    (xdc_Char)0x72,  /* [2669] */
    (xdc_Char)0x65,  /* [2670] */
    (xdc_Char)0x61,  /* [2671] */
    (xdc_Char)0x74,  /* [2672] */
    (xdc_Char)0x65,  /* [2673] */
    (xdc_Char)0x27,  /* [2674] */
    (xdc_Char)0x73,  /* [2675] */
    (xdc_Char)0x20,  /* [2676] */
    (xdc_Char)0x62,  /* [2677] */
    (xdc_Char)0x75,  /* [2678] */
    (xdc_Char)0x66,  /* [2679] */
    (xdc_Char)0x53,  /* [2680] */
    (xdc_Char)0x69,  /* [2681] */
    (xdc_Char)0x7a,  /* [2682] */
    (xdc_Char)0x65,  /* [2683] */
    (xdc_Char)0x20,  /* [2684] */
    (xdc_Char)0x70,  /* [2685] */
    (xdc_Char)0x61,  /* [2686] */
    (xdc_Char)0x72,  /* [2687] */
    (xdc_Char)0x61,  /* [2688] */
    (xdc_Char)0x6d,  /* [2689] */
    (xdc_Char)0x65,  /* [2690] */
    (xdc_Char)0x74,  /* [2691] */
    (xdc_Char)0x65,  /* [2692] */
    (xdc_Char)0x72,  /* [2693] */
    (xdc_Char)0x20,  /* [2694] */
    (xdc_Char)0x69,  /* [2695] */
    (xdc_Char)0x73,  /* [2696] */
    (xdc_Char)0x20,  /* [2697] */
    (xdc_Char)0x69,  /* [2698] */
    (xdc_Char)0x6e,  /* [2699] */
    (xdc_Char)0x76,  /* [2700] */
    (xdc_Char)0x61,  /* [2701] */
    (xdc_Char)0x6c,  /* [2702] */
    (xdc_Char)0x69,  /* [2703] */
    (xdc_Char)0x64,  /* [2704] */
    (xdc_Char)0x20,  /* [2705] */
    (xdc_Char)0x28,  /* [2706] */
    (xdc_Char)0x74,  /* [2707] */
    (xdc_Char)0x6f,  /* [2708] */
    (xdc_Char)0x6f,  /* [2709] */
    (xdc_Char)0x20,  /* [2710] */
    (xdc_Char)0x73,  /* [2711] */
    (xdc_Char)0x6d,  /* [2712] */
    (xdc_Char)0x61,  /* [2713] */
    (xdc_Char)0x6c,  /* [2714] */
    (xdc_Char)0x6c,  /* [2715] */
    (xdc_Char)0x29,  /* [2716] */
    (xdc_Char)0x0,  /* [2717] */
    (xdc_Char)0x43,  /* [2718] */
    (xdc_Char)0x61,  /* [2719] */
    (xdc_Char)0x6e,  /* [2720] */
    (xdc_Char)0x6e,  /* [2721] */
    (xdc_Char)0x6f,  /* [2722] */
    (xdc_Char)0x74,  /* [2723] */
    (xdc_Char)0x20,  /* [2724] */
    (xdc_Char)0x63,  /* [2725] */
    (xdc_Char)0x61,  /* [2726] */
    (xdc_Char)0x6c,  /* [2727] */
    (xdc_Char)0x6c,  /* [2728] */
    (xdc_Char)0x20,  /* [2729] */
    (xdc_Char)0x48,  /* [2730] */
    (xdc_Char)0x65,  /* [2731] */
    (xdc_Char)0x61,  /* [2732] */
    (xdc_Char)0x70,  /* [2733] */
    (xdc_Char)0x42,  /* [2734] */
    (xdc_Char)0x75,  /* [2735] */
    (xdc_Char)0x66,  /* [2736] */
    (xdc_Char)0x5f,  /* [2737] */
    (xdc_Char)0x66,  /* [2738] */
    (xdc_Char)0x72,  /* [2739] */
    (xdc_Char)0x65,  /* [2740] */
    (xdc_Char)0x65,  /* [2741] */
    (xdc_Char)0x20,  /* [2742] */
    (xdc_Char)0x77,  /* [2743] */
    (xdc_Char)0x68,  /* [2744] */
    (xdc_Char)0x65,  /* [2745] */
    (xdc_Char)0x6e,  /* [2746] */
    (xdc_Char)0x20,  /* [2747] */
    (xdc_Char)0x6e,  /* [2748] */
    (xdc_Char)0x6f,  /* [2749] */
    (xdc_Char)0x20,  /* [2750] */
    (xdc_Char)0x62,  /* [2751] */
    (xdc_Char)0x6c,  /* [2752] */
    (xdc_Char)0x6f,  /* [2753] */
    (xdc_Char)0x63,  /* [2754] */
    (xdc_Char)0x6b,  /* [2755] */
    (xdc_Char)0x73,  /* [2756] */
    (xdc_Char)0x20,  /* [2757] */
    (xdc_Char)0x68,  /* [2758] */
    (xdc_Char)0x61,  /* [2759] */
    (xdc_Char)0x76,  /* [2760] */
    (xdc_Char)0x65,  /* [2761] */
    (xdc_Char)0x20,  /* [2762] */
    (xdc_Char)0x62,  /* [2763] */
    (xdc_Char)0x65,  /* [2764] */
    (xdc_Char)0x65,  /* [2765] */
    (xdc_Char)0x6e,  /* [2766] */
    (xdc_Char)0x20,  /* [2767] */
    (xdc_Char)0x61,  /* [2768] */
    (xdc_Char)0x6c,  /* [2769] */
    (xdc_Char)0x6c,  /* [2770] */
    (xdc_Char)0x6f,  /* [2771] */
    (xdc_Char)0x63,  /* [2772] */
    (xdc_Char)0x61,  /* [2773] */
    (xdc_Char)0x74,  /* [2774] */
    (xdc_Char)0x65,  /* [2775] */
    (xdc_Char)0x64,  /* [2776] */
    (xdc_Char)0x0,  /* [2777] */
    (xdc_Char)0x41,  /* [2778] */
    (xdc_Char)0x5f,  /* [2779] */
    (xdc_Char)0x69,  /* [2780] */
    (xdc_Char)0x6e,  /* [2781] */
    (xdc_Char)0x76,  /* [2782] */
    (xdc_Char)0x61,  /* [2783] */
    (xdc_Char)0x6c,  /* [2784] */
    (xdc_Char)0x69,  /* [2785] */
    (xdc_Char)0x64,  /* [2786] */
    (xdc_Char)0x46,  /* [2787] */
    (xdc_Char)0x72,  /* [2788] */
    (xdc_Char)0x65,  /* [2789] */
    (xdc_Char)0x65,  /* [2790] */
    (xdc_Char)0x3a,  /* [2791] */
    (xdc_Char)0x20,  /* [2792] */
    (xdc_Char)0x49,  /* [2793] */
    (xdc_Char)0x6e,  /* [2794] */
    (xdc_Char)0x76,  /* [2795] */
    (xdc_Char)0x61,  /* [2796] */
    (xdc_Char)0x6c,  /* [2797] */
    (xdc_Char)0x69,  /* [2798] */
    (xdc_Char)0x64,  /* [2799] */
    (xdc_Char)0x20,  /* [2800] */
    (xdc_Char)0x66,  /* [2801] */
    (xdc_Char)0x72,  /* [2802] */
    (xdc_Char)0x65,  /* [2803] */
    (xdc_Char)0x65,  /* [2804] */
    (xdc_Char)0x0,  /* [2805] */
    (xdc_Char)0x41,  /* [2806] */
    (xdc_Char)0x5f,  /* [2807] */
    (xdc_Char)0x7a,  /* [2808] */
    (xdc_Char)0x65,  /* [2809] */
    (xdc_Char)0x72,  /* [2810] */
    (xdc_Char)0x6f,  /* [2811] */
    (xdc_Char)0x42,  /* [2812] */
    (xdc_Char)0x6c,  /* [2813] */
    (xdc_Char)0x6f,  /* [2814] */
    (xdc_Char)0x63,  /* [2815] */
    (xdc_Char)0x6b,  /* [2816] */
    (xdc_Char)0x3a,  /* [2817] */
    (xdc_Char)0x20,  /* [2818] */
    (xdc_Char)0x43,  /* [2819] */
    (xdc_Char)0x61,  /* [2820] */
    (xdc_Char)0x6e,  /* [2821] */
    (xdc_Char)0x6e,  /* [2822] */
    (xdc_Char)0x6f,  /* [2823] */
    (xdc_Char)0x74,  /* [2824] */
    (xdc_Char)0x20,  /* [2825] */
    (xdc_Char)0x61,  /* [2826] */
    (xdc_Char)0x6c,  /* [2827] */
    (xdc_Char)0x6c,  /* [2828] */
    (xdc_Char)0x6f,  /* [2829] */
    (xdc_Char)0x63,  /* [2830] */
    (xdc_Char)0x61,  /* [2831] */
    (xdc_Char)0x74,  /* [2832] */
    (xdc_Char)0x65,  /* [2833] */
    (xdc_Char)0x20,  /* [2834] */
    (xdc_Char)0x73,  /* [2835] */
    (xdc_Char)0x69,  /* [2836] */
    (xdc_Char)0x7a,  /* [2837] */
    (xdc_Char)0x65,  /* [2838] */
    (xdc_Char)0x20,  /* [2839] */
    (xdc_Char)0x30,  /* [2840] */
    (xdc_Char)0x0,  /* [2841] */
    (xdc_Char)0x41,  /* [2842] */
    (xdc_Char)0x5f,  /* [2843] */
    (xdc_Char)0x68,  /* [2844] */
    (xdc_Char)0x65,  /* [2845] */
    (xdc_Char)0x61,  /* [2846] */
    (xdc_Char)0x70,  /* [2847] */
    (xdc_Char)0x53,  /* [2848] */
    (xdc_Char)0x69,  /* [2849] */
    (xdc_Char)0x7a,  /* [2850] */
    (xdc_Char)0x65,  /* [2851] */
    (xdc_Char)0x3a,  /* [2852] */
    (xdc_Char)0x20,  /* [2853] */
    (xdc_Char)0x52,  /* [2854] */
    (xdc_Char)0x65,  /* [2855] */
    (xdc_Char)0x71,  /* [2856] */
    (xdc_Char)0x75,  /* [2857] */
    (xdc_Char)0x65,  /* [2858] */
    (xdc_Char)0x73,  /* [2859] */
    (xdc_Char)0x74,  /* [2860] */
    (xdc_Char)0x65,  /* [2861] */
    (xdc_Char)0x64,  /* [2862] */
    (xdc_Char)0x20,  /* [2863] */
    (xdc_Char)0x68,  /* [2864] */
    (xdc_Char)0x65,  /* [2865] */
    (xdc_Char)0x61,  /* [2866] */
    (xdc_Char)0x70,  /* [2867] */
    (xdc_Char)0x20,  /* [2868] */
    (xdc_Char)0x73,  /* [2869] */
    (xdc_Char)0x69,  /* [2870] */
    (xdc_Char)0x7a,  /* [2871] */
    (xdc_Char)0x65,  /* [2872] */
    (xdc_Char)0x20,  /* [2873] */
    (xdc_Char)0x69,  /* [2874] */
    (xdc_Char)0x73,  /* [2875] */
    (xdc_Char)0x20,  /* [2876] */
    (xdc_Char)0x74,  /* [2877] */
    (xdc_Char)0x6f,  /* [2878] */
    (xdc_Char)0x6f,  /* [2879] */
    (xdc_Char)0x20,  /* [2880] */
    (xdc_Char)0x73,  /* [2881] */
    (xdc_Char)0x6d,  /* [2882] */
    (xdc_Char)0x61,  /* [2883] */
    (xdc_Char)0x6c,  /* [2884] */
    (xdc_Char)0x6c,  /* [2885] */
    (xdc_Char)0x0,  /* [2886] */
    (xdc_Char)0x41,  /* [2887] */
    (xdc_Char)0x5f,  /* [2888] */
    (xdc_Char)0x61,  /* [2889] */
    (xdc_Char)0x6c,  /* [2890] */
    (xdc_Char)0x69,  /* [2891] */
    (xdc_Char)0x67,  /* [2892] */
    (xdc_Char)0x6e,  /* [2893] */
    (xdc_Char)0x3a,  /* [2894] */
    (xdc_Char)0x20,  /* [2895] */
    (xdc_Char)0x52,  /* [2896] */
    (xdc_Char)0x65,  /* [2897] */
    (xdc_Char)0x71,  /* [2898] */
    (xdc_Char)0x75,  /* [2899] */
    (xdc_Char)0x65,  /* [2900] */
    (xdc_Char)0x73,  /* [2901] */
    (xdc_Char)0x74,  /* [2902] */
    (xdc_Char)0x65,  /* [2903] */
    (xdc_Char)0x64,  /* [2904] */
    (xdc_Char)0x20,  /* [2905] */
    (xdc_Char)0x61,  /* [2906] */
    (xdc_Char)0x6c,  /* [2907] */
    (xdc_Char)0x69,  /* [2908] */
    (xdc_Char)0x67,  /* [2909] */
    (xdc_Char)0x6e,  /* [2910] */
    (xdc_Char)0x20,  /* [2911] */
    (xdc_Char)0x69,  /* [2912] */
    (xdc_Char)0x73,  /* [2913] */
    (xdc_Char)0x20,  /* [2914] */
    (xdc_Char)0x6e,  /* [2915] */
    (xdc_Char)0x6f,  /* [2916] */
    (xdc_Char)0x74,  /* [2917] */
    (xdc_Char)0x20,  /* [2918] */
    (xdc_Char)0x61,  /* [2919] */
    (xdc_Char)0x20,  /* [2920] */
    (xdc_Char)0x70,  /* [2921] */
    (xdc_Char)0x6f,  /* [2922] */
    (xdc_Char)0x77,  /* [2923] */
    (xdc_Char)0x65,  /* [2924] */
    (xdc_Char)0x72,  /* [2925] */
    (xdc_Char)0x20,  /* [2926] */
    (xdc_Char)0x6f,  /* [2927] */
    (xdc_Char)0x66,  /* [2928] */
    (xdc_Char)0x20,  /* [2929] */
    (xdc_Char)0x32,  /* [2930] */
    (xdc_Char)0x0,  /* [2931] */
    (xdc_Char)0x49,  /* [2932] */
    (xdc_Char)0x6e,  /* [2933] */
    (xdc_Char)0x76,  /* [2934] */
    (xdc_Char)0x61,  /* [2935] */
    (xdc_Char)0x6c,  /* [2936] */
    (xdc_Char)0x69,  /* [2937] */
    (xdc_Char)0x64,  /* [2938] */
    (xdc_Char)0x20,  /* [2939] */
    (xdc_Char)0x62,  /* [2940] */
    (xdc_Char)0x6c,  /* [2941] */
    (xdc_Char)0x6f,  /* [2942] */
    (xdc_Char)0x63,  /* [2943] */
    (xdc_Char)0x6b,  /* [2944] */
    (xdc_Char)0x20,  /* [2945] */
    (xdc_Char)0x61,  /* [2946] */
    (xdc_Char)0x64,  /* [2947] */
    (xdc_Char)0x64,  /* [2948] */
    (xdc_Char)0x72,  /* [2949] */
    (xdc_Char)0x65,  /* [2950] */
    (xdc_Char)0x73,  /* [2951] */
    (xdc_Char)0x73,  /* [2952] */
    (xdc_Char)0x20,  /* [2953] */
    (xdc_Char)0x6f,  /* [2954] */
    (xdc_Char)0x6e,  /* [2955] */
    (xdc_Char)0x20,  /* [2956] */
    (xdc_Char)0x74,  /* [2957] */
    (xdc_Char)0x68,  /* [2958] */
    (xdc_Char)0x65,  /* [2959] */
    (xdc_Char)0x20,  /* [2960] */
    (xdc_Char)0x66,  /* [2961] */
    (xdc_Char)0x72,  /* [2962] */
    (xdc_Char)0x65,  /* [2963] */
    (xdc_Char)0x65,  /* [2964] */
    (xdc_Char)0x2e,  /* [2965] */
    (xdc_Char)0x20,  /* [2966] */
    (xdc_Char)0x46,  /* [2967] */
    (xdc_Char)0x61,  /* [2968] */
    (xdc_Char)0x69,  /* [2969] */
    (xdc_Char)0x6c,  /* [2970] */
    (xdc_Char)0x65,  /* [2971] */
    (xdc_Char)0x64,  /* [2972] */
    (xdc_Char)0x20,  /* [2973] */
    (xdc_Char)0x74,  /* [2974] */
    (xdc_Char)0x6f,  /* [2975] */
    (xdc_Char)0x20,  /* [2976] */
    (xdc_Char)0x66,  /* [2977] */
    (xdc_Char)0x72,  /* [2978] */
    (xdc_Char)0x65,  /* [2979] */
    (xdc_Char)0x65,  /* [2980] */
    (xdc_Char)0x20,  /* [2981] */
    (xdc_Char)0x62,  /* [2982] */
    (xdc_Char)0x6c,  /* [2983] */
    (xdc_Char)0x6f,  /* [2984] */
    (xdc_Char)0x63,  /* [2985] */
    (xdc_Char)0x6b,  /* [2986] */
    (xdc_Char)0x20,  /* [2987] */
    (xdc_Char)0x62,  /* [2988] */
    (xdc_Char)0x61,  /* [2989] */
    (xdc_Char)0x63,  /* [2990] */
    (xdc_Char)0x6b,  /* [2991] */
    (xdc_Char)0x20,  /* [2992] */
    (xdc_Char)0x74,  /* [2993] */
    (xdc_Char)0x6f,  /* [2994] */
    (xdc_Char)0x20,  /* [2995] */
    (xdc_Char)0x68,  /* [2996] */
    (xdc_Char)0x65,  /* [2997] */
    (xdc_Char)0x61,  /* [2998] */
    (xdc_Char)0x70,  /* [2999] */
    (xdc_Char)0x2e,  /* [3000] */
    (xdc_Char)0x0,  /* [3001] */
    (xdc_Char)0x41,  /* [3002] */
    (xdc_Char)0x5f,  /* [3003] */
    (xdc_Char)0x64,  /* [3004] */
    (xdc_Char)0x6f,  /* [3005] */
    (xdc_Char)0x75,  /* [3006] */
    (xdc_Char)0x62,  /* [3007] */
    (xdc_Char)0x6c,  /* [3008] */
    (xdc_Char)0x65,  /* [3009] */
    (xdc_Char)0x46,  /* [3010] */
    (xdc_Char)0x72,  /* [3011] */
    (xdc_Char)0x65,  /* [3012] */
    (xdc_Char)0x65,  /* [3013] */
    (xdc_Char)0x3a,  /* [3014] */
    (xdc_Char)0x20,  /* [3015] */
    (xdc_Char)0x42,  /* [3016] */
    (xdc_Char)0x75,  /* [3017] */
    (xdc_Char)0x66,  /* [3018] */
    (xdc_Char)0x66,  /* [3019] */
    (xdc_Char)0x65,  /* [3020] */
    (xdc_Char)0x72,  /* [3021] */
    (xdc_Char)0x20,  /* [3022] */
    (xdc_Char)0x61,  /* [3023] */
    (xdc_Char)0x6c,  /* [3024] */
    (xdc_Char)0x72,  /* [3025] */
    (xdc_Char)0x65,  /* [3026] */
    (xdc_Char)0x61,  /* [3027] */
    (xdc_Char)0x64,  /* [3028] */
    (xdc_Char)0x79,  /* [3029] */
    (xdc_Char)0x20,  /* [3030] */
    (xdc_Char)0x66,  /* [3031] */
    (xdc_Char)0x72,  /* [3032] */
    (xdc_Char)0x65,  /* [3033] */
    (xdc_Char)0x65,  /* [3034] */
    (xdc_Char)0x0,  /* [3035] */
    (xdc_Char)0x41,  /* [3036] */
    (xdc_Char)0x5f,  /* [3037] */
    (xdc_Char)0x62,  /* [3038] */
    (xdc_Char)0x75,  /* [3039] */
    (xdc_Char)0x66,  /* [3040] */
    (xdc_Char)0x4f,  /* [3041] */
    (xdc_Char)0x76,  /* [3042] */
    (xdc_Char)0x65,  /* [3043] */
    (xdc_Char)0x72,  /* [3044] */
    (xdc_Char)0x66,  /* [3045] */
    (xdc_Char)0x6c,  /* [3046] */
    (xdc_Char)0x6f,  /* [3047] */
    (xdc_Char)0x77,  /* [3048] */
    (xdc_Char)0x3a,  /* [3049] */
    (xdc_Char)0x20,  /* [3050] */
    (xdc_Char)0x42,  /* [3051] */
    (xdc_Char)0x75,  /* [3052] */
    (xdc_Char)0x66,  /* [3053] */
    (xdc_Char)0x66,  /* [3054] */
    (xdc_Char)0x65,  /* [3055] */
    (xdc_Char)0x72,  /* [3056] */
    (xdc_Char)0x20,  /* [3057] */
    (xdc_Char)0x6f,  /* [3058] */
    (xdc_Char)0x76,  /* [3059] */
    (xdc_Char)0x65,  /* [3060] */
    (xdc_Char)0x72,  /* [3061] */
    (xdc_Char)0x66,  /* [3062] */
    (xdc_Char)0x6c,  /* [3063] */
    (xdc_Char)0x6f,  /* [3064] */
    (xdc_Char)0x77,  /* [3065] */
    (xdc_Char)0x0,  /* [3066] */
    (xdc_Char)0x41,  /* [3067] */
    (xdc_Char)0x5f,  /* [3068] */
    (xdc_Char)0x6e,  /* [3069] */
    (xdc_Char)0x6f,  /* [3070] */
    (xdc_Char)0x74,  /* [3071] */
    (xdc_Char)0x45,  /* [3072] */
    (xdc_Char)0x6d,  /* [3073] */
    (xdc_Char)0x70,  /* [3074] */
    (xdc_Char)0x74,  /* [3075] */
    (xdc_Char)0x79,  /* [3076] */
    (xdc_Char)0x3a,  /* [3077] */
    (xdc_Char)0x20,  /* [3078] */
    (xdc_Char)0x48,  /* [3079] */
    (xdc_Char)0x65,  /* [3080] */
    (xdc_Char)0x61,  /* [3081] */
    (xdc_Char)0x70,  /* [3082] */
    (xdc_Char)0x20,  /* [3083] */
    (xdc_Char)0x6e,  /* [3084] */
    (xdc_Char)0x6f,  /* [3085] */
    (xdc_Char)0x74,  /* [3086] */
    (xdc_Char)0x20,  /* [3087] */
    (xdc_Char)0x65,  /* [3088] */
    (xdc_Char)0x6d,  /* [3089] */
    (xdc_Char)0x70,  /* [3090] */
    (xdc_Char)0x74,  /* [3091] */
    (xdc_Char)0x79,  /* [3092] */
    (xdc_Char)0x0,  /* [3093] */
    (xdc_Char)0x41,  /* [3094] */
    (xdc_Char)0x5f,  /* [3095] */
    (xdc_Char)0x6e,  /* [3096] */
    (xdc_Char)0x75,  /* [3097] */
    (xdc_Char)0x6c,  /* [3098] */
    (xdc_Char)0x6c,  /* [3099] */
    (xdc_Char)0x4f,  /* [3100] */
    (xdc_Char)0x62,  /* [3101] */
    (xdc_Char)0x6a,  /* [3102] */
    (xdc_Char)0x65,  /* [3103] */
    (xdc_Char)0x63,  /* [3104] */
    (xdc_Char)0x74,  /* [3105] */
    (xdc_Char)0x3a,  /* [3106] */
    (xdc_Char)0x20,  /* [3107] */
    (xdc_Char)0x48,  /* [3108] */
    (xdc_Char)0x65,  /* [3109] */
    (xdc_Char)0x61,  /* [3110] */
    (xdc_Char)0x70,  /* [3111] */
    (xdc_Char)0x54,  /* [3112] */
    (xdc_Char)0x72,  /* [3113] */
    (xdc_Char)0x61,  /* [3114] */
    (xdc_Char)0x63,  /* [3115] */
    (xdc_Char)0x6b,  /* [3116] */
    (xdc_Char)0x5f,  /* [3117] */
    (xdc_Char)0x70,  /* [3118] */
    (xdc_Char)0x72,  /* [3119] */
    (xdc_Char)0x69,  /* [3120] */
    (xdc_Char)0x6e,  /* [3121] */
    (xdc_Char)0x74,  /* [3122] */
    (xdc_Char)0x48,  /* [3123] */
    (xdc_Char)0x65,  /* [3124] */
    (xdc_Char)0x61,  /* [3125] */
    (xdc_Char)0x70,  /* [3126] */
    (xdc_Char)0x20,  /* [3127] */
    (xdc_Char)0x63,  /* [3128] */
    (xdc_Char)0x61,  /* [3129] */
    (xdc_Char)0x6c,  /* [3130] */
    (xdc_Char)0x6c,  /* [3131] */
    (xdc_Char)0x65,  /* [3132] */
    (xdc_Char)0x64,  /* [3133] */
    (xdc_Char)0x20,  /* [3134] */
    (xdc_Char)0x77,  /* [3135] */
    (xdc_Char)0x69,  /* [3136] */
    (xdc_Char)0x74,  /* [3137] */
    (xdc_Char)0x68,  /* [3138] */
    (xdc_Char)0x20,  /* [3139] */
    (xdc_Char)0x6e,  /* [3140] */
    (xdc_Char)0x75,  /* [3141] */
    (xdc_Char)0x6c,  /* [3142] */
    (xdc_Char)0x6c,  /* [3143] */
    (xdc_Char)0x20,  /* [3144] */
    (xdc_Char)0x6f,  /* [3145] */
    (xdc_Char)0x62,  /* [3146] */
    (xdc_Char)0x6a,  /* [3147] */
    (xdc_Char)0x0,  /* [3148] */
    (xdc_Char)0x41,  /* [3149] */
    (xdc_Char)0x5f,  /* [3150] */
    (xdc_Char)0x6e,  /* [3151] */
    (xdc_Char)0x75,  /* [3152] */
    (xdc_Char)0x6c,  /* [3153] */
    (xdc_Char)0x6c,  /* [3154] */
    (xdc_Char)0x48,  /* [3155] */
    (xdc_Char)0x61,  /* [3156] */
    (xdc_Char)0x6e,  /* [3157] */
    (xdc_Char)0x64,  /* [3158] */
    (xdc_Char)0x6c,  /* [3159] */
    (xdc_Char)0x65,  /* [3160] */
    (xdc_Char)0x3a,  /* [3161] */
    (xdc_Char)0x20,  /* [3162] */
    (xdc_Char)0x4e,  /* [3163] */
    (xdc_Char)0x75,  /* [3164] */
    (xdc_Char)0x6c,  /* [3165] */
    (xdc_Char)0x6c,  /* [3166] */
    (xdc_Char)0x20,  /* [3167] */
    (xdc_Char)0x68,  /* [3168] */
    (xdc_Char)0x61,  /* [3169] */
    (xdc_Char)0x6e,  /* [3170] */
    (xdc_Char)0x64,  /* [3171] */
    (xdc_Char)0x6c,  /* [3172] */
    (xdc_Char)0x65,  /* [3173] */
    (xdc_Char)0x20,  /* [3174] */
    (xdc_Char)0x70,  /* [3175] */
    (xdc_Char)0x61,  /* [3176] */
    (xdc_Char)0x73,  /* [3177] */
    (xdc_Char)0x73,  /* [3178] */
    (xdc_Char)0x65,  /* [3179] */
    (xdc_Char)0x64,  /* [3180] */
    (xdc_Char)0x20,  /* [3181] */
    (xdc_Char)0x74,  /* [3182] */
    (xdc_Char)0x6f,  /* [3183] */
    (xdc_Char)0x20,  /* [3184] */
    (xdc_Char)0x63,  /* [3185] */
    (xdc_Char)0x72,  /* [3186] */
    (xdc_Char)0x65,  /* [3187] */
    (xdc_Char)0x61,  /* [3188] */
    (xdc_Char)0x74,  /* [3189] */
    (xdc_Char)0x65,  /* [3190] */
    (xdc_Char)0x0,  /* [3191] */
    (xdc_Char)0x41,  /* [3192] */
    (xdc_Char)0x5f,  /* [3193] */
    (xdc_Char)0x62,  /* [3194] */
    (xdc_Char)0x61,  /* [3195] */
    (xdc_Char)0x64,  /* [3196] */
    (xdc_Char)0x43,  /* [3197] */
    (xdc_Char)0x6f,  /* [3198] */
    (xdc_Char)0x6e,  /* [3199] */
    (xdc_Char)0x74,  /* [3200] */
    (xdc_Char)0x65,  /* [3201] */
    (xdc_Char)0x78,  /* [3202] */
    (xdc_Char)0x74,  /* [3203] */
    (xdc_Char)0x3a,  /* [3204] */
    (xdc_Char)0x20,  /* [3205] */
    (xdc_Char)0x62,  /* [3206] */
    (xdc_Char)0x61,  /* [3207] */
    (xdc_Char)0x64,  /* [3208] */
    (xdc_Char)0x20,  /* [3209] */
    (xdc_Char)0x63,  /* [3210] */
    (xdc_Char)0x61,  /* [3211] */
    (xdc_Char)0x6c,  /* [3212] */
    (xdc_Char)0x6c,  /* [3213] */
    (xdc_Char)0x69,  /* [3214] */
    (xdc_Char)0x6e,  /* [3215] */
    (xdc_Char)0x67,  /* [3216] */
    (xdc_Char)0x20,  /* [3217] */
    (xdc_Char)0x63,  /* [3218] */
    (xdc_Char)0x6f,  /* [3219] */
    (xdc_Char)0x6e,  /* [3220] */
    (xdc_Char)0x74,  /* [3221] */
    (xdc_Char)0x65,  /* [3222] */
    (xdc_Char)0x78,  /* [3223] */
    (xdc_Char)0x74,  /* [3224] */
    (xdc_Char)0x2e,  /* [3225] */
    (xdc_Char)0x20,  /* [3226] */
    (xdc_Char)0x4d,  /* [3227] */
    (xdc_Char)0x61,  /* [3228] */
    (xdc_Char)0x79,  /* [3229] */
    (xdc_Char)0x20,  /* [3230] */
    (xdc_Char)0x6e,  /* [3231] */
    (xdc_Char)0x6f,  /* [3232] */
    (xdc_Char)0x74,  /* [3233] */
    (xdc_Char)0x20,  /* [3234] */
    (xdc_Char)0x62,  /* [3235] */
    (xdc_Char)0x65,  /* [3236] */
    (xdc_Char)0x20,  /* [3237] */
    (xdc_Char)0x65,  /* [3238] */
    (xdc_Char)0x6e,  /* [3239] */
    (xdc_Char)0x74,  /* [3240] */
    (xdc_Char)0x65,  /* [3241] */
    (xdc_Char)0x72,  /* [3242] */
    (xdc_Char)0x65,  /* [3243] */
    (xdc_Char)0x64,  /* [3244] */
    (xdc_Char)0x20,  /* [3245] */
    (xdc_Char)0x66,  /* [3246] */
    (xdc_Char)0x72,  /* [3247] */
    (xdc_Char)0x6f,  /* [3248] */
    (xdc_Char)0x6d,  /* [3249] */
    (xdc_Char)0x20,  /* [3250] */
    (xdc_Char)0x61,  /* [3251] */
    (xdc_Char)0x20,  /* [3252] */
    (xdc_Char)0x68,  /* [3253] */
    (xdc_Char)0x61,  /* [3254] */
    (xdc_Char)0x72,  /* [3255] */
    (xdc_Char)0x64,  /* [3256] */
    (xdc_Char)0x77,  /* [3257] */
    (xdc_Char)0x61,  /* [3258] */
    (xdc_Char)0x72,  /* [3259] */
    (xdc_Char)0x65,  /* [3260] */
    (xdc_Char)0x20,  /* [3261] */
    (xdc_Char)0x69,  /* [3262] */
    (xdc_Char)0x6e,  /* [3263] */
    (xdc_Char)0x74,  /* [3264] */
    (xdc_Char)0x65,  /* [3265] */
    (xdc_Char)0x72,  /* [3266] */
    (xdc_Char)0x72,  /* [3267] */
    (xdc_Char)0x75,  /* [3268] */
    (xdc_Char)0x70,  /* [3269] */
    (xdc_Char)0x74,  /* [3270] */
    (xdc_Char)0x20,  /* [3271] */
    (xdc_Char)0x74,  /* [3272] */
    (xdc_Char)0x68,  /* [3273] */
    (xdc_Char)0x72,  /* [3274] */
    (xdc_Char)0x65,  /* [3275] */
    (xdc_Char)0x61,  /* [3276] */
    (xdc_Char)0x64,  /* [3277] */
    (xdc_Char)0x2e,  /* [3278] */
    (xdc_Char)0x0,  /* [3279] */
    (xdc_Char)0x41,  /* [3280] */
    (xdc_Char)0x5f,  /* [3281] */
    (xdc_Char)0x62,  /* [3282] */
    (xdc_Char)0x61,  /* [3283] */
    (xdc_Char)0x64,  /* [3284] */
    (xdc_Char)0x43,  /* [3285] */
    (xdc_Char)0x6f,  /* [3286] */
    (xdc_Char)0x6e,  /* [3287] */
    (xdc_Char)0x74,  /* [3288] */
    (xdc_Char)0x65,  /* [3289] */
    (xdc_Char)0x78,  /* [3290] */
    (xdc_Char)0x74,  /* [3291] */
    (xdc_Char)0x3a,  /* [3292] */
    (xdc_Char)0x20,  /* [3293] */
    (xdc_Char)0x62,  /* [3294] */
    (xdc_Char)0x61,  /* [3295] */
    (xdc_Char)0x64,  /* [3296] */
    (xdc_Char)0x20,  /* [3297] */
    (xdc_Char)0x63,  /* [3298] */
    (xdc_Char)0x61,  /* [3299] */
    (xdc_Char)0x6c,  /* [3300] */
    (xdc_Char)0x6c,  /* [3301] */
    (xdc_Char)0x69,  /* [3302] */
    (xdc_Char)0x6e,  /* [3303] */
    (xdc_Char)0x67,  /* [3304] */
    (xdc_Char)0x20,  /* [3305] */
    (xdc_Char)0x63,  /* [3306] */
    (xdc_Char)0x6f,  /* [3307] */
    (xdc_Char)0x6e,  /* [3308] */
    (xdc_Char)0x74,  /* [3309] */
    (xdc_Char)0x65,  /* [3310] */
    (xdc_Char)0x78,  /* [3311] */
    (xdc_Char)0x74,  /* [3312] */
    (xdc_Char)0x2e,  /* [3313] */
    (xdc_Char)0x20,  /* [3314] */
    (xdc_Char)0x4d,  /* [3315] */
    (xdc_Char)0x61,  /* [3316] */
    (xdc_Char)0x79,  /* [3317] */
    (xdc_Char)0x20,  /* [3318] */
    (xdc_Char)0x6e,  /* [3319] */
    (xdc_Char)0x6f,  /* [3320] */
    (xdc_Char)0x74,  /* [3321] */
    (xdc_Char)0x20,  /* [3322] */
    (xdc_Char)0x62,  /* [3323] */
    (xdc_Char)0x65,  /* [3324] */
    (xdc_Char)0x20,  /* [3325] */
    (xdc_Char)0x65,  /* [3326] */
    (xdc_Char)0x6e,  /* [3327] */
    (xdc_Char)0x74,  /* [3328] */
    (xdc_Char)0x65,  /* [3329] */
    (xdc_Char)0x72,  /* [3330] */
    (xdc_Char)0x65,  /* [3331] */
    (xdc_Char)0x64,  /* [3332] */
    (xdc_Char)0x20,  /* [3333] */
    (xdc_Char)0x66,  /* [3334] */
    (xdc_Char)0x72,  /* [3335] */
    (xdc_Char)0x6f,  /* [3336] */
    (xdc_Char)0x6d,  /* [3337] */
    (xdc_Char)0x20,  /* [3338] */
    (xdc_Char)0x61,  /* [3339] */
    (xdc_Char)0x20,  /* [3340] */
    (xdc_Char)0x73,  /* [3341] */
    (xdc_Char)0x6f,  /* [3342] */
    (xdc_Char)0x66,  /* [3343] */
    (xdc_Char)0x74,  /* [3344] */
    (xdc_Char)0x77,  /* [3345] */
    (xdc_Char)0x61,  /* [3346] */
    (xdc_Char)0x72,  /* [3347] */
    (xdc_Char)0x65,  /* [3348] */
    (xdc_Char)0x20,  /* [3349] */
    (xdc_Char)0x6f,  /* [3350] */
    (xdc_Char)0x72,  /* [3351] */
    (xdc_Char)0x20,  /* [3352] */
    (xdc_Char)0x68,  /* [3353] */
    (xdc_Char)0x61,  /* [3354] */
    (xdc_Char)0x72,  /* [3355] */
    (xdc_Char)0x64,  /* [3356] */
    (xdc_Char)0x77,  /* [3357] */
    (xdc_Char)0x61,  /* [3358] */
    (xdc_Char)0x72,  /* [3359] */
    (xdc_Char)0x65,  /* [3360] */
    (xdc_Char)0x20,  /* [3361] */
    (xdc_Char)0x69,  /* [3362] */
    (xdc_Char)0x6e,  /* [3363] */
    (xdc_Char)0x74,  /* [3364] */
    (xdc_Char)0x65,  /* [3365] */
    (xdc_Char)0x72,  /* [3366] */
    (xdc_Char)0x72,  /* [3367] */
    (xdc_Char)0x75,  /* [3368] */
    (xdc_Char)0x70,  /* [3369] */
    (xdc_Char)0x74,  /* [3370] */
    (xdc_Char)0x20,  /* [3371] */
    (xdc_Char)0x74,  /* [3372] */
    (xdc_Char)0x68,  /* [3373] */
    (xdc_Char)0x72,  /* [3374] */
    (xdc_Char)0x65,  /* [3375] */
    (xdc_Char)0x61,  /* [3376] */
    (xdc_Char)0x64,  /* [3377] */
    (xdc_Char)0x2e,  /* [3378] */
    (xdc_Char)0x0,  /* [3379] */
    (xdc_Char)0x41,  /* [3380] */
    (xdc_Char)0x5f,  /* [3381] */
    (xdc_Char)0x62,  /* [3382] */
    (xdc_Char)0x61,  /* [3383] */
    (xdc_Char)0x64,  /* [3384] */
    (xdc_Char)0x43,  /* [3385] */
    (xdc_Char)0x6f,  /* [3386] */
    (xdc_Char)0x6e,  /* [3387] */
    (xdc_Char)0x74,  /* [3388] */
    (xdc_Char)0x65,  /* [3389] */
    (xdc_Char)0x78,  /* [3390] */
    (xdc_Char)0x74,  /* [3391] */
    (xdc_Char)0x3a,  /* [3392] */
    (xdc_Char)0x20,  /* [3393] */
    (xdc_Char)0x62,  /* [3394] */
    (xdc_Char)0x61,  /* [3395] */
    (xdc_Char)0x64,  /* [3396] */
    (xdc_Char)0x20,  /* [3397] */
    (xdc_Char)0x63,  /* [3398] */
    (xdc_Char)0x61,  /* [3399] */
    (xdc_Char)0x6c,  /* [3400] */
    (xdc_Char)0x6c,  /* [3401] */
    (xdc_Char)0x69,  /* [3402] */
    (xdc_Char)0x6e,  /* [3403] */
    (xdc_Char)0x67,  /* [3404] */
    (xdc_Char)0x20,  /* [3405] */
    (xdc_Char)0x63,  /* [3406] */
    (xdc_Char)0x6f,  /* [3407] */
    (xdc_Char)0x6e,  /* [3408] */
    (xdc_Char)0x74,  /* [3409] */
    (xdc_Char)0x65,  /* [3410] */
    (xdc_Char)0x78,  /* [3411] */
    (xdc_Char)0x74,  /* [3412] */
    (xdc_Char)0x2e,  /* [3413] */
    (xdc_Char)0x20,  /* [3414] */
    (xdc_Char)0x53,  /* [3415] */
    (xdc_Char)0x65,  /* [3416] */
    (xdc_Char)0x65,  /* [3417] */
    (xdc_Char)0x20,  /* [3418] */
    (xdc_Char)0x47,  /* [3419] */
    (xdc_Char)0x61,  /* [3420] */
    (xdc_Char)0x74,  /* [3421] */
    (xdc_Char)0x65,  /* [3422] */
    (xdc_Char)0x4d,  /* [3423] */
    (xdc_Char)0x75,  /* [3424] */
    (xdc_Char)0x74,  /* [3425] */
    (xdc_Char)0x65,  /* [3426] */
    (xdc_Char)0x78,  /* [3427] */
    (xdc_Char)0x50,  /* [3428] */
    (xdc_Char)0x72,  /* [3429] */
    (xdc_Char)0x69,  /* [3430] */
    (xdc_Char)0x20,  /* [3431] */
    (xdc_Char)0x41,  /* [3432] */
    (xdc_Char)0x50,  /* [3433] */
    (xdc_Char)0x49,  /* [3434] */
    (xdc_Char)0x20,  /* [3435] */
    (xdc_Char)0x64,  /* [3436] */
    (xdc_Char)0x6f,  /* [3437] */
    (xdc_Char)0x63,  /* [3438] */
    (xdc_Char)0x20,  /* [3439] */
    (xdc_Char)0x66,  /* [3440] */
    (xdc_Char)0x6f,  /* [3441] */
    (xdc_Char)0x72,  /* [3442] */
    (xdc_Char)0x20,  /* [3443] */
    (xdc_Char)0x64,  /* [3444] */
    (xdc_Char)0x65,  /* [3445] */
    (xdc_Char)0x74,  /* [3446] */
    (xdc_Char)0x61,  /* [3447] */
    (xdc_Char)0x69,  /* [3448] */
    (xdc_Char)0x6c,  /* [3449] */
    (xdc_Char)0x73,  /* [3450] */
    (xdc_Char)0x2e,  /* [3451] */
    (xdc_Char)0x0,  /* [3452] */
    (xdc_Char)0x41,  /* [3453] */
    (xdc_Char)0x5f,  /* [3454] */
    (xdc_Char)0x65,  /* [3455] */
    (xdc_Char)0x6e,  /* [3456] */
    (xdc_Char)0x74,  /* [3457] */
    (xdc_Char)0x65,  /* [3458] */
    (xdc_Char)0x72,  /* [3459] */
    (xdc_Char)0x54,  /* [3460] */
    (xdc_Char)0x61,  /* [3461] */
    (xdc_Char)0x73,  /* [3462] */
    (xdc_Char)0x6b,  /* [3463] */
    (xdc_Char)0x44,  /* [3464] */
    (xdc_Char)0x69,  /* [3465] */
    (xdc_Char)0x73,  /* [3466] */
    (xdc_Char)0x61,  /* [3467] */
    (xdc_Char)0x62,  /* [3468] */
    (xdc_Char)0x6c,  /* [3469] */
    (xdc_Char)0x65,  /* [3470] */
    (xdc_Char)0x64,  /* [3471] */
    (xdc_Char)0x3a,  /* [3472] */
    (xdc_Char)0x20,  /* [3473] */
    (xdc_Char)0x43,  /* [3474] */
    (xdc_Char)0x61,  /* [3475] */
    (xdc_Char)0x6e,  /* [3476] */
    (xdc_Char)0x6e,  /* [3477] */
    (xdc_Char)0x6f,  /* [3478] */
    (xdc_Char)0x74,  /* [3479] */
    (xdc_Char)0x20,  /* [3480] */
    (xdc_Char)0x63,  /* [3481] */
    (xdc_Char)0x61,  /* [3482] */
    (xdc_Char)0x6c,  /* [3483] */
    (xdc_Char)0x6c,  /* [3484] */
    (xdc_Char)0x20,  /* [3485] */
    (xdc_Char)0x47,  /* [3486] */
    (xdc_Char)0x61,  /* [3487] */
    (xdc_Char)0x74,  /* [3488] */
    (xdc_Char)0x65,  /* [3489] */
    (xdc_Char)0x4d,  /* [3490] */
    (xdc_Char)0x75,  /* [3491] */
    (xdc_Char)0x74,  /* [3492] */
    (xdc_Char)0x65,  /* [3493] */
    (xdc_Char)0x78,  /* [3494] */
    (xdc_Char)0x50,  /* [3495] */
    (xdc_Char)0x72,  /* [3496] */
    (xdc_Char)0x69,  /* [3497] */
    (xdc_Char)0x5f,  /* [3498] */
    (xdc_Char)0x65,  /* [3499] */
    (xdc_Char)0x6e,  /* [3500] */
    (xdc_Char)0x74,  /* [3501] */
    (xdc_Char)0x65,  /* [3502] */
    (xdc_Char)0x72,  /* [3503] */
    (xdc_Char)0x28,  /* [3504] */
    (xdc_Char)0x29,  /* [3505] */
    (xdc_Char)0x20,  /* [3506] */
    (xdc_Char)0x77,  /* [3507] */
    (xdc_Char)0x68,  /* [3508] */
    (xdc_Char)0x69,  /* [3509] */
    (xdc_Char)0x6c,  /* [3510] */
    (xdc_Char)0x65,  /* [3511] */
    (xdc_Char)0x20,  /* [3512] */
    (xdc_Char)0x74,  /* [3513] */
    (xdc_Char)0x68,  /* [3514] */
    (xdc_Char)0x65,  /* [3515] */
    (xdc_Char)0x20,  /* [3516] */
    (xdc_Char)0x54,  /* [3517] */
    (xdc_Char)0x61,  /* [3518] */
    (xdc_Char)0x73,  /* [3519] */
    (xdc_Char)0x6b,  /* [3520] */
    (xdc_Char)0x20,  /* [3521] */
    (xdc_Char)0x6f,  /* [3522] */
    (xdc_Char)0x72,  /* [3523] */
    (xdc_Char)0x20,  /* [3524] */
    (xdc_Char)0x53,  /* [3525] */
    (xdc_Char)0x77,  /* [3526] */
    (xdc_Char)0x69,  /* [3527] */
    (xdc_Char)0x20,  /* [3528] */
    (xdc_Char)0x73,  /* [3529] */
    (xdc_Char)0x63,  /* [3530] */
    (xdc_Char)0x68,  /* [3531] */
    (xdc_Char)0x65,  /* [3532] */
    (xdc_Char)0x64,  /* [3533] */
    (xdc_Char)0x75,  /* [3534] */
    (xdc_Char)0x6c,  /* [3535] */
    (xdc_Char)0x65,  /* [3536] */
    (xdc_Char)0x72,  /* [3537] */
    (xdc_Char)0x20,  /* [3538] */
    (xdc_Char)0x69,  /* [3539] */
    (xdc_Char)0x73,  /* [3540] */
    (xdc_Char)0x20,  /* [3541] */
    (xdc_Char)0x64,  /* [3542] */
    (xdc_Char)0x69,  /* [3543] */
    (xdc_Char)0x73,  /* [3544] */
    (xdc_Char)0x61,  /* [3545] */
    (xdc_Char)0x62,  /* [3546] */
    (xdc_Char)0x6c,  /* [3547] */
    (xdc_Char)0x65,  /* [3548] */
    (xdc_Char)0x64,  /* [3549] */
    (xdc_Char)0x2e,  /* [3550] */
    (xdc_Char)0x0,  /* [3551] */
    (xdc_Char)0x41,  /* [3552] */
    (xdc_Char)0x5f,  /* [3553] */
    (xdc_Char)0x62,  /* [3554] */
    (xdc_Char)0x61,  /* [3555] */
    (xdc_Char)0x64,  /* [3556] */
    (xdc_Char)0x43,  /* [3557] */
    (xdc_Char)0x6f,  /* [3558] */
    (xdc_Char)0x6e,  /* [3559] */
    (xdc_Char)0x74,  /* [3560] */
    (xdc_Char)0x65,  /* [3561] */
    (xdc_Char)0x78,  /* [3562] */
    (xdc_Char)0x74,  /* [3563] */
    (xdc_Char)0x3a,  /* [3564] */
    (xdc_Char)0x20,  /* [3565] */
    (xdc_Char)0x62,  /* [3566] */
    (xdc_Char)0x61,  /* [3567] */
    (xdc_Char)0x64,  /* [3568] */
    (xdc_Char)0x20,  /* [3569] */
    (xdc_Char)0x63,  /* [3570] */
    (xdc_Char)0x61,  /* [3571] */
    (xdc_Char)0x6c,  /* [3572] */
    (xdc_Char)0x6c,  /* [3573] */
    (xdc_Char)0x69,  /* [3574] */
    (xdc_Char)0x6e,  /* [3575] */
    (xdc_Char)0x67,  /* [3576] */
    (xdc_Char)0x20,  /* [3577] */
    (xdc_Char)0x63,  /* [3578] */
    (xdc_Char)0x6f,  /* [3579] */
    (xdc_Char)0x6e,  /* [3580] */
    (xdc_Char)0x74,  /* [3581] */
    (xdc_Char)0x65,  /* [3582] */
    (xdc_Char)0x78,  /* [3583] */
    (xdc_Char)0x74,  /* [3584] */
    (xdc_Char)0x2e,  /* [3585] */
    (xdc_Char)0x20,  /* [3586] */
    (xdc_Char)0x53,  /* [3587] */
    (xdc_Char)0x65,  /* [3588] */
    (xdc_Char)0x65,  /* [3589] */
    (xdc_Char)0x20,  /* [3590] */
    (xdc_Char)0x47,  /* [3591] */
    (xdc_Char)0x61,  /* [3592] */
    (xdc_Char)0x74,  /* [3593] */
    (xdc_Char)0x65,  /* [3594] */
    (xdc_Char)0x4d,  /* [3595] */
    (xdc_Char)0x75,  /* [3596] */
    (xdc_Char)0x74,  /* [3597] */
    (xdc_Char)0x65,  /* [3598] */
    (xdc_Char)0x78,  /* [3599] */
    (xdc_Char)0x20,  /* [3600] */
    (xdc_Char)0x41,  /* [3601] */
    (xdc_Char)0x50,  /* [3602] */
    (xdc_Char)0x49,  /* [3603] */
    (xdc_Char)0x20,  /* [3604] */
    (xdc_Char)0x64,  /* [3605] */
    (xdc_Char)0x6f,  /* [3606] */
    (xdc_Char)0x63,  /* [3607] */
    (xdc_Char)0x20,  /* [3608] */
    (xdc_Char)0x66,  /* [3609] */
    (xdc_Char)0x6f,  /* [3610] */
    (xdc_Char)0x72,  /* [3611] */
    (xdc_Char)0x20,  /* [3612] */
    (xdc_Char)0x64,  /* [3613] */
    (xdc_Char)0x65,  /* [3614] */
    (xdc_Char)0x74,  /* [3615] */
    (xdc_Char)0x61,  /* [3616] */
    (xdc_Char)0x69,  /* [3617] */
    (xdc_Char)0x6c,  /* [3618] */
    (xdc_Char)0x73,  /* [3619] */
    (xdc_Char)0x2e,  /* [3620] */
    (xdc_Char)0x0,  /* [3621] */
    (xdc_Char)0x41,  /* [3622] */
    (xdc_Char)0x5f,  /* [3623] */
    (xdc_Char)0x62,  /* [3624] */
    (xdc_Char)0x61,  /* [3625] */
    (xdc_Char)0x64,  /* [3626] */
    (xdc_Char)0x43,  /* [3627] */
    (xdc_Char)0x6f,  /* [3628] */
    (xdc_Char)0x6e,  /* [3629] */
    (xdc_Char)0x74,  /* [3630] */
    (xdc_Char)0x65,  /* [3631] */
    (xdc_Char)0x78,  /* [3632] */
    (xdc_Char)0x74,  /* [3633] */
    (xdc_Char)0x3a,  /* [3634] */
    (xdc_Char)0x20,  /* [3635] */
    (xdc_Char)0x62,  /* [3636] */
    (xdc_Char)0x61,  /* [3637] */
    (xdc_Char)0x64,  /* [3638] */
    (xdc_Char)0x20,  /* [3639] */
    (xdc_Char)0x63,  /* [3640] */
    (xdc_Char)0x61,  /* [3641] */
    (xdc_Char)0x6c,  /* [3642] */
    (xdc_Char)0x6c,  /* [3643] */
    (xdc_Char)0x69,  /* [3644] */
    (xdc_Char)0x6e,  /* [3645] */
    (xdc_Char)0x67,  /* [3646] */
    (xdc_Char)0x20,  /* [3647] */
    (xdc_Char)0x63,  /* [3648] */
    (xdc_Char)0x6f,  /* [3649] */
    (xdc_Char)0x6e,  /* [3650] */
    (xdc_Char)0x74,  /* [3651] */
    (xdc_Char)0x65,  /* [3652] */
    (xdc_Char)0x78,  /* [3653] */
    (xdc_Char)0x74,  /* [3654] */
    (xdc_Char)0x2e,  /* [3655] */
    (xdc_Char)0x20,  /* [3656] */
    (xdc_Char)0x53,  /* [3657] */
    (xdc_Char)0x65,  /* [3658] */
    (xdc_Char)0x65,  /* [3659] */
    (xdc_Char)0x20,  /* [3660] */
    (xdc_Char)0x47,  /* [3661] */
    (xdc_Char)0x61,  /* [3662] */
    (xdc_Char)0x74,  /* [3663] */
    (xdc_Char)0x65,  /* [3664] */
    (xdc_Char)0x53,  /* [3665] */
    (xdc_Char)0x70,  /* [3666] */
    (xdc_Char)0x69,  /* [3667] */
    (xdc_Char)0x6e,  /* [3668] */
    (xdc_Char)0x6c,  /* [3669] */
    (xdc_Char)0x6f,  /* [3670] */
    (xdc_Char)0x63,  /* [3671] */
    (xdc_Char)0x6b,  /* [3672] */
    (xdc_Char)0x20,  /* [3673] */
    (xdc_Char)0x41,  /* [3674] */
    (xdc_Char)0x50,  /* [3675] */
    (xdc_Char)0x49,  /* [3676] */
    (xdc_Char)0x20,  /* [3677] */
    (xdc_Char)0x64,  /* [3678] */
    (xdc_Char)0x6f,  /* [3679] */
    (xdc_Char)0x63,  /* [3680] */
    (xdc_Char)0x20,  /* [3681] */
    (xdc_Char)0x66,  /* [3682] */
    (xdc_Char)0x6f,  /* [3683] */
    (xdc_Char)0x72,  /* [3684] */
    (xdc_Char)0x20,  /* [3685] */
    (xdc_Char)0x64,  /* [3686] */
    (xdc_Char)0x65,  /* [3687] */
    (xdc_Char)0x74,  /* [3688] */
    (xdc_Char)0x61,  /* [3689] */
    (xdc_Char)0x69,  /* [3690] */
    (xdc_Char)0x6c,  /* [3691] */
    (xdc_Char)0x73,  /* [3692] */
    (xdc_Char)0x2e,  /* [3693] */
    (xdc_Char)0x0,  /* [3694] */
    (xdc_Char)0x41,  /* [3695] */
    (xdc_Char)0x5f,  /* [3696] */
    (xdc_Char)0x69,  /* [3697] */
    (xdc_Char)0x6e,  /* [3698] */
    (xdc_Char)0x76,  /* [3699] */
    (xdc_Char)0x61,  /* [3700] */
    (xdc_Char)0x6c,  /* [3701] */
    (xdc_Char)0x69,  /* [3702] */
    (xdc_Char)0x64,  /* [3703] */
    (xdc_Char)0x51,  /* [3704] */
    (xdc_Char)0x75,  /* [3705] */
    (xdc_Char)0x61,  /* [3706] */
    (xdc_Char)0x6c,  /* [3707] */
    (xdc_Char)0x69,  /* [3708] */
    (xdc_Char)0x74,  /* [3709] */
    (xdc_Char)0x79,  /* [3710] */
    (xdc_Char)0x3a,  /* [3711] */
    (xdc_Char)0x20,  /* [3712] */
    (xdc_Char)0x53,  /* [3713] */
    (xdc_Char)0x65,  /* [3714] */
    (xdc_Char)0x65,  /* [3715] */
    (xdc_Char)0x20,  /* [3716] */
    (xdc_Char)0x47,  /* [3717] */
    (xdc_Char)0x61,  /* [3718] */
    (xdc_Char)0x74,  /* [3719] */
    (xdc_Char)0x65,  /* [3720] */
    (xdc_Char)0x53,  /* [3721] */
    (xdc_Char)0x70,  /* [3722] */
    (xdc_Char)0x69,  /* [3723] */
    (xdc_Char)0x6e,  /* [3724] */
    (xdc_Char)0x6c,  /* [3725] */
    (xdc_Char)0x6f,  /* [3726] */
    (xdc_Char)0x63,  /* [3727] */
    (xdc_Char)0x6b,  /* [3728] */
    (xdc_Char)0x20,  /* [3729] */
    (xdc_Char)0x41,  /* [3730] */
    (xdc_Char)0x50,  /* [3731] */
    (xdc_Char)0x49,  /* [3732] */
    (xdc_Char)0x20,  /* [3733] */
    (xdc_Char)0x64,  /* [3734] */
    (xdc_Char)0x6f,  /* [3735] */
    (xdc_Char)0x63,  /* [3736] */
    (xdc_Char)0x20,  /* [3737] */
    (xdc_Char)0x66,  /* [3738] */
    (xdc_Char)0x6f,  /* [3739] */
    (xdc_Char)0x72,  /* [3740] */
    (xdc_Char)0x20,  /* [3741] */
    (xdc_Char)0x64,  /* [3742] */
    (xdc_Char)0x65,  /* [3743] */
    (xdc_Char)0x74,  /* [3744] */
    (xdc_Char)0x61,  /* [3745] */
    (xdc_Char)0x69,  /* [3746] */
    (xdc_Char)0x6c,  /* [3747] */
    (xdc_Char)0x73,  /* [3748] */
    (xdc_Char)0x2e,  /* [3749] */
    (xdc_Char)0x0,  /* [3750] */
    (xdc_Char)0x41,  /* [3751] */
    (xdc_Char)0x5f,  /* [3752] */
    (xdc_Char)0x6e,  /* [3753] */
    (xdc_Char)0x6f,  /* [3754] */
    (xdc_Char)0x74,  /* [3755] */
    (xdc_Char)0x41,  /* [3756] */
    (xdc_Char)0x76,  /* [3757] */
    (xdc_Char)0x61,  /* [3758] */
    (xdc_Char)0x69,  /* [3759] */
    (xdc_Char)0x6c,  /* [3760] */
    (xdc_Char)0x61,  /* [3761] */
    (xdc_Char)0x62,  /* [3762] */
    (xdc_Char)0x6c,  /* [3763] */
    (xdc_Char)0x65,  /* [3764] */
    (xdc_Char)0x3a,  /* [3765] */
    (xdc_Char)0x20,  /* [3766] */
    (xdc_Char)0x73,  /* [3767] */
    (xdc_Char)0x74,  /* [3768] */
    (xdc_Char)0x61,  /* [3769] */
    (xdc_Char)0x74,  /* [3770] */
    (xdc_Char)0x69,  /* [3771] */
    (xdc_Char)0x63,  /* [3772] */
    (xdc_Char)0x61,  /* [3773] */
    (xdc_Char)0x6c,  /* [3774] */
    (xdc_Char)0x6c,  /* [3775] */
    (xdc_Char)0x79,  /* [3776] */
    (xdc_Char)0x20,  /* [3777] */
    (xdc_Char)0x63,  /* [3778] */
    (xdc_Char)0x72,  /* [3779] */
    (xdc_Char)0x65,  /* [3780] */
    (xdc_Char)0x61,  /* [3781] */
    (xdc_Char)0x74,  /* [3782] */
    (xdc_Char)0x65,  /* [3783] */
    (xdc_Char)0x64,  /* [3784] */
    (xdc_Char)0x20,  /* [3785] */
    (xdc_Char)0x74,  /* [3786] */
    (xdc_Char)0x69,  /* [3787] */
    (xdc_Char)0x6d,  /* [3788] */
    (xdc_Char)0x65,  /* [3789] */
    (xdc_Char)0x72,  /* [3790] */
    (xdc_Char)0x20,  /* [3791] */
    (xdc_Char)0x6e,  /* [3792] */
    (xdc_Char)0x6f,  /* [3793] */
    (xdc_Char)0x74,  /* [3794] */
    (xdc_Char)0x20,  /* [3795] */
    (xdc_Char)0x61,  /* [3796] */
    (xdc_Char)0x76,  /* [3797] */
    (xdc_Char)0x61,  /* [3798] */
    (xdc_Char)0x69,  /* [3799] */
    (xdc_Char)0x6c,  /* [3800] */
    (xdc_Char)0x61,  /* [3801] */
    (xdc_Char)0x62,  /* [3802] */
    (xdc_Char)0x6c,  /* [3803] */
    (xdc_Char)0x65,  /* [3804] */
    (xdc_Char)0x0,  /* [3805] */
    (xdc_Char)0x61,  /* [3806] */
    (xdc_Char)0x73,  /* [3807] */
    (xdc_Char)0x73,  /* [3808] */
    (xdc_Char)0x65,  /* [3809] */
    (xdc_Char)0x72,  /* [3810] */
    (xdc_Char)0x74,  /* [3811] */
    (xdc_Char)0x69,  /* [3812] */
    (xdc_Char)0x6f,  /* [3813] */
    (xdc_Char)0x6e,  /* [3814] */
    (xdc_Char)0x20,  /* [3815] */
    (xdc_Char)0x66,  /* [3816] */
    (xdc_Char)0x61,  /* [3817] */
    (xdc_Char)0x69,  /* [3818] */
    (xdc_Char)0x6c,  /* [3819] */
    (xdc_Char)0x75,  /* [3820] */
    (xdc_Char)0x72,  /* [3821] */
    (xdc_Char)0x65,  /* [3822] */
    (xdc_Char)0x25,  /* [3823] */
    (xdc_Char)0x73,  /* [3824] */
    (xdc_Char)0x25,  /* [3825] */
    (xdc_Char)0x73,  /* [3826] */
    (xdc_Char)0x0,  /* [3827] */
    (xdc_Char)0x25,  /* [3828] */
    (xdc_Char)0x24,  /* [3829] */
    (xdc_Char)0x53,  /* [3830] */
    (xdc_Char)0x0,  /* [3831] */
    (xdc_Char)0x6f,  /* [3832] */
    (xdc_Char)0x75,  /* [3833] */
    (xdc_Char)0x74,  /* [3834] */
    (xdc_Char)0x20,  /* [3835] */
    (xdc_Char)0x6f,  /* [3836] */
    (xdc_Char)0x66,  /* [3837] */
    (xdc_Char)0x20,  /* [3838] */
    (xdc_Char)0x6d,  /* [3839] */
    (xdc_Char)0x65,  /* [3840] */
    (xdc_Char)0x6d,  /* [3841] */
    (xdc_Char)0x6f,  /* [3842] */
    (xdc_Char)0x72,  /* [3843] */
    (xdc_Char)0x79,  /* [3844] */
    (xdc_Char)0x3a,  /* [3845] */
    (xdc_Char)0x20,  /* [3846] */
    (xdc_Char)0x68,  /* [3847] */
    (xdc_Char)0x65,  /* [3848] */
    (xdc_Char)0x61,  /* [3849] */
    (xdc_Char)0x70,  /* [3850] */
    (xdc_Char)0x3d,  /* [3851] */
    (xdc_Char)0x30,  /* [3852] */
    (xdc_Char)0x78,  /* [3853] */
    (xdc_Char)0x25,  /* [3854] */
    (xdc_Char)0x78,  /* [3855] */
    (xdc_Char)0x2c,  /* [3856] */
    (xdc_Char)0x20,  /* [3857] */
    (xdc_Char)0x73,  /* [3858] */
    (xdc_Char)0x69,  /* [3859] */
    (xdc_Char)0x7a,  /* [3860] */
    (xdc_Char)0x65,  /* [3861] */
    (xdc_Char)0x3d,  /* [3862] */
    (xdc_Char)0x25,  /* [3863] */
    (xdc_Char)0x75,  /* [3864] */
    (xdc_Char)0x0,  /* [3865] */
    (xdc_Char)0x25,  /* [3866] */
    (xdc_Char)0x73,  /* [3867] */
    (xdc_Char)0x20,  /* [3868] */
    (xdc_Char)0x30,  /* [3869] */
    (xdc_Char)0x78,  /* [3870] */
    (xdc_Char)0x25,  /* [3871] */
    (xdc_Char)0x78,  /* [3872] */
    (xdc_Char)0x0,  /* [3873] */
    (xdc_Char)0x45,  /* [3874] */
    (xdc_Char)0x5f,  /* [3875] */
    (xdc_Char)0x62,  /* [3876] */
    (xdc_Char)0x61,  /* [3877] */
    (xdc_Char)0x64,  /* [3878] */
    (xdc_Char)0x4c,  /* [3879] */
    (xdc_Char)0x65,  /* [3880] */
    (xdc_Char)0x76,  /* [3881] */
    (xdc_Char)0x65,  /* [3882] */
    (xdc_Char)0x6c,  /* [3883] */
    (xdc_Char)0x3a,  /* [3884] */
    (xdc_Char)0x20,  /* [3885] */
    (xdc_Char)0x42,  /* [3886] */
    (xdc_Char)0x61,  /* [3887] */
    (xdc_Char)0x64,  /* [3888] */
    (xdc_Char)0x20,  /* [3889] */
    (xdc_Char)0x66,  /* [3890] */
    (xdc_Char)0x69,  /* [3891] */
    (xdc_Char)0x6c,  /* [3892] */
    (xdc_Char)0x74,  /* [3893] */
    (xdc_Char)0x65,  /* [3894] */
    (xdc_Char)0x72,  /* [3895] */
    (xdc_Char)0x20,  /* [3896] */
    (xdc_Char)0x6c,  /* [3897] */
    (xdc_Char)0x65,  /* [3898] */
    (xdc_Char)0x76,  /* [3899] */
    (xdc_Char)0x65,  /* [3900] */
    (xdc_Char)0x6c,  /* [3901] */
    (xdc_Char)0x20,  /* [3902] */
    (xdc_Char)0x76,  /* [3903] */
    (xdc_Char)0x61,  /* [3904] */
    (xdc_Char)0x6c,  /* [3905] */
    (xdc_Char)0x75,  /* [3906] */
    (xdc_Char)0x65,  /* [3907] */
    (xdc_Char)0x3a,  /* [3908] */
    (xdc_Char)0x20,  /* [3909] */
    (xdc_Char)0x25,  /* [3910] */
    (xdc_Char)0x64,  /* [3911] */
    (xdc_Char)0x0,  /* [3912] */
    (xdc_Char)0x66,  /* [3913] */
    (xdc_Char)0x72,  /* [3914] */
    (xdc_Char)0x65,  /* [3915] */
    (xdc_Char)0x65,  /* [3916] */
    (xdc_Char)0x28,  /* [3917] */
    (xdc_Char)0x29,  /* [3918] */
    (xdc_Char)0x20,  /* [3919] */
    (xdc_Char)0x69,  /* [3920] */
    (xdc_Char)0x6e,  /* [3921] */
    (xdc_Char)0x76,  /* [3922] */
    (xdc_Char)0x61,  /* [3923] */
    (xdc_Char)0x6c,  /* [3924] */
    (xdc_Char)0x69,  /* [3925] */
    (xdc_Char)0x64,  /* [3926] */
    (xdc_Char)0x20,  /* [3927] */
    (xdc_Char)0x69,  /* [3928] */
    (xdc_Char)0x6e,  /* [3929] */
    (xdc_Char)0x20,  /* [3930] */
    (xdc_Char)0x67,  /* [3931] */
    (xdc_Char)0x72,  /* [3932] */
    (xdc_Char)0x6f,  /* [3933] */
    (xdc_Char)0x77,  /* [3934] */
    (xdc_Char)0x74,  /* [3935] */
    (xdc_Char)0x68,  /* [3936] */
    (xdc_Char)0x2d,  /* [3937] */
    (xdc_Char)0x6f,  /* [3938] */
    (xdc_Char)0x6e,  /* [3939] */
    (xdc_Char)0x6c,  /* [3940] */
    (xdc_Char)0x79,  /* [3941] */
    (xdc_Char)0x20,  /* [3942] */
    (xdc_Char)0x48,  /* [3943] */
    (xdc_Char)0x65,  /* [3944] */
    (xdc_Char)0x61,  /* [3945] */
    (xdc_Char)0x70,  /* [3946] */
    (xdc_Char)0x4d,  /* [3947] */
    (xdc_Char)0x69,  /* [3948] */
    (xdc_Char)0x6e,  /* [3949] */
    (xdc_Char)0x0,  /* [3950] */
    (xdc_Char)0x54,  /* [3951] */
    (xdc_Char)0x68,  /* [3952] */
    (xdc_Char)0x65,  /* [3953] */
    (xdc_Char)0x20,  /* [3954] */
    (xdc_Char)0x52,  /* [3955] */
    (xdc_Char)0x54,  /* [3956] */
    (xdc_Char)0x53,  /* [3957] */
    (xdc_Char)0x20,  /* [3958] */
    (xdc_Char)0x68,  /* [3959] */
    (xdc_Char)0x65,  /* [3960] */
    (xdc_Char)0x61,  /* [3961] */
    (xdc_Char)0x70,  /* [3962] */
    (xdc_Char)0x20,  /* [3963] */
    (xdc_Char)0x69,  /* [3964] */
    (xdc_Char)0x73,  /* [3965] */
    (xdc_Char)0x20,  /* [3966] */
    (xdc_Char)0x75,  /* [3967] */
    (xdc_Char)0x73,  /* [3968] */
    (xdc_Char)0x65,  /* [3969] */
    (xdc_Char)0x64,  /* [3970] */
    (xdc_Char)0x20,  /* [3971] */
    (xdc_Char)0x75,  /* [3972] */
    (xdc_Char)0x70,  /* [3973] */
    (xdc_Char)0x2e,  /* [3974] */
    (xdc_Char)0x20,  /* [3975] */
    (xdc_Char)0x45,  /* [3976] */
    (xdc_Char)0x78,  /* [3977] */
    (xdc_Char)0x61,  /* [3978] */
    (xdc_Char)0x6d,  /* [3979] */
    (xdc_Char)0x69,  /* [3980] */
    (xdc_Char)0x6e,  /* [3981] */
    (xdc_Char)0x65,  /* [3982] */
    (xdc_Char)0x20,  /* [3983] */
    (xdc_Char)0x50,  /* [3984] */
    (xdc_Char)0x72,  /* [3985] */
    (xdc_Char)0x6f,  /* [3986] */
    (xdc_Char)0x67,  /* [3987] */
    (xdc_Char)0x72,  /* [3988] */
    (xdc_Char)0x61,  /* [3989] */
    (xdc_Char)0x6d,  /* [3990] */
    (xdc_Char)0x2e,  /* [3991] */
    (xdc_Char)0x68,  /* [3992] */
    (xdc_Char)0x65,  /* [3993] */
    (xdc_Char)0x61,  /* [3994] */
    (xdc_Char)0x70,  /* [3995] */
    (xdc_Char)0x2e,  /* [3996] */
    (xdc_Char)0x0,  /* [3997] */
    (xdc_Char)0x45,  /* [3998] */
    (xdc_Char)0x5f,  /* [3999] */
    (xdc_Char)0x62,  /* [4000] */
    (xdc_Char)0x61,  /* [4001] */
    (xdc_Char)0x64,  /* [4002] */
    (xdc_Char)0x43,  /* [4003] */
    (xdc_Char)0x6f,  /* [4004] */
    (xdc_Char)0x6d,  /* [4005] */
    (xdc_Char)0x6d,  /* [4006] */
    (xdc_Char)0x61,  /* [4007] */
    (xdc_Char)0x6e,  /* [4008] */
    (xdc_Char)0x64,  /* [4009] */
    (xdc_Char)0x3a,  /* [4010] */
    (xdc_Char)0x20,  /* [4011] */
    (xdc_Char)0x52,  /* [4012] */
    (xdc_Char)0x65,  /* [4013] */
    (xdc_Char)0x63,  /* [4014] */
    (xdc_Char)0x65,  /* [4015] */
    (xdc_Char)0x69,  /* [4016] */
    (xdc_Char)0x76,  /* [4017] */
    (xdc_Char)0x65,  /* [4018] */
    (xdc_Char)0x64,  /* [4019] */
    (xdc_Char)0x20,  /* [4020] */
    (xdc_Char)0x69,  /* [4021] */
    (xdc_Char)0x6e,  /* [4022] */
    (xdc_Char)0x76,  /* [4023] */
    (xdc_Char)0x61,  /* [4024] */
    (xdc_Char)0x6c,  /* [4025] */
    (xdc_Char)0x69,  /* [4026] */
    (xdc_Char)0x64,  /* [4027] */
    (xdc_Char)0x20,  /* [4028] */
    (xdc_Char)0x63,  /* [4029] */
    (xdc_Char)0x6f,  /* [4030] */
    (xdc_Char)0x6d,  /* [4031] */
    (xdc_Char)0x6d,  /* [4032] */
    (xdc_Char)0x61,  /* [4033] */
    (xdc_Char)0x6e,  /* [4034] */
    (xdc_Char)0x64,  /* [4035] */
    (xdc_Char)0x2c,  /* [4036] */
    (xdc_Char)0x20,  /* [4037] */
    (xdc_Char)0x69,  /* [4038] */
    (xdc_Char)0x64,  /* [4039] */
    (xdc_Char)0x3a,  /* [4040] */
    (xdc_Char)0x20,  /* [4041] */
    (xdc_Char)0x25,  /* [4042] */
    (xdc_Char)0x64,  /* [4043] */
    (xdc_Char)0x2e,  /* [4044] */
    (xdc_Char)0x0,  /* [4045] */
    (xdc_Char)0x45,  /* [4046] */
    (xdc_Char)0x5f,  /* [4047] */
    (xdc_Char)0x65,  /* [4048] */
    (xdc_Char)0x78,  /* [4049] */
    (xdc_Char)0x63,  /* [4050] */
    (xdc_Char)0x65,  /* [4051] */
    (xdc_Char)0x70,  /* [4052] */
    (xdc_Char)0x74,  /* [4053] */
    (xdc_Char)0x69,  /* [4054] */
    (xdc_Char)0x6f,  /* [4055] */
    (xdc_Char)0x6e,  /* [4056] */
    (xdc_Char)0x4d,  /* [4057] */
    (xdc_Char)0x69,  /* [4058] */
    (xdc_Char)0x6e,  /* [4059] */
    (xdc_Char)0x3a,  /* [4060] */
    (xdc_Char)0x20,  /* [4061] */
    (xdc_Char)0x70,  /* [4062] */
    (xdc_Char)0x63,  /* [4063] */
    (xdc_Char)0x20,  /* [4064] */
    (xdc_Char)0x3d,  /* [4065] */
    (xdc_Char)0x20,  /* [4066] */
    (xdc_Char)0x30,  /* [4067] */
    (xdc_Char)0x78,  /* [4068] */
    (xdc_Char)0x25,  /* [4069] */
    (xdc_Char)0x30,  /* [4070] */
    (xdc_Char)0x38,  /* [4071] */
    (xdc_Char)0x78,  /* [4072] */
    (xdc_Char)0x2c,  /* [4073] */
    (xdc_Char)0x20,  /* [4074] */
    (xdc_Char)0x73,  /* [4075] */
    (xdc_Char)0x70,  /* [4076] */
    (xdc_Char)0x20,  /* [4077] */
    (xdc_Char)0x3d,  /* [4078] */
    (xdc_Char)0x20,  /* [4079] */
    (xdc_Char)0x30,  /* [4080] */
    (xdc_Char)0x78,  /* [4081] */
    (xdc_Char)0x25,  /* [4082] */
    (xdc_Char)0x30,  /* [4083] */
    (xdc_Char)0x38,  /* [4084] */
    (xdc_Char)0x78,  /* [4085] */
    (xdc_Char)0x2e,  /* [4086] */
    (xdc_Char)0xa,  /* [4087] */
    (xdc_Char)0x54,  /* [4088] */
    (xdc_Char)0x6f,  /* [4089] */
    (xdc_Char)0x20,  /* [4090] */
    (xdc_Char)0x73,  /* [4091] */
    (xdc_Char)0x65,  /* [4092] */
    (xdc_Char)0x65,  /* [4093] */
    (xdc_Char)0x20,  /* [4094] */
    (xdc_Char)0x6d,  /* [4095] */
    (xdc_Char)0x6f,  /* [4096] */
    (xdc_Char)0x72,  /* [4097] */
    (xdc_Char)0x65,  /* [4098] */
    (xdc_Char)0x20,  /* [4099] */
    (xdc_Char)0x65,  /* [4100] */
    (xdc_Char)0x78,  /* [4101] */
    (xdc_Char)0x63,  /* [4102] */
    (xdc_Char)0x65,  /* [4103] */
    (xdc_Char)0x70,  /* [4104] */
    (xdc_Char)0x74,  /* [4105] */
    (xdc_Char)0x69,  /* [4106] */
    (xdc_Char)0x6f,  /* [4107] */
    (xdc_Char)0x6e,  /* [4108] */
    (xdc_Char)0x20,  /* [4109] */
    (xdc_Char)0x64,  /* [4110] */
    (xdc_Char)0x65,  /* [4111] */
    (xdc_Char)0x74,  /* [4112] */
    (xdc_Char)0x61,  /* [4113] */
    (xdc_Char)0x69,  /* [4114] */
    (xdc_Char)0x6c,  /* [4115] */
    (xdc_Char)0x2c,  /* [4116] */
    (xdc_Char)0x20,  /* [4117] */
    (xdc_Char)0x75,  /* [4118] */
    (xdc_Char)0x73,  /* [4119] */
    (xdc_Char)0x65,  /* [4120] */
    (xdc_Char)0x20,  /* [4121] */
    (xdc_Char)0x52,  /* [4122] */
    (xdc_Char)0x4f,  /* [4123] */
    (xdc_Char)0x56,  /* [4124] */
    (xdc_Char)0x20,  /* [4125] */
    (xdc_Char)0x6f,  /* [4126] */
    (xdc_Char)0x72,  /* [4127] */
    (xdc_Char)0x20,  /* [4128] */
    (xdc_Char)0x73,  /* [4129] */
    (xdc_Char)0x65,  /* [4130] */
    (xdc_Char)0x74,  /* [4131] */
    (xdc_Char)0x20,  /* [4132] */
    (xdc_Char)0x27,  /* [4133] */
    (xdc_Char)0x74,  /* [4134] */
    (xdc_Char)0x69,  /* [4135] */
    (xdc_Char)0x2e,  /* [4136] */
    (xdc_Char)0x73,  /* [4137] */
    (xdc_Char)0x79,  /* [4138] */
    (xdc_Char)0x73,  /* [4139] */
    (xdc_Char)0x62,  /* [4140] */
    (xdc_Char)0x69,  /* [4141] */
    (xdc_Char)0x6f,  /* [4142] */
    (xdc_Char)0x73,  /* [4143] */
    (xdc_Char)0x2e,  /* [4144] */
    (xdc_Char)0x66,  /* [4145] */
    (xdc_Char)0x61,  /* [4146] */
    (xdc_Char)0x6d,  /* [4147] */
    (xdc_Char)0x69,  /* [4148] */
    (xdc_Char)0x6c,  /* [4149] */
    (xdc_Char)0x79,  /* [4150] */
    (xdc_Char)0x2e,  /* [4151] */
    (xdc_Char)0x63,  /* [4152] */
    (xdc_Char)0x36,  /* [4153] */
    (xdc_Char)0x34,  /* [4154] */
    (xdc_Char)0x70,  /* [4155] */
    (xdc_Char)0x2e,  /* [4156] */
    (xdc_Char)0x45,  /* [4157] */
    (xdc_Char)0x78,  /* [4158] */
    (xdc_Char)0x63,  /* [4159] */
    (xdc_Char)0x65,  /* [4160] */
    (xdc_Char)0x70,  /* [4161] */
    (xdc_Char)0x74,  /* [4162] */
    (xdc_Char)0x69,  /* [4163] */
    (xdc_Char)0x6f,  /* [4164] */
    (xdc_Char)0x6e,  /* [4165] */
    (xdc_Char)0x2e,  /* [4166] */
    (xdc_Char)0x65,  /* [4167] */
    (xdc_Char)0x6e,  /* [4168] */
    (xdc_Char)0x61,  /* [4169] */
    (xdc_Char)0x62,  /* [4170] */
    (xdc_Char)0x6c,  /* [4171] */
    (xdc_Char)0x65,  /* [4172] */
    (xdc_Char)0x50,  /* [4173] */
    (xdc_Char)0x72,  /* [4174] */
    (xdc_Char)0x69,  /* [4175] */
    (xdc_Char)0x6e,  /* [4176] */
    (xdc_Char)0x74,  /* [4177] */
    (xdc_Char)0x20,  /* [4178] */
    (xdc_Char)0x3d,  /* [4179] */
    (xdc_Char)0x20,  /* [4180] */
    (xdc_Char)0x74,  /* [4181] */
    (xdc_Char)0x72,  /* [4182] */
    (xdc_Char)0x75,  /* [4183] */
    (xdc_Char)0x65,  /* [4184] */
    (xdc_Char)0x3b,  /* [4185] */
    (xdc_Char)0x27,  /* [4186] */
    (xdc_Char)0x0,  /* [4187] */
    (xdc_Char)0x45,  /* [4188] */
    (xdc_Char)0x5f,  /* [4189] */
    (xdc_Char)0x65,  /* [4190] */
    (xdc_Char)0x78,  /* [4191] */
    (xdc_Char)0x63,  /* [4192] */
    (xdc_Char)0x65,  /* [4193] */
    (xdc_Char)0x70,  /* [4194] */
    (xdc_Char)0x74,  /* [4195] */
    (xdc_Char)0x69,  /* [4196] */
    (xdc_Char)0x6f,  /* [4197] */
    (xdc_Char)0x6e,  /* [4198] */
    (xdc_Char)0x4d,  /* [4199] */
    (xdc_Char)0x61,  /* [4200] */
    (xdc_Char)0x78,  /* [4201] */
    (xdc_Char)0x3a,  /* [4202] */
    (xdc_Char)0x20,  /* [4203] */
    (xdc_Char)0x70,  /* [4204] */
    (xdc_Char)0x63,  /* [4205] */
    (xdc_Char)0x20,  /* [4206] */
    (xdc_Char)0x3d,  /* [4207] */
    (xdc_Char)0x20,  /* [4208] */
    (xdc_Char)0x30,  /* [4209] */
    (xdc_Char)0x78,  /* [4210] */
    (xdc_Char)0x25,  /* [4211] */
    (xdc_Char)0x30,  /* [4212] */
    (xdc_Char)0x38,  /* [4213] */
    (xdc_Char)0x78,  /* [4214] */
    (xdc_Char)0x2c,  /* [4215] */
    (xdc_Char)0x20,  /* [4216] */
    (xdc_Char)0x73,  /* [4217] */
    (xdc_Char)0x70,  /* [4218] */
    (xdc_Char)0x20,  /* [4219] */
    (xdc_Char)0x3d,  /* [4220] */
    (xdc_Char)0x20,  /* [4221] */
    (xdc_Char)0x30,  /* [4222] */
    (xdc_Char)0x78,  /* [4223] */
    (xdc_Char)0x25,  /* [4224] */
    (xdc_Char)0x30,  /* [4225] */
    (xdc_Char)0x38,  /* [4226] */
    (xdc_Char)0x78,  /* [4227] */
    (xdc_Char)0x2e,  /* [4228] */
    (xdc_Char)0x0,  /* [4229] */
    (xdc_Char)0x45,  /* [4230] */
    (xdc_Char)0x5f,  /* [4231] */
    (xdc_Char)0x61,  /* [4232] */
    (xdc_Char)0x6c,  /* [4233] */
    (xdc_Char)0x72,  /* [4234] */
    (xdc_Char)0x65,  /* [4235] */
    (xdc_Char)0x61,  /* [4236] */
    (xdc_Char)0x64,  /* [4237] */
    (xdc_Char)0x79,  /* [4238] */
    (xdc_Char)0x44,  /* [4239] */
    (xdc_Char)0x65,  /* [4240] */
    (xdc_Char)0x66,  /* [4241] */
    (xdc_Char)0x69,  /* [4242] */
    (xdc_Char)0x6e,  /* [4243] */
    (xdc_Char)0x65,  /* [4244] */
    (xdc_Char)0x64,  /* [4245] */
    (xdc_Char)0x3a,  /* [4246] */
    (xdc_Char)0x20,  /* [4247] */
    (xdc_Char)0x48,  /* [4248] */
    (xdc_Char)0x77,  /* [4249] */
    (xdc_Char)0x69,  /* [4250] */
    (xdc_Char)0x20,  /* [4251] */
    (xdc_Char)0x61,  /* [4252] */
    (xdc_Char)0x6c,  /* [4253] */
    (xdc_Char)0x72,  /* [4254] */
    (xdc_Char)0x65,  /* [4255] */
    (xdc_Char)0x61,  /* [4256] */
    (xdc_Char)0x64,  /* [4257] */
    (xdc_Char)0x79,  /* [4258] */
    (xdc_Char)0x20,  /* [4259] */
    (xdc_Char)0x64,  /* [4260] */
    (xdc_Char)0x65,  /* [4261] */
    (xdc_Char)0x66,  /* [4262] */
    (xdc_Char)0x69,  /* [4263] */
    (xdc_Char)0x6e,  /* [4264] */
    (xdc_Char)0x65,  /* [4265] */
    (xdc_Char)0x64,  /* [4266] */
    (xdc_Char)0x3a,  /* [4267] */
    (xdc_Char)0x20,  /* [4268] */
    (xdc_Char)0x69,  /* [4269] */
    (xdc_Char)0x6e,  /* [4270] */
    (xdc_Char)0x74,  /* [4271] */
    (xdc_Char)0x72,  /* [4272] */
    (xdc_Char)0x23,  /* [4273] */
    (xdc_Char)0x20,  /* [4274] */
    (xdc_Char)0x25,  /* [4275] */
    (xdc_Char)0x64,  /* [4276] */
    (xdc_Char)0x0,  /* [4277] */
    (xdc_Char)0x45,  /* [4278] */
    (xdc_Char)0x5f,  /* [4279] */
    (xdc_Char)0x68,  /* [4280] */
    (xdc_Char)0x61,  /* [4281] */
    (xdc_Char)0x6e,  /* [4282] */
    (xdc_Char)0x64,  /* [4283] */
    (xdc_Char)0x6c,  /* [4284] */
    (xdc_Char)0x65,  /* [4285] */
    (xdc_Char)0x4e,  /* [4286] */
    (xdc_Char)0x6f,  /* [4287] */
    (xdc_Char)0x74,  /* [4288] */
    (xdc_Char)0x46,  /* [4289] */
    (xdc_Char)0x6f,  /* [4290] */
    (xdc_Char)0x75,  /* [4291] */
    (xdc_Char)0x6e,  /* [4292] */
    (xdc_Char)0x64,  /* [4293] */
    (xdc_Char)0x3a,  /* [4294] */
    (xdc_Char)0x20,  /* [4295] */
    (xdc_Char)0x48,  /* [4296] */
    (xdc_Char)0x77,  /* [4297] */
    (xdc_Char)0x69,  /* [4298] */
    (xdc_Char)0x20,  /* [4299] */
    (xdc_Char)0x68,  /* [4300] */
    (xdc_Char)0x61,  /* [4301] */
    (xdc_Char)0x6e,  /* [4302] */
    (xdc_Char)0x64,  /* [4303] */
    (xdc_Char)0x6c,  /* [4304] */
    (xdc_Char)0x65,  /* [4305] */
    (xdc_Char)0x20,  /* [4306] */
    (xdc_Char)0x6e,  /* [4307] */
    (xdc_Char)0x6f,  /* [4308] */
    (xdc_Char)0x74,  /* [4309] */
    (xdc_Char)0x20,  /* [4310] */
    (xdc_Char)0x66,  /* [4311] */
    (xdc_Char)0x6f,  /* [4312] */
    (xdc_Char)0x75,  /* [4313] */
    (xdc_Char)0x6e,  /* [4314] */
    (xdc_Char)0x64,  /* [4315] */
    (xdc_Char)0x3a,  /* [4316] */
    (xdc_Char)0x20,  /* [4317] */
    (xdc_Char)0x30,  /* [4318] */
    (xdc_Char)0x78,  /* [4319] */
    (xdc_Char)0x25,  /* [4320] */
    (xdc_Char)0x78,  /* [4321] */
    (xdc_Char)0x0,  /* [4322] */
    (xdc_Char)0x45,  /* [4323] */
    (xdc_Char)0x5f,  /* [4324] */
    (xdc_Char)0x61,  /* [4325] */
    (xdc_Char)0x6c,  /* [4326] */
    (xdc_Char)0x6c,  /* [4327] */
    (xdc_Char)0x6f,  /* [4328] */
    (xdc_Char)0x63,  /* [4329] */
    (xdc_Char)0x53,  /* [4330] */
    (xdc_Char)0x43,  /* [4331] */
    (xdc_Char)0x46,  /* [4332] */
    (xdc_Char)0x61,  /* [4333] */
    (xdc_Char)0x69,  /* [4334] */
    (xdc_Char)0x6c,  /* [4335] */
    (xdc_Char)0x65,  /* [4336] */
    (xdc_Char)0x64,  /* [4337] */
    (xdc_Char)0x3a,  /* [4338] */
    (xdc_Char)0x20,  /* [4339] */
    (xdc_Char)0x41,  /* [4340] */
    (xdc_Char)0x6c,  /* [4341] */
    (xdc_Char)0x6c,  /* [4342] */
    (xdc_Char)0x6f,  /* [4343] */
    (xdc_Char)0x63,  /* [4344] */
    (xdc_Char)0x20,  /* [4345] */
    (xdc_Char)0x73,  /* [4346] */
    (xdc_Char)0x65,  /* [4347] */
    (xdc_Char)0x63,  /* [4348] */
    (xdc_Char)0x75,  /* [4349] */
    (xdc_Char)0x72,  /* [4350] */
    (xdc_Char)0x65,  /* [4351] */
    (xdc_Char)0x20,  /* [4352] */
    (xdc_Char)0x63,  /* [4353] */
    (xdc_Char)0x6f,  /* [4354] */
    (xdc_Char)0x6e,  /* [4355] */
    (xdc_Char)0x74,  /* [4356] */
    (xdc_Char)0x65,  /* [4357] */
    (xdc_Char)0x78,  /* [4358] */
    (xdc_Char)0x74,  /* [4359] */
    (xdc_Char)0x20,  /* [4360] */
    (xdc_Char)0x66,  /* [4361] */
    (xdc_Char)0x61,  /* [4362] */
    (xdc_Char)0x69,  /* [4363] */
    (xdc_Char)0x6c,  /* [4364] */
    (xdc_Char)0x65,  /* [4365] */
    (xdc_Char)0x64,  /* [4366] */
    (xdc_Char)0x0,  /* [4367] */
    (xdc_Char)0x45,  /* [4368] */
    (xdc_Char)0x5f,  /* [4369] */
    (xdc_Char)0x72,  /* [4370] */
    (xdc_Char)0x65,  /* [4371] */
    (xdc_Char)0x67,  /* [4372] */
    (xdc_Char)0x69,  /* [4373] */
    (xdc_Char)0x73,  /* [4374] */
    (xdc_Char)0x74,  /* [4375] */
    (xdc_Char)0x65,  /* [4376] */
    (xdc_Char)0x72,  /* [4377] */
    (xdc_Char)0x53,  /* [4378] */
    (xdc_Char)0x43,  /* [4379] */
    (xdc_Char)0x46,  /* [4380] */
    (xdc_Char)0x61,  /* [4381] */
    (xdc_Char)0x69,  /* [4382] */
    (xdc_Char)0x6c,  /* [4383] */
    (xdc_Char)0x65,  /* [4384] */
    (xdc_Char)0x64,  /* [4385] */
    (xdc_Char)0x3a,  /* [4386] */
    (xdc_Char)0x20,  /* [4387] */
    (xdc_Char)0x52,  /* [4388] */
    (xdc_Char)0x65,  /* [4389] */
    (xdc_Char)0x67,  /* [4390] */
    (xdc_Char)0x69,  /* [4391] */
    (xdc_Char)0x73,  /* [4392] */
    (xdc_Char)0x74,  /* [4393] */
    (xdc_Char)0x65,  /* [4394] */
    (xdc_Char)0x72,  /* [4395] */
    (xdc_Char)0x20,  /* [4396] */
    (xdc_Char)0x73,  /* [4397] */
    (xdc_Char)0x65,  /* [4398] */
    (xdc_Char)0x63,  /* [4399] */
    (xdc_Char)0x75,  /* [4400] */
    (xdc_Char)0x72,  /* [4401] */
    (xdc_Char)0x65,  /* [4402] */
    (xdc_Char)0x20,  /* [4403] */
    (xdc_Char)0x63,  /* [4404] */
    (xdc_Char)0x6f,  /* [4405] */
    (xdc_Char)0x6e,  /* [4406] */
    (xdc_Char)0x74,  /* [4407] */
    (xdc_Char)0x65,  /* [4408] */
    (xdc_Char)0x78,  /* [4409] */
    (xdc_Char)0x74,  /* [4410] */
    (xdc_Char)0x20,  /* [4411] */
    (xdc_Char)0x66,  /* [4412] */
    (xdc_Char)0x61,  /* [4413] */
    (xdc_Char)0x69,  /* [4414] */
    (xdc_Char)0x6c,  /* [4415] */
    (xdc_Char)0x65,  /* [4416] */
    (xdc_Char)0x64,  /* [4417] */
    (xdc_Char)0x0,  /* [4418] */
    (xdc_Char)0x45,  /* [4419] */
    (xdc_Char)0x5f,  /* [4420] */
    (xdc_Char)0x69,  /* [4421] */
    (xdc_Char)0x6e,  /* [4422] */
    (xdc_Char)0x76,  /* [4423] */
    (xdc_Char)0x61,  /* [4424] */
    (xdc_Char)0x6c,  /* [4425] */
    (xdc_Char)0x69,  /* [4426] */
    (xdc_Char)0x64,  /* [4427] */
    (xdc_Char)0x49,  /* [4428] */
    (xdc_Char)0x6e,  /* [4429] */
    (xdc_Char)0x74,  /* [4430] */
    (xdc_Char)0x4e,  /* [4431] */
    (xdc_Char)0x75,  /* [4432] */
    (xdc_Char)0x6d,  /* [4433] */
    (xdc_Char)0x3a,  /* [4434] */
    (xdc_Char)0x20,  /* [4435] */
    (xdc_Char)0x49,  /* [4436] */
    (xdc_Char)0x6e,  /* [4437] */
    (xdc_Char)0x76,  /* [4438] */
    (xdc_Char)0x61,  /* [4439] */
    (xdc_Char)0x6c,  /* [4440] */
    (xdc_Char)0x69,  /* [4441] */
    (xdc_Char)0x64,  /* [4442] */
    (xdc_Char)0x20,  /* [4443] */
    (xdc_Char)0x69,  /* [4444] */
    (xdc_Char)0x6e,  /* [4445] */
    (xdc_Char)0x74,  /* [4446] */
    (xdc_Char)0x65,  /* [4447] */
    (xdc_Char)0x72,  /* [4448] */
    (xdc_Char)0x72,  /* [4449] */
    (xdc_Char)0x75,  /* [4450] */
    (xdc_Char)0x70,  /* [4451] */
    (xdc_Char)0x74,  /* [4452] */
    (xdc_Char)0x20,  /* [4453] */
    (xdc_Char)0x6e,  /* [4454] */
    (xdc_Char)0x75,  /* [4455] */
    (xdc_Char)0x6d,  /* [4456] */
    (xdc_Char)0x62,  /* [4457] */
    (xdc_Char)0x65,  /* [4458] */
    (xdc_Char)0x72,  /* [4459] */
    (xdc_Char)0x3a,  /* [4460] */
    (xdc_Char)0x20,  /* [4461] */
    (xdc_Char)0x69,  /* [4462] */
    (xdc_Char)0x6e,  /* [4463] */
    (xdc_Char)0x74,  /* [4464] */
    (xdc_Char)0x72,  /* [4465] */
    (xdc_Char)0x23,  /* [4466] */
    (xdc_Char)0x20,  /* [4467] */
    (xdc_Char)0x25,  /* [4468] */
    (xdc_Char)0x64,  /* [4469] */
    (xdc_Char)0x0,  /* [4470] */
    (xdc_Char)0x45,  /* [4471] */
    (xdc_Char)0x5f,  /* [4472] */
    (xdc_Char)0x69,  /* [4473] */
    (xdc_Char)0x6e,  /* [4474] */
    (xdc_Char)0x76,  /* [4475] */
    (xdc_Char)0x61,  /* [4476] */
    (xdc_Char)0x6c,  /* [4477] */
    (xdc_Char)0x69,  /* [4478] */
    (xdc_Char)0x64,  /* [4479] */
    (xdc_Char)0x50,  /* [4480] */
    (xdc_Char)0x72,  /* [4481] */
    (xdc_Char)0x69,  /* [4482] */
    (xdc_Char)0x6f,  /* [4483] */
    (xdc_Char)0x72,  /* [4484] */
    (xdc_Char)0x69,  /* [4485] */
    (xdc_Char)0x74,  /* [4486] */
    (xdc_Char)0x79,  /* [4487] */
    (xdc_Char)0x3a,  /* [4488] */
    (xdc_Char)0x20,  /* [4489] */
    (xdc_Char)0x49,  /* [4490] */
    (xdc_Char)0x6e,  /* [4491] */
    (xdc_Char)0x76,  /* [4492] */
    (xdc_Char)0x61,  /* [4493] */
    (xdc_Char)0x6c,  /* [4494] */
    (xdc_Char)0x69,  /* [4495] */
    (xdc_Char)0x64,  /* [4496] */
    (xdc_Char)0x20,  /* [4497] */
    (xdc_Char)0x70,  /* [4498] */
    (xdc_Char)0x72,  /* [4499] */
    (xdc_Char)0x69,  /* [4500] */
    (xdc_Char)0x6f,  /* [4501] */
    (xdc_Char)0x72,  /* [4502] */
    (xdc_Char)0x69,  /* [4503] */
    (xdc_Char)0x74,  /* [4504] */
    (xdc_Char)0x79,  /* [4505] */
    (xdc_Char)0x3a,  /* [4506] */
    (xdc_Char)0x20,  /* [4507] */
    (xdc_Char)0x25,  /* [4508] */
    (xdc_Char)0x64,  /* [4509] */
    (xdc_Char)0x20,  /* [4510] */
    (xdc_Char)0x28,  /* [4511] */
    (xdc_Char)0x73,  /* [4512] */
    (xdc_Char)0x68,  /* [4513] */
    (xdc_Char)0x6f,  /* [4514] */
    (xdc_Char)0x75,  /* [4515] */
    (xdc_Char)0x6c,  /* [4516] */
    (xdc_Char)0x64,  /* [4517] */
    (xdc_Char)0x20,  /* [4518] */
    (xdc_Char)0x62,  /* [4519] */
    (xdc_Char)0x65,  /* [4520] */
    (xdc_Char)0x20,  /* [4521] */
    (xdc_Char)0x31,  /* [4522] */
    (xdc_Char)0x2d,  /* [4523] */
    (xdc_Char)0x37,  /* [4524] */
    (xdc_Char)0x29,  /* [4525] */
    (xdc_Char)0x0,  /* [4526] */
    (xdc_Char)0x45,  /* [4527] */
    (xdc_Char)0x5f,  /* [4528] */
    (xdc_Char)0x72,  /* [4529] */
    (xdc_Char)0x65,  /* [4530] */
    (xdc_Char)0x71,  /* [4531] */
    (xdc_Char)0x75,  /* [4532] */
    (xdc_Char)0x65,  /* [4533] */
    (xdc_Char)0x73,  /* [4534] */
    (xdc_Char)0x74,  /* [4535] */
    (xdc_Char)0x46,  /* [4536] */
    (xdc_Char)0x61,  /* [4537] */
    (xdc_Char)0x69,  /* [4538] */
    (xdc_Char)0x6c,  /* [4539] */
    (xdc_Char)0x65,  /* [4540] */
    (xdc_Char)0x64,  /* [4541] */
    (xdc_Char)0x3a,  /* [4542] */
    (xdc_Char)0x20,  /* [4543] */
    (xdc_Char)0x52,  /* [4544] */
    (xdc_Char)0x65,  /* [4545] */
    (xdc_Char)0x71,  /* [4546] */
    (xdc_Char)0x75,  /* [4547] */
    (xdc_Char)0x65,  /* [4548] */
    (xdc_Char)0x73,  /* [4549] */
    (xdc_Char)0x74,  /* [4550] */
    (xdc_Char)0x20,  /* [4551] */
    (xdc_Char)0x28,  /* [4552] */
    (xdc_Char)0x53,  /* [4553] */
    (xdc_Char)0x59,  /* [4554] */
    (xdc_Char)0x53,  /* [4555] */
    (xdc_Char)0x43,  /* [4556] */
    (xdc_Char)0x41,  /* [4557] */
    (xdc_Char)0x4c,  /* [4558] */
    (xdc_Char)0x4c,  /* [4559] */
    (xdc_Char)0x20,  /* [4560] */
    (xdc_Char)0x23,  /* [4561] */
    (xdc_Char)0x25,  /* [4562] */
    (xdc_Char)0x64,  /* [4563] */
    (xdc_Char)0x29,  /* [4564] */
    (xdc_Char)0x20,  /* [4565] */
    (xdc_Char)0x74,  /* [4566] */
    (xdc_Char)0x6f,  /* [4567] */
    (xdc_Char)0x20,  /* [4568] */
    (xdc_Char)0x73,  /* [4569] */
    (xdc_Char)0x77,  /* [4570] */
    (xdc_Char)0x69,  /* [4571] */
    (xdc_Char)0x74,  /* [4572] */
    (xdc_Char)0x63,  /* [4573] */
    (xdc_Char)0x68,  /* [4574] */
    (xdc_Char)0x20,  /* [4575] */
    (xdc_Char)0x43,  /* [4576] */
    (xdc_Char)0x50,  /* [4577] */
    (xdc_Char)0x55,  /* [4578] */
    (xdc_Char)0x20,  /* [4579] */
    (xdc_Char)0x70,  /* [4580] */
    (xdc_Char)0x72,  /* [4581] */
    (xdc_Char)0x69,  /* [4582] */
    (xdc_Char)0x76,  /* [4583] */
    (xdc_Char)0x69,  /* [4584] */
    (xdc_Char)0x6c,  /* [4585] */
    (xdc_Char)0x65,  /* [4586] */
    (xdc_Char)0x67,  /* [4587] */
    (xdc_Char)0x65,  /* [4588] */
    (xdc_Char)0x20,  /* [4589] */
    (xdc_Char)0x64,  /* [4590] */
    (xdc_Char)0x65,  /* [4591] */
    (xdc_Char)0x6e,  /* [4592] */
    (xdc_Char)0x69,  /* [4593] */
    (xdc_Char)0x65,  /* [4594] */
    (xdc_Char)0x64,  /* [4595] */
    (xdc_Char)0x2e,  /* [4596] */
    (xdc_Char)0x0,  /* [4597] */
    (xdc_Char)0x45,  /* [4598] */
    (xdc_Char)0x5f,  /* [4599] */
    (xdc_Char)0x6f,  /* [4600] */
    (xdc_Char)0x62,  /* [4601] */
    (xdc_Char)0x6a,  /* [4602] */
    (xdc_Char)0x65,  /* [4603] */
    (xdc_Char)0x63,  /* [4604] */
    (xdc_Char)0x74,  /* [4605] */
    (xdc_Char)0x4e,  /* [4606] */
    (xdc_Char)0x6f,  /* [4607] */
    (xdc_Char)0x74,  /* [4608] */
    (xdc_Char)0x49,  /* [4609] */
    (xdc_Char)0x6e,  /* [4610] */
    (xdc_Char)0x4b,  /* [4611] */
    (xdc_Char)0x65,  /* [4612] */
    (xdc_Char)0x72,  /* [4613] */
    (xdc_Char)0x6e,  /* [4614] */
    (xdc_Char)0x65,  /* [4615] */
    (xdc_Char)0x6c,  /* [4616] */
    (xdc_Char)0x53,  /* [4617] */
    (xdc_Char)0x70,  /* [4618] */
    (xdc_Char)0x61,  /* [4619] */
    (xdc_Char)0x63,  /* [4620] */
    (xdc_Char)0x65,  /* [4621] */
    (xdc_Char)0x3a,  /* [4622] */
    (xdc_Char)0x20,  /* [4623] */
    (xdc_Char)0x53,  /* [4624] */
    (xdc_Char)0x65,  /* [4625] */
    (xdc_Char)0x6d,  /* [4626] */
    (xdc_Char)0x61,  /* [4627] */
    (xdc_Char)0x70,  /* [4628] */
    (xdc_Char)0x68,  /* [4629] */
    (xdc_Char)0x6f,  /* [4630] */
    (xdc_Char)0x72,  /* [4631] */
    (xdc_Char)0x65,  /* [4632] */
    (xdc_Char)0x20,  /* [4633] */
    (xdc_Char)0x6f,  /* [4634] */
    (xdc_Char)0x62,  /* [4635] */
    (xdc_Char)0x6a,  /* [4636] */
    (xdc_Char)0x65,  /* [4637] */
    (xdc_Char)0x63,  /* [4638] */
    (xdc_Char)0x74,  /* [4639] */
    (xdc_Char)0x20,  /* [4640] */
    (xdc_Char)0x70,  /* [4641] */
    (xdc_Char)0x61,  /* [4642] */
    (xdc_Char)0x73,  /* [4643] */
    (xdc_Char)0x73,  /* [4644] */
    (xdc_Char)0x65,  /* [4645] */
    (xdc_Char)0x64,  /* [4646] */
    (xdc_Char)0x20,  /* [4647] */
    (xdc_Char)0x6e,  /* [4648] */
    (xdc_Char)0x6f,  /* [4649] */
    (xdc_Char)0x74,  /* [4650] */
    (xdc_Char)0x20,  /* [4651] */
    (xdc_Char)0x69,  /* [4652] */
    (xdc_Char)0x6e,  /* [4653] */
    (xdc_Char)0x20,  /* [4654] */
    (xdc_Char)0x4b,  /* [4655] */
    (xdc_Char)0x65,  /* [4656] */
    (xdc_Char)0x72,  /* [4657] */
    (xdc_Char)0x6e,  /* [4658] */
    (xdc_Char)0x65,  /* [4659] */
    (xdc_Char)0x6c,  /* [4660] */
    (xdc_Char)0x20,  /* [4661] */
    (xdc_Char)0x61,  /* [4662] */
    (xdc_Char)0x64,  /* [4663] */
    (xdc_Char)0x64,  /* [4664] */
    (xdc_Char)0x72,  /* [4665] */
    (xdc_Char)0x65,  /* [4666] */
    (xdc_Char)0x73,  /* [4667] */
    (xdc_Char)0x73,  /* [4668] */
    (xdc_Char)0x20,  /* [4669] */
    (xdc_Char)0x73,  /* [4670] */
    (xdc_Char)0x70,  /* [4671] */
    (xdc_Char)0x61,  /* [4672] */
    (xdc_Char)0x63,  /* [4673] */
    (xdc_Char)0x65,  /* [4674] */
    (xdc_Char)0x2e,  /* [4675] */
    (xdc_Char)0x0,  /* [4676] */
    (xdc_Char)0x45,  /* [4677] */
    (xdc_Char)0x5f,  /* [4678] */
    (xdc_Char)0x73,  /* [4679] */
    (xdc_Char)0x74,  /* [4680] */
    (xdc_Char)0x61,  /* [4681] */
    (xdc_Char)0x63,  /* [4682] */
    (xdc_Char)0x6b,  /* [4683] */
    (xdc_Char)0x4f,  /* [4684] */
    (xdc_Char)0x76,  /* [4685] */
    (xdc_Char)0x65,  /* [4686] */
    (xdc_Char)0x72,  /* [4687] */
    (xdc_Char)0x66,  /* [4688] */
    (xdc_Char)0x6c,  /* [4689] */
    (xdc_Char)0x6f,  /* [4690] */
    (xdc_Char)0x77,  /* [4691] */
    (xdc_Char)0x3a,  /* [4692] */
    (xdc_Char)0x20,  /* [4693] */
    (xdc_Char)0x54,  /* [4694] */
    (xdc_Char)0x61,  /* [4695] */
    (xdc_Char)0x73,  /* [4696] */
    (xdc_Char)0x6b,  /* [4697] */
    (xdc_Char)0x20,  /* [4698] */
    (xdc_Char)0x30,  /* [4699] */
    (xdc_Char)0x78,  /* [4700] */
    (xdc_Char)0x25,  /* [4701] */
    (xdc_Char)0x78,  /* [4702] */
    (xdc_Char)0x20,  /* [4703] */
    (xdc_Char)0x73,  /* [4704] */
    (xdc_Char)0x74,  /* [4705] */
    (xdc_Char)0x61,  /* [4706] */
    (xdc_Char)0x63,  /* [4707] */
    (xdc_Char)0x6b,  /* [4708] */
    (xdc_Char)0x20,  /* [4709] */
    (xdc_Char)0x6f,  /* [4710] */
    (xdc_Char)0x76,  /* [4711] */
    (xdc_Char)0x65,  /* [4712] */
    (xdc_Char)0x72,  /* [4713] */
    (xdc_Char)0x66,  /* [4714] */
    (xdc_Char)0x6c,  /* [4715] */
    (xdc_Char)0x6f,  /* [4716] */
    (xdc_Char)0x77,  /* [4717] */
    (xdc_Char)0x2e,  /* [4718] */
    (xdc_Char)0x0,  /* [4719] */
    (xdc_Char)0x45,  /* [4720] */
    (xdc_Char)0x5f,  /* [4721] */
    (xdc_Char)0x73,  /* [4722] */
    (xdc_Char)0x70,  /* [4723] */
    (xdc_Char)0x4f,  /* [4724] */
    (xdc_Char)0x75,  /* [4725] */
    (xdc_Char)0x74,  /* [4726] */
    (xdc_Char)0x4f,  /* [4727] */
    (xdc_Char)0x66,  /* [4728] */
    (xdc_Char)0x42,  /* [4729] */
    (xdc_Char)0x6f,  /* [4730] */
    (xdc_Char)0x75,  /* [4731] */
    (xdc_Char)0x6e,  /* [4732] */
    (xdc_Char)0x64,  /* [4733] */
    (xdc_Char)0x73,  /* [4734] */
    (xdc_Char)0x3a,  /* [4735] */
    (xdc_Char)0x20,  /* [4736] */
    (xdc_Char)0x54,  /* [4737] */
    (xdc_Char)0x61,  /* [4738] */
    (xdc_Char)0x73,  /* [4739] */
    (xdc_Char)0x6b,  /* [4740] */
    (xdc_Char)0x20,  /* [4741] */
    (xdc_Char)0x30,  /* [4742] */
    (xdc_Char)0x78,  /* [4743] */
    (xdc_Char)0x25,  /* [4744] */
    (xdc_Char)0x78,  /* [4745] */
    (xdc_Char)0x20,  /* [4746] */
    (xdc_Char)0x73,  /* [4747] */
    (xdc_Char)0x74,  /* [4748] */
    (xdc_Char)0x61,  /* [4749] */
    (xdc_Char)0x63,  /* [4750] */
    (xdc_Char)0x6b,  /* [4751] */
    (xdc_Char)0x20,  /* [4752] */
    (xdc_Char)0x65,  /* [4753] */
    (xdc_Char)0x72,  /* [4754] */
    (xdc_Char)0x72,  /* [4755] */
    (xdc_Char)0x6f,  /* [4756] */
    (xdc_Char)0x72,  /* [4757] */
    (xdc_Char)0x2c,  /* [4758] */
    (xdc_Char)0x20,  /* [4759] */
    (xdc_Char)0x53,  /* [4760] */
    (xdc_Char)0x50,  /* [4761] */
    (xdc_Char)0x20,  /* [4762] */
    (xdc_Char)0x3d,  /* [4763] */
    (xdc_Char)0x20,  /* [4764] */
    (xdc_Char)0x30,  /* [4765] */
    (xdc_Char)0x78,  /* [4766] */
    (xdc_Char)0x25,  /* [4767] */
    (xdc_Char)0x78,  /* [4768] */
    (xdc_Char)0x2e,  /* [4769] */
    (xdc_Char)0x0,  /* [4770] */
    (xdc_Char)0x45,  /* [4771] */
    (xdc_Char)0x5f,  /* [4772] */
    (xdc_Char)0x64,  /* [4773] */
    (xdc_Char)0x65,  /* [4774] */
    (xdc_Char)0x6c,  /* [4775] */
    (xdc_Char)0x65,  /* [4776] */
    (xdc_Char)0x74,  /* [4777] */
    (xdc_Char)0x65,  /* [4778] */
    (xdc_Char)0x4e,  /* [4779] */
    (xdc_Char)0x6f,  /* [4780] */
    (xdc_Char)0x74,  /* [4781] */
    (xdc_Char)0x41,  /* [4782] */
    (xdc_Char)0x6c,  /* [4783] */
    (xdc_Char)0x6c,  /* [4784] */
    (xdc_Char)0x6f,  /* [4785] */
    (xdc_Char)0x77,  /* [4786] */
    (xdc_Char)0x65,  /* [4787] */
    (xdc_Char)0x64,  /* [4788] */
    (xdc_Char)0x3a,  /* [4789] */
    (xdc_Char)0x20,  /* [4790] */
    (xdc_Char)0x54,  /* [4791] */
    (xdc_Char)0x61,  /* [4792] */
    (xdc_Char)0x73,  /* [4793] */
    (xdc_Char)0x6b,  /* [4794] */
    (xdc_Char)0x20,  /* [4795] */
    (xdc_Char)0x30,  /* [4796] */
    (xdc_Char)0x78,  /* [4797] */
    (xdc_Char)0x25,  /* [4798] */
    (xdc_Char)0x78,  /* [4799] */
    (xdc_Char)0x2e,  /* [4800] */
    (xdc_Char)0x0,  /* [4801] */
    (xdc_Char)0x45,  /* [4802] */
    (xdc_Char)0x5f,  /* [4803] */
    (xdc_Char)0x6d,  /* [4804] */
    (xdc_Char)0x6f,  /* [4805] */
    (xdc_Char)0x64,  /* [4806] */
    (xdc_Char)0x75,  /* [4807] */
    (xdc_Char)0x6c,  /* [4808] */
    (xdc_Char)0x65,  /* [4809] */
    (xdc_Char)0x53,  /* [4810] */
    (xdc_Char)0x74,  /* [4811] */
    (xdc_Char)0x61,  /* [4812] */
    (xdc_Char)0x74,  /* [4813] */
    (xdc_Char)0x65,  /* [4814] */
    (xdc_Char)0x43,  /* [4815] */
    (xdc_Char)0x68,  /* [4816] */
    (xdc_Char)0x65,  /* [4817] */
    (xdc_Char)0x63,  /* [4818] */
    (xdc_Char)0x6b,  /* [4819] */
    (xdc_Char)0x46,  /* [4820] */
    (xdc_Char)0x61,  /* [4821] */
    (xdc_Char)0x69,  /* [4822] */
    (xdc_Char)0x6c,  /* [4823] */
    (xdc_Char)0x65,  /* [4824] */
    (xdc_Char)0x64,  /* [4825] */
    (xdc_Char)0x3a,  /* [4826] */
    (xdc_Char)0x20,  /* [4827] */
    (xdc_Char)0x54,  /* [4828] */
    (xdc_Char)0x61,  /* [4829] */
    (xdc_Char)0x73,  /* [4830] */
    (xdc_Char)0x6b,  /* [4831] */
    (xdc_Char)0x20,  /* [4832] */
    (xdc_Char)0x6d,  /* [4833] */
    (xdc_Char)0x6f,  /* [4834] */
    (xdc_Char)0x64,  /* [4835] */
    (xdc_Char)0x75,  /* [4836] */
    (xdc_Char)0x6c,  /* [4837] */
    (xdc_Char)0x65,  /* [4838] */
    (xdc_Char)0x20,  /* [4839] */
    (xdc_Char)0x73,  /* [4840] */
    (xdc_Char)0x74,  /* [4841] */
    (xdc_Char)0x61,  /* [4842] */
    (xdc_Char)0x74,  /* [4843] */
    (xdc_Char)0x65,  /* [4844] */
    (xdc_Char)0x20,  /* [4845] */
    (xdc_Char)0x64,  /* [4846] */
    (xdc_Char)0x61,  /* [4847] */
    (xdc_Char)0x74,  /* [4848] */
    (xdc_Char)0x61,  /* [4849] */
    (xdc_Char)0x20,  /* [4850] */
    (xdc_Char)0x69,  /* [4851] */
    (xdc_Char)0x6e,  /* [4852] */
    (xdc_Char)0x74,  /* [4853] */
    (xdc_Char)0x65,  /* [4854] */
    (xdc_Char)0x67,  /* [4855] */
    (xdc_Char)0x72,  /* [4856] */
    (xdc_Char)0x69,  /* [4857] */
    (xdc_Char)0x74,  /* [4858] */
    (xdc_Char)0x79,  /* [4859] */
    (xdc_Char)0x20,  /* [4860] */
    (xdc_Char)0x63,  /* [4861] */
    (xdc_Char)0x68,  /* [4862] */
    (xdc_Char)0x65,  /* [4863] */
    (xdc_Char)0x63,  /* [4864] */
    (xdc_Char)0x6b,  /* [4865] */
    (xdc_Char)0x20,  /* [4866] */
    (xdc_Char)0x66,  /* [4867] */
    (xdc_Char)0x61,  /* [4868] */
    (xdc_Char)0x69,  /* [4869] */
    (xdc_Char)0x6c,  /* [4870] */
    (xdc_Char)0x65,  /* [4871] */
    (xdc_Char)0x64,  /* [4872] */
    (xdc_Char)0x2e,  /* [4873] */
    (xdc_Char)0x0,  /* [4874] */
    (xdc_Char)0x45,  /* [4875] */
    (xdc_Char)0x5f,  /* [4876] */
    (xdc_Char)0x6f,  /* [4877] */
    (xdc_Char)0x62,  /* [4878] */
    (xdc_Char)0x6a,  /* [4879] */
    (xdc_Char)0x65,  /* [4880] */
    (xdc_Char)0x63,  /* [4881] */
    (xdc_Char)0x74,  /* [4882] */
    (xdc_Char)0x43,  /* [4883] */
    (xdc_Char)0x68,  /* [4884] */
    (xdc_Char)0x65,  /* [4885] */
    (xdc_Char)0x63,  /* [4886] */
    (xdc_Char)0x6b,  /* [4887] */
    (xdc_Char)0x46,  /* [4888] */
    (xdc_Char)0x61,  /* [4889] */
    (xdc_Char)0x69,  /* [4890] */
    (xdc_Char)0x6c,  /* [4891] */
    (xdc_Char)0x65,  /* [4892] */
    (xdc_Char)0x64,  /* [4893] */
    (xdc_Char)0x3a,  /* [4894] */
    (xdc_Char)0x20,  /* [4895] */
    (xdc_Char)0x54,  /* [4896] */
    (xdc_Char)0x61,  /* [4897] */
    (xdc_Char)0x73,  /* [4898] */
    (xdc_Char)0x6b,  /* [4899] */
    (xdc_Char)0x20,  /* [4900] */
    (xdc_Char)0x30,  /* [4901] */
    (xdc_Char)0x78,  /* [4902] */
    (xdc_Char)0x25,  /* [4903] */
    (xdc_Char)0x78,  /* [4904] */
    (xdc_Char)0x20,  /* [4905] */
    (xdc_Char)0x6f,  /* [4906] */
    (xdc_Char)0x62,  /* [4907] */
    (xdc_Char)0x6a,  /* [4908] */
    (xdc_Char)0x65,  /* [4909] */
    (xdc_Char)0x63,  /* [4910] */
    (xdc_Char)0x74,  /* [4911] */
    (xdc_Char)0x20,  /* [4912] */
    (xdc_Char)0x64,  /* [4913] */
    (xdc_Char)0x61,  /* [4914] */
    (xdc_Char)0x74,  /* [4915] */
    (xdc_Char)0x61,  /* [4916] */
    (xdc_Char)0x20,  /* [4917] */
    (xdc_Char)0x69,  /* [4918] */
    (xdc_Char)0x6e,  /* [4919] */
    (xdc_Char)0x74,  /* [4920] */
    (xdc_Char)0x65,  /* [4921] */
    (xdc_Char)0x67,  /* [4922] */
    (xdc_Char)0x72,  /* [4923] */
    (xdc_Char)0x69,  /* [4924] */
    (xdc_Char)0x74,  /* [4925] */
    (xdc_Char)0x79,  /* [4926] */
    (xdc_Char)0x20,  /* [4927] */
    (xdc_Char)0x63,  /* [4928] */
    (xdc_Char)0x68,  /* [4929] */
    (xdc_Char)0x65,  /* [4930] */
    (xdc_Char)0x63,  /* [4931] */
    (xdc_Char)0x6b,  /* [4932] */
    (xdc_Char)0x20,  /* [4933] */
    (xdc_Char)0x66,  /* [4934] */
    (xdc_Char)0x61,  /* [4935] */
    (xdc_Char)0x69,  /* [4936] */
    (xdc_Char)0x6c,  /* [4937] */
    (xdc_Char)0x65,  /* [4938] */
    (xdc_Char)0x64,  /* [4939] */
    (xdc_Char)0x2e,  /* [4940] */
    (xdc_Char)0x0,  /* [4941] */
    (xdc_Char)0x45,  /* [4942] */
    (xdc_Char)0x5f,  /* [4943] */
    (xdc_Char)0x6f,  /* [4944] */
    (xdc_Char)0x62,  /* [4945] */
    (xdc_Char)0x6a,  /* [4946] */
    (xdc_Char)0x65,  /* [4947] */
    (xdc_Char)0x63,  /* [4948] */
    (xdc_Char)0x74,  /* [4949] */
    (xdc_Char)0x4e,  /* [4950] */
    (xdc_Char)0x6f,  /* [4951] */
    (xdc_Char)0x74,  /* [4952] */
    (xdc_Char)0x49,  /* [4953] */
    (xdc_Char)0x6e,  /* [4954] */
    (xdc_Char)0x4b,  /* [4955] */
    (xdc_Char)0x65,  /* [4956] */
    (xdc_Char)0x72,  /* [4957] */
    (xdc_Char)0x6e,  /* [4958] */
    (xdc_Char)0x65,  /* [4959] */
    (xdc_Char)0x6c,  /* [4960] */
    (xdc_Char)0x53,  /* [4961] */
    (xdc_Char)0x70,  /* [4962] */
    (xdc_Char)0x61,  /* [4963] */
    (xdc_Char)0x63,  /* [4964] */
    (xdc_Char)0x65,  /* [4965] */
    (xdc_Char)0x3a,  /* [4966] */
    (xdc_Char)0x20,  /* [4967] */
    (xdc_Char)0x54,  /* [4968] */
    (xdc_Char)0x61,  /* [4969] */
    (xdc_Char)0x73,  /* [4970] */
    (xdc_Char)0x6b,  /* [4971] */
    (xdc_Char)0x20,  /* [4972] */
    (xdc_Char)0x6f,  /* [4973] */
    (xdc_Char)0x62,  /* [4974] */
    (xdc_Char)0x6a,  /* [4975] */
    (xdc_Char)0x65,  /* [4976] */
    (xdc_Char)0x63,  /* [4977] */
    (xdc_Char)0x74,  /* [4978] */
    (xdc_Char)0x20,  /* [4979] */
    (xdc_Char)0x70,  /* [4980] */
    (xdc_Char)0x61,  /* [4981] */
    (xdc_Char)0x73,  /* [4982] */
    (xdc_Char)0x73,  /* [4983] */
    (xdc_Char)0x65,  /* [4984] */
    (xdc_Char)0x64,  /* [4985] */
    (xdc_Char)0x20,  /* [4986] */
    (xdc_Char)0x6e,  /* [4987] */
    (xdc_Char)0x6f,  /* [4988] */
    (xdc_Char)0x74,  /* [4989] */
    (xdc_Char)0x20,  /* [4990] */
    (xdc_Char)0x69,  /* [4991] */
    (xdc_Char)0x6e,  /* [4992] */
    (xdc_Char)0x20,  /* [4993] */
    (xdc_Char)0x4b,  /* [4994] */
    (xdc_Char)0x65,  /* [4995] */
    (xdc_Char)0x72,  /* [4996] */
    (xdc_Char)0x6e,  /* [4997] */
    (xdc_Char)0x65,  /* [4998] */
    (xdc_Char)0x6c,  /* [4999] */
    (xdc_Char)0x20,  /* [5000] */
    (xdc_Char)0x61,  /* [5001] */
    (xdc_Char)0x64,  /* [5002] */
    (xdc_Char)0x64,  /* [5003] */
    (xdc_Char)0x72,  /* [5004] */
    (xdc_Char)0x65,  /* [5005] */
    (xdc_Char)0x73,  /* [5006] */
    (xdc_Char)0x73,  /* [5007] */
    (xdc_Char)0x20,  /* [5008] */
    (xdc_Char)0x73,  /* [5009] */
    (xdc_Char)0x70,  /* [5010] */
    (xdc_Char)0x61,  /* [5011] */
    (xdc_Char)0x63,  /* [5012] */
    (xdc_Char)0x65,  /* [5013] */
    (xdc_Char)0x2e,  /* [5014] */
    (xdc_Char)0x0,  /* [5015] */
    (xdc_Char)0x45,  /* [5016] */
    (xdc_Char)0x5f,  /* [5017] */
    (xdc_Char)0x73,  /* [5018] */
    (xdc_Char)0x74,  /* [5019] */
    (xdc_Char)0x61,  /* [5020] */
    (xdc_Char)0x63,  /* [5021] */
    (xdc_Char)0x6b,  /* [5022] */
    (xdc_Char)0x4f,  /* [5023] */
    (xdc_Char)0x76,  /* [5024] */
    (xdc_Char)0x65,  /* [5025] */
    (xdc_Char)0x72,  /* [5026] */
    (xdc_Char)0x66,  /* [5027] */
    (xdc_Char)0x6c,  /* [5028] */
    (xdc_Char)0x6f,  /* [5029] */
    (xdc_Char)0x77,  /* [5030] */
    (xdc_Char)0x3a,  /* [5031] */
    (xdc_Char)0x20,  /* [5032] */
    (xdc_Char)0x49,  /* [5033] */
    (xdc_Char)0x53,  /* [5034] */
    (xdc_Char)0x52,  /* [5035] */
    (xdc_Char)0x20,  /* [5036] */
    (xdc_Char)0x73,  /* [5037] */
    (xdc_Char)0x74,  /* [5038] */
    (xdc_Char)0x61,  /* [5039] */
    (xdc_Char)0x63,  /* [5040] */
    (xdc_Char)0x6b,  /* [5041] */
    (xdc_Char)0x20,  /* [5042] */
    (xdc_Char)0x6f,  /* [5043] */
    (xdc_Char)0x76,  /* [5044] */
    (xdc_Char)0x65,  /* [5045] */
    (xdc_Char)0x72,  /* [5046] */
    (xdc_Char)0x66,  /* [5047] */
    (xdc_Char)0x6c,  /* [5048] */
    (xdc_Char)0x6f,  /* [5049] */
    (xdc_Char)0x77,  /* [5050] */
    (xdc_Char)0x2e,  /* [5051] */
    (xdc_Char)0x0,  /* [5052] */
    (xdc_Char)0x72,  /* [5053] */
    (xdc_Char)0x65,  /* [5054] */
    (xdc_Char)0x71,  /* [5055] */
    (xdc_Char)0x75,  /* [5056] */
    (xdc_Char)0x65,  /* [5057] */
    (xdc_Char)0x73,  /* [5058] */
    (xdc_Char)0x74,  /* [5059] */
    (xdc_Char)0x65,  /* [5060] */
    (xdc_Char)0x64,  /* [5061] */
    (xdc_Char)0x20,  /* [5062] */
    (xdc_Char)0x73,  /* [5063] */
    (xdc_Char)0x69,  /* [5064] */
    (xdc_Char)0x7a,  /* [5065] */
    (xdc_Char)0x65,  /* [5066] */
    (xdc_Char)0x20,  /* [5067] */
    (xdc_Char)0x69,  /* [5068] */
    (xdc_Char)0x73,  /* [5069] */
    (xdc_Char)0x20,  /* [5070] */
    (xdc_Char)0x74,  /* [5071] */
    (xdc_Char)0x6f,  /* [5072] */
    (xdc_Char)0x6f,  /* [5073] */
    (xdc_Char)0x20,  /* [5074] */
    (xdc_Char)0x62,  /* [5075] */
    (xdc_Char)0x69,  /* [5076] */
    (xdc_Char)0x67,  /* [5077] */
    (xdc_Char)0x3a,  /* [5078] */
    (xdc_Char)0x20,  /* [5079] */
    (xdc_Char)0x68,  /* [5080] */
    (xdc_Char)0x61,  /* [5081] */
    (xdc_Char)0x6e,  /* [5082] */
    (xdc_Char)0x64,  /* [5083] */
    (xdc_Char)0x6c,  /* [5084] */
    (xdc_Char)0x65,  /* [5085] */
    (xdc_Char)0x3d,  /* [5086] */
    (xdc_Char)0x30,  /* [5087] */
    (xdc_Char)0x78,  /* [5088] */
    (xdc_Char)0x25,  /* [5089] */
    (xdc_Char)0x78,  /* [5090] */
    (xdc_Char)0x2c,  /* [5091] */
    (xdc_Char)0x20,  /* [5092] */
    (xdc_Char)0x73,  /* [5093] */
    (xdc_Char)0x69,  /* [5094] */
    (xdc_Char)0x7a,  /* [5095] */
    (xdc_Char)0x65,  /* [5096] */
    (xdc_Char)0x3d,  /* [5097] */
    (xdc_Char)0x25,  /* [5098] */
    (xdc_Char)0x75,  /* [5099] */
    (xdc_Char)0x0,  /* [5100] */
    (xdc_Char)0x6f,  /* [5101] */
    (xdc_Char)0x75,  /* [5102] */
    (xdc_Char)0x74,  /* [5103] */
    (xdc_Char)0x20,  /* [5104] */
    (xdc_Char)0x6f,  /* [5105] */
    (xdc_Char)0x66,  /* [5106] */
    (xdc_Char)0x20,  /* [5107] */
    (xdc_Char)0x6d,  /* [5108] */
    (xdc_Char)0x65,  /* [5109] */
    (xdc_Char)0x6d,  /* [5110] */
    (xdc_Char)0x6f,  /* [5111] */
    (xdc_Char)0x72,  /* [5112] */
    (xdc_Char)0x79,  /* [5113] */
    (xdc_Char)0x3a,  /* [5114] */
    (xdc_Char)0x20,  /* [5115] */
    (xdc_Char)0x68,  /* [5116] */
    (xdc_Char)0x61,  /* [5117] */
    (xdc_Char)0x6e,  /* [5118] */
    (xdc_Char)0x64,  /* [5119] */
    (xdc_Char)0x6c,  /* [5120] */
    (xdc_Char)0x65,  /* [5121] */
    (xdc_Char)0x3d,  /* [5122] */
    (xdc_Char)0x30,  /* [5123] */
    (xdc_Char)0x78,  /* [5124] */
    (xdc_Char)0x25,  /* [5125] */
    (xdc_Char)0x78,  /* [5126] */
    (xdc_Char)0x2c,  /* [5127] */
    (xdc_Char)0x20,  /* [5128] */
    (xdc_Char)0x73,  /* [5129] */
    (xdc_Char)0x69,  /* [5130] */
    (xdc_Char)0x7a,  /* [5131] */
    (xdc_Char)0x65,  /* [5132] */
    (xdc_Char)0x3d,  /* [5133] */
    (xdc_Char)0x25,  /* [5134] */
    (xdc_Char)0x75,  /* [5135] */
    (xdc_Char)0x0,  /* [5136] */
    (xdc_Char)0x45,  /* [5137] */
    (xdc_Char)0x5f,  /* [5138] */
    (xdc_Char)0x69,  /* [5139] */
    (xdc_Char)0x6e,  /* [5140] */
    (xdc_Char)0x76,  /* [5141] */
    (xdc_Char)0x61,  /* [5142] */
    (xdc_Char)0x6c,  /* [5143] */
    (xdc_Char)0x69,  /* [5144] */
    (xdc_Char)0x64,  /* [5145] */
    (xdc_Char)0x54,  /* [5146] */
    (xdc_Char)0x69,  /* [5147] */
    (xdc_Char)0x6d,  /* [5148] */
    (xdc_Char)0x65,  /* [5149] */
    (xdc_Char)0x72,  /* [5150] */
    (xdc_Char)0x3a,  /* [5151] */
    (xdc_Char)0x20,  /* [5152] */
    (xdc_Char)0x49,  /* [5153] */
    (xdc_Char)0x6e,  /* [5154] */
    (xdc_Char)0x76,  /* [5155] */
    (xdc_Char)0x61,  /* [5156] */
    (xdc_Char)0x6c,  /* [5157] */
    (xdc_Char)0x69,  /* [5158] */
    (xdc_Char)0x64,  /* [5159] */
    (xdc_Char)0x20,  /* [5160] */
    (xdc_Char)0x54,  /* [5161] */
    (xdc_Char)0x69,  /* [5162] */
    (xdc_Char)0x6d,  /* [5163] */
    (xdc_Char)0x65,  /* [5164] */
    (xdc_Char)0x72,  /* [5165] */
    (xdc_Char)0x20,  /* [5166] */
    (xdc_Char)0x49,  /* [5167] */
    (xdc_Char)0x64,  /* [5168] */
    (xdc_Char)0x20,  /* [5169] */
    (xdc_Char)0x25,  /* [5170] */
    (xdc_Char)0x64,  /* [5171] */
    (xdc_Char)0x0,  /* [5172] */
    (xdc_Char)0x45,  /* [5173] */
    (xdc_Char)0x5f,  /* [5174] */
    (xdc_Char)0x6e,  /* [5175] */
    (xdc_Char)0x6f,  /* [5176] */
    (xdc_Char)0x74,  /* [5177] */
    (xdc_Char)0x41,  /* [5178] */
    (xdc_Char)0x76,  /* [5179] */
    (xdc_Char)0x61,  /* [5180] */
    (xdc_Char)0x69,  /* [5181] */
    (xdc_Char)0x6c,  /* [5182] */
    (xdc_Char)0x61,  /* [5183] */
    (xdc_Char)0x62,  /* [5184] */
    (xdc_Char)0x6c,  /* [5185] */
    (xdc_Char)0x65,  /* [5186] */
    (xdc_Char)0x3a,  /* [5187] */
    (xdc_Char)0x20,  /* [5188] */
    (xdc_Char)0x54,  /* [5189] */
    (xdc_Char)0x69,  /* [5190] */
    (xdc_Char)0x6d,  /* [5191] */
    (xdc_Char)0x65,  /* [5192] */
    (xdc_Char)0x72,  /* [5193] */
    (xdc_Char)0x20,  /* [5194] */
    (xdc_Char)0x6e,  /* [5195] */
    (xdc_Char)0x6f,  /* [5196] */
    (xdc_Char)0x74,  /* [5197] */
    (xdc_Char)0x20,  /* [5198] */
    (xdc_Char)0x61,  /* [5199] */
    (xdc_Char)0x76,  /* [5200] */
    (xdc_Char)0x61,  /* [5201] */
    (xdc_Char)0x69,  /* [5202] */
    (xdc_Char)0x6c,  /* [5203] */
    (xdc_Char)0x61,  /* [5204] */
    (xdc_Char)0x62,  /* [5205] */
    (xdc_Char)0x6c,  /* [5206] */
    (xdc_Char)0x65,  /* [5207] */
    (xdc_Char)0x20,  /* [5208] */
    (xdc_Char)0x25,  /* [5209] */
    (xdc_Char)0x64,  /* [5210] */
    (xdc_Char)0x0,  /* [5211] */
    (xdc_Char)0x45,  /* [5212] */
    (xdc_Char)0x5f,  /* [5213] */
    (xdc_Char)0x63,  /* [5214] */
    (xdc_Char)0x61,  /* [5215] */
    (xdc_Char)0x6e,  /* [5216] */
    (xdc_Char)0x6e,  /* [5217] */
    (xdc_Char)0x6f,  /* [5218] */
    (xdc_Char)0x74,  /* [5219] */
    (xdc_Char)0x53,  /* [5220] */
    (xdc_Char)0x75,  /* [5221] */
    (xdc_Char)0x70,  /* [5222] */
    (xdc_Char)0x70,  /* [5223] */
    (xdc_Char)0x6f,  /* [5224] */
    (xdc_Char)0x72,  /* [5225] */
    (xdc_Char)0x74,  /* [5226] */
    (xdc_Char)0x3a,  /* [5227] */
    (xdc_Char)0x20,  /* [5228] */
    (xdc_Char)0x54,  /* [5229] */
    (xdc_Char)0x69,  /* [5230] */
    (xdc_Char)0x6d,  /* [5231] */
    (xdc_Char)0x65,  /* [5232] */
    (xdc_Char)0x72,  /* [5233] */
    (xdc_Char)0x20,  /* [5234] */
    (xdc_Char)0x63,  /* [5235] */
    (xdc_Char)0x61,  /* [5236] */
    (xdc_Char)0x6e,  /* [5237] */
    (xdc_Char)0x6e,  /* [5238] */
    (xdc_Char)0x6f,  /* [5239] */
    (xdc_Char)0x74,  /* [5240] */
    (xdc_Char)0x20,  /* [5241] */
    (xdc_Char)0x73,  /* [5242] */
    (xdc_Char)0x75,  /* [5243] */
    (xdc_Char)0x70,  /* [5244] */
    (xdc_Char)0x70,  /* [5245] */
    (xdc_Char)0x6f,  /* [5246] */
    (xdc_Char)0x72,  /* [5247] */
    (xdc_Char)0x74,  /* [5248] */
    (xdc_Char)0x20,  /* [5249] */
    (xdc_Char)0x72,  /* [5250] */
    (xdc_Char)0x65,  /* [5251] */
    (xdc_Char)0x71,  /* [5252] */
    (xdc_Char)0x75,  /* [5253] */
    (xdc_Char)0x65,  /* [5254] */
    (xdc_Char)0x73,  /* [5255] */
    (xdc_Char)0x74,  /* [5256] */
    (xdc_Char)0x65,  /* [5257] */
    (xdc_Char)0x64,  /* [5258] */
    (xdc_Char)0x20,  /* [5259] */
    (xdc_Char)0x70,  /* [5260] */
    (xdc_Char)0x65,  /* [5261] */
    (xdc_Char)0x72,  /* [5262] */
    (xdc_Char)0x69,  /* [5263] */
    (xdc_Char)0x6f,  /* [5264] */
    (xdc_Char)0x64,  /* [5265] */
    (xdc_Char)0x20,  /* [5266] */
    (xdc_Char)0x25,  /* [5267] */
    (xdc_Char)0x64,  /* [5268] */
    (xdc_Char)0x0,  /* [5269] */
    (xdc_Char)0x45,  /* [5270] */
    (xdc_Char)0x5f,  /* [5271] */
    (xdc_Char)0x66,  /* [5272] */
    (xdc_Char)0x72,  /* [5273] */
    (xdc_Char)0x65,  /* [5274] */
    (xdc_Char)0x71,  /* [5275] */
    (xdc_Char)0x4d,  /* [5276] */
    (xdc_Char)0x69,  /* [5277] */
    (xdc_Char)0x73,  /* [5278] */
    (xdc_Char)0x6d,  /* [5279] */
    (xdc_Char)0x61,  /* [5280] */
    (xdc_Char)0x74,  /* [5281] */
    (xdc_Char)0x63,  /* [5282] */
    (xdc_Char)0x68,  /* [5283] */
    (xdc_Char)0x3a,  /* [5284] */
    (xdc_Char)0x20,  /* [5285] */
    (xdc_Char)0x46,  /* [5286] */
    (xdc_Char)0x72,  /* [5287] */
    (xdc_Char)0x65,  /* [5288] */
    (xdc_Char)0x71,  /* [5289] */
    (xdc_Char)0x75,  /* [5290] */
    (xdc_Char)0x65,  /* [5291] */
    (xdc_Char)0x6e,  /* [5292] */
    (xdc_Char)0x63,  /* [5293] */
    (xdc_Char)0x79,  /* [5294] */
    (xdc_Char)0x20,  /* [5295] */
    (xdc_Char)0x6d,  /* [5296] */
    (xdc_Char)0x69,  /* [5297] */
    (xdc_Char)0x73,  /* [5298] */
    (xdc_Char)0x6d,  /* [5299] */
    (xdc_Char)0x61,  /* [5300] */
    (xdc_Char)0x74,  /* [5301] */
    (xdc_Char)0x63,  /* [5302] */
    (xdc_Char)0x68,  /* [5303] */
    (xdc_Char)0x3a,  /* [5304] */
    (xdc_Char)0x20,  /* [5305] */
    (xdc_Char)0x45,  /* [5306] */
    (xdc_Char)0x78,  /* [5307] */
    (xdc_Char)0x70,  /* [5308] */
    (xdc_Char)0x65,  /* [5309] */
    (xdc_Char)0x63,  /* [5310] */
    (xdc_Char)0x74,  /* [5311] */
    (xdc_Char)0x65,  /* [5312] */
    (xdc_Char)0x64,  /* [5313] */
    (xdc_Char)0x20,  /* [5314] */
    (xdc_Char)0x25,  /* [5315] */
    (xdc_Char)0x64,  /* [5316] */
    (xdc_Char)0x20,  /* [5317] */
    (xdc_Char)0x48,  /* [5318] */
    (xdc_Char)0x7a,  /* [5319] */
    (xdc_Char)0x2c,  /* [5320] */
    (xdc_Char)0x20,  /* [5321] */
    (xdc_Char)0x61,  /* [5322] */
    (xdc_Char)0x63,  /* [5323] */
    (xdc_Char)0x74,  /* [5324] */
    (xdc_Char)0x75,  /* [5325] */
    (xdc_Char)0x61,  /* [5326] */
    (xdc_Char)0x6c,  /* [5327] */
    (xdc_Char)0x3a,  /* [5328] */
    (xdc_Char)0x20,  /* [5329] */
    (xdc_Char)0x25,  /* [5330] */
    (xdc_Char)0x64,  /* [5331] */
    (xdc_Char)0x20,  /* [5332] */
    (xdc_Char)0x48,  /* [5333] */
    (xdc_Char)0x7a,  /* [5334] */
    (xdc_Char)0x2e,  /* [5335] */
    (xdc_Char)0x20,  /* [5336] */
    (xdc_Char)0x20,  /* [5337] */
    (xdc_Char)0x59,  /* [5338] */
    (xdc_Char)0x6f,  /* [5339] */
    (xdc_Char)0x75,  /* [5340] */
    (xdc_Char)0x20,  /* [5341] */
    (xdc_Char)0x6e,  /* [5342] */
    (xdc_Char)0x65,  /* [5343] */
    (xdc_Char)0x65,  /* [5344] */
    (xdc_Char)0x64,  /* [5345] */
    (xdc_Char)0x20,  /* [5346] */
    (xdc_Char)0x74,  /* [5347] */
    (xdc_Char)0x6f,  /* [5348] */
    (xdc_Char)0x20,  /* [5349] */
    (xdc_Char)0x6d,  /* [5350] */
    (xdc_Char)0x6f,  /* [5351] */
    (xdc_Char)0x64,  /* [5352] */
    (xdc_Char)0x69,  /* [5353] */
    (xdc_Char)0x66,  /* [5354] */
    (xdc_Char)0x79,  /* [5355] */
    (xdc_Char)0x20,  /* [5356] */
    (xdc_Char)0x54,  /* [5357] */
    (xdc_Char)0x69,  /* [5358] */
    (xdc_Char)0x6d,  /* [5359] */
    (xdc_Char)0x65,  /* [5360] */
    (xdc_Char)0x72,  /* [5361] */
    (xdc_Char)0x2e,  /* [5362] */
    (xdc_Char)0x69,  /* [5363] */
    (xdc_Char)0x6e,  /* [5364] */
    (xdc_Char)0x74,  /* [5365] */
    (xdc_Char)0x46,  /* [5366] */
    (xdc_Char)0x72,  /* [5367] */
    (xdc_Char)0x65,  /* [5368] */
    (xdc_Char)0x71,  /* [5369] */
    (xdc_Char)0x2e,  /* [5370] */
    (xdc_Char)0x6c,  /* [5371] */
    (xdc_Char)0x6f,  /* [5372] */
    (xdc_Char)0x20,  /* [5373] */
    (xdc_Char)0x74,  /* [5374] */
    (xdc_Char)0x6f,  /* [5375] */
    (xdc_Char)0x20,  /* [5376] */
    (xdc_Char)0x6d,  /* [5377] */
    (xdc_Char)0x61,  /* [5378] */
    (xdc_Char)0x74,  /* [5379] */
    (xdc_Char)0x63,  /* [5380] */
    (xdc_Char)0x68,  /* [5381] */
    (xdc_Char)0x20,  /* [5382] */
    (xdc_Char)0x74,  /* [5383] */
    (xdc_Char)0x68,  /* [5384] */
    (xdc_Char)0x65,  /* [5385] */
    (xdc_Char)0x20,  /* [5386] */
    (xdc_Char)0x61,  /* [5387] */
    (xdc_Char)0x63,  /* [5388] */
    (xdc_Char)0x74,  /* [5389] */
    (xdc_Char)0x75,  /* [5390] */
    (xdc_Char)0x61,  /* [5391] */
    (xdc_Char)0x6c,  /* [5392] */
    (xdc_Char)0x20,  /* [5393] */
    (xdc_Char)0x66,  /* [5394] */
    (xdc_Char)0x72,  /* [5395] */
    (xdc_Char)0x65,  /* [5396] */
    (xdc_Char)0x71,  /* [5397] */
    (xdc_Char)0x75,  /* [5398] */
    (xdc_Char)0x65,  /* [5399] */
    (xdc_Char)0x6e,  /* [5400] */
    (xdc_Char)0x63,  /* [5401] */
    (xdc_Char)0x79,  /* [5402] */
    (xdc_Char)0x2e,  /* [5403] */
    (xdc_Char)0x0,  /* [5404] */
    (xdc_Char)0x45,  /* [5405] */
    (xdc_Char)0x5f,  /* [5406] */
    (xdc_Char)0x62,  /* [5407] */
    (xdc_Char)0x61,  /* [5408] */
    (xdc_Char)0x64,  /* [5409] */
    (xdc_Char)0x49,  /* [5410] */
    (xdc_Char)0x6e,  /* [5411] */
    (xdc_Char)0x74,  /* [5412] */
    (xdc_Char)0x4e,  /* [5413] */
    (xdc_Char)0x75,  /* [5414] */
    (xdc_Char)0x6d,  /* [5415] */
    (xdc_Char)0x3a,  /* [5416] */
    (xdc_Char)0x20,  /* [5417] */
    (xdc_Char)0x54,  /* [5418] */
    (xdc_Char)0x69,  /* [5419] */
    (xdc_Char)0x6d,  /* [5420] */
    (xdc_Char)0x65,  /* [5421] */
    (xdc_Char)0x72,  /* [5422] */
    (xdc_Char)0x20,  /* [5423] */
    (xdc_Char)0x72,  /* [5424] */
    (xdc_Char)0x65,  /* [5425] */
    (xdc_Char)0x71,  /* [5426] */
    (xdc_Char)0x75,  /* [5427] */
    (xdc_Char)0x69,  /* [5428] */
    (xdc_Char)0x72,  /* [5429] */
    (xdc_Char)0x65,  /* [5430] */
    (xdc_Char)0x73,  /* [5431] */
    (xdc_Char)0x20,  /* [5432] */
    (xdc_Char)0x61,  /* [5433] */
    (xdc_Char)0x20,  /* [5434] */
    (xdc_Char)0x76,  /* [5435] */
    (xdc_Char)0x61,  /* [5436] */
    (xdc_Char)0x6c,  /* [5437] */
    (xdc_Char)0x69,  /* [5438] */
    (xdc_Char)0x64,  /* [5439] */
    (xdc_Char)0x20,  /* [5440] */
    (xdc_Char)0x69,  /* [5441] */
    (xdc_Char)0x6e,  /* [5442] */
    (xdc_Char)0x74,  /* [5443] */
    (xdc_Char)0x4e,  /* [5444] */
    (xdc_Char)0x75,  /* [5445] */
    (xdc_Char)0x6d,  /* [5446] */
    (xdc_Char)0x2e,  /* [5447] */
    (xdc_Char)0x20,  /* [5448] */
    (xdc_Char)0x50,  /* [5449] */
    (xdc_Char)0x6c,  /* [5450] */
    (xdc_Char)0x65,  /* [5451] */
    (xdc_Char)0x61,  /* [5452] */
    (xdc_Char)0x73,  /* [5453] */
    (xdc_Char)0x65,  /* [5454] */
    (xdc_Char)0x20,  /* [5455] */
    (xdc_Char)0x75,  /* [5456] */
    (xdc_Char)0x73,  /* [5457] */
    (xdc_Char)0x65,  /* [5458] */
    (xdc_Char)0x20,  /* [5459] */
    (xdc_Char)0x44,  /* [5460] */
    (xdc_Char)0x4d,  /* [5461] */
    (xdc_Char)0x54,  /* [5462] */
    (xdc_Char)0x69,  /* [5463] */
    (xdc_Char)0x6d,  /* [5464] */
    (xdc_Char)0x65,  /* [5465] */
    (xdc_Char)0x72,  /* [5466] */
    (xdc_Char)0x20,  /* [5467] */
    (xdc_Char)0x6d,  /* [5468] */
    (xdc_Char)0x6f,  /* [5469] */
    (xdc_Char)0x64,  /* [5470] */
    (xdc_Char)0x75,  /* [5471] */
    (xdc_Char)0x6c,  /* [5472] */
    (xdc_Char)0x65,  /* [5473] */
    (xdc_Char)0x27,  /* [5474] */
    (xdc_Char)0x73,  /* [5475] */
    (xdc_Char)0x20,  /* [5476] */
    (xdc_Char)0x74,  /* [5477] */
    (xdc_Char)0x69,  /* [5478] */
    (xdc_Char)0x6d,  /* [5479] */
    (xdc_Char)0x65,  /* [5480] */
    (xdc_Char)0x72,  /* [5481] */
    (xdc_Char)0x53,  /* [5482] */
    (xdc_Char)0x65,  /* [5483] */
    (xdc_Char)0x74,  /* [5484] */
    (xdc_Char)0x74,  /* [5485] */
    (xdc_Char)0x69,  /* [5486] */
    (xdc_Char)0x6e,  /* [5487] */
    (xdc_Char)0x67,  /* [5488] */
    (xdc_Char)0x73,  /* [5489] */
    (xdc_Char)0x20,  /* [5490] */
    (xdc_Char)0x63,  /* [5491] */
    (xdc_Char)0x6f,  /* [5492] */
    (xdc_Char)0x6e,  /* [5493] */
    (xdc_Char)0x66,  /* [5494] */
    (xdc_Char)0x69,  /* [5495] */
    (xdc_Char)0x67,  /* [5496] */
    (xdc_Char)0x20,  /* [5497] */
    (xdc_Char)0x70,  /* [5498] */
    (xdc_Char)0x61,  /* [5499] */
    (xdc_Char)0x72,  /* [5500] */
    (xdc_Char)0x61,  /* [5501] */
    (xdc_Char)0x6d,  /* [5502] */
    (xdc_Char)0x20,  /* [5503] */
    (xdc_Char)0x74,  /* [5504] */
    (xdc_Char)0x6f,  /* [5505] */
    (xdc_Char)0x20,  /* [5506] */
    (xdc_Char)0x61,  /* [5507] */
    (xdc_Char)0x73,  /* [5508] */
    (xdc_Char)0x73,  /* [5509] */
    (xdc_Char)0x69,  /* [5510] */
    (xdc_Char)0x67,  /* [5511] */
    (xdc_Char)0x6e,  /* [5512] */
    (xdc_Char)0x20,  /* [5513] */
    (xdc_Char)0x61,  /* [5514] */
    (xdc_Char)0x20,  /* [5515] */
    (xdc_Char)0x76,  /* [5516] */
    (xdc_Char)0x61,  /* [5517] */
    (xdc_Char)0x6c,  /* [5518] */
    (xdc_Char)0x69,  /* [5519] */
    (xdc_Char)0x64,  /* [5520] */
    (xdc_Char)0x20,  /* [5521] */
    (xdc_Char)0x69,  /* [5522] */
    (xdc_Char)0x6e,  /* [5523] */
    (xdc_Char)0x74,  /* [5524] */
    (xdc_Char)0x4e,  /* [5525] */
    (xdc_Char)0x75,  /* [5526] */
    (xdc_Char)0x6d,  /* [5527] */
    (xdc_Char)0x20,  /* [5528] */
    (xdc_Char)0x66,  /* [5529] */
    (xdc_Char)0x6f,  /* [5530] */
    (xdc_Char)0x72,  /* [5531] */
    (xdc_Char)0x20,  /* [5532] */
    (xdc_Char)0x74,  /* [5533] */
    (xdc_Char)0x68,  /* [5534] */
    (xdc_Char)0x65,  /* [5535] */
    (xdc_Char)0x20,  /* [5536] */
    (xdc_Char)0x73,  /* [5537] */
    (xdc_Char)0x65,  /* [5538] */
    (xdc_Char)0x6c,  /* [5539] */
    (xdc_Char)0x65,  /* [5540] */
    (xdc_Char)0x63,  /* [5541] */
    (xdc_Char)0x74,  /* [5542] */
    (xdc_Char)0x65,  /* [5543] */
    (xdc_Char)0x64,  /* [5544] */
    (xdc_Char)0x20,  /* [5545] */
    (xdc_Char)0x74,  /* [5546] */
    (xdc_Char)0x69,  /* [5547] */
    (xdc_Char)0x6d,  /* [5548] */
    (xdc_Char)0x65,  /* [5549] */
    (xdc_Char)0x72,  /* [5550] */
    (xdc_Char)0x2e,  /* [5551] */
    (xdc_Char)0x0,  /* [5552] */
    (xdc_Char)0x45,  /* [5553] */
    (xdc_Char)0x5f,  /* [5554] */
    (xdc_Char)0x70,  /* [5555] */
    (xdc_Char)0x72,  /* [5556] */
    (xdc_Char)0x69,  /* [5557] */
    (xdc_Char)0x6f,  /* [5558] */
    (xdc_Char)0x72,  /* [5559] */
    (xdc_Char)0x69,  /* [5560] */
    (xdc_Char)0x74,  /* [5561] */
    (xdc_Char)0x79,  /* [5562] */
    (xdc_Char)0x3a,  /* [5563] */
    (xdc_Char)0x20,  /* [5564] */
    (xdc_Char)0x54,  /* [5565] */
    (xdc_Char)0x68,  /* [5566] */
    (xdc_Char)0x72,  /* [5567] */
    (xdc_Char)0x65,  /* [5568] */
    (xdc_Char)0x61,  /* [5569] */
    (xdc_Char)0x64,  /* [5570] */
    (xdc_Char)0x20,  /* [5571] */
    (xdc_Char)0x70,  /* [5572] */
    (xdc_Char)0x72,  /* [5573] */
    (xdc_Char)0x69,  /* [5574] */
    (xdc_Char)0x6f,  /* [5575] */
    (xdc_Char)0x72,  /* [5576] */
    (xdc_Char)0x69,  /* [5577] */
    (xdc_Char)0x74,  /* [5578] */
    (xdc_Char)0x79,  /* [5579] */
    (xdc_Char)0x20,  /* [5580] */
    (xdc_Char)0x69,  /* [5581] */
    (xdc_Char)0x73,  /* [5582] */
    (xdc_Char)0x20,  /* [5583] */
    (xdc_Char)0x69,  /* [5584] */
    (xdc_Char)0x6e,  /* [5585] */
    (xdc_Char)0x76,  /* [5586] */
    (xdc_Char)0x61,  /* [5587] */
    (xdc_Char)0x6c,  /* [5588] */
    (xdc_Char)0x69,  /* [5589] */
    (xdc_Char)0x64,  /* [5590] */
    (xdc_Char)0x20,  /* [5591] */
    (xdc_Char)0x25,  /* [5592] */
    (xdc_Char)0x64,  /* [5593] */
    (xdc_Char)0x0,  /* [5594] */
    (xdc_Char)0x3c,  /* [5595] */
    (xdc_Char)0x2d,  /* [5596] */
    (xdc_Char)0x2d,  /* [5597] */
    (xdc_Char)0x20,  /* [5598] */
    (xdc_Char)0x63,  /* [5599] */
    (xdc_Char)0x6f,  /* [5600] */
    (xdc_Char)0x6e,  /* [5601] */
    (xdc_Char)0x73,  /* [5602] */
    (xdc_Char)0x74,  /* [5603] */
    (xdc_Char)0x72,  /* [5604] */
    (xdc_Char)0x75,  /* [5605] */
    (xdc_Char)0x63,  /* [5606] */
    (xdc_Char)0x74,  /* [5607] */
    (xdc_Char)0x3a,  /* [5608] */
    (xdc_Char)0x20,  /* [5609] */
    (xdc_Char)0x25,  /* [5610] */
    (xdc_Char)0x70,  /* [5611] */
    (xdc_Char)0x28,  /* [5612] */
    (xdc_Char)0x27,  /* [5613] */
    (xdc_Char)0x25,  /* [5614] */
    (xdc_Char)0x73,  /* [5615] */
    (xdc_Char)0x27,  /* [5616] */
    (xdc_Char)0x29,  /* [5617] */
    (xdc_Char)0x0,  /* [5618] */
    (xdc_Char)0x3c,  /* [5619] */
    (xdc_Char)0x2d,  /* [5620] */
    (xdc_Char)0x2d,  /* [5621] */
    (xdc_Char)0x20,  /* [5622] */
    (xdc_Char)0x63,  /* [5623] */
    (xdc_Char)0x72,  /* [5624] */
    (xdc_Char)0x65,  /* [5625] */
    (xdc_Char)0x61,  /* [5626] */
    (xdc_Char)0x74,  /* [5627] */
    (xdc_Char)0x65,  /* [5628] */
    (xdc_Char)0x3a,  /* [5629] */
    (xdc_Char)0x20,  /* [5630] */
    (xdc_Char)0x25,  /* [5631] */
    (xdc_Char)0x70,  /* [5632] */
    (xdc_Char)0x28,  /* [5633] */
    (xdc_Char)0x27,  /* [5634] */
    (xdc_Char)0x25,  /* [5635] */
    (xdc_Char)0x73,  /* [5636] */
    (xdc_Char)0x27,  /* [5637] */
    (xdc_Char)0x29,  /* [5638] */
    (xdc_Char)0x0,  /* [5639] */
    (xdc_Char)0x2d,  /* [5640] */
    (xdc_Char)0x2d,  /* [5641] */
    (xdc_Char)0x3e,  /* [5642] */
    (xdc_Char)0x20,  /* [5643] */
    (xdc_Char)0x64,  /* [5644] */
    (xdc_Char)0x65,  /* [5645] */
    (xdc_Char)0x73,  /* [5646] */
    (xdc_Char)0x74,  /* [5647] */
    (xdc_Char)0x72,  /* [5648] */
    (xdc_Char)0x75,  /* [5649] */
    (xdc_Char)0x63,  /* [5650] */
    (xdc_Char)0x74,  /* [5651] */
    (xdc_Char)0x3a,  /* [5652] */
    (xdc_Char)0x20,  /* [5653] */
    (xdc_Char)0x28,  /* [5654] */
    (xdc_Char)0x25,  /* [5655] */
    (xdc_Char)0x70,  /* [5656] */
    (xdc_Char)0x29,  /* [5657] */
    (xdc_Char)0x0,  /* [5658] */
    (xdc_Char)0x2d,  /* [5659] */
    (xdc_Char)0x2d,  /* [5660] */
    (xdc_Char)0x3e,  /* [5661] */
    (xdc_Char)0x20,  /* [5662] */
    (xdc_Char)0x64,  /* [5663] */
    (xdc_Char)0x65,  /* [5664] */
    (xdc_Char)0x6c,  /* [5665] */
    (xdc_Char)0x65,  /* [5666] */
    (xdc_Char)0x74,  /* [5667] */
    (xdc_Char)0x65,  /* [5668] */
    (xdc_Char)0x3a,  /* [5669] */
    (xdc_Char)0x20,  /* [5670] */
    (xdc_Char)0x28,  /* [5671] */
    (xdc_Char)0x25,  /* [5672] */
    (xdc_Char)0x70,  /* [5673] */
    (xdc_Char)0x29,  /* [5674] */
    (xdc_Char)0x0,  /* [5675] */
    (xdc_Char)0x45,  /* [5676] */
    (xdc_Char)0x52,  /* [5677] */
    (xdc_Char)0x52,  /* [5678] */
    (xdc_Char)0x4f,  /* [5679] */
    (xdc_Char)0x52,  /* [5680] */
    (xdc_Char)0x3a,  /* [5681] */
    (xdc_Char)0x20,  /* [5682] */
    (xdc_Char)0x25,  /* [5683] */
    (xdc_Char)0x24,  /* [5684] */
    (xdc_Char)0x46,  /* [5685] */
    (xdc_Char)0x25,  /* [5686] */
    (xdc_Char)0x24,  /* [5687] */
    (xdc_Char)0x53,  /* [5688] */
    (xdc_Char)0x0,  /* [5689] */
    (xdc_Char)0x57,  /* [5690] */
    (xdc_Char)0x41,  /* [5691] */
    (xdc_Char)0x52,  /* [5692] */
    (xdc_Char)0x4e,  /* [5693] */
    (xdc_Char)0x49,  /* [5694] */
    (xdc_Char)0x4e,  /* [5695] */
    (xdc_Char)0x47,  /* [5696] */
    (xdc_Char)0x3a,  /* [5697] */
    (xdc_Char)0x20,  /* [5698] */
    (xdc_Char)0x25,  /* [5699] */
    (xdc_Char)0x24,  /* [5700] */
    (xdc_Char)0x46,  /* [5701] */
    (xdc_Char)0x25,  /* [5702] */
    (xdc_Char)0x24,  /* [5703] */
    (xdc_Char)0x53,  /* [5704] */
    (xdc_Char)0x0,  /* [5705] */
    (xdc_Char)0x25,  /* [5706] */
    (xdc_Char)0x24,  /* [5707] */
    (xdc_Char)0x46,  /* [5708] */
    (xdc_Char)0x25,  /* [5709] */
    (xdc_Char)0x24,  /* [5710] */
    (xdc_Char)0x53,  /* [5711] */
    (xdc_Char)0x0,  /* [5712] */
    (xdc_Char)0x53,  /* [5713] */
    (xdc_Char)0x74,  /* [5714] */
    (xdc_Char)0x61,  /* [5715] */
    (xdc_Char)0x72,  /* [5716] */
    (xdc_Char)0x74,  /* [5717] */
    (xdc_Char)0x3a,  /* [5718] */
    (xdc_Char)0x20,  /* [5719] */
    (xdc_Char)0x25,  /* [5720] */
    (xdc_Char)0x24,  /* [5721] */
    (xdc_Char)0x53,  /* [5722] */
    (xdc_Char)0x0,  /* [5723] */
    (xdc_Char)0x53,  /* [5724] */
    (xdc_Char)0x74,  /* [5725] */
    (xdc_Char)0x6f,  /* [5726] */
    (xdc_Char)0x70,  /* [5727] */
    (xdc_Char)0x3a,  /* [5728] */
    (xdc_Char)0x20,  /* [5729] */
    (xdc_Char)0x25,  /* [5730] */
    (xdc_Char)0x24,  /* [5731] */
    (xdc_Char)0x53,  /* [5732] */
    (xdc_Char)0x0,  /* [5733] */
    (xdc_Char)0x53,  /* [5734] */
    (xdc_Char)0x74,  /* [5735] */
    (xdc_Char)0x61,  /* [5736] */
    (xdc_Char)0x72,  /* [5737] */
    (xdc_Char)0x74,  /* [5738] */
    (xdc_Char)0x49,  /* [5739] */
    (xdc_Char)0x6e,  /* [5740] */
    (xdc_Char)0x73,  /* [5741] */
    (xdc_Char)0x74,  /* [5742] */
    (xdc_Char)0x61,  /* [5743] */
    (xdc_Char)0x6e,  /* [5744] */
    (xdc_Char)0x63,  /* [5745] */
    (xdc_Char)0x65,  /* [5746] */
    (xdc_Char)0x3a,  /* [5747] */
    (xdc_Char)0x20,  /* [5748] */
    (xdc_Char)0x25,  /* [5749] */
    (xdc_Char)0x24,  /* [5750] */
    (xdc_Char)0x53,  /* [5751] */
    (xdc_Char)0x0,  /* [5752] */
    (xdc_Char)0x53,  /* [5753] */
    (xdc_Char)0x74,  /* [5754] */
    (xdc_Char)0x6f,  /* [5755] */
    (xdc_Char)0x70,  /* [5756] */
    (xdc_Char)0x49,  /* [5757] */
    (xdc_Char)0x6e,  /* [5758] */
    (xdc_Char)0x73,  /* [5759] */
    (xdc_Char)0x74,  /* [5760] */
    (xdc_Char)0x61,  /* [5761] */
    (xdc_Char)0x6e,  /* [5762] */
    (xdc_Char)0x63,  /* [5763] */
    (xdc_Char)0x65,  /* [5764] */
    (xdc_Char)0x3a,  /* [5765] */
    (xdc_Char)0x20,  /* [5766] */
    (xdc_Char)0x25,  /* [5767] */
    (xdc_Char)0x24,  /* [5768] */
    (xdc_Char)0x53,  /* [5769] */
    (xdc_Char)0x0,  /* [5770] */
    (xdc_Char)0x4c,  /* [5771] */
    (xdc_Char)0x4d,  /* [5772] */
    (xdc_Char)0x5f,  /* [5773] */
    (xdc_Char)0x62,  /* [5774] */
    (xdc_Char)0x65,  /* [5775] */
    (xdc_Char)0x67,  /* [5776] */
    (xdc_Char)0x69,  /* [5777] */
    (xdc_Char)0x6e,  /* [5778] */
    (xdc_Char)0x3a,  /* [5779] */
    (xdc_Char)0x20,  /* [5780] */
    (xdc_Char)0x68,  /* [5781] */
    (xdc_Char)0x77,  /* [5782] */
    (xdc_Char)0x69,  /* [5783] */
    (xdc_Char)0x3a,  /* [5784] */
    (xdc_Char)0x20,  /* [5785] */
    (xdc_Char)0x30,  /* [5786] */
    (xdc_Char)0x78,  /* [5787] */
    (xdc_Char)0x25,  /* [5788] */
    (xdc_Char)0x78,  /* [5789] */
    (xdc_Char)0x2c,  /* [5790] */
    (xdc_Char)0x20,  /* [5791] */
    (xdc_Char)0x66,  /* [5792] */
    (xdc_Char)0x75,  /* [5793] */
    (xdc_Char)0x6e,  /* [5794] */
    (xdc_Char)0x63,  /* [5795] */
    (xdc_Char)0x3a,  /* [5796] */
    (xdc_Char)0x20,  /* [5797] */
    (xdc_Char)0x30,  /* [5798] */
    (xdc_Char)0x78,  /* [5799] */
    (xdc_Char)0x25,  /* [5800] */
    (xdc_Char)0x78,  /* [5801] */
    (xdc_Char)0x2c,  /* [5802] */
    (xdc_Char)0x20,  /* [5803] */
    (xdc_Char)0x70,  /* [5804] */
    (xdc_Char)0x72,  /* [5805] */
    (xdc_Char)0x65,  /* [5806] */
    (xdc_Char)0x54,  /* [5807] */
    (xdc_Char)0x68,  /* [5808] */
    (xdc_Char)0x72,  /* [5809] */
    (xdc_Char)0x65,  /* [5810] */
    (xdc_Char)0x61,  /* [5811] */
    (xdc_Char)0x64,  /* [5812] */
    (xdc_Char)0x3a,  /* [5813] */
    (xdc_Char)0x20,  /* [5814] */
    (xdc_Char)0x25,  /* [5815] */
    (xdc_Char)0x64,  /* [5816] */
    (xdc_Char)0x2c,  /* [5817] */
    (xdc_Char)0x20,  /* [5818] */
    (xdc_Char)0x69,  /* [5819] */
    (xdc_Char)0x6e,  /* [5820] */
    (xdc_Char)0x74,  /* [5821] */
    (xdc_Char)0x4e,  /* [5822] */
    (xdc_Char)0x75,  /* [5823] */
    (xdc_Char)0x6d,  /* [5824] */
    (xdc_Char)0x3a,  /* [5825] */
    (xdc_Char)0x20,  /* [5826] */
    (xdc_Char)0x25,  /* [5827] */
    (xdc_Char)0x64,  /* [5828] */
    (xdc_Char)0x2c,  /* [5829] */
    (xdc_Char)0x20,  /* [5830] */
    (xdc_Char)0x69,  /* [5831] */
    (xdc_Char)0x72,  /* [5832] */
    (xdc_Char)0x70,  /* [5833] */
    (xdc_Char)0x3a,  /* [5834] */
    (xdc_Char)0x20,  /* [5835] */
    (xdc_Char)0x30,  /* [5836] */
    (xdc_Char)0x78,  /* [5837] */
    (xdc_Char)0x25,  /* [5838] */
    (xdc_Char)0x78,  /* [5839] */
    (xdc_Char)0x0,  /* [5840] */
    (xdc_Char)0x4c,  /* [5841] */
    (xdc_Char)0x44,  /* [5842] */
    (xdc_Char)0x5f,  /* [5843] */
    (xdc_Char)0x65,  /* [5844] */
    (xdc_Char)0x6e,  /* [5845] */
    (xdc_Char)0x64,  /* [5846] */
    (xdc_Char)0x3a,  /* [5847] */
    (xdc_Char)0x20,  /* [5848] */
    (xdc_Char)0x68,  /* [5849] */
    (xdc_Char)0x77,  /* [5850] */
    (xdc_Char)0x69,  /* [5851] */
    (xdc_Char)0x3a,  /* [5852] */
    (xdc_Char)0x20,  /* [5853] */
    (xdc_Char)0x30,  /* [5854] */
    (xdc_Char)0x78,  /* [5855] */
    (xdc_Char)0x25,  /* [5856] */
    (xdc_Char)0x78,  /* [5857] */
    (xdc_Char)0x0,  /* [5858] */
    (xdc_Char)0x4c,  /* [5859] */
    (xdc_Char)0x57,  /* [5860] */
    (xdc_Char)0x5f,  /* [5861] */
    (xdc_Char)0x64,  /* [5862] */
    (xdc_Char)0x65,  /* [5863] */
    (xdc_Char)0x6c,  /* [5864] */
    (xdc_Char)0x61,  /* [5865] */
    (xdc_Char)0x79,  /* [5866] */
    (xdc_Char)0x65,  /* [5867] */
    (xdc_Char)0x64,  /* [5868] */
    (xdc_Char)0x3a,  /* [5869] */
    (xdc_Char)0x20,  /* [5870] */
    (xdc_Char)0x64,  /* [5871] */
    (xdc_Char)0x65,  /* [5872] */
    (xdc_Char)0x6c,  /* [5873] */
    (xdc_Char)0x61,  /* [5874] */
    (xdc_Char)0x79,  /* [5875] */
    (xdc_Char)0x3a,  /* [5876] */
    (xdc_Char)0x20,  /* [5877] */
    (xdc_Char)0x25,  /* [5878] */
    (xdc_Char)0x64,  /* [5879] */
    (xdc_Char)0x0,  /* [5880] */
    (xdc_Char)0x4c,  /* [5881] */
    (xdc_Char)0x4d,  /* [5882] */
    (xdc_Char)0x5f,  /* [5883] */
    (xdc_Char)0x74,  /* [5884] */
    (xdc_Char)0x69,  /* [5885] */
    (xdc_Char)0x63,  /* [5886] */
    (xdc_Char)0x6b,  /* [5887] */
    (xdc_Char)0x3a,  /* [5888] */
    (xdc_Char)0x20,  /* [5889] */
    (xdc_Char)0x74,  /* [5890] */
    (xdc_Char)0x69,  /* [5891] */
    (xdc_Char)0x63,  /* [5892] */
    (xdc_Char)0x6b,  /* [5893] */
    (xdc_Char)0x3a,  /* [5894] */
    (xdc_Char)0x20,  /* [5895] */
    (xdc_Char)0x25,  /* [5896] */
    (xdc_Char)0x64,  /* [5897] */
    (xdc_Char)0x0,  /* [5898] */
    (xdc_Char)0x4c,  /* [5899] */
    (xdc_Char)0x4d,  /* [5900] */
    (xdc_Char)0x5f,  /* [5901] */
    (xdc_Char)0x62,  /* [5902] */
    (xdc_Char)0x65,  /* [5903] */
    (xdc_Char)0x67,  /* [5904] */
    (xdc_Char)0x69,  /* [5905] */
    (xdc_Char)0x6e,  /* [5906] */
    (xdc_Char)0x3a,  /* [5907] */
    (xdc_Char)0x20,  /* [5908] */
    (xdc_Char)0x63,  /* [5909] */
    (xdc_Char)0x6c,  /* [5910] */
    (xdc_Char)0x6b,  /* [5911] */
    (xdc_Char)0x3a,  /* [5912] */
    (xdc_Char)0x20,  /* [5913] */
    (xdc_Char)0x30,  /* [5914] */
    (xdc_Char)0x78,  /* [5915] */
    (xdc_Char)0x25,  /* [5916] */
    (xdc_Char)0x78,  /* [5917] */
    (xdc_Char)0x2c,  /* [5918] */
    (xdc_Char)0x20,  /* [5919] */
    (xdc_Char)0x66,  /* [5920] */
    (xdc_Char)0x75,  /* [5921] */
    (xdc_Char)0x6e,  /* [5922] */
    (xdc_Char)0x63,  /* [5923] */
    (xdc_Char)0x3a,  /* [5924] */
    (xdc_Char)0x20,  /* [5925] */
    (xdc_Char)0x30,  /* [5926] */
    (xdc_Char)0x78,  /* [5927] */
    (xdc_Char)0x25,  /* [5928] */
    (xdc_Char)0x78,  /* [5929] */
    (xdc_Char)0x0,  /* [5930] */
    (xdc_Char)0x4c,  /* [5931] */
    (xdc_Char)0x4d,  /* [5932] */
    (xdc_Char)0x5f,  /* [5933] */
    (xdc_Char)0x70,  /* [5934] */
    (xdc_Char)0x6f,  /* [5935] */
    (xdc_Char)0x73,  /* [5936] */
    (xdc_Char)0x74,  /* [5937] */
    (xdc_Char)0x3a,  /* [5938] */
    (xdc_Char)0x20,  /* [5939] */
    (xdc_Char)0x65,  /* [5940] */
    (xdc_Char)0x76,  /* [5941] */
    (xdc_Char)0x65,  /* [5942] */
    (xdc_Char)0x6e,  /* [5943] */
    (xdc_Char)0x74,  /* [5944] */
    (xdc_Char)0x3a,  /* [5945] */
    (xdc_Char)0x20,  /* [5946] */
    (xdc_Char)0x30,  /* [5947] */
    (xdc_Char)0x78,  /* [5948] */
    (xdc_Char)0x25,  /* [5949] */
    (xdc_Char)0x78,  /* [5950] */
    (xdc_Char)0x2c,  /* [5951] */
    (xdc_Char)0x20,  /* [5952] */
    (xdc_Char)0x63,  /* [5953] */
    (xdc_Char)0x75,  /* [5954] */
    (xdc_Char)0x72,  /* [5955] */
    (xdc_Char)0x72,  /* [5956] */
    (xdc_Char)0x45,  /* [5957] */
    (xdc_Char)0x76,  /* [5958] */
    (xdc_Char)0x65,  /* [5959] */
    (xdc_Char)0x6e,  /* [5960] */
    (xdc_Char)0x74,  /* [5961] */
    (xdc_Char)0x73,  /* [5962] */
    (xdc_Char)0x3a,  /* [5963] */
    (xdc_Char)0x20,  /* [5964] */
    (xdc_Char)0x30,  /* [5965] */
    (xdc_Char)0x78,  /* [5966] */
    (xdc_Char)0x25,  /* [5967] */
    (xdc_Char)0x78,  /* [5968] */
    (xdc_Char)0x2c,  /* [5969] */
    (xdc_Char)0x20,  /* [5970] */
    (xdc_Char)0x65,  /* [5971] */
    (xdc_Char)0x76,  /* [5972] */
    (xdc_Char)0x65,  /* [5973] */
    (xdc_Char)0x6e,  /* [5974] */
    (xdc_Char)0x74,  /* [5975] */
    (xdc_Char)0x49,  /* [5976] */
    (xdc_Char)0x64,  /* [5977] */
    (xdc_Char)0x3a,  /* [5978] */
    (xdc_Char)0x20,  /* [5979] */
    (xdc_Char)0x30,  /* [5980] */
    (xdc_Char)0x78,  /* [5981] */
    (xdc_Char)0x25,  /* [5982] */
    (xdc_Char)0x78,  /* [5983] */
    (xdc_Char)0x0,  /* [5984] */
    (xdc_Char)0x4c,  /* [5985] */
    (xdc_Char)0x4d,  /* [5986] */
    (xdc_Char)0x5f,  /* [5987] */
    (xdc_Char)0x70,  /* [5988] */
    (xdc_Char)0x65,  /* [5989] */
    (xdc_Char)0x6e,  /* [5990] */
    (xdc_Char)0x64,  /* [5991] */
    (xdc_Char)0x3a,  /* [5992] */
    (xdc_Char)0x20,  /* [5993] */
    (xdc_Char)0x65,  /* [5994] */
    (xdc_Char)0x76,  /* [5995] */
    (xdc_Char)0x65,  /* [5996] */
    (xdc_Char)0x6e,  /* [5997] */
    (xdc_Char)0x74,  /* [5998] */
    (xdc_Char)0x3a,  /* [5999] */
    (xdc_Char)0x20,  /* [6000] */
    (xdc_Char)0x30,  /* [6001] */
    (xdc_Char)0x78,  /* [6002] */
    (xdc_Char)0x25,  /* [6003] */
    (xdc_Char)0x78,  /* [6004] */
    (xdc_Char)0x2c,  /* [6005] */
    (xdc_Char)0x20,  /* [6006] */
    (xdc_Char)0x63,  /* [6007] */
    (xdc_Char)0x75,  /* [6008] */
    (xdc_Char)0x72,  /* [6009] */
    (xdc_Char)0x72,  /* [6010] */
    (xdc_Char)0x45,  /* [6011] */
    (xdc_Char)0x76,  /* [6012] */
    (xdc_Char)0x65,  /* [6013] */
    (xdc_Char)0x6e,  /* [6014] */
    (xdc_Char)0x74,  /* [6015] */
    (xdc_Char)0x73,  /* [6016] */
    (xdc_Char)0x3a,  /* [6017] */
    (xdc_Char)0x20,  /* [6018] */
    (xdc_Char)0x30,  /* [6019] */
    (xdc_Char)0x78,  /* [6020] */
    (xdc_Char)0x25,  /* [6021] */
    (xdc_Char)0x78,  /* [6022] */
    (xdc_Char)0x2c,  /* [6023] */
    (xdc_Char)0x20,  /* [6024] */
    (xdc_Char)0x61,  /* [6025] */
    (xdc_Char)0x6e,  /* [6026] */
    (xdc_Char)0x64,  /* [6027] */
    (xdc_Char)0x4d,  /* [6028] */
    (xdc_Char)0x61,  /* [6029] */
    (xdc_Char)0x73,  /* [6030] */
    (xdc_Char)0x6b,  /* [6031] */
    (xdc_Char)0x3a,  /* [6032] */
    (xdc_Char)0x20,  /* [6033] */
    (xdc_Char)0x30,  /* [6034] */
    (xdc_Char)0x78,  /* [6035] */
    (xdc_Char)0x25,  /* [6036] */
    (xdc_Char)0x78,  /* [6037] */
    (xdc_Char)0x2c,  /* [6038] */
    (xdc_Char)0x20,  /* [6039] */
    (xdc_Char)0x6f,  /* [6040] */
    (xdc_Char)0x72,  /* [6041] */
    (xdc_Char)0x4d,  /* [6042] */
    (xdc_Char)0x61,  /* [6043] */
    (xdc_Char)0x73,  /* [6044] */
    (xdc_Char)0x6b,  /* [6045] */
    (xdc_Char)0x3a,  /* [6046] */
    (xdc_Char)0x20,  /* [6047] */
    (xdc_Char)0x30,  /* [6048] */
    (xdc_Char)0x78,  /* [6049] */
    (xdc_Char)0x25,  /* [6050] */
    (xdc_Char)0x78,  /* [6051] */
    (xdc_Char)0x2c,  /* [6052] */
    (xdc_Char)0x20,  /* [6053] */
    (xdc_Char)0x74,  /* [6054] */
    (xdc_Char)0x69,  /* [6055] */
    (xdc_Char)0x6d,  /* [6056] */
    (xdc_Char)0x65,  /* [6057] */
    (xdc_Char)0x6f,  /* [6058] */
    (xdc_Char)0x75,  /* [6059] */
    (xdc_Char)0x74,  /* [6060] */
    (xdc_Char)0x3a,  /* [6061] */
    (xdc_Char)0x20,  /* [6062] */
    (xdc_Char)0x25,  /* [6063] */
    (xdc_Char)0x64,  /* [6064] */
    (xdc_Char)0x0,  /* [6065] */
    (xdc_Char)0x4c,  /* [6066] */
    (xdc_Char)0x4d,  /* [6067] */
    (xdc_Char)0x5f,  /* [6068] */
    (xdc_Char)0x70,  /* [6069] */
    (xdc_Char)0x6f,  /* [6070] */
    (xdc_Char)0x73,  /* [6071] */
    (xdc_Char)0x74,  /* [6072] */
    (xdc_Char)0x3a,  /* [6073] */
    (xdc_Char)0x20,  /* [6074] */
    (xdc_Char)0x73,  /* [6075] */
    (xdc_Char)0x65,  /* [6076] */
    (xdc_Char)0x6d,  /* [6077] */
    (xdc_Char)0x3a,  /* [6078] */
    (xdc_Char)0x20,  /* [6079] */
    (xdc_Char)0x30,  /* [6080] */
    (xdc_Char)0x78,  /* [6081] */
    (xdc_Char)0x25,  /* [6082] */
    (xdc_Char)0x78,  /* [6083] */
    (xdc_Char)0x2c,  /* [6084] */
    (xdc_Char)0x20,  /* [6085] */
    (xdc_Char)0x63,  /* [6086] */
    (xdc_Char)0x6f,  /* [6087] */
    (xdc_Char)0x75,  /* [6088] */
    (xdc_Char)0x6e,  /* [6089] */
    (xdc_Char)0x74,  /* [6090] */
    (xdc_Char)0x3a,  /* [6091] */
    (xdc_Char)0x20,  /* [6092] */
    (xdc_Char)0x25,  /* [6093] */
    (xdc_Char)0x64,  /* [6094] */
    (xdc_Char)0x0,  /* [6095] */
    (xdc_Char)0x4c,  /* [6096] */
    (xdc_Char)0x4d,  /* [6097] */
    (xdc_Char)0x5f,  /* [6098] */
    (xdc_Char)0x70,  /* [6099] */
    (xdc_Char)0x65,  /* [6100] */
    (xdc_Char)0x6e,  /* [6101] */
    (xdc_Char)0x64,  /* [6102] */
    (xdc_Char)0x3a,  /* [6103] */
    (xdc_Char)0x20,  /* [6104] */
    (xdc_Char)0x73,  /* [6105] */
    (xdc_Char)0x65,  /* [6106] */
    (xdc_Char)0x6d,  /* [6107] */
    (xdc_Char)0x3a,  /* [6108] */
    (xdc_Char)0x20,  /* [6109] */
    (xdc_Char)0x30,  /* [6110] */
    (xdc_Char)0x78,  /* [6111] */
    (xdc_Char)0x25,  /* [6112] */
    (xdc_Char)0x78,  /* [6113] */
    (xdc_Char)0x2c,  /* [6114] */
    (xdc_Char)0x20,  /* [6115] */
    (xdc_Char)0x63,  /* [6116] */
    (xdc_Char)0x6f,  /* [6117] */
    (xdc_Char)0x75,  /* [6118] */
    (xdc_Char)0x6e,  /* [6119] */
    (xdc_Char)0x74,  /* [6120] */
    (xdc_Char)0x3a,  /* [6121] */
    (xdc_Char)0x20,  /* [6122] */
    (xdc_Char)0x25,  /* [6123] */
    (xdc_Char)0x64,  /* [6124] */
    (xdc_Char)0x2c,  /* [6125] */
    (xdc_Char)0x20,  /* [6126] */
    (xdc_Char)0x74,  /* [6127] */
    (xdc_Char)0x69,  /* [6128] */
    (xdc_Char)0x6d,  /* [6129] */
    (xdc_Char)0x65,  /* [6130] */
    (xdc_Char)0x6f,  /* [6131] */
    (xdc_Char)0x75,  /* [6132] */
    (xdc_Char)0x74,  /* [6133] */
    (xdc_Char)0x3a,  /* [6134] */
    (xdc_Char)0x20,  /* [6135] */
    (xdc_Char)0x25,  /* [6136] */
    (xdc_Char)0x64,  /* [6137] */
    (xdc_Char)0x0,  /* [6138] */
    (xdc_Char)0x4c,  /* [6139] */
    (xdc_Char)0x4d,  /* [6140] */
    (xdc_Char)0x5f,  /* [6141] */
    (xdc_Char)0x62,  /* [6142] */
    (xdc_Char)0x65,  /* [6143] */
    (xdc_Char)0x67,  /* [6144] */
    (xdc_Char)0x69,  /* [6145] */
    (xdc_Char)0x6e,  /* [6146] */
    (xdc_Char)0x3a,  /* [6147] */
    (xdc_Char)0x20,  /* [6148] */
    (xdc_Char)0x73,  /* [6149] */
    (xdc_Char)0x77,  /* [6150] */
    (xdc_Char)0x69,  /* [6151] */
    (xdc_Char)0x3a,  /* [6152] */
    (xdc_Char)0x20,  /* [6153] */
    (xdc_Char)0x30,  /* [6154] */
    (xdc_Char)0x78,  /* [6155] */
    (xdc_Char)0x25,  /* [6156] */
    (xdc_Char)0x78,  /* [6157] */
    (xdc_Char)0x2c,  /* [6158] */
    (xdc_Char)0x20,  /* [6159] */
    (xdc_Char)0x66,  /* [6160] */
    (xdc_Char)0x75,  /* [6161] */
    (xdc_Char)0x6e,  /* [6162] */
    (xdc_Char)0x63,  /* [6163] */
    (xdc_Char)0x3a,  /* [6164] */
    (xdc_Char)0x20,  /* [6165] */
    (xdc_Char)0x30,  /* [6166] */
    (xdc_Char)0x78,  /* [6167] */
    (xdc_Char)0x25,  /* [6168] */
    (xdc_Char)0x78,  /* [6169] */
    (xdc_Char)0x2c,  /* [6170] */
    (xdc_Char)0x20,  /* [6171] */
    (xdc_Char)0x70,  /* [6172] */
    (xdc_Char)0x72,  /* [6173] */
    (xdc_Char)0x65,  /* [6174] */
    (xdc_Char)0x54,  /* [6175] */
    (xdc_Char)0x68,  /* [6176] */
    (xdc_Char)0x72,  /* [6177] */
    (xdc_Char)0x65,  /* [6178] */
    (xdc_Char)0x61,  /* [6179] */
    (xdc_Char)0x64,  /* [6180] */
    (xdc_Char)0x3a,  /* [6181] */
    (xdc_Char)0x20,  /* [6182] */
    (xdc_Char)0x25,  /* [6183] */
    (xdc_Char)0x64,  /* [6184] */
    (xdc_Char)0x0,  /* [6185] */
    (xdc_Char)0x4c,  /* [6186] */
    (xdc_Char)0x44,  /* [6187] */
    (xdc_Char)0x5f,  /* [6188] */
    (xdc_Char)0x65,  /* [6189] */
    (xdc_Char)0x6e,  /* [6190] */
    (xdc_Char)0x64,  /* [6191] */
    (xdc_Char)0x3a,  /* [6192] */
    (xdc_Char)0x20,  /* [6193] */
    (xdc_Char)0x73,  /* [6194] */
    (xdc_Char)0x77,  /* [6195] */
    (xdc_Char)0x69,  /* [6196] */
    (xdc_Char)0x3a,  /* [6197] */
    (xdc_Char)0x20,  /* [6198] */
    (xdc_Char)0x30,  /* [6199] */
    (xdc_Char)0x78,  /* [6200] */
    (xdc_Char)0x25,  /* [6201] */
    (xdc_Char)0x78,  /* [6202] */
    (xdc_Char)0x0,  /* [6203] */
    (xdc_Char)0x4c,  /* [6204] */
    (xdc_Char)0x4d,  /* [6205] */
    (xdc_Char)0x5f,  /* [6206] */
    (xdc_Char)0x70,  /* [6207] */
    (xdc_Char)0x6f,  /* [6208] */
    (xdc_Char)0x73,  /* [6209] */
    (xdc_Char)0x74,  /* [6210] */
    (xdc_Char)0x3a,  /* [6211] */
    (xdc_Char)0x20,  /* [6212] */
    (xdc_Char)0x73,  /* [6213] */
    (xdc_Char)0x77,  /* [6214] */
    (xdc_Char)0x69,  /* [6215] */
    (xdc_Char)0x3a,  /* [6216] */
    (xdc_Char)0x20,  /* [6217] */
    (xdc_Char)0x30,  /* [6218] */
    (xdc_Char)0x78,  /* [6219] */
    (xdc_Char)0x25,  /* [6220] */
    (xdc_Char)0x78,  /* [6221] */
    (xdc_Char)0x2c,  /* [6222] */
    (xdc_Char)0x20,  /* [6223] */
    (xdc_Char)0x66,  /* [6224] */
    (xdc_Char)0x75,  /* [6225] */
    (xdc_Char)0x6e,  /* [6226] */
    (xdc_Char)0x63,  /* [6227] */
    (xdc_Char)0x3a,  /* [6228] */
    (xdc_Char)0x20,  /* [6229] */
    (xdc_Char)0x30,  /* [6230] */
    (xdc_Char)0x78,  /* [6231] */
    (xdc_Char)0x25,  /* [6232] */
    (xdc_Char)0x78,  /* [6233] */
    (xdc_Char)0x2c,  /* [6234] */
    (xdc_Char)0x20,  /* [6235] */
    (xdc_Char)0x70,  /* [6236] */
    (xdc_Char)0x72,  /* [6237] */
    (xdc_Char)0x69,  /* [6238] */
    (xdc_Char)0x3a,  /* [6239] */
    (xdc_Char)0x20,  /* [6240] */
    (xdc_Char)0x25,  /* [6241] */
    (xdc_Char)0x64,  /* [6242] */
    (xdc_Char)0x0,  /* [6243] */
    (xdc_Char)0x4c,  /* [6244] */
    (xdc_Char)0x4d,  /* [6245] */
    (xdc_Char)0x5f,  /* [6246] */
    (xdc_Char)0x73,  /* [6247] */
    (xdc_Char)0x77,  /* [6248] */
    (xdc_Char)0x69,  /* [6249] */
    (xdc_Char)0x74,  /* [6250] */
    (xdc_Char)0x63,  /* [6251] */
    (xdc_Char)0x68,  /* [6252] */
    (xdc_Char)0x3a,  /* [6253] */
    (xdc_Char)0x20,  /* [6254] */
    (xdc_Char)0x6f,  /* [6255] */
    (xdc_Char)0x6c,  /* [6256] */
    (xdc_Char)0x64,  /* [6257] */
    (xdc_Char)0x74,  /* [6258] */
    (xdc_Char)0x73,  /* [6259] */
    (xdc_Char)0x6b,  /* [6260] */
    (xdc_Char)0x3a,  /* [6261] */
    (xdc_Char)0x20,  /* [6262] */
    (xdc_Char)0x30,  /* [6263] */
    (xdc_Char)0x78,  /* [6264] */
    (xdc_Char)0x25,  /* [6265] */
    (xdc_Char)0x78,  /* [6266] */
    (xdc_Char)0x2c,  /* [6267] */
    (xdc_Char)0x20,  /* [6268] */
    (xdc_Char)0x6f,  /* [6269] */
    (xdc_Char)0x6c,  /* [6270] */
    (xdc_Char)0x64,  /* [6271] */
    (xdc_Char)0x66,  /* [6272] */
    (xdc_Char)0x75,  /* [6273] */
    (xdc_Char)0x6e,  /* [6274] */
    (xdc_Char)0x63,  /* [6275] */
    (xdc_Char)0x3a,  /* [6276] */
    (xdc_Char)0x20,  /* [6277] */
    (xdc_Char)0x30,  /* [6278] */
    (xdc_Char)0x78,  /* [6279] */
    (xdc_Char)0x25,  /* [6280] */
    (xdc_Char)0x78,  /* [6281] */
    (xdc_Char)0x2c,  /* [6282] */
    (xdc_Char)0x20,  /* [6283] */
    (xdc_Char)0x6e,  /* [6284] */
    (xdc_Char)0x65,  /* [6285] */
    (xdc_Char)0x77,  /* [6286] */
    (xdc_Char)0x74,  /* [6287] */
    (xdc_Char)0x73,  /* [6288] */
    (xdc_Char)0x6b,  /* [6289] */
    (xdc_Char)0x3a,  /* [6290] */
    (xdc_Char)0x20,  /* [6291] */
    (xdc_Char)0x30,  /* [6292] */
    (xdc_Char)0x78,  /* [6293] */
    (xdc_Char)0x25,  /* [6294] */
    (xdc_Char)0x78,  /* [6295] */
    (xdc_Char)0x2c,  /* [6296] */
    (xdc_Char)0x20,  /* [6297] */
    (xdc_Char)0x6e,  /* [6298] */
    (xdc_Char)0x65,  /* [6299] */
    (xdc_Char)0x77,  /* [6300] */
    (xdc_Char)0x66,  /* [6301] */
    (xdc_Char)0x75,  /* [6302] */
    (xdc_Char)0x6e,  /* [6303] */
    (xdc_Char)0x63,  /* [6304] */
    (xdc_Char)0x3a,  /* [6305] */
    (xdc_Char)0x20,  /* [6306] */
    (xdc_Char)0x30,  /* [6307] */
    (xdc_Char)0x78,  /* [6308] */
    (xdc_Char)0x25,  /* [6309] */
    (xdc_Char)0x78,  /* [6310] */
    (xdc_Char)0x0,  /* [6311] */
    (xdc_Char)0x4c,  /* [6312] */
    (xdc_Char)0x4d,  /* [6313] */
    (xdc_Char)0x5f,  /* [6314] */
    (xdc_Char)0x73,  /* [6315] */
    (xdc_Char)0x6c,  /* [6316] */
    (xdc_Char)0x65,  /* [6317] */
    (xdc_Char)0x65,  /* [6318] */
    (xdc_Char)0x70,  /* [6319] */
    (xdc_Char)0x3a,  /* [6320] */
    (xdc_Char)0x20,  /* [6321] */
    (xdc_Char)0x74,  /* [6322] */
    (xdc_Char)0x73,  /* [6323] */
    (xdc_Char)0x6b,  /* [6324] */
    (xdc_Char)0x3a,  /* [6325] */
    (xdc_Char)0x20,  /* [6326] */
    (xdc_Char)0x30,  /* [6327] */
    (xdc_Char)0x78,  /* [6328] */
    (xdc_Char)0x25,  /* [6329] */
    (xdc_Char)0x78,  /* [6330] */
    (xdc_Char)0x2c,  /* [6331] */
    (xdc_Char)0x20,  /* [6332] */
    (xdc_Char)0x66,  /* [6333] */
    (xdc_Char)0x75,  /* [6334] */
    (xdc_Char)0x6e,  /* [6335] */
    (xdc_Char)0x63,  /* [6336] */
    (xdc_Char)0x3a,  /* [6337] */
    (xdc_Char)0x20,  /* [6338] */
    (xdc_Char)0x30,  /* [6339] */
    (xdc_Char)0x78,  /* [6340] */
    (xdc_Char)0x25,  /* [6341] */
    (xdc_Char)0x78,  /* [6342] */
    (xdc_Char)0x2c,  /* [6343] */
    (xdc_Char)0x20,  /* [6344] */
    (xdc_Char)0x74,  /* [6345] */
    (xdc_Char)0x69,  /* [6346] */
    (xdc_Char)0x6d,  /* [6347] */
    (xdc_Char)0x65,  /* [6348] */
    (xdc_Char)0x6f,  /* [6349] */
    (xdc_Char)0x75,  /* [6350] */
    (xdc_Char)0x74,  /* [6351] */
    (xdc_Char)0x3a,  /* [6352] */
    (xdc_Char)0x20,  /* [6353] */
    (xdc_Char)0x25,  /* [6354] */
    (xdc_Char)0x64,  /* [6355] */
    (xdc_Char)0x0,  /* [6356] */
    (xdc_Char)0x4c,  /* [6357] */
    (xdc_Char)0x44,  /* [6358] */
    (xdc_Char)0x5f,  /* [6359] */
    (xdc_Char)0x72,  /* [6360] */
    (xdc_Char)0x65,  /* [6361] */
    (xdc_Char)0x61,  /* [6362] */
    (xdc_Char)0x64,  /* [6363] */
    (xdc_Char)0x79,  /* [6364] */
    (xdc_Char)0x3a,  /* [6365] */
    (xdc_Char)0x20,  /* [6366] */
    (xdc_Char)0x74,  /* [6367] */
    (xdc_Char)0x73,  /* [6368] */
    (xdc_Char)0x6b,  /* [6369] */
    (xdc_Char)0x3a,  /* [6370] */
    (xdc_Char)0x20,  /* [6371] */
    (xdc_Char)0x30,  /* [6372] */
    (xdc_Char)0x78,  /* [6373] */
    (xdc_Char)0x25,  /* [6374] */
    (xdc_Char)0x78,  /* [6375] */
    (xdc_Char)0x2c,  /* [6376] */
    (xdc_Char)0x20,  /* [6377] */
    (xdc_Char)0x66,  /* [6378] */
    (xdc_Char)0x75,  /* [6379] */
    (xdc_Char)0x6e,  /* [6380] */
    (xdc_Char)0x63,  /* [6381] */
    (xdc_Char)0x3a,  /* [6382] */
    (xdc_Char)0x20,  /* [6383] */
    (xdc_Char)0x30,  /* [6384] */
    (xdc_Char)0x78,  /* [6385] */
    (xdc_Char)0x25,  /* [6386] */
    (xdc_Char)0x78,  /* [6387] */
    (xdc_Char)0x2c,  /* [6388] */
    (xdc_Char)0x20,  /* [6389] */
    (xdc_Char)0x70,  /* [6390] */
    (xdc_Char)0x72,  /* [6391] */
    (xdc_Char)0x69,  /* [6392] */
    (xdc_Char)0x3a,  /* [6393] */
    (xdc_Char)0x20,  /* [6394] */
    (xdc_Char)0x25,  /* [6395] */
    (xdc_Char)0x64,  /* [6396] */
    (xdc_Char)0x0,  /* [6397] */
    (xdc_Char)0x4c,  /* [6398] */
    (xdc_Char)0x44,  /* [6399] */
    (xdc_Char)0x5f,  /* [6400] */
    (xdc_Char)0x62,  /* [6401] */
    (xdc_Char)0x6c,  /* [6402] */
    (xdc_Char)0x6f,  /* [6403] */
    (xdc_Char)0x63,  /* [6404] */
    (xdc_Char)0x6b,  /* [6405] */
    (xdc_Char)0x3a,  /* [6406] */
    (xdc_Char)0x20,  /* [6407] */
    (xdc_Char)0x74,  /* [6408] */
    (xdc_Char)0x73,  /* [6409] */
    (xdc_Char)0x6b,  /* [6410] */
    (xdc_Char)0x3a,  /* [6411] */
    (xdc_Char)0x20,  /* [6412] */
    (xdc_Char)0x30,  /* [6413] */
    (xdc_Char)0x78,  /* [6414] */
    (xdc_Char)0x25,  /* [6415] */
    (xdc_Char)0x78,  /* [6416] */
    (xdc_Char)0x2c,  /* [6417] */
    (xdc_Char)0x20,  /* [6418] */
    (xdc_Char)0x66,  /* [6419] */
    (xdc_Char)0x75,  /* [6420] */
    (xdc_Char)0x6e,  /* [6421] */
    (xdc_Char)0x63,  /* [6422] */
    (xdc_Char)0x3a,  /* [6423] */
    (xdc_Char)0x20,  /* [6424] */
    (xdc_Char)0x30,  /* [6425] */
    (xdc_Char)0x78,  /* [6426] */
    (xdc_Char)0x25,  /* [6427] */
    (xdc_Char)0x78,  /* [6428] */
    (xdc_Char)0x0,  /* [6429] */
    (xdc_Char)0x4c,  /* [6430] */
    (xdc_Char)0x4d,  /* [6431] */
    (xdc_Char)0x5f,  /* [6432] */
    (xdc_Char)0x79,  /* [6433] */
    (xdc_Char)0x69,  /* [6434] */
    (xdc_Char)0x65,  /* [6435] */
    (xdc_Char)0x6c,  /* [6436] */
    (xdc_Char)0x64,  /* [6437] */
    (xdc_Char)0x3a,  /* [6438] */
    (xdc_Char)0x20,  /* [6439] */
    (xdc_Char)0x74,  /* [6440] */
    (xdc_Char)0x73,  /* [6441] */
    (xdc_Char)0x6b,  /* [6442] */
    (xdc_Char)0x3a,  /* [6443] */
    (xdc_Char)0x20,  /* [6444] */
    (xdc_Char)0x30,  /* [6445] */
    (xdc_Char)0x78,  /* [6446] */
    (xdc_Char)0x25,  /* [6447] */
    (xdc_Char)0x78,  /* [6448] */
    (xdc_Char)0x2c,  /* [6449] */
    (xdc_Char)0x20,  /* [6450] */
    (xdc_Char)0x66,  /* [6451] */
    (xdc_Char)0x75,  /* [6452] */
    (xdc_Char)0x6e,  /* [6453] */
    (xdc_Char)0x63,  /* [6454] */
    (xdc_Char)0x3a,  /* [6455] */
    (xdc_Char)0x20,  /* [6456] */
    (xdc_Char)0x30,  /* [6457] */
    (xdc_Char)0x78,  /* [6458] */
    (xdc_Char)0x25,  /* [6459] */
    (xdc_Char)0x78,  /* [6460] */
    (xdc_Char)0x2c,  /* [6461] */
    (xdc_Char)0x20,  /* [6462] */
    (xdc_Char)0x63,  /* [6463] */
    (xdc_Char)0x75,  /* [6464] */
    (xdc_Char)0x72,  /* [6465] */
    (xdc_Char)0x72,  /* [6466] */
    (xdc_Char)0x54,  /* [6467] */
    (xdc_Char)0x68,  /* [6468] */
    (xdc_Char)0x72,  /* [6469] */
    (xdc_Char)0x65,  /* [6470] */
    (xdc_Char)0x61,  /* [6471] */
    (xdc_Char)0x64,  /* [6472] */
    (xdc_Char)0x3a,  /* [6473] */
    (xdc_Char)0x20,  /* [6474] */
    (xdc_Char)0x25,  /* [6475] */
    (xdc_Char)0x64,  /* [6476] */
    (xdc_Char)0x0,  /* [6477] */
    (xdc_Char)0x4c,  /* [6478] */
    (xdc_Char)0x4d,  /* [6479] */
    (xdc_Char)0x5f,  /* [6480] */
    (xdc_Char)0x73,  /* [6481] */
    (xdc_Char)0x65,  /* [6482] */
    (xdc_Char)0x74,  /* [6483] */
    (xdc_Char)0x50,  /* [6484] */
    (xdc_Char)0x72,  /* [6485] */
    (xdc_Char)0x69,  /* [6486] */
    (xdc_Char)0x3a,  /* [6487] */
    (xdc_Char)0x20,  /* [6488] */
    (xdc_Char)0x74,  /* [6489] */
    (xdc_Char)0x73,  /* [6490] */
    (xdc_Char)0x6b,  /* [6491] */
    (xdc_Char)0x3a,  /* [6492] */
    (xdc_Char)0x20,  /* [6493] */
    (xdc_Char)0x30,  /* [6494] */
    (xdc_Char)0x78,  /* [6495] */
    (xdc_Char)0x25,  /* [6496] */
    (xdc_Char)0x78,  /* [6497] */
    (xdc_Char)0x2c,  /* [6498] */
    (xdc_Char)0x20,  /* [6499] */
    (xdc_Char)0x66,  /* [6500] */
    (xdc_Char)0x75,  /* [6501] */
    (xdc_Char)0x6e,  /* [6502] */
    (xdc_Char)0x63,  /* [6503] */
    (xdc_Char)0x3a,  /* [6504] */
    (xdc_Char)0x20,  /* [6505] */
    (xdc_Char)0x30,  /* [6506] */
    (xdc_Char)0x78,  /* [6507] */
    (xdc_Char)0x25,  /* [6508] */
    (xdc_Char)0x78,  /* [6509] */
    (xdc_Char)0x2c,  /* [6510] */
    (xdc_Char)0x20,  /* [6511] */
    (xdc_Char)0x6f,  /* [6512] */
    (xdc_Char)0x6c,  /* [6513] */
    (xdc_Char)0x64,  /* [6514] */
    (xdc_Char)0x50,  /* [6515] */
    (xdc_Char)0x72,  /* [6516] */
    (xdc_Char)0x69,  /* [6517] */
    (xdc_Char)0x3a,  /* [6518] */
    (xdc_Char)0x20,  /* [6519] */
    (xdc_Char)0x25,  /* [6520] */
    (xdc_Char)0x64,  /* [6521] */
    (xdc_Char)0x2c,  /* [6522] */
    (xdc_Char)0x20,  /* [6523] */
    (xdc_Char)0x6e,  /* [6524] */
    (xdc_Char)0x65,  /* [6525] */
    (xdc_Char)0x77,  /* [6526] */
    (xdc_Char)0x50,  /* [6527] */
    (xdc_Char)0x72,  /* [6528] */
    (xdc_Char)0x69,  /* [6529] */
    (xdc_Char)0x20,  /* [6530] */
    (xdc_Char)0x25,  /* [6531] */
    (xdc_Char)0x64,  /* [6532] */
    (xdc_Char)0x0,  /* [6533] */
    (xdc_Char)0x4c,  /* [6534] */
    (xdc_Char)0x44,  /* [6535] */
    (xdc_Char)0x5f,  /* [6536] */
    (xdc_Char)0x65,  /* [6537] */
    (xdc_Char)0x78,  /* [6538] */
    (xdc_Char)0x69,  /* [6539] */
    (xdc_Char)0x74,  /* [6540] */
    (xdc_Char)0x3a,  /* [6541] */
    (xdc_Char)0x20,  /* [6542] */
    (xdc_Char)0x74,  /* [6543] */
    (xdc_Char)0x73,  /* [6544] */
    (xdc_Char)0x6b,  /* [6545] */
    (xdc_Char)0x3a,  /* [6546] */
    (xdc_Char)0x20,  /* [6547] */
    (xdc_Char)0x30,  /* [6548] */
    (xdc_Char)0x78,  /* [6549] */
    (xdc_Char)0x25,  /* [6550] */
    (xdc_Char)0x78,  /* [6551] */
    (xdc_Char)0x2c,  /* [6552] */
    (xdc_Char)0x20,  /* [6553] */
    (xdc_Char)0x66,  /* [6554] */
    (xdc_Char)0x75,  /* [6555] */
    (xdc_Char)0x6e,  /* [6556] */
    (xdc_Char)0x63,  /* [6557] */
    (xdc_Char)0x3a,  /* [6558] */
    (xdc_Char)0x20,  /* [6559] */
    (xdc_Char)0x30,  /* [6560] */
    (xdc_Char)0x78,  /* [6561] */
    (xdc_Char)0x25,  /* [6562] */
    (xdc_Char)0x78,  /* [6563] */
    (xdc_Char)0x0,  /* [6564] */
    (xdc_Char)0x4c,  /* [6565] */
    (xdc_Char)0x4d,  /* [6566] */
    (xdc_Char)0x5f,  /* [6567] */
    (xdc_Char)0x73,  /* [6568] */
    (xdc_Char)0x65,  /* [6569] */
    (xdc_Char)0x74,  /* [6570] */
    (xdc_Char)0x41,  /* [6571] */
    (xdc_Char)0x66,  /* [6572] */
    (xdc_Char)0x66,  /* [6573] */
    (xdc_Char)0x69,  /* [6574] */
    (xdc_Char)0x6e,  /* [6575] */
    (xdc_Char)0x69,  /* [6576] */
    (xdc_Char)0x74,  /* [6577] */
    (xdc_Char)0x79,  /* [6578] */
    (xdc_Char)0x3a,  /* [6579] */
    (xdc_Char)0x20,  /* [6580] */
    (xdc_Char)0x74,  /* [6581] */
    (xdc_Char)0x73,  /* [6582] */
    (xdc_Char)0x6b,  /* [6583] */
    (xdc_Char)0x3a,  /* [6584] */
    (xdc_Char)0x20,  /* [6585] */
    (xdc_Char)0x30,  /* [6586] */
    (xdc_Char)0x78,  /* [6587] */
    (xdc_Char)0x25,  /* [6588] */
    (xdc_Char)0x78,  /* [6589] */
    (xdc_Char)0x2c,  /* [6590] */
    (xdc_Char)0x20,  /* [6591] */
    (xdc_Char)0x66,  /* [6592] */
    (xdc_Char)0x75,  /* [6593] */
    (xdc_Char)0x6e,  /* [6594] */
    (xdc_Char)0x63,  /* [6595] */
    (xdc_Char)0x3a,  /* [6596] */
    (xdc_Char)0x20,  /* [6597] */
    (xdc_Char)0x30,  /* [6598] */
    (xdc_Char)0x78,  /* [6599] */
    (xdc_Char)0x25,  /* [6600] */
    (xdc_Char)0x78,  /* [6601] */
    (xdc_Char)0x2c,  /* [6602] */
    (xdc_Char)0x20,  /* [6603] */
    (xdc_Char)0x6f,  /* [6604] */
    (xdc_Char)0x6c,  /* [6605] */
    (xdc_Char)0x64,  /* [6606] */
    (xdc_Char)0x43,  /* [6607] */
    (xdc_Char)0x6f,  /* [6608] */
    (xdc_Char)0x72,  /* [6609] */
    (xdc_Char)0x65,  /* [6610] */
    (xdc_Char)0x3a,  /* [6611] */
    (xdc_Char)0x20,  /* [6612] */
    (xdc_Char)0x25,  /* [6613] */
    (xdc_Char)0x64,  /* [6614] */
    (xdc_Char)0x2c,  /* [6615] */
    (xdc_Char)0x20,  /* [6616] */
    (xdc_Char)0x6f,  /* [6617] */
    (xdc_Char)0x6c,  /* [6618] */
    (xdc_Char)0x64,  /* [6619] */
    (xdc_Char)0x41,  /* [6620] */
    (xdc_Char)0x66,  /* [6621] */
    (xdc_Char)0x66,  /* [6622] */
    (xdc_Char)0x69,  /* [6623] */
    (xdc_Char)0x6e,  /* [6624] */
    (xdc_Char)0x69,  /* [6625] */
    (xdc_Char)0x74,  /* [6626] */
    (xdc_Char)0x79,  /* [6627] */
    (xdc_Char)0x20,  /* [6628] */
    (xdc_Char)0x25,  /* [6629] */
    (xdc_Char)0x64,  /* [6630] */
    (xdc_Char)0x2c,  /* [6631] */
    (xdc_Char)0x20,  /* [6632] */
    (xdc_Char)0x6e,  /* [6633] */
    (xdc_Char)0x65,  /* [6634] */
    (xdc_Char)0x77,  /* [6635] */
    (xdc_Char)0x41,  /* [6636] */
    (xdc_Char)0x66,  /* [6637] */
    (xdc_Char)0x66,  /* [6638] */
    (xdc_Char)0x69,  /* [6639] */
    (xdc_Char)0x6e,  /* [6640] */
    (xdc_Char)0x69,  /* [6641] */
    (xdc_Char)0x74,  /* [6642] */
    (xdc_Char)0x79,  /* [6643] */
    (xdc_Char)0x20,  /* [6644] */
    (xdc_Char)0x25,  /* [6645] */
    (xdc_Char)0x64,  /* [6646] */
    (xdc_Char)0x0,  /* [6647] */
    (xdc_Char)0x4c,  /* [6648] */
    (xdc_Char)0x44,  /* [6649] */
    (xdc_Char)0x5f,  /* [6650] */
    (xdc_Char)0x73,  /* [6651] */
    (xdc_Char)0x63,  /* [6652] */
    (xdc_Char)0x68,  /* [6653] */
    (xdc_Char)0x65,  /* [6654] */
    (xdc_Char)0x64,  /* [6655] */
    (xdc_Char)0x75,  /* [6656] */
    (xdc_Char)0x6c,  /* [6657] */
    (xdc_Char)0x65,  /* [6658] */
    (xdc_Char)0x3a,  /* [6659] */
    (xdc_Char)0x20,  /* [6660] */
    (xdc_Char)0x63,  /* [6661] */
    (xdc_Char)0x6f,  /* [6662] */
    (xdc_Char)0x72,  /* [6663] */
    (xdc_Char)0x65,  /* [6664] */
    (xdc_Char)0x49,  /* [6665] */
    (xdc_Char)0x64,  /* [6666] */
    (xdc_Char)0x3a,  /* [6667] */
    (xdc_Char)0x20,  /* [6668] */
    (xdc_Char)0x25,  /* [6669] */
    (xdc_Char)0x64,  /* [6670] */
    (xdc_Char)0x2c,  /* [6671] */
    (xdc_Char)0x20,  /* [6672] */
    (xdc_Char)0x77,  /* [6673] */
    (xdc_Char)0x6f,  /* [6674] */
    (xdc_Char)0x72,  /* [6675] */
    (xdc_Char)0x6b,  /* [6676] */
    (xdc_Char)0x46,  /* [6677] */
    (xdc_Char)0x6c,  /* [6678] */
    (xdc_Char)0x61,  /* [6679] */
    (xdc_Char)0x67,  /* [6680] */
    (xdc_Char)0x3a,  /* [6681] */
    (xdc_Char)0x20,  /* [6682] */
    (xdc_Char)0x25,  /* [6683] */
    (xdc_Char)0x64,  /* [6684] */
    (xdc_Char)0x2c,  /* [6685] */
    (xdc_Char)0x20,  /* [6686] */
    (xdc_Char)0x63,  /* [6687] */
    (xdc_Char)0x75,  /* [6688] */
    (xdc_Char)0x72,  /* [6689] */
    (xdc_Char)0x53,  /* [6690] */
    (xdc_Char)0x65,  /* [6691] */
    (xdc_Char)0x74,  /* [6692] */
    (xdc_Char)0x4c,  /* [6693] */
    (xdc_Char)0x6f,  /* [6694] */
    (xdc_Char)0x63,  /* [6695] */
    (xdc_Char)0x61,  /* [6696] */
    (xdc_Char)0x6c,  /* [6697] */
    (xdc_Char)0x3a,  /* [6698] */
    (xdc_Char)0x20,  /* [6699] */
    (xdc_Char)0x25,  /* [6700] */
    (xdc_Char)0x64,  /* [6701] */
    (xdc_Char)0x2c,  /* [6702] */
    (xdc_Char)0x20,  /* [6703] */
    (xdc_Char)0x63,  /* [6704] */
    (xdc_Char)0x75,  /* [6705] */
    (xdc_Char)0x72,  /* [6706] */
    (xdc_Char)0x53,  /* [6707] */
    (xdc_Char)0x65,  /* [6708] */
    (xdc_Char)0x74,  /* [6709] */
    (xdc_Char)0x58,  /* [6710] */
    (xdc_Char)0x3a,  /* [6711] */
    (xdc_Char)0x20,  /* [6712] */
    (xdc_Char)0x25,  /* [6713] */
    (xdc_Char)0x64,  /* [6714] */
    (xdc_Char)0x2c,  /* [6715] */
    (xdc_Char)0x20,  /* [6716] */
    (xdc_Char)0x63,  /* [6717] */
    (xdc_Char)0x75,  /* [6718] */
    (xdc_Char)0x72,  /* [6719] */
    (xdc_Char)0x4d,  /* [6720] */
    (xdc_Char)0x61,  /* [6721] */
    (xdc_Char)0x73,  /* [6722] */
    (xdc_Char)0x6b,  /* [6723] */
    (xdc_Char)0x4c,  /* [6724] */
    (xdc_Char)0x6f,  /* [6725] */
    (xdc_Char)0x63,  /* [6726] */
    (xdc_Char)0x61,  /* [6727] */
    (xdc_Char)0x6c,  /* [6728] */
    (xdc_Char)0x3a,  /* [6729] */
    (xdc_Char)0x20,  /* [6730] */
    (xdc_Char)0x25,  /* [6731] */
    (xdc_Char)0x64,  /* [6732] */
    (xdc_Char)0x0,  /* [6733] */
    (xdc_Char)0x4c,  /* [6734] */
    (xdc_Char)0x44,  /* [6735] */
    (xdc_Char)0x5f,  /* [6736] */
    (xdc_Char)0x6e,  /* [6737] */
    (xdc_Char)0x6f,  /* [6738] */
    (xdc_Char)0x57,  /* [6739] */
    (xdc_Char)0x6f,  /* [6740] */
    (xdc_Char)0x72,  /* [6741] */
    (xdc_Char)0x6b,  /* [6742] */
    (xdc_Char)0x3a,  /* [6743] */
    (xdc_Char)0x20,  /* [6744] */
    (xdc_Char)0x63,  /* [6745] */
    (xdc_Char)0x6f,  /* [6746] */
    (xdc_Char)0x72,  /* [6747] */
    (xdc_Char)0x65,  /* [6748] */
    (xdc_Char)0x49,  /* [6749] */
    (xdc_Char)0x64,  /* [6750] */
    (xdc_Char)0x3a,  /* [6751] */
    (xdc_Char)0x20,  /* [6752] */
    (xdc_Char)0x25,  /* [6753] */
    (xdc_Char)0x64,  /* [6754] */
    (xdc_Char)0x2c,  /* [6755] */
    (xdc_Char)0x20,  /* [6756] */
    (xdc_Char)0x63,  /* [6757] */
    (xdc_Char)0x75,  /* [6758] */
    (xdc_Char)0x72,  /* [6759] */
    (xdc_Char)0x53,  /* [6760] */
    (xdc_Char)0x65,  /* [6761] */
    (xdc_Char)0x74,  /* [6762] */
    (xdc_Char)0x4c,  /* [6763] */
    (xdc_Char)0x6f,  /* [6764] */
    (xdc_Char)0x63,  /* [6765] */
    (xdc_Char)0x61,  /* [6766] */
    (xdc_Char)0x6c,  /* [6767] */
    (xdc_Char)0x3a,  /* [6768] */
    (xdc_Char)0x20,  /* [6769] */
    (xdc_Char)0x25,  /* [6770] */
    (xdc_Char)0x64,  /* [6771] */
    (xdc_Char)0x2c,  /* [6772] */
    (xdc_Char)0x20,  /* [6773] */
    (xdc_Char)0x63,  /* [6774] */
    (xdc_Char)0x75,  /* [6775] */
    (xdc_Char)0x72,  /* [6776] */
    (xdc_Char)0x53,  /* [6777] */
    (xdc_Char)0x65,  /* [6778] */
    (xdc_Char)0x74,  /* [6779] */
    (xdc_Char)0x58,  /* [6780] */
    (xdc_Char)0x3a,  /* [6781] */
    (xdc_Char)0x20,  /* [6782] */
    (xdc_Char)0x25,  /* [6783] */
    (xdc_Char)0x64,  /* [6784] */
    (xdc_Char)0x2c,  /* [6785] */
    (xdc_Char)0x20,  /* [6786] */
    (xdc_Char)0x63,  /* [6787] */
    (xdc_Char)0x75,  /* [6788] */
    (xdc_Char)0x72,  /* [6789] */
    (xdc_Char)0x4d,  /* [6790] */
    (xdc_Char)0x61,  /* [6791] */
    (xdc_Char)0x73,  /* [6792] */
    (xdc_Char)0x6b,  /* [6793] */
    (xdc_Char)0x4c,  /* [6794] */
    (xdc_Char)0x6f,  /* [6795] */
    (xdc_Char)0x63,  /* [6796] */
    (xdc_Char)0x61,  /* [6797] */
    (xdc_Char)0x6c,  /* [6798] */
    (xdc_Char)0x3a,  /* [6799] */
    (xdc_Char)0x20,  /* [6800] */
    (xdc_Char)0x25,  /* [6801] */
    (xdc_Char)0x64,  /* [6802] */
    (xdc_Char)0x0,  /* [6803] */
    (xdc_Char)0x4c,  /* [6804] */
    (xdc_Char)0x53,  /* [6805] */
    (xdc_Char)0x5f,  /* [6806] */
    (xdc_Char)0x63,  /* [6807] */
    (xdc_Char)0x70,  /* [6808] */
    (xdc_Char)0x75,  /* [6809] */
    (xdc_Char)0x4c,  /* [6810] */
    (xdc_Char)0x6f,  /* [6811] */
    (xdc_Char)0x61,  /* [6812] */
    (xdc_Char)0x64,  /* [6813] */
    (xdc_Char)0x3a,  /* [6814] */
    (xdc_Char)0x20,  /* [6815] */
    (xdc_Char)0x25,  /* [6816] */
    (xdc_Char)0x64,  /* [6817] */
    (xdc_Char)0x25,  /* [6818] */
    (xdc_Char)0x25,  /* [6819] */
    (xdc_Char)0x0,  /* [6820] */
    (xdc_Char)0x4c,  /* [6821] */
    (xdc_Char)0x53,  /* [6822] */
    (xdc_Char)0x5f,  /* [6823] */
    (xdc_Char)0x68,  /* [6824] */
    (xdc_Char)0x77,  /* [6825] */
    (xdc_Char)0x69,  /* [6826] */
    (xdc_Char)0x4c,  /* [6827] */
    (xdc_Char)0x6f,  /* [6828] */
    (xdc_Char)0x61,  /* [6829] */
    (xdc_Char)0x64,  /* [6830] */
    (xdc_Char)0x3a,  /* [6831] */
    (xdc_Char)0x20,  /* [6832] */
    (xdc_Char)0x25,  /* [6833] */
    (xdc_Char)0x64,  /* [6834] */
    (xdc_Char)0x2c,  /* [6835] */
    (xdc_Char)0x25,  /* [6836] */
    (xdc_Char)0x64,  /* [6837] */
    (xdc_Char)0x0,  /* [6838] */
    (xdc_Char)0x4c,  /* [6839] */
    (xdc_Char)0x53,  /* [6840] */
    (xdc_Char)0x5f,  /* [6841] */
    (xdc_Char)0x73,  /* [6842] */
    (xdc_Char)0x77,  /* [6843] */
    (xdc_Char)0x69,  /* [6844] */
    (xdc_Char)0x4c,  /* [6845] */
    (xdc_Char)0x6f,  /* [6846] */
    (xdc_Char)0x61,  /* [6847] */
    (xdc_Char)0x64,  /* [6848] */
    (xdc_Char)0x3a,  /* [6849] */
    (xdc_Char)0x20,  /* [6850] */
    (xdc_Char)0x25,  /* [6851] */
    (xdc_Char)0x64,  /* [6852] */
    (xdc_Char)0x2c,  /* [6853] */
    (xdc_Char)0x25,  /* [6854] */
    (xdc_Char)0x64,  /* [6855] */
    (xdc_Char)0x0,  /* [6856] */
    (xdc_Char)0x4c,  /* [6857] */
    (xdc_Char)0x53,  /* [6858] */
    (xdc_Char)0x5f,  /* [6859] */
    (xdc_Char)0x74,  /* [6860] */
    (xdc_Char)0x61,  /* [6861] */
    (xdc_Char)0x73,  /* [6862] */
    (xdc_Char)0x6b,  /* [6863] */
    (xdc_Char)0x4c,  /* [6864] */
    (xdc_Char)0x6f,  /* [6865] */
    (xdc_Char)0x61,  /* [6866] */
    (xdc_Char)0x64,  /* [6867] */
    (xdc_Char)0x3a,  /* [6868] */
    (xdc_Char)0x20,  /* [6869] */
    (xdc_Char)0x30,  /* [6870] */
    (xdc_Char)0x78,  /* [6871] */
    (xdc_Char)0x25,  /* [6872] */
    (xdc_Char)0x78,  /* [6873] */
    (xdc_Char)0x2c,  /* [6874] */
    (xdc_Char)0x25,  /* [6875] */
    (xdc_Char)0x64,  /* [6876] */
    (xdc_Char)0x2c,  /* [6877] */
    (xdc_Char)0x25,  /* [6878] */
    (xdc_Char)0x64,  /* [6879] */
    (xdc_Char)0x2c,  /* [6880] */
    (xdc_Char)0x30,  /* [6881] */
    (xdc_Char)0x78,  /* [6882] */
    (xdc_Char)0x25,  /* [6883] */
    (xdc_Char)0x78,  /* [6884] */
    (xdc_Char)0x0,  /* [6885] */
    (xdc_Char)0x78,  /* [6886] */
    (xdc_Char)0x64,  /* [6887] */
    (xdc_Char)0x63,  /* [6888] */
    (xdc_Char)0x2e,  /* [6889] */
    (xdc_Char)0x0,  /* [6890] */
    (xdc_Char)0x72,  /* [6891] */
    (xdc_Char)0x75,  /* [6892] */
    (xdc_Char)0x6e,  /* [6893] */
    (xdc_Char)0x74,  /* [6894] */
    (xdc_Char)0x69,  /* [6895] */
    (xdc_Char)0x6d,  /* [6896] */
    (xdc_Char)0x65,  /* [6897] */
    (xdc_Char)0x2e,  /* [6898] */
    (xdc_Char)0x0,  /* [6899] */
    (xdc_Char)0x41,  /* [6900] */
    (xdc_Char)0x73,  /* [6901] */
    (xdc_Char)0x73,  /* [6902] */
    (xdc_Char)0x65,  /* [6903] */
    (xdc_Char)0x72,  /* [6904] */
    (xdc_Char)0x74,  /* [6905] */
    (xdc_Char)0x0,  /* [6906] */
    (xdc_Char)0x43,  /* [6907] */
    (xdc_Char)0x6f,  /* [6908] */
    (xdc_Char)0x72,  /* [6909] */
    (xdc_Char)0x65,  /* [6910] */
    (xdc_Char)0x0,  /* [6911] */
    (xdc_Char)0x44,  /* [6912] */
    (xdc_Char)0x65,  /* [6913] */
    (xdc_Char)0x66,  /* [6914] */
    (xdc_Char)0x61,  /* [6915] */
    (xdc_Char)0x75,  /* [6916] */
    (xdc_Char)0x6c,  /* [6917] */
    (xdc_Char)0x74,  /* [6918] */
    (xdc_Char)0x73,  /* [6919] */
    (xdc_Char)0x0,  /* [6920] */
    (xdc_Char)0x44,  /* [6921] */
    (xdc_Char)0x69,  /* [6922] */
    (xdc_Char)0x61,  /* [6923] */
    (xdc_Char)0x67,  /* [6924] */
    (xdc_Char)0x73,  /* [6925] */
    (xdc_Char)0x0,  /* [6926] */
    (xdc_Char)0x45,  /* [6927] */
    (xdc_Char)0x72,  /* [6928] */
    (xdc_Char)0x72,  /* [6929] */
    (xdc_Char)0x6f,  /* [6930] */
    (xdc_Char)0x72,  /* [6931] */
    (xdc_Char)0x0,  /* [6932] */
    (xdc_Char)0x47,  /* [6933] */
    (xdc_Char)0x61,  /* [6934] */
    (xdc_Char)0x74,  /* [6935] */
    (xdc_Char)0x65,  /* [6936] */
    (xdc_Char)0x0,  /* [6937] */
    (xdc_Char)0x4c,  /* [6938] */
    (xdc_Char)0x6f,  /* [6939] */
    (xdc_Char)0x67,  /* [6940] */
    (xdc_Char)0x0,  /* [6941] */
    (xdc_Char)0x4d,  /* [6942] */
    (xdc_Char)0x61,  /* [6943] */
    (xdc_Char)0x69,  /* [6944] */
    (xdc_Char)0x6e,  /* [6945] */
    (xdc_Char)0x0,  /* [6946] */
    (xdc_Char)0x4d,  /* [6947] */
    (xdc_Char)0x65,  /* [6948] */
    (xdc_Char)0x6d,  /* [6949] */
    (xdc_Char)0x6f,  /* [6950] */
    (xdc_Char)0x72,  /* [6951] */
    (xdc_Char)0x79,  /* [6952] */
    (xdc_Char)0x0,  /* [6953] */
    (xdc_Char)0x52,  /* [6954] */
    (xdc_Char)0x65,  /* [6955] */
    (xdc_Char)0x67,  /* [6956] */
    (xdc_Char)0x69,  /* [6957] */
    (xdc_Char)0x73,  /* [6958] */
    (xdc_Char)0x74,  /* [6959] */
    (xdc_Char)0x72,  /* [6960] */
    (xdc_Char)0x79,  /* [6961] */
    (xdc_Char)0x0,  /* [6962] */
    (xdc_Char)0x53,  /* [6963] */
    (xdc_Char)0x74,  /* [6964] */
    (xdc_Char)0x61,  /* [6965] */
    (xdc_Char)0x72,  /* [6966] */
    (xdc_Char)0x74,  /* [6967] */
    (xdc_Char)0x75,  /* [6968] */
    (xdc_Char)0x70,  /* [6969] */
    (xdc_Char)0x0,  /* [6970] */
    (xdc_Char)0x53,  /* [6971] */
    (xdc_Char)0x79,  /* [6972] */
    (xdc_Char)0x73,  /* [6973] */
    (xdc_Char)0x74,  /* [6974] */
    (xdc_Char)0x65,  /* [6975] */
    (xdc_Char)0x6d,  /* [6976] */
    (xdc_Char)0x0,  /* [6977] */
    (xdc_Char)0x53,  /* [6978] */
    (xdc_Char)0x79,  /* [6979] */
    (xdc_Char)0x73,  /* [6980] */
    (xdc_Char)0x4d,  /* [6981] */
    (xdc_Char)0x69,  /* [6982] */
    (xdc_Char)0x6e,  /* [6983] */
    (xdc_Char)0x0,  /* [6984] */
    (xdc_Char)0x53,  /* [6985] */
    (xdc_Char)0x79,  /* [6986] */
    (xdc_Char)0x73,  /* [6987] */
    (xdc_Char)0x53,  /* [6988] */
    (xdc_Char)0x74,  /* [6989] */
    (xdc_Char)0x64,  /* [6990] */
    (xdc_Char)0x0,  /* [6991] */
    (xdc_Char)0x54,  /* [6992] */
    (xdc_Char)0x65,  /* [6993] */
    (xdc_Char)0x78,  /* [6994] */
    (xdc_Char)0x74,  /* [6995] */
    (xdc_Char)0x0,  /* [6996] */
    (xdc_Char)0x54,  /* [6997] */
    (xdc_Char)0x69,  /* [6998] */
    (xdc_Char)0x6d,  /* [6999] */
    (xdc_Char)0x65,  /* [7000] */
    (xdc_Char)0x73,  /* [7001] */
    (xdc_Char)0x74,  /* [7002] */
    (xdc_Char)0x61,  /* [7003] */
    (xdc_Char)0x6d,  /* [7004] */
    (xdc_Char)0x70,  /* [7005] */
    (xdc_Char)0x0,  /* [7006] */
    (xdc_Char)0x54,  /* [7007] */
    (xdc_Char)0x69,  /* [7008] */
    (xdc_Char)0x6d,  /* [7009] */
    (xdc_Char)0x65,  /* [7010] */
    (xdc_Char)0x73,  /* [7011] */
    (xdc_Char)0x74,  /* [7012] */
    (xdc_Char)0x61,  /* [7013] */
    (xdc_Char)0x6d,  /* [7014] */
    (xdc_Char)0x70,  /* [7015] */
    (xdc_Char)0x4e,  /* [7016] */
    (xdc_Char)0x75,  /* [7017] */
    (xdc_Char)0x6c,  /* [7018] */
    (xdc_Char)0x6c,  /* [7019] */
    (xdc_Char)0x0,  /* [7020] */
    (xdc_Char)0x54,  /* [7021] */
    (xdc_Char)0x79,  /* [7022] */
    (xdc_Char)0x70,  /* [7023] */
    (xdc_Char)0x65,  /* [7024] */
    (xdc_Char)0x73,  /* [7025] */
    (xdc_Char)0x0,  /* [7026] */
    (xdc_Char)0x74,  /* [7027] */
    (xdc_Char)0x69,  /* [7028] */
    (xdc_Char)0x2e,  /* [7029] */
    (xdc_Char)0x0,  /* [7030] */
    (xdc_Char)0x73,  /* [7031] */
    (xdc_Char)0x79,  /* [7032] */
    (xdc_Char)0x73,  /* [7033] */
    (xdc_Char)0x62,  /* [7034] */
    (xdc_Char)0x69,  /* [7035] */
    (xdc_Char)0x6f,  /* [7036] */
    (xdc_Char)0x73,  /* [7037] */
    (xdc_Char)0x2e,  /* [7038] */
    (xdc_Char)0x0,  /* [7039] */
    (xdc_Char)0x66,  /* [7040] */
    (xdc_Char)0x61,  /* [7041] */
    (xdc_Char)0x6d,  /* [7042] */
    (xdc_Char)0x69,  /* [7043] */
    (xdc_Char)0x6c,  /* [7044] */
    (xdc_Char)0x79,  /* [7045] */
    (xdc_Char)0x2e,  /* [7046] */
    (xdc_Char)0x0,  /* [7047] */
    (xdc_Char)0x63,  /* [7048] */
    (xdc_Char)0x37,  /* [7049] */
    (xdc_Char)0x78,  /* [7050] */
    (xdc_Char)0x2e,  /* [7051] */
    (xdc_Char)0x0,  /* [7052] */
    (xdc_Char)0x43,  /* [7053] */
    (xdc_Char)0x61,  /* [7054] */
    (xdc_Char)0x63,  /* [7055] */
    (xdc_Char)0x68,  /* [7056] */
    (xdc_Char)0x65,  /* [7057] */
    (xdc_Char)0x0,  /* [7058] */
    (xdc_Char)0x45,  /* [7059] */
    (xdc_Char)0x78,  /* [7060] */
    (xdc_Char)0x63,  /* [7061] */
    (xdc_Char)0x65,  /* [7062] */
    (xdc_Char)0x70,  /* [7063] */
    (xdc_Char)0x74,  /* [7064] */
    (xdc_Char)0x69,  /* [7065] */
    (xdc_Char)0x6f,  /* [7066] */
    (xdc_Char)0x6e,  /* [7067] */
    (xdc_Char)0x0,  /* [7068] */
    (xdc_Char)0x48,  /* [7069] */
    (xdc_Char)0x77,  /* [7070] */
    (xdc_Char)0x69,  /* [7071] */
    (xdc_Char)0x0,  /* [7072] */
    (xdc_Char)0x49,  /* [7073] */
    (xdc_Char)0x6e,  /* [7074] */
    (xdc_Char)0x74,  /* [7075] */
    (xdc_Char)0x72,  /* [7076] */
    (xdc_Char)0x69,  /* [7077] */
    (xdc_Char)0x6e,  /* [7078] */
    (xdc_Char)0x73,  /* [7079] */
    (xdc_Char)0x69,  /* [7080] */
    (xdc_Char)0x63,  /* [7081] */
    (xdc_Char)0x73,  /* [7082] */
    (xdc_Char)0x53,  /* [7083] */
    (xdc_Char)0x75,  /* [7084] */
    (xdc_Char)0x70,  /* [7085] */
    (xdc_Char)0x70,  /* [7086] */
    (xdc_Char)0x6f,  /* [7087] */
    (xdc_Char)0x72,  /* [7088] */
    (xdc_Char)0x74,  /* [7089] */
    (xdc_Char)0x0,  /* [7090] */
    (xdc_Char)0x4d,  /* [7091] */
    (xdc_Char)0x6d,  /* [7092] */
    (xdc_Char)0x75,  /* [7093] */
    (xdc_Char)0x0,  /* [7094] */
    (xdc_Char)0x54,  /* [7095] */
    (xdc_Char)0x61,  /* [7096] */
    (xdc_Char)0x73,  /* [7097] */
    (xdc_Char)0x6b,  /* [7098] */
    (xdc_Char)0x53,  /* [7099] */
    (xdc_Char)0x75,  /* [7100] */
    (xdc_Char)0x70,  /* [7101] */
    (xdc_Char)0x70,  /* [7102] */
    (xdc_Char)0x6f,  /* [7103] */
    (xdc_Char)0x72,  /* [7104] */
    (xdc_Char)0x74,  /* [7105] */
    (xdc_Char)0x0,  /* [7106] */
    (xdc_Char)0x54,  /* [7107] */
    (xdc_Char)0x69,  /* [7108] */
    (xdc_Char)0x6d,  /* [7109] */
    (xdc_Char)0x65,  /* [7110] */
    (xdc_Char)0x73,  /* [7111] */
    (xdc_Char)0x74,  /* [7112] */
    (xdc_Char)0x61,  /* [7113] */
    (xdc_Char)0x6d,  /* [7114] */
    (xdc_Char)0x70,  /* [7115] */
    (xdc_Char)0x50,  /* [7116] */
    (xdc_Char)0x72,  /* [7117] */
    (xdc_Char)0x6f,  /* [7118] */
    (xdc_Char)0x76,  /* [7119] */
    (xdc_Char)0x69,  /* [7120] */
    (xdc_Char)0x64,  /* [7121] */
    (xdc_Char)0x65,  /* [7122] */
    (xdc_Char)0x72,  /* [7123] */
    (xdc_Char)0x0,  /* [7124] */
    (xdc_Char)0x42,  /* [7125] */
    (xdc_Char)0x49,  /* [7126] */
    (xdc_Char)0x4f,  /* [7127] */
    (xdc_Char)0x53,  /* [7128] */
    (xdc_Char)0x0,  /* [7129] */
    (xdc_Char)0x6b,  /* [7130] */
    (xdc_Char)0x6e,  /* [7131] */
    (xdc_Char)0x6c,  /* [7132] */
    (xdc_Char)0x2e,  /* [7133] */
    (xdc_Char)0x0,  /* [7134] */
    (xdc_Char)0x43,  /* [7135] */
    (xdc_Char)0x6c,  /* [7136] */
    (xdc_Char)0x6f,  /* [7137] */
    (xdc_Char)0x63,  /* [7138] */
    (xdc_Char)0x6b,  /* [7139] */
    (xdc_Char)0x0,  /* [7140] */
    (xdc_Char)0x49,  /* [7141] */
    (xdc_Char)0x64,  /* [7142] */
    (xdc_Char)0x6c,  /* [7143] */
    (xdc_Char)0x65,  /* [7144] */
    (xdc_Char)0x0,  /* [7145] */
    (xdc_Char)0x49,  /* [7146] */
    (xdc_Char)0x6e,  /* [7147] */
    (xdc_Char)0x74,  /* [7148] */
    (xdc_Char)0x72,  /* [7149] */
    (xdc_Char)0x69,  /* [7150] */
    (xdc_Char)0x6e,  /* [7151] */
    (xdc_Char)0x73,  /* [7152] */
    (xdc_Char)0x69,  /* [7153] */
    (xdc_Char)0x63,  /* [7154] */
    (xdc_Char)0x73,  /* [7155] */
    (xdc_Char)0x0,  /* [7156] */
    (xdc_Char)0x45,  /* [7157] */
    (xdc_Char)0x76,  /* [7158] */
    (xdc_Char)0x65,  /* [7159] */
    (xdc_Char)0x6e,  /* [7160] */
    (xdc_Char)0x74,  /* [7161] */
    (xdc_Char)0x0,  /* [7162] */
    (xdc_Char)0x51,  /* [7163] */
    (xdc_Char)0x75,  /* [7164] */
    (xdc_Char)0x65,  /* [7165] */
    (xdc_Char)0x75,  /* [7166] */
    (xdc_Char)0x65,  /* [7167] */
    (xdc_Char)0x0,  /* [7168] */
    (xdc_Char)0x53,  /* [7169] */
    (xdc_Char)0x65,  /* [7170] */
    (xdc_Char)0x6d,  /* [7171] */
    (xdc_Char)0x61,  /* [7172] */
    (xdc_Char)0x70,  /* [7173] */
    (xdc_Char)0x68,  /* [7174] */
    (xdc_Char)0x6f,  /* [7175] */
    (xdc_Char)0x72,  /* [7176] */
    (xdc_Char)0x65,  /* [7177] */
    (xdc_Char)0x0,  /* [7178] */
    (xdc_Char)0x53,  /* [7179] */
    (xdc_Char)0x77,  /* [7180] */
    (xdc_Char)0x69,  /* [7181] */
    (xdc_Char)0x0,  /* [7182] */
    (xdc_Char)0x54,  /* [7183] */
    (xdc_Char)0x61,  /* [7184] */
    (xdc_Char)0x73,  /* [7185] */
    (xdc_Char)0x6b,  /* [7186] */
    (xdc_Char)0x0,  /* [7187] */
    (xdc_Char)0x47,  /* [7188] */
    (xdc_Char)0x61,  /* [7189] */
    (xdc_Char)0x74,  /* [7190] */
    (xdc_Char)0x65,  /* [7191] */
    (xdc_Char)0x48,  /* [7192] */
    (xdc_Char)0x0,  /* [7193] */
    (xdc_Char)0x47,  /* [7194] */
    (xdc_Char)0x61,  /* [7195] */
    (xdc_Char)0x74,  /* [7196] */
    (xdc_Char)0x65,  /* [7197] */
    (xdc_Char)0x54,  /* [7198] */
    (xdc_Char)0x68,  /* [7199] */
    (xdc_Char)0x72,  /* [7200] */
    (xdc_Char)0x65,  /* [7201] */
    (xdc_Char)0x61,  /* [7202] */
    (xdc_Char)0x64,  /* [7203] */
    (xdc_Char)0x0,  /* [7204] */
    (xdc_Char)0x68,  /* [7205] */
    (xdc_Char)0x61,  /* [7206] */
    (xdc_Char)0x6c,  /* [7207] */
    (xdc_Char)0x2e,  /* [7208] */
    (xdc_Char)0x0,  /* [7209] */
    (xdc_Char)0x43,  /* [7210] */
    (xdc_Char)0x6f,  /* [7211] */
    (xdc_Char)0x72,  /* [7212] */
    (xdc_Char)0x65,  /* [7213] */
    (xdc_Char)0x4e,  /* [7214] */
    (xdc_Char)0x75,  /* [7215] */
    (xdc_Char)0x6c,  /* [7216] */
    (xdc_Char)0x6c,  /* [7217] */
    (xdc_Char)0x0,  /* [7218] */
    (xdc_Char)0x68,  /* [7219] */
    (xdc_Char)0x65,  /* [7220] */
    (xdc_Char)0x61,  /* [7221] */
    (xdc_Char)0x70,  /* [7222] */
    (xdc_Char)0x73,  /* [7223] */
    (xdc_Char)0x2e,  /* [7224] */
    (xdc_Char)0x0,  /* [7225] */
    (xdc_Char)0x48,  /* [7226] */
    (xdc_Char)0x65,  /* [7227] */
    (xdc_Char)0x61,  /* [7228] */
    (xdc_Char)0x70,  /* [7229] */
    (xdc_Char)0x42,  /* [7230] */
    (xdc_Char)0x75,  /* [7231] */
    (xdc_Char)0x66,  /* [7232] */
    (xdc_Char)0x0,  /* [7233] */
    (xdc_Char)0x48,  /* [7234] */
    (xdc_Char)0x65,  /* [7235] */
    (xdc_Char)0x61,  /* [7236] */
    (xdc_Char)0x70,  /* [7237] */
    (xdc_Char)0x4d,  /* [7238] */
    (xdc_Char)0x65,  /* [7239] */
    (xdc_Char)0x6d,  /* [7240] */
    (xdc_Char)0x0,  /* [7241] */
    (xdc_Char)0x73,  /* [7242] */
    (xdc_Char)0x79,  /* [7243] */
    (xdc_Char)0x6e,  /* [7244] */
    (xdc_Char)0x63,  /* [7245] */
    (xdc_Char)0x73,  /* [7246] */
    (xdc_Char)0x2e,  /* [7247] */
    (xdc_Char)0x0,  /* [7248] */
    (xdc_Char)0x53,  /* [7249] */
    (xdc_Char)0x79,  /* [7250] */
    (xdc_Char)0x6e,  /* [7251] */
    (xdc_Char)0x63,  /* [7252] */
    (xdc_Char)0x53,  /* [7253] */
    (xdc_Char)0x65,  /* [7254] */
    (xdc_Char)0x6d,  /* [7255] */
    (xdc_Char)0x0,  /* [7256] */
    (xdc_Char)0x67,  /* [7257] */
    (xdc_Char)0x61,  /* [7258] */
    (xdc_Char)0x74,  /* [7259] */
    (xdc_Char)0x65,  /* [7260] */
    (xdc_Char)0x73,  /* [7261] */
    (xdc_Char)0x2e,  /* [7262] */
    (xdc_Char)0x0,  /* [7263] */
    (xdc_Char)0x47,  /* [7264] */
    (xdc_Char)0x61,  /* [7265] */
    (xdc_Char)0x74,  /* [7266] */
    (xdc_Char)0x65,  /* [7267] */
    (xdc_Char)0x48,  /* [7268] */
    (xdc_Char)0x77,  /* [7269] */
    (xdc_Char)0x69,  /* [7270] */
    (xdc_Char)0x0,  /* [7271] */
    (xdc_Char)0x47,  /* [7272] */
    (xdc_Char)0x61,  /* [7273] */
    (xdc_Char)0x74,  /* [7274] */
    (xdc_Char)0x65,  /* [7275] */
    (xdc_Char)0x53,  /* [7276] */
    (xdc_Char)0x77,  /* [7277] */
    (xdc_Char)0x69,  /* [7278] */
    (xdc_Char)0x0,  /* [7279] */
    (xdc_Char)0x47,  /* [7280] */
    (xdc_Char)0x61,  /* [7281] */
    (xdc_Char)0x74,  /* [7282] */
    (xdc_Char)0x65,  /* [7283] */
    (xdc_Char)0x54,  /* [7284] */
    (xdc_Char)0x61,  /* [7285] */
    (xdc_Char)0x73,  /* [7286] */
    (xdc_Char)0x6b,  /* [7287] */
    (xdc_Char)0x0,  /* [7288] */
    (xdc_Char)0x47,  /* [7289] */
    (xdc_Char)0x61,  /* [7290] */
    (xdc_Char)0x74,  /* [7291] */
    (xdc_Char)0x65,  /* [7292] */
    (xdc_Char)0x4d,  /* [7293] */
    (xdc_Char)0x75,  /* [7294] */
    (xdc_Char)0x74,  /* [7295] */
    (xdc_Char)0x65,  /* [7296] */
    (xdc_Char)0x78,  /* [7297] */
    (xdc_Char)0x50,  /* [7298] */
    (xdc_Char)0x72,  /* [7299] */
    (xdc_Char)0x69,  /* [7300] */
    (xdc_Char)0x0,  /* [7301] */
    (xdc_Char)0x47,  /* [7302] */
    (xdc_Char)0x61,  /* [7303] */
    (xdc_Char)0x74,  /* [7304] */
    (xdc_Char)0x65,  /* [7305] */
    (xdc_Char)0x4d,  /* [7306] */
    (xdc_Char)0x75,  /* [7307] */
    (xdc_Char)0x74,  /* [7308] */
    (xdc_Char)0x65,  /* [7309] */
    (xdc_Char)0x78,  /* [7310] */
    (xdc_Char)0x0,  /* [7311] */
    (xdc_Char)0x75,  /* [7312] */
    (xdc_Char)0x74,  /* [7313] */
    (xdc_Char)0x69,  /* [7314] */
    (xdc_Char)0x6c,  /* [7315] */
    (xdc_Char)0x73,  /* [7316] */
    (xdc_Char)0x2e,  /* [7317] */
    (xdc_Char)0x0,  /* [7318] */
    (xdc_Char)0x4c,  /* [7319] */
    (xdc_Char)0x6f,  /* [7320] */
    (xdc_Char)0x61,  /* [7321] */
    (xdc_Char)0x64,  /* [7322] */
    (xdc_Char)0x0,  /* [7323] */
    (xdc_Char)0x74,  /* [7324] */
    (xdc_Char)0x69,  /* [7325] */
    (xdc_Char)0x6d,  /* [7326] */
    (xdc_Char)0x65,  /* [7327] */
    (xdc_Char)0x72,  /* [7328] */
    (xdc_Char)0x73,  /* [7329] */
    (xdc_Char)0x2e,  /* [7330] */
    (xdc_Char)0x0,  /* [7331] */
    (xdc_Char)0x64,  /* [7332] */
    (xdc_Char)0x6d,  /* [7333] */
    (xdc_Char)0x74,  /* [7334] */
    (xdc_Char)0x69,  /* [7335] */
    (xdc_Char)0x6d,  /* [7336] */
    (xdc_Char)0x65,  /* [7337] */
    (xdc_Char)0x72,  /* [7338] */
    (xdc_Char)0x2e,  /* [7339] */
    (xdc_Char)0x0,  /* [7340] */
    (xdc_Char)0x54,  /* [7341] */
    (xdc_Char)0x69,  /* [7342] */
    (xdc_Char)0x6d,  /* [7343] */
    (xdc_Char)0x65,  /* [7344] */
    (xdc_Char)0x72,  /* [7345] */
    (xdc_Char)0x0,  /* [7346] */
    (xdc_Char)0x63,  /* [7347] */
    (xdc_Char)0x36,  /* [7348] */
    (xdc_Char)0x34,  /* [7349] */
    (xdc_Char)0x70,  /* [7350] */
    (xdc_Char)0x2e,  /* [7351] */
    (xdc_Char)0x0,  /* [7352] */
    (xdc_Char)0x74,  /* [7353] */
    (xdc_Char)0x63,  /* [7354] */
    (xdc_Char)0x69,  /* [7355] */
    (xdc_Char)0x36,  /* [7356] */
    (xdc_Char)0x34,  /* [7357] */
    (xdc_Char)0x38,  /* [7358] */
    (xdc_Char)0x38,  /* [7359] */
    (xdc_Char)0x2e,  /* [7360] */
    (xdc_Char)0x0,  /* [7361] */
    (xdc_Char)0x54,  /* [7362] */
    (xdc_Char)0x69,  /* [7363] */
    (xdc_Char)0x6d,  /* [7364] */
    (xdc_Char)0x65,  /* [7365] */
    (xdc_Char)0x72,  /* [7366] */
    (xdc_Char)0x53,  /* [7367] */
    (xdc_Char)0x75,  /* [7368] */
    (xdc_Char)0x70,  /* [7369] */
    (xdc_Char)0x70,  /* [7370] */
    (xdc_Char)0x6f,  /* [7371] */
    (xdc_Char)0x72,  /* [7372] */
    (xdc_Char)0x74,  /* [7373] */
    (xdc_Char)0x0,  /* [7374] */
    (xdc_Char)0x78,  /* [7375] */
    (xdc_Char)0x64,  /* [7376] */
    (xdc_Char)0x63,  /* [7377] */
    (xdc_Char)0x72,  /* [7378] */
    (xdc_Char)0x75,  /* [7379] */
    (xdc_Char)0x6e,  /* [7380] */
    (xdc_Char)0x74,  /* [7381] */
    (xdc_Char)0x69,  /* [7382] */
    (xdc_Char)0x6d,  /* [7383] */
    (xdc_Char)0x65,  /* [7384] */
    (xdc_Char)0x2e,  /* [7385] */
    (xdc_Char)0x0,  /* [7386] */
    (xdc_Char)0x47,  /* [7387] */
    (xdc_Char)0x61,  /* [7388] */
    (xdc_Char)0x74,  /* [7389] */
    (xdc_Char)0x65,  /* [7390] */
    (xdc_Char)0x54,  /* [7391] */
    (xdc_Char)0x68,  /* [7392] */
    (xdc_Char)0x72,  /* [7393] */
    (xdc_Char)0x65,  /* [7394] */
    (xdc_Char)0x61,  /* [7395] */
    (xdc_Char)0x64,  /* [7396] */
    (xdc_Char)0x53,  /* [7397] */
    (xdc_Char)0x75,  /* [7398] */
    (xdc_Char)0x70,  /* [7399] */
    (xdc_Char)0x70,  /* [7400] */
    (xdc_Char)0x6f,  /* [7401] */
    (xdc_Char)0x72,  /* [7402] */
    (xdc_Char)0x74,  /* [7403] */
    (xdc_Char)0x0,  /* [7404] */
    (xdc_Char)0x74,  /* [7405] */
    (xdc_Char)0x69,  /* [7406] */
    (xdc_Char)0x2e,  /* [7407] */
    (xdc_Char)0x73,  /* [7408] */
    (xdc_Char)0x79,  /* [7409] */
    (xdc_Char)0x73,  /* [7410] */
    (xdc_Char)0x62,  /* [7411] */
    (xdc_Char)0x69,  /* [7412] */
    (xdc_Char)0x6f,  /* [7413] */
    (xdc_Char)0x73,  /* [7414] */
    (xdc_Char)0x2e,  /* [7415] */
    (xdc_Char)0x6b,  /* [7416] */
    (xdc_Char)0x6e,  /* [7417] */
    (xdc_Char)0x6c,  /* [7418] */
    (xdc_Char)0x2e,  /* [7419] */
    (xdc_Char)0x54,  /* [7420] */
    (xdc_Char)0x61,  /* [7421] */
    (xdc_Char)0x73,  /* [7422] */
    (xdc_Char)0x6b,  /* [7423] */
    (xdc_Char)0x2e,  /* [7424] */
    (xdc_Char)0x49,  /* [7425] */
    (xdc_Char)0x64,  /* [7426] */
    (xdc_Char)0x6c,  /* [7427] */
    (xdc_Char)0x65,  /* [7428] */
    (xdc_Char)0x54,  /* [7429] */
    (xdc_Char)0x61,  /* [7430] */
    (xdc_Char)0x73,  /* [7431] */
    (xdc_Char)0x6b,  /* [7432] */
    (xdc_Char)0x0,  /* [7433] */
};

/* --> xdc_runtime_Text_nodeTab__A */
#pragma DATA_SECTION(xdc_runtime_Text_nodeTab__A, ".const:xdc_runtime_Text_nodeTab__A");
const __T1_xdc_runtime_Text_nodeTab xdc_runtime_Text_nodeTab__A[69] = {
    {
        (xdc_Bits16)0x0U,  /* left */
        (xdc_Bits16)0x0U,  /* right */
    },  /* [0] */
    {
        (xdc_Bits16)0x1ae6U,  /* left */
        (xdc_Bits16)0x1aebU,  /* right */
    },  /* [1] */
    {
        (xdc_Bits16)0x8001U,  /* left */
        (xdc_Bits16)0x1af4U,  /* right */
    },  /* [2] */
    {
        (xdc_Bits16)0x8001U,  /* left */
        (xdc_Bits16)0x1afbU,  /* right */
    },  /* [3] */
    {
        (xdc_Bits16)0x8001U,  /* left */
        (xdc_Bits16)0x1b00U,  /* right */
    },  /* [4] */
    {
        (xdc_Bits16)0x8001U,  /* left */
        (xdc_Bits16)0x1b09U,  /* right */
    },  /* [5] */
    {
        (xdc_Bits16)0x8001U,  /* left */
        (xdc_Bits16)0x1b0fU,  /* right */
    },  /* [6] */
    {
        (xdc_Bits16)0x8001U,  /* left */
        (xdc_Bits16)0x1b15U,  /* right */
    },  /* [7] */
    {
        (xdc_Bits16)0x8001U,  /* left */
        (xdc_Bits16)0x1b1aU,  /* right */
    },  /* [8] */
    {
        (xdc_Bits16)0x8001U,  /* left */
        (xdc_Bits16)0x1b1eU,  /* right */
    },  /* [9] */
    {
        (xdc_Bits16)0x8001U,  /* left */
        (xdc_Bits16)0x1b23U,  /* right */
    },  /* [10] */
    {
        (xdc_Bits16)0x8001U,  /* left */
        (xdc_Bits16)0x1b2aU,  /* right */
    },  /* [11] */
    {
        (xdc_Bits16)0x8001U,  /* left */
        (xdc_Bits16)0x1b33U,  /* right */
    },  /* [12] */
    {
        (xdc_Bits16)0x8001U,  /* left */
        (xdc_Bits16)0x1b3bU,  /* right */
    },  /* [13] */
    {
        (xdc_Bits16)0x8001U,  /* left */
        (xdc_Bits16)0x1b42U,  /* right */
    },  /* [14] */
    {
        (xdc_Bits16)0x8001U,  /* left */
        (xdc_Bits16)0x1b49U,  /* right */
    },  /* [15] */
    {
        (xdc_Bits16)0x8001U,  /* left */
        (xdc_Bits16)0x1b50U,  /* right */
    },  /* [16] */
    {
        (xdc_Bits16)0x8001U,  /* left */
        (xdc_Bits16)0x1b55U,  /* right */
    },  /* [17] */
    {
        (xdc_Bits16)0x8001U,  /* left */
        (xdc_Bits16)0x1b5fU,  /* right */
    },  /* [18] */
    {
        (xdc_Bits16)0x8001U,  /* left */
        (xdc_Bits16)0x1b6dU,  /* right */
    },  /* [19] */
    {
        (xdc_Bits16)0x1b73U,  /* left */
        (xdc_Bits16)0x1b77U,  /* right */
    },  /* [20] */
    {
        (xdc_Bits16)0x8014U,  /* left */
        (xdc_Bits16)0x1b80U,  /* right */
    },  /* [21] */
    {
        (xdc_Bits16)0x8015U,  /* left */
        (xdc_Bits16)0x1b88U,  /* right */
    },  /* [22] */
    {
        (xdc_Bits16)0x8016U,  /* left */
        (xdc_Bits16)0x1b8dU,  /* right */
    },  /* [23] */
    {
        (xdc_Bits16)0x8016U,  /* left */
        (xdc_Bits16)0x1b93U,  /* right */
    },  /* [24] */
    {
        (xdc_Bits16)0x8016U,  /* left */
        (xdc_Bits16)0x1b9dU,  /* right */
    },  /* [25] */
    {
        (xdc_Bits16)0x8016U,  /* left */
        (xdc_Bits16)0x1ba1U,  /* right */
    },  /* [26] */
    {
        (xdc_Bits16)0x8016U,  /* left */
        (xdc_Bits16)0x1bb3U,  /* right */
    },  /* [27] */
    {
        (xdc_Bits16)0x8016U,  /* left */
        (xdc_Bits16)0x1bb7U,  /* right */
    },  /* [28] */
    {
        (xdc_Bits16)0x8016U,  /* left */
        (xdc_Bits16)0x1bc3U,  /* right */
    },  /* [29] */
    {
        (xdc_Bits16)0x8014U,  /* left */
        (xdc_Bits16)0x1bd5U,  /* right */
    },  /* [30] */
    {
        (xdc_Bits16)0x8014U,  /* left */
        (xdc_Bits16)0x1bdaU,  /* right */
    },  /* [31] */
    {
        (xdc_Bits16)0x801fU,  /* left */
        (xdc_Bits16)0x1bdfU,  /* right */
    },  /* [32] */
    {
        (xdc_Bits16)0x801fU,  /* left */
        (xdc_Bits16)0x1be5U,  /* right */
    },  /* [33] */
    {
        (xdc_Bits16)0x801fU,  /* left */
        (xdc_Bits16)0x1beaU,  /* right */
    },  /* [34] */
    {
        (xdc_Bits16)0x801fU,  /* left */
        (xdc_Bits16)0x1bf5U,  /* right */
    },  /* [35] */
    {
        (xdc_Bits16)0x801fU,  /* left */
        (xdc_Bits16)0x1bfbU,  /* right */
    },  /* [36] */
    {
        (xdc_Bits16)0x801fU,  /* left */
        (xdc_Bits16)0x1c01U,  /* right */
    },  /* [37] */
    {
        (xdc_Bits16)0x801fU,  /* left */
        (xdc_Bits16)0x1c0bU,  /* right */
    },  /* [38] */
    {
        (xdc_Bits16)0x801fU,  /* left */
        (xdc_Bits16)0x1c0fU,  /* right */
    },  /* [39] */
    {
        (xdc_Bits16)0x8001U,  /* left */
        (xdc_Bits16)0x1bdaU,  /* right */
    },  /* [40] */
    {
        (xdc_Bits16)0x8028U,  /* left */
        (xdc_Bits16)0x1c14U,  /* right */
    },  /* [41] */
    {
        (xdc_Bits16)0x8028U,  /* left */
        (xdc_Bits16)0x1c1aU,  /* right */
    },  /* [42] */
    {
        (xdc_Bits16)0x8014U,  /* left */
        (xdc_Bits16)0x1c25U,  /* right */
    },  /* [43] */
    {
        (xdc_Bits16)0x802bU,  /* left */
        (xdc_Bits16)0x1b8dU,  /* right */
    },  /* [44] */
    {
        (xdc_Bits16)0x802bU,  /* left */
        (xdc_Bits16)0x1afbU,  /* right */
    },  /* [45] */
    {
        (xdc_Bits16)0x802bU,  /* left */
        (xdc_Bits16)0x1c2aU,  /* right */
    },  /* [46] */
    {
        (xdc_Bits16)0x802bU,  /* left */
        (xdc_Bits16)0x1b9dU,  /* right */
    },  /* [47] */
    {
        (xdc_Bits16)0x8014U,  /* left */
        (xdc_Bits16)0x1c33U,  /* right */
    },  /* [48] */
    {
        (xdc_Bits16)0x8030U,  /* left */
        (xdc_Bits16)0x1c3aU,  /* right */
    },  /* [49] */
    {
        (xdc_Bits16)0x8030U,  /* left */
        (xdc_Bits16)0x1c42U,  /* right */
    },  /* [50] */
    {
        (xdc_Bits16)0x8014U,  /* left */
        (xdc_Bits16)0x1c4aU,  /* right */
    },  /* [51] */
    {
        (xdc_Bits16)0x8033U,  /* left */
        (xdc_Bits16)0x1c51U,  /* right */
    },  /* [52] */
    {
        (xdc_Bits16)0x8014U,  /* left */
        (xdc_Bits16)0x1c59U,  /* right */
    },  /* [53] */
    {
        (xdc_Bits16)0x8035U,  /* left */
        (xdc_Bits16)0x1c60U,  /* right */
    },  /* [54] */
    {
        (xdc_Bits16)0x8035U,  /* left */
        (xdc_Bits16)0x1c68U,  /* right */
    },  /* [55] */
    {
        (xdc_Bits16)0x8035U,  /* left */
        (xdc_Bits16)0x1c70U,  /* right */
    },  /* [56] */
    {
        (xdc_Bits16)0x8035U,  /* left */
        (xdc_Bits16)0x1c79U,  /* right */
    },  /* [57] */
    {
        (xdc_Bits16)0x8035U,  /* left */
        (xdc_Bits16)0x1c86U,  /* right */
    },  /* [58] */
    {
        (xdc_Bits16)0x8014U,  /* left */
        (xdc_Bits16)0x1c90U,  /* right */
    },  /* [59] */
    {
        (xdc_Bits16)0x803bU,  /* left */
        (xdc_Bits16)0x1c97U,  /* right */
    },  /* [60] */
    {
        (xdc_Bits16)0x8014U,  /* left */
        (xdc_Bits16)0x1c9cU,  /* right */
    },  /* [61] */
    {
        (xdc_Bits16)0x803dU,  /* left */
        (xdc_Bits16)0x1ca4U,  /* right */
    },  /* [62] */
    {
        (xdc_Bits16)0x803eU,  /* left */
        (xdc_Bits16)0x1cadU,  /* right */
    },  /* [63] */
    {
        (xdc_Bits16)0x8015U,  /* left */
        (xdc_Bits16)0x1cb3U,  /* right */
    },  /* [64] */
    {
        (xdc_Bits16)0x8040U,  /* left */
        (xdc_Bits16)0x1cb9U,  /* right */
    },  /* [65] */
    {
        (xdc_Bits16)0x8041U,  /* left */
        (xdc_Bits16)0x1cc2U,  /* right */
    },  /* [66] */
    {
        (xdc_Bits16)0x8014U,  /* left */
        (xdc_Bits16)0x1ccfU,  /* right */
    },  /* [67] */
    {
        (xdc_Bits16)0x8043U,  /* left */
        (xdc_Bits16)0x1cdbU,  /* right */
    },  /* [68] */
};

/* Module__diagsEnabled__C */
#pragma DATA_SECTION(xdc_runtime_Text_Module__diagsEnabled__C, ".const:xdc_runtime_Text_Module__diagsEnabled__C");
__FAR__ const CT__xdc_runtime_Text_Module__diagsEnabled xdc_runtime_Text_Module__diagsEnabled__C = (xdc_Bits32)0x10U;

/* Module__diagsIncluded__C */
#pragma DATA_SECTION(xdc_runtime_Text_Module__diagsIncluded__C, ".const:xdc_runtime_Text_Module__diagsIncluded__C");
__FAR__ const CT__xdc_runtime_Text_Module__diagsIncluded xdc_runtime_Text_Module__diagsIncluded__C = (xdc_Bits32)0x10U;

/* Module__diagsMask__C */
#pragma DATA_SECTION(xdc_runtime_Text_Module__diagsMask__C, ".const:xdc_runtime_Text_Module__diagsMask__C");
__FAR__ const CT__xdc_runtime_Text_Module__diagsMask xdc_runtime_Text_Module__diagsMask__C = ((const CT__xdc_runtime_Text_Module__diagsMask)NULL);

/* Module__gateObj__C */
#pragma DATA_SECTION(xdc_runtime_Text_Module__gateObj__C, ".const:xdc_runtime_Text_Module__gateObj__C");
__FAR__ const CT__xdc_runtime_Text_Module__gateObj xdc_runtime_Text_Module__gateObj__C = ((const CT__xdc_runtime_Text_Module__gateObj)NULL);

/* Module__gatePrms__C */
#pragma DATA_SECTION(xdc_runtime_Text_Module__gatePrms__C, ".const:xdc_runtime_Text_Module__gatePrms__C");
__FAR__ const CT__xdc_runtime_Text_Module__gatePrms xdc_runtime_Text_Module__gatePrms__C = ((const CT__xdc_runtime_Text_Module__gatePrms)NULL);

/* Module__id__C */
#pragma DATA_SECTION(xdc_runtime_Text_Module__id__C, ".const:xdc_runtime_Text_Module__id__C");
__FAR__ const CT__xdc_runtime_Text_Module__id xdc_runtime_Text_Module__id__C = (xdc_Bits16)0x8010U;

/* Module__loggerDefined__C */
#pragma DATA_SECTION(xdc_runtime_Text_Module__loggerDefined__C, ".const:xdc_runtime_Text_Module__loggerDefined__C");
__FAR__ const CT__xdc_runtime_Text_Module__loggerDefined xdc_runtime_Text_Module__loggerDefined__C = 0;

/* Module__loggerObj__C */
#pragma DATA_SECTION(xdc_runtime_Text_Module__loggerObj__C, ".const:xdc_runtime_Text_Module__loggerObj__C");
__FAR__ const CT__xdc_runtime_Text_Module__loggerObj xdc_runtime_Text_Module__loggerObj__C = ((const CT__xdc_runtime_Text_Module__loggerObj)NULL);

/* Module__loggerFxn0__C */
#pragma DATA_SECTION(xdc_runtime_Text_Module__loggerFxn0__C, ".const:xdc_runtime_Text_Module__loggerFxn0__C");
__FAR__ const CT__xdc_runtime_Text_Module__loggerFxn0 xdc_runtime_Text_Module__loggerFxn0__C = ((const CT__xdc_runtime_Text_Module__loggerFxn0)NULL);

/* Module__loggerFxn1__C */
#pragma DATA_SECTION(xdc_runtime_Text_Module__loggerFxn1__C, ".const:xdc_runtime_Text_Module__loggerFxn1__C");
__FAR__ const CT__xdc_runtime_Text_Module__loggerFxn1 xdc_runtime_Text_Module__loggerFxn1__C = ((const CT__xdc_runtime_Text_Module__loggerFxn1)NULL);

/* Module__loggerFxn2__C */
#pragma DATA_SECTION(xdc_runtime_Text_Module__loggerFxn2__C, ".const:xdc_runtime_Text_Module__loggerFxn2__C");
__FAR__ const CT__xdc_runtime_Text_Module__loggerFxn2 xdc_runtime_Text_Module__loggerFxn2__C = ((const CT__xdc_runtime_Text_Module__loggerFxn2)NULL);

/* Module__loggerFxn4__C */
#pragma DATA_SECTION(xdc_runtime_Text_Module__loggerFxn4__C, ".const:xdc_runtime_Text_Module__loggerFxn4__C");
__FAR__ const CT__xdc_runtime_Text_Module__loggerFxn4 xdc_runtime_Text_Module__loggerFxn4__C = ((const CT__xdc_runtime_Text_Module__loggerFxn4)NULL);

/* Module__loggerFxn8__C */
#pragma DATA_SECTION(xdc_runtime_Text_Module__loggerFxn8__C, ".const:xdc_runtime_Text_Module__loggerFxn8__C");
__FAR__ const CT__xdc_runtime_Text_Module__loggerFxn8 xdc_runtime_Text_Module__loggerFxn8__C = ((const CT__xdc_runtime_Text_Module__loggerFxn8)NULL);

/* Object__count__C */
#pragma DATA_SECTION(xdc_runtime_Text_Object__count__C, ".const:xdc_runtime_Text_Object__count__C");
__FAR__ const CT__xdc_runtime_Text_Object__count xdc_runtime_Text_Object__count__C = 0;

/* Object__heap__C */
#pragma DATA_SECTION(xdc_runtime_Text_Object__heap__C, ".const:xdc_runtime_Text_Object__heap__C");
__FAR__ const CT__xdc_runtime_Text_Object__heap xdc_runtime_Text_Object__heap__C = NULL;

/* Object__sizeof__C */
#pragma DATA_SECTION(xdc_runtime_Text_Object__sizeof__C, ".const:xdc_runtime_Text_Object__sizeof__C");
__FAR__ const CT__xdc_runtime_Text_Object__sizeof xdc_runtime_Text_Object__sizeof__C = 0;

/* Object__table__C */
#pragma DATA_SECTION(xdc_runtime_Text_Object__table__C, ".const:xdc_runtime_Text_Object__table__C");
__FAR__ const CT__xdc_runtime_Text_Object__table xdc_runtime_Text_Object__table__C = NULL;

/* nameUnknown__C */
#pragma DATA_SECTION(xdc_runtime_Text_nameUnknown__C, ".const:xdc_runtime_Text_nameUnknown__C");
__FAR__ const CT__xdc_runtime_Text_nameUnknown xdc_runtime_Text_nameUnknown__C = "{unknown-instance-name}";

/* nameEmpty__C */
#pragma DATA_SECTION(xdc_runtime_Text_nameEmpty__C, ".const:xdc_runtime_Text_nameEmpty__C");
__FAR__ const CT__xdc_runtime_Text_nameEmpty xdc_runtime_Text_nameEmpty__C = "{empty-instance-name}";

/* nameStatic__C */
#pragma DATA_SECTION(xdc_runtime_Text_nameStatic__C, ".const:xdc_runtime_Text_nameStatic__C");
__FAR__ const CT__xdc_runtime_Text_nameStatic xdc_runtime_Text_nameStatic__C = "{static-instance-name}";

/* isLoaded__C */
#pragma DATA_SECTION(xdc_runtime_Text_isLoaded__C, ".const:xdc_runtime_Text_isLoaded__C");
__FAR__ const CT__xdc_runtime_Text_isLoaded xdc_runtime_Text_isLoaded__C = 1;

/* charTab__C */
#pragma DATA_SECTION(xdc_runtime_Text_charTab__C, ".const:xdc_runtime_Text_charTab__C");
__FAR__ const CT__xdc_runtime_Text_charTab xdc_runtime_Text_charTab__C = ((const CT__xdc_runtime_Text_charTab)xdc_runtime_Text_charTab__A);

/* nodeTab__C */
#pragma DATA_SECTION(xdc_runtime_Text_nodeTab__C, ".const:xdc_runtime_Text_nodeTab__C");
__FAR__ const CT__xdc_runtime_Text_nodeTab xdc_runtime_Text_nodeTab__C = ((const CT__xdc_runtime_Text_nodeTab)xdc_runtime_Text_nodeTab__A);

/* charCnt__C */
#pragma DATA_SECTION(xdc_runtime_Text_charCnt__C, ".const:xdc_runtime_Text_charCnt__C");
__FAR__ const CT__xdc_runtime_Text_charCnt xdc_runtime_Text_charCnt__C = (xdc_Int16)0x1d0a;

/* nodeCnt__C */
#pragma DATA_SECTION(xdc_runtime_Text_nodeCnt__C, ".const:xdc_runtime_Text_nodeCnt__C");
__FAR__ const CT__xdc_runtime_Text_nodeCnt xdc_runtime_Text_nodeCnt__C = (xdc_Int16)0x45;

/* unnamedModsLastId__C */
#pragma DATA_SECTION(xdc_runtime_Text_unnamedModsLastId__C, ".const:xdc_runtime_Text_unnamedModsLastId__C");
__FAR__ const CT__xdc_runtime_Text_unnamedModsLastId xdc_runtime_Text_unnamedModsLastId__C = (xdc_UInt16)0x4000U;

/* registryModsLastId__C */
#pragma DATA_SECTION(xdc_runtime_Text_registryModsLastId__C, ".const:xdc_runtime_Text_registryModsLastId__C");
__FAR__ const CT__xdc_runtime_Text_registryModsLastId xdc_runtime_Text_registryModsLastId__C = (xdc_UInt16)0x7fffU;

/* visitRopeFxn__C */
#pragma DATA_SECTION(xdc_runtime_Text_visitRopeFxn__C, ".const:xdc_runtime_Text_visitRopeFxn__C");
__FAR__ const CT__xdc_runtime_Text_visitRopeFxn xdc_runtime_Text_visitRopeFxn__C = ((const CT__xdc_runtime_Text_visitRopeFxn)(xdc_runtime_Text_visitRope__I));

/* visitRopeFxn2__C */
#pragma DATA_SECTION(xdc_runtime_Text_visitRopeFxn2__C, ".const:xdc_runtime_Text_visitRopeFxn2__C");
__FAR__ const CT__xdc_runtime_Text_visitRopeFxn2 xdc_runtime_Text_visitRopeFxn2__C = ((const CT__xdc_runtime_Text_visitRopeFxn2)(xdc_runtime_Text_visitRope2__I));


/*
 * ======== xdc.runtime.Timestamp INITIALIZERS ========
 */

/* Module__diagsEnabled__C */
#pragma DATA_SECTION(xdc_runtime_Timestamp_Module__diagsEnabled__C, ".const:xdc_runtime_Timestamp_Module__diagsEnabled__C");
__FAR__ const CT__xdc_runtime_Timestamp_Module__diagsEnabled xdc_runtime_Timestamp_Module__diagsEnabled__C = (xdc_Bits32)0x10U;

/* Module__diagsIncluded__C */
#pragma DATA_SECTION(xdc_runtime_Timestamp_Module__diagsIncluded__C, ".const:xdc_runtime_Timestamp_Module__diagsIncluded__C");
__FAR__ const CT__xdc_runtime_Timestamp_Module__diagsIncluded xdc_runtime_Timestamp_Module__diagsIncluded__C = (xdc_Bits32)0x10U;

/* Module__diagsMask__C */
#pragma DATA_SECTION(xdc_runtime_Timestamp_Module__diagsMask__C, ".const:xdc_runtime_Timestamp_Module__diagsMask__C");
__FAR__ const CT__xdc_runtime_Timestamp_Module__diagsMask xdc_runtime_Timestamp_Module__diagsMask__C = ((const CT__xdc_runtime_Timestamp_Module__diagsMask)NULL);

/* Module__gateObj__C */
#pragma DATA_SECTION(xdc_runtime_Timestamp_Module__gateObj__C, ".const:xdc_runtime_Timestamp_Module__gateObj__C");
__FAR__ const CT__xdc_runtime_Timestamp_Module__gateObj xdc_runtime_Timestamp_Module__gateObj__C = ((const CT__xdc_runtime_Timestamp_Module__gateObj)NULL);

/* Module__gatePrms__C */
#pragma DATA_SECTION(xdc_runtime_Timestamp_Module__gatePrms__C, ".const:xdc_runtime_Timestamp_Module__gatePrms__C");
__FAR__ const CT__xdc_runtime_Timestamp_Module__gatePrms xdc_runtime_Timestamp_Module__gatePrms__C = ((const CT__xdc_runtime_Timestamp_Module__gatePrms)NULL);

/* Module__id__C */
#pragma DATA_SECTION(xdc_runtime_Timestamp_Module__id__C, ".const:xdc_runtime_Timestamp_Module__id__C");
__FAR__ const CT__xdc_runtime_Timestamp_Module__id xdc_runtime_Timestamp_Module__id__C = (xdc_Bits16)0x8011U;

/* Module__loggerDefined__C */
#pragma DATA_SECTION(xdc_runtime_Timestamp_Module__loggerDefined__C, ".const:xdc_runtime_Timestamp_Module__loggerDefined__C");
__FAR__ const CT__xdc_runtime_Timestamp_Module__loggerDefined xdc_runtime_Timestamp_Module__loggerDefined__C = 0;

/* Module__loggerObj__C */
#pragma DATA_SECTION(xdc_runtime_Timestamp_Module__loggerObj__C, ".const:xdc_runtime_Timestamp_Module__loggerObj__C");
__FAR__ const CT__xdc_runtime_Timestamp_Module__loggerObj xdc_runtime_Timestamp_Module__loggerObj__C = ((const CT__xdc_runtime_Timestamp_Module__loggerObj)NULL);

/* Module__loggerFxn0__C */
#pragma DATA_SECTION(xdc_runtime_Timestamp_Module__loggerFxn0__C, ".const:xdc_runtime_Timestamp_Module__loggerFxn0__C");
__FAR__ const CT__xdc_runtime_Timestamp_Module__loggerFxn0 xdc_runtime_Timestamp_Module__loggerFxn0__C = ((const CT__xdc_runtime_Timestamp_Module__loggerFxn0)NULL);

/* Module__loggerFxn1__C */
#pragma DATA_SECTION(xdc_runtime_Timestamp_Module__loggerFxn1__C, ".const:xdc_runtime_Timestamp_Module__loggerFxn1__C");
__FAR__ const CT__xdc_runtime_Timestamp_Module__loggerFxn1 xdc_runtime_Timestamp_Module__loggerFxn1__C = ((const CT__xdc_runtime_Timestamp_Module__loggerFxn1)NULL);

/* Module__loggerFxn2__C */
#pragma DATA_SECTION(xdc_runtime_Timestamp_Module__loggerFxn2__C, ".const:xdc_runtime_Timestamp_Module__loggerFxn2__C");
__FAR__ const CT__xdc_runtime_Timestamp_Module__loggerFxn2 xdc_runtime_Timestamp_Module__loggerFxn2__C = ((const CT__xdc_runtime_Timestamp_Module__loggerFxn2)NULL);

/* Module__loggerFxn4__C */
#pragma DATA_SECTION(xdc_runtime_Timestamp_Module__loggerFxn4__C, ".const:xdc_runtime_Timestamp_Module__loggerFxn4__C");
__FAR__ const CT__xdc_runtime_Timestamp_Module__loggerFxn4 xdc_runtime_Timestamp_Module__loggerFxn4__C = ((const CT__xdc_runtime_Timestamp_Module__loggerFxn4)NULL);

/* Module__loggerFxn8__C */
#pragma DATA_SECTION(xdc_runtime_Timestamp_Module__loggerFxn8__C, ".const:xdc_runtime_Timestamp_Module__loggerFxn8__C");
__FAR__ const CT__xdc_runtime_Timestamp_Module__loggerFxn8 xdc_runtime_Timestamp_Module__loggerFxn8__C = ((const CT__xdc_runtime_Timestamp_Module__loggerFxn8)NULL);

/* Object__count__C */
#pragma DATA_SECTION(xdc_runtime_Timestamp_Object__count__C, ".const:xdc_runtime_Timestamp_Object__count__C");
__FAR__ const CT__xdc_runtime_Timestamp_Object__count xdc_runtime_Timestamp_Object__count__C = 0;

/* Object__heap__C */
#pragma DATA_SECTION(xdc_runtime_Timestamp_Object__heap__C, ".const:xdc_runtime_Timestamp_Object__heap__C");
__FAR__ const CT__xdc_runtime_Timestamp_Object__heap xdc_runtime_Timestamp_Object__heap__C = NULL;

/* Object__sizeof__C */
#pragma DATA_SECTION(xdc_runtime_Timestamp_Object__sizeof__C, ".const:xdc_runtime_Timestamp_Object__sizeof__C");
__FAR__ const CT__xdc_runtime_Timestamp_Object__sizeof xdc_runtime_Timestamp_Object__sizeof__C = 0;

/* Object__table__C */
#pragma DATA_SECTION(xdc_runtime_Timestamp_Object__table__C, ".const:xdc_runtime_Timestamp_Object__table__C");
__FAR__ const CT__xdc_runtime_Timestamp_Object__table xdc_runtime_Timestamp_Object__table__C = NULL;


/*
 * ======== xdc.runtime.TimestampNull INITIALIZERS ========
 */

/* Module__diagsEnabled__C */
#pragma DATA_SECTION(xdc_runtime_TimestampNull_Module__diagsEnabled__C, ".const:xdc_runtime_TimestampNull_Module__diagsEnabled__C");
__FAR__ const CT__xdc_runtime_TimestampNull_Module__diagsEnabled xdc_runtime_TimestampNull_Module__diagsEnabled__C = (xdc_Bits32)0x10U;

/* Module__diagsIncluded__C */
#pragma DATA_SECTION(xdc_runtime_TimestampNull_Module__diagsIncluded__C, ".const:xdc_runtime_TimestampNull_Module__diagsIncluded__C");
__FAR__ const CT__xdc_runtime_TimestampNull_Module__diagsIncluded xdc_runtime_TimestampNull_Module__diagsIncluded__C = (xdc_Bits32)0x10U;

/* Module__diagsMask__C */
#pragma DATA_SECTION(xdc_runtime_TimestampNull_Module__diagsMask__C, ".const:xdc_runtime_TimestampNull_Module__diagsMask__C");
__FAR__ const CT__xdc_runtime_TimestampNull_Module__diagsMask xdc_runtime_TimestampNull_Module__diagsMask__C = ((const CT__xdc_runtime_TimestampNull_Module__diagsMask)NULL);

/* Module__gateObj__C */
#pragma DATA_SECTION(xdc_runtime_TimestampNull_Module__gateObj__C, ".const:xdc_runtime_TimestampNull_Module__gateObj__C");
__FAR__ const CT__xdc_runtime_TimestampNull_Module__gateObj xdc_runtime_TimestampNull_Module__gateObj__C = ((const CT__xdc_runtime_TimestampNull_Module__gateObj)NULL);

/* Module__gatePrms__C */
#pragma DATA_SECTION(xdc_runtime_TimestampNull_Module__gatePrms__C, ".const:xdc_runtime_TimestampNull_Module__gatePrms__C");
__FAR__ const CT__xdc_runtime_TimestampNull_Module__gatePrms xdc_runtime_TimestampNull_Module__gatePrms__C = ((const CT__xdc_runtime_TimestampNull_Module__gatePrms)NULL);

/* Module__id__C */
#pragma DATA_SECTION(xdc_runtime_TimestampNull_Module__id__C, ".const:xdc_runtime_TimestampNull_Module__id__C");
__FAR__ const CT__xdc_runtime_TimestampNull_Module__id xdc_runtime_TimestampNull_Module__id__C = (xdc_Bits16)0x8012U;

/* Module__loggerDefined__C */
#pragma DATA_SECTION(xdc_runtime_TimestampNull_Module__loggerDefined__C, ".const:xdc_runtime_TimestampNull_Module__loggerDefined__C");
__FAR__ const CT__xdc_runtime_TimestampNull_Module__loggerDefined xdc_runtime_TimestampNull_Module__loggerDefined__C = 0;

/* Module__loggerObj__C */
#pragma DATA_SECTION(xdc_runtime_TimestampNull_Module__loggerObj__C, ".const:xdc_runtime_TimestampNull_Module__loggerObj__C");
__FAR__ const CT__xdc_runtime_TimestampNull_Module__loggerObj xdc_runtime_TimestampNull_Module__loggerObj__C = ((const CT__xdc_runtime_TimestampNull_Module__loggerObj)NULL);

/* Module__loggerFxn0__C */
#pragma DATA_SECTION(xdc_runtime_TimestampNull_Module__loggerFxn0__C, ".const:xdc_runtime_TimestampNull_Module__loggerFxn0__C");
__FAR__ const CT__xdc_runtime_TimestampNull_Module__loggerFxn0 xdc_runtime_TimestampNull_Module__loggerFxn0__C = ((const CT__xdc_runtime_TimestampNull_Module__loggerFxn0)NULL);

/* Module__loggerFxn1__C */
#pragma DATA_SECTION(xdc_runtime_TimestampNull_Module__loggerFxn1__C, ".const:xdc_runtime_TimestampNull_Module__loggerFxn1__C");
__FAR__ const CT__xdc_runtime_TimestampNull_Module__loggerFxn1 xdc_runtime_TimestampNull_Module__loggerFxn1__C = ((const CT__xdc_runtime_TimestampNull_Module__loggerFxn1)NULL);

/* Module__loggerFxn2__C */
#pragma DATA_SECTION(xdc_runtime_TimestampNull_Module__loggerFxn2__C, ".const:xdc_runtime_TimestampNull_Module__loggerFxn2__C");
__FAR__ const CT__xdc_runtime_TimestampNull_Module__loggerFxn2 xdc_runtime_TimestampNull_Module__loggerFxn2__C = ((const CT__xdc_runtime_TimestampNull_Module__loggerFxn2)NULL);

/* Module__loggerFxn4__C */
#pragma DATA_SECTION(xdc_runtime_TimestampNull_Module__loggerFxn4__C, ".const:xdc_runtime_TimestampNull_Module__loggerFxn4__C");
__FAR__ const CT__xdc_runtime_TimestampNull_Module__loggerFxn4 xdc_runtime_TimestampNull_Module__loggerFxn4__C = ((const CT__xdc_runtime_TimestampNull_Module__loggerFxn4)NULL);

/* Module__loggerFxn8__C */
#pragma DATA_SECTION(xdc_runtime_TimestampNull_Module__loggerFxn8__C, ".const:xdc_runtime_TimestampNull_Module__loggerFxn8__C");
__FAR__ const CT__xdc_runtime_TimestampNull_Module__loggerFxn8 xdc_runtime_TimestampNull_Module__loggerFxn8__C = ((const CT__xdc_runtime_TimestampNull_Module__loggerFxn8)NULL);

/* Object__count__C */
#pragma DATA_SECTION(xdc_runtime_TimestampNull_Object__count__C, ".const:xdc_runtime_TimestampNull_Object__count__C");
__FAR__ const CT__xdc_runtime_TimestampNull_Object__count xdc_runtime_TimestampNull_Object__count__C = 0;

/* Object__heap__C */
#pragma DATA_SECTION(xdc_runtime_TimestampNull_Object__heap__C, ".const:xdc_runtime_TimestampNull_Object__heap__C");
__FAR__ const CT__xdc_runtime_TimestampNull_Object__heap xdc_runtime_TimestampNull_Object__heap__C = NULL;

/* Object__sizeof__C */
#pragma DATA_SECTION(xdc_runtime_TimestampNull_Object__sizeof__C, ".const:xdc_runtime_TimestampNull_Object__sizeof__C");
__FAR__ const CT__xdc_runtime_TimestampNull_Object__sizeof xdc_runtime_TimestampNull_Object__sizeof__C = 0;

/* Object__table__C */
#pragma DATA_SECTION(xdc_runtime_TimestampNull_Object__table__C, ".const:xdc_runtime_TimestampNull_Object__table__C");
__FAR__ const CT__xdc_runtime_TimestampNull_Object__table xdc_runtime_TimestampNull_Object__table__C = NULL;


/*
 * ======== xdc.runtime.Timestamp_SupportProxy INITIALIZERS ========
 */


/*
 * ======== xdc.runtime.Types INITIALIZERS ========
 */

/* Module__diagsEnabled__C */
#pragma DATA_SECTION(xdc_runtime_Types_Module__diagsEnabled__C, ".const:xdc_runtime_Types_Module__diagsEnabled__C");
__FAR__ const CT__xdc_runtime_Types_Module__diagsEnabled xdc_runtime_Types_Module__diagsEnabled__C = (xdc_Bits32)0x10U;

/* Module__diagsIncluded__C */
#pragma DATA_SECTION(xdc_runtime_Types_Module__diagsIncluded__C, ".const:xdc_runtime_Types_Module__diagsIncluded__C");
__FAR__ const CT__xdc_runtime_Types_Module__diagsIncluded xdc_runtime_Types_Module__diagsIncluded__C = (xdc_Bits32)0x10U;

/* Module__diagsMask__C */
#pragma DATA_SECTION(xdc_runtime_Types_Module__diagsMask__C, ".const:xdc_runtime_Types_Module__diagsMask__C");
__FAR__ const CT__xdc_runtime_Types_Module__diagsMask xdc_runtime_Types_Module__diagsMask__C = ((const CT__xdc_runtime_Types_Module__diagsMask)NULL);

/* Module__gateObj__C */
#pragma DATA_SECTION(xdc_runtime_Types_Module__gateObj__C, ".const:xdc_runtime_Types_Module__gateObj__C");
__FAR__ const CT__xdc_runtime_Types_Module__gateObj xdc_runtime_Types_Module__gateObj__C = ((const CT__xdc_runtime_Types_Module__gateObj)NULL);

/* Module__gatePrms__C */
#pragma DATA_SECTION(xdc_runtime_Types_Module__gatePrms__C, ".const:xdc_runtime_Types_Module__gatePrms__C");
__FAR__ const CT__xdc_runtime_Types_Module__gatePrms xdc_runtime_Types_Module__gatePrms__C = ((const CT__xdc_runtime_Types_Module__gatePrms)NULL);

/* Module__id__C */
#pragma DATA_SECTION(xdc_runtime_Types_Module__id__C, ".const:xdc_runtime_Types_Module__id__C");
__FAR__ const CT__xdc_runtime_Types_Module__id xdc_runtime_Types_Module__id__C = (xdc_Bits16)0x8013U;

/* Module__loggerDefined__C */
#pragma DATA_SECTION(xdc_runtime_Types_Module__loggerDefined__C, ".const:xdc_runtime_Types_Module__loggerDefined__C");
__FAR__ const CT__xdc_runtime_Types_Module__loggerDefined xdc_runtime_Types_Module__loggerDefined__C = 0;

/* Module__loggerObj__C */
#pragma DATA_SECTION(xdc_runtime_Types_Module__loggerObj__C, ".const:xdc_runtime_Types_Module__loggerObj__C");
__FAR__ const CT__xdc_runtime_Types_Module__loggerObj xdc_runtime_Types_Module__loggerObj__C = ((const CT__xdc_runtime_Types_Module__loggerObj)NULL);

/* Module__loggerFxn0__C */
#pragma DATA_SECTION(xdc_runtime_Types_Module__loggerFxn0__C, ".const:xdc_runtime_Types_Module__loggerFxn0__C");
__FAR__ const CT__xdc_runtime_Types_Module__loggerFxn0 xdc_runtime_Types_Module__loggerFxn0__C = ((const CT__xdc_runtime_Types_Module__loggerFxn0)NULL);

/* Module__loggerFxn1__C */
#pragma DATA_SECTION(xdc_runtime_Types_Module__loggerFxn1__C, ".const:xdc_runtime_Types_Module__loggerFxn1__C");
__FAR__ const CT__xdc_runtime_Types_Module__loggerFxn1 xdc_runtime_Types_Module__loggerFxn1__C = ((const CT__xdc_runtime_Types_Module__loggerFxn1)NULL);

/* Module__loggerFxn2__C */
#pragma DATA_SECTION(xdc_runtime_Types_Module__loggerFxn2__C, ".const:xdc_runtime_Types_Module__loggerFxn2__C");
__FAR__ const CT__xdc_runtime_Types_Module__loggerFxn2 xdc_runtime_Types_Module__loggerFxn2__C = ((const CT__xdc_runtime_Types_Module__loggerFxn2)NULL);

/* Module__loggerFxn4__C */
#pragma DATA_SECTION(xdc_runtime_Types_Module__loggerFxn4__C, ".const:xdc_runtime_Types_Module__loggerFxn4__C");
__FAR__ const CT__xdc_runtime_Types_Module__loggerFxn4 xdc_runtime_Types_Module__loggerFxn4__C = ((const CT__xdc_runtime_Types_Module__loggerFxn4)NULL);

/* Module__loggerFxn8__C */
#pragma DATA_SECTION(xdc_runtime_Types_Module__loggerFxn8__C, ".const:xdc_runtime_Types_Module__loggerFxn8__C");
__FAR__ const CT__xdc_runtime_Types_Module__loggerFxn8 xdc_runtime_Types_Module__loggerFxn8__C = ((const CT__xdc_runtime_Types_Module__loggerFxn8)NULL);

/* Object__count__C */
#pragma DATA_SECTION(xdc_runtime_Types_Object__count__C, ".const:xdc_runtime_Types_Object__count__C");
__FAR__ const CT__xdc_runtime_Types_Object__count xdc_runtime_Types_Object__count__C = 0;

/* Object__heap__C */
#pragma DATA_SECTION(xdc_runtime_Types_Object__heap__C, ".const:xdc_runtime_Types_Object__heap__C");
__FAR__ const CT__xdc_runtime_Types_Object__heap xdc_runtime_Types_Object__heap__C = NULL;

/* Object__sizeof__C */
#pragma DATA_SECTION(xdc_runtime_Types_Object__sizeof__C, ".const:xdc_runtime_Types_Object__sizeof__C");
__FAR__ const CT__xdc_runtime_Types_Object__sizeof xdc_runtime_Types_Object__sizeof__C = 0;

/* Object__table__C */
#pragma DATA_SECTION(xdc_runtime_Types_Object__table__C, ".const:xdc_runtime_Types_Object__table__C");
__FAR__ const CT__xdc_runtime_Types_Object__table xdc_runtime_Types_Object__table__C = NULL;


/*
 * ======== xdc.runtime.knl.GateH INITIALIZERS ========
 */

/* Module__diagsEnabled__C */
#pragma DATA_SECTION(xdc_runtime_knl_GateH_Module__diagsEnabled__C, ".const:xdc_runtime_knl_GateH_Module__diagsEnabled__C");
__FAR__ const CT__xdc_runtime_knl_GateH_Module__diagsEnabled xdc_runtime_knl_GateH_Module__diagsEnabled__C = (xdc_Bits32)0x90U;

/* Module__diagsIncluded__C */
#pragma DATA_SECTION(xdc_runtime_knl_GateH_Module__diagsIncluded__C, ".const:xdc_runtime_knl_GateH_Module__diagsIncluded__C");
__FAR__ const CT__xdc_runtime_knl_GateH_Module__diagsIncluded xdc_runtime_knl_GateH_Module__diagsIncluded__C = (xdc_Bits32)0x90U;

/* Module__diagsMask__C */
#pragma DATA_SECTION(xdc_runtime_knl_GateH_Module__diagsMask__C, ".const:xdc_runtime_knl_GateH_Module__diagsMask__C");
__FAR__ const CT__xdc_runtime_knl_GateH_Module__diagsMask xdc_runtime_knl_GateH_Module__diagsMask__C = ((const CT__xdc_runtime_knl_GateH_Module__diagsMask)NULL);

/* Module__gateObj__C */
#pragma DATA_SECTION(xdc_runtime_knl_GateH_Module__gateObj__C, ".const:xdc_runtime_knl_GateH_Module__gateObj__C");
__FAR__ const CT__xdc_runtime_knl_GateH_Module__gateObj xdc_runtime_knl_GateH_Module__gateObj__C = ((const CT__xdc_runtime_knl_GateH_Module__gateObj)NULL);

/* Module__gatePrms__C */
#pragma DATA_SECTION(xdc_runtime_knl_GateH_Module__gatePrms__C, ".const:xdc_runtime_knl_GateH_Module__gatePrms__C");
__FAR__ const CT__xdc_runtime_knl_GateH_Module__gatePrms xdc_runtime_knl_GateH_Module__gatePrms__C = ((const CT__xdc_runtime_knl_GateH_Module__gatePrms)NULL);

/* Module__id__C */
#pragma DATA_SECTION(xdc_runtime_knl_GateH_Module__id__C, ".const:xdc_runtime_knl_GateH_Module__id__C");
__FAR__ const CT__xdc_runtime_knl_GateH_Module__id xdc_runtime_knl_GateH_Module__id__C = (xdc_Bits16)0x8029U;

/* Module__loggerDefined__C */
#pragma DATA_SECTION(xdc_runtime_knl_GateH_Module__loggerDefined__C, ".const:xdc_runtime_knl_GateH_Module__loggerDefined__C");
__FAR__ const CT__xdc_runtime_knl_GateH_Module__loggerDefined xdc_runtime_knl_GateH_Module__loggerDefined__C = 0;

/* Module__loggerObj__C */
#pragma DATA_SECTION(xdc_runtime_knl_GateH_Module__loggerObj__C, ".const:xdc_runtime_knl_GateH_Module__loggerObj__C");
__FAR__ const CT__xdc_runtime_knl_GateH_Module__loggerObj xdc_runtime_knl_GateH_Module__loggerObj__C = ((const CT__xdc_runtime_knl_GateH_Module__loggerObj)NULL);

/* Module__loggerFxn0__C */
#pragma DATA_SECTION(xdc_runtime_knl_GateH_Module__loggerFxn0__C, ".const:xdc_runtime_knl_GateH_Module__loggerFxn0__C");
__FAR__ const CT__xdc_runtime_knl_GateH_Module__loggerFxn0 xdc_runtime_knl_GateH_Module__loggerFxn0__C = ((const CT__xdc_runtime_knl_GateH_Module__loggerFxn0)NULL);

/* Module__loggerFxn1__C */
#pragma DATA_SECTION(xdc_runtime_knl_GateH_Module__loggerFxn1__C, ".const:xdc_runtime_knl_GateH_Module__loggerFxn1__C");
__FAR__ const CT__xdc_runtime_knl_GateH_Module__loggerFxn1 xdc_runtime_knl_GateH_Module__loggerFxn1__C = ((const CT__xdc_runtime_knl_GateH_Module__loggerFxn1)NULL);

/* Module__loggerFxn2__C */
#pragma DATA_SECTION(xdc_runtime_knl_GateH_Module__loggerFxn2__C, ".const:xdc_runtime_knl_GateH_Module__loggerFxn2__C");
__FAR__ const CT__xdc_runtime_knl_GateH_Module__loggerFxn2 xdc_runtime_knl_GateH_Module__loggerFxn2__C = ((const CT__xdc_runtime_knl_GateH_Module__loggerFxn2)NULL);

/* Module__loggerFxn4__C */
#pragma DATA_SECTION(xdc_runtime_knl_GateH_Module__loggerFxn4__C, ".const:xdc_runtime_knl_GateH_Module__loggerFxn4__C");
__FAR__ const CT__xdc_runtime_knl_GateH_Module__loggerFxn4 xdc_runtime_knl_GateH_Module__loggerFxn4__C = ((const CT__xdc_runtime_knl_GateH_Module__loggerFxn4)NULL);

/* Module__loggerFxn8__C */
#pragma DATA_SECTION(xdc_runtime_knl_GateH_Module__loggerFxn8__C, ".const:xdc_runtime_knl_GateH_Module__loggerFxn8__C");
__FAR__ const CT__xdc_runtime_knl_GateH_Module__loggerFxn8 xdc_runtime_knl_GateH_Module__loggerFxn8__C = ((const CT__xdc_runtime_knl_GateH_Module__loggerFxn8)NULL);

/* Object__count__C */
#pragma DATA_SECTION(xdc_runtime_knl_GateH_Object__count__C, ".const:xdc_runtime_knl_GateH_Object__count__C");
__FAR__ const CT__xdc_runtime_knl_GateH_Object__count xdc_runtime_knl_GateH_Object__count__C = 0;

/* Object__heap__C */
#pragma DATA_SECTION(xdc_runtime_knl_GateH_Object__heap__C, ".const:xdc_runtime_knl_GateH_Object__heap__C");
__FAR__ const CT__xdc_runtime_knl_GateH_Object__heap xdc_runtime_knl_GateH_Object__heap__C = NULL;

/* Object__sizeof__C */
#pragma DATA_SECTION(xdc_runtime_knl_GateH_Object__sizeof__C, ".const:xdc_runtime_knl_GateH_Object__sizeof__C");
__FAR__ const CT__xdc_runtime_knl_GateH_Object__sizeof xdc_runtime_knl_GateH_Object__sizeof__C = 0;

/* Object__table__C */
#pragma DATA_SECTION(xdc_runtime_knl_GateH_Object__table__C, ".const:xdc_runtime_knl_GateH_Object__table__C");
__FAR__ const CT__xdc_runtime_knl_GateH_Object__table xdc_runtime_knl_GateH_Object__table__C = NULL;


/*
 * ======== xdc.runtime.knl.GateH_Proxy INITIALIZERS ========
 */


/*
 * ======== xdc.runtime.knl.GateThread INITIALIZERS ========
 */

/* Object__DESC__C */
__FAR__ const xdc_runtime_Core_ObjDesc xdc_runtime_knl_GateThread_Object__DESC__C;

/* Object__PARAMS__C */
#pragma DATA_SECTION(xdc_runtime_knl_GateThread_Object__PARAMS__C, ".const:xdc_runtime_knl_GateThread_Object__PARAMS__C");
__FAR__ const xdc_runtime_knl_GateThread_Params xdc_runtime_knl_GateThread_Object__PARAMS__C = {
    sizeof (xdc_runtime_knl_GateThread_Params), /* __size */
    NULL, /* __self */
    NULL, /* __fxns */
    (xdc_runtime_IInstance_Params*)&xdc_runtime_knl_GateThread_Object__PARAMS__C.__iprms, /* instance */
    {
        sizeof (xdc_runtime_IInstance_Params), /* __size */
        0,  /* name */
    },  /* instance */
};

/* Module__root__V */
xdc_runtime_knl_GateThread_Module__ xdc_runtime_knl_GateThread_Module__root__V = {
    {&xdc_runtime_knl_GateThread_Module__root__V.link,  /* link.next */
    &xdc_runtime_knl_GateThread_Module__root__V.link},  /* link.prev */
};

/* Module__diagsEnabled__C */
#pragma DATA_SECTION(xdc_runtime_knl_GateThread_Module__diagsEnabled__C, ".const:xdc_runtime_knl_GateThread_Module__diagsEnabled__C");
__FAR__ const CT__xdc_runtime_knl_GateThread_Module__diagsEnabled xdc_runtime_knl_GateThread_Module__diagsEnabled__C = (xdc_Bits32)0x90U;

/* Module__diagsIncluded__C */
#pragma DATA_SECTION(xdc_runtime_knl_GateThread_Module__diagsIncluded__C, ".const:xdc_runtime_knl_GateThread_Module__diagsIncluded__C");
__FAR__ const CT__xdc_runtime_knl_GateThread_Module__diagsIncluded xdc_runtime_knl_GateThread_Module__diagsIncluded__C = (xdc_Bits32)0x90U;

/* Module__diagsMask__C */
#pragma DATA_SECTION(xdc_runtime_knl_GateThread_Module__diagsMask__C, ".const:xdc_runtime_knl_GateThread_Module__diagsMask__C");
__FAR__ const CT__xdc_runtime_knl_GateThread_Module__diagsMask xdc_runtime_knl_GateThread_Module__diagsMask__C = ((const CT__xdc_runtime_knl_GateThread_Module__diagsMask)NULL);

/* Module__gateObj__C */
#pragma DATA_SECTION(xdc_runtime_knl_GateThread_Module__gateObj__C, ".const:xdc_runtime_knl_GateThread_Module__gateObj__C");
__FAR__ const CT__xdc_runtime_knl_GateThread_Module__gateObj xdc_runtime_knl_GateThread_Module__gateObj__C = ((const CT__xdc_runtime_knl_GateThread_Module__gateObj)NULL);

/* Module__gatePrms__C */
#pragma DATA_SECTION(xdc_runtime_knl_GateThread_Module__gatePrms__C, ".const:xdc_runtime_knl_GateThread_Module__gatePrms__C");
__FAR__ const CT__xdc_runtime_knl_GateThread_Module__gatePrms xdc_runtime_knl_GateThread_Module__gatePrms__C = ((const CT__xdc_runtime_knl_GateThread_Module__gatePrms)NULL);

/* Module__id__C */
#pragma DATA_SECTION(xdc_runtime_knl_GateThread_Module__id__C, ".const:xdc_runtime_knl_GateThread_Module__id__C");
__FAR__ const CT__xdc_runtime_knl_GateThread_Module__id xdc_runtime_knl_GateThread_Module__id__C = (xdc_Bits16)0x802aU;

/* Module__loggerDefined__C */
#pragma DATA_SECTION(xdc_runtime_knl_GateThread_Module__loggerDefined__C, ".const:xdc_runtime_knl_GateThread_Module__loggerDefined__C");
__FAR__ const CT__xdc_runtime_knl_GateThread_Module__loggerDefined xdc_runtime_knl_GateThread_Module__loggerDefined__C = 0;

/* Module__loggerObj__C */
#pragma DATA_SECTION(xdc_runtime_knl_GateThread_Module__loggerObj__C, ".const:xdc_runtime_knl_GateThread_Module__loggerObj__C");
__FAR__ const CT__xdc_runtime_knl_GateThread_Module__loggerObj xdc_runtime_knl_GateThread_Module__loggerObj__C = ((const CT__xdc_runtime_knl_GateThread_Module__loggerObj)NULL);

/* Module__loggerFxn0__C */
#pragma DATA_SECTION(xdc_runtime_knl_GateThread_Module__loggerFxn0__C, ".const:xdc_runtime_knl_GateThread_Module__loggerFxn0__C");
__FAR__ const CT__xdc_runtime_knl_GateThread_Module__loggerFxn0 xdc_runtime_knl_GateThread_Module__loggerFxn0__C = ((const CT__xdc_runtime_knl_GateThread_Module__loggerFxn0)NULL);

/* Module__loggerFxn1__C */
#pragma DATA_SECTION(xdc_runtime_knl_GateThread_Module__loggerFxn1__C, ".const:xdc_runtime_knl_GateThread_Module__loggerFxn1__C");
__FAR__ const CT__xdc_runtime_knl_GateThread_Module__loggerFxn1 xdc_runtime_knl_GateThread_Module__loggerFxn1__C = ((const CT__xdc_runtime_knl_GateThread_Module__loggerFxn1)NULL);

/* Module__loggerFxn2__C */
#pragma DATA_SECTION(xdc_runtime_knl_GateThread_Module__loggerFxn2__C, ".const:xdc_runtime_knl_GateThread_Module__loggerFxn2__C");
__FAR__ const CT__xdc_runtime_knl_GateThread_Module__loggerFxn2 xdc_runtime_knl_GateThread_Module__loggerFxn2__C = ((const CT__xdc_runtime_knl_GateThread_Module__loggerFxn2)NULL);

/* Module__loggerFxn4__C */
#pragma DATA_SECTION(xdc_runtime_knl_GateThread_Module__loggerFxn4__C, ".const:xdc_runtime_knl_GateThread_Module__loggerFxn4__C");
__FAR__ const CT__xdc_runtime_knl_GateThread_Module__loggerFxn4 xdc_runtime_knl_GateThread_Module__loggerFxn4__C = ((const CT__xdc_runtime_knl_GateThread_Module__loggerFxn4)NULL);

/* Module__loggerFxn8__C */
#pragma DATA_SECTION(xdc_runtime_knl_GateThread_Module__loggerFxn8__C, ".const:xdc_runtime_knl_GateThread_Module__loggerFxn8__C");
__FAR__ const CT__xdc_runtime_knl_GateThread_Module__loggerFxn8 xdc_runtime_knl_GateThread_Module__loggerFxn8__C = ((const CT__xdc_runtime_knl_GateThread_Module__loggerFxn8)NULL);

/* Object__count__C */
#pragma DATA_SECTION(xdc_runtime_knl_GateThread_Object__count__C, ".const:xdc_runtime_knl_GateThread_Object__count__C");
__FAR__ const CT__xdc_runtime_knl_GateThread_Object__count xdc_runtime_knl_GateThread_Object__count__C = 0;

/* Object__heap__C */
#pragma DATA_SECTION(xdc_runtime_knl_GateThread_Object__heap__C, ".const:xdc_runtime_knl_GateThread_Object__heap__C");
__FAR__ const CT__xdc_runtime_knl_GateThread_Object__heap xdc_runtime_knl_GateThread_Object__heap__C = NULL;

/* Object__sizeof__C */
#pragma DATA_SECTION(xdc_runtime_knl_GateThread_Object__sizeof__C, ".const:xdc_runtime_knl_GateThread_Object__sizeof__C");
__FAR__ const CT__xdc_runtime_knl_GateThread_Object__sizeof xdc_runtime_knl_GateThread_Object__sizeof__C = sizeof(xdc_runtime_knl_GateThread_Object__);

/* Object__table__C */
#pragma DATA_SECTION(xdc_runtime_knl_GateThread_Object__table__C, ".const:xdc_runtime_knl_GateThread_Object__table__C");
__FAR__ const CT__xdc_runtime_knl_GateThread_Object__table xdc_runtime_knl_GateThread_Object__table__C = NULL;


/*
 * ======== xdc.runtime.knl.GateThread_Proxy INITIALIZERS ========
 */


/*
 * ======== xdc.runtime.System FUNCTION STUBS ========
 */

/* printf_va__E */
xdc_Int xdc_runtime_System_printf_va__E( xdc_CString fmt, va_list arg__va)
{
    return xdc_runtime_System_printf_va__F(fmt, arg__va);
}

/* printf__E */
xdc_Int xdc_runtime_System_printf__E( xdc_CString fmt, ...)
{
    xdc_Int retval;

    va_list arg__va;
    (void)va_start(arg__va, fmt);
    retval = xdc_runtime_System_printf_va__F(fmt, arg__va);

    va_end(arg__va);
    return retval;
}

/* aprintf_va__E */
xdc_Int xdc_runtime_System_aprintf_va__E( xdc_CString fmt, va_list arg__va)
{
    return xdc_runtime_System_aprintf_va__F(fmt, arg__va);
}

/* aprintf__E */
xdc_Int xdc_runtime_System_aprintf__E( xdc_CString fmt, ...)
{
    xdc_Int retval;

    va_list arg__va;
    (void)va_start(arg__va, fmt);
    retval = xdc_runtime_System_aprintf_va__F(fmt, arg__va);

    va_end(arg__va);
    return retval;
}

/* sprintf_va__E */
xdc_Int xdc_runtime_System_sprintf_va__E( xdc_Char buf[], xdc_CString fmt, va_list arg__va)
{
    return xdc_runtime_System_sprintf_va__F(buf, fmt, arg__va);
}

/* sprintf__E */
xdc_Int xdc_runtime_System_sprintf__E( xdc_Char buf[], xdc_CString fmt, ...)
{
    xdc_Int retval;

    va_list arg__va;
    (void)va_start(arg__va, fmt);
    retval = xdc_runtime_System_sprintf_va__F(buf, fmt, arg__va);

    va_end(arg__va);
    return retval;
}

/* asprintf_va__E */
xdc_Int xdc_runtime_System_asprintf_va__E( xdc_Char buf[], xdc_CString fmt, va_list arg__va)
{
    return xdc_runtime_System_asprintf_va__F(buf, fmt, arg__va);
}

/* asprintf__E */
xdc_Int xdc_runtime_System_asprintf__E( xdc_Char buf[], xdc_CString fmt, ...)
{
    xdc_Int retval;

    va_list arg__va;
    (void)va_start(arg__va, fmt);
    retval = xdc_runtime_System_asprintf_va__F(buf, fmt, arg__va);

    va_end(arg__va);
    return retval;
}

/* snprintf_va__E */
xdc_Int xdc_runtime_System_snprintf_va__E( xdc_Char buf[], xdc_SizeT n, xdc_CString fmt, va_list arg__va)
{
    return xdc_runtime_System_snprintf_va__F(buf, n, fmt, arg__va);
}

/* snprintf__E */
xdc_Int xdc_runtime_System_snprintf__E( xdc_Char buf[], xdc_SizeT n, xdc_CString fmt, ...)
{
    xdc_Int retval;

    va_list arg__va;
    (void)va_start(arg__va, fmt);
    retval = xdc_runtime_System_snprintf_va__F(buf, n, fmt, arg__va);

    va_end(arg__va);
    return retval;
}


/*
 * ======== ti.sysbios.BIOS_RtsGateProxy PROXY BODY ========
 */

/* DELEGATES TO ti.sysbios.gates.GateMutex */

/* Module__startupDone__S */
xdc_Bool ti_sysbios_BIOS_RtsGateProxy_Module__startupDone__S(void)
{
    return ti_sysbios_gates_GateMutex_Module__startupDone__S();
}

/* create */
ti_sysbios_BIOS_RtsGateProxy_Handle ti_sysbios_BIOS_RtsGateProxy_create( const ti_sysbios_BIOS_RtsGateProxy_Params *prms, xdc_runtime_Error_Block *eb )
{
    return (ti_sysbios_BIOS_RtsGateProxy_Handle)ti_sysbios_gates_GateMutex_create((const ti_sysbios_gates_GateMutex_Params *)prms, eb);
}

/* delete */
void ti_sysbios_BIOS_RtsGateProxy_delete(ti_sysbios_BIOS_RtsGateProxy_Handle *instp)
{
    ti_sysbios_gates_GateMutex_Object__delete__S(instp);
}

/* Params__init__S */
void ti_sysbios_BIOS_RtsGateProxy_Params__init__S( xdc_Ptr dst, const void *src, xdc_SizeT psz, xdc_SizeT isz )
{
    ti_sysbios_gates_GateMutex_Params__init__S(dst, src, psz, isz);
}

/* Handle__label__S */
xdc_runtime_Types_Label *ti_sysbios_BIOS_RtsGateProxy_Handle__label__S(xdc_Ptr obj, xdc_runtime_Types_Label *lab)
{
    return ti_sysbios_gates_GateMutex_Handle__label__S(obj, lab);
}

/* query__E */
xdc_Bool ti_sysbios_BIOS_RtsGateProxy_query__E( xdc_Int qual )
{
    return ti_sysbios_gates_GateMutex_query(qual);
}

/* enter__E */
xdc_IArg ti_sysbios_BIOS_RtsGateProxy_enter__E( ti_sysbios_BIOS_RtsGateProxy_Handle inst )
{
    return ti_sysbios_gates_GateMutex_enter((ti_sysbios_gates_GateMutex_Handle)inst);
}

/* leave__E */
xdc_Void ti_sysbios_BIOS_RtsGateProxy_leave__E( ti_sysbios_BIOS_RtsGateProxy_Handle inst, xdc_IArg key )
{
    ti_sysbios_gates_GateMutex_leave((ti_sysbios_gates_GateMutex_Handle)inst, key);
}


/*
 * ======== ti.sysbios.hal.Cache_CacheProxy PROXY BODY ========
 */

/* DELEGATES TO ti.sysbios.family.c7x.Cache */

/* Module__startupDone__S */
xdc_Bool ti_sysbios_hal_Cache_CacheProxy_Module__startupDone__S(void)
{
    return ti_sysbios_family_c7x_Cache_Module__startupDone__S();
}

/* enable__E */
xdc_Void ti_sysbios_hal_Cache_CacheProxy_enable__E( xdc_Bits16 type )
{
    ti_sysbios_family_c7x_Cache_enable(type);
}

/* disable__E */
xdc_Void ti_sysbios_hal_Cache_CacheProxy_disable__E( xdc_Bits16 type )
{
    ti_sysbios_family_c7x_Cache_disable(type);
}

/* inv__E */
xdc_Void ti_sysbios_hal_Cache_CacheProxy_inv__E( xdc_Ptr blockPtr, xdc_SizeT byteCnt, xdc_Bits16 type, xdc_Bool wait )
{
    ti_sysbios_family_c7x_Cache_inv(blockPtr, byteCnt, type, wait);
}

/* wb__E */
xdc_Void ti_sysbios_hal_Cache_CacheProxy_wb__E( xdc_Ptr blockPtr, xdc_SizeT byteCnt, xdc_Bits16 type, xdc_Bool wait )
{
    ti_sysbios_family_c7x_Cache_wb(blockPtr, byteCnt, type, wait);
}

/* wbInv__E */
xdc_Void ti_sysbios_hal_Cache_CacheProxy_wbInv__E( xdc_Ptr blockPtr, xdc_SizeT byteCnt, xdc_Bits16 type, xdc_Bool wait )
{
    ti_sysbios_family_c7x_Cache_wbInv(blockPtr, byteCnt, type, wait);
}

/* wbAll__E */
xdc_Void ti_sysbios_hal_Cache_CacheProxy_wbAll__E( void )
{
    ti_sysbios_family_c7x_Cache_wbAll();
}

/* wbInvAll__E */
xdc_Void ti_sysbios_hal_Cache_CacheProxy_wbInvAll__E( void )
{
    ti_sysbios_family_c7x_Cache_wbInvAll();
}

/* wait__E */
xdc_Void ti_sysbios_hal_Cache_CacheProxy_wait__E( void )
{
    ti_sysbios_family_c7x_Cache_wait();
}


/*
 * ======== ti.sysbios.hal.Core_CoreProxy PROXY BODY ========
 */

/* DELEGATES TO ti.sysbios.hal.CoreNull */

/* Module__startupDone__S */
xdc_Bool ti_sysbios_hal_Core_CoreProxy_Module__startupDone__S(void)
{
    return ti_sysbios_hal_CoreNull_Module__startupDone__S();
}

/* getId__E */
xdc_UInt ti_sysbios_hal_Core_CoreProxy_getId__E( void )
{
    return ti_sysbios_hal_CoreNull_getId();
}

/* interruptCore__E */
xdc_Void ti_sysbios_hal_Core_CoreProxy_interruptCore__E( xdc_UInt coreId )
{
    ti_sysbios_hal_CoreNull_interruptCore(coreId);
}

/* lock__E */
xdc_IArg ti_sysbios_hal_Core_CoreProxy_lock__E( void )
{
    return ti_sysbios_hal_CoreNull_lock();
}

/* unlock__E */
xdc_Void ti_sysbios_hal_Core_CoreProxy_unlock__E( void )
{
    ti_sysbios_hal_CoreNull_unlock();
}

/* hwiDisable__E */
xdc_UInt ti_sysbios_hal_Core_CoreProxy_hwiDisable__E( void )
{
    return ti_sysbios_hal_CoreNull_hwiDisable();
}

/* hwiEnable__E */
xdc_UInt ti_sysbios_hal_Core_CoreProxy_hwiEnable__E( void )
{
    return ti_sysbios_hal_CoreNull_hwiEnable();
}

/* hwiRestore__E */
xdc_Void ti_sysbios_hal_Core_CoreProxy_hwiRestore__E( xdc_UInt key )
{
    ti_sysbios_hal_CoreNull_hwiRestore(key);
}


/*
 * ======== ti.sysbios.hal.Hwi_HwiProxy PROXY BODY ========
 */

/* DELEGATES TO ti.sysbios.family.c7x.Hwi */

/* Module__startupDone__S */
xdc_Bool ti_sysbios_hal_Hwi_HwiProxy_Module__startupDone__S(void)
{
    return ti_sysbios_family_c7x_Hwi_Module__startupDone__S();
}

/* create */
ti_sysbios_hal_Hwi_HwiProxy_Handle ti_sysbios_hal_Hwi_HwiProxy_create( xdc_Int intNum, ti_sysbios_interfaces_IHwi_FuncPtr hwiFxn, const ti_sysbios_hal_Hwi_HwiProxy_Params *prms, xdc_runtime_Error_Block *eb )
{
    return (ti_sysbios_hal_Hwi_HwiProxy_Handle)ti_sysbios_family_c7x_Hwi_create(intNum, hwiFxn, (const ti_sysbios_family_c7x_Hwi_Params *)prms, eb);
}

/* delete */
void ti_sysbios_hal_Hwi_HwiProxy_delete(ti_sysbios_hal_Hwi_HwiProxy_Handle *instp)
{
    ti_sysbios_family_c7x_Hwi_Object__delete__S(instp);
}

/* Params__init__S */
void ti_sysbios_hal_Hwi_HwiProxy_Params__init__S( xdc_Ptr dst, const void *src, xdc_SizeT psz, xdc_SizeT isz )
{
    ti_sysbios_family_c7x_Hwi_Params__init__S(dst, src, psz, isz);
}

/* Handle__label__S */
xdc_runtime_Types_Label *ti_sysbios_hal_Hwi_HwiProxy_Handle__label__S(xdc_Ptr obj, xdc_runtime_Types_Label *lab)
{
    return ti_sysbios_family_c7x_Hwi_Handle__label__S(obj, lab);
}

/* getStackInfo__E */
xdc_Bool ti_sysbios_hal_Hwi_HwiProxy_getStackInfo__E( ti_sysbios_interfaces_IHwi_StackInfo *stkInfo, xdc_Bool computeStackDepth )
{
    return ti_sysbios_family_c7x_Hwi_getStackInfo(stkInfo, computeStackDepth);
}

/* getCoreStackInfo__E */
xdc_Bool ti_sysbios_hal_Hwi_HwiProxy_getCoreStackInfo__E( ti_sysbios_interfaces_IHwi_StackInfo *stkInfo, xdc_Bool computeStackDepth, xdc_UInt coreId )
{
    return ti_sysbios_family_c7x_Hwi_getCoreStackInfo(stkInfo, computeStackDepth, coreId);
}

/* startup__E */
xdc_Void ti_sysbios_hal_Hwi_HwiProxy_startup__E( void )
{
    ti_sysbios_family_c7x_Hwi_startup();
}

/* disable__E */
xdc_UInt ti_sysbios_hal_Hwi_HwiProxy_disable__E( void )
{
    return ti_sysbios_family_c7x_Hwi_disable();
}

/* enable__E */
xdc_UInt ti_sysbios_hal_Hwi_HwiProxy_enable__E( void )
{
    return ti_sysbios_family_c7x_Hwi_enable();
}

/* restore__E */
xdc_Void ti_sysbios_hal_Hwi_HwiProxy_restore__E( xdc_UInt key )
{
    ti_sysbios_family_c7x_Hwi_restore(key);
}

/* switchFromBootStack__E */
xdc_Void ti_sysbios_hal_Hwi_HwiProxy_switchFromBootStack__E( void )
{
    ti_sysbios_family_c7x_Hwi_switchFromBootStack();
}

/* post__E */
xdc_Void ti_sysbios_hal_Hwi_HwiProxy_post__E( xdc_UInt intNum )
{
    ti_sysbios_family_c7x_Hwi_post(intNum);
}

/* getTaskSP__E */
xdc_Char *ti_sysbios_hal_Hwi_HwiProxy_getTaskSP__E( void )
{
    return ti_sysbios_family_c7x_Hwi_getTaskSP();
}

/* disableInterrupt__E */
xdc_UInt ti_sysbios_hal_Hwi_HwiProxy_disableInterrupt__E( xdc_UInt intNum )
{
    return ti_sysbios_family_c7x_Hwi_disableInterrupt(intNum);
}

/* enableInterrupt__E */
xdc_UInt ti_sysbios_hal_Hwi_HwiProxy_enableInterrupt__E( xdc_UInt intNum )
{
    return ti_sysbios_family_c7x_Hwi_enableInterrupt(intNum);
}

/* restoreInterrupt__E */
xdc_Void ti_sysbios_hal_Hwi_HwiProxy_restoreInterrupt__E( xdc_UInt intNum, xdc_UInt key )
{
    ti_sysbios_family_c7x_Hwi_restoreInterrupt(intNum, key);
}

/* clearInterrupt__E */
xdc_Void ti_sysbios_hal_Hwi_HwiProxy_clearInterrupt__E( xdc_UInt intNum )
{
    ti_sysbios_family_c7x_Hwi_clearInterrupt(intNum);
}

/* getFunc__E */
ti_sysbios_interfaces_IHwi_FuncPtr ti_sysbios_hal_Hwi_HwiProxy_getFunc__E( ti_sysbios_hal_Hwi_HwiProxy_Handle inst, xdc_UArg *arg )
{
    return ti_sysbios_family_c7x_Hwi_getFunc((ti_sysbios_family_c7x_Hwi_Handle)inst, arg);
}

/* setFunc__E */
xdc_Void ti_sysbios_hal_Hwi_HwiProxy_setFunc__E( ti_sysbios_hal_Hwi_HwiProxy_Handle inst, ti_sysbios_interfaces_IHwi_FuncPtr fxn, xdc_UArg arg )
{
    ti_sysbios_family_c7x_Hwi_setFunc((ti_sysbios_family_c7x_Hwi_Handle)inst, fxn, arg);
}

/* getHookContext__E */
xdc_Ptr ti_sysbios_hal_Hwi_HwiProxy_getHookContext__E( ti_sysbios_hal_Hwi_HwiProxy_Handle inst, xdc_Int id )
{
    return ti_sysbios_family_c7x_Hwi_getHookContext((ti_sysbios_family_c7x_Hwi_Handle)inst, id);
}

/* setHookContext__E */
xdc_Void ti_sysbios_hal_Hwi_HwiProxy_setHookContext__E( ti_sysbios_hal_Hwi_HwiProxy_Handle inst, xdc_Int id, xdc_Ptr hookContext )
{
    ti_sysbios_family_c7x_Hwi_setHookContext((ti_sysbios_family_c7x_Hwi_Handle)inst, id, hookContext);
}

/* getIrp__E */
ti_sysbios_interfaces_IHwi_Irp ti_sysbios_hal_Hwi_HwiProxy_getIrp__E( ti_sysbios_hal_Hwi_HwiProxy_Handle inst )
{
    return ti_sysbios_family_c7x_Hwi_getIrp((ti_sysbios_family_c7x_Hwi_Handle)inst);
}


/*
 * ======== ti.sysbios.heaps.HeapMem_Module_GateProxy PROXY BODY ========
 */

/* DELEGATES TO ti.sysbios.gates.GateMutex */

/* Module__startupDone__S */
xdc_Bool ti_sysbios_heaps_HeapMem_Module_GateProxy_Module__startupDone__S(void)
{
    return ti_sysbios_gates_GateMutex_Module__startupDone__S();
}

/* create */
ti_sysbios_heaps_HeapMem_Module_GateProxy_Handle ti_sysbios_heaps_HeapMem_Module_GateProxy_create( const ti_sysbios_heaps_HeapMem_Module_GateProxy_Params *prms, xdc_runtime_Error_Block *eb )
{
    return (ti_sysbios_heaps_HeapMem_Module_GateProxy_Handle)ti_sysbios_gates_GateMutex_create((const ti_sysbios_gates_GateMutex_Params *)prms, eb);
}

/* delete */
void ti_sysbios_heaps_HeapMem_Module_GateProxy_delete(ti_sysbios_heaps_HeapMem_Module_GateProxy_Handle *instp)
{
    ti_sysbios_gates_GateMutex_Object__delete__S(instp);
}

/* Params__init__S */
void ti_sysbios_heaps_HeapMem_Module_GateProxy_Params__init__S( xdc_Ptr dst, const void *src, xdc_SizeT psz, xdc_SizeT isz )
{
    ti_sysbios_gates_GateMutex_Params__init__S(dst, src, psz, isz);
}

/* Handle__label__S */
xdc_runtime_Types_Label *ti_sysbios_heaps_HeapMem_Module_GateProxy_Handle__label__S(xdc_Ptr obj, xdc_runtime_Types_Label *lab)
{
    return ti_sysbios_gates_GateMutex_Handle__label__S(obj, lab);
}

/* query__E */
xdc_Bool ti_sysbios_heaps_HeapMem_Module_GateProxy_query__E( xdc_Int qual )
{
    return ti_sysbios_gates_GateMutex_query(qual);
}

/* enter__E */
xdc_IArg ti_sysbios_heaps_HeapMem_Module_GateProxy_enter__E( ti_sysbios_heaps_HeapMem_Module_GateProxy_Handle inst )
{
    return ti_sysbios_gates_GateMutex_enter((ti_sysbios_gates_GateMutex_Handle)inst);
}

/* leave__E */
xdc_Void ti_sysbios_heaps_HeapMem_Module_GateProxy_leave__E( ti_sysbios_heaps_HeapMem_Module_GateProxy_Handle inst, xdc_IArg key )
{
    ti_sysbios_gates_GateMutex_leave((ti_sysbios_gates_GateMutex_Handle)inst, key);
}


/*
 * ======== ti.sysbios.knl.Clock_TimerProxy PROXY BODY ========
 */

/* DELEGATES TO ti.sysbios.timers.dmtimer.Timer */

/* Module__startupDone__S */
xdc_Bool ti_sysbios_knl_Clock_TimerProxy_Module__startupDone__S(void)
{
    return ti_sysbios_timers_dmtimer_Timer_Module__startupDone__S();
}

/* create */
ti_sysbios_knl_Clock_TimerProxy_Handle ti_sysbios_knl_Clock_TimerProxy_create( xdc_Int id, ti_sysbios_interfaces_ITimer_FuncPtr tickFxn, const ti_sysbios_knl_Clock_TimerProxy_Params *prms, xdc_runtime_Error_Block *eb )
{
    return (ti_sysbios_knl_Clock_TimerProxy_Handle)ti_sysbios_timers_dmtimer_Timer_create(id, tickFxn, (const ti_sysbios_timers_dmtimer_Timer_Params *)prms, eb);
}

/* delete */
void ti_sysbios_knl_Clock_TimerProxy_delete(ti_sysbios_knl_Clock_TimerProxy_Handle *instp)
{
    ti_sysbios_timers_dmtimer_Timer_Object__delete__S(instp);
}

/* Params__init__S */
void ti_sysbios_knl_Clock_TimerProxy_Params__init__S( xdc_Ptr dst, const void *src, xdc_SizeT psz, xdc_SizeT isz )
{
    ti_sysbios_timers_dmtimer_Timer_Params__init__S(dst, src, psz, isz);
}

/* Handle__label__S */
xdc_runtime_Types_Label *ti_sysbios_knl_Clock_TimerProxy_Handle__label__S(xdc_Ptr obj, xdc_runtime_Types_Label *lab)
{
    return ti_sysbios_timers_dmtimer_Timer_Handle__label__S(obj, lab);
}

/* getNumTimers__E */
xdc_UInt ti_sysbios_knl_Clock_TimerProxy_getNumTimers__E( void )
{
    return ti_sysbios_timers_dmtimer_Timer_getNumTimers();
}

/* getStatus__E */
ti_sysbios_interfaces_ITimer_Status ti_sysbios_knl_Clock_TimerProxy_getStatus__E( xdc_UInt id )
{
    return ti_sysbios_timers_dmtimer_Timer_getStatus(id);
}

/* startup__E */
xdc_Void ti_sysbios_knl_Clock_TimerProxy_startup__E( void )
{
    ti_sysbios_timers_dmtimer_Timer_startup();
}

/* getMaxTicks__E */
xdc_UInt32 ti_sysbios_knl_Clock_TimerProxy_getMaxTicks__E( ti_sysbios_knl_Clock_TimerProxy_Handle inst )
{
    return ti_sysbios_timers_dmtimer_Timer_getMaxTicks((ti_sysbios_timers_dmtimer_Timer_Handle)inst);
}

/* setNextTick__E */
xdc_Void ti_sysbios_knl_Clock_TimerProxy_setNextTick__E( ti_sysbios_knl_Clock_TimerProxy_Handle inst, xdc_UInt32 ticks )
{
    ti_sysbios_timers_dmtimer_Timer_setNextTick((ti_sysbios_timers_dmtimer_Timer_Handle)inst, ticks);
}

/* start__E */
xdc_Void ti_sysbios_knl_Clock_TimerProxy_start__E( ti_sysbios_knl_Clock_TimerProxy_Handle inst )
{
    ti_sysbios_timers_dmtimer_Timer_start((ti_sysbios_timers_dmtimer_Timer_Handle)inst);
}

/* stop__E */
xdc_Void ti_sysbios_knl_Clock_TimerProxy_stop__E( ti_sysbios_knl_Clock_TimerProxy_Handle inst )
{
    ti_sysbios_timers_dmtimer_Timer_stop((ti_sysbios_timers_dmtimer_Timer_Handle)inst);
}

/* setPeriod__E */
xdc_Void ti_sysbios_knl_Clock_TimerProxy_setPeriod__E( ti_sysbios_knl_Clock_TimerProxy_Handle inst, xdc_UInt32 period )
{
    ti_sysbios_timers_dmtimer_Timer_setPeriod((ti_sysbios_timers_dmtimer_Timer_Handle)inst, period);
}

/* setPeriodMicroSecs__E */
xdc_Bool ti_sysbios_knl_Clock_TimerProxy_setPeriodMicroSecs__E( ti_sysbios_knl_Clock_TimerProxy_Handle inst, xdc_UInt32 microsecs )
{
    return ti_sysbios_timers_dmtimer_Timer_setPeriodMicroSecs((ti_sysbios_timers_dmtimer_Timer_Handle)inst, microsecs);
}

/* getPeriod__E */
xdc_UInt32 ti_sysbios_knl_Clock_TimerProxy_getPeriod__E( ti_sysbios_knl_Clock_TimerProxy_Handle inst )
{
    return ti_sysbios_timers_dmtimer_Timer_getPeriod((ti_sysbios_timers_dmtimer_Timer_Handle)inst);
}

/* getCount__E */
xdc_UInt32 ti_sysbios_knl_Clock_TimerProxy_getCount__E( ti_sysbios_knl_Clock_TimerProxy_Handle inst )
{
    return ti_sysbios_timers_dmtimer_Timer_getCount((ti_sysbios_timers_dmtimer_Timer_Handle)inst);
}

/* getFreq__E */
xdc_Void ti_sysbios_knl_Clock_TimerProxy_getFreq__E( ti_sysbios_knl_Clock_TimerProxy_Handle inst, xdc_runtime_Types_FreqHz *freq )
{
    ti_sysbios_timers_dmtimer_Timer_getFreq((ti_sysbios_timers_dmtimer_Timer_Handle)inst, freq);
}

/* getFunc__E */
ti_sysbios_interfaces_ITimer_FuncPtr ti_sysbios_knl_Clock_TimerProxy_getFunc__E( ti_sysbios_knl_Clock_TimerProxy_Handle inst, xdc_UArg *arg )
{
    return ti_sysbios_timers_dmtimer_Timer_getFunc((ti_sysbios_timers_dmtimer_Timer_Handle)inst, arg);
}

/* setFunc__E */
xdc_Void ti_sysbios_knl_Clock_TimerProxy_setFunc__E( ti_sysbios_knl_Clock_TimerProxy_Handle inst, ti_sysbios_interfaces_ITimer_FuncPtr fxn, xdc_UArg arg )
{
    ti_sysbios_timers_dmtimer_Timer_setFunc((ti_sysbios_timers_dmtimer_Timer_Handle)inst, fxn, arg);
}

/* trigger__E */
xdc_Void ti_sysbios_knl_Clock_TimerProxy_trigger__E( ti_sysbios_knl_Clock_TimerProxy_Handle inst, xdc_UInt32 cycles )
{
    ti_sysbios_timers_dmtimer_Timer_trigger((ti_sysbios_timers_dmtimer_Timer_Handle)inst, cycles);
}

/* getExpiredCounts__E */
xdc_UInt32 ti_sysbios_knl_Clock_TimerProxy_getExpiredCounts__E( ti_sysbios_knl_Clock_TimerProxy_Handle inst )
{
    return ti_sysbios_timers_dmtimer_Timer_getExpiredCounts((ti_sysbios_timers_dmtimer_Timer_Handle)inst);
}

/* getExpiredTicks__E */
xdc_UInt32 ti_sysbios_knl_Clock_TimerProxy_getExpiredTicks__E( ti_sysbios_knl_Clock_TimerProxy_Handle inst, xdc_UInt32 tickPeriod )
{
    return ti_sysbios_timers_dmtimer_Timer_getExpiredTicks((ti_sysbios_timers_dmtimer_Timer_Handle)inst, tickPeriod);
}

/* getCurrentTick__E */
xdc_UInt32 ti_sysbios_knl_Clock_TimerProxy_getCurrentTick__E( ti_sysbios_knl_Clock_TimerProxy_Handle inst, xdc_Bool save )
{
    return ti_sysbios_timers_dmtimer_Timer_getCurrentTick((ti_sysbios_timers_dmtimer_Timer_Handle)inst, save);
}


/*
 * ======== ti.sysbios.knl.Intrinsics_SupportProxy PROXY BODY ========
 */

/* DELEGATES TO ti.sysbios.family.c7x.IntrinsicsSupport */

/* Module__startupDone__S */
xdc_Bool ti_sysbios_knl_Intrinsics_SupportProxy_Module__startupDone__S(void)
{
    return ti_sysbios_family_c7x_IntrinsicsSupport_Module__startupDone__S();
}

/* maxbit__E */
xdc_UInt ti_sysbios_knl_Intrinsics_SupportProxy_maxbit__E( xdc_UInt bits )
{
    return ti_sysbios_family_c7x_IntrinsicsSupport_maxbit(bits);
}


/*
 * ======== ti.sysbios.knl.Task_SupportProxy PROXY BODY ========
 */

/* DELEGATES TO ti.sysbios.family.c7x.TaskSupport */

/* Module__startupDone__S */
xdc_Bool ti_sysbios_knl_Task_SupportProxy_Module__startupDone__S(void)
{
    return ti_sysbios_family_c7x_TaskSupport_Module__startupDone__S();
}

/* start__E */
xdc_Ptr ti_sysbios_knl_Task_SupportProxy_start__E( xdc_Ptr curTask, ti_sysbios_interfaces_ITaskSupport_FuncPtr enterFxn, ti_sysbios_interfaces_ITaskSupport_FuncPtr exitFxn, xdc_runtime_Error_Block *eb )
{
    return ti_sysbios_family_c7x_TaskSupport_start(curTask, enterFxn, exitFxn, eb);
}

/* swap__E */
xdc_Void ti_sysbios_knl_Task_SupportProxy_swap__E( xdc_Ptr *oldtskContext, xdc_Ptr *newtskContext )
{
    ti_sysbios_family_c7x_TaskSupport_swap(oldtskContext, newtskContext);
}

/* checkStack__E */
xdc_Bool ti_sysbios_knl_Task_SupportProxy_checkStack__E( xdc_Char *stack, xdc_SizeT size )
{
    return ti_sysbios_family_c7x_TaskSupport_checkStack(stack, size);
}

/* stackUsed__E */
xdc_SizeT ti_sysbios_knl_Task_SupportProxy_stackUsed__E( xdc_Char *stack, xdc_SizeT size )
{
    return ti_sysbios_family_c7x_TaskSupport_stackUsed(stack, size);
}

/* getStackAlignment__E */
xdc_UInt ti_sysbios_knl_Task_SupportProxy_getStackAlignment__E( void )
{
    return ti_sysbios_family_c7x_TaskSupport_getStackAlignment();
}


/*
 * ======== ti.sysbios.timers.dmtimer.Timer_TimerSupportProxy PROXY BODY ========
 */

/* DELEGATES TO ti.sysbios.family.c64p.tci6488.TimerSupport */

/* Module__startupDone__S */
xdc_Bool ti_sysbios_timers_dmtimer_Timer_TimerSupportProxy_Module__startupDone__S(void)
{
    return ti_sysbios_family_c64p_tci6488_TimerSupport_Module__startupDone__S();
}

/* enable__E */
xdc_Void ti_sysbios_timers_dmtimer_Timer_TimerSupportProxy_enable__E( xdc_UInt timerId, xdc_runtime_Error_Block *eb )
{
    ti_sysbios_family_c64p_tci6488_TimerSupport_enable(timerId, eb);
}


/*
 * ======== xdc.runtime.Main_Module_GateProxy PROXY BODY ========
 */

/* DELEGATES TO ti.sysbios.gates.GateHwi */

/* Module__startupDone__S */
xdc_Bool xdc_runtime_Main_Module_GateProxy_Module__startupDone__S(void)
{
    return ti_sysbios_gates_GateHwi_Module__startupDone__S();
}

/* create */
xdc_runtime_Main_Module_GateProxy_Handle xdc_runtime_Main_Module_GateProxy_create( const xdc_runtime_Main_Module_GateProxy_Params *prms, xdc_runtime_Error_Block *eb )
{
    return (xdc_runtime_Main_Module_GateProxy_Handle)ti_sysbios_gates_GateHwi_create((const ti_sysbios_gates_GateHwi_Params *)prms, eb);
}

/* delete */
void xdc_runtime_Main_Module_GateProxy_delete(xdc_runtime_Main_Module_GateProxy_Handle *instp)
{
    ti_sysbios_gates_GateHwi_Object__delete__S(instp);
}

/* Params__init__S */
void xdc_runtime_Main_Module_GateProxy_Params__init__S( xdc_Ptr dst, const void *src, xdc_SizeT psz, xdc_SizeT isz )
{
    ti_sysbios_gates_GateHwi_Params__init__S(dst, src, psz, isz);
}

/* Handle__label__S */
xdc_runtime_Types_Label *xdc_runtime_Main_Module_GateProxy_Handle__label__S(xdc_Ptr obj, xdc_runtime_Types_Label *lab)
{
    return ti_sysbios_gates_GateHwi_Handle__label__S(obj, lab);
}

/* query__E */
xdc_Bool xdc_runtime_Main_Module_GateProxy_query__E( xdc_Int qual )
{
    return ti_sysbios_gates_GateHwi_query(qual);
}

/* enter__E */
xdc_IArg xdc_runtime_Main_Module_GateProxy_enter__E( xdc_runtime_Main_Module_GateProxy_Handle inst )
{
    return ti_sysbios_gates_GateHwi_enter((ti_sysbios_gates_GateHwi_Handle)inst);
}

/* leave__E */
xdc_Void xdc_runtime_Main_Module_GateProxy_leave__E( xdc_runtime_Main_Module_GateProxy_Handle inst, xdc_IArg key )
{
    ti_sysbios_gates_GateHwi_leave((ti_sysbios_gates_GateHwi_Handle)inst, key);
}


/*
 * ======== xdc.runtime.Memory_HeapProxy PROXY BODY ========
 */

/* DELEGATES TO ti.sysbios.heaps.HeapMem */

/* Module__startupDone__S */
xdc_Bool xdc_runtime_Memory_HeapProxy_Module__startupDone__S(void)
{
    return ti_sysbios_heaps_HeapMem_Module__startupDone__S();
}

/* create */
xdc_runtime_Memory_HeapProxy_Handle xdc_runtime_Memory_HeapProxy_create( const xdc_runtime_Memory_HeapProxy_Params *prms, xdc_runtime_Error_Block *eb )
{
    return (xdc_runtime_Memory_HeapProxy_Handle)ti_sysbios_heaps_HeapMem_create((const ti_sysbios_heaps_HeapMem_Params *)prms, eb);
}

/* delete */
void xdc_runtime_Memory_HeapProxy_delete(xdc_runtime_Memory_HeapProxy_Handle *instp)
{
    ti_sysbios_heaps_HeapMem_Object__delete__S(instp);
}

/* Params__init__S */
void xdc_runtime_Memory_HeapProxy_Params__init__S( xdc_Ptr dst, const void *src, xdc_SizeT psz, xdc_SizeT isz )
{
    ti_sysbios_heaps_HeapMem_Params__init__S(dst, src, psz, isz);
}

/* Handle__label__S */
xdc_runtime_Types_Label *xdc_runtime_Memory_HeapProxy_Handle__label__S(xdc_Ptr obj, xdc_runtime_Types_Label *lab)
{
    return ti_sysbios_heaps_HeapMem_Handle__label__S(obj, lab);
}

/* alloc__E */
xdc_Ptr xdc_runtime_Memory_HeapProxy_alloc__E( xdc_runtime_Memory_HeapProxy_Handle inst, xdc_SizeT size, xdc_SizeT align, xdc_runtime_Error_Block *eb )
{
    return xdc_runtime_IHeap_alloc((xdc_runtime_IHeap_Handle)inst, size, align, eb);
}

/* free__E */
xdc_Void xdc_runtime_Memory_HeapProxy_free__E( xdc_runtime_Memory_HeapProxy_Handle inst, xdc_Ptr block, xdc_SizeT size )
{
    xdc_runtime_IHeap_free((xdc_runtime_IHeap_Handle)inst, block, size);
}

/* isBlocking__E */
xdc_Bool xdc_runtime_Memory_HeapProxy_isBlocking__E( xdc_runtime_Memory_HeapProxy_Handle inst )
{
    return xdc_runtime_IHeap_isBlocking((xdc_runtime_IHeap_Handle)inst);
}

/* getStats__E */
xdc_Void xdc_runtime_Memory_HeapProxy_getStats__E( xdc_runtime_Memory_HeapProxy_Handle inst, xdc_runtime_Memory_Stats *stats )
{
    xdc_runtime_IHeap_getStats((xdc_runtime_IHeap_Handle)inst, stats);
}


/*
 * ======== xdc.runtime.System_Module_GateProxy PROXY BODY ========
 */

/* DELEGATES TO ti.sysbios.gates.GateHwi */

/* Module__startupDone__S */
xdc_Bool xdc_runtime_System_Module_GateProxy_Module__startupDone__S(void)
{
    return ti_sysbios_gates_GateHwi_Module__startupDone__S();
}

/* create */
xdc_runtime_System_Module_GateProxy_Handle xdc_runtime_System_Module_GateProxy_create( const xdc_runtime_System_Module_GateProxy_Params *prms, xdc_runtime_Error_Block *eb )
{
    return (xdc_runtime_System_Module_GateProxy_Handle)ti_sysbios_gates_GateHwi_create((const ti_sysbios_gates_GateHwi_Params *)prms, eb);
}

/* delete */
void xdc_runtime_System_Module_GateProxy_delete(xdc_runtime_System_Module_GateProxy_Handle *instp)
{
    ti_sysbios_gates_GateHwi_Object__delete__S(instp);
}

/* Params__init__S */
void xdc_runtime_System_Module_GateProxy_Params__init__S( xdc_Ptr dst, const void *src, xdc_SizeT psz, xdc_SizeT isz )
{
    ti_sysbios_gates_GateHwi_Params__init__S(dst, src, psz, isz);
}

/* Handle__label__S */
xdc_runtime_Types_Label *xdc_runtime_System_Module_GateProxy_Handle__label__S(xdc_Ptr obj, xdc_runtime_Types_Label *lab)
{
    return ti_sysbios_gates_GateHwi_Handle__label__S(obj, lab);
}

/* query__E */
xdc_Bool xdc_runtime_System_Module_GateProxy_query__E( xdc_Int qual )
{
    return ti_sysbios_gates_GateHwi_query(qual);
}

/* enter__E */
xdc_IArg xdc_runtime_System_Module_GateProxy_enter__E( xdc_runtime_System_Module_GateProxy_Handle inst )
{
    return ti_sysbios_gates_GateHwi_enter((ti_sysbios_gates_GateHwi_Handle)inst);
}

/* leave__E */
xdc_Void xdc_runtime_System_Module_GateProxy_leave__E( xdc_runtime_System_Module_GateProxy_Handle inst, xdc_IArg key )
{
    ti_sysbios_gates_GateHwi_leave((ti_sysbios_gates_GateHwi_Handle)inst, key);
}


/*
 * ======== xdc.runtime.System_SupportProxy PROXY BODY ========
 */

/* DELEGATES TO xdc.runtime.SysMin */

/* Module__startupDone__S */
xdc_Bool xdc_runtime_System_SupportProxy_Module__startupDone__S(void)
{
    return xdc_runtime_SysMin_Module__startupDone__S();
}

/* abort__E */
xdc_Void xdc_runtime_System_SupportProxy_abort__E( xdc_CString str )
{
    xdc_runtime_SysMin_abort(str);
}

/* exit__E */
xdc_Void xdc_runtime_System_SupportProxy_exit__E( xdc_Int stat )
{
    xdc_runtime_SysMin_exit(stat);
}

/* flush__E */
xdc_Void xdc_runtime_System_SupportProxy_flush__E( void )
{
    xdc_runtime_SysMin_flush();
}

/* putch__E */
xdc_Void xdc_runtime_System_SupportProxy_putch__E( xdc_Char ch )
{
    xdc_runtime_SysMin_putch(ch);
}

/* ready__E */
xdc_Bool xdc_runtime_System_SupportProxy_ready__E( void )
{
    return xdc_runtime_SysMin_ready();
}


/*
 * ======== xdc.runtime.Timestamp_SupportProxy PROXY BODY ========
 */

/* DELEGATES TO ti.sysbios.family.c7x.TimestampProvider */

/* Module__startupDone__S */
xdc_Bool xdc_runtime_Timestamp_SupportProxy_Module__startupDone__S(void)
{
    return ti_sysbios_family_c7x_TimestampProvider_Module__startupDone__S();
}

/* get32__E */
xdc_Bits32 xdc_runtime_Timestamp_SupportProxy_get32__E( void )
{
    return ti_sysbios_family_c7x_TimestampProvider_get32();
}

/* get64__E */
xdc_Void xdc_runtime_Timestamp_SupportProxy_get64__E( xdc_runtime_Types_Timestamp64 *result )
{
    ti_sysbios_family_c7x_TimestampProvider_get64(result);
}

/* getFreq__E */
xdc_Void xdc_runtime_Timestamp_SupportProxy_getFreq__E( xdc_runtime_Types_FreqHz *freq )
{
    ti_sysbios_family_c7x_TimestampProvider_getFreq(freq);
}


/*
 * ======== xdc.runtime.knl.GateH_Proxy PROXY BODY ========
 */

/* DELEGATES TO xdc.runtime.knl.GateThread */

/* Module__startupDone__S */
xdc_Bool xdc_runtime_knl_GateH_Proxy_Module__startupDone__S(void)
{
    return xdc_runtime_knl_GateThread_Module__startupDone__S();
}

/* create */
xdc_runtime_knl_GateH_Proxy_Handle xdc_runtime_knl_GateH_Proxy_create( const xdc_runtime_knl_GateH_Proxy_Params *prms, xdc_runtime_Error_Block *eb )
{
    return (xdc_runtime_knl_GateH_Proxy_Handle)xdc_runtime_knl_GateThread_create((const xdc_runtime_knl_GateThread_Params *)prms, eb);
}

/* delete */
void xdc_runtime_knl_GateH_Proxy_delete(xdc_runtime_knl_GateH_Proxy_Handle *instp)
{
    xdc_runtime_knl_GateThread_Object__delete__S(instp);
}

/* Params__init__S */
void xdc_runtime_knl_GateH_Proxy_Params__init__S( xdc_Ptr dst, const void *src, xdc_SizeT psz, xdc_SizeT isz )
{
    xdc_runtime_knl_GateThread_Params__init__S(dst, src, psz, isz);
}

/* Handle__label__S */
xdc_runtime_Types_Label *xdc_runtime_knl_GateH_Proxy_Handle__label__S(xdc_Ptr obj, xdc_runtime_Types_Label *lab)
{
    return xdc_runtime_knl_GateThread_Handle__label__S(obj, lab);
}

/* query__E */
xdc_Bool xdc_runtime_knl_GateH_Proxy_query__E( xdc_Int qual )
{
    return xdc_runtime_knl_GateThread_query(qual);
}

/* enter__E */
xdc_IArg xdc_runtime_knl_GateH_Proxy_enter__E( xdc_runtime_knl_GateH_Proxy_Handle inst )
{
    return xdc_runtime_IGateProvider_enter((xdc_runtime_IGateProvider_Handle)inst);
}

/* leave__E */
xdc_Void xdc_runtime_knl_GateH_Proxy_leave__E( xdc_runtime_knl_GateH_Proxy_Handle inst, xdc_IArg key )
{
    xdc_runtime_IGateProvider_leave((xdc_runtime_IGateProvider_Handle)inst, key);
}


/*
 * ======== xdc.runtime.knl.GateThread_Proxy PROXY BODY ========
 */

/* DELEGATES TO ti.sysbios.xdcruntime.GateThreadSupport */

/* Module__startupDone__S */
xdc_Bool xdc_runtime_knl_GateThread_Proxy_Module__startupDone__S(void)
{
    return ti_sysbios_xdcruntime_GateThreadSupport_Module__startupDone__S();
}

/* create */
xdc_runtime_knl_GateThread_Proxy_Handle xdc_runtime_knl_GateThread_Proxy_create( const xdc_runtime_knl_GateThread_Proxy_Params *prms, xdc_runtime_Error_Block *eb )
{
    return (xdc_runtime_knl_GateThread_Proxy_Handle)ti_sysbios_xdcruntime_GateThreadSupport_create((const ti_sysbios_xdcruntime_GateThreadSupport_Params *)prms, eb);
}

/* delete */
void xdc_runtime_knl_GateThread_Proxy_delete(xdc_runtime_knl_GateThread_Proxy_Handle *instp)
{
    ti_sysbios_xdcruntime_GateThreadSupport_Object__delete__S(instp);
}

/* Params__init__S */
void xdc_runtime_knl_GateThread_Proxy_Params__init__S( xdc_Ptr dst, const void *src, xdc_SizeT psz, xdc_SizeT isz )
{
    ti_sysbios_xdcruntime_GateThreadSupport_Params__init__S(dst, src, psz, isz);
}

/* Handle__label__S */
xdc_runtime_Types_Label *xdc_runtime_knl_GateThread_Proxy_Handle__label__S(xdc_Ptr obj, xdc_runtime_Types_Label *lab)
{
    return ti_sysbios_xdcruntime_GateThreadSupport_Handle__label__S(obj, lab);
}

/* query__E */
xdc_Bool xdc_runtime_knl_GateThread_Proxy_query__E( xdc_Int qual )
{
    return ti_sysbios_xdcruntime_GateThreadSupport_query(qual);
}

/* enter__E */
xdc_IArg xdc_runtime_knl_GateThread_Proxy_enter__E( xdc_runtime_knl_GateThread_Proxy_Handle inst )
{
    return ti_sysbios_xdcruntime_GateThreadSupport_enter((ti_sysbios_xdcruntime_GateThreadSupport_Handle)inst);
}

/* leave__E */
xdc_Void xdc_runtime_knl_GateThread_Proxy_leave__E( xdc_runtime_knl_GateThread_Proxy_Handle inst, xdc_IArg key )
{
    ti_sysbios_xdcruntime_GateThreadSupport_leave((ti_sysbios_xdcruntime_GateThreadSupport_Handle)inst, key);
}


/*
 * ======== ti.sysbios.family.c7x.Hwi OBJECT DESCRIPTOR ========
 */

/* Object__DESC__C */
typedef struct { ti_sysbios_family_c7x_Hwi_Object2__ s0; char c; } ti_sysbios_family_c7x_Hwi___S1;
#pragma DATA_SECTION(ti_sysbios_family_c7x_Hwi_Object__DESC__C, ".const:ti_sysbios_family_c7x_Hwi_Object__DESC__C");
__FAR__ const xdc_runtime_Core_ObjDesc ti_sysbios_family_c7x_Hwi_Object__DESC__C = {
    (xdc_CPtr)NULL, /* fxnTab */
    &ti_sysbios_family_c7x_Hwi_Module__root__V.link, /* modLink */
    sizeof(ti_sysbios_family_c7x_Hwi___S1) - sizeof(ti_sysbios_family_c7x_Hwi_Object2__), /* objAlign */
    0, /* objHeap */
    0, /* objName */
    sizeof(ti_sysbios_family_c7x_Hwi_Object2__), /* objSize */
    (xdc_CPtr)&ti_sysbios_family_c7x_Hwi_Object__PARAMS__C, /* prmsInit */
    sizeof(ti_sysbios_family_c7x_Hwi_Params), /* prmsSize */
};


/*
 * ======== ti.sysbios.gates.GateHwi OBJECT DESCRIPTOR ========
 */

/* Object__DESC__C */
typedef struct { ti_sysbios_gates_GateHwi_Object2__ s0; char c; } ti_sysbios_gates_GateHwi___S1;
#pragma DATA_SECTION(ti_sysbios_gates_GateHwi_Object__DESC__C, ".const:ti_sysbios_gates_GateHwi_Object__DESC__C");
__FAR__ const xdc_runtime_Core_ObjDesc ti_sysbios_gates_GateHwi_Object__DESC__C = {
    (xdc_CPtr)&ti_sysbios_gates_GateHwi_Module__FXNS__C, /* fxnTab */
    &ti_sysbios_gates_GateHwi_Module__root__V.link, /* modLink */
    sizeof(ti_sysbios_gates_GateHwi___S1) - sizeof(ti_sysbios_gates_GateHwi_Object2__), /* objAlign */
    0, /* objHeap */
    0, /* objName */
    sizeof(ti_sysbios_gates_GateHwi_Object2__), /* objSize */
    (xdc_CPtr)&ti_sysbios_gates_GateHwi_Object__PARAMS__C, /* prmsInit */
    sizeof(ti_sysbios_gates_GateHwi_Params), /* prmsSize */
};


/*
 * ======== ti.sysbios.gates.GateMutex OBJECT DESCRIPTOR ========
 */

/* Object__DESC__C */
typedef struct { ti_sysbios_gates_GateMutex_Object2__ s0; char c; } ti_sysbios_gates_GateMutex___S1;
#pragma DATA_SECTION(ti_sysbios_gates_GateMutex_Object__DESC__C, ".const:ti_sysbios_gates_GateMutex_Object__DESC__C");
__FAR__ const xdc_runtime_Core_ObjDesc ti_sysbios_gates_GateMutex_Object__DESC__C = {
    (xdc_CPtr)&ti_sysbios_gates_GateMutex_Module__FXNS__C, /* fxnTab */
    &ti_sysbios_gates_GateMutex_Module__root__V.link, /* modLink */
    sizeof(ti_sysbios_gates_GateMutex___S1) - sizeof(ti_sysbios_gates_GateMutex_Object2__), /* objAlign */
    0, /* objHeap */
    0, /* objName */
    sizeof(ti_sysbios_gates_GateMutex_Object2__), /* objSize */
    (xdc_CPtr)&ti_sysbios_gates_GateMutex_Object__PARAMS__C, /* prmsInit */
    sizeof(ti_sysbios_gates_GateMutex_Params), /* prmsSize */
};


/*
 * ======== ti.sysbios.gates.GateMutexPri OBJECT DESCRIPTOR ========
 */

/* Object__DESC__C */
typedef struct { ti_sysbios_gates_GateMutexPri_Object2__ s0; char c; } ti_sysbios_gates_GateMutexPri___S1;
#pragma DATA_SECTION(ti_sysbios_gates_GateMutexPri_Object__DESC__C, ".const:ti_sysbios_gates_GateMutexPri_Object__DESC__C");
__FAR__ const xdc_runtime_Core_ObjDesc ti_sysbios_gates_GateMutexPri_Object__DESC__C = {
    (xdc_CPtr)&ti_sysbios_gates_GateMutexPri_Module__FXNS__C, /* fxnTab */
    &ti_sysbios_gates_GateMutexPri_Module__root__V.link, /* modLink */
    sizeof(ti_sysbios_gates_GateMutexPri___S1) - sizeof(ti_sysbios_gates_GateMutexPri_Object2__), /* objAlign */
    0, /* objHeap */
    0, /* objName */
    sizeof(ti_sysbios_gates_GateMutexPri_Object2__), /* objSize */
    (xdc_CPtr)&ti_sysbios_gates_GateMutexPri_Object__PARAMS__C, /* prmsInit */
    sizeof(ti_sysbios_gates_GateMutexPri_Params), /* prmsSize */
};


/*
 * ======== ti.sysbios.gates.GateSwi OBJECT DESCRIPTOR ========
 */

/* Object__DESC__C */
typedef struct { ti_sysbios_gates_GateSwi_Object2__ s0; char c; } ti_sysbios_gates_GateSwi___S1;
#pragma DATA_SECTION(ti_sysbios_gates_GateSwi_Object__DESC__C, ".const:ti_sysbios_gates_GateSwi_Object__DESC__C");
__FAR__ const xdc_runtime_Core_ObjDesc ti_sysbios_gates_GateSwi_Object__DESC__C = {
    (xdc_CPtr)&ti_sysbios_gates_GateSwi_Module__FXNS__C, /* fxnTab */
    &ti_sysbios_gates_GateSwi_Module__root__V.link, /* modLink */
    sizeof(ti_sysbios_gates_GateSwi___S1) - sizeof(ti_sysbios_gates_GateSwi_Object2__), /* objAlign */
    0, /* objHeap */
    0, /* objName */
    sizeof(ti_sysbios_gates_GateSwi_Object2__), /* objSize */
    (xdc_CPtr)&ti_sysbios_gates_GateSwi_Object__PARAMS__C, /* prmsInit */
    sizeof(ti_sysbios_gates_GateSwi_Params), /* prmsSize */
};


/*
 * ======== ti.sysbios.gates.GateTask OBJECT DESCRIPTOR ========
 */

/* Object__DESC__C */
typedef struct { ti_sysbios_gates_GateTask_Object2__ s0; char c; } ti_sysbios_gates_GateTask___S1;
#pragma DATA_SECTION(ti_sysbios_gates_GateTask_Object__DESC__C, ".const:ti_sysbios_gates_GateTask_Object__DESC__C");
__FAR__ const xdc_runtime_Core_ObjDesc ti_sysbios_gates_GateTask_Object__DESC__C = {
    (xdc_CPtr)&ti_sysbios_gates_GateTask_Module__FXNS__C, /* fxnTab */
    &ti_sysbios_gates_GateTask_Module__root__V.link, /* modLink */
    sizeof(ti_sysbios_gates_GateTask___S1) - sizeof(ti_sysbios_gates_GateTask_Object2__), /* objAlign */
    0, /* objHeap */
    0, /* objName */
    sizeof(ti_sysbios_gates_GateTask_Object2__), /* objSize */
    (xdc_CPtr)&ti_sysbios_gates_GateTask_Object__PARAMS__C, /* prmsInit */
    sizeof(ti_sysbios_gates_GateTask_Params), /* prmsSize */
};


/*
 * ======== ti.sysbios.hal.Hwi OBJECT DESCRIPTOR ========
 */

/* Object__DESC__C */
typedef struct { ti_sysbios_hal_Hwi_Object2__ s0; char c; } ti_sysbios_hal_Hwi___S1;
#pragma DATA_SECTION(ti_sysbios_hal_Hwi_Object__DESC__C, ".const:ti_sysbios_hal_Hwi_Object__DESC__C");
__FAR__ const xdc_runtime_Core_ObjDesc ti_sysbios_hal_Hwi_Object__DESC__C = {
    (xdc_CPtr)NULL, /* fxnTab */
    &ti_sysbios_hal_Hwi_Module__root__V.link, /* modLink */
    sizeof(ti_sysbios_hal_Hwi___S1) - sizeof(ti_sysbios_hal_Hwi_Object2__), /* objAlign */
    0, /* objHeap */
    0, /* objName */
    sizeof(ti_sysbios_hal_Hwi_Object2__), /* objSize */
    (xdc_CPtr)&ti_sysbios_hal_Hwi_Object__PARAMS__C, /* prmsInit */
    sizeof(ti_sysbios_hal_Hwi_Params), /* prmsSize */
};


/*
 * ======== ti.sysbios.heaps.HeapBuf OBJECT DESCRIPTOR ========
 */

/* Object__DESC__C */
typedef struct { ti_sysbios_heaps_HeapBuf_Object2__ s0; char c; } ti_sysbios_heaps_HeapBuf___S1;
#pragma DATA_SECTION(ti_sysbios_heaps_HeapBuf_Object__DESC__C, ".const:ti_sysbios_heaps_HeapBuf_Object__DESC__C");
__FAR__ const xdc_runtime_Core_ObjDesc ti_sysbios_heaps_HeapBuf_Object__DESC__C = {
    (xdc_CPtr)&ti_sysbios_heaps_HeapBuf_Module__FXNS__C, /* fxnTab */
    &ti_sysbios_heaps_HeapBuf_Module__root__V.link, /* modLink */
    sizeof(ti_sysbios_heaps_HeapBuf___S1) - sizeof(ti_sysbios_heaps_HeapBuf_Object2__), /* objAlign */
    0, /* objHeap */
    0, /* objName */
    sizeof(ti_sysbios_heaps_HeapBuf_Object2__), /* objSize */
    (xdc_CPtr)&ti_sysbios_heaps_HeapBuf_Object__PARAMS__C, /* prmsInit */
    sizeof(ti_sysbios_heaps_HeapBuf_Params), /* prmsSize */
};


/*
 * ======== ti.sysbios.heaps.HeapMem OBJECT DESCRIPTOR ========
 */

/* Object__DESC__C */
typedef struct { ti_sysbios_heaps_HeapMem_Object2__ s0; char c; } ti_sysbios_heaps_HeapMem___S1;
#pragma DATA_SECTION(ti_sysbios_heaps_HeapMem_Object__DESC__C, ".const:ti_sysbios_heaps_HeapMem_Object__DESC__C");
__FAR__ const xdc_runtime_Core_ObjDesc ti_sysbios_heaps_HeapMem_Object__DESC__C = {
    (xdc_CPtr)&ti_sysbios_heaps_HeapMem_Module__FXNS__C, /* fxnTab */
    &ti_sysbios_heaps_HeapMem_Module__root__V.link, /* modLink */
    sizeof(ti_sysbios_heaps_HeapMem___S1) - sizeof(ti_sysbios_heaps_HeapMem_Object2__), /* objAlign */
    0, /* objHeap */
    0, /* objName */
    sizeof(ti_sysbios_heaps_HeapMem_Object2__), /* objSize */
    (xdc_CPtr)&ti_sysbios_heaps_HeapMem_Object__PARAMS__C, /* prmsInit */
    sizeof(ti_sysbios_heaps_HeapMem_Params), /* prmsSize */
};


/*
 * ======== ti.sysbios.knl.Clock OBJECT DESCRIPTOR ========
 */

/* Object__DESC__C */
typedef struct { ti_sysbios_knl_Clock_Object2__ s0; char c; } ti_sysbios_knl_Clock___S1;
#pragma DATA_SECTION(ti_sysbios_knl_Clock_Object__DESC__C, ".const:ti_sysbios_knl_Clock_Object__DESC__C");
__FAR__ const xdc_runtime_Core_ObjDesc ti_sysbios_knl_Clock_Object__DESC__C = {
    (xdc_CPtr)-1, /* fxnTab */
    &ti_sysbios_knl_Clock_Module__root__V.link, /* modLink */
    sizeof(ti_sysbios_knl_Clock___S1) - sizeof(ti_sysbios_knl_Clock_Object2__), /* objAlign */
    0, /* objHeap */
    0, /* objName */
    sizeof(ti_sysbios_knl_Clock_Object2__), /* objSize */
    (xdc_CPtr)&ti_sysbios_knl_Clock_Object__PARAMS__C, /* prmsInit */
    sizeof(ti_sysbios_knl_Clock_Params), /* prmsSize */
};


/*
 * ======== ti.sysbios.knl.Event OBJECT DESCRIPTOR ========
 */

/* Object__DESC__C */
typedef struct { ti_sysbios_knl_Event_Object2__ s0; char c; } ti_sysbios_knl_Event___S1;
#pragma DATA_SECTION(ti_sysbios_knl_Event_Object__DESC__C, ".const:ti_sysbios_knl_Event_Object__DESC__C");
__FAR__ const xdc_runtime_Core_ObjDesc ti_sysbios_knl_Event_Object__DESC__C = {
    (xdc_CPtr)-1, /* fxnTab */
    &ti_sysbios_knl_Event_Module__root__V.link, /* modLink */
    sizeof(ti_sysbios_knl_Event___S1) - sizeof(ti_sysbios_knl_Event_Object2__), /* objAlign */
    0, /* objHeap */
    0, /* objName */
    sizeof(ti_sysbios_knl_Event_Object2__), /* objSize */
    (xdc_CPtr)&ti_sysbios_knl_Event_Object__PARAMS__C, /* prmsInit */
    sizeof(ti_sysbios_knl_Event_Params), /* prmsSize */
};


/*
 * ======== ti.sysbios.knl.Queue OBJECT DESCRIPTOR ========
 */

/* Object__DESC__C */
typedef struct { ti_sysbios_knl_Queue_Object2__ s0; char c; } ti_sysbios_knl_Queue___S1;
#pragma DATA_SECTION(ti_sysbios_knl_Queue_Object__DESC__C, ".const:ti_sysbios_knl_Queue_Object__DESC__C");
__FAR__ const xdc_runtime_Core_ObjDesc ti_sysbios_knl_Queue_Object__DESC__C = {
    (xdc_CPtr)-1, /* fxnTab */
    &ti_sysbios_knl_Queue_Module__root__V.link, /* modLink */
    sizeof(ti_sysbios_knl_Queue___S1) - sizeof(ti_sysbios_knl_Queue_Object2__), /* objAlign */
    0, /* objHeap */
    0, /* objName */
    sizeof(ti_sysbios_knl_Queue_Object2__), /* objSize */
    (xdc_CPtr)&ti_sysbios_knl_Queue_Object__PARAMS__C, /* prmsInit */
    sizeof(ti_sysbios_knl_Queue_Params), /* prmsSize */
};


/*
 * ======== ti.sysbios.knl.Semaphore OBJECT DESCRIPTOR ========
 */

/* Object__DESC__C */
typedef struct { ti_sysbios_knl_Semaphore_Object2__ s0; char c; } ti_sysbios_knl_Semaphore___S1;
#pragma DATA_SECTION(ti_sysbios_knl_Semaphore_Object__DESC__C, ".const:ti_sysbios_knl_Semaphore_Object__DESC__C");
__FAR__ const xdc_runtime_Core_ObjDesc ti_sysbios_knl_Semaphore_Object__DESC__C = {
    (xdc_CPtr)-1, /* fxnTab */
    &ti_sysbios_knl_Semaphore_Module__root__V.link, /* modLink */
    sizeof(ti_sysbios_knl_Semaphore___S1) - sizeof(ti_sysbios_knl_Semaphore_Object2__), /* objAlign */
    0, /* objHeap */
    0, /* objName */
    sizeof(ti_sysbios_knl_Semaphore_Object2__), /* objSize */
    (xdc_CPtr)&ti_sysbios_knl_Semaphore_Object__PARAMS__C, /* prmsInit */
    sizeof(ti_sysbios_knl_Semaphore_Params), /* prmsSize */
};


/*
 * ======== ti.sysbios.knl.Swi OBJECT DESCRIPTOR ========
 */

/* Object__DESC__C */
typedef struct { ti_sysbios_knl_Swi_Object2__ s0; char c; } ti_sysbios_knl_Swi___S1;
#pragma DATA_SECTION(ti_sysbios_knl_Swi_Object__DESC__C, ".const:ti_sysbios_knl_Swi_Object__DESC__C");
__FAR__ const xdc_runtime_Core_ObjDesc ti_sysbios_knl_Swi_Object__DESC__C = {
    (xdc_CPtr)-1, /* fxnTab */
    &ti_sysbios_knl_Swi_Module__root__V.link, /* modLink */
    sizeof(ti_sysbios_knl_Swi___S1) - sizeof(ti_sysbios_knl_Swi_Object2__), /* objAlign */
    0, /* objHeap */
    0, /* objName */
    sizeof(ti_sysbios_knl_Swi_Object2__), /* objSize */
    (xdc_CPtr)&ti_sysbios_knl_Swi_Object__PARAMS__C, /* prmsInit */
    sizeof(ti_sysbios_knl_Swi_Params), /* prmsSize */
};


/*
 * ======== ti.sysbios.knl.Task OBJECT DESCRIPTOR ========
 */

/* Object__DESC__C */
typedef struct { ti_sysbios_knl_Task_Object2__ s0; char c; } ti_sysbios_knl_Task___S1;
#pragma DATA_SECTION(ti_sysbios_knl_Task_Object__DESC__C, ".const:ti_sysbios_knl_Task_Object__DESC__C");
__FAR__ const xdc_runtime_Core_ObjDesc ti_sysbios_knl_Task_Object__DESC__C = {
    (xdc_CPtr)-1, /* fxnTab */
    &ti_sysbios_knl_Task_Module__root__V.link, /* modLink */
    sizeof(ti_sysbios_knl_Task___S1) - sizeof(ti_sysbios_knl_Task_Object2__), /* objAlign */
    0, /* objHeap */
    0, /* objName */
    sizeof(ti_sysbios_knl_Task_Object2__), /* objSize */
    (xdc_CPtr)&ti_sysbios_knl_Task_Object__PARAMS__C, /* prmsInit */
    sizeof(ti_sysbios_knl_Task_Params), /* prmsSize */
};


/*
 * ======== ti.sysbios.syncs.SyncSem OBJECT DESCRIPTOR ========
 */

/* Object__DESC__C */
typedef struct { ti_sysbios_syncs_SyncSem_Object2__ s0; char c; } ti_sysbios_syncs_SyncSem___S1;
#pragma DATA_SECTION(ti_sysbios_syncs_SyncSem_Object__DESC__C, ".const:ti_sysbios_syncs_SyncSem_Object__DESC__C");
__FAR__ const xdc_runtime_Core_ObjDesc ti_sysbios_syncs_SyncSem_Object__DESC__C = {
    (xdc_CPtr)&ti_sysbios_syncs_SyncSem_Module__FXNS__C, /* fxnTab */
    &ti_sysbios_syncs_SyncSem_Module__root__V.link, /* modLink */
    sizeof(ti_sysbios_syncs_SyncSem___S1) - sizeof(ti_sysbios_syncs_SyncSem_Object2__), /* objAlign */
    0, /* objHeap */
    0, /* objName */
    sizeof(ti_sysbios_syncs_SyncSem_Object2__), /* objSize */
    (xdc_CPtr)&ti_sysbios_syncs_SyncSem_Object__PARAMS__C, /* prmsInit */
    sizeof(ti_sysbios_syncs_SyncSem_Params), /* prmsSize */
};


/*
 * ======== ti.sysbios.timers.dmtimer.Timer OBJECT DESCRIPTOR ========
 */

/* Object__DESC__C */
typedef struct { ti_sysbios_timers_dmtimer_Timer_Object2__ s0; char c; } ti_sysbios_timers_dmtimer_Timer___S1;
#pragma DATA_SECTION(ti_sysbios_timers_dmtimer_Timer_Object__DESC__C, ".const:ti_sysbios_timers_dmtimer_Timer_Object__DESC__C");
__FAR__ const xdc_runtime_Core_ObjDesc ti_sysbios_timers_dmtimer_Timer_Object__DESC__C = {
    (xdc_CPtr)NULL, /* fxnTab */
    &ti_sysbios_timers_dmtimer_Timer_Module__root__V.link, /* modLink */
    sizeof(ti_sysbios_timers_dmtimer_Timer___S1) - sizeof(ti_sysbios_timers_dmtimer_Timer_Object2__), /* objAlign */
    0, /* objHeap */
    0, /* objName */
    sizeof(ti_sysbios_timers_dmtimer_Timer_Object2__), /* objSize */
    (xdc_CPtr)&ti_sysbios_timers_dmtimer_Timer_Object__PARAMS__C, /* prmsInit */
    sizeof(ti_sysbios_timers_dmtimer_Timer_Params), /* prmsSize */
};


/*
 * ======== ti.sysbios.xdcruntime.GateThreadSupport OBJECT DESCRIPTOR ========
 */

/* Object__DESC__C */
typedef struct { ti_sysbios_xdcruntime_GateThreadSupport_Object2__ s0; char c; } ti_sysbios_xdcruntime_GateThreadSupport___S1;
#pragma DATA_SECTION(ti_sysbios_xdcruntime_GateThreadSupport_Object__DESC__C, ".const:ti_sysbios_xdcruntime_GateThreadSupport_Object__DESC__C");
__FAR__ const xdc_runtime_Core_ObjDesc ti_sysbios_xdcruntime_GateThreadSupport_Object__DESC__C = {
    (xdc_CPtr)NULL, /* fxnTab */
    &ti_sysbios_xdcruntime_GateThreadSupport_Module__root__V.link, /* modLink */
    sizeof(ti_sysbios_xdcruntime_GateThreadSupport___S1) - sizeof(ti_sysbios_xdcruntime_GateThreadSupport_Object2__), /* objAlign */
    0, /* objHeap */
    0, /* objName */
    sizeof(ti_sysbios_xdcruntime_GateThreadSupport_Object2__), /* objSize */
    (xdc_CPtr)&ti_sysbios_xdcruntime_GateThreadSupport_Object__PARAMS__C, /* prmsInit */
    sizeof(ti_sysbios_xdcruntime_GateThreadSupport_Params), /* prmsSize */
};


/*
 * ======== xdc.runtime.knl.GateThread OBJECT DESCRIPTOR ========
 */

/* Object__DESC__C */
typedef struct { xdc_runtime_knl_GateThread_Object2__ s0; char c; } xdc_runtime_knl_GateThread___S1;
#pragma DATA_SECTION(xdc_runtime_knl_GateThread_Object__DESC__C, ".const:xdc_runtime_knl_GateThread_Object__DESC__C");
__FAR__ const xdc_runtime_Core_ObjDesc xdc_runtime_knl_GateThread_Object__DESC__C = {
    (xdc_CPtr)&xdc_runtime_knl_GateThread_Module__FXNS__C, /* fxnTab */
    &xdc_runtime_knl_GateThread_Module__root__V.link, /* modLink */
    sizeof(xdc_runtime_knl_GateThread___S1) - sizeof(xdc_runtime_knl_GateThread_Object2__), /* objAlign */
    0, /* objHeap */
    0, /* objName */
    sizeof(xdc_runtime_knl_GateThread_Object2__), /* objSize */
    (xdc_CPtr)&xdc_runtime_knl_GateThread_Object__PARAMS__C, /* prmsInit */
    sizeof(xdc_runtime_knl_GateThread_Params), /* prmsSize */
};


/*
 * ======== xdc.runtime.knl.ISync VIRTUAL FUNCTIONS ========
 */

/* create */
xdc_runtime_knl_ISync_Handle xdc_runtime_knl_ISync_create( xdc_runtime_knl_ISync_Module mod, const xdc_runtime_knl_ISync_Params *prms, xdc_runtime_Error_Block *eb )
{
    return (xdc_runtime_knl_ISync_Handle) mod->__sysp->__create(0, (const xdc_UChar*)prms, sizeof (xdc_runtime_knl_ISync_Params), eb);
}

/* delete */
void xdc_runtime_knl_ISync_delete( xdc_runtime_knl_ISync_Handle *instp )
{
    (*instp)->__fxns->__sysp->__delete(instp);
}


/*
 * ======== xdc.runtime.IGateProvider VIRTUAL FUNCTIONS ========
 */

/* create */
xdc_runtime_IGateProvider_Handle xdc_runtime_IGateProvider_create( xdc_runtime_IGateProvider_Module mod, const xdc_runtime_IGateProvider_Params *prms, xdc_runtime_Error_Block *eb )
{
    return (xdc_runtime_IGateProvider_Handle) mod->__sysp->__create(0, (const xdc_UChar*)prms, sizeof (xdc_runtime_IGateProvider_Params), eb);
}

/* delete */
void xdc_runtime_IGateProvider_delete( xdc_runtime_IGateProvider_Handle *instp )
{
    (*instp)->__fxns->__sysp->__delete(instp);
}


/*
 * ======== xdc.runtime.IHeap VIRTUAL FUNCTIONS ========
 */

/* create */
xdc_runtime_IHeap_Handle xdc_runtime_IHeap_create( xdc_runtime_IHeap_Module mod, const xdc_runtime_IHeap_Params *prms, xdc_runtime_Error_Block *eb )
{
    return (xdc_runtime_IHeap_Handle) mod->__sysp->__create(0, (const xdc_UChar*)prms, sizeof (xdc_runtime_IHeap_Params), eb);
}

/* delete */
void xdc_runtime_IHeap_delete( xdc_runtime_IHeap_Handle *instp )
{
    (*instp)->__fxns->__sysp->__delete(instp);
}


/*
 * ======== ti.sysbios.BIOS SYSTEM FUNCTIONS ========
 */

/* Module__startupDone__S */
xdc_Bool ti_sysbios_BIOS_Module__startupDone__S( void )
{
    return 1;
}



/*
 * ======== ti.sysbios.BIOS_RtsGateProxy SYSTEM FUNCTIONS ========
 */

/* per-module runtime symbols */
#undef Module__MID
#define Module__MID ti_sysbios_BIOS_RtsGateProxy_Module__id__C

#undef Module__DGSINCL
#define Module__DGSINCL ti_sysbios_BIOS_RtsGateProxy_Module__diagsIncluded__C

#undef Module__DGSENAB
#define Module__DGSENAB ti_sysbios_BIOS_RtsGateProxy_Module__diagsEnabled__C

#undef Module__DGSMASK
#define Module__DGSMASK ti_sysbios_BIOS_RtsGateProxy_Module__diagsMask__C

#undef Module__LOGDEF
#define Module__LOGDEF ti_sysbios_BIOS_RtsGateProxy_Module__loggerDefined__C

#undef Module__LOGOBJ
#define Module__LOGOBJ ti_sysbios_BIOS_RtsGateProxy_Module__loggerObj__C

#undef Module__LOGFXN0
#define Module__LOGFXN0 ti_sysbios_BIOS_RtsGateProxy_Module__loggerFxn0__C

#undef Module__LOGFXN1
#define Module__LOGFXN1 ti_sysbios_BIOS_RtsGateProxy_Module__loggerFxn1__C

#undef Module__LOGFXN2
#define Module__LOGFXN2 ti_sysbios_BIOS_RtsGateProxy_Module__loggerFxn2__C

#undef Module__LOGFXN4
#define Module__LOGFXN4 ti_sysbios_BIOS_RtsGateProxy_Module__loggerFxn4__C

#undef Module__LOGFXN8
#define Module__LOGFXN8 ti_sysbios_BIOS_RtsGateProxy_Module__loggerFxn8__C

#undef Module__G_OBJ
#define Module__G_OBJ ti_sysbios_BIOS_RtsGateProxy_Module__gateObj__C

#undef Module__G_PRMS
#define Module__G_PRMS ti_sysbios_BIOS_RtsGateProxy_Module__gatePrms__C

#undef Module__GP_create
#define Module__GP_create ti_sysbios_BIOS_RtsGateProxy_Module_GateProxy_create
#undef Module__GP_delete
#define Module__GP_delete ti_sysbios_BIOS_RtsGateProxy_Module_GateProxy_delete
#undef Module__GP_enter
#define Module__GP_enter ti_sysbios_BIOS_RtsGateProxy_Module_GateProxy_enter
#undef Module__GP_leave
#define Module__GP_leave ti_sysbios_BIOS_RtsGateProxy_Module_GateProxy_leave
#undef Module__GP_query
#define Module__GP_query ti_sysbios_BIOS_RtsGateProxy_Module_GateProxy_query

xdc_Bool ti_sysbios_BIOS_RtsGateProxy_Proxy__abstract__S(void)
{
    return 0;
}
xdc_CPtr ti_sysbios_BIOS_RtsGateProxy_Proxy__delegate__S(void)
{
    return (const void *)&ti_sysbios_gates_GateMutex_Module__FXNS__C;
}



/*
 * ======== ti.sysbios.family.c64p.tci6488.TimerSupport SYSTEM FUNCTIONS ========
 */

/* Module__startupDone__S */
xdc_Bool ti_sysbios_family_c64p_tci6488_TimerSupport_Module__startupDone__S( void )
{
    return 1;
}



/*
 * ======== ti.sysbios.family.c7x.Cache SYSTEM FUNCTIONS ========
 */

/* Module__startupDone__S */
xdc_Bool ti_sysbios_family_c7x_Cache_Module__startupDone__S( void )
{
    return ti_sysbios_family_c7x_Cache_Module__startupDone__F();
}



/*
 * ======== ti.sysbios.family.c7x.Exception SYSTEM FUNCTIONS ========
 */

/* Module__startupDone__S */
xdc_Bool ti_sysbios_family_c7x_Exception_Module__startupDone__S( void )
{
    return ti_sysbios_family_c7x_Exception_Module__startupDone__F();
}



/*
 * ======== ti.sysbios.family.c7x.Hwi SYSTEM FUNCTIONS ========
 */

/* per-module runtime symbols */
#undef Module__MID
#define Module__MID ti_sysbios_family_c7x_Hwi_Module__id__C

#undef Module__DGSINCL
#define Module__DGSINCL ti_sysbios_family_c7x_Hwi_Module__diagsIncluded__C

#undef Module__DGSENAB
#define Module__DGSENAB ti_sysbios_family_c7x_Hwi_Module__diagsEnabled__C

#undef Module__DGSMASK
#define Module__DGSMASK ti_sysbios_family_c7x_Hwi_Module__diagsMask__C

#undef Module__LOGDEF
#define Module__LOGDEF ti_sysbios_family_c7x_Hwi_Module__loggerDefined__C

#undef Module__LOGOBJ
#define Module__LOGOBJ ti_sysbios_family_c7x_Hwi_Module__loggerObj__C

#undef Module__LOGFXN0
#define Module__LOGFXN0 ti_sysbios_family_c7x_Hwi_Module__loggerFxn0__C

#undef Module__LOGFXN1
#define Module__LOGFXN1 ti_sysbios_family_c7x_Hwi_Module__loggerFxn1__C

#undef Module__LOGFXN2
#define Module__LOGFXN2 ti_sysbios_family_c7x_Hwi_Module__loggerFxn2__C

#undef Module__LOGFXN4
#define Module__LOGFXN4 ti_sysbios_family_c7x_Hwi_Module__loggerFxn4__C

#undef Module__LOGFXN8
#define Module__LOGFXN8 ti_sysbios_family_c7x_Hwi_Module__loggerFxn8__C

#undef Module__G_OBJ
#define Module__G_OBJ ti_sysbios_family_c7x_Hwi_Module__gateObj__C

#undef Module__G_PRMS
#define Module__G_PRMS ti_sysbios_family_c7x_Hwi_Module__gatePrms__C

#undef Module__GP_create
#define Module__GP_create ti_sysbios_family_c7x_Hwi_Module_GateProxy_create
#undef Module__GP_delete
#define Module__GP_delete ti_sysbios_family_c7x_Hwi_Module_GateProxy_delete
#undef Module__GP_enter
#define Module__GP_enter ti_sysbios_family_c7x_Hwi_Module_GateProxy_enter
#undef Module__GP_leave
#define Module__GP_leave ti_sysbios_family_c7x_Hwi_Module_GateProxy_leave
#undef Module__GP_query
#define Module__GP_query ti_sysbios_family_c7x_Hwi_Module_GateProxy_query

/* Module__startupDone__S */
xdc_Bool ti_sysbios_family_c7x_Hwi_Module__startupDone__S( void )
{
    return ti_sysbios_family_c7x_Hwi_Module__startupDone__F();
}

/* Handle__label__S */
xdc_runtime_Types_Label *ti_sysbios_family_c7x_Hwi_Handle__label__S(xdc_Ptr obj, xdc_runtime_Types_Label *lab) 
{
    lab->handle = obj;
    lab->modId = 32793;
    lab->named = FALSE;
    lab->iname = xdc_runtime_Text_nameUnknown;

    return lab;
}

/* Params__init__S */
xdc_Void ti_sysbios_family_c7x_Hwi_Params__init__S( xdc_Ptr prms, const void *src, xdc_SizeT psz, xdc_SizeT isz ) 
{
    xdc_runtime_Core_assignParams__I(prms, (xdc_CPtr)(src ? src : &ti_sysbios_family_c7x_Hwi_Object__PARAMS__C), psz, isz);
}

/* Object__get__S */
xdc_Ptr ti_sysbios_family_c7x_Hwi_Object__get__S(xdc_Ptr oa, xdc_Int i)
{
    if (oa != NULL) {
        return ((ti_sysbios_family_c7x_Hwi_Object__ *)oa) + i;
    }

    /* the bogus 'const' in the cast suppresses Klocwork MISRA complaints */
    return ((ti_sysbios_family_c7x_Hwi_Object__ * const)ti_sysbios_family_c7x_Hwi_Object__table__C) + i;
}

/* Object__first__S */
xdc_Ptr ti_sysbios_family_c7x_Hwi_Object__first__S( void ) 
{
    xdc_runtime_Types_InstHdr *iHdr = (xdc_runtime_Types_InstHdr *)ti_sysbios_family_c7x_Hwi_Module__root__V.link.next;

    if (iHdr != (xdc_runtime_Types_InstHdr *)&ti_sysbios_family_c7x_Hwi_Module__root__V.link) {
        return iHdr + 1;
    }
    else {
        return NULL;
    }
}

/* Object__next__S */
xdc_Ptr ti_sysbios_family_c7x_Hwi_Object__next__S( xdc_Ptr obj )
{
    xdc_runtime_Types_InstHdr *iHdr = ((xdc_runtime_Types_InstHdr *)obj) - 1;

    if (iHdr->link.next != (xdc_runtime_Types_Link *)&ti_sysbios_family_c7x_Hwi_Module__root__V.link) {
        return (xdc_runtime_Types_InstHdr *)(iHdr->link.next) + 1;
    }
    else {
        return NULL;
    }
}

/* Object__create__S */
xdc_Ptr ti_sysbios_family_c7x_Hwi_Object__create__S(
    xdc_CPtr req_args,
    const xdc_UChar *paramsPtr,
    xdc_SizeT prm_size,
    xdc_runtime_Error_Block *eb)
{
    const ti_sysbios_family_c7x_Hwi_Args__create *__args = req_args;
    ti_sysbios_family_c7x_Hwi_Params instPrms;
    ti_sysbios_family_c7x_Hwi_Object *objp;
    int iStat;

    /* common instance initialization */
    objp = xdc_runtime_Core_createObject__I(&ti_sysbios_family_c7x_Hwi_Object__DESC__C, NULL, &instPrms, (xdc_CPtr)paramsPtr, 0, eb);
    if (objp == NULL) {
        return NULL;
    }


    /* module-specific initialization */
    iStat = ti_sysbios_family_c7x_Hwi_Instance_init__E(objp, __args->intNum, __args->hwiFxn, &instPrms, eb);
    if (iStat) {
        xdc_runtime_Core_deleteObject__I(&ti_sysbios_family_c7x_Hwi_Object__DESC__C, objp, (xdc_Fxn)ti_sysbios_family_c7x_Hwi_Instance_finalize__E, iStat, 0);
        return NULL;
    }

    return objp;
}

/* create */
ti_sysbios_family_c7x_Hwi_Handle ti_sysbios_family_c7x_Hwi_create( xdc_Int intNum, ti_sysbios_interfaces_IHwi_FuncPtr hwiFxn, const ti_sysbios_family_c7x_Hwi_Params *paramsPtr, xdc_runtime_Error_Block *eb)
{
    ti_sysbios_family_c7x_Hwi_Params prms;
    ti_sysbios_family_c7x_Hwi_Object *obj;

    int iStat;

    /* common instance initialization */
    obj = xdc_runtime_Core_createObject__I(&ti_sysbios_family_c7x_Hwi_Object__DESC__C, NULL, &prms, (xdc_CPtr)paramsPtr, 0, eb);
    if (obj == NULL) {
        return NULL;
    }

    /* module-specific initialization */
    iStat = ti_sysbios_family_c7x_Hwi_Instance_init__E(obj, intNum, hwiFxn, &prms, eb);
    if (iStat) {
        xdc_runtime_Core_deleteObject__I(&ti_sysbios_family_c7x_Hwi_Object__DESC__C, obj, (xdc_Fxn)ti_sysbios_family_c7x_Hwi_Instance_finalize__E, iStat, 0);
        return NULL;
    }

    return obj;
}

/* construct */
void ti_sysbios_family_c7x_Hwi_construct(ti_sysbios_family_c7x_Hwi_Struct *objp, xdc_Int intNum, ti_sysbios_interfaces_IHwi_FuncPtr hwiFxn, const ti_sysbios_family_c7x_Hwi_Params *paramsPtr, xdc_runtime_Error_Block *eb)
{
    ti_sysbios_family_c7x_Hwi_Params instPrms;
    int iStat;

    /* common instance initialization */
    (Void)xdc_runtime_Core_constructObject__I(&ti_sysbios_family_c7x_Hwi_Object__DESC__C, objp, &instPrms, (xdc_CPtr)paramsPtr, 0, eb);

    /* module-specific initialization */
    iStat = ti_sysbios_family_c7x_Hwi_Instance_init__E((xdc_Ptr)objp, intNum, hwiFxn, &instPrms, eb);
    if (iStat) {
        xdc_runtime_Core_deleteObject__I(&ti_sysbios_family_c7x_Hwi_Object__DESC__C, objp, (xdc_Fxn)ti_sysbios_family_c7x_Hwi_Instance_finalize__E, iStat, 1);
    }

}

/* destruct */
void ti_sysbios_family_c7x_Hwi_destruct(ti_sysbios_family_c7x_Hwi_Struct *obj)
{
    xdc_runtime_Core_deleteObject__I(&ti_sysbios_family_c7x_Hwi_Object__DESC__C, obj, (xdc_Fxn)ti_sysbios_family_c7x_Hwi_Instance_finalize__E, 0, TRUE);
}

/* Object__delete__S */
xdc_Void ti_sysbios_family_c7x_Hwi_Object__delete__S( xdc_Ptr instp ) 
{
    xdc_runtime_Core_deleteObject__I(&ti_sysbios_family_c7x_Hwi_Object__DESC__C, *((ti_sysbios_family_c7x_Hwi_Object**)instp), (xdc_Fxn)ti_sysbios_family_c7x_Hwi_Instance_finalize__E, 0, FALSE);
    *((ti_sysbios_family_c7x_Hwi_Handle*)instp) = NULL;
}

/* delete */
void ti_sysbios_family_c7x_Hwi_delete(ti_sysbios_family_c7x_Hwi_Handle *instp)
{
    ti_sysbios_family_c7x_Hwi_Object__delete__S(instp);
}


/*
 * ======== ti.sysbios.family.c7x.IntrinsicsSupport SYSTEM FUNCTIONS ========
 */

/* Module__startupDone__S */
xdc_Bool ti_sysbios_family_c7x_IntrinsicsSupport_Module__startupDone__S( void )
{
    return 1;
}



/*
 * ======== ti.sysbios.family.c7x.Mmu SYSTEM FUNCTIONS ========
 */

/* Module__startupDone__S */
xdc_Bool ti_sysbios_family_c7x_Mmu_Module__startupDone__S( void )
{
    return 1;
}



/*
 * ======== ti.sysbios.family.c7x.TaskSupport SYSTEM FUNCTIONS ========
 */

/* Module__startupDone__S */
xdc_Bool ti_sysbios_family_c7x_TaskSupport_Module__startupDone__S( void )
{
    return 1;
}



/*
 * ======== ti.sysbios.family.c7x.TimestampProvider SYSTEM FUNCTIONS ========
 */

/* Module__startupDone__S */
xdc_Bool ti_sysbios_family_c7x_TimestampProvider_Module__startupDone__S( void )
{
    return ti_sysbios_family_c7x_TimestampProvider_Module__startupDone__F();
}



/*
 * ======== ti.sysbios.gates.GateHwi SYSTEM FUNCTIONS ========
 */

/* per-module runtime symbols */
#undef Module__MID
#define Module__MID ti_sysbios_gates_GateHwi_Module__id__C

#undef Module__DGSINCL
#define Module__DGSINCL ti_sysbios_gates_GateHwi_Module__diagsIncluded__C

#undef Module__DGSENAB
#define Module__DGSENAB ti_sysbios_gates_GateHwi_Module__diagsEnabled__C

#undef Module__DGSMASK
#define Module__DGSMASK ti_sysbios_gates_GateHwi_Module__diagsMask__C

#undef Module__LOGDEF
#define Module__LOGDEF ti_sysbios_gates_GateHwi_Module__loggerDefined__C

#undef Module__LOGOBJ
#define Module__LOGOBJ ti_sysbios_gates_GateHwi_Module__loggerObj__C

#undef Module__LOGFXN0
#define Module__LOGFXN0 ti_sysbios_gates_GateHwi_Module__loggerFxn0__C

#undef Module__LOGFXN1
#define Module__LOGFXN1 ti_sysbios_gates_GateHwi_Module__loggerFxn1__C

#undef Module__LOGFXN2
#define Module__LOGFXN2 ti_sysbios_gates_GateHwi_Module__loggerFxn2__C

#undef Module__LOGFXN4
#define Module__LOGFXN4 ti_sysbios_gates_GateHwi_Module__loggerFxn4__C

#undef Module__LOGFXN8
#define Module__LOGFXN8 ti_sysbios_gates_GateHwi_Module__loggerFxn8__C

#undef Module__G_OBJ
#define Module__G_OBJ ti_sysbios_gates_GateHwi_Module__gateObj__C

#undef Module__G_PRMS
#define Module__G_PRMS ti_sysbios_gates_GateHwi_Module__gatePrms__C

#undef Module__GP_create
#define Module__GP_create ti_sysbios_gates_GateHwi_Module_GateProxy_create
#undef Module__GP_delete
#define Module__GP_delete ti_sysbios_gates_GateHwi_Module_GateProxy_delete
#undef Module__GP_enter
#define Module__GP_enter ti_sysbios_gates_GateHwi_Module_GateProxy_enter
#undef Module__GP_leave
#define Module__GP_leave ti_sysbios_gates_GateHwi_Module_GateProxy_leave
#undef Module__GP_query
#define Module__GP_query ti_sysbios_gates_GateHwi_Module_GateProxy_query

/* Module__startupDone__S */
xdc_Bool ti_sysbios_gates_GateHwi_Module__startupDone__S( void )
{
    return 1;
}

/* Handle__label__S */
xdc_runtime_Types_Label *ti_sysbios_gates_GateHwi_Handle__label__S(xdc_Ptr obj, xdc_runtime_Types_Label *lab) 
{
    lab->handle = obj;
    lab->modId = 32822;
    lab->named = FALSE;
    lab->iname = xdc_runtime_Text_nameUnknown;

    return lab;
}

/* Params__init__S */
xdc_Void ti_sysbios_gates_GateHwi_Params__init__S( xdc_Ptr prms, const void *src, xdc_SizeT psz, xdc_SizeT isz ) 
{
    xdc_runtime_Core_assignParams__I(prms, (xdc_CPtr)(src ? src : &ti_sysbios_gates_GateHwi_Object__PARAMS__C), psz, isz);
}

/* Object__get__S */
xdc_Ptr ti_sysbios_gates_GateHwi_Object__get__S(xdc_Ptr oa, xdc_Int i)
{
    if (oa != NULL) {
        return ((ti_sysbios_gates_GateHwi_Object__ *)oa) + i;
    }

    /* the bogus 'const' in the cast suppresses Klocwork MISRA complaints */
    return ((ti_sysbios_gates_GateHwi_Object__ * const)ti_sysbios_gates_GateHwi_Object__table__C) + i;
}

/* Object__first__S */
xdc_Ptr ti_sysbios_gates_GateHwi_Object__first__S( void ) 
{
    xdc_runtime_Types_InstHdr *iHdr = (xdc_runtime_Types_InstHdr *)ti_sysbios_gates_GateHwi_Module__root__V.link.next;

    if (iHdr != (xdc_runtime_Types_InstHdr *)&ti_sysbios_gates_GateHwi_Module__root__V.link) {
        return iHdr + 1;
    }
    else {
        return NULL;
    }
}

/* Object__next__S */
xdc_Ptr ti_sysbios_gates_GateHwi_Object__next__S( xdc_Ptr obj )
{
    xdc_runtime_Types_InstHdr *iHdr = ((xdc_runtime_Types_InstHdr *)obj) - 1;

    if (iHdr->link.next != (xdc_runtime_Types_Link *)&ti_sysbios_gates_GateHwi_Module__root__V.link) {
        return (xdc_runtime_Types_InstHdr *)(iHdr->link.next) + 1;
    }
    else {
        return NULL;
    }
}

/* Object__create__S */
xdc_Ptr ti_sysbios_gates_GateHwi_Object__create__S(
    xdc_CPtr req_args,
    const xdc_UChar *paramsPtr,
    xdc_SizeT prm_size,
    xdc_runtime_Error_Block *eb)
{
    ti_sysbios_gates_GateHwi_Params instPrms;
    ti_sysbios_gates_GateHwi_Object *objp;

    /* common instance initialization */
    objp = xdc_runtime_Core_createObject__I(&ti_sysbios_gates_GateHwi_Object__DESC__C, NULL, &instPrms, (xdc_CPtr)paramsPtr, 0, eb);
    if (objp == NULL) {
        return NULL;
    }


    /* module-specific initialization */
    ti_sysbios_gates_GateHwi_Instance_init__E(objp, &instPrms);
    return objp;
}

/* create */
ti_sysbios_gates_GateHwi_Handle ti_sysbios_gates_GateHwi_create( const ti_sysbios_gates_GateHwi_Params *paramsPtr, xdc_runtime_Error_Block *eb)
{
    ti_sysbios_gates_GateHwi_Params prms;
    ti_sysbios_gates_GateHwi_Object *obj;


    /* common instance initialization */
    obj = xdc_runtime_Core_createObject__I(&ti_sysbios_gates_GateHwi_Object__DESC__C, NULL, &prms, (xdc_CPtr)paramsPtr, 0, eb);
    if (obj == NULL) {
        return NULL;
    }

    /* module-specific initialization */
    ti_sysbios_gates_GateHwi_Instance_init__E(obj, &prms);
    return obj;
}

/* construct */
void ti_sysbios_gates_GateHwi_construct(ti_sysbios_gates_GateHwi_Struct *objp, const ti_sysbios_gates_GateHwi_Params *paramsPtr)
{
    ti_sysbios_gates_GateHwi_Params instPrms;

    /* common instance initialization */
    (Void)xdc_runtime_Core_constructObject__I(&ti_sysbios_gates_GateHwi_Object__DESC__C, objp, &instPrms, (xdc_CPtr)paramsPtr, 0, NULL);

    /* module-specific initialization */
    ti_sysbios_gates_GateHwi_Instance_init__E((xdc_Ptr)objp, &instPrms);
}

/* destruct */
void ti_sysbios_gates_GateHwi_destruct(ti_sysbios_gates_GateHwi_Struct *obj)
{
    xdc_runtime_Core_deleteObject__I(&ti_sysbios_gates_GateHwi_Object__DESC__C, obj, NULL, xdc_runtime_Core_NOSTATE, TRUE);
}

/* Object__delete__S */
xdc_Void ti_sysbios_gates_GateHwi_Object__delete__S( xdc_Ptr instp ) 
{
    xdc_runtime_Core_deleteObject__I(&ti_sysbios_gates_GateHwi_Object__DESC__C, *((ti_sysbios_gates_GateHwi_Object**)instp), NULL, xdc_runtime_Core_NOSTATE, FALSE);
    *((ti_sysbios_gates_GateHwi_Handle*)instp) = NULL;
}

/* delete */
void ti_sysbios_gates_GateHwi_delete(ti_sysbios_gates_GateHwi_Handle *instp)
{
    ti_sysbios_gates_GateHwi_Object__delete__S(instp);
}


/*
 * ======== ti.sysbios.gates.GateMutex SYSTEM FUNCTIONS ========
 */

/* per-module runtime symbols */
#undef Module__MID
#define Module__MID ti_sysbios_gates_GateMutex_Module__id__C

#undef Module__DGSINCL
#define Module__DGSINCL ti_sysbios_gates_GateMutex_Module__diagsIncluded__C

#undef Module__DGSENAB
#define Module__DGSENAB ti_sysbios_gates_GateMutex_Module__diagsEnabled__C

#undef Module__DGSMASK
#define Module__DGSMASK ti_sysbios_gates_GateMutex_Module__diagsMask__C

#undef Module__LOGDEF
#define Module__LOGDEF ti_sysbios_gates_GateMutex_Module__loggerDefined__C

#undef Module__LOGOBJ
#define Module__LOGOBJ ti_sysbios_gates_GateMutex_Module__loggerObj__C

#undef Module__LOGFXN0
#define Module__LOGFXN0 ti_sysbios_gates_GateMutex_Module__loggerFxn0__C

#undef Module__LOGFXN1
#define Module__LOGFXN1 ti_sysbios_gates_GateMutex_Module__loggerFxn1__C

#undef Module__LOGFXN2
#define Module__LOGFXN2 ti_sysbios_gates_GateMutex_Module__loggerFxn2__C

#undef Module__LOGFXN4
#define Module__LOGFXN4 ti_sysbios_gates_GateMutex_Module__loggerFxn4__C

#undef Module__LOGFXN8
#define Module__LOGFXN8 ti_sysbios_gates_GateMutex_Module__loggerFxn8__C

#undef Module__G_OBJ
#define Module__G_OBJ ti_sysbios_gates_GateMutex_Module__gateObj__C

#undef Module__G_PRMS
#define Module__G_PRMS ti_sysbios_gates_GateMutex_Module__gatePrms__C

#undef Module__GP_create
#define Module__GP_create ti_sysbios_gates_GateMutex_Module_GateProxy_create
#undef Module__GP_delete
#define Module__GP_delete ti_sysbios_gates_GateMutex_Module_GateProxy_delete
#undef Module__GP_enter
#define Module__GP_enter ti_sysbios_gates_GateMutex_Module_GateProxy_enter
#undef Module__GP_leave
#define Module__GP_leave ti_sysbios_gates_GateMutex_Module_GateProxy_leave
#undef Module__GP_query
#define Module__GP_query ti_sysbios_gates_GateMutex_Module_GateProxy_query

/* Module__startupDone__S */
xdc_Bool ti_sysbios_gates_GateMutex_Module__startupDone__S( void )
{
    return 1;
}

/* Handle__label__S */
xdc_runtime_Types_Label *ti_sysbios_gates_GateMutex_Handle__label__S(xdc_Ptr obj, xdc_runtime_Types_Label *lab) 
{
    lab->handle = obj;
    lab->modId = 32826;
    lab->named = FALSE;
    lab->iname = xdc_runtime_Text_nameUnknown;

    return lab;
}

/* Params__init__S */
xdc_Void ti_sysbios_gates_GateMutex_Params__init__S( xdc_Ptr prms, const void *src, xdc_SizeT psz, xdc_SizeT isz ) 
{
    xdc_runtime_Core_assignParams__I(prms, (xdc_CPtr)(src ? src : &ti_sysbios_gates_GateMutex_Object__PARAMS__C), psz, isz);
}

/* Object__get__S */
xdc_Ptr ti_sysbios_gates_GateMutex_Object__get__S(xdc_Ptr oa, xdc_Int i)
{
    if (oa != NULL) {
        return ((ti_sysbios_gates_GateMutex_Object__ *)oa) + i;
    }

    /* the bogus 'const' in the cast suppresses Klocwork MISRA complaints */
    return ((ti_sysbios_gates_GateMutex_Object__ * const)ti_sysbios_gates_GateMutex_Object__table__C) + i;
}

/* Object__first__S */
xdc_Ptr ti_sysbios_gates_GateMutex_Object__first__S( void ) 
{
    xdc_runtime_Types_InstHdr *iHdr = (xdc_runtime_Types_InstHdr *)ti_sysbios_gates_GateMutex_Module__root__V.link.next;

    if (iHdr != (xdc_runtime_Types_InstHdr *)&ti_sysbios_gates_GateMutex_Module__root__V.link) {
        return iHdr + 1;
    }
    else {
        return NULL;
    }
}

/* Object__next__S */
xdc_Ptr ti_sysbios_gates_GateMutex_Object__next__S( xdc_Ptr obj )
{
    xdc_runtime_Types_InstHdr *iHdr = ((xdc_runtime_Types_InstHdr *)obj) - 1;

    if (iHdr->link.next != (xdc_runtime_Types_Link *)&ti_sysbios_gates_GateMutex_Module__root__V.link) {
        return (xdc_runtime_Types_InstHdr *)(iHdr->link.next) + 1;
    }
    else {
        return NULL;
    }
}

/* Object__create__S */
xdc_Ptr ti_sysbios_gates_GateMutex_Object__create__S(
    xdc_CPtr req_args,
    const xdc_UChar *paramsPtr,
    xdc_SizeT prm_size,
    xdc_runtime_Error_Block *eb)
{
    ti_sysbios_gates_GateMutex_Params instPrms;
    ti_sysbios_gates_GateMutex_Object *objp;

    /* common instance initialization */
    objp = xdc_runtime_Core_createObject__I(&ti_sysbios_gates_GateMutex_Object__DESC__C, NULL, &instPrms, (xdc_CPtr)paramsPtr, 0, eb);
    if (objp == NULL) {
        return NULL;
    }


    /* module-specific initialization */
    ti_sysbios_gates_GateMutex_Instance_init__E(objp, &instPrms);
    return objp;
}

/* create */
ti_sysbios_gates_GateMutex_Handle ti_sysbios_gates_GateMutex_create( const ti_sysbios_gates_GateMutex_Params *paramsPtr, xdc_runtime_Error_Block *eb)
{
    ti_sysbios_gates_GateMutex_Params prms;
    ti_sysbios_gates_GateMutex_Object *obj;


    /* common instance initialization */
    obj = xdc_runtime_Core_createObject__I(&ti_sysbios_gates_GateMutex_Object__DESC__C, NULL, &prms, (xdc_CPtr)paramsPtr, 0, eb);
    if (obj == NULL) {
        return NULL;
    }

    /* module-specific initialization */
    ti_sysbios_gates_GateMutex_Instance_init__E(obj, &prms);
    return obj;
}

/* construct */
void ti_sysbios_gates_GateMutex_construct(ti_sysbios_gates_GateMutex_Struct *objp, const ti_sysbios_gates_GateMutex_Params *paramsPtr)
{
    ti_sysbios_gates_GateMutex_Params instPrms;

    /* common instance initialization */
    (Void)xdc_runtime_Core_constructObject__I(&ti_sysbios_gates_GateMutex_Object__DESC__C, objp, &instPrms, (xdc_CPtr)paramsPtr, 0, NULL);

    /* module-specific initialization */
    ti_sysbios_gates_GateMutex_Instance_init__E((xdc_Ptr)objp, &instPrms);
}

/* destruct */
void ti_sysbios_gates_GateMutex_destruct(ti_sysbios_gates_GateMutex_Struct *obj)
{
    xdc_runtime_Core_deleteObject__I(&ti_sysbios_gates_GateMutex_Object__DESC__C, obj, (xdc_Fxn)ti_sysbios_gates_GateMutex_Instance_finalize__E, xdc_runtime_Core_NOSTATE, TRUE);
}

/* Object__delete__S */
xdc_Void ti_sysbios_gates_GateMutex_Object__delete__S( xdc_Ptr instp ) 
{
    xdc_runtime_Core_deleteObject__I(&ti_sysbios_gates_GateMutex_Object__DESC__C, *((ti_sysbios_gates_GateMutex_Object**)instp), (xdc_Fxn)ti_sysbios_gates_GateMutex_Instance_finalize__E, xdc_runtime_Core_NOSTATE, FALSE);
    *((ti_sysbios_gates_GateMutex_Handle*)instp) = NULL;
}

/* delete */
void ti_sysbios_gates_GateMutex_delete(ti_sysbios_gates_GateMutex_Handle *instp)
{
    ti_sysbios_gates_GateMutex_Object__delete__S(instp);
}


/*
 * ======== ti.sysbios.gates.GateMutexPri SYSTEM FUNCTIONS ========
 */

/* per-module runtime symbols */
#undef Module__MID
#define Module__MID ti_sysbios_gates_GateMutexPri_Module__id__C

#undef Module__DGSINCL
#define Module__DGSINCL ti_sysbios_gates_GateMutexPri_Module__diagsIncluded__C

#undef Module__DGSENAB
#define Module__DGSENAB ti_sysbios_gates_GateMutexPri_Module__diagsEnabled__C

#undef Module__DGSMASK
#define Module__DGSMASK ti_sysbios_gates_GateMutexPri_Module__diagsMask__C

#undef Module__LOGDEF
#define Module__LOGDEF ti_sysbios_gates_GateMutexPri_Module__loggerDefined__C

#undef Module__LOGOBJ
#define Module__LOGOBJ ti_sysbios_gates_GateMutexPri_Module__loggerObj__C

#undef Module__LOGFXN0
#define Module__LOGFXN0 ti_sysbios_gates_GateMutexPri_Module__loggerFxn0__C

#undef Module__LOGFXN1
#define Module__LOGFXN1 ti_sysbios_gates_GateMutexPri_Module__loggerFxn1__C

#undef Module__LOGFXN2
#define Module__LOGFXN2 ti_sysbios_gates_GateMutexPri_Module__loggerFxn2__C

#undef Module__LOGFXN4
#define Module__LOGFXN4 ti_sysbios_gates_GateMutexPri_Module__loggerFxn4__C

#undef Module__LOGFXN8
#define Module__LOGFXN8 ti_sysbios_gates_GateMutexPri_Module__loggerFxn8__C

#undef Module__G_OBJ
#define Module__G_OBJ ti_sysbios_gates_GateMutexPri_Module__gateObj__C

#undef Module__G_PRMS
#define Module__G_PRMS ti_sysbios_gates_GateMutexPri_Module__gatePrms__C

#undef Module__GP_create
#define Module__GP_create ti_sysbios_gates_GateMutexPri_Module_GateProxy_create
#undef Module__GP_delete
#define Module__GP_delete ti_sysbios_gates_GateMutexPri_Module_GateProxy_delete
#undef Module__GP_enter
#define Module__GP_enter ti_sysbios_gates_GateMutexPri_Module_GateProxy_enter
#undef Module__GP_leave
#define Module__GP_leave ti_sysbios_gates_GateMutexPri_Module_GateProxy_leave
#undef Module__GP_query
#define Module__GP_query ti_sysbios_gates_GateMutexPri_Module_GateProxy_query

/* Module__startupDone__S */
xdc_Bool ti_sysbios_gates_GateMutexPri_Module__startupDone__S( void )
{
    return 1;
}

/* Handle__label__S */
xdc_runtime_Types_Label *ti_sysbios_gates_GateMutexPri_Handle__label__S(xdc_Ptr obj, xdc_runtime_Types_Label *lab) 
{
    lab->handle = obj;
    lab->modId = 32825;
    lab->named = FALSE;
    lab->iname = xdc_runtime_Text_nameUnknown;

    return lab;
}

/* Params__init__S */
xdc_Void ti_sysbios_gates_GateMutexPri_Params__init__S( xdc_Ptr prms, const void *src, xdc_SizeT psz, xdc_SizeT isz ) 
{
    xdc_runtime_Core_assignParams__I(prms, (xdc_CPtr)(src ? src : &ti_sysbios_gates_GateMutexPri_Object__PARAMS__C), psz, isz);
}

/* Object__get__S */
xdc_Ptr ti_sysbios_gates_GateMutexPri_Object__get__S(xdc_Ptr oa, xdc_Int i)
{
    if (oa != NULL) {
        return ((ti_sysbios_gates_GateMutexPri_Object__ *)oa) + i;
    }

    return NULL;
}


/* Object__first__S */
xdc_Ptr ti_sysbios_gates_GateMutexPri_Object__first__S( void ) 
{
    xdc_runtime_Types_InstHdr *iHdr = (xdc_runtime_Types_InstHdr *)ti_sysbios_gates_GateMutexPri_Module__root__V.link.next;

    if (iHdr != (xdc_runtime_Types_InstHdr *)&ti_sysbios_gates_GateMutexPri_Module__root__V.link) {
        return iHdr + 1;
    }
    else {
        return NULL;
    }
}

/* Object__next__S */
xdc_Ptr ti_sysbios_gates_GateMutexPri_Object__next__S( xdc_Ptr obj )
{
    xdc_runtime_Types_InstHdr *iHdr = ((xdc_runtime_Types_InstHdr *)obj) - 1;

    if (iHdr->link.next != (xdc_runtime_Types_Link *)&ti_sysbios_gates_GateMutexPri_Module__root__V.link) {
        return (xdc_runtime_Types_InstHdr *)(iHdr->link.next) + 1;
    }
    else {
        return NULL;
    }
}

/* Object__create__S */
xdc_Ptr ti_sysbios_gates_GateMutexPri_Object__create__S(
    xdc_CPtr req_args,
    const xdc_UChar *paramsPtr,
    xdc_SizeT prm_size,
    xdc_runtime_Error_Block *eb)
{
    ti_sysbios_gates_GateMutexPri_Params instPrms;
    ti_sysbios_gates_GateMutexPri_Object *objp;

    /* common instance initialization */
    objp = xdc_runtime_Core_createObject__I(&ti_sysbios_gates_GateMutexPri_Object__DESC__C, NULL, &instPrms, (xdc_CPtr)paramsPtr, 0, eb);
    if (objp == NULL) {
        return NULL;
    }


    /* module-specific initialization */
    ti_sysbios_gates_GateMutexPri_Instance_init__E(objp, &instPrms);
    return objp;
}

/* create */
ti_sysbios_gates_GateMutexPri_Handle ti_sysbios_gates_GateMutexPri_create( const ti_sysbios_gates_GateMutexPri_Params *paramsPtr, xdc_runtime_Error_Block *eb)
{
    ti_sysbios_gates_GateMutexPri_Params prms;
    ti_sysbios_gates_GateMutexPri_Object *obj;


    /* common instance initialization */
    obj = xdc_runtime_Core_createObject__I(&ti_sysbios_gates_GateMutexPri_Object__DESC__C, NULL, &prms, (xdc_CPtr)paramsPtr, 0, eb);
    if (obj == NULL) {
        return NULL;
    }

    /* module-specific initialization */
    ti_sysbios_gates_GateMutexPri_Instance_init__E(obj, &prms);
    return obj;
}

/* construct */
void ti_sysbios_gates_GateMutexPri_construct(ti_sysbios_gates_GateMutexPri_Struct *objp, const ti_sysbios_gates_GateMutexPri_Params *paramsPtr)
{
    ti_sysbios_gates_GateMutexPri_Params instPrms;

    /* common instance initialization */
    (Void)xdc_runtime_Core_constructObject__I(&ti_sysbios_gates_GateMutexPri_Object__DESC__C, objp, &instPrms, (xdc_CPtr)paramsPtr, 0, NULL);

    /* module-specific initialization */
    ti_sysbios_gates_GateMutexPri_Instance_init__E((xdc_Ptr)objp, &instPrms);
}

/* destruct */
void ti_sysbios_gates_GateMutexPri_destruct(ti_sysbios_gates_GateMutexPri_Struct *obj)
{
    xdc_runtime_Core_deleteObject__I(&ti_sysbios_gates_GateMutexPri_Object__DESC__C, obj, (xdc_Fxn)ti_sysbios_gates_GateMutexPri_Instance_finalize__E, xdc_runtime_Core_NOSTATE, TRUE);
}

/* Object__delete__S */
xdc_Void ti_sysbios_gates_GateMutexPri_Object__delete__S( xdc_Ptr instp ) 
{
    xdc_runtime_Core_deleteObject__I(&ti_sysbios_gates_GateMutexPri_Object__DESC__C, *((ti_sysbios_gates_GateMutexPri_Object**)instp), (xdc_Fxn)ti_sysbios_gates_GateMutexPri_Instance_finalize__E, xdc_runtime_Core_NOSTATE, FALSE);
    *((ti_sysbios_gates_GateMutexPri_Handle*)instp) = NULL;
}

/* delete */
void ti_sysbios_gates_GateMutexPri_delete(ti_sysbios_gates_GateMutexPri_Handle *instp)
{
    ti_sysbios_gates_GateMutexPri_Object__delete__S(instp);
}


/*
 * ======== ti.sysbios.gates.GateSwi SYSTEM FUNCTIONS ========
 */

/* per-module runtime symbols */
#undef Module__MID
#define Module__MID ti_sysbios_gates_GateSwi_Module__id__C

#undef Module__DGSINCL
#define Module__DGSINCL ti_sysbios_gates_GateSwi_Module__diagsIncluded__C

#undef Module__DGSENAB
#define Module__DGSENAB ti_sysbios_gates_GateSwi_Module__diagsEnabled__C

#undef Module__DGSMASK
#define Module__DGSMASK ti_sysbios_gates_GateSwi_Module__diagsMask__C

#undef Module__LOGDEF
#define Module__LOGDEF ti_sysbios_gates_GateSwi_Module__loggerDefined__C

#undef Module__LOGOBJ
#define Module__LOGOBJ ti_sysbios_gates_GateSwi_Module__loggerObj__C

#undef Module__LOGFXN0
#define Module__LOGFXN0 ti_sysbios_gates_GateSwi_Module__loggerFxn0__C

#undef Module__LOGFXN1
#define Module__LOGFXN1 ti_sysbios_gates_GateSwi_Module__loggerFxn1__C

#undef Module__LOGFXN2
#define Module__LOGFXN2 ti_sysbios_gates_GateSwi_Module__loggerFxn2__C

#undef Module__LOGFXN4
#define Module__LOGFXN4 ti_sysbios_gates_GateSwi_Module__loggerFxn4__C

#undef Module__LOGFXN8
#define Module__LOGFXN8 ti_sysbios_gates_GateSwi_Module__loggerFxn8__C

#undef Module__G_OBJ
#define Module__G_OBJ ti_sysbios_gates_GateSwi_Module__gateObj__C

#undef Module__G_PRMS
#define Module__G_PRMS ti_sysbios_gates_GateSwi_Module__gatePrms__C

#undef Module__GP_create
#define Module__GP_create ti_sysbios_gates_GateSwi_Module_GateProxy_create
#undef Module__GP_delete
#define Module__GP_delete ti_sysbios_gates_GateSwi_Module_GateProxy_delete
#undef Module__GP_enter
#define Module__GP_enter ti_sysbios_gates_GateSwi_Module_GateProxy_enter
#undef Module__GP_leave
#define Module__GP_leave ti_sysbios_gates_GateSwi_Module_GateProxy_leave
#undef Module__GP_query
#define Module__GP_query ti_sysbios_gates_GateSwi_Module_GateProxy_query

/* Module__startupDone__S */
xdc_Bool ti_sysbios_gates_GateSwi_Module__startupDone__S( void )
{
    return 1;
}

/* Handle__label__S */
xdc_runtime_Types_Label *ti_sysbios_gates_GateSwi_Handle__label__S(xdc_Ptr obj, xdc_runtime_Types_Label *lab) 
{
    lab->handle = obj;
    lab->modId = 32823;
    lab->named = FALSE;
    lab->iname = xdc_runtime_Text_nameUnknown;

    return lab;
}

/* Params__init__S */
xdc_Void ti_sysbios_gates_GateSwi_Params__init__S( xdc_Ptr prms, const void *src, xdc_SizeT psz, xdc_SizeT isz ) 
{
    xdc_runtime_Core_assignParams__I(prms, (xdc_CPtr)(src ? src : &ti_sysbios_gates_GateSwi_Object__PARAMS__C), psz, isz);
}

/* Object__get__S */
xdc_Ptr ti_sysbios_gates_GateSwi_Object__get__S(xdc_Ptr oa, xdc_Int i)
{
    if (oa != NULL) {
        return ((ti_sysbios_gates_GateSwi_Object__ *)oa) + i;
    }

    return NULL;
}


/* Object__first__S */
xdc_Ptr ti_sysbios_gates_GateSwi_Object__first__S( void ) 
{
    xdc_runtime_Types_InstHdr *iHdr = (xdc_runtime_Types_InstHdr *)ti_sysbios_gates_GateSwi_Module__root__V.link.next;

    if (iHdr != (xdc_runtime_Types_InstHdr *)&ti_sysbios_gates_GateSwi_Module__root__V.link) {
        return iHdr + 1;
    }
    else {
        return NULL;
    }
}

/* Object__next__S */
xdc_Ptr ti_sysbios_gates_GateSwi_Object__next__S( xdc_Ptr obj )
{
    xdc_runtime_Types_InstHdr *iHdr = ((xdc_runtime_Types_InstHdr *)obj) - 1;

    if (iHdr->link.next != (xdc_runtime_Types_Link *)&ti_sysbios_gates_GateSwi_Module__root__V.link) {
        return (xdc_runtime_Types_InstHdr *)(iHdr->link.next) + 1;
    }
    else {
        return NULL;
    }
}

/* Object__create__S */
xdc_Ptr ti_sysbios_gates_GateSwi_Object__create__S(
    xdc_CPtr req_args,
    const xdc_UChar *paramsPtr,
    xdc_SizeT prm_size,
    xdc_runtime_Error_Block *eb)
{
    ti_sysbios_gates_GateSwi_Params instPrms;
    ti_sysbios_gates_GateSwi_Object *objp;

    /* common instance initialization */
    objp = xdc_runtime_Core_createObject__I(&ti_sysbios_gates_GateSwi_Object__DESC__C, NULL, &instPrms, (xdc_CPtr)paramsPtr, 0, eb);
    if (objp == NULL) {
        return NULL;
    }


    /* module-specific initialization */
    ti_sysbios_gates_GateSwi_Instance_init__E(objp, &instPrms);
    return objp;
}

/* create */
ti_sysbios_gates_GateSwi_Handle ti_sysbios_gates_GateSwi_create( const ti_sysbios_gates_GateSwi_Params *paramsPtr, xdc_runtime_Error_Block *eb)
{
    ti_sysbios_gates_GateSwi_Params prms;
    ti_sysbios_gates_GateSwi_Object *obj;


    /* common instance initialization */
    obj = xdc_runtime_Core_createObject__I(&ti_sysbios_gates_GateSwi_Object__DESC__C, NULL, &prms, (xdc_CPtr)paramsPtr, 0, eb);
    if (obj == NULL) {
        return NULL;
    }

    /* module-specific initialization */
    ti_sysbios_gates_GateSwi_Instance_init__E(obj, &prms);
    return obj;
}

/* construct */
void ti_sysbios_gates_GateSwi_construct(ti_sysbios_gates_GateSwi_Struct *objp, const ti_sysbios_gates_GateSwi_Params *paramsPtr)
{
    ti_sysbios_gates_GateSwi_Params instPrms;

    /* common instance initialization */
    (Void)xdc_runtime_Core_constructObject__I(&ti_sysbios_gates_GateSwi_Object__DESC__C, objp, &instPrms, (xdc_CPtr)paramsPtr, 0, NULL);

    /* module-specific initialization */
    ti_sysbios_gates_GateSwi_Instance_init__E((xdc_Ptr)objp, &instPrms);
}

/* destruct */
void ti_sysbios_gates_GateSwi_destruct(ti_sysbios_gates_GateSwi_Struct *obj)
{
    xdc_runtime_Core_deleteObject__I(&ti_sysbios_gates_GateSwi_Object__DESC__C, obj, NULL, xdc_runtime_Core_NOSTATE, TRUE);
}

/* Object__delete__S */
xdc_Void ti_sysbios_gates_GateSwi_Object__delete__S( xdc_Ptr instp ) 
{
    xdc_runtime_Core_deleteObject__I(&ti_sysbios_gates_GateSwi_Object__DESC__C, *((ti_sysbios_gates_GateSwi_Object**)instp), NULL, xdc_runtime_Core_NOSTATE, FALSE);
    *((ti_sysbios_gates_GateSwi_Handle*)instp) = NULL;
}

/* delete */
void ti_sysbios_gates_GateSwi_delete(ti_sysbios_gates_GateSwi_Handle *instp)
{
    ti_sysbios_gates_GateSwi_Object__delete__S(instp);
}


/*
 * ======== ti.sysbios.gates.GateTask SYSTEM FUNCTIONS ========
 */

/* per-module runtime symbols */
#undef Module__MID
#define Module__MID ti_sysbios_gates_GateTask_Module__id__C

#undef Module__DGSINCL
#define Module__DGSINCL ti_sysbios_gates_GateTask_Module__diagsIncluded__C

#undef Module__DGSENAB
#define Module__DGSENAB ti_sysbios_gates_GateTask_Module__diagsEnabled__C

#undef Module__DGSMASK
#define Module__DGSMASK ti_sysbios_gates_GateTask_Module__diagsMask__C

#undef Module__LOGDEF
#define Module__LOGDEF ti_sysbios_gates_GateTask_Module__loggerDefined__C

#undef Module__LOGOBJ
#define Module__LOGOBJ ti_sysbios_gates_GateTask_Module__loggerObj__C

#undef Module__LOGFXN0
#define Module__LOGFXN0 ti_sysbios_gates_GateTask_Module__loggerFxn0__C

#undef Module__LOGFXN1
#define Module__LOGFXN1 ti_sysbios_gates_GateTask_Module__loggerFxn1__C

#undef Module__LOGFXN2
#define Module__LOGFXN2 ti_sysbios_gates_GateTask_Module__loggerFxn2__C

#undef Module__LOGFXN4
#define Module__LOGFXN4 ti_sysbios_gates_GateTask_Module__loggerFxn4__C

#undef Module__LOGFXN8
#define Module__LOGFXN8 ti_sysbios_gates_GateTask_Module__loggerFxn8__C

#undef Module__G_OBJ
#define Module__G_OBJ ti_sysbios_gates_GateTask_Module__gateObj__C

#undef Module__G_PRMS
#define Module__G_PRMS ti_sysbios_gates_GateTask_Module__gatePrms__C

#undef Module__GP_create
#define Module__GP_create ti_sysbios_gates_GateTask_Module_GateProxy_create
#undef Module__GP_delete
#define Module__GP_delete ti_sysbios_gates_GateTask_Module_GateProxy_delete
#undef Module__GP_enter
#define Module__GP_enter ti_sysbios_gates_GateTask_Module_GateProxy_enter
#undef Module__GP_leave
#define Module__GP_leave ti_sysbios_gates_GateTask_Module_GateProxy_leave
#undef Module__GP_query
#define Module__GP_query ti_sysbios_gates_GateTask_Module_GateProxy_query

/* Module__startupDone__S */
xdc_Bool ti_sysbios_gates_GateTask_Module__startupDone__S( void )
{
    return 1;
}

/* Handle__label__S */
xdc_runtime_Types_Label *ti_sysbios_gates_GateTask_Handle__label__S(xdc_Ptr obj, xdc_runtime_Types_Label *lab) 
{
    lab->handle = obj;
    lab->modId = 32824;
    lab->named = FALSE;
    lab->iname = xdc_runtime_Text_nameUnknown;

    return lab;
}

/* Params__init__S */
xdc_Void ti_sysbios_gates_GateTask_Params__init__S( xdc_Ptr prms, const void *src, xdc_SizeT psz, xdc_SizeT isz ) 
{
    xdc_runtime_Core_assignParams__I(prms, (xdc_CPtr)(src ? src : &ti_sysbios_gates_GateTask_Object__PARAMS__C), psz, isz);
}

/* Object__get__S */
xdc_Ptr ti_sysbios_gates_GateTask_Object__get__S(xdc_Ptr oa, xdc_Int i)
{
    if (oa != NULL) {
        return ((ti_sysbios_gates_GateTask_Object__ *)oa) + i;
    }

    return NULL;
}


/* Object__first__S */
xdc_Ptr ti_sysbios_gates_GateTask_Object__first__S( void ) 
{
    xdc_runtime_Types_InstHdr *iHdr = (xdc_runtime_Types_InstHdr *)ti_sysbios_gates_GateTask_Module__root__V.link.next;

    if (iHdr != (xdc_runtime_Types_InstHdr *)&ti_sysbios_gates_GateTask_Module__root__V.link) {
        return iHdr + 1;
    }
    else {
        return NULL;
    }
}

/* Object__next__S */
xdc_Ptr ti_sysbios_gates_GateTask_Object__next__S( xdc_Ptr obj )
{
    xdc_runtime_Types_InstHdr *iHdr = ((xdc_runtime_Types_InstHdr *)obj) - 1;

    if (iHdr->link.next != (xdc_runtime_Types_Link *)&ti_sysbios_gates_GateTask_Module__root__V.link) {
        return (xdc_runtime_Types_InstHdr *)(iHdr->link.next) + 1;
    }
    else {
        return NULL;
    }
}

/* Object__create__S */
xdc_Ptr ti_sysbios_gates_GateTask_Object__create__S(
    xdc_CPtr req_args,
    const xdc_UChar *paramsPtr,
    xdc_SizeT prm_size,
    xdc_runtime_Error_Block *eb)
{
    ti_sysbios_gates_GateTask_Params instPrms;
    ti_sysbios_gates_GateTask_Object *objp;

    /* common instance initialization */
    objp = xdc_runtime_Core_createObject__I(&ti_sysbios_gates_GateTask_Object__DESC__C, NULL, &instPrms, (xdc_CPtr)paramsPtr, 0, eb);
    if (objp == NULL) {
        return NULL;
    }


    /* module-specific initialization */
    ti_sysbios_gates_GateTask_Instance_init__E(objp, &instPrms);
    return objp;
}

/* create */
ti_sysbios_gates_GateTask_Handle ti_sysbios_gates_GateTask_create( const ti_sysbios_gates_GateTask_Params *paramsPtr, xdc_runtime_Error_Block *eb)
{
    ti_sysbios_gates_GateTask_Params prms;
    ti_sysbios_gates_GateTask_Object *obj;


    /* common instance initialization */
    obj = xdc_runtime_Core_createObject__I(&ti_sysbios_gates_GateTask_Object__DESC__C, NULL, &prms, (xdc_CPtr)paramsPtr, 0, eb);
    if (obj == NULL) {
        return NULL;
    }

    /* module-specific initialization */
    ti_sysbios_gates_GateTask_Instance_init__E(obj, &prms);
    return obj;
}

/* construct */
void ti_sysbios_gates_GateTask_construct(ti_sysbios_gates_GateTask_Struct *objp, const ti_sysbios_gates_GateTask_Params *paramsPtr)
{
    ti_sysbios_gates_GateTask_Params instPrms;

    /* common instance initialization */
    (Void)xdc_runtime_Core_constructObject__I(&ti_sysbios_gates_GateTask_Object__DESC__C, objp, &instPrms, (xdc_CPtr)paramsPtr, 0, NULL);

    /* module-specific initialization */
    ti_sysbios_gates_GateTask_Instance_init__E((xdc_Ptr)objp, &instPrms);
}

/* destruct */
void ti_sysbios_gates_GateTask_destruct(ti_sysbios_gates_GateTask_Struct *obj)
{
    xdc_runtime_Core_deleteObject__I(&ti_sysbios_gates_GateTask_Object__DESC__C, obj, NULL, xdc_runtime_Core_NOSTATE, TRUE);
}

/* Object__delete__S */
xdc_Void ti_sysbios_gates_GateTask_Object__delete__S( xdc_Ptr instp ) 
{
    xdc_runtime_Core_deleteObject__I(&ti_sysbios_gates_GateTask_Object__DESC__C, *((ti_sysbios_gates_GateTask_Object**)instp), NULL, xdc_runtime_Core_NOSTATE, FALSE);
    *((ti_sysbios_gates_GateTask_Handle*)instp) = NULL;
}

/* delete */
void ti_sysbios_gates_GateTask_delete(ti_sysbios_gates_GateTask_Handle *instp)
{
    ti_sysbios_gates_GateTask_Object__delete__S(instp);
}


/*
 * ======== ti.sysbios.hal.Cache SYSTEM FUNCTIONS ========
 */

/* Module__startupDone__S */
xdc_Bool ti_sysbios_hal_Cache_Module__startupDone__S( void )
{
    return 1;
}



/*
 * ======== ti.sysbios.hal.Cache_CacheProxy SYSTEM FUNCTIONS ========
 */


xdc_Bool ti_sysbios_hal_Cache_CacheProxy_Proxy__abstract__S(void)
{
    return 0;
}
xdc_CPtr ti_sysbios_hal_Cache_CacheProxy_Proxy__delegate__S(void)
{
    return 0;
}


/*
 * ======== ti.sysbios.hal.Core SYSTEM FUNCTIONS ========
 */

/* Module__startupDone__S */
xdc_Bool ti_sysbios_hal_Core_Module__startupDone__S( void )
{
    return 1;
}



/*
 * ======== ti.sysbios.hal.CoreNull SYSTEM FUNCTIONS ========
 */

/* Module__startupDone__S */
xdc_Bool ti_sysbios_hal_CoreNull_Module__startupDone__S( void )
{
    return 1;
}



/*
 * ======== ti.sysbios.hal.Core_CoreProxy SYSTEM FUNCTIONS ========
 */


xdc_Bool ti_sysbios_hal_Core_CoreProxy_Proxy__abstract__S(void)
{
    return 0;
}
xdc_CPtr ti_sysbios_hal_Core_CoreProxy_Proxy__delegate__S(void)
{
    return (const void *)&ti_sysbios_hal_CoreNull_Module__FXNS__C;
}


/*
 * ======== ti.sysbios.hal.Hwi SYSTEM FUNCTIONS ========
 */

/* per-module runtime symbols */
#undef Module__MID
#define Module__MID ti_sysbios_hal_Hwi_Module__id__C

#undef Module__DGSINCL
#define Module__DGSINCL ti_sysbios_hal_Hwi_Module__diagsIncluded__C

#undef Module__DGSENAB
#define Module__DGSENAB ti_sysbios_hal_Hwi_Module__diagsEnabled__C

#undef Module__DGSMASK
#define Module__DGSMASK ti_sysbios_hal_Hwi_Module__diagsMask__C

#undef Module__LOGDEF
#define Module__LOGDEF ti_sysbios_hal_Hwi_Module__loggerDefined__C

#undef Module__LOGOBJ
#define Module__LOGOBJ ti_sysbios_hal_Hwi_Module__loggerObj__C

#undef Module__LOGFXN0
#define Module__LOGFXN0 ti_sysbios_hal_Hwi_Module__loggerFxn0__C

#undef Module__LOGFXN1
#define Module__LOGFXN1 ti_sysbios_hal_Hwi_Module__loggerFxn1__C

#undef Module__LOGFXN2
#define Module__LOGFXN2 ti_sysbios_hal_Hwi_Module__loggerFxn2__C

#undef Module__LOGFXN4
#define Module__LOGFXN4 ti_sysbios_hal_Hwi_Module__loggerFxn4__C

#undef Module__LOGFXN8
#define Module__LOGFXN8 ti_sysbios_hal_Hwi_Module__loggerFxn8__C

#undef Module__G_OBJ
#define Module__G_OBJ ti_sysbios_hal_Hwi_Module__gateObj__C

#undef Module__G_PRMS
#define Module__G_PRMS ti_sysbios_hal_Hwi_Module__gatePrms__C

#undef Module__GP_create
#define Module__GP_create ti_sysbios_hal_Hwi_Module_GateProxy_create
#undef Module__GP_delete
#define Module__GP_delete ti_sysbios_hal_Hwi_Module_GateProxy_delete
#undef Module__GP_enter
#define Module__GP_enter ti_sysbios_hal_Hwi_Module_GateProxy_enter
#undef Module__GP_leave
#define Module__GP_leave ti_sysbios_hal_Hwi_Module_GateProxy_leave
#undef Module__GP_query
#define Module__GP_query ti_sysbios_hal_Hwi_Module_GateProxy_query

/* Module__startupDone__S */
xdc_Bool ti_sysbios_hal_Hwi_Module__startupDone__S( void )
{
    return ti_sysbios_hal_Hwi_Module__startupDone__F();
}

/* Handle__label__S */
xdc_runtime_Types_Label *ti_sysbios_hal_Hwi_Handle__label__S(xdc_Ptr obj, xdc_runtime_Types_Label *lab) 
{
    lab->handle = obj;
    lab->modId = 32815;
    lab->named = FALSE;
    lab->iname = xdc_runtime_Text_nameUnknown;

    return lab;
}

/* Params__init__S */
xdc_Void ti_sysbios_hal_Hwi_Params__init__S( xdc_Ptr prms, const void *src, xdc_SizeT psz, xdc_SizeT isz ) 
{
    xdc_runtime_Core_assignParams__I(prms, (xdc_CPtr)(src ? src : &ti_sysbios_hal_Hwi_Object__PARAMS__C), psz, isz);
}

/* Object__get__S */
xdc_Ptr ti_sysbios_hal_Hwi_Object__get__S(xdc_Ptr oa, xdc_Int i)
{
    if (oa != NULL) {
        return ((ti_sysbios_hal_Hwi_Object__ *)oa) + i;
    }

    /* the bogus 'const' in the cast suppresses Klocwork MISRA complaints */
    return ((ti_sysbios_hal_Hwi_Object__ * const)ti_sysbios_hal_Hwi_Object__table__C) + i;
}

/* Object__first__S */
xdc_Ptr ti_sysbios_hal_Hwi_Object__first__S( void ) 
{
    xdc_runtime_Types_InstHdr *iHdr = (xdc_runtime_Types_InstHdr *)ti_sysbios_hal_Hwi_Module__root__V.link.next;

    if (iHdr != (xdc_runtime_Types_InstHdr *)&ti_sysbios_hal_Hwi_Module__root__V.link) {
        return iHdr + 1;
    }
    else {
        return NULL;
    }
}

/* Object__next__S */
xdc_Ptr ti_sysbios_hal_Hwi_Object__next__S( xdc_Ptr obj )
{
    xdc_runtime_Types_InstHdr *iHdr = ((xdc_runtime_Types_InstHdr *)obj) - 1;

    if (iHdr->link.next != (xdc_runtime_Types_Link *)&ti_sysbios_hal_Hwi_Module__root__V.link) {
        return (xdc_runtime_Types_InstHdr *)(iHdr->link.next) + 1;
    }
    else {
        return NULL;
    }
}

/* Object__create__S */
xdc_Ptr ti_sysbios_hal_Hwi_Object__create__S(
    xdc_CPtr req_args,
    const xdc_UChar *paramsPtr,
    xdc_SizeT prm_size,
    xdc_runtime_Error_Block *eb)
{
    const ti_sysbios_hal_Hwi_Args__create *__args = req_args;
    ti_sysbios_hal_Hwi_Params instPrms;
    ti_sysbios_hal_Hwi_Object *objp;
    int iStat;

    /* common instance initialization */
    objp = xdc_runtime_Core_createObject__I(&ti_sysbios_hal_Hwi_Object__DESC__C, NULL, &instPrms, (xdc_CPtr)paramsPtr, 0, eb);
    if (objp == NULL) {
        return NULL;
    }


    /* module-specific initialization */
    iStat = ti_sysbios_hal_Hwi_Instance_init__E(objp, __args->intNum, __args->hwiFxn, &instPrms, eb);
    if (iStat) {
        xdc_runtime_Core_deleteObject__I(&ti_sysbios_hal_Hwi_Object__DESC__C, objp, (xdc_Fxn)ti_sysbios_hal_Hwi_Instance_finalize__E, iStat, 0);
        return NULL;
    }

    return objp;
}

/* create */
ti_sysbios_hal_Hwi_Handle ti_sysbios_hal_Hwi_create( xdc_Int intNum, ti_sysbios_hal_Hwi_FuncPtr hwiFxn, const ti_sysbios_hal_Hwi_Params *paramsPtr, xdc_runtime_Error_Block *eb)
{
    ti_sysbios_hal_Hwi_Params prms;
    ti_sysbios_hal_Hwi_Object *obj;

    int iStat;

    /* common instance initialization */
    obj = xdc_runtime_Core_createObject__I(&ti_sysbios_hal_Hwi_Object__DESC__C, NULL, &prms, (xdc_CPtr)paramsPtr, 0, eb);
    if (obj == NULL) {
        return NULL;
    }

    /* module-specific initialization */
    iStat = ti_sysbios_hal_Hwi_Instance_init__E(obj, intNum, hwiFxn, &prms, eb);
    if (iStat) {
        xdc_runtime_Core_deleteObject__I(&ti_sysbios_hal_Hwi_Object__DESC__C, obj, (xdc_Fxn)ti_sysbios_hal_Hwi_Instance_finalize__E, iStat, 0);
        return NULL;
    }

    return obj;
}

/* construct */
void ti_sysbios_hal_Hwi_construct(ti_sysbios_hal_Hwi_Struct *objp, xdc_Int intNum, ti_sysbios_hal_Hwi_FuncPtr hwiFxn, const ti_sysbios_hal_Hwi_Params *paramsPtr, xdc_runtime_Error_Block *eb)
{
    ti_sysbios_hal_Hwi_Params instPrms;
    int iStat;

    /* common instance initialization */
    (Void)xdc_runtime_Core_constructObject__I(&ti_sysbios_hal_Hwi_Object__DESC__C, objp, &instPrms, (xdc_CPtr)paramsPtr, 0, eb);

    /* module-specific initialization */
    iStat = ti_sysbios_hal_Hwi_Instance_init__E((xdc_Ptr)objp, intNum, hwiFxn, &instPrms, eb);
    if (iStat) {
        xdc_runtime_Core_deleteObject__I(&ti_sysbios_hal_Hwi_Object__DESC__C, objp, (xdc_Fxn)ti_sysbios_hal_Hwi_Instance_finalize__E, iStat, 1);
    }

}

/* destruct */
void ti_sysbios_hal_Hwi_destruct(ti_sysbios_hal_Hwi_Struct *obj)
{
    xdc_runtime_Core_deleteObject__I(&ti_sysbios_hal_Hwi_Object__DESC__C, obj, (xdc_Fxn)ti_sysbios_hal_Hwi_Instance_finalize__E, 0, TRUE);
}

/* Object__delete__S */
xdc_Void ti_sysbios_hal_Hwi_Object__delete__S( xdc_Ptr instp ) 
{
    xdc_runtime_Core_deleteObject__I(&ti_sysbios_hal_Hwi_Object__DESC__C, *((ti_sysbios_hal_Hwi_Object**)instp), (xdc_Fxn)ti_sysbios_hal_Hwi_Instance_finalize__E, 0, FALSE);
    *((ti_sysbios_hal_Hwi_Handle*)instp) = NULL;
}

/* delete */
void ti_sysbios_hal_Hwi_delete(ti_sysbios_hal_Hwi_Handle *instp)
{
    ti_sysbios_hal_Hwi_Object__delete__S(instp);
}


/*
 * ======== ti.sysbios.hal.Hwi_HwiProxy SYSTEM FUNCTIONS ========
 */

/* per-module runtime symbols */
#undef Module__MID
#define Module__MID ti_sysbios_hal_Hwi_HwiProxy_Module__id__C

#undef Module__DGSINCL
#define Module__DGSINCL ti_sysbios_hal_Hwi_HwiProxy_Module__diagsIncluded__C

#undef Module__DGSENAB
#define Module__DGSENAB ti_sysbios_hal_Hwi_HwiProxy_Module__diagsEnabled__C

#undef Module__DGSMASK
#define Module__DGSMASK ti_sysbios_hal_Hwi_HwiProxy_Module__diagsMask__C

#undef Module__LOGDEF
#define Module__LOGDEF ti_sysbios_hal_Hwi_HwiProxy_Module__loggerDefined__C

#undef Module__LOGOBJ
#define Module__LOGOBJ ti_sysbios_hal_Hwi_HwiProxy_Module__loggerObj__C

#undef Module__LOGFXN0
#define Module__LOGFXN0 ti_sysbios_hal_Hwi_HwiProxy_Module__loggerFxn0__C

#undef Module__LOGFXN1
#define Module__LOGFXN1 ti_sysbios_hal_Hwi_HwiProxy_Module__loggerFxn1__C

#undef Module__LOGFXN2
#define Module__LOGFXN2 ti_sysbios_hal_Hwi_HwiProxy_Module__loggerFxn2__C

#undef Module__LOGFXN4
#define Module__LOGFXN4 ti_sysbios_hal_Hwi_HwiProxy_Module__loggerFxn4__C

#undef Module__LOGFXN8
#define Module__LOGFXN8 ti_sysbios_hal_Hwi_HwiProxy_Module__loggerFxn8__C

#undef Module__G_OBJ
#define Module__G_OBJ ti_sysbios_hal_Hwi_HwiProxy_Module__gateObj__C

#undef Module__G_PRMS
#define Module__G_PRMS ti_sysbios_hal_Hwi_HwiProxy_Module__gatePrms__C

#undef Module__GP_create
#define Module__GP_create ti_sysbios_hal_Hwi_HwiProxy_Module_GateProxy_create
#undef Module__GP_delete
#define Module__GP_delete ti_sysbios_hal_Hwi_HwiProxy_Module_GateProxy_delete
#undef Module__GP_enter
#define Module__GP_enter ti_sysbios_hal_Hwi_HwiProxy_Module_GateProxy_enter
#undef Module__GP_leave
#define Module__GP_leave ti_sysbios_hal_Hwi_HwiProxy_Module_GateProxy_leave
#undef Module__GP_query
#define Module__GP_query ti_sysbios_hal_Hwi_HwiProxy_Module_GateProxy_query

xdc_Bool ti_sysbios_hal_Hwi_HwiProxy_Proxy__abstract__S(void)
{
    return 0;
}
xdc_CPtr ti_sysbios_hal_Hwi_HwiProxy_Proxy__delegate__S(void)
{
    return 0;
}



/*
 * ======== ti.sysbios.heaps.HeapBuf SYSTEM FUNCTIONS ========
 */

/* per-module runtime symbols */
#undef Module__MID
#define Module__MID ti_sysbios_heaps_HeapBuf_Module__id__C

#undef Module__DGSINCL
#define Module__DGSINCL ti_sysbios_heaps_HeapBuf_Module__diagsIncluded__C

#undef Module__DGSENAB
#define Module__DGSENAB ti_sysbios_heaps_HeapBuf_Module__diagsEnabled__C

#undef Module__DGSMASK
#define Module__DGSMASK ti_sysbios_heaps_HeapBuf_Module__diagsMask__C

#undef Module__LOGDEF
#define Module__LOGDEF ti_sysbios_heaps_HeapBuf_Module__loggerDefined__C

#undef Module__LOGOBJ
#define Module__LOGOBJ ti_sysbios_heaps_HeapBuf_Module__loggerObj__C

#undef Module__LOGFXN0
#define Module__LOGFXN0 ti_sysbios_heaps_HeapBuf_Module__loggerFxn0__C

#undef Module__LOGFXN1
#define Module__LOGFXN1 ti_sysbios_heaps_HeapBuf_Module__loggerFxn1__C

#undef Module__LOGFXN2
#define Module__LOGFXN2 ti_sysbios_heaps_HeapBuf_Module__loggerFxn2__C

#undef Module__LOGFXN4
#define Module__LOGFXN4 ti_sysbios_heaps_HeapBuf_Module__loggerFxn4__C

#undef Module__LOGFXN8
#define Module__LOGFXN8 ti_sysbios_heaps_HeapBuf_Module__loggerFxn8__C

#undef Module__G_OBJ
#define Module__G_OBJ ti_sysbios_heaps_HeapBuf_Module__gateObj__C

#undef Module__G_PRMS
#define Module__G_PRMS ti_sysbios_heaps_HeapBuf_Module__gatePrms__C

#undef Module__GP_create
#define Module__GP_create ti_sysbios_heaps_HeapBuf_Module_GateProxy_create
#undef Module__GP_delete
#define Module__GP_delete ti_sysbios_heaps_HeapBuf_Module_GateProxy_delete
#undef Module__GP_enter
#define Module__GP_enter ti_sysbios_heaps_HeapBuf_Module_GateProxy_enter
#undef Module__GP_leave
#define Module__GP_leave ti_sysbios_heaps_HeapBuf_Module_GateProxy_leave
#undef Module__GP_query
#define Module__GP_query ti_sysbios_heaps_HeapBuf_Module_GateProxy_query

/* Module__startupDone__S */
xdc_Bool ti_sysbios_heaps_HeapBuf_Module__startupDone__S( void )
{
    return ti_sysbios_heaps_HeapBuf_Module__startupDone__F();
}

/* Handle__label__S */
xdc_runtime_Types_Label *ti_sysbios_heaps_HeapBuf_Handle__label__S(xdc_Ptr obj, xdc_runtime_Types_Label *lab) 
{
    lab->handle = obj;
    lab->modId = 32817;
    lab->named = FALSE;
    lab->iname = xdc_runtime_Text_nameUnknown;

    return lab;
}

/* Params__init__S */
xdc_Void ti_sysbios_heaps_HeapBuf_Params__init__S( xdc_Ptr prms, const void *src, xdc_SizeT psz, xdc_SizeT isz ) 
{
    xdc_runtime_Core_assignParams__I(prms, (xdc_CPtr)(src ? src : &ti_sysbios_heaps_HeapBuf_Object__PARAMS__C), psz, isz);
}

/* Object__get__S */
xdc_Ptr ti_sysbios_heaps_HeapBuf_Object__get__S(xdc_Ptr oa, xdc_Int i)
{
    if (oa != NULL) {
        return ((ti_sysbios_heaps_HeapBuf_Object__ *)oa) + i;
    }

    return NULL;
}


/* Object__first__S */
xdc_Ptr ti_sysbios_heaps_HeapBuf_Object__first__S( void ) 
{
    xdc_runtime_Types_InstHdr *iHdr = (xdc_runtime_Types_InstHdr *)ti_sysbios_heaps_HeapBuf_Module__root__V.link.next;

    if (iHdr != (xdc_runtime_Types_InstHdr *)&ti_sysbios_heaps_HeapBuf_Module__root__V.link) {
        return iHdr + 1;
    }
    else {
        return NULL;
    }
}

/* Object__next__S */
xdc_Ptr ti_sysbios_heaps_HeapBuf_Object__next__S( xdc_Ptr obj )
{
    xdc_runtime_Types_InstHdr *iHdr = ((xdc_runtime_Types_InstHdr *)obj) - 1;

    if (iHdr->link.next != (xdc_runtime_Types_Link *)&ti_sysbios_heaps_HeapBuf_Module__root__V.link) {
        return (xdc_runtime_Types_InstHdr *)(iHdr->link.next) + 1;
    }
    else {
        return NULL;
    }
}

/* Object__create__S */
xdc_Ptr ti_sysbios_heaps_HeapBuf_Object__create__S(
    xdc_CPtr req_args,
    const xdc_UChar *paramsPtr,
    xdc_SizeT prm_size,
    xdc_runtime_Error_Block *eb)
{
    ti_sysbios_heaps_HeapBuf_Params instPrms;
    ti_sysbios_heaps_HeapBuf_Object *objp;
    int iStat;

    /* common instance initialization */
    objp = xdc_runtime_Core_createObject__I(&ti_sysbios_heaps_HeapBuf_Object__DESC__C, NULL, &instPrms, (xdc_CPtr)paramsPtr, 0, eb);
    if (objp == NULL) {
        return NULL;
    }


    /* module-specific initialization */
    iStat = ti_sysbios_heaps_HeapBuf_Instance_init__E(objp, &instPrms, eb);
    if (iStat) {
        xdc_runtime_Core_deleteObject__I(&ti_sysbios_heaps_HeapBuf_Object__DESC__C, objp, (xdc_Fxn)ti_sysbios_heaps_HeapBuf_Instance_finalize__E, iStat, 0);
        return NULL;
    }

    return objp;
}

/* create */
ti_sysbios_heaps_HeapBuf_Handle ti_sysbios_heaps_HeapBuf_create( const ti_sysbios_heaps_HeapBuf_Params *paramsPtr, xdc_runtime_Error_Block *eb)
{
    ti_sysbios_heaps_HeapBuf_Params prms;
    ti_sysbios_heaps_HeapBuf_Object *obj;

    int iStat;

    /* common instance initialization */
    obj = xdc_runtime_Core_createObject__I(&ti_sysbios_heaps_HeapBuf_Object__DESC__C, NULL, &prms, (xdc_CPtr)paramsPtr, 0, eb);
    if (obj == NULL) {
        return NULL;
    }

    /* module-specific initialization */
    iStat = ti_sysbios_heaps_HeapBuf_Instance_init__E(obj, &prms, eb);
    if (iStat) {
        xdc_runtime_Core_deleteObject__I(&ti_sysbios_heaps_HeapBuf_Object__DESC__C, obj, (xdc_Fxn)ti_sysbios_heaps_HeapBuf_Instance_finalize__E, iStat, 0);
        return NULL;
    }

    return obj;
}

/* construct */
void ti_sysbios_heaps_HeapBuf_construct(ti_sysbios_heaps_HeapBuf_Struct *objp, const ti_sysbios_heaps_HeapBuf_Params *paramsPtr, xdc_runtime_Error_Block *eb)
{
    ti_sysbios_heaps_HeapBuf_Params instPrms;
    int iStat;

    /* common instance initialization */
    (Void)xdc_runtime_Core_constructObject__I(&ti_sysbios_heaps_HeapBuf_Object__DESC__C, objp, &instPrms, (xdc_CPtr)paramsPtr, 0, eb);

    /* module-specific initialization */
    iStat = ti_sysbios_heaps_HeapBuf_Instance_init__E((xdc_Ptr)objp, &instPrms, eb);
    if (iStat) {
        xdc_runtime_Core_deleteObject__I(&ti_sysbios_heaps_HeapBuf_Object__DESC__C, objp, (xdc_Fxn)ti_sysbios_heaps_HeapBuf_Instance_finalize__E, iStat, 1);
    }

}

/* destruct */
void ti_sysbios_heaps_HeapBuf_destruct(ti_sysbios_heaps_HeapBuf_Struct *obj)
{
    xdc_runtime_Core_deleteObject__I(&ti_sysbios_heaps_HeapBuf_Object__DESC__C, obj, (xdc_Fxn)ti_sysbios_heaps_HeapBuf_Instance_finalize__E, 0, TRUE);
}

/* Object__delete__S */
xdc_Void ti_sysbios_heaps_HeapBuf_Object__delete__S( xdc_Ptr instp ) 
{
    xdc_runtime_Core_deleteObject__I(&ti_sysbios_heaps_HeapBuf_Object__DESC__C, *((ti_sysbios_heaps_HeapBuf_Object**)instp), (xdc_Fxn)ti_sysbios_heaps_HeapBuf_Instance_finalize__E, 0, FALSE);
    *((ti_sysbios_heaps_HeapBuf_Handle*)instp) = NULL;
}

/* delete */
void ti_sysbios_heaps_HeapBuf_delete(ti_sysbios_heaps_HeapBuf_Handle *instp)
{
    ti_sysbios_heaps_HeapBuf_Object__delete__S(instp);
}


/*
 * ======== ti.sysbios.heaps.HeapMem SYSTEM FUNCTIONS ========
 */

/* per-module runtime symbols */
#undef Module__MID
#define Module__MID ti_sysbios_heaps_HeapMem_Module__id__C

#undef Module__DGSINCL
#define Module__DGSINCL ti_sysbios_heaps_HeapMem_Module__diagsIncluded__C

#undef Module__DGSENAB
#define Module__DGSENAB ti_sysbios_heaps_HeapMem_Module__diagsEnabled__C

#undef Module__DGSMASK
#define Module__DGSMASK ti_sysbios_heaps_HeapMem_Module__diagsMask__C

#undef Module__LOGDEF
#define Module__LOGDEF ti_sysbios_heaps_HeapMem_Module__loggerDefined__C

#undef Module__LOGOBJ
#define Module__LOGOBJ ti_sysbios_heaps_HeapMem_Module__loggerObj__C

#undef Module__LOGFXN0
#define Module__LOGFXN0 ti_sysbios_heaps_HeapMem_Module__loggerFxn0__C

#undef Module__LOGFXN1
#define Module__LOGFXN1 ti_sysbios_heaps_HeapMem_Module__loggerFxn1__C

#undef Module__LOGFXN2
#define Module__LOGFXN2 ti_sysbios_heaps_HeapMem_Module__loggerFxn2__C

#undef Module__LOGFXN4
#define Module__LOGFXN4 ti_sysbios_heaps_HeapMem_Module__loggerFxn4__C

#undef Module__LOGFXN8
#define Module__LOGFXN8 ti_sysbios_heaps_HeapMem_Module__loggerFxn8__C

#undef Module__G_OBJ
#define Module__G_OBJ ti_sysbios_heaps_HeapMem_Module__gateObj__C

#undef Module__G_PRMS
#define Module__G_PRMS ti_sysbios_heaps_HeapMem_Module__gatePrms__C

#undef Module__GP_create
#define Module__GP_create ti_sysbios_heaps_HeapMem_Module_GateProxy_create
#undef Module__GP_delete
#define Module__GP_delete ti_sysbios_heaps_HeapMem_Module_GateProxy_delete
#undef Module__GP_enter
#define Module__GP_enter ti_sysbios_heaps_HeapMem_Module_GateProxy_enter
#undef Module__GP_leave
#define Module__GP_leave ti_sysbios_heaps_HeapMem_Module_GateProxy_leave
#undef Module__GP_query
#define Module__GP_query ti_sysbios_heaps_HeapMem_Module_GateProxy_query

/* Module__startupDone__S */
xdc_Bool ti_sysbios_heaps_HeapMem_Module__startupDone__S( void )
{
    return 1;
}

/* Handle__label__S */
xdc_runtime_Types_Label *ti_sysbios_heaps_HeapMem_Handle__label__S(xdc_Ptr obj, xdc_runtime_Types_Label *lab) 
{
    lab->handle = obj;
    lab->modId = 32818;
    lab->named = FALSE;
    lab->iname = xdc_runtime_Text_nameUnknown;

    return lab;
}

/* Params__init__S */
xdc_Void ti_sysbios_heaps_HeapMem_Params__init__S( xdc_Ptr prms, const void *src, xdc_SizeT psz, xdc_SizeT isz ) 
{
    xdc_runtime_Core_assignParams__I(prms, (xdc_CPtr)(src ? src : &ti_sysbios_heaps_HeapMem_Object__PARAMS__C), psz, isz);
}

/* Object__get__S */
xdc_Ptr ti_sysbios_heaps_HeapMem_Object__get__S(xdc_Ptr oa, xdc_Int i)
{
    if (oa != NULL) {
        return ((ti_sysbios_heaps_HeapMem_Object__ *)oa) + i;
    }

    /* the bogus 'const' in the cast suppresses Klocwork MISRA complaints */
    return ((ti_sysbios_heaps_HeapMem_Object__ * const)ti_sysbios_heaps_HeapMem_Object__table__C) + i;
}

/* Object__first__S */
xdc_Ptr ti_sysbios_heaps_HeapMem_Object__first__S( void ) 
{
    xdc_runtime_Types_InstHdr *iHdr = (xdc_runtime_Types_InstHdr *)ti_sysbios_heaps_HeapMem_Module__root__V.link.next;

    if (iHdr != (xdc_runtime_Types_InstHdr *)&ti_sysbios_heaps_HeapMem_Module__root__V.link) {
        return iHdr + 1;
    }
    else {
        return NULL;
    }
}

/* Object__next__S */
xdc_Ptr ti_sysbios_heaps_HeapMem_Object__next__S( xdc_Ptr obj )
{
    xdc_runtime_Types_InstHdr *iHdr = ((xdc_runtime_Types_InstHdr *)obj) - 1;

    if (iHdr->link.next != (xdc_runtime_Types_Link *)&ti_sysbios_heaps_HeapMem_Module__root__V.link) {
        return (xdc_runtime_Types_InstHdr *)(iHdr->link.next) + 1;
    }
    else {
        return NULL;
    }
}

/* Object__create__S */
xdc_Ptr ti_sysbios_heaps_HeapMem_Object__create__S(
    xdc_CPtr req_args,
    const xdc_UChar *paramsPtr,
    xdc_SizeT prm_size,
    xdc_runtime_Error_Block *eb)
{
    ti_sysbios_heaps_HeapMem_Params instPrms;
    ti_sysbios_heaps_HeapMem_Object *objp;

    /* common instance initialization */
    objp = xdc_runtime_Core_createObject__I(&ti_sysbios_heaps_HeapMem_Object__DESC__C, NULL, &instPrms, (xdc_CPtr)paramsPtr, 0, eb);
    if (objp == NULL) {
        return NULL;
    }


    /* module-specific initialization */
    ti_sysbios_heaps_HeapMem_Instance_init__E(objp, &instPrms);
    return objp;
}

/* create */
ti_sysbios_heaps_HeapMem_Handle ti_sysbios_heaps_HeapMem_create( const ti_sysbios_heaps_HeapMem_Params *paramsPtr, xdc_runtime_Error_Block *eb)
{
    ti_sysbios_heaps_HeapMem_Params prms;
    ti_sysbios_heaps_HeapMem_Object *obj;


    /* common instance initialization */
    obj = xdc_runtime_Core_createObject__I(&ti_sysbios_heaps_HeapMem_Object__DESC__C, NULL, &prms, (xdc_CPtr)paramsPtr, 0, eb);
    if (obj == NULL) {
        return NULL;
    }

    /* module-specific initialization */
    ti_sysbios_heaps_HeapMem_Instance_init__E(obj, &prms);
    return obj;
}

/* construct */
void ti_sysbios_heaps_HeapMem_construct(ti_sysbios_heaps_HeapMem_Struct *objp, const ti_sysbios_heaps_HeapMem_Params *paramsPtr)
{
    ti_sysbios_heaps_HeapMem_Params instPrms;

    /* common instance initialization */
    (Void)xdc_runtime_Core_constructObject__I(&ti_sysbios_heaps_HeapMem_Object__DESC__C, objp, &instPrms, (xdc_CPtr)paramsPtr, 0, NULL);

    /* module-specific initialization */
    ti_sysbios_heaps_HeapMem_Instance_init__E((xdc_Ptr)objp, &instPrms);
}

/* destruct */
void ti_sysbios_heaps_HeapMem_destruct(ti_sysbios_heaps_HeapMem_Struct *obj)
{
    xdc_runtime_Core_deleteObject__I(&ti_sysbios_heaps_HeapMem_Object__DESC__C, obj, NULL, xdc_runtime_Core_NOSTATE, TRUE);
}

/* Object__delete__S */
xdc_Void ti_sysbios_heaps_HeapMem_Object__delete__S( xdc_Ptr instp ) 
{
    xdc_runtime_Core_deleteObject__I(&ti_sysbios_heaps_HeapMem_Object__DESC__C, *((ti_sysbios_heaps_HeapMem_Object**)instp), NULL, xdc_runtime_Core_NOSTATE, FALSE);
    *((ti_sysbios_heaps_HeapMem_Handle*)instp) = NULL;
}

/* delete */
void ti_sysbios_heaps_HeapMem_delete(ti_sysbios_heaps_HeapMem_Handle *instp)
{
    ti_sysbios_heaps_HeapMem_Object__delete__S(instp);
}


/*
 * ======== ti.sysbios.heaps.HeapMem_Module_GateProxy SYSTEM FUNCTIONS ========
 */

/* per-module runtime symbols */
#undef Module__MID
#define Module__MID ti_sysbios_heaps_HeapMem_Module_GateProxy_Module__id__C

#undef Module__DGSINCL
#define Module__DGSINCL ti_sysbios_heaps_HeapMem_Module_GateProxy_Module__diagsIncluded__C

#undef Module__DGSENAB
#define Module__DGSENAB ti_sysbios_heaps_HeapMem_Module_GateProxy_Module__diagsEnabled__C

#undef Module__DGSMASK
#define Module__DGSMASK ti_sysbios_heaps_HeapMem_Module_GateProxy_Module__diagsMask__C

#undef Module__LOGDEF
#define Module__LOGDEF ti_sysbios_heaps_HeapMem_Module_GateProxy_Module__loggerDefined__C

#undef Module__LOGOBJ
#define Module__LOGOBJ ti_sysbios_heaps_HeapMem_Module_GateProxy_Module__loggerObj__C

#undef Module__LOGFXN0
#define Module__LOGFXN0 ti_sysbios_heaps_HeapMem_Module_GateProxy_Module__loggerFxn0__C

#undef Module__LOGFXN1
#define Module__LOGFXN1 ti_sysbios_heaps_HeapMem_Module_GateProxy_Module__loggerFxn1__C

#undef Module__LOGFXN2
#define Module__LOGFXN2 ti_sysbios_heaps_HeapMem_Module_GateProxy_Module__loggerFxn2__C

#undef Module__LOGFXN4
#define Module__LOGFXN4 ti_sysbios_heaps_HeapMem_Module_GateProxy_Module__loggerFxn4__C

#undef Module__LOGFXN8
#define Module__LOGFXN8 ti_sysbios_heaps_HeapMem_Module_GateProxy_Module__loggerFxn8__C

#undef Module__G_OBJ
#define Module__G_OBJ ti_sysbios_heaps_HeapMem_Module_GateProxy_Module__gateObj__C

#undef Module__G_PRMS
#define Module__G_PRMS ti_sysbios_heaps_HeapMem_Module_GateProxy_Module__gatePrms__C

#undef Module__GP_create
#define Module__GP_create ti_sysbios_heaps_HeapMem_Module_GateProxy_Module_GateProxy_create
#undef Module__GP_delete
#define Module__GP_delete ti_sysbios_heaps_HeapMem_Module_GateProxy_Module_GateProxy_delete
#undef Module__GP_enter
#define Module__GP_enter ti_sysbios_heaps_HeapMem_Module_GateProxy_Module_GateProxy_enter
#undef Module__GP_leave
#define Module__GP_leave ti_sysbios_heaps_HeapMem_Module_GateProxy_Module_GateProxy_leave
#undef Module__GP_query
#define Module__GP_query ti_sysbios_heaps_HeapMem_Module_GateProxy_Module_GateProxy_query

xdc_Bool ti_sysbios_heaps_HeapMem_Module_GateProxy_Proxy__abstract__S(void)
{
    return 0;
}
xdc_CPtr ti_sysbios_heaps_HeapMem_Module_GateProxy_Proxy__delegate__S(void)
{
    return (const void *)&ti_sysbios_gates_GateMutex_Module__FXNS__C;
}



/*
 * ======== ti.sysbios.knl.Clock SYSTEM FUNCTIONS ========
 */

/* per-module runtime symbols */
#undef Module__MID
#define Module__MID ti_sysbios_knl_Clock_Module__id__C

#undef Module__DGSINCL
#define Module__DGSINCL ti_sysbios_knl_Clock_Module__diagsIncluded__C

#undef Module__DGSENAB
#define Module__DGSENAB ti_sysbios_knl_Clock_Module__diagsEnabled__C

#undef Module__DGSMASK
#define Module__DGSMASK ti_sysbios_knl_Clock_Module__diagsMask__C

#undef Module__LOGDEF
#define Module__LOGDEF ti_sysbios_knl_Clock_Module__loggerDefined__C

#undef Module__LOGOBJ
#define Module__LOGOBJ ti_sysbios_knl_Clock_Module__loggerObj__C

#undef Module__LOGFXN0
#define Module__LOGFXN0 ti_sysbios_knl_Clock_Module__loggerFxn0__C

#undef Module__LOGFXN1
#define Module__LOGFXN1 ti_sysbios_knl_Clock_Module__loggerFxn1__C

#undef Module__LOGFXN2
#define Module__LOGFXN2 ti_sysbios_knl_Clock_Module__loggerFxn2__C

#undef Module__LOGFXN4
#define Module__LOGFXN4 ti_sysbios_knl_Clock_Module__loggerFxn4__C

#undef Module__LOGFXN8
#define Module__LOGFXN8 ti_sysbios_knl_Clock_Module__loggerFxn8__C

#undef Module__G_OBJ
#define Module__G_OBJ ti_sysbios_knl_Clock_Module__gateObj__C

#undef Module__G_PRMS
#define Module__G_PRMS ti_sysbios_knl_Clock_Module__gatePrms__C

#undef Module__GP_create
#define Module__GP_create ti_sysbios_knl_Clock_Module_GateProxy_create
#undef Module__GP_delete
#define Module__GP_delete ti_sysbios_knl_Clock_Module_GateProxy_delete
#undef Module__GP_enter
#define Module__GP_enter ti_sysbios_knl_Clock_Module_GateProxy_enter
#undef Module__GP_leave
#define Module__GP_leave ti_sysbios_knl_Clock_Module_GateProxy_leave
#undef Module__GP_query
#define Module__GP_query ti_sysbios_knl_Clock_Module_GateProxy_query

/* Module__startupDone__S */
xdc_Bool ti_sysbios_knl_Clock_Module__startupDone__S( void )
{
    return ti_sysbios_knl_Clock_Module__startupDone__F();
}

/* Handle__label__S */
xdc_runtime_Types_Label *ti_sysbios_knl_Clock_Handle__label__S(xdc_Ptr obj, xdc_runtime_Types_Label *lab) 
{
    lab->handle = obj;
    lab->modId = 32800;
    lab->named = FALSE;
    lab->iname = xdc_runtime_Text_nameUnknown;

    return lab;
}

/* Params__init__S */
xdc_Void ti_sysbios_knl_Clock_Params__init__S( xdc_Ptr prms, const void *src, xdc_SizeT psz, xdc_SizeT isz ) 
{
    xdc_runtime_Core_assignParams__I(prms, (xdc_CPtr)(src ? src : &ti_sysbios_knl_Clock_Object__PARAMS__C), psz, isz);
}

/* Object__get__S */
xdc_Ptr ti_sysbios_knl_Clock_Object__get__S(xdc_Ptr oa, xdc_Int i)
{
    if (oa != NULL) {
        return ((ti_sysbios_knl_Clock_Object__ *)oa) + i;
    }

    return NULL;
}


/* Object__first__S */
xdc_Ptr ti_sysbios_knl_Clock_Object__first__S( void ) 
{
    xdc_runtime_Types_InstHdr *iHdr = (xdc_runtime_Types_InstHdr *)ti_sysbios_knl_Clock_Module__root__V.link.next;

    if (iHdr != (xdc_runtime_Types_InstHdr *)&ti_sysbios_knl_Clock_Module__root__V.link) {
        return iHdr + 1;
    }
    else {
        return NULL;
    }
}

/* Object__next__S */
xdc_Ptr ti_sysbios_knl_Clock_Object__next__S( xdc_Ptr obj )
{
    xdc_runtime_Types_InstHdr *iHdr = ((xdc_runtime_Types_InstHdr *)obj) - 1;

    if (iHdr->link.next != (xdc_runtime_Types_Link *)&ti_sysbios_knl_Clock_Module__root__V.link) {
        return (xdc_runtime_Types_InstHdr *)(iHdr->link.next) + 1;
    }
    else {
        return NULL;
    }
}

/* Object__create__S */
xdc_Ptr ti_sysbios_knl_Clock_Object__create__S(
    xdc_CPtr req_args,
    const xdc_UChar *paramsPtr,
    xdc_SizeT prm_size,
    xdc_runtime_Error_Block *eb)
{
    const ti_sysbios_knl_Clock_Args__create *__args = req_args;
    ti_sysbios_knl_Clock_Params instPrms;
    ti_sysbios_knl_Clock_Object *objp;

    /* common instance initialization */
    objp = xdc_runtime_Core_createObject__I(&ti_sysbios_knl_Clock_Object__DESC__C, NULL, &instPrms, (xdc_CPtr)paramsPtr, 0, eb);
    if (objp == NULL) {
        return NULL;
    }


    /* module-specific initialization */
    ti_sysbios_knl_Clock_Instance_init__E(objp, __args->clockFxn, __args->timeout, &instPrms);
    return objp;
}

/* create */
ti_sysbios_knl_Clock_Handle ti_sysbios_knl_Clock_create( ti_sysbios_knl_Clock_FuncPtr clockFxn, xdc_UInt timeout, const ti_sysbios_knl_Clock_Params *paramsPtr, xdc_runtime_Error_Block *eb)
{
    ti_sysbios_knl_Clock_Params prms;
    ti_sysbios_knl_Clock_Object *obj;


    /* common instance initialization */
    obj = xdc_runtime_Core_createObject__I(&ti_sysbios_knl_Clock_Object__DESC__C, NULL, &prms, (xdc_CPtr)paramsPtr, 0, eb);
    if (obj == NULL) {
        return NULL;
    }

    /* module-specific initialization */
    ti_sysbios_knl_Clock_Instance_init__E(obj, clockFxn, timeout, &prms);
    return obj;
}

/* construct */
void ti_sysbios_knl_Clock_construct(ti_sysbios_knl_Clock_Struct *objp, ti_sysbios_knl_Clock_FuncPtr clockFxn, xdc_UInt timeout, const ti_sysbios_knl_Clock_Params *paramsPtr)
{
    ti_sysbios_knl_Clock_Params instPrms;

    /* common instance initialization */
    (Void)xdc_runtime_Core_constructObject__I(&ti_sysbios_knl_Clock_Object__DESC__C, objp, &instPrms, (xdc_CPtr)paramsPtr, 0, NULL);

    /* module-specific initialization */
    ti_sysbios_knl_Clock_Instance_init__E((xdc_Ptr)objp, clockFxn, timeout, &instPrms);
}

/* destruct */
void ti_sysbios_knl_Clock_destruct(ti_sysbios_knl_Clock_Struct *obj)
{
    xdc_runtime_Core_deleteObject__I(&ti_sysbios_knl_Clock_Object__DESC__C, obj, (xdc_Fxn)ti_sysbios_knl_Clock_Instance_finalize__E, xdc_runtime_Core_NOSTATE, TRUE);
}

/* Object__delete__S */
xdc_Void ti_sysbios_knl_Clock_Object__delete__S( xdc_Ptr instp ) 
{
    xdc_runtime_Core_deleteObject__I(&ti_sysbios_knl_Clock_Object__DESC__C, *((ti_sysbios_knl_Clock_Object**)instp), (xdc_Fxn)ti_sysbios_knl_Clock_Instance_finalize__E, xdc_runtime_Core_NOSTATE, FALSE);
    *((ti_sysbios_knl_Clock_Handle*)instp) = NULL;
}

/* delete */
void ti_sysbios_knl_Clock_delete(ti_sysbios_knl_Clock_Handle *instp)
{
    ti_sysbios_knl_Clock_Object__delete__S(instp);
}


/*
 * ======== ti.sysbios.knl.Clock_TimerProxy SYSTEM FUNCTIONS ========
 */

/* per-module runtime symbols */
#undef Module__MID
#define Module__MID ti_sysbios_knl_Clock_TimerProxy_Module__id__C

#undef Module__DGSINCL
#define Module__DGSINCL ti_sysbios_knl_Clock_TimerProxy_Module__diagsIncluded__C

#undef Module__DGSENAB
#define Module__DGSENAB ti_sysbios_knl_Clock_TimerProxy_Module__diagsEnabled__C

#undef Module__DGSMASK
#define Module__DGSMASK ti_sysbios_knl_Clock_TimerProxy_Module__diagsMask__C

#undef Module__LOGDEF
#define Module__LOGDEF ti_sysbios_knl_Clock_TimerProxy_Module__loggerDefined__C

#undef Module__LOGOBJ
#define Module__LOGOBJ ti_sysbios_knl_Clock_TimerProxy_Module__loggerObj__C

#undef Module__LOGFXN0
#define Module__LOGFXN0 ti_sysbios_knl_Clock_TimerProxy_Module__loggerFxn0__C

#undef Module__LOGFXN1
#define Module__LOGFXN1 ti_sysbios_knl_Clock_TimerProxy_Module__loggerFxn1__C

#undef Module__LOGFXN2
#define Module__LOGFXN2 ti_sysbios_knl_Clock_TimerProxy_Module__loggerFxn2__C

#undef Module__LOGFXN4
#define Module__LOGFXN4 ti_sysbios_knl_Clock_TimerProxy_Module__loggerFxn4__C

#undef Module__LOGFXN8
#define Module__LOGFXN8 ti_sysbios_knl_Clock_TimerProxy_Module__loggerFxn8__C

#undef Module__G_OBJ
#define Module__G_OBJ ti_sysbios_knl_Clock_TimerProxy_Module__gateObj__C

#undef Module__G_PRMS
#define Module__G_PRMS ti_sysbios_knl_Clock_TimerProxy_Module__gatePrms__C

#undef Module__GP_create
#define Module__GP_create ti_sysbios_knl_Clock_TimerProxy_Module_GateProxy_create
#undef Module__GP_delete
#define Module__GP_delete ti_sysbios_knl_Clock_TimerProxy_Module_GateProxy_delete
#undef Module__GP_enter
#define Module__GP_enter ti_sysbios_knl_Clock_TimerProxy_Module_GateProxy_enter
#undef Module__GP_leave
#define Module__GP_leave ti_sysbios_knl_Clock_TimerProxy_Module_GateProxy_leave
#undef Module__GP_query
#define Module__GP_query ti_sysbios_knl_Clock_TimerProxy_Module_GateProxy_query

xdc_Bool ti_sysbios_knl_Clock_TimerProxy_Proxy__abstract__S(void)
{
    return 0;
}
xdc_CPtr ti_sysbios_knl_Clock_TimerProxy_Proxy__delegate__S(void)
{
    return 0;
}



/*
 * ======== ti.sysbios.knl.Event SYSTEM FUNCTIONS ========
 */

/* per-module runtime symbols */
#undef Module__MID
#define Module__MID ti_sysbios_knl_Event_Module__id__C

#undef Module__DGSINCL
#define Module__DGSINCL ti_sysbios_knl_Event_Module__diagsIncluded__C

#undef Module__DGSENAB
#define Module__DGSENAB ti_sysbios_knl_Event_Module__diagsEnabled__C

#undef Module__DGSMASK
#define Module__DGSMASK ti_sysbios_knl_Event_Module__diagsMask__C

#undef Module__LOGDEF
#define Module__LOGDEF ti_sysbios_knl_Event_Module__loggerDefined__C

#undef Module__LOGOBJ
#define Module__LOGOBJ ti_sysbios_knl_Event_Module__loggerObj__C

#undef Module__LOGFXN0
#define Module__LOGFXN0 ti_sysbios_knl_Event_Module__loggerFxn0__C

#undef Module__LOGFXN1
#define Module__LOGFXN1 ti_sysbios_knl_Event_Module__loggerFxn1__C

#undef Module__LOGFXN2
#define Module__LOGFXN2 ti_sysbios_knl_Event_Module__loggerFxn2__C

#undef Module__LOGFXN4
#define Module__LOGFXN4 ti_sysbios_knl_Event_Module__loggerFxn4__C

#undef Module__LOGFXN8
#define Module__LOGFXN8 ti_sysbios_knl_Event_Module__loggerFxn8__C

#undef Module__G_OBJ
#define Module__G_OBJ ti_sysbios_knl_Event_Module__gateObj__C

#undef Module__G_PRMS
#define Module__G_PRMS ti_sysbios_knl_Event_Module__gatePrms__C

#undef Module__GP_create
#define Module__GP_create ti_sysbios_knl_Event_Module_GateProxy_create
#undef Module__GP_delete
#define Module__GP_delete ti_sysbios_knl_Event_Module_GateProxy_delete
#undef Module__GP_enter
#define Module__GP_enter ti_sysbios_knl_Event_Module_GateProxy_enter
#undef Module__GP_leave
#define Module__GP_leave ti_sysbios_knl_Event_Module_GateProxy_leave
#undef Module__GP_query
#define Module__GP_query ti_sysbios_knl_Event_Module_GateProxy_query

/* Module__startupDone__S */
xdc_Bool ti_sysbios_knl_Event_Module__startupDone__S( void )
{
    return 1;
}

/* Handle__label__S */
xdc_runtime_Types_Label *ti_sysbios_knl_Event_Handle__label__S(xdc_Ptr obj, xdc_runtime_Types_Label *lab) 
{
    lab->handle = obj;
    lab->modId = 32803;
    lab->named = FALSE;
    lab->iname = xdc_runtime_Text_nameUnknown;

    return lab;
}

/* Params__init__S */
xdc_Void ti_sysbios_knl_Event_Params__init__S( xdc_Ptr prms, const void *src, xdc_SizeT psz, xdc_SizeT isz ) 
{
    xdc_runtime_Core_assignParams__I(prms, (xdc_CPtr)(src ? src : &ti_sysbios_knl_Event_Object__PARAMS__C), psz, isz);
}

/* Object__get__S */
xdc_Ptr ti_sysbios_knl_Event_Object__get__S(xdc_Ptr oa, xdc_Int i)
{
    if (oa != NULL) {
        return ((ti_sysbios_knl_Event_Object__ *)oa) + i;
    }

    return NULL;
}


/* Object__first__S */
xdc_Ptr ti_sysbios_knl_Event_Object__first__S( void ) 
{
    xdc_runtime_Types_InstHdr *iHdr = (xdc_runtime_Types_InstHdr *)ti_sysbios_knl_Event_Module__root__V.link.next;

    if (iHdr != (xdc_runtime_Types_InstHdr *)&ti_sysbios_knl_Event_Module__root__V.link) {
        return iHdr + 1;
    }
    else {
        return NULL;
    }
}

/* Object__next__S */
xdc_Ptr ti_sysbios_knl_Event_Object__next__S( xdc_Ptr obj )
{
    xdc_runtime_Types_InstHdr *iHdr = ((xdc_runtime_Types_InstHdr *)obj) - 1;

    if (iHdr->link.next != (xdc_runtime_Types_Link *)&ti_sysbios_knl_Event_Module__root__V.link) {
        return (xdc_runtime_Types_InstHdr *)(iHdr->link.next) + 1;
    }
    else {
        return NULL;
    }
}

/* Object__create__S */
xdc_Ptr ti_sysbios_knl_Event_Object__create__S(
    xdc_CPtr req_args,
    const xdc_UChar *paramsPtr,
    xdc_SizeT prm_size,
    xdc_runtime_Error_Block *eb)
{
    ti_sysbios_knl_Event_Params instPrms;
    ti_sysbios_knl_Event_Object *objp;

    /* common instance initialization */
    objp = xdc_runtime_Core_createObject__I(&ti_sysbios_knl_Event_Object__DESC__C, NULL, &instPrms, (xdc_CPtr)paramsPtr, 0, eb);
    if (objp == NULL) {
        return NULL;
    }


    /* module-specific initialization */
    ti_sysbios_knl_Event_Instance_init__E(objp, &instPrms);
    return objp;
}

/* create */
ti_sysbios_knl_Event_Handle ti_sysbios_knl_Event_create( const ti_sysbios_knl_Event_Params *paramsPtr, xdc_runtime_Error_Block *eb)
{
    ti_sysbios_knl_Event_Params prms;
    ti_sysbios_knl_Event_Object *obj;


    /* common instance initialization */
    obj = xdc_runtime_Core_createObject__I(&ti_sysbios_knl_Event_Object__DESC__C, NULL, &prms, (xdc_CPtr)paramsPtr, 0, eb);
    if (obj == NULL) {
        return NULL;
    }

    /* module-specific initialization */
    ti_sysbios_knl_Event_Instance_init__E(obj, &prms);
    return obj;
}

/* construct */
void ti_sysbios_knl_Event_construct(ti_sysbios_knl_Event_Struct *objp, const ti_sysbios_knl_Event_Params *paramsPtr)
{
    ti_sysbios_knl_Event_Params instPrms;

    /* common instance initialization */
    (Void)xdc_runtime_Core_constructObject__I(&ti_sysbios_knl_Event_Object__DESC__C, objp, &instPrms, (xdc_CPtr)paramsPtr, 0, NULL);

    /* module-specific initialization */
    ti_sysbios_knl_Event_Instance_init__E((xdc_Ptr)objp, &instPrms);
}

/* destruct */
void ti_sysbios_knl_Event_destruct(ti_sysbios_knl_Event_Struct *obj)
{
    xdc_runtime_Core_deleteObject__I(&ti_sysbios_knl_Event_Object__DESC__C, obj, NULL, xdc_runtime_Core_NOSTATE, TRUE);
}

/* Object__delete__S */
xdc_Void ti_sysbios_knl_Event_Object__delete__S( xdc_Ptr instp ) 
{
    xdc_runtime_Core_deleteObject__I(&ti_sysbios_knl_Event_Object__DESC__C, *((ti_sysbios_knl_Event_Object**)instp), NULL, xdc_runtime_Core_NOSTATE, FALSE);
    *((ti_sysbios_knl_Event_Handle*)instp) = NULL;
}

/* delete */
void ti_sysbios_knl_Event_delete(ti_sysbios_knl_Event_Handle *instp)
{
    ti_sysbios_knl_Event_Object__delete__S(instp);
}


/*
 * ======== ti.sysbios.knl.Idle SYSTEM FUNCTIONS ========
 */

/* Module__startupDone__S */
xdc_Bool ti_sysbios_knl_Idle_Module__startupDone__S( void )
{
    return 1;
}



/*
 * ======== ti.sysbios.knl.Intrinsics SYSTEM FUNCTIONS ========
 */

/* Module__startupDone__S */
xdc_Bool ti_sysbios_knl_Intrinsics_Module__startupDone__S( void )
{
    return 1;
}



/*
 * ======== ti.sysbios.knl.Intrinsics_SupportProxy SYSTEM FUNCTIONS ========
 */


xdc_Bool ti_sysbios_knl_Intrinsics_SupportProxy_Proxy__abstract__S(void)
{
    return 0;
}
xdc_CPtr ti_sysbios_knl_Intrinsics_SupportProxy_Proxy__delegate__S(void)
{
    return 0;
}


/*
 * ======== ti.sysbios.knl.Queue SYSTEM FUNCTIONS ========
 */

/* per-module runtime symbols */
#undef Module__MID
#define Module__MID ti_sysbios_knl_Queue_Module__id__C

#undef Module__DGSINCL
#define Module__DGSINCL ti_sysbios_knl_Queue_Module__diagsIncluded__C

#undef Module__DGSENAB
#define Module__DGSENAB ti_sysbios_knl_Queue_Module__diagsEnabled__C

#undef Module__DGSMASK
#define Module__DGSMASK ti_sysbios_knl_Queue_Module__diagsMask__C

#undef Module__LOGDEF
#define Module__LOGDEF ti_sysbios_knl_Queue_Module__loggerDefined__C

#undef Module__LOGOBJ
#define Module__LOGOBJ ti_sysbios_knl_Queue_Module__loggerObj__C

#undef Module__LOGFXN0
#define Module__LOGFXN0 ti_sysbios_knl_Queue_Module__loggerFxn0__C

#undef Module__LOGFXN1
#define Module__LOGFXN1 ti_sysbios_knl_Queue_Module__loggerFxn1__C

#undef Module__LOGFXN2
#define Module__LOGFXN2 ti_sysbios_knl_Queue_Module__loggerFxn2__C

#undef Module__LOGFXN4
#define Module__LOGFXN4 ti_sysbios_knl_Queue_Module__loggerFxn4__C

#undef Module__LOGFXN8
#define Module__LOGFXN8 ti_sysbios_knl_Queue_Module__loggerFxn8__C

#undef Module__G_OBJ
#define Module__G_OBJ ti_sysbios_knl_Queue_Module__gateObj__C

#undef Module__G_PRMS
#define Module__G_PRMS ti_sysbios_knl_Queue_Module__gatePrms__C

#undef Module__GP_create
#define Module__GP_create ti_sysbios_knl_Queue_Module_GateProxy_create
#undef Module__GP_delete
#define Module__GP_delete ti_sysbios_knl_Queue_Module_GateProxy_delete
#undef Module__GP_enter
#define Module__GP_enter ti_sysbios_knl_Queue_Module_GateProxy_enter
#undef Module__GP_leave
#define Module__GP_leave ti_sysbios_knl_Queue_Module_GateProxy_leave
#undef Module__GP_query
#define Module__GP_query ti_sysbios_knl_Queue_Module_GateProxy_query

/* Module__startupDone__S */
xdc_Bool ti_sysbios_knl_Queue_Module__startupDone__S( void )
{
    return 1;
}

/* Handle__label__S */
xdc_runtime_Types_Label *ti_sysbios_knl_Queue_Handle__label__S(xdc_Ptr obj, xdc_runtime_Types_Label *lab) 
{
    lab->handle = obj;
    lab->modId = 32804;
    lab->named = FALSE;
    lab->iname = xdc_runtime_Text_nameUnknown;

    return lab;
}

/* Params__init__S */
xdc_Void ti_sysbios_knl_Queue_Params__init__S( xdc_Ptr prms, const void *src, xdc_SizeT psz, xdc_SizeT isz ) 
{
    xdc_runtime_Core_assignParams__I(prms, (xdc_CPtr)(src ? src : &ti_sysbios_knl_Queue_Object__PARAMS__C), psz, isz);
}

/* Object__get__S */
xdc_Ptr ti_sysbios_knl_Queue_Object__get__S(xdc_Ptr oa, xdc_Int i)
{
    if (oa != NULL) {
        return ((ti_sysbios_knl_Queue_Object__ *)oa) + i;
    }

    return NULL;
}


/* Object__first__S */
xdc_Ptr ti_sysbios_knl_Queue_Object__first__S( void ) 
{
    xdc_runtime_Types_InstHdr *iHdr = (xdc_runtime_Types_InstHdr *)ti_sysbios_knl_Queue_Module__root__V.link.next;

    if (iHdr != (xdc_runtime_Types_InstHdr *)&ti_sysbios_knl_Queue_Module__root__V.link) {
        return iHdr + 1;
    }
    else {
        return NULL;
    }
}

/* Object__next__S */
xdc_Ptr ti_sysbios_knl_Queue_Object__next__S( xdc_Ptr obj )
{
    xdc_runtime_Types_InstHdr *iHdr = ((xdc_runtime_Types_InstHdr *)obj) - 1;

    if (iHdr->link.next != (xdc_runtime_Types_Link *)&ti_sysbios_knl_Queue_Module__root__V.link) {
        return (xdc_runtime_Types_InstHdr *)(iHdr->link.next) + 1;
    }
    else {
        return NULL;
    }
}

/* Object__create__S */
xdc_Ptr ti_sysbios_knl_Queue_Object__create__S(
    xdc_CPtr req_args,
    const xdc_UChar *paramsPtr,
    xdc_SizeT prm_size,
    xdc_runtime_Error_Block *eb)
{
    ti_sysbios_knl_Queue_Params instPrms;
    ti_sysbios_knl_Queue_Object *objp;

    /* common instance initialization */
    objp = xdc_runtime_Core_createObject__I(&ti_sysbios_knl_Queue_Object__DESC__C, NULL, &instPrms, (xdc_CPtr)paramsPtr, 0, eb);
    if (objp == NULL) {
        return NULL;
    }


    /* module-specific initialization */
    ti_sysbios_knl_Queue_Instance_init__E(objp, &instPrms);
    return objp;
}

/* create */
ti_sysbios_knl_Queue_Handle ti_sysbios_knl_Queue_create( const ti_sysbios_knl_Queue_Params *paramsPtr, xdc_runtime_Error_Block *eb)
{
    ti_sysbios_knl_Queue_Params prms;
    ti_sysbios_knl_Queue_Object *obj;


    /* common instance initialization */
    obj = xdc_runtime_Core_createObject__I(&ti_sysbios_knl_Queue_Object__DESC__C, NULL, &prms, (xdc_CPtr)paramsPtr, 0, eb);
    if (obj == NULL) {
        return NULL;
    }

    /* module-specific initialization */
    ti_sysbios_knl_Queue_Instance_init__E(obj, &prms);
    return obj;
}

/* construct */
void ti_sysbios_knl_Queue_construct(ti_sysbios_knl_Queue_Struct *objp, const ti_sysbios_knl_Queue_Params *paramsPtr)
{
    ti_sysbios_knl_Queue_Params instPrms;

    /* common instance initialization */
    (Void)xdc_runtime_Core_constructObject__I(&ti_sysbios_knl_Queue_Object__DESC__C, objp, &instPrms, (xdc_CPtr)paramsPtr, 0, NULL);

    /* module-specific initialization */
    ti_sysbios_knl_Queue_Instance_init__E((xdc_Ptr)objp, &instPrms);
}

/* destruct */
void ti_sysbios_knl_Queue_destruct(ti_sysbios_knl_Queue_Struct *obj)
{
    xdc_runtime_Core_deleteObject__I(&ti_sysbios_knl_Queue_Object__DESC__C, obj, NULL, xdc_runtime_Core_NOSTATE, TRUE);
}

/* Object__delete__S */
xdc_Void ti_sysbios_knl_Queue_Object__delete__S( xdc_Ptr instp ) 
{
    xdc_runtime_Core_deleteObject__I(&ti_sysbios_knl_Queue_Object__DESC__C, *((ti_sysbios_knl_Queue_Object**)instp), NULL, xdc_runtime_Core_NOSTATE, FALSE);
    *((ti_sysbios_knl_Queue_Handle*)instp) = NULL;
}

/* delete */
void ti_sysbios_knl_Queue_delete(ti_sysbios_knl_Queue_Handle *instp)
{
    ti_sysbios_knl_Queue_Object__delete__S(instp);
}


/*
 * ======== ti.sysbios.knl.Semaphore SYSTEM FUNCTIONS ========
 */

/* per-module runtime symbols */
#undef Module__MID
#define Module__MID ti_sysbios_knl_Semaphore_Module__id__C

#undef Module__DGSINCL
#define Module__DGSINCL ti_sysbios_knl_Semaphore_Module__diagsIncluded__C

#undef Module__DGSENAB
#define Module__DGSENAB ti_sysbios_knl_Semaphore_Module__diagsEnabled__C

#undef Module__DGSMASK
#define Module__DGSMASK ti_sysbios_knl_Semaphore_Module__diagsMask__C

#undef Module__LOGDEF
#define Module__LOGDEF ti_sysbios_knl_Semaphore_Module__loggerDefined__C

#undef Module__LOGOBJ
#define Module__LOGOBJ ti_sysbios_knl_Semaphore_Module__loggerObj__C

#undef Module__LOGFXN0
#define Module__LOGFXN0 ti_sysbios_knl_Semaphore_Module__loggerFxn0__C

#undef Module__LOGFXN1
#define Module__LOGFXN1 ti_sysbios_knl_Semaphore_Module__loggerFxn1__C

#undef Module__LOGFXN2
#define Module__LOGFXN2 ti_sysbios_knl_Semaphore_Module__loggerFxn2__C

#undef Module__LOGFXN4
#define Module__LOGFXN4 ti_sysbios_knl_Semaphore_Module__loggerFxn4__C

#undef Module__LOGFXN8
#define Module__LOGFXN8 ti_sysbios_knl_Semaphore_Module__loggerFxn8__C

#undef Module__G_OBJ
#define Module__G_OBJ ti_sysbios_knl_Semaphore_Module__gateObj__C

#undef Module__G_PRMS
#define Module__G_PRMS ti_sysbios_knl_Semaphore_Module__gatePrms__C

#undef Module__GP_create
#define Module__GP_create ti_sysbios_knl_Semaphore_Module_GateProxy_create
#undef Module__GP_delete
#define Module__GP_delete ti_sysbios_knl_Semaphore_Module_GateProxy_delete
#undef Module__GP_enter
#define Module__GP_enter ti_sysbios_knl_Semaphore_Module_GateProxy_enter
#undef Module__GP_leave
#define Module__GP_leave ti_sysbios_knl_Semaphore_Module_GateProxy_leave
#undef Module__GP_query
#define Module__GP_query ti_sysbios_knl_Semaphore_Module_GateProxy_query

/* Module__startupDone__S */
xdc_Bool ti_sysbios_knl_Semaphore_Module__startupDone__S( void )
{
    return 1;
}

/* Handle__label__S */
xdc_runtime_Types_Label *ti_sysbios_knl_Semaphore_Handle__label__S(xdc_Ptr obj, xdc_runtime_Types_Label *lab) 
{
    lab->handle = obj;
    lab->modId = 32805;
    lab->named = FALSE;
    lab->iname = xdc_runtime_Text_nameUnknown;

    return lab;
}

/* Params__init__S */
xdc_Void ti_sysbios_knl_Semaphore_Params__init__S( xdc_Ptr prms, const void *src, xdc_SizeT psz, xdc_SizeT isz ) 
{
    xdc_runtime_Core_assignParams__I(prms, (xdc_CPtr)(src ? src : &ti_sysbios_knl_Semaphore_Object__PARAMS__C), psz, isz);
}

/* Object__get__S */
xdc_Ptr ti_sysbios_knl_Semaphore_Object__get__S(xdc_Ptr oa, xdc_Int i)
{
    if (oa != NULL) {
        return ((ti_sysbios_knl_Semaphore_Object__ *)oa) + i;
    }

    return NULL;
}


/* Object__first__S */
xdc_Ptr ti_sysbios_knl_Semaphore_Object__first__S( void ) 
{
    xdc_runtime_Types_InstHdr *iHdr = (xdc_runtime_Types_InstHdr *)ti_sysbios_knl_Semaphore_Module__root__V.link.next;

    if (iHdr != (xdc_runtime_Types_InstHdr *)&ti_sysbios_knl_Semaphore_Module__root__V.link) {
        return iHdr + 1;
    }
    else {
        return NULL;
    }
}

/* Object__next__S */
xdc_Ptr ti_sysbios_knl_Semaphore_Object__next__S( xdc_Ptr obj )
{
    xdc_runtime_Types_InstHdr *iHdr = ((xdc_runtime_Types_InstHdr *)obj) - 1;

    if (iHdr->link.next != (xdc_runtime_Types_Link *)&ti_sysbios_knl_Semaphore_Module__root__V.link) {
        return (xdc_runtime_Types_InstHdr *)(iHdr->link.next) + 1;
    }
    else {
        return NULL;
    }
}

/* Object__create__S */
xdc_Ptr ti_sysbios_knl_Semaphore_Object__create__S(
    xdc_CPtr req_args,
    const xdc_UChar *paramsPtr,
    xdc_SizeT prm_size,
    xdc_runtime_Error_Block *eb)
{
    const ti_sysbios_knl_Semaphore_Args__create *__args = req_args;
    ti_sysbios_knl_Semaphore_Params instPrms;
    ti_sysbios_knl_Semaphore_Object *objp;

    /* common instance initialization */
    objp = xdc_runtime_Core_createObject__I(&ti_sysbios_knl_Semaphore_Object__DESC__C, NULL, &instPrms, (xdc_CPtr)paramsPtr, 0, eb);
    if (objp == NULL) {
        return NULL;
    }


    /* module-specific initialization */
    ti_sysbios_knl_Semaphore_Instance_init__E(objp, __args->count, &instPrms);
    return objp;
}

/* create */
ti_sysbios_knl_Semaphore_Handle ti_sysbios_knl_Semaphore_create( xdc_Int count, const ti_sysbios_knl_Semaphore_Params *paramsPtr, xdc_runtime_Error_Block *eb)
{
    ti_sysbios_knl_Semaphore_Params prms;
    ti_sysbios_knl_Semaphore_Object *obj;


    /* common instance initialization */
    obj = xdc_runtime_Core_createObject__I(&ti_sysbios_knl_Semaphore_Object__DESC__C, NULL, &prms, (xdc_CPtr)paramsPtr, 0, eb);
    if (obj == NULL) {
        return NULL;
    }

    /* module-specific initialization */
    ti_sysbios_knl_Semaphore_Instance_init__E(obj, count, &prms);
    return obj;
}

/* construct */
void ti_sysbios_knl_Semaphore_construct(ti_sysbios_knl_Semaphore_Struct *objp, xdc_Int count, const ti_sysbios_knl_Semaphore_Params *paramsPtr)
{
    ti_sysbios_knl_Semaphore_Params instPrms;

    /* common instance initialization */
    (Void)xdc_runtime_Core_constructObject__I(&ti_sysbios_knl_Semaphore_Object__DESC__C, objp, &instPrms, (xdc_CPtr)paramsPtr, 0, NULL);

    /* module-specific initialization */
    ti_sysbios_knl_Semaphore_Instance_init__E((xdc_Ptr)objp, count, &instPrms);
}

/* destruct */
void ti_sysbios_knl_Semaphore_destruct(ti_sysbios_knl_Semaphore_Struct *obj)
{
    xdc_runtime_Core_deleteObject__I(&ti_sysbios_knl_Semaphore_Object__DESC__C, obj, (xdc_Fxn)ti_sysbios_knl_Semaphore_Instance_finalize__E, xdc_runtime_Core_NOSTATE, TRUE);
}

/* Object__delete__S */
xdc_Void ti_sysbios_knl_Semaphore_Object__delete__S( xdc_Ptr instp ) 
{
    xdc_runtime_Core_deleteObject__I(&ti_sysbios_knl_Semaphore_Object__DESC__C, *((ti_sysbios_knl_Semaphore_Object**)instp), (xdc_Fxn)ti_sysbios_knl_Semaphore_Instance_finalize__E, xdc_runtime_Core_NOSTATE, FALSE);
    *((ti_sysbios_knl_Semaphore_Handle*)instp) = NULL;
}

/* delete */
void ti_sysbios_knl_Semaphore_delete(ti_sysbios_knl_Semaphore_Handle *instp)
{
    ti_sysbios_knl_Semaphore_Object__delete__S(instp);
}


/*
 * ======== ti.sysbios.knl.Swi SYSTEM FUNCTIONS ========
 */

/* per-module runtime symbols */
#undef Module__MID
#define Module__MID ti_sysbios_knl_Swi_Module__id__C

#undef Module__DGSINCL
#define Module__DGSINCL ti_sysbios_knl_Swi_Module__diagsIncluded__C

#undef Module__DGSENAB
#define Module__DGSENAB ti_sysbios_knl_Swi_Module__diagsEnabled__C

#undef Module__DGSMASK
#define Module__DGSMASK ti_sysbios_knl_Swi_Module__diagsMask__C

#undef Module__LOGDEF
#define Module__LOGDEF ti_sysbios_knl_Swi_Module__loggerDefined__C

#undef Module__LOGOBJ
#define Module__LOGOBJ ti_sysbios_knl_Swi_Module__loggerObj__C

#undef Module__LOGFXN0
#define Module__LOGFXN0 ti_sysbios_knl_Swi_Module__loggerFxn0__C

#undef Module__LOGFXN1
#define Module__LOGFXN1 ti_sysbios_knl_Swi_Module__loggerFxn1__C

#undef Module__LOGFXN2
#define Module__LOGFXN2 ti_sysbios_knl_Swi_Module__loggerFxn2__C

#undef Module__LOGFXN4
#define Module__LOGFXN4 ti_sysbios_knl_Swi_Module__loggerFxn4__C

#undef Module__LOGFXN8
#define Module__LOGFXN8 ti_sysbios_knl_Swi_Module__loggerFxn8__C

#undef Module__G_OBJ
#define Module__G_OBJ ti_sysbios_knl_Swi_Module__gateObj__C

#undef Module__G_PRMS
#define Module__G_PRMS ti_sysbios_knl_Swi_Module__gatePrms__C

#undef Module__GP_create
#define Module__GP_create ti_sysbios_knl_Swi_Module_GateProxy_create
#undef Module__GP_delete
#define Module__GP_delete ti_sysbios_knl_Swi_Module_GateProxy_delete
#undef Module__GP_enter
#define Module__GP_enter ti_sysbios_knl_Swi_Module_GateProxy_enter
#undef Module__GP_leave
#define Module__GP_leave ti_sysbios_knl_Swi_Module_GateProxy_leave
#undef Module__GP_query
#define Module__GP_query ti_sysbios_knl_Swi_Module_GateProxy_query

/* Module__startupDone__S */
xdc_Bool ti_sysbios_knl_Swi_Module__startupDone__S( void )
{
    return ti_sysbios_knl_Swi_Module__startupDone__F();
}

/* Handle__label__S */
xdc_runtime_Types_Label *ti_sysbios_knl_Swi_Handle__label__S(xdc_Ptr obj, xdc_runtime_Types_Label *lab) 
{
    lab->handle = obj;
    lab->modId = 32806;
    lab->named = FALSE;
    lab->iname = xdc_runtime_Text_nameUnknown;

    return lab;
}

/* Params__init__S */
xdc_Void ti_sysbios_knl_Swi_Params__init__S( xdc_Ptr prms, const void *src, xdc_SizeT psz, xdc_SizeT isz ) 
{
    xdc_runtime_Core_assignParams__I(prms, (xdc_CPtr)(src ? src : &ti_sysbios_knl_Swi_Object__PARAMS__C), psz, isz);
}

/* Object__get__S */
xdc_Ptr ti_sysbios_knl_Swi_Object__get__S(xdc_Ptr oa, xdc_Int i)
{
    if (oa != NULL) {
        return ((ti_sysbios_knl_Swi_Object__ *)oa) + i;
    }

    /* the bogus 'const' in the cast suppresses Klocwork MISRA complaints */
    return ((ti_sysbios_knl_Swi_Object__ * const)ti_sysbios_knl_Swi_Object__table__C) + i;
}

/* Object__first__S */
xdc_Ptr ti_sysbios_knl_Swi_Object__first__S( void ) 
{
    xdc_runtime_Types_InstHdr *iHdr = (xdc_runtime_Types_InstHdr *)ti_sysbios_knl_Swi_Module__root__V.link.next;

    if (iHdr != (xdc_runtime_Types_InstHdr *)&ti_sysbios_knl_Swi_Module__root__V.link) {
        return iHdr + 1;
    }
    else {
        return NULL;
    }
}

/* Object__next__S */
xdc_Ptr ti_sysbios_knl_Swi_Object__next__S( xdc_Ptr obj )
{
    xdc_runtime_Types_InstHdr *iHdr = ((xdc_runtime_Types_InstHdr *)obj) - 1;

    if (iHdr->link.next != (xdc_runtime_Types_Link *)&ti_sysbios_knl_Swi_Module__root__V.link) {
        return (xdc_runtime_Types_InstHdr *)(iHdr->link.next) + 1;
    }
    else {
        return NULL;
    }
}

/* Object__create__S */
xdc_Ptr ti_sysbios_knl_Swi_Object__create__S(
    xdc_CPtr req_args,
    const xdc_UChar *paramsPtr,
    xdc_SizeT prm_size,
    xdc_runtime_Error_Block *eb)
{
    const ti_sysbios_knl_Swi_Args__create *__args = req_args;
    ti_sysbios_knl_Swi_Params instPrms;
    ti_sysbios_knl_Swi_Object *objp;
    int iStat;

    /* common instance initialization */
    objp = xdc_runtime_Core_createObject__I(&ti_sysbios_knl_Swi_Object__DESC__C, NULL, &instPrms, (xdc_CPtr)paramsPtr, 0, eb);
    if (objp == NULL) {
        return NULL;
    }


    /* module-specific initialization */
    iStat = ti_sysbios_knl_Swi_Instance_init__E(objp, __args->swiFxn, &instPrms, eb);
    if (iStat) {
        xdc_runtime_Core_deleteObject__I(&ti_sysbios_knl_Swi_Object__DESC__C, objp, (xdc_Fxn)ti_sysbios_knl_Swi_Instance_finalize__E, iStat, 0);
        return NULL;
    }

    return objp;
}

/* create */
ti_sysbios_knl_Swi_Handle ti_sysbios_knl_Swi_create( ti_sysbios_knl_Swi_FuncPtr swiFxn, const ti_sysbios_knl_Swi_Params *paramsPtr, xdc_runtime_Error_Block *eb)
{
    ti_sysbios_knl_Swi_Params prms;
    ti_sysbios_knl_Swi_Object *obj;

    int iStat;

    /* common instance initialization */
    obj = xdc_runtime_Core_createObject__I(&ti_sysbios_knl_Swi_Object__DESC__C, NULL, &prms, (xdc_CPtr)paramsPtr, 0, eb);
    if (obj == NULL) {
        return NULL;
    }

    /* module-specific initialization */
    iStat = ti_sysbios_knl_Swi_Instance_init__E(obj, swiFxn, &prms, eb);
    if (iStat) {
        xdc_runtime_Core_deleteObject__I(&ti_sysbios_knl_Swi_Object__DESC__C, obj, (xdc_Fxn)ti_sysbios_knl_Swi_Instance_finalize__E, iStat, 0);
        return NULL;
    }

    return obj;
}

/* construct */
void ti_sysbios_knl_Swi_construct(ti_sysbios_knl_Swi_Struct *objp, ti_sysbios_knl_Swi_FuncPtr swiFxn, const ti_sysbios_knl_Swi_Params *paramsPtr, xdc_runtime_Error_Block *eb)
{
    ti_sysbios_knl_Swi_Params instPrms;
    int iStat;

    /* common instance initialization */
    (Void)xdc_runtime_Core_constructObject__I(&ti_sysbios_knl_Swi_Object__DESC__C, objp, &instPrms, (xdc_CPtr)paramsPtr, 0, eb);

    /* module-specific initialization */
    iStat = ti_sysbios_knl_Swi_Instance_init__E((xdc_Ptr)objp, swiFxn, &instPrms, eb);
    if (iStat) {
        xdc_runtime_Core_deleteObject__I(&ti_sysbios_knl_Swi_Object__DESC__C, objp, (xdc_Fxn)ti_sysbios_knl_Swi_Instance_finalize__E, iStat, 1);
    }

}

/* destruct */
void ti_sysbios_knl_Swi_destruct(ti_sysbios_knl_Swi_Struct *obj)
{
    xdc_runtime_Core_deleteObject__I(&ti_sysbios_knl_Swi_Object__DESC__C, obj, (xdc_Fxn)ti_sysbios_knl_Swi_Instance_finalize__E, 0, TRUE);
}

/* Object__delete__S */
xdc_Void ti_sysbios_knl_Swi_Object__delete__S( xdc_Ptr instp ) 
{
    xdc_runtime_Core_deleteObject__I(&ti_sysbios_knl_Swi_Object__DESC__C, *((ti_sysbios_knl_Swi_Object**)instp), (xdc_Fxn)ti_sysbios_knl_Swi_Instance_finalize__E, 0, FALSE);
    *((ti_sysbios_knl_Swi_Handle*)instp) = NULL;
}

/* delete */
void ti_sysbios_knl_Swi_delete(ti_sysbios_knl_Swi_Handle *instp)
{
    ti_sysbios_knl_Swi_Object__delete__S(instp);
}


/*
 * ======== ti.sysbios.knl.Task SYSTEM FUNCTIONS ========
 */

/* per-module runtime symbols */
#undef Module__MID
#define Module__MID ti_sysbios_knl_Task_Module__id__C

#undef Module__DGSINCL
#define Module__DGSINCL ti_sysbios_knl_Task_Module__diagsIncluded__C

#undef Module__DGSENAB
#define Module__DGSENAB ti_sysbios_knl_Task_Module__diagsEnabled__C

#undef Module__DGSMASK
#define Module__DGSMASK ti_sysbios_knl_Task_Module__diagsMask__C

#undef Module__LOGDEF
#define Module__LOGDEF ti_sysbios_knl_Task_Module__loggerDefined__C

#undef Module__LOGOBJ
#define Module__LOGOBJ ti_sysbios_knl_Task_Module__loggerObj__C

#undef Module__LOGFXN0
#define Module__LOGFXN0 ti_sysbios_knl_Task_Module__loggerFxn0__C

#undef Module__LOGFXN1
#define Module__LOGFXN1 ti_sysbios_knl_Task_Module__loggerFxn1__C

#undef Module__LOGFXN2
#define Module__LOGFXN2 ti_sysbios_knl_Task_Module__loggerFxn2__C

#undef Module__LOGFXN4
#define Module__LOGFXN4 ti_sysbios_knl_Task_Module__loggerFxn4__C

#undef Module__LOGFXN8
#define Module__LOGFXN8 ti_sysbios_knl_Task_Module__loggerFxn8__C

#undef Module__G_OBJ
#define Module__G_OBJ ti_sysbios_knl_Task_Module__gateObj__C

#undef Module__G_PRMS
#define Module__G_PRMS ti_sysbios_knl_Task_Module__gatePrms__C

#undef Module__GP_create
#define Module__GP_create ti_sysbios_knl_Task_Module_GateProxy_create
#undef Module__GP_delete
#define Module__GP_delete ti_sysbios_knl_Task_Module_GateProxy_delete
#undef Module__GP_enter
#define Module__GP_enter ti_sysbios_knl_Task_Module_GateProxy_enter
#undef Module__GP_leave
#define Module__GP_leave ti_sysbios_knl_Task_Module_GateProxy_leave
#undef Module__GP_query
#define Module__GP_query ti_sysbios_knl_Task_Module_GateProxy_query

/* Module__startupDone__S */
xdc_Bool ti_sysbios_knl_Task_Module__startupDone__S( void )
{
    return ti_sysbios_knl_Task_Module__startupDone__F();
}

/* Handle__label__S */
xdc_runtime_Types_Label *ti_sysbios_knl_Task_Handle__label__S(xdc_Ptr obj, xdc_runtime_Types_Label *lab) 
{
    lab->handle = obj;
    lab->modId = 32807;
    lab->named = FALSE;
    lab->iname = xdc_runtime_Text_nameUnknown;

    return lab;
}

/* Params__init__S */
xdc_Void ti_sysbios_knl_Task_Params__init__S( xdc_Ptr prms, const void *src, xdc_SizeT psz, xdc_SizeT isz ) 
{
    xdc_runtime_Core_assignParams__I(prms, (xdc_CPtr)(src ? src : &ti_sysbios_knl_Task_Object__PARAMS__C), psz, isz);
}

/* Object__get__S */
xdc_Ptr ti_sysbios_knl_Task_Object__get__S(xdc_Ptr oa, xdc_Int i)
{
    if (oa != NULL) {
        return ((ti_sysbios_knl_Task_Object__ *)oa) + i;
    }

    /* the bogus 'const' in the cast suppresses Klocwork MISRA complaints */
    return ((ti_sysbios_knl_Task_Object__ * const)ti_sysbios_knl_Task_Object__table__C) + i;
}

/* Object__first__S */
xdc_Ptr ti_sysbios_knl_Task_Object__first__S( void ) 
{
    xdc_runtime_Types_InstHdr *iHdr = (xdc_runtime_Types_InstHdr *)ti_sysbios_knl_Task_Module__root__V.link.next;

    if (iHdr != (xdc_runtime_Types_InstHdr *)&ti_sysbios_knl_Task_Module__root__V.link) {
        return iHdr + 1;
    }
    else {
        return NULL;
    }
}

/* Object__next__S */
xdc_Ptr ti_sysbios_knl_Task_Object__next__S( xdc_Ptr obj )
{
    xdc_runtime_Types_InstHdr *iHdr = ((xdc_runtime_Types_InstHdr *)obj) - 1;

    if (iHdr->link.next != (xdc_runtime_Types_Link *)&ti_sysbios_knl_Task_Module__root__V.link) {
        return (xdc_runtime_Types_InstHdr *)(iHdr->link.next) + 1;
    }
    else {
        return NULL;
    }
}

/* Object__create__S */
xdc_Ptr ti_sysbios_knl_Task_Object__create__S(
    xdc_CPtr req_args,
    const xdc_UChar *paramsPtr,
    xdc_SizeT prm_size,
    xdc_runtime_Error_Block *eb)
{
    const ti_sysbios_knl_Task_Args__create *__args = req_args;
    ti_sysbios_knl_Task_Params instPrms;
    ti_sysbios_knl_Task_Object *objp;
    int iStat;

    /* common instance initialization */
    objp = xdc_runtime_Core_createObject__I(&ti_sysbios_knl_Task_Object__DESC__C, NULL, &instPrms, (xdc_CPtr)paramsPtr, 0, eb);
    if (objp == NULL) {
        return NULL;
    }


    /* module-specific initialization */
    iStat = ti_sysbios_knl_Task_Instance_init__E(objp, __args->fxn, &instPrms, eb);
    if (iStat) {
        xdc_runtime_Core_deleteObject__I(&ti_sysbios_knl_Task_Object__DESC__C, objp, (xdc_Fxn)ti_sysbios_knl_Task_Instance_finalize__E, iStat, 0);
        return NULL;
    }

    return objp;
}

/* create */
ti_sysbios_knl_Task_Handle ti_sysbios_knl_Task_create( ti_sysbios_knl_Task_FuncPtr fxn, const ti_sysbios_knl_Task_Params *paramsPtr, xdc_runtime_Error_Block *eb)
{
    ti_sysbios_knl_Task_Params prms;
    ti_sysbios_knl_Task_Object *obj;

    int iStat;

    /* common instance initialization */
    obj = xdc_runtime_Core_createObject__I(&ti_sysbios_knl_Task_Object__DESC__C, NULL, &prms, (xdc_CPtr)paramsPtr, 0, eb);
    if (obj == NULL) {
        return NULL;
    }

    /* module-specific initialization */
    iStat = ti_sysbios_knl_Task_Instance_init__E(obj, fxn, &prms, eb);
    if (iStat) {
        xdc_runtime_Core_deleteObject__I(&ti_sysbios_knl_Task_Object__DESC__C, obj, (xdc_Fxn)ti_sysbios_knl_Task_Instance_finalize__E, iStat, 0);
        return NULL;
    }

    return obj;
}

/* construct */
void ti_sysbios_knl_Task_construct(ti_sysbios_knl_Task_Struct *objp, ti_sysbios_knl_Task_FuncPtr fxn, const ti_sysbios_knl_Task_Params *paramsPtr, xdc_runtime_Error_Block *eb)
{
    ti_sysbios_knl_Task_Params instPrms;
    int iStat;

    /* common instance initialization */
    (Void)xdc_runtime_Core_constructObject__I(&ti_sysbios_knl_Task_Object__DESC__C, objp, &instPrms, (xdc_CPtr)paramsPtr, 0, eb);

    /* module-specific initialization */
    iStat = ti_sysbios_knl_Task_Instance_init__E((xdc_Ptr)objp, fxn, &instPrms, eb);
    if (iStat) {
        xdc_runtime_Core_deleteObject__I(&ti_sysbios_knl_Task_Object__DESC__C, objp, (xdc_Fxn)ti_sysbios_knl_Task_Instance_finalize__E, iStat, 1);
    }

}

/* destruct */
void ti_sysbios_knl_Task_destruct(ti_sysbios_knl_Task_Struct *obj)
{
    xdc_runtime_Core_deleteObject__I(&ti_sysbios_knl_Task_Object__DESC__C, obj, (xdc_Fxn)ti_sysbios_knl_Task_Instance_finalize__E, 0, TRUE);
}

/* Object__delete__S */
xdc_Void ti_sysbios_knl_Task_Object__delete__S( xdc_Ptr instp ) 
{
    xdc_runtime_Core_deleteObject__I(&ti_sysbios_knl_Task_Object__DESC__C, *((ti_sysbios_knl_Task_Object**)instp), (xdc_Fxn)ti_sysbios_knl_Task_Instance_finalize__E, 0, FALSE);
    *((ti_sysbios_knl_Task_Handle*)instp) = NULL;
}

/* delete */
void ti_sysbios_knl_Task_delete(ti_sysbios_knl_Task_Handle *instp)
{
    ti_sysbios_knl_Task_Object__delete__S(instp);
}


/*
 * ======== ti.sysbios.knl.Task_SupportProxy SYSTEM FUNCTIONS ========
 */


xdc_Bool ti_sysbios_knl_Task_SupportProxy_Proxy__abstract__S(void)
{
    return 0;
}
xdc_CPtr ti_sysbios_knl_Task_SupportProxy_Proxy__delegate__S(void)
{
    return 0;
}


/*
 * ======== ti.sysbios.syncs.SyncSem SYSTEM FUNCTIONS ========
 */

/* per-module runtime symbols */
#undef Module__MID
#define Module__MID ti_sysbios_syncs_SyncSem_Module__id__C

#undef Module__DGSINCL
#define Module__DGSINCL ti_sysbios_syncs_SyncSem_Module__diagsIncluded__C

#undef Module__DGSENAB
#define Module__DGSENAB ti_sysbios_syncs_SyncSem_Module__diagsEnabled__C

#undef Module__DGSMASK
#define Module__DGSMASK ti_sysbios_syncs_SyncSem_Module__diagsMask__C

#undef Module__LOGDEF
#define Module__LOGDEF ti_sysbios_syncs_SyncSem_Module__loggerDefined__C

#undef Module__LOGOBJ
#define Module__LOGOBJ ti_sysbios_syncs_SyncSem_Module__loggerObj__C

#undef Module__LOGFXN0
#define Module__LOGFXN0 ti_sysbios_syncs_SyncSem_Module__loggerFxn0__C

#undef Module__LOGFXN1
#define Module__LOGFXN1 ti_sysbios_syncs_SyncSem_Module__loggerFxn1__C

#undef Module__LOGFXN2
#define Module__LOGFXN2 ti_sysbios_syncs_SyncSem_Module__loggerFxn2__C

#undef Module__LOGFXN4
#define Module__LOGFXN4 ti_sysbios_syncs_SyncSem_Module__loggerFxn4__C

#undef Module__LOGFXN8
#define Module__LOGFXN8 ti_sysbios_syncs_SyncSem_Module__loggerFxn8__C

#undef Module__G_OBJ
#define Module__G_OBJ ti_sysbios_syncs_SyncSem_Module__gateObj__C

#undef Module__G_PRMS
#define Module__G_PRMS ti_sysbios_syncs_SyncSem_Module__gatePrms__C

#undef Module__GP_create
#define Module__GP_create ti_sysbios_syncs_SyncSem_Module_GateProxy_create
#undef Module__GP_delete
#define Module__GP_delete ti_sysbios_syncs_SyncSem_Module_GateProxy_delete
#undef Module__GP_enter
#define Module__GP_enter ti_sysbios_syncs_SyncSem_Module_GateProxy_enter
#undef Module__GP_leave
#define Module__GP_leave ti_sysbios_syncs_SyncSem_Module_GateProxy_leave
#undef Module__GP_query
#define Module__GP_query ti_sysbios_syncs_SyncSem_Module_GateProxy_query

/* Module__startupDone__S */
xdc_Bool ti_sysbios_syncs_SyncSem_Module__startupDone__S( void )
{
    return 1;
}

/* Handle__label__S */
xdc_runtime_Types_Label *ti_sysbios_syncs_SyncSem_Handle__label__S(xdc_Ptr obj, xdc_runtime_Types_Label *lab) 
{
    lab->handle = obj;
    lab->modId = 32820;
    lab->named = FALSE;
    lab->iname = xdc_runtime_Text_nameUnknown;

    return lab;
}

/* Params__init__S */
xdc_Void ti_sysbios_syncs_SyncSem_Params__init__S( xdc_Ptr prms, const void *src, xdc_SizeT psz, xdc_SizeT isz ) 
{
    xdc_runtime_Core_assignParams__I(prms, (xdc_CPtr)(src ? src : &ti_sysbios_syncs_SyncSem_Object__PARAMS__C), psz, isz);
}

/* Object__get__S */
xdc_Ptr ti_sysbios_syncs_SyncSem_Object__get__S(xdc_Ptr oa, xdc_Int i)
{
    if (oa != NULL) {
        return ((ti_sysbios_syncs_SyncSem_Object__ *)oa) + i;
    }

    return NULL;
}


/* Object__first__S */
xdc_Ptr ti_sysbios_syncs_SyncSem_Object__first__S( void ) 
{
    xdc_runtime_Types_InstHdr *iHdr = (xdc_runtime_Types_InstHdr *)ti_sysbios_syncs_SyncSem_Module__root__V.link.next;

    if (iHdr != (xdc_runtime_Types_InstHdr *)&ti_sysbios_syncs_SyncSem_Module__root__V.link) {
        return iHdr + 1;
    }
    else {
        return NULL;
    }
}

/* Object__next__S */
xdc_Ptr ti_sysbios_syncs_SyncSem_Object__next__S( xdc_Ptr obj )
{
    xdc_runtime_Types_InstHdr *iHdr = ((xdc_runtime_Types_InstHdr *)obj) - 1;

    if (iHdr->link.next != (xdc_runtime_Types_Link *)&ti_sysbios_syncs_SyncSem_Module__root__V.link) {
        return (xdc_runtime_Types_InstHdr *)(iHdr->link.next) + 1;
    }
    else {
        return NULL;
    }
}

/* Object__create__S */
xdc_Ptr ti_sysbios_syncs_SyncSem_Object__create__S(
    xdc_CPtr req_args,
    const xdc_UChar *paramsPtr,
    xdc_SizeT prm_size,
    xdc_runtime_Error_Block *eb)
{
    ti_sysbios_syncs_SyncSem_Params instPrms;
    ti_sysbios_syncs_SyncSem_Object *objp;
    int iStat;

    /* common instance initialization */
    objp = xdc_runtime_Core_createObject__I(&ti_sysbios_syncs_SyncSem_Object__DESC__C, NULL, &instPrms, (xdc_CPtr)paramsPtr, 0, eb);
    if (objp == NULL) {
        return NULL;
    }


    /* module-specific initialization */
    iStat = ti_sysbios_syncs_SyncSem_Instance_init__E(objp, &instPrms, eb);
    if (iStat) {
        xdc_runtime_Core_deleteObject__I(&ti_sysbios_syncs_SyncSem_Object__DESC__C, objp, (xdc_Fxn)ti_sysbios_syncs_SyncSem_Instance_finalize__E, iStat, 0);
        return NULL;
    }

    return objp;
}

/* create */
ti_sysbios_syncs_SyncSem_Handle ti_sysbios_syncs_SyncSem_create( const ti_sysbios_syncs_SyncSem_Params *paramsPtr, xdc_runtime_Error_Block *eb)
{
    ti_sysbios_syncs_SyncSem_Params prms;
    ti_sysbios_syncs_SyncSem_Object *obj;

    int iStat;

    /* common instance initialization */
    obj = xdc_runtime_Core_createObject__I(&ti_sysbios_syncs_SyncSem_Object__DESC__C, NULL, &prms, (xdc_CPtr)paramsPtr, 0, eb);
    if (obj == NULL) {
        return NULL;
    }

    /* module-specific initialization */
    iStat = ti_sysbios_syncs_SyncSem_Instance_init__E(obj, &prms, eb);
    if (iStat) {
        xdc_runtime_Core_deleteObject__I(&ti_sysbios_syncs_SyncSem_Object__DESC__C, obj, (xdc_Fxn)ti_sysbios_syncs_SyncSem_Instance_finalize__E, iStat, 0);
        return NULL;
    }

    return obj;
}

/* construct */
void ti_sysbios_syncs_SyncSem_construct(ti_sysbios_syncs_SyncSem_Struct *objp, const ti_sysbios_syncs_SyncSem_Params *paramsPtr, xdc_runtime_Error_Block *eb)
{
    ti_sysbios_syncs_SyncSem_Params instPrms;
    int iStat;

    /* common instance initialization */
    (Void)xdc_runtime_Core_constructObject__I(&ti_sysbios_syncs_SyncSem_Object__DESC__C, objp, &instPrms, (xdc_CPtr)paramsPtr, 0, eb);

    /* module-specific initialization */
    iStat = ti_sysbios_syncs_SyncSem_Instance_init__E((xdc_Ptr)objp, &instPrms, eb);
    if (iStat) {
        xdc_runtime_Core_deleteObject__I(&ti_sysbios_syncs_SyncSem_Object__DESC__C, objp, (xdc_Fxn)ti_sysbios_syncs_SyncSem_Instance_finalize__E, iStat, 1);
    }

}

/* destruct */
void ti_sysbios_syncs_SyncSem_destruct(ti_sysbios_syncs_SyncSem_Struct *obj)
{
    xdc_runtime_Core_deleteObject__I(&ti_sysbios_syncs_SyncSem_Object__DESC__C, obj, (xdc_Fxn)ti_sysbios_syncs_SyncSem_Instance_finalize__E, 0, TRUE);
}

/* Object__delete__S */
xdc_Void ti_sysbios_syncs_SyncSem_Object__delete__S( xdc_Ptr instp ) 
{
    xdc_runtime_Core_deleteObject__I(&ti_sysbios_syncs_SyncSem_Object__DESC__C, *((ti_sysbios_syncs_SyncSem_Object**)instp), (xdc_Fxn)ti_sysbios_syncs_SyncSem_Instance_finalize__E, 0, FALSE);
    *((ti_sysbios_syncs_SyncSem_Handle*)instp) = NULL;
}

/* delete */
void ti_sysbios_syncs_SyncSem_delete(ti_sysbios_syncs_SyncSem_Handle *instp)
{
    ti_sysbios_syncs_SyncSem_Object__delete__S(instp);
}


/*
 * ======== ti.sysbios.timers.dmtimer.Timer SYSTEM FUNCTIONS ========
 */

/* per-module runtime symbols */
#undef Module__MID
#define Module__MID ti_sysbios_timers_dmtimer_Timer_Module__id__C

#undef Module__DGSINCL
#define Module__DGSINCL ti_sysbios_timers_dmtimer_Timer_Module__diagsIncluded__C

#undef Module__DGSENAB
#define Module__DGSENAB ti_sysbios_timers_dmtimer_Timer_Module__diagsEnabled__C

#undef Module__DGSMASK
#define Module__DGSMASK ti_sysbios_timers_dmtimer_Timer_Module__diagsMask__C

#undef Module__LOGDEF
#define Module__LOGDEF ti_sysbios_timers_dmtimer_Timer_Module__loggerDefined__C

#undef Module__LOGOBJ
#define Module__LOGOBJ ti_sysbios_timers_dmtimer_Timer_Module__loggerObj__C

#undef Module__LOGFXN0
#define Module__LOGFXN0 ti_sysbios_timers_dmtimer_Timer_Module__loggerFxn0__C

#undef Module__LOGFXN1
#define Module__LOGFXN1 ti_sysbios_timers_dmtimer_Timer_Module__loggerFxn1__C

#undef Module__LOGFXN2
#define Module__LOGFXN2 ti_sysbios_timers_dmtimer_Timer_Module__loggerFxn2__C

#undef Module__LOGFXN4
#define Module__LOGFXN4 ti_sysbios_timers_dmtimer_Timer_Module__loggerFxn4__C

#undef Module__LOGFXN8
#define Module__LOGFXN8 ti_sysbios_timers_dmtimer_Timer_Module__loggerFxn8__C

#undef Module__G_OBJ
#define Module__G_OBJ ti_sysbios_timers_dmtimer_Timer_Module__gateObj__C

#undef Module__G_PRMS
#define Module__G_PRMS ti_sysbios_timers_dmtimer_Timer_Module__gatePrms__C

#undef Module__GP_create
#define Module__GP_create ti_sysbios_timers_dmtimer_Timer_Module_GateProxy_create
#undef Module__GP_delete
#define Module__GP_delete ti_sysbios_timers_dmtimer_Timer_Module_GateProxy_delete
#undef Module__GP_enter
#define Module__GP_enter ti_sysbios_timers_dmtimer_Timer_Module_GateProxy_enter
#undef Module__GP_leave
#define Module__GP_leave ti_sysbios_timers_dmtimer_Timer_Module_GateProxy_leave
#undef Module__GP_query
#define Module__GP_query ti_sysbios_timers_dmtimer_Timer_Module_GateProxy_query

/* Module__startupDone__S */
xdc_Bool ti_sysbios_timers_dmtimer_Timer_Module__startupDone__S( void )
{
    return ti_sysbios_timers_dmtimer_Timer_Module__startupDone__F();
}

/* Handle__label__S */
xdc_runtime_Types_Label *ti_sysbios_timers_dmtimer_Timer_Handle__label__S(xdc_Ptr obj, xdc_runtime_Types_Label *lab) 
{
    lab->handle = obj;
    lab->modId = 32831;
    lab->named = FALSE;
    lab->iname = xdc_runtime_Text_nameUnknown;

    return lab;
}

/* Params__init__S */
xdc_Void ti_sysbios_timers_dmtimer_Timer_Params__init__S( xdc_Ptr prms, const void *src, xdc_SizeT psz, xdc_SizeT isz ) 
{
    xdc_runtime_Core_assignParams__I(prms, (xdc_CPtr)(src ? src : &ti_sysbios_timers_dmtimer_Timer_Object__PARAMS__C), psz, isz);
}

/* Object__get__S */
xdc_Ptr ti_sysbios_timers_dmtimer_Timer_Object__get__S(xdc_Ptr oa, xdc_Int i)
{
    if (oa != NULL) {
        return ((ti_sysbios_timers_dmtimer_Timer_Object__ *)oa) + i;
    }

    /* the bogus 'const' in the cast suppresses Klocwork MISRA complaints */
    return ((ti_sysbios_timers_dmtimer_Timer_Object__ * const)ti_sysbios_timers_dmtimer_Timer_Object__table__C) + i;
}

/* Object__first__S */
xdc_Ptr ti_sysbios_timers_dmtimer_Timer_Object__first__S( void ) 
{
    xdc_runtime_Types_InstHdr *iHdr = (xdc_runtime_Types_InstHdr *)ti_sysbios_timers_dmtimer_Timer_Module__root__V.link.next;

    if (iHdr != (xdc_runtime_Types_InstHdr *)&ti_sysbios_timers_dmtimer_Timer_Module__root__V.link) {
        return iHdr + 1;
    }
    else {
        return NULL;
    }
}

/* Object__next__S */
xdc_Ptr ti_sysbios_timers_dmtimer_Timer_Object__next__S( xdc_Ptr obj )
{
    xdc_runtime_Types_InstHdr *iHdr = ((xdc_runtime_Types_InstHdr *)obj) - 1;

    if (iHdr->link.next != (xdc_runtime_Types_Link *)&ti_sysbios_timers_dmtimer_Timer_Module__root__V.link) {
        return (xdc_runtime_Types_InstHdr *)(iHdr->link.next) + 1;
    }
    else {
        return NULL;
    }
}

/* Object__create__S */
xdc_Ptr ti_sysbios_timers_dmtimer_Timer_Object__create__S(
    xdc_CPtr req_args,
    const xdc_UChar *paramsPtr,
    xdc_SizeT prm_size,
    xdc_runtime_Error_Block *eb)
{
    const ti_sysbios_timers_dmtimer_Timer_Args__create *__args = req_args;
    ti_sysbios_timers_dmtimer_Timer_Params instPrms;
    ti_sysbios_timers_dmtimer_Timer_Object *objp;
    int iStat;

    /* common instance initialization */
    objp = xdc_runtime_Core_createObject__I(&ti_sysbios_timers_dmtimer_Timer_Object__DESC__C, NULL, &instPrms, (xdc_CPtr)paramsPtr, 0, eb);
    if (objp == NULL) {
        return NULL;
    }


    /* module-specific initialization */
    iStat = ti_sysbios_timers_dmtimer_Timer_Instance_init__E(objp, __args->id, __args->tickFxn, &instPrms, eb);
    if (iStat) {
        xdc_runtime_Core_deleteObject__I(&ti_sysbios_timers_dmtimer_Timer_Object__DESC__C, objp, (xdc_Fxn)ti_sysbios_timers_dmtimer_Timer_Instance_finalize__E, iStat, 0);
        return NULL;
    }

    return objp;
}

/* create */
ti_sysbios_timers_dmtimer_Timer_Handle ti_sysbios_timers_dmtimer_Timer_create( xdc_Int id, ti_sysbios_interfaces_ITimer_FuncPtr tickFxn, const ti_sysbios_timers_dmtimer_Timer_Params *paramsPtr, xdc_runtime_Error_Block *eb)
{
    ti_sysbios_timers_dmtimer_Timer_Params prms;
    ti_sysbios_timers_dmtimer_Timer_Object *obj;

    int iStat;

    /* common instance initialization */
    obj = xdc_runtime_Core_createObject__I(&ti_sysbios_timers_dmtimer_Timer_Object__DESC__C, NULL, &prms, (xdc_CPtr)paramsPtr, 0, eb);
    if (obj == NULL) {
        return NULL;
    }

    /* module-specific initialization */
    iStat = ti_sysbios_timers_dmtimer_Timer_Instance_init__E(obj, id, tickFxn, &prms, eb);
    if (iStat) {
        xdc_runtime_Core_deleteObject__I(&ti_sysbios_timers_dmtimer_Timer_Object__DESC__C, obj, (xdc_Fxn)ti_sysbios_timers_dmtimer_Timer_Instance_finalize__E, iStat, 0);
        return NULL;
    }

    return obj;
}

/* construct */
void ti_sysbios_timers_dmtimer_Timer_construct(ti_sysbios_timers_dmtimer_Timer_Struct *objp, xdc_Int id, ti_sysbios_interfaces_ITimer_FuncPtr tickFxn, const ti_sysbios_timers_dmtimer_Timer_Params *paramsPtr, xdc_runtime_Error_Block *eb)
{
    ti_sysbios_timers_dmtimer_Timer_Params instPrms;
    int iStat;

    /* common instance initialization */
    (Void)xdc_runtime_Core_constructObject__I(&ti_sysbios_timers_dmtimer_Timer_Object__DESC__C, objp, &instPrms, (xdc_CPtr)paramsPtr, 0, eb);

    /* module-specific initialization */
    iStat = ti_sysbios_timers_dmtimer_Timer_Instance_init__E((xdc_Ptr)objp, id, tickFxn, &instPrms, eb);
    if (iStat) {
        xdc_runtime_Core_deleteObject__I(&ti_sysbios_timers_dmtimer_Timer_Object__DESC__C, objp, (xdc_Fxn)ti_sysbios_timers_dmtimer_Timer_Instance_finalize__E, iStat, 1);
    }

}

/* destruct */
void ti_sysbios_timers_dmtimer_Timer_destruct(ti_sysbios_timers_dmtimer_Timer_Struct *obj)
{
    xdc_runtime_Core_deleteObject__I(&ti_sysbios_timers_dmtimer_Timer_Object__DESC__C, obj, (xdc_Fxn)ti_sysbios_timers_dmtimer_Timer_Instance_finalize__E, 0, TRUE);
}

/* Object__delete__S */
xdc_Void ti_sysbios_timers_dmtimer_Timer_Object__delete__S( xdc_Ptr instp ) 
{
    xdc_runtime_Core_deleteObject__I(&ti_sysbios_timers_dmtimer_Timer_Object__DESC__C, *((ti_sysbios_timers_dmtimer_Timer_Object**)instp), (xdc_Fxn)ti_sysbios_timers_dmtimer_Timer_Instance_finalize__E, 0, FALSE);
    *((ti_sysbios_timers_dmtimer_Timer_Handle*)instp) = NULL;
}

/* delete */
void ti_sysbios_timers_dmtimer_Timer_delete(ti_sysbios_timers_dmtimer_Timer_Handle *instp)
{
    ti_sysbios_timers_dmtimer_Timer_Object__delete__S(instp);
}


/*
 * ======== ti.sysbios.timers.dmtimer.Timer_TimerSupportProxy SYSTEM FUNCTIONS ========
 */


xdc_Bool ti_sysbios_timers_dmtimer_Timer_TimerSupportProxy_Proxy__abstract__S(void)
{
    return 0;
}
xdc_CPtr ti_sysbios_timers_dmtimer_Timer_TimerSupportProxy_Proxy__delegate__S(void)
{
    return (const void *)&ti_sysbios_family_c64p_tci6488_TimerSupport_Module__FXNS__C;
}


/*
 * ======== ti.sysbios.utils.Load SYSTEM FUNCTIONS ========
 */

/* Module__startupDone__S */
xdc_Bool ti_sysbios_utils_Load_Module__startupDone__S( void )
{
    return 1;
}



/*
 * ======== ti.sysbios.xdcruntime.GateThreadSupport SYSTEM FUNCTIONS ========
 */

/* per-module runtime symbols */
#undef Module__MID
#define Module__MID ti_sysbios_xdcruntime_GateThreadSupport_Module__id__C

#undef Module__DGSINCL
#define Module__DGSINCL ti_sysbios_xdcruntime_GateThreadSupport_Module__diagsIncluded__C

#undef Module__DGSENAB
#define Module__DGSENAB ti_sysbios_xdcruntime_GateThreadSupport_Module__diagsEnabled__C

#undef Module__DGSMASK
#define Module__DGSMASK ti_sysbios_xdcruntime_GateThreadSupport_Module__diagsMask__C

#undef Module__LOGDEF
#define Module__LOGDEF ti_sysbios_xdcruntime_GateThreadSupport_Module__loggerDefined__C

#undef Module__LOGOBJ
#define Module__LOGOBJ ti_sysbios_xdcruntime_GateThreadSupport_Module__loggerObj__C

#undef Module__LOGFXN0
#define Module__LOGFXN0 ti_sysbios_xdcruntime_GateThreadSupport_Module__loggerFxn0__C

#undef Module__LOGFXN1
#define Module__LOGFXN1 ti_sysbios_xdcruntime_GateThreadSupport_Module__loggerFxn1__C

#undef Module__LOGFXN2
#define Module__LOGFXN2 ti_sysbios_xdcruntime_GateThreadSupport_Module__loggerFxn2__C

#undef Module__LOGFXN4
#define Module__LOGFXN4 ti_sysbios_xdcruntime_GateThreadSupport_Module__loggerFxn4__C

#undef Module__LOGFXN8
#define Module__LOGFXN8 ti_sysbios_xdcruntime_GateThreadSupport_Module__loggerFxn8__C

#undef Module__G_OBJ
#define Module__G_OBJ ti_sysbios_xdcruntime_GateThreadSupport_Module__gateObj__C

#undef Module__G_PRMS
#define Module__G_PRMS ti_sysbios_xdcruntime_GateThreadSupport_Module__gatePrms__C

#undef Module__GP_create
#define Module__GP_create ti_sysbios_xdcruntime_GateThreadSupport_Module_GateProxy_create
#undef Module__GP_delete
#define Module__GP_delete ti_sysbios_xdcruntime_GateThreadSupport_Module_GateProxy_delete
#undef Module__GP_enter
#define Module__GP_enter ti_sysbios_xdcruntime_GateThreadSupport_Module_GateProxy_enter
#undef Module__GP_leave
#define Module__GP_leave ti_sysbios_xdcruntime_GateThreadSupport_Module_GateProxy_leave
#undef Module__GP_query
#define Module__GP_query ti_sysbios_xdcruntime_GateThreadSupport_Module_GateProxy_query

/* Module__startupDone__S */
xdc_Bool ti_sysbios_xdcruntime_GateThreadSupport_Module__startupDone__S( void )
{
    return 1;
}

/* Handle__label__S */
xdc_runtime_Types_Label *ti_sysbios_xdcruntime_GateThreadSupport_Handle__label__S(xdc_Ptr obj, xdc_runtime_Types_Label *lab) 
{
    lab->handle = obj;
    lab->modId = 32836;
    lab->named = FALSE;
    lab->iname = xdc_runtime_Text_nameUnknown;

    return lab;
}

/* Params__init__S */
xdc_Void ti_sysbios_xdcruntime_GateThreadSupport_Params__init__S( xdc_Ptr prms, const void *src, xdc_SizeT psz, xdc_SizeT isz ) 
{
    xdc_runtime_Core_assignParams__I(prms, (xdc_CPtr)(src ? src : &ti_sysbios_xdcruntime_GateThreadSupport_Object__PARAMS__C), psz, isz);
}

/* Object__get__S */
xdc_Ptr ti_sysbios_xdcruntime_GateThreadSupport_Object__get__S(xdc_Ptr oa, xdc_Int i)
{
    if (oa != NULL) {
        return ((ti_sysbios_xdcruntime_GateThreadSupport_Object__ *)oa) + i;
    }

    return NULL;
}


/* Object__first__S */
xdc_Ptr ti_sysbios_xdcruntime_GateThreadSupport_Object__first__S( void ) 
{
    xdc_runtime_Types_InstHdr *iHdr = (xdc_runtime_Types_InstHdr *)ti_sysbios_xdcruntime_GateThreadSupport_Module__root__V.link.next;

    if (iHdr != (xdc_runtime_Types_InstHdr *)&ti_sysbios_xdcruntime_GateThreadSupport_Module__root__V.link) {
        return iHdr + 1;
    }
    else {
        return NULL;
    }
}

/* Object__next__S */
xdc_Ptr ti_sysbios_xdcruntime_GateThreadSupport_Object__next__S( xdc_Ptr obj )
{
    xdc_runtime_Types_InstHdr *iHdr = ((xdc_runtime_Types_InstHdr *)obj) - 1;

    if (iHdr->link.next != (xdc_runtime_Types_Link *)&ti_sysbios_xdcruntime_GateThreadSupport_Module__root__V.link) {
        return (xdc_runtime_Types_InstHdr *)(iHdr->link.next) + 1;
    }
    else {
        return NULL;
    }
}

/* Object__create__S */
xdc_Ptr ti_sysbios_xdcruntime_GateThreadSupport_Object__create__S(
    xdc_CPtr req_args,
    const xdc_UChar *paramsPtr,
    xdc_SizeT prm_size,
    xdc_runtime_Error_Block *eb)
{
    ti_sysbios_xdcruntime_GateThreadSupport_Params instPrms;
    ti_sysbios_xdcruntime_GateThreadSupport_Object *objp;

    /* common instance initialization */
    objp = xdc_runtime_Core_createObject__I(&ti_sysbios_xdcruntime_GateThreadSupport_Object__DESC__C, NULL, &instPrms, (xdc_CPtr)paramsPtr, 0, eb);
    if (objp == NULL) {
        return NULL;
    }


    /* module-specific initialization */
    ti_sysbios_xdcruntime_GateThreadSupport_Instance_init__E(objp, &instPrms);
    return objp;
}

/* create */
ti_sysbios_xdcruntime_GateThreadSupport_Handle ti_sysbios_xdcruntime_GateThreadSupport_create( const ti_sysbios_xdcruntime_GateThreadSupport_Params *paramsPtr, xdc_runtime_Error_Block *eb)
{
    ti_sysbios_xdcruntime_GateThreadSupport_Params prms;
    ti_sysbios_xdcruntime_GateThreadSupport_Object *obj;


    /* common instance initialization */
    obj = xdc_runtime_Core_createObject__I(&ti_sysbios_xdcruntime_GateThreadSupport_Object__DESC__C, NULL, &prms, (xdc_CPtr)paramsPtr, 0, eb);
    if (obj == NULL) {
        return NULL;
    }

    /* module-specific initialization */
    ti_sysbios_xdcruntime_GateThreadSupport_Instance_init__E(obj, &prms);
    return obj;
}

/* construct */
void ti_sysbios_xdcruntime_GateThreadSupport_construct(ti_sysbios_xdcruntime_GateThreadSupport_Struct *objp, const ti_sysbios_xdcruntime_GateThreadSupport_Params *paramsPtr)
{
    ti_sysbios_xdcruntime_GateThreadSupport_Params instPrms;

    /* common instance initialization */
    (Void)xdc_runtime_Core_constructObject__I(&ti_sysbios_xdcruntime_GateThreadSupport_Object__DESC__C, objp, &instPrms, (xdc_CPtr)paramsPtr, 0, NULL);

    /* module-specific initialization */
    ti_sysbios_xdcruntime_GateThreadSupport_Instance_init__E((xdc_Ptr)objp, &instPrms);
}

/* destruct */
void ti_sysbios_xdcruntime_GateThreadSupport_destruct(ti_sysbios_xdcruntime_GateThreadSupport_Struct *obj)
{
    xdc_runtime_Core_deleteObject__I(&ti_sysbios_xdcruntime_GateThreadSupport_Object__DESC__C, obj, (xdc_Fxn)ti_sysbios_xdcruntime_GateThreadSupport_Instance_finalize__E, xdc_runtime_Core_NOSTATE, TRUE);
}

/* Object__delete__S */
xdc_Void ti_sysbios_xdcruntime_GateThreadSupport_Object__delete__S( xdc_Ptr instp ) 
{
    xdc_runtime_Core_deleteObject__I(&ti_sysbios_xdcruntime_GateThreadSupport_Object__DESC__C, *((ti_sysbios_xdcruntime_GateThreadSupport_Object**)instp), (xdc_Fxn)ti_sysbios_xdcruntime_GateThreadSupport_Instance_finalize__E, xdc_runtime_Core_NOSTATE, FALSE);
    *((ti_sysbios_xdcruntime_GateThreadSupport_Handle*)instp) = NULL;
}

/* delete */
void ti_sysbios_xdcruntime_GateThreadSupport_delete(ti_sysbios_xdcruntime_GateThreadSupport_Handle *instp)
{
    ti_sysbios_xdcruntime_GateThreadSupport_Object__delete__S(instp);
}


/*
 * ======== xdc.runtime.Assert SYSTEM FUNCTIONS ========
 */

/* Module__startupDone__S */
xdc_Bool xdc_runtime_Assert_Module__startupDone__S( void )
{
    return 1;
}



/*
 * ======== xdc.runtime.Core SYSTEM FUNCTIONS ========
 */

/* Module__startupDone__S */
xdc_Bool xdc_runtime_Core_Module__startupDone__S( void )
{
    return 1;
}



/*
 * ======== xdc.runtime.Defaults SYSTEM FUNCTIONS ========
 */

/* Module__startupDone__S */
xdc_Bool xdc_runtime_Defaults_Module__startupDone__S( void )
{
    return 1;
}



/*
 * ======== xdc.runtime.Diags SYSTEM FUNCTIONS ========
 */

/* Module__startupDone__S */
xdc_Bool xdc_runtime_Diags_Module__startupDone__S( void )
{
    return 1;
}



/*
 * ======== xdc.runtime.Error SYSTEM FUNCTIONS ========
 */

/* Module__startupDone__S */
xdc_Bool xdc_runtime_Error_Module__startupDone__S( void )
{
    return 1;
}



/*
 * ======== xdc.runtime.Gate SYSTEM FUNCTIONS ========
 */

/* Module__startupDone__S */
xdc_Bool xdc_runtime_Gate_Module__startupDone__S( void )
{
    return 1;
}



/*
 * ======== xdc.runtime.Log SYSTEM FUNCTIONS ========
 */

/* Module__startupDone__S */
xdc_Bool xdc_runtime_Log_Module__startupDone__S( void )
{
    return 1;
}



/*
 * ======== xdc.runtime.Main SYSTEM FUNCTIONS ========
 */

/* Module__startupDone__S */
xdc_Bool xdc_runtime_Main_Module__startupDone__S( void )
{
    return 1;
}



/*
 * ======== xdc.runtime.Main_Module_GateProxy SYSTEM FUNCTIONS ========
 */

/* per-module runtime symbols */
#undef Module__MID
#define Module__MID xdc_runtime_Main_Module_GateProxy_Module__id__C

#undef Module__DGSINCL
#define Module__DGSINCL xdc_runtime_Main_Module_GateProxy_Module__diagsIncluded__C

#undef Module__DGSENAB
#define Module__DGSENAB xdc_runtime_Main_Module_GateProxy_Module__diagsEnabled__C

#undef Module__DGSMASK
#define Module__DGSMASK xdc_runtime_Main_Module_GateProxy_Module__diagsMask__C

#undef Module__LOGDEF
#define Module__LOGDEF xdc_runtime_Main_Module_GateProxy_Module__loggerDefined__C

#undef Module__LOGOBJ
#define Module__LOGOBJ xdc_runtime_Main_Module_GateProxy_Module__loggerObj__C

#undef Module__LOGFXN0
#define Module__LOGFXN0 xdc_runtime_Main_Module_GateProxy_Module__loggerFxn0__C

#undef Module__LOGFXN1
#define Module__LOGFXN1 xdc_runtime_Main_Module_GateProxy_Module__loggerFxn1__C

#undef Module__LOGFXN2
#define Module__LOGFXN2 xdc_runtime_Main_Module_GateProxy_Module__loggerFxn2__C

#undef Module__LOGFXN4
#define Module__LOGFXN4 xdc_runtime_Main_Module_GateProxy_Module__loggerFxn4__C

#undef Module__LOGFXN8
#define Module__LOGFXN8 xdc_runtime_Main_Module_GateProxy_Module__loggerFxn8__C

#undef Module__G_OBJ
#define Module__G_OBJ xdc_runtime_Main_Module_GateProxy_Module__gateObj__C

#undef Module__G_PRMS
#define Module__G_PRMS xdc_runtime_Main_Module_GateProxy_Module__gatePrms__C

#undef Module__GP_create
#define Module__GP_create xdc_runtime_Main_Module_GateProxy_Module_GateProxy_create
#undef Module__GP_delete
#define Module__GP_delete xdc_runtime_Main_Module_GateProxy_Module_GateProxy_delete
#undef Module__GP_enter
#define Module__GP_enter xdc_runtime_Main_Module_GateProxy_Module_GateProxy_enter
#undef Module__GP_leave
#define Module__GP_leave xdc_runtime_Main_Module_GateProxy_Module_GateProxy_leave
#undef Module__GP_query
#define Module__GP_query xdc_runtime_Main_Module_GateProxy_Module_GateProxy_query

xdc_Bool xdc_runtime_Main_Module_GateProxy_Proxy__abstract__S(void)
{
    return 0;
}
xdc_CPtr xdc_runtime_Main_Module_GateProxy_Proxy__delegate__S(void)
{
    return (const void *)&ti_sysbios_gates_GateHwi_Module__FXNS__C;
}



/*
 * ======== xdc.runtime.Memory SYSTEM FUNCTIONS ========
 */

/* Module__startupDone__S */
xdc_Bool xdc_runtime_Memory_Module__startupDone__S( void )
{
    return 1;
}



/*
 * ======== xdc.runtime.Memory_HeapProxy SYSTEM FUNCTIONS ========
 */

/* per-module runtime symbols */
#undef Module__MID
#define Module__MID xdc_runtime_Memory_HeapProxy_Module__id__C

#undef Module__DGSINCL
#define Module__DGSINCL xdc_runtime_Memory_HeapProxy_Module__diagsIncluded__C

#undef Module__DGSENAB
#define Module__DGSENAB xdc_runtime_Memory_HeapProxy_Module__diagsEnabled__C

#undef Module__DGSMASK
#define Module__DGSMASK xdc_runtime_Memory_HeapProxy_Module__diagsMask__C

#undef Module__LOGDEF
#define Module__LOGDEF xdc_runtime_Memory_HeapProxy_Module__loggerDefined__C

#undef Module__LOGOBJ
#define Module__LOGOBJ xdc_runtime_Memory_HeapProxy_Module__loggerObj__C

#undef Module__LOGFXN0
#define Module__LOGFXN0 xdc_runtime_Memory_HeapProxy_Module__loggerFxn0__C

#undef Module__LOGFXN1
#define Module__LOGFXN1 xdc_runtime_Memory_HeapProxy_Module__loggerFxn1__C

#undef Module__LOGFXN2
#define Module__LOGFXN2 xdc_runtime_Memory_HeapProxy_Module__loggerFxn2__C

#undef Module__LOGFXN4
#define Module__LOGFXN4 xdc_runtime_Memory_HeapProxy_Module__loggerFxn4__C

#undef Module__LOGFXN8
#define Module__LOGFXN8 xdc_runtime_Memory_HeapProxy_Module__loggerFxn8__C

#undef Module__G_OBJ
#define Module__G_OBJ xdc_runtime_Memory_HeapProxy_Module__gateObj__C

#undef Module__G_PRMS
#define Module__G_PRMS xdc_runtime_Memory_HeapProxy_Module__gatePrms__C

#undef Module__GP_create
#define Module__GP_create xdc_runtime_Memory_HeapProxy_Module_GateProxy_create
#undef Module__GP_delete
#define Module__GP_delete xdc_runtime_Memory_HeapProxy_Module_GateProxy_delete
#undef Module__GP_enter
#define Module__GP_enter xdc_runtime_Memory_HeapProxy_Module_GateProxy_enter
#undef Module__GP_leave
#define Module__GP_leave xdc_runtime_Memory_HeapProxy_Module_GateProxy_leave
#undef Module__GP_query
#define Module__GP_query xdc_runtime_Memory_HeapProxy_Module_GateProxy_query

xdc_Bool xdc_runtime_Memory_HeapProxy_Proxy__abstract__S(void)
{
    return 1;
}
xdc_CPtr xdc_runtime_Memory_HeapProxy_Proxy__delegate__S(void)
{
    return (const void *)&ti_sysbios_heaps_HeapMem_Module__FXNS__C;
}



/*
 * ======== xdc.runtime.Registry SYSTEM FUNCTIONS ========
 */

/* Module__startupDone__S */
xdc_Bool xdc_runtime_Registry_Module__startupDone__S( void )
{
    return 1;
}



/*
 * ======== xdc.runtime.Startup SYSTEM FUNCTIONS ========
 */

/* Module__startupDone__S */
xdc_Bool xdc_runtime_Startup_Module__startupDone__S( void )
{
    return 1;
}



/*
 * ======== xdc.runtime.SysMin SYSTEM FUNCTIONS ========
 */

/* Module__startupDone__S */
xdc_Bool xdc_runtime_SysMin_Module__startupDone__S( void )
{
    return xdc_runtime_SysMin_Module__startupDone__F();
}



/*
 * ======== xdc.runtime.SysStd SYSTEM FUNCTIONS ========
 */

/* Module__startupDone__S */
xdc_Bool xdc_runtime_SysStd_Module__startupDone__S( void )
{
    return 1;
}



/*
 * ======== xdc.runtime.System SYSTEM FUNCTIONS ========
 */

/* Module__startupDone__S */
xdc_Bool xdc_runtime_System_Module__startupDone__S( void )
{
    return xdc_runtime_System_Module__startupDone__F();
}



/*
 * ======== xdc.runtime.System_Module_GateProxy SYSTEM FUNCTIONS ========
 */

/* per-module runtime symbols */
#undef Module__MID
#define Module__MID xdc_runtime_System_Module_GateProxy_Module__id__C

#undef Module__DGSINCL
#define Module__DGSINCL xdc_runtime_System_Module_GateProxy_Module__diagsIncluded__C

#undef Module__DGSENAB
#define Module__DGSENAB xdc_runtime_System_Module_GateProxy_Module__diagsEnabled__C

#undef Module__DGSMASK
#define Module__DGSMASK xdc_runtime_System_Module_GateProxy_Module__diagsMask__C

#undef Module__LOGDEF
#define Module__LOGDEF xdc_runtime_System_Module_GateProxy_Module__loggerDefined__C

#undef Module__LOGOBJ
#define Module__LOGOBJ xdc_runtime_System_Module_GateProxy_Module__loggerObj__C

#undef Module__LOGFXN0
#define Module__LOGFXN0 xdc_runtime_System_Module_GateProxy_Module__loggerFxn0__C

#undef Module__LOGFXN1
#define Module__LOGFXN1 xdc_runtime_System_Module_GateProxy_Module__loggerFxn1__C

#undef Module__LOGFXN2
#define Module__LOGFXN2 xdc_runtime_System_Module_GateProxy_Module__loggerFxn2__C

#undef Module__LOGFXN4
#define Module__LOGFXN4 xdc_runtime_System_Module_GateProxy_Module__loggerFxn4__C

#undef Module__LOGFXN8
#define Module__LOGFXN8 xdc_runtime_System_Module_GateProxy_Module__loggerFxn8__C

#undef Module__G_OBJ
#define Module__G_OBJ xdc_runtime_System_Module_GateProxy_Module__gateObj__C

#undef Module__G_PRMS
#define Module__G_PRMS xdc_runtime_System_Module_GateProxy_Module__gatePrms__C

#undef Module__GP_create
#define Module__GP_create xdc_runtime_System_Module_GateProxy_Module_GateProxy_create
#undef Module__GP_delete
#define Module__GP_delete xdc_runtime_System_Module_GateProxy_Module_GateProxy_delete
#undef Module__GP_enter
#define Module__GP_enter xdc_runtime_System_Module_GateProxy_Module_GateProxy_enter
#undef Module__GP_leave
#define Module__GP_leave xdc_runtime_System_Module_GateProxy_Module_GateProxy_leave
#undef Module__GP_query
#define Module__GP_query xdc_runtime_System_Module_GateProxy_Module_GateProxy_query

xdc_Bool xdc_runtime_System_Module_GateProxy_Proxy__abstract__S(void)
{
    return 0;
}
xdc_CPtr xdc_runtime_System_Module_GateProxy_Proxy__delegate__S(void)
{
    return (const void *)&ti_sysbios_gates_GateHwi_Module__FXNS__C;
}



/*
 * ======== xdc.runtime.System_SupportProxy SYSTEM FUNCTIONS ========
 */


xdc_Bool xdc_runtime_System_SupportProxy_Proxy__abstract__S(void)
{
    return 0;
}
xdc_CPtr xdc_runtime_System_SupportProxy_Proxy__delegate__S(void)
{
    return (const void *)&xdc_runtime_SysMin_Module__FXNS__C;
}


/*
 * ======== xdc.runtime.Text SYSTEM FUNCTIONS ========
 */

/* Module__startupDone__S */
xdc_Bool xdc_runtime_Text_Module__startupDone__S( void )
{
    return 1;
}



/*
 * ======== xdc.runtime.Timestamp SYSTEM FUNCTIONS ========
 */

/* Module__startupDone__S */
xdc_Bool xdc_runtime_Timestamp_Module__startupDone__S( void )
{
    return 1;
}



/*
 * ======== xdc.runtime.TimestampNull SYSTEM FUNCTIONS ========
 */

/* Module__startupDone__S */
xdc_Bool xdc_runtime_TimestampNull_Module__startupDone__S( void )
{
    return 1;
}



/*
 * ======== xdc.runtime.Timestamp_SupportProxy SYSTEM FUNCTIONS ========
 */


xdc_Bool xdc_runtime_Timestamp_SupportProxy_Proxy__abstract__S(void)
{
    return 0;
}
xdc_CPtr xdc_runtime_Timestamp_SupportProxy_Proxy__delegate__S(void)
{
    return 0;
}


/*
 * ======== xdc.runtime.Types SYSTEM FUNCTIONS ========
 */

/* Module__startupDone__S */
xdc_Bool xdc_runtime_Types_Module__startupDone__S( void )
{
    return 1;
}



/*
 * ======== xdc.runtime.knl.GateH SYSTEM FUNCTIONS ========
 */

/* Module__startupDone__S */
xdc_Bool xdc_runtime_knl_GateH_Module__startupDone__S( void )
{
    return 1;
}



/*
 * ======== xdc.runtime.knl.GateH_Proxy SYSTEM FUNCTIONS ========
 */

/* per-module runtime symbols */
#undef Module__MID
#define Module__MID xdc_runtime_knl_GateH_Proxy_Module__id__C

#undef Module__DGSINCL
#define Module__DGSINCL xdc_runtime_knl_GateH_Proxy_Module__diagsIncluded__C

#undef Module__DGSENAB
#define Module__DGSENAB xdc_runtime_knl_GateH_Proxy_Module__diagsEnabled__C

#undef Module__DGSMASK
#define Module__DGSMASK xdc_runtime_knl_GateH_Proxy_Module__diagsMask__C

#undef Module__LOGDEF
#define Module__LOGDEF xdc_runtime_knl_GateH_Proxy_Module__loggerDefined__C

#undef Module__LOGOBJ
#define Module__LOGOBJ xdc_runtime_knl_GateH_Proxy_Module__loggerObj__C

#undef Module__LOGFXN0
#define Module__LOGFXN0 xdc_runtime_knl_GateH_Proxy_Module__loggerFxn0__C

#undef Module__LOGFXN1
#define Module__LOGFXN1 xdc_runtime_knl_GateH_Proxy_Module__loggerFxn1__C

#undef Module__LOGFXN2
#define Module__LOGFXN2 xdc_runtime_knl_GateH_Proxy_Module__loggerFxn2__C

#undef Module__LOGFXN4
#define Module__LOGFXN4 xdc_runtime_knl_GateH_Proxy_Module__loggerFxn4__C

#undef Module__LOGFXN8
#define Module__LOGFXN8 xdc_runtime_knl_GateH_Proxy_Module__loggerFxn8__C

#undef Module__G_OBJ
#define Module__G_OBJ xdc_runtime_knl_GateH_Proxy_Module__gateObj__C

#undef Module__G_PRMS
#define Module__G_PRMS xdc_runtime_knl_GateH_Proxy_Module__gatePrms__C

#undef Module__GP_create
#define Module__GP_create xdc_runtime_knl_GateH_Proxy_Module_GateProxy_create
#undef Module__GP_delete
#define Module__GP_delete xdc_runtime_knl_GateH_Proxy_Module_GateProxy_delete
#undef Module__GP_enter
#define Module__GP_enter xdc_runtime_knl_GateH_Proxy_Module_GateProxy_enter
#undef Module__GP_leave
#define Module__GP_leave xdc_runtime_knl_GateH_Proxy_Module_GateProxy_leave
#undef Module__GP_query
#define Module__GP_query xdc_runtime_knl_GateH_Proxy_Module_GateProxy_query

xdc_Bool xdc_runtime_knl_GateH_Proxy_Proxy__abstract__S(void)
{
    return 1;
}
xdc_CPtr xdc_runtime_knl_GateH_Proxy_Proxy__delegate__S(void)
{
    return (const void *)&xdc_runtime_knl_GateThread_Module__FXNS__C;
}



/*
 * ======== xdc.runtime.knl.GateThread SYSTEM FUNCTIONS ========
 */

/* per-module runtime symbols */
#undef Module__MID
#define Module__MID xdc_runtime_knl_GateThread_Module__id__C

#undef Module__DGSINCL
#define Module__DGSINCL xdc_runtime_knl_GateThread_Module__diagsIncluded__C

#undef Module__DGSENAB
#define Module__DGSENAB xdc_runtime_knl_GateThread_Module__diagsEnabled__C

#undef Module__DGSMASK
#define Module__DGSMASK xdc_runtime_knl_GateThread_Module__diagsMask__C

#undef Module__LOGDEF
#define Module__LOGDEF xdc_runtime_knl_GateThread_Module__loggerDefined__C

#undef Module__LOGOBJ
#define Module__LOGOBJ xdc_runtime_knl_GateThread_Module__loggerObj__C

#undef Module__LOGFXN0
#define Module__LOGFXN0 xdc_runtime_knl_GateThread_Module__loggerFxn0__C

#undef Module__LOGFXN1
#define Module__LOGFXN1 xdc_runtime_knl_GateThread_Module__loggerFxn1__C

#undef Module__LOGFXN2
#define Module__LOGFXN2 xdc_runtime_knl_GateThread_Module__loggerFxn2__C

#undef Module__LOGFXN4
#define Module__LOGFXN4 xdc_runtime_knl_GateThread_Module__loggerFxn4__C

#undef Module__LOGFXN8
#define Module__LOGFXN8 xdc_runtime_knl_GateThread_Module__loggerFxn8__C

#undef Module__G_OBJ
#define Module__G_OBJ xdc_runtime_knl_GateThread_Module__gateObj__C

#undef Module__G_PRMS
#define Module__G_PRMS xdc_runtime_knl_GateThread_Module__gatePrms__C

#undef Module__GP_create
#define Module__GP_create xdc_runtime_knl_GateThread_Module_GateProxy_create
#undef Module__GP_delete
#define Module__GP_delete xdc_runtime_knl_GateThread_Module_GateProxy_delete
#undef Module__GP_enter
#define Module__GP_enter xdc_runtime_knl_GateThread_Module_GateProxy_enter
#undef Module__GP_leave
#define Module__GP_leave xdc_runtime_knl_GateThread_Module_GateProxy_leave
#undef Module__GP_query
#define Module__GP_query xdc_runtime_knl_GateThread_Module_GateProxy_query

/* Module__startupDone__S */
xdc_Bool xdc_runtime_knl_GateThread_Module__startupDone__S( void )
{
    return 1;
}

/* Handle__label__S */
xdc_runtime_Types_Label *xdc_runtime_knl_GateThread_Handle__label__S(xdc_Ptr obj, xdc_runtime_Types_Label *lab) 
{
    lab->handle = obj;
    lab->modId = 32810;
    lab->named = FALSE;
    lab->iname = xdc_runtime_Text_nameUnknown;

    return lab;
}

/* Params__init__S */
xdc_Void xdc_runtime_knl_GateThread_Params__init__S( xdc_Ptr prms, const void *src, xdc_SizeT psz, xdc_SizeT isz ) 
{
    xdc_runtime_Core_assignParams__I(prms, (xdc_CPtr)(src ? src : &xdc_runtime_knl_GateThread_Object__PARAMS__C), psz, isz);
}

/* Object__get__S */
xdc_Ptr xdc_runtime_knl_GateThread_Object__get__S(xdc_Ptr oa, xdc_Int i)
{
    if (oa != NULL) {
        return ((xdc_runtime_knl_GateThread_Object__ *)oa) + i;
    }

    return NULL;
}


/* Object__first__S */
xdc_Ptr xdc_runtime_knl_GateThread_Object__first__S( void ) 
{
    xdc_runtime_Types_InstHdr *iHdr = (xdc_runtime_Types_InstHdr *)xdc_runtime_knl_GateThread_Module__root__V.link.next;

    if (iHdr != (xdc_runtime_Types_InstHdr *)&xdc_runtime_knl_GateThread_Module__root__V.link) {
        return iHdr + 1;
    }
    else {
        return NULL;
    }
}

/* Object__next__S */
xdc_Ptr xdc_runtime_knl_GateThread_Object__next__S( xdc_Ptr obj )
{
    xdc_runtime_Types_InstHdr *iHdr = ((xdc_runtime_Types_InstHdr *)obj) - 1;

    if (iHdr->link.next != (xdc_runtime_Types_Link *)&xdc_runtime_knl_GateThread_Module__root__V.link) {
        return (xdc_runtime_Types_InstHdr *)(iHdr->link.next) + 1;
    }
    else {
        return NULL;
    }
}

/* Object__create__S */
xdc_Ptr xdc_runtime_knl_GateThread_Object__create__S(
    xdc_CPtr req_args,
    const xdc_UChar *paramsPtr,
    xdc_SizeT prm_size,
    xdc_runtime_Error_Block *eb)
{
    xdc_runtime_knl_GateThread_Params instPrms;
    xdc_runtime_knl_GateThread_Object *objp;
    int iStat;

    /* common instance initialization */
    objp = xdc_runtime_Core_createObject__I(&xdc_runtime_knl_GateThread_Object__DESC__C, NULL, &instPrms, (xdc_CPtr)paramsPtr, 0, eb);
    if (objp == NULL) {
        return NULL;
    }


    /* module-specific initialization */
    iStat = xdc_runtime_knl_GateThread_Instance_init__E(objp, &instPrms, eb);
    if (iStat) {
        xdc_runtime_Core_deleteObject__I(&xdc_runtime_knl_GateThread_Object__DESC__C, objp, (xdc_Fxn)xdc_runtime_knl_GateThread_Instance_finalize__E, iStat, 0);
        return NULL;
    }

    return objp;
}

/* create */
xdc_runtime_knl_GateThread_Handle xdc_runtime_knl_GateThread_create( const xdc_runtime_knl_GateThread_Params *paramsPtr, xdc_runtime_Error_Block *eb)
{
    xdc_runtime_knl_GateThread_Params prms;
    xdc_runtime_knl_GateThread_Object *obj;

    int iStat;

    /* common instance initialization */
    obj = xdc_runtime_Core_createObject__I(&xdc_runtime_knl_GateThread_Object__DESC__C, NULL, &prms, (xdc_CPtr)paramsPtr, 0, eb);
    if (obj == NULL) {
        return NULL;
    }

    /* module-specific initialization */
    iStat = xdc_runtime_knl_GateThread_Instance_init__E(obj, &prms, eb);
    if (iStat) {
        xdc_runtime_Core_deleteObject__I(&xdc_runtime_knl_GateThread_Object__DESC__C, obj, (xdc_Fxn)xdc_runtime_knl_GateThread_Instance_finalize__E, iStat, 0);
        return NULL;
    }

    return obj;
}

/* construct */
void xdc_runtime_knl_GateThread_construct(xdc_runtime_knl_GateThread_Struct *objp, const xdc_runtime_knl_GateThread_Params *paramsPtr, xdc_runtime_Error_Block *eb)
{
    xdc_runtime_knl_GateThread_Params instPrms;
    int iStat;

    /* common instance initialization */
    (Void)xdc_runtime_Core_constructObject__I(&xdc_runtime_knl_GateThread_Object__DESC__C, objp, &instPrms, (xdc_CPtr)paramsPtr, 0, eb);

    /* module-specific initialization */
    iStat = xdc_runtime_knl_GateThread_Instance_init__E((xdc_Ptr)objp, &instPrms, eb);
    if (iStat) {
        xdc_runtime_Core_deleteObject__I(&xdc_runtime_knl_GateThread_Object__DESC__C, objp, (xdc_Fxn)xdc_runtime_knl_GateThread_Instance_finalize__E, iStat, 1);
    }

}

/* destruct */
void xdc_runtime_knl_GateThread_destruct(xdc_runtime_knl_GateThread_Struct *obj)
{
    xdc_runtime_Core_deleteObject__I(&xdc_runtime_knl_GateThread_Object__DESC__C, obj, (xdc_Fxn)xdc_runtime_knl_GateThread_Instance_finalize__E, 0, TRUE);
}

/* Object__delete__S */
xdc_Void xdc_runtime_knl_GateThread_Object__delete__S( xdc_Ptr instp ) 
{
    xdc_runtime_Core_deleteObject__I(&xdc_runtime_knl_GateThread_Object__DESC__C, *((xdc_runtime_knl_GateThread_Object**)instp), (xdc_Fxn)xdc_runtime_knl_GateThread_Instance_finalize__E, 0, FALSE);
    *((xdc_runtime_knl_GateThread_Handle*)instp) = NULL;
}

/* delete */
void xdc_runtime_knl_GateThread_delete(xdc_runtime_knl_GateThread_Handle *instp)
{
    xdc_runtime_knl_GateThread_Object__delete__S(instp);
}


/*
 * ======== xdc.runtime.knl.GateThread_Proxy SYSTEM FUNCTIONS ========
 */

/* per-module runtime symbols */
#undef Module__MID
#define Module__MID xdc_runtime_knl_GateThread_Proxy_Module__id__C

#undef Module__DGSINCL
#define Module__DGSINCL xdc_runtime_knl_GateThread_Proxy_Module__diagsIncluded__C

#undef Module__DGSENAB
#define Module__DGSENAB xdc_runtime_knl_GateThread_Proxy_Module__diagsEnabled__C

#undef Module__DGSMASK
#define Module__DGSMASK xdc_runtime_knl_GateThread_Proxy_Module__diagsMask__C

#undef Module__LOGDEF
#define Module__LOGDEF xdc_runtime_knl_GateThread_Proxy_Module__loggerDefined__C

#undef Module__LOGOBJ
#define Module__LOGOBJ xdc_runtime_knl_GateThread_Proxy_Module__loggerObj__C

#undef Module__LOGFXN0
#define Module__LOGFXN0 xdc_runtime_knl_GateThread_Proxy_Module__loggerFxn0__C

#undef Module__LOGFXN1
#define Module__LOGFXN1 xdc_runtime_knl_GateThread_Proxy_Module__loggerFxn1__C

#undef Module__LOGFXN2
#define Module__LOGFXN2 xdc_runtime_knl_GateThread_Proxy_Module__loggerFxn2__C

#undef Module__LOGFXN4
#define Module__LOGFXN4 xdc_runtime_knl_GateThread_Proxy_Module__loggerFxn4__C

#undef Module__LOGFXN8
#define Module__LOGFXN8 xdc_runtime_knl_GateThread_Proxy_Module__loggerFxn8__C

#undef Module__G_OBJ
#define Module__G_OBJ xdc_runtime_knl_GateThread_Proxy_Module__gateObj__C

#undef Module__G_PRMS
#define Module__G_PRMS xdc_runtime_knl_GateThread_Proxy_Module__gatePrms__C

#undef Module__GP_create
#define Module__GP_create xdc_runtime_knl_GateThread_Proxy_Module_GateProxy_create
#undef Module__GP_delete
#define Module__GP_delete xdc_runtime_knl_GateThread_Proxy_Module_GateProxy_delete
#undef Module__GP_enter
#define Module__GP_enter xdc_runtime_knl_GateThread_Proxy_Module_GateProxy_enter
#undef Module__GP_leave
#define Module__GP_leave xdc_runtime_knl_GateThread_Proxy_Module_GateProxy_leave
#undef Module__GP_query
#define Module__GP_query xdc_runtime_knl_GateThread_Proxy_Module_GateProxy_query

xdc_Bool xdc_runtime_knl_GateThread_Proxy_Proxy__abstract__S(void)
{
    return 0;
}
xdc_CPtr xdc_runtime_knl_GateThread_Proxy_Proxy__delegate__S(void)
{
    return 0;
}



/*
 * ======== INITIALIZATION ENTRY POINT ========
 */

#include <stdint.h>
extern int_least32_t __xdc__init(void);
    __attribute__ ((used))
__FAR__ int_least32_t (* volatile __xdc__init__addr)(void) = &__xdc__init;


/*
 * ======== PROGRAM GLOBALS ========
 */

