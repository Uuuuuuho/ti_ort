/*
 * Do not modify this file; it is automatically generated from the template
 * linkcmd.xdt in the ti.platforms.tiva package and will be overwritten.
 */

/*
 * put '"'s around paths because, without this, the linker
 * considers '-' as minus operator, not a file name character.
 */


-l"/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/tidl_j7_08_00_00_10/ti_dl/test/out/J7_SDK/C71/SYSBIOS/release/module/src.dsp_sdk/configuro/package/cfg/c7x_1_pe71.oe71"
-l"/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/tidl_j7_08_00_00_10/ti_dl/test/out/J7_SDK/C71/SYSBIOS/release/module/src.dsp_sdk/configuro/package/cfg/c7x_1_pe71.src/sysbios/sysbios.ae71"
-l"/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/bios_6_83_02_07/packages/ti/targets/rts7000/lib/ti.targets.rts7000.ae71"
-l"/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/bios_6_83_02_07/packages/ti/targets/rts7000/lib/boot.ae71"

--retain="*(xdc.meta)"



--args 0x0
-heap  0x0
-stack 0x2000

/*
 * Linker command file contributions from all loaded packages:
 */

/* Content from xdc.services.global (null): */

/* Content from xdc (null): */

/* Content from xdc.corevers (null): */

/* Content from xdc.rov (null): */

/* Content from xdc.runtime (null): */

/* Content from xdc.rov.runtime (null): */

/* Content from xdc.shelf (null): */

/* Content from xdc.services.spec (null): */

/* Content from xdc.services.intern.xsr (null): */

/* Content from xdc.services.intern.gen (null): */

/* Content from xdc.services.intern.cmd (null): */

/* Content from xdc.bld (null): */

/* Content from ti.targets (null): */

/* Content from ti.targets.elf (null): */

/* Content from ti.targets.rts7000 (null): */

/* Content from ti.sysbios.interfaces (null): */

/* Content from ti.sysbios.family (null): */

/* Content from xdc.services.getset (null): */

/* Content from xdc.runtime.knl (null): */

/* Content from ti.sysbios.family.c64p.tci6488 (null): */

/* Content from ti.sysbios.rts (null): */

/* Content from ti.catalog.c7000 (null): */

/* Content from ti.catalog (null): */

/* Content from xdc.platform (null): */

/* Content from xdc.cfg (null): */

/* Content from ti.platforms.tms320C7x (null): */

/* Content from ti.sysbios.hal (null): */

/* Content from ti.sysbios.family.c7x (ti/sysbios/family/c7x/linkcmd.xdt): */


/* Content from ti.sysbios.knl (null): */

/* Content from ti.sysbios.timers.dmtimer (null): */

/* Content from ti.sysbios (null): */

/* Content from ti.sysbios.syncs (null): */

/* Content from ti.sysbios.gates (null): */

/* Content from ti.sysbios.heaps (null): */

/* Content from ti.sysbios.utils (null): */

/* Content from ti.sysbios.xdcruntime (null): */

/* Content from configuro (null): */

/* Content from xdc.services.io (null): */

/* Content from ti.sysbios.family.c28 (null): */



/*
 * symbolic aliases for static instance objects
 */
xdc_runtime_Startup__EXECFXN__C = 1;
xdc_runtime_Startup__RESETFXN__C = 1;
xdc_rov_runtime_Mon__checksum = 1;
xdc_rov_runtime_Mon__write = 1;


SECTIONS
{



    xdc.meta: type = COPY
}
