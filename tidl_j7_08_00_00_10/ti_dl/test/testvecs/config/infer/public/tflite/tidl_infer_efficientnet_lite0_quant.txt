inFileFormat    = 2
postProcType = 1
numFrames   = 1
netBinFile      = "testvecs/config/tidl_models/tflite/tidl_net_tflite_efficientnet_lite0_quant.bin"
ioConfigFile   = "testvecs/config/tidl_models/tflite/tidl_io_tflite_efficientnet_lite0_quant_1.bin"
inData  =   testvecs/config/classification_list.txt
outData =   testvecs/output/airshow_tflite_efficientnet_lite0_quant.bin
writeTraceLevel = 0
debugTraceLevel = 0

