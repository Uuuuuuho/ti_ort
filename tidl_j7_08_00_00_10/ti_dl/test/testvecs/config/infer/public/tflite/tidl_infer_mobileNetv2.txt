inFileFormat    = 2
postProcType = 1
numFrames   = 1
netBinFile      = "testvecs/config/tidl_models/tflite/mobilenetV2/tidl_net_tflite_mobilenet_v2_1.0_224.bin"
ioConfigFile   = "testvecs/config/tidl_models/tflite/mobilenetV2/tidl_io_tflite_mobilenet_v2_1.0_224_1.bin"
inData  =   testvecs/config/classification_list_1.txt
outData =   testvecs/output/airshow_mobilenetv2_tf.bin
writeTraceLevel = 3

