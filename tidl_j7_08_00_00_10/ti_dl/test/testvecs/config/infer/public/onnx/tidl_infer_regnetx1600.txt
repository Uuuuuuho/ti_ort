inFileFormat    = 2
postProcType = 1
numFrames   = 1
netBinFile      = "testvecs/config/tidl_models/onnx/tidl_net_regnetx1600.bin"
ioConfigFile   = "testvecs/config/tidl_models/onnx/tidl_io_regnetx1600_1.bin"
inData  =   testvecs/config/classification_list.txt
outData =   testvecs/output/airshow_onnx_regnetx1600.bin
