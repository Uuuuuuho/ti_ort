inFileFormat    = 2
postProcType = 1
numFrames   = 1
netBinFile      = "testvecs/config/tidl_models/onnx/tidl_net_resnet18v1_4mp.bin"
ioConfigFile   = "testvecs/config/tidl_models/onnx/tidl_io_resnet18v1_4mp_1.bin"
inData  =   testvecs/config/classification_list.txt
outData =   testvecs/output/airshow_onnx_resnet18v1_4mp.bin
debugTraceLevel = 0
writeTraceLevel = 0

