inFileFormat    = 2
postProcType = 1
numFrames   = 1
netBinFile      = "testvecs/config/tidl_models/onnx/tidl_net_resnet34v1.bin"
ioConfigFile   = "testvecs/config/tidl_models/onnx/tidl_io_resnet34v1_1.bin"
inData  =   testvecs/config/classification_list.txt
outData =   testvecs/output/airshow_onnx_resnet34v1.bin
debugTraceLevel = 0
writeTraceLevel = 0

