inFileFormat    = 2
postProcType = 1
numFrames   = 1
netBinFile      = "testvecs/config/tidl_models/caffe/tidl_net_mobilenet_v1_0.5mp_calib16.bin"
ioConfigFile   = "testvecs/config/tidl_models/caffe/tidl_io_mobilenet_v1_0.5mp_calib16_1.bin"
outData =   testvecs/output/airshow_mobileNetv1_0.5mp_calib16.bin
inData  =   testvecs/config/classification_list.txt
quantRangeExpansionFactor = 1.0
quantRangeUpdateFactor = 0.0
##inFileFormat    = 3
#inData  =   testvecs/input/0000_3_1024x512_paded.y
#writeBinsAsHeader = 1
debugTraceLevel = 0
writeTraceLevel = 0


