inFileFormat    = 2
postProcType = 2
numFrames   = 1
netBinFile      = "testvecs/config/tidl_models/tensorflow/tidl_net_mobilenet_v1_0.75_224_ssd.bin"
ioConfigFile   = "testvecs/config/tidl_models/tensorflow/tidl_io_mobilenet_v1_0.75_224_ssd_1.bin"
inData  =   "testvecs/config/detection_list.txt"
outData =   testvecs/output/tidl_io_mobilenet_v1_0.75_ssd.bin
#inData = "D:\work\vision\CNN\tensorFlow\models\research\object_detection\test_images\trace\0000_Preprocessor_map_TensorArrayStack_TensorArrayGatherV3_1x300x300x3.y"
debugTraceLevel = 0
writeTraceLevel = 0


