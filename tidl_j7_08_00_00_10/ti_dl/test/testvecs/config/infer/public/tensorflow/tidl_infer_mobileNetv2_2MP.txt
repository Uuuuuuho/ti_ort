inFileFormat    = 2
postProcType = 0
numFrames   = 1
netBinFile      = "testvecs/config/tidl_models/tensorflow/tidl_net_mobilenet_v2_1.0_224_2mp.bin"
ioConfigFile   = "testvecs/config/tidl_models/tensorflow/tidl_net_mobilenet_v2_1.0_224_2mp_1.bin"
inData  =   testvecs/config/classification_list_1.txt
outData =   testvecs/output/airshow_mobilenetv2_tf_2mp.bin
debugTraceLevel = 0
writeTraceLevel = 0

