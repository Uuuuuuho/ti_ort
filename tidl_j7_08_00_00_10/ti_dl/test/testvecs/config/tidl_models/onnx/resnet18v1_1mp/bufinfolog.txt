------Layer #(Type) [Exec ID , Data ID] --[Ni x inW x inH] => [No x outW x outH] [Ni/G] [dataflowType] [preFetch, preFetchAlign, procSize, inPlaneSize] [dmaFreq] [dmaFreqWt] [kernelFreq] [In Data Ids] -----
------  0(    Data) [0, 0] --[0 x 0 x  0] => [3 x 224 x  224] *** [0] ***[FRAME] ***[0, 0, 1, 0]**** [0], [1],[1] -[]---
  IN: DDR, DMA,   cbf6( 52214),      0(     0),   24(   36),  26800( 157696),   0,   b248d4 ||||  L2, DMA,      0(     0),      0(     0),    0(    0),      0(      0),   0, ffffffff 
 OUT:NONE,  NA,      0(     0),      0(     0),    0(    0),      0(      0),   0, ffffffff |||| DDR, DMA,   cbf6( 52214),   cbf5( 52213),    3(    3),  26800( 157696), 2ac,   b248d4 
  WT:NONE,  NA,      0(     0),      0(     0),   24(   36),      0(      0),   0,        0 ||||  L2, CPU,      0(     0),      0(     0),    0(    0),      0(      0),   0, ffffffff 
 STG:  L2,  NA,      0(     0),      0(     0),    0(    0),      0(      0),   0, ffffffff 
CONT:  L2, CPU,      0(     0),      0(     0),    0(    0),      0(      0),   0,        0 
Padding Info [Row, Col]: Actual  IN -> OUT : [ 0,  0] -> [ 3,  3], Required OUT : [ 3,  3],  To fill zero OUT: [ 0,  0]
------  1(    Conv) [1, 1] --[3 x 224 x  224] => [64 x 112 x  112] *** [3] ***[ROW_L] ***[1138, 1138, 51075, 52213]**** [1], [1],[1] -[0 ]---
  IN: DDR, DMA,   cbf6( 52214),   cbf5( 52213),    3(    3),  26800( 157696),   0,   b248d4 ||||  L2, DMA,   cc40( 52288),   cc40( 52288),    3(    3),  26480( 156800),   0,        0 
 OUT:NONE,  NA,      0(     0),      0(     0),    0(    0),      0(      0),   0, ffffffff ||||MSMC, CPU,   32c0( 12992),   3253( 12883),   40(   64),  cb080( 831616),  72,        e 
  WT: DDR, DMA,     94(   148),     94(   148),   40(   64),   2500(   9472),   0,        0 ||||  L2, DMA,     c0(   192),     94(   148),   40(   64),   3000(  12288),   0,    26480 
 STG:  L2,  NA,      0(     0),      0(     0),    0(    0),      0(      0),   0, ffffffff 
CONT:  L2, CPU,      0(     0),      0(     0),    0(    0),      0(      0),   0,        0 
Padding Info [Row, Col]: Actual  IN -> OUT : [ 3,  3] -> [ 1,  1], Required OUT : [ 1,  1],  To fill zero OUT: [ 1,  1]
------  2(    Pool) [2, 2] --[64 x 112 x  112] => [64 x 56 x  56] *** [64] ***[ COL] ***[0, 0, 12883, 12883]**** [4], [1],[4] -[1 ]---
  IN:MSMC, DMA,   32c0( 12992),   3253( 12883),   40(   64),  cb080( 831616),   0,        e ||||  L2, DMA,   32c0( 12992),   32c0( 12992),   20(   32),  65800( 415744),   0,        0 
 OUT:NONE,  NA,      0(     0),      0(     0),    0(    0),      0(      0),   0, ffffffff ||||MSMC, CPU,    d40(  3392),    ceb(  3307),   40(   64),  35080( 217216),  3a,       46 
  WT: DDR, DMA,      0(     0),      0(     0),  300(  768),      0(      0),   0,     2500 ||||  L2, DMA,      0(     0),      0(     0),    0(    0),      0(      0),   0, ffffffff 
 STG:  L2,  NA,      0(     0),      0(     0),    0(    0),      0(      0),   0, ffffffff 
CONT:  L2, CPU,      0(     0),      0(     0),    0(    0),      0(      0),   0,        0 
Padding Info [Row, Col]: Actual  IN -> OUT : [ 1,  1] -> [ 1,  1], Required OUT : [ 1,  1],  To fill zero OUT: [ 1,  1]
------  3(    Conv) [3, 3] --[64 x 56 x  56] => [64 x 56 x  56] *** [64] ***[ROW_L] ***[116, 128, 3072, 3307]**** [2], [1],[2] -[2 ]---
  IN:MSMC, DMA,    d40(  3392),    ceb(  3307),   40(   64),  35080( 217216),   0,       46 ||||  L2, DMA,   18c0(  6336),   18c0(  6336),   40(   64),  63000( 405504),   0,        0 
 OUT:NONE,  NA,      0(     0),      0(     0),    0(    0),      0(      0),   0, ffffffff ||||MSMC, CPU,    d40(  3392),    ceb(  3307),   40(   64),  35080( 217216),  3a,    350c6 
  WT: DDR, DMA,    241(   577),    241(   577),   40(   64),   9080(  36992),   0,     2500 ||||  L2, DMA,    2c0(   704),    241(   577),   40(   64),   b000(  45056),   0,    63000 
 STG:  L2,  NA,      0(     0),      0(     0),    0(    0),      0(      0),   0, ffffffff 
CONT:  L2, CPU,      0(     0),      0(     0),    0(    0),      0(      0),   0,        0 
Padding Info [Row, Col]: Actual  IN -> OUT : [ 1,  1] -> [ 1,  1], Required OUT : [ 1,  1],  To fill zero OUT: [ 1,  0]
------  4(    Conv) [4, 4] --[64 x 56 x  56] => [64 x 56 x  56] *** [64] ***[ROW_L] ***[116, 128, 3072, 3307]**** [2], [1],[2] -[3 ]---
  IN:MSMC, DMA,    d40(  3392),    ceb(  3307),   40(   64),  35080( 217216),   0,    350c6 ||||  L2, DMA,   18c0(  6336),   18c0(  6336),   40(   64),  63000( 405504),   0,        0 
 OUT:NONE,  NA,      0(     0),      0(     0),    0(    0),      0(      0),   0, ffffffff ||||MSMC, CPU,    d40(  3392),    ceb(  3307),   40(   64),  35080( 217216),  3a,    350c6 
  WT: DDR, DMA,    241(   577),    241(   577),   40(   64),   9080(  36992),   0,     b580 ||||  L2, DMA,    2c0(   704),    241(   577),   40(   64),   b000(  45056),   0,    63000 
 STG:  L2,  NA,      0(     0),      0(     0),    0(    0),      0(      0),   0, ffffffff 
CONT:  L2, CPU,      0(     0),      0(     0),    0(    0),      0(      0),   0,        0 
Padding Info [Row, Col]: Actual  IN -> OUT : [ 1,  1] -> [ 1,  1], Required OUT : [ 1,  1],  To fill zero OUT: [ 0,  0]
------  5( EltWise) [5, 5] --[128 x 56 x  56] => [64 x 56 x  56] *** [128] ***[ COL] ***[0, 0, 3191, 3192]**** [1], [1],[1] -[2 4 ]---
  IN:MSMC, DMA,    d40(  3392),    ceb(  3307),   40(   64),  35080( 217216),  3a,       46 ||||  L2, DMA,    d40(  3392),    d40(  3392),   40(   64),  6a000( 434176),   0,        0 
 OUT:NONE,  NA,      0(     0),      0(     0),    0(    0),      0(      0),   0, ffffffff ||||MSMC, CPU,    d40(  3392),    ceb(  3307),   40(   64),  35080( 217216),  3a,       46 
  WT: DDR, DMA,      0(     0),      0(     0),  300(  768),      0(      0),   0,    14600 ||||  L2, DMA,      0(     0),      0(     0),    0(    0),      0(      0),   0, ffffffff 
 STG:  L2,  NA,      0(     0),      0(     0),    0(    0),      0(      0),   0, ffffffff 
CONT:  L2, CPU,      0(     0),      0(     0),    0(    0),      0(      0),   0,        0 
Padding Info [Row, Col]: Actual  IN -> OUT : [ 1,  1] -> [ 1,  1], Required OUT : [ 1,  1],  To fill zero OUT: [ 0,  1]
------  6(    Conv) [6, 6] --[64 x 56 x  56] => [64 x 56 x  56] *** [64] ***[ROW_L] ***[116, 128, 3072, 3307]**** [2], [1],[2] -[5 ]---
  IN:MSMC, DMA,    d40(  3392),    ceb(  3307),   40(   64),  35080( 217216),   0,       46 ||||  L2, DMA,   18c0(  6336),   18c0(  6336),   40(   64),  63000( 405504),   0,        0 
 OUT:NONE,  NA,      0(     0),      0(     0),    0(    0),      0(      0),   0, ffffffff ||||MSMC, CPU,    d40(  3392),    ceb(  3307),   40(   64),  35080( 217216),  3a,    350c6 
  WT: DDR, DMA,    241(   577),    241(   577),   40(   64),   9080(  36992),   0,    14600 ||||  L2, DMA,    2c0(   704),    241(   577),   40(   64),   b000(  45056),   0,    63000 
 STG:  L2,  NA,      0(     0),      0(     0),    0(    0),      0(      0),   0, ffffffff 
CONT:  L2, CPU,      0(     0),      0(     0),    0(    0),      0(      0),   0,        0 
Padding Info [Row, Col]: Actual  IN -> OUT : [ 1,  1] -> [ 1,  1], Required OUT : [ 1,  1],  To fill zero OUT: [ 1,  0]
------  7(    Conv) [7, 7] --[64 x 56 x  56] => [64 x 56 x  56] *** [64] ***[ROW_L] ***[116, 128, 3072, 3307]**** [2], [1],[2] -[6 ]---
  IN:MSMC, DMA,    d40(  3392),    ceb(  3307),   40(   64),  35080( 217216),   0,    350c6 ||||  L2, DMA,   18c0(  6336),   18c0(  6336),   40(   64),  63000( 405504),   0,        0 
 OUT:NONE,  NA,      0(     0),      0(     0),    0(    0),      0(      0),   0, ffffffff ||||MSMC, CPU,    d40(  3392),    ceb(  3307),   40(   64),  35080( 217216),  3a,    350c6 
  WT: DDR, DMA,    241(   577),    241(   577),   40(   64),   9080(  36992),   0,    1d680 ||||  L2, DMA,    2c0(   704),    241(   577),   40(   64),   b000(  45056),   0,    63000 
 STG:  L2,  NA,      0(     0),      0(     0),    0(    0),      0(      0),   0, ffffffff 
CONT:  L2, CPU,      0(     0),      0(     0),    0(    0),      0(      0),   0,        0 
Padding Info [Row, Col]: Actual  IN -> OUT : [ 1,  1] -> [ 1,  1], Required OUT : [ 1,  1],  To fill zero OUT: [ 0,  0]
------  8( EltWise) [8, 8] --[128 x 56 x  56] => [64 x 56 x  56] *** [128] ***[ COL] ***[0, 0, 3191, 3192]**** [1], [1],[1] -[5 7 ]---
  IN:MSMC, DMA,    d40(  3392),    ceb(  3307),   40(   64),  35080( 217216),  3a,       46 ||||  L2, DMA,    d40(  3392),    d40(  3392),   40(   64),  6a000( 434176),   0,        0 
 OUT:NONE,  NA,      0(     0),      0(     0),    0(    0),      0(      0),   0, ffffffff ||||MSMC, CPU,    d40(  3392),    ceb(  3307),   40(   64),  35080( 217216),  3a,       46 
  WT: DDR, DMA,      0(     0),      0(     0),  300(  768),      0(      0),   0,    26700 ||||  L2, DMA,      0(     0),      0(     0),    0(    0),      0(      0),   0, ffffffff 
 STG:  L2,  NA,      0(     0),      0(     0),    0(    0),      0(      0),   0, ffffffff 
CONT:  L2, CPU,      0(     0),      0(     0),    0(    0),      0(      0),   0,        0 
Padding Info [Row, Col]: Actual  IN -> OUT : [ 1,  1] -> [ 1,  1], Required OUT : [ 1,  1],  To fill zero OUT: [ 0,  1]
------  9(    Conv) [9, 9] --[64 x 56 x  56] => [128 x 28 x  28] *** [64] ***[ROW_L] ***[58, 58, 2736, 3307]**** [2], [1],[2] -[8 ]---
  IN:MSMC, DMA,    d40(  3392),    ceb(  3307),   40(   64),  35080( 217216),   0,       46 ||||  L2, DMA,   15c0(  5568),   15c0(  5568),   40(   64),  57000( 356352),   0,        0 
 OUT:NONE,  NA,      0(     0),      0(     0),    0(    0),      0(      0),   0, ffffffff ||||MSMC, CPU,    3c0(   960),    367(   871),   80(  128),  1e080( 123008),  1e,    350e2 
  WT: DDR, DMA,    241(   577),    241(   577),   80(  128),  12080(  73856),   0,    26700 ||||  L2, DMA,    2c0(   704),    241(   577),   80(  128),  16000(  90112),   0,    57000 
 STG:  L2,  NA,      0(     0),      0(     0),    0(    0),      0(      0),   0, ffffffff 
CONT:  L2, CPU,      0(     0),      0(     0),    0(    0),      0(      0),   0,        0 
Padding Info [Row, Col]: Actual  IN -> OUT : [ 1,  1] -> [ 1,  1], Required OUT : [ 1,  1],  To fill zero OUT: [ 1,  1]
------  11(    Conv) [10, 11] --[128 x 28 x  28] => [128 x 28 x  28] *** [128] ***[ROW_L] ***[60, 64, 807, 871]**** [1], [1],[1] -[9 ]---
  IN:MSMC, DMA,    3c0(   960),    367(   871),   80(  128),  1e080( 123008),   0,    350e2 ||||  L2, DMA,    3c0(   960),    3c0(   960),   80(  128),  1e000( 122880),   0,        0 
 OUT:NONE,  NA,      0(     0),      0(     0),    0(    0),      0(      0),   0, ffffffff ||||MSMC, CPU,    3c0(   960),    367(   871),   80(  128),  1e080( 123008),  1e,    350e2 
  WT: DDR, DMA,    481(  1153),    481(  1153),   80(  128),  24080( 147584),   0,    38780 ||||  L2, DMA,    4c0(  1216),    481(  1153),   80(  128),  26000( 155648),   0,    1e000 
 STG:  L2,  NA,      0(     0),      0(     0),    0(    0),      0(      0),   0, ffffffff 
CONT:  L2, CPU,      0(     0),      0(     0),    0(    0),      0(      0),   0,        0 
Padding Info [Row, Col]: Actual  IN -> OUT : [ 1,  1] -> [ 1,  1], Required OUT : [ 0,  1],  To fill zero OUT: [ 0,  0]
------  10(    Conv) [11, 10] --[64 x 56 x  56] => [128 x 28 x  28] *** [64] ***[ROW_L] ***[0, 0, 3192, 3192]**** [1], [1],[1] -[8 ]---
  IN:MSMC, DMA,    d40(  3392),    ceb(  3307),   40(   64),  35080( 217216),  3a,       46 ||||  L2, DMA,    cc0(  3264),    cc0(  3264),   40(   64),  33000( 208896),   0,        0 
 OUT:NONE,  NA,      0(     0),      0(     0),    0(    0),      0(      0),   0, ffffffff ||||MSMC, CPU,    3c0(   960),    367(   871),   80(  128),  1e080( 123008),  1e,    53162 
  WT: DDR, DMA,     41(    65),     41(    65),   80(  128),   2080(   8320),   0,    5c800 ||||  L2, DMA,     c0(   192),     41(    65),   80(  128),   6000(  24576),   0,    33000 
 STG:MSMC, DMA_ONCE,     c0(   192),     c0(   192),   80(  128),   6000(  24576),   0,   7b3f80 
CONT:  L2, CPU,      0(     0),      0(     0),    0(    0),      0(      0),   0,        0 
Padding Info [Row, Col]: Actual  IN -> OUT : [ 1,  1] -> [ 1,  1], Required OUT : [ 0,  0],  To fill zero OUT: [ 0,  0]
------  12( EltWise) [12, 12] --[256 x 28 x  28] => [128 x 28 x  28] *** [256] ***[ COL] ***[0, 0, 811, 812]**** [1], [1],[1] -[10 11 ]---
  IN:MSMC, DMA,    3c0(   960),    367(   871),   80(  128),  1e080( 123008),  1e,    53162 ||||  L2, DMA,    3c0(   960),    3c0(   960),   80(  128),  3c000( 245760),   0,        0 
 OUT:NONE,  NA,      0(     0),      0(     0),    0(    0),      0(      0),   0, ffffffff ||||MSMC, CPU,    3c0(   960),    367(   871),   80(  128),  1e080( 123008),  1e,       62 
  WT: DDR, DMA,      0(     0),      0(     0),  600( 1536),      0(      0),   0,    5e880 ||||  L2, DMA,      0(     0),      0(     0),    0(    0),      0(      0),   0, ffffffff 
 STG:  L2,  NA,      0(     0),      0(     0),    0(    0),      0(      0),   0, ffffffff 
CONT:  L2, CPU,      0(     0),      0(     0),    0(    0),      0(      0),   0,        0 
Padding Info [Row, Col]: Actual  IN -> OUT : [ 1,  1] -> [ 1,  1], Required OUT : [ 1,  1],  To fill zero OUT: [ 1,  1]
------  13(    Conv) [13, 13] --[128 x 28 x  28] => [128 x 28 x  28] *** [128] ***[ROW_L] ***[60, 64, 807, 871]**** [1], [1],[1] -[12 ]---
  IN:MSMC, DMA,    3c0(   960),    367(   871),   80(  128),  1e080( 123008),   0,       62 ||||  L2, DMA,    3c0(   960),    3c0(   960),   80(  128),  1e000( 122880),   0,        0 
 OUT:NONE,  NA,      0(     0),      0(     0),    0(    0),      0(      0),   0, ffffffff ||||MSMC, CPU,    3c0(   960),    367(   871),   80(  128),  1e080( 123008),  1e,    1e0e2 
  WT: DDR, DMA,    481(  1153),    481(  1153),   80(  128),  24080( 147584),   0,    5e880 ||||  L2, DMA,    4c0(  1216),    481(  1153),   80(  128),  26000( 155648),   0,    1e000 
 STG:  L2,  NA,      0(     0),      0(     0),    0(    0),      0(      0),   0, ffffffff 
CONT:  L2, CPU,      0(     0),      0(     0),    0(    0),      0(      0),   0,        0 
Padding Info [Row, Col]: Actual  IN -> OUT : [ 1,  1] -> [ 1,  1], Required OUT : [ 1,  1],  To fill zero OUT: [ 1,  0]
------  14(    Conv) [14, 14] --[128 x 28 x  28] => [128 x 28 x  28] *** [128] ***[ROW_L] ***[60, 64, 807, 871]**** [1], [1],[1] -[13 ]---
  IN:MSMC, DMA,    3c0(   960),    367(   871),   80(  128),  1e080( 123008),   0,    1e0e2 ||||  L2, DMA,    3c0(   960),    3c0(   960),   80(  128),  1e000( 122880),   0,        0 
 OUT:NONE,  NA,      0(     0),      0(     0),    0(    0),      0(      0),   0, ffffffff ||||MSMC, CPU,    3c0(   960),    367(   871),   80(  128),  1e080( 123008),  1e,    1e0e2 
  WT: DDR, DMA,    481(  1153),    481(  1153),   80(  128),  24080( 147584),   0,    82900 ||||  L2, DMA,    4c0(  1216),    481(  1153),   80(  128),  26000( 155648),   0,    1e000 
 STG:  L2,  NA,      0(     0),      0(     0),    0(    0),      0(      0),   0, ffffffff 
CONT:  L2, CPU,      0(     0),      0(     0),    0(    0),      0(      0),   0,        0 
Padding Info [Row, Col]: Actual  IN -> OUT : [ 1,  1] -> [ 1,  1], Required OUT : [ 1,  1],  To fill zero OUT: [ 0,  0]
------  15( EltWise) [15, 15] --[256 x 28 x  28] => [128 x 28 x  28] *** [256] ***[ COL] ***[0, 0, 811, 812]**** [1], [1],[1] -[12 14 ]---
  IN:MSMC, DMA,    3c0(   960),    367(   871),   80(  128),  1e080( 123008),  1e,       62 ||||  L2, DMA,    3c0(   960),    3c0(   960),   80(  128),  3c000( 245760),   0,        0 
 OUT:NONE,  NA,      0(     0),      0(     0),    0(    0),      0(      0),   0, ffffffff ||||MSMC, CPU,    3c0(   960),    367(   871),   80(  128),  1e080( 123008),  1e,       62 
  WT: DDR, DMA,      0(     0),      0(     0),  600( 1536),      0(      0),   0,    a6980 ||||  L2, DMA,      0(     0),      0(     0),    0(    0),      0(      0),   0, ffffffff 
 STG:  L2,  NA,      0(     0),      0(     0),    0(    0),      0(      0),   0, ffffffff 
CONT:  L2, CPU,      0(     0),      0(     0),    0(    0),      0(      0),   0,        0 
Padding Info [Row, Col]: Actual  IN -> OUT : [ 1,  1] -> [ 1,  1], Required OUT : [ 1,  1],  To fill zero OUT: [ 0,  1]
------  16(    Conv) [16, 16] --[128 x 28 x  28] => [256 x 14 x  14] *** [128] ***[ROW_L] ***[30, 30, 522, 871]**** [2], [1],[2] -[15 ]---
  IN:MSMC, DMA,    3c0(   960),    367(   871),   80(  128),  1e080( 123008),   0,       62 ||||  L2, DMA,    440(  1088),    440(  1088),   80(  128),  22000( 139264),   0,        0 
 OUT:NONE,  NA,      0(     0),      0(     0),    0(    0),      0(      0),   0, ffffffff ||||MSMC, CPU,    140(   320),     f1(   241),  100(  256),  14080(  82048),  10,    1e0f0 
  WT: DDR, DMA,    481(  1153),    481(  1153),  100(  256),  48100( 295168),   0,    a6980 ||||  L2, DMA,    4c0(  1216),    481(  1153),  100(  256),  4c000( 311296),   0,    22000 
 STG:  L2,  NA,      0(     0),      0(     0),    0(    0),      0(      0),   0, ffffffff 
CONT:  L2, CPU,      0(     0),      0(     0),    0(    0),      0(      0),   0,        0 
Padding Info [Row, Col]: Actual  IN -> OUT : [ 1,  1] -> [ 1,  1], Required OUT : [ 1,  1],  To fill zero OUT: [ 1,  1]
------  18(    Conv) [17, 18] --[256 x 14 x  14] => [256 x 14 x  14] *** [256] ***[ROW_L] ***[32, 64, 177, 241]**** [1], [4],[4] -[16 ]---
  IN:MSMC, DMA,    140(   320),     f1(   241),  100(  256),  14080(  82048),   0,    1e0f0 ||||  L2, DMA,    140(   320),    140(   320),  100(  256),  14000(  81920),   0,        0 
 OUT:NONE,  NA,      0(     0),      0(     0),    0(    0),      0(      0),   0, ffffffff ||||MSMC, CPU,    140(   320),     f1(   241),  100(  256),  14080(  82048),  10,    c6170 
  WT: DDR, DMA,    901(  2305),    901(  2305),  100(  256),  90100( 590080),   0,    eea80 ||||  L2, DMA,    940(  2368),    901(  2305),   80(  128),  4a000( 303104),   0,    14000 
 STG:MSMC, DMA,    940(  2368),    940(  2368),  100(  256),  94000( 606208),   0,    32100 
CONT:  L2, CPU,      0(     0),      0(     0),    0(    0),      0(      0),   0,        0 
Padding Info [Row, Col]: Actual  IN -> OUT : [ 1,  1] -> [ 1,  1], Required OUT : [ 0,  1],  To fill zero OUT: [ 0,  0]
------  17(    Conv) [18, 17] --[128 x 28 x  28] => [256 x 14 x  14] *** [128] ***[ROW_L] ***[0, 0, 812, 812]**** [1], [1],[1] -[15 ]---
  IN:MSMC, DMA,    3c0(   960),    367(   871),   80(  128),  1e080( 123008),  1e,       62 ||||  L2, DMA,    340(   832),    340(   832),   80(  128),  1a000( 106496),   0,        0 
 OUT:NONE,  NA,      0(     0),      0(     0),    0(    0),      0(      0),   0, ffffffff ||||MSMC, CPU,    140(   320),     f1(   241),  100(  256),  14080(  82048),  10,    1e0f0 
  WT: DDR, DMA,     81(   129),     81(   129),  100(  256),   8100(  33024),   0,   17eb80 ||||  L2, DMA,     c0(   192),     81(   129),  100(  256),   c000(  49152),   0,    1a000 
 STG:  L2,  NA,      0(     0),      0(     0),    0(    0),      0(      0),   0, ffffffff 
CONT:  L2, CPU,      0(     0),      0(     0),    0(    0),      0(      0),   0,        0 
Padding Info [Row, Col]: Actual  IN -> OUT : [ 1,  1] -> [ 1,  1], Required OUT : [ 1,  0],  To fill zero OUT: [ 0,  0]
------  19( EltWise) [19, 19] --[512 x 14 x  14] => [256 x 14 x  14] *** [512] ***[ COL] ***[0, 0, 209, 210]**** [1], [1],[1] -[17 18 ]---
  IN:MSMC, DMA,    140(   320),     f1(   241),  100(  256),  14080(  82048),  10,    1e0f0 ||||  L2, DMA,    140(   320),    140(   320),  100(  256),  28000( 163840),   0,        0 
 OUT:NONE,  NA,      0(     0),      0(     0),    0(    0),      0(      0),   0, ffffffff ||||MSMC, CPU,    140(   320),     f1(   241),  100(  256),  14080(  82048),  10,       70 
  WT: DDR, DMA,      0(     0),      0(     0),  c00( 3072),      0(      0),   0,   186c80 ||||  L2, DMA,      0(     0),      0(     0),    0(    0),      0(      0),   0, ffffffff 
 STG:  L2,  NA,      0(     0),      0(     0),    0(    0),      0(      0),   0, ffffffff 
CONT:  L2, CPU,      0(     0),      0(     0),    0(    0),      0(      0),   0,        0 
Padding Info [Row, Col]: Actual  IN -> OUT : [ 1,  1] -> [ 1,  1], Required OUT : [ 1,  1],  To fill zero OUT: [ 1,  1]
------  20(    Conv) [20, 20] --[256 x 14 x  14] => [256 x 14 x  14] *** [256] ***[ROW_L] ***[32, 64, 177, 241]**** [1], [4],[4] -[19 ]---
  IN:MSMC, DMA,    140(   320),     f1(   241),  100(  256),  14080(  82048),   0,       70 ||||  L2, DMA,    140(   320),    140(   320),  100(  256),  14000(  81920),   0,        0 
 OUT:NONE,  NA,      0(     0),      0(     0),    0(    0),      0(      0),   0, ffffffff ||||MSMC, CPU,    140(   320),     f1(   241),  100(  256),  14080(  82048),  10,    140f0 
  WT: DDR, DMA,    901(  2305),    901(  2305),  100(  256),  90100( 590080),   0,   186c80 ||||  L2, DMA,    940(  2368),    901(  2305),   80(  128),  4a000( 303104),   0,    14000 
 STG:MSMC, DMA_ONCE,    940(  2368),    940(  2368),  100(  256),  94000( 606208),   0,   693580 
CONT:  L2, CPU,      0(     0),      0(     0),    0(    0),      0(      0),   0,        0 
Padding Info [Row, Col]: Actual  IN -> OUT : [ 1,  1] -> [ 1,  1], Required OUT : [ 1,  1],  To fill zero OUT: [ 1,  0]
------  21(    Conv) [21, 21] --[256 x 14 x  14] => [256 x 14 x  14] *** [256] ***[ROW_L] ***[32, 64, 177, 241]**** [1], [4],[4] -[20 ]---
  IN:MSMC, DMA,    140(   320),     f1(   241),  100(  256),  14080(  82048),   0,    140f0 ||||  L2, DMA,    140(   320),    140(   320),  100(  256),  14000(  81920),   0,        0 
 OUT:NONE,  NA,      0(     0),      0(     0),    0(    0),      0(      0),   0, ffffffff ||||MSMC, CPU,    140(   320),     f1(   241),  100(  256),  14080(  82048),  10,    bc170 
  WT: DDR, DMA,    901(  2305),    901(  2305),  100(  256),  90100( 590080),   0,   216d80 ||||  L2, DMA,    940(  2368),    901(  2305),   80(  128),  4a000( 303104),   0,    14000 
 STG:MSMC, DMA,    940(  2368),    940(  2368),  100(  256),  94000( 606208),   0,    28100 
CONT:  L2, CPU,      0(     0),      0(     0),    0(    0),      0(      0),   0,        0 
Padding Info [Row, Col]: Actual  IN -> OUT : [ 1,  1] -> [ 1,  1], Required OUT : [ 0,  1],  To fill zero OUT: [ 0,  0]
------  22( EltWise) [22, 22] --[512 x 14 x  14] => [256 x 14 x  14] *** [512] ***[ COL] ***[0, 0, 209, 210]**** [1], [1],[1] -[19 21 ]---
  IN:MSMC, DMA,    140(   320),     f1(   241),  100(  256),  14080(  82048),  10,       70 ||||  L2, DMA,    140(   320),    140(   320),  100(  256),  28000( 163840),   0,        0 
 OUT:NONE,  NA,      0(     0),      0(     0),    0(    0),      0(      0),   0, ffffffff ||||MSMC, CPU,    140(   320),     f1(   241),  100(  256),  14080(  82048),  10,       70 
  WT: DDR, DMA,      0(     0),      0(     0),  c00( 3072),      0(      0),   0,   2a6e80 ||||  L2, DMA,      0(     0),      0(     0),    0(    0),      0(      0),   0, ffffffff 
 STG:  L2,  NA,      0(     0),      0(     0),    0(    0),      0(      0),   0, ffffffff 
CONT:  L2, CPU,      0(     0),      0(     0),    0(    0),      0(      0),   0,        0 
Padding Info [Row, Col]: Actual  IN -> OUT : [ 1,  1] -> [ 1,  1], Required OUT : [ 1,  1],  To fill zero OUT: [ 0,  1]
------  23(    Conv) [23, 23] --[256 x 14 x  14] => [512 x 7 x  7] *** [256] ***[ROW_L] ***[16, 16, 225, 241]**** [1], [8],[8] -[22 ]---
  IN:MSMC, DMA,    140(   320),     f1(   241),  100(  256),  14080(  82048),   0,       70 ||||  L2, DMA,    140(   320),    140(   320),  100(  256),  14000(  81920),   0,        0 
 OUT:NONE,  NA,      0(     0),      0(     0),    0(    0),      0(      0),   0, ffffffff ||||MSMC, CPU,     c0(   192),     49(    73),  200(  512),  18080(  98432),   9,    140f7 
  WT: DDR, DMA,    901(  2305),    901(  2305),  200(  512), 120200(1180160),   0,   2a6e80 ||||  L2, DMA,    940(  2368),    901(  2305),   80(  128),  4a000( 303104),   0,    14000 
 STG:MSMC, DMA_ONCE,    940(  2368),    940(  2368),  200(  512), 128000(1212416),   0,   56b580 
CONT:  L2, CPU,      0(     0),      0(     0),    0(    0),      0(      0),   0,        0 
Padding Info [Row, Col]: Actual  IN -> OUT : [ 1,  1] -> [ 1,  1], Required OUT : [ 1,  1],  To fill zero OUT: [ 1,  1]
------  25(    Conv) [24, 25] --[512 x 7 x  7] => [512 x 7 x  7] *** [512] ***[ROW_L] ***[18, 64, 9, 73]**** [1], [13],[13] -[23 ]---
  IN:MSMC, DMA,     c0(   192),     49(    73),  200(  512),  18080(  98432),   0,    140f7 ||||  L2, DMA,     80(   128),     80(   128),  200(  512),  10000(  65536),   0,        0 
 OUT:NONE,  NA,      0(     0),      0(     0),    0(    0),      0(      0),   0, ffffffff ||||MSMC, CPU,     c0(   192),     49(    73),  200(  512),  1af00( 110336),   9,   2bacf7 
  WT: DDR, DMA,   1201(  4609),   1201(  4609),  200(  512), 240200(2359808),   0,   3c7080 ||||  L2, DMA,   1240(  4672),   1201(  4609),   52(   82),  5d880( 383104),   0,    10000 
 STG:MSMC, DMA_ONCE,   1475(  5237),   1475(  5237),  200(  512), 28eb80(2681728),   0,   2dca00 
CONT:  L2, CPU,      0(     0),      0(     0),    0(    0),      0(      0),   0,        0 
Padding Info [Row, Col]: Actual  IN -> OUT : [ 1,  1] -> [ 1,  1], Required OUT : [ 0,  1],  To fill zero OUT: [ 0,  0]
------  24(    Conv) [25, 24] --[256 x 14 x  14] => [512 x 7 x  7] *** [256] ***[ROW_L] ***[0, 0, 210, 210]**** [1], [1],[1] -[22 ]---
  IN:MSMC, DMA,    140(   320),     f1(   241),  100(  256),  14080(  82048),  10,       70 ||||  L2, DMA,    140(   320),    140(   320),  100(  256),  14000(  81920),   0,        0 
 OUT:NONE,  NA,      0(     0),      0(     0),    0(    0),      0(      0),   0, ffffffff ||||MSMC, CPU,     c0(   192),     49(    73),  200(  512),  18080(  98432),   9,    140f7 
  WT: DDR, DMA,    101(   257),    101(   257),  200(  512),  20200( 131584),   0,   607280 ||||  L2, DMA,    140(   320),    101(   257),  200(  512),  28000( 163840),   0,    14000 
 STG:  L2,  NA,      0(     0),      0(     0),    0(    0),      0(      0),   0, ffffffff 
CONT:  L2, CPU,      0(     0),      0(     0),    0(    0),      0(      0),   0,        0 
Padding Info [Row, Col]: Actual  IN -> OUT : [ 1,  1] -> [ 1,  1], Required OUT : [ 1,  0],  To fill zero OUT: [ 0,  0]
------  26( EltWise) [26, 26] --[1024 x 7 x  7] => [512 x 7 x  7] *** [1024] ***[ COL] ***[0, 0, 55, 56]**** [1], [1],[1] -[24 25 ]---
  IN:MSMC, DMA,     c0(   192),     49(    73),  200(  512),  18080(  98432),   9,    140f7 ||||  L2, DMA,     c0(   192),     c0(   192),  200(  512),  30000( 196608),   0,        0 
 OUT:NONE,  NA,      0(     0),      0(     0),    0(    0),      0(      0),   0, ffffffff ||||MSMC, CPU,     c0(   192),     49(    73),  200(  512),  18080(  98432),   9,       77 
  WT: DDR, DMA,      0(     0),      0(     0), 1800( 6144),      0(      0),   0,   627480 ||||  L2, DMA,      0(     0),      0(     0),    0(    0),      0(      0),   0, ffffffff 
 STG:  L2,  NA,      0(     0),      0(     0),    0(    0),      0(      0),   0, ffffffff 
CONT:  L2, CPU,      0(     0),      0(     0),    0(    0),      0(      0),   0,        0 
Padding Info [Row, Col]: Actual  IN -> OUT : [ 1,  1] -> [ 1,  1], Required OUT : [ 1,  1],  To fill zero OUT: [ 1,  1]
------  27(    Conv) [27, 27] --[512 x 7 x  7] => [512 x 7 x  7] *** [512] ***[ROW_L] ***[18, 64, 9, 73]**** [1], [13],[13] -[26 ]---
  IN:MSMC, DMA,     c0(   192),     49(    73),  200(  512),  18080(  98432),   0,       77 ||||  L2, DMA,     80(   128),     80(   128),  200(  512),  10000(  65536),   0,        0 
 OUT:NONE,  NA,      0(     0),      0(     0),    0(    0),      0(      0),   0, ffffffff ||||MSMC, CPU,     c0(   192),     49(    73),  200(  512),  1af00( 110336),   9,    180f7 
  WT: DDR, DMA,   1201(  4609),   1201(  4609),  200(  512), 240200(2359808),   0,   627480 ||||  L2, DMA,   1240(  4672),   1201(  4609),   52(   82),  5d880( 383104),   0,    10000 
 STG:MSMC, DMA,   1475(  5237),   1475(  5237),  200(  512), 28eb80(2681728),   0,    32f80 
CONT:  L2, CPU,      0(     0),      0(     0),    0(    0),      0(      0),   0,        0 
Padding Info [Row, Col]: Actual  IN -> OUT : [ 1,  1] -> [ 1,  1], Required OUT : [ 1,  1],  To fill zero OUT: [ 1,  0]
------  28(    Conv) [28, 28] --[512 x 7 x  7] => [512 x 7 x  7] *** [512] ***[ROW_L] ***[18, 64, 9, 73]**** [1], [13],[13] -[27 ]---
  IN:MSMC, DMA,     c0(   192),     49(    73),  200(  512),  1af00( 110336),   0,    180f7 ||||  L2, DMA,     80(   128),     80(   128),  200(  512),  10000(  65536),   0,        0 
 OUT:NONE,  NA,      0(     0),      0(     0),    0(    0),      0(      0),   0, ffffffff ||||MSMC, CPU,     c0(   192),     49(    73),  200(  512),  1af00( 110336),   9,   2c1b77 
  WT: DDR, DMA,   1201(  4609),   1201(  4609),  200(  512), 240200(2359808),   0,   867680 ||||  L2, DMA,   1240(  4672),   1201(  4609),   52(   82),  5d880( 383104),   0,    10000 
 STG:MSMC, DMA,   1475(  5237),   1475(  5237),  200(  512), 28eb80(2681728),   0,    32f80 
CONT:  L2, CPU,      0(     0),      0(     0),    0(    0),      0(      0),   0,        0 
Padding Info [Row, Col]: Actual  IN -> OUT : [ 1,  1] -> [ 1,  1], Required OUT : [ 0,  1],  To fill zero OUT: [ 0,  0]
------  29( EltWise) [29, 29] --[1024 x 7 x  7] => [512 x 7 x  7] *** [1024] ***[ COL] ***[0, 0, 55, 56]**** [1], [1],[1] -[26 28 ]---
  IN:MSMC, DMA,     c0(   192),     49(    73),  200(  512),  18080(  98432),   9,       77 ||||  L2, DMA,     c0(   192),     c0(   192),  200(  512),  30000( 196608),   0,        0 
 OUT:NONE,  NA,      0(     0),      0(     0),    0(    0),      0(      0),   0, ffffffff ||||MSMC, CPU,     c0(   192),     49(    73),  200(  512),  18080(  98432),   9,       77 
  WT: DDR, DMA,      0(     0),      0(     0), 1800( 6144),      0(      0),   0,   aa7880 ||||  L2, DMA,      0(     0),      0(     0),    0(    0),      0(      0),   0, ffffffff 
 STG:  L2,  NA,      0(     0),      0(     0),    0(    0),      0(      0),   0, ffffffff 
CONT:  L2, CPU,      0(     0),      0(     0),    0(    0),      0(      0),   0,        0 
Padding Info [Row, Col]: Actual  IN -> OUT : [ 1,  1] -> [ 1,  1], Required OUT : [ 1,  0],  To fill zero OUT: [ 0,  0]
------  30(    Pool) [30, 30] --[512 x 7 x  7] => [512 x 1 x  1] *** [512] ***[ COL] ***[0, 0, 55, 56]**** [1], [1],[1] -[29 ]---
  IN:MSMC, DMA,     c0(   192),     49(    73),  200(  512),  18080(  98432),   9,       77 ||||  L2, DMA,     c0(   192),     c0(   192),  200(  512),  18000(  98304),   0,        0 
 OUT:NONE,  NA,      0(     0),      0(     0),    0(    0),      0(      0),   0, ffffffff ||||MSMC, CPU,      7(     7),      7(     7),  200(  512),    e80(   3712),   3,       7d 
  WT: DDR, DMA,      0(     0),      0(     0), 1800( 6144),      0(      0),   0,   aa7880 ||||  L2, DMA,      0(     0),      0(     0),    0(    0),      0(      0),   0, ffffffff 
 STG:  L2,  NA,      0(     0),      0(     0),    0(    0),      0(      0),   0, ffffffff 
CONT:  L2, CPU,      0(     0),      0(     0),    0(    0),      0(      0),   0,        0 
Padding Info [Row, Col]: Actual  IN -> OUT : [ 1,  1] -> [ 1,  1], Required OUT : [ 0,  0],  To fill zero OUT: [ 0,  0]
------  31(      FC) [31, 31] --[512 x 1 x  1] => [1000 x 1 x  1] *** [512] ***[ COL] ***[0, 0, 1, 2]**** [4], [1],[4] -[30 ]---
  IN:MSMC, DMA,      7(     7),      7(     7),  200(  512),    e80(   3712),   3,       7d ||||  L2, DMA,      7(     7),      7(     7),  1f4(  500),    e00(   3584),   0,        0 
 OUT:MSMC,  NA,      2(     2),      0(     0),  3e8( 1000),    3e8(   1000),   0,    8d880 |||| DDR, DMA,      2(     2),      1(     1),  3e8( 1000),    c00(   3072),   0,   b24880 
  WT: DDR, DMA_ONCE,    200(   512),    200(   512),  3e8( 1000),  7d000( 512000),   0,   aa7880 ||||MSMC, DMA_ONCE,    240(   576),    200(   512),  3e8( 1000),  8ca00( 576000),   0,   727580 
 STG:  L2,  NA,      0(     0),      0(     0),    0(    0),      0(      0),   0, ffffffff 
CONT:  L2, CPU,      0(     0),      0(     0),    0(    0),      0(      0),   0,        0 
Padding Info [Row, Col]: Actual  IN -> OUT : [ 1,  1] -> [ 0,  0], Required OUT : [ 0,  0],  To fill zero OUT: [ 0,  0]
------  32(    Data) [32, 0] --[1 x 1000 x  1] => [0 x 0 x  0] *** [1] ***[FRAME] ***[0, 0, 1000, 1000]**** [1], [1],[1] -[31 ]---
  IN: DDR, DMA,      2(     2),      1(     1),  3e8( 1000),    c00(   3072),   0,   b24880 ||||  L2, DMA,      0(     0),    3e8(  1000),    1(    1),      0(      0),   0,        0 
 OUT:NONE,  NA,      0(     0),      0(     0),    0(    0),      0(      0),   0, ffffffff |||| DDR, DMA,   cbf6( 52214),      0(     0),    0(    0),  26800( 157696),   0,   b248d4 
  WT:NONE,  NA,      0(     0),      0(     0),    0(    0),      0(      0),   0,   b24880 ||||  L2, CPU,      0(     0),      0(     0),    0(    0),      0(      0),   0, ffffffff 
 STG:  L2,  NA,      0(     0),      0(     0),    0(    0),      0(      0),   0, ffffffff 
CONT:  L2, CPU,      0(     0),      0(     0),    0(    0),      0(      0),   0,        0 
Padding Info [Row, Col]: Actual  IN -> OUT : [ 0,  0] -> [ 0,  0], Required OUT : [ 3,  3],  To fill zero OUT: [ 3,  3]
