modelType          = 3
numParamBits      = 8
numFeatureBits      = 8
inputNetFile      = ../../test/testvecs/models/public/tflite/efficientnet_lite4_fp32_2.tflite
outputNetFile      = "../../test/testvecs/config/tidl_models/tflite/tidl_net_tflite_efficientnet_lite4.bin"
outputParamsFile   = "../../test/testvecs/config/tidl_models/tflite/tidl_io_tflite_efficientnet_lite4_"
inDataNorm  = 1
inMean = 128 128 128
inScale =  0.0078125 0.0078125 0.0078125
resizeWidth = 332
resizeHeight = 332
inWidth  = 300
inHeight = 300 
inNumChannels = 3
inData = ../../test/testvecs/config/imageNet_sample_val.txt
postProcType = 1
quantizationStyle = 3
numFrames = 2
inDataNamesList = "images"
outDataNamesList = "Softmax"
compileConstraintsFlag = 1601



