modelType          = 3
numParamBits      = 8
numFeatureBits      = 8
inputNetFile      = ../../test/testvecs/models/public/tflite/efficientdet-lite4_512x512_fp32.tflite
outputNetFile      = "../../test/testvecs/config/tidl_models/tflite/efficientdet-lite4_512x512_fp32.bin"
outputParamsFile   = "../../test/testvecs/config/tidl_models/tflite/efficientdet-lite4_512x512_fp32_"
inDataNorm  = 1
inMean = 128 128 128
inScale =  0.0078125 0.0078125 0.0078125
resizeWidth = 512
resizeHeight = 512
inWidth  = 512
inHeight = 512
inNumChannels = 3
inData = ../../test/testvecs/config/imageNet_sample_val.txt
postProcType = 1
quantizationStyle = 3
numFrames = 2
debugTraceLevel = 0
inDataNamesList = "serving_default_input_1:0"
outDataNamesList = "StatefulPartitionedCall:9,StatefulPartitionedCall:8,StatefulPartitionedCall:7,StatefulPartitionedCall:6,StatefulPartitionedCall:5,StatefulPartitionedCall:4,StatefulPartitionedCall:3,StatefulPartitionedCall:2,StatefulPartitionedCall:1,StatefulPartitionedCall:0"



