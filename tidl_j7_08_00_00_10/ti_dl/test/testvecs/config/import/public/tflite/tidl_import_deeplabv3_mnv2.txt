modelType          = 3
numParamBits      = 16
numFeatureBits    = 8
quantizationStyle  = 3
inputNetFile      = "../../test/testvecs/models/public/tflite/deplabv3/deeplabv3_mnv2_pascal_train_aug_8bit_2019_04_26/frozen_inference_graph.tflite"
outputNetFile      = "../../test/testvecs/config/tidl_models/tflite/tidl_net_deeplabv3_mnv2.bin"
outputParamsFile   = "../../test/testvecs/config/tidl_models/tflite/tidl_io_deeplabv3_mnv2_"
inDataNorm  = 1
inMean = 128 128 128
inScale =  0.0078125 0.0078125 0.0078125
inWidth  = 513
inHeight = 513
inNumChannels = 3
inData = ../../test/testvecs/config/segmentation_list.txt
postProcType = 3
perfSimConfig = ../../test/testvecs/config/import/device_config.cfg
