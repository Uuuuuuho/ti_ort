modelType          = 2
numParamBits       = 8
inputNetFile      = "../../test/testvecs/models/public/onnx/resnet18v1.onnx"
outputNetFile      = "../../test/testvecs/config/tidl_models/onnx/tidl_net_resnet18v1.bin"
outputParamsFile   = "../../test/testvecs/config/tidl_models/onnx/tidl_io_resnet18v1_"
inDataNorm  = 1
inMean = 123.675 116.28 103.53
inScale = 0.017125 0.017507 0.017429
#inWidth  = 224
#inHeight = 224 
#inNumChannels = 3
inData = ../../test/testvecs/config/imageNet_sample_val.txt
postProcType = 1
perfSimConfig = ../../test/testvecs/config/import/device_config.cfg

