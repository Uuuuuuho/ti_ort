modelType          = 1
numParamBits       = 8
numFeatureBits     = 8
quantizationStyle  = 2
inputNetFile      = ../../test/testvecs/models/public/tensorflow/mobilenet_v1_2.0_ssd/frozen_inference_graph_opt_1.pb"
outputNetFile      = "../../test/testvecs/config/tidl_models/tensorflow/tidl_net_mobilenet_v2_ssd.bin"
outputParamsFile   = "../../test/testvecs/config/tidl_models/tensorflow/tidl_io_mobilenet_v2_ssd_"
inDataNorm  = 1
inMean = 128 128 128
inScale =  0.0078125 0.0078125 0.0078125
inWidth  = 300
inHeight = 300 
inNumChannels = 3
inDataNamesList = "Preprocessor/sub"
outDataNamesList = "BoxPredictor_0/BoxEncodingPredictor/BiasAdd,BoxPredictor_0/ClassPredictor/BiasAdd,BoxPredictor_1/BoxEncodingPredictor/BiasAdd,BoxPredictor_1/ClassPredictor/BiasAdd,BoxPredictor_2/BoxEncodingPredictor/BiasAdd,BoxPredictor_2/ClassPredictor/BiasAdd,BoxPredictor_3/BoxEncodingPredictor/BiasAdd,BoxPredictor_3/ClassPredictor/BiasAdd,BoxPredictor_4/BoxEncodingPredictor/BiasAdd,BoxPredictor_4/ClassPredictor/BiasAdd,BoxPredictor_5/BoxEncodingPredictor/BiasAdd,BoxPredictor_5/ClassPredictor/BiasAdd"
metaArchType = 1
metaLayersNamesList = "../../test/testvecs/config/import/public/tensorflow/mobilenet_ssd_pipeline.config"
inData  =   "../../test/testvecs/config/detection_list.txt"
postProcType = 2
perfSimConfig = ../../test/testvecs/config/import/public/tensorflow/mnet_v2_ssd_device_config.cfg



