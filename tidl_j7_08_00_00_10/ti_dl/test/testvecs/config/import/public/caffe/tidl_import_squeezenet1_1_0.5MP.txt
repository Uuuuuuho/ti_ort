# 0: Caffe, 1: TensorFlow, 2: onnx Default - 0
modelType          = 0
#numParamBits      = 8
inputNetFile       = "../../test/testvecs/models/public/caffe/squeezeNet1.1/deploy.prototxt"
inputParamsFile    = "../../test/testvecs/models/public/caffe/squeezeNet1.1/squeezenet_v1.1.caffemodel"
outputNetFile      = "../../test/testvecs/config/tidl_models/caffe/tidl_net_suqeezenet_1_1_0.5mp.bin"
outputParamsFile   = "../../test/testvecs/config/tidl_models/caffe/tidl_io_suqeezenet_1_1_0.5mp_"
inDataNorm  = 1
inMean = 104 117 123
inScale = 1 1 1
#1 : TIDL_inDataFormatRGBPlanar, 0: TIDL_inDataFormatBGRPlanar, Default - 1
inDataFormat = 0
resizeWidth = 1024
resizeHeight = 512
inWidth  = 1024
inHeight = 512
inNumChannels = 3
numFrames = 1
inData = ../../test/testvecs/config/imageNet_sample_val.txt
postProcType = 1
perfSimConfig = ../../test/testvecs/config/import/device_config.cfg
compileConstraintsFlag = 609
enableHighResOptimization = 1
