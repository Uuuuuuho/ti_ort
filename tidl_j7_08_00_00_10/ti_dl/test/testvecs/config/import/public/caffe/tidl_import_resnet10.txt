modelType          = 0
inputNetFile       = "../../test/testvecs/models/public/caffe/resNet10/deploy.prototxt"
inputParamsFile    = "../../test/testvecs/models/public/caffe/resNet10/resnet10_cvgj_iter_320000.caffemodel"
outputNetFile      = "../../test/testvecs/config/tidl_models/caffe/tidl_net_resnet10.bin"
outputParamsFile   = "../../test/testvecs/config/tidl_models/caffe/tidl_io_resnet10_"
inDataFormat = 0
resizeWidth = 256
resizeHeight = 256
inData = ../../test/testvecs/config/imageNet_sample_val.txt
postProcType = 1
quantRangeExpansionFactor = 1.5

#inWidth  = 1024
#inHeight = 512
#inNumChannels = 3
perfSimConfig = ../../test/testvecs/config/import/device_config.cfg
