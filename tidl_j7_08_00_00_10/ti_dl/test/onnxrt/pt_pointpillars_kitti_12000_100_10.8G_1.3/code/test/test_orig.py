# Copyright 2019 Xilinx Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


# This code is based on: https://github.com/nutonomy/second.pytorch.git
# 
# MIT License
# Copyright (c) 2018 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import warnings
warnings.filterwarnings("ignore")

import torch
import time
import os
import sys
import argparse
import numpy as np
from pathlib import Path
import pickle
from builder import voxel_builder
from builder import box_coder_builder
from builder import target_assigner_builder
from builder import second_builder
from builder import input_reader_builder
from datasets.dataset_tools import merge_second_batch
import datasets.kitti_common as kitti
from utils.progress_bar import ProgressBar
from utils.eval import get_official_eval_result, get_coco_eval_result
from second.protos import pipeline_pb2
from google.protobuf import text_format

if os.environ["W_QUANT"]=='1':
    from pytorch_nndct.apis import torch_quantizer, dump_xmodel

import pdb

#############################
#  ORT IMPORT 
#############################
import onnxruntime as rt
from m_config import *
import multiprocessing
import platform
os.environ["TIDL_RT_PERFSTATS"] = "1"



so = rt.SessionOptions()
so.enable_profiling = True          # <-Profile function enabled



""" set EP_list """
# CPU COMPILE OFFLOAD
CPU = 1
COMPILE = 2
OFFLOAD = 3
ONNX_EXPORT = 4


# models = ['pfe', 'rpn']
models = [  'pfe',
            'rpn_sub1', 'rpn_sub2', 'rpn_sub3',
            'rpn_tc2',
            # 'rpn_tc1', 'rpn_tc2', 'rpn_tc3',
            'rpn_box' , 'rpn_cls', 'rpn_dir',
            'rpn_cat']

log = f'\nRunning {len(models)} Models - {models}\n'
print(log)

EP = CPU

if EP==CPU:
    EP_list = ['CPUExecutionProvider']
elif EP==COMPILE:    
    EP_list = ['TIDLCompilationProvider','CPUExecutionProvider']
elif EP==OFFLOAD:
    # EP_list = ['CPUExecutionProvider']
    EP_list = ['TIDLExecutionProvider','CPUExecutionProvider']
else:
    EP_list = ['']



calib_images = ['/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/tidl_j7_08_00_00_10/ti_dl/test/testvecs/input/airshow.jpg',
                '/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/tidl_j7_08_00_00_10/ti_dl/test/testvecs/input/ADE_val_00001801.jpg']
class_test_images = ['/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/tidl_j7_08_00_00_10/ti_dl/test/testvecs/input/airshow.jpg']
od_test_images    = ['/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/tidl_j7_08_00_00_10/ti_dl/test/testvecs/input/ADE_val_00001801.jpg']
seg_test_images   = ['/home/uho/workspace/ti-processor-sdk-rtos-j721e-evm-08_00_00_12/tidl_j7_08_00_00_10/ti_dl/test/testvecs/input/ADE_val_00001801.jpg']





def example_convert_to_torch(example, dtype, device) -> dict:
    example_torch = {}
    float_names = [ 
        "voxels", "anchors", "reg_targets", "reg_weights", "bev_map", "rect",
        "Trv2c", "P2"
    ]   

    for k, v in example.items():
        if k in float_names:
            example_torch[k] = torch.as_tensor(v, dtype=dtype, device=device)
        elif k in ["coordinates", "labels", "num_points"]:
            example_torch[k] = torch.as_tensor(
                v, dtype=torch.int32, device=device)
        elif k in ["anchors_mask"]:
            example_torch[k] = torch.as_tensor(
                v, dtype=torch.uint8, device=device)
        else:
            example_torch[k] = v 
    return example_torch


def onnx_export(net,
                preprocessor,
                postprocessor,
                example,
                class_names,
                center_limit_range=None,
                lidar_input=False,
                global_set=None):
    batch_image_shape = example['image_shape']
    batch_imgidx = example['image_idx']

    voxels = example["voxels"]
    num_points = example["num_points"]
    coors = example["coordinates"]
    # batch_anchors = example["anchors"]

    prev_t = time.time()
    aug_voxels = preprocessor(voxels, num_points, coors)
    curr_t = time.time()
    print("pre-process: ", (curr_t - prev_t) * 1000, "[ms]")    

    """ EXPORT ONNX HERE """

    ############
    # PFE EXPORT
    ############
    # torch.onnx.export(net, aug_voxels ,"pfe.onnx", verbose = False)
    
    ############
    # RPN EXPORT
    ############
    # pdb.set_trace()
    # rpn_input_feature = net.forward_FOR_RPN(aug_voxels, coors)
    # rpn_input_feature = torch.ones([1, 64, 62, 54], dtype=torch.float32)
    # rpn_input_feature = torch.ones([1, 64, 496, 432], dtype=torch.float32)
    # rpn_input_feature = torch.ones([1, 32, 248, 216], dtype=torch.float32)
    # rpn_input_feature = torch.ones([1, 32, 124, 108], dtype=torch.float32)
    # rpn_input_feature = torch.ones([1, 192, 248, 216], dtype=torch.float32)
    # torch.onnx.export(net, rpn_input_feature,"rpn.onnx", verbose = False)
    
    rpn_input_feature_1 = torch.ones([1, 42, 248, 216], dtype=torch.float32)
    rpn_input_feature_2 = torch.ones([1, 18, 248, 216], dtype=torch.float32)
    rpn_input_feature_3 = torch.ones([1, 12, 248, 216], dtype=torch.float32)
    rpn_input_feature = [rpn_input_feature_1, rpn_input_feature_2, rpn_input_feature_3]
    torch.onnx.export(net, rpn_input_feature, "rpn.onnx", verbose = False)
    
    return

def get_benchmark_output(interpreter):
    benchmark_dict = interpreter.get_TI_benchmark_data()
    proc_time = copy_time = 0
    cp_in_time = cp_out_time = 0
    subgraphIds = []
    for stat in benchmark_dict.keys():
        if 'proc_start' in stat:
            value = stat.split("ts:subgraph_")
            value = value[1].split("_proc_start")
            subgraphIds.append(value[0])
    for i in range(len(subgraphIds)):
        proc_time += benchmark_dict['ts:subgraph_'+str(subgraphIds[i])+'_proc_end'] - benchmark_dict['ts:subgraph_'+str(subgraphIds[i])+'_proc_start']
        cp_in_time += benchmark_dict['ts:subgraph_'+str(subgraphIds[i])+'_copy_in_end'] - benchmark_dict['ts:subgraph_'+str(subgraphIds[i])+'_copy_in_start']
        cp_out_time += benchmark_dict['ts:subgraph_'+str(subgraphIds[i])+'_copy_out_end'] - benchmark_dict['ts:subgraph_'+str(subgraphIds[i])+'_copy_out_start']
        copy_time += cp_in_time + cp_out_time
    copy_time = copy_time if len(subgraphIds) == 1 else 0
    totaltime = benchmark_dict['ts:run_end'] -  benchmark_dict['ts:run_start']
    return copy_time, proc_time, totaltime    

def infer_test(sess, input_tensor, model):
    start_time = time.time()

    if model == 'rpn_cat':
        """ RPN OUTPUT """
        rpn_input_name  = sess.get_inputs()[0].name
        rpn_input_name_1  = sess.get_inputs()[1].name
        rpn_input_name_2  = sess.get_inputs()[2].name
        rpn_output_name   = sess.get_outputs()[0].name
        prev_t = time.time()
        output = list(sess.run([rpn_output_name],
                        {
                            rpn_input_name:input_tensor[0], 
                            rpn_input_name_1:input_tensor[1], 
                            rpn_input_name_2:input_tensor[2]
                        }))
        curr_t = time.time()
        print("Inference time: ", (curr_t - prev_t) * 1000, "[ms]")
        stop_time = time.time()
        infer_time = stop_time - start_time
        copy_time, sub_graphs_proc_time, totaltime = get_benchmark_output(sess)
        proc_time = totaltime - copy_time
        return output, proc_time, sub_graphs_proc_time    

    elif model == 'rpn_trans':
        rpn_input_name    = sess.get_inputs()[0].name
        rpn_input_name_1  = sess.get_inputs()[1].name
        rpn_input_name_2  = sess.get_inputs()[2].name
        rpn_output_name   = sess.get_outputs()[0].name
        rpn_output_name_1 = sess.get_outputs()[1].name
        rpn_output_name_2 = sess.get_outputs()[2].name
        prev_t = time.time()
        output = list(sess.run([rpn_output_name, rpn_output_name_1, rpn_output_name_2], 
                        {
                            rpn_input_name:input_tensor[0], 
                            rpn_input_name_1:input_tensor[1], 
                            rpn_input_name_2:input_tensor[2]
                        }))
        curr_t = time.time()
        print("Inference time: ", (curr_t - prev_t) * 1000, "[ms]")
        stop_time = time.time()
        infer_time = stop_time - start_time
        copy_time, sub_graphs_proc_time, totaltime = get_benchmark_output(sess)
        proc_time = totaltime - copy_time
        return output, proc_time, sub_graphs_proc_time    

    else:
    
        rpn_input_name  = sess.get_inputs()[0].name
        rpn_output_name = sess.get_outputs()[0].name
        
        prev_t = time.time()
        output = list(sess.run([rpn_output_name], {rpn_input_name:input_tensor}))
        curr_t = time.time()
        print("Inference time: ", (curr_t - prev_t) * 1000, "[ms]")
        
        stop_time = time.time()
        infer_time = stop_time - start_time
        copy_time, sub_graphs_proc_time, totaltime = get_benchmark_output(sess)
        proc_time = totaltime - copy_time

        return output, proc_time, sub_graphs_proc_time    




def compile_model(model, mIdx):
    print("\nRunning_Model : ", model, " \n")
    # pdb.set_trace()
    config = models_configs[model]

    #onnx shape inference
    #if not os.path.isfile(os.path.join(models_base_path, model + '_shape.onnx')):
    #    print("Writing model with shapes after running onnx shape inference -- ", os.path.join(models_base_path, model + '_shape.onnx'))
    #    onnx.shape_inference.infer_shapes_path(config['model_path'], config['model_path'])#os.path.join(models_base_path, model + '_shape.onnx'))


    #set input images for demo
    config = models_configs[model]
    if config['model_type'] == 'classification':
        test_images = class_test_images
    elif config['model_type'] == 'od':
        test_images = od_test_images
    elif config['model_type'] == 'seg':
        test_images = seg_test_images
    
    delegate_options = {}
    delegate_options.update(required_options)
    delegate_options.update(optional_options)  

    delegate_options['artifacts_folder'] = delegate_options['artifacts_folder'] + '/' + model + '/' #+ 'tempDir/' 

    if config['model_type'] == 'od':
        delegate_options['object_detection:meta_layers_names_list'] = config['meta_layers_names_list'] if ('meta_layers_names_list' in config) else ''
        delegate_options['object_detection:meta_arch_type'] = config['meta_arch_type'] if ('meta_arch_type' in config) else -1

    
    if EP==COMPILE:
        # delete the contents of this folder
        os.makedirs(delegate_options['artifacts_folder'], exist_ok=True)
        for root, dirs, files in os.walk(delegate_options['artifacts_folder'], topdown=False):
            [os.remove(os.path.join(root, f)) for f in files]
            [os.rmdir(os.path.join(root, d)) for d in dirs]

    input_image = calib_images
    input_image = test_images

    numFrames = config['num_images']
    if numFrames > delegate_options['advanced_options:calibration_frames']:
        numFrames = delegate_options['advanced_options:calibration_frames']
    
    print("set interpreter!!")

    sess = rt.InferenceSession(config['model_path'] ,providers=EP_list, provider_options=[delegate_options, {}], sess_options=so)


    if model == 'pfe':
        """ PFE INPUT """
        
        rpn_inputs = torch.ones([1, 4, 12000, 100], dtype=torch.float32).tolist()
        # pfe_inputs = torch.ones([1, 4, 12000, 100], dtype=torch.float32).tolist()

    elif model == 'rpn_sub1':
        """ RPN INPUT """
        
        rpn_inputs = torch.ones([1, 64, 496, 432], dtype=torch.float32).tolist()
    
    elif model == 'rpn_sub2':
        """ RPN INPUT """
        
        rpn_inputs = torch.ones([1, 32, 248, 216], dtype=torch.float32).tolist()

    elif model == 'rpn_sub3':
        """ RPN INPUT """
        
        rpn_inputs = torch.ones([1, 32, 124, 108], dtype=torch.float32).tolist()

    elif model == 'rpn_tc1':
        """ RPN INPUT """
        
        rpn_inputs = torch.ones([1, 32, 248, 216], dtype=torch.float32).tolist()

    elif model == 'rpn_tc2':
        """ RPN INPUT """
        
        rpn_inputs = torch.ones([1, 32, 124, 108], dtype=torch.float32).tolist()

    elif model == 'rpn_tc3':
        """ RPN INPUT """
        
        rpn_inputs = torch.ones([1, 64, 62, 54], dtype=torch.float32).tolist()

    elif model == 'rpn_box':
        """ RPN INPUT """
        
        rpn_inputs = torch.ones([1, 192,248, 216], dtype=torch.float32).tolist()

    elif model == 'rpn_cls':
        """ RPN INPUT """
        
        rpn_inputs = torch.ones([1, 192, 248, 216], dtype=torch.float32).tolist()

    elif model == 'rpn_dir':
        """ RPN INPUT """
        
        rpn_inputs = torch.ones([1, 192, 248, 216], dtype=torch.float32).tolist()

    elif model == 'rpn_trans':
        """ RPN INPUT """
        
        rpn_inputs1 = torch.ones([1, 42, 248, 216], dtype=torch.float32).tolist()
        rpn_inputs2 = torch.ones([1, 18, 248, 216], dtype=torch.float32).tolist()
        rpn_inputs3 = torch.ones([1, 12, 248, 216], dtype=torch.float32).tolist()
        rpn_inputs = (rpn_inputs1, rpn_inputs2, rpn_inputs3)

    elif model == 'rpn_cat':
        """ RPN INPUT """
        
        rpn_inputs1 = torch.ones([1, 64, 248, 216], dtype=torch.float32).tolist()
        rpn_inputs2 = torch.ones([1, 64, 248, 216], dtype=torch.float32).tolist()
        rpn_inputs3 = torch.ones([1, 64, 248, 216], dtype=torch.float32).tolist()
        rpn_inputs = (rpn_inputs1, rpn_inputs2, rpn_inputs3)

    # run session
    for i in range(numFrames):
        output, proc_time, sub_graph_time = infer_test(sess, rpn_inputs, model)
        total_proc_time = total_proc_time + proc_time if ('total_proc_time' in locals()) else proc_time
        sub_graphs_time = sub_graphs_time + sub_graph_time if ('sub_graphs_time' in locals()) else sub_graph_time

    log = f'\n \nCompleted_Model : {mIdx+1:5d}, Name : {model:50s}, Total time : {total_proc_time/(i+1):10.1f}, Offload Time : {sub_graphs_time/(i+1):10.1f} , DDR RW MBs : 0 \n \n ' #{classes} \n \n'
    print(log) 

    return

def infer_model(net,
                preprocessor,
                postprocessor,
                example,
                class_names,
                center_limit_range=None,
                lidar_input=False,
                global_set=None):
    batch_image_shape = example['image_shape']
    batch_imgidx = example['image_idx']

    voxels = example["voxels"]
    num_points = example["num_points"]
    coors = example["coordinates"]
    # batch_anchors = example["anchors"]

    #########################
    #  MODEL LOAD 
    #########################
    delegate_options = {}
    delegate_options.update(required_options)
    delegate_options.update(optional_options)  

    delegate_options['artifacts_folder'] = delegate_options['artifacts_folder'] + '/pfe/' 
    pfe_session         = rt.InferenceSession("./pfe.onnx" ,providers=EP_list, provider_options=[delegate_options, {}]) #, sess_options=so)
    pfe_input_name      = pfe_session.get_inputs()[0].name
    pfe_output_name     = pfe_session.get_outputs()[0].name

    delegate_options['artifacts_folder'] = delegate_options['artifacts_folder'] + '../rpn_sub1/' 
    rpn_sub1_session        = rt.InferenceSession("./rpn_sub1.onnx", providers=EP_list, provider_options=[delegate_options, {}]) #, sess_options=so)
    rpn_sub1_input_name     = rpn_sub1_session.get_inputs()[0].name
    rpn_sub1_output_name    = rpn_sub1_session.get_outputs()[0].name

    delegate_options['artifacts_folder'] = delegate_options['artifacts_folder'] + '../rpn_sub2/' 
    rpn_sub2_session        = rt.InferenceSession("./rpn_sub2.onnx", providers=EP_list, provider_options=[delegate_options, {}]) #, sess_options=so)
    rpn_sub2_input_name     = rpn_sub2_session.get_inputs()[0].name
    rpn_sub2_output_name    = rpn_sub2_session.get_outputs()[0].name

    delegate_options['artifacts_folder'] = delegate_options['artifacts_folder'] + '../rpn_sub3/' 
    rpn_sub3_session        = rt.InferenceSession("./rpn_sub3.onnx", providers=EP_list, provider_options=[delegate_options, {}]) #, sess_options=so)
    rpn_sub3_input_name     = rpn_sub3_session.get_inputs()[0].name
    rpn_sub3_output_name    = rpn_sub3_session.get_outputs()[0].name

    delegate_options['artifacts_folder'] = delegate_options['artifacts_folder'] + '../rpn_tc1/' 
    rpn_tc1_session     = rt.InferenceSession("./rpn_tc1.onnx", providers=['CPUExecutionProvider']) #, sess_options=so)
    rpn_tc1_input_name  = rpn_tc1_session.get_inputs()[0].name
    rpn_tc1_output_name = rpn_tc1_session.get_outputs()[0].name

    delegate_options['artifacts_folder'] = delegate_options['artifacts_folder'] + '../rpn_tc2/' 
    rpn_tc2_session     = rt.InferenceSession("./rpn_tc2.onnx", providers=EP_list, provider_options=[delegate_options, {}]) #, sess_options=so)
    rpn_tc2_input_name  = rpn_tc2_session.get_inputs()[0].name
    rpn_tc2_output_name = rpn_tc2_session.get_outputs()[0].name
    
    delegate_options['artifacts_folder'] = delegate_options['artifacts_folder'] + '../rpn_tc3/' 
    rpn_tc3_session     = rt.InferenceSession("./rpn_tc3.onnx", providers=['CPUExecutionProvider']) #, sess_options=so)
    rpn_tc3_input_name  = rpn_tc3_session.get_inputs()[0].name
    rpn_tc3_output_name = rpn_tc3_session.get_outputs()[0].name
    
    delegate_options['artifacts_folder'] = delegate_options['artifacts_folder'] + '../rpn_box/' 
    rpn_box_session     = rt.InferenceSession("./rpn_box.onnx", providers=EP_list, provider_options=[delegate_options, {}]) #, sess_options=so)
    rpn_box_input_name  = rpn_box_session.get_inputs()[0].name
    rpn_box_output_name = rpn_box_session.get_outputs()[0].name
    
    delegate_options['artifacts_folder'] = delegate_options['artifacts_folder'] + '../rpn_cls/' 
    rpn_cls_session     = rt.InferenceSession("./rpn_cls.onnx", providers=EP_list, provider_options=[delegate_options, {}]) #, sess_options=so)
    rpn_cls_input_name  = rpn_cls_session.get_inputs()[0].name
    rpn_cls_output_name = rpn_cls_session.get_outputs()[0].name
    
    delegate_options['artifacts_folder'] = delegate_options['artifacts_folder'] + '../rpn_dir/' 
    rpn_dir_session     = rt.InferenceSession("./rpn_dir.onnx", providers=EP_list, provider_options=[delegate_options, {}]) #, sess_options=so)
    rpn_dir_input_name  = rpn_dir_session.get_inputs()[0].name
    rpn_dir_output_name = rpn_dir_session.get_outputs()[0].name
    
    delegate_options['artifacts_folder'] = delegate_options['artifacts_folder'] + '../rpn_cat/' 
    rpn_cat_session         = rt.InferenceSession("./rpn_cat.onnx", providers=EP_list, provider_options=[delegate_options, {}]) #, sess_options=so)
    rpn_cat_input_name      = rpn_cat_session.get_inputs()[0].name
    rpn_cat_input_name_1    = rpn_cat_session.get_inputs()[1].name
    rpn_cat_input_name_2    = rpn_cat_session.get_inputs()[2].name
    rpn_cat_output_name     = rpn_cat_session.get_outputs()[0].name
    
    rpn_trans_session       = rt.InferenceSession("./rpn_trans.onnx", providers=['CPUExecutionProvider']) #, sess_options=so)
    rpn_trans_input_name    = rpn_trans_session.get_inputs()[0].name
    rpn_trans_input_name_1  = rpn_trans_session.get_inputs()[1].name
    rpn_trans_input_name_2  = rpn_trans_session.get_inputs()[2].name
    rpn_trans_output_name   = rpn_trans_session.get_outputs()[0].name
    rpn_trans_output_name_1 = rpn_trans_session.get_outputs()[1].name
    rpn_trans_output_name_2 = rpn_trans_session.get_outputs()[2].name




    prev_t = time.time()
    pfe_inputs = preprocessor(voxels, num_points, coors)
    curr_t = time.time()
    t_PRE_PROCESS = (curr_t - prev_t)
    # print("PFE-PROESS: ", (curr_t - prev_t) * 1000, "[ms]")   

    pfe_inputs = pfe_inputs.tolist()

    prev_t = time.time()
    pfe_outs = pfe_session.run([pfe_output_name], {pfe_input_name:pfe_inputs})
    curr_t = time.time()
    t_PFE = (curr_t - prev_t)
    # print("PFE: ", (curr_t - prev_t) * 1000, "[ms]")

    pfe_outs = torch.tensor(pfe_outs).squeeze()
    prev_t = time.time()
    pseudo_image = net.forward_SCATTER(pfe_outs, coors)
    curr_t = time.time()
    t_SCATTER = (curr_t - prev_t)
    # print("SCATTER: ", (curr_t - prev_t) * 1000, "[ms]")


    ##########################
    #  RPN 
    ##########################
    pseudo_image = pseudo_image.tolist()
    prev_t = time.time()

    rpn_sub1_outs = rpn_sub1_session.run([rpn_sub1_output_name], {rpn_sub1_input_name:pseudo_image})
    rpn_sub2_outs = rpn_sub2_session.run([rpn_sub2_output_name], {rpn_sub2_input_name:rpn_sub1_outs[0]})
    rpn_sub3_outs = rpn_sub3_session.run([rpn_sub3_output_name], {rpn_sub3_input_name:rpn_sub2_outs[0]})
    rpn_tc3_outs = rpn_tc3_session.run([rpn_tc3_output_name], {rpn_tc3_input_name:rpn_sub3_outs[0]})
    rpn_tc1_outs = rpn_tc1_session.run([rpn_tc1_output_name], {rpn_tc1_input_name:rpn_sub1_outs[0]})
    rpn_tc2_outs = rpn_tc2_session.run([rpn_tc2_output_name], {rpn_tc2_input_name:rpn_sub2_outs[0]})
    
    rpn_cat_outs = rpn_cat_session.run([rpn_cat_output_name], {
                                        rpn_cat_input_name:rpn_tc1_outs[0],
                                        rpn_cat_input_name_1:rpn_tc2_outs[0],
                                        rpn_cat_input_name_2:rpn_tc3_outs[0]
                                        })

    rpn_box_outs = rpn_box_session.run([rpn_box_output_name], {rpn_box_input_name:rpn_cat_outs[0]})
    rpn_cls_outs = rpn_cls_session.run([rpn_cls_output_name], {rpn_cls_input_name:rpn_cat_outs[0]})
    rpn_dir_outs = rpn_dir_session.run([rpn_dir_output_name], {rpn_dir_input_name:rpn_cat_outs[0]})

    rpn_outs = rpn_trans_session.run([rpn_trans_output_name, rpn_trans_output_name_1, rpn_trans_output_name_2], {
                                        rpn_trans_input_name:rpn_box_outs[0],
                                        rpn_trans_input_name_1:rpn_cls_outs[0],
                                        rpn_trans_input_name_2:rpn_dir_outs[0]
                                        })

    
 
    curr_t = time.time()
    t_RPN = (curr_t - prev_t)
    # print("RPN: ", (curr_t - prev_t) * 1000, "[ms]")


    
    preds_dict = {}
    if len(rpn_outs) == 3:
        # box_preds, cls_preds, dir_cls_preds = rpn_outs
        preds_dict['box_preds'] = torch.tensor(rpn_outs[0])
        preds_dict['cls_preds'] = torch.tensor(rpn_outs[1])
        preds_dict['dir_cls_preds'] = torch.tensor(rpn_outs[2])
    else:
        # box_preds, cls_preds = rpn_outs
        preds_dict['box_preds'] = torch.tensor(rpn_outs[0])
        preds_dict['cls_preds'] = torch.tensor(rpn_outs[1])

    prev_t = time.time()
    predictions_dicts = postprocessor(example, preds_dict)
    curr_t = time.time()
    t_POST_PROCESS = (curr_t - prev_t)
    # print("post-process: ", (curr_t - prev_t) * 1000, "[ms]")



    return [t_PRE_PROCESS, t_PFE, 
        t_SCATTER, t_RPN, t_POST_PROCESS]



def onnx_inference(net,
                preprocessor,
                postprocessor,
                example,
                class_names,
                center_limit_range=None,
                lidar_input=False,
                global_set=None):
    batch_image_shape = example['image_shape']
    batch_imgidx = example['image_idx']

    voxels = example["voxels"]
    num_points = example["num_points"]
    coors = example["coordinates"]
    # batch_anchors = example["anchors"]


    # pfe_session = rt.InferenceSession(
    #             "./pfe.onnx", 
    #             providers=EP_list, 
    #             sess_options=so)

    # pfe_input_name  = pfe_session.get_inputs()[0].name
    # pfe_output_name = pfe_session.get_outputs()[0].name

    # rpn_sub1_session = rt.InferenceSession(
    #             "./rpn_sub1.onnx", 
    #             providers=EP_list, 
    #             sess_options=so)

    # rpn_sub1_input_name  = rpn_sub1_session.get_inputs()[0].name
    # rpn_cpu1 = rpn_sub1_session.get_outputs()[0].name
    # rpn_up2  = rpn_sub1_session.get_outputs()[1].name
    # rpn_cpu2 = rpn_sub1_session.get_outputs()[2].name


    # rpn_tc1_session = rt.InferenceSession(
    #             "./rpn_tc1.onnx", 
    #             providers=EP_list, 
    #             sess_options=so)

    # rpn_tc1_input_name  = rpn_tc1_session.get_inputs()[0].name
    # rpn_up1  = rpn_tc1_session.get_outputs()[0].name


    # rpn_tc2_session = rt.InferenceSession(
    #             "./rpn_tc2.onnx", 
    #             providers=EP_list, 
    #             sess_options=so)

    # rpn_tc2_input_name  = rpn_tc2_session.get_inputs()[0].name
    # rpn_up3  = rpn_tc2_session.get_outputs()[0].name


    # rpn_sub2_session = rt.InferenceSession(
    #             "./rpn_sub2.onnx", 
    #             providers=EP_list, 
    #             sess_options=so)

    # rpn_tc1  = rpn_sub2_session.get_inputs()[0].name
    # rpn_tc2  = rpn_sub2_session.get_inputs()[1].name
    # rpn_tc3  = rpn_sub2_session.get_inputs()[2].name
    # rpn_box_preds = rpn_sub2_session.get_outputs()[0].name
    # rpn_cls_preds = rpn_sub2_session.get_outputs()[1].name
    # rpn_dir_preds = rpn_sub2_session.get_outputs()[2].name

    """ rpn_orig.onnx test """
    # rpn_session = rt.InferenceSession(
    #             "./rpn.onnx", 
    #             providers=EP_list, 
    #             sess_options=so)

    # rpn_input_name  = rpn_session.get_inputs()[0].name
    # rpn_box_preds = rpn_session.get_outputs()[0].name
    # rpn_cls_preds = rpn_session.get_outputs()[1].name
    # rpn_dir_preds = rpn_session.get_outputs()[2].name


    pfe_session         = rt.InferenceSession("./pfe.onnx" ,providers=EP_list, sess_options=so)
    pfe_input_name      = pfe_session.get_inputs()[0].name
    pfe_output_name     = pfe_session.get_outputs()[0].name

    rpn_sub1_session        = rt.InferenceSession("./rpn_sub1.onnx", providers=EP_list, sess_options=so)
    rpn_sub1_input_name     = rpn_sub1_session.get_inputs()[0].name
    rpn_sub1_output_name    = rpn_sub1_session.get_outputs()[0].name

    rpn_sub2_session        = rt.InferenceSession("./rpn_sub2.onnx", providers=EP_list, sess_options=so)
    rpn_sub2_input_name     = rpn_sub2_session.get_inputs()[0].name
    rpn_sub2_output_name    = rpn_sub2_session.get_outputs()[0].name

    rpn_sub3_session        = rt.InferenceSession("./rpn_sub3.onnx", providers=EP_list) #, sess_options=so)
    rpn_sub3_input_name     = rpn_sub3_session.get_inputs()[0].name
    rpn_sub3_output_name    = rpn_sub3_session.get_outputs()[0].name

    rpn_tc1_session     = rt.InferenceSession("./rpn_tc1.onnx", providers=EP_list) #, sess_options=so)
    rpn_tc1_input_name  = rpn_tc1_session.get_inputs()[0].name
    rpn_tc1_output_name = rpn_tc1_session.get_outputs()[0].name

    rpn_tc2_session     = rt.InferenceSession("./rpn_tc2.onnx", providers=EP_list) #, sess_options=so)
    rpn_tc2_input_name  = rpn_tc2_session.get_inputs()[0].name
    rpn_tc2_output_name = rpn_tc2_session.get_outputs()[0].name
    
    rpn_tc3_session     = rt.InferenceSession("./rpn_tc3.onnx", providers=EP_list) #, sess_options=so)
    rpn_tc3_input_name  = rpn_tc3_session.get_inputs()[0].name
    rpn_tc3_output_name = rpn_tc3_session.get_outputs()[0].name
    
    rpn_box_session     = rt.InferenceSession("./rpn_box.onnx", providers=EP_list) #, sess_options=so)
    rpn_box_input_name  = rpn_box_session.get_inputs()[0].name
    rpn_box_output_name = rpn_box_session.get_outputs()[0].name
    
    rpn_cls_session     = rt.InferenceSession("./rpn_cls.onnx", providers=EP_list) #, sess_options=so)
    rpn_cls_input_name  = rpn_cls_session.get_inputs()[0].name
    rpn_cls_output_name = rpn_cls_session.get_outputs()[0].name
    
    rpn_dir_session     = rt.InferenceSession("./rpn_dir.onnx", providers=EP_list) #, sess_options=so)
    rpn_dir_input_name  = rpn_dir_session.get_inputs()[0].name
    rpn_dir_output_name = rpn_dir_session.get_outputs()[0].name
    
    rpn_cat_session         = rt.InferenceSession("./rpn_cat.onnx", providers=EP_list) #, sess_options=so)
    rpn_cat_input_name      = rpn_cat_session.get_inputs()[0].name
    rpn_cat_input_name_1    = rpn_cat_session.get_inputs()[1].name
    rpn_cat_input_name_2    = rpn_cat_session.get_inputs()[2].name
    rpn_cat_output_name     = rpn_cat_session.get_outputs()[0].name
    
    rpn_trans_session       = rt.InferenceSession("./rpn_trans.onnx", providers=['CPUExecutionProvider']) #, sess_options=so)
    rpn_trans_input_name    = rpn_trans_session.get_inputs()[0].name
    rpn_trans_input_name_1  = rpn_trans_session.get_inputs()[1].name
    rpn_trans_input_name_2  = rpn_trans_session.get_inputs()[2].name
    rpn_trans_output_name   = rpn_trans_session.get_outputs()[0].name
    rpn_trans_output_name_1 = rpn_trans_session.get_outputs()[1].name
    rpn_trans_output_name_2 = rpn_trans_session.get_outputs()[2].name



    prev_t = time.time()
    pfe_inputs = preprocessor(voxels, num_points, coors)
    curr_t = time.time()
    t_PRE_PROCESS = (curr_t - prev_t)
    # print("PFE-PROESS: ", (curr_t - prev_t) * 1000, "[ms]")   

    pfe_inputs = pfe_inputs.tolist()

    prev_t = time.time()
    pfe_outs = pfe_session.run([pfe_output_name], {pfe_input_name:pfe_inputs})
    curr_t = time.time()
    pfe_session.end_profiling()
    t_PFE = (curr_t - prev_t)
    # print("PFE: ", (curr_t - prev_t) * 1000, "[ms]")

    pfe_outs = torch.tensor(pfe_outs).squeeze()
    prev_t = time.time()
    pseudo_image = net.forward_SCATTER(pfe_outs, coors)
    curr_t = time.time()
    t_SCATTER = (curr_t - prev_t)
    # print("SCATTER: ", (curr_t - prev_t) * 1000, "[ms]")

    # pseudo_image = pseudo_image.tolist()
    # prev_t = time.time()

    # """ rpn_sub_1.onnx """
    # rpn_sub1_outs = rpn_sub1_session.run(
    #     [rpn_cpu1, rpn_up2, rpn_cpu2]
    #     ,{rpn_sub1_input_name:pseudo_image}
    # )

    # """ rpn_tc1.onnx """
    # rpn_up1 = rpn_tc1_session.run(
    #     [rpn_up1]
    #     ,{rpn_tc1_input_name:rpn_sub1_outs[0]}
    # )

    # """ rpn_tc2.onnx """
    # rpn_up3 = rpn_tc2_session.run(
    #     [rpn_up3]
    #     ,{rpn_tc1_input_name:rpn_sub1_outs[2]}
    # )

    # # pdb.set_trace()
    # """ rpn_sub_2.onnx """
    # rpn_outs = rpn_sub2_session.run(
    #     [rpn_box_preds, rpn_cls_preds, rpn_dir_preds]
    #     ,{  rpn_tc1: rpn_up1[0],
    #         rpn_tc2: rpn_sub1_outs[1],
    #         rpn_tc3: rpn_up3[0]}
    # )


    # curr_t = time.time()
    # print("RPN: ", (curr_t - prev_t) * 1000, "[ms]")

    """ RPN ORIG TEST """
    # prev_t = time.time()
    # rpn_outs = rpn_session.run(
    #     [rpn_box_preds, rpn_cls_preds, rpn_dir_preds]
    #     ,{rpn_input_name:pseudo_image}
    # )
    # curr_t = time.time()
    # print("RPN: ", (curr_t - prev_t) * 1000, "[ms]")


    # pdb.set_trace()

    """ profiling test  """

    
    
    pseudo_image = pseudo_image.tolist()
    prev_t = time.time()

    rpn_sub1_outs = rpn_sub1_session.run([rpn_sub1_output_name], {rpn_sub1_input_name:pseudo_image})
    rpn_sub2_outs = rpn_sub2_session.run([rpn_sub2_output_name], {rpn_sub2_input_name:rpn_sub1_outs[0]})
    rpn_sub3_outs = rpn_sub3_session.run([rpn_sub3_output_name], {rpn_sub3_input_name:rpn_sub2_outs[0]})
    rpn_tc3_outs = rpn_tc3_session.run([rpn_tc3_output_name], {rpn_tc3_input_name:rpn_sub3_outs[0]})
    rpn_tc1_outs = rpn_tc1_session.run([rpn_tc1_output_name], {rpn_tc1_input_name:rpn_sub1_outs[0]})
    rpn_tc2_outs = rpn_tc2_session.run([rpn_tc2_output_name], {rpn_tc2_input_name:rpn_sub2_outs[0]})
    
    rpn_cat_outs = rpn_cat_session.run([rpn_cat_output_name], {
                                        rpn_cat_input_name:rpn_tc1_outs[0],
                                        rpn_cat_input_name_1:rpn_tc2_outs[0],
                                        rpn_cat_input_name_2:rpn_tc3_outs[0]
                                        })

    rpn_box_outs = rpn_box_session.run([rpn_box_output_name], {rpn_box_input_name:rpn_cat_outs[0]})
    rpn_cls_outs = rpn_cls_session.run([rpn_cls_output_name], {rpn_cls_input_name:rpn_cat_outs[0]})
    rpn_dir_outs = rpn_dir_session.run([rpn_dir_output_name], {rpn_dir_input_name:rpn_cat_outs[0]})

    rpn_outs = rpn_trans_session.run([rpn_trans_output_name, rpn_trans_output_name_1, rpn_trans_output_name_2], {
                                        rpn_trans_input_name:rpn_box_outs[0],
                                        rpn_trans_input_name_1:rpn_cls_outs[0],
                                        rpn_trans_input_name_2:rpn_dir_outs[0]
                                        })

    curr_t = time.time()
    t_RPN = (curr_t - prev_t)
    # print("RPN: ", (curr_t - prev_t) * 1000, "[ms]")

    
    preds_dict = {}
    if len(rpn_outs) == 3:
        # box_preds, cls_preds, dir_cls_preds = rpn_outs
        preds_dict['box_preds'] = torch.tensor(rpn_outs[0])
        preds_dict['cls_preds'] = torch.tensor(rpn_outs[1])
        preds_dict['dir_cls_preds'] = torch.tensor(rpn_outs[2])
    else:
        # box_preds, cls_preds = rpn_outs
        preds_dict['box_preds'] = torch.tensor(rpn_outs[0])
        preds_dict['cls_preds'] = torch.tensor(rpn_outs[1])

    prev_t = time.time()
    predictions_dicts = postprocessor(example, preds_dict)
    curr_t = time.time()
    t_POST_PROCESS = (curr_t - prev_t)
    # print("post-process: ", (curr_t - prev_t) * 1000, "[ms]")

    return [t_PRE_PROCESS, t_PFE, 
        t_SCATTER, t_RPN, t_POST_PROCESS]



def predict_kitti_to_anno(net,
                          preprocessor,
                          postprocessor,
                          example,
                          class_names,
                          center_limit_range=None,
                          lidar_input=False,
                          global_set=None):
    batch_image_shape = example['image_shape']
    batch_imgidx = example['image_idx']

    voxels = example["voxels"]
    num_points = example["num_points"]
    coors = example["coordinates"]
    # batch_anchors = example["anchors"]

    prev_t = time.time()
    aug_voxels = preprocessor(voxels, num_points, coors)
    curr_t = time.time()
    print("pre-process: ", (curr_t - prev_t) * 1000, "[ms]")    

    pred_tuples = net.forward(aug_voxels, coors)

    preds_dict = {}
    if len(pred_tuples) == 3:
        box_preds, cls_preds, dir_cls_preds = pred_tuples
        preds_dict['box_preds'] = box_preds
        preds_dict['cls_preds'] = cls_preds
        preds_dict['dir_cls_preds'] = dir_cls_preds
    else:
        box_preds, cls_preds = pred_tuples
        preds_dict['box_preds'] = box_preds
        preds_dict['cls_preds'] = cls_preds

    prev_t = time.time()
    predictions_dicts = postprocessor(example, preds_dict)
    curr_t = time.time()
    print("post-process: ", (curr_t - prev_t) * 1000, "[ms]")

    annos = []
    for i, preds_dict in enumerate(predictions_dicts):
        image_shape = batch_image_shape[i]
        img_idx = preds_dict["image_idx"]
        if preds_dict["bbox"] is not None:
            box_2d_preds = preds_dict["bbox"].detach().cpu().numpy()
            box_preds = preds_dict["box3d_camera"].detach().cpu().numpy()
            scores = preds_dict["scores"].detach().cpu().numpy()
            box_preds_lidar = preds_dict["box3d_lidar"].detach().cpu().numpy()
            # write pred to file
            label_preds = preds_dict["label_preds"].detach().cpu().numpy()
            # label_preds = np.zeros([box_2d_preds.shape[0]], dtype=np.int32)
            anno = kitti.get_start_result_anno()
            num_example = 0
            for box, box_lidar, bbox, score, label in zip(
                    box_preds, box_preds_lidar, box_2d_preds, scores,
                    label_preds):
                if not lidar_input:
                    if bbox[0] > image_shape[1] or bbox[1] > image_shape[0]:
                        continue
                    if bbox[2] < 0 or bbox[3] < 0:
                        continue
                if center_limit_range is not None:
                    limit_range = np.array(center_limit_range)
                    if (np.any(box_lidar[:3] < limit_range[:3])
                            or np.any(box_lidar[:3] > limit_range[3:])):
                        continue
                bbox[2:] = np.minimum(bbox[2:], image_shape[::-1])
                bbox[:2] = np.maximum(bbox[:2], [0, 0])
                anno["name"].append(class_names[int(label)])
                anno["truncated"].append(0.0)
                anno["occluded"].append(0)
                anno["alpha"].append(-np.arctan2(-box_lidar[1], box_lidar[0]) +
                                     box[6])
                anno["bbox"].append(bbox)
                anno["dimensions"].append(box[3:6])
                anno["location"].append(box[:3])
                anno["rotation_y"].append(box[6])
                if global_set is not None:
                    for i in range(100000):
                        if score in global_set:
                            score -= 1 / 100000
                        else:
                            global_set.add(score)
                            break
                anno["score"].append(score)

                num_example += 1
            if num_example != 0:
                anno = {n: np.stack(v) for n, v in anno.items()}
                annos.append(anno)
            else:
                annos.append(kitti.empty_result_anno())
        else:
            annos.append(kitti.empty_result_anno())
        num_example = annos[-1]["name"].shape[0]
        annos[-1]["image_idx"] = np.array(
            [img_idx] * num_example, dtype=np.int64)
    return annos


def predict_kitti_to_file(net,
                          preprocessor,
                          postprocessor,
                          example,
                          result_dir,
                          class_names,
                          center_limit_range=None,
                          lidar_input=False):
    batch_image_shape = example['image_shape']
    batch_imgidx = example['image_idx']
    voxels = example["voxels"]
    num_points = example["num_points"]
    coors = example["coordinates"]

    prev_t = time.time()
    aug_voxels = preprocessor(voxels, num_points, coors)
    curr_t = time.time()
    print("preprocessor: ", curr_t - prev_t)

    prev_t = time.time()
    pred_tuples = net.forward(aug_voxels, coors)
    curr_t = time.time()
    print("PFE + Scatter + RPN: ", curr_t - prev_t)

    preds_dict = {}
    if len(pred_tuples) == 3:
        box_preds, cls_preds, dir_cls_preds = pred_tuples
        preds_dict['box_preds'] = box_preds
        preds_dict['cls_preds'] = cls_preds
        preds_dict['dir_cls_preds'] = dir_cls_preds
    else:
        box_preds, cls_preds = pred_tuples
        preds_dict['box_preds'] = box_preds
        preds_dict['cls_preds'] = cls_preds

    prev_t = time.time()
    predictions_dicts = postprocessor(example, preds_dict)
    curr_t = time.time()
    print("postprocessor: ", curr_t - prev_t)

    for i, preds_dict in enumerate(predictions_dicts):
        image_shape = batch_image_shape[i]
        img_idx = preds_dict["image_idx"]
        if preds_dict["bbox"] is not None:
            box_2d_preds = preds_dict["bbox"].data.cpu().numpy()
            box_preds = preds_dict["box3d_camera"].data.cpu().numpy()
            scores = preds_dict["scores"].data.cpu().numpy()
            box_preds_lidar = preds_dict["box3d_lidar"].data.cpu().numpy()
            # write pred to file
            box_preds = box_preds[:, [0, 1, 2, 4, 5, 3,
                                      6]]  # lhw->hwl(label file format)
            label_preds = preds_dict["label_preds"].data.cpu().numpy()
            # label_preds = np.zeros([box_2d_preds.shape[0]], dtype=np.int32)
            result_lines = []
            for box, box_lidar, bbox, score, label in zip(
                    box_preds, box_preds_lidar, box_2d_preds, scores,
                    label_preds):
                if not lidar_input:
                    if bbox[0] > image_shape[1] or bbox[1] > image_shape[0]:
                        continue
                    if bbox[2] < 0 or bbox[3] < 0:
                        continue
                # print(img_shape)
                if center_limit_range is not None:
                    limit_range = np.array(center_limit_range)
                    if (np.any(box_lidar[:3] < limit_range[:3])
                            or np.any(box_lidar[:3] > limit_range[3:])):
                        continue
                bbox[2:] = np.minimum(bbox[2:], image_shape[::-1])
                bbox[:2] = np.maximum(bbox[:2], [0, 0])
                result_dict = {
                    'name': class_names[int(label)],
                    'alpha': -np.arctan2(-box_lidar[1], box_lidar[0]) + box[6],
                    'bbox': bbox,
                    'location': box[:3],
                    'dimensions': box[3:6],
                    'rotation_y': box[6],
                    'score': score,
                }
                result_line = kitti.kitti_result_line(result_dict)
                result_lines.append(result_line)
        else:
            result_lines = []
        result_file = f"{result_dir}/{kitti.get_image_index_str(img_idx)}.txt"
        result_str = '\n'.join(result_lines)
        with open(result_file, 'w') as f:
            f.write(result_str)


def restore(ckpt_path, model, device):
    if not Path(ckpt_path).is_file():
        raise ValueError("checkpoint {} not exist.".format(ckpt_path))
    model.to(device)
    model_dict = model.state_dict()
    ckpt_dict = torch.load(ckpt_path, map_location=device)
    for k in model_dict.keys():
        model_dict[k] = ckpt_dict[k]
    model.load_state_dict(model_dict)
    print("Restoring parameters from {}".format(ckpt_path))


def main(args):

    t_PRE_PROCESS = 0
    t_PFE = 0
    t_SCATTER = 0
    t_RPN = 0
    t_POST_PROCESS = 0

    t_rec = [t_PRE_PROCESS, t_PFE,
            t_SCATTER, t_RPN, t_POST_PROCESS]

    SAMPLE_NUM = 1

    quant_mode = args.quant_mode
    pred_dir = Path(args.pred_dir)
    output_dir = args.quant_dir
    batch_size = args.batch_size
    
    if args.dump_xmodel:
        args.device='cpu'
        batch_size = 1

    if args.device=='cpu':
        device = torch.device("cpu")
    else:
        device = torch.device("cuda")


    config = pipeline_pb2.TrainEvalPipelineConfig()
    with open(args.config, "r") as f:
        proto_str = f.read()
        text_format.Merge(proto_str, config)

    input_cfg = config.eval_input_reader
    model_cfg = config.model.second
    train_cfg = config.train_config
    class_names = list(input_cfg.class_names)
    center_limit_range = model_cfg.post_center_limit_range

    voxel_generator = voxel_builder.build(model_cfg.voxel_generator)
    bv_range = voxel_generator.point_cloud_range[[0, 1, 3, 4]] 
    box_coder = box_coder_builder.build(model_cfg.box_coder)
    target_assigner_cfg = model_cfg.target_assigner
    target_assigner = target_assigner_builder.build(target_assigner_cfg, bv_range, box_coder)
    
    net, preprocessor, postprocessor = second_builder.build(model_cfg, voxel_generator, target_assigner)

    if train_cfg.enable_mixed_precision:
        net.half()
        net.metrics_to_float()
        net.convert_norm_to_float(net)
    
    # pdb.set_trace()

    restore(args.weights, net, device)
    
    eval_dataset = input_reader_builder.build(
        input_cfg,
        model_cfg,
        training=False,
        voxel_generator=voxel_generator,
        target_assigner=target_assigner)
    eval_dataloader = torch.utils.data.DataLoader(
        eval_dataset,
        batch_size=batch_size,
        shuffle=False,
        num_workers=input_cfg.num_workers,
        pin_memory=False,
        collate_fn=merge_second_batch)
    
    if train_cfg.enable_mixed_precision:
        float_dtype = torch.float16
    else:
        float_dtype = torch.float32
    
    if quant_mode != 'float':
        max_voxel_num = config.eval_input_reader.max_number_of_voxels
        max_point_num_per_voxel = model_cfg.voxel_generator.max_number_of_points_per_voxel
        aug_voxels = torch.randn((1, 4, max_voxel_num, max_point_num_per_voxel)).to(device)
        coors = torch.randn((max_voxel_num, 4)).to(device)
        quantizer = torch_quantizer(quant_mode=quant_mode,
                                    module=net, 
                                    input_args=(aug_voxels, coors), 
                                    output_dir=output_dir,
                                    device=device,
                                    )
        net = quantizer.quant_model

    net.eval()

    pred_dir.mkdir(parents=True, exist_ok=True)
    dt_annos = []
    global_set = None
    print("Generate output labels...")
    bar = ProgressBar()
    bar.start(len(eval_dataset) // batch_size + 1)

    # pdb.set_trace()

    sample = 0
    for example in iter(eval_dataloader):
        example = example_convert_to_torch(example, float_dtype, device)

        #######################
        # ONNX EXPORT 
        #######################
        if EP==ONNX_EXPORT:
            onnx_export(net, preprocessor, postprocessor, example, class_names, center_limit_range,
                        model_cfg.lidar_input, global_set)
        
        #######################
        # ONNX INFERENCE
        #######################
        elif EP==CPU:
            ret_t = onnx_inference(net, preprocessor, postprocessor, example, class_names, center_limit_range,
                    model_cfg.lidar_input, global_set)


        #######################
        # TIDL COMPILE
        #######################
        elif EP==COMPILE:
            for mIdx, model in enumerate(models):
                compile_model(model, mIdx)


        elif EP==OFFLOAD:

            ret_t = infer_model(net, preprocessor, postprocessor, example, class_names, center_limit_range,
                    model_cfg.lidar_input, global_set)


        #########################
        # PYTORCH INFERENCE 
        #########################
        # example = example_convert_to_torch(example, float_dtype, device)
        # if args.pickle_result:
        #     dt_annos += predict_kitti_to_anno(
        #         net, preprocessor, postprocessor, example, class_names, center_limit_range,
        #         model_cfg.lidar_input, global_set)
        # else:
        #     predict_kitti_to_file(net, preprocessor, postprocessor, example, pred_dir,
        #         class_names, center_limit_range, model_cfg.lidar_input)
        # bar.print_bar()

        t_PRE_PROCESS  += ret_t[0]
        t_PFE          += ret_t[1]
        t_SCATTER      += ret_t[2]
        t_RPN          += ret_t[3]
        t_POST_PROCESS += ret_t[4]
        
        




        if quant_mode == 'test' and args.dump_xmodel:
            dump_xmodel(output_dir=output_dir, deploy_check=True)
            sys.exit()

        """ BREAK LOOP FOR THE LIMITED SAMPELS """
        sample += 1
        if sample == SAMPLE_NUM:
            break
    

    print("PRE_PROESS   : ",  t_PRE_PROCESS  * 1000 / SAMPLE_NUM, "[ms]")   
    print("PFE          : ",  t_PFE          * 1000 / SAMPLE_NUM, "[ms]")   
    print("SCATTER      : ",  t_SCATTER      * 1000 / SAMPLE_NUM, "[ms]")   
    print("RPN          : ",  t_RPN          * 1000 / SAMPLE_NUM, "[ms]")   
    print("POST_PROESS  : ",  t_POST_PROCESS * 1000 / SAMPLE_NUM, "[ms]")   

    gt_annos = [info["annos"] for info in eval_dataset.dataset.kitti_infos]
    if not args.pickle_result:
        dt_annos = kitti.get_label_annos(pred_dir)

    result = get_official_eval_result(gt_annos, dt_annos, class_names)
    print(result)
    result = get_coco_eval_result(gt_annos, dt_annos, class_names)
    print(result)
    with open(pred_dir / "result.pkl", 'wb') as f:
        pickle.dump(dt_annos, f)

    if quant_mode == 'calib':
        quantizer.export_quant_config()
   

if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Script for testing accuracy of PointPillars on KITTI dataset.')
    parser.add_argument('-config', default='float/config.proto')
    parser.add_argument('-weights', default='float/float.tckpt')
    parser.add_argument('-pred_dir', default='data/result')
    parser.add_argument('-quant_dir', default='q_results')
    parser.add_argument('-quant_mode', default='calib', choices=['float', 'calib', 'test'], help='quantization mode. float: no quantization, evaluate float model, calib: quantize, test: evaluate quantized model')
    parser.add_argument('-dump_xmodel', dest='dump_xmodel', action='store_true', help='dump xmodel after test')
    parser.add_argument('-device', default='cpu', choices=['gpu, cpu'], help='assign runtime device')
    parser.add_argument('-pickle_result', default=True, type=bool)
    parser.add_argument('-batch_size',default=1, type=int)
    args = parser.parse_args()

    main(args)
