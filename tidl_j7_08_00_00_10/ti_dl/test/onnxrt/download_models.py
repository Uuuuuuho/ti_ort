import os
import requests
import onnx

models_base_path = '../testvecs/models/public/onnx/'

models = {
    #'mobilenetv2-7.onnx': {'url': 'https://github.com/vinitra-zz/models/raw/7301ce1e16891ed5f75dd15a6a53a643001288f0/vision/classification/mobilenet/model/mobilenetv2-7.onnx', 'dir': '../testvecs/models/public/onnx/'},
    'resnet18-v1-7': {'model_url': 'https://github.com/onnx/models/blob/master/vision/classification/resnet/model/resnet18-v1-7.onnx?raw=true', 'dir': '../testvecs/models/public/onnx/'},
    'deeplabv3lite_mobilenetv2': {'model_url': 'https://git.ti.com/cgit/jacinto-ai/jacinto-ai-modelzoo/plain/models/vision/segmentation/ade20k32/jai-pytorch/deeplabv3lite_mobilenetv2_512x512_ade20k32_20210308.onnx', 'dir': '../testvecs/models/public/onnx/'},
    'ssd-lite_mobilenetv2_fpn': {'model_url': 'https://git.ti.com/cgit/jacinto-ai/jacinto-ai-modelzoo/plain/models/vision/detection/coco/edgeai-mmdet/ssd-lite_mobilenetv2_fpn_512x512_20201110_model.onnx', 'dir': '../testvecs/models/public/onnx/', 
                                      'model_prototxt' : 'https://git.ti.com/cgit/jacinto-ai/jacinto-ai-modelzoo/plain/models/vision/detection/coco/edgeai-mmdet/ssd-lite_mobilenetv2_fpn_512x512_20201110_model.prototxt'},
}

def download(model_name, suffix, type):
    model_file_name = model_name + suffix
    model_path = models_base_path + model_file_name
    if(not os.path.isfile(model_path)):
        if(type in models[model_name].keys()):
            print("Downloading  ", model_file_name)
            url = models[model_name][type]
            r = requests.get(url, allow_redirects=True)
            open(model_path, 'wb').write(r.content)

for model_name in models:
    download(model_name, '.onnx', 'model_url')
    download(model_name, '.prototxt', 'model_prototxt')

#run shape inference
for model_name in models:
    model_file_name = model_name + '.onnx'
    model_path = models_base_path + model_file_name
    print("Running shape inference for ", model_file_name)
    onnx.shape_inference.infer_shapes_path(model_path, model_path)