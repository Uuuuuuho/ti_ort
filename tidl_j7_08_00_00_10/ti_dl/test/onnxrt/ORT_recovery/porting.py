#####################
#  PYTORCH 
#####################

import os
import pathlib
import pickle
import shutil
import time
from functools import partial
import sys
sys.path.append('../')
from pathlib import Path
import fire
import numpy as np
import torch
from google.protobuf import text_format
# from tensorboardX import SummaryWriter


from second.pytorch.builder import (box_coder_builder, input_reader_builder,
                                    lr_scheduler_builder, optimizer_builder,
                                    second_builder)

from second.protos import pipeline_pb2
from second.builder import target_assigner_builder, voxel_builder
from second.data.preprocess import merge_second_batch


import pdb
#############################
#  ORT IMPORT 
#############################
import onnxruntime as rt
from models import *
import argparse
import multiprocessing
import platform
parser = argparse.ArgumentParser()
parser.add_argument('-c','--compile', action='store_true', help='Run in Model compilation mode')
parser.add_argument('-d','--disable_offload', action='store_true',  help='Disable offload to TIDL')
args = parser.parse_args()
os.environ["TIDL_RT_PERFSTATS"] = "1"

# models = ['pfe', 'rpn']
models = ['rpn']

log = f'\nRunning {len(models)} Models - {models}\n'
print(log)



so = rt.SessionOptions()
so.enable_profiling = False          # <-Profile function enabled

print("Available execution providers : ", rt.get_available_providers())


####################################
## input imaged configured here!!!
####################################

calib_images = ['../testvecs/input/airshow.jpg',
                '../testvecs/input/ADE_val_00001801.jpg']
class_test_images = ['../testvecs/input/airshow.jpg']
od_test_images    = ['../testvecs/input/ADE_val_00001801.jpg']
seg_test_images   = ['../testvecs/input/ADE_val_00001801.jpg']
sem = multiprocessing.Semaphore(0)
if platform.machine() == 'aarch64':
    ncpus = 1
else:
    ncpus = os.cpu_count()
#ncpus = 1
idx = 0
nthreads = 0
run_count = 0

def get_benchmark_output(interpreter):
    benchmark_dict = interpreter.get_TI_benchmark_data()
    proc_time = copy_time = 0
    cp_in_time = cp_out_time = 0
    subgraphIds = []
    for stat in benchmark_dict.keys():
        if 'proc_start' in stat:
            value = stat.split("ts:subgraph_")
            value = value[1].split("_proc_start")
            subgraphIds.append(value[0])
    for i in range(len(subgraphIds)):
        proc_time += benchmark_dict['ts:subgraph_'+str(subgraphIds[i])+'_proc_end'] - benchmark_dict['ts:subgraph_'+str(subgraphIds[i])+'_proc_start']
        cp_in_time += benchmark_dict['ts:subgraph_'+str(subgraphIds[i])+'_copy_in_end'] - benchmark_dict['ts:subgraph_'+str(subgraphIds[i])+'_copy_in_start']
        cp_out_time += benchmark_dict['ts:subgraph_'+str(subgraphIds[i])+'_copy_out_end'] - benchmark_dict['ts:subgraph_'+str(subgraphIds[i])+'_copy_out_start']
        copy_time += cp_in_time + cp_out_time
    copy_time = copy_time if len(subgraphIds) == 1 else 0
    totaltime = benchmark_dict['ts:run_end'] -  benchmark_dict['ts:run_start']
    return copy_time, proc_time, totaltime


def infer_image(sess, image_file, config):
  input_details = sess.get_inputs()
  input_name = input_details[0].name
  floating_model = (input_details[0].type == 'tensor(float)')
  height = input_details[0].shape[2]
  width  = input_details[0].shape[3]
  img    = Image.open(image_file).convert('RGB').resize((width, height))
  #img    = Image.open(image_file).convert('RGB').resize((416,416))
  input_data = np.expand_dims(img, axis=0)
  input_data = np.transpose(input_data, (0, 3, 1, 2))

  if floating_model:
    # input_data = np.int8(input_data)
    input_data = np.float32(input_data)
    for mean, scale, ch in zip(config['mean'], config['std'], range(input_data.shape[1])):
        input_data[:,ch,:,:] = ((input_data[:,ch,:,:]- mean) * scale)
  
  start_time = time.time()
  output = list(sess.run(None, {input_name: input_data}))
#   output = list(sess.run(None, {input_name: input_data, input_details[1].name: np.array([[416,416]], dtype = np.float32)}))
#   output = list(sess.run(None, {input_name: input_data, input_details[1].name: np.array(dtype = np.float32)}))

  stop_time = time.time()
  infer_time = stop_time - start_time

  copy_time, sub_graphs_proc_time, totaltime = get_benchmark_output(sess)
  proc_time = totaltime - copy_time

  return img, output, proc_time, sub_graphs_proc_time


def infer_test(sess, input_tensor, config):
#   pdb.set_trace()
  start_time = time.time()
  output = list(sess.run(None, input_tensor))

  stop_time = time.time()
  infer_time = stop_time - start_time
  copy_time, sub_graphs_proc_time, totaltime = get_benchmark_output(sess)
  proc_time = totaltime - copy_time

  return output, proc_time, sub_graphs_proc_time


def run_model(model, mIdx):
    print("\nRunning_Model : ", model, " \n")
    config = models_configs[model]

    #onnx shape inference
    #if not os.path.isfile(os.path.join(models_base_path, model + '_shape.onnx')):
    #    print("Writing model with shapes after running onnx shape inference -- ", os.path.join(models_base_path, model + '_shape.onnx'))
    #    onnx.shape_inference.infer_shapes_path(config['model_path'], config['model_path'])#os.path.join(models_base_path, model + '_shape.onnx'))
    
    #set input images for demo
    config = models_configs[model]
    if config['model_type'] == 'classification':
        test_images = class_test_images
    elif config['model_type'] == 'od':
        test_images = od_test_images
    elif config['model_type'] == 'seg':
        test_images = seg_test_images
    
    delegate_options = {}
    delegate_options.update(required_options)
    delegate_options.update(optional_options)  

    delegate_options['artifacts_folder'] = delegate_options['artifacts_folder'] + '/' + model + '/' #+ 'tempDir/' 

    if config['model_type'] == 'od':
        delegate_options['object_detection:meta_layers_names_list'] = config['meta_layers_names_list'] if ('meta_layers_names_list' in config) else ''
        delegate_options['object_detection:meta_arch_type'] = config['meta_arch_type'] if ('meta_arch_type' in config) else -1

    
    # delete the contents of this folder
    if args.compile or args.disable_offload:
        os.makedirs(delegate_options['artifacts_folder'], exist_ok=True)
        for root, dirs, files in os.walk(delegate_options['artifacts_folder'], topdown=False):
            [os.remove(os.path.join(root, f)) for f in files]
            [os.rmdir(os.path.join(root, d)) for d in dirs]

    if(args.compile == True):
        input_image = calib_images
    else:
        input_image = test_images

    numFrames = config['num_images']
    if(args.compile):
        if numFrames > delegate_options['advanced_options:calibration_frames']:
            numFrames = delegate_options['advanced_options:calibration_frames']
    
    print("set interpreter!!")

    ############   set interpreter  ################################
    if args.disable_offload : 
        EP_list = ['CPUExecutionProvider']
        sess = rt.InferenceSession(config['model_path'] , providers=EP_list,sess_options=so)
    elif args.compile:
        EP_list = ['TIDLCompilationProvider','CPUExecutionProvider']
        sess = rt.InferenceSession(config['model_path'] ,providers=EP_list, provider_options=[delegate_options, {}], sess_options=so)
    else:
        EP_list = ['TIDLExecutionProvider','CPUExecutionProvider']
        sess = rt.InferenceSession(config['model_path'] ,providers=EP_list, provider_options=[delegate_options, {}], sess_options=so)
    ################################################################
    
    print("run session!!")


    if model == 'pfe':
        """ PFE INPUT """

        pillar_x = torch.ones([1, 1, 12000, 100], dtype=torch.float32)
        pillar_y = torch.ones([1, 1, 12000, 100], dtype=torch.float32)
        pillar_z = torch.ones([1, 1, 12000, 100], dtype=torch.float32)
        pillar_i = torch.ones([1, 1, 12000, 100], dtype=torch.float32)
        num_points_per_pillar = torch.ones([1, 12000], dtype=torch.float32)
        x_sub_shaped = torch.ones([1, 1, 12000, 100], dtype=torch.float32)
        y_sub_shaped = torch.ones([1, 1, 12000, 100], dtype=torch.float32)
        mask = torch.ones([1, 1, 12000, 100], dtype=torch.float32)

        pfe_inputs = {sess.get_inputs()[0].name: (pillar_x.data.cpu().numpy()),
                    sess.get_inputs()[1].name: (pillar_y.data.cpu().numpy()),
                    sess.get_inputs()[2].name: (pillar_z.data.cpu().numpy()),
                    sess.get_inputs()[3].name: (pillar_i.data.cpu().numpy()),
                    sess.get_inputs()[4].name: (num_points_per_pillar.data.cpu().numpy()),
                    sess.get_inputs()[5].name: (x_sub_shaped.data.cpu().numpy()),
                    sess.get_inputs()[6].name: (y_sub_shaped.data.cpu().numpy()),
                    sess.get_inputs()[7].name: (mask.data.cpu().numpy())}
        
        # run session
        for i in range(numFrames):
            output, proc_time, sub_graph_time = infer_test(sess, pfe_inputs, config)
            total_proc_time = total_proc_time + proc_time if ('total_proc_time' in locals()) else proc_time
            sub_graphs_time = sub_graphs_time + sub_graph_time if ('sub_graphs_time' in locals()) else sub_graph_time

    else:
        rpn_input_features = torch.ones([1, 64, 496, 432], dtype=torch.float32)
        rpn_inputs = {sess.get_inputs()[0].name: (rpn_input_features.data.cpu().numpy())}
        
        # run session

        for i in range(numFrames):
            output, proc_time, sub_graph_time = infer_test(sess, rpn_inputs, config)
            total_proc_time = total_proc_time + proc_time if ('total_proc_time' in locals()) else proc_time
            sub_graphs_time = sub_graphs_time + sub_graph_time if ('sub_graphs_time' in locals()) else sub_graph_time

    
    total_proc_time = total_proc_time /1000000
    sub_graphs_time = sub_graphs_time/1000000

    # output post processing
    output_file_name = "post_proc_out_"+os.path.basename(config['model_path'])+'_'+os.path.basename(input_image[i%len(input_image)])
                    # if(args.compile == False):  # post processing enabled only for inference
                    #     if config['model_type'] == 'classification':
                    #         classes, image = get_class_labels(output[0],img)
                    #         print("\n", classes)
                    #     elif config['model_type'] == 'od':
                    #         classes, image = det_box_overlay(output, img, args.disable_offload, config['od_type'], config['framework'])
                    #     elif config['model_type'] == 'seg':
                    #         classes, image = seg_mask_overlay(output[0], img)
                    #     else:
                    #         print("Not a valid model type")

                    #     print("\nSaving image to ", delegate_options['artifacts_folder'])
                    #     image.save(delegate_options['artifacts_folder'] + output_file_name, "JPEG") 
    
    log = f'\n \nCompleted_Model : {mIdx+1:5d}, Name : {model:50s}, Total time : {total_proc_time/(i+1):10.1f}, Offload Time : {sub_graphs_time/(i+1):10.1f} , DDR RW MBs : 0, Output File : {output_file_name} \n \n ' #{classes} \n \n'
    print(log) 

    #############  print profiling result  ################
    prof_file = sess.end_profiling()
    print(prof_file)



    if ncpus > 1:
        sem.release()



################################################
# ORT PROGRAMMING MODEL




from second.pytorch.models.pointpillars import PillarFeatureNet, PointPillarsScatter


def example_convert_to_torch(example, dtype=torch.float32, device=None) -> dict:
    # device = device or torch.device("cuda:0")
    device = torch.device("cpu")
    example_torch = {}
    float_names = ["voxels", "anchors", "reg_targets", "reg_weights", "bev_map", "rect", "Trv2c", "P2"]

    for k, v in example.items():
        if k in float_names:
            example_torch[k] = torch.as_tensor(v, dtype=dtype, device=device)
        elif k in ["coordinates", "labels", "num_points"]:
            example_torch[k] = torch.as_tensor(v, dtype=torch.int32, device=device)
        elif k in ["anchors_mask"]:
            example_torch[k] = torch.as_tensor(v, dtype=torch.uint8, device=device)
            # torch.uint8 is now deprecated, please use a dtype torch.bool instead
        else:
            example_torch[k] = v
    return example_torch



def get_paddings_indicator(actual_num, max_num, axis=0):
    """
    Create boolean mask by actually number of a padded tensor.
    :param actual_num:
    :param max_num:
    :param axis:
    :return: [type]: [description]
    """
    actual_num = torch.unsqueeze(actual_num, axis+1)
    max_num_shape = [1] * len(actual_num.shape)
    max_num_shape[axis+1] = -1
    max_num = torch.arange(max_num, dtype=torch.int, device=actual_num.device).view(max_num_shape)
    # tiled_actual_num : [N, M, 1]
    # tiled_actual_num : [[3,3,3,3,3], [4,4,4,4,4], [2,2,2,2,2]]
    # title_max_num : [[0,1,2,3,4], [0,1,2,3,4], [0,1,2,3,4]]
    paddings_indicator = actual_num.int() > max_num
    # paddings_indicator shape : [batch_size, max_num]
    return paddings_indicator


def pre_process(net,
                          example,
                          class_names,
                          center_limit_range=None,
                          lidar_input=False,
                          global_set=None):

    # eval example : [0: 'voxels', 1: 'num_points', 2: 'coordinates', 3: 'rect'
    #                 4: 'Trv2c', 5: 'P2', 6: 'anchors', 7: 'anchors_mask'
    #                 8: 'image_idx', 9: 'image_shape']


    batch_image_shape = example[9]

    batch_imgidx = example[8]
    pillar_x = example[0][:, :, 0].unsqueeze(0).unsqueeze(0)
    pillar_y = example[0][:, :, 1].unsqueeze(0).unsqueeze(0)
    pillar_z = example[0][:, :, 2].unsqueeze(0).unsqueeze(0)
    pillar_i = example[0][:, :, 3].unsqueeze(0).unsqueeze(0)
    num_points_per_pillar = example[1].float().unsqueeze(0)

    # Find distance of x, y, and z from pillar center
    # assuming xyres_16.proto
    coors_x = example[2][:, 3].float()
    coors_y = example[2][:, 2].float()
    x_sub = coors_x.unsqueeze(1) * 0.16 + 0.1
    y_sub = coors_y.unsqueeze(1) * 0.16 + -39.9

    ####  xyres_20.proto  ####
    # x_sub = coors_x.unsqueeze(1) * 0.2 + 0.1
    # y_sub = coors_y.unsqueeze(1) * 0.2 - 40

    ones = torch.ones([1, 100], dtype=torch.float32, device=pillar_x.device)
    x_sub_shaped = np.matmul(x_sub, ones)
    y_sub_shaped = np.matmul(y_sub, ones)
    x_sub_shaped = torch.tensor(x_sub_shaped).unsqueeze(0).unsqueeze(0)
    y_sub_shaped = torch.tensor(y_sub_shaped).unsqueeze(0).unsqueeze(0)

    # x_sub_shaped = torch.mm(x_sub, ones).unsqueeze(0).unsqueeze(0)
    # y_sub_shaped = torch.mm(y_sub, ones).unsqueeze(0).unsqueeze(0)

    num_points_for_a_pillar = pillar_x.size()[3]
    mask = get_paddings_indicator(num_points_per_pillar, num_points_for_a_pillar, axis=0)
    mask = mask.permute(0, 2, 1)
    mask = mask.unsqueeze(1)
    mask = mask.type_as(pillar_x)

    coors   = example[2]
    anchors = example[6]
    anchors_mask = example[7]
    anchors_mask = torch.as_tensor(anchors_mask, dtype=torch.uint8, device=pillar_x.device)
    anchors_mask = anchors_mask.byte()
    rect = example[3]
    Trv2c = example[4]
    P2 = example[5]
    image_idx = example[8]


    input = [pillar_x, pillar_y, pillar_z, pillar_i,
             num_points_per_pillar, x_sub_shaped, y_sub_shaped,
             mask, coors, anchors, anchors_mask, rect, Trv2c, P2, image_idx]

    return input

def pre_reshape(inter_pre):
    pillar_x = torch.zeros([1,1,12000,100])
    pillar_y = torch.zeros([1,1,12000,100])
    pillar_z = torch.zeros([1,1,12000,100])
    pillar_i = torch.zeros([1,1,12000,100])
    num_points_per_pillar = torch.zeros([1,12000])
    x_sub_shaped = torch.zeros([1,1,12000,100])
    y_sub_shaped = torch.zeros([1,1,12000,100])
    mask = torch.zeros([1,1,12000,100])

    # pdb.set_trace()

    dim = inter_pre[0][0][0].shape[0] - 1

    for j in range(0,dim):
        num_points_per_pillar[0][j] = inter_pre[4][0][j]
        for k in range(0,99):
            pillar_x[0][0][j][k] = inter_pre[0][0][0][j][k]
            pillar_y[0][0][j][k] = inter_pre[1][0][0][j][k]
            pillar_z[0][0][j][k] = inter_pre[2][0][0][j][k]
            pillar_i[0][0][j][k] = inter_pre[3][0][0][j][k]
            x_sub_shaped[0][0][j][k] = inter_pre[5][0][0][j][k]
            y_sub_shaped[0][0][j][k] = inter_pre[6][0][0][j][k]
            mask[0][0][j][k] = inter_pre[7][0][0][j][k]

    input = [pillar_x, pillar_y, pillar_z, pillar_i,
             num_points_per_pillar, x_sub_shaped, y_sub_shaped, mask]
    
    return input


def onnx_model_predict(config_path="./second/configs/pointpillars/car/xyres_16.proto"):
    """ ORT """
    # for mIdx, model in enumerate(models):
    #     print("\nRunning_Model : ", model, " \n")
    #     config = models_configs[model]
    #     test_images    = ['../../testvecs/input/ADE_val_00001801.jpg']
        
    #     delegate_options = {}
    #     delegate_options.update(required_options)
    #     delegate_options.update(optional_options)
    #     delegate_options['artifacts_folder'] = delegate_options['artifacts_folder'] + '/' + model + '/' #+ 'tempDir/' 
        
    #     # delete the contents of this folder
    #     if args.compile or args.disable_offload:
    #         os.makedirs(delegate_options['artifacts_folder'], exist_ok=True)
    #         for root, dirs, files in os.walk(delegate_options['artifacts_folder'], topdown=False):
    #             [os.remove(os.path.join(root, f)) for f in files]
    #             [os.rmdir(os.path.join(root, d)) for d in dirs]
        
    #     EP_list = ['TIDLCompilationProvider','CPUExecutionProvider']
    #     sess = rt.InferenceSession(config['model_path'] ,providers=EP_list, provider_options=[delegate_options, {}], sess_options=so)
    #     return
        
    #     infer_image(sess, test_images, config)

    # return

    """ PYTORCH """
    
    if isinstance(config_path, str):
        config = pipeline_pb2.TrainEvalPipelineConfig()
        with open(config_path, "r") as f:
            proto_str = f.read()
            text_format.Merge(proto_str, config)
    else:
        config = config_path

    input_cfg = config.eval_input_reader
    model_cfg = config.model.second
    train_cfg = config.train_config
    class_names = list(input_cfg.class_names)
    center_limit_range = model_cfg.post_center_limit_range

    #########################
    # Build Voxel Generator
    #########################
    voxel_generator = voxel_builder.build(model_cfg.voxel_generator)
    bv_range = voxel_generator.point_cloud_range[[0, 1, 3, 4]]
    box_coder = box_coder_builder.build(model_cfg.box_coder)
    target_assigner_cfg = model_cfg.target_assigner
    target_assigner = target_assigner_builder.build(target_assigner_cfg,
                                                    bv_range, box_coder)


    net = second_builder.build(model_cfg, voxel_generator, target_assigner, input_cfg.batch_size)

    eval_dataset = input_reader_builder.build(
        input_cfg,
        model_cfg,
        training=False,
        voxel_generator=voxel_generator,
        target_assigner=target_assigner)
    eval_dataloader = torch.utils.data.DataLoader(
        eval_dataset,
        batch_size=input_cfg.batch_size,
        shuffle=False,
        num_workers=input_cfg.num_workers,
        pin_memory=False,
        collate_fn=merge_second_batch)


    vfe_num_filters = list(model_cfg.voxel_feature_extractor.num_filters)
    # voxel_generator = voxel_builder.build(model_cfg.voxel_generator)
    grid_size = voxel_generator.grid_size
    output_shape = [1] + grid_size[::-1].tolist() + [vfe_num_filters[-1]]
    num_input_features = vfe_num_filters[-1]
    batch_size = 1
    mid_feature_extractor = PointPillarsScatter(output_shape,
                                                num_input_features,
                                                batch_size)    


    coors_numpy = np.loadtxt('./second/pytorch/onnx_predict_outputs/coors.txt', dtype=np.int32)
    coors = torch.from_numpy(coors_numpy)

    ##########################
    #  PRE-PROCESING
    ##########################

    ###########################
    #  TEST INPUT TENSOR
    ###########################
    # check the pfe onnx model IR input paramters as follows 

    # pillar_x = torch.ones([1, 1, 12000, 100], dtype=torch.float32)
    # pillar_y = torch.ones([1, 1, 12000, 100], dtype=torch.float32)
    # pillar_z = torch.ones([1, 1, 12000, 100], dtype=torch.float32)
    # pillar_i = torch.ones([1, 1, 12000, 100], dtype=torch.float32)
    # num_points_per_pillar = torch.ones([1, 12000], dtype=torch.float32)
    # x_sub_shaped = torch.ones([1, 1, 12000, 100], dtype=torch.float32)
    # y_sub_shaped = torch.ones([1, 1, 12000, 100], dtype=torch.float32)
    # mask = torch.ones([1, 1, 12000, 100], dtype=torch.float32)


    ##############################
    #  INPUT READ AND PRE-PROCESS
    ##############################

    inter_pre = []
    #for one batch profiling
    sample = 1

    """ not aware what is 'global_set' used for """
    global_set = None

    for example in iter(eval_dataloader):
        # eval example [0: 'voxels', 1: 'num_points', 2: 'coordinates', 3: 'rect'
        #               4: 'Trv2c', 5: 'P2', 6: 'anchors', 7: 'anchors_mask'
        #               8: 'image_idx', 9: 'image_shape']
        example = example_convert_to_torch(example, torch.float32)

        example_tuple = list(example.values())
        example_tuple[8] = torch.from_numpy(example_tuple[8])
        example_tuple[9] = torch.from_numpy(example_tuple[9])

        if (example_tuple[6].size()[0] != input_cfg.batch_size):
            continue

        """ dunno the meaning of inter_pre' """
        
        # for kernel profiling
        t = time.time()

        inter_pre += pre_process(
            net, example_tuple, class_names, center_limit_range,
            model_cfg.lidar_input, global_set)
        
        cur_t = time.time()

        print("pre_process: ", cur_t-t)


        t = time.time()

        after_pre = pre_reshape(inter_pre)

        cur_t = time.time()

        print("pre_reshape: ", cur_t-t)


        if sample == 1:
            break
        sample += 1

    pillar_x = after_pre[0]
    pillar_y = after_pre[1]
    pillar_z = after_pre[2]
    pillar_i = after_pre[3]
    num_points_per_pillar = after_pre[4]
    x_sub_shaped = after_pre[5]
    y_sub_shaped = after_pre[6]
    mask = after_pre[7]

    # pdb.set_trace()

    EP_list = ['CPUExecutionProvider']
    # EP_list = ['TIDLCompilationProvider','CPUExecutionProvider']
    # pdb.set_trace()
    pfe_session = rt.InferenceSession("./second/onnx_zip/pfe.onnx", providers=EP_list, sess_options=so)

    # Compute ONNX Runtime output prediction
    pfe_inputs = {pfe_session.get_inputs()[0].name: (pillar_x.data.cpu().numpy()),
                  pfe_session.get_inputs()[1].name: (pillar_y.data.cpu().numpy()),
                  pfe_session.get_inputs()[2].name: (pillar_z.data.cpu().numpy()),
                  pfe_session.get_inputs()[3].name: (pillar_i.data.cpu().numpy()),
                  pfe_session.get_inputs()[4].name: (num_points_per_pillar.data.cpu().numpy()),
                  pfe_session.get_inputs()[5].name: (x_sub_shaped.data.cpu().numpy()),
                  pfe_session.get_inputs()[6].name: (y_sub_shaped.data.cpu().numpy()),
                  pfe_session.get_inputs()[7].name: (mask.data.cpu().numpy())}

    t = time.time()
    pfe_outs = pfe_session.run(None, pfe_inputs)
    cur_t = time.time()

    print("PFE: ", cur_t - t)
    # print('-------------------------- PFE ONNX Outputs ----------------------------')
    # print(pfe_outs) # also you could save it to file for comparing
    # print('-------------------------- PFE ONNX Ending ----------------------------')
    print('\n\n-------------------------- PFE ONNX Ending ----------------------------\n')
    

    ##########################
    # SCATTER
    ##########################
    
    # numpy --> tensor
    pfe_outs = np.array(pfe_outs)
    voxel_features_tensor = torch.from_numpy(pfe_outs)

    voxel_features = voxel_features_tensor.squeeze()
    # voxel_features = np.array(pfe_outs).squeeze()
    voxel_features = voxel_features.permute(1, 0)

    t = time.time()
    rpn_input_features = mid_feature_extractor(voxel_features, coors)
    cur_t = time.time()
    print("Scatter: ", cur_t - t)

    #################################RPN-Feature-Extractor########################################
    # rpn_input_features = torch.ones([1, 64, 496, 432], dtype=torch.float32)
    rpn_session = rt.InferenceSession("./second/onnx_zip/rpn.onnx", providers=EP_list, sess_options=so)
    # compute RPN ONNX Runtime output prediction
    rpn_inputs = {rpn_session.get_inputs()[0].name: (rpn_input_features.data.cpu().numpy())}

    t = time.time()
    rpn_outs = rpn_session.run(None, rpn_inputs)
    cur_t = time.time()
    print("RPN: ", cur_t - t)
    # print('---------------------- RPN ONNX Outputs ----------------------')
    # print(rpn_outs)
    # print('---------------------- RPN ONNX Ending ----------------------')
    print('\n---------------------- RPN ONNX Ending ----------------------\n')

    t = time.time()
    net.predict(inter_pre, rpn_outs)
    cur_t = time.time()
    print("NMS: ", cur_t - t)
    print('\n---------------------- NMS Ending ----------------------\n\n')

if __name__ == '__main__':
    """ ONNX RUNTIME CPU SCHEDULING """
    # onnx_model_predict()

    """ TIDL COMPILATION AND OFFLOADING """
    for mIdx, model in enumerate(models):
        run_model(model, mIdx)

    """ COMMAND BASED """
    # fire.Fire()
