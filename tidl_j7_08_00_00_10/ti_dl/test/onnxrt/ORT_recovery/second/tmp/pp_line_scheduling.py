import onnxruntime

import numpy as np
import os
import json

import threading
import sys
import pdb

# # DO NOT CHANGE THE ORDER
ep_list = ['CPUExecutionProvider', 'CUDAExecutionProvider']

def createProfileData(ep_list):
	onnxruntime.set_default_logger_severity(0)
	sess_opts = onnxruntime.SessionOptions()
	sess_opts.enable_profiling = True
	sess_opts.graph_optimization_level = onnxruntime.GraphOptimizationLevel.ORT_DISABLE_ALL

	sess = onnxruntime.InferenceSession("rpn.onnx", sess_opts)

	# INPUT & OUTPUT SETTING 

	input_name 	= sess.get_inputs()[0].name
	input_shape = sess.get_inputs()[0].shape
	input_type 	= sess.get_inputs()[0].type

	output_name 	= sess.get_outputs()[0].name
	output_shape 	= sess.get_outputs()[0].shape
	output_type 	= sess.get_outputs()[0].type

	x = np.random.random(input_shape)
	x = x.astype(np.float32)

	for ep_type in ep_list:
		sess_opts.profile_file_prefix = ep_type

		sess.set_providers([ep_type])

		sess_output = sess.run(None, {input_name: x})


def run_profile():
	results = []

	t1 = threading.Thread(target=createProfileData(ep_list))
	t1.start()
	t1.join()

	pcie_speed = 17179869184 / 1000
	ep_profiles = {'nodes' : []}

	for ep_type in ep_list:
		files = [f for f in os.listdir('.') if os.path.isfile(f) and ep_type in f]
		for f in files:
			ep_json = json.load(open(f))
			for item in ep_json:
				if (item['name'].find('_kernel_time') != -1):
					node_name = item['name'].replace('_kernel_time', '')
					ep_type = item['args']['provider']
					ep_profile = next((ep_profile for ep_profile in ep_profiles['nodes'] if ep_profile['name'] == node_name), False)

					if (ep_profile):
						if (ep_type == 'CUDAExecutionProvider'):
							ep_profile['supportedOnGpu'] = 1
							ep_profile['gpuLatency'] = float(float(item['dur']) / 1000)
					else:
						if (ep_type == 'CPUExecutionProvider'):
							ep_profile = {}
							ep_profile['name'] = node_name
							ep_profile['id'] = int(item['args']['graph_index'])
							ep_profile['cpuLatency'] = float(float(item['dur']) / 1000)
							ep_profiles['nodes'].append(ep_profile)	
							ep_profile['inputTransferCost'] = round(float(float(item['args']['parameter_size']) + float(item['args']['activation_size']))/pcie_speed, 5)
							ep_profile['outputTransferCost'] = round(float(item['args']['output_size'])/pcie_speed, 3)
	
	return ep_profiles


def dnnAssembly(ep_list, ep_profiles):
	
	NUM_NODES = len(ep_profiles['nodes'])

	dnn_partition = {'CPUExecutionProvider' : [], 'CUDAExecutionProvider': []}
	nodes = ep_profiles['nodes']
	d1 = [0 for i in range(NUM_NODES)]
	d2 = [0 for i in range(NUM_NODES)]
	T1 = [0 for i in range(NUM_NODES)]
	T2 = [0 for i in range(NUM_NODES)]	
	# CPU
	T1[0] = 0 + nodes[0]['cpuLatency']
	# GPU
	T2[0] = nodes[0]['inputTransferCost'] + nodes[0]['gpuLatency']

	for i in range(1, NUM_NODES):
		if (T1[i-1] + nodes[i]['cpuLatency'] <= T2[i-1] + nodes[i]['inputTransferCost'] + nodes[i]['cpuLatency']):
			T1[i] = T1[i-1] + nodes[i]['cpuLatency']
			d1[i] = 1
		else:
			T1[i] = T2[i-1] + nodes[i]['inputTransferCost'] + nodes[i]['cpuLatency']
			d1[i] = 2
		if (T2[i-1] + nodes[i]['gpuLatency'] <= T1[i-1] + nodes[i]['inputTransferCost'] + nodes[i]['gpuLatency']):
			T2[i] = T2[i-1] + nodes[i]['gpuLatency']
			d2[i] = 2
		else:
			T2[i] = T1[i-1] + nodes[i]['inputTransferCost'] + nodes[i]['gpuLatency']
			d2[i] = 1	

	if (T1[NUM_NODES - 1] + 0 <= T2[NUM_NODES - 1] + nodes[NUM_NODES - 1]['outputTransferCost']):
		f_star = T1[NUM_NODES - 1] + 0
		d_star = 1
	else:
		f_star = T2[NUM_NODES - 1] + nodes[NUM_NODES - 1]['outputTransferCost']
		d_star = 2

	f = lambda x : 'CPUExecutionProvider' if x == 1 else ('CUDAExecutionProvider' if x == 2 else 'WRG')
	d = d_star
	dnn_partition[f(d)].append(NUM_NODES-1)

	for i in reversed(range(1, NUM_NODES)):
		if (d == 1):
			d = d1[i]
			dnn_partition[f(d)].append(i-1)
		else:
			d = d2[i]
			dnn_partition[f(d)].append(i-1)
	totalCpuLatency = 0
	totalGpuLatency = 0
	for i in range(0, NUM_NODES):
		totalCpuLatency += nodes[i]['cpuLatency']
		totalGpuLatency += nodes[i]['gpuLatency']
	print("Optimal latency: ", round(f_star, 2), 'ms')
	print("CPU latency: ", round(totalCpuLatency, 2), 'ms', (round((totalCpuLatency - f_star)/ f_star * 100)),'%')
	print("GPU latency: ", round(totalGpuLatency, 2), 'ms', (round((totalGpuLatency - f_star)/ f_star * 100)),'%')
	return dnn_partition


def ort_run():
	ep_profiles = run_profile()

	dnn_partition = dnnAssembly(ep_list, ep_profiles)

	print("\n\n")
	print(dnn_partition)
	print("\n\n")

	onnxruntime.set_default_logger_severity(0)
	sess_opts = onnxruntime.SessionOptions()
	sess_opts.enable_profiling = True
	sess_opts.graph_optimization_level = onnxruntime.GraphOptimizationLevel.ORT_DISABLE_ALL
	sess_opts.profile_file_prefix = 'OPT_PARTITIONING'

	pdb.set_trace()
	sess = onnxruntime.InferenceSession("rpn.onnx", sess_opts)

	sess_opts.enable_partitioning = True
	sess.set_partitions(dnn_partition)

	input_name 	= sess.get_inputs()[0].name
	input_shape = sess.get_inputs()[0].shape
	input_type 	= sess.get_inputs()[0].type

	output_name 	= sess.get_outputs()[0].name
	output_shape 	= sess.get_outputs()[0].shape
	output_type 	= sess.get_outputs()[0].type

	x = np.random.random(input_shape)
	x = x.astype(np.float32)

	sess_output = sess.run(None, {input_name: x})

	return 0


if __name__ == '__main__':
    ort_run()
