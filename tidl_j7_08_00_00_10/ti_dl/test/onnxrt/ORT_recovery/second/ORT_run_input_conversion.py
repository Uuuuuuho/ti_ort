# Copyright (c) Microsoft Corporation. All rights reserved.
# Licensed under the MIT License.

"""

.. _l-example-backend-api:

ONNX Runtime Backend for ONNX
=============================

*ONNX Runtime* extends the 
`onnx backend API <https://github.com/onnx/onnx/blob/master/docs/ImplementingAnOnnxBackend.md>`_
to run predictions using this runtime.
Let's use the API to compute the prediction
of a simple logistic regression model.
"""
import numpy as np
import onnxruntime as ort
from onnxruntime import datasets

from onnxruntime.capi.onnxruntime_pybind11_state import InvalidArgument
import pdb
from PIL import Image
# import onnxruntime.backend as backend
# from onnx import load


# ###############################
# ADDED to run PointPillar model
# ###############################

import os
import pathlib
import pickle
import shutil
import time
from functools import partial
import sys
sys.path.append('../')
from pathlib import Path
import fire
import numpy as np

#### should remove torch later if possible ####
import torch

from google.protobuf import text_format
from tensorboardX import SummaryWriter

import torchplus
import second.data.kitti_common as kitti
from second.builder import target_assigner_builder, voxel_builder
from second.data.preprocess import merge_second_batch
from second.protos import pipeline_pb2
from second.pytorch.builder import (box_coder_builder, input_reader_builder,
                                    lr_scheduler_builder, optimizer_builder,
                                    second_builder)
from second.utils.eval import get_coco_eval_result, get_official_eval_result
from second.utils.progress_bar import ProgressBar


def ort_run(config_path=None,
            model_dir=None,
            result_path=None,
            create_folder=False,
            display_step=50,
            summary_step=5,
            pickle_result=True):

    #################################  Session option  ########################################
    sess_options = ort.SessionOptions()

    # #### Set graph optimization level
    sess_options.graph_optimization_level = ort.GraphOptimizationLevel.ORT_DISABLE_ALL
    # sess_options.graph_optimization_level = ort.GraphOptimizationLevel.ORT_ENABLE_BASIC 
    # sess_options.graph_optimization_level = ort.GraphOptimizationLevel.ORT_ENABLE_EXTENDED  
    # sess_options.graph_optimization_level = ort.GraphOptimizationLevel.ORT_ENABLE_ALL
    # sess_options.enable_profiling = True

    #### save optimized graph in ONNX IR? 
    #### if possible, load the optimized graph & execute 
    #### then, the optimized graph is saved in ONNX IR
    # sess_options.optimized_model_filepath = "/home/uho/workspace/ONNX_pointpillar/second/disabled_optimization.onnx" 
    # sess_options.optimized_model_filepath = "/home/uho/workspace/ONNX_pointpillar/second/layout_optimized.onnx" 

    # name = datasets.get_example("sigmoid.onnx")
    # name = datasets.get_example("logreg_iris.onnx")
    # sess = ort.InferenceSession('resnet50-v1-7.onnx')

    pfe_session = ort.InferenceSession("pfe.onnx", sess_options)
    pscat_session = ort.InferenceSession("pscat.onnx", sess_options)
    rpn_session = ort.InferenceSession("rpn.onnx", sess_options)
    # nms_session = ort.InferenceSession("nms.onnx", sess_options)

    # pfe_session = ort.InferenceSession("pfe.onnx")
    # pscat_session = ort.InferenceSession("pscat.onnx")
    # rpn_session = ort.InferenceSession("rpn.onnx")
    # nms_session = ort.InferenceSession("nms.onnx")

    # pfe_session.set_providers(  ['CPUExecutionProvider'])
    # pscat_session.set_providers(['CPUExecutionProvider'])
    # rpn_session.set_providers(  ['CPUExecutionProvider'])
    # nms_session.set_providers(  ['CPUExecutionProvider'])

    # pfe_session.set_providers(  ['CUDAExecutionProvider'])
    # pscat_session.set_providers(['CUDAExecutionProvider'])
    # rpn_session.set_providers(  ['CUDAExecutionProvider'])
    # nms_session.set_providers(  ['CUDAExecutionProvider'])


    #################################  Read a config file  ########################################


    model_dir = pathlib.Path(model_dir)
    config = pipeline_pb2.TrainEvalPipelineConfig()
    with open(config_path, "r") as f:
        proto_str = f.read()
        text_format.Merge(proto_str, config)

    input_cfg = config.eval_input_reader
    model_cfg = config.model.second
    train_cfg = config.train_config
    class_names = list(input_cfg.class_names)
    center_limit_range = model_cfg.post_center_limit_range

    #########################
    # Build Voxel Generator
    #########################
    

    voxel_generator = voxel_builder.build(model_cfg.voxel_generator)
    bv_range = voxel_generator.point_cloud_range[[0, 1, 3, 4]]
    box_coder = box_coder_builder.build(model_cfg.box_coder)
    target_assigner_cfg = model_cfg.target_assigner
    target_assigner = target_assigner_builder.build(target_assigner_cfg,
                                                    bv_range, box_coder)

    # net = second_builder.build(model_cfg, voxel_generator, target_assigner, 2)
    net = second_builder.build(model_cfg, 
                voxel_generator, target_assigner, input_cfg.batch_size)
    
    eval_dataset = input_reader_builder.build(
        input_cfg,
        model_cfg,
        training=False,
        voxel_generator=voxel_generator,
        target_assigner=target_assigner)
    eval_dataloader = torch.utils.data.DataLoader(
        eval_dataset,
        batch_size=input_cfg.batch_size,
        shuffle=False,
        num_workers=input_cfg.num_workers,
        pin_memory=False,
        collate_fn=merge_second_batch)

    float_dtype = torch.float32



    ######################
    # Evaluate
    ######################

    dt_annos = []

    ####  for Pillar Scatter Net  ####

    vfe_num_filters = list(model_cfg.voxel_feature_extractor.num_filters)
    grid_size = voxel_generator.grid_size
    output_shape = [1] + grid_size[::-1].tolist() + [vfe_num_filters[-1]]
    num_input_features = vfe_num_filters[-1]
    batch_size = 2


    print("Generate output labels...")
    init_time = time.time()
    interval = 0
    pfe_interval = 0
    pscat_interval = 0
    rpn_interval = 0
    nms_interval = 0
    # interval = time.time()

    bar = ProgressBar()
    bar.start(len(eval_dataset) // input_cfg.batch_size + 1)

    dt_annos = []
    global_set = None

    sample = 1

    for example in iter(eval_dataloader):
        # eval example [0: 'voxels', 1: 'num_points', 2: 'coordinates', 3: 'rect'
        #               4: 'Trv2c', 5: 'P2', 6: 'anchors', 7: 'anchors_mask'
        #               8: 'image_idx', 9: 'image_shape']
        # pdb.set_trace()
        example = example_convert_to_torch(example, float_dtype)

        example_tuple = list(example.values())
        # pdb.set_trace()
        batch_image_shape = example["image_shape"]
        # batch_image_shape = example_tuple[8]
        example_tuple[8] = torch.from_numpy(example_tuple[8])
        example_tuple[9] = torch.from_numpy(example_tuple[9])

        if (example_tuple[6].size()[0] != input_cfg.batch_size):
            continue

        inputs_returned = predict_kitti_to_anno(example_tuple)
        
        coors = torch.tensor(inputs_returned[8], dtype=torch.int32)

        ####  Pillar Fearture Net  ####

        pfe_inputs = {  pfe_session.get_inputs()[0].name: (inputs_returned[0].data.numpy()),
                        pfe_session.get_inputs()[1].name: (inputs_returned[1].data.numpy()),
                        pfe_session.get_inputs()[2].name: (inputs_returned[2].data.numpy()),
                        pfe_session.get_inputs()[3].name: (inputs_returned[3].data.numpy()),
                        pfe_session.get_inputs()[4].name: (inputs_returned[4].data.numpy()),
                        pfe_session.get_inputs()[5].name: (inputs_returned[5].data.numpy()),
                        pfe_session.get_inputs()[6].name: (inputs_returned[6].data.numpy()),
                        pfe_session.get_inputs()[7].name: (inputs_returned[7].data.numpy())
                    }

        t = time.time()

        pillar_features = pfe_session.run(None, pfe_inputs)

        pfe_interval += (time.time() - t)
        
        ####  Pillar Scatter Net  ####    

                    
        pillar_features_tensor = torch.FloatTensor(pillar_features)
        pillar_features = pillar_features_tensor.squeeze()
        pillar_features = pillar_features.permute(1, 0) 

        pscat_inputs = {pscat_session.get_inputs()[0].name: (pillar_features.data.numpy()),
                        pscat_session.get_inputs()[1].name: (coors.data.numpy())
                        }

        t = time.time()

        rpn_inputs = pscat_session.run(None, pscat_inputs)

        pscat_interval += (time.time() - t)

        ####  Reigion Proposal Network  ###

        rpn_inputs = {rpn_session.get_inputs()[0].name: rpn_inputs[0]}

        t = time.time()

        rpn_outs = rpn_session.run(None, rpn_inputs)

        rpn_interval += (time.time() - t)



        predict_inputs_0 = torch.Tensor(rpn_outs[0])
        predict_inputs_1 = torch.Tensor(rpn_outs[1])
        predict_inputs_2 = torch.Tensor(rpn_outs[2])

        t = time.time()

        # predict comparison with PyTorch framework
        post_nms = net.predict_nms(inputs_returned, predict_inputs_0,
                                    predict_inputs_1, predict_inputs_2)

        nms_interval += (time.time() - t)



        #################################
        ####  process post nms outputs
        #################################
        merge_annos = []
        annos = []
        for i, preds_dict in enumerate(post_nms):
            image_shape = batch_image_shape[i]
            img_idx = preds_dict[5]

            if preds_dict[0] is not None: # bbox list
                box_2d_preds = preds_dict[0].detach().cpu().numpy() # bbox
                box_preds = preds_dict[1].detach().cpu().numpy() # bbox3d_camera
                scores = preds_dict[3].detach().cpu().numpy() # scores
                box_preds_lidar = preds_dict[2].detach().cpu().numpy() # box3d_lidar
                # write pred to file
                label_preds = preds_dict[4].detach().cpu().numpy() # label_preds

                anno = kitti.get_start_result_anno()
                num_example = 0
                
                for box, box_lidar, bbox, score, label in zip(
                        box_preds, box_preds_lidar, box_2d_preds, scores,
                        label_preds):
                    
                    # pdb.set_trace()

                    if bbox[0] > image_shape[1] or bbox[1] > image_shape[0]:
                        continue
                    if bbox[2] < 0 or bbox[3] < 0:
                        continue

                    if center_limit_range is not None:
                        limit_range = np.array(center_limit_range)
                        if (np.any(box_lidar[:3] < limit_range[:3])
                                or np.any(box_lidar[:3] > limit_range[3:])):
                            continue
                    image_shape = [image_shape[0], image_shape[1]]
                    bbox[2:] = np.minimum(bbox[2:], image_shape[::-1])
                    bbox[:2] = np.maximum(bbox[:2], [0, 0])
                    anno["name"].append(class_names[int(label)])
                    anno["truncated"].append(0.0)
                    anno["occluded"].append(0)
                    anno["alpha"].append(-np.arctan2(-box_lidar[1], box_lidar[0]) +
                                        box[6])
                    anno["bbox"].append(bbox)
                    anno["dimensions"].append(box[3:6])
                    anno["location"].append(box[:3])
                    anno["rotation_y"].append(box[6])
                    if global_set is not None:
                        for i in range(100000):
                            if score in global_set:
                                score -= 1 / 100000
                            else:
                                global_set.add(score)
                                break
                    anno["score"].append(score)

                    num_example += 1
                if num_example != 0:
                    anno = {n: np.stack(v) for n, v in anno.items()}
                    annos.append(anno)
                else:
                    annos.append(kitti.empty_result_anno())
            else:
                annos.append(kitti.empty_result_anno())
            num_example = annos[-1]["name"].shape[0]
            annos[-1]["image_idx"] = np.array(
                [img_idx] * num_example, dtype=np.int64)

            merge_annos += annos
        
        bar.print_bar()
        # if sample == 1:
        #     break
        sample += 1





        # # intermediate value checking
        # print(rpn_outs)

        ####  Non-maximum suppression  ####

        # nms_inputs = {  nms_session.get_inputs()[0].name: (inputs_returned[9].data.numpy()),
        #                 nms_session.get_inputs()[1].name: (inputs_returned[10].data.numpy()),
        #                 nms_session.get_inputs()[2].name: (inputs_returned[11].data.numpy()),
        #                 nms_session.get_inputs()[3].name: (inputs_returned[12].data.numpy()),
        #                 nms_session.get_inputs()[4].name: (inputs_returned[13].data.numpy()),
        #                 nms_session.get_inputs()[5].name: (rpn_outs[0]),
        #                 nms_session.get_inputs()[6].name: (rpn_outs[1]),
        #                 nms_session.get_inputs()[7].name: (rpn_outs[2])
        #             }

        # nms_inputs = {  nms_session.get_inputs()[0].name: (inputs_returned[9].data.numpy()),
        #                 nms_session.get_inputs()[1].name: (inputs_returned[10].data.numpy()),
        #                 nms_session.get_inputs()[2].name: (inputs_returned[11].data.numpy()),
        #                 nms_session.get_inputs()[3].name: (inputs_returned[12].data.numpy()),
        #                 nms_session.get_inputs()[4].name: (inputs_returned[13].data.numpy()),
        #                 nms_session.get_inputs()[5].name: (inputs_returned[14].data.numpy()),
        #                 nms_session.get_inputs()[6].name: (rpn_outs[0]),
        #                 nms_session.get_inputs()[7].name: (rpn_outs[1])
        #             }

        # nms_inputs = {  nms_session.get_inputs()[0].name: (inputs_returned[9].data.numpy()),
        #                 nms_session.get_inputs()[1].name: (rpn_outs[0]),
        #                 nms_session.get_inputs()[2].name: (rpn_outs[1]),
        #                 nms_session.get_inputs()[3].name: (rpn_outs[2])
        #            }

        # this input is for pre_nms.onnx
        # nms_inputs = {  nms_session.get_inputs()[0].name: (inputs_returned[9].data.numpy()),
        #                 nms_session.get_inputs()[1].name: (inputs_returned[10].data.numpy()),
        #                 nms_session.get_inputs()[2].name: (rpn_outs[0]),
        #                 nms_session.get_inputs()[3].name: (rpn_outs[1])
        #            }
        
        # nms_inputs = {  nms_session.get_inputs()[0].name: (inputs_returned[9].data.numpy()),
        #                 nms_session.get_inputs()[1].name: (inputs_returned[10].data.numpy()),
        #                 nms_session.get_inputs()[2].name: (rpn_outs[0])
        #            }
        

        # nms_inputs = {  nms_session.get_inputs()[0].name: (inputs_returned[10].data.numpy()),
        #                 nms_session.get_inputs()[1].name: (rpn_outs[1])
        #             }


        # t = time.time()

        # nms_outputs = nms_session.run(None, nms_inputs)

        # nms_interval += (time.time() - t)











        # pdb.set_trace()

        # print(f'interval: {interval:.6f} s')


        # break
        # print('\nepoch done\n\n')

    interval = (time.time() - init_time)
    sec_per_example = len(eval_dataset) / (time.time() - init_time)
    print('\n\n')
    print(f'generate label finished({sec_per_example:.2f}/s). start eval:')
    print('\n')
    # print('total time interval: ')  # only time interval of execution
    print(f'interval: {interval:.6f} s')
    print(f'pfe_interval: {pfe_interval:.6f} s')
    print(f'pscat_interval: {pscat_interval:.6f} s')
    print(f'rpn_interval: {rpn_interval:.6f} s')
    print(f'nms_interval: {nms_interval:.6f} s')
    print('\n')

    return 0


def example_convert_to_torch(example, dtype=torch.float32, device='cpu') -> dict:
    # device = device or torch.device("cpu")
    # device = device or torch.device("cuda:0")
    example_torch = {}
    float_names = ["voxels", "anchors", "reg_targets", "reg_weights", "bev_map", "rect", "Trv2c", "P2"]

    for k, v in example.items():
        if k in float_names:
            example_torch[k] = torch.as_tensor(v, dtype=dtype, device=device)
        elif k in ["coordinates", "labels", "num_points"]:
            example_torch[k] = torch.as_tensor(v, dtype=torch.int32, device=device)
        elif k in ["anchors_mask"]:
            example_torch[k] = torch.as_tensor(v, dtype=torch.uint8, device=device)
            # torch.uint8 is now deprecated, please use a dtype torch.bool instead
        else:
            example_torch[k] = v
    return example_torch

def dim_change_2d(obj):
    obj.resize_([12000,100])
    return obj
    # tmp = torch.zeros([20000, 100], dtype=torch.float32)
    # row = len(obj)
    # col = len(obj[0])
    # for i in range(row):
    #     for j in range(col):
    #         tmp[i][j] = obj[i][j]
    # return tmp

def dim_change_coor(obj):
    obj = obj.float()
    obj.resize_([12000,4])
    return obj
    # tmp = torch.zeros([20000, 4], dtype=torch.int32)
    # row = len(obj)
    # col = len(obj[0])
    # for i in range(row):
    #     for j in range(col):
    #         tmp[i][j] = obj[i][j]
    # return tmp

def dim_change_1d(obj):
    obj = obj.float()
    obj.resize_(12000)
    return obj
    # tmp = torch.zeros([20000], dtype=torch.int32)
    # row = len(obj)
    # for i in range(row):
    #     tmp[i] = obj[i]
    # return tmp


def predict_kitti_to_anno(example):

    # eval example : [0: 'voxels', 1: 'num_points', 2: 'coordinates', 3: 'rect'
    #                 4: 'Trv2c', 5: 'P2', 6: 'anchors', 7: 'anchors_mask'
    #                 8: 'image_idx', 9: 'image_shape']
    
    ##############################################
    # dim change for static shape input
    # edited by UHO

    pillar_x = dim_change_2d(example[0][:, :, 0]).unsqueeze(0).unsqueeze(0)
    pillar_y = dim_change_2d(example[0][:, :, 1]).unsqueeze(0).unsqueeze(0)
    pillar_z = dim_change_2d(example[0][:, :, 2]).unsqueeze(0).unsqueeze(0)
    pillar_i = dim_change_2d(example[0][:, :, 3]).unsqueeze(0).unsqueeze(0)
    num_points_per_pillar = dim_change_1d(example[1]).float().unsqueeze(0)



    ##############################################

    batch_image_shape = example[9]

    batch_imgidx = example[8]

    # Find distance of x, y, and z from pillar center
    # assuming xyres_16.proto
    coors_x = dim_change_1d(example[2][:, 3]).float()
    coors_y = dim_change_1d(example[2][:, 2]).float()
    
    # assuming xyres_24.proto
    # x_sub = coors_x.unsqueeze(1) * 0.24 + 0.08
    # y_sub = coors_y.unsqueeze(1) * 0.24 - 40.0
    # assuming xyres_16.proto
    x_sub = coors_x.unsqueeze(1) * 0.16 + 0.1
    y_sub = coors_y.unsqueeze(1) * 0.16 + -39.9
    ones = torch.ones([1, 100], dtype=torch.float32, device=pillar_x.device)
    x_sub_shaped = torch.mm(x_sub, ones).unsqueeze(0).unsqueeze(0)
    y_sub_shaped = torch.mm(y_sub, ones).unsqueeze(0).unsqueeze(0)


    num_points_for_a_pillar = pillar_x.size()[3]
    mask = get_paddings_indicator(num_points_per_pillar, num_points_for_a_pillar, axis=0)
    mask = mask.permute(0, 2, 1)
    mask = mask.unsqueeze(1)
    mask = mask.type_as(pillar_x)

    coors   = dim_change_coor(example[2])
    anchors = example[6]
    anchors_mask = example[7]
    # opriginal code
    anchors_mask = torch.as_tensor(anchors_mask, dtype=torch.uint8, device=pillar_x.device)
    anchors_mask = anchors_mask.byte()
    # anchors_mask = anchors_mask.type(dtype=torch.uint8)

    # pdb.set_trace()


    # anchors_mask = torch.as_tensor(anchors_mask, dtype=torch.bool, device=pillar_x.device)
    # anchors_mask = anchors_mask.byte()


    non_zeros = anchors_mask.nonzero()
    # non_zeros = non_zeros[1]
    
    rect = example[3]
    Trv2c = example[4]
    P2 = example[5]
    image_idx = example[8].float()




    inputs = [pillar_x, pillar_y, pillar_z, pillar_i,
             num_points_per_pillar, x_sub_shaped, y_sub_shaped,
            #  mask, coors, anchors, non_zeros, rect, Trv2c, P2, image_idx]
             mask, coors, anchors, anchors_mask, rect, Trv2c, P2, image_idx]

    return inputs




def get_paddings_indicator(actual_num, max_num, axis=0):
    """
    Create boolean mask by actually number of a padded tensor.
    :param actual_num:
    :param max_num:
    :param axis:
    :return: [type]: [description]
    """
    actual_num = torch.unsqueeze(actual_num, axis+1)
    max_num_shape = [1] * len(actual_num.shape)
    max_num_shape[axis+1] = -1
    max_num = torch.arange(max_num, dtype=torch.int, device=actual_num.device).view(max_num_shape)
    # tiled_actual_num : [N, M, 1]
    # tiled_actual_num : [[3,3,3,3,3], [4,4,4,4,4], [2,2,2,2,2]]
    # title_max_num : [[0,1,2,3,4], [0,1,2,3,4], [0,1,2,3,4]]
    paddings_indicator = actual_num.int() > max_num
    # paddings_indicator shape : [batch_size, max_num]
    return paddings_indicator

if __name__ == '__main__':
    fire.Fire()