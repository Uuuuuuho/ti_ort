from enum import Enum
from functools import reduce

import numpy as np
from second.pytorch.core import box_torch_ops
from second.pytorch.utils import get_paddings_indicator
import pdb

def predict(example, preds_dict, input_config, model_config):
    
    batch_size = input_config.batch_size

    batch_anchors = example[9].view(batch_size, -1, 7)
    batch_rect = example[11]
    batch_Trv2c = example[12]
    batch_P2 = example[13]

    # if "anchors_mask" not in example:
    #     batch_anchors_mask = [None] * batch_size
    # else:
    #     batch_anchors_mask = example["anchors_mask"].view(batch_size, -1)
    
    ###########################################################
    ####  have to comment this line to export NMS into ONNX IR
    # assert 15==len(example), "somthing write with example size!"
    
    batch_anchors_mask = example[10].view(batch_size, -1)
    batch_imgidx = example[14]

    batch_box_preds = preds_dict[0]
    batch_cls_preds = preds_dict[1]
    batch_box_preds = batch_box_preds.view(batch_size, -1,
                                            model_config.box_coder.code_size)
    num_class_with_bg = model_config.num_class
    if not model_config.encode_background_as_zeros:
        num_class_with_bg = model_config.num_class + 1

    batch_cls_preds = batch_cls_preds.view(batch_size, -1,
                                            num_class_with_bg)
    batch_box_preds = model_config.box_coder.decode_torch(batch_box_preds,
                                                    batch_anchors)
    if model_config.use_direction_classifier:
        batch_dir_preds = preds_dict[2]
        batch_dir_preds = batch_dir_preds.view(batch_size, -1, 2)
    else:
        batch_dir_preds = [None] * batch_size

    # predictions_dicts = []
    predictions_dicts = ()
    for box_preds, cls_preds, dir_preds, rect, Trv2c, P2, img_idx, a_mask in zip(
            batch_box_preds, batch_cls_preds, batch_dir_preds, batch_rect,
            batch_Trv2c, batch_P2, batch_imgidx, batch_anchors_mask
    ):
        if a_mask is not None:
            box_preds = box_preds[a_mask]
            cls_preds = cls_preds[a_mask]
        if model_config.use_direction_classifier:
            if a_mask is not None:
                dir_preds = dir_preds[a_mask]
            # print(dir_preds.shape)
            dir_labels = torch.max(dir_preds, dim=-1)[1]
        if model_config.encode_background_as_zeros:
            # this don't support softmax
            assert model_config.use_sigmoid_score is True
            total_scores = torch.sigmoid(cls_preds)
        else:
            # encode background as first element in one-hot vector
            if model_config.use_sigmoid_score:
                total_scores = torch.sigmoid(cls_preds)[..., 1:]
            else:
                total_scores = F.softmax(cls_preds, dim=-1)[..., 1:]
        # Apply NMS in birdeye view
        if model_config.use_rotate_nms:
            nms_func = box_torch_ops.rotate_nms
        else:
            nms_func = box_torch_ops.nms
        selected_boxes = None
        selected_labels = None
        selected_scores = None
        selected_dir_labels = None

        # removed multi_class option
        # get highest score per prediction, than apply nms
        # to remove overlapped box.
        if num_class_with_bg == 1:
            top_scores = total_scores.squeeze(-1)
            top_labels = torch.zeros(
                total_scores.shape[0],
                device=total_scores.device,
                dtype=torch.long)
        else:
            top_scores, top_labels = torch.max(total_scores, dim=-1)

        if model_config.nms_score_threshold > 0.0:
            thresh = torch.tensor(
                [model_config.nms_score_threshold],
                device=total_scores.device).type_as(total_scores)
            top_scores_keep = (top_scores >= thresh)
            top_scores = top_scores.masked_select(top_scores_keep)
        if top_scores.shape[0] != 0:
            if model_config.nms_score_threshold > 0.0:
                box_preds = box_preds[top_scores_keep]
                if model_config.use_direction_classifier:
                    dir_labels = dir_labels[top_scores_keep]
                top_labels = top_labels[top_scores_keep]
            boxes_for_nms = box_preds[:, [0, 1, 3, 4, 6]]

            # the nms in 3d detection just remove overlap boxes.
            selected = nms_func(
                boxes_for_nms,
                top_scores,
                # pre_max_size=120000,
                pre_max_size=model_config.nms_pre_max_size,
                # post_max_size=120000,
                post_max_size=model_config.nms_post_max_size,
                iou_threshold=model_config.nms_iou_threshold,
            )
        else:
            selected = None
        if selected is not None:
            selected_boxes = box_preds[selected]
            if model_config.use_direction_classifier:
                selected_dir_labels = dir_labels[selected]
            selected_labels = top_labels[selected]
            selected_scores = top_scores[selected]
        # finally generate predictions.

        if selected_boxes is not None:
            box_preds = selected_boxes
            scores = selected_scores
            label_preds = selected_labels
            
            if model_config.use_direction_classifier:
                dir_labels = selected_dir_labels
                # opp_labels = (box_preds[..., -1] > 0)
                
                #### original code ####
                opp_labels = (box_preds[..., -1] > 0) ^ (dir_labels.byte() > 0)


                box_preds[..., -1] += torch.where(
                    opp_labels,
                    torch.tensor(np.pi).type_as(box_preds),
                    torch.tensor(0.0).type_as(box_preds))
                # box_preds[..., -1] += (
                #     ~(dir_labels.byte())).type_as(box_preds) * np.pi
            final_box_preds = box_preds
            final_scores = scores
            final_labels = label_preds
            final_box_preds_camera = box_torch_ops.box_lidar_to_camera(
                final_box_preds, rect, Trv2c)
            locs = final_box_preds_camera[:, :3]
            dims = final_box_preds_camera[:, 3:6]
            angles = final_box_preds_camera[:, 6]
            camera_box_origin = [0.5, 1.0, 0.5]
            box_corners = box_torch_ops.center_to_corner_box3d(
                locs, dims, angles, camera_box_origin, axis=1)
            box_corners_in_image = box_torch_ops.project_to_image(
                box_corners, P2)
            # box_corners_in_image: [N, 8, 2]
            minxy = torch.min(box_corners_in_image, dim=1)[0]
            maxxy = torch.max(box_corners_in_image, dim=1)[0]
            # minx = torch.min(box_corners_in_image[..., 0], dim=1)[0]
            # maxx = torch.max(box_corners_in_image[..., 0], dim=1)[0]
            # miny = torch.min(box_corners_in_image[..., 1], dim=1)[0]
            # maxy = torch.max(box_corners_in_image[..., 1], dim=1)[0]
            # box_2d_preds = torch.stack([minx, miny, maxx, maxy], dim=1)
            box_2d_preds = torch.cat([minxy, maxxy], dim=1)
            # pdb.set_trace()

            predictions_dict = (box_2d_preds, final_box_preds_camera,
                                final_box_preds, final_scores, label_preds, img_idx)
        else:
            predictions_dict = (None, None, None, None, None, img_idx)
    
        predictions_dicts += (predictions_dict)
    
    return predictions_dicts
