49
0 resnetv15_conv0_fwd Conv outputAdjNodes 1 1 inputAdjNodes 0 data diagInfo 
1 resnetv15_relu0_fwd Relu outputAdjNodes 1 2 inputAdjNodes 1 0 diagInfo 
2 resnetv15_pool0_fwd MaxPool outputAdjNodes 2 3 6 inputAdjNodes 1 1 diagInfo 
3 resnetv15_stage1_conv0_fwd Conv outputAdjNodes 1 4 inputAdjNodes 1 2 diagInfo 
4 resnetv15_stage1_relu0_fwd Relu outputAdjNodes 1 5 inputAdjNodes 1 3 diagInfo 
5 resnetv15_stage1_conv1_fwd Conv outputAdjNodes 1 6 inputAdjNodes 1 4 diagInfo 
6 resnetv15_stage1__plus0 Add outputAdjNodes 1 7 inputAdjNodes 2 2 5 diagInfo 
7 resnetv15_stage1_activation0 Relu outputAdjNodes 2 8 11 inputAdjNodes 1 6 diagInfo 
8 resnetv15_stage1_conv2_fwd Conv outputAdjNodes 1 9 inputAdjNodes 1 7 diagInfo 
9 resnetv15_stage1_relu1_fwd Relu outputAdjNodes 1 10 inputAdjNodes 1 8 diagInfo 
10 resnetv15_stage1_conv3_fwd Conv outputAdjNodes 1 11 inputAdjNodes 1 9 diagInfo 
11 resnetv15_stage1__plus1 Add outputAdjNodes 1 12 inputAdjNodes 2 7 10 diagInfo 
12 resnetv15_stage1_activation1 Relu outputAdjNodes 2 13 16 inputAdjNodes 1 11 diagInfo 
13 resnetv15_stage2_conv0_fwd Conv outputAdjNodes 1 14 inputAdjNodes 1 12 diagInfo 
14 resnetv15_stage2_relu0_fwd Relu outputAdjNodes 1 15 inputAdjNodes 1 13 diagInfo 
15 resnetv15_stage2_conv1_fwd Conv outputAdjNodes 1 17 inputAdjNodes 1 14 diagInfo 
16 resnetv15_stage2_conv2_fwd Conv outputAdjNodes 1 17 inputAdjNodes 1 12 diagInfo 
17 resnetv15_stage2__plus0 Add outputAdjNodes 1 18 inputAdjNodes 2 16 15 diagInfo 
18 resnetv15_stage2_activation0 Relu outputAdjNodes 2 19 22 inputAdjNodes 1 17 diagInfo 
19 resnetv15_stage2_conv3_fwd Conv outputAdjNodes 1 20 inputAdjNodes 1 18 diagInfo 
20 resnetv15_stage2_relu1_fwd Relu outputAdjNodes 1 21 inputAdjNodes 1 19 diagInfo 
21 resnetv15_stage2_conv4_fwd Conv outputAdjNodes 1 22 inputAdjNodes 1 20 diagInfo 
22 resnetv15_stage2__plus1 Add outputAdjNodes 1 23 inputAdjNodes 2 18 21 diagInfo 
23 resnetv15_stage2_activation1 Relu outputAdjNodes 2 24 27 inputAdjNodes 1 22 diagInfo 
24 resnetv15_stage3_conv0_fwd Conv outputAdjNodes 1 25 inputAdjNodes 1 23 diagInfo 
25 resnetv15_stage3_relu0_fwd Relu outputAdjNodes 1 26 inputAdjNodes 1 24 diagInfo 
26 resnetv15_stage3_conv1_fwd Conv outputAdjNodes 1 28 inputAdjNodes 1 25 diagInfo 
27 resnetv15_stage3_conv2_fwd Conv outputAdjNodes 1 28 inputAdjNodes 1 23 diagInfo 
28 resnetv15_stage3__plus0 Add outputAdjNodes 1 29 inputAdjNodes 2 27 26 diagInfo 
29 resnetv15_stage3_activation0 Relu outputAdjNodes 2 30 33 inputAdjNodes 1 28 diagInfo 
30 resnetv15_stage3_conv3_fwd Conv outputAdjNodes 1 31 inputAdjNodes 1 29 diagInfo 
31 resnetv15_stage3_relu1_fwd Relu outputAdjNodes 1 32 inputAdjNodes 1 30 diagInfo 
32 resnetv15_stage3_conv4_fwd Conv outputAdjNodes 1 33 inputAdjNodes 1 31 diagInfo 
33 resnetv15_stage3__plus1 Add outputAdjNodes 1 34 inputAdjNodes 2 29 32 diagInfo 
34 resnetv15_stage3_activation1 Relu outputAdjNodes 2 35 38 inputAdjNodes 1 33 diagInfo 
35 resnetv15_stage4_conv0_fwd Conv outputAdjNodes 1 36 inputAdjNodes 1 34 diagInfo 
36 resnetv15_stage4_relu0_fwd Relu outputAdjNodes 1 37 inputAdjNodes 1 35 diagInfo 
37 resnetv15_stage4_conv1_fwd Conv outputAdjNodes 1 39 inputAdjNodes 1 36 diagInfo 
38 resnetv15_stage4_conv2_fwd Conv outputAdjNodes 1 39 inputAdjNodes 1 34 diagInfo 
39 resnetv15_stage4__plus0 Add outputAdjNodes 1 40 inputAdjNodes 2 38 37 diagInfo 
40 resnetv15_stage4_activation0 Relu outputAdjNodes 2 41 44 inputAdjNodes 1 39 diagInfo 
41 resnetv15_stage4_conv3_fwd Conv outputAdjNodes 1 42 inputAdjNodes 1 40 diagInfo 
42 resnetv15_stage4_relu1_fwd Relu outputAdjNodes 1 43 inputAdjNodes 1 41 diagInfo 
43 resnetv15_stage4_conv4_fwd Conv outputAdjNodes 1 44 inputAdjNodes 1 42 diagInfo 
44 resnetv15_stage4__plus1 Add outputAdjNodes 1 45 inputAdjNodes 2 40 43 diagInfo 
45 resnetv15_stage4_activation1 Relu outputAdjNodes 1 46 inputAdjNodes 1 44 diagInfo 
46 resnetv15_pool1_fwd GlobalAveragePool outputAdjNodes 1 47 inputAdjNodes 1 45 diagInfo 
47 flatten_170 Flatten outputAdjNodes 1 48 inputAdjNodes 1 46 diagInfo 
48 resnetv15_dense0_fwd Gemm outputAdjNodes 0 resnetv15_dense0_fwd inputAdjNodes 1 47 diagInfo 
