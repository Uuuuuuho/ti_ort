import os
import sys
import queue
import argparse
import threading
import subprocess
import time
import json

parser = argparse.ArgumentParser()
parser.add_argument('--batch-mode', action='store_true')
parser.add_argument('--log-oneshot', action='store_true')
parser.add_argument('-j', '--jobs', nargs = '?', const = os.cpu_count(), default = 1, type=int)
parser.add_argument('-v', action = 'count', default = 0)
parser.add_argument('--args', nargs = argparse.REMAINDER, default = [])
parser.add_argument('--tb_tool')
args = parser.parse_args()

if sys.platform == 'linux':
    if os.path.exists(os.path.join(os.getenv('HOME'), 'time_data.json')):
        with open(os.path.join(os.getenv('HOME'), 'time_data.json')) as fp:
            time_data = json.load(fp)
    else:
        time_data = {}
else :
    time_data = {}

if not args.batch_mode:
    from termcolor import colored
    if sys.platform == 'win32':
        import colorama
        colorama.init()
else:
    def colored(msg, color):
        return msg


def msg(level, message):
    if not args.v < level:
        print(message, file=sys.stderr)

tb_tool_args = []
if args.tb_tool is not None:
    tb_tool = args.tb_tool.split()[0]
    tb_tool_args = args.tb_tool.split()[1:]
elif sys.platform == 'win32':
    tb_tool = 'PC_dsp_test_dl_algo.out.exe'
elif sys.platform == 'linux':
    tb_tool = './PC_dsp_test_dl_algo.out'
else:
    msg(0, 'Unrecognised system: %s' % sys.platform)
    sys.exit(1)

list_file = 'testvecs/config/config_list.txt'

console_path = 'consoles'

if os.path.exists(console_path):
    if not os.path.isdir(console_path):
        msg(0, 'Console path %s exists and not a directory' % console_path)
else:
    os.makedirs(console_path)

configs = []
config_args = {}

with open(list_file, 'r') as batch:
    for line in batch.readlines():
        if not line.startswith('2'):
            if line.startswith('0'):
                break
            lsplit = line.rstrip().split()
            config  = lsplit[1]
            c_args = lsplit[2:]

            configs.append(config)
            config_args[config] = c_args

colors = ['blue', 'yellow', 'cyan', 'magenta']

msg(1, 'Running inference on %d models' % len(configs))

ncpus = args.jobs
if ncpus > os.cpu_count():
    ncpus = os.cpu_count()
sem = threading.Semaphore(0)
idx = 0
nthreads = 0
run_count = 0

pq = queue.Queue()

cq = queue.Queue()
for c in range(ncpus):
    cq.put(colors[c%len(colors)])

def run_one(config_name):
    command = [tb_tool]
    command.extend(tb_tool_args)
    command.extend(['s:' + config_name])
    command.extend(args.args)
    command.extend(config_args[config_name])

    rtype = 'ref'
    for i, arg in enumerate(command):
        if arg == '--flowCtrl':
            if i + 1 >= len(arg):
                rtype = 'unknown'
            elif command[i + 1] == '0':
                rtype = 'ci'
            elif command[i + 1] == '12':
                rtype = 'natc'
            elif command[i + 1] == '1':
                rtype = 'ref'
            else:
                rtype = 'unknown'

    color = cq.get(False)

    record = []

    with open(os.path.join(console_path, os.path.splitext(os.path.basename(config_name))[0] + '.console'), 'wb') as fp:
        start = time.perf_counter()
        r = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
        while True:
            outl = r.stdout.readline()
            if outl == b'':
                break
            record.append(outl.decode('utf-8'))
            shortname = ' ' * 40 + config_name
            shortname = shortname[-40:]
            if not shortname.strip() == config_name:
                shortname = '...' + shortname[3:]
            if not args.log_oneshot:
                msg(2, colored('[%s]: %s' % (shortname, outl.decode('utf-8').rstrip()) , color))
            fp.write(outl)

        r.wait()
        end = time.perf_counter()
        cq.put(color)
        pq.put((config_name, r, record, rtype, int(end - start)))
        sem.release()

def spawn_one(configs, idx, nthreads):
    th = threading.Thread(target=run_one, args=(configs[idx],))
    th.start()
    return idx + 1, nthreads + 1

for t in range(min(len(configs), ncpus)):
    idx, nthreads = spawn_one(configs, idx, nthreads)

error = False

def join_one(nthreads):
    global run_count
    sem.acquire()
    elem = pq.get(False)

    key = 'infer_%s_%s' % (elem[3], os.path.splitext(os.path.basename(elem[0]))[0]);
    if time_data is not None:
        if key in time_data.keys() and isinstance(time_data[key], list):
            time_data[key].append(elem[4])
            time_data[key] = time_data[key][-100:]
        else:
            time_data[key] = [elem[4]]

    msg(1, '[%03d %s]: ' % (run_count, elem[0]) + (colored('Done', 'green') if elem[1].returncode == 0 else colored('Failed', 'red')))
    if args.log_oneshot:
        for line in elem[2]:
            msg(2, line.rstrip())

    if not elem[1].returncode == 0:
        error = True
    run_count = run_count + 1
    return nthreads - 1

while idx < len(configs):
    nthreads = join_one(nthreads)
    idx, nthreads = spawn_one(configs, idx, nthreads)

for n in range(nthreads):
    nthreads = join_one(nthreads)

if sys.platform == 'linux':
    with open(os.path.join(os.getenv('HOME'), 'time_data.json'), 'w') as fp:
        json.dump(time_data, fp, indent=4)

if error:
    sys.exit(1)

sys.exit(0)
