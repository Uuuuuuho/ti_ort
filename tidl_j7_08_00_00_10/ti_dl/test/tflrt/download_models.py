import os
import requests

models_base_path = '../testvecs/models/public/tflite/'

models = {
    'mobilenet_v1_1.0_224.tflite' : 'https://tfhub.dev/tensorflow/lite-model/mobilenet_v1_1.0_224/1/default/1?lite-format=tflite',
    'deeplabv3_mnv2_ade20k_float.tflite' : 'https://github.com/mlcommons/mobile_models/blob/main/v0_7/tflite/deeplabv3_mnv2_ade20k_float.tflite?raw=true',
    'ssd_mobilenet_v2_300_float.tflite' : 'https://github.com/mlcommons/mobile_models/blob/main/v0_7/tflite/ssd_mobilenet_v2_300_float.tflite?raw=true'
}


for model_name in models:
    model_path = models_base_path + model_name
    if(not os.path.isfile(model_path)):
        print("Downloading  ", model_name)
        url = models[model_name]
        r = requests.get(url, allow_redirects=True)
        open(model_path, 'wb').write(r.content)
