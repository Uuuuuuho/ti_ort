#!/bin/bash

if [ $# -eq "0" ]
then
install_pip=1
download_models=1
else
install_pip=$1
download_models=$2
fi

if [ -z ${TIDL_BASE_PATH+x} ]; then cd ../../../  && export TIDL_BASE_PATH=$(pwd) && cd ti_dl/test/tflrt && echo "Setting TIDL_BASE_PATH - '$TIDL_BASE_PATH'" ;  fi

#Install required python packages
if [ $install_pip -eq "1" ]
then
echo "Installing required python packages"
echo "Installing pillow"
pip3 install --upgrade pillow
echo "Installing tflite runtime"
pip3 install --upgrade --force-reinstall  tflite_runtime-2.4.0-py3-none-any.whl
echo "Installing numpy"
pip3 install --upgrade numpy
echo "Installing requests"
pip3 install --upgrade requests
echo "Installing flatbuffers"
pip3 install --upgrade flatbuffers==1.12.0
fi

#set LD_LIBRARY_PATHS
tidl_tools_path=$TIDL_BASE_PATH

import_path=":${tidl_tools_path}/ti_dl/utils/tidlModelImport/out"
rt_path=":${tidl_tools_path}/ti_dl/rt/out/PC/x86_64/LINUX/release"
tfl_delegate_path=":${tidl_tools_path}/ti_dl/tfl_delegate/out/PC/x86_64/LINUX/release"
onnx_ep_path=":${tidl_tools_path}/ti_dl/onnxrt_EP/out/PC/x86_64/LINUX/release"

echo "Setting LD_LIBRARY_PATH"
unset LD_LIBRARY_PATH
ld_library_path="${LD_LIBRARY_PATH}${import_path}${rt_path}${tfl_delegate_path}${onnx_ep_path}"
export LD_LIBRARY_PATH=$ld_library_path

if [ $download_models -eq "1" ]
then

mkdir -p '../testvecs/models/public/tflite/' 

echo "Downloading models ..."
# Call download models script
cd $TIDL_BASE_PATH/ti_dl/test/tflrt/ > /dev/null
python3 download_models.py
cd -   > /dev/null

echo "Models download done"
fi


cd $TIDL_BASE_PATH/ti_dl/rt/out/PC/x86_64/LINUX/release
rm -f libvx_tidl_rt.so
ln -s libvx_tidl_rt.so.1.0 libvx_tidl_rt.so
cd -   > /dev/null

#checking all the tidl_tools are available
echo "Checking TIDL tools .... "
if [ ! -e $TIDL_BASE_PATH/tidl_tools/device_config.cfg ]; then echo "ERROR : device_config.cfg not found in tidl_tools ... Exiting" && return; fi 
if [ ! -e $TIDL_BASE_PATH/tidl_tools/PC_dsp_test_dl_algo.out ]; then echo "ERROR : PC_dsp_test_dl_algo.out not found in tidl_tools ... Exiting" && return; fi 
if [ ! -e $TIDL_BASE_PATH/tidl_tools/ti_cnnperfsim.out ]; then echo "ERROR : ti_cnnperfsim.out not found in tidl_tools ... Exiting" && return; fi 

echo "TIDL tools ok !"

echo "Model environment set successfully"
