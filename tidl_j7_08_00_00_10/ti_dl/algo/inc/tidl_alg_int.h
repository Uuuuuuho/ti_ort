/*
*
* Copyright (c) {2015 - 2020} Texas Instruments Incorporated
*
* All rights reserved not granted herein.
*
* Limited License.
*
* Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
* license under copyrights and patents it now or hereafter owns or controls to make,
* have made, use, import, offer to sell and sell ("Utilize") this software subject to the
* terms herein.  With respect to the foregoing patent license, such license is granted
* solely to the extent that any such patent is necessary to Utilize the software alone.
* The patent license shall not apply to any combinations which include this software,
* other than combinations with devices manufactured by or for TI ("TI Devices").
* No hardware patent is licensed hereunder.
*
* Redistributions must preserve existing copyright notices and reproduce this license
* (including the above copyright notice and the disclaimer and (if applicable) source
* code license limitations below) in the documentation and/or other materials provided
* with the distribution
*
* Redistribution and use in binary form, without modification, are permitted provided
* that the following conditions are met:
*
* *       No reverse engineering, decompilation, or disassembly of this software is
* permitted with respect to any software provided in binary form.
*
* *       any redistribution and use are licensed by TI for use only with TI Devices.
*
* *       Nothing shall obligate TI to provide you with source code for the software
* licensed and provided to you in object code.
*
* If software source code is provided to you, modification and redistribution of the
* source code are permitted provided that the following conditions are met:
*
* *       any redistribution and use of the source code, including any resulting derivative
* works, are licensed by TI for use only with TI Devices.
*
* *       any redistribution and use of any object code compiled from the source code
* and any resulting derivative works, are licensed by TI for use only with TI Devices.
*
* Neither the name of Texas Instruments Incorporated nor the names of its suppliers
*
* may be used to endorse or promote products derived from this software without
* specific prior written permission.
*
* DISCLAIMER.
*
* THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
* OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
* OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
* IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
* BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
* OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
* OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
* OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/**
 ----------------------------------------------------------------------------
 @file    tidl_alg_int.h
 @brief   This file defines process function and interface structures for
          convolution layer also some definitions and enums used in other
          layers.
 @version 0.1 (Oct 2016) : Initial version [ADK]
 @version 0.2 (Feb 2017) : Cleaned up [EPR]
 ----------------------------------------------------------------------------
*/

#ifndef ITIDL_ALG_INT_H
#define ITIDL_ALG_INT_H

#pragma CHECK_MISRA ("none")
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <stdint.h>
#include "itidl_ti.h"
#include "tidl_dataflow.h"
#include "tidl_deviceInfo.h"

#include <stdarg.h>
#include "mmalib_cnn.h"
#include "perfsim.h"
#include "tidl_generic_datatypes.h"


#include "ti/drv/udma/dmautils/dmautils.h"

#ifdef HOST_EMULATION
#define CSPRAGMA(f,s) //
#define DSPRAGMA(f,s) //
#else
#define PRAGMA(x) _Pragma(#x)
#ifdef __cplusplus
#define CSPRAGMA(f,s) PRAGMA(CODE_SECTION(s))
#define DSPRAGMA(f,s) PRAGMA(DATA_SECTION(s))
#else
#define CSPRAGMA(f,s) PRAGMA(CODE_SECTION(f, s))
#define DSPRAGMA(f,s) PRAGMA(DATA_SECTION(f, s))
#endif
#endif

#define TIDL_ALIGN_CEIL(VAL, ALIGN) ((((VAL) + (ALIGN) - 1)/(ALIGN)) * (ALIGN) )
#define TIDL_ALIGN_CLIP(VAL, ALIGN) (((VAL)/(ALIGN)) * (ALIGN) )
#define TIDL_CEIL_DIV(NUM, DENOM) (((NUM) + (DENOM) - 1)/(DENOM))
#ifndef MMALIB_MMA_SIZE_8_BIT
#define MMALIB_MMA_SIZE_8_BIT (64)
#endif

#define MMA_SIZE_8_BIT_POWER_2 (6)
#define TIDL_MIN_ROWFLOW_PROCSIZE (MMALIB_MMA_SIZE_8_BIT << 1U)
#define TIDL_MMA_PANEL_SIZE (MMALIB_MMA_SIZE_8_BIT)
/* Maximum number of bias split */
#define TIDL_MAX_NUM_BIAS (1U)

#define TIDL_MSMC_NUM_VIRTUAL_BANKS ( 16U)
#define TIDL_MSMC_VIRTUAL_BANK_SIZE (64U)

#define TIDL_MSMC_NUM_PHY_BANKS ( 4U)
#define TIDL_MSMC_PHY_BANK_SIZE (256U)

#define TIDL_MSMC_BANK_PITCH (TIDL_MSMC_NUM_PHY_BANKS * TIDL_MSMC_PHY_BANK_SIZE)

#define TIDL_L2_NUM_VIRTUAL_BANKS (8U)
#define TIDL_L2_VIRTUAL_BANK_SIZE (64U)

#define TIDL_L2_NUM_PHY_BANKS ( 4U)
#define TIDL_L2_PHY_BANK_SIZE (128U)

#define TIDL_L2_BANK_PITCH (TIDL_L2_NUM_PHY_BANKS * TIDL_L2_PHY_BANK_SIZE)

#define TIDL_MSMC_CACHE_LINE_SIZE (128U)
#define TIDL_L2_CACHE_LINE_SIZE (128U)

#define TIDL_WT_STAGING_MEM ((1024U * 1025U) + (128U * 1024U))

/* This is the worst case memory required in L2 for DMA/compute of the last block */
#define TIDL_CONV2D_EXTRA_L2MEM_FOR_FEAT_REQ  (256U * 1024U)


#define NUM_BMA_PBLOCK        (512)
#define NUM_COEFFS_PER_LOOP   (4U)

#define ALIGN_SIZE(x,y)       ((((x) + ((y)-1)) / (y)) * (y))
#define MAX_IMAGE_PITCH       (1280)
#define MAX_IMAGE_HEIGHT      (720)
#define MAX_NUM_CONV_GROUPS   (1024)

#define INBUF_INT_MEM_SIZE    (1024U*16U)
#define ACC_INT_MEM_SIZE      (1024*32)
#define DENSE_COEFF_MEM_SIZE  (1024*12)
#define OUTBUF_INT_MEM_SIZE   (INBUF_INT_MEM_SIZE)
#define TIDL_BLOCK_WIDTH			(32U)
#define TIDL_BLOCK_HEIGHT			(32U)

#ifdef CORE_DSP
#define MAX_NUM_PROC_IN_CH      (1024U)
#define TIDL_CONV2D_NUM_AUTODMA (2)
#define NUM_MAX_SCATTER_GATHER_TRANSFERS (32)
#define TIDL_MAX_BLOCK_WIDTH      (128)
#define TIDL_ENABLE_MULTIPLE_COPY
#define TIDL_ALIGN_BUFFER_CONV_KERNEL
#define MAX_NUM_LOCAL_OUT_CH     (32U)
#define TIDL_NUM_LOCAL_IN_CHS    (4)
#define TIDL_PROC_BLOCK_WIDTH    (128U)
#define TIDL_PROC_BLOCK_HEIGHT   (8U)
#define TIDL_MIN_PROC_BLOCK_HEIGHT    (4U)
#define NUM_COFFES_INT_MEM       ((uint32_t)((uint32_t)1U << 11U))
#define NUM_COFFES_INT_MEM_MASK  ((uint32_t)((uint32_t)1U << 12U)-1U)
#define NUM_OUT_CH_INT_MEM       ((uint32_t)((uint32_t)1U << 8))
#define NUM_OUT_CH_INT_MEM_MASK  ((uint32_t)((uint32_t)1U << 9U)-1U)

#else
#define MAX_NUM_PROC_IN_CH        (1024U)
#define TIDL_CONV2D_NUM_AUTODMA   (1)
#define TIDL_MAX_BLOCK_WIDTH      (32)
#define MAX_NUM_LOCAL_OUT_CH      (32U)
#define TIDL_NUM_LOCAL_IN_CHS     (4)
#define TIDL_PROC_BLOCK_WIDTH     (32U)
#define TIDL_PROC_BLOCK_HEIGHT    (32U)
#define TIDL_MIN_PROC_BLOCK_HEIGHT    (16U)
#define NUM_MAX_SCATTER_GATHER_TRANSFERS (32U)
#define NUM_COFFES_INT_MEM        (1UL << 10UL)
#define NUM_COFFES_INT_MEM_MASK   ((1UL << 11UL)-1UL)
#define NUM_OUT_CH_INT_MEM        (1UL << 8UL)
#define NUM_OUT_CH_INT_MEM_MASK   ((1UL << 9UL)-1UL)
#endif

#define MIN_INPUT_BLOCK_WIDTH     (1)
#define MIN_INPUT_BLOCK_HEIGHT    (1)
#define MIN_OUTPUT_BLOCK_WIDTH    (1)
#define MIN_OUTPUT_BLOCK_HEIGHT   (1)
#define DEF_INPUT_BLOCK_WIDTH     (1)
#define DEF_INPUT_BLOCK_HEIGHT    (1)
#define DEF_OUTPUT_BLOCK_WIDTH    (1)
#define DEF_OUTPUT_BLOCK_HEIGHT   (1)

#define DEF_POOL_WIDTH            (2)
#define DEF_POOL_HEIGHT           (2)
#define DEF_POOL_STRIDE_WIDTH     (2)
#define DEF_POOL_STRIDE_HEIGHT    (2)

#define CONV_KER_MIN_WIDTH        (1)
#define CONV_KER_MAX_WIDTH        (9)
#define CONV_KER_MIN_HEIGHT       (1)
#define CONV_KER_MAX_HEIGHT       (9)
#define CONV_STRIDE_MIN_WIDTH     (1)
#define CONV_STRIDE_MAX_WIDTH     (4)
#define CONV_STRIDE_MIN_HEIGHT    (1)
#define CONV_STRIDE_MAX_HEIGHT    (4)

#define DECONV_STRIDE_MIN_WIDTH   (2)
#define DECONV_STRIDE_MAX_WIDTH   (2)
#define DECONV_STRIDE_MIN_HEIGHT  (2)
#define DECONV_STRIDE_MAX_HEIGHT  (2)
#define DECONV_KER_MIN_WIDTH      (2)
#define DECONV_KER_MAX_WIDTH      (4)
#define DECONV_KER_MIN_HEIGHT     (2)
#define DECONV_KER_MAX_HEIGHT     (4)


#define TIDL_SAT_LO_UINT8              (0)
#define TIDL_SAT_HI_UINT8              (255)
#define TIDL_SAT_LO_INT8               (-128)
#define TIDL_SAT_HI_INT8               (127)
#define TIDL_SAT_LO_UINT16             (0)
#define TIDL_SAT_HI_UINT16             (65535)
#define TIDL_SAT_LO_INT16              (-32768)
#define TIDL_SAT_HI_INT16              (32767)
#define TIDL_SAT_LO_INT32              (0x80000000)
#define TIDL_SAT_HI_INT32              (0x7FFFFFFF)

#define TIDL_DEFAULT_L1_MEM_SIZE       (16 * 1024)
#define TIDL_DEFAULT_L2_MEM_SIZE       (L2MEM_SCRATCH_BUF_SIZEKB * 1024)
#define TIDL_DEFAULT_L3_MEM_SIZE       (L3MEM_SCRATCH_BUF_SIZEKB * 1024)

/* Define TRUE/FALSE to go with Bool */
#ifndef TRUE

#define TRUE		( 1)
#define FALSE		( 0)

#endif

/**
 *  \anchor TIDL_KernelType
 *  \name   TIDL Kernel Type
 *
 *  This group defines the different types of kernel
 *
 *  @{
 */
#define TIDL_NonLinearKernel     ((int32_t) 0)
#define TIDL_LinearKernel        ((int32_t) 1)
/* @} */

#if defined(HOST_EMULATION)
#define ENABLE_TRACE_PROFILE      (0)
/* To enable the natural C-code for the optimized C66x kernels */
#define ENABLE_CN_CODE            (0)
#else
#if defined(LOKI_BUILD)
#define ENABLE_TRACE_PROFILE      (0)
#define ENABLE_CN_CODE            (0)
#else
#define ENABLE_TRACE_PROFILE      (0)
#define ENABLE_CN_CODE            (0)
#endif

#endif


#define QUANT_DIVISOR_Q (16)
#define IP_QUANT_DIVISOR_Q (16)
#define PRELU_SLOPE_Q (8)

#if (ENABLE_TRACE_PROFILE) && defined(CORE_DSP) && !defined(HOST_EMULATION)
int64_t _TSC_read(void);
#endif

#if (ENABLE_TRACE_PROFILE) && defined(CORE_DSP) && defined(HOST_EMULATION)
#define _TSC_read() (0)
#endif

/**
@struct BBox
@brief  This structure defines the BBox

@param  xmin
indicates the min value in x direction
@param  ymin
indicates the min value in y direction
@param  xmax
indicates the max value in x direction
@param  ymax
indicates the max value in y direction
@param  size
indicates the size of the BBox
*/
typedef struct
{
  float32_tidl xmin;
  float32_tidl ymin;
  float32_tidl xmax;
  float32_tidl ymax;
}BBox;

/**
 @struct  tidlConv2dBuffParams_t

 @brief   This structure contains parameters used for transferring input data
          from external memory to internal memory for computation and for
          transferring output data from internal memory to external memory after
          computation. It is used for block level operations.

 @params  inWidth
          Width of input data in external memory.

 @params  inHeight
          Height of input data in external memory.

 @params  inPitch
          Pitch of input data in external memory.

 @params  outWidth
          Width of output data in external memory.

 @params  outHeight
          Height of output data in external memory.

 @params  outPitch
          Pitch of output data in external memory.

 @params  outPitchPooling
          Pitch of output data used for pooling in external memory.

 @params  inChPitch
          Channel Pitch of input data in external memory. It contains the size
          of one complete channel data in external memory.

 @params  outChPitch
          Channel Pitch of output data in external memory. It contains the size
          of one complete channel data in external memory.

 @params  outChPitchPooling
          Channel Pitch of output data for pooling in external memory.

 @params  inElementType
          Indicates the type of input as signed or unsigned.

 @params  outElementType
          Indicates the type of output as signed or unsigned.

 @params  firstRoundBits
          Round bits used for intermediate sum results in convolution
          operations.

 @params  secondRoundBits
          Round bits used for final output in convolution operations.

 @params  avgPoolingRoundBits
          Round bits used for final output in pooling operations.

 @param   quantizationStyle
          Value to indicate the quantizationStyle used for Convolution

 @param   biasQFact
          Value to indicate Q factor used for Bias values in Convolution

 @param  zeroWeightValue
          value of weights added for dynamic quantSytle

 @param  max
          max value across the all the convolution output values

 @param  min
          min value across the all the convolution output values

 @param  kernelType
          value indicates the type of kernel coeffs used either dense or sparse

 @param  quantFactor
          value indicates the quantization factor used in convolution

 @param  outElementSize
          value indicates the size of the convolution output element

*/
typedef struct {
  int32_t    inWidth;
  int32_t    inHeight;
  int32_t    inPitch;
  int32_t    outWidth;
  int32_t    outHeight;
  int32_t    outPitch;
  int32_t    outPitchPooling;
  int32_t    inChPitch;
  int32_t    outChPitch;
  int32_t    outChPitchPooling;
  uint32_t    inBatchPitch;
  uint32_t    outBatchPitch;
  int32_t     inElementType;
  int32_t     outElementType;
  int32_t     firstRoundBits;
  int32_t     secondRoundBits;
  int32_t     avgPoolingRoundBits;
  int32_t     quantizationStyle;
  int32_t     biasQFact;
  int32_t     zeroWeightValue;
  int32_t     max;
  int32_t     min;
  int32_t     kernelType;
  int32_t     actType;
  uint16_t    quantFactor;
  int32_t     numTotRoi;
  int32_t     scratchSize;
  void *      scratchMem;
  void *      outRoundBitsPtr;
  uint8_t*    memcpyTr;
  float       inScaleFactor;
}tidlConv2dBuffParams_t;

/**
 @struct  sTIDL_LayerBuf_t
 @brief   This structure define the variables used in finding free
           output buffer in TIDL
 @param  outDataSize
          New outDataSize after finding free output buffer for current layer
 @param  newInDataId
          Address pointing to the new input buffer after finding
          free output buffer
 @param  newOutDataId
          Address pointing to the new output buffer after finding
          free output buffer
*/
typedef struct {
  int32_t outDataSize[TIDL_NUM_MAX_LAYERS];
  int32_t newInDataId[TIDL_NUM_MAX_LAYERS][TIDL_NUM_IN_BUFS];
  int32_t newOutDataId[TIDL_NUM_MAX_LAYERS][TIDL_NUM_OUT_BUFS];
}sTIDL_LayerBuf_t;

static inline void TIDL_edmaCopy(void * dst, const void * src, uint16_t size);

/**
*  @enum   eMemrecs
*  @brief  Memory records for image pyra,id application
*/
typedef enum
{
  /* Memory records for handle */
  ALG_HANDLE_MEMREC,/*0*/
  ALG_CREATE_PARAM_MEMREC,/*1*/
  ALG_SCRATCH_L1_MEM_MEMREC,/*2*/
  ALG_SCRATCH_L2_MEM_MEMREC,/*3*/
  ALG_SCRATCH_L3_MEM_MEMREC,/*4*/
  ALG_LAYERS_PARAMS_BUFF_MEMREC,/*5*/
  ALG_SCRATCH_DATA_BUFF_MEMREC,/*6*/
  ALG_SCRATCH_DATA_BUFF_EXT_MEMREC,/*7*/
  ALG_REF_SCRATCH_BUFF_MEMREC,/*8*/
  ALG_REF_OUTPUT_BUFF_MEMREC,/*9*/
  ALG_ANALYZE_NETWORK_MEMREC,/*10*/
  ALG_LAYERS_MEMREC,/*11*/
  ALG_SCRATCH_DDR_MEM_MEMREC,/*12*/
  MAX_NUM_MEMRECS
} eMemrecs;

/**
*  @enum   eAlgState
*  @brief  State of application
*/
typedef enum
{
  ALG_NOT_ACTIVE,
  ALG_ACTIVE
} eAlgState;

/**
 *  \anchor TIDL_ProfilePoints
 *  \name   TIDL Profile Points
 *
 *  Profile points for TIDL
 *
 *  @{
 */
#define TIDL_PROFILE_LAYER                      ((int32_t) 0)
#define TIDL_PROFILE_KERNEL_ONLY                ((int32_t) 1)
#define TIDL_PROFILE_CORE_LOOP                  ((int32_t) 2)
#define TIDL_PROFILE_LAYER_SETUP                ((int32_t) 3)
#define TIDL_PROFILE_PREFETCH                   ((int32_t) 4)
#define TIDL_PROFILE_DMA_PIPEUP                 ((int32_t) 5)
#define TIDL_PROFILE_LAYER_WITHOUT_PADDING      ((int32_t) 6)
#define TIDL_PROFILE_LAYER_PADDING_TRIGGER      ((int32_t) 7)
#define TIDL_PROFILE_LAYER_PADDING_WAIT         ((int32_t) 8)
#define TIDL_PROFILE_KERNEL_COPY                ((int32_t) 9)
#define TIDL_PROFILE_LAYER_SETUP_HANDLE_COPY    ((int32_t) 10)
#define TIDL_PROFILE_LAYER_DEINIT               ((int32_t) 11)
#define TIDL_PROFILE_LAST_BLOCK_CYCLES          ((int32_t) 12)
#define TIDL_PROFILE_DMA_PIPEDOWN               ((int32_t) 13)
#define TIDL_PROFILE_BACKUP                     ((int32_t) 14)
#define TIDL_PROFILE_RESTORE                    ((int32_t) 15)
#define TIDL_PROFILE_MAX                        ((int32_t) 16)
/* @} */

/**
 *  \anchor TIDL_DmaChannels
 *  \name   TIDL DMA Channels
 *
 *  DMA Channels used for TIDL
 *
 *  @{
 */
#define TIDL_DMA_CHANNEL_DIRECT_TR_INFEATMAP      ((int32_t) 0)
#define TIDL_DMA_CHANNEL_DIRECT_TR_INFEATMAP_2    ((int32_t) 1)
#define TIDL_DMA_CHANNEL_DIRECT_TR_INFEATMAP_3    ((int32_t) 2)
#define TIDL_DMA_CHANNEL_DIRECT_TR_INFEATMAP_4    ((int32_t) 3)
#define TIDL_DMA_CHANNEL_DIRECT_TR_WTMAP          ((int32_t) 4)
#define TIDL_DMA_CHANNEL_DIRECT_TR_OUTFEATMAP     ((int32_t) 5)
#define TIDL_DMA_CHANNEL_DIRECT_TR_OUTFEATMAP_2   ((int32_t) 6)
#define TIDL_DMA_CHANNEL_DIRECT_TR_OUTFEATMAP_3   ((int32_t) 8)
#define TIDL_DMA_CHANNEL_DIRECT_TR_OUTFEATMAP_4   ((int32_t) 7)
#define TIDL_DMA_CHANNEL_DIRECT_TR_GENERIC_1   ((int32_t) 9)
#define TIDL_DMA_CHANNEL_DIRECT_TR_GENERIC_2   ((int32_t) 10)
#define TIDL_DMA_CHANNEL_WT_STAGING               ((int32_t) 11) /*Till this all are direct TR channels*/
#define TIDL_DMA_CHANNEL_ST                       ((int32_t) 12) /*Till this all are direct TR channels*/
#define TIDL_DMA_CHANNEL_MEMCPY                   ((int32_t) 13) /*Till this all are direct TR channels*/
#define TIDL_DMA_CHANNEL_MAX                      ((int32_t) 14)
/* @} */


/**
 *  \anchor TIDL_BuildType
 *  \name   TIDL Build Type
 *
 *  This group defines the different build types supported by TIDL
 *
 *  @{
 */
#define TIDL_BUILD_TYPE_RUN_REF_ONLY            (0x00000001U)
#define TIDL_BUILD_TYPE_NATC_MMALIB             (0x00000002U)
#define TIDL_BUILD_TYPE_NATC_DSP                (0x00000004U)
#define TIDL_BUILD_TYPE_REF_COMP                (0x00000008U)
#define TIDL_BUILD_TYPE_WITH_DMA                (0x00000020U)

#define TIDL_REF_CODE_FOR_IP                    (0x00000001U)
#define TIDL_REF_CODE_OD_SOFTMAX                (0x00000002U)
#define TIDL_REF_CODE_OD_DECODE_BOX             (0x00000004U)
#define TIDL_NATC_CODE_SIGNED_MAXPOOL           (0x00000008U)
/* @} */


#define TIDL_PADDING_TYPE_TOP                   (0)
#define TIDL_PADDING_TYPE_BOTTOM                (1)
#define TIDL_PADDING_TYPE_BOTH                  (2)
#define TIDL_PADDING_TYPE_TOP_LEFT              (3)
#define TIDL_PADDING_TYPE_ST_TOP_LEFT           (4)
#define TIDL_PADDING_TYPE_PAD_LAYER             (5)
#define TIDL_PADDING_TYPE_PAD_LAYER_BOTH        (6)
#define TIDL_PADDING_TYPE_PAD_LAYER_TOP         (7)
#define TIDL_PADDING_TYPE_PAD_LAYER_BOTTOM      (8)

#define  TIDL_MMA_HANDLE_MAX                    (3)
#define  TIDL_ST_HANDLE_MAX                     (3U)

#define TIDL_OBJ_DET_MAX_HEADS (16)
/**
@struct sTIDL_ALgDetectOutputParams_t
@brief  This structure defines the buffers used in the
Detecttion Output Layer

@param  all_loc_preds
Scratch buffer used to store predictions of
prior boxes
@param  all_decode_bboxes
Buffer to store all the decoded Bboxs
@param  all_decode_kps
Buffer to store all the decoded Key points
@param  all_conf_scores
Buffer to store the scores and indexs after
threshold calculation for all the classes
@param  all_conf_scores
Buffer to store the scores and indexs after
threshold calculation for all the classes
@param  indices
Buffer to store the scores and indexs after
NMS and overlap for each classes
@param  score
Buffer to store the sorting scores and indexs
for all classes to select top k objects
@param  index
Buffer to store size of each class or number of
objects in each class after NMS
@param  labels
Buffer to store labels for all classes
@param  scoreLut
Buffer to store Sigmoid Score
*/
typedef struct {

  void                     *inLocDataList[TIDL_OBJ_DET_MAX_HEADS];
  float32_tidl             inLocdataQList[TIDL_OBJ_DET_MAX_HEADS];
  void                     *inConfDataList[TIDL_OBJ_DET_MAX_HEADS];
  float32_tidl             inConfdataQList[TIDL_OBJ_DET_MAX_HEADS];
  int32_t                  locHeadPitchList[TIDL_OBJ_DET_MAX_HEADS][3];
  int32_t                  confHeadPitchList[TIDL_OBJ_DET_MAX_HEADS][3];
  float32_tidl             confHeadInvPitchList[TIDL_OBJ_DET_MAX_HEADS][3];
  int32_t                  locDataOffset[TIDL_OBJ_DET_MAX_HEADS];
  int32_t                  confDataOffset[TIDL_OBJ_DET_MAX_HEADS];
  int32_t                  numAnchors[TIDL_OBJ_DET_MAX_HEADS];

  /*Needed in valid point score caluclation API dependent on M. allocated in L2*/
  uint16_t * topMScore;
  uint16_t * topMScoreSorted; // needed in softmax only
  float    * tempScore; // number of elements equal to number of classes, needed in softmax only
  int32_t  * topMIndices;
  int32_t  * topMIndicesSorted;
  /*------------------------------------------*/ /*allocated in L1D*/
  int32_t  * countMList; //
  int32_t  * countMListAcc;
  int32_t  * topKIndices;
  void     * topKLoc;
  BBox     * topKBbox; // equal to topKLoc
  uint16_t * topKScore;
  int32_t  * nmsKeptIndices; // Needed in NMS depdent on K only
  /*--------------------------------------------*/
  /*temprorary buffer for breaking the software pipelined loop. Allocated in L3*/
  // buffer dependent on feature map resolution
  long long  * pred;
  void       * featMaxMinVal;
  /*-------------------------------------------*/
  /*Needed in valid point score caluclation API dependent on M when executing from DDR flow*/
  uint16_t * topMScoreDdr;
  uint16_t * topMScoreSortedDdr;
  int32_t  * topMIndicesDdr;
  int32_t  * topMIndicesSortedDdr;
  void     * scratchPtr;
  /*-------------------------------------------*/
  int32_t elementType;
  int32_t elmSize;
  int32_t topM;
  int32_t topMDdr;
  int32_t scratchDDRConsumed;
  int32_t topMAllClasses;
  int32_t isBckClsAvailable;
}sTIDL_ALgDetectOutputParams_t;

typedef struct {
  void * pCoeffs[TIDL_MMA_HANDLE_MAX];
  void * pCoeffStagePtr;
  void * tempOutPr;
  void * biasParamMem;
  /* This is applicable only if channelwise quantization is enabled */
  void * outRoundBitsPtr;
  /* This is applicable only if channelwise quantization is enabled */
  void * biasBPtr;
  void * mmaHandleArgsMem[TIDL_MMA_HANDLE_MAX];
  uint32_t dataFlow;
  sDataFlowInfo_t * dataFlowInfo;
  int32_t biasParamSize;
  int32_t mmaHandleArgsSize[TIDL_MMA_HANDLE_MAX];
  uint32_t totalNumCoeffs[MAX_NUM_CONV_GROUPS];
  uint32_t totalActualCoeffs[MAX_NUM_CONV_GROUPS];
  /* Size of processing */
  int32_t  procElemSize;
}sTIDL_ALgConvParams_t;

typedef struct {
  void * biasParamMem;
  void * wtTranformMem;
  void * accMem;
  int32_t wtTranformSize;
  int32_t biasParamSize;
  int32_t accMemSize;
}sTIDL_ALgInnerProductParams_t;

typedef struct {
  void* reserved;
}sTIDL_ALgReLULayer_t;

typedef struct {
  void* reserved;
}sTIDL_ALgArgMaxLayer_t;

typedef struct {
  int32_t isOptResizeAvail;
  int32_t scratchSize;
  int32_t isMmaCiAvailable;
  int32_t inNumBytes;
  __HWA_CONFIG_REG_v1 mma_config_reg;
  void* reserved;
}sTIDL_ALgResizeLayer_t;

typedef struct {
  void* reserved;
}sTIDL_ALgColorConversionParams_t;

/**<
 *******************************************************************************
  @struct sTIDL_ALgEltWiseParams_t
  @brief  This structure used to store Eltwise layer parameters to use in the Process call

  @param whichInputIsSigned  : To indicate which one input is signed out of 2 inputs
  @param buffInputBlockOffset: Offset of the second input buffer from first input address
  @param inputScale          : input scales of the two inputs
  @param isMmaCiAvailable    : To indicate in the process call that MMA kernel is available
  @param inNumBytes          : Input elementSize in bytes, this required in MMA HW init in Process call
  @param mma_config_reg      : MMA config register required in MMA HW init in Process call
  @param mma_offset_reg      : MMA offset register required in MMA HW init in Process call
 *******************************************************************************
*/
typedef struct {
  int32_t whichInputIsSigned;
  int64_t buffInputBlockOffset[TIDL_ST_HANDLE_MAX];
  uint16_t inputScale[TIDL_SIMD_WIDTH>>1];
  int32_t isMmaCiAvailable;
  int32_t inNumBytes;
  __HWA_CONFIG_REG_v1 mma_config_reg;
  __HWA_OFFSET_REG mma_offset_reg;
}sTIDL_ALgEltWiseParams_t;

typedef struct {
  int32_t predicateBufSize;
  uint32_t scaleMemSize[TIDL_ST_HANDLE_MAX];
  void *scalePtr[TIDL_ST_HANDLE_MAX];
  /** For average pooling, Q value of the weight used to replace division */
  int32_t internalPoolingWeightQ;
  int32_t nextOutOffset; /* Denotes offset where the kernel will write data during next call */
  int32_t startRowNumberInTensor; /*Denotes the offset in row number of Tensor */
}sTIDL_ALgPoolingParams_t;

typedef struct {
  int32_t outOffsetIncPerTensor;
  int32_t isMmaCiAvailable;
  __HWA_CONFIG_REG_v1 mma_config_reg;
  __HWA_OFFSET_REG mma_offset_reg;
}sTIDL_ALgConcatParams_t;

typedef struct {
  void *slopeFactMem;
  int32_t slopeFactSize;
  int32_t *slopeFact;
  void * biasParamMem;
  int32_t biasParamSize;
  int32_t biasB;
  int32_t isMmaCiAvailable;
  int32_t inNumBytes;
}sTIDL_ALgBatchNormParams_t;



/* CHECK_MISRA("-18.4")  -> Disable rule 18.4  */
typedef union {
  sTIDL_ALgConvParams_t              convParams;
  sTIDL_ALgEltWiseParams_t           eltWiseParams;
  sTIDL_ALgPoolingParams_t           poolParams;
  sTIDL_ALgArgMaxLayer_t             argMaxParams;
  sTIDL_ALgResizeLayer_t             resizeParams;
  sTIDL_ALgInnerProductParams_t      innerProductParams;
  sTIDL_ALgDetectOutputParams_t      detectionOutputParams;
  sTIDL_ALgConcatParams_t            concatParams;
  sTIDL_ALgBatchNormParams_t         batchNormParams;
  sTIDL_ALgColorConversionParams_t   colorConversionParams;
}sTIDL_AlgLayerParams_t;
/*RESET_MISRA("18.4")  -> Reset rule 18.4 */

typedef struct {
  int32_t totalOps;
  int32_t actualOps;
  uint64_t profilePoint[TIDL_PROFILE_MAX];
}sTIDL_commonLayerMetaData_t;

/* Generic Flow related structures */
/**
 *  @enum   eTensorType
 *  @brief  Enumeration for different tensor types
 *
 */
typedef enum
{
  /**< This value indicates tensor tpye as input data */
  TIDL_INPUT_TENSOR       = 0,
  /**< This value indicates tensor tpye as output data */
  TIDL_OUTPUT_TENSOR      = 1,
  /**< This value indicates tensor tpye as parameters(weights) */
  TIDL_PARAMETER_TENSOR   = 2,
  /**< This value indicates different tensor tpyes */
  TIDL_MAX_TENSOR_TYPES   = 3
} eTensorType ;

/**<
 *******************************************************************************
  @struct TIDL_TensorTransferInfo
  @brief  This structure used to store tensor parameters for each tesnor and
             to use them in the process call for configuration of DMA

  @param tensorType        : Different tensor types supported, refer to eTensorType
  @param transferSize      : Size of data to be transfered,
                             In case of input/params it is from DDR/MSMC to L2
                             In case of output it is from MSMC to DDR
  @param elementSizeinBytes: Size of elements of the tensor in Bytes,
  @param dmaNumChs         : Number of DMA channels required to transfer each tensor data
  @param dmaStartChIndex   : Indicates the DMA start channel Index for each tensor
  @param perChTrSize       : Size of data to be transfered to L2 for each channel
  @param lastChTrSize      : Size of data to be transfered to L2 for last channel
  @param srcChOffset       : OffsetInBytes in the src buffer for the next DMA channel
  @param chPitchL3InBytes  : chPitch in src for input read and in dst for output write,
                             In case of IN: srcPitch : Pitch of buffer in source being in MSMC/DDR
                             In case of OUT: If final destination is MSMC then this pitch is not used
                             In case of OUT: If final destination is DDR then pitch of the buffer in DDR
 *******************************************************************************
*/
typedef struct
{
  eTensorType tensorType;
  int32_t transferSize;
  int32_t elementSizeinBytes;
  int32_t dmaNumChs;
  int32_t dmaStartChIndex;
  int32_t perChTrSize;
  int32_t lastChTrSize;
  int32_t srcChOffset;
  int32_t chPitchL3InBytes;
}TIDL_TensorTransferInfo;

/**<
 *******************************************************************************
  @struct localDataFlowInfo_t
  @brief  This structure holds the copy of DataFlowInfo elements those
          are required in the Process time, and this will be in internal memory

  @param Nci                 : Number of input channels being processed in chunk
  @param numSplit            : Number of splits in the input feature map,
                               If (numSplit == 1) then it is Small Feature Map
                               If (numSplit > 1) then it is Large Feature Map
  @param dmaFreq             : Frequency of DMA invocation
  @param kernelFreq          : Frequency of kernel invocation
  @param activeBufWidth      : Width of the buffer in internal memory for IN dma transfer
  @param isInTensorDmaReq    : Flag to indicate if DMA is required for input
  @param memSpaceIN          : Buffer space of the internal memory for input DMA transfer,
                               this is from DataFlowInfo and used to get dst DMA address for IN dma
  @param addrOffsetIN        : Buffer offset from Base of the internal memory from DataFlowInfo
                               this is from DataFlowInfo and used to get dst DMA address for IN dma
  @param memSpaceOUT         : Buffer space of the internal memory for output DMA transfer
                               this is from DataFlowInfo and used to get src DMA address for OUT dma
  @param addrOffsetOUT       : Buffer offset from base in the internal memory for output DMA transfer
                               this is from DataFlowInfo and used to get src DMA address for OUT dma
  @param accessOffsetRead    : Initial offset for Input data, this is accessOffset from DataFlowInfo
  @param accessOffsetWrite   : Initial offset for Ouptut data, this is accessOffset from DataFlowInfo
 *******************************************************************************
*/
typedef struct
{
  int32_t Nci;
  int32_t numSplit;
  int32_t dmaFreq;
  int32_t kernelFreq;
  int32_t activeBufWidth;
  int32_t isInTensorDmaReq;
  int32_t memSpaceIN;
  int32_t addrOffsetIN;
  int32_t memSpaceOUT ;
  int32_t addrOffsetOUT;
  int32_t accessOffsetRead;
  int32_t accessOffsetWrite;
} localDataFlowInfo_t ;

/* Each tensor is DMAed with different DMA channel so the TIDL_MAX_IN_TENSORS
   is limited by Max number of DMA channels available for generic flow */
#define TIDL_MAX_IN_TENSORS      (8U)
#define TIDL_MAX_OUT_TENSORS     (1U)
#define TIDL_MAX_PARAM_BUFFERS   (1U)
#define TIDL_TOTAL_ALL_TENSORS   (TIDL_MAX_IN_TENSORS + TIDL_MAX_OUT_TENSORS + TIDL_MAX_PARAM_BUFFERS)
/* 64 is DmaUtilsAutoInc3d_getTrMemReq(1) */
#define DMAUTILS_AUTOINC_3D_TR_MEM_SIZE (64)

/**<
 *******************************************************************************
  @struct TIDL_GenericHandle
  @brief  This structure is the main handle for GenericFlow process call
  @param numberTensors      : Total number of tensors requires generic data flow,
                              Multiple input and parameter tensors and single
                              output Tensor is supported
  @param disableMSMCstaging : Used indicate for the GenericFlow that MSMC staging is disabled
  @param inBlkProcessingJump: Processing jump used for ping/pong buffers at blk boundary on the Input side
  @param inChProcessingJump : Processing jump used for ping/pong buffers at channel boundary on the Input side
  @param outBlkProcessingJump: Processing jump used for ping/pong buffers at blk boundary on the Output side
  @param outChProcessingJump : Processing jump used for ping/pong buffers at channel boundary on the Output side
  @param isCPUcopyNeeded     : Flag indicates if someBytes to be filled as zeros in the L2 before DMA transfer
                               This is required when procsize > activeBufWidthDst from DataFlowInfo
  @param inDMAChList       : List holds the channel ID's of DMA for input tensors
  @param outDMAChList      : List holds the channel ID's of DMA for output tensor
  @param paramDMAChList    : List holds the channel ID's of DMA for weight tensors
  @param localDataFlowInfo : Holds the copy of DataFlowInfo elements those are required in the Process time
  @param tensorTransferInfo : Holds the tensor level info for all tensors, refer to TIDL_TensorTransferInfo
  @param trMemTr            : Memory used to store trasfer parameters to prepare a TR
  @param execInArgs      : Used to send process level parameters to the kernel
  @param execOutArgs     : Used to read process level parameters from the kernel
  @param functionPtr       : Function pointer for the layer execution function
 *******************************************************************************
*/
typedef struct
{
  int32_t numberTensors;
  int32_t disableMSMCstaging;
  int32_t inBlkProcessingJump;
  int32_t inChProcessingJump;
  int32_t outBlkProcessingJump;
  int32_t outChProcessingJump;
  int32_t outBatchProcessingJump;
  int32_t numSplitsPerBatch;
  int32_t isCPUcopyNeeded;
  int32_t mixPrecisionWithSignedInput;
  int32_t mixPrecisionEnabledForOutput;
  int32_t inDMAChList[TIDL_DMA_CHANNEL_MAX];
  int32_t outDMAChList[TIDL_DMA_CHANNEL_MAX];
  int32_t paramDMAChList[TIDL_DMA_CHANNEL_MAX];
  localDataFlowInfo_t localDataFlowInfo;
  TIDL_TensorTransferInfo tensorTransferInfo[TIDL_TOTAL_ALL_TENSORS];
  uint8_t trMemTr[TIDL_DMA_CHANNEL_MAX][DMAUTILS_AUTOINC_3D_TR_MEM_SIZE];
  TIDL_ExecInArgs execInArgs;
  TIDL_ExecOutArgs execOutArgs;
  TIDL_genericExecFuncPtr functionPtr;
  TIDL_genericUpdateInArgsPtr updateFunctionPtr;
}TIDL_GenericHandle;

typedef struct {
  sTIDL_AlgLayerParams_t layerParams;
  sTIDL_commonLayerMetaData_t metaData;
  int32_t layerIdx;
  int32_t procType;
  int32_t inLayerIdx[TIDL_NUM_IN_BUFS];
  int32_t isInData[TIDL_NUM_IN_BUFS];
  int32_t isInOutData[TIDL_NUM_IN_BUFS];
  int32_t isOutData[TIDL_NUM_OUT_BUFS];
  void  * scratchMem;
  int32_t scratchSize;
  /* New Generic flow related */
  sDataFlowInfo_t * dataFlowInfo;
  TIDL_GenericHandle * TIDL_GenericFlowHandle[TIDL_MMA_HANDLE_MAX][TIDL_MAX_IN_TENSORS];
  void* kernelHandle[TIDL_MMA_HANDLE_MAX][TIDL_MAX_IN_TENSORS];
  int32_t kerHandleSize;
  int32_t totalL1Size;
  uint8_t *dataFlowMemPtr;
  int32_t dataFlowMemSize;
}sTIDL_AlgLayer_t;

typedef struct
{
  uint8_t                 * l1BasePtr;
  uint8_t                 * l2BasePtr;
  uint8_t                 * l3BasePtr;
  uint8_t                 * ddrBasePtr;
}TIDL_sysScratchPtr;

typedef struct {
  uint32_t    isWtDmaPending;
  uint8_t      *wtMemBasePtr;
  uint32_t     currPtrOffset;
  uint32_t     lastPtrOffset;
  uint32_t     totStageMemAvail;
  uint8_t      trMem[64];
}sTIDL_weightStagingSync;

typedef struct {
  int32_t    isWtDmaPending;
  uint8_t      *wtMemBasePtr;
  int32_t     currPtrOffset;
  int32_t     lastPtrOffset;
  int32_t     totStageMemAvail;
  uint8_t      trMem[64];
  }sTIDL_acrossLayerShare;

typedef struct
{
  uint8_t  *trMem;
  uint32_t trMemSize;
  uint32_t numDruChannel;
}TIDL_DmaRetParams;

/**
@struct TIDL_Obj
@brief  This structure is the main handle of
applet.

@param  ivision    All public function pointers
@param  algState   State of algorithm to indicate
@param  numMemRecs Number of memory records
@param  memRec     Array of memory records
@param  privContext Pointer to the conctext memory of Private algo
*/
typedef struct
{
  const IVISION_Fxns*   ivision;
  uint8_t               algState;
  uint32_t              numMemRecs;
  IALG_MemRec           memRec[MAX_NUM_MEMRECS];
  TIDL_CreateParams    * createParams ;
  void *                dataBuf[TIDL_MAX_DATA_BUFS];
  sTIDL_sysMemHandle_t sysMems[TIDL_SYSMEM_MAX];
  sTIDL_AlgLayer_t *    alglayerParams;
  uint32_t              procCallCounter;
  sTIDL_LayerBuf_t *    TIDLLayersBuf;
  uint8_t                 * refScratchBuf;
  int32_t                   refScratchBufSize;
  int32_t              * activationHistPtr;
  float32_tidl         * activationRangePtr;
  TIDL_sysScratchPtr  sysScratchPtr;
  sPerfSim_t             * perfSimOutput;
  uint8_t * zeroVector1k;
  uint8_t                   * intAlgHandle;
  uint32_t                    memAvailForFeat;
  uint8_t                      isPadDmaPending;
  void                      * udmaDrvObj;
  void                      * dmaUtilsContext;
  void                      * privContext;
  sTIDL_acrossLayerShare layerShare;
  sTIDL_weightStagingSync weightStageSync;
  uint8_t memcpyTr[64U];
  uint8_t *stagingMemPtr;
  int32_t currAlgLayer;
} TIDL_Obj;

typedef TIDL_Obj * TIDL_Handle;

/*--------------------------------------------------------*/
/* IALG functions                                         */
/* Refer XDAIS ialg.h file for details on these functions */
/*--------------------------------------------------------*/
int32_t TIDL_numAlloc(void);
int32_t TIDL_alloc(const IALG_Params *params,
                       IALG_Fxns **parentFxns, IALG_MemRec *memRec);
int32_t TIDL_init(IALG_Handle handle,
                      const IALG_MemRec *memRec, IALG_Handle parent,
                      const IALG_Params *params);
void  TIDL_activate(IALG_Handle handle);
void  TIDL_deactivate(IALG_Handle handle);
int32_t TIDL_free(IALG_Handle handle, IALG_MemRec *memRec);

int32_t TIDL_control(IVISION_Handle Handle, IALG_Cmd cmd,
                         const IALG_Params *inParams, IALG_Params *outParams);

int32_t TIDL_process(IVISION_Handle Handle, IVISION_InBufs *inBufs,
                         IVISION_OutBufs *outBufs, IVISION_InArgs *inArgs,
                         IVISION_OutArgs *outArgs);


/**
----------------------------------------------------------------------------
@ingroup    TIDL_UTILS
@fn         TIDL_getSysScratchPtr
@brief      Function gives you a pointer to system memory used to store networks
            intermediate output
@param      bufInfo : Pointer to buffer info. This will come from dataflow
@param      sysScratchPtr    : Pointer which stores the base pointer for all system scratch
@param      baseAccessFlag   : Set it to one to get base pointer of feature map. Set it to zero to
                             go to the location where DMA/CPU will READ/WRITE.

@remarks    None
@return     Pointer to the memory. NULL if  buffers accessor is NONE
----------------------------------------------------------------------------
*/
void * TIDL_getSysScratchPtr(const sBufferInfo_t * bufInfo, const TIDL_sysScratchPtr *sysScratchPtr, uint32_t baseAccessFlag);

void * TIDL_getMemoryChunkFromSysmem(sTIDL_sysMemHandle_t sysMems[TIDL_SYSMEM_MAX],
  uint32_t size, uint32_t alignment, uint32_t space, uint32_t attribute);

void TIDL_resetSysmem(sTIDL_sysMemHandle_t sysMems[TIDL_SYSMEM_MAX]);


/* If it is inter Buff Allocate buffer or Initilize it NULL */
/* At start of process call, all the null Ptrs as to be initlized
by alg using the in and out buffers provided by user */
/* At the end of processing the these has be again initialized with NULL*/
int32_t TIDL_IsInterBuffer(sTIDL_Network_t * net, int32_t buffId,
int32_t currLayersGroupId);

/* Initilze dataMemTabID as zero if it is not allocated by this core */
/* If any of the previusly allocated buffer can be
re-used (layer IDx is less than the current layer) then gwt the buf ID.*/
/* Update the memeTab to max of prev and current values */
int32_t TIDL_getReuseBufferId(int32_t buffId);


int32_t TIDL_conv2dParamsInit(sTIDL_AlgLayer_t * algLayer,
        const sTIDL_ConvParams_t * params,uint16_t imHeight,uint16_t imWidth,
        uint32_t l2ScratchSize, uint32_t l3ScratchSize, int32_t	quantizationStyle,
        uint16_t interElementSize, int32_t numCoeffsBytes);

sTIDL_DataParams_t * TIDL_getDataParams(
sTIDL_Network_t * pTIDLNetStructure,
int32_t dataId);

int32_t TIDL_conv2dProcess(
                        TIDL_Handle          intAlgHandle,
                        sTIDL_AlgLayer_t     * algLayer,
                        sTIDL_Layer_t        * tidlLayer,
                        void                 * inPtrs[],
                        void                 * outPtrs[],
                        sTIDL_sysMemHandle_t * sysMems);

int32_t isPerChannelQuantizationEnabled(const sTIDL_ConvParams_t * convParams,
                                                 int32_t calibrationOption);
static  uint32_t TIDL_findHCF(uint32_t num1,uint32_t num2);
static  int64_t TIDL_roundSat(int64_t val, int32_t bits, int32_t min, int32_t max);

/** ======================= ============================
*  @name   TIDLCONV_dataFlowType
*
*  @desc   Describes the various data flow used in convolution
*
*  @field  TIDLCONV_DATAFLOW_ROW
*             Row flow
*  @field  TIDLCONV_DATAFLOW_COL
*             Column flow
*  ===============================================================
*/

#define  TIDLCONV_DATAFLOW_ROW (0)
#define  TIDLCONV_DATAFLOW_COL (1)


#define  TIDLCONV_TYPE_CONV (0)
#define  TIDLCONV_TYPE_DECONV (1)
/* For DepthToSpace/PixelShuffle Layer */
#define  TIDLCONV_TYPE_CONVPS (2)

int32_t TIDL_conv2DCoeffMemRequired(const sTIDL_ConvParams_t * conv2dparams,
                                                            uint32_t weightsElementSizeInBits,
                                                            uint32_t dataFlowType,
                                                            uint32_t * coeffBufPitch);

int32_t TIDL_deconv2DCoeffMemRequired(const sTIDL_ConvParams_t * conv2dparams,
                                                            uint32_t weightsElementSizeInBits,
                                                            uint32_t dataFlowType,
                                                            uint32_t * coeffBufPitch);

int32_t TIDL_innerProductCoeffMemRequired(const sTIDL_InnerProductParams_t * innerProductParams);

void TIDL_conv2DConvertCoeffBuffer(const sTIDL_ConvParams_t * conv2dparams,
                                                                        const  sDataFlowInfo_t * dataFlowInfo,
                                                                        uint16_t inFeatWidth,
                                                                        uint16_t inFeatHeight,
                                                                        const void *coeffNetBuf,
                                                                        const void *biasBuf,
                                                                        void  *coeffAlgBuf,
                                                                        int32_t weightsElementSizeInBits,
                                                                        int32_t isChannelwiseQuantEnable,
                                                                        int32_t targetDevice);

void TIDL_deconv2DConvertCoeffBuffer(const sTIDL_ConvParams_t * conv2dparams,
                                                                        const  sDataFlowInfo_t * dataFlowInfo,
                                                                        uint16_t inFeatWidth,
                                                                        uint16_t inFeatHeight,
                                                                        const void *coeffNetBuf,
                                                                        const void *biasBuf,
                                                                        void  *coeffAlgBuf,
                                                                        int32_t weightsElementSizeInBits);


typedef struct {
  int32_t imWidth;
  int32_t imHeight;
  int32_t pad;
  int32_t mmaPanelWidth;
  int32_t NoPerG;
  int32_t isStridedFlow;
  const sDataFlowInfo_t * dataFlowInfo;
} TIDL_conv2dRowFlow_mmalibHandleArgs;

template <class TypeInitArgs>
int32_t TIDL_conv2dRowFlow_setupMmaHandle(sTIDL_ConvParams_t * params,
                                                          sTIDL_ALgConvParams_t *algConvParams,
                                                          const sDataFlowInfo_t * dataFlowInfo,
                                                          sTIDL_DataParams_t *inDataParams,
                                                          sTIDL_DataParams_t *outDataParams,
                                                          sTIDL_sysMemHandle_t   sysMems[TIDL_SYSMEM_MAX],
                                                          int32_t funcStyle,
                                                          int32_t layerIdx,
                                                          int32_t procType,
                                                          int32_t targetDevice);

int32_t TIDL_conv2dColFlow_setupMmaHandle(sTIDL_ConvParams_t * params,
                                                          sTIDL_ALgConvParams_t *algConvParams,
                                                          const sDataFlowInfo_t * dataFlowInfo,
                                                          sTIDL_DataParams_t *inDataParams,
                                                          sTIDL_DataParams_t *outDataParams,
                                                          int32_t funcStyle,
                                                          int32_t layerIdx,
                                                          int32_t procType,
                                                          int32_t isChannelwiseQuantEnable,
                                                          int32_t targetDevice);

int32_t TIDL_conv2dRowFlow_getLayerSetupSize( int32_t convType,
                                              TIDL_conv2dRowFlow_mmalibHandleArgs * args,
                                              int32_t nHandles,
                                              int32_t targetDevice);

int32_t TIDL_conv2dColFlow_getLayerSetupSize();


int32_t TIDL_conv2dGetCoeffBufSizeForStaging(
                                  sTIDL_ConvParams_t     * convParams,
                                  sDataFlowInfo_t      * dataFlowInfo,
                                  int32_t                inWidth,
                                  int32_t                isChannelwiseQuantEnable,
                                  int32_t                targetDevice
                              );


static int32_t TIDL_ABS(int32_t x);

/* Get Pointer helper functions */
static inline int64_t * get_int64_t_pointer(int64_t arr[], int32_t offset);

static inline uint64_t * get_uint64_t_pointer(uint64_t arr[], int32_t offset);

static inline int32_t * get_int32_t_pointer(int32_t arr[], int32_t offset);

static inline uint32_t * get_uint32_t_pointer(uint32_t arr[], int32_t offset);

static inline uint8_t * get_uint8_t_pointer(uint8_t arr[], int32_t offset);

static inline uint16_t * get_uint16_t_pointer(uint16_t arr[], int32_t offset);

static inline int8_t * get_int8_t_pointer(int8_t arr[], int32_t offset);

static inline int16_t * get_int16_t_pointer(int16_t arr[], int32_t offset);

static inline sTIDL_AlgLayer_t * get_AlgLayer_t_pointer(sTIDL_AlgLayer_t arr[], int32_t offset);

static inline float32_tidl * get_float32_t_pointer(float32_tidl arr[], int32_t offset);

static inline BBox * get_Bbox_pointer(BBox arr[], int32_t offset);


/* CHECK_MISRA("-8.5")  -> Disable rule 8.5  */

/* MISRA.ONEDEFRULE.FUNC
   MISRAC_2004 Rule_8.5
   MISRAC_WAIVER:
   This is flagged in a single file called: tidl_alg_int.h.
   This function needs to be an inline function, and it is
   declared and defined in the header file. Also, it is
   downgraded to advisory in MISRA-C 2012
*/

static inline void TIDL_edmaCopy(void * dst, const void * src, uint16_t size)
{
#if 0
  EDMA_UTILS_memcpy2D((void *)       dst,
    (const void  *)		 src,
    (uint16_t)     size,
    (uint16_t)     1,
    (int16_t)      0,
    (int16_t)      0);
#endif
}

template <typename typeScalerIn>
static inline void TIDL_memset(typeScalerIn *ptr, typeScalerIn val, int32_t elements){
  for(int32_t offset = 0; offset < elements ; offset++)
  {
    ptr[offset] = val ;
  }
  return ;
}

static int32_t TIDL_ABS(int32_t x)
{
	int32_t y;
	if(x < 0)
	{
		y = (-x);
	}
	else
	{
	  y = (x);
	}
	return (y);
}

static float TIDL_ABS_FLOAT(float x)
{
  float y;
  if (x < 0)
  {
    y = (-x);
  }
  else
  {
    y = (x);
  }
  return (y);
}

static uint32_t TIDL_findHCF(uint32_t num1,uint32_t num2)
{
  uint32_t min, i;
  uint32_t hcf = 1U;
  min=(num1>num2)? num2 : num1;
  for(i=min;((i>=1U) && (hcf==1U));--i)
  {
    if(((num1%i)==0) && ((num2%i)==0))
    {
      hcf = i;
    }
  }
  return hcf;
}


static int64_t TIDL_roundSat(int64_t val, int32_t bits, int32_t min, int32_t max)
{
   uint32_t temp;
  if(bits > 0)
  {
    temp = ((uint32_t)1U << (bits - 1));
    val += (int64_t)temp;

    /* CHECK_MISRA("-12.7")  -> Disable rule 12.7 */
    val >>= bits;
    /*RESET_MISRA("12.7")  -> Reset rule 12.7 */
  }
  val =  val < min ? min :  val;
  val =  val > max  ? max  :  val;
  return val;
}

static int64_t TIDL_roundSatMMA(int64_t val, int32_t bits, int32_t min, int32_t max)
{
  int64_t temp;
  if(bits > 0)
  {
    temp = __shift_right( val, (bits - 1) ) + 1;
    val = __shift_right(temp, 1);
  }
  val =  val < min ? min :  val;
  val =  val > max  ? max  :  val;
  return val;
}

static inline int64_t * get_int64_t_pointer(int64_t arr[], int32_t offset)
{
    return &arr[offset];
}

static inline uint64_t * get_uint64_t_pointer(uint64_t arr[], int32_t offset)
{
    return &arr[offset];
}

static inline int32_t * get_int32_t_pointer(int32_t arr[], int32_t offset)
{
    return &arr[offset];
}

static inline uint32_t * get_uint32_t_pointer(uint32_t arr[], int32_t offset)
{
    return &arr[offset];
}

static inline uint8_t * get_uint8_t_pointer(uint8_t arr[], int32_t offset)
{
    return &arr[offset];
}

static inline uint16_t * get_uint16_t_pointer(uint16_t arr[], int32_t offset)
{
    return &arr[offset];
}

static inline int8_t * get_int8_t_pointer(int8_t arr[], int32_t offset)
{
    return &arr[offset];
}

static inline int16_t * get_int16_t_pointer(int16_t arr[], int32_t offset)
{
    return &arr[offset];
}

static inline sTIDL_AlgLayer_t * get_AlgLayer_t_pointer(sTIDL_AlgLayer_t arr[], int32_t offset)
{
    return &arr[offset];
}

static inline float32_tidl * get_float32_t_pointer(float32_tidl arr[], int32_t offset)
{
  return &arr[offset];
}

static inline BBox * get_Bbox_pointer(BBox arr[], int32_t offset)
{
  return &arr[offset];
}

int32_t TIDL_getDatElementSize(int32_t elementType);
int32_t TIDL_getLayerNum(const sTIDL_Network_t * pTIDLNetStructure, int32_t dataId);
int32_t TIDL_getDatElementSign(int32_t elementType);
int32_t TIDL_layerProcess(
    TIDL_Handle intAlgHandle,
    sTIDL_AlgLayer_t *algLayer,
    sTIDL_Layer_t *TIDLLayer,
    void *inPtrs[],
    void *outPtrs[],
    sTIDL_sysMemHandle_t *sysMems,
    int32_t i);

int32_t TIDL_FillPaddedRows(uint8_t *ptr,
                                    const sBufferInfo_t *bufInfo,
                                    int32_t width,
                                    int32_t height,
                                    int32_t numBatches,
                                    int32_t linePitch,
                                    int32_t channelPitch,
                                    uint32_t batchPitch,
                                    void *dmautilsContext,
                                    uint8_t *zeroVec,
                                    uint8_t *isPadDmaPending,
                                    uint8_t *trMem,
                                    int32_t elementType,
                                    int32_t paddingType,
                                    void * padLayerParams,
                                    int32_t outDataPadH,
                                    int32_t outPadaPadW,
                                    int32_t isFlowCtrlNatc,
                                    void * perChannelPadValPtr);

int32_t TIDL_FillPaddedCols(
  uint8_t *ptr,
  const sBufferInfo_t *bufInfo,
  int32_t width,
  int32_t height,
  int32_t numBatches,
  int32_t linePitch,
  int32_t channelPitch,
  uint32_t batchPitch,
  int32_t elementType,
  int32_t flowCtrl,
  int32_t paddingType,
  void * padLayerParams,
  int32_t outDataPadH,
  int32_t outDataPadW,
  void * perChannelPadValPtr);

int32_t TIDL_layerPadding(
    TIDL_Handle intAlgHandle,
    sTIDL_Layer_t *TIDLLayer,
    void *outPtrs[],
    uint8_t *trMem,
    int32_t i);
 
int32_t  TIDL_DspFillPaddedBatches(
    uint8_t * ptr,
    int32_t ptrOffset,
    int32_t batchPadW,
    int32_t height,
    int32_t numChs,
    int32_t numBatches,
    int32_t linePitch,
    int32_t channelPitch,
    uint32_t batchPitch);
/* RESET_MISRA("8.5")  -> Reset rule 8.5 */
#endif /*ITIDL_ALG_INT_H */
