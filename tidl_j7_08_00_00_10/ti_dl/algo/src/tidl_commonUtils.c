/*
*
* Copyright (c) {2015 - 2020} Texas Instruments Incorporated
*
* All rights reserved not granted herein.
*
* Limited License.
*
* Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
* license under copyrights and patents it now or hereafter owns or controls to make,
* have made, use, import, offer to sell and sell ("Utilize") this software subject to the
* terms herein.  With respect to the foregoing patent license, such license is granted
* solely to the extent that any such patent is necessary to Utilize the software alone.
* The patent license shall not apply to any combinations which include this software,
* other than combinations with devices manufactured by or for TI ("TI Devices").
* No hardware patent is licensed hereunder.
*
* Redistributions must preserve existing copyright notices and reproduce this license
* (including the above copyright notice and the disclaimer and (if applicable) source
* code license limitations below) in the documentation and/or other materials provided
* with the distribution
*
* Redistribution and use in binary form, without modification, are permitted provided
* that the following conditions are met:
*
* *       No reverse engineering, decompilation, or disassembly of this software is
* permitted with respect to any software provided in binary form.
*
* *       any redistribution and use are licensed by TI for use only with TI Devices.
*
* *       Nothing shall obligate TI to provide you with source code for the software
* licensed and provided to you in object code.
*
* If software source code is provided to you, modification and redistribution of the
* source code are permitted provided that the following conditions are met:
*
* *       any redistribution and use of the source code, including any resulting derivative
* works, are licensed by TI for use only with TI Devices.
*
* *       any redistribution and use of any object code compiled from the source code
* and any resulting derivative works, are licensed by TI for use only with TI Devices.
*
* Neither the name of Texas Instruments Incorporated nor the names of its suppliers
*
* may be used to endorse or promote products derived from this software without
* specific prior written permission.
*
* DISCLAIMER.
*
* THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
* OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
* OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
* IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
* BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
* OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
* OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
* OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/**
----------------------------------------------------------------------------
@file    tidl_commonUtils.c
@brief   This file contains TILD common utilities
@version 0.1 (May 2018) : Initial version [ADK]
----------------------------------------------------------------------------
*/

#include "tidl_types.h"
#include "tidl_alg_int.h"
#include "tidl_commonUtils.h"
#include "../../custom/tidl_custom.h"
#include <stdint.h>
#include <limits>
#include <math.h>
#include <float.h>
#define USE_16BIT_BIAS_FOR_8BIT_MODE (1)

#include "ti/drv/udma/dmautils/dmautils.h"

static int32_t tidlLogLevel;
static int32_t tidlWriteLevel;
static tidlVprintf_t tidlVprintf;
static tidlWriteBinToFile_t tidlWriteBinToFile;
static tidlReadBinFromFile_t tidlReadBinFromFile;
static void * tidlTraceBaseName;



/**
 * @brief To find min and max in the tensor
 *
 * @param ptr : pointer to tensor data
 * @param dataPrms : parametes of the data buffer
 * @param min : to store the min value
 * @param max : to store the max value
 */
template <class Tsrc, class TminMax>
void TIDL_TensorMinMax(const Tsrc * ptr, const sTIDL_DataParams_t * dataPrms, TminMax *min, TminMax * max)
{
  int32_t padOffset = dataPrms->padW + (dataPrms->padH*dataPrms->pitch[TIDL_LINE_PITCH]);
  int32_t i0, i1, i2, i3;
  TminMax val;

  for (i0 = 0; i0 < dataPrms->dimValues[TIDL_DIM_BATCH]; i0++)
  {
    for (i1 = 0; i1 < dataPrms->dimValues[TIDL_DIM_NUMCH]; i1++)
    {
      for (i2 = 0; i2 < dataPrms->dimValues[TIDL_DIM_HEIGHT]; i2++)
      {
        for (i3 = 0; i3 < dataPrms->dimValues[TIDL_DIM_WIDTH]; i3++)
        {
          val = (TminMax)ptr[padOffset + (i0*dataPrms->pitch[TIDL_ROI_PITCH]) + (i1*dataPrms->pitch[TIDL_CHANNEL_PITCH]) + (i2*dataPrms->pitch[TIDL_LINE_PITCH]) + i3];
          *min = (val < *min) ? val : *min;
          *max = (val > *max) ? val : *max;
        }
      }
    }
  }
}

/**
 * @brief  To find histogram min and max values of the tensor
 *
 * @param ptr  : pointer to the tensor data
 * @param dataPrms : parametes of the tensor data
 * @param numBins : no of bins used for histogram
 * @param percentileActRangeShrink : active range percentage
 * @param histogramPtr : Pointer to the histogram buffer
 * @param min : to store the min value
 * @param max : to store the max value
 */
template <class Tsrc, class TminMax>
int32_t TIDL_TensorMinMaxHist(const Tsrc * ptr,
                                      const sTIDL_DataParams_t * dataPrms,
                                      int32_t numBins,
                                      float32_tidl percentileActRangeShrink,
                                      int32_t * histogramPtr,
                                      TminMax* min,
                                      TminMax* max)
{
  int32_t status = TIDL_SUCCESS;
  int32_t padOffset = dataPrms->padW + (dataPrms->padH*dataPrms->pitch[TIDL_LINE_PITCH]);
  int32_t i0, i1, i2, i3, i4, i;
  TminMax val;
  float32_tidl valFloat, valNorm;
  int32_t numVals;
  int32_t binIdx, minBinIdx = 0, maxBinIdx = (numBins - 1);
  float32_tidl minValue = *min / dataPrms->tensorScale;
  float32_tidl maxValue = *max / dataPrms->tensorScale;
  float32_tidl minFloatOut;
  float32_tidl maxFloatOut;

  if((maxValue - minValue) != 0) /*not all values in tensor constant */
  {
    for (i0 = 0; i0 < dataPrms->dimValues[TIDL_DIM_BATCH]; i0++)
    {
      for (i1 = 0; i1 < dataPrms->dimValues[TIDL_DIM_NUMCH]; i1++)
      {
        for (i2 = 0; i2 < dataPrms->dimValues[TIDL_DIM_HEIGHT]; i2++)
        {
          for (i3 = 0; i3 < dataPrms->dimValues[TIDL_DIM_WIDTH]; i3++)
          {
            val = (TminMax)ptr[padOffset + (i0*dataPrms->pitch[TIDL_ROI_PITCH]) + (i1*dataPrms->pitch[TIDL_CHANNEL_PITCH]) + (i2*dataPrms->pitch[TIDL_LINE_PITCH]) + i3];
            valFloat = val / dataPrms->tensorScale;
            valNorm = (valFloat - minValue)/(maxValue - minValue) * (numBins-1);

            binIdx = (int32_t)(valNorm + 0.5);/* Round to nearest integer */

            if ( binIdx > (numBins-1) )
            {
              binIdx = (numBins-1);
            }
            histogramPtr[binIdx]++;
          }
        }
      }
    }

    numVals = 0;
    for (i = 0; i < numBins; i++)
    {
      numVals += histogramPtr[i];
    }

    int32_t pct_freq = (int32_t)((percentileActRangeShrink / 100.0) * numVals);
    int32_t count = 0;
    if (*min < 0)  /* minimum to be shrinked only for signed */
    {
      for(i4 = 0; i4 < numBins; i4++)
      {
        count += histogramPtr[i4];
        if(count >= pct_freq)
        {
          minBinIdx = i4;
          break;
        }
      }
    }
    else
    {
      minBinIdx = 0;
    }
    count = 0;
    for(i4 = numBins - 1; i4 >= 0; i4--)
    {
      count += histogramPtr[i4];
      if(count >= pct_freq)
      {
        maxBinIdx = i4;
        break;
      }
    }
    minFloatOut = minValue + (float32_tidl)minBinIdx / (numBins-1) * (maxValue - minValue);
    maxFloatOut = minValue + (float32_tidl)maxBinIdx / (numBins-1) * (maxValue - minValue);
    maxFloatOut = (maxFloatOut > maxValue) ? maxValue : maxFloatOut; /*to ensure back calculated bin value not greater than original max */
    minFloatOut = (minFloatOut < minValue) ? minValue : minFloatOut;
    *min = (TminMax)(minFloatOut * dataPrms->tensorScale);
    *max = (TminMax)(maxFloatOut * dataPrms->tensorScale);
  }

  return status;
}

/**
 * @brief This function is to find Current Offset For PerChannelMean
 *
 * @param net : tidl network structure
 * @param currLayerNum : layer no of the current layer
 * @param totalMemReq : to store total memory requiremet
 * @return int32_t : returns the current ch offset
 */
static int32_t TIDL_findCurrentOffsetForPerChannelMean(sTIDL_Network_t * net, int32_t currLayerNum, int32_t * totalMemReq)
{
  int layerIdx = 0;
  int32_t totNumOutChannels = 0;
  int32_t currNumOutChannels = 0;
  for (layerIdx = 0; layerIdx < net->numLayers; layerIdx++)
  {

    if ( layerIdx == (currLayerNum) )
    {
      currNumOutChannels = totNumOutChannels;
    }

    if ( (net->TIDLLayers[layerIdx].layerType != TIDL_DataLayer) )
    {
      //:TODO: This can eventually be done only for the layers where bias is applicable
      totNumOutChannels += net->TIDLLayers[layerIdx].outData[0].dimValues[TIDL_DIM_NUMCH];
    }
  }

  *totalMemReq = totNumOutChannels * sizeof(float32_tidl);
  return (currNumOutChannels * sizeof(float32_tidl));
}

/**
 * @brief To find tensor perChannel mean
 *
 * @param net : tidl network structure
 * @param currLayerNum : current layer number
 * @param ptr : pointer to the tensor data
 * @param dataPrms : parameters of the data buffer
 * @param scratchBuf : pointer to the scratch memory
 * @param scratchBufSize : size of the scratch buffer
 * @param currIterationCount : count for the current iteration
 * @return int32_t : returns tensor perchannelmean
 */
template <class Tsrc>
int32_t TIDL_TensorPerChannelMean(sTIDL_Network_t * net,
                                                      int32_t currLayerNum,
                                                      const Tsrc * ptr,
                                                      const sTIDL_DataParams_t * dataPrms,
                                                      void * scratchBuf,
                                                      int32_t scratchBufSize,
                                                      int32_t currIterationCount
                                                      )
{
  int32_t status = TIDL_SUCCESS;
  int32_t padOffset = dataPrms->padW + (dataPrms->padH*dataPrms->pitch[TIDL_LINE_PITCH]);
  int32_t i0, i1, i2, i3;
  float32_tidl * meanPtr;
  int32_t totalMemReqInBytes;
  int8_t * traceDumpName = (int8_t *)(scratchBuf);
  int32_t currOffsetInBytes = 0;
  int32_t currOffsetInfloats = 0;
  float32_tidl currChannelMean = 0.0;
  float32_tidl runningChannelMean = 0.0;
  float32_tidl sum;
  float32_tidl updateFactor;
  Tsrc val;

  if(tidlTraceBaseName != NULL)
  {
    sprintf((char *)traceDumpName, "%s_LayerPerChannelMean.bin", (char *)tidlTraceBaseName);
  }
  else
  {
    (void)strcpy((char *)traceDumpName, (char *)"");
  }

  /* Find the total memory required and current offset */
  currOffsetInBytes = TIDL_findCurrentOffsetForPerChannelMean(net,
                                                              currLayerNum,
                                                              &totalMemReqInBytes);

  currOffsetInfloats = ( currOffsetInBytes / sizeof(float32_tidl) );

  if (totalMemReqInBytes < (scratchBufSize -TRACE_STRING_SIZE) )
  {
    meanPtr = (float32_tidl*)((int8_t *)scratchBuf + TRACE_STRING_SIZE);
  }
  else
  {
    tidl_printf(0,"TIDL_TensorPerChannelMean : Not enough memory to allocate for per channel mean \n");
    status = TIDL_ERR_FAILURE;
  }

  if ( status == TIDL_SUCCESS )
  {
    if ( (currIterationCount == 0) && (currOffsetInBytes == 0 ) )
    {
      memset(meanPtr, 0, totalMemReqInBytes);
    }
    else
    {
      (void)tidlReadBinFromFile((const char *)traceDumpName, meanPtr, totalMemReqInBytes );
    }

    updateFactor = 1.0/(currIterationCount + 1U);
      //:TODO: Add minimum value so that when num images are we don't loose information from end images

    for (i0 = 0; i0 < dataPrms->dimValues[TIDL_DIM_BATCH]; i0++)
    {
      for (i1 = 0; i1 < dataPrms->dimValues[TIDL_DIM_NUMCH]; i1++)
      {
        sum = 0;
        for (i2 = 0; i2 < dataPrms->dimValues[TIDL_DIM_HEIGHT]; i2++)
        {
          for (i3 = 0; i3 < dataPrms->dimValues[TIDL_DIM_WIDTH]; i3++)
          {
            val = (Tsrc)ptr[padOffset + (i0*dataPrms->pitch[TIDL_ROI_PITCH]) + (i1*dataPrms->pitch[TIDL_CHANNEL_PITCH]) + (i2*dataPrms->pitch[TIDL_LINE_PITCH]) + i3];
            sum += (float32_tidl)val;
          }
        }
        currChannelMean = sum / (dataPrms->dimValues[TIDL_DIM_WIDTH] *
                                                  dataPrms->dimValues[TIDL_DIM_HEIGHT]);
        currChannelMean = currChannelMean / dataPrms->tensorScale;
        runningChannelMean = meanPtr[currOffsetInfloats + i1];
        meanPtr[currOffsetInfloats + i1] = (runningChannelMean * (1.0 - updateFactor)) +
                                                        (currChannelMean * (updateFactor));
      }
    }

    (void)tidlWriteBinToFile((const char *)traceDumpName, meanPtr, totalMemReqInBytes);
  }
  return status;
}

#define TIDL_HISTOGRAM_ACTIVATION_RANGEFACTOR (4.0)

/**
 * @brief This function is to update the tesnor range
 *
 * @param intAlgHandle : tidl algorithm handle
 * @param layerIdx : index of the current layer
 * @param outDataIdx : index of the output data buffer
 * @param ptr : pointer to the tensor data
 * @return  IALG_EOK   - Successful
 *          IALG_EFAIL - Unspecified error
 */
int32_t TIDL_UpdateTensorRange(TIDL_Handle intAlgHandle, int32_t layerIdx, int32_t outDataIdx, void * ptr)
{
  float32_tidl min = FLT_MAX;
  float32_tidl max = -1.0 * FLT_MAX;
  int32_t temp;
  int32_t status = TIDL_SUCCESS;
  sTIDL_Network_t * net = intAlgHandle->createParams->net;
  int32_t numBins = TIDL_NUM_ACTIVATION_HISTOGRAM_BINS;
  int32_t * histogramPtr = NULL;
  if (TIDL_getDatElementSize(net->TIDLLayers[layerIdx].outData[0].elementType) <= 2)
  {
    int32_t minTemp, maxTemp;
    int32_t minActHist;
    int32_t maxActHist;
    minActHist = minTemp  = (int32_t)((((uint32_t)1) << ((sizeof(int32_t) * 8U) - 1U)) - 1U);
    temp = (int32_t)((((uint32_t)1) << ((sizeof(int32_t) * 8U) - 1U)));
    maxActHist = maxTemp = -1 * temp;

    if (intAlgHandle->activationHistPtr != NULL )
    {
      histogramPtr = &intAlgHandle->activationHistPtr[layerIdx * TIDL_NUM_ACTIVATION_HISTOGRAM_BINS];
      /* activationRangePtr being NULL indicates that we want to update histogram for each frame */
      if ( intAlgHandle->activationRangePtr == NULL )
      {
        memset(histogramPtr, 0 , TIDL_NUM_ACTIVATION_HISTOGRAM_BINS * sizeof(int32_t));
      }
      else
      {
        /* THis indicates that user wants to use global histogram across all the
        frames. So read the activation ranges from the previous iteration and multiply
        it by a factor to account for any variation in stats across iterations. These
        min and max value will be use to find the histogram across all the frames */
        minActHist = (int32_t)(intAlgHandle->activationRangePtr[2* layerIdx] *
                    TIDL_HISTOGRAM_ACTIVATION_RANGEFACTOR *
                    net->TIDLLayers[layerIdx].outData[outDataIdx].tensorScale);
        maxActHist = (int32_t)(intAlgHandle->activationRangePtr[2* layerIdx + 1] *
                    TIDL_HISTOGRAM_ACTIVATION_RANGEFACTOR *
                    net->TIDLLayers[layerIdx].outData[outDataIdx].tensorScale);
      }
    }

    if (net->TIDLLayers[layerIdx].outData[outDataIdx].elementType == TIDL_SignedChar)
    {
      TIDL_TensorMinMax((int8_t*)ptr, &net->TIDLLayers[layerIdx].outData[outDataIdx], (int32_t *)&minTemp, (int32_t *)&maxTemp);

      if ( histogramPtr != NULL )
      {
        /* For global histogram always pick the range from histogram from previous stats collection
        iteration */
        if ( net->calibrationParams.activationRangeMethod == TIDL_ActivationRangeMethodGlobalHistogram )
        {
          minTemp = minActHist;
          maxTemp = maxActHist;
        }
        status = TIDL_TensorMinMaxHist((int8_t *)ptr,
                                          &net->TIDLLayers[layerIdx].outData[outDataIdx],
                                          numBins,
                                          net->calibrationParams.percentileActRangeShrink,
                                          histogramPtr,
                                          (int32_t*)&minTemp, (int32_t*)&maxTemp);

      }
    }
    else if (net->TIDLLayers[layerIdx].outData[outDataIdx].elementType == TIDL_UnsignedChar)
    {
      TIDL_TensorMinMax((uint8_t*)ptr, &net->TIDLLayers[layerIdx].outData[outDataIdx], (int32_t *)&minTemp, (int32_t *)&maxTemp);

      if ( histogramPtr != NULL )
      {
        /* For global histogram always pick the range from histogram from previous stats collection
        iteration */
        if ( net->calibrationParams.activationRangeMethod == TIDL_ActivationRangeMethodGlobalHistogram )
        {
          minTemp = minActHist;
          maxTemp = maxActHist;
        }
        status = TIDL_TensorMinMaxHist((uint8_t *)ptr,
                                          &net->TIDLLayers[layerIdx].outData[outDataIdx],
                                          numBins,
                                          net->calibrationParams.percentileActRangeShrink,
                                          histogramPtr,
                                          (int32_t*)&minTemp, (int32_t*)&maxTemp);

      }
    }
    else if (net->TIDLLayers[layerIdx].outData[outDataIdx].elementType == TIDL_SignedShort)
    {
      TIDL_TensorMinMax((int16_t*)ptr, &net->TIDLLayers[layerIdx].outData[outDataIdx], (int32_t *)&minTemp, (int32_t *)&maxTemp);

      if ( histogramPtr != NULL )
      {
        /* For global histogram always pick the range from histogram from previous stats collection
        iteration */
        if ( net->calibrationParams.activationRangeMethod == TIDL_ActivationRangeMethodGlobalHistogram )
        {
          minTemp = minActHist;
          maxTemp = maxActHist;
        }
        status = TIDL_TensorMinMaxHist((int16_t *)ptr,
                                          &net->TIDLLayers[layerIdx].outData[outDataIdx],
                                          numBins,
                                          net->calibrationParams.percentileActRangeShrink,
                                          histogramPtr,
                                          (int32_t*)&minTemp, (int32_t*)&maxTemp);

      }
    }
    else if (net->TIDLLayers[layerIdx].outData[outDataIdx].elementType == TIDL_UnsignedShort)
    {
      TIDL_TensorMinMax((uint16_t*)ptr, &net->TIDLLayers[layerIdx].outData[outDataIdx], (int32_t *)&minTemp, (int32_t *)&maxTemp);

      if ( histogramPtr != NULL )
      {
        /* For global histogram always pick the range from histogram from previous stats collection
        iteration */
        if ( net->calibrationParams.activationRangeMethod == TIDL_ActivationRangeMethodGlobalHistogram )
        {
          minTemp = minActHist;
          maxTemp = maxActHist;
        }
        status = TIDL_TensorMinMaxHist((uint16_t *)ptr,
                                          &net->TIDLLayers[layerIdx].outData[outDataIdx],
                                          numBins,
                                          net->calibrationParams.percentileActRangeShrink,
                                          histogramPtr,
                                          (int32_t*)&minTemp, (int32_t*)&maxTemp);

      }
    }
    else
    {
      status = TIDL_ERR_FAILURE;
    }
    min = ( float32_tidl) minTemp;
    max = ( float32_tidl) maxTemp;
  }
  else
  {
    TIDL_TensorMinMax((float32_tidl *)ptr, &net->TIDLLayers[layerIdx].outData[outDataIdx], (float32_tidl *)&min, (float32_tidl *)&max);
    net->TIDLLayers[layerIdx].outData[outDataIdx].tensorScale = 1.0;

#if 0
    if ( ( net->calibrationOption & TIDL_CalibOptionActivationRange ) == TIDL_CalibOptionActivationRange )
    {
      if ( net->calibrationParams.activationRangeMethod == TIDL_ActivationRangeMethodHistogram )
      {
        status = TIDL_TensorMinMaxHist((float32_tidl *)ptr,
                                           &net->TIDLLayers[layerIdx].outData[outDataIdx],
                                           numBins,
                                           net->calibrationParams.percentileActRangeShrink,
                                           intAlgHandle->refScratchBuf,
                                           intAlgHandle->refScratchBufSize,
                                           (float32_tidl*)&min, (float32_tidl*)&max);
      }
    }
#endif
  }

  if(status == TIDL_SUCCESS)
  {
    if (net->TIDLLayers[layerIdx].outData[outDataIdx].tensorScale != 0.0f)
    {
      float32_tidl curMin = (1.0f * (float32_tidl)min) / net->TIDLLayers[layerIdx].outData[outDataIdx].tensorScale;
      float32_tidl curMax = (1.0f * (float32_tidl)max) / net->TIDLLayers[layerIdx].outData[outDataIdx].tensorScale;
      if(intAlgHandle->createParams->quantRangeExpansionFactor != 1.0f)
      {
        curMin = curMin * intAlgHandle->createParams->quantRangeExpansionFactor;
        curMax = curMax * intAlgHandle->createParams->quantRangeExpansionFactor;
      }

      if ((intAlgHandle->procCallCounter == 0U) && (intAlgHandle->createParams->quantRangeUpdateFactor != 0 ) )
      {
        net->TIDLLayers[layerIdx].outData[outDataIdx].minTensorValue = curMin;
        net->TIDLLayers[layerIdx].outData[outDataIdx].maxTensorValue = curMax;
      }
      else
      {
        float32_tidl beta = intAlgHandle->createParams->quantRangeUpdateFactor;
        float32_tidl alpha = 1.0f - beta;

        if(intAlgHandle->createParams->quantRangeUpdateFactor == -1.0f)
        {
          beta = 1.0f / (intAlgHandle->procCallCounter + 1.0f);
          alpha = 1.0f - beta;
        }

        net->TIDLLayers[layerIdx].outData[outDataIdx].minTensorValue = (net->TIDLLayers[layerIdx].outData[outDataIdx].minTensorValue *alpha) + (curMin*beta);
        net->TIDLLayers[layerIdx].outData[outDataIdx].maxTensorValue = (net->TIDLLayers[layerIdx].outData[outDataIdx].maxTensorValue *alpha) + (curMax*beta);
      }
    }


    if (net->TIDLLayers[layerIdx].actParams.actType == TIDL_Clip)
    {
      net->TIDLLayers[layerIdx].outData[outDataIdx].minTensorValue = net->TIDLLayers[layerIdx].actParams.clipMin;
      net->TIDLLayers[layerIdx].outData[outDataIdx].maxTensorValue = net->TIDLLayers[layerIdx].actParams.clipMax;
    }

    if (net->TIDLLayers[layerIdx].actParams.actType == TIDL_RelU6)
    {
      if ( net->TIDLLayers[layerIdx].outData[outDataIdx].maxTensorValue > 6.0f)
      {
        net->TIDLLayers[layerIdx].outData[outDataIdx].maxTensorValue = 6.0f;
      }
    }

    /* For Argmax layer max is decided based on number of input channels*/
    if ( net->TIDLLayers[layerIdx].layerType == TIDL_ArgMaxLayer )
    {
      net->TIDLLayers[layerIdx].outData[outDataIdx].minTensorValue = 0.0;
      net->TIDLLayers[layerIdx].outData[outDataIdx].maxTensorValue =
            (net->TIDLLayers[layerIdx].inData[0].dimValues[TIDL_DIM_NUMCH] -1);
    }

  }

  return status;
}

/**
 * @brief This function is to Update Tensor PerChannelMean
 *
 * @param intAlgHandle : tidl algorithm handle
 * @param layerIdx : index of the current layer
 * @param outDataIdx : index of the output data buffer
 * @param ptr : pointer to the tensor data
 * @return  IALG_EOK   - Successful
 *          IALG_EFAIL - Unspecified error
 */
int32_t TIDL_UpdateTensorPerChannelMean(TIDL_Handle intAlgHandle,
                                        int32_t layerIdx,
                                        int32_t outDataIdx,
                                        void * ptr)
{
  int32_t status = TIDL_SUCCESS;
  sTIDL_Network_t * net = intAlgHandle->createParams->net;
  if (TIDL_getDatElementSize(net->TIDLLayers[layerIdx].outData[0].elementType) <= 2)
  {
    if (net->TIDLLayers[layerIdx].outData[outDataIdx].elementType == TIDL_SignedChar)
    {
      TIDL_TensorPerChannelMean(net,
                                             layerIdx,
                                             (int8_t*)ptr,
                                             &net->TIDLLayers[layerIdx].outData[outDataIdx],
                                             intAlgHandle->refScratchBuf,
                                             intAlgHandle->refScratchBufSize,
                                             intAlgHandle->procCallCounter);
    }
    else if (net->TIDLLayers[layerIdx].outData[outDataIdx].elementType == TIDL_UnsignedChar)
    {
      TIDL_TensorPerChannelMean(net,
                                             layerIdx,
                                             (uint8_t*)ptr,
                                             &net->TIDLLayers[layerIdx].outData[outDataIdx],
                                             intAlgHandle->refScratchBuf,
                                             intAlgHandle->refScratchBufSize,
                                             intAlgHandle->procCallCounter);
    }
    else if (net->TIDLLayers[layerIdx].outData[outDataIdx].elementType == TIDL_SignedShort)
    {
      TIDL_TensorPerChannelMean(net,
                                             layerIdx,
                                             (int16_t*)ptr,
                                             &net->TIDLLayers[layerIdx].outData[outDataIdx],
                                             intAlgHandle->refScratchBuf,
                                             intAlgHandle->refScratchBufSize,
                                             intAlgHandle->procCallCounter);
    }
    else if (net->TIDLLayers[layerIdx].outData[outDataIdx].elementType == TIDL_UnsignedShort)
    {
      TIDL_TensorPerChannelMean(net,
                                             layerIdx,
                                             (uint16_t*)ptr,
                                             &net->TIDLLayers[layerIdx].outData[outDataIdx],
                                             intAlgHandle->refScratchBuf,
                                             intAlgHandle->refScratchBufSize,
                                             intAlgHandle->procCallCounter);
    }
    else
    {
      status = TIDL_ERR_FAILURE;
    }
  }
  else
  {
    TIDL_TensorPerChannelMean(net,
                                           layerIdx,
                                           (float32_tidl*)ptr,
                                           &net->TIDLLayers[layerIdx].outData[outDataIdx],
                                           intAlgHandle->refScratchBuf,
                                           intAlgHandle->refScratchBufSize,
                                           intAlgHandle->procCallCounter);
  }

  return status;
}


/**
 * @brief This function is to update scale factors
 *
 * @param intAlgHandle : tidl algorithm instance handle
 * @param i : Current layer index
 * @param updateStats : flag to update stats
 * @param accMin : Min value in the accumlator buffer
 * @param accMax  : Max value in the accumlator buffer
 */
void TIDL_UpdateScaleFactors(TIDL_Handle intAlgHandle, int32_t i, int32_t updateStats, int64_t accMin, int64_t accMax)
{
  sTIDL_Network_t * net = intAlgHandle->createParams->net;
  int32_t elementSizeBytes = TIDL_getDatElementSize(net->TIDLLayers[i].outData[0].elementType);
  /* For float we don't have to call update anything for stats collection */
  if ( elementSizeBytes == 4 )
  {
    net->TIDLLayers[i].outData[0].roundBits = 0;
    net->TIDLLayers[i].outData[0].tensorScale = 1.0f;
  }
  return;
}

//J7AM Code:

//This implementation is temporary : Future versions won't need this sort of reconstruction (Direct 32/64 - bit bias consumption w/o any sort of overhead)
/**
 * @brief The function does the bias split for the conv parameters
 *
 * @param srcPtr : Pointer to the source buffer
 * @param dstPtr : Pointer to the destination buffer
 * @param biasB : Pointer to the bias buffer
 * @param dataSize : no of output channels
 * @param inScaleFactor : scale factor for the input
 * @param satLow : Min value of the saturation
 * @param satHigh : Max value of the saturation
 * @param biasBMax : the max value of the bias values
 * @param inFeatSign : the sign of the input feature map
 */
template <class Tdst, class Tsrc>
void TIDL_AM_conv2dBiasSplit(Tsrc * srcPtr, Tdst * dstPtr, int32_t * biasB, int32_t dataSize, float32_tidl inScaleFactor, int32_t satLow, int32_t satHigh, int32_t biasBMax, int32_t inFeatSign)
{
  if(inScaleFactor == 0)
  {
    for (int32_t idx = 0; idx < dataSize; idx++)
    {
        dstPtr[idx] = 0;
    }
    *biasB = 0;
  }
  else
  {
#if USE_16BIT_BIAS_FOR_8BIT_MODE
  int32_t orgBiasVal, biasVal = 0;
  if (inFeatSign == 1)
  {
    biasBMax = biasBMax / 2;
  }

  if (inScaleFactor > biasBMax)
  {
    int32_t min, max, temp;
    if (inFeatSign == 1)
    {
      max = biasBMax;
    }
    else
    {
      max = biasBMax/2;
    }
    temp = max+1;
    min = -1 * temp;

    for (int32_t idx = 0; idx < dataSize; idx++)
    {
        orgBiasVal = srcPtr[idx] * inScaleFactor;
        biasVal = (orgBiasVal < satLow) ? satLow : orgBiasVal;
        biasVal = (biasVal >  satHigh) ? satHigh : biasVal;
        temp = (biasVal / biasBMax);
        temp = (temp < min) ? min : temp;
        temp = (temp > max) ? max : temp;
        dstPtr[idx] = temp;
    }
    *biasB = biasBMax;
  }
  else if (inScaleFactor > 1)
  {
    for (int32_t idx = 0; idx < dataSize; idx++)
    {
      dstPtr[idx] = srcPtr[idx];
    }
    *biasB = inScaleFactor;
  }
  else
  {
    for (int32_t idx = 0; idx < dataSize; idx++)
    {
      dstPtr[idx] = srcPtr[idx]* inScaleFactor;
    }
    *biasB = 1;
  }

#else
  int32_t biasVal = 0;
  int32_t orgBiasVal = 0;
  for (int32_t idx = 0; idx < dataSize; idx++)
  {
    orgBiasVal = srcPtr[idx] * inScaleFactor;
    biasVal = orgBiasVal < satLow ? satLow : orgBiasVal;
    biasVal = biasVal >  satHigh ? satHigh : biasVal;
    dstPtr[idx] = biasVal;
    if (orgBiasVal != biasVal)
    {
      tidl_printf(0,"Conv2D bias is greater than 16 bits\n");
    }
  }
  *biasB = 1;
#endif
  }

  //AM Specific bias restoration:
  //printf("inScaleFactor = %d\n",inScaleFactor);
  for (int32_t idx = 0; idx < dataSize; idx++)
  {
    tidl_printf(2,"Bias B= %d && dstPtr[%d] = %d && srcPtr[%d] = %d\n",*biasB,idx,dstPtr[idx],idx,srcPtr[idx]);
    dstPtr[idx] *= (*(biasB));
  }

  return;
}



//
/**
 * @brief The function does the bias split for the conv parameters
 *
 * @param srcPtr : Pointer to the source buffer
 * @param dstPtr : Pointer to the destination buffer
 * @param biasB : Pointer to the bias buffer
 * @param dataSize : no of output channels
 * @param inScaleFactor : scale factor for the input
 * @param satLow : Min value of the saturation
 * @param satHigh : Max value of the saturation
 * @param biasBMax : the max value of the bias values
 * @param inFeatSign : the sign of the input feature map
 */
template <class Tdst, class Tsrc>
void TIDL_conv2dBiasSplitWithFixedBiasB(Tsrc * srcPtr,
                                        Tdst * dstPtr,
                                        int32_t * biasB,
                                        int32_t dataSize,
                                        float32_tidl inScaleFactor,
                                        int32_t satLow,
                                        int32_t satHigh,
                                        int32_t biasBMax,
                                        int32_t inFeatSign)
{
  if(inScaleFactor == 0)
  {
    for (int32_t idx = 0; idx < dataSize; idx++)
    {
        dstPtr[idx] = 0;
    }
  }
  else
  {
    int32_t orgBiasVal, biasVal = 0;
    if (inFeatSign == 1)
    {
      biasBMax = biasBMax / 2;
    }

    int32_t min, max, temp;
    if (inFeatSign == 1)
    {
      max = biasBMax;
    }
    else
    {
      max = biasBMax/2;
    }
    temp = max+1;
    min = -1 * temp;

    for (int32_t idx = 0; idx < dataSize; idx++)
    {
      orgBiasVal = srcPtr[idx] * inScaleFactor;
      biasVal = (orgBiasVal < satLow) ? satLow : orgBiasVal;
      biasVal = (biasVal >  satHigh) ? satHigh : biasVal;
      temp = (biasVal / (*biasB));
      temp = (temp < min) ? min : temp;
      temp = (temp > max) ? max : temp;
      dstPtr[idx] = temp;
    }
  }
  return;
}

/**
 * @brief The function does the bias split for the conv parameters
 *
 * @param srcPtr : Pointer to the source buffer
 * @param dstPtr : Pointer to the destination buffer
 * @param biasB : Pointer to the bias buffer
 * @param dataSize : no of output channels
 * @param inScaleFactor : scale factor for the input
 * @param satLow : Min value of the saturation
 * @param satHigh : Max value of the saturation
 * @param biasBMax : the max value of the bias values
 * @param inFeatSign : the sign of the input feature map
 */
template <class Tdst, class Tsrc>
void TIDL_conv2dBiasSplit(Tsrc * srcPtr, Tdst * dstPtr, int32_t * biasB, int32_t dataSize, float32_tidl inScaleFactor, int32_t satLow, int32_t satHigh, int32_t biasBMax, int32_t inFeatSign)
{
  if(inScaleFactor == 0)
  {
    for (int32_t idx = 0; idx < dataSize; idx++)
    {
        dstPtr[idx] = 0;
    }
    *biasB = 0;
  }
  else
  {
#if USE_16BIT_BIAS_FOR_8BIT_MODE
    int32_t orgBiasVal, biasVal = 0;
    int32_t biasBMaxNew = biasBMax;

    if (inFeatSign == 1)
    {
      biasBMaxNew = biasBMax / 2;
    }
    if (inScaleFactor > biasBMaxNew)
    {
      int32_t temp;
      int32_t biasAMin, biasAMax;

      biasAMax = biasBMax/2;

      temp = biasAMax+1;
      biasAMin = -1 * temp;

      if ( (biasBMax == 0xFF) && (satHigh == TIDL_SAT_HI_INT32) )
      {
        /* Not the cleanest way to handle but to avoid function signature
        change doing it this way. This condition indicates signed input with
        16 bit processing and hence biasB is limited to 0xFF. In this case
        biasA can still be 16 bits*/
        biasAMin = std::numeric_limits<int16_t>::lowest();
        biasAMax = std::numeric_limits<int16_t>::max();
      }


      for (int32_t idx = 0; idx < dataSize; idx++)
      {
        orgBiasVal = srcPtr[idx] * inScaleFactor;
        biasVal = (orgBiasVal < satLow) ? satLow : orgBiasVal;
        biasVal = (biasVal >  satHigh) ? satHigh : biasVal;
        temp = (biasVal / biasBMaxNew);
        temp = (temp < biasAMin) ? biasAMin : temp;
        temp = (temp > biasAMax) ? biasAMax : temp;
        dstPtr[idx] = temp;
      }
      *biasB = biasBMaxNew;
   }
    else if (inScaleFactor > 1)
    {
      for (int32_t idx = 0; idx < dataSize; idx++)
      {
        dstPtr[idx] = srcPtr[idx];
      }
      *biasB = inScaleFactor;
    }
    else
    {
      for (int32_t idx = 0; idx < dataSize; idx++)
      {
        dstPtr[idx] = srcPtr[idx]* inScaleFactor;
      }
      *biasB = 1;
    }

#else
    int32_t biasVal = 0;
    int32_t orgBiasVal = 0;
    for (int32_t idx = 0; idx < dataSize; idx++)
    {
      orgBiasVal = srcPtr[idx] * inScaleFactor;
      biasVal = orgBiasVal < satLow ? satLow : orgBiasVal;
      biasVal = biasVal >  satHigh ? satHigh : biasVal;
      dstPtr[idx] = biasVal;
      if (orgBiasVal != biasVal)
      {
        tidl_printf(0,"Conv2D bias is greater than 16 bits\n");
      }
    }
    *biasB = 1;
#endif
  }
  return;
}

/**
 * @brief Function is to prepare transfer prooperties
 *
 * @param trMem  : pointer to trasfer memory
 */
void  TIDL_prePareMemcpyTr
(
  void * trMem
)
{
  DmaUtilsAutoInc3d_TrPrepareParam trPrepParams;
  DmaUtilsAutoInc3d_TransferProp      transferProp;
  trPrepParams.channelId = TIDL_DMA_CHANNEL_MEMCPY;
  trPrepParams.numTRs    = 1U;
  trPrepParams.trMem          = (uint8_t*)trMem;
  trPrepParams.trMemSize  = 64U;


  transferProp.syncType = (uint32_t)DMAUTILSAUTOINC3D_SYNC_2D;

  transferProp.circProp.circDir = (uint8_t)DMAUTILSAUTOINC3D_CIRCDIR_DST;
  transferProp.circProp.circSize1 = 0;
  transferProp.circProp.circSize2 = 0;
  transferProp.circProp.addrModeIcnt0 = (uint8_t)DMAUTILSAUTOINC3D_ADDR_LINEAR;
  transferProp.circProp.addrModeIcnt1 = (uint8_t)DMAUTILSAUTOINC3D_ADDR_LINEAR;
  transferProp.circProp.addrModeIcnt2 = (uint8_t)DMAUTILSAUTOINC3D_ADDR_LINEAR;
  transferProp.circProp.addrModeIcnt3 = (uint8_t)DMAUTILSAUTOINC3D_ADDR_LINEAR;

  transferProp.transferDim.sicnt0 = 1;
  transferProp.transferDim.sicnt1 =1;
  transferProp.transferDim.sicnt2 =1;
  transferProp.transferDim.sicnt3 =1;
  transferProp.transferDim.sdim1= 1;

  transferProp.transferDim.dicnt0 = 1;
  transferProp.transferDim.dicnt1 = 1;
  transferProp.transferDim.dicnt2 =1;
  transferProp.transferDim.dicnt3 =1;
  transferProp.transferDim.ddim1 = 1;

  transferProp.ioPointers.srcPtr = (uint8_t *)NULL;
  transferProp.ioPointers.dstPtr = (uint8_t *)NULL;

  (void)DmaUtilsAutoInc3d_prepareTr( &trPrepParams,&transferProp);
}

/*
 * Async mode of transfer when Autoincrement TR is not possible
 * and src and dst needs to be accessed in 2D pattern
 *
 * This API uses the same channel as TIDL_memcpy2D which is
 * a blocking call and therefore the following sequence should
 * be avoided at all costs
 *
 * TIDL_memcpy2DAsyncTrigger();
 * TIDL_memcpy2D();
 * TIDL_memcpy2DAsyncWait();
 *
 * TODO: Fix this in a nice way
 */

/**
 * @brief Memory copy for Async trigger
 *
 * @param dstPtr : Pointer to dst memory
 * @param srcPtr : Pointer to src memory
 * @param width : Width of the memory copy
 * @param height : Height of the memory copy
 * @param dstStride : Stride of the dst buffer
 * @param srcStride : Stride of the src buffer
 * @param dmaUtilsContext : memory for dmaUtilsContext
 * @param trMem : pointer to transfer memory
 */
void TIDL_memcpy2DAsyncTrigger(const void   *dstPtr,
    const void   *srcPtr,
    uint16_t     width,
    uint16_t     height,
    int32_t      dstStride,
    int32_t      srcStride,
    void * dmaUtilsContext,
    int32_t   convert16bitTo8Bit,
    uint8_t * trMem)
{
  if(dmaUtilsContext != NULL)
  {
    DmaUtilsAutoInc3d_TrPrepareParam trPrepParam;
    int32_t fmtFlagsBkp;
    uint32_t convertMask = DMAUTILSAUTOINC3D_ADDRCONVERTMASK_SRCADDR |
                            DMAUTILSAUTOINC3D_ADDRCONVERTMASK_DSTADDR;
    trPrepParam.channelId = TIDL_DMA_CHANNEL_MEMCPY;
    trPrepParam.numTRs    = 1;
    trPrepParam.trMemSize = 64;
    trPrepParam.trMem     = trMem;



    CSL_UdmapTR * tr;
    tr = (CSL_UdmapTR *) trMem;
    tr->addr   = (uintptr_t)srcPtr;
    tr->daddr  = (uintptr_t)dstPtr;
    tr->icnt0  = width;
    tr->icnt1  = height;
    tr->dicnt0 = width;
    tr->dicnt1 = height;
    tr->dim1   = srcStride;
    tr->ddim1  = dstStride;
    fmtFlagsBkp = (tr->fmtflags);
    if ( convert16bitTo8Bit == 1)
    {
      /* Enable conversion from 16 bit to 8 bit */
      tr->fmtflags = (tr->fmtflags) | CSL_FMK(UDMAP_TR_FMTFLAGS_ELYPE, CSL_UDMAP_TR_FMTFLAGS_ELYPE_2_1);
    }

    DmaUtilsAutoInc3d_convertTrVirtToPhyAddr(dmaUtilsContext, &trPrepParam, convertMask);

    /* Re-use the same contexxt as feat for memcpy */
    (void)DmaUtilsAutoInc3d_configure(dmaUtilsContext, TIDL_DMA_CHANNEL_MEMCPY, trMem, 1U);
    tr->fmtflags = fmtFlagsBkp;

    (void)DmaUtilsAutoInc3d_trigger(dmaUtilsContext, TIDL_DMA_CHANNEL_MEMCPY);
  }
  else
  {

    uint32_t rowCnt;

    tidl_printf(0, "Calling TIDL_memcpy2DTrigger with dmaUtilsContext == NULL\n");

    for (rowCnt = 0; rowCnt < height; rowCnt++)
    {
      memcpy((uint8_t *)dstPtr + rowCnt * dstStride, (uint8_t *)srcPtr + rowCnt * srcStride, width);
    }
  }
}

/**
 * @brief Function for memory copy Async wait
 *
 * @param dmaUtilsContext : memory for dmaUtilsContext
 */
void TIDL_memcpy2DAsyncWait(void * dmaUtilsContext)
{
  if(dmaUtilsContext != NULL)
  {
    DmaUtilsAutoInc3d_wait(dmaUtilsContext, TIDL_DMA_CHANNEL_MEMCPY);
    //DmaUtilsAutoInc3d_deconfigure(dmaUtilsContext, TIDL_DMA_CHANNEL_MEMCPY, trMem, 1U);
  }
}

/**
 * @brief Memory copy for trigger
 *
 * @param dstPtr : Pointer to dst memory
 * @param srcPtr : Pointer to src memory
 * @param width : Width of the memory copy
 * @param height : Height of the memory copy
 * @param dstStride : Stride of the dst buffer
 * @param srcStride : Stride of the src buffer
 * @param dmaUtilsContext : memory for dmaUtilsContext
 * @param trMem : pointer to transfer memory
 */
void  TIDL_memcpy2D
(
  const void   *dstPtr,
  const void   *srcPtr,
  uint16_t     width,
  uint16_t     height,
  int32_t      dstStride,
  int32_t      srcStride,
  void * dmaUtilsContext,
  uint8_t * trMem
  )
{
  if(dmaUtilsContext != NULL)
  {
    DmaUtilsAutoInc3d_TrPrepareParam trPrepParam;
    uint32_t convertMask = DMAUTILSAUTOINC3D_ADDRCONVERTMASK_SRCADDR |
                            DMAUTILSAUTOINC3D_ADDRCONVERTMASK_DSTADDR;
    trPrepParam.channelId = TIDL_DMA_CHANNEL_MEMCPY;
    trPrepParam.numTRs    = 1;
    trPrepParam.trMemSize = 64;
    trPrepParam.trMem     = trMem;

    CSL_UdmapTR * tr;
    tr = (CSL_UdmapTR *) trMem;
    tr->addr   = (uintptr_t)srcPtr;
    tr->daddr  = (uintptr_t)dstPtr;
    tr->icnt0  = width;
    tr->icnt1  = height;
    tr->dicnt0 = width;
    tr->dicnt1 = height;
    tr->dim1   = srcStride;
    tr->ddim1  = dstStride;

    DmaUtilsAutoInc3d_convertTrVirtToPhyAddr(dmaUtilsContext, &trPrepParam, convertMask);

    /* Re-use the same contexxt as feat for memcpy */
    (void)DmaUtilsAutoInc3d_configure(dmaUtilsContext, TIDL_DMA_CHANNEL_MEMCPY, trMem, 1U);

    (void)DmaUtilsAutoInc3d_trigger(dmaUtilsContext, TIDL_DMA_CHANNEL_MEMCPY);
    DmaUtilsAutoInc3d_wait(dmaUtilsContext, TIDL_DMA_CHANNEL_MEMCPY);

//  DmaUtilsAutoInc3d_deconfigure(dmaUtilsContext, TIDL_DMA_CHANNEL_MEMCPY, trMem, 1U);
  }
  else
  {
    uint32_t rowCnt;

    for (rowCnt = 0; rowCnt < height; rowCnt++)
    {
      memcpy((uint8_t *)dstPtr + rowCnt * dstStride, (uint8_t *)srcPtr + rowCnt * srcStride, width);
    }
  }
}

template void TIDL_conv2dBiasSplit<int16_t, int16_t>(int16_t * srcPtr, int16_t * dstPtr, int32_t * biasB, int32_t dataSize, float32_tidl inScaleFactor, int32_t satLow, int32_t satHigh, int32_t biasBMax, int32_t inFeatSign);
template void TIDL_conv2dBiasSplit<int8_t, int16_t>(int16_t * srcPtr, int8_t * dstPtr, int32_t * biasB, int32_t dataSize, float32_tidl inScaleFactor, int32_t satLow, int32_t satHigh, int32_t biasBMax, int32_t inFeatSign);
template void TIDL_conv2dBiasSplit<int32_t, int16_t>(int16_t * srcPtr, int32_t * dstPtr, int32_t * biasB, int32_t dataSize, float32_tidl inScaleFactor, int32_t satLow, int32_t satHigh, int32_t biasBMax, int32_t inFeatSign);

template void TIDL_conv2dBiasSplit<int16_t, int32_t>(int32_t * srcPtr, int16_t * dstPtr, int32_t * biasB, int32_t dataSize, float32_tidl inScaleFactor, int32_t satLow, int32_t satHigh, int32_t biasBMax, int32_t inFeatSign);
template void TIDL_conv2dBiasSplit<int8_t, int32_t>(int32_t * srcPtr, int8_t * dstPtr, int32_t * biasB, int32_t dataSize, float32_tidl inScaleFactor, int32_t satLow, int32_t satHigh, int32_t biasBMax, int32_t inFeatSign);
template void TIDL_conv2dBiasSplit<int32_t, int32_t>(int32_t * srcPtr, int32_t * dstPtr, int32_t * biasB, int32_t dataSize, float32_tidl inScaleFactor, int32_t satLow, int32_t satHigh, int32_t biasBMax, int32_t inFeatSign);

//AM
template void TIDL_AM_conv2dBiasSplit<int32_t, int16_t>(int16_t * srcPtr, int32_t * dstPtr, int32_t * biasB, int32_t dataSize, float32_tidl inScaleFactor, int32_t satLow, int32_t satHigh, int32_t biasBMax, int32_t inFeatSign);
//
template void TIDL_conv2dBiasSplitWithFixedBiasB<int16_t, int16_t>(int16_t * srcPtr, int16_t * dstPtr, int32_t * biasB, int32_t dataSize, float32_tidl inScaleFactor, int32_t satLow, int32_t satHigh, int32_t biasBMax, int32_t inFeatSign);
template void TIDL_conv2dBiasSplitWithFixedBiasB<int8_t, int16_t>(int16_t * srcPtr, int8_t * dstPtr, int32_t * biasB, int32_t dataSize, float32_tidl inScaleFactor, int32_t satLow, int32_t satHigh, int32_t biasBMax, int32_t inFeatSign);
template void TIDL_conv2dBiasSplitWithFixedBiasB<int32_t, int16_t>(int16_t * srcPtr, int32_t * dstPtr, int32_t * biasB, int32_t dataSize, float32_tidl inScaleFactor, int32_t satLow, int32_t satHigh, int32_t biasBMax, int32_t inFeatSign);

template void TIDL_conv2dBiasSplitWithFixedBiasB<int16_t, int32_t>(int32_t * srcPtr, int16_t * dstPtr, int32_t * biasB, int32_t dataSize, float32_tidl inScaleFactor, int32_t satLow, int32_t satHigh, int32_t biasBMax, int32_t inFeatSign);
template void TIDL_conv2dBiasSplitWithFixedBiasB<int8_t, int32_t>(int32_t * srcPtr, int8_t * dstPtr, int32_t * biasB, int32_t dataSize, float32_tidl inScaleFactor, int32_t satLow, int32_t satHigh, int32_t biasBMax, int32_t inFeatSign);
template void TIDL_conv2dBiasSplitWithFixedBiasB<int32_t, int32_t>(int32_t * srcPtr, int32_t * dstPtr, int32_t * biasB, int32_t dataSize, float32_tidl inScaleFactor, int32_t satLow, int32_t satHigh, int32_t biasBMax, int32_t inFeatSign);

void * my_malloc(int32_t size)
{
  void *ptr;

  ptr = malloc(size);
  return ptr;
}

void my_free(void *ptr)
{
  //fprintf(fpAlloc, "Free: Ptr: %0x\n",ptr);
  //fflush(fpAlloc);
  free(ptr);
}

/**
 * @brief Function to initilize the debug parameters
 *
 * @param traceLogLevel : debug  level for trace log
 * @param traceWriteLevel : debug level for trace write
 * @param tifVprintfFuncPtr : Pointer to print buffer
 * @param writeBinFuncPtr : Pointer to write bins
 * @param readBinFuncPtr : Pointer to the read bins
 * @param traceBaseName : Name of the trace buffer
 * @return int32_t : Returns SUCCESS or FAIL
 */
int32_t TIDL_initDebugTraceParams(int32_t traceLogLevel, int32_t traceWriteLevel, tidlVprintf_t tifVprintfFuncPtr, tidlWriteBinToFile_t writeBinFuncPtr, tidlReadBinFromFile_t readBinFuncPtr, void * traceBaseName)
{
  int32_t status = TIDL_SUCCESS;

  if (((traceLogLevel > 0) && (tifVprintfFuncPtr == NULL)) ||
      ((traceWriteLevel > 0) && (writeBinFuncPtr == NULL)))
  {
    status = TIDL_ERR_FAILURE;
  }

  if(status == TIDL_SUCCESS)
  {
    tidlWriteBinToFile = (tidlWriteBinToFile_t)writeBinFuncPtr;
    tidlReadBinFromFile = (tidlReadBinFromFile_t)readBinFuncPtr;
    tidlVprintf = (tidlVprintf_t)tifVprintfFuncPtr;
    tidlLogLevel = traceLogLevel;
    tidlWriteLevel = traceWriteLevel;
    tidlTraceBaseName = traceBaseName;
  }
  return status;
}


void tidl_printf(int32_t traceLevel, const char *format, ...)
{
  va_list args;
  if (traceLevel < tidlLogLevel)
  {
    (void)va_start(args, format);
    (void)tidlVprintf(format, args);
    va_end(args);
  }
}

int32_t tidl_getTraceLogLevel()
{
  return tidlLogLevel;
}

float32_tidl * tidl_convertParamsToFloat(void * buf,
                                               int32_t numElements,
                                               int32_t dataBits,
                                               float32_tidl dataScale,
                                               void * scratchPtr,
                                               int32_t scratchSize)
{
  float32_tidl * weightPtrFloat = NULL;

  if (( numElements  * sizeof(float32_tidl) <= scratchSize ) &&
      (dataScale != 0.0) )
  {
    int32_t i;
    weightPtrFloat = (float32_tidl *)scratchPtr;

    for ( i = 0; i < numElements; i++ )
    {
      if (dataBits <= 8 )
      {
        int8_t * inPtr = (int8_t *)buf;
        weightPtrFloat[i] = (float32_tidl)inPtr[i] / dataScale;

      }
      else if (dataBits <= 16 )
      {
        int16_t * inPtr = (int16_t *)buf;
        weightPtrFloat[i] = ((float32_tidl)inPtr[i]) / dataScale;
      }
      else if (dataBits == 32 )
      {
        float32_tidl * inPtr = (float32_tidl *)buf;
        weightPtrFloat[i] = inPtr[i] / dataScale;
      }
      else
      {
        tidl_printf(0,"Invalid dataBits in tidl_convertParamsToFloat\n");
        weightPtrFloat = NULL;
        break;
      }
    }
  }

  return weightPtrFloat;
}


int32_t tidl_writeTraceParamBuf(sTIDL_Network_t * net,
                                      sTIDL_AlgLayer_t     * algLayer,
                                      sTIDL_Layer_t        * tidlLayer,
                                      int32_t dataBuffId,
                                      int8_t * orgScratchPtr,
                                      uint32_t orgScratchSize)
{
  int32_t status = TIDL_SUCCESS;

  if (tidlWriteLevel == 4)
  {
    if ( ( tidlLayer->layerType == TIDL_ConvolutionLayer ) ||
         ( tidlLayer->layerType == TIDL_Deconv2DLayer) ||
         ( tidlLayer->layerType == TIDL_BatchNormLayer) ||
         ( tidlLayer->layerType == TIDL_BatchReshapeLayer) ||
         ( tidlLayer->layerType == TIDL_InnerProductLayer))
    {
      int8_t * scratchPtr =  (int8_t *)(orgScratchPtr  + TRACE_STRINGS_MEM_SIZE);
      int8_t * traceDumpName = (int8_t *)(orgScratchPtr);
      int8_t * traceStringName = (int8_t *)(orgScratchPtr  + (1*TRACE_STRING_SIZE));
      int8_t * DataIDString  = (int8_t *)(orgScratchPtr  + (2*TRACE_STRING_SIZE));
      int32_t scratchSize =  orgScratchSize - TRACE_STRINGS_MEM_SIZE;
      int32_t dataSize = 0;

      if(tidlTraceBaseName != NULL)
      {
        (void)strcpy((char *)traceDumpName, (char *)tidlTraceBaseName);
      }
      else
      {
        (void)strcpy((char *)traceDumpName, (char *)"");
      }

      (void)sprintf((char *)DataIDString, "%04d", dataBuffId);
      (void)strcat((char *)traceDumpName, (char *)DataIDString);
      (void)strcat((char *)traceDumpName, "_");

      if((tidlLayer->layerType == TIDL_ConvolutionLayer) || (tidlLayer->layerType == TIDL_Deconv2DLayer))
      {
        void * weightPtr = ((int8_t *)net + tidlLayer->layerParams.convParams.weights);
        float32_tidl * weightPtrFloat;

        dataSize = ( tidlLayer->layerParams.convParams.numOutChannels *
                   tidlLayer->layerParams.convParams.numInChannels *
                   tidlLayer->layerParams.convParams.kernelW *
                   tidlLayer->layerParams.convParams.kernelH )/
                   tidlLayer->layerParams.convParams.numGroups;

        weightPtrFloat = tidl_convertParamsToFloat(weightPtr,
                                                        dataSize,
                                                        tidlLayer->weightsElementSizeInBits,
                                                        tidlLayer->layerParams.convParams.weightScale,
                                                        scratchPtr,
                                                        scratchSize);

        if ( weightPtrFloat == NULL )
        {
          status = TIDL_ERR_FAILURE;
          tidl_printf(0, " In sufficient Memory to write Weight Traces\n");
          goto Exit;
        }
        sprintf((char *)traceStringName, "%sweights_float.bin", traceDumpName);


        (void)tidlWriteBinToFile((const char *)traceStringName, weightPtrFloat, dataSize*sizeof(float32_tidl));

        if (tidlLayer->layerParams.convParams.enableBias)
        {
          float32_tidl * biasPtrFloat;
          void * biasPtr = ((int8_t *)(net) + tidlLayer->layerParams.convParams.bias);
          dataSize = tidlLayer->layerParams.convParams.numOutChannels;

          //:TODO: Ideally this we should dump after bias split
          biasPtrFloat = tidl_convertParamsToFloat(biasPtr,
                                                  dataSize,
                                                  tidlLayer->weightsElementSizeInBits,
                                                  tidlLayer->layerParams.convParams.biasScale,
                                                  scratchPtr,
                                                  scratchSize);
          if ( biasPtrFloat == NULL )
          {
            status = TIDL_ERR_FAILURE;
            tidl_printf(0, " In sufficient Memory to write Bias Traces\n");

            goto Exit;
          }

          sprintf((char *)traceStringName, "%sbias_float.bin", traceDumpName);
         (void)tidlWriteBinToFile((const char *)traceStringName, biasPtrFloat, dataSize*sizeof(float32_tidl));

        }
      }

    }
  }
Exit:
  return status;
}


/**
 * @brief Function to write traces from data buffer
 *
 * @param ptr : Pointer to the data buffer
 * @param net : tidl network structure
 * @param dataBuffId : id of the databuffer
 * @param orgScratchPtr : pointer to the scratch buffer
 * @param orgScratchSize : Size of the scratch buffer
 * @param currentLineWriteOffset : Offset to the current line
 * @param currentNumLines : No of lines for writting
 * @param currChPitch : current channel pitch
 */
int32_t tidl_writeTraceDataBuf(int8_t * ptr, sTIDL_Network_t * net, int32_t dataBuffId, int8_t * orgScratchPtr, uint32_t orgScratchSize, int32_t currentLineWriteOffset, int32_t currentNumLines, int32_t currChPitch)
{
  int32_t status = TIDL_SUCCESS;
  if (tidlWriteLevel > 0)
  {
    int8_t * scratchPtr =  (int8_t *)(orgScratchPtr  + TRACE_STRINGS_MEM_SIZE);
    int8_t * traceDumpName = (int8_t *)(orgScratchPtr);
    int8_t * traceBaseName = (int8_t *)(orgScratchPtr  + (1*TRACE_STRING_SIZE));
    int8_t * DataIDString  = (int8_t *)(orgScratchPtr  + (2*TRACE_STRING_SIZE));
    int32_t scratchSize =  orgScratchSize - TRACE_STRINGS_MEM_SIZE;
    int32_t tidlWriteLevelOrig = tidlWriteLevel;
    sTIDL_DataParams_t * dataBuffParam;
    dataBuffParam = TIDL_getDataParams(net, dataBuffId);

    /* for trace Level 4 enable trace level 3 by default */
    if ( tidlWriteLevel == 4 )
    {
      tidlWriteLevel = 3;
    }
    if (dataBuffParam != NULL)
    {    
    if ((dataBuffParam->numBatchW>1 || dataBuffParam->numBatchH>1)&& (dataBuffParam->dimValues[0] == 1))
    {
      int32_t elementSizeBytes;
      elementSizeBytes = TIDL_getDatElementSize(dataBuffParam->elementType);
      if(elementSizeBytes == 4)
      {
        tidlWriteLevel = tidlWriteLevel == 3 ? 1 : tidlWriteLevel;
      }
      
      int32_t outWidth = (dataBuffParam->dimValues[3] - dataBuffParam->batchPadW * (dataBuffParam->numBatchW -1))/dataBuffParam->numBatchW;
      int32_t outHeight = dataBuffParam->dimValues[2];
      int32_t outChannels = dataBuffParam->dimValues[1];
      int32_t outNumBatches = dataBuffParam->numBatchW;
      int32_t dataSize = outWidth * outHeight * outChannels * outNumBatches;
      int32_t linePitch = dataBuffParam->pitch[TIDL_LINE_PITCH];
      int32_t chPitch = dataBuffParam->pitch[TIDL_CHANNEL_PITCH];
      int32_t batchPitch  = dataBuffParam->pitch[TIDL_ROI_PITCH];
      int32_t inBlkPitchW        =  outWidth + dataBuffParam->batchPadW; 
      int32_t paddeOutSize = linePitch * (dataBuffParam->dimValues[2] + (dataBuffParam->padH * 2) + 1);
      int32_t padedDataSize =  dataBuffParam->dimValues[0] * dataBuffParam->dimValues[1] * paddeOutSize;
      int8_t * outPtr        = (int8_t *)scratchPtr;
      int8_t * outWithPadPtr = (int8_t *)(scratchPtr + (dataSize*elementSizeBytes));
      float32_tidl  * floatPtr = (float32_tidl  *)(scratchPtr + (dataSize*elementSizeBytes));
      int32_t i;
      int32_t j, k1;
      int32_t batchIdx;
      int32_t batchOffset;      
      int32_t offset;
      int32_t totalSize = 0;

      if (tidlWriteLevel == 1)
      {
        totalSize = (dataSize*elementSizeBytes);
      }
      else if (tidlWriteLevel == 2)
      {
        totalSize = ((dataSize*elementSizeBytes) + (padedDataSize*elementSizeBytes));
      }
      else if (tidlWriteLevel == 3)
      {
        totalSize = (dataSize*elementSizeBytes) + ((int32_t)sizeof(float32_tidl)*dataSize);
      }
      else
      {
        tidl_printf(0, "Un supported tidlWriteLevel \n");
        status = TIDL_ERR_FAILURE;
      }

      if(dataBuffParam->elementType == TIDL_SinglePrecFloat)
      {
        float32_tidl curMin = FLT_MAX, curMax = -FLT_MAX;
        TIDL_TensorMinMax((float32_tidl*)ptr, dataBuffParam, &curMin, &curMax);
        tidl_printf(0, " %3d %10.5f %10.5f %10.5f %d\n", dataBuffId, dataBuffParam->tensorScale, curMin, curMax, dataBuffParam->elementType);
      }
      else
      {
        tidl_printf(0, " %3d %10.5f %10.5f %10.5f %d\n", dataBuffId, dataBuffParam->tensorScale, dataBuffParam->minTensorValue, dataBuffParam->maxTensorValue, dataBuffParam->elementType);
      }


      if ((totalSize > scratchSize) && (status == TIDL_SUCCESS))
      {
        tidl_printf(0, " In sufficient Memory to write Traces\n");
        status = TIDL_ERR_FAILURE;
      }

      if(status == TIDL_SUCCESS)
      {
        if(tidlTraceBaseName != NULL)
        {
          (void)strcpy((char *)traceDumpName, (char *)tidlTraceBaseName);
        }
        else
        {
          (void)strcpy((char *)traceDumpName, (char *)"");
        }

        (void)sprintf((char *)DataIDString, "%04d", dataBuffId);
        (void)strcat((char *)traceDumpName, (char *)DataIDString);
        (void)strcat((char *)traceDumpName, "_");
        (void)sprintf((char *)DataIDString, "%05d", outNumBatches);
        (void)strcat((char *)traceDumpName, (char *)DataIDString);
        (void)strcat((char *)traceDumpName, "_");
        (void)sprintf((char *)DataIDString, "%05d", outChannels);
        (void)strcat((char *)traceDumpName, (char *)DataIDString);
        (void)strcat((char *)traceDumpName, "_");
        (void)sprintf((char *)DataIDString, "%05d", outWidth);
        (void)strcat((char *)traceDumpName, (char *)DataIDString);
        (void)strcat((char *)traceDumpName, "x");
        (void)sprintf((char *)DataIDString, "%05d", outHeight);
        (void)strcat((char *)traceDumpName, (char *)DataIDString);
        (void)strcpy((char *)traceBaseName, (char *)traceDumpName);

        if(currentLineWriteOffset != 0)
        {
          (void)strcpy((char *)traceDumpName, (char *)traceBaseName);
          (void)strcat((char *)traceDumpName, ".y");

          if(elementSizeBytes == 4)
          {
            (void)strcpy((char *)traceDumpName, (char *)traceBaseName);
            (void)strcat((char *)traceDumpName, "_float.bin");
          }

          (void)tidlReadBinFromFile((const char *)traceDumpName, outPtr, dataSize*elementSizeBytes);
          if (tidlWriteLevel == 2)
          {
            (void)strcpy((char *)traceDumpName, (char *)traceBaseName);
            (void)strcat((char *)traceDumpName, "_paded.y");
            (void)tidlReadBinFromFile((const char *)traceDumpName, outWithPadPtr, padedDataSize*elementSizeBytes);
          }

          if (tidlWriteLevel == 3)
          {
            (void)strcpy((char *)traceDumpName, (char *)traceBaseName);
            (void)strcat((char *)traceDumpName, "_float.bin");
            (void)tidlReadBinFromFile((const char *)traceDumpName, floatPtr, dataSize*((int32_t)sizeof(float32_tidl)));
          }
        }

      for (batchIdx = 0; batchIdx < 1; batchIdx++)
      {
        batchOffset =  outChannels  * outWidth* outHeight;

        for (j = 0; j < outChannels; j++)
        {
          for (i = 0; i < currentNumLines; i++)
          {
            for (k1 = 0; k1 < outNumBatches; k1++)
            {
              offset = (j*outHeight * outWidth) +
                       ((i + currentLineWriteOffset)*outWidth) + k1*batchOffset;            
            if (dataBuffParam->elementType == 0)
            {
              uint8_t * src, *dst;
              int32_t k;
              src = (uint8_t *)(ptr + (k1 * inBlkPitchW) + (j*currChPitch) + (i*linePitch) + (dataBuffParam->padH*linePitch) + dataBuffParam->padW);
              dst = (uint8_t *)(outPtr + offset);
              for (k = 0; k < outWidth; k++)
              {
                dst[k] = src[k];
                if (tidlWriteLevel == 3)
                {
                  floatPtr[offset + k] = ((float32_tidl)src[k]) / (dataBuffParam->tensorScale);
                }
              }
            }
            else if (dataBuffParam->elementType == 1)
            {
              int8_t * src;
              int8_t * dst;
              int32_t k;
              src = (ptr + (k1 * inBlkPitchW) + (j*currChPitch) + (i*linePitch) + (dataBuffParam->padH*linePitch) + dataBuffParam->padW);
              dst = (outPtr + offset);
              for (k = 0; k < outWidth; k++)
              {
                //dst[k] = src[k] + 128;
                dst[k] = src[k];
                if (tidlWriteLevel == 3)
                {
                  floatPtr[offset + k] = ((float32_tidl)src[k]) / (dataBuffParam->tensorScale);
                }
              }
            }
            else if (dataBuffParam->elementType == 2)
            {
              uint16_t * src;
              uint16_t * dst;
              int32_t k;
              src = (((uint16_t *)ptr) + batchIdx * batchPitch + (j*currChPitch) + (i*linePitch) + (dataBuffParam->padH*linePitch) + dataBuffParam->padW);
              dst = (((uint16_t *)outPtr) + offset);
              for (k = 0; k < dataBuffParam->dimValues[3]; k++)
              {
                dst[k] = src[k];
                if (tidlWriteLevel == 3)
                {
                  floatPtr[offset + k] = ((float32_tidl)src[k]) / (dataBuffParam->tensorScale);
                }
              }
            }
            else if (dataBuffParam->elementType == 3)
            {
              int16_t * src;
              int16_t * dst;
              int32_t k;
              src = (((int16_t *)ptr) + batchIdx * batchPitch + (j*currChPitch) + (i*linePitch) + (dataBuffParam->padH*linePitch) + dataBuffParam->padW);
              dst = (((int16_t *)outPtr) + offset);
              for (k = 0; k < dataBuffParam->dimValues[3]; k++)
              {
                //dst[k] = src[k] + 128;
                dst[k] = src[k];
                if (tidlWriteLevel == 3)
                {
                  floatPtr[offset + k] = ((float32_tidl)src[k]) / (dataBuffParam->tensorScale);
                }
              }
            }
            else
            {
              (void)memcpy((outPtr + ((batchIdx * dataBuffParam->dimValues[1]  * dataBuffParam->dimValues[2] * dataBuffParam->dimValues[3])
              + (j*dataBuffParam->dimValues[2] * dataBuffParam->dimValues[3]) +
                (i*dataBuffParam->dimValues[3]))*elementSizeBytes),
                (ptr + (((batchIdx * batchPitch) + (j*currChPitch) + (i*linePitch) + (dataBuffParam->padH*linePitch) +
                  dataBuffParam->padW)*elementSizeBytes)),
                (size_t)(dataBuffParam->dimValues[3] * elementSizeBytes));
            }
            

          }
          if (tidlWriteLevel == 2)
          {
            if((currentNumLines != dataBuffParam->dimValues[2]) && (currentLineWriteOffset != 0))
            {
              tidl_printf(0, "tidlWriteLevel = 2 with ST is not supported now \n");
              status = IALG_EFAIL;
            }
            else
            {
              (void)memcpy((outWithPadPtr + batchIdx *dataBuffParam->dimValues[1] * paddeOutSize + j*paddeOutSize), (ptr + (batchIdx * batchPitch) + (j*chPitch)), paddeOutSize);
            }
          }
        }
      }
        (void)strcpy((char *)traceDumpName, (char *)traceBaseName);
        (void)strcat((char *)traceDumpName, ".y");
        if(elementSizeBytes == 4)
        {
          (void)strcpy((char *)traceDumpName, (char *)traceBaseName);
          (void)strcat((char *)traceDumpName, "_float.bin");
        }
        (void)tidlWriteBinToFile((const char *)traceDumpName, outPtr, dataSize*elementSizeBytes);
        if (tidlWriteLevel == 2)
        {
          (void)strcpy((char *)traceDumpName, (char *)traceBaseName);
          (void)strcat((char *)traceDumpName, "_paded.y");
          (void)tidlWriteBinToFile((const char *)traceDumpName, outWithPadPtr, padedDataSize*elementSizeBytes);
        }

        if (tidlWriteLevel == 3)
        {
          (void)strcpy((char *)traceDumpName, (char *)traceBaseName);
          (void)strcat((char *)traceDumpName, "_float.bin");
          (void)tidlWriteBinToFile((const char *)traceDumpName, floatPtr, dataSize*((int32_t)sizeof(float32_tidl)));
        }
      }
    }
    }
    else
    {
      int32_t elementSizeBytes;
      elementSizeBytes = TIDL_getDatElementSize(dataBuffParam->elementType);
      if(elementSizeBytes == 4)
      {
        tidlWriteLevel = tidlWriteLevel == 3 ? 1 : tidlWriteLevel;
      }

      int32_t dataSize = dataBuffParam->dimValues[0] * dataBuffParam->dimValues[1] * dataBuffParam->dimValues[2] * dataBuffParam->dimValues[3];
      int32_t linePitch = dataBuffParam->pitch[TIDL_LINE_PITCH];
      int32_t chPitch = dataBuffParam->pitch[TIDL_CHANNEL_PITCH];
      int32_t batchPitch  = dataBuffParam->pitch[TIDL_ROI_PITCH];
      int32_t paddeOutSize = linePitch * (dataBuffParam->dimValues[2] + (dataBuffParam->padH * 2) + 1);
      int32_t padedDataSize =  dataBuffParam->dimValues[0] * dataBuffParam->dimValues[1] * paddeOutSize;
      int8_t * outPtr        = (int8_t *)scratchPtr;
      int8_t * outWithPadPtr = (int8_t *)(scratchPtr + (dataSize*elementSizeBytes));
      float32_tidl  * floatPtr = (float32_tidl  *)(scratchPtr + (dataSize*elementSizeBytes));
      int32_t i;
      int32_t j;
      int32_t batchIdx;
      int32_t batchOffset;      
      int32_t offset;
      int32_t totalSize = 0;

      if (tidlWriteLevel == 1)
      {
        totalSize = (dataSize*elementSizeBytes);
      }
      else if (tidlWriteLevel == 2)
      {
        totalSize = ((dataSize*elementSizeBytes) + (padedDataSize*elementSizeBytes));
      }
      else if (tidlWriteLevel == 3)
      {
        totalSize = (dataSize*elementSizeBytes) + ((int32_t)sizeof(float32_tidl)*dataSize);
      }
      else
      {
        tidl_printf(0, "Un supported tidlWriteLevel \n");
        status = TIDL_ERR_FAILURE;
      }

      if(dataBuffParam->elementType == TIDL_SinglePrecFloat)
      {
        float32_tidl curMin = FLT_MAX, curMax = -FLT_MAX;
        TIDL_TensorMinMax((float32_tidl*)ptr, dataBuffParam, &curMin, &curMax);
        tidl_printf(0, " %3d %15.5f %15.5f %15.5f %d\n", dataBuffId, dataBuffParam->tensorScale, curMin, curMax, dataBuffParam->elementType);
      }
      else
      {
        tidl_printf(0, " %3d %15.5f %15.5f %15.5f %d\n", dataBuffId, dataBuffParam->tensorScale, dataBuffParam->minTensorValue, dataBuffParam->maxTensorValue, dataBuffParam->elementType);
      }


      if ((totalSize > scratchSize) && (status == TIDL_SUCCESS))
      {
        tidl_printf(0, " In sufficient Memory to write Traces\n");
        status = TIDL_ERR_FAILURE;
      }

      if(status == TIDL_SUCCESS)
      {
        if(tidlTraceBaseName != NULL)
        {
          (void)strcpy((char *)traceDumpName, (char *)tidlTraceBaseName);
        }
        else
        {
          (void)strcpy((char *)traceDumpName, (char *)"");
        }

        (void)sprintf((char *)DataIDString, "%04d", dataBuffId);
        (void)strcat((char *)traceDumpName, (char *)DataIDString);
        (void)strcat((char *)traceDumpName, "_");
        (void)sprintf((char *)DataIDString, "%05d", dataBuffParam->dimValues[0]);
        (void)strcat((char *)traceDumpName, (char *)DataIDString);
        (void)strcat((char *)traceDumpName, "_");
        (void)sprintf((char *)DataIDString, "%05d", dataBuffParam->dimValues[1]);
        (void)strcat((char *)traceDumpName, (char *)DataIDString);
        (void)strcat((char *)traceDumpName, "_");
        (void)sprintf((char *)DataIDString, "%05d", dataBuffParam->dimValues[3]);
        (void)strcat((char *)traceDumpName, (char *)DataIDString);
        (void)strcat((char *)traceDumpName, "x");
        (void)sprintf((char *)DataIDString, "%05d", dataBuffParam->dimValues[2]);
        (void)strcat((char *)traceDumpName, (char *)DataIDString);
        (void)strcpy((char *)traceBaseName, (char *)traceDumpName);

        if(currentLineWriteOffset != 0)
        {
          (void)strcpy((char *)traceDumpName, (char *)traceBaseName);
          (void)strcat((char *)traceDumpName, ".y");

          if(elementSizeBytes == 4)
          {
            (void)strcpy((char *)traceDumpName, (char *)traceBaseName);
            (void)strcat((char *)traceDumpName, "_float.bin");
          }

          (void)tidlReadBinFromFile((const char *)traceDumpName, outPtr, dataSize*elementSizeBytes);
          if (tidlWriteLevel == 2)
          {
            (void)strcpy((char *)traceDumpName, (char *)traceBaseName);
            (void)strcat((char *)traceDumpName, "_paded.y");
            (void)tidlReadBinFromFile((const char *)traceDumpName, outWithPadPtr, padedDataSize*elementSizeBytes);
          }

          if (tidlWriteLevel == 3)
          {
            (void)strcpy((char *)traceDumpName, (char *)traceBaseName);
            (void)strcat((char *)traceDumpName, "_float.bin");
            (void)tidlReadBinFromFile((const char *)traceDumpName, floatPtr, dataSize*((int32_t)sizeof(float32_tidl)));
          }
        }

      for (batchIdx = 0; batchIdx < dataBuffParam->dimValues[0]; batchIdx++)
      {
        batchOffset = batchIdx * dataBuffParam->dimValues[1]  * dataBuffParam->dimValues[2] * dataBuffParam->dimValues[3];

        for (j = 0; j < dataBuffParam->dimValues[1]; j++)
        {
          for (i = 0; i < currentNumLines; i++)
          {
            offset = (j*dataBuffParam->dimValues[2] * dataBuffParam->dimValues[3]) +
                     ((i + currentLineWriteOffset)*dataBuffParam->dimValues[3]) + batchOffset;

            if (dataBuffParam->elementType == 0)
            {
              uint8_t * src, *dst;
              int32_t k;
              src = (uint8_t *)(ptr + batchIdx * batchPitch + (j*currChPitch) + (i*linePitch) + (dataBuffParam->padH*linePitch) + dataBuffParam->padW);
              dst = (uint8_t *)(outPtr + offset);
              for (k = 0; k < dataBuffParam->dimValues[3]; k++)
              {
                dst[k] = src[k];
                if (tidlWriteLevel == 3)
                {
                  floatPtr[offset + k] = ((float32_tidl)src[k]) / (dataBuffParam->tensorScale);
                }
              }
            }
            else if (dataBuffParam->elementType == 1)
            {
              int8_t * src;
              int8_t * dst;
              int32_t k;
              src = (ptr + batchIdx * batchPitch + (j*currChPitch) + (i*linePitch) + (dataBuffParam->padH*linePitch) + dataBuffParam->padW);
              dst = (outPtr + offset);
              for (k = 0; k < dataBuffParam->dimValues[3]; k++)
              {
                //dst[k] = src[k] + 128;
                dst[k] = src[k];
                if (tidlWriteLevel == 3)
                {
                  floatPtr[offset + k] = ((float32_tidl)src[k]) / (dataBuffParam->tensorScale);
                }
              }
            }
            else if (dataBuffParam->elementType == 2)
            {
              uint16_t * src;
              uint16_t * dst;
              int32_t k;
              src = (((uint16_t *)ptr) + batchIdx * batchPitch + (j*currChPitch) + (i*linePitch) + (dataBuffParam->padH*linePitch) + dataBuffParam->padW);
              dst = (((uint16_t *)outPtr) + offset);
              for (k = 0; k < dataBuffParam->dimValues[3]; k++)
              {
                dst[k] = src[k];
                if (tidlWriteLevel == 3)
                {
                  floatPtr[offset + k] = ((float32_tidl)src[k]) / (dataBuffParam->tensorScale);
                }
              }
            }
            else if (dataBuffParam->elementType == 3)
            {
              int16_t * src;
              int16_t * dst;
              int32_t k;
              src = (((int16_t *)ptr) + batchIdx * batchPitch + (j*currChPitch) + (i*linePitch) + (dataBuffParam->padH*linePitch) + dataBuffParam->padW);
              dst = (((int16_t *)outPtr) + offset);
              for (k = 0; k < dataBuffParam->dimValues[3]; k++)
              {
                //dst[k] = src[k] + 128;
                dst[k] = src[k];
                if (tidlWriteLevel == 3)
                {
                  floatPtr[offset + k] = ((float32_tidl)src[k]) / (dataBuffParam->tensorScale);
                }
              }
            }
            else
            {
              (void)memcpy((outPtr + ((batchIdx * dataBuffParam->dimValues[1]  * dataBuffParam->dimValues[2] * dataBuffParam->dimValues[3])
              + (j*dataBuffParam->dimValues[2] * dataBuffParam->dimValues[3]) +
                (i*dataBuffParam->dimValues[3]))*elementSizeBytes),
                (ptr + (((batchIdx * batchPitch) + (j*currChPitch) + (i*linePitch) + (dataBuffParam->padH*linePitch) +
                  dataBuffParam->padW)*elementSizeBytes)),
                (size_t)(dataBuffParam->dimValues[3] * elementSizeBytes));
            }

          }
          if (tidlWriteLevel == 2)
          {
            if((currentNumLines != dataBuffParam->dimValues[2]) && (currentLineWriteOffset != 0))
            {
              tidl_printf(0, "tidlWriteLevel = 2 with ST is not supported now \n");
              status = IALG_EFAIL;
            }
            else
            {
              (void)memcpy((outWithPadPtr + batchIdx *dataBuffParam->dimValues[1] * paddeOutSize + j*paddeOutSize), (ptr + (batchIdx * batchPitch) + (j*chPitch)), paddeOutSize);
            }
          }
        }
      }
        (void)strcpy((char *)traceDumpName, (char *)traceBaseName);
        (void)strcat((char *)traceDumpName, ".y");
        if(elementSizeBytes == 4)
        {
          (void)strcpy((char *)traceDumpName, (char *)traceBaseName);
          (void)strcat((char *)traceDumpName, "_float.bin");
        }
        (void)tidlWriteBinToFile((const char *)traceDumpName, outPtr, dataSize*elementSizeBytes);
        if (tidlWriteLevel == 2)
        {
          (void)strcpy((char *)traceDumpName, (char *)traceBaseName);
          (void)strcat((char *)traceDumpName, "_paded.y");
          (void)tidlWriteBinToFile((const char *)traceDumpName, outWithPadPtr, padedDataSize*elementSizeBytes);
        }

        if (tidlWriteLevel == 3)
        {
          (void)strcpy((char *)traceDumpName, (char *)traceBaseName);
          (void)strcat((char *)traceDumpName, "_float.bin");
          (void)tidlWriteBinToFile((const char *)traceDumpName, floatPtr, dataSize*((int32_t)sizeof(float32_tidl)));
        }
      }
    }
      
    }
    tidlWriteLevel= tidlWriteLevelOrig;
  }
  return status;
}

/**
 * @brief Function to compare to reference and target implementations
 *
 * @param targetPtr : output from target implementation
 * @param refPtr : output from reference implementation
 * @param outWidth : Width of the ouptut buffer
 * @param outHeight : Height of the ouptut buffer
 * @param numOutChannels : no of output channels
 * @param numBatches : no of Batches
 * @param outPitch : Pitch of the output buffer
 * @param outChPitch : Channel pitch of the output buffer
 * @param outBatchPitch : Batch pitch of the output buffer
 * @param layerIdx : current layer Index
 * @return  IALG_EOK   - for no Mismatch
 *          IALG_EFAIL - for mismatch
 */
template <class Tfeat>
int32_t TIDL_RefComparison(
  Tfeat * targetPtr,
  Tfeat * refPtr,
  int32_t outWidth,
  int32_t outHeight,
  int32_t numOutChannels,
  int32_t numBatches,
  int32_t outPitch,
  int32_t outChPitch,
  int32_t outBatchPitch,
  int32_t layerIdx
)
{
  int32_t i;
  int32_t j;
  int32_t k;
  int32_t batchIdx;
  int32_t status = TIDL_SUCCESS;
  int32_t offset;

  for(batchIdx = 0;batchIdx < numBatches; batchIdx++)
  {
    for(i = 0; i < numOutChannels; i++)
    {
      for(j = 0; j < (outHeight); j++)
      {
        for(k = 0; k < (outWidth); k++)
        {
          offset = (batchIdx * outBatchPitch) +  (i*outChPitch) +  (j * outPitch) + k;
          if(targetPtr[offset] != refPtr[offset])
          {
            tidl_printf(0,"Layer %d ,Ref Comparison :  FAILED!!!!!! at ROI %4d Channel %4d, height %5d, width %5d, target %5d, ref %5d \n", layerIdx, batchIdx, i, j, k,
              targetPtr[(i*outChPitch) +  (j * outPitch) + k],
              refPtr[(i*outChPitch) + (j*outPitch) + k]);
            status = TIDL_ERR_FAILURE;
            break;
          }
        }
        if(TIDL_SUCCESS != status)
        {
          break;
        }
      }
      if(TIDL_SUCCESS != status)
      {
        break;
      }
    }
  }
  return status;
}

/**
 * @brief Function to generate Fill Seam Predicate Registers
 *
 * @param numTiles : no of tiles in the cuurent tensor
 * @param bufPredicateStore : buffer to store predicate stores
 * @param srcAddr : pointer to source data buffer
 * @param dstAddr : pointer to dst data buffer
 * @return int32_t : total bytes to store
 */
int32_t TIDL_generateFillSeamPredicateRegisters(
        int32_t numTiles,
        void *bufPredicateStore,
        const TIDL_bufParams3D_t * srcAddr,
        const TIDL_bufParams3D_t * dstAddr)
{
    int32_t numBlocks;
    int32_t mmaWidth;
    int32_t totalBytes;
    int32_t i,j, k;
    int32_t numBytes = TIDL_sizeof(srcAddr->data_type);
    int32_t inputWidth            = srcAddr->dim_x;
    int32_t processHeight         = dstAddr->dim_y;
    int32_t processWidth          = srcAddr->stride_y / numBytes;
    int32_t matrixColumn          = processWidth*processHeight;
    int32_t blockCounter          = 0;
    int64_t *predRegister         = (int64_t *)bufPredicateStore;
    uint64_t predictedValue = 0x0;

    numBlocks = numTiles;
    totalBytes = numBlocks*8;

    j = 0;
    k = 1;

    if (numBytes== 1) {
        mmaWidth= MMALIB_MMA_SIZE_8_BIT;
    }
    else if (numBytes== 2) {
        mmaWidth= MMALIB_MMA_SIZE_8_BIT/2;
    }
    else {
        mmaWidth= MMALIB_MMA_SIZE_8_BIT/4;
    }

    while (blockCounter < numBlocks) {
        for (i = 0; i < mmaWidth; i++) {
            if (j < inputWidth) {
                if (mmaWidth == (MMALIB_MMA_SIZE_8_BIT/2)) {
                    predictedValue |= (uint64_t)((uint64_t)1 << i*2);
                    predictedValue |= (uint64_t)((uint64_t)1 << (i*2 + 1));
                }
                else if (mmaWidth == MMALIB_MMA_SIZE_8_BIT)
                    predictedValue |= (uint64_t)((uint64_t)1 << i);
                j++;
                k++;
            }
            else if(j >= inputWidth && k <= processWidth) {
                j++;
                k++;
            }
            else if ((blockCounter == numBlocks - 1) && ((blockCounter*mmaWidth + i) >= matrixColumn)) {
                k = 1;
                j = processWidth;
            }
            else {
                k = 1;
                j = 0;
                if (mmaWidth == (MMALIB_MMA_SIZE_8_BIT/2)) {
                    predictedValue |= (uint64_t)((uint64_t)1 << i*2);
                    predictedValue |= (uint64_t)((uint64_t)1 << (i*2 + 1));
                }
                else if (mmaWidth == MMALIB_MMA_SIZE_8_BIT)
                    predictedValue |= (uint64_t)((uint64_t)1 << i);
                j++;
                k++;
            }
        }
        *predRegister = predictedValue;

        predRegister++;
        blockCounter++;
        predictedValue = 0x0;
    }

    return totalBytes;
}

template int32_t TIDL_RefComparison<uint16_t>(
  uint16_t * targetPtr,
  uint16_t * refPtr,
  int32_t outWidth,
  int32_t outHeight,
  int32_t numOutChannels,
  int32_t numBatches,
  int32_t outPitch,
  int32_t outChPitch,
  int32_t outBatchPitch,
  int32_t layerIdx
);

template int32_t TIDL_RefComparison<uint8_t>(
  uint8_t * targetPtr,
  uint8_t * refPtr,
  int32_t outWidth,
  int32_t outHeight,
  int32_t numOutChannels,
  int32_t numBatches,
  int32_t outPitch,
  int32_t outChPitch,
  int32_t outBatchPitch,
  int32_t layerIdx
);

template int32_t TIDL_RefComparison<int16_t>(
  int16_t * targetPtr,
  int16_t * refPtr,
  int32_t outWidth,
  int32_t outHeight,
  int32_t numOutChannels,
  int32_t numBatches,
  int32_t outPitch,
  int32_t outChPitch,
  int32_t outBatchPitch,
  int32_t layerIdx
);

template int32_t TIDL_RefComparison<int8_t>(
  int8_t * targetPtr,
  int8_t * refPtr,
  int32_t outWidth,
  int32_t outHeight,
  int32_t numOutChannels,
  int32_t numBatches,
  int32_t outPitch,
  int32_t outChPitch,
  int32_t outBatchPitch,
  int32_t layerIdx
);


void TIDL_getSaturationLimits(int32_t elementType, int32_t * satLow, int32_t * satHigh)
{
  if ( elementType == TIDL_UnsignedChar )
  {
    *satLow  = std::numeric_limits<uint8_t>::lowest();
    *satHigh = std::numeric_limits<uint8_t>::max();
  }
  else if ( elementType == TIDL_SignedChar )
  {
    *satLow  = std::numeric_limits<int8_t>::lowest();
    *satHigh = std::numeric_limits<int8_t>::max();
  }
  else if ( elementType == TIDL_UnsignedShort )
  {
    *satLow  = std::numeric_limits<uint16_t>::lowest();
    *satHigh = std::numeric_limits<uint16_t>::max();
  }
  else if ( elementType == TIDL_SignedShort )
  {
    *satLow  = std::numeric_limits<int16_t>::lowest();
    *satHigh = std::numeric_limits<int16_t>::max();
  }
  else
  {
    *satLow  = std::numeric_limits<uint8_t>::lowest();
    *satHigh = std::numeric_limits<uint8_t>::max();
  }

}


/**
 * @brief Function to store saturation float values
 *
 * @param tidlLayer : Pointer to the common layer parameters
 * @param min : pointer to store min value
 * @param max : pointer to store max value
 * @return None
 */
void TIDL_getSaturationFloat(
  sTIDL_Layer_t        * tidlLayer,
  float * min,
  float * max
  )
{
  if(tidlLayer->actParams.actType == TIDL_NoAct || tidlLayer->actParams.actType == TIDL_PRelU)
  {
    *max =  FLT_MAX;
    *min = -FLT_MAX;
  }
  else if(tidlLayer->actParams.actType == TIDL_RelU)
  {
    *max =  FLT_MAX;
    *min = 0;
  }
  else if(tidlLayer->actParams.actType == TIDL_RelU6)
  {
    *max = 6.0;
    *min = 0;
  }
  else if(tidlLayer->actParams.actType == TIDL_Clip)
  {
    *max = tidlLayer->actParams.clipMax;
    *min = tidlLayer->actParams.clipMin;
  }
  else
  {
    tidl_printf(0, "actType Not supported in Line %d of %s \n", __LINE__, __FILE__);
    /*return IALG_EFAIL;*/
  }
  /*return IALG_EOK;*/
}

/**
 * @brief Function to calculate saturation float values
 *
 * @param tidlLayer : Pointer to the common layer parameters
 * @param outAcc : current accumlator value
 */
float32_tidl TIDL_floatSat(
  float32_tidl outAcc,
  sTIDL_Layer_t  * tidlLayer
  )
{
  float32_tidl min, max;
  TIDL_getSaturationFloat(tidlLayer, &min, &max);
  outAcc = (outAcc > max) ? max : outAcc;
  outAcc = (outAcc < min) ? min : outAcc;
  return outAcc;
}

int32_t TIDL_getProcessingElementSizeInBytes(const sTIDL_Layer_t  * tidlLayer)
{
  int32_t procElemSizeInBytes;
  if ( tidlLayer->weightsElementSizeInBits <= 8 )
  {
    procElemSizeInBytes = 1;
  }
  else if ( tidlLayer->weightsElementSizeInBits <= 16 )
  {
    procElemSizeInBytes = 2;
  }
  else
  {
    procElemSizeInBytes = 4;
  }

  return procElemSizeInBytes;
}

int32_t TIDL_conv2dGetKernelDataType(int32_t tidlElemType, int32_t procElemSize)
{
  int32_t mmaDataType;

  if (tidlElemType == (int32_t)TIDL_SignedChar)
  {
    mmaDataType = (uint32_t)TIDL_INT8;
    if (procElemSize == 2 )
    {
      mmaDataType = (uint32_t)TIDL_INT16;
    }
  }
  else if (tidlElemType  == (int32_t)TIDL_UnsignedChar)
  {
   mmaDataType = (uint32_t)TIDL_UINT8;
   if (procElemSize == 2 )
   {
     mmaDataType = (uint32_t)TIDL_UINT16;
   }
  }
  else if (tidlElemType == (int32_t)TIDL_UnsignedShort)
  {
   mmaDataType = (uint32_t)TIDL_UINT16;
  }
  else if (tidlElemType == (int32_t)TIDL_SignedShort)
  {
   mmaDataType = (uint32_t)TIDL_INT16;
  }
  else
  {
    mmaDataType = (uint32_t)TIDL_UINT8;
    if (procElemSize == 2 )
    {
     mmaDataType = (uint32_t)TIDL_UINT16;
    }
  }

  return mmaDataType;

}

int32_t TIDL_conv2dGetMmalibDataType(int32_t tidlElemType, int32_t procElemSize)
{
  int32_t mmaDataType;

  if (tidlElemType == (int32_t)TIDL_SignedChar)
  {
    mmaDataType = (uint32_t)MMALIB_INT8;
    if (procElemSize == 2 )
    {
      mmaDataType = (uint32_t)MMALIB_INT16;
    }
  }
  else if (tidlElemType  == (int32_t)TIDL_UnsignedChar)
  {
   mmaDataType = (uint32_t)MMALIB_UINT8;
   if (procElemSize == 2 )
   {
     mmaDataType = (uint32_t)MMALIB_UINT16;
   }
  }
  else if (tidlElemType == (int32_t)TIDL_UnsignedShort)
  {
   mmaDataType = (uint32_t)MMALIB_UINT16;
  }
  else if (tidlElemType == (int32_t)TIDL_SignedShort)
  {
   mmaDataType = (uint32_t)MMALIB_INT16;
  }
  else if (tidlElemType == (int32_t)TIDL_SinglePrecFloat)
  {
    mmaDataType = (uint32_t)MMALIB_FLOAT32;
  }

  return mmaDataType;

}


void TIDL_printDMATr(void * dmaTr)
{
  CSL_UdmapTR * tr = (CSL_UdmapTR  *) dmaTr;
  tidl_printf(0,"sicnt_0 %16d \n", tr->icnt0);
  tidl_printf(0,"sicnt_1 %16d sdim1 %16d\n", tr->icnt1, tr->dim1);
  tidl_printf(0,"sicnt_2 %16d sdim1 %16d\n", tr->icnt2, tr->dim2);
  tidl_printf(0,"sicnt_3 %16d sdim1 %16d\n", tr->icnt3, tr->dim3);

  tidl_printf(0,"dicnt_0 %16d \n", tr->dicnt0);
  tidl_printf(0,"dicnt_1 %16d ddim1 %16d\n", tr->dicnt1, tr->ddim1);
  tidl_printf(0,"dicnt_2 %16d ddim1 %16d\n", tr->dicnt2, tr->ddim2);
  tidl_printf(0,"dicnt_3 %16d ddim1 %16d\n", tr->dicnt3, tr->ddim3);
}

/**
* @brief Check for border pixel
*
* @param spatialOffsetY : Offset value in Height dimension
* @param spatialOffsetX : Offset value in Width dimension
* @param validPosXMin : Minimum width value to be a valid pixel
* @param validPosXMax : Maximum width value to be a valid pixel
* @param validPosYMin : Minimum height value to be a valid pixel
* @param validPosYMax : Maximum height value to be a valid pixel
* @return isBorderPixel : Returning true for border pixels
*/
int32_t TIDL_checkPixelInPadRegion(int32_t spatialOffsetY,
                       int32_t spatialOffsetX,
                       int32_t validPosXMin,
                       int32_t validPosXMax,
                       int32_t validPosYMin,
                       int32_t validPosYMax)
{
  int32_t isBorderPixel = 0;

  if ((spatialOffsetY < validPosYMin) || (spatialOffsetY >= validPosYMax))
    isBorderPixel = 1;
  if ((spatialOffsetX < validPosXMin) || (spatialOffsetX >= validPosXMax))
    isBorderPixel = 1;

  return isBorderPixel;
}
