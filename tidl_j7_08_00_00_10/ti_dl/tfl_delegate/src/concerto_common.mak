TARGET      := tidl_tfl_delegate
TARGETTYPE  := dsmo

CPPSOURCES    += ../tfl_delegate.cpp
CSOURCES    += ../../../common/tivx_utils.c

IDIRS += $($(_MODULE)_SDIR)/../../../inc
IDIRS += $($(_MODULE)_SDIR)/../../../rt/inc
IDIRS += $($(_MODULE)_SDIR)/../../../utils/tidlModelImport
IDIRS += $($(_MODULE)_SDIR)/../../../common

IDIRS += $(TF_REPO_PATH)  
IDIRS += $(PSDK_INSTALL_PATH)/ivision

IDIRS += $(TIOVX_PATH)/include
IDIRS += $(TIOVX_PATH)/kernels/include
IDIRS += $(TIOVX_PATH)/kernels_j7/include
IDIRS += $(TIOVX_PATH)/kernels_j7/include
IDIRS += $(TIOVX_PATH)/utils/include

LDIRS += $($(_MODULE)_SDIR)/../../../rt/out/$(TARGET_PLATFORM)/$(TARGET_CPU)/LINUX/$(TARGET_BUILD)

SHARED_LIBS += dl
SHARED_LIBS += vx_tidl_rt


CPPFLAGS  += --std=c++11 \
             -Wno-maybe-uninitialized \
             -Wno-unused-variable \
             -Wno-sign-compare \
             -Wno-unused-but-set-variable \
             -Wno-unused-result \
             -Wno-format-overflow \
             -Wno-format-truncation
