/*
* Copyright (C) 2020 Texas Instruments Incorporated - http://www.ti.com/
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

#include "itidl_ti.h"
#include "itidl_rt.h"
#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "float.h"
#include "dlfcn.h"
#include "assert.h"
#include <string>
#include <vector>
#include "tensorflow/lite/c/common.h"
#include "tensorflow/lite/builtin_ops.h"
#include "tensorflow/lite/context_util.h"

#include "tivx_utils.h"

static int tidlrt_perfstats = 0;
static void __attribute__((constructor)) lib_init()
{
	char *perf_str;

	perf_str = getenv("TIDL_RT_PERFSTATS");
	if(!perf_str)
		tidlrt_perfstats = 0;
	else
		tidlrt_perfstats = atoi(perf_str);
}
static int32_t TIDL_tfliteRtGetTypeAndPtr(TfLiteTensor *tensor, int32_t * type, void ** ptr)
{
  int32_t status = 0;
  if(tensor->type == kTfLiteUInt8)
  {
    *type =  TIDL_UnsignedChar;
    *ptr = (tensor->data.uint8);
  }
  else if(tensor->type == kTfLiteInt8)
  {
    *type =  TIDL_SignedChar;
    *ptr = (tensor->data.int8);
  }
  else if(tensor->type == kTfLiteInt16)
  {
    *type =  TIDL_SignedShort;
    *ptr = (tensor->data.i16);
  }
  else if(tensor->type == kTfLiteFloat32)
  {
    *type =  TIDL_SinglePrecFloat;
    *ptr = (tensor->data.f);
  }
  else if(tensor->type == kTfLiteInt32)
  {
    *type =  TIDLRT_Int32;
    *ptr = (tensor->data.i32);
  }
  else if(tensor->type == kTfLiteInt64)
  {
    *type =  TIDLRT_Int64;
    *ptr = (tensor->data.i64);
  }
  else
  {
    printf("Tflite data type not supported by TIDL");
    status = -1;
  }
  return status;
}

static void TIDL_tfliteRtGetScaleAndZeroPoint(TfLiteTensor *tensor , float * scale, int32_t * zp)
{
    if ((tensor->type == kTfLiteUInt8) || (tensor->type == kTfLiteInt8))
    {
        TfLiteAffineQuantization *quantization = (TfLiteAffineQuantization *)tensor->quantization.params;
        *zp = quantization->zero_point->data[0];
        *scale = 1 / quantization->scale->data[0];
    }
    else
    {
      *zp    = 0;
      *scale = 1.0;
    }
}

namespace tflite {
namespace tfl_delegate {

struct  TfLiteTIDLDelegateOptions {
  int debug_level;
  void * rtHandle;
  void * rtInList;
  void * rtOutList;
  sTIDLRT_PerfStats_t * stats;
  char artifacts_folder[512];
  struct {
    void *lib;
    decltype(&TIDLRT_create) TIDLRT_create;
    decltype(&TIDLRT_delete) TIDLRT_delete;
    decltype(&TIDLRT_invoke) TIDLRT_invoke;
    decltype(&TIDLRT_deactivate) TIDLRT_deactivate;
    decltype(&TIDLRT_setParamsDefault) TIDLRT_setParamsDefault;
    decltype(&TIDLRT_setTensorDefault) TIDLRT_setTensorDefault;
    decltype(&TIDLRT_getDdrStats) TIDLRT_getDdrStats;
  } infer_ops;
};

constexpr int kMaxTIDLGraphs = 32;

class tidlDelegate {
public:
  // Any initialization code needed
  TfLiteStatus Init(TfLiteContext* context, const TfLiteDelegateParams* params);
  // Any preparation work needed (e.g. allocate buffers)
  TfLiteStatus Prepare(TfLiteContext* context, TfLiteNode* node);
  // Actual running of the delegate subgraph.
  TfLiteStatus Invoke(TfLiteContext* context, TfLiteNode* node);
  // ... Add any other methods needed.
  TfLiteStatus CustomData(TfLiteContext* context, TfLiteNode* node, const char *op_,
          char **node_name, void **node_data);
  //Destructor
  ~tidlDelegate();

private:
  TfLiteTIDLDelegateOptions options_;
  const char *subGraphName_;
};

#define ASSERT_VALID_REF(ref)   (APP_ASSERT(vxGetStatus((vx_reference)(ref))==VX_SUCCESS));

#define MAX_FILE_PATH  (512)
#define FILE_NAME_SIZE (256)


void tfldelegate_printf(int32_t debugLevel, char * format, ...)
{
  va_list args;
  if (debugLevel >= 1)
  {
    (void)va_start(args, format);
    (void)vprintf(format, args);
    va_end(args);
  }
}


TfLiteStatus tidl_subgraph_rt_create(TfLiteContext *context, TfLiteTIDLDelegateOptions *options, int32_t outTensorIdx);

int32_t TIDLRT_ReadBinFromFile(const char *fileName, void *addr, int32_t size)
{
    FILE *fptr = NULL;
    fptr = fopen((const char *)fileName, "rb");
    if (fptr)
    {
      fread(addr, size, 1, fptr);
      fclose(fptr);
      return 0;
    }
    else
    {
      printf("Could not open %s file for reading \n", fileName);
    }
    return -1;
}

bool IsNodeSupportedByTIDL(const TfLiteRegistration* registration, const TfLiteNode* node, TfLiteContext* context,
                            FILE *fp, TfLiteTIDLDelegateOptions *options, int nodeIx, int32_t debugLevel)
{
    int idx;
    fseek(fp, 0, SEEK_SET);
    while (!feof(fp))
    {
      fscanf(fp, "%d\n", &idx);
      if(nodeIx == idx)
      {
        return true;
      }
    }
  return false;
}

int32_t getOutTensorIdx(TfLiteContext* context, const TfLiteDelegateParams* params)
{
  int32_t outTensorIdx = 0;
  // Return the index of the first output encountered - this is used to identify subgraph, so one is enough
  for (auto tensor_index : TfLiteIntArrayView(params->output_tensors))
  {
    if (tensor_index == kTfLiteOptionalTensor)
    {
      continue;
    }
    TfLiteTensor* tensor = &context->tensors[tensor_index];
    // Const tensors should be added as const nodes during graph construction.
    if(tensor->allocation_type == kTfLiteArenaRw)
    {
      outTensorIdx = tensor_index;
    }
  }
  return outTensorIdx;
}

TfLiteStatus tidlDelegate::Init(TfLiteContext* context,
                                         const TfLiteDelegateParams* params) {
  int status = 0;

  TfLiteTIDLDelegateOptions* options_ptr = nullptr;
  if (params != nullptr && params->delegate != nullptr) {
    options_ptr = reinterpret_cast<TfLiteTIDLDelegateOptions*>(params->delegate->data_);
    if (options_ptr != nullptr) options_ = *options_ptr;
  }
   tfldelegate_printf(options_ptr->debug_level, "\n ****** In tidlDelegate::Init ****** \n");

  int32_t subgraphId = getOutTensorIdx(context, params);

  asprintf(const_cast<char **>(&subGraphName_), "%d", subgraphId);

  status = tidl_subgraph_rt_create(context, &options_, subgraphId);

  return kTfLiteOk;
}

TfLiteStatus tidl_subgraph_rt_create(TfLiteContext *context, TfLiteTIDLDelegateOptions *options, int32_t outTensorIdx)
{
  tfldelegate_printf(options->debug_level, "************ in tidl_subgraph_rt_create ************ \n ");

  int status = 0;
  int j = 0;
  uint32_t previous_tensor_size = 0;
  sTIDLRT_Params_t prms;
  FILE *fp_network;
  FILE *fp_config;
  uint32_t size = 0;
  uint32_t numInputBuf = 0;
  uint32_t total_output_buf_size = 0;
  uint32_t offset = 0;
  char network_file[512];
  char config_file[512];
  void *handle = NULL;

  status = options->infer_ops.TIDLRT_setParamsDefault(&prms);

  snprintf(network_file, FILE_NAME_SIZE, "%s/%d_tidl_net.bin", options->artifacts_folder, outTensorIdx);
  snprintf(config_file, FILE_NAME_SIZE, "%s/%d_tidl_io_1.bin", options->artifacts_folder, outTensorIdx);

  fp_network = fopen(&network_file[0], "rb");
  if (fp_network == NULL)
  {
    printf("Invoke  : ERROR: Unable to open network file %s \n", network_file);
    return kTfLiteError;
  }
  prms.stats = (sTIDLRT_PerfStats_t*)malloc(sizeof(sTIDLRT_PerfStats_t));

  fseek(fp_network, 0, SEEK_END);
  prms.net_capacity = ftell(fp_network);
  fseek(fp_network, 0, SEEK_SET);
  fclose(fp_network);
  prms.netPtr = malloc(prms.net_capacity);

  prms.TIDLReadBinFromFile = TIDLRT_ReadBinFromFile;
  status = prms.TIDLReadBinFromFile(&network_file[0], prms.netPtr, prms.net_capacity);

  fp_config = fopen(&config_file[0], "rb");
  if (fp_config == NULL)
  {
    printf("Invoke  : ERROR: Unable to open IO config file %s \n", config_file);
    return kTfLiteError;
  }
  fseek(fp_config, 0, SEEK_END);
  prms.io_capacity = ftell(fp_config);
  fseek(fp_config, 0, SEEK_SET);
  fclose(fp_config);
  prms.ioBufDescPtr = malloc(prms.io_capacity);
  status = prms.TIDLReadBinFromFile(&config_file[0], prms.ioBufDescPtr, prms.io_capacity);

  if(options->debug_level >= 2)
  {
    prms.traceLogLevel = options->debug_level;
    prms.traceWriteLevel = 3;
  }

  status = options->infer_ops.TIDLRT_create(&prms, &handle);

  sTIDL_IOBufDesc_t *ioBufDesc = (sTIDL_IOBufDesc_t *)prms.ioBufDescPtr;

  options->rtInList  = (void *)malloc(ioBufDesc->numInputBuf * sizeof(sTIDLRT_Tensor_t));
  options->rtOutList = (void *)malloc(ioBufDesc->numOutputBuf * sizeof(sTIDLRT_Tensor_t));
  options->rtHandle    = handle;
  options->stats       = prms.stats;

  return kTfLiteOk;
}
TfLiteStatus tidl_subgraph_rt_delete(TfLiteTIDLDelegateOptions *options)
{
  tfldelegate_printf(options->debug_level, "************ in tidl_subgraph_rt_delete ************ \n ");
  int status = 0;
  if(options->rtHandle)
  {
    status = options->infer_ops.TIDLRT_deactivate(options->rtHandle);
    status = options->infer_ops.TIDLRT_delete(options->rtHandle);
  }
  free(options->rtInList);
  free(options->rtOutList);
  return kTfLiteOk;
}

TfLiteStatus tidl_subgraph_rt_invoke(TfLiteContext *context, TfLiteNode *node, TfLiteTIDLDelegateOptions *options)
{
  int status = 0;
  int j = 0;

  void *handle = options->rtHandle;
  sTIDLRT_PerfStats_t *stats = (sTIDLRT_PerfStats_t *)options->stats;

  sTIDLRT_Tensor_t *in[128];
  sTIDLRT_Tensor_t *out[128];
  sTIDLRT_Tensor_t *ins;
  sTIDLRT_Tensor_t *outs;

  ins = (sTIDLRT_Tensor_t *)options->rtInList;
  outs = (sTIDLRT_Tensor_t *)options->rtOutList;

  if ((ins == NULL) || (outs == NULL))
  {
    printf("Invoke  : ERROR: Unable to allocate memory for TIDL RT in[] out [] tensor struct\n");
    return kTfLiteError;
  }
  else
  {
    /* Input tesnsors property set up */
    j = 0;
    for (auto tensor_index : TfLiteIntArrayView(node->inputs))
    {
        if (tensor_index == kTfLiteOptionalTensor)
        {
            continue;
        }
        TfLiteTensor *tensor = &context->tensors[tensor_index];
        if ((tensor->allocation_type == kTfLiteArenaRw) ||
            (tensor->allocation_type == kTfLiteCustom))
        {
            in[j] = &(ins[j]);
            status = options->infer_ops.TIDLRT_setTensorDefault(in[j]);
            in[j]->layout = TIDLRT_LT_NHWC;
            strcpy((char *)in[j]->name, tensor->name);
            TIDL_tfliteRtGetScaleAndZeroPoint(tensor, &in[j]->scale, &in[j]->zeroPoint);
            status = TIDL_tfliteRtGetTypeAndPtr(tensor, &in[j]->elementType, &in[j]->ptr);

            if((tensor->allocation_type == kTfLiteCustom) && (tidl_is_tivx_shared_mem(in[j]->ptr)))
            {
              in[j]->memType = TIDLRT_MEM_SHARED;
            }

            if(status == -1)
            {
                return kTfLiteError;
            }
            j++;
      }
    }

    /* Output tensors property set up */
    j = 0;
    for (auto tensor_index : TfLiteIntArrayView(node->outputs))
    {
        if (tensor_index == kTfLiteOptionalTensor)
        {
            continue;
        }
        TfLiteTensor *tensor = &context->tensors[tensor_index];
        if ((tensor->allocation_type == kTfLiteArenaRw ) ||
              (tensor->allocation_type == kTfLiteCustom) )
        {
          out[j] = &(outs[j]);
          status = options->infer_ops.TIDLRT_setTensorDefault(out[j]);
          out[j]->layout = TIDLRT_LT_NHWC;
          strcpy((char *)out[j]->name, tensor->name);
          TIDL_tfliteRtGetScaleAndZeroPoint(tensor, &out[j]->scale, &out[j]->zeroPoint);
          status = TIDL_tfliteRtGetTypeAndPtr(tensor, &out[j]->elementType, &out[j]->ptr);
          if((tensor->allocation_type == kTfLiteCustom) && (tidl_is_tivx_shared_mem(out[j]->ptr)))
          {
            out[j]->memType = TIDLRT_MEM_SHARED;
          }

          if(status == -1)
          {
              return kTfLiteError;
          }
          j++;
        }
    }
  }
  status = options->infer_ops.TIDLRT_invoke(handle, in, out);

  if(options->debug_level > 0)
  {
    double proc_time    = (stats->proc_time_end - stats->proc_time_start)  / 1000;
    double cp_in_time   = (stats->cpIn_time_end - stats->cpIn_time_start)  / 1000;
    double cp_out_time  = (stats->cpOut_time_end - stats->cpOut_time_start)/ 1000;

    printf("Sub Graph Stats %f %f %f \n", cp_in_time, proc_time, cp_out_time);
  }
#if 0
  // check if the converted output of TIDL subgraph for tflite runtime is correct
  FILE * fpFloat = fopen("/tmp/tflite_data_float.bin", "wb");
  FILE * fpUint8 = fopen("/tmp/tflite_data_uint8.bin", "wb");
  for (auto tensor_index : TfLiteIntArrayView(node->outputs))
  {
    TfLiteTensor *tensor = &context->tensors[tensor_index];
    uint8_t * tfliteData = (uint8_t *)(tensor->data.uint8);
    TfLiteAffineQuantization *quantization = (TfLiteAffineQuantization *)tensor->quantization.params;
    int32_t zeroPoint = quantization->zero_point->data[0];
    float scale = quantization->scale->data[0];
    //printf("Final tflite zero point = %d, scale = %f \n", zeroPoint, scale);
    float * outFloat = (float *)malloc(tensor->bytes * 4);
    for(int i = 0; i < tensor->bytes; i++)
    {
      outFloat[i] = (tfliteData[i] - zeroPoint) * scale;
      //fwrite(&tfliteData[i], 1, 1, fpUint8);
      //fwrite(&outFloat[i], 1, 4, fpFloat);
    }
  }
  fclose(fpFloat);
  fclose(fpUint8);
#endif
  return kTfLiteOk;
}

TfLiteStatus tidlDelegate::Invoke(TfLiteContext* context, TfLiteNode* node)
{
  tfldelegate_printf(options_.debug_level, "\n ****** tidlDelegate::Invoke ****** \n");
  TfLiteStatus status = kTfLiteOk;

  int outTensorIdx = 0;
  status = tidl_subgraph_rt_invoke(context, node, &options_);

  return status;
}

tidlDelegate::~tidlDelegate()
{
  tfldelegate_printf(options_.debug_level, "************ in ~tidlDelegate ************ \n ");
  tidl_subgraph_rt_delete(&options_);
}


TfLiteStatus tidlDelegate::Prepare(TfLiteContext* context, TfLiteNode* node)
{
  tfldelegate_printf(options_.debug_level, "\n ****** tidlDelegate::Prepare ****** \n");

  for (auto tensor_index : TfLiteIntArrayView(node->outputs)) {
    if (tensor_index == kTfLiteOptionalTensor) {
      continue;
    }
    TfLiteTensor* tensor = &context->tensors[tensor_index];

    if(tensor->allocation_type == kTfLiteArenaRw)
      tfldelegate_printf(options_.debug_level, " Outputs Tensor name and id -  %s, %d\n", tensor->name, tensor_index);//tensor->is_variable);
  }

  return kTfLiteOk;
}

// Create the TfLiteRegistration for the Kernel node which will replace
// the subgraph in the main TfLite graph.
TfLiteRegistration GetTIDLNodeRegistration() {
  // This is the registration for the Delegate Node that gets added to
  // the TFLite graph instead of the subGraph it replaces.
  // It is treated as a an OP node. But in our case
  // Init will initialize the delegate
  // Invoke will run the delegate graph.
  // Prepare for preparing the delegate.
  // Free for any cleaning needed by the delegate.

  TfLiteRegistration kernel_registration = {0};
  kernel_registration.builtin_code = kTfLiteBuiltinDelegate;
  kernel_registration.custom_name = "tidlDelegate";
  kernel_registration.free = [](TfLiteContext* context, void* buffer) -> void {
    delete reinterpret_cast<tidlDelegate*>(buffer);
  };

  kernel_registration.init = [](TfLiteContext* context, const char* buffer,
                                   size_t) -> void* {
    // In the node init phase, initialize MyDelegate instance
    const TfLiteDelegateParams* params =
        reinterpret_cast<const TfLiteDelegateParams*>(buffer);
    tidlDelegate* delegate = new tidlDelegate;
    if (delegate->Init(context, params) != kTfLiteOk) {
      return nullptr;
    }
    return delegate;
  };

  kernel_registration.invoke = [](TfLiteContext* context,
                                   TfLiteNode* node) -> TfLiteStatus {
    tidlDelegate* kernel = reinterpret_cast<tidlDelegate*>(node->user_data);
    return kernel->Invoke(context, node);
  };

  kernel_registration.prepare = [](TfLiteContext* context,
                                    TfLiteNode* node) -> TfLiteStatus {
    tidlDelegate* kernel = reinterpret_cast<tidlDelegate*>(node->user_data);
    return kernel->Prepare(context, node);
  };

  kernel_registration.get_custom_data = [](TfLiteContext* context,
                                    TfLiteNode* node, const char *op_name,
                                    char **node_name, void **node_data) -> TfLiteStatus {
    tidlDelegate* kernel = reinterpret_cast<tidlDelegate*>(node->user_data);
    return kernel->CustomData(context, node, op_name, node_name, node_data);
  };

  return kernel_registration;
}

TfLiteStatus tidlDelegate::CustomData(TfLiteContext* context, TfLiteNode* node, const char *op_,
        char **node_name, void **node_data)
{
    std::string op = std::string(op_);

    if(op == "ddr_stats") {
        std::pair<uint64_t, uint64_t> *v = new std::pair<uint64_t, uint64_t>;

        options_.infer_ops.TIDLRT_getDdrStats(&v->first, &v->second);

        *node_data = static_cast<void *>(v);
    } else if(op == "perf_stats") {
        if(!tidlrt_perfstats)
            return kTfLiteError;

        std::vector<uint64_t> *v = new std::vector<uint64_t>();

        v->push_back(uint64_t(options_.stats->cpIn_time_start));
        v->push_back(uint64_t(options_.stats->cpIn_time_end));
        v->push_back(uint64_t(options_.stats->proc_time_start));
        v->push_back(uint64_t(options_.stats->proc_time_end));
        v->push_back(uint64_t(options_.stats->cpOut_time_start));
        v->push_back(uint64_t(options_.stats->cpOut_time_end));

        *node_data = static_cast<void *>(v);
    } else {
        return kTfLiteError;
    }

    *node_name = const_cast<char *>(subGraphName_);
    return kTfLiteOk;
}

TfLiteStatus DelegatePrepareInfer(TfLiteContext* context, TfLiteDelegate* delegate) {
  // Reserve 1 element, since we need first element to be size, will be updated
  // later.
  FILE *fp;

  TfLiteTIDLDelegateOptions* options_ptr = reinterpret_cast<TfLiteTIDLDelegateOptions*>(delegate->data_);
  tfldelegate_printf(options_ptr->debug_level, "\n ****** In DelegatePrepare ****** \n");

  char fileName[500];
  sprintf((char *)fileName, "%s/allowedNode.txt", options_ptr->artifacts_folder);

  std::vector<int> supported_nodes(1);
  TfLiteIntArray* plan;
  TF_LITE_ENSURE_STATUS(context->GetExecutionPlan(context, &plan));
  TfLiteNode* node;
  TfLiteRegistration* registration;

  // Rudimentary mechanism to check how many TIDL graphs we initialize.
  fp = fopen(fileName, "r");
  if(fp == NULL)
  {
      printf("\n \n ERROR : In infer mode - could not open %s for reading, please make sure artifacts are present in the artifacts_folder(check if compilation is done successfully) .... exiting \n \n", fileName);
      return kTfLiteError;
  }

  int32_t suportedNodeGroupsSize;
  fscanf(fp, "%d\n", &suportedNodeGroupsSize);
  
  for(int i = 0; i < suportedNodeGroupsSize; i++)
  {
    int32_t subgraphSize;
    int32_t nodeIdx;

    fscanf(fp, "%d\n", &subgraphSize);
    
    for(int j = 0; j < subgraphSize; j++)
    {
      fscanf(fp, "%d\n", &nodeIdx);
      supported_nodes.push_back(nodeIdx);
    }
  }

  fclose(fp);

  // Set first element to the number of nodes to replace.
  supported_nodes[0] = supported_nodes.size() - 1;
  printf("\n Number of subgraphs:%d , %d nodes delegated out of %d nodes \n \n", suportedNodeGroupsSize, supported_nodes[0], plan->size);
  //TFLITE_LOG_PROD(tflite::TFLITE_LOG_INFO,
  //                "TIDL delegate: %d nodes delegated out of %d nodes.\n",
  //                supported_nodes[0], plan->size);

  TfLiteRegistration TIDL_kernel_registration =
      GetTIDLNodeRegistration();

  return context->ReplaceNodeSubsetsWithDelegateKernels(
      context, TIDL_kernel_registration,
      reinterpret_cast<TfLiteIntArray*>(supported_nodes.data()), delegate);

  return kTfLiteOk;
}

template <typename T>
T LoadSymbol(void *lib, const char* symbol) {
    T sym = reinterpret_cast<T>(dlsym(lib, symbol));
    assert(sym);
    return sym;
}

TfLiteDelegate* TfLiteTIDLDelegateCreate(TfLiteTIDLDelegateOptions* options)
{
  TfLiteDelegate* delegate = new TfLiteDelegate;

  options->infer_ops.lib = dlopen("libvx_tidl_rt.so", RTLD_NOW | RTLD_GLOBAL);
  assert(options->infer_ops.lib);

  options->infer_ops.TIDLRT_create = LoadSymbol<decltype(options->infer_ops.TIDLRT_create)>  (options->infer_ops.lib, "TIDLRT_create");
  options->infer_ops.TIDLRT_delete = LoadSymbol<decltype(options->infer_ops.TIDLRT_delete)>  (options->infer_ops.lib, "TIDLRT_delete");
  options->infer_ops.TIDLRT_invoke = LoadSymbol<decltype(options->infer_ops.TIDLRT_invoke)>  (options->infer_ops.lib, "TIDLRT_invoke");
  options->infer_ops.TIDLRT_deactivate = LoadSymbol<decltype(options->infer_ops.TIDLRT_deactivate)>(options->infer_ops.lib, "TIDLRT_deactivate");
  options->infer_ops.TIDLRT_setParamsDefault = LoadSymbol<decltype(options->infer_ops.TIDLRT_setParamsDefault)>(options->infer_ops.lib, "TIDLRT_setParamsDefault");
  options->infer_ops.TIDLRT_setTensorDefault = LoadSymbol<decltype(options->infer_ops.TIDLRT_setTensorDefault)>(options->infer_ops.lib, "TIDLRT_setTensorDefault");
  options->infer_ops.TIDLRT_getDdrStats = LoadSymbol<decltype(options->infer_ops.TIDLRT_getDdrStats)>(options->infer_ops.lib, "TIDLRT_getDdrStats");

  delegate->data_ = (void*)options;
  delegate->flags = kTfLiteDelegateFlagsNone;
  delegate->Prepare = DelegatePrepareInfer;
  delegate->CopyFromBufferHandle = nullptr;
  delegate->CopyToBufferHandle = nullptr;
  delegate->FreeBufferHandle = nullptr;
  return delegate;
}

extern "C"{
TfLiteDelegate* tflite_plugin_create_delegate(char** options_keys,
                                              char** options_values,
                                              size_t num_options,
                                              void (*error_handler)(const char*)) {
  tfl_delegate::TfLiteTIDLDelegateOptions *options =
    (tfl_delegate::TfLiteTIDLDelegateOptions*)malloc(sizeof(tfl_delegate::TfLiteTIDLDelegateOptions));

  strcpy(options->artifacts_folder, "");
  options->debug_level = 0;

  char deny_list[512];

  for (uint32_t idx = 0; idx < num_options; idx++)
  {
    if (strcmp("artifacts_folder", options_keys[idx]) == 0)
    {
        strcpy(options->artifacts_folder, options_values[idx]);
    }
    if (strcmp("debug_level", options_keys[idx]) == 0)
    {
        sscanf(options_values[idx], "%d", &options->debug_level);
    }
  }

  if(strcmp(options->artifacts_folder, "") == 0)
  {
    printf("ERROR : Please provide artifacts folders path ... exiting \n");
    exit(-1);
  }

  strcat(options->artifacts_folder, "/");

  TfLiteDelegate *tidl_delegate = TfLiteTIDLDelegateCreate(options);

  return tidl_delegate;

}

void tflite_plugin_destroy_delegate(TfLiteDelegate* delegate) {
  free(delegate->data_);
  delete delegate;
}
}
}  //namespace tfl_delegate
}  // namespace tflite


