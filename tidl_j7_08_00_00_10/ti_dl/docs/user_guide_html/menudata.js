/*
@ @licstart  The following is the entire license notice for the
JavaScript code in this file.

Copyright (C) 1997-2017 by Dimitri van Heesch

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

@licend  The above is the entire license notice
for the JavaScript code in this file
*/
var menudata={children:[
{text:"Main Page",url:"index.html"},
{text:"Open Source Runtime",url:"usergroup0.html",children:[
{text:"Tensorflow Lite runtime",url:"md_tidl_osr_tflrt_tidl.html"},
{text:"ONNX runtime",url:"md_tidl_osr_onnxrt_tidl.html"},
{text:"TVM/Neo-AI-DLR",url:"md_tidl_osr_tvm_tidl.html"},
{text:"Jupyter Notebooks",url:"md_tidl_notebook.html"},
{text:"OSRT Troubleshooting Guide",url:"md_tidl_osr_debug.html"}]},
{text:"TIDL Runtime",url:"usergroup1.html",children:[
{text:"TIDL-RT Overview",url:"md_tidl_overview.html"},
{text:"TIDL-RT Getting Started",url:"md_tidl_user_model_deployment.html"},
{text:"TIDL-RT Supported layers",url:"md_tidl_layers_info.html"},
{text:"TIDL-RT Model Zoo",url:"md_tidl_models_info.html"},
{text:"TIDL-RT Importer",url:"md_tidl_model_import.html"},
{text:"TIDL-RT Inference",url:"md_tidl_sample_test.html"},
{text:"TIDL-RT Build and Run Instructions",url:"usergroup2.html",children:[
{text:"Dependant software components",url:"md_tidl_dependency_info.html"},
{text:"Build Instructions",url:"md_tidl_build_instruction.html"}]},
{text:"TIDL-RT API Guide",url:"itidl__ti_8h.html"},
{text:"Feature Specific Guides",url:"usergroup3.html",children:[
{text:"TIDL-RT Model Importer Design",url:"md_tidl_fsg_import_tool_design.html"},
{text:"TIDL-RT Quantization",url:"md_tidl_fsg_quantization.html"},
{text:"TIDL-RT Meta Architecture Support",url:"md_tidl_fsg_meta_arch_support.html"},
{text:"TIDL-RT Model Visualization",url:"md_tidl_fsg_model_visualization.html"},
{text:"TIDL-RT Input and Output tensors Format ",url:"md_tidl_fsg_io_tensors_format.html"},
{text:"TIDL-RT Output Post Processing in Test Application",url:"md_tidl_fsg_output_post_processing.html"},
{text:"TIDL-RT Batch Processing",url:"md_tidl_fsg_batch_processing.html"}]},
{text:"Troubleshooting",url:"usergroup4.html",children:[
{text:"TIDL-RT performance issues",url:"md_tidl_fsg_steps_to_debug_performance.html"},
{text:"Accuracy/Functional issues",url:"md_tidl_fsg_steps_to_debug_mismatch.html"}]}]},
{text:"TIDL Package Contents",url:"md_tidl_package_contents.html"},
{text:"Appendix",url:"usergroup5.html",children:[
{text:"References",url:"md_tidl_dl_cnn_useful_links.html"},
{text:"Acronyms",url:"md_tidl_acronyms.html"}]},
{text:"TI Disclaimer",url:"md_ti_disclaimer.html"}]}
