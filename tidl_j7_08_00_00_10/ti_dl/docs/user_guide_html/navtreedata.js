/*
@ @licstart  The following is the entire license notice for the
JavaScript code in this file.

Copyright (C) 1997-2017 by Dimitri van Heesch

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

@licend  The above is the entire license notice
for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "TI Deep Learning Product User Guide", "index.html", [
    [ "TIDL - TI Deep Learning Product", "index.html", null ],
    [ "Open Source Runtime", "usergroup0.html", [
      [ "Tensorflow Lite runtime", "md_tidl_osr_tflrt_tidl.html", null ],
      [ "ONNX runtime", "md_tidl_osr_onnxrt_tidl.html", null ],
      [ "TVM/Neo-AI-DLR", "md_tidl_osr_tvm_tidl.html", null ],
      [ "Jupyter Notebooks", "md_tidl_notebook.html", null ],
      [ "OSRT Troubleshooting Guide", "md_tidl_osr_debug.html", null ]
    ] ],
    [ "TIDL Runtime", "usergroup1.html", [
      [ "TIDL-RT Overview", "md_tidl_overview.html", null ],
      [ "TIDL-RT Getting Started", "md_tidl_user_model_deployment.html", null ],
      [ "TIDL-RT Supported layers", "md_tidl_layers_info.html", null ],
      [ "TIDL-RT Model Zoo", "md_tidl_models_info.html", null ],
      [ "TIDL-RT Importer", "md_tidl_model_import.html", null ],
      [ "TIDL-RT Inference", "md_tidl_sample_test.html", null ],
      [ "TIDL-RT Build and Run Instructions", "usergroup2.html", [
        [ "Dependant software components", "md_tidl_dependency_info.html", null ],
        [ "Build Instructions", "md_tidl_build_instruction.html", null ]
      ] ],
      [ "TIDL-RT API Guide", "itidl__ti_8h.html", null ],
      [ "Feature Specific Guides", "usergroup3.html", [
        [ "TIDL-RT Model Importer Design", "md_tidl_fsg_import_tool_design.html", null ],
        [ "TIDL-RT Quantization", "md_tidl_fsg_quantization.html", null ],
        [ "TIDL-RT Meta Architecture Support", "md_tidl_fsg_meta_arch_support.html", null ],
        [ "TIDL-RT Model Visualization", "md_tidl_fsg_model_visualization.html", null ],
        [ "TIDL-RT Input and Output tensors Format ", "md_tidl_fsg_io_tensors_format.html", null ],
        [ "TIDL-RT Output Post Processing in Test Application", "md_tidl_fsg_output_post_processing.html", null ],
        [ "TIDL-RT Batch Processing", "md_tidl_fsg_batch_processing.html", null ]
      ] ],
      [ "Troubleshooting", "usergroup4.html", [
        [ "TIDL-RT performance issues", "md_tidl_fsg_steps_to_debug_performance.html", null ],
        [ "Accuracy/Functional issues", "md_tidl_fsg_steps_to_debug_mismatch.html", null ]
      ] ]
    ] ],
    [ "TIDL Package Contents", "md_tidl_package_contents.html", null ],
    [ "Appendix", "usergroup5.html", [
      [ "References", "md_tidl_dl_cnn_useful_links.html", null ],
      [ "Acronyms", "md_tidl_acronyms.html", null ]
    ] ],
    [ "TI Disclaimer", "md_ti_disclaimer.html", null ]
  ] ]
];

var NAVTREEINDEX =
[
"index.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';