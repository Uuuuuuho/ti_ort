TARGET      := tidl_onnxrt_EP
TARGETTYPE  := dsmo

CPPSOURCES    += ../onnxrt_EP.cpp
CPPSOURCES    += ../onnx-ml.proto3.pb.cpp
CSOURCES    += ../../../common/tivx_utils.c

IDIRS += $($(_MODULE)_SDIR)/../../../inc
IDIRS += $($(_MODULE)_SDIR)/../../../rt/inc
IDIRS += $($(_MODULE)_SDIR)/../../../utils/tidlModelImport
IDIRS += $(PSDK_INSTALL_PATH)/ivision
IDIRS += $($(_MODULE)_SDIR)/../../../utils/onnxImport/onnx_cc
IDIRS += $(TIDL_PROTOBUF_PATH)/src
IDIRS += $($(_MODULE)_SDIR)/../../../../common
IDIRS += $($(_MODULE)_SDIR)/../../../common

IDIRS += $(ONNX_REPO_PATH)/onnxruntime
IDIRS += $(ONNX_REPO_PATH)/include/onnxruntime

IDIRS += $(TIOVX_PATH)/include
IDIRS += $(TIOVX_PATH)/kernels/include
IDIRS += $(TIOVX_PATH)/kernels_j7/include
IDIRS += $(TIOVX_PATH)/kernels_j7/include
IDIRS += $(TIOVX_PATH)/utils/include


ifeq ($(TARGET_PLATFORM),PC)
LDIRS += $(TIDL_PROTOBUF_PATH)/src/.libs
else
LDIRS += $(ONNX_REPO_PATH)/cmake/external/protobuf/cmake
endif

STATIC_LIBS += protobuf-lite

LDIRS += $($(_MODULE)_SDIR)/../../../rt/out/$(TARGET_PLATFORM)/$(TARGET_CPU)/LINUX/$(TARGET_BUILD)

SHARED_LIBS += dl
SHARED_LIBS += vx_tidl_rt



CPPFLAGS  += --std=c++11 \
             -Wno-maybe-uninitialized \
             -Wno-unused-variable \
             -Wno-sign-compare \
             -Wno-unused-but-set-variable \
             -Wno-unused-result \
             -Wno-format-overflow \
             -Wno-format-truncation
