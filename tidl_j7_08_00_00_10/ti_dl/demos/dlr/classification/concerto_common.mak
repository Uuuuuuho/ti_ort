TARGETTYPE  := exe
TARGET      := tidl_dlr_classification

CPPSOURCES    += ../classification_dlr.cpp

# include search directories needed by all platforms
IDIRS += $(LINUX_FS_PATH)/usr/include
IDIRS += $(TIDL_OPENCV_PATH)/modules/core/include
IDIRS += $(TIDL_OPENCV_PATH)/modules/highgui/include
IDIRS += $(TIDL_OPENCV_PATH)/modules/imgcodecs/include
IDIRS += $(TIDL_OPENCV_PATH)/modules/videoio/include
IDIRS += $(TIDL_OPENCV_PATH)/modules/imgproc/include
IDIRS += $(TIDL_OPENCV_PATH)/cmake
# Note: this is linked as shared library. that means you will need to copy
# the build .so file to the target device (PC or EVM)
LDIRS += $($(_MODULE)_SDIR)/../../../../rt/out/$(TARGET_PLATFORM)/$(TARGET_CPU)/LINUX/$(TARGET_BUILD)

SHARED_LIBS += dl
SHARED_LIBS += pthread
SHARED_LIBS += dlr

CPPFLAGS  += --std=c++11
