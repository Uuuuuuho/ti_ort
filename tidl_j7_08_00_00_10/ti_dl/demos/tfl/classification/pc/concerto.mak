ifeq ($(TARGET_PLATFORM), PC)

include $(PRELUDE)
CSOURCES    := 
CPPSOURCES  :=

LDIRS += $(TIOVX_PATH)/lib/PC/$(TARGET_CPU)/$(TARGET_OS)/$(TARGET_BUILD)
LDIRS += $(TF_REPO_PATH)/tensorflow/lite/tools/make/gen/linux_x86_64/lib
# search path for opencv library
LDIRS += $(TIDL_OPENCV_PATH)/cmake/lib
LDIRS += $(TIDL_OPENCV_PATH)/cmake/3rdparty/lib


# opencv libraries
STATIC_LIBS += opencv_imgproc
STATIC_LIBS += opencv_imgcodecs
STATIC_LIBS += opencv_core
STATIC_LIBS += libtiff 
STATIC_LIBS += libwebp
STATIC_LIBS += libpng
STATIC_LIBS += libjpeg-turbo
STATIC_LIBS += IlmImf
STATIC_LIBS += zlib
STATIC_LIBS += libjasper

# get the common make flags from test/src/<plat>/../concerto_common.mak
include $($(_MODULE)_SDIR)/../concerto_common.mak

# override CC so that build uses g++-5
override CC := g++-5

include $(FINALE)

endif
