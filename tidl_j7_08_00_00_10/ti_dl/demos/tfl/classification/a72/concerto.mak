ifeq ($(TARGET_PLATFORM), J7)

include $(PRELUDE)
CSOURCES    := 
CPPSOURCES  :=


LDIRS += $(TIOVX_PATH)/lib/J7/$(TARGET_CPU)/$(TARGET_OS)/$(TARGET_BUILD)
LDIRS += $(LINUX_FS_PATH)/usr/lib

# get the common make flags from test/src/<plat>/../concerto_common.mak
include $($(_MODULE)_SDIR)/../concerto_common.mak
SHARED_LIBS += dl 
SHARED_LIBS += ti_rpmsg_char
SHARED_LIBS += opencv_imgproc
SHARED_LIBS += opencv_imgcodecs
SHARED_LIBS += opencv_core
SHARED_LIBS += z
SHARED_LIBS += tbb
SHARED_LIBS += tiff
SHARED_LIBS += webp
SHARED_LIBS += png16
SHARED_LIBS += jpeg

# This compiler keeps screaming about warnings
CPPFLAGS += -Wno-unknown-pragmas \
	  -Wno-format-overflow \
	  -Wno-maybe-uninitialized \
	  -Wno-unused-variable \
	  -Wno-unused-function \
	  -Wno-sign-compare \
	  -Wno-parentheses \
	  -Wno-conversion-null

include $(FINALE)

endif
