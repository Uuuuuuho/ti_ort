TARGETTYPE  := exe
TARGET      := tidl_tfl_classification
CPPSOURCES    += ../classification.cpp
CSOURCES      += ../../../../common/tivx_utils.c


# include search directories needed by all platforms
IDIRS += $($(_MODULE)_SDIR)/..
IDIRS += $($(_MODULE)_SDIR)/../../../../rt/inc
IDIRS += $($(_MODULE)_SDIR)/../../../../common
IDIRS += $($(_MODULE)_SDIR)/../../../../tfl_delegate/src
IDIRS += $($(_MODULE)_SDIR)/../../../../utils/tidlModelImport
IDIRS += $(TF_REPO_PATH)/tensorflow/lite/tools/make/downloads/flatbuffers/include
IDIRS += $(TF_REPO_PATH)  
IDIRS += $(TF_REPO_PATH)/tensorflow/lite/tools/make/downloads/absl

IDIRS += $(TIDL_OPENCV_PATH)/modules/core/include
IDIRS += $(TIDL_OPENCV_PATH)/modules/highgui/include
IDIRS += $(TIDL_OPENCV_PATH)/modules/imgcodecs/include
IDIRS += $(TIDL_OPENCV_PATH)/modules/videoio/include
IDIRS += $(TIDL_OPENCV_PATH)/modules/imgproc/include
IDIRS += $(TIDL_OPENCV_PATH)/cmake
IDIRS += $(TIOVX_PATH)/include
IDIRS += $(TIOVX_PATH)/kernels/include
IDIRS += $(TIOVX_PATH)/kernels_j7/include
IDIRS += $(TIOVX_PATH)/kernels_j7/include
IDIRS += $(TIOVX_PATH)/utils/include


# Note: this is linked as shared library. that means you will need to copy
# the build .so file to the target device (PC or EVM)
LDIRS += $($(_MODULE)_SDIR)/../../../../tfl_delegate/out/$(TARGET_PLATFORM)/$(TARGET_CPU)/LINUX/$(TARGET_BUILD)
LDIRS += $($(_MODULE)_SDIR)/../../../../rt/out/$(TARGET_PLATFORM)/$(TARGET_CPU)/LINUX/$(TARGET_BUILD)

SHARED_LIBS += dl
SHARED_LIBS += tidl_tfl_delegate
STATIC_LIBS += tensorflow-lite
SHARED_LIBS += pthread
SHARED_LIBS += vx_tidl_rt

CPPFLAGS  += --std=c++11

