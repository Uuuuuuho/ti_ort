// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: tensorflow/core/framework/resource_handle.proto

#include "tensorflow/core/framework/resource_handle.pb.h"

#include <algorithm>

#include <google/protobuf/io/coded_stream.h>
#include <google/protobuf/extension_set.h>
#include <google/protobuf/wire_format_lite.h>
#include <google/protobuf/descriptor.h>
#include <google/protobuf/generated_message_reflection.h>
#include <google/protobuf/reflection_ops.h>
#include <google/protobuf/wire_format.h>
// @@protoc_insertion_point(includes)
#include <google/protobuf/port_def.inc>
namespace tensorflow {
class ResourceHandleDefaultTypeInternal {
 public:
  ::PROTOBUF_NAMESPACE_ID::internal::ExplicitlyConstructed<ResourceHandle> _instance;
} _ResourceHandle_default_instance_;
}  // namespace tensorflow
static void InitDefaultsscc_info_ResourceHandle_tensorflow_2fcore_2fframework_2fresource_5fhandle_2eproto() {
  GOOGLE_PROTOBUF_VERIFY_VERSION;

  {
    void* ptr = &::tensorflow::_ResourceHandle_default_instance_;
    new (ptr) ::tensorflow::ResourceHandle();
    ::PROTOBUF_NAMESPACE_ID::internal::OnShutdownDestroyMessage(ptr);
  }
  ::tensorflow::ResourceHandle::InitAsDefaultInstance();
}

::PROTOBUF_NAMESPACE_ID::internal::SCCInfo<0> scc_info_ResourceHandle_tensorflow_2fcore_2fframework_2fresource_5fhandle_2eproto =
    {{ATOMIC_VAR_INIT(::PROTOBUF_NAMESPACE_ID::internal::SCCInfoBase::kUninitialized), 0, 0, InitDefaultsscc_info_ResourceHandle_tensorflow_2fcore_2fframework_2fresource_5fhandle_2eproto}, {}};

static ::PROTOBUF_NAMESPACE_ID::Metadata file_level_metadata_tensorflow_2fcore_2fframework_2fresource_5fhandle_2eproto[1];
static constexpr ::PROTOBUF_NAMESPACE_ID::EnumDescriptor const** file_level_enum_descriptors_tensorflow_2fcore_2fframework_2fresource_5fhandle_2eproto = nullptr;
static constexpr ::PROTOBUF_NAMESPACE_ID::ServiceDescriptor const** file_level_service_descriptors_tensorflow_2fcore_2fframework_2fresource_5fhandle_2eproto = nullptr;

const ::PROTOBUF_NAMESPACE_ID::uint32 TableStruct_tensorflow_2fcore_2fframework_2fresource_5fhandle_2eproto::offsets[] PROTOBUF_SECTION_VARIABLE(protodesc_cold) = {
  ~0u,  // no _has_bits_
  PROTOBUF_FIELD_OFFSET(::tensorflow::ResourceHandle, _internal_metadata_),
  ~0u,  // no _extensions_
  ~0u,  // no _oneof_case_
  ~0u,  // no _weak_field_map_
  PROTOBUF_FIELD_OFFSET(::tensorflow::ResourceHandle, device_),
  PROTOBUF_FIELD_OFFSET(::tensorflow::ResourceHandle, container_),
  PROTOBUF_FIELD_OFFSET(::tensorflow::ResourceHandle, name_),
  PROTOBUF_FIELD_OFFSET(::tensorflow::ResourceHandle, hash_code_),
  PROTOBUF_FIELD_OFFSET(::tensorflow::ResourceHandle, maybe_type_name_),
};
static const ::PROTOBUF_NAMESPACE_ID::internal::MigrationSchema schemas[] PROTOBUF_SECTION_VARIABLE(protodesc_cold) = {
  { 0, -1, sizeof(::tensorflow::ResourceHandle)},
};

static ::PROTOBUF_NAMESPACE_ID::Message const * const file_default_instances[] = {
  reinterpret_cast<const ::PROTOBUF_NAMESPACE_ID::Message*>(&::tensorflow::_ResourceHandle_default_instance_),
};

const char descriptor_table_protodef_tensorflow_2fcore_2fframework_2fresource_5fhandle_2eproto[] PROTOBUF_SECTION_VARIABLE(protodesc_cold) =
  "\n/tensorflow/core/framework/resource_han"
  "dle.proto\022\ntensorflow\"m\n\016ResourceHandle\022"
  "\016\n\006device\030\001 \001(\t\022\021\n\tcontainer\030\002 \001(\t\022\014\n\004na"
  "me\030\003 \001(\t\022\021\n\thash_code\030\004 \001(\004\022\027\n\017maybe_typ"
  "e_name\030\005 \001(\tB4\n\030org.tensorflow.framework"
  "B\023ResourceHandleProtoP\001\370\001\001b\006proto3"
  ;
static const ::PROTOBUF_NAMESPACE_ID::internal::DescriptorTable*const descriptor_table_tensorflow_2fcore_2fframework_2fresource_5fhandle_2eproto_deps[1] = {
};
static ::PROTOBUF_NAMESPACE_ID::internal::SCCInfoBase*const descriptor_table_tensorflow_2fcore_2fframework_2fresource_5fhandle_2eproto_sccs[1] = {
  &scc_info_ResourceHandle_tensorflow_2fcore_2fframework_2fresource_5fhandle_2eproto.base,
};
static ::PROTOBUF_NAMESPACE_ID::internal::once_flag descriptor_table_tensorflow_2fcore_2fframework_2fresource_5fhandle_2eproto_once;
static bool descriptor_table_tensorflow_2fcore_2fframework_2fresource_5fhandle_2eproto_initialized = false;
const ::PROTOBUF_NAMESPACE_ID::internal::DescriptorTable descriptor_table_tensorflow_2fcore_2fframework_2fresource_5fhandle_2eproto = {
  &descriptor_table_tensorflow_2fcore_2fframework_2fresource_5fhandle_2eproto_initialized, descriptor_table_protodef_tensorflow_2fcore_2fframework_2fresource_5fhandle_2eproto, "tensorflow/core/framework/resource_handle.proto", 234,
  &descriptor_table_tensorflow_2fcore_2fframework_2fresource_5fhandle_2eproto_once, descriptor_table_tensorflow_2fcore_2fframework_2fresource_5fhandle_2eproto_sccs, descriptor_table_tensorflow_2fcore_2fframework_2fresource_5fhandle_2eproto_deps, 1, 0,
  schemas, file_default_instances, TableStruct_tensorflow_2fcore_2fframework_2fresource_5fhandle_2eproto::offsets,
  file_level_metadata_tensorflow_2fcore_2fframework_2fresource_5fhandle_2eproto, 1, file_level_enum_descriptors_tensorflow_2fcore_2fframework_2fresource_5fhandle_2eproto, file_level_service_descriptors_tensorflow_2fcore_2fframework_2fresource_5fhandle_2eproto,
};

// Force running AddDescriptors() at dynamic initialization time.
static bool dynamic_init_dummy_tensorflow_2fcore_2fframework_2fresource_5fhandle_2eproto = (  ::PROTOBUF_NAMESPACE_ID::internal::AddDescriptors(&descriptor_table_tensorflow_2fcore_2fframework_2fresource_5fhandle_2eproto), true);
namespace tensorflow {

// ===================================================================

void ResourceHandle::InitAsDefaultInstance() {
}
class ResourceHandle::_Internal {
 public:
};

ResourceHandle::ResourceHandle()
  : ::PROTOBUF_NAMESPACE_ID::Message(), _internal_metadata_(nullptr) {
  SharedCtor();
  // @@protoc_insertion_point(constructor:tensorflow.ResourceHandle)
}
ResourceHandle::ResourceHandle(::PROTOBUF_NAMESPACE_ID::Arena* arena)
  : ::PROTOBUF_NAMESPACE_ID::Message(),
  _internal_metadata_(arena) {
  SharedCtor();
  RegisterArenaDtor(arena);
  // @@protoc_insertion_point(arena_constructor:tensorflow.ResourceHandle)
}
ResourceHandle::ResourceHandle(const ResourceHandle& from)
  : ::PROTOBUF_NAMESPACE_ID::Message(),
      _internal_metadata_(nullptr) {
  _internal_metadata_.MergeFrom(from._internal_metadata_);
  device_.UnsafeSetDefault(&::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited());
  if (!from._internal_device().empty()) {
    device_.Set(&::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited(), from._internal_device(),
      GetArenaNoVirtual());
  }
  container_.UnsafeSetDefault(&::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited());
  if (!from._internal_container().empty()) {
    container_.Set(&::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited(), from._internal_container(),
      GetArenaNoVirtual());
  }
  name_.UnsafeSetDefault(&::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited());
  if (!from._internal_name().empty()) {
    name_.Set(&::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited(), from._internal_name(),
      GetArenaNoVirtual());
  }
  maybe_type_name_.UnsafeSetDefault(&::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited());
  if (!from._internal_maybe_type_name().empty()) {
    maybe_type_name_.Set(&::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited(), from._internal_maybe_type_name(),
      GetArenaNoVirtual());
  }
  hash_code_ = from.hash_code_;
  // @@protoc_insertion_point(copy_constructor:tensorflow.ResourceHandle)
}

void ResourceHandle::SharedCtor() {
  ::PROTOBUF_NAMESPACE_ID::internal::InitSCC(&scc_info_ResourceHandle_tensorflow_2fcore_2fframework_2fresource_5fhandle_2eproto.base);
  device_.UnsafeSetDefault(&::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited());
  container_.UnsafeSetDefault(&::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited());
  name_.UnsafeSetDefault(&::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited());
  maybe_type_name_.UnsafeSetDefault(&::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited());
  hash_code_ = PROTOBUF_ULONGLONG(0);
}

ResourceHandle::~ResourceHandle() {
  // @@protoc_insertion_point(destructor:tensorflow.ResourceHandle)
  SharedDtor();
}

void ResourceHandle::SharedDtor() {
  GOOGLE_DCHECK(GetArenaNoVirtual() == nullptr);
  device_.DestroyNoArena(&::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited());
  container_.DestroyNoArena(&::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited());
  name_.DestroyNoArena(&::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited());
  maybe_type_name_.DestroyNoArena(&::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited());
}

void ResourceHandle::ArenaDtor(void* object) {
  ResourceHandle* _this = reinterpret_cast< ResourceHandle* >(object);
  (void)_this;
}
void ResourceHandle::RegisterArenaDtor(::PROTOBUF_NAMESPACE_ID::Arena*) {
}
void ResourceHandle::SetCachedSize(int size) const {
  _cached_size_.Set(size);
}
const ResourceHandle& ResourceHandle::default_instance() {
  ::PROTOBUF_NAMESPACE_ID::internal::InitSCC(&::scc_info_ResourceHandle_tensorflow_2fcore_2fframework_2fresource_5fhandle_2eproto.base);
  return *internal_default_instance();
}


void ResourceHandle::Clear() {
// @@protoc_insertion_point(message_clear_start:tensorflow.ResourceHandle)
  ::PROTOBUF_NAMESPACE_ID::uint32 cached_has_bits = 0;
  // Prevent compiler warnings about cached_has_bits being unused
  (void) cached_has_bits;

  device_.ClearToEmpty(&::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited(), GetArenaNoVirtual());
  container_.ClearToEmpty(&::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited(), GetArenaNoVirtual());
  name_.ClearToEmpty(&::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited(), GetArenaNoVirtual());
  maybe_type_name_.ClearToEmpty(&::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited(), GetArenaNoVirtual());
  hash_code_ = PROTOBUF_ULONGLONG(0);
  _internal_metadata_.Clear();
}

const char* ResourceHandle::_InternalParse(const char* ptr, ::PROTOBUF_NAMESPACE_ID::internal::ParseContext* ctx) {
#define CHK_(x) if (PROTOBUF_PREDICT_FALSE(!(x))) goto failure
  ::PROTOBUF_NAMESPACE_ID::Arena* arena = GetArenaNoVirtual(); (void)arena;
  while (!ctx->Done(&ptr)) {
    ::PROTOBUF_NAMESPACE_ID::uint32 tag;
    ptr = ::PROTOBUF_NAMESPACE_ID::internal::ReadTag(ptr, &tag);
    CHK_(ptr);
    switch (tag >> 3) {
      // string device = 1;
      case 1:
        if (PROTOBUF_PREDICT_TRUE(static_cast<::PROTOBUF_NAMESPACE_ID::uint8>(tag) == 10)) {
          auto str = _internal_mutable_device();
          ptr = ::PROTOBUF_NAMESPACE_ID::internal::InlineGreedyStringParser(str, ptr, ctx);
          CHK_(::PROTOBUF_NAMESPACE_ID::internal::VerifyUTF8(str, "tensorflow.ResourceHandle.device"));
          CHK_(ptr);
        } else goto handle_unusual;
        continue;
      // string container = 2;
      case 2:
        if (PROTOBUF_PREDICT_TRUE(static_cast<::PROTOBUF_NAMESPACE_ID::uint8>(tag) == 18)) {
          auto str = _internal_mutable_container();
          ptr = ::PROTOBUF_NAMESPACE_ID::internal::InlineGreedyStringParser(str, ptr, ctx);
          CHK_(::PROTOBUF_NAMESPACE_ID::internal::VerifyUTF8(str, "tensorflow.ResourceHandle.container"));
          CHK_(ptr);
        } else goto handle_unusual;
        continue;
      // string name = 3;
      case 3:
        if (PROTOBUF_PREDICT_TRUE(static_cast<::PROTOBUF_NAMESPACE_ID::uint8>(tag) == 26)) {
          auto str = _internal_mutable_name();
          ptr = ::PROTOBUF_NAMESPACE_ID::internal::InlineGreedyStringParser(str, ptr, ctx);
          CHK_(::PROTOBUF_NAMESPACE_ID::internal::VerifyUTF8(str, "tensorflow.ResourceHandle.name"));
          CHK_(ptr);
        } else goto handle_unusual;
        continue;
      // uint64 hash_code = 4;
      case 4:
        if (PROTOBUF_PREDICT_TRUE(static_cast<::PROTOBUF_NAMESPACE_ID::uint8>(tag) == 32)) {
          hash_code_ = ::PROTOBUF_NAMESPACE_ID::internal::ReadVarint(&ptr);
          CHK_(ptr);
        } else goto handle_unusual;
        continue;
      // string maybe_type_name = 5;
      case 5:
        if (PROTOBUF_PREDICT_TRUE(static_cast<::PROTOBUF_NAMESPACE_ID::uint8>(tag) == 42)) {
          auto str = _internal_mutable_maybe_type_name();
          ptr = ::PROTOBUF_NAMESPACE_ID::internal::InlineGreedyStringParser(str, ptr, ctx);
          CHK_(::PROTOBUF_NAMESPACE_ID::internal::VerifyUTF8(str, "tensorflow.ResourceHandle.maybe_type_name"));
          CHK_(ptr);
        } else goto handle_unusual;
        continue;
      default: {
      handle_unusual:
        if ((tag & 7) == 4 || tag == 0) {
          ctx->SetLastTag(tag);
          goto success;
        }
        ptr = UnknownFieldParse(tag, &_internal_metadata_, ptr, ctx);
        CHK_(ptr != nullptr);
        continue;
      }
    }  // switch
  }  // while
success:
  return ptr;
failure:
  ptr = nullptr;
  goto success;
#undef CHK_
}

::PROTOBUF_NAMESPACE_ID::uint8* ResourceHandle::_InternalSerialize(
    ::PROTOBUF_NAMESPACE_ID::uint8* target, ::PROTOBUF_NAMESPACE_ID::io::EpsCopyOutputStream* stream) const {
  // @@protoc_insertion_point(serialize_to_array_start:tensorflow.ResourceHandle)
  ::PROTOBUF_NAMESPACE_ID::uint32 cached_has_bits = 0;
  (void) cached_has_bits;

  // string device = 1;
  if (this->device().size() > 0) {
    ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::VerifyUtf8String(
      this->_internal_device().data(), static_cast<int>(this->_internal_device().length()),
      ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::SERIALIZE,
      "tensorflow.ResourceHandle.device");
    target = stream->WriteStringMaybeAliased(
        1, this->_internal_device(), target);
  }

  // string container = 2;
  if (this->container().size() > 0) {
    ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::VerifyUtf8String(
      this->_internal_container().data(), static_cast<int>(this->_internal_container().length()),
      ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::SERIALIZE,
      "tensorflow.ResourceHandle.container");
    target = stream->WriteStringMaybeAliased(
        2, this->_internal_container(), target);
  }

  // string name = 3;
  if (this->name().size() > 0) {
    ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::VerifyUtf8String(
      this->_internal_name().data(), static_cast<int>(this->_internal_name().length()),
      ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::SERIALIZE,
      "tensorflow.ResourceHandle.name");
    target = stream->WriteStringMaybeAliased(
        3, this->_internal_name(), target);
  }

  // uint64 hash_code = 4;
  if (this->hash_code() != 0) {
    target = stream->EnsureSpace(target);
    target = ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::WriteUInt64ToArray(4, this->_internal_hash_code(), target);
  }

  // string maybe_type_name = 5;
  if (this->maybe_type_name().size() > 0) {
    ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::VerifyUtf8String(
      this->_internal_maybe_type_name().data(), static_cast<int>(this->_internal_maybe_type_name().length()),
      ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::SERIALIZE,
      "tensorflow.ResourceHandle.maybe_type_name");
    target = stream->WriteStringMaybeAliased(
        5, this->_internal_maybe_type_name(), target);
  }

  if (PROTOBUF_PREDICT_FALSE(_internal_metadata_.have_unknown_fields())) {
    target = ::PROTOBUF_NAMESPACE_ID::internal::WireFormat::InternalSerializeUnknownFieldsToArray(
        _internal_metadata_.unknown_fields(), target, stream);
  }
  // @@protoc_insertion_point(serialize_to_array_end:tensorflow.ResourceHandle)
  return target;
}

size_t ResourceHandle::ByteSizeLong() const {
// @@protoc_insertion_point(message_byte_size_start:tensorflow.ResourceHandle)
  size_t total_size = 0;

  ::PROTOBUF_NAMESPACE_ID::uint32 cached_has_bits = 0;
  // Prevent compiler warnings about cached_has_bits being unused
  (void) cached_has_bits;

  // string device = 1;
  if (this->device().size() > 0) {
    total_size += 1 +
      ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::StringSize(
        this->_internal_device());
  }

  // string container = 2;
  if (this->container().size() > 0) {
    total_size += 1 +
      ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::StringSize(
        this->_internal_container());
  }

  // string name = 3;
  if (this->name().size() > 0) {
    total_size += 1 +
      ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::StringSize(
        this->_internal_name());
  }

  // string maybe_type_name = 5;
  if (this->maybe_type_name().size() > 0) {
    total_size += 1 +
      ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::StringSize(
        this->_internal_maybe_type_name());
  }

  // uint64 hash_code = 4;
  if (this->hash_code() != 0) {
    total_size += 1 +
      ::PROTOBUF_NAMESPACE_ID::internal::WireFormatLite::UInt64Size(
        this->_internal_hash_code());
  }

  if (PROTOBUF_PREDICT_FALSE(_internal_metadata_.have_unknown_fields())) {
    return ::PROTOBUF_NAMESPACE_ID::internal::ComputeUnknownFieldsSize(
        _internal_metadata_, total_size, &_cached_size_);
  }
  int cached_size = ::PROTOBUF_NAMESPACE_ID::internal::ToCachedSize(total_size);
  SetCachedSize(cached_size);
  return total_size;
}

void ResourceHandle::MergeFrom(const ::PROTOBUF_NAMESPACE_ID::Message& from) {
// @@protoc_insertion_point(generalized_merge_from_start:tensorflow.ResourceHandle)
  GOOGLE_DCHECK_NE(&from, this);
  const ResourceHandle* source =
      ::PROTOBUF_NAMESPACE_ID::DynamicCastToGenerated<ResourceHandle>(
          &from);
  if (source == nullptr) {
  // @@protoc_insertion_point(generalized_merge_from_cast_fail:tensorflow.ResourceHandle)
    ::PROTOBUF_NAMESPACE_ID::internal::ReflectionOps::Merge(from, this);
  } else {
  // @@protoc_insertion_point(generalized_merge_from_cast_success:tensorflow.ResourceHandle)
    MergeFrom(*source);
  }
}

void ResourceHandle::MergeFrom(const ResourceHandle& from) {
// @@protoc_insertion_point(class_specific_merge_from_start:tensorflow.ResourceHandle)
  GOOGLE_DCHECK_NE(&from, this);
  _internal_metadata_.MergeFrom(from._internal_metadata_);
  ::PROTOBUF_NAMESPACE_ID::uint32 cached_has_bits = 0;
  (void) cached_has_bits;

  if (from.device().size() > 0) {
    _internal_set_device(from._internal_device());
  }
  if (from.container().size() > 0) {
    _internal_set_container(from._internal_container());
  }
  if (from.name().size() > 0) {
    _internal_set_name(from._internal_name());
  }
  if (from.maybe_type_name().size() > 0) {
    _internal_set_maybe_type_name(from._internal_maybe_type_name());
  }
  if (from.hash_code() != 0) {
    _internal_set_hash_code(from._internal_hash_code());
  }
}

void ResourceHandle::CopyFrom(const ::PROTOBUF_NAMESPACE_ID::Message& from) {
// @@protoc_insertion_point(generalized_copy_from_start:tensorflow.ResourceHandle)
  if (&from == this) return;
  Clear();
  MergeFrom(from);
}

void ResourceHandle::CopyFrom(const ResourceHandle& from) {
// @@protoc_insertion_point(class_specific_copy_from_start:tensorflow.ResourceHandle)
  if (&from == this) return;
  Clear();
  MergeFrom(from);
}

bool ResourceHandle::IsInitialized() const {
  return true;
}

void ResourceHandle::InternalSwap(ResourceHandle* other) {
  using std::swap;
  _internal_metadata_.Swap(&other->_internal_metadata_);
  device_.Swap(&other->device_, &::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited(),
    GetArenaNoVirtual());
  container_.Swap(&other->container_, &::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited(),
    GetArenaNoVirtual());
  name_.Swap(&other->name_, &::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited(),
    GetArenaNoVirtual());
  maybe_type_name_.Swap(&other->maybe_type_name_, &::PROTOBUF_NAMESPACE_ID::internal::GetEmptyStringAlreadyInited(),
    GetArenaNoVirtual());
  swap(hash_code_, other->hash_code_);
}

::PROTOBUF_NAMESPACE_ID::Metadata ResourceHandle::GetMetadata() const {
  return GetMetadataStatic();
}


// @@protoc_insertion_point(namespace_scope)
}  // namespace tensorflow
PROTOBUF_NAMESPACE_OPEN
template<> PROTOBUF_NOINLINE ::tensorflow::ResourceHandle* Arena::CreateMaybeMessage< ::tensorflow::ResourceHandle >(Arena* arena) {
  return Arena::CreateMessageInternal< ::tensorflow::ResourceHandle >(arena);
}
PROTOBUF_NAMESPACE_CLOSE

// @@protoc_insertion_point(global_scope)
#include <google/protobuf/port_undef.inc>
