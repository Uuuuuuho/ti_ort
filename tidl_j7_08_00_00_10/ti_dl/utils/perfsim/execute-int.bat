REM #-------------PUBLIC MODELS (CALSSFICATION)--------------
del /Q tidl_net.bin
ti_cnnperfsim.out.exe config/public/tensorflow/inceptionNetv1.cfg  0 0 2 %1 %2 3 8 0 %3
del /Q tidl_net.bin                                        
ti_cnnperfsim.out.exe config/public/caffe/mobileNet_v2.cfg  0 0 2 %1 %2 3 8 0 %3    
del /Q tidl_net.bin                                        
ti_cnnperfsim.out.exe config/public/caffe/mobileNet1.0v1.cfg  0 0 2 %1 %2 3 8 0 %3  
del /Q tidl_net.bin                                        
ti_cnnperfsim.out.exe config/public/caffe/resNet10.cfg  0 0 2 %1 %2 3 8 0 %3        
del /Q tidl_net.bin
ti_cnnperfsim.out.exe config/public/onnx/resnet18v1.cfg  0 0 2 %1 %2 3 8 0 %3
del /Q tidl_net.bin
ti_cnnperfsim.out.exe config/public/onnx/resnet18v2.cfg  0 0 2 %1 %2 3 8 0 %3
del /Q tidl_net.bin                                          
ti_cnnperfsim.out.exe config/public/tensorflow/resnet50v1.cfg  0 0 2 %1 %2 3 8 0 %3   
del /Q tidl_net.bin                                          
ti_cnnperfsim.out.exe config/public/tensorflow/resnet50v2.cfg  0 0 2 %1 %2 3 8 0 %3   
del /Q tidl_net.bin
ti_cnnperfsim.out.exe config/public/onnx/shufflenet.cfg  0 0 2 %1 %2 3 8 0 %3
del /Q tidl_net.bin                                        
ti_cnnperfsim.out.exe config/public/onnx/vggnet16.cfg  0 0 2 %1 %2 3 8 0 %3 
del /Q tidl_net.bin                                        
ti_cnnperfsim.out.exe config/public/caffe/resNet50.cfg  0 0 2 %1 %2 3 8 0 %3        
del /Q tidl_net.bin                                        
ti_cnnperfsim.out.exe config/public/caffe/densenet-121.cfg  0 0 2 %1 %2 3 8 0 %3    
del /Q tidl_net.bin                                        
ti_cnnperfsim.out.exe config/public/caffe/shuffleNet1xG3x.cfg  0 0 2 %1 %2 3 8 0 %3 
del /Q tidl_net.bin                                        
ti_cnnperfsim.out.exe config/public/caffe/squeezeNet1.1.cfg  0 0 2 %1 %2 3 8 0 %3   
del /Q tidl_net.bin                                        
ti_cnnperfsim.out.exe config/public/caffe/resnext50-32x4d.cfg  0 0 2 %1 %2 3 8 0 %3 
del /Q tidl_net.bin                                        
ti_cnnperfsim.out.exe config/public/caffe/jacintoNet11v2.cfg  0 0 2 %1 %2 3 8 0 %3  

REM #-------------PUBLIC TENSORFLOW MODELS (CALSSFICATION)---------
echo OFF
REM del /Q tidl_net.bin                                          
REM ti_cnnperfsim.out.exe config/public/tensorflow/mobileNetv1.cfg  0 0 2 %1 %2 3 8 0 %3  
REM del /Q tidl_net.bin                                          
REM ti_cnnperfsim.out.exe config/public/tensorflow/mobileNetv2.cfg  0 0 2 %1 %2 3 8 0 %3  

REM #-------------PUBLIC ONNX MODELS (CALSSFICATION)--------------
echo ON
REM del /Q tidl_net.bin
REM ti_cnnperfsim.out.exe config/public/onnx/mobileNetv2.cfg  0 0 2 %1 %2 3 8 0 %3
REM del /Q tidl_net.bin
REM ti_cnnperfsim.out.exe config/public/onnx/squeezenet1_1.cfg  0 0 2 %1 %2 3 8 0 %3

REM #-------------PUBLIC CAFFE MODELS (CALSSFICATION)--------------
REM del /Q tidl_net.bin                                        
REM ti_cnnperfsim.out.exe config/public/caffe/vggNet16.cfg  0 0 2 %1 %2 3 8 0 %3        

REM #-------PRIVATE CAFFE MODELS (OD and Semantic segemntation)-------
REM echo ON
del /Q tidl_net.bin
ti_cnnperfsim.out.exe config/internal/caffe/jpsdNet.cfg  0 10 2 512 512  3 8 0 %3
REM del /Q tidl_net.bin
REM ti_cnnperfsim.out.exe config/internal/caffe/jSegNet.cfg  0 0 2 %1 %2 3 8 0 %3

REM 
REM REM #-------PRIVATE ONNX MODELS (OD and Semantic segemntation)-------
REM echo ON
del /Q tidl_net.bin
ti_cnnperfsim.out.exe config/internal/onnx/tiad_jdepthmodSegNet.cfg  1 3 2 768 384 3 8 0 %3
del /Q tidl_net.bin
ti_cnnperfsim.out.exe config/internal/onnx/tiad_jmodSegNet.cfg  1 2 2 768 384 3 8 0 %3
del /Q tidl_net.bin
ti_cnnperfsim.out.exe config/internal/onnx/tiad_jsegNet.cfg  1 1 2 768 384 3 8 0 %3
del /Q tidl_net.bin
ti_cnnperfsim.out.exe config/internal/onnx/tiad_jmodNet.cfg  1 0 2 768 384 3 8 0 %3
