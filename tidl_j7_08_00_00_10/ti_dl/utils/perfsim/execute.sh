echo OFF

#X #-------------PUBLIC CAFFE MODELS (CALSSFICATION)--------------
del /Q tidl_net.bin
./ti_cnnperfsim.out ./config/public/caffe/jacintoNet11v2.cfg
#X del /Q tidl_net.bin
#X ./ti_cnnperfsim.out ./config/public/caffe/mobileNet1.0v1.cfg
#X rm tidl_net.bin
#X ./ti_cnnperfsim.out ./config/public/caffe/densenet-121.cfg
#X rm tidl_net.bin
#X ./ti_cnnperfsim.out ./config/public/caffe/jdetNet512x512.cfg
#X rm tidl_net.bin
#X ./ti_cnnperfsim.out ./config/public/caffe/mobileNet_v2.cfg
#X rm tidl_net.bin
#X ./ti_cnnperfsim.out ./config/public/caffe/resNet10.cfg
#X rm tidl_net.bin
#X ./ti_cnnperfsim.out ./config/public/caffe/resNet50.cfg
#X rm tidl_net.bin
#X ./ti_cnnperfsim.out ./config/public/caffe/resnext50-32x4d.cfg
#X rm tidl_net.bin
#X ./ti_cnnperfsim.out ./config/public/caffe/shuffleNet1xG3x.cfg
#X rm tidl_net.bin
#X ./ti_cnnperfsim.out ./config/public/caffe/squeezeNet1.1.cfg
#X rm tidl_net.bin
#X ./ti_cnnperfsim.out ./config/public/caffe/vggNet16.cfg

#X #-------------PUBLIC TENSORFLOW MODELS (CALSSFICATION)---------
#X echo OFF
#X rm tidl_net.bin
#X ./ti_cnnperfsim.out ./config/public/tensorflow/inceptionNetv1.cfg
#X rm tidl_net.bin                                          
#X ./ti_cnnperfsim.out ./config/public/tensorflow/mobileNetv1.cfg   
#X rm tidl_net.bin                                          
#X ./ti_cnnperfsim.out ./config/public/tensorflow/mobileNetv2.cfg   
#X rm tidl_net.bin                                          
#X ./ti_cnnperfsim.out ./config/public/tensorflow/resnet50v1.cfg    
#X rm tidl_net.bin                                          
#X ./ti_cnnperfsim.out ./config/public/tensorflow/resnet50v2.cfg    

#X #-------------PUBLIC ONNX MODELS (CALSSFICATION)--------------
#X echo ON
#X rm tidl_net.bin
#X ./ti_cnnperfsim.out ./config/public/onnx/mobileNetv2.cfg
#X rm tidl_net.bin
#X ./ti_cnnperfsim.out ./config/public/onnx/resnet18v1.cfg
#X rm tidl_net.bin
#X ./ti_cnnperfsim.out ./config/public/onnx/resnet18v2.cfg
#X rm tidl_net.bin
#X ./ti_cnnperfsim.out ./config/public/onnx/shufflenet.cfg
#X rm tidl_net.bin
#X ./ti_cnnperfsim.out ./config/public/onnx/squeezenet1_1.cfg

#X #-------PRIVATE CAFFE MODELS (OD and Semantic segemntation)-------
#X echo ON
#X rm tidl_net.bin
#X ./ti_cnnperfsim.out ./config/internal/caffe/jpsdNet.cfg
#X rm tidl_net.bin
#X ./ti_cnnperfsim.out ./config/internal/caffe/jSegNet.cfg

#X #-------PRIVATE ONNX MODELS (OD and Semantic segemntation)-------
#X echo ON
#X rm tidl_net.bin
#X ./ti_cnnperfsim.out ./config/internal/onnx/tiad_jmodNet.cfg
#X rm tidl_net.bin
#X ./ti_cnnperfsim.out ./config/internal/onnx/tiad_jmodSegNet.cfg
#X rm tidl_net.bin

#X ./ti_cnnperfsim.out ./config/internal/onnx/tiad_jsegNet.cfg



rm tidl_net.bin
./ti_cnnperfsim.out ././config/jacintonet11.cfg
#rm tidl_net.bin
#./ti_cnnperfsim.out ././config/densenet-121.cfg
#rm tidl_net.bin
#./ti_cnnperfsim.out ././config/googlenet.cfg
#rm tidl_net.bin
#./ti_cnnperfsim.out ././config/jdetnet.cfg
#rm tidl_net.bin
#./ti_cnnperfsim.out ././config/jsegnet.cfg
#rm tidl_net.bin
#./ti_cnnperfsim.out ././config/mobilenet.cfg
#rm tidl_net.bin
#./ti_cnnperfsim.out ././config/resnet-50.cfg
#rm tidl_net.bin
#./ti_cnnperfsim.out ././config/resnet-10.cfg
#rm tidl_net.bin
#./ti_cnnperfsim.out ././config/shufflenet.cfg
#rm tidl_net.bin
#./ti_cnnperfsim.out ././config/squezzenet1.1.cfg
