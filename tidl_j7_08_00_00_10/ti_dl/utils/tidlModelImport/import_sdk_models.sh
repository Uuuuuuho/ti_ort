# Usage ./import_sdk_models.sh <FolderName>
mkdir -p $1
mkdir -p $1/tivx
mkdir -p $1/psdkra
mkdir -p $1/tivx/tidl_models
mkdir -p $1/psdkra/tidl_models

./out/tidl_model_import.out ../../test/testvecs/config/import/public/caffe/tidl_import_mobilenet_v1.txt
./out/tidl_model_import.out ../../test/testvecs/config/import/public/caffe/tidl_import_mobilenet_v1_u16.txt
./out/tidl_model_import.out ../../test/testvecs/config/import/public/caffe/tidl_import_peeleNet.txt
./out/tidl_model_import.out ../../test/testvecs/config/import/internal/caffe/tidl_import_jpsdNet.txt
./out/tidl_model_import.out ../../test/testvecs/config/import/internal/caffe/tidl_import_tivd.txt
./out/tidl_model_import.out ../../test/testvecs/config/import/internal/onnx/tidl_import_tiad_jsegNet.txt
./out/tidl_model_import.out ../../test/testvecs/config/import/internal/onnx/tidl_import_tiad_jsegNet_u16.txt
./out/tidl_model_import.out ../../test/testvecs/config/import/internal/onnx/tidl_import_tiad_ssd.txt
./out/tidl_model_import.out ../../test/testvecs/config/import/internal/onnx/tidl_import_ti_ad_jdepthModSegNet.txt
./out/tidl_model_import.out ../../test/testvecs/config/import/internal/onnx/tidl_import_tiad_jdakaze.txt

cp ../../test/testvecs/config/tidl_models/caffe/tidl_io_mobilenet_v1_1.bin   $1/tivx/tidl_models/tidl_io_mobilenet_v1_1.bin
cp ../../test/testvecs/config/tidl_models/caffe/tidl_net_mobilenet_v1.bin	$1/tivx/tidl_models/tidl_net_mobilenet_v1.bin
	
cp ../../test/testvecs/config/tidl_models/caffe/tidl_io_mobilenet_v1_u161.bin	$1/tivx/tidl_models/tidl_io_mobilenet_v1_u16_1.bin
cp ../../test/testvecs/config/tidl_models/caffe/tidl_net_mobilenet_v1_u16.bin	$1/tivx/tidl_models/tidl_net_mobilenet_v1_u16.bin

cp ../../test/testvecs/config/tidl_models/caffe/tidl_io_peele_300_1.bin	$1/psdkra/tidl_models/tidl_io_peele_300_1.bin
cp ../../test/testvecs/config/tidl_models/caffe/tidl_net_peele_300.bin	$1/psdkra/tidl_models/tidl_net_peele_300.bin

cp ../../test/testvecs/config/tidl_models/caffe/tidl_io_jpsdNet_1.bin	$1/psdkra/tidl_models/tidl_io_jpsdNet_1.bin
cp ../../test/testvecs/config/tidl_models/caffe/tidl_net_jpsdNet.bin	$1/psdkra/tidl_models/tidl_net_jpsdNet.bin

cp ../../test/testvecs/config/tidl_models/caffe/tidl_io_tivd_1.bin	$1/psdkra/tidl_models/tidl_io_jvdNet_1.bin
cp ../../test/testvecs/config/tidl_models/caffe/tidl_net_tivd.bin	$1/psdkra/tidl_models/tidl_net_jvdNet.bin

cp ../../test/testvecs/config/tidl_models/onnx/tidl_io_onnx_tiad_jdepthmodSegNet_1.bin	$1/psdkra/tidl_models/tidl_io_onnx_tiad_jDepthModSegNet_1.bin
cp ../../test/testvecs/config/tidl_models/onnx/tidl_net_onnx_tiad_jdepthmodSegNet.bin	$1/psdkra/tidl_models/tidl_net_onnx_tiad_jDepthModSegNet.bin

cp ../../test/testvecs/config/tidl_models/onnx/tidl_io_onnx_tiadsegNet_v2_1.bin	$1/psdkra/tidl_models/tidl_io_onnx_tiad_jSegNet_v2_1.bin
cp ../../test/testvecs/config/tidl_models/onnx/tidl_net_onnx_tiadsegNet_v2.bin	$1/psdkra/tidl_models/tidl_net_onnx_tiad_jSegNet_v2.bin

cp ../../test/testvecs/config/tidl_models/onnx/tidl_io_onnx_tiadsegNet_v2_u161.bin	$1/psdkra/tidl_models/tidl_io_onnx_tiad_jSegNet_v2_u16_1.bin
cp ../../test/testvecs/config/tidl_models/onnx/tidl_net_onnx_tiadsegNet_v2_u16.bin	$1/psdkra/tidl_models/tidl_net_onnx_tiad_jSegNet_v2_u16.bin

cp ../../test/testvecs/config/tidl_models/onnx/tidl_io_tiad_ssd_1.bin	$1/psdkra/tidl_models/tidl_io_onnx_tiad_ssd_1.bin
cp ../../test/testvecs/config/tidl_models/onnx/tidl_net_tiad_ssd.bin	$1/psdkra/tidl_models/tidl_net_onnx_tiad_ssd.bin

cp ../../test/testvecs/config/tidl_models/onnx/tidl_io_tiad_ssd_1.bin	$1/psdkra/tidl_models/tidl_io_onnx_tiad_jdakaze_pw2_1.bin
cp ../../test/testvecs/config/tidl_models/onnx/tidl_net_tiad_ssd.bin	$1/psdkra/tidl_models/tidl_net_onnx_tiad_jdakaze_pw2.bin
