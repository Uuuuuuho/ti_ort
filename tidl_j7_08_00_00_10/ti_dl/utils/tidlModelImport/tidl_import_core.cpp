/*
*
* Copyright (c) {2015 - 2020} Texas Instruments Incorporated
*
* All rights reserved not granted herein.
*
* Limited License.
*
* Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
* license under copyrights and patents it now or hereafter owns or controls to make,
* have made, use, import, offer to sell and sell ("Utilize") this software subject to the
* terms herein.  With respect to the foregoing patent license, such license is granted
* solely to the extent that any such patent is necessary to Utilize the software alone.
* The patent license shall not apply to any combinations which include this software,
* other than combinations with devices manufactured by or for TI ("TI Devices").
* No hardware patent is licensed hereunder.
*
* Redistributions must preserve existing copyright notices and reproduce this license
* (including the above copyright notice and the disclaimer and (if applicable) source
* code license limitations below) in the documentation and/or other materials provided
* with the distribution
*
* Redistribution and use in binary form, without modification, are permitted provided
* that the following conditions are met:
*
* *       No reverse engineering, decompilation, or disassembly of this software is
* permitted with respect to any software provided in binary form.
*
* *       any redistribution and use are licensed by TI for use only with TI Devices.
*
* *       Nothing shall obligate TI to provide you with source code for the software
* licensed and provided to you in object code.
*
* If software source code is provided to you, modification and redistribution of the
* source code are permitted provided that the following conditions are met:
*
* *       any redistribution and use of the source code, including any resulting derivative
* works, are licensed by TI for use only with TI Devices.
*
* *       any redistribution and use of any object code compiled from the source code
* and any resulting derivative works, are licensed by TI for use only with TI Devices.
*
* Neither the name of Texas Instruments Incorporated nor the names of its suppliers
*
* may be used to endorse or promote products derived from this software without
* specific prior written permission.
*
* DISCLAIMER.
*
* THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
* OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
* OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
* IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
* BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
* OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
* OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
* OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <float.h>
#ifdef _WIN32
#include <asprintf.h>
#endif

#include <google/protobuf/io/coded_stream.h>
#include <google/protobuf/io/zero_copy_stream_impl.h>
#include <google/protobuf/message.h>
#include <google/protobuf/text_format.h>
using namespace std;
using ::google::protobuf::Message;

#include "ti_dl.h"
#include "tidl_import_api.h"
#include "tidl_import_config.h"
#include "tidl_import_common.h"
#include "tidl_custom_import.h"
#include "tidl_import_common_model_check.h"
#include "perfsim.h"
#include "tidl_deviceInfo.h"
#include "tidl_import_quantize.h"
sTIDL_OrgNetwork_t  orgTIDLNetStructure;
sTIDL_OrgNetwork_t tempTIDLNetStructure;
sTIDL_Network_t tIDLNetStructure;
char inDataNames[TIDL_MAX_ALG_IN_BUFS][TIDL_MAX_DATA_NAME];
char outDataNames[TIDL_MAX_ALG_OUT_BUFS][TIDL_MAX_DATA_NAME];
char outMetaLayerNames[TIDL_MAX_ALG_OUT_BUFS][TIDL_MAX_DATA_NAME];
int32_t numNetInData = 0;
int32_t numNetOutData = 0;
char inConfigFilename[2*FILE_NAME_SIZE];
tidl_import_config gParams;

char* getFileNameFromPath(char* path)
{
  for (int32_t i = strlen(path) - 1; i; i--)
  {
    if ((path[i] == '/') || (path[i] == '\\'))
    {
      return &path[i + 1];
    }
  }
  return path;
}

void getDirFromPath(char* path)
{
  for (int32_t i = strlen(path) - 1; i; i--)
  {
    if ((path[i] == '/') || (path[i] == '\\'))
    {
      path[i] = '\0';
      return;
    }
  }
  path[0] = '\0';
  return;
}
void getDosPath(char* orgpath, char * dosPath)
{
  strcpy(dosPath, orgpath);
  for (int32_t i = 0; i < strlen(dosPath); i++)
  {
    if (dosPath[i] == '/')
    {
      dosPath[i] = '\\';
    }
  }
  return;
}

void getAbsPath(char* path, char * absPath)
{
  char syscmd[500];
  char dosPath[500];
#ifdef _WIN32
  getDosPath(path, dosPath);
  sprintf(syscmd, "dir /b /s %s", dosPath);
#else
  sprintf(syscmd, "readlink -f %s", path);
#endif

#ifdef _WIN32
  FILE * fp = _popen(syscmd,  "r");
#else
  FILE * fp = popen(syscmd,  "r");
#endif

  if (fp == NULL)
  {
    printf("Error while runing command : %s", syscmd);
  }
  fscanf(fp, "%s", absPath);
  fclose(fp);
  return;
}
void stringReplace(char* dstStr, const char* srcStr, const char* toStr, const char* fromStr)
{
  char *dstPos = NULL, *srcPos  = NULL;
  int32_t index ;
  strcpy(dstStr, srcStr);
  dstPos = strstr(dstStr,fromStr) ;
  if (dstPos == NULL)
  {
    printf("substitute string %s not found\n", fromStr);
  }
  else
  {
    srcPos = dstPos + strlen(fromStr);
    //Put the substitue string
    index = 0;
    while (toStr && (toStr[index] != '\0'))
    {
      *(dstPos++) = *(toStr + index);
      index++;
    }

    //Append the remaining part after substitution
    index = 0;
    while (srcPos[index] != '\0')
    {
      *(dstPos++) = *(srcPos + index);
      index++;
    }
    *(dstPos) = '\0';
  }
  return ;
}

void setDefaultParams(tidl_import_config * params)
{
  int32_t i;
  params->randParams          = 0;
  params->modelType           = 0; // 0 - caffe, 1- tensorFlow
  params->quantizationStyle   = TIDL_QuantStyleNP2Fixed;
  params->calibrationOption = 0;
  params->activationRangeMethod = TIDL_ActivationRangeMethodHistogram;
  params->weightRangeMethod = TIDL_WeightRangeMethodMedian;
  params->percentileActRangeShrink = 0.01;
  params->percentileWtRangeShrink = 0.01;
  params->biasCalibrationFactor = 0.05;
  params->biasCalibrationIterations = -1;
  params->quantRoundAdd       = 50; // 0 - caffe, 1- tensorFlow
  params->numParamBits        = 8;
  params->inFileFormat        = 2; // 0 - Encoded, 1- RAW
  params->numFrames           = -1;
  params->numFramesBiasCalibration = -1;  // number of frames to be used for bias calibration
  params->foldBnInConv2D      = 1;
  params->foldPreBnConv2D      = 1;
  params->foldEltWiseInConv2D = 0;
  params->foldMaxPoolInConv2D = 0;
  params->foldDepthToSpaceInConv2D = 1;
  params->postProcType        = 0;
  params->postProcDataId      = 0;
  params->numFeatureBits      = 8;
  params->metaArchType        =-1;
  params->debugTraceLevel     = 0;
  params->writeTraceLevel     = 0;
  params->quantRangeUpdateFactor       = -1.0;
  params->compileConstraintsFlag = DEFAULT_COMPILE_CONSTRAINT_NC_FLAGS;
  params->executeNetworkCompiler = 1;
  params->executeQuantsTool   = 1;
  params->enableHighResOptimization = 0;
  params->enableCustomLayers = 0;
  params->msmcSizeKB = -1;
  params->deviceName = -1;
  params->quantRangeExpansionFactor = 1.0;
  params->addDataConvertToNet = 0;

#ifdef _WIN32
  strcpy((char*)params->tidlStatsTool,"..\\..\\test\\PC_dsp_test_dl_algo.out.exe");
  strcpy((char*)params->perfSimTool,  "..\\..\\utils\\perfsim\\ti_cnnperfsim.out.exe");
  strcpy((char*)params->graphVizTool, "..\\..\\utils\\tidlModelGraphviz\\out\\tidl_graphVisualiser.out.exe");
  strcpy((char*)params->modelDumpTool, "..\\..\\utils\\tidlModelDump\\out\\tidl_dump.out.exe");
  strcpy((char*)params->perfSimConfig, "..\\..\\test\\testvecs\\config\\import\\device_config.cfg");
#else
  strcpy((char*)params->tidlStatsTool, "../../test/PC_dsp_test_dl_algo.out");
  strcpy((char*)params->perfSimTool, "../../utils/perfsim/ti_cnnperfsim.out");
  strcpy((char*)params->graphVizTool, "../../utils/tidlModelGraphviz/out/tidl_graphVisualiser.out");
  strcpy((char*)params->modelDumpTool, "../../utils/tidlModelDump/out/tidl_dump.out");
  strcpy((char*)params->perfSimConfig, "../../test/testvecs/config/import/device_config.cfg");
#endif

  strcpy((char*)params->inDataNamesList, "");
  strcpy((char*)params->outDataNamesList, "");
  strcpy((char*)params->outputFeature16bitNamesList, "");
  strcpy((char*)params->params16bitNamesList, "");
  strcpy((char*)params->fileNameGrpInfo, "");

  for (i = 0; i < TIDL_MAX_ALG_IN_BUFS; i++)
  {
    params->inElementType[i] = TIDL_UnsignedChar;
    params->rawDataInElementType[i] = -1;
    params->inZeroPoint[i] = 0;
    params->inLayout[i] = TIDL_LT_NCHW;
    params->inQuantFactor[i] =  1.0;
    params->inWidth[i]       = -1;
    params->inHeight[i]      = -1;
    params->inNumChannels[i] = -1;
    params->numBatches[i] = 1;
    params->resizeWidth[i]  = -1;
    params->resizeHeight[i] = -1;
    params->inResizeType[i] = TIDL_inResizeTypeDefault;
    params->inDataFormat[i] = TIDL_inDataFormatRGBPlanar;
    params->reserved[i]     = 0;
    params->inDataNorm[i]   = 0;
    params->inYuvFormat[i]  = NOT_VALID;
    params->inDataPadInTIDL[i]  = 0;
  }
  for (i = 0; i < TIDL_MAX_ALG_OUT_BUFS; i++)
  {
    params->outElementSize[i] = -1;
    params->outElementType[i] = -1;
    params->outTensorScale[i] = 1.0;
    params->outZeroPoint[i]   = 0;
    params->outLayout[i]      = TIDL_LT_NCHW;

  }
  params->ddrLayers[0] = -1;
}

/**
----------------------------------------------------------------------------
@ingroup    TIDL_Import
@fn         tidlValidateImportParams
@brief      Function validates input parameters related to tidl import
            sets appropriate error in response to violation from
            expected values.

@param      params : TIDL Create time parameters
@remarks    None
@return     Error related to parameter.
----------------------------------------------------------------------------
*/
int32_t tidlValidateImportParams(tidl_import_config * params)
{
  /* randParams can be either 0 or 1*/
  if(params->foldMaxPoolInConv2D == 1)
  {
     params->foldMaxPoolInConv2D = 0;
     printf("\n foldMaxPoolInConv2D is NOT suported in the current release. Disabling Now");
  }
  if(params->foldEltWiseInConv2D == 1)
  {
     params->foldEltWiseInConv2D = 0;
     printf("\n foldEltWiseInConv2D is NOT suported in the current release. Disabling Now");
  }
  if(params->numParamBits == 32)
  {
    params->numFeatureBits = 32;
  }

  if((params->numParamBits > 8) && (params->numFeatureBits <= 8))
  {
    params->numFeatureBits = 16;
  }
  if((params->numParamBits <= 8) && (params->numFeatureBits > 8))
  {
    params->numParamBits = 12;
  }
  /* Set inElementType based on numFeatureBits if not set in config file */
  if (params->numFeatureBits > 8)
  {
    for (int32_t i = 0; i < TIDL_MAX_ALG_IN_BUFS; i++)
    {
      if(params->numFeatureBits == 32)
      {
        params->inElementType[i] = TIDL_SinglePrecFloat;
      }
      else if(params->inElementType[i] == TIDL_UnsignedChar)
      {
        params->inElementType[i] = TIDL_UnsignedShort;
      }
      else if(params->inElementType[i] == TIDL_SignedChar)
      {
        params->inElementType[i] = TIDL_SignedShort;
      }
    }
  }

  if((params->randParams != 0) && (params->randParams != 1))
  {
    printf("\n Invalid randParams setting : set either 0 or 1");
    return -1;
  }
  /* modelType must be one of the supported types */
  else if((params->modelType != 0) && (params->modelType != 1) && (params->modelType != 2)
          && (params->modelType != 3) && (params->modelType != 4) && (params->modelType != 5)&& (params->modelType != 6))
  {
    printf("\n Invalid modelType parameter setting : set either 0 or 1 or 2 or 3 or 4 or 5 or 6");
    return -1;
  }
  /* Currently quantizationStyle = 1 is supported */
  /*else if(params->quantizationStyle != 1)
  {
    printf("\n Invalid quantizationStyle parameter setting : set it to 1");
    return -1;
  }*/
  /* quantRoundAdd can be 0 to 100 */
  else if((params->quantRoundAdd < 0) || (params->quantRoundAdd > 100))
  {
    printf("\n Invalid quantRoundAdd parameter setting : set it 0 to 100");
    return -1;
  }
  /* numParamBits can be 4 to 12 */
  else if(((params->numParamBits < 4) || (params->numParamBits > 16)) && (params->numParamBits != 32))
  {
    printf("\n Invalid numParamBits parameter setting : set it 4 to 16 or 32 for Float mode");
    return -1;
  }
  /* inFileFormat can be either 0 or 1*/
  else if((params->inFileFormat < 0) && (params->inFileFormat > 2))
  {
    printf("\n Invalid inFileFormat parameter setting");
    return -1;
  }
  /* numFrames can be >0  */
  else if(params->numFrames < -1)
  {
    printf("\n Invalid numFrames parameter setting : set it to >0 ");
    return -1;
  }
  else if(params->numFramesBiasCalibration < -1)
  {
    printf("\n Invalid numFramesBiasCalibration parameter setting : set it to >0 ");
    return -1;
  }
  /* foldBnInConv2D can be either 0 or 1*/
  else if((params->foldBnInConv2D != 0) && (params->foldBnInConv2D != 1))
  {
    printf("\n Invalid foldBnInConv2D parameter setting : set either 0 or 1");
    return -1;
  }
  else if((params->foldPreBnConv2D != 0) && (params->foldPreBnConv2D != 1) && (params->foldPreBnConv2D != 2))
  {
    printf("\n Invalid foldPreBnConv2D parameter setting : set either 0, 1 or 2");
    return -1;
  }
  /* inElementType can be either 0 or 1*/
  else if((params->inElementType[0] < 0) && (params->inElementType[0] > 3 ))
  {
    printf("\n Invalid inElementType parameter setting : set either 0 to 3");
    return -1;
  }
  /* inQuantFactor can be >0  */
  else if(params->inQuantFactor[0] <= 0)
  {
    printf("\n Invalid inQuantFactor parameter setting : set it to >0 ");
    return -1;
  }
  /* inWidth can be >0  */
  else if((params->inWidth[0] < -1) || (params->inWidth[0] == 0))
  {
    printf("\n Invalid inWidth parameter setting : set it to >0 ");
    return -1;
  }
  /* inHeight can be >0  */
  else if((params->inHeight[0] < -1) || (params->inHeight[0] == 0))
  {
    printf("\n Invalid inHeight parameter setting : set it to >0 ");
    return -1;
  }
  /* inNumChannels can be 1 to 1024  */
  else if((params->inNumChannels[0] < -1) || (params->inNumChannels[0] == 0) || (params->inNumChannels[0] > 2048))
  {
    printf("\n Invalid inNumChannels parameter setting : set it 1 to 1024 ");
    return -1;
  }
  else if((params->numBatches[0] < -1) || (params->numBatches[0] == 0) || (params->numBatches[0] > 1024))
  {
    printf("\n Invalid numBatches parameter setting : set it 1 to 1024 ");
    return -1;
  }
  else
  {
    return 0;
  }

}

#define TIDL_CFG_MAX_LINE_SIZE (3000)
int32_t getNumberOfLinesIntheFile(char * fileName)
{
  FILE * fp1;
  int32_t i, lineCnt = 0;
  char line[TIDL_CFG_MAX_LINE_SIZE];

  fp1 = fopen((const char *)fileName, "r");
  if (fp1 == NULL)
  {
    printf("Could not open %s file for reading \n", fileName);
    return 0;
  }
  while (!feof(fp1))
  {
    fgets(line, TIDL_CFG_MAX_LINE_SIZE, fp1);
    lineCnt++;
  }
  return(lineCnt);
}

void tidlQuantStatsTool(tidl_import_config * params)
{
  FILE * fp;
  char sysCommand[500];
  char orgPath[500];
  char absPath[500];
  char fileName[500];
  char qsFileName[500];
  char cfgFileName[500];
  char dirName[500];
  char outDirName[500];

  getAbsPath((char *)params->outputNetFile, outDirName);
  getDirFromPath(outDirName);

  sprintf(qsFileName, "%s/%s.qunat_stats_config.txt", outDirName,getFileNameFromPath(inConfigFilename));
  fp = fopen(qsFileName, "w+");
  if(fp== NULL)
  {
    printf("Could not open config  file %s  \n", qsFileName);
    return;
  }
  fprintf(fp, "inFileFormat    = %d\n",params->inFileFormat);
  fprintf(fp, "numFrames   = %d\n",params->numFrames);
  fprintf(fp, "postProcType   = %d\n",params->postProcType);
  fprintf(fp, "postProcDataId   = %d\n", params->postProcDataId);
  fprintf(fp, "quantRangeUpdateFactor   = %f\n", params->quantRangeUpdateFactor);
  getAbsPath((char *)params->inData, absPath);
  fprintf(fp, "inData   = %s\n",absPath);
  fprintf(fp, "outData   = \"%s/%s_stats_tool_out.bin\"\n", outDirName,getFileNameFromPath(inConfigFilename));

  getAbsPath((char *)params->outputNetFile, absPath);
  fprintf(fp, "netBinFile     = %s\n", absPath);

  sprintf(fileName, "%s%d.bin", (char *)params->outputParamsFile, 1);
  getAbsPath(fileName, absPath);
  fprintf(fp, "ioConfigFile        = %s\n", absPath);
  fprintf(fp, "flowCtrl        = 3\n");
  fprintf(fp, "writeTraceLevel        = %d\n", params->writeTraceLevel);
  fprintf(fp, "debugTraceLevel        = %d\n", params->debugTraceLevel);
  fprintf(fp, "traceDumpBaseName        = \"%s/%s\n", outDirName,getFileNameFromPath(inConfigFilename));


  fclose(fp);

  strcpy(orgPath, qsFileName);
  getAbsPath(orgPath, absPath);

  strcpy(fileName, getFileNameFromPath((char *)params->tidlStatsTool));

  getAbsPath((char *)params->tidlStatsTool, dirName);
  getDirFromPath(dirName);

#ifdef _WIN32
  sprintf(sysCommand, "cd %s && %s s:%s", dirName, fileName, absPath);
#else
  sprintf(sysCommand, "cd %s && ./%s s:%s", dirName, fileName, absPath);
#endif
  if(params->debugTraceLevel > 0)
  {
    printf("%s\n", sysCommand);
  }
  system(sysCommand);

  return;

}

int32_t tidl_getModelSize(uint8_t * fileString)
{
  FILE * fptr;
  int32_t netSize;
  fptr = fopen((const char *)fileString, "rb");
  if (fptr)
  {
    fseek(fptr, 0L, SEEK_END);
    netSize = ftell(fptr);
    fclose(fptr);
    return netSize;
  }
  else
  {
    printf("Could Not Open Files %s\n", fileString);
    return -1;
  }

}

int tidlRunGraphVizTool(tidl_import_config * params)
{
  FILE * fp;
  char sysCommand[500];
  char absPath[500];

  fp = fopen((const char *)params->graphVizTool, "r");
  if (fp == NULL)
  {
    //printf("INFO : Couldn't open graphVizTool file: %s , Skipping Visualization \n", params->graphVizTool);
    return(0);
  }
  fclose(fp);

  getAbsPath((char *)params->graphVizTool, absPath);
  sprintf(sysCommand, "%s %s", absPath, params->outputNetFile);
  if(gParams.debugTraceLevel > 0)
  {
    printf("%s\n", sysCommand);
  }
  system(sysCommand);
}

int tidlRunModelDumpTool(tidl_import_config * params)
{
  FILE * fp;
  char sysCommand[2048];
  char absPath[500];
  char dumpFileName[500];

  fp = fopen((char *)params->modelDumpTool, "r");
  if (fp == NULL)
  {
    //printf("INFO : Couldn't open modelDumpTool file: %s , Skipping\n", params->modelDumpTool);
    return(0);
  }
  fclose(fp);
  getAbsPath((char *)params->modelDumpTool, absPath);

  strcpy(dumpFileName, (char*)params->outputNetFile);
  strcat(dumpFileName, ".txt");

  sprintf(sysCommand, "%s -model %s -io %s1.bin -perfsim -o %s",
          absPath, params->outputNetFile, params->outputParamsFile, dumpFileName);
  if(gParams.debugTraceLevel > 0)
  {
    printf("%s\n", sysCommand);
  }
  system(sysCommand);
}

int tidlWriteTensorNamesToFile(tidl_import_config * params, const char* suffix)
{
  FILE  * fp;
  char *fname;
  asprintf(&fname, "%s%s.layer_info.txt", params->outputNetFile, suffix);
  FILE *layerInfoFile = fopen(fname, "w");
  free(fname);
  int32_t i;
  int modelSize = tidl_getModelSize(params->outputNetFile);
  sTIDL_Network_t * tidlNet = (sTIDL_Network_t *)malloc(modelSize);
  if (tidlNet == NULL)
  {
    printf("Could not Allocate memory for model read\n");
    return 0;
  }
  fp = fopen((const char *)params->outputNetFile, "rb");
  if (fp)
  {
    fread(tidlNet, 1, modelSize, fp);
    fclose(fp);
  }
  else
  {
    printf("Could not open %s\n", params->outputNetFile);
    return 0;
  }

  for (i = 0; i < tidlNet->numLayers; i++)
  {
    fprintf(layerInfoFile, "%d %d %s \n", i, tidlNet->TIDLLayers[i].outData[0].dataId,
      TIDL_getOutDataName(&orgTIDLNetStructure, tidlNet->TIDLLayers[i].outData[0].dataId));
  }
  free(tidlNet);
  fclose(layerInfoFile);
}

int tidlRunPerfSimTool(tidl_import_config * params)
{
  FILE * fp;
  char sysCommand[500];
  char orgPath[500];
  char absPath[500];
  char fileName[500];
  char psFileName[500];
  char dirName[500];
  sPerfSim_t * perSimInfo = (sPerfSim_t *)malloc(sizeof(sPerfSim_t));
  sPerfSim_t * orderedPerSimInfo = (sPerfSim_t *)malloc(sizeof(sPerfSim_t));

  int i, j, k, l, foundInData, numFeatureBytes;
  int32_t currLayersGroupId = 1;

  fp = fopen((const char *)params->perfSimConfig, "r");
  if (fp == NULL)
  {
    printf("INFO : Couldn't open perfSimConfig file: %s , Skipping Performance Simulation \n", params->perfSimConfig);
    return(0);
  }
  fclose(fp);

  fp = fopen((const char *)params->perfSimTool, "r");
  if (fp == NULL)
  {
    printf("Couldn't open perfSimTool file: %s  \n", params->perfSimTool);
    return(0);
  }
  fclose(fp);


  getAbsPath((char *)params->perfSimConfig, absPath);

  getAbsPath((char *)params->outputNetFile, dirName);
  getDirFromPath(dirName);

#ifdef _WIN32
  sprintf(psFileName, "%s\\%s.perf_sim_config.txt", dirName, getFileNameFromPath(inConfigFilename));
  sprintf(sysCommand, "copy /Y %s %s", absPath, psFileName);
#else
  sprintf(psFileName, "%s/%s.perf_sim_config.txt", dirName, getFileNameFromPath(inConfigFilename));
  sprintf(sysCommand, "cp %s %s", absPath, psFileName);
#endif
  if(gParams.debugTraceLevel > 0)
  {
    printf("%s\n", sysCommand);
  }
  system(sysCommand);


  fp = fopen(psFileName, "a+");
  if (fp == NULL)
  {
    printf("Could not open config  file %s  \n", psFileName);
    return 0;
  }
  getAbsPath((char *)params->outputNetFile, absPath);
  fprintf(fp, "FILENAME_NET     = %s\n", absPath);
  fprintf(fp, "FILEFORMAT_NET     = -1\n");
  if(strcmp((char *)params->fileNameGrpInfo,"") != 0)
  {
    getAbsPath((char *)params->fileNameGrpInfo, absPath);
    fprintf(fp, "FILENAME_GRPINFO     = %s\n", absPath);
  }

  if (params->msmcSizeKB > 0 )
  {
    fprintf(fp, "MSMCSIZE_KB     = %d\n", params->msmcSizeKB);
  }
  if (params->deviceName != -1 )
  {
    fprintf(fp, "DEVICE_NAME     = %d\n", params->deviceName);
  }

#ifdef _WIN32
  fprintf(fp, "OUTPUT_DIR     = %s\\%s\n", dirName,getFileNameFromPath((char *)params->outputNetFile));
#else
  fprintf(fp, "OUTPUT_DIR     = %s/%s\n", dirName,getFileNameFromPath((char *)params->outputNetFile));
#endif

  if ((params->numParamBits > 8) || (params->numFeatureBits > 8))
  {
	  fprintf(fp, "DATATYPE     =   1\n");
  }
  if(params->ddrLayers[0] != -1) {
      int32_t *ddrLayers = &params->ddrLayers[0];

	  fprintf(fp, "DDRLAYERS    = ");
      while(*ddrLayers >= 0) {
          fprintf(fp, "%d ", *ddrLayers);
          ddrLayers++;
      }
	  fprintf(fp, "-1\n");
  }
  fclose(fp);

  strcpy(orgPath, psFileName);
  getAbsPath(orgPath, absPath);

  strcpy(fileName, getFileNameFromPath((char *)params->perfSimTool));

  getAbsPath((char *)params->perfSimTool, dirName);
  getDirFromPath(dirName);

  /* Add default compile  constraint incase user gives something else from the config file */
  params->compileConstraintsFlag |= DEFAULT_COMPILE_CONSTRAINT_NC_FLAGS;

#ifdef _WIN32
  sprintf(sysCommand, "cd %s && %s %s %d %d 2", dirName, fileName, absPath, params->compileConstraintsFlag, params->reserved[0]);
#else
  sprintf(sysCommand, "cd %s && ./%s %s %d %d 2", dirName, fileName, absPath, params->compileConstraintsFlag, params->reserved[0]);
#endif
  if(gParams.debugTraceLevel > 0)
  {
    printf("%s\n", sysCommand);
  }
  system(sysCommand);

  getAbsPath((char *)params->outputNetFile, dirName);
  getDirFromPath(dirName);

#ifdef _WIN32
 sprintf(dirName + strlen(dirName), "\\%s\\perfSimInfo.bin", getFileNameFromPath((char *)params->outputNetFile));
#else
  sprintf(dirName + strlen(dirName), "/%s/perfSimInfo.bin", getFileNameFromPath((char *)params->outputNetFile));
#endif
  stringReplace(dirName, dirName, NULL, "tidl_net_") ;
  stringReplace(dirName, dirName, NULL, ".bin") ;

  fp = fopen(dirName, "rb");
  if (fp)
  {
    fread(perSimInfo, 1, sizeof(sPerfSim_t), fp);
    fclose(fp);

#if 0 //Why to remove the bin file generated, its useful for dump comparison
#ifdef _WIN32
  sprintf(sysCommand, "del %s", dirName);
#else
  sprintf(sysCommand, "rm %s", dirName);
#endif
  system(sysCommand);
#endif

  }
  else
  {
    printf("Could not open %s\n", dirName);
    return 0;
  }
  memcpy(orderedPerSimInfo, perSimInfo, sizeof(sPerfSim_t));

  int modelSize = tidl_getModelSize(params->outputNetFile);
  sTIDL_Network_t * tidlNet = (sTIDL_Network_t *)malloc(modelSize);
  if(tidlNet == NULL)
  {
    printf("Could not Allocate memory for model read\n");
    return 0;
  }
  fp = fopen((const char *)params->outputNetFile, "rb");
  if (fp)
  {
    fread(tidlNet, 1, modelSize, fp);
    fclose(fp);
  }
  else
  {
    printf("Could not open %s\n", params->outputNetFile);
    return 0;
  }

  tidlNet->deviceName = perSimInfo->simConfig.deviceName; //Copying over the deviceName into the n/w structure

  for (i = 0; i < tidlNet->numLayers; i++)
  {
    /* Currently do this only for convolution layers but in general this should be true for all layers */
    numFeatureBytes = tidl_getElementSizeInBits(tidlNet->TIDLLayers[i].outData[0].elementType) / 8;

    /* Batch processing exit conditions for Large feature map and ST cases */
    if (params->numBatches[0] > 1)
    {
      if (perSimInfo->sdataFlowInfo[i].numSplit > 1)
      {
        printf("ERROR: Batch processing is not supported with large feature map case \n");
        exit(-1);
      }
    }

    //printf("%2d : | %d %d | %d %d |\n", i, tidlNet->TIDLLayers[i].outData[0].padW, perSimInfo->sdataFlowInfo[i].bufInfo[OUT_FEAT_MAP][WRITE].padC,
    //  tidlNet->TIDLLayers[i].outData[0].padH, perSimInfo->sdataFlowInfo[i].bufInfo[OUT_FEAT_MAP][WRITE].padR);
    if(tidlNet->TIDLLayers[i].numOutBufs > 0)
    {
      tidlNet->TIDLLayers[i].outData[0].pitch[TIDL_LINE_PITCH] = tidlNet->TIDLLayers[i].outData[0].dimValues[TIDL_DIM_WIDTH] +
        perSimInfo->sdataFlowInfo[i].bufInfo[OUT_FEAT_MAP][WRITE].padC;
      tidlNet->TIDLLayers[i].outData[0].pitch[TIDL_CHANNEL_PITCH] = perSimInfo->sdataFlowInfo[i].bufInfo[OUT_FEAT_MAP][WRITE].bufWidth/numFeatureBytes;
      //tidlNet->TIDLLayers[i].outData[0].pitch[TIDL_ROI_PITCH] = tidlNet->TIDLLayers[i].outData[0].dimValues[TIDL_DIM_NUMCH] * tidlNet->TIDLLayers[i].outData[0].pitch[TIDL_CHANNEL_PITCH];
      /* bufSize includes size for all the Batches, so divide bufSize with numBatches to get each batcPitch */
      tidlNet->TIDLLayers[i].outData[0].pitch[TIDL_ROI_PITCH] = perSimInfo->sdataFlowInfo[i].bufInfo[OUT_FEAT_MAP][WRITE].bufSize/(numFeatureBytes * tidlNet->TIDLLayers[i].outData[0].dimValues[TIDL_DIM_BATCH]);
      tidlNet->TIDLLayers[i].outData[0].padW = perSimInfo->sdataFlowInfo[i].bufInfo[OUT_FEAT_MAP][WRITE].padC;
      tidlNet->TIDLLayers[i].outData[0].padH = perSimInfo->sdataFlowInfo[i].bufInfo[OUT_FEAT_MAP][WRITE].padR;
      if((tidlNet->TIDLLayers[i].layerType == TIDL_PoolingLayer) && (tidlNet->TIDLLayers[i].layerParams.poolParams.kernelW == 0))
      { 
        /* Bug fix for TIDL-1338 */
        /* In the global pooling when MSMC staging enabled then pitch is coming >1, so making it to 1 */
        perSimInfo->sdataFlowInfo[i].bufInfo[OUT_FEAT_MAP][READ].bufWidth = 1;
        /* The TIDL_LINE_PITCH updated above should be restored back incase of golbalAvg pooling layer is the last layer in the network with pad > 0 */
        if (tidltb_isOutDataBuff(tidlNet, tidlNet->TIDLLayers[i].outData[0].dataId,currLayersGroupId))
        {
          tidlNet->TIDLLayers[i].outData[0].pitch[TIDL_LINE_PITCH] = tidlNet->TIDLLayers[i].outData[0].dimValues[TIDL_DIM_WIDTH];
        }
        tidlNet->TIDLLayers[i].outData[0].pitch[TIDL_CHANNEL_PITCH] = perSimInfo->sdataFlowInfo[i].bufInfo[OUT_FEAT_MAP][WRITE].bufWidth = tidlNet->TIDLLayers[i].outData[0].dimValues[TIDL_DIM_WIDTH];
        tidlNet->TIDLLayers[i].outData[0].pitch[TIDL_ROI_PITCH] = perSimInfo->sdataFlowInfo[i].bufInfo[OUT_FEAT_MAP][WRITE].bufSize = tidlNet->TIDLLayers[i].outData[0].pitch[TIDL_CHANNEL_PITCH]*tidlNet->TIDLLayers[i].outData[0].dimValues[TIDL_DIM_NUMCH];
        tidlNet->TIDLLayers[i].outData[0].padW = perSimInfo->sdataFlowInfo[i].bufInfo[OUT_FEAT_MAP][WRITE].padC = 0;
        tidlNet->TIDLLayers[i].outData[0].padH = perSimInfo->sdataFlowInfo[i].bufInfo[OUT_FEAT_MAP][WRITE].padR = 0;
      }
      else if(tidlNet->TIDLLayers[i].layerType == TIDL_InnerProductLayer)
      {
        tidlNet->TIDLLayers[i].outData[0].pitch[TIDL_CHANNEL_PITCH] =  tidlNet->TIDLLayers[i].outData[0].pitch[TIDL_ROI_PITCH];
      }

      if(((tidlNet->TIDLLayers[i].layersGroupId != currLayersGroupId) && tidltb_isInDataBuff(tidlNet, tidlNet->TIDLLayers[i].outData[0].dataId,currLayersGroupId)) ||
              tidltb_isOutDataBuff(tidlNet, tidlNet->TIDLLayers[i].outData[0].dataId,currLayersGroupId) )
      {

          int32_t inPadL = tidlNet->TIDLLayers[i].outData[0].padW;
          int32_t inPadT = tidlNet->TIDLLayers[i].outData[0].padH;
          int32_t inPadR = 0;
          int32_t inPadB = 0;
          if(tidlNet->TIDLLayers[i].outData[0].padW)
            inPadB = tidlNet->TIDLLayers[i].outData[0].padH + 1;
          else
            inPadB = tidlNet->TIDLLayers[i].outData[0].padH;

          int32_t tempChannelPicth = tidlNet->TIDLLayers[i].outData[0].pitch[TIDL_LINE_PITCH] *
                                               (tidlNet->TIDLLayers[i].outData[0].dimValues[TIDL_DIM_HEIGHT] + inPadT + inPadB);

          if(tempChannelPicth < tidlNet->TIDLLayers[i].outData[0].pitch[TIDL_CHANNEL_PITCH])
          {
            int32_t totalHeight = (tidlNet->TIDLLayers[i].outData[0].pitch[TIDL_CHANNEL_PITCH] +
                                   tidlNet->TIDLLayers[i].outData[0].pitch[TIDL_LINE_PITCH] -1)/ tidlNet->TIDLLayers[i].outData[0].pitch[TIDL_LINE_PITCH];
            tidlNet->TIDLLayers[i].outData[0].pitch[TIDL_CHANNEL_PITCH] = totalHeight * tidlNet->TIDLLayers[i].outData[0].pitch[TIDL_LINE_PITCH];
          }
          else
          {
            tidlNet->TIDLLayers[i].outData[0].pitch[TIDL_CHANNEL_PITCH] = tempChannelPicth;
          }

          tidlNet->TIDLLayers[i].outData[0].pitch[TIDL_ROI_PITCH] = tidlNet->TIDLLayers[i].outData[0].pitch[TIDL_CHANNEL_PITCH]*
                                                                    tidlNet->TIDLLayers[i].outData[0].dimValues[TIDL_DIM_NUMCH];


      }
    }

  }

  for (i = 0; i < tidlNet->numLayers; i++)
  {
    for (j = 0; j < tidlNet->TIDLLayers[i].numInBufs; j++)
    {
      foundInData = 0;
      for (k = 0; ((k < tidlNet->numLayers) && (foundInData == 0)); k++)
      {
        for (l = 0; ((l < tidlNet->TIDLLayers[k].numOutBufs) && (foundInData == 0)); l++)
        {
          if (tidlNet->TIDLLayers[i].inData[j].dataId == tidlNet->TIDLLayers[k].outData[l].dataId)
          {
            if ((tidlNet->TIDLLayers[i].layerType == TIDL_PoolingLayer) && (tidlNet->TIDLLayers[i].layerParams.poolParams.kernelW == 0))
            {
              perSimInfo->sdataFlowInfo[k].bufInfo[OUT_FEAT_MAP][WRITE].padCFillZeros = perSimInfo->sdataFlowInfo[k].bufInfo[OUT_FEAT_MAP][WRITE].padC;
            }
            tidlNet->TIDLLayers[i].inData[j].padW = tidlNet->TIDLLayers[k].outData[l].padW;
            tidlNet->TIDLLayers[i].inData[j].padH = tidlNet->TIDLLayers[k].outData[l].padH;
            tidlNet->TIDLLayers[i].inData[j].pitch[TIDL_LINE_PITCH]    = tidlNet->TIDLLayers[k].outData[l].pitch[TIDL_LINE_PITCH];
            tidlNet->TIDLLayers[i].inData[j].pitch[TIDL_CHANNEL_PITCH] = tidlNet->TIDLLayers[k].outData[l].pitch[TIDL_CHANNEL_PITCH];
            tidlNet->TIDLLayers[i].inData[j].pitch[TIDL_ROI_PITCH]     = tidlNet->TIDLLayers[k].outData[l].pitch[TIDL_ROI_PITCH];
            foundInData = 1;
          }
        }
      }
      if (foundInData == 0)
      {
        printf("Could not find Indata for data ID %d \n", tidlNet->TIDLLayers[i].inData[j].dataId);
      }
    }
  }
  int netSize = (char*)(&tidlNet->TIDLLayers[tidlNet->numLayers]) - (char*)(tidlNet);
  sTIDL_Network_t * tempTidlNet = (sTIDL_Network_t *)malloc(netSize);
  memcpy(tempTidlNet, tidlNet, netSize);
  for (i = 0; i < tidlNet->numLayers; i++)
  {
    tempTidlNet->TIDLLayers[i] = tidlNet->TIDLLayers[perSimInfo->layerExecutionOrder[i]];
    orderedPerSimInfo->sdataFlowInfo[i] = perSimInfo->sdataFlowInfo[perSimInfo->layerExecutionOrder[i]];
  }
  memcpy(tidlNet, tempTidlNet, netSize);

  uint32_t alignedModelSize = modelSize;
  sBuffer_t buf;
  buf.ptr = (void*)orderedPerSimInfo;
  /* We don't want to copy dataFlowInfo for MAX_LAYERS but we still want to
  copy other output in sPerfSim_t hence find the size of only relevant information
  needed during TIDL inference. The assumption is nothing beyond sdataFlowInfo will
  be used by TIDL */
  buf.bufSize = sizeof(sPerfSim_t ) -
                    sizeof(orderedPerSimInfo->layerExecutionOrder) -
                    sizeof(orderedPerSimInfo->sdataFlowInfo) +
                    (sizeof(sDataFlowInfo_t) * tidlNet->numLayers);

  tidlNet->dataFlowInfo = TIDL_alignParamsWrite(NULL, &buf, &alignedModelSize, 1);

  fp = fopen((const char *)params->outputNetFile, "wb+");
  if (fp)
  {
    fwrite(tidlNet, 1, modelSize, fp);
  }
  else
  {
    printf("Could not open %s\n", params->outputNetFile);
    return 0;
  }
  alignedModelSize = modelSize;
  TIDL_writeInfo(tidlNet,
                        &orgTIDLNetStructure,
                        (const char *)params->outputParamsFile,
                        tidlNet->numLayers,
                        1,
                        orderedPerSimInfo,
                        &gParams);

  TIDL_alignParamsWrite(fp, &buf, &alignedModelSize, 1);
  if ( orderedPerSimInfo != NULL )
  {
    free(orderedPerSimInfo);
  }
  free(perSimInfo);
  free(tidlNet);
  fclose(fp);
  return 0;

}


/* Copy tensor stats from float model after STATS_COLLECTION_FLOAT calibration
 *                   to   quantized model
 * skip_input_datalayers: we don't want to update the input DataLayers
 */
void TIDL_copyTensorStats(sTIDL_OrgNetwork_t * pOrgTIDLNetStructureDst,
                          sTIDL_OrgNetwork_t * pOrgTIDLNetStructureSrc,
                          int32_t skip_input_datalayers)
{
  int32_t i, layerIdx;
  pOrgTIDLNetStructureDst->quantStats = pOrgTIDLNetStructureSrc->quantStats;
  for ( layerIdx = 0; layerIdx <pOrgTIDLNetStructureSrc->numLayers; layerIdx++)
  {
    sTIDL_LayerPC_t& srcLayer = pOrgTIDLNetStructureSrc->TIDLPCLayers[layerIdx];
    sTIDL_LayerPC_t& dstLayer = pOrgTIDLNetStructureDst->TIDLPCLayers[layerIdx];

    if (skip_input_datalayers && srcLayer.layerType == TIDL_DataLayer &&
                                 srcLayer.numInBufs == -1)
      continue;

    for ( i = 0; i < srcLayer.numInBufs; i++)
    {
      dstLayer.inData[i].maxTensorValue = srcLayer.inData[i].maxTensorValue;
      dstLayer.inData[i].minTensorValue = srcLayer.inData[i].minTensorValue;
      dstLayer.inData[i].tensorScale    = srcLayer.inData[i].tensorScale;
      dstLayer.inData[i].roundBits      = srcLayer.inData[i].roundBits;

    }

    for ( i = 0; i < srcLayer.numOutBufs; i++)
    {
      dstLayer.outData[i].maxTensorValue = srcLayer.outData[i].maxTensorValue;
      dstLayer.outData[i].minTensorValue = srcLayer.outData[i].minTensorValue;
      dstLayer.outData[i].tensorScale    = srcLayer.outData[i].tensorScale;
      dstLayer.outData[i].roundBits      = srcLayer.outData[i].roundBits;
    }
  }
}
int32_t TIDL_readQuantStats(sTIDL_OrgNetwork_t * pOrgTIDLNetStructure, tidl_import_config * params, uint32_t numLayers)
{
  int32_t layerIdx;
  int32_t idx;

  FILE  * fp;
  int32_t i;
  int32_t isQuantStatsAvail = 0;
  int modelSize = tidl_getModelSize(params->outputNetFile);
  sTIDL_Network_t * tidlNet = (sTIDL_Network_t *)malloc(modelSize);
  if (tidlNet == NULL)
  {
    printf("Could not Allocate memory for model read\n");
    return -1;
  }
  fp = fopen((const char *)params->outputNetFile, "rb");
  if (fp)
  {
    fread(tidlNet, 1, modelSize, fp);
    fclose(fp);
  }
  else
  {
    printf("Could not open %s\n", params->outputNetFile);
    return -1;
  }

  pOrgTIDLNetStructure->quantStats = TIDL_QUANT_STATS_NONE;

  if ( tidlNet->isQuantStatsAvailable == 1 )
  {
    pOrgTIDLNetStructure->quantStats = TIDL_QUANT_STATS_FIXED;
    if (tidlNet->weightsElementSize == 4 )
    {
      pOrgTIDLNetStructure->quantStats = TIDL_QUANT_STATS_FLOAT;
    }

    for ( layerIdx = 0; layerIdx < numLayers; layerIdx++)
    {
      for ( idx = 0; idx < tidlNet->TIDLLayers[layerIdx].numInBufs; idx++)
      {
        pOrgTIDLNetStructure->TIDLPCLayers[layerIdx].inData[idx].maxTensorValue =
            tidlNet->TIDLLayers[layerIdx].inData[idx].maxTensorValue;
        pOrgTIDLNetStructure->TIDLPCLayers[layerIdx].inData[idx].minTensorValue=
            tidlNet->TIDLLayers[layerIdx].inData[idx].minTensorValue;
        pOrgTIDLNetStructure->TIDLPCLayers[layerIdx].inData[idx].tensorScale =
            tidlNet->TIDLLayers[layerIdx].inData[idx].tensorScale;
        pOrgTIDLNetStructure->TIDLPCLayers[layerIdx].inData[idx].roundBits =
            tidlNet->TIDLLayers[layerIdx].inData[idx].roundBits;
      }

      for ( idx = 0; idx < tidlNet->TIDLLayers[layerIdx].numOutBufs; idx++)
      {
        pOrgTIDLNetStructure->TIDLPCLayers[layerIdx].outData[idx].maxTensorValue =
            tidlNet->TIDLLayers[layerIdx].outData[idx].maxTensorValue;
        pOrgTIDLNetStructure->TIDLPCLayers[layerIdx].outData[idx].minTensorValue=
            tidlNet->TIDLLayers[layerIdx].outData[idx].minTensorValue;
        pOrgTIDLNetStructure->TIDLPCLayers[layerIdx].outData[idx].tensorScale =
            tidlNet->TIDLLayers[layerIdx].outData[idx].tensorScale;
        pOrgTIDLNetStructure->TIDLPCLayers[layerIdx].outData[idx].roundBits =
            tidlNet->TIDLLayers[layerIdx].outData[idx].roundBits;

      }
    }
  }

  isQuantStatsAvail = tidlNet->isQuantStatsAvailable;

  if ( tidlNet != NULL )
  {
    free(tidlNet);
  }
  return (isQuantStatsAvail);
}

int tidlRunQuantStatsTool(sTIDL_OrgNetwork_t * pOrgTIDLNetStructure,
                                                sTIDL_Network_t      *  pTIDLNetStructure,
                                                tidl_import_config * params,
                                                uint32_t numLayers)
{
  FILE * fp;
  int32_t quantStatus;

  if(!params->executeQuantsTool)
  {
    return 0;
  }
  fp = fopen((const char *)params->inData, "r");
  if (fp == NULL)
  {
    printf("INFO : Couldn't open inData file: %s  , Skipping Range Collection for Quantization \n", params->inData);
    return(0);
  }
  fclose(fp);

  fp = fopen((const char *)params->tidlStatsTool, "r");
  if (fp == NULL)
  {
    printf("Couldn't open tidlStatsTool file: %s  \n", params->tidlStatsTool);
    return(0);
  }
  fclose(fp);

  if (params->numFrames == -1)
  {
    if (params->inFileFormat == 2)
    {
      params->numFrames = getNumberOfLinesIntheFile((char *)params->inData);
    }
    else if ((params->inFileFormat == 0) || (params->inFileFormat == 1) || (params->inFileFormat == 3))
    {
      params->numFrames = 1;
    }
  }
  if (params->numFrames > 0)
  {
    printf("\n~~~~~Running TIDL in PC emulation mode to collect Activations range for each layer~~~~~\n");
    tidlQuantStatsTool(params);
  }

  /* Read back the stats from the model written after quants stats tool */
 quantStatus = TIDL_readQuantStats(pOrgTIDLNetStructure, params, pOrgTIDLNetStructure->numLayers);
  if (quantStatus != -1 )
  {
    pTIDLNetStructure->isQuantStatsAvailable = quantStatus;
  }



}

void tidl_updateNetPitch(sTIDL_Network_t * tidlNet)
{
  int32_t i, j;
  for (i = 0; i < tidlNet->numLayers; i++)
  {
    for (j = 0; j < tidlNet->TIDLLayers[i].numOutBufs; j++)
    {
      tidlNet->TIDLLayers[i].outData[j].pitch[TIDL_LINE_PITCH] = tidlNet->TIDLLayers[i].outData[j].dimValues[TIDL_DIM_WIDTH] +
        tidlNet->TIDLLayers[i].outData[j].padW;
      int32_t isPadW = tidlNet->TIDLLayers[i].outData[j].padW ? 1 : 0;
      tidlNet->TIDLLayers[i].outData[j].pitch[TIDL_CHANNEL_PITCH] = tidlNet->TIDLLayers[i].outData[j].pitch[TIDL_LINE_PITCH] * (tidlNet->TIDLLayers[i].outData[j].dimValues[TIDL_DIM_HEIGHT] + tidlNet->TIDLLayers[i].outData[j].padH * 2 + isPadW);
      tidlNet->TIDLLayers[i].outData[j].pitch[TIDL_ROI_PITCH] = tidlNet->TIDLLayers[i].outData[j].dimValues[TIDL_DIM_NUMCH] * tidlNet->TIDLLayers[i].outData[j].pitch[TIDL_CHANNEL_PITCH];
    }
  }
  for (i = 0; i < tidlNet->numLayers; i++)
  {
    for (j = 0; j < tidlNet->TIDLLayers[i].numInBufs; j++)
    {
      tidlNet->TIDLLayers[i].inData[j].pitch[TIDL_LINE_PITCH] = tidlNet->TIDLLayers[i].inData[j].dimValues[TIDL_DIM_WIDTH] +
        tidlNet->TIDLLayers[i].inData[j].padW;
      int32_t isPadW = tidlNet->TIDLLayers[i].inData[j].padW ? 1 : 0;
      tidlNet->TIDLLayers[i].inData[j].pitch[TIDL_CHANNEL_PITCH] = tidlNet->TIDLLayers[i].inData[j].pitch[TIDL_LINE_PITCH] * (tidlNet->TIDLLayers[i].inData[j].dimValues[TIDL_DIM_HEIGHT] + tidlNet->TIDLLayers[i].inData[j].padH * 2 + isPadW);
      tidlNet->TIDLLayers[i].inData[j].pitch[TIDL_ROI_PITCH] = tidlNet->TIDLLayers[i].inData[j].dimValues[TIDL_DIM_NUMCH] * tidlNet->TIDLLayers[i].inData[j].pitch[TIDL_CHANNEL_PITCH];
    }
  }
}

sTIDL_tfOutReshapeMap_t sTIDL_OutReshapeTable[] =
{
  { TIDL_DataLayer                     ,  TIDL_tfOutReshapeDataLayer },
  { TIDL_ConvolutionLayer              ,  TIDL_tfOutReshapeConvLayer },
  { TIDL_PoolingLayer                  ,  TIDL_tfOutReshapePoolingLayer },
  { TIDL_ReLULayer                     ,  TIDL_tfOutReshapeRelu },
  { TIDL_PReLULayer                    ,  TIDL_tfOutReshapePRelu },
  { TIDL_EltWiseLayer                  ,  TIDL_tfOutReshapeEltwise },
  { TIDL_InnerProductLayer             ,  TIDL_tfOutReshapeIPLayer },
  { TIDL_SoftMaxLayer                  ,  TIDL_tfOutReshapeSoftmax },
  { TIDL_BatchNormLayer                ,  TIDL_tfOutReshapeBN },
  { TIDL_BiasLayer                     ,  TIDL_tfOutReshapeIdentity },
  { TIDL_ScaleLayer                    ,  TIDL_tfOutReshapeIdentity },
  { TIDL_Deconv2DLayer                 ,  TIDL_tfOutReshapeDeConvLayer },
  { TIDL_ConcatLayer                   ,  TIDL_tfOutReshapeConcatLayer },
  { TIDL_SplitLayer                    ,  TIDL_tfOutReshapeSliceLayer },
  { TIDL_SliceLayer                    ,  TIDL_tfOutReshapeSliceLayer },
  { TIDL_CropLayer                     ,  TIDL_tfOutReshapeCropLayer },
  { TIDL_FlattenLayer                  ,  TIDL_tfOutReshapeFlattenLayer },
  { TIDL_DropOutLayer                  ,  TIDL_tfOutReshapeIdentity },
  { TIDL_ArgMaxLayer                   ,  TIDL_tfOutReshapeArgmaxLayer },
  { TIDL_DetectionOutputLayer          ,  TIDL_tfOutReshapeDetOutLayer },
  { TIDL_ShuffleChannelLayer           ,  TIDL_tfOutReshapeIdentity },
  { TIDL_ResizeLayer                   ,  TIDL_tfOutReshapeResize },
  { TIDL_RoiPoolingLayer               ,  TIDL_tfOutReshapeRoiPoolingLayer },
  { TIDL_OdPostProcessingLayer         ,  TIDL_tfOutReshapeOdPostProcessingLayer },
  { TIDL_DepthToSpaceLayer             ,  TIDL_tfOutReshapeDeptoSpace },
  { TIDL_SigmoidLayer                  ,  TIDL_tfOutReshapeSigmoid },
  { TIDL_PadLayer                      ,  TIDL_tfOutReshapePadLayer },
  { TIDL_ColorConversionLayer          ,  TIDL_tfOutReshapeColorConversionLayer },
  { TIDL_OdOutputReformatLayer         ,  TIDL_tfOutReshapeOdOutputReformatLayer },
  { TIDL_DataConvertLayer              ,  TIDL_tfOutReshapeDataConvert },
  { TIDL_CustomLayer                   ,  TIDL_tfOutReshapeCustomLayer },
  { TIDL_BatchReshapeLayer             ,  TIDL_tfOutReshapeBatchReshape },
  { TIDL_UnsupportedLayer              ,  TIDL_tfOutReshapeIdentity },
  { TIDL_ConstDataLayer                ,  TIDL_tfOutReshapeIdentity },
  { TIDL_PriorBoxLayer                 ,  TIDL_tfOutReshapeIdentity },
  { TIDL_PermuteLayer                  ,  TIDL_tfOutReshapeIdentity },
  { TIDL_ReshapeLayer                  ,  TIDL_tfOutReshapeIdentity },
  { TIDL_ShapeLayer                    ,  TIDL_tfOutReshapeIdentity },
  { TIDL_SqueezeLayer                  ,  TIDL_tfOutReshapeIdentity },
  { TIDL_TransposeLayer                ,  TIDL_tfOutReshapeIdentity },
  { TIDL_ClipLayer                     ,  TIDL_tfOutReshapeClip },
  { TIDL_MinimumLayer                  ,  TIDL_tfOutReshapeIdentity },
  { TIDL_LeakyReluLayer                ,  TIDL_tfOutReshapeIdentity },
  { TIDL_IdentityLayer                 ,  TIDL_tfOutReshapeIdentity },
  { TIDL_BatchToSpaceLayer             ,  TIDL_tfOutReshapeBatchtoSpace },
  { TIDL_SpaceToBatchLayer             ,  TIDL_tfOutReshapeSpacetoBatch },
  { TIDL_PackLayer                     ,  TIDL_tfOutReshapeIdentity },
  { TIDL_DequantizeLayer               ,  TIDL_tfOutReshapeIdentity },
  { TIDL_CastLayer                     ,  TIDL_tfOutReshapeIdentity },
};

int32_t tidl_updateHighResOptimization(sTIDL_OrgNetwork_t  &pOrgTIDLNetStructure, int32_t layerIndex)
{
  int32_t i;
  for(i=0; i< layerIndex; i++)
  {
    if((pOrgTIDLNetStructure.TIDLPCLayers[i].layerType == TIDL_CustomLayer) ||
       (pOrgTIDLNetStructure.TIDLPCLayers[i].layerType == TIDL_PadLayer) ||
       (gParams.numFeatureBits > 8))
    {
      gParams.enableHighResOptimization = 0;
      break;
    }
  }

  return 0;
}

bool tidl_isYuvLayerPresent(void)
{
  bool isYuvLayerPresent = false;
  int32_t i;
  for(i=0; i<TIDL_NUM_IN_BUFS; i++)
  {
    if(TIDL_inYuvFormatYuv420_NV12 == gParams.inYuvFormat[i])
    {
      isYuvLayerPresent = true;
      break;
    }
  }
  return isYuvLayerPresent;
}

void tidl_printOpsList(sTIDL_OrgNetwork_t  &pOrgTIDLNetStructure)
{
  printf("printing Current net\n");
  for (int i = 0; i < pOrgTIDLNetStructure.numLayers; i++)
  {
    printf("%5d|%-30s|%-50s|%-50s|%5d|%5d|\n", i, TIDL_LayerString[pOrgTIDLNetStructure.TIDLPCLayers[i].layerType], pOrgTIDLNetStructure.TIDLPCLayers[i].inDataNames[0], pOrgTIDLNetStructure.TIDLPCLayers[i].outDataNames[0], pOrgTIDLNetStructure.TIDLPCLayers[i].inData[0].dataId, pOrgTIDLNetStructure.TIDLPCLayers[i].outData[0].dataId);
  }
}

void tidl_optimizeNet(sTIDL_OrgNetwork_t  &pOrgTIDLNetStructure, int32_t &layerIndex, int32_t &dataIndex)
{
  if(gParams.debugTraceLevel > 0)
  {
    printf("Running tidl_optimizeNet \n");
  }
  pOrgTIDLNetStructure.numLayers = layerIndex;
  if(gParams.modelType == TIDL_IMPORT_MODEL_FORMAT_TFLITE_RT && gParams.metaArchType == -1)
  {
    tidl_AddTfODOutputLayers(orgTIDLNetStructure, layerIndex, &dataIndex);
  }

    // Check all required input are present in model
  tidl_addInDataLayer(orgTIDLNetStructure, orgTIDLNetStructure.numLayers, &dataIndex);
  tidl_sortLayersInProcOrder(&orgTIDLNetStructure, &tempTIDLNetStructure, orgTIDLNetStructure.numLayers);

  // Update input data shape with import config file
  tidl_fillInDataLayerShape(orgTIDLNetStructure, &gParams, orgTIDLNetStructure.numLayers);

  // Split single outDataLayer with multi-data to multiple outDataLayers
  tidl_splitOutDataLayer(orgTIDLNetStructure, orgTIDLNetStructure.numLayers);
  tidl_sortLayersInProcOrder(&orgTIDLNetStructure, &tempTIDLNetStructure,
                             orgTIDLNetStructure.numLayers);

  if(true == tidl_isYuvLayerPresent())
  {
    tidl_addLayersForUVConversion(orgTIDLNetStructure, &dataIndex, &gParams);
    tidl_sortLayersInProcOrder(&orgTIDLNetStructure, &tempTIDLNetStructure, orgTIDLNetStructure.numLayers);
  }

  /* Add Normalization layer if user requested the same */
  if ((gParams.inDataNorm[0]) && (false == tidl_isYuvLayerPresent()))
  {
    tidl_addNormLayerToInData(orgTIDLNetStructure, orgTIDLNetStructure.numLayers, &dataIndex, &gParams);
    tidl_sortLayersInProcOrder(&orgTIDLNetStructure, &tempTIDLNetStructure, orgTIDLNetStructure.numLayers);
  }
  
  // Parse Shuffle Layer (ONNX ONLY)
  tidl_FindOnnxShuffleLayer(orgTIDLNetStructure, orgTIDLNetStructure.numLayers);
  tidl_removeMergedLayersFromNet(&orgTIDLNetStructure, &tempTIDLNetStructure);

  // Remove Padding Layer, not appliable in Caffe (ONNX ONLY)
  tidl_mergePadLayer(orgTIDLNetStructure, orgTIDLNetStructure.numLayers);
  tidl_removeMergedLayersFromNet(&orgTIDLNetStructure, &tempTIDLNetStructure);

  // Remove Split Layers
  tidl_mergeSplitLayer(orgTIDLNetStructure, orgTIDLNetStructure.numLayers);
  tidl_removeMergedLayersFromNet(&orgTIDLNetStructure, &tempTIDLNetStructure);

  //Replace Pack-Pack-Reshape with NN resize
  tidl_mergePackToNNResizeLayer(orgTIDLNetStructure, orgTIDLNetStructure.numLayers);
  tidl_removeMergedLayersFromNet(&orgTIDLNetStructure, &tempTIDLNetStructure);
  tidl_sortLayersInProcOrder(&orgTIDLNetStructure, &tempTIDLNetStructure, orgTIDLNetStructure.numLayers);

  if(!tidlInputTensorDimCheck(orgTIDLNetStructure))
  {
    exit(0);
  }

  if(gParams.debugTraceLevel > 0)
  {
    tidl_printOpsList(orgTIDLNetStructure);
  }


  // Call Reshape functions to determine the input/output shape of each layer.
  tidl_updateOutDataShape(orgTIDLNetStructure, 0, orgTIDLNetStructure.numLayers, (sTIDL_tfOutReshapeMap_t *)&sTIDL_OutReshapeTable);

  // Duplicate Slice Layer. 3 layers for 3 outputs.
  tidl_duplicateSliceLayer(orgTIDLNetStructure, orgTIDLNetStructure.numLayers);
  tidl_removeMergedLayersFromNet(&orgTIDLNetStructure, &tempTIDLNetStructure);

  tidl_sortLayersInProcOrder(&orgTIDLNetStructure, &tempTIDLNetStructure, orgTIDLNetStructure.numLayers);

  // Split Resize layer into multiple layers when resize ratio is > 4x.
  tidl_splitResizeLayer(orgTIDLNetStructure, orgTIDLNetStructure.numLayers, dataIndex);
  tidl_removeMergedLayersFromNet(&orgTIDLNetStructure, &tempTIDLNetStructure);

  tidl_sortLayersInProcOrder(&orgTIDLNetStructure, &tempTIDLNetStructure, orgTIDLNetStructure.numLayers);

  tidl_convertSqueezeToFlattenLayer(orgTIDLNetStructure, orgTIDLNetStructure.numLayers);
  tidl_removeMergedLayersFromNet(&orgTIDLNetStructure, &tempTIDLNetStructure);

  tidl_mergeIdentitytLayer(orgTIDLNetStructure, orgTIDLNetStructure.numLayers);
  tidl_removeMergedLayersFromNet(&orgTIDLNetStructure, &tempTIDLNetStructure);

  tidl_mergeReluLayer(orgTIDLNetStructure, orgTIDLNetStructure.numLayers);
  tidl_removeMergedLayersFromNet(&orgTIDLNetStructure, &tempTIDLNetStructure);

  tidl_mergeBiasLayer(orgTIDLNetStructure, orgTIDLNetStructure.numLayers);
  tidl_removeMergedLayersFromNet(&orgTIDLNetStructure, &tempTIDLNetStructure);

  tidl_convertBiasToBNLayer(orgTIDLNetStructure, orgTIDLNetStructure.numLayers);

  tidl_mergeBNLayer(orgTIDLNetStructure, orgTIDLNetStructure.numLayers);
  tidl_removeMergedLayersFromNet(&orgTIDLNetStructure, &tempTIDLNetStructure);

  tidl_mergeReluLayer(orgTIDLNetStructure, orgTIDLNetStructure.numLayers);
  tidl_removeMergedLayersFromNet(&orgTIDLNetStructure, &tempTIDLNetStructure);

  if(gParams.foldMaxPoolInConv2D == 1)
  {
    tidl_mergePoolingLayer(orgTIDLNetStructure, orgTIDLNetStructure.numLayers);
  }
  else if(gParams.foldDepthToSpaceInConv2D == 1)
  {
    tidl_mergeDepthToSpaceLayer(orgTIDLNetStructure, orgTIDLNetStructure.numLayers);
  }

  tidl_removeDivideByOneLayer(orgTIDLNetStructure, orgTIDLNetStructure.numLayers);
  tidl_removeMergedLayersFromNet(&orgTIDLNetStructure, &tempTIDLNetStructure);
  tidl_sortLayersInProcOrder(&orgTIDLNetStructure, &tempTIDLNetStructure, orgTIDLNetStructure.numLayers);

  tidl_mergeBatchToSpaceInDWConvLayer(orgTIDLNetStructure, orgTIDLNetStructure.numLayers);
  tidl_removeMergedLayersFromNet(&orgTIDLNetStructure, &tempTIDLNetStructure);
  tidl_sortLayersInProcOrder(&orgTIDLNetStructure, &tempTIDLNetStructure, orgTIDLNetStructure.numLayers);

  tidl_merge1x1MaxPoolingLayer(orgTIDLNetStructure, orgTIDLNetStructure.numLayers);
  tidl_removeMergedLayersFromNet(&orgTIDLNetStructure, &tempTIDLNetStructure);

  tidl_merge1x1ResizeLayer(orgTIDLNetStructure, orgTIDLNetStructure.numLayers);
  tidl_removeMergedLayersFromNet(&orgTIDLNetStructure, &tempTIDLNetStructure);

  tidl_mergeFlattenLayer(orgTIDLNetStructure, orgTIDLNetStructure.numLayers);
  tidl_removeMergedLayersFromNet(&orgTIDLNetStructure, &tempTIDLNetStructure);

  tidl_mergeMinimumLayer(orgTIDLNetStructure, orgTIDLNetStructure.numLayers);
  tidl_removeMergedLayersFromNet(&orgTIDLNetStructure, &tempTIDLNetStructure);

  tidl_mergeClipLayer(orgTIDLNetStructure, orgTIDLNetStructure.numLayers);
  tidl_removeMergedLayersFromNet(&orgTIDLNetStructure, &tempTIDLNetStructure);

  tidl_convertRelUToBNLayer(orgTIDLNetStructure, orgTIDLNetStructure.numLayers);
  tidl_FindFlattenLayer(orgTIDLNetStructure, orgTIDLNetStructure.numLayers);
  tidl_removeMergedLayersFromNet(&orgTIDLNetStructure, &tempTIDLNetStructure);

  tidl_convertConv2DToIpLayer(orgTIDLNetStructure, orgTIDLNetStructure.numLayers, (sTIDL_tfOutReshapeMap_t *)&sTIDL_OutReshapeTable);
  tidl_mergeFalttenLayer(orgTIDLNetStructure, orgTIDLNetStructure.numLayers);
  tidl_removeMergedLayersFromNet(&orgTIDLNetStructure, &tempTIDLNetStructure);

  tidl_mergeReshapeLayer(orgTIDLNetStructure, orgTIDLNetStructure.numLayers, (sTIDL_tfOutReshapeMap_t *)&sTIDL_OutReshapeTable);
  tidl_removeMergedLayersFromNet(&orgTIDLNetStructure, &tempTIDLNetStructure);

  if (gParams.numBatches[0] > 1)
  {
    if (gParams.enableHighResOptimization == 0) // check for ST is disabled
    {
      int32_t layerAdded = 0;
      int32_t layerIdx = 0;
      int32_t batchPadTotal = 1;
      do
      {
        layerAdded = tidl_addBatchReshapeLayerForBatchProcessing(orgTIDLNetStructure, &dataIndex, orgTIDLNetStructure.numLayers, &layerIdx, &batchPadTotal);
        if (layerAdded == -1)
        {
          printf("ERROR: Batch processing can not support for these set of Layers \n");
          exit(-1);
        }
        tidl_sortLayersInProcOrder(&orgTIDLNetStructure, &tempTIDLNetStructure, orgTIDLNetStructure.numLayers);
      }while(layerAdded);
      tidl_initAndUpdateBatchPadRequirements(&orgTIDLNetStructure);
      tidl_updateOutDataShape(orgTIDLNetStructure, 0, orgTIDLNetStructure.numLayers, (sTIDL_tfOutReshapeMap_t *)&sTIDL_OutReshapeTable);
    }
    else
    {
      printf("ERROR: Batch processing is not supported with super tiling case \n");
      exit(-1);
    }
  }

  if ((gParams.modelType == TIDL_IMPORT_MODEL_FORMAT_TVM_RELAY /* (Reshape, InnerProduct) sequence */)|| 
      (gParams.modelType == TIDL_IMPORT_MODEL_FORMAT_TFLITE))
    tidl_convertReshapeToFlatten(orgTIDLNetStructure, orgTIDLNetStructure.numLayers);
  /*
   * Parsing Softmax/InnerProduct Structure
   * Steps:
   * 1. Change the shape of pooling output & IP input.
   * 2. Change the shape of pooling output & Softmax input.
   *
   * Support 2 formats:
   * 1. global_pooling: true
   * 2. 7x7 avg pooling with channelx7x7 blob
   */
  tidl_convertIpLayerInputShape(orgTIDLNetStructure, orgTIDLNetStructure.numLayers);
  tidl_convertSoftMaxLayerInputShape(orgTIDLNetStructure, orgTIDLNetStructure.numLayers);

  /*
   * Parsing SSD Structure
   * Steps:
   * 1. Merge Softmax Layer into DetectionOut Layer
   * 2. Merge Reshape Layer into DetectionOut Layer
   * 3. Handle width-wise Concat shape
   * 4. Remove Permute Layer
   * 5. Remove all Priorbox Layer, and send params to DetectionOut Layer.
   *    Search all concat layer with priorbox input and detection out output.
   *    Parse each priorbox in order.
   */
  tidl_mergeDetectionoutLayer(orgTIDLNetStructure, orgTIDLNetStructure.numLayers);
  tidl_removeMergedLayersFromNet(&orgTIDLNetStructure, &tempTIDLNetStructure);

  tidl_addPadLayer(orgTIDLNetStructure, orgTIDLNetStructure.numLayers, &dataIndex, &gParams);
  tidl_sortLayersInProcOrder(&orgTIDLNetStructure, &tempTIDLNetStructure, orgTIDLNetStructure.numLayers);

  if(gParams.foldPreBnConv2D)
  {
    tidl_mergePreBNLayer(orgTIDLNetStructure, orgTIDLNetStructure.numLayers);
    tidl_removeMergedLayersFromNet(&orgTIDLNetStructure, &tempTIDLNetStructure);
  }

  if(true == tidl_isYuvLayerPresent())
  {
    tidl_mergeYUVtoRGBConversion(orgTIDLNetStructure, &dataIndex, &gParams);
    tidl_sortLayersInProcOrder(&orgTIDLNetStructure, &tempTIDLNetStructure, orgTIDLNetStructure.numLayers);
  }

  if ( (gParams.metaArchType == TIDL_metaArchTIDLSSD) ||
       (gParams.metaArchType == TIDL_metaArchTFSSD)   ||
       (gParams.metaArchType == TIDL_metaArchTFFasterRcnn) ||
       (gParams.metaArchType == TIDL_metaArchTIDLYolo) ||
       (gParams.metaArchType == TIDL_metaArchTIDLRetinaNet) ||
       (gParams.metaArchType == TIDL_metaArchTIDLYoloV5) 
       )   //TODO : can we just write (gParams.metaArchType != -1) instead of all above conditions
  {
    tidl_addMetaArchLayersTONet(orgTIDLNetStructure, orgTIDLNetStructure.numLayers, &dataIndex, &gParams);
    tidl_removeMergedLayersFromNet(&orgTIDLNetStructure, &tempTIDLNetStructure);
    tidl_sortLayersInProcOrder(&orgTIDLNetStructure, &tempTIDLNetStructure, orgTIDLNetStructure.numLayers);
  }

  /*Remove Concat and Flatten layer in detection layer*/
  if(gParams.modelType == TIDL_IMPORT_MODEL_FORMAT_TFLITE_RT && gParams.metaArchType == -1) //meta arch type specifically set to -1 if network has detection post proc layer
  {
    //we do not want to call this function if detection post proc layer is added by TIDL, call only if actual network has detection post proc layer
    tidl_removeConcatReshapeLogisticLayerInDetNet(orgTIDLNetStructure, orgTIDLNetStructure.numLayers);
  }
  else
  {
    tidl_removeConcatFlattenLayerInDetNet(orgTIDLNetStructure, orgTIDLNetStructure.numLayers);
  }
  tidl_removeMergedLayersFromNet(&orgTIDLNetStructure, &tempTIDLNetStructure);
  tidl_sortLayersInProcOrder(&orgTIDLNetStructure, &tempTIDLNetStructure, orgTIDLNetStructure.numLayers);
  
  if(gParams.modelType == TIDL_IMPORT_MODEL_FORMAT_TFLITE_RT && gParams.metaArchType != -1)
  {
    tidl_AddTfODOutputLayersEfficientDet(orgTIDLNetStructure, orgTIDLNetStructure.numLayers, &dataIndex);
  }
  if(gParams.modelType == TIDL_IMPORT_MODEL_FORMAT_ONNX_RT)
  {
    tidl_AddOnnxODOutputLayers(orgTIDLNetStructure, orgTIDLNetStructure.numLayers, &dataIndex);
  }
  tidl_sortLayersInProcOrder(&orgTIDLNetStructure, &tempTIDLNetStructure, orgTIDLNetStructure.numLayers);

  if (gParams.addDataConvertToNet)
  {
    if(false == tidl_isYuvLayerPresent())
    {
      tidl_addDataConvertLayer(orgTIDLNetStructure, orgTIDLNetStructure.numLayers, &dataIndex, &gParams);
      tidl_sortLayersInProcOrder(&orgTIDLNetStructure, &tempTIDLNetStructure, orgTIDLNetStructure.numLayers);
    }
    else
    {
      printf("Warning :: addDataConvertToNet is ignored as this feature is not supported for YUV inputs \n");    
    }
  }

  layerIndex = orgTIDLNetStructure.numLayers;

  tidl_convertReshapeToFlatten(orgTIDLNetStructure, orgTIDLNetStructure.numLayers);

  TIDL_convertDeconv2DtoConv(orgTIDLNetStructure, layerIndex);
  tidl_makeDataIdLayerIdSame(&orgTIDLNetStructure, layerIndex);

  tidl_updateHighResOptimization(orgTIDLNetStructure, orgTIDLNetStructure.numLayers);
  if(gParams.debugTraceLevel > 0)
  {
    printf("Completed tidl_optimizeNet \n");
  }
}

int32_t updatePadAndWriteModel(sTIDL_OrgNetwork_t * pTidlOrigNet,
                                                             sTIDL_Network_t  * pTidlNet,
                                                             tidl_import_config * configParams)
{
  int32_t numLayers = pTidlOrigNet->numLayers;

  int32_t tiLayerIndex = tidl_copyPCNetToDeviceNet(pTidlOrigNet,
                                                                              pTidlNet,
                                                                              configParams,
                                                                              numLayers);

  tidl_addOutDataLayer(pTidlNet , tiLayerIndex);

  TIDL_fillDataBufPadRequirements(pTidlNet);
  tidl_updateNetPitch(pTidlNet);
  /* Offsets in the Net needs to be updated before writing the net to file */
  TIDL_writeModel(pTidlNet, pTidlOrigNet, (const char *)NULL, numLayers, configParams);
  TIDL_writeModel(pTidlNet, pTidlOrigNet, (const char *)(configParams)->outputNetFile, numLayers, configParams);
  TIDL_writeInfo(pTidlNet,  pTidlOrigNet,  (const char *)(configParams)->outputParamsFile, numLayers, 1, NULL, configParams);

  return 0;
}

int32_t TIDL_doesLayerSupportBatchProcessing(sTIDL_LayerPC_t * layer)
{
  int32_t doesLayerSupportBatchProcessing = 0;

  if ( layer->layerType == TIDL_ConvolutionLayer )
  {
    doesLayerSupportBatchProcessing = 1;
  }
  else if ( layer->layerType == TIDL_PoolingLayer)
  {
    doesLayerSupportBatchProcessing = 1;
    if (layer->layerParams.poolParams.kernelW == 0)
      doesLayerSupportBatchProcessing = 0;
  }
  else if ( layer->layerType == TIDL_BatchNormLayer)
  {
    doesLayerSupportBatchProcessing = 1;
  }
  else if ( layer->layerType == TIDL_EltWiseLayer)
  {
    doesLayerSupportBatchProcessing = 1;
  }
  else if ( layer->layerType == TIDL_ConcatLayer)
  {
    doesLayerSupportBatchProcessing = 1;
  }  
  else
  {
    doesLayerSupportBatchProcessing = 0;
  }

  return doesLayerSupportBatchProcessing;
}

// This function post-processes the network after it has been imported and optimized.
uint32_t TIDL_import_backend(uint32_t layerIndex)
{
  /* At this point model is frozen */
  int32_t i;
  /* If user has not set any value for raw elment type then use the default
  which is same as input element type */
  for ( i = 0; i < TIDL_MAX_ALG_IN_BUFS; i++)
  {
    if ( ( gParams.inFileFormat == 1 ) || ( gParams.inFileFormat == 3 ) )
    {
      if ( gParams.rawDataInElementType[i] == -1 )
      {
        gParams.rawDataInElementType[i]   = gParams.inElementType[i];
        printf("Raw data format used is same as inElementType %d \n", gParams.inElementType[i]);
      }
    }
  }

  TIDL_import_quantize(layerIndex);
  int32_t dimcheck = tidlModelTensorDimCheck(orgTIDLNetStructure);

  /* Save calib run's copy.  perfsim run could update network layer ordering */
  if (gParams.modelType == 4 /* TVM Relay */)
    tidlWriteTensorNamesToFile(&gParams, "_calib");
  if(gParams.executeNetworkCompiler && dimcheck)
  {
    tidlRunPerfSimTool(&gParams);
  }
  tidlWriteTensorNamesToFile(&gParams, "");
  tidlRunGraphVizTool(&gParams);
  if (gParams.debugTraceLevel > 0)
     tidlRunModelDumpTool(&gParams);

  int errorCount = tidlModelCheck(&gParams, &orgTIDLNetStructure);
  TIDL_freeModelParams(&orgTIDLNetStructure, layerIndex);

  return errorCount;
}
