import time
import random
import os
import threading
import subprocess
import queue
import sys
import argparse
import json
#import select
#import re

parser = argparse.ArgumentParser()
parser.add_argument('--batch-mode', action='store_true')
parser.add_argument('-j', '--jobs', nargs = '?', const = os.cpu_count(), default = 1, type=int)
parser.add_argument('-v', action = 'count', default = 0)
parser.add_argument('-n','--numFrames', default = 20 )
parser.add_argument('--args', nargs = argparse.REMAINDER, default = [])
parser.add_argument('-c','--configOptions', action='append', dest='configOptions',
                    default=[''],
                    choices = ['','float','16bit','calib0','calib7','calib13'],
                    help='Add which config to import : options available : float, 16bit, calib0, calib7, calib13',
                    )
args = parser.parse_args()

#"KEY" : numParamBits, calibrationOption
configOptionMapping = {
    "float": ['32', '0',   '0'],
    "16bit": ['16', '0',   '1'],
    "calib0": ['8', '0',   '1'],
    "calib7": ['8', '7',   '50'],
    "calib13": ['8', '13', '1']
}
if sys.platform == 'linux':
    if os.path.exists(os.path.join(os.getenv('HOME'), 'time_data.json')):
        with open(os.path.join(os.getenv('HOME'), 'time_data.json')) as fp:
            time_data = json.load(fp)
    else:
        time_data = {}
else:
  time_data = {}  

if not args.batch_mode:
    from termcolor import colored
    if sys.platform == 'win32':
        import colorama
        colorama.init()
else:
    def colored(msg, color):
        return msg

def msg(level, message):
    if not args.v < level:
        print(message, file=sys.stderr)

if sys.platform == 'win32':
    import_tool = 'out/tidl_model_import.out.exe'
    comment_lead = '::'
elif sys.platform == 'linux':
    import_tool = './out/tidl_model_import.out'
    comment_lead = '#'
else:
    msg(0, 'Unrecognised system: %s' % sys.platform)
    sys.exit(1)

console_path = 'consoles'
batch_file = 'import_accuracy_benchmarking_list.txt'
if os.path.exists(console_path):
    if not os.path.isdir(console_path):
        msg(0, 'Console path %s exists and not a directory' % console_path)
else:
    os.makedirs(console_path)

configs = []
config_args = {}

def createNewConfig(userConfigOption):
    # Find the net file name, use this to derive intermediate config file generation directory
    with open(config, 'r') as importConfig:
        for line in importConfig.readlines():
            if not line.strip() == '':
                if not line.startswith(comment_lead):
                    if line.startswith('exit'):
                        break

                    split_lines = line.rstrip().split("=")
                    if "outputNetFile" in line:
                        path = os.path.splitext(split_lines[1].replace('\"',"").strip())
                        outputNetFile = '"'+path[0]+"_"+userConfigOption+path[1]+'"'

    origConfigFileName,file_extension = os.path.splitext(config)
    newConfigFileName = origConfigFileName+"_"+userConfigOption+file_extension
    newConfigFileName = os.path.join(os.path.dirname(outputNetFile),os.path.basename(newConfigFileName)).strip('"')    

    with open(config, 'r') as origConfig,open(newConfigFileName, 'w') as newConfig:
        for line in origConfig.readlines():
            newLine = line
            if userConfigOption != '':
                if "outputNetFile" in line:
                    outputNetFile,file_extension  = os.path.splitext(line)
                    newLine = outputNetFile+"_"+userConfigOption+file_extension
                elif "outputParamsFile" in line:                    
                    outputParamsFile = line.strip()
                    outputParamsFile = outputParamsFile.strip('"')
                    newLine = outputParamsFile + userConfigOption+"_" +'"'+"\n"                    
                else:
                    newLine = line
            newConfig.writelines(newLine)
        newConfig.writelines("\n")

        if userConfigOption != '':
            newLine = "numParamBits = "+configOptionMapping[userConfig][0]+"\n"
            newConfig.writelines(newLine)
            newLine = "calibrationOption = " + configOptionMapping[userConfig][1] + "\n"
            newConfig.writelines(newLine)
            newLine = "biasCalibrationIterations = " + configOptionMapping[userConfig][2] + "\n"
            newConfig.writelines(newLine)

        newLine = "numFrames = " + str(args.numFrames) + "\n"
        newConfig.writelines(newLine)
        newLine = "inData = " + inData + "\n"
        newConfig.writelines(newLine)

    return newConfigFileName
                     

with open(batch_file, 'r') as batch:
    for line in batch.readlines():
        if not line.strip() == '':
            if not line.startswith(comment_lead):
                if line.startswith('exit'):
                    break
                lsplit = line.rstrip().split()
                config  = lsplit[0]
                inData  = lsplit[1]
                print("config", config)
                print("indata", inData)
                configOptionsIter = iter(args.configOptions)
                print(len(args.configOptions))
                if len(args.configOptions) > 1:
                    next(configOptionsIter)
                for userConfig in configOptionsIter:
                    c_args = ""
                    newConfigName = createNewConfig(userConfig)
                    configs.append(newConfigName)
                    config_args[newConfigName] = c_args

print(configs)
print(config_args)
msg(1, 'Importing %d models' % len(configs))

ncpus = args.jobs
if ncpus > os.cpu_count():
    ncpus = os.cpu_count()
sem = threading.Semaphore(0)
idx = 0
nthreads = 0
run_count = 0
#
pq = queue.Queue()
#
#
##p = subprocess.Popen([import_tool, configs[0]], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
##out = []
##err = []
##while True:
##	outl = errl = None
##	r, w, x = select.select([p.stdout, p.stderr], [], [])
##	if p.stdout in r:
##		outl = p.stdout.readline() 
##		out.append(outl.decode('utf-8'))
##	if p.stderr in r:
##		errl = p.stderr.readline()
##		out.append(errl.decode('utf-8'))
##		err.append(errl.decode('utf-8'))
##	
##	if outl is None or errl is None:
##		pass
##	elif outl == b'' and errl == b'':
##		break
##
##print('[ALL]')
##for line in out:
##    print("%s" % line.rstrip())
##print('[ERR]')
##for line in err:
##    print("%s" % line.rstrip())
##print(p.returncode)
##
##sys.exit(0)
#
#
#
#
#
def run_one(config_name):
    command = [import_tool, config_name]
    command.extend(args.args)
    command.extend(config_args[config_name])
#
    start = time.perf_counter()
    r = subprocess.run(command, check=False, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    end = time.perf_counter()
    pq.put((config_name, r, int(end - start)))
    sem.release()
#
def spawn_one(configs, idx, nthreads):
    th = threading.Thread(target=run_one, args=(configs[idx],))
    th.start()
    return idx + 1, nthreads + 1
#
for t in range(min(len(configs), ncpus)):
    idx, nthreads = spawn_one(configs, idx, nthreads)
#
error = False
#
def join_one(nthreads):
    global run_count
    sem.acquire()
    elem = pq.get(False)
    key = 'import_%s' % os.path.splitext(os.path.basename(elem[0]))[0];
    if time_data is not None:
        if key in time_data.keys() and isinstance(time_data[key], list):
            time_data[key].append(elem[2])
            time_data[key] = time_data[key][-100:]
        else:
            time_data[key] = [elem[2]]
    with open(os.path.join(console_path, os.path.splitext(os.path.basename(elem[0]))[0] + '.console'), 'w') as fp:
        fp.writelines(elem[1].stdout.decode('utf-8'))
        msg(1, '[%03d %s]: ' % (run_count, elem[0]) + (colored('Done', 'green') if elem[1].returncode == 0 else colored('Failed', 'red')))
        msg(2, elem[1].stdout.decode('utf-8'))
#
    if not elem[1].returncode == 0:
        error = True
    run_count = run_count + 1
    return nthreads - 1
#
while idx < len(configs):
    nthreads = join_one(nthreads)
    idx, nthreads = spawn_one(configs, idx, nthreads)
#
for n in range(nthreads):
    nthreads = join_one(nthreads)
#
#
if sys.platform == 'linux':
    with open(os.path.join(os.getenv('HOME'), 'time_data.json'), 'w') as fp:
        json.dump(time_data, fp, indent=4)
if error:
    sys.exit(1)
#
sys.exit(0)
#