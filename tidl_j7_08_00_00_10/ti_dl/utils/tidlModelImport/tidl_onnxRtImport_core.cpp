/*
*
* Copyright (c) {2015 - 2017} Texas Instruments Incorporated
*
* All rights reserved not granted herein.
*
* Limited License.
*
* Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
* license under copyrights and patents it now or hereafter owns or controls to make,
* have made, use, import, offer to sell and sell ("Utilize") this software subject to the
* terms herein.  With respect to the foregoing patent license, such license is granted
* solely to the extent that any such patent is necessary to Utilize the software alone.
* The patent license shall not apply to any combinations which include this software,
* other than combinations with devices manufactured by or for TI ("TI Devices").
* No hardware patent is licensed hereunder.
*
* Redistributions must preserve existing copyright notices and reproduce this license
* (including the above copyright notice and the disclaimer and (if applicable) source
* code license limitations below) in the documentation and/or other materials provided
* with the distribution
*
* Redistribution and use in binary form, without modification, are permitted provided
* that the following conditions are met:
*
* *       No reverse engineering, decompilation, or disassembly of this software is
* permitted with respect to any software provided in binary form.
*
* *       any redistribution and use are licensed by TI for use only with TI Devices.
*
* *       Nothing shall obligate TI to provide you with source code for the software
* licensed and provided to you in object code.
*
* If software source code is provided to you, modification and redistribution of the
* source code are permitted provided that the following conditions are met:
*
* *       any redistribution and use of the source code, including any resulting derivative
* works, are licensed by TI for use only with TI Devices.
*
* *       any redistribution and use of any object code compiled from the source code
* and any resulting derivative works, are licensed by TI for use only with TI Devices.
*
* Neither the name of Texas Instruments Incorporated nor the names of its suppliers
*
* may be used to endorse or promote products derived from this software without
* specific prior written permission.
*
* DISCLAIMER.
*
* THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
* OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
* OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
* IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
* BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
* OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
* OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
* OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

#include <google/protobuf/io/coded_stream.h>
#include <google/protobuf/io/zero_copy_stream_impl.h>
#include <google/protobuf/message.h>
#include <google/protobuf/text_format.h>
using namespace std;
using ::google::protobuf::Message;
#include <fcntl.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <float.h>
#include <cmath>
#include <stdarg.h>

#include "ti_dl.h"
#include "tidl_import_api.h"
#include "tidl_import_config.h"
//#include "onnx/onnx-ml.proto3.pb.h"
#include "onnx/onnx-ml.proto3.pb.h"
#include "tidl_custom_import.h"
#include "tidl_meta_arch.pb.h"

using namespace std;
using namespace onnx;

#include "tidl_onnxImport.h"
#include "tidl_onnxRtImport_core.h"
#include "tidl_import_common.h"
#include "tidl_import_common_model_check.h"
#include "itidl_ti.h"
#include "tidl_onnxRtImport_EP.h"

extern "C"
{
extern std::vector<std::string> diagsInfo;
}

static int tidl_onnxRt_debuglevel = 0;
#define onnxRtDebugPrint(...)  if (tidl_onnxRt_debuglevel > 0) \
                                printf("onnxRtImportDebug: " __VA_ARGS__)

// Global data structures for importing
static struct onnxRtImportState_t {
  int32_t                 dataIndex;
  int32_t                 layerIndex;
  int32_t                 numErrors;
  int32_t                 numUnsupportedLayers;
  int32_t                 numInputDataLayers;
} onnxRt_import_state;

// Functions
void onnxrt_printf(int32_t debugLevel, char * format, ...)
{
  va_list args;
  if (debugLevel >= 1)
  {
    (void)va_start(args, format);
    (void)vprintf(format, args);
    va_end(args);
  }
}

/** This function read the names of the OD heads from meta arch file and returns them as a vector of strings
 * Also checks if number of outputs provided in meta arch matches actual number of network's outputs
 * */
std::vector<std::string> TIDL_readMetaArchInfo(std::string filePath, int32_t graphOutputSize)
{
  int32_t  i, j, k, l;
  tidl_meta_arch::TIDLMetaArch  tidlMetaArch;
  std::pair<std::vector<std::string>, std::vector<std::string>> inOutDataNames;
  std::vector<std::string> inOdDataNames;
  std::vector<int> inOdDataId;

  printf("TIDL Meta PipeLine (Proto) File  : %s  \n", filePath.c_str());
  TIDL_readProtoFromTextFile((const char *)filePath.c_str(), &tidlMetaArch);
  
  printf("%s\n",tidlMetaArch.name().c_str());
  for (j = 0; j < tidlMetaArch.caffe_ssd_size(); j++)
  {
    printf("%s\n",tidlMetaArch.caffe_ssd(j).name().c_str());

    for(k =0; k < tidlMetaArch.caffe_ssd(j).box_input_size(); k++)
    {
      inOdDataNames.push_back(tidlMetaArch.caffe_ssd(j).box_input(k));
    }
    
    for(k =0; k < tidlMetaArch.caffe_ssd(j).class_input_size(); k++)
    {
      inOdDataNames.push_back(tidlMetaArch.caffe_ssd(j).class_input(k));
    }
    //Check
    if(tidlMetaArch.caffe_ssd(j).output_size() != graphOutputSize)
    {
      printf("\nERROR : Number of output names provided in prototxt - %d - do not match actual number of outputs of OD network - %d \n\n", tidlMetaArch.caffe_ssd(j).output_size(), graphOutputSize);
      exit(-1);
    }
  }

  for (j = 0; j < tidlMetaArch.tidl_retinanet_size(); j++)
  {
    printf("%s\n",tidlMetaArch.tidl_retinanet(j).name().c_str());
    
    for(k =0; k < tidlMetaArch.tidl_retinanet(j).box_input_size(); k++)
    {
      inOdDataNames.push_back(tidlMetaArch.tidl_retinanet(j).box_input(k).c_str());
    }
    
    for(k =0; k < tidlMetaArch.tidl_retinanet(j).class_input_size(); k++)
    {
      inOdDataNames.push_back(tidlMetaArch.tidl_retinanet(j).class_input(k).c_str());
    }
    //Check
    if(tidlMetaArch.tidl_retinanet(j).output_size() != graphOutputSize)
    {
      printf("\nERROR : Number of output names provided in prototxt - %d - do not match actual number of outputs of OD network - %d \n\n", tidlMetaArch.tidl_retinanet(j).output_size(), graphOutputSize);
      exit(-1);
    }
  }
  
  for (j = 0; j < tidlMetaArch.tidl_yolo_size(); j++)
  {
    printf("%s\n",tidlMetaArch.tidl_yolo(j).name().c_str());
    
    for(k =0; k < tidlMetaArch.tidl_yolo(j).yolo_param_size(); k++)
    {
      inOdDataNames.push_back(tidlMetaArch.tidl_yolo(j).yolo_param(k).input().c_str());
    }
    //Check
    if(tidlMetaArch.tidl_yolo(j).output_size() != graphOutputSize)
    {
      printf("\nERROR : Number of output names provided in prototxt - %d - do not match actual number of outputs of OD network - %d \n\n", tidlMetaArch.tidl_yolo(j).output_size(), graphOutputSize);
      exit(-1);
    }
  }
  return inOdDataNames;
}

/* This function finds graph indices corresponding to names of OD heads */ 
std::vector<int> TIDL_getPostProcInputIds(GraphProto& onnxGraph, std::vector<std::string> odPostProcIndataNames)
{
  std::vector<int> odPostProcIndataIds = {};
  for(int i = 0; i < odPostProcIndataNames.size(); i++)
  {
    for(int j = 0; j < onnxGraph.node_size(); j++)
    {
      if((strcmp(odPostProcIndataNames[i].c_str(), onnxGraph.node(j).output(0).c_str())) == 0)
      {
        odPostProcIndataIds.push_back(j);
      }
    }
  }
  return odPostProcIndataIds;
}

static int32_t TIDL_onnxRtImportGetNewLayerIndex()
{
  return onnxRt_import_state.layerIndex++;
}
int32_t TIDL_onnxMapIdentityBaseParams(GraphProto&   onnGraph, int32_t i, sTIDL_LayerPC_t &layer)
{
  layer.layerType = TIDL_IdentityLayer;
  return 0;
}
static int32_t TIDL_onnxRtImportGetNewDataIndex()
{
  return onnxRt_import_state.dataIndex++;
}
// Convert a onnx operator to a TIDL layer
static int32_t TIDL_onnxRtMapNode(GraphProto&   onnxGraph, int32_t nodeIdx, sTIDL_LayerPC_t &layer)
{
  int32_t status = 0;
  
  /*** TODO : Add layer mapping using table as in tfliteImport ***/
  if( strcmp(onnxGraph.node(nodeIdx).op_type().c_str(), "Conv") ==0)
  {
    status = TIDL_onnxMapConvBaseParams(onnxGraph, nodeIdx, layer);
  }
  else if( strcmp(onnxGraph.node(nodeIdx).op_type().c_str(), "Relu") ==0)
  {
    status = TIDL_onnxMapReluBaseParams(onnxGraph, nodeIdx, layer);
  }
  else if( strcmp(onnxGraph.node(nodeIdx).op_type().c_str(), "PRelu") ==0)
  {
    status = TIDL_onnxMapPReluBaseParams(onnxGraph, nodeIdx, layer);
  }  
  else if( strcmp(onnxGraph.node(nodeIdx).op_type().c_str(), "LeakyRelu") ==0)
  {
    status = TIDL_onnxMapLeakyReluBaseParams(onnxGraph, nodeIdx, layer);
  }
  else if( strcmp(onnxGraph.node(nodeIdx).op_type().c_str(), "Identity") ==0)
  {
    status = TIDL_onnxMapIdentityBaseParams(onnxGraph, nodeIdx, layer);
  }    
  else if( strcmp(onnxGraph.node(nodeIdx).op_type().c_str(), "Clip") ==0)
  {
    status = TIDL_onnxMapClipBaseParams(onnxGraph, nodeIdx, layer);
  }
  else if((strcmp(onnxGraph.node(nodeIdx).op_type().c_str(), "Sigmoid") ==0) ||
           strcmp(onnxGraph.node(nodeIdx).op_type().c_str(), "Logistic") ==0)
  {
    status = TIDL_onnxMapSigmoidBaseParams(onnxGraph, nodeIdx, layer);
  }
  else if( strcmp(onnxGraph.node(nodeIdx).op_type().c_str(), "Concat") ==0)
  {
    status = TIDL_onnxMapConcatBaseParams(onnxGraph, nodeIdx, layer);
  }
  else if( (strcmp(onnxGraph.node(nodeIdx).op_type().c_str(), "Add") ==0) ||
           (strcmp(onnxGraph.node(nodeIdx).op_type().c_str(), "Sum") ==0))
  {
    status = TIDL_onnxMapAddBaseParams(onnxGraph, nodeIdx, layer);
  }
  else if( strcmp(onnxGraph.node(nodeIdx).op_type().c_str(), "MaxPool") ==0)
  {
    status = TIDL_onnxMapMaxPoolBaseParams(onnxGraph, nodeIdx, layer);
  }
  else if( strcmp(onnxGraph.node(nodeIdx).op_type().c_str(), "GlobalAveragePool") ==0)
  {
    status = TIDL_onnxMapGlobalAvgPoolBaseParams(onnxGraph, nodeIdx, layer);
  }
  else if( strcmp(onnxGraph.node(nodeIdx).op_type().c_str(), "AveragePool") ==0)
  {
    status = TIDL_onnxMapAvgPoolBaseParams(onnxGraph, nodeIdx, layer);
  }
  else if( strcmp(onnxGraph.node(nodeIdx).op_type().c_str(), "Upsample") ==0)
  {
    status = TIDL_onnxMapUpsampleBaseParams(onnxGraph, nodeIdx, layer);
  }
  else if( strcmp(onnxGraph.node(nodeIdx).op_type().c_str(), "Resize") ==0)
  {
    status = TIDL_onnxMapResizeBaseParams(onnxGraph, nodeIdx, layer);
  }
  else if( strcmp(onnxGraph.node(nodeIdx).op_type().c_str(), "ConvTranspose") ==0)
  {
    status = TIDL_onnxMapConvTransposeBaseParams(onnxGraph, nodeIdx, layer);
  }
  else if( strcmp(onnxGraph.node(nodeIdx).op_type().c_str(), "Reshape") ==0)
  {
    status = TIDL_onnxMapReshapeBaseParams(onnxGraph, nodeIdx, layer); 
  }
  else if( strcmp(onnxGraph.node(nodeIdx).op_type().c_str(), "Transpose") ==0)
  {
    status = TIDL_onnxMapTransposeBaseParams(onnxGraph, nodeIdx, layer);
  }
  else if( strcmp(onnxGraph.node(nodeIdx).op_type().c_str(), "ArgMax") ==0)
  {
    status = TIDL_onnxMapArgmaxBaseParams(onnxGraph, nodeIdx, layer);
  }
  else if( strcmp(onnxGraph.node(nodeIdx).op_type().c_str(), "BatchNormalization") ==0)
  {
    status = TIDL_onnxMapBNBaseParams(onnxGraph, nodeIdx, layer);
  }
  else if( strcmp(onnxGraph.node(nodeIdx).op_type().c_str(), "Gemm") ==0)
  {
    status = TIDL_onnxGemmBaseParams(onnxGraph, nodeIdx, layer); 
  }
  else if( strcmp(onnxGraph.node(nodeIdx).op_type().c_str(), "Softmax") ==0)
  {
    status = TIDL_onnxMapSoftmaxBaseParams(onnxGraph, nodeIdx, layer); 
  }
  else if( strcmp(onnxGraph.node(nodeIdx).op_type().c_str(), "Split") ==0)
  {
    status = TIDL_onnxMapSplitBaseParams(onnxGraph, nodeIdx, layer); 
  }
  else if( strcmp(onnxGraph.node(nodeIdx).op_type().c_str(), "Slice") ==0)
  {
    status = TIDL_onnxMapSliceBaseParams(onnxGraph, nodeIdx, layer); 
  }
  else if( strcmp(onnxGraph.node(nodeIdx).op_type().c_str(), "Flatten") ==0)
  {
    status = TIDL_onnxMapFlattenBaseParams(onnxGraph, nodeIdx, layer); 
  }
  else if( strcmp(onnxGraph.node(nodeIdx).op_type().c_str(), "Pad") ==0)
  {
    status = TIDL_onnxMapPadBaseParams(onnxGraph, nodeIdx, layer); 
  }
  else if( strcmp(onnxGraph.node(nodeIdx).op_type().c_str(), "Mul") ==0)
  {
    status = TIDL_onnxMapMulBaseParams(onnxGraph, nodeIdx, layer); 
  }
  else if( strcmp(onnxGraph.node(nodeIdx).op_type().c_str(), "DepthToSpace") ==0)
  {
    status = TIDL_onnxMapDepthToSpaceBaseParams(onnxGraph, nodeIdx, layer); 
  }
  else if( strcmp(onnxGraph.node(nodeIdx).op_type().c_str(), "DropOut") ==0)
  {
    status = TIDL_onnxMapDropOutBaseParams(onnxGraph, nodeIdx, layer);
  }
  else
  {
    //printf("Layer not supported \n");
    return -1;
  }
  return status;
}
extern int64_t onnxOpSetVersion;
// Called from onnx_rt to identify nodes that are supported by TIDL.
int32_t TIDL_onnxAllowlistNode(GraphProto&   onnxGraph, int32_t i, int32_t debugLevel, std::vector<std::string> denyList, int32_t opSetVersion, 
                               bool isObjectDetectionNetwork, std::vector<int> odProcNodesComplement)
{
  int32_t status = -1;
  sTIDL_LayerPC_t layer;
  memset(&layer, 0, sizeof(sTIDL_LayerPC_t));
  
  //printf("Layer %d  input name --- %s \n", i, onnxGraph.node(i).input(0).c_str());
  if(isObjectDetectionNetwork)
  {
    if(odProcNodesComplement.size() == 0) //means ARM mode, allowlist based on numDims != 4
    {
      std::vector<int32_t> nodeInputDims = getNodeInputShape(onnxGraph,  onnxGraph.node(i).input(0), debugLevel);

      int32_t numDims = nodeInputDims.size();

      onnxrt_printf(debugLevel, "Layer %d -- layer name -- %s \n Input dims size = %d     dims --- ", i, onnxGraph.node(i).name().c_str(), numDims);
      for(int i = 0; i < numDims; i++)
      {
        onnxrt_printf(debugLevel, "%d   ", nodeInputDims[i]);
      }
      onnxrt_printf(debugLevel, "\n");

      if(numDims != 4)
      {
        onnxrt_printf(debugLevel, "Layer %d --- op type -  %s,   Number of input dims %d  !=  4 .. not supported by TIDL \n", i, onnxGraph.node(i).op_type().c_str(), numDims);
        diagsInfo.push_back("Number of input dims != 4 not supported for OD networks");
        return 0;
      }
    }
    else if(std::find(odProcNodesComplement.begin(), odProcNodesComplement.end(), i) == odProcNodesComplement.end()) //if node is not part of backbone network, mark it supported
    {
      diagsInfo.push_back("");
      return 1;
    }
  }

  onnxOpSetVersion = opSetVersion;
  status = TIDL_onnxRtMapNode(onnxGraph, i, layer);


  //modelType is required in the model checker below
  gParams.modelType = TIDL_IMPORT_MODEL_FORMAT_ONNX_RT; 
  
  int32_t isNodeInDenyList = 0;
 
  for(int j = 0; j < denyList.size(); j++)
  {
    if(onnxGraph.node(i).op_type().c_str() == denyList[j])
    {
      isNodeInDenyList = 1;
      break;
    }
  }

  if(isNodeInDenyList)
  {
    onnxrt_printf(debugLevel, "Op type %s, TIDL layer type -- %d added to unsupported nodes as specified in deny list \n", onnxGraph.node(i).op_type().c_str(), layer.layerType);
    diagsInfo.push_back("Node in deny list...delegated to ARM");
    return 0;
  }

  if (status != 0)
  {
    onnxrt_printf(debugLevel, "Unsupported (import) TIDL layer type --- %d  op type --- %s \n", layer.layerType, onnxGraph.node(i).op_type().c_str());
    diagsInfo.push_back("Layer type not supported by TIDL");
    return 0;
  }

  // Run the model check on the layer to see if it's supported  
  if (!tidlModelCheckOffload(gParams, layer))
  {
    onnxrt_printf(debugLevel, " Unsupported (TIDL check) TIDL layer type --- %15s \n", onnxGraph.node(i).op_type().c_str());
    return 0;
  }
  onnxrt_printf(debugLevel, " Supported TIDL layer type --- %15s -- %s \n", onnxGraph.node(i).op_type().c_str(), onnxGraph.node(i).name().c_str());
  return 1;
}

/** Create adjacency list of inputs of all nodes in graph
 * Returns list of form < (input node x1, input node y1...), (input node x2, input node y2...) > for nodes < node 1, node 2....>
 * */
std::vector<std::vector<int>> TIDL_createInputAdjacencyList(GraphProto&   onnxGraph)
{
  std::vector<int> inputAdjacentNodes = {};
  std::vector<std::vector<int>> adjacencyList;
  for(int i = 0; i < onnxGraph.node_size(); i++)
  {
    inputAdjacentNodes.clear();
    for(int j = 0; j < onnxGraph.node(i).input_size(); j++)
    {
      for(int k = 0; k < onnxGraph.node_size(); k++)
      {
        for(int l = 0; l < onnxGraph.node(k).output_size(); l++)
        {
          if(strcmp(onnxGraph.node(i).input(j).c_str(), onnxGraph.node(k).output(l).c_str()) == 0)
          {
            inputAdjacentNodes.push_back(k);
          }
        }
      }
    }
    adjacencyList.push_back(inputAdjacentNodes);
  }
  return adjacencyList;
}

/** Create adjacency list of outputs of all nodes in graph
 * Returns list of form < (input node x1, input node y1...), (input node x2, input node y2...) > for nodes < node 1, node 2....>
 * */
std::vector<std::vector<int>> TIDL_createOutputAdjacencyList(GraphProto& onnxGraph)
{
  std::vector<int> outputAdjacentNodes = {};
  std::vector<std::vector<int>> adjacencyList;
  for(int i = 0; i < onnxGraph.node_size(); i++)
  {
    outputAdjacentNodes.clear();
    for(int j = 0; j < onnxGraph.node(i).output_size(); j++)
    {
      for(int k = 0; k < onnxGraph.node_size(); k++)
      {
        for(int l = 0; l < onnxGraph.node(k).input_size(); l++)
        {
          if(strcmp(onnxGraph.node(i).output(j).c_str(), onnxGraph.node(k).input(l).c_str()) == 0)
          {
            outputAdjacentNodes.push_back(k);
            break;
          }
        }
      }
    }
    adjacencyList.push_back(outputAdjacentNodes);
  }
  return adjacencyList;
}

int32_t TIDL_onnxRtInit(int32_t tensor_bits, int32_t opSetVersion, int addDataConvertToNet)
{
  /* Set global tensor data layout format */
  gloab_data_format = 0;  // 1 for NCHW, 0 for NHWC
  onnxOpSetVersion = opSetVersion;

  /* Set global import config parameters */
  setDefaultParams(&gParams);
  gParams.numParamBits = tensor_bits;
  gParams.numFeatureBits = tensor_bits;
  gParams.modelType = TIDL_IMPORT_MODEL_FORMAT_ONNX_RT;   // 6
  gParams.addDataConvertToNet = addDataConvertToNet;
  
  char *onnxRt_import_debug= getenv("TIDL_ONNXRT_IMPORT_DEBUG");
  if (onnxRt_import_debug != nullptr)
  {
    tidl_onnxRt_debuglevel = atoi(onnxRt_import_debug);
    gParams.debugTraceLevel = std::min(tidl_onnxRt_debuglevel, 3);
    gParams.writeTraceLevel = (tidl_onnxRt_debuglevel > 3) ? 3 : 0;
  }
}

int32_t TIDL_ortGetType(int64_t ortType, int32_t * type)
{
  int32_t status = 0;
  if(ortType == ONNX_TENSOR_ELEMENT_DATA_TYPE_UINT8)
  {
    *type =  TIDL_UnsignedChar;
  }
  else if(ortType == ONNX_TENSOR_ELEMENT_DATA_TYPE_INT8)
  {
    *type =  TIDL_SignedChar;
  }
  else if(ortType == ONNX_TENSOR_ELEMENT_DATA_TYPE_UINT16)
  {
    *type =  TIDL_UnsignedShort;
  }
  else if(ortType == ONNX_TENSOR_ELEMENT_DATA_TYPE_INT16)
  {
    *type =  TIDL_SignedShort;
  }
  else if(ortType == ONNX_TENSOR_ELEMENT_DATA_TYPE_FLOAT)
  {
    *type =  TIDL_SinglePrecFloat;
  }
  else if(ortType == ONNX_TENSOR_ELEMENT_DATA_TYPE_INT32)
  {
    *type =  TIDLRT_Int32;
  }
  else if(ortType == ONNX_TENSOR_ELEMENT_DATA_TYPE_INT64)
  {
    *type =  TIDLRT_Int64;
  }
  else
  {
    printf("ONNX RT data type : %d not supported by TIDL\n", ortType);
    status = -1;
  }
  return status;
}


int32_t TIDL_onnxRtImportInit(GraphProto&   onnxGraph, onnxRtParams_t *onnxRtParams, char* subgraph_name, 
                            int32_t tensor_bits, char * tidl_tools_path, int32_t debugLevel, std::string metaLayersNamesList,
                            int32_t metaArchType, int32_t opSetVersion, std::vector<std::string>outDataOd, bool isSubgraphOD, int addDataConvertToNet)
{
  int32_t i;
  int32_t status;
  onnxrt_printf(debugLevel, "In TIDL_onnxRtImportInit subgraph_name=%s\n", subgraph_name);
  // Reset all the memories to to NULL, there could be multiple subgraphs
  memset(&orgTIDLNetStructure, 0, sizeof(sTIDL_OrgNetwork_t));
  memset(&tIDLNetStructure,    0, sizeof(sTIDL_Network_t));
  onnxRt_import_state.layerIndex = 0;
  onnxRt_import_state.dataIndex  = 0;
  
  // Initialize layer independent parameters of gParams
  TIDL_onnxRtInit(tensor_bits, opSetVersion, addDataConvertToNet);
  
  sprintf((char *)gParams.metaLayersNamesList, metaLayersNamesList.c_str());
  
  
  if((outDataOd.size() == 0) || (! isSubgraphOD)) //Not an OD network / Subgaph does not contain OD post processing part
  {
    gParams.metaArchType = -1;
    numNetOutData = onnxRtParams->numNetOutData ;
    for (i = 0; i < numNetOutData; i++) 
    {
      int32_t layerIndex = TIDL_onnxRtImportGetNewLayerIndex();
      sTIDL_LayerPC_t& layer = orgTIDLNetStructure.TIDLPCLayers[layerIndex];
      layer.layerType         = TIDL_DataLayer;
      layer.numInBufs         = 1;
      layer.numOutBufs        = -1;
      layer.outData[0].dataId = 0;
      strcpy((char*)layer.name, (char*)onnxRtParams->outDataNames[i]);
      strcpy((char*)layer.inDataNames[0], (char*)onnxRtParams->outDataNames[i]);
      strcpy((char*)layer.outDataNames[0], (char*)onnxRtParams->outDataNames[i]);
      onnxrt_printf(debugLevel, "Layer %d, subgraph input %s, name=%s\n", layerIndex, subgraph_name,
                      (char*)layer.outDataNames[0]);
      layer.outConsumerCnt[0] = 0;
      strcpy((char*)outDataNames[i], (char*)onnxRtParams->outDataNames[i]);
      if(gParams.addDataConvertToNet & ADD_DC_LAYER_AT_OUTPUT)
      {
        gParams.outTensorScale[i] = 1.0;
        gParams.outZeroPoint[i] = 0.0;
        gParams.outElementType[i] = TIDL_SinglePrecFloat; //TODO : Need find the output element type from the Graph if possible
      }
    }
  }
  else
  {
    gParams.metaArchType = metaArchType;
    numNetOutData = outDataOd.size() ; //The OD heads are treated as outputs, since meta arch import assumes output data layers at heads
    for (i = 0; i < numNetOutData; i++) 
    {
      int32_t layerIndex = TIDL_onnxRtImportGetNewLayerIndex();
      sTIDL_LayerPC_t& layer = orgTIDLNetStructure.TIDLPCLayers[layerIndex];
      layer.layerType         = TIDL_DataLayer;
      layer.numInBufs         = 1;
      layer.numOutBufs        = -1;
      layer.outData[0].dataId = 0;
      strcpy((char*)layer.name, (char*)outDataOd[i].c_str());
      strcpy((char*)layer.inDataNames[0], (char*)outDataOd[i].c_str());
      strcpy((char*)layer.outDataNames[0], (char*)outDataOd[i].c_str());
      onnxrt_printf(debugLevel, "Layer %d, subgraph input %s, name=%s\n", layerIndex, subgraph_name,
                      (char*)layer.outDataNames[0]);
      layer.outConsumerCnt[0] = 0;
      //strcpy((char*)outDataNames[i], (char*)outDataOd[i].c_str());
      if(gParams.addDataConvertToNet & ADD_DC_LAYER_AT_OUTPUT)
      {
        gParams.outTensorScale[i] = 1.0;
        gParams.outZeroPoint[i] = 0.0;
        gParams.outElementType[i] = TIDL_SinglePrecFloat; //TODO : Need find the output element type from the Graph if possible
      }

    }
  }
    
  onnxRt_import_state.numInputDataLayers = onnxRtParams->numNetInData;
  for (i = 0; i < (int)onnxRt_import_state.numInputDataLayers; i++) 
  {
    gParams.numBatches[i] = 1;
    gParams.inNumChannels[i] = onnxRtParams->tensorShape[i][1];
    gParams.inHeight[i] = onnxRtParams->tensorShape[i][2];
    gParams.inWidth[i] = onnxRtParams->tensorShape[i][3];
    if(gParams.addDataConvertToNet & ADD_DC_LAYER_AT_INPUT)
    {
      gParams.inQuantFactor[i] = 1.0;
      gParams.inZeroPoint[i] = 0.0;
      status = TIDL_ortGetType(onnxRtParams->inputTensorElementType[i], &gParams.inElementType[i]);
      if(status != 0)
      {
        return status;
      }
    }
    else
    {
      if(gParams.numFeatureBits == 32)
      {
        gParams.inElementType[i] = TIDL_SinglePrecFloat;
      }
      else if (tensor_bits > 8)
      {
        gParams.inElementType[i] = TIDL_SignedShort;
      }
      else
      {
          gParams.inElementType[i] = TIDL_SignedChar;
      }
    }
   
    /* Create one DataLayer for each input, enforce ordering */
    /* 1. tidl_makeDataIdLayerIdSame() only allows one outDataId per layer */
    /* 2. tidl_sortLayersInProcOrder() topo-sort layers in layer index order */
    /* 3. writeInfo() looks for input tensors in layer index order */
    /* Thus, input DataLayers ordering at creation is preserved in IOBufDesc */
    int32_t layerIndex = TIDL_onnxRtImportGetNewLayerIndex();
    sTIDL_LayerPC_t& layer = orgTIDLNetStructure.TIDLPCLayers[layerIndex];
    layer.layerType         = TIDL_DataLayer;
    layer.numInBufs         = -1;
    layer.numOutBufs        = 1;
    layer.outData[0].dataId = TIDL_onnxRtImportGetNewDataIndex();
    layer.outData[0].elementType  = gParams.inElementType[i];
    //layer.outData[0].numDim       = 4;
    layer.outData[0].dimValues[0] = 1;
    layer.outData[0].dimValues[1] = gParams.inNumChannels[i];
    layer.outData[0].dimValues[2] = gParams.inHeight[i];
    layer.outData[0].dimValues[3] = gParams.inWidth[i];
    strcpy((char *)layer.name,  (char*)onnxRtParams->inDataNames[i]);
    strcpy((char *)layer.outDataNames[0],  (char*)onnxRtParams->inDataNames[i]);
    layer.outConsumerCnt[0] = 1;
    layer.outConsumerLinked[0] = 0;
    tidl_onnxLayerUpdateConsumerCount(&orgTIDLNetStructure, layerIndex, layerIndex, onnxGraph);
    tidl_linkOutputTensors(&orgTIDLNetStructure, layerIndex);
    onnxrt_printf(debugLevel, "Layer %d, subgraph input %s, name=%s\n", layerIndex, subgraph_name,
                    (char*)layer.outDataNames[0]);
  }

  // Initialize rest of the layers
  for (int i = onnxRt_import_state.numInputDataLayers; i < TIDL_NUM_MAX_PC_LAYERS; i++)
  {
    sTIDL_LayerPC_t& layer_i = orgTIDLNetStructure.TIDLPCLayers[i];
    layer_i.actParams.actType  = TIDL_NoAct;
    layer_i.strideOffsetMethod = TIDL_StrideOffsetCenter;
  }

  gParams.inFileFormat = 1;  // raw data
  
  if(tidl_tools_path)
  {
    strcpy((char*)gParams.tidlStatsTool, tidl_tools_path);
    strcat((char*)gParams.tidlStatsTool,"/PC_dsp_test_dl_algo.out");
    strcpy((char*)gParams.perfSimTool, tidl_tools_path);
    strcat((char*)gParams.perfSimTool,"/ti_cnnperfsim.out");
    strcpy((char*)gParams.graphVizTool, tidl_tools_path);
    strcat((char*)gParams.graphVizTool,"/tidl_graphVisualiser.out");
    strcpy((char*)gParams.perfSimConfig, tidl_tools_path);
    strcat((char*)gParams.perfSimConfig,"/device_config.cfg");
  }
  else
  {
    printf("Please provide TIDL tools path \n");
    exit(-1);
  }

  if (tidlValidateImportParams(&gParams) == -1)
  {
    printf("Validation of TIDL tflite runtime import config parameters failed!\n");
    return -1;
  }

  if(gParams.metaArchType != -1)
  {
    tidl_metaArch_import(&gParams);
  }

  return 0;
}

int32_t TIDL_onnxRtImportAndLinkNode(GraphProto&   onnxGraph, int32_t nodeIdx, int32_t debugLevel)
{
  int32_t status = 0;

  // Get new layerIndex, dataIndex
  int32_t layerIndex = TIDL_onnxRtImportGetNewLayerIndex();
  sTIDL_LayerPC_t& layer = orgTIDLNetStructure.TIDLPCLayers[layerIndex];
  // set layer defaults
  layer.numInBufs  = 1;
  layer.numOutBufs = onnxGraph.node(nodeIdx).output_size(); 
  for (int j = 0; j < layer.numOutBufs; j++)
  {
    int32_t dataIndex  = TIDL_onnxRtImportGetNewDataIndex();
    layer.outData[j].dataId = dataIndex;
  }
  
  status = TIDL_onnxRtMapNode(onnxGraph, nodeIdx, layer);
  onnxrt_printf(debugLevel, "In TIDL_onnxRtImportNode  TIDL Layer type %d  \n", layer.layerType);
  tidl_onnxLayerFillTensorNames(&orgTIDLNetStructure, nodeIdx, layerIndex, onnxGraph);
  tidl_onnxLayerUpdateConsumerCount(&orgTIDLNetStructure, nodeIdx, layerIndex, onnxGraph);
  tidl_linkInputTensors(&orgTIDLNetStructure, layerIndex);
  tidl_linkOutputTensors(&orgTIDLNetStructure, layerIndex);

  return status;
}

int32_t TIDL_onnxRtOptimizeNet(int32_t debugLevel)
{
  onnxrt_printf(debugLevel, "In TIDL_onnxRtOptimizeNet: LayerIndex = %d, dataIndex = %d \n", onnxRt_import_state.layerIndex, onnxRt_import_state.dataIndex);

  tidl_optimizeNet(orgTIDLNetStructure, onnxRt_import_state.layerIndex,
                   onnxRt_import_state.dataIndex);

  return 0;
}

void TIDL_saveTidlOnnxRtSubGraph(void ** subGraphPtr)
{
  *subGraphPtr = (void *)malloc(sizeof(sTIDL_OrgNetwork_t));
  if ( subGraphPtr == NULL )
  {
    printf("Unable to allocate memory to save the subgraph \n");
    return;
  }
  memcpy(*subGraphPtr, &orgTIDLNetStructure, sizeof(sTIDL_OrgNetwork_t));
}

int32_t tidl_setParamsForPostProcessNet(int32_t tensor_bits)
{
  gParams.numParamBits = tensor_bits;
  gParams.numFeatureBits = tensor_bits;
  
  for (int i = 0; i < TIDL_MAX_ALG_IN_BUFS; i++)
  {
    gParams.inElementType[i] = TIDL_SignedChar;  /** TODO : Set to signed or unsigned? */
    gParams.rawDataInElementType[i]   = TIDL_SinglePrecFloat;//gParams.inElementType[i];
  }
  if (gParams.numFeatureBits > 8)
  {
    for (int i = 0; i < TIDL_MAX_ALG_IN_BUFS; i++)
    {
      if(gParams.numFeatureBits == 32)
      {
        gParams.inElementType[i] = TIDL_SinglePrecFloat;
      }
      else if(gParams.inElementType[i] == TIDL_UnsignedChar)
      {
        gParams.inElementType[i] = TIDL_UnsignedShort;
      }
      else if(gParams.inElementType[i] == TIDL_SignedChar)
      {
        gParams.inElementType[i] = TIDL_SignedShort;
      }
    }
  }
  gParams.executeNetworkCompiler = 1;
}

int32_t TIDL_onnxRtPostProcessNet(int32_t numFrames, int32_t numParamBits, int32_t calibrationOption, int32_t biasCalibrationIterations, 
                                  char* artifacts_folder, void * subGraphPtr, float * inQuantFactor, char* subGraphName, int32_t debugLevel,
                                  std::string outputFeature16bitNamesList , std::string params16bitNamesList, int32_t enableHighResOptimization,
                                  int32_t quantizationStyle, int32_t foldPreBnConv2D, int32_t compileConstraintsFlag, std::vector<int> ctrl1)
{
  onnxrt_printf(debugLevel, "In TIDL_onnxRtPostProcessNet: \n");
  int32_t status = 0;
  memcpy(&orgTIDLNetStructure, subGraphPtr, sizeof(sTIDL_OrgNetwork_t));
  TIDL_allocAndCopyModelParams(&orgTIDLNetStructure, (sTIDL_OrgNetwork_t *)subGraphPtr, orgTIDLNetStructure.numLayers);
  
  memset(&tIDLNetStructure, 0, sizeof(sTIDL_Network_t));
  
  tidl_setParamsForPostProcessNet(numParamBits);
  gParams.numFrames = numFrames;
  gParams.calibrationOption = calibrationOption;
  gParams.biasCalibrationIterations = biasCalibrationIterations;
  gParams.enableHighResOptimization = enableHighResOptimization;
  gParams.quantizationStyle = quantizationStyle;
  gParams.compileConstraintsFlag = compileConstraintsFlag;
  gParams.foldPreBnConv2D = foldPreBnConv2D;

  strcpy((char *)&gParams.outputFeature16bitNamesList[0], const_cast<char *>(outputFeature16bitNamesList.c_str()));
  strcpy((char *)&gParams.params16bitNamesList[0], const_cast<char *>(params16bitNamesList.c_str()));

  for(int i = 0; i < ctrl1.size(); i++)
  {
    gParams.ddrLayers[i] = ctrl1[i];
  }
  
  if((gParams.addDataConvertToNet & ADD_DC_LAYER_AT_INPUT) == 0)
  {
    for(int i = 0; i < TIDL_MAX_ALG_IN_BUFS; i++)
    {
      gParams.inQuantFactor[i] = inQuantFactor[i];
      if(numParamBits == 32)
      {
        gParams.inQuantFactor[i] = 1.0;
      }
    }
  }

  int j = 0;
  for(int i = 0; i < orgTIDLNetStructure.numLayers; i++)
  {
    if((orgTIDLNetStructure.TIDLPCLayers[i].layerType == TIDL_DataLayer) && (orgTIDLNetStructure.TIDLPCLayers[i].numInBufs == -1))
    {
      orgTIDLNetStructure.TIDLPCLayers[i].outData[0].tensorScale = gParams.inQuantFactor[j++];
    }
  }
  //tidl_updateWeightElemSize(&orgTIDLNetStructure, &gParams, orgTIDLNetStructure.numLayers);
  for(int i = 0; i < orgTIDLNetStructure.numLayers; i++)
  {
    if(orgTIDLNetStructure.TIDLPCLayers[i].layerType != TIDL_DataLayer)
      orgTIDLNetStructure.TIDLPCLayers[i].weightsElementSizeInBits = gParams.numParamBits;
  }
  
  if(numParamBits == 32)
  {
    tidl_convertElementTypeToFloat(&orgTIDLNetStructure, orgTIDLNetStructure.numLayers);
  }
  snprintf((char *)inConfigFilename, FILE_NAME_SIZE, "%s/%s_tidl_io_", artifacts_folder, subGraphName);
  snprintf((char *)gParams.outputNetFile, FILE_NAME_SIZE, "%s/%s_tidl_net.bin", artifacts_folder, subGraphName);
  snprintf((char *)gParams.outputParamsFile, FILE_NAME_SIZE, "%s/%s_tidl_io_", artifacts_folder, subGraphName);
  sprintf((char *)gParams.inData, "%s/%s_calib_raw_data.bin", artifacts_folder, subGraphName);
  
  status = TIDL_import_backend(orgTIDLNetStructure.numLayers);

  return status;
}
