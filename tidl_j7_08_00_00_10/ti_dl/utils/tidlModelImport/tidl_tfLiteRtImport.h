/*
*
* Copyright (c) {2015 - 2017} Texas Instruments Incorporated
*
* All rights reserved not granted herein.
*
* Limited License.
*
* Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
* license under copyrights and patents it now or hereafter owns or controls to make,
* have made, use, import, offer to sell and sell ("Utilize") this software subject to the
* terms herein.  With respect to the foregoing patent license, such license is granted
* solely to the extent that any such patent is necessary to Utilize the software alone.
* The patent license shall not apply to any combinations which include this software,
* other than combinations with devices manufactured by or for TI ("TI Devices").
* No hardware patent is licensed hereunder.
*
* Redistributions must preserve existing copyright notices and reproduce this license
* (including the above copyright notice and the disclaimer and (if applicable) source
* code license limitations below) in the documentation and/or other materials provided
* with the distribution
*
* Redistribution and use in binary form, without modification, are permitted provided
* that the following conditions are met:
*
* *       No reverse engineering, decompilation, or disassembly of this software is
* permitted with respect to any software provided in binary form.
*
* *       any redistribution and use are licensed by TI for use only with TI Devices.
*
* *       Nothing shall obligate TI to provide you with source code for the software
* licensed and provided to you in object code.
*
* If software source code is provided to you, modification and redistribution of the
* source code are permitted provided that the following conditions are met:
*
* *       any redistribution and use of the source code, including any resulting derivative
* works, are licensed by TI for use only with TI Devices.
*
* *       any redistribution and use of any object code compiled from the source code
* and any resulting derivative works, are licensed by TI for use only with TI Devices.
*
* Neither the name of Texas Instruments Incorporated nor the names of its suppliers
*
* may be used to endorse or promote products derived from this software without
* specific prior written permission.
*
* DISCLAIMER.
*
* THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
* OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
* OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
* IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
* BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
* OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
* OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
* OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

#ifndef TIDL_TFLTE_RT_IMPORT_H
#define TIDL_TFLTE_RT_IMPORT_H 1

#ifdef __cplusplus
extern "C"
{
#endif
typedef struct
{
    float confidence_threshold;
    float nms_threshold;
    int top_k;
    int keep_top_k;
} od_parameters;

int32_t TIDL_tfliteAllowlistNode(const TfLiteRegistration* registration, const TfLiteNode* node, int32_t node_index, TfLiteContext* context, 
                                   bool isObjectDetectionNetwork, int32_t debugLevel, int32_t * denyList, int32_t denyListSize,
                                   std::vector<int> odProcNodesComplement, bool hasDetectionPostprocLayer, od_parameters* odUserParams);

int32_t TIDL_tfliteRtInit(int32_t tensor_bits, int32_t foldPreBnConv2D, int32_t addDataConvertToNet);

int32_t TIDL_tfliteRtImportInit(TfLiteContext* context, const TfLiteDelegateParams* params, 
                                   int32_t subgraph_id, int32_t tensor_bits, int32_t foldPreBnConv2D, char * tidl_tools_path, int32_t debugLevel,
                                   std::string metaLayersNamesList, int32_t metaArchType, std::vector<std::string>outDataOd, bool isSubgraphOD, 
                                   int32_t addDataConvertToNet, od_parameters* odUserParams);

int32_t TIDL_tfliteRtImportAndLinkNode(TfLiteRegistration* registration, TfLiteContext* context, const TfLiteDelegateParams* params, 
                                        TfLiteNode* node, int32_t debugLevel, od_parameters* odUserParams);

int32_t TIDL_tfliteRtOptimizeNet(int32_t debugLevel);

int32_t TIDL_tfliteRtPostProcessNet(int32_t numFrames, int32_t numParamBits, int32_t calibrationOption, int32_t biasCalibrationIterations, 
                                    int32_t quantizationStyle, int32_t enableHighResOptimization, int32_t compileConstraintsFlag, 
                                    int32_t foldPreBnConv2D, void * subGraphPtr, float * inQuantFactor, char* artifacts_folder, 
                                    int32_t outTensorIdx, int32_t debugLevel, std::string outputFeature16bitNamesList, std::string params16bitNamesList);

void TIDL_saveTidlSubGraph(TfLiteContext* context, const TfLiteDelegateParams* params, void ** subGraphPtr);

std::vector<std::string> TIDL_readMetaArchInfo(std::string filePath);
std::vector<int> TIDL_getPostProcInputIds(TfLiteContext* context, TfLiteIntArray* plan, std::vector<std::string> odPostProcIndataNames);
std::vector<std::vector<int>> TIDL_createInputAdjacencyList(TfLiteContext* context, TfLiteIntArray* plan);
std::vector<std::vector<int>> TIDL_createOutputAdjacencyList(TfLiteContext* context, TfLiteIntArray* plan);

void tflrt_printf(int32_t debugLevel, char * format, ...);
int32_t TIDL_tfliteRtGetTypeAndPtr(TfLiteTensor *tensor, int32_t *type, void **ptr);
void TIDL_tfliteRtGetScaleAndZeroPoint(TfLiteTensor *tensor, float *scale, int32_t *zp);


#ifdef __cplusplus
}
#endif


#endif  /* TIDL_TFLTE_RT_IMPORT_H */
