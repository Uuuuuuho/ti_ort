#include <set>
#include <memory>
#include <vector>
#include <map>
#include <sstream>
#include <cstring>
#include <algorithm>
#include <dlfcn.h>
#include <cmath>
#include <float.h>
#include <errno.h>
#include <sys/stat.h>
#include <dirent.h>
#include <unistd.h>
#include <fcntl.h>
#include <fstream>

#include "tensorflow/lite/c/common.h"
#include "tensorflow/lite/builtin_ops.h"
#include "tensorflow/lite/context_util.h"
#include "../tfliteImport/schema_generated.h"

#include "itidl_ti.h"
#include "itidl_rt.h"
#include "tidl_tfLiteRtImport.h"
#include "tidl_import_config.h"
#include "tidl_runtimes_import_common.h"

namespace tflite {
namespace tfl_delegate {

#define MAX_NUM_TIDL_SUBGRAPHS (16)
#define TENSOR_FLOAT (0)
#define TENSOR_UINT8 (1)
#define TENSOR_INT8 (2)

class tidlDelegate;

class TIDL_TfLiteDelegateData {
public:
    TIDL_TfLiteDelegateData() :
    m_debug_level(0),
    m_num_param_bits(8),
    m_num_tidl_subgraphs(16),
    m_calibration_frames(20),
    m_calibration_iterations(50),
    m_tidl_calibration_flags(7),
    m_quantization_scale_type(2),
    m_high_resolution_optimization(0),
    m_compileConstraintsFlag(DEFAULT_COMPILE_CONSTRAINT_NC_FLAGS),
    m_pre_batchnorm_fold(1),
    m_activation_clipping(1),
    m_weight_clipping(1),
    m_bias_calibration(1),
    m_channel_wise_quantization(0),
    m_bias_clipping(0),
    m_add_data_convert_ops(0),
    m_meta_arch_type(-1)
    {}

    int m_debug_level;
    int m_num_param_bits;
    int m_num_tidl_subgraphs;
    int m_calibration_frames;
    int m_calibration_iterations;
    int m_tidl_calibration_flags;
    std::set<tidlDelegate *> m_valid_subgraphs;
    std::set<tidlDelegate *> m_invalid_subgraphs;
    std::set<tidlDelegate *> m_pending_subgraphs;
    int m_quantization_scale_type;
    int m_high_resolution_optimization;
    int m_compileConstraintsFlag;
    int m_pre_batchnorm_fold;
    int m_add_data_convert_ops;
    int m_meta_arch_type;

    int m_activation_clipping;
    int m_weight_clipping;
    int m_bias_calibration;
    int m_channel_wise_quantization;
    int m_bias_clipping;

    std::string m_artifacts_folder;
    std::string m_temp_folder;
    std::string m_tidl_tools_path;
    std::vector<int> m_deny_list;
    std::vector<int> m_supported_nodes;
    std::string m_output_feature_16bit_names_list; 
    std::string m_params_16bit_names_list;
    std::string m_meta_layers_names_list;

    std::vector<std::string> odPostProcHeadNames;
    std::vector<std::string> odBackboneNodeNames;
    std::vector<std::vector<int>> supported_node_groups;
    std::vector<std::vector<int>> inputAdjacencyList;
    std::vector<std::vector<int>> outputAdjacencyList;
    
    od_parameters odUserParams;

    // TODO: sanitize
    struct {
        void *lib;
        decltype(&::TIDLRT_create) TIDLRT_create;
        decltype(&::TIDLRT_delete) TIDLRT_delete;
        decltype(&::TIDLRT_invoke) TIDLRT_invoke;
        decltype(&::TIDLRT_deactivate) TIDLRT_deactivate;
        decltype(&::TIDLRT_setParamsDefault) TIDLRT_setParamsDefault;
        decltype(&::TIDLRT_setTensorDefault) TIDLRT_setTensorDefault;
        decltype(&::TIDLRT_getDdrStats) TIDLRT_getDdrStats;
    } infer_ops;
};

class tidlDelegate {
public:
    // Constructor
    tidlDelegate() :
    currNumInTensors_(0),
    currFrameIdx_(0),
    subGraphPtr_(NULL),
    inTensorsQuantFactor_(new float32_tidl[TIDL_MAX_ALG_IN_BUFS]),
    inTensorsMin_(new float32_tidl[TIDL_MAX_ALG_IN_BUFS]),
    inTensorsMax_(new float32_tidl[TIDL_MAX_ALG_IN_BUFS])
    {}

    // Any initialization code needed
    TfLiteStatus Init(TfLiteContext* context, const TfLiteDelegateParams* params);

    // Any preparation work needed (e.g. allocate buffers)
    TfLiteStatus Prepare(TfLiteContext* context, TfLiteNode* node);

    // Actual running of the delegate subgraph.
    TfLiteStatus Invoke(TfLiteContext* context, TfLiteNode* node);

    //Destructor
    ~tidlDelegate();

    std::vector<int> nodes_;
    int32_t currNumInTensors_;
    float32_tidl *inTensorsQuantFactor_;
    float32_tidl * inTensorsMin_;
    float32_tidl * inTensorsMax_;
    int32_t currFrameIdx_;
    void *subGraphPtr_;
    int32_t subgraphId_;
    TIDL_TfLiteDelegateData *data_;

    void *rtHandle;
    void *rtInList;
    void *rtOutList;
    sTIDLRT_PerfStats_t *stats;
    void *ioBufDesc;
    void * netPtr;

};

static int32_t getOneOutTensorIdx(TfLiteContext* context, const TfLiteDelegateParams* params)
{
    int32_t outTensorIdx = 0;
    // Return the index of the first output encountered - this is used to identify subgraph, so one is enough
    for (auto tensor_index : TfLiteIntArrayView(params->output_tensors)) 
    {
        if (tensor_index == kTfLiteOptionalTensor) 
        {
            continue;
        }
        TfLiteTensor* tensor = &context->tensors[tensor_index];
        // Const tensors should be added as const nodes during graph construction.
        if(tensor->allocation_type == kTfLiteArenaRw)
        {
            outTensorIdx = tensor_index;
        }
    }
    return outTensorIdx;
}

static int32_t TIDLRT_ReadBinFromFile(const char *fileName, void *addr, int32_t size)
{
    FILE *fptr = NULL;
    fptr = fopen((const char *)fileName, "rb");
    if (fptr)
    {
      fread(addr, size, 1, fptr);
      fclose(fptr);
      return 0;
    }
    else
    {
      printf("Could not open %s file for reading \n", fileName);
    }
    return -1;
}

template <class Tin>
static TfLiteStatus tidl_tflite_find_range( Tin* src, int32_t c, int32_t h, int32_t w, float src_scale, int32_t zero, float &min, float &max)
{
    float curr;
    min = FLT_MAX;
    max = -FLT_MAX;
    int32_t i0, i1, i2;
    for (i0 = 0; i0 < c; i0++) 
    {
        for (i1 = 0; i1 < h; i1++) 
        {
            for (i2 = 0; i2 < w; i2++) 
            {
                curr = ((src[i0 + i1*w*c + i2*c] - zero)*src_scale);
                min = curr < min ? curr : min;
                max = curr > max ? curr : max;
            }
        } 
    }
    return kTfLiteOk;
}

template <class Tin, class Tout>
static TfLiteStatus tidl_tflite_data_format_hwc2chw(Tout* dst, Tin* src, int32_t c, int32_t h, int32_t w, float src_scale, float dst_scale, int32_t zero)
{
    int32_t i0, i1, i2;
    float out;
    for (i0 = 0; i0 < c; i0++)
    {
        for (i1 = 0; i1 < h; i1++) 
        {
            for (i2 = 0; i2 < w; i2++) 
            {
                out = (((src[i0 + i1*w*c + i2*c] - zero)*src_scale)*(1/dst_scale));
                dst[i0 * h * w + i1 * h + i2] = (Tout)out;
            }
        } 
    }
    return kTfLiteOk;
}

static float TIDL_findMaxQuantizationScale(float min, float max, int32_t elementSizeInBits)
{
    float absRange = (fabs(max) > fabs(min)) ? fabs(max) : fabs(min);
    absRange = (float)ceil(log((double)absRange) / log((double)2));
    absRange = pow(2.0, (double)absRange);
    float quantPrec;
    if (absRange != 0)
    {
        quantPrec = ((1.0*(1 << (elementSizeInBits - 1))) / absRange);
    }
    else
    {
        quantPrec = 1;
    }

    return quantPrec;
}

static TfLiteStatus tidl_writeQuantizedInput(TfLiteContext* context, TfLiteNode* node, char * inputName, int32_t numParamBits,
                                             int32_t *numpInputs, float ** inTensorsMin, float ** inTensorsMax, float ** quantScale, int32_t * tensorType)
{
    FILE* fp = fopen(inputName, "ab");

    int32_t w[16];
    int32_t h[16];
    int32_t c[16];
    int32_t currInIdx = 0;
    float * inputMin = *inTensorsMin;
    float * inputMax = *inTensorsMax;
    float outScale = 0.0;

    if (fp == NULL) 
    {
        printf("Could not open file to save the input tensors \n");
        return kTfLiteError;
    }

    for (auto tensor_index : TfLiteIntArrayView(node->inputs)) 
    {
        if (tensor_index == kTfLiteOptionalTensor) 
        {
            continue;
        }
        TfLiteTensor* tensor = &context->tensors[tensor_index];
        if(tensor->allocation_type == kTfLiteArenaRw)
        {
            //printf(op" input Tensor name and id -  %s , %d\n", tensor->name, tensor->is_variable);

            w[currInIdx] = tensor->dims->data[2];
            h[currInIdx] = tensor->dims->data[1];
            c[currInIdx] = tensor->dims->data[3];

            int32_t tensorSize = w[currInIdx] * h[currInIdx] * c[currInIdx];

            float * pInputData = (float *)malloc(tensorSize*(32/8));
            if(pInputData == NULL)
            {
                return kTfLiteError;
            }
            if(tensor->type == kTfLiteUInt8)
            {
                *tensorType = TENSOR_UINT8;
                TfLiteAffineQuantization* quantization = (TfLiteAffineQuantization*)tensor->quantization.params;

                float scale = 1.0; 
                tidl_tflite_data_format_hwc2chw(
                        pInputData, (uint8_t*)(tensor->data.uint8),
                        tensor->dims->data[3], tensor->dims->data[1],
                        tensor->dims->data[2], quantization->scale->data[0], scale,
                        quantization->zero_point->data[0]);
                fwrite(pInputData, 1, tensorSize*(32/8), fp);
                scale = (float)ceil(log((double)quantization->scale->data[0]) / log((double)2));
                scale = pow(2.0, (double)scale);
                (*quantScale)[currInIdx] = 1 / scale;
            } 
            else if(tensor->type == kTfLiteInt8)
            {
                *tensorType = TENSOR_INT8;
                TfLiteAffineQuantization* quantization = (TfLiteAffineQuantization*)tensor->quantization.params;

                float scale = 1.0; 
                tidl_tflite_data_format_hwc2chw(
                        pInputData, (int8_t*)(tensor->data.int8),
                        tensor->dims->data[3], tensor->dims->data[1],
                        tensor->dims->data[2], quantization->scale->data[0], scale,
                        quantization->zero_point->data[0]);
                fwrite(pInputData, 1, tensorSize*(32/8), fp);
                scale = (float)ceil(log((double)quantization->scale->data[0]) / log((double)2));
                scale = pow(2.0, (double)scale);
                (*quantScale)[currInIdx] = 1 / scale;
            } 
            else if(tensor->type == kTfLiteFloat32)
            {
                *tensorType = TENSOR_FLOAT;

                float min, max;
                tidl_tflite_find_range((float *)(tensor->data.f), tensor->dims->data[3],
                        tensor->dims->data[1], tensor->dims->data[2], 1.0, 0, min, max);

                float scale = 1.0;
                tidl_tflite_data_format_hwc2chw(
                        (float *)pInputData, (float *)(tensor->data.f),
                        tensor->dims->data[3], tensor->dims->data[1],
                        tensor->dims->data[2], 1.0, 1 / scale, 0);
                fwrite(pInputData, 1, tensorSize * (32 / 8), fp);

                inputMin[currInIdx] = (inputMin[currInIdx] < min) ? inputMin[currInIdx] : min;
                inputMax[currInIdx] = (inputMax[currInIdx] > max) ? inputMax[currInIdx] : max;
            }
            else 
            {
                printf("Unsupported Tensor->type %d \n", tensor->type);
            }
            currInIdx++;
            free(pInputData);
        }
    }
    fclose(fp);
    *numpInputs = currInIdx; 
    return kTfLiteOk;

}

static bool tidl_subgraph_import(TfLiteContext* context, TfLiteNode* node, tidlDelegate *subGraphDelegate,
        int32_t * currNumInTensors, int32_t currFrameIdx, float ** inQuantFactorCalibTensors, float ** inTensorMin, float ** inTensorMax)
{
    bool res = false;
    float * inQuantFactorAllTensors = *inQuantFactorCalibTensors;
    TIDL_TfLiteDelegateData *data = subGraphDelegate->data_;
    int32_t tensorType = TENSOR_FLOAT;

    if(currFrameIdx <= data->m_calibration_frames) //need to copy input of subgraphs only before calibration is done
    {
        std::string inputName;
        inputName = data->m_temp_folder + "/calib_raw_data_" + std::to_string(subGraphDelegate->subgraphId_) + ".bin";

        int32_t numParamBits = data->m_num_param_bits;
        int32_t numInputTensors = 0;
    
        tidl_writeQuantizedInput(context, node, const_cast<char *>(inputName.c_str()), numParamBits,
                                 &numInputTensors, inTensorMin, inTensorMax, &inQuantFactorAllTensors, &tensorType);

        /* OK, here we write the model in quant, but
         * the quant models will not be used in the
         * next invokes. create() has already been
         * called with float models
         */
        if(currFrameIdx == data->m_calibration_frames) //Have all inputs available now, run calibration
        {
            printf("\n ************ Frame index %d : Running fixed point mode for calibration **************** \n", currFrameIdx);
            for (int i = 0; i < numInputTensors; i++)
            {
                if(tensorType == TENSOR_FLOAT)
                {
                    inQuantFactorAllTensors[i] =TIDL_findMaxQuantizationScale((*inTensorMin)[i], (*inTensorMax)[i], (numParamBits - 1)); // TODO : Is (-1) needed here? Seeing degradation in accuracy on removing it
                }
            }
            TIDL_tfliteRtPostProcessNet(data->m_calibration_frames, data->m_num_param_bits,
                    data->m_tidl_calibration_flags, data->m_calibration_iterations, data->m_quantization_scale_type,
                    data->m_high_resolution_optimization, data->m_compileConstraintsFlag, 
                    data->m_pre_batchnorm_fold, subGraphDelegate->subGraphPtr_, inQuantFactorAllTensors, 
                    const_cast<char *>(data->m_temp_folder.c_str()), subGraphDelegate->subgraphId_, data->m_debug_level,
                    data->m_output_feature_16bit_names_list, data->m_params_16bit_names_list);
            res = true;
        }
        else
        {
            printf("\n ************ Frame index %d : Running float inference **************** \n", currFrameIdx);
        }
    }

    return res;
}

static TfLiteStatus tidl_subgraph_rt_create(TfLiteContext *context, tidlDelegate *tidldelegate)
{
    int status = 0;
    int j = 0;
    uint32_t previous_tensor_size = 0;
    sTIDLRT_Params_t prms;
    FILE *fp_network;
    FILE *fp_config;
    uint32_t size = 0;
    uint32_t numInputBuf = 0;
    uint32_t total_output_buf_size = 0;
    uint32_t offset = 0;
    std::string network_file;
    std::string config_file;
    void *handle = NULL;
    TIDL_TfLiteDelegateData *data = tidldelegate->data_;

    tflrt_printf(data->m_debug_level, "%s %s %d \n", __FILE__, __func__, __LINE__);
    status = data->infer_ops.TIDLRT_setParamsDefault(&prms);

    network_file = data->m_temp_folder + "/" + std::to_string(tidldelegate->subgraphId_) + "_tidl_net.bin";
    config_file  = data->m_temp_folder + "/" + std::to_string(tidldelegate->subgraphId_) + "_tidl_io_1.bin";

    fp_network = fopen(network_file.c_str(), "rb");
    if (fp_network == NULL)
    {
        printf("Invoke  : ERROR: Unable to open network file %s \n", network_file.c_str());
        return kTfLiteError;
    }
    prms.stats = (sTIDLRT_PerfStats_t*)malloc(sizeof(sTIDLRT_PerfStats_t));

    fseek(fp_network, 0, SEEK_END);
    prms.net_capacity = ftell(fp_network);
    fseek(fp_network, 0, SEEK_SET);
    fclose(fp_network);
    prms.netPtr = malloc(prms.net_capacity);

    prms.TIDLReadBinFromFile = TIDLRT_ReadBinFromFile;
    status = prms.TIDLReadBinFromFile(network_file.c_str(), prms.netPtr, prms.net_capacity);

    fp_config = fopen(config_file.c_str(), "rb");
    if (fp_config == NULL)
    {
        printf("Invoke  : ERROR: Unable to open IO config file %s \n", config_file.c_str());
        return kTfLiteError;
    }
    fseek(fp_config, 0, SEEK_END);
    prms.io_capacity = ftell(fp_config);
    fseek(fp_config, 0, SEEK_SET);
    fclose(fp_config);
    prms.ioBufDescPtr = malloc(prms.io_capacity);
    status = prms.TIDLReadBinFromFile(config_file.c_str(), prms.ioBufDescPtr, prms.io_capacity);

    if(data->m_debug_level >= 2)
    {
        prms.traceLogLevel = data->m_debug_level;
        prms.traceWriteLevel = 3;
    }

    tflrt_printf(data->m_debug_level,"%s %s %d \n", __FILE__, __func__, __LINE__);
    status = data->infer_ops.TIDLRT_create(&prms, &handle);
    tflrt_printf(data->m_debug_level,"%s %s %d \n", __FILE__, __func__, __LINE__);

    sTIDL_IOBufDesc_t *ioBufDesc = (sTIDL_IOBufDesc_t *)prms.ioBufDescPtr;

    tidldelegate->rtInList  = (void *)malloc(ioBufDesc->numInputBuf * sizeof(sTIDLRT_Tensor_t));
    tidldelegate->rtOutList = (void *)malloc(ioBufDesc->numOutputBuf * sizeof(sTIDLRT_Tensor_t));
    tidldelegate->rtHandle    = handle;
    tidldelegate->stats       = prms.stats;
    tidldelegate->ioBufDesc  = prms.ioBufDescPtr;
    tidldelegate->netPtr     = prms.netPtr;

    tflrt_printf(data->m_debug_level,"%s %s %d \n", __FILE__, __func__, __LINE__);
    return kTfLiteOk;
}

TfLiteStatus tidl_subgraph_rt_delete(tidlDelegate *tidldelegate)
{
    int status = 0;
    TIDL_TfLiteDelegateData *options = tidldelegate->data_;

    if(tidldelegate->rtHandle)
    {
        status = options->infer_ops.TIDLRT_deactivate(tidldelegate->rtHandle);
        status = options->infer_ops.TIDLRT_delete(tidldelegate->rtHandle);
    }
    free(tidldelegate->rtInList);
    free(tidldelegate->rtOutList);
    free(tidldelegate->netPtr);
    free(tidldelegate->ioBufDesc);
    free(tidldelegate->stats);
    return kTfLiteOk;
}

TfLiteStatus tidl_subgraph_rt_invoke(TfLiteContext *context, TfLiteNode *node, tidlDelegate *tidldelegate)
{
    int status = 0;
    int j = 0;
    TIDL_TfLiteDelegateData *data = tidldelegate->data_;
    tflrt_printf(data->m_debug_level,"%s %s %d \n", __FILE__, __func__, __LINE__);

    void *handle = tidldelegate->rtHandle;
    sTIDLRT_PerfStats_t *stats = (sTIDLRT_PerfStats_t *)tidldelegate->stats;

    sTIDLRT_Tensor_t *in[128];
    sTIDLRT_Tensor_t *out[128];
    sTIDLRT_Tensor_t *ins;
    sTIDLRT_Tensor_t *outs;

    ins = (sTIDLRT_Tensor_t *)tidldelegate->rtInList;
    outs = (sTIDLRT_Tensor_t *)tidldelegate->rtOutList;

    if ((ins == NULL) || (outs == NULL))
    {
        printf("Invoke  : ERROR: Unable to allocate memory for TIDL RT in[] out [] tensor struct\n");
        return kTfLiteError;
    }
    else
    {
        /* Input tesnsors property set up */
        j = 0;
        for (auto tensor_index : TfLiteIntArrayView(node->inputs))
        {
            if (tensor_index == kTfLiteOptionalTensor)
            {
                continue;
            }
            TfLiteTensor *tensor = &context->tensors[tensor_index];
            if (tensor->allocation_type == kTfLiteArenaRw)
            {
                in[j] = &(ins[j]);
                status = data->infer_ops.TIDLRT_setTensorDefault(in[j]);
                in[j]->layout = TIDLRT_LT_NHWC;
                strcpy((char *)in[j]->name, tensor->name);
                TIDL_tfliteRtGetScaleAndZeroPoint(tensor, &in[j]->scale, &in[j]->zeroPoint);
                status = TIDL_tfliteRtGetTypeAndPtr(tensor, &in[j]->elementType, &in[j]->ptr);
                if(status == -1)
                {
                    return kTfLiteError;
                }
                j++;
          }
        }

        /* Output tensors property set up */
        j = 0;
        for (auto tensor_index : TfLiteIntArrayView(node->outputs))
        {
            if (tensor_index == kTfLiteOptionalTensor)
            {
                continue;
            }
            TfLiteTensor *tensor = &context->tensors[tensor_index];
            if (tensor->allocation_type == kTfLiteArenaRw)
            {
              out[j] = &(outs[j]);
              status = data->infer_ops.TIDLRT_setTensorDefault(out[j]);
              out[j]->layout = TIDLRT_LT_NHWC;
              strcpy((char *)out[j]->name, tensor->name);
              TIDL_tfliteRtGetScaleAndZeroPoint(tensor, &out[j]->scale, &out[j]->zeroPoint);
              status = TIDL_tfliteRtGetTypeAndPtr(tensor, &out[j]->elementType, &out[j]->ptr);
              if(status == -1)
              {
                  return kTfLiteError;
              }
              j++;
           }
        }
    }
    tflrt_printf(data->m_debug_level,"%s %s %d \n", __FILE__, __func__, __LINE__);
    status = data->infer_ops.TIDLRT_invoke(handle, in, out);
    tflrt_printf(data->m_debug_level,"%s %s %d \n", __FILE__, __func__, __LINE__);

    if(data->m_debug_level > 0)
    {
        double proc_time    = (stats->proc_time_end - stats->proc_time_start)  / 1000;
        double cp_in_time   = (stats->cpIn_time_end - stats->cpIn_time_start)  / 1000;
        double cp_out_time  = (stats->cpOut_time_end - stats->cpOut_time_start)/ 1000;

        printf("Sub Graph Stats %f %f %f \n", cp_in_time, proc_time, cp_out_time);
    }
#if 0
    // check if the converted output of TIDL subgraph for tflite runtime is correct
    FILE * fpFloat = fopen("/tmp/tflite_data_float.bin", "wb");
    FILE * fpUint8 = fopen("/tmp/tflite_data_uint8.bin", "wb");
    for (auto tensor_index : TfLiteIntArrayView(node->outputs))
    {
        TfLiteTensor *tensor = &context->tensors[tensor_index];
        uint8_t * tfliteData = (uint8_t *)(tensor->data.uint8);
        TfLiteAffineQuantization *quantization = (TfLiteAffineQuantization *)tensor->quantization.params;
        int32_t zeroPoint = quantization->zero_point->data[0];
        float scale = quantization->scale->data[0];
        //printf("Final tflite zero point = %d, scale = %f \n", zeroPoint, scale);
        float * outFloat = (float *)malloc(tensor->bytes * 4);
        for(int i = 0; i < tensor->bytes; i++)
        {
            outFloat[i] = (tfliteData[i] - zeroPoint) * scale;
            //fwrite(&tfliteData[i], 1, 1, fpUint8);
            //fwrite(&outFloat[i], 1, 4, fpFloat);
        }
    }
    fclose(fpFloat);
    fclose(fpUint8);
#endif
    return kTfLiteOk;
}

TfLiteStatus tidlDelegate::Init(TfLiteContext* context, const TfLiteDelegateParams* params) {
    TIDL_TfLiteDelegateData* data = reinterpret_cast<TIDL_TfLiteDelegateData *>(params->delegate->data_);

    data_ = data;
    data_->m_valid_subgraphs.insert(this);
    data_->m_pending_subgraphs.insert(this);
    subgraphId_ = getOneOutTensorIdx(context, params);

    // Check if subgraph contains OD post processing part
    bool isSubgraphOD = false;

    std::vector<int> nodesInSubgraph;
    
    for (auto node_index : TfLiteIntArrayView(params->nodes_to_replace)) 
    {
        nodesInSubgraph.push_back(node_index);
        TfLiteNode* node;
        TfLiteRegistration* registration;
        TF_LITE_ENSURE_STATUS(context->GetNodeAndRegistration(context, node_index, &node, &registration));

        if((registration->builtin_code == kTfLiteBuiltinCustom) || (registration->builtin_code == kTfLiteBuiltinNonMaxSuppressionV4)
           || (registration->builtin_code == kTfLiteBuiltinNonMaxSuppressionV5))
        {
            isSubgraphOD = true;
        }
    }
    data->supported_node_groups.push_back(nodesInSubgraph);

    TIDL_tfliteRtImportInit(context, params, subgraphId_, data->m_num_param_bits, data->m_pre_batchnorm_fold,
            const_cast<char *>(data->m_tidl_tools_path.c_str()), data->m_debug_level, data->m_meta_layers_names_list,
                          data->m_meta_arch_type, data->odPostProcHeadNames, isSubgraphOD, data->m_add_data_convert_ops, &data->odUserParams);
    for (auto node_index : TfLiteIntArrayView(params->nodes_to_replace)) 
    {
        TfLiteNode* node;
        TfLiteRegistration* registration;
        TF_LITE_ENSURE_STATUS(context->GetNodeAndRegistration(
                    context, node_index, &node, &registration));
        nodes_.push_back(node_index);

        if((data->odBackboneNodeNames.size() == 0)  //non OD network
          || (std::find(data->odBackboneNodeNames.begin(), data->odBackboneNodeNames.end(), context->tensors[node_index].name) != data->odBackboneNodeNames.end()))
        {
            //Map all nodes for non OD network. For OD network, map nodes only if they are part of backbone, do not map the post proc nodes
            TIDL_tfliteRtImportAndLinkNode(registration, context, params, node, data->m_debug_level, &data->odUserParams);
        }
    }

    TIDL_tfliteRtOptimizeNet(data->m_debug_level);

    TIDL_saveTidlSubGraph(context, params, &subGraphPtr_);

    /* save in float mode in Init and do an RT create
     * all intermediate tensors will be done in float
     * mode
     */
    std::vector<float32_tidl> qfloat(TIDL_MAX_ALG_IN_BUFS);
    printf("\n ************** Frame index 1 : Running float import ************* \n");
    TIDL_tfliteRtPostProcessNet(1, 32, data->m_tidl_calibration_flags, data->m_calibration_iterations,
            data->m_quantization_scale_type, data->m_high_resolution_optimization, data->m_compileConstraintsFlag,
            data->m_pre_batchnorm_fold, subGraphPtr_, qfloat.data(), const_cast<char *>(data->m_temp_folder.c_str()), 
            subgraphId_, data->m_debug_level, data->m_output_feature_16bit_names_list, data->m_params_16bit_names_list);
    tidl_subgraph_rt_create(context, this);

    return kTfLiteOk;
}

TfLiteStatus tidlDelegate::Prepare(TfLiteContext* context, TfLiteNode* node) {
    return kTfLiteOk;
}

static void copy_file(std::string basename, std::string dstdir, std::string srcdir) {
    std::string src_fname = srcdir + "/" + basename;
    std::string dst_fname = dstdir + "/" + basename;
    int src_fd = open(src_fname.c_str(), O_RDONLY);
    int dst_fd = open(dst_fname.c_str(), O_WRONLY | O_CREAT | O_TRUNC, 0644);
    ssize_t size = lseek(src_fd, 0, SEEK_END); lseek(src_fd, 0, SEEK_SET);
    std::unique_ptr<char[]> buffer = std::make_unique<char[]>(size);

    {
        auto done = 0;
        auto remaining = size;
        while(remaining) {
            int ret = read(src_fd, buffer.get() + done, remaining);
            done += ret;
            remaining -= ret;
        }
    }
    {
        auto done = 0;
        auto remaining = size;
        while(remaining) {
            int ret = write(dst_fd, buffer.get() + done, remaining);
            done += ret;
            remaining -= ret;
        }
    }

    close(src_fd);
    close(dst_fd);
}

TfLiteStatus tidlDelegate::Invoke(TfLiteContext* context, TfLiteNode* node) {
    TfLiteStatus status = kTfLiteOk;
    int outTensorIdx;
    currFrameIdx_++;
    std::fill_n(inTensorsMin_, TIDL_MAX_ALG_IN_BUFS, FLT_MAX);
    std::fill_n(inTensorsMax_, TIDL_MAX_ALG_IN_BUFS, -FLT_MAX);
    tflrt_printf(data_->m_debug_level,"%s %s %d \n", __FILE__, __func__, __LINE__);

    /* RT invoke, this will always run in float mode
     * as the RT create was done in Init with float
     * models
     */
    tidl_subgraph_rt_invoke(context, node, this);
    tflrt_printf(data_->m_debug_level,"%s %s %d \n", __FILE__, __func__, __LINE__);
    /* Now import and this will write quant model
     * when all frames are fed in
     */
    // This will happen for the first invoke of the first subgraph, and never again
    // TODO: should this go to Prepare?
    while(data_->m_valid_subgraphs.size() > data_->m_num_tidl_subgraphs) {
        auto it = data_->m_valid_subgraphs.rbegin();
        data_->m_invalid_subgraphs.insert(*it);

        // delete from pending list, and then from valid list
        data_->m_pending_subgraphs.erase(*it);
        data_->m_valid_subgraphs.erase(*it);
    }

    // import only valid subgraphs
    if(data_->m_valid_subgraphs.find(this) != data_->m_valid_subgraphs.end()) 
    {
        if(tidl_subgraph_import(context, node, this, &currNumInTensors_, currFrameIdx_, &inTensorsQuantFactor_,
                &inTensorsMin_, &inTensorsMax_)) 
        {
            // subgraph import complete, copy from tempDir to artifacts
            data_->m_pending_subgraphs.erase(this);

            copy_file(std::to_string(subgraphId_) + "_tidl_net.bin", data_->m_artifacts_folder, data_->m_temp_folder);
            copy_file(std::to_string(subgraphId_) + "_tidl_io_1.bin", data_->m_artifacts_folder, data_->m_temp_folder);

            if(data_->m_pending_subgraphs.empty()) 
            {
                // all subgraphs import complete, write out nodes list
                std::set<int> delete_nodes;
                for (auto it : data_->m_invalid_subgraphs)
                    delete_nodes.insert(it->nodes_.begin(), it->nodes_.end());

                std::string allow_fname = data_->m_artifacts_folder + "/allowedNode.txt";
                FILE *fp = fopen(allow_fname.c_str(), "w");
                for (auto node_index : data_->m_supported_nodes)
                {
                    if(delete_nodes.find(node_index) != delete_nodes.end())
                    {
                        for(int i = 0; i < data_->supported_node_groups.size(); i++)
                        {
                            for(auto& node : data_->supported_node_groups[i])
                            {
                                if(node == node_index)
                                {
                                    data_->supported_node_groups.erase(std::find(data_->supported_node_groups.begin(), data_->supported_node_groups.end(), data_->supported_node_groups[i]));
                                    i--;
                                    break;
                                }
                            }
                        }
                        //fprintf(fp, "%d\n", node_index);
                    }
                }
                int32_t numSuportedNodes = 0;
                fprintf(fp, "%d\n", data_->supported_node_groups.size());
                for(int i = 0; i < data_->supported_node_groups.size(); i++)
                {
                    std::vector<int> subgraph = data_->supported_node_groups[i];
                    fprintf(fp, "%d\n", subgraph.size());
                    for(int j = 0; j < subgraph.size(); j++)
                    {
                    fprintf(fp, "%d\n", subgraph[j]);
                    numSuportedNodes++;
                    }
                }
                fclose(fp);

                TIDL_runGraphvizToolRuntimes(data_->m_tidl_tools_path, data_->m_artifacts_folder, data_->m_debug_level);
            }
        }
    }
    tflrt_printf(data_->m_debug_level,"%s %s %d \n", __FILE__, __func__, __LINE__);

    return status;
}

tidlDelegate::~tidlDelegate() {
    free(subGraphPtr_);
    tidl_subgraph_rt_delete(this);
    delete [] inTensorsQuantFactor_;
    delete [] inTensorsMin_;
    delete [] inTensorsMax_;
}

/*
 * Create the TfLiteRegistration for the Kernel node which will replace
 * the subgraph in the main TfLite graph.
 */
static TfLiteRegistration GetTIDLNodeRegistration() {
    /*
     * This is the registration for the Delegate Node that gets added to
     * the TFLite graph instead of the subGraph it replaces.
     * It is treated as a an OP node. But in our case
     * Init will initialize the delegate
     * Invoke will run the delegate graph.
     * Prepare for preparing the delegate.
     * Free for any cleaning needed by the delegate.
     */

    TfLiteRegistration kernel_registration = {0};
    kernel_registration.builtin_code = kTfLiteBuiltinDelegate;
    kernel_registration.custom_name = "tidlDelegate";
    kernel_registration.free = [](TfLiteContext* context, void* buffer) -> void {
        delete reinterpret_cast<tidlDelegate*>(buffer);
    };

    kernel_registration.init = [](TfLiteContext* context, const char* buffer, size_t) -> void* {
        const TfLiteDelegateParams* params = reinterpret_cast<const TfLiteDelegateParams*>(buffer);
        tidlDelegate* delegate = new tidlDelegate;
        if (delegate->Init(context, params) != kTfLiteOk)
            return nullptr;
        return delegate;
    };

    kernel_registration.invoke = [](TfLiteContext* context,
            TfLiteNode* node) -> TfLiteStatus {
        tidlDelegate* kernel = reinterpret_cast<tidlDelegate*>(node->user_data);
        return kernel->Invoke(context, node);
    };

    kernel_registration.prepare = [](TfLiteContext* context,
            TfLiteNode* node) -> TfLiteStatus {
        tidlDelegate* kernel = reinterpret_cast<tidlDelegate*>(node->user_data);
        return kernel->Prepare(context, node);
    };

    kernel_registration.get_custom_data = [](TfLiteContext* context,
            TfLiteNode* node, const char *op_name,
            char **node_name, void **node_data) -> TfLiteStatus {
        return kTfLiteError;
    };


    return kernel_registration;
}

std::vector<std::vector<int>> fusedCombinations = {{kTfLiteBuiltinPack, kTfLiteBuiltinPack, kTfLiteBuiltinReshape},
                                                   /*{kTfLiteBuiltinReshape, kTfLiteBuiltinTranspose, kTfLiteBuiltinReshape}*/};


/* Logic for allowlisting possible fusable combinations of layers:
*  Maintain vector fusedCombinations of possible combinations
*  Maintain vector activeChecks of form {{fusedCombinations Index, current check location in fusedCombinations Index}, {supported nodes in this combination till now}}
*  If a layer matches the beginning of a combination, say i, then push {{i, 1}, {current node index}} in activeChecks; if i not already in activeChecks
*  If activeChecks not empty, then check if next location in index i matches next layer - if yes, increment location by 1 and push node in supported list; if
doesn't match, remove the vector from activeChecks
*  On reaching end of a combination, push nodes from activeChecks to vector supportedNodes to be returned; and remove the vector from activeChecks
*/
std::vector<int> TIDL_tfliteAllowlistFusedLayers(TfLiteIntArray * plan, TfLiteContext* context)
{
    std::vector<int> supportedNodes = {};
    TfLiteNode* node;
    TfLiteRegistration* registration;
    std::vector<std::pair<std::vector<int>, std::vector<int>>> activeChecks = {};

    bool checking = false;

    for (int node_index : TfLiteIntArrayView(plan)) 
    {
        context->GetNodeAndRegistration(context, node_index, &node, &registration);
        if(activeChecks.size() > 0)
        {
            for(int i = 0; i < activeChecks.size(); i++)
            {
                if(registration->builtin_code == fusedCombinations[activeChecks[i].first[0]][activeChecks[i].first[1]])
                {
                    activeChecks[i].first[1] += 1;
                    activeChecks[i].second.push_back(node_index);
                    if(activeChecks[i].first[1] == fusedCombinations[activeChecks[i].first[0]].size()) //we have checked all layers in combination
                    {
                        supportedNodes.insert(supportedNodes.end(), activeChecks[i].second.begin(), activeChecks[i].second.end());
                        activeChecks.erase(activeChecks.begin() + i);
                    }
                }
                else
                {
                    activeChecks.erase(activeChecks.begin() + i);
                }
            }
        }
        for(int i = 0; i < fusedCombinations.size(); i++)
        {
            if(registration->builtin_code == fusedCombinations[i][0])
            {
                checking = false;
                for(int j = 0; j < activeChecks.size(); j++) //check if i already in activeChecks
                {
                    if(i == activeChecks[j].first[0])
                    {
                        checking = true;
                        break;
                    }
                }
                if(!checking)
                {
                    std::vector<int> map = {i,1};
                    std::vector<int> nodes = {node_index};
                    activeChecks.push_back(std::make_pair(map, nodes));
                }
            }
        }
    }
    return supportedNodes;
}

void TIDL_OdOptionChecks(TIDL_TfLiteDelegateData* data, bool hasDetectionPostprocLayer)
{
  if(data->m_meta_layers_names_list.empty())
  {
    data->m_meta_arch_type = -1;
    if(! hasDetectionPostprocLayer)
    {
        printf("\nWARNING : 'meta_layers_names_list' is not provided - running OD post processing in ARM mode \n \n");
    }
  }
  else
  {
    //Check if meta_layers_names_list file exists
    std::ifstream ifile(data->m_meta_layers_names_list.c_str());
    if(! ifile.good())
    {
      printf("\nERROR : File provided in meta_layers_names_list does not exist : %s \n \n", data->m_meta_layers_names_list.c_str());
      delete data;
      exit(-1);
    }
    //Check if metaArchType is valid
    std::vector<int> valid_meta_arch_type{TIDL_metaArchTIDLRetinaNet};
    if(std::find(valid_meta_arch_type.begin(), valid_meta_arch_type.end(), data->m_meta_arch_type) == valid_meta_arch_type.end()) 
    {
      printf("\nERROR : Please provide valid 'meta_arch_type' : supported values - 5 \n \n");
      delete data;
      exit(-1);
    }
  }
}

extern "C"
{
extern std::vector<std::string> diagsInfo;

std::string GetOpNameByRegistration(const TfLiteRegistration& registration) {
  auto op = registration.builtin_code;
  std::string result =
      EnumNameBuiltinOperator(static_cast<BuiltinOperator>(op));
  if ((op == kTfLiteBuiltinCustom || op == kTfLiteBuiltinDelegate) &&
      registration.custom_name) {
    result += "_" + std::string(registration.custom_name);
  }
  return result;
}

void TIDL_getGraphVisualizationInfo(TfLiteContext* context, TfLiteIntArray* plan, std::string artifactsFolder, std::vector<std::vector<int>> outputAdjacencyList, std::vector<std::vector<int>> inputAdjacencyList)
{
  TIDL_runtimesVisualisationInfo * visInfo = new TIDL_runtimesVisualisationInfo[plan->size];
  
  TfLiteNode* node;
  TfLiteRegistration* registration;
  for (int node_index : TfLiteIntArrayView(plan))
  {
    context->GetNodeAndRegistration(context, node_index, &node, &registration);
    
    TfLiteTensor* tensor = &context->tensors[node->outputs->data[0]]; //tflite nodes don't have "name" attribute, so identify using node output tensor name
    visInfo[node_index].nodeModelIdx = node_index;
    visInfo[node_index].nodeName = tensor->name;
    visInfo[node_index].outputAdjNodes = outputAdjacencyList[node_index];
    visInfo[node_index].inputAdjNodes = inputAdjacencyList[node_index];
    visInfo[node_index].opType = GetOpNameByRegistration(*registration);
    visInfo[node_index].diagnosticInfo = diagsInfo[node_index];
  }

  std::ofstream outfile;
  std::string graphvizFileName = artifactsFolder + "/tempDir/graphvizInfo.txt";
  outfile.open(graphvizFileName);
  if(outfile.is_open())
  {
    outfile << std::to_string(plan->size) <<std::endl;
    for(int i = 0; i < plan->size; i++)
    {
      outfile << std::to_string(visInfo[i].nodeModelIdx) << " " << visInfo[i].nodeName << " " << visInfo[i].opType << " ";
      outfile << "outputAdjNodes " << visInfo[i].outputAdjNodes.size() << " "; 
      if(visInfo[i].outputAdjNodes.size() == 0)
      {
        context->GetNodeAndRegistration(context, TfLiteIntArrayView(plan)[i], &node, &registration);
        std::string output_name = context->tensors[node->outputs->data[0]].name;
        outfile << output_name << " " ;
      }
      for(auto& adjNode : visInfo[i].outputAdjNodes)
      {
        outfile << adjNode << " ";
      }
      outfile << "inputAdjNodes " << visInfo[i].inputAdjNodes.size() << " "; 
      if(visInfo[i].inputAdjNodes.size() == 0)
      {
        context->GetNodeAndRegistration(context, TfLiteIntArrayView(plan)[i], &node, &registration);
        std::string input_name = context->tensors[node->inputs->data[0]].name;
        outfile << input_name << " " ;
      }
      for(auto& adjNode : visInfo[i].inputAdjNodes)
      {
        outfile << adjNode << " ";
      }
      outfile << "diagInfo " << visInfo[i].diagnosticInfo;
      outfile << std::endl;
    }
    outfile.close();
  }
  else
  {
    printf("Warning :: Cannot open %s -- graph visualisation will not work \n", graphvizFileName.c_str());
  }

  delete [] visInfo;
}

static TfLiteStatus DelegatePrepare(TfLiteContext* context, TfLiteDelegate* delegate) {
    TfLiteStatus ret;
    TfLiteNode* node;
    TfLiteRegistration* registration;

    TIDL_TfLiteDelegateData* data = reinterpret_cast<TIDL_TfLiteDelegateData *>(delegate->data_);
    
    diagsInfo = {};
    data->m_valid_subgraphs = {};
    data->m_pending_subgraphs = {};
    data->m_supported_nodes = {};
    data->supported_node_groups = {};

    TIDL_tfliteRtInit(data->m_num_param_bits, data->m_pre_batchnorm_fold, data->m_add_data_convert_ops);

    TfLiteIntArray* plan;
    TF_LITE_ENSURE_STATUS(context->GetExecutionPlan(context, &plan));

    bool isObjectDetectionNetwork = false;
    bool hasDetectionPostprocLayer = false;
    for (int node_index : TfLiteIntArrayView(plan)) 
    {
        TF_LITE_ENSURE_STATUS(context->GetNodeAndRegistration(
                    context, node_index, &node, &registration));
        if((registration->builtin_code == kTfLiteBuiltinCustom) || (registration->builtin_code == kTfLiteBuiltinNonMaxSuppressionV4)
           || (registration->builtin_code == kTfLiteBuiltinNonMaxSuppressionV5))
        {
            isObjectDetectionNetwork = true;
        }
        if(registration->builtin_code == kTfLiteBuiltinCustom) // Tflite detection post process layer present, do not use meta arch parameters
        {
            hasDetectionPostprocLayer = true;
            data->m_meta_layers_names_list = "";
            data->m_meta_arch_type = -1;
        }
    }

    std::vector<int> odBackboneNodeIds = {};
    data->odBackboneNodeNames = {};
    data->outputAdjacencyList = {{}};
    data->inputAdjacencyList = {{}};
    data->outputAdjacencyList = TIDL_createOutputAdjacencyList(context, plan);
    data->inputAdjacencyList = TIDL_createInputAdjacencyList(context, plan);

    if(isObjectDetectionNetwork)
    {
        TIDL_OdOptionChecks(data, hasDetectionPostprocLayer);  //These checks make sense only if network is OD
        
        data->odPostProcHeadNames = TIDL_readMetaArchInfo(data->m_meta_layers_names_list);

        std::vector<int> postProcInputIds = TIDL_getPostProcInputIds(context, plan, data->odPostProcHeadNames); 

        std::vector<bool> visited;
        visited.assign(plan->size, false);
        
        // Run DFS on the graph with OD "heads" as root and traversal towards the network input to get nodes of the backbone network 
        odBackboneNodeIds =  TIDL_callNodeTraversal(data->inputAdjacencyList, postProcInputIds, plan->size);

        printf("Size of odBackboneNodeIds = %d \n", odBackboneNodeIds.size());
        for(int i = 0; i < odBackboneNodeIds.size(); i++)
        {
          data->odBackboneNodeNames.push_back(context->tensors[odBackboneNodeIds[i]].name); //convert node indices to corresponding names since indices change after graph partition
        }
    }
    else
    {
        data->odBackboneNodeNames = {};
        data->odPostProcHeadNames = {};
        data->m_meta_arch_type = -1;
        data->m_meta_layers_names_list = "";
    }


    int num_components = 1;
    int last_index = -1;

    std::vector<int> supportedFusedNodes = TIDL_tfliteAllowlistFusedLayers(plan, context);
    bool isSupported = false;

    for (int node_index : TfLiteIntArrayView(plan)) 
    {
        isSupported = false;
        for(int i = 0; i < supportedFusedNodes.size(); i++)
        {
            if(supportedFusedNodes[i] == node_index)
            {
                isSupported = true;
                break;
            }
        }

        TF_LITE_ENSURE_STATUS(context->GetNodeAndRegistration(
                    context, node_index, &node, &registration));
        if(!isSupported)
        {
            isSupported = TIDL_tfliteAllowlistNode(registration, node, node_index, context, isObjectDetectionNetwork,
                                  data->m_debug_level, data->m_deny_list.data(), data->m_deny_list.size(), odBackboneNodeIds, hasDetectionPostprocLayer,
                                  &data->odUserParams);
        }
        else
        {
            diagsInfo.push_back("");
        }

        if (isSupported) 
        {
            if (last_index != -1 && node_index != last_index + 1) 
            {
                ++num_components;  //TODO: Primitive logic to check number of subgraphs, need to have a more robust logic
            }
            data->m_supported_nodes.push_back(node_index);
            last_index = node_index;
        }
    }
    printf("\n Number of subgraphs:%d , %d nodes delegated out of %d nodes \n \n", num_components, data->m_supported_nodes.size(), plan->size);
    TfLiteRegistration TIDL_kernel_registration = GetTIDLNodeRegistration();

    TfLiteIntArray *nodes = static_cast<TfLiteIntArray *>(malloc(sizeof(*nodes) + sizeof(nodes->data[0]) * data->m_supported_nodes.size()));
    nodes->size = data->m_supported_nodes.size();
    std::copy(data->m_supported_nodes.begin(), data->m_supported_nodes.end(), &nodes->data[0]);
    ret = context->ReplaceNodeSubsetsWithDelegateKernels(context, TIDL_kernel_registration, nodes, delegate);
    free(nodes);

    TIDL_getGraphVisualizationInfo(context, plan, data->m_artifacts_folder, data->outputAdjacencyList, data->inputAdjacencyList);

    return ret;
}
}

template <typename T>
static T LoadSymbol(void *lib, const char* symbol) {
    T sym = reinterpret_cast<T>(dlsym(lib, symbol));
    if(!sym)
        throw "dlsym(" + std::string(symbol) + ")";
    return sym;
}

static bool check_isdir(const char *path) {
    const char *real = realpath(path, NULL);
    if(!real)
        return false;

    struct stat st;
    int res = stat(real, &st);
    if(res)
        return false;

    bool ret = false;
    if ((st.st_mode & S_IFMT) == S_IFDIR) {
        ret = true;
        free(const_cast<char *>(real));
    }

    return ret;
}

static bool check_isempty(const char *path) {
    if (!check_isdir(path))
        return false;

    struct dirent *e;
    DIR *d = opendir(path);

    if(!d)
        return false;

    errno = 0;
    while(e = readdir(d)) {
        /* do not recurse into . and .. */
        if(!strcmp(e->d_name, ".") || !strcmp(e->d_name, ".."))
            continue;
        return false;
    }

    if(errno)
        return false;

    return true;
}

std::vector<int32_t> tidl_fillDenyListOption(char * deny_list)
{
    std::vector<int32_t> ret;
    char * token = strtok(deny_list, ",");
    while( token != NULL ) 
    {
        for(int i = 0; i < strlen(token); i++)
        {
            if(token[i] ==  ' ')
            {
                memmove(token+i, token+i+1, strlen(token) - i);
            } 
        }
        int32_t itoken;
        sscanf(token, "%d", &itoken);
        ret.push_back(itoken);
        token = strtok(NULL, ",");
    }
    return ret;
}

extern "C"{
TfLiteDelegate* tflite_plugin_create_delegate(char** options_keys,
                                              char** options_values,
                                              size_t num_options,
                                              void (*error_handler)(const char*)) {
    TIDL_TfLiteDelegateData *data = new TIDL_TfLiteDelegateData;

    data->infer_ops.lib = dlopen("libvx_tidl_rt.so", RTLD_NOW | RTLD_GLOBAL);
    if(!data->infer_ops.lib) {
        delete data;

        // TODO: make a function to take va_list arguments
        if (error_handler) error_handler("could not load library libvx_tidl_rt.so");
        return nullptr;
    }
    try {
        data->infer_ops.TIDLRT_create = LoadSymbol<decltype(data->infer_ops.TIDLRT_create)>(data->infer_ops.lib, "TIDLRT_create");
        data->infer_ops.TIDLRT_delete = LoadSymbol<decltype(data->infer_ops.TIDLRT_delete)>(data->infer_ops.lib, "TIDLRT_delete");
        data->infer_ops.TIDLRT_invoke = LoadSymbol<decltype(data->infer_ops.TIDLRT_invoke)>(data->infer_ops.lib, "TIDLRT_invoke");
        data->infer_ops.TIDLRT_deactivate = LoadSymbol<decltype(data->infer_ops.TIDLRT_deactivate)>(data->infer_ops.lib, "TIDLRT_deactivate");
        data->infer_ops.TIDLRT_setParamsDefault = LoadSymbol<decltype(data->infer_ops.TIDLRT_setParamsDefault)>(data->infer_ops.lib, "TIDLRT_setParamsDefault");
        data->infer_ops.TIDLRT_setTensorDefault = LoadSymbol<decltype(data->infer_ops.TIDLRT_setTensorDefault)>(data->infer_ops.lib, "TIDLRT_setTensorDefault");
        data->infer_ops.TIDLRT_getDdrStats = LoadSymbol<decltype(data->infer_ops.TIDLRT_getDdrStats)>(data->infer_ops.lib, "TIDLRT_getDdrStats");
    } catch (std::string &e) {
        delete data;

        if (error_handler) error_handler("could not load symbol from library libvx_tidl_rt.so");
        return nullptr;
    }

    data->odUserParams.confidence_threshold = -1;
    data->odUserParams.nms_threshold = -1;
    data->odUserParams.top_k = -1;
    data->odUserParams.keep_top_k = -1;

    for (uint32_t idx = 0; idx < num_options; idx++) 
    {
        if (!strcmp("tidl_tools_path", options_keys[idx])) {
            data->m_tidl_tools_path = options_values[idx];
            if(!check_isdir(data->m_tidl_tools_path.c_str())) {
                delete data;

                if (error_handler) error_handler("tidl_tools_path not a directory");
                return nullptr;
            }

            // TODO: maybe check for the libs, quants tools, GC tool are contained inside
        }

        if (!strcmp("artifacts_folder", options_keys[idx])) {
            data->m_artifacts_folder = options_values[idx];
            if(!check_isdir(data->m_artifacts_folder.c_str())) {
                delete data;

                if (error_handler) error_handler("artifacts_folder not a directory");
                return nullptr;
            }
            if(!check_isempty(data->m_artifacts_folder.c_str())) {
                delete data;

                if (error_handler) error_handler("artifacts_folder not empty");
                return nullptr;
            }
        }

        if (!strcmp("debug_level", options_keys[idx])) {
            std::stringstream(options_values[idx]) >> data->m_debug_level;
            // TODO: any invalid values? like negative, or beyond supported range?
        }

        if (!strcmp("tensor_bits", options_keys[idx])) {
            std::stringstream(options_values[idx]) >> data->m_num_param_bits;

            std::vector<int> valid_num_params{8, 16, 32};
            if(std::find(valid_num_params.begin(), valid_num_params.end(), data->m_num_param_bits) == valid_num_params.end()) {
                delete data;

                if (error_handler) error_handler("unsupported tensor_bits");
                return nullptr;
            }
        }

        if (!strcmp("max_num_subgraphs", options_keys[idx])) {
            std::stringstream(options_values[idx]) >> data->m_num_tidl_subgraphs;

            if(data->m_num_tidl_subgraphs > MAX_NUM_TIDL_SUBGRAPHS) {
                delete data;

                if (error_handler) error_handler("max_num_subgraphs > MAX_NUM_TIDL_SUBGRAPHS not allowed");
                return nullptr;
            }
        }

        // TODO: fix denylist
        if (strcmp("deny_list", options_keys[idx]) == 0)  {
            try {
                data->m_deny_list = tidl_fillDenyListOption(options_values[idx]);
            } catch(std::string &e) {
                delete data;

                if (error_handler) error_handler("could not parse malformed 'deny_list' option");
                return nullptr;
            } 
        }

        if (!strcmp("accuracy_level", options_keys[idx])) {
            std::map<std::string, int> valid_calibs {{"0", 64}, {"1", 7}, {"9", 9}}; // 9 will be mapped to suitable flag based on advanced options
            if(valid_calibs.find(options_values[idx]) == valid_calibs.end()) {
                delete data;

                if (error_handler) error_handler("unsupported accuracy_level");
                return nullptr;
            }

            data->m_tidl_calibration_flags = valid_calibs[options_values[idx]];
        }

        if (!strcmp("advanced_options:calibration_frames", options_keys[idx])) {
            std::stringstream(options_values[idx]) >> data->m_calibration_frames;
            // TODO: any invalid values? like negative, or too many frames?
        }

        if (!strcmp("advanced_options:calibration_iterations", options_keys[idx])) { 
            std::stringstream(options_values[idx]) >> data->m_calibration_iterations;
            // TODO: any invalid values? like negative, or too many iters?
        }

        if (!strcmp("advanced_options:quantization_scale_type", options_keys[idx])) { 
            std::map<std::string, int> quantization_scale_type_mapping {{"1", 3}, {"0", 2}};
            if(quantization_scale_type_mapping.find(options_values[idx]) == quantization_scale_type_mapping.end()) {
                delete data;

                if (error_handler) error_handler("unsupported 'quantization_scale_type' : specify either 1 or 0");
                return nullptr;
            }

            data->m_quantization_scale_type = quantization_scale_type_mapping[options_values[idx]];
        }

        if (!strcmp("advanced_options:high_resolution_optimization", options_keys[idx])) { 
            std::stringstream(options_values[idx]) >> data->m_high_resolution_optimization;
        }

        if (!strcmp("advanced_options:pre_batchnorm_fold", options_keys[idx])) { 
            std::stringstream(options_values[idx]) >> data->m_pre_batchnorm_fold;
        }
        if (!strcmp("advanced_options:add_data_convert_ops", options_keys[idx])) { 
            std::stringstream(options_values[idx]) >> data->m_add_data_convert_ops;
        }
        if (!strcmp("ti_internal_nc_flag", options_keys[idx])) { 
            std::stringstream(options_values[idx]) >> data->m_compileConstraintsFlag;
        }

        if (!strcmp("advanced_options:output_feature_16bit_names_list", options_keys[idx])) {
            data->m_output_feature_16bit_names_list = options_values[idx];
        }
        if (!strcmp("advanced_options:params_16bit_names_list", options_keys[idx])) {
            data->m_params_16bit_names_list = options_values[idx];
        }

        if (!strcmp("object_detection:meta_layers_names_list", options_keys[idx])) {
            data->m_meta_layers_names_list = options_values[idx];
        }
        if (!strcmp("object_detection:meta_arch_type", options_keys[idx])) { 
            std::stringstream(options_values[idx]) >> data->m_meta_arch_type;
        }
        if (!strcmp("object_detection:confidence_threshold", options_keys[idx])) {
            std::stringstream(options_values[idx]) >> data->odUserParams.confidence_threshold;
        }
        if (!strcmp("object_detection:nms_threshold", options_keys[idx])) {
            std::stringstream(options_values[idx]) >> data->odUserParams.nms_threshold;
        }
        if (!strcmp("object_detection:top_k", options_keys[idx])) {
            std::stringstream(options_values[idx]) >> data->odUserParams.top_k;
        }
        if (!strcmp("object_detection:keep_top_k", options_keys[idx])) {
            std::stringstream(options_values[idx]) >> data->odUserParams.keep_top_k;
        }

        // below options will be used only if accuracy_level = 9
        if (!strcmp("advanced_options:activation_clipping", options_keys[idx])) { 
            std::stringstream(options_values[idx]) >> data->m_activation_clipping;
        }
        if (!strcmp("advanced_options:weight_clipping", options_keys[idx])) { 
            std::stringstream(options_values[idx]) >> data->m_weight_clipping;
        }
        if (!strcmp("advanced_options:bias_calibration", options_keys[idx])) { 
            std::stringstream(options_values[idx]) >> data->m_bias_calibration;
        }
        if (!strcmp("advanced_options:channel_wise_quantization", options_keys[idx])) { 
            std::stringstream(options_values[idx]) >> data->m_channel_wise_quantization;
        }
        if (!strcmp("advanced_options:bias_clipping", options_keys[idx])) { 
            std::stringstream(options_values[idx]) >> data->m_bias_clipping;
        }
    }

    if(data->m_tidl_calibration_flags == 9) //user defined accuracy level
    {
        data->m_tidl_calibration_flags = data->m_activation_clipping * TIDL_CalibOptionActivationRange +     //default 1
                                         data->m_weight_clipping * TIDL_CalibOptionWeightRange +     //default 1
                                         data->m_bias_calibration * TIDL_CalibOptionBiasCalibration +    //default 1
                                         data->m_channel_wise_quantization * TIDL_CalibOptionPerChannelWeightQuantization + //default 0
                                         data->m_bias_clipping * TIDL_CalibOptionBiasRange;   //default 0;   
    }

    if (data->m_tidl_tools_path.empty()) {
        delete data;

        if (error_handler) error_handler("tidl_tools_path must be provided");
        return nullptr;
    }

    if (data->m_artifacts_folder.empty()) {
        delete data;

        if (error_handler) error_handler("artifacts_folder must be provided");
        return nullptr;
    }

    data->m_temp_folder = data->m_artifacts_folder + "/tempDir";
    if(mkdir(data->m_temp_folder.c_str(), 0755)) {
        delete data;

        if (error_handler) error_handler("mkdir tempDir failed");
        return nullptr;
    }


    // TODO: any unparsed arguments?

    TfLiteDelegate* delegate = new TfLiteDelegate;
    delegate->data_ = static_cast<void*>(data);
    delegate->flags = kTfLiteDelegateFlagsNone;
    delegate->Prepare = DelegatePrepare;
    delegate->CopyFromBufferHandle = nullptr;
    delegate->CopyToBufferHandle = nullptr;
    delegate->FreeBufferHandle = nullptr;
    return delegate;
}

void tflite_plugin_destroy_delegate(TfLiteDelegate* delegate) {
    TIDL_TfLiteDelegateData *data = nullptr;

    if(delegate)
        data = static_cast<TIDL_TfLiteDelegateData *>(delegate->data_);

    delete delegate;
    delete data;
}

}
}
}
