/*
*
* Copyright (c) {2015 - 2017} Texas Instruments Incorporated
*
* All rights reserved not granted herein.
*
* Limited License.
*
* Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
* license under copyrights and patents it now or hereafter owns or controls to make,
* have made, use, import, offer to sell and sell ("Utilize") this software subject to the
* terms herein.  With respect to the foregoing patent license, such license is granted
* solely to the extent that any such patent is necessary to Utilize the software alone.
* The patent license shall not apply to any combinations which include this software,
* other than combinations with devices manufactured by or for TI ("TI Devices").
* No hardware patent is licensed hereunder.
*
* Redistributions must preserve existing copyright notices and reproduce this license
* (including the above copyright notice and the disclaimer and (if applicable) source
* code license limitations below) in the documentation and/or other materials provided
* with the distribution
*
* Redistribution and use in binary form, without modification, are permitted provided
* that the following conditions are met:
*
* *       No reverse engineering, decompilation, or disassembly of this software is
* permitted with respect to any software provided in binary form.
*
* *       any redistribution and use are licensed by TI for use only with TI Devices.
*
* *       Nothing shall obligate TI to provide you with source code for the software
* licensed and provided to you in object code.
*
* If software source code is provided to you, modification and redistribution of the
* source code are permitted provided that the following conditions are met:
*
* *       any redistribution and use of the source code, including any resulting derivative
* works, are licensed by TI for use only with TI Devices.
*
* *       any redistribution and use of any object code compiled from the source code
* and any resulting derivative works, are licensed by TI for use only with TI Devices.
*
* Neither the name of Texas Instruments Incorporated nor the names of its suppliers
*
* may be used to endorse or promote products derived from this software without
* specific prior written permission.
*
* DISCLAIMER.
*
* THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
* OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
* OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
* IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
* BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
* OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
* OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
* OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

#ifndef TIDL_ONNX_RT_IMPORT_H
#define TIDL_ONNX_RT_IMPORT_H 1

using namespace std;
using namespace onnx;
#include "core/providers/tidl/tidl_execution_provider_common.h"

int32_t TIDL_onnxAllowlistNode(GraphProto&   onnxGraph, int32_t i, int32_t debugLevel, std::vector<std::string> denyList, int32_t opSetVersion, 
                               bool isObjectDetectionNetwork, std::vector<int> odProcNodesComplement);
int32_t TIDL_onnxRtImportInit(GraphProto&   onnxGraph, onnxRtParams_t *onnxRtParams, char* subgraph_name, 
                            int32_t tensor_bits, char * tidl_tools_path, int32_t debugLevel, std::string metaLayersNamesList,
                            int32_t metaArchType, int32_t opSetVersion, std::vector<std::string>outDataOd, bool isSubgraphOD, int addDataConvertToNet);
int32_t TIDL_onnxRtImportAndLinkNode(GraphProto&   onnxGraph, int32_t nodeIdx, int32_t debugLevel);
int32_t TIDL_onnxRtOptimizeNet(int32_t debugLevel);
void TIDL_saveTidlOnnxRtSubGraph(void ** subGraphPtr);
int32_t TIDL_onnxRtPostProcessNet(int32_t numFrames, int32_t numParamBits, int32_t calibrationOption, int32_t biasCalibrationIterations, 
                                  char* artifacts_folder, void * subGraphPtr, float * inQuantFactor, char* subGraphName, int32_t debugLevel,
                                  std::string outputFeature16bitNamesList , std::string params16bitNamesList, int32_t enableHighResOptimization,
                                  int32_t quantizationStyle, int32_t foldPreBnConv2D, int32_t compileConstraintsFlag, std::vector<int> ctrl1);
std::vector<std::string> TIDL_readMetaArchInfo(std::string filePath, int32_t graphOutputSize);
std::vector<std::vector<int>> TIDL_createInputAdjacencyList(GraphProto&   onnxGraph);
std::vector<std::vector<int>> TIDL_createOutputAdjacencyList(GraphProto&   onnxGraph);
std::vector<int> TIDL_getPostProcInputIds(GraphProto& onnxGraph, std::vector<std::string> odPostProcIndataNames);

int32_t TIDL_ortGetType(int64_t ortType, int32_t *type);

#endif  /* TIDL_TFLTE_RT_IMPORT_H */
