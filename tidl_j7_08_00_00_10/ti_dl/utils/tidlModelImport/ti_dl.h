/*
*
* Copyright (c) {2015 - 2020} Texas Instruments Incorporated
*
* All rights reserved not granted herein.
*
* Limited License.
*
* Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
* license under copyrights and patents it now or hereafter owns or controls to make,
* have made, use, import, offer to sell and sell ("Utilize") this software subject to the
* terms herein.  With respect to the foregoing patent license, such license is granted
* solely to the extent that any such patent is necessary to Utilize the software alone.
* The patent license shall not apply to any combinations which include this software,
* other than combinations with devices manufactured by or for TI ("TI Devices").
* No hardware patent is licensed hereunder.
*
* Redistributions must preserve existing copyright notices and reproduce this license
* (including the above copyright notice and the disclaimer and (if applicable) source
* code license limitations below) in the documentation and/or other materials provided
* with the distribution
*
* Redistribution and use in binary form, without modification, are permitted provided
* that the following conditions are met:
*
* *       No reverse engineering, decompilation, or disassembly of this software is
* permitted with respect to any software provided in binary form.
*
* *       any redistribution and use are licensed by TI for use only with TI Devices.
*
* *       Nothing shall obligate TI to provide you with source code for the software
* licensed and provided to you in object code.
*
* If software source code is provided to you, modification and redistribution of the
* source code are permitted provided that the following conditions are met:
*
* *       any redistribution and use of the source code, including any resulting derivative
* works, are licensed by TI for use only with TI Devices.
*
* *       any redistribution and use of any object code compiled from the source code
* and any resulting derivative works, are licensed by TI for use only with TI Devices.
*
* Neither the name of Texas Instruments Incorporated nor the names of its suppliers
*
* may be used to endorse or promote products derived from this software without
* specific prior written permission.
*
* DISCLAIMER.
*
* THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
* OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
* OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
* IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
* BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
* OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
* OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
* OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/


/**
----------------------------------------------------------------------------
@file    ti_dl.h
@brief     This file defines the ivision interface for TI deep learning libary
@version 0.1 (Oct 2016) : Initial Code [ADK]
@version 0.5 (Jan 2017) : Cleaned up [ADK]
----------------------------------------------------------------------------
*/

#ifndef TIDL_H_
#define TIDL_H_ 1

#include "itidl_ti.h"
#include "tidl_import_quantize.h"

typedef enum
{
  TIDL_ConstDataLayer       = TIDL_UnsupportedLayer+1,
  TIDL_PriorBoxLayer         ,
  TIDL_PermuteLayer          ,
  TIDL_ReshapeLayer          ,
  TIDL_ShapeLayer            ,
  TIDL_SqueezeLayer          ,
  TIDL_TransposeLayer        ,
  TIDL_ClipLayer             ,
  TIDL_MinimumLayer          ,
  TIDL_LeakyReluLayer        ,
  TIDL_IdentityLayer         ,
  TIDL_BatchToSpaceLayer     ,
  TIDL_SpaceToBatchLayer     ,
  TIDL_PackLayer             ,
  TIDL_DequantizeLayer       ,
  TIDL_CastLayer
}eTIDL_PCLayerType;


typedef enum
{
  TIDL_QUANT_STATS_NONE = 0,
  TIDL_QUANT_STATS_FLOAT,
  TIDL_QUANT_STATS_FIXED,
}eTIDL_QuantStatsType;

extern const char * TIDL_LayerString[];
#define TIDL_NUM_MAX_PC_LAYERS (1024)


typedef struct {
  /** Buffer containing Dim values for output tensor */
  int32_t   outDims[TIDL_DIM_MAX];
}sTIDL_ReshapeParams_t;

typedef struct {
  /** Buffer containing Axis values, to be squeezed if 1*/
  int32_t   axis[TIDL_DIM_MAX];
}sTIDL_SqueezeParams_t;

typedef struct {
  float   min;
}sTIDL_MinimumParams_t;

typedef struct {
  float   alpha;
}sTIDL_LeakyReluParams_t;

typedef struct{
  int32_t blockHeight;
  int32_t blockWidth;
  int32_t cropT;
  int32_t cropB;
  int32_t cropL;
  int32_t cropR;
}sTIDL_BatchToSpaceParams_t;

typedef struct{
  int32_t blockHeight;
  int32_t blockWidth;
  int32_t padT;
  int32_t padB;
  int32_t padL;
  int32_t padR;
}sTIDL_SpaceToBatchParams_t;

typedef struct{
  int32_t axis;
  int32_t valuesCount;
}sTIDL_PackParams_t;

typedef struct{
  float scale;
  float zeroPoint;
}sTIDL_DequantizeParams_t;

typedef union {
  sTIDL_ReshapeParams_t      reshapeParams;
  sTIDL_SqueezeParams_t      squeezeParams;
  sTIDL_MinimumParams_t      minimumParams;
  sTIDL_LeakyReluParams_t    leakyReluParams;
  sTIDL_BatchToSpaceParams_t  batchToSpaceParams;
  sTIDL_SpaceToBatchParams_t  spaceToBatchParams;
  sTIDL_PackParams_t  packParams;
  sTIDL_DequantizeParams_t dequantParams;
} sTIDL_LayerPCParams_t;

typedef struct {
    sTIDL_LayerParams_t layerParams;
    sTIDL_LayerPCParams_t layerPCParams;
    sTIDL_ActParams_t    actParams;
    int32_t layerType;
    int32_t numInBufs;
    int32_t numOutBufs;
    int64_t numMacs;
    int8_t  name[TIDL_STRING_SIZE];
    int8_t  inDataNames[TIDL_NUM_IN_BUFS][TIDL_STRING_SIZE];
    int8_t  outDataNames[TIDL_NUM_OUT_BUFS][TIDL_STRING_SIZE];
    int32_t outConsumerCnt[TIDL_NUM_OUT_BUFS];
    int32_t outConsumerLinked[TIDL_NUM_OUT_BUFS];
    sTIDL_DataParams_t inData[TIDL_NUM_IN_BUFS];
    sTIDL_DataParams_t outData[TIDL_NUM_OUT_BUFS];
    sBuffer_t weights;
    sBuffer_t bias;
    /* Buffer to store per channel weight scales */
    sBuffer_t perChannelWeightScale;
    sBuffer_t slope;
    sBuffer_t priorBox;
    sBuffer_t perChannelPadConstTensor;
    int32_t weightsElementSizeInBits;  //kernel weights in bits
    /** Offset selection method for stride. \ref eTIDL_StrideOffsetMethod */
    int32_t strideOffsetMethod;
    sTIDL_QuantParams_t quantParams[TIDL_MAX_QUANT_PARAMS];

}sTIDL_LayerPC_t;

typedef struct {
  int32_t numLayers;
  /** Quantization statistics availability - Updated with tidl_quant_stats_tool */
  /* Refere eTIDL_QuantStatsType for valid values */
  int32_t quantStats;
  sTIDL_LayerPC_t TIDLPCLayers[TIDL_NUM_MAX_PC_LAYERS];
}sTIDL_OrgNetwork_t;


#endif  /* TI_DL_H_ */

/* =========================================================================*/
/*  End of file:  ti_od_cnn.h                                               */
/* =========================================================================*/
