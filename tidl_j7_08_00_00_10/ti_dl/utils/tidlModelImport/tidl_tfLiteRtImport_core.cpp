/*
*
* Copyright (c) {2015 - 2017} Texas Instruments Incorporated
*
* All rights reserved not granted herein.
*
* Limited License.
*
* Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
* license under copyrights and patents it now or hereafter owns or controls to make,
* have made, use, import, offer to sell and sell ("Utilize") this software subject to the
* terms herein.  With respect to the foregoing patent license, such license is granted
* solely to the extent that any such patent is necessary to Utilize the software alone.
* The patent license shall not apply to any combinations which include this software,
* other than combinations with devices manufactured by or for TI ("TI Devices").
* No hardware patent is licensed hereunder.
*
* Redistributions must preserve existing copyright notices and reproduce this license
* (including the above copyright notice and the disclaimer and (if applicable) source
* code license limitations below) in the documentation and/or other materials provided
* with the distribution
*
* Redistribution and use in binary form, without modification, are permitted provided
* that the following conditions are met:
*
* *       No reverse engineering, decompilation, or disassembly of this software is
* permitted with respect to any software provided in binary form.
*
* *       any redistribution and use are licensed by TI for use only with TI Devices.
*
* *       Nothing shall obligate TI to provide you with source code for the software
* licensed and provided to you in object code.
*
* If software source code is provided to you, modification and redistribution of the
* source code are permitted provided that the following conditions are met:
*
* *       any redistribution and use of the source code, including any resulting derivative
* works, are licensed by TI for use only with TI Devices.
*
* *       any redistribution and use of any object code compiled from the source code
* and any resulting derivative works, are licensed by TI for use only with TI Devices.
*
* Neither the name of Texas Instruments Incorporated nor the names of its suppliers
*
* may be used to endorse or promote products derived from this software without
* specific prior written permission.
*
* DISCLAIMER.
*
* THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
* OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
* OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
* IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
* BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
* OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
* OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
* OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

#include <google/protobuf/io/coded_stream.h>
#include <google/protobuf/io/zero_copy_stream_impl.h>
#include <google/protobuf/message.h>
#include <google/protobuf/text_format.h>
using namespace std;
using ::google::protobuf::Message;
#include <fcntl.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <float.h>
#include <cmath>
#include <stdarg.h>

#include "ti_dl.h"
#include "tidl_import_api.h"
#include "schema_generated.h"
#include "flatbuffers/flexbuffers.h"
#include "tidl_import_config.h"

using namespace std;
using namespace tflite;

#include "tidl_import_common.h"
#include "tidl_import_common_model_check.h"
#include "itidl_ti.h"
#include "tidl_meta_arch.pb.h"
#include "itidl_rt.h"

#include "tensorflow/lite/context_util.h"
#include "tensorflow/lite/builtin_ops.h"
#include "tensorflow/lite/kernels/kernel_util.h"
#include "tensorflow/lite/minimal_logging.h"

#include "tidl_tfLiteRtImport.h"

namespace tflite {
namespace tfl_delegate {

static int tidl_tfliteRt_debuglevel = 0;
#define TfliteRtDebugPrint(...)  if (tidl_tfliteRt_debuglevel > 0) \
                                printf("TfliteRtImportDebug: " __VA_ARGS__)

#define MAX_NAME_LEN 16

#define X86_EXEC //PC emulation mode
#define IPC_TIOVX //IPC communication using OpenVX
#define DEBUG
#define MAX_FILE_PATH (512)
#define TIDL_BIT_DEPTH (16)
#define TIDL_MAX_ALG_IN_BUFS    ((int32_t) 32)

// Global data structures for importing
static struct tfliteRtImportState_t {
  int32_t                 dataIndex;
  int32_t                 layerIndex;
  int32_t                 numErrors;
  int32_t                 numUnsupportedLayers;
  int32_t                 numInputDataLayers;
} tfliteRt_import_state;

#ifdef __cplusplus
extern "C"
{
#endif
// Functions
void tflrt_printf(int32_t debugLevel, char * format, ...)
{
  va_list args;
  if (debugLevel >= 1)
  {
    (void)va_start(args, format);
    (void)vprintf(format, args);
    va_end(args);
  }
}


static int32_t TIDL_tfliteRtImportGetNewLayerIndex()
{
  return tfliteRt_import_state.layerIndex++;
}

static int32_t TIDL_tfliteRtImportGetNewDataIndex()
{
  return tfliteRt_import_state.dataIndex++;
}

extern "C"
{

extern std::vector<std::string> diagsInfo;

/** This function read the names of the OD heads from meta arch file and returns them as a vector of strings
 * */
std::vector<std::string> TIDL_readMetaArchInfo(std::string filePath)
{
  int32_t  i, j, k, l;
  tidl_meta_arch::TIDLMetaArch  tidlMetaArch;
  std::vector<std::string> inOdDataNames;

  printf("TIDL Meta PipeLine (Proto) File  : %s  \n", filePath.c_str());
  TIDL_readProtoFromTextFile((const char *)filePath.c_str(), &tidlMetaArch);
  
  printf("%s\n",tidlMetaArch.name().c_str());
  
  for (j = 0; j < tidlMetaArch.tidl_retinanet_size(); j++)
  {
    printf("%s\n",tidlMetaArch.tidl_retinanet(j).name().c_str());
    
    for(k =0; k < tidlMetaArch.tidl_retinanet(j).box_input_size(); k++)
    {
      inOdDataNames.push_back(tidlMetaArch.tidl_retinanet(j).box_input(k).c_str());
    }
    
    for(k =0; k < tidlMetaArch.tidl_retinanet(j).class_input_size(); k++)
    {
      inOdDataNames.push_back(tidlMetaArch.tidl_retinanet(j).class_input(k).c_str());
    }
    //Check
    //if(tidlMetaArch.tidl_retinanet(j).output_size() != graphOutputSize)
    //{
    //  printf("\nERROR : Number of output names provided in prototxt - %d - do not match actual number of outputs of OD network - %d \n\n", tidlMetaArch.tidl_retinanet(j).output_size(), graphOutputSize);
    //  exit(-1);
    //}
  }
  return inOdDataNames;
}

/* This function finds graph indices corresponding to names of OD heads */ 
std::vector<int> TIDL_getPostProcInputIds(TfLiteContext* context, TfLiteIntArray* plan, std::vector<std::string> odPostProcIndataNames)
{
  std::vector<int> odPostProcIndataIds = {};
  TfLiteNode* node;
  TfLiteRegistration* registration;
  for(int i = 0; i < odPostProcIndataNames.size(); i++)
  {
    for (int node_index : TfLiteIntArrayView(plan)) 
    {
      //TF_LITE_ENSURE_STATUS(context->GetNodeAndRegistration(context, node_index, &node, &registration));
      context->GetNodeAndRegistration(context, node_index, &node, &registration);
      for (auto tensor_index : TfLiteIntArrayView(node->outputs))
      {
        if (tensor_index == kTfLiteOptionalTensor)
        {
            continue;
        }
        TfLiteTensor *tensor = &context->tensors[tensor_index];

        if((strcmp(odPostProcIndataNames[i].c_str(), tensor->name)) == 0)
        {
          odPostProcIndataIds.push_back(node_index);
        }
      }
    }
  }
  return odPostProcIndataIds;
}

/** Create adjacency list of outputs of all nodes in graph
 * Returns list of form < (output node x1, output node y1...), (output node x2, output node y2...) > for nodes < node 1, node 2....>
 * */
std::vector<std::vector<int>> TIDL_createOutputAdjacencyList(TfLiteContext* context, TfLiteIntArray* plan)
{
  std::vector<int> outputAdjacentNodes = {};
  std::vector<std::vector<int>> adjacencyList;

  TfLiteNode* node1, *node2;
  TfLiteRegistration* registration1, *registration2;

  for (int node_index1 : TfLiteIntArrayView(plan))
  {
    outputAdjacentNodes.clear();
    context->GetNodeAndRegistration(context, node_index1, &node1, &registration1);
    for (auto tensor_index_outputs : TfLiteIntArrayView(node1->outputs))
    {
      TfLiteTensor *output_tensor = &context->tensors[tensor_index_outputs];
      for (int node_index2 : TfLiteIntArrayView(plan))
      {
        context->GetNodeAndRegistration(context, node_index2, &node2, &registration2);
        for (auto tensor_index_inputs : TfLiteIntArrayView(node2->inputs))
        {
          TfLiteTensor *input_tensor = &context->tensors[tensor_index_inputs];
          if(strcmp(input_tensor->name, output_tensor->name) == 0)
          {
            outputAdjacentNodes.push_back(node_index2);
          }
        }
      }
    }
    adjacencyList.push_back(outputAdjacentNodes);
  }
  return adjacencyList;
}

/** Create adjacency list of inputs of all nodes in graph
 * Returns list of form < (input node x1, input node y1...), (input node x2, input node y2...) > for nodes < node 1, node 2....>
 * */
std::vector<std::vector<int>> TIDL_createInputAdjacencyList(TfLiteContext* context, TfLiteIntArray* plan)
{
  std::vector<int> inputAdjacentNodes = {};
  std::vector<std::vector<int>> adjacencyList;

  TfLiteNode* node1, *node2;
  TfLiteRegistration* registration1, *registration2;

  for (int node_index1 : TfLiteIntArrayView(plan))
  {
    inputAdjacentNodes.clear();
    context->GetNodeAndRegistration(context, node_index1, &node1, &registration1);
    for (auto tensor_index_inputs : TfLiteIntArrayView(node1->inputs))
    {
      TfLiteTensor *input_tensor = &context->tensors[tensor_index_inputs];
      for (int node_index2 : TfLiteIntArrayView(plan))
      {
        context->GetNodeAndRegistration(context, node_index2, &node2, &registration2);
        for (auto tensor_index_outputs : TfLiteIntArrayView(node2->outputs))
        {
          TfLiteTensor *output_tensor = &context->tensors[tensor_index_outputs];
          if(strcmp(input_tensor->name, output_tensor->name) == 0)
          {
            inputAdjacentNodes.push_back(node_index2);
          }
        }
      }
    }
    adjacencyList.push_back(inputAdjacentNodes);
  }
  return adjacencyList;
}

}  //extern C


void TIDL_tfLiteRtFillActParams(sTIDL_ActParams_t & actParams, int32_t tfLiteActType)
{
  actParams.actType = TIDL_NoAct;
  if (tfLiteActType == kTfLiteActRelu)
  {
    actParams.actType = TIDL_RelU;
  }
  if (tfLiteActType == kTfLiteActRelu6)
  {
    actParams.actType = TIDL_RelU6;
  }
  if(tfLiteActType == kTfLiteActSigmoid)
  {
    actParams.actType = TIDL_Sigmoid;
  }
}

int32_t TIDL_tfliteRtGetTypeAndPtr(TfLiteTensor *tensor, int32_t * type, void ** ptr)
{
  int32_t status = 0;
  if(tensor->type == kTfLiteUInt8)
  {
    *type =  TIDL_UnsignedChar;
    *ptr = (tensor->data.uint8);
  }
  else if(tensor->type == kTfLiteInt8)
  {
    *type =  TIDL_SignedChar;
    *ptr = (tensor->data.int8);
  }
  else if(tensor->type == kTfLiteInt16)
  {
    *type =  TIDL_SignedShort;
    *ptr = (tensor->data.i16);
  }
  else if(tensor->type == kTfLiteFloat32)
  {
    *type =  TIDL_SinglePrecFloat;
    *ptr = (tensor->data.f);
  }
  else if(tensor->type == kTfLiteInt32)
  {
    *type =  TIDLRT_Int32;
    *ptr = (tensor->data.i32);
  }
  else if(tensor->type == kTfLiteInt64)
  {
    *type =  TIDLRT_Int64;
    *ptr = (tensor->data.i64);
  }
  else
  {
    printf("Tflite data type not supported by TIDL");
    status = -1;
  }
  return status;
}

void TIDL_tfliteRtGetScaleAndZeroPoint(TfLiteTensor *tensor , float * scale, int32_t * zp)
{
    if ((tensor->type == kTfLiteUInt8) || (tensor->type == kTfLiteInt8))
    {
        TfLiteAffineQuantization *quantization = (TfLiteAffineQuantization *)tensor->quantization.params;
        *zp = quantization->zero_point->data[0];
        *scale = 1 / quantization->scale->data[0];
    }
    else 
    {
      *zp    = 0;
      *scale = 1.0;
    }
}
#ifdef __cplusplus
}
#endif
uint32_t TIDL_tfliteRtKernelReshape(float * param, uint32_t w, uint32_t h, uint32_t ci, uint32_t co)
{
  float * tPtr = (float * )my_malloc(w*h*ci*co*sizeof(float));
  int32_t counter = 0;
  for(int l1 = 0; l1 < co; ++l1){
    for(int l = 0; l < ci; ++l){
      int k = l;
      for(int j = 1; j<=w*h; ++j){
        tPtr[counter] = param[l1*w*h*ci + k];
        k+=ci;
        counter++;
      }
    }
  }
  memcpy(param,tPtr,w*h*ci*co*sizeof(float));
  free(tPtr);
  return 0;
}

void TIDL_tfliteRtDequantTensor(sBuffer_t &buf, sBuffer_t &scaleBuf, sBuffer_t &zpBuf, int32_t size)
{
  int   * src      = (int *)buf.ptr;
  float * dst      = (float *)buf.ptr;
  float * qscale   = (float *)scaleBuf.ptr;
  int * qzero_point = (int *)zpBuf.ptr;

  if((qscale != NULL) && (qzero_point != NULL))
  {
    int co = buf.bufSize / size;
    int numScales = scaleBuf.bufSize;
    for (int i = 0; i < co; i++)
    {
      for (int j = 0; j < size; j++)
      {
        int param = src[i*size+j] - qzero_point[i % numScales];
        dst[i*size + j] = param * qscale[i % numScales];
        if (param == qzero_point[i % numScales])
        {
          dst[i*size + j] = 0;
        }
      }
    }
    my_free(qscale);
    my_free(qzero_point);
  }
}

int32_t TIDL_tfliteRtCopyInputConstTensor(const TfLiteRegistration* registration, const TfLiteNode* node, TfLiteContext* context, 
                                            int32_t inIdx, sBuffer_t &buf, sBuffer_t &scaleBuf, sBuffer_t &zpBuf)
{
  
  const TfLiteTensor* input;
  input = &context->tensors[node->inputs->data[inIdx]];
  
  auto * ptr = reinterpret_cast<float *>(input->data.raw);
  
  if(input->type == kTfLiteFloat32)
  {
    buf.bufSize = input->bytes / sizeof(float);
    buf.ptr = (float *)my_malloc(buf.bufSize*sizeof(float));
    memcpy(buf.ptr, ptr, buf.bufSize*sizeof(float));
    scaleBuf.ptr = NULL;
    zpBuf.ptr = NULL;
    return 0;
  }
  else if (input->type == kTfLiteInt32)
  {
    buf.bufSize = input->bytes / sizeof(int);
    buf.ptr = (float *)my_malloc(buf.bufSize*sizeof(float));
    memcpy(buf.ptr, ptr, buf.bufSize*sizeof(int));

    const auto* quantization =
        reinterpret_cast<TfLiteAffineQuantization*>(input->quantization.params);
    auto *scale = quantization->scale;

    if (scale)
    {
      scaleBuf.bufSize = scale->size;
      scaleBuf.ptr = (float *)my_malloc(scaleBuf.bufSize*sizeof(float));
      zpBuf.bufSize = scaleBuf.bufSize;
      zpBuf.ptr = (int *)my_malloc(zpBuf.bufSize * sizeof(int));

      float * qscale        = (float *)scaleBuf.ptr;
      int32_t * qzero_point = (int32_t*)zpBuf.ptr;
      for (int i = 0; i < scaleBuf.bufSize; i++)
      {
        qscale[i] = scale->data[i];
        qzero_point[i] = 0;
      }
    }
    return 0;
  }
  else if ((input->type == kTfLiteUInt8) || (input->type == kTfLiteInt8))
  {
    buf.bufSize = input->bytes;
    buf.ptr = (float *)my_malloc(buf.bufSize*sizeof(float));

    int * dst = (int *)buf.ptr;
    if(input->type == kTfLiteUInt8)
    {
      uint8_t * src = (uint8_t *)ptr;
      for (int i = 0; i < buf.bufSize; i++)
      {
        dst[i] = src[i];
      }
    }
    else // (input->type == kTfLiteInt8) 
    {
      int8_t * src = (int8_t *)ptr;
      for (int i = 0; i < buf.bufSize; i++)
      {
        dst[i] = src[i];
      }
    }

    const auto* quantization =
        reinterpret_cast<TfLiteAffineQuantization*>(input->quantization.params);
    auto *scale = quantization->scale;
    auto *zero_point = quantization->zero_point;

    if (scale && zero_point)
    {
      if(scale->size != zero_point->size)
      {
        printf(" Size of scale vector and zero_point shall match. It is not matching for Tensor %s \n", input->name);
        exit(-1);
      }
      scaleBuf.bufSize = scale->size;
      scaleBuf.ptr = (float *)my_malloc(scaleBuf.bufSize*sizeof(float));
      zpBuf.bufSize = zero_point->size;
      zpBuf.ptr = (int *)my_malloc(zpBuf.bufSize*sizeof(int));

      float * qscale        = (float *)scaleBuf.ptr;
      int32_t * qzero_point = (int32_t*)zpBuf.ptr;
      for (int i = 0; i < scaleBuf.bufSize; i++)
      {
        qscale[i] = scale->data[i];
        qzero_point[i] = zero_point->data[i];
      }
    }
    else
    {
        printf(" scale vector or zero_point Not found for Tensor %s \n", input->name);
        exit(-1);
    }
  }
  else
  {
    printf("\nOnly float, DT_INT32 and DT_UNT8 tensor is suported \n");
    return -1;
  }
}

bool isRtTensorVariable(TfLiteContext* context, const TfLiteNode* node)
{
  // Check all inputs, if any of the inputs is not kTfLiteArenaRw (not outputs from previous layer), return false
  TfLiteTensor * tensor;
  bool flag = true;
  for(int i = 0; i < node->inputs->size; i++)
  {
    tensor = &context->tensors[node->inputs->data[i]];
    if(tensor->allocation_type != kTfLiteArenaRw)
    {
      flag = false;
    }
  }
  return flag;
}

int32_t TIDL_tfliteRtMapConvParams(const TfLiteRegistration* registration, const TfLiteNode* node, TfLiteContext* context, sTIDL_LayerPC_t &layer)
{ 
  int32_t status;
  int32_t padType;

  sTIDL_ConvParams_t &convParams      = layer.layerParams.convParams;

  layer.layerType = TIDL_ConvolutionLayer;
  layer.outData[0].elementType = tidl_getElementType(1);

  TfLiteTensor* output;
  output = &context->tensors[node->outputs->data[0]];
  const TfLiteTensor* input;
  input = &context->tensors[node->inputs->data[0]];
  const TfLiteTensor* filter;
  filter = &context->tensors[node->inputs->data[1]];

  // OHWI layout for tflite conv filter
  convParams.numInChannels   = filter->dims->data[3];
  convParams.numOutChannels  = filter->dims->data[0];
  convParams.kernelW         = filter->dims->data[2];
  convParams.kernelH         = filter->dims->data[1];

  convParams.numGroups       = 1;
  convParams.dilationW       = 1;
  convParams.dilationH       = 1;
  convParams.strideW         = 1;
  convParams.strideH         = 1;
  convParams.padW            = 0;
  convParams.padH            = 0;
  convParams.enableBias      = 0;
  convParams.enablePooling   = 0;

  const TfLiteConvParams* conv_params =
          reinterpret_cast<const TfLiteConvParams*>(node->builtin_data);

  convParams.strideW = conv_params->stride_width;
  convParams.strideH = conv_params->stride_height;
  convParams.dilationW = conv_params->dilation_width_factor;
  convParams.dilationH = conv_params->dilation_height_factor;

  TIDL_tfLiteRtFillActParams(layer.actParams,  conv_params->activation);

  padType = conv_params->padding;
  layer.strideOffsetMethod = TIDL_StrideOffsetCenter;
  if (padType == kTfLitePaddingSame)   // SAME : Padding done to input 
  {
    convParams.padW = ((convParams.kernelW - 1)*convParams.dilationW) / 2;
    convParams.padH = ((convParams.kernelH - 1)*convParams.dilationH) / 2;
  }
  else if (padType == kTfLitePaddingValid)  // VALID : No padding to input 
  {
    layer.strideOffsetMethod = TIDL_StrideOffsetTopLeft;
  }
  
  sBuffer_t scale;
  sBuffer_t zero_point;
  TIDL_tfliteRtCopyInputConstTensor(registration, node, context, 1, layer.weights, scale, zero_point);
  TIDL_tfliteRtKernelReshape((float *)layer.weights.ptr, convParams.kernelW, convParams.kernelH, convParams.numInChannels, convParams.numOutChannels);
  TIDL_tfliteRtDequantTensor(layer.weights, scale, zero_point, convParams.kernelW * convParams.kernelH * convParams.numInChannels);
  if (node->inputs->size == 3)
  {
    TIDL_tfliteRtCopyInputConstTensor(registration, node, context, 2, layer.bias, scale, zero_point);
    TIDL_tfliteRtDequantTensor(layer.bias, scale, zero_point, 1);
    convParams.enableBias = 1;
  }
  return 0;
}

int32_t TIDL_tfliteRtMapDWConvParams(const TfLiteRegistration* registration, const TfLiteNode* node, TfLiteContext* context, sTIDL_LayerPC_t &layer)
{
  int32_t status;
  int32_t padType;
  sTIDL_ConvParams_t &convParams      = layer.layerParams.convParams;
  int32_t depth_multiplier;
  
  layer.layerType = TIDL_ConvolutionLayer;
  layer.outData[0].elementType = tidl_getElementType(1);

  TfLiteTensor* output;
  output = &context->tensors[node->outputs->data[0]];
  const TfLiteTensor* input;
  input = &context->tensors[node->inputs->data[0]];
  const TfLiteTensor* filter;
  filter = &context->tensors[node->inputs->data[1]];
  
  // OHWI layout for tflite conv filter
  convParams.numOutChannels  = output->dims->data[3];
  convParams.kernelW         = filter->dims->data[2];
  convParams.kernelH         = filter->dims->data[1];

  convParams.numGroups       = 1;
  convParams.dilationW       = 1;
  convParams.dilationH       = 1;
  convParams.strideW         = 1;
  convParams.strideH         = 1;
  convParams.padW            = 0;
  convParams.padH            = 0;
  convParams.enableBias      = 0;
  convParams.enablePooling   = 0;
  
  const TfLiteDepthwiseConvParams* DWConv2DParams =
          reinterpret_cast<const TfLiteDepthwiseConvParams*>(node->builtin_data);
  
  convParams.strideW = DWConv2DParams->stride_width;
  convParams.strideH = DWConv2DParams->stride_height;
  convParams.dilationW = DWConv2DParams->dilation_width_factor;
  convParams.dilationH = DWConv2DParams->dilation_height_factor;
  depth_multiplier = DWConv2DParams->depth_multiplier;
  TIDL_tfLiteRtFillActParams(layer.actParams,  DWConv2DParams->activation);

  padType = DWConv2DParams->padding;
  layer.strideOffsetMethod = TIDL_StrideOffsetCenter;
  if (padType == kTfLitePaddingSame)   /* SAME : Padding done to input */
  {
    convParams.padW = ((convParams.kernelW - 1)*convParams.dilationW) / 2;
    convParams.padH = ((convParams.kernelH - 1)*convParams.dilationH) / 2;
  }
  else if (padType == kTfLitePaddingValid)  /* VALID : No padding to input */
  {
    layer.strideOffsetMethod = TIDL_StrideOffsetTopLeft;
  }

  convParams.numInChannels = (convParams.numOutChannels / depth_multiplier);
  convParams.numGroups      = convParams.numInChannels;

  sBuffer_t scale;
  sBuffer_t zero_point;

  TIDL_tfliteRtCopyInputConstTensor(registration, node, context, 1, layer.weights, scale, zero_point);
  TIDL_tfliteRtKernelReshape((float *)layer.weights.ptr, convParams.kernelW, convParams.kernelH,
   convParams.numOutChannels, convParams.numInChannels/ convParams.numGroups );
  TIDL_tfliteRtDequantTensor(layer.weights, scale, zero_point, convParams.kernelW * convParams.kernelH * (convParams.numInChannels/convParams.numGroups));

  if(node->inputs->size == 3)
  {
    TIDL_tfliteRtCopyInputConstTensor(registration, node, context, 2, layer.bias, scale, zero_point);
    TIDL_tfliteRtDequantTensor(layer.bias, scale, zero_point, 1);
    convParams.enableBias = 1;
  }
  return 0;
}

int32_t TIDL_tfliteRtMapMaxPoolParams(const TfLiteRegistration* registration, const TfLiteNode* node, TfLiteContext* context, sTIDL_LayerPC_t &layer)
{
  int32_t status;
  int32_t padType;
  
  sTIDL_PoolingParams_t &poolParams = layer.layerParams.poolParams;

  layer.layerType = TIDL_PoolingLayer;
  poolParams.poolingType = TIDL_MaxPooling;

  poolParams.strideW = 1;
  poolParams.strideH = 1;
  poolParams.padW = 0;
  poolParams.padH = 0;
  
  const TfLitePoolParams* Pool2DParams =
          reinterpret_cast<const TfLitePoolParams*>(node->builtin_data);
  poolParams.strideW = Pool2DParams->stride_width;
  poolParams.strideH = Pool2DParams->stride_height;
  poolParams.kernelW = Pool2DParams->filter_width;
  poolParams.kernelH = Pool2DParams->filter_height;

  padType = Pool2DParams->padding;
  if (padType == kTfLitePaddingSame)
  {
    poolParams.padW = ((poolParams.kernelW - 1)) / 2;
    poolParams.padH = ((poolParams.kernelH - 1)) / 2;
  }

  return 0;
}

int32_t TIDL_tfliteRtMapAvgPoolParams(const TfLiteRegistration* registration, const TfLiteNode* node, TfLiteContext* context, sTIDL_LayerPC_t &layer)
{ 
  layer.layerType = TIDL_PoolingLayer;
  sTIDL_PoolingParams_t &poolParams = layer.layerParams.poolParams;
  poolParams.useCeil = 0;  // TODO : check the useCeil values for tflite
  int32_t status = 0;
  char poolingType[256] = "";
  TfLiteTensor* tensor;

  // Convert avgpool2d to global pooling if the kernel size matches the HxW dimensions.
  // This happens in TIDL_tfOutReshapePoolingLayer but we need it prior to calling the model
  // checker for allow/deny.
  if (registration->builtin_code == kTfLiteBuiltinAveragePool2d)
  {
    const TfLitePoolParams* Pool2DParams =
          reinterpret_cast<const TfLitePoolParams*>(node->builtin_data);
    uint32_t kernelW = Pool2DParams->filter_width;
    uint32_t kernelH = Pool2DParams->filter_height;
    tensor = &context->tensors[node->inputs->data[0]];
    int  dims = tensor->dims->size;
    if (dims == 4)
    { //NHWC format
      uint32_t inH = tensor->dims->data[1];
      uint32_t inW = tensor->dims->data[2];
      if (kernelH == inH && kernelW == inW)
      {
        strcpy(poolingType,"GlobalAveragePool");
      }
    }
  }

  if (strcmp(poolingType, "GlobalAveragePool") == 0)
  {
    poolParams.poolingType = TIDL_AveragePooling;
    // Pool size is set to input layer size
    poolParams.kernelH = 0;
    poolParams.kernelW = 0;
    poolParams.padH    = 0;
    poolParams.padW    = 0;
    poolParams.strideH = 1;
    poolParams.strideW = 1;
  }
  else
  {
    TIDL_tfliteRtMapMaxPoolParams(registration, node, context, layer);
    poolParams.poolingType = TIDL_AveragePooling;
    // Convert 1x1 average pooling into max pooling (operations are identical)
    // mxnet_resnet50_v1d
    if (poolParams.kernelH == 1 && poolParams.kernelW == 1 &&
       ((poolParams.strideH == 2 && poolParams.strideW == 2) || 
        (poolParams.strideH == 1 && poolParams.strideW == 1)))
    {
      poolParams.poolingType = TIDL_MaxPooling;
    }
  }
  return 0;
}

int32_t TIDL_tfliteRtMapSoftmaxParams(const TfLiteRegistration* registration, const TfLiteNode* node, TfLiteContext* context, sTIDL_LayerPC_t &layer)
{
  layer.layerType = TIDL_SoftMaxLayer;
  
  TfLiteTensor* tensor;
  int32_t numDims;

  for (int i = 0; i < node->inputs->size; i++)
  {
    tensor = &context->tensors[node->inputs->data[i]];
    if(tensor->allocation_type == kTfLiteArenaRw)
    {
      numDims = tensor->dims->size;
      // NHWC input data format
      layer.inData[0].dimValues[TIDL_DIM_BATCH]  = 1;
      layer.inData[0].dimValues[TIDL_DIM_NUMCH]  = (numDims > 3) ? tensor->dims->data[numDims - 3] : 1;
      layer.inData[0].dimValues[TIDL_DIM_HEIGHT] = (numDims > 2) ? tensor->dims->data[numDims - 2] : 1;
      layer.inData[0].dimValues[TIDL_DIM_WIDTH]  = tensor->dims->data[numDims - 1];
    }
  }
  return 0;
}

int32_t TIDL_tfliteRtMapAddParams(const TfLiteRegistration* registration, const TfLiteNode* node, TfLiteContext* context, sTIDL_LayerPC_t &layer)
{
  const TfLiteAddParams* AddParams =
          reinterpret_cast<const TfLiteAddParams*>(node->builtin_data);
  if(isRtTensorVariable(context, node))
  {
    sTIDL_EltWiseParams_t &addParams = layer.layerParams.eltWiseParams;
    layer.layerType = TIDL_EltWiseLayer;
    layer.outData[0].elementType = tidl_getElementType(1);
    addParams.eltWiseType = TIDL_EltWiseSum;
    layer.numInBufs = node->inputs->size;
    layer.layerParams.eltWiseParams.numInData = node->inputs->size;
    TIDL_tfLiteRtFillActParams(layer.actParams,  AddParams->activation);
    return 0;
  }
  else
  {
    sTIDL_BatchNormParams_t &addParams = layer.layerParams.batchNormParams;
    layer.layerType = TIDL_BatchNormLayer;
    layer.outData[0].elementType = tidl_getElementType(1);
    TIDL_tfLiteRtFillActParams(layer.actParams,  AddParams->activation);
    
    TfLiteTensor* tensor = &context->tensors[node->inputs->data[1]];
    int32_t dataSize = (tensor->bytes)/sizeof(float);
    layer.weights.ptr     = my_malloc(dataSize*sizeof(float));
    layer.weights.bufSize = dataSize;
    layer.bias.ptr        = my_malloc(dataSize*sizeof(float));
    layer.bias.bufSize    = dataSize;
    float* ptr = (float*)malloc(dataSize*sizeof(float));
    for(int lc = 0; lc < dataSize; ++lc)
      ptr[lc] = 1;
    memcpy((void*)layer.weights.ptr, ptr, dataSize*sizeof(float));
    sBuffer_t scale;
    sBuffer_t zero_point;
    TIDL_tfliteRtCopyInputConstTensor(registration, node, context, 1, layer.bias, scale, zero_point);
    TIDL_tfliteRtDequantTensor(layer.bias, scale, zero_point, 1);
   return 0;
  }
}

int32_t TIDL_tfliteRtMapMulParams(const TfLiteRegistration* registration, const TfLiteNode* node, TfLiteContext* context, sTIDL_LayerPC_t &layer)
{
  const TfLiteMulParams* TfliteMulParams =
          reinterpret_cast<const TfLiteMulParams*>(node->builtin_data);
  
  if(isRtTensorVariable(context, node))
  {
    sTIDL_EltWiseParams_t &mulParams = layer.layerParams.eltWiseParams;
    layer.layerType = TIDL_EltWiseLayer;
    layer.outData[0].elementType = tidl_getElementType(1);
    mulParams.eltWiseType = TIDL_EltWiseProduct;
    layer.numInBufs = node->inputs->size;
    layer.layerParams.eltWiseParams.numInData = node->inputs->size;
    TIDL_tfLiteRtFillActParams(layer.actParams, TfliteMulParams->activation);
    return 0;
  }
  else
  {
    sTIDL_BatchNormParams_t &mulParams = layer.layerParams.batchNormParams;
    layer.layerType = TIDL_BatchNormLayer;
    layer.outData[0].elementType = tidl_getElementType(1);
    TIDL_tfLiteRtFillActParams(layer.actParams, TfliteMulParams->activation);
    
    TfLiteTensor* weights = &context->tensors[node->inputs->data[1]];
    int32_t weightsSize = (weights->bytes)/sizeof(float);
    //TfLiteTensor* input = &context->tensors[node->inputs->data[0]];
    //int32_t numInChannels = input->dims->data[3];
    
    int32_t dataSize = weightsSize;
    sBuffer_t scale;
    sBuffer_t zero_point;
    TIDL_tfliteRtCopyInputConstTensor(registration, node, context, 1, layer.weights, scale, zero_point);
    TIDL_tfliteRtDequantTensor(layer.weights, scale, zero_point, 1);
    
    /*** TODO : Take care of the case with constant multiplication - need to broadcast weights and bias to the size of numInChannels */

    /*if(weightsSize != numInChannels)
    {
      dataSize = numInChannels;
      auto * wtPtrData = reinterpret_cast<float *>(weights->data.raw);
      layer.weights.bufSize = dataSize;
      layer.weights.ptr = (float *)my_malloc(dataSize*sizeof(float));
      float * wtPtr = (float*)malloc(dataSize*sizeof(float));
      for(int i = 0; i < dataSize; i++)
      {
        wtPtr[i] = wtPtrData[0]; 
      }
      memcpy(layer.weights.ptr, wtPtr, layer.weights.bufSize*sizeof(float));
      scale.ptr = NULL;
      zero_point.ptr = NULL;
    }
    else
    {
      dataSize = weightsSize;
    }*/

    layer.bias.ptr        = my_malloc(dataSize*sizeof(float));
    layer.bias.bufSize    = dataSize;
    float* ptr = (float*)malloc(dataSize*sizeof(float));
    for(int lc = 0; lc < dataSize; ++lc)
      ptr[lc] = 0;
    memcpy((void*)layer.bias.ptr, ptr, dataSize*sizeof(float));
    return 0;
  }
}

int32_t TIDL_tfliteRtMapDivParams(const TfLiteRegistration* registration, const TfLiteNode* node, TfLiteContext* context, sTIDL_LayerPC_t &layer)
{
  const TfLiteDivParams* TfliteDivParams =
          reinterpret_cast<const TfLiteDivParams*>(node->builtin_data);
  
  sTIDL_BatchNormParams_t &mulParams = layer.layerParams.batchNormParams;
  layer.layerType = TIDL_BatchNormLayer;
  layer.outData[0].elementType = tidl_getElementType(1);
  TIDL_tfLiteRtFillActParams(layer.actParams, TfliteDivParams->activation);
  
  TfLiteTensor* weights = &context->tensors[node->inputs->data[1]];
  int32_t weightsSize = (weights->bytes)/sizeof(float);
  TfLiteTensor* input = &context->tensors[node->inputs->data[0]];
  int32_t numInChannels = input->dims->data[3];
  
  int32_t dataSize = weightsSize;
  sBuffer_t scale;
  sBuffer_t zero_point;
  
  if(weightsSize != numInChannels)
  {
    dataSize = numInChannels;
    auto * wtPtrData = reinterpret_cast<float *>(weights->data.raw);
    layer.weights.bufSize = dataSize;
    layer.weights.ptr = (float *)my_malloc(dataSize*sizeof(float));
    float * wtPtr = (float*)malloc(dataSize*sizeof(float));
    for(int i = 0; i < dataSize; i++)
    {
      wtPtr[i] = 1.0 / wtPtrData[0]; 
    }
    memcpy(layer.weights.ptr, wtPtr, layer.weights.bufSize*sizeof(float));
    scale.ptr = NULL;
    zero_point.ptr = NULL;
  }
  else
  {
    dataSize = weightsSize;
  }

  layer.bias.ptr        = my_malloc(dataSize*sizeof(float));
  layer.bias.bufSize    = dataSize;
  float* ptr = (float*)malloc(dataSize*sizeof(float));
  for(int lc = 0; lc < dataSize; ++lc)
    ptr[lc] = 0;
  memcpy((void*)layer.bias.ptr, ptr, dataSize*sizeof(float));

  return 0;
}

int32_t TIDL_tfliteRtMapConcatParams(const TfLiteRegistration* registration, const TfLiteNode* node, TfLiteContext* context, sTIDL_LayerPC_t &layer)
{
  layer.layerType = TIDL_ConcatLayer;
  layer.numInBufs = node->inputs->size;

  sTIDL_ConcatParams_t &concatParams = layer.layerParams.concatParams;
  
  const TfLiteConcatenationParams* concat_params =
          reinterpret_cast<const TfLiteConcatenationParams*>(node->builtin_data);

  int32_t dims = context->tensors[node->inputs->data[0]].dims->size;

  if (dims != TIDL_DIM_MAX) 
  {
    //printf("Concat input dims %d  layer.numInBufs %d \n", dims, layer.numInBufs);
    printf("Warning : concat requires 4D input tensors - only %d dims present..  Ignore if object detection network\n", dims);
    return -1;
  }

  int32_t axis = concat_params->axis;
  if (axis < 0)
    axis += dims;
  // NHWC --> NCHW
  axis = axis == 1 ? 2 :   // H
        axis == 2 ? 3 :   // W
        axis == 3 ? 1 :   // C
        axis;

  concatParams.axis = axis;

  return 0;
}

int32_t TIDL_tfliteRtMapReshapeParams(const TfLiteRegistration* registration, const TfLiteNode* node, TfLiteContext* context, sTIDL_LayerPC_t &layer)
{
  TfLiteTensor * tensor = &context->tensors[node->inputs->data[1]];
  int32_t numDims = tensor->dims->data[0];
  layer.weights.ptr = my_malloc(numDims*sizeof(int32_t));
  layer.weights.bufSize = numDims;
  int32_t * shape = (int32_t *)layer.weights.ptr;
  int32_t * reshapeDims = (int32_t *)tensor->data.i32;
  
  for(int i = 0; i < numDims; i++)
  {
    shape[i] = reshapeDims[i];
  }

  layer.layerType = TIDL_ReshapeLayer;
  return 0;
}

int32_t TIDL_tfliteRtMapMeanParams(const TfLiteRegistration* registration, const TfLiteNode* node, TfLiteContext* context, sTIDL_LayerPC_t &layer)
{
  int32_t status;
  sTIDL_PoolingParams_t &poolParams = layer.layerParams.poolParams;

  layer.layerType = TIDL_PoolingLayer;

  poolParams.poolingType = TIDL_AveragePooling;
  poolParams.strideW = 1;
  poolParams.strideH = 1;
  poolParams.padW = 0;
  poolParams.padH = 0;
  poolParams.kernelW = 0;
  poolParams.kernelH = 0;
  return 0;
}

int32_t TIDL_tfliteRtMapPadParams(const TfLiteRegistration* registration, const TfLiteNode* node, TfLiteContext* context, sTIDL_LayerPC_t &layer)
{
  int32_t idx,j;
  float * ptr;
  
  layer.layerType = TIDL_PadLayer;

  const TfLiteTensor* padInput;
  padInput = &context->tensors[node->inputs->data[1]]; // node->inputs->data[1] gives paddings (array of size [4,2])
  
  
  if(padInput)
  {
    ptr = (float *)malloc(padInput->bytes);
    memcpy(ptr, (uint8_t *) padInput->data.raw, padInput->bytes);
  }

  int32_t padT, padB, padL, padR;
  padT = ((int32_t *)ptr)[2];
  padB = ((int32_t *)ptr)[3];
  padL = ((int32_t *)ptr)[4];
  padR = ((int32_t *)ptr)[5];
  layer.layerParams.padLayerParams.padT = padT;
  layer.layerParams.padLayerParams.padB = padB;
  layer.layerParams.padLayerParams.padL = padL;
  layer.layerParams.padLayerParams.padR = padR;

  my_free(ptr);
  return 0;
}

int32_t TIDL_tfliteRtMapFullyConnectedParams(const TfLiteRegistration* registration, const TfLiteNode* node, TfLiteContext* context, sTIDL_LayerPC_t &layer)
{
  int numVarInputs = 0;
  for(int i = 0; i < node->inputs->size; i++)
  {
    if((context->tensors[node->inputs->data[i]].allocation_type == kTfLiteArenaRw)) //variable input
    {
      numVarInputs++;
    }
  }
  if(numVarInputs > 1)
  {
    return -1;  //Mark unsupported if more than 1 variable inputs
  }
  sTIDL_InnerProductParams_t &innerProductParams = layer.layerParams.innerProductParams;

  layer.layerType = TIDL_InnerProductLayer;
  layer.outData[0].elementType = tidl_getElementType(1);
  
  const TfLiteTensor* input;
  input = &context->tensors[node->inputs->data[0]];
  const TfLiteTensor* filter;
  filter = &context->tensors[node->inputs->data[1]];

  if(! ((input->allocation_type == kTfLiteArenaRw) && (filter->allocation_type != kTfLiteArenaRw)) ) ///input 0 (input) should be variable and input 1 (filter) constant
  {
    return -1;
  }

  innerProductParams.numOutNodes = filter->dims->data[0];
  innerProductParams.numInNodes = filter->dims->data[1];
  
  sBuffer_t scale;
  sBuffer_t zero_point;
  TIDL_tfliteRtCopyInputConstTensor(registration, node, context, 1, layer.weights, scale, zero_point);
  TIDL_tfliteRtDequantTensor(layer.weights, scale, zero_point, 1);
  TIDL_tfliteRtCopyInputConstTensor(registration, node, context, 2, layer.bias, scale, zero_point);
  TIDL_tfliteRtDequantTensor(layer.bias, scale, zero_point, 1);
  
  layer.inData[0].dimValues[TIDL_DIM_BATCH]  = 1;
  layer.inData[0].dimValues[TIDL_DIM_NUMCH]  = 1;
  layer.inData[0].dimValues[TIDL_DIM_HEIGHT] = 1;
  layer.inData[0].dimValues[TIDL_DIM_WIDTH]  = input->dims->data[1];
  layer.outData[0].dimValues[TIDL_DIM_WIDTH] = filter->dims->data[0];
  return 0;
}

int32_t TIDL_tfliteRtMapBiInterPResizeParams(const TfLiteRegistration* registration, const TfLiteNode* node, TfLiteContext* context, sTIDL_LayerPC_t &layer)
{
  sTIDL_ResizeLayerParams_t& params = layer.layerParams.resizeParams;
  layer.layerType = TIDL_ResizeLayer;
  
  const TfLiteTensor* input;
  input = &context->tensors[node->inputs->data[0]];
  const TfLiteTensor* resizeShape;
  resizeShape = &context->tensors[node->inputs->data[1]];
  
  int32_t * newSize = reinterpret_cast<int32_t *>(resizeShape->data.raw);
  int new_height = newSize[0];
  int new_width  = newSize[1];

  
  int orig_height = input->dims->data[1];
  int orig_width  = input->dims->data[2];

  params.resizeRatio[TIDL_DIM_HEIGHT] = (new_height * 1.0) / orig_height;
  params.resizeRatio[TIDL_DIM_WIDTH]  = (new_width  * 1.0) / orig_width;

  layer.layerParams.resizeParams.mode = TIDL_ResizeBilinear;

  return 0;
}

int32_t TIDL_tfliteRtMapNNResizeParams(const TfLiteRegistration* registration, const TfLiteNode* node, TfLiteContext* context, sTIDL_LayerPC_t &layer)
{
  TIDL_tfliteRtMapBiInterPResizeParams(registration, node, context, layer);
  layer.layerParams.resizeParams.mode = TIDL_ResizeNearest;
  return 0;
}

int32_t TIDL_tfliteRtMapArgmaxParams(const TfLiteRegistration* registration, const TfLiteNode* node, TfLiteContext* context, sTIDL_LayerPC_t &layer)
{
  int32_t status = -1;

  const TfLiteTensor* input;
  input = &context->tensors[node->inputs->data[0]];
  const TfLiteTensor* argMaxDim;
  argMaxDim = &context->tensors[node->inputs->data[1]];

  int32_t * argMaxDimData = reinterpret_cast<int32_t *>(argMaxDim->data.raw);

  int32_t dims = input->dims->size;

  int32_t axis = argMaxDimData[0];
  if (axis < 0)
    axis += dims;
  // NHWC --> NCHW
  axis = axis == 1 ? 2 :   // H
        axis == 2 ? 3 :   // W
        axis == 3 ? 1 :   // C
        axis;

  layer.layerType = TIDL_ArgMaxLayer;
  // TIDL supports argmax only on axis 1.
  // Check attributes here because tidlModelCheckLayer() doesn't have access to them
  
  if (axis == TIDL_DIM_NUMCH)
  {
    status = 0;  // No error - this op is supported
  }

  // Return -1 if there are unsupported attributes 

  return status;
}

int32_t TIDL_tfliteRtMapSigmoidParams(const TfLiteRegistration* registration, const TfLiteNode* node, TfLiteContext* context, sTIDL_LayerPC_t &layer)
{
  layer.layerType = TIDL_SigmoidLayer;
  layer.actParams.actType = TIDL_Sigmoid;
  return 0;
}

int32_t TIDL_tfliteRtMapBatchToSpaceParams(const TfLiteRegistration* registration, const TfLiteNode* node, TfLiteContext* context, sTIDL_LayerPC_t &layer)
{
  layer.layerType = TIDL_BatchToSpaceLayer;

  //fill block size
  const TfLiteTensor* tensor_blockSize;
  tensor_blockSize = &context->tensors[node->inputs->data[1]];
  auto * blockSizeData = reinterpret_cast<int32_t *>(tensor_blockSize->data.raw);
  int32_t * ptrBlockSize = (int32_t *)malloc(tensor_blockSize->bytes);
  memcpy(ptrBlockSize, blockSizeData, tensor_blockSize->bytes);
  layer.layerPCParams.batchToSpaceParams.blockHeight = ptrBlockSize[0];
  layer.layerPCParams.batchToSpaceParams.blockWidth = ptrBlockSize[1];
  
  //fill cropping dimensions
  const TfLiteTensor* tensor_crop;
  tensor_crop = &context->tensors[node->inputs->data[2]];
  auto * croppingData = reinterpret_cast<int32_t *>(tensor_crop->data.raw);
  int32_t * ptrCropping = (int32_t *)malloc(tensor_crop->bytes);
  memcpy(ptrCropping, croppingData, tensor_crop->bytes);
  layer.layerPCParams.batchToSpaceParams.cropT = ptrCropping[0];
  layer.layerPCParams.batchToSpaceParams.cropB = ptrCropping[1];
  layer.layerPCParams.batchToSpaceParams.cropL = ptrCropping[2];
  layer.layerPCParams.batchToSpaceParams.cropR = ptrCropping[3];

  free(ptrCropping);
  free(ptrBlockSize);
  return 0;
}

int32_t TIDL_tfliteRtMapSpaceToBatchParams(const TfLiteRegistration* registration, const TfLiteNode* node, TfLiteContext* context, sTIDL_LayerPC_t &layer)
{
  layer.layerType = TIDL_SpaceToBatchLayer;

  // fill block shape
  const TfLiteTensor* tensor_blockSize;
  tensor_blockSize = &context->tensors[node->inputs->data[1]];
  auto * blockSizeData = reinterpret_cast<int32_t *>(tensor_blockSize->data.raw);
  int32_t * ptrBlockSize = (int32_t *)malloc(tensor_blockSize->bytes);
  memcpy(ptrBlockSize, blockSizeData, tensor_blockSize->bytes);
  layer.layerPCParams.spaceToBatchParams.blockHeight = ptrBlockSize[0];
  layer.layerPCParams.spaceToBatchParams.blockWidth = ptrBlockSize[1];

  //fill padding
  const TfLiteTensor* tensor_padding;
  tensor_padding = &context->tensors[node->inputs->data[2]];
  auto * padding = reinterpret_cast<int32_t *>(tensor_padding->data.raw);
  int32_t * ptrPadding = (int32_t *)malloc(tensor_padding->bytes);
  memcpy(ptrPadding, padding, tensor_padding->bytes);
  layer.layerPCParams.spaceToBatchParams.padT = ptrPadding[0];
  layer.layerPCParams.spaceToBatchParams.padB = ptrPadding[1];
  layer.layerPCParams.spaceToBatchParams.padL = ptrPadding[2];
  layer.layerPCParams.spaceToBatchParams.padR = ptrPadding[3];

  free(ptrBlockSize);
  free(ptrPadding);
  return 0;
}

int32_t TIDL_tfliteRtMapSqueezeParams(const TfLiteRegistration* registration, const TfLiteNode* node, TfLiteContext* context, sTIDL_LayerPC_t &layer)
{
  layer.layerType = TIDL_SqueezeLayer;
  const TfLiteSqueezeParams* squeeze_params =
          reinterpret_cast<const TfLiteSqueezeParams*>(node->builtin_data);
  int32_t numDims = squeeze_params->num_squeeze_dims;
  int32_t * squeezeDims = (int32_t *)squeeze_params->squeeze_dims; 
  
  const TfLiteTensor* input;
  input = &context->tensors[node->inputs->data[0]];
  
  for (int32_t  j = 0; j < numDims; j++)
  {
    if((squeezeDims[j] >= 0)  && (squeezeDims[j] < TIDL_DIM_MAX)  && (input->dims->data[squeezeDims[j]] == 1))
    {
      layer.layerPCParams.squeezeParams.axis[squeezeDims[j]] = 1; 
    }
  }
  int32_t c,h,w;
  c = layer.layerPCParams.squeezeParams.axis[3];
  h = layer.layerPCParams.squeezeParams.axis[1];
  w = layer.layerPCParams.squeezeParams.axis[2];
  layer.layerPCParams.squeezeParams.axis[TIDL_DIM_NUMCH]  = c;
  layer.layerPCParams.squeezeParams.axis[TIDL_DIM_HEIGHT] = h;
  layer.layerPCParams.squeezeParams.axis[TIDL_DIM_WIDTH]  = w;
  
  return 0;
}

int32_t TIDL_tfliteRtMapPackParams(const TfLiteRegistration* registration, const TfLiteNode* node, TfLiteContext* context, sTIDL_LayerPC_t &layer)
{
  layer.layerType = TIDL_PackLayer;
  const TfLitePackParams* pack_params =
          reinterpret_cast<const TfLitePackParams*>(node->builtin_data);
  layer.layerPCParams.packParams.axis = pack_params->axis;
  layer.layerPCParams.packParams.valuesCount = pack_params->values_count; 
  return 0;
}

int32_t TIDL_tfliteRtMapCastParams(const TfLiteRegistration* registration, const TfLiteNode* node, TfLiteContext* context, sTIDL_LayerPC_t &layer)
{
  layer.layerType = TIDL_CastLayer;
  return 0;
}

int32_t TIDL_tfliteRtMapDequantizeParams(const TfLiteRegistration* registration, const TfLiteNode* node, TfLiteContext* context, sTIDL_LayerPC_t &layer)
{
  layer.layerType = TIDL_DequantizeLayer;
  auto tensor = &context->tensors[node->inputs->data[0]];
  layer.layerPCParams.dequantParams.scale = 0.1;
  layer.layerPCParams.dequantParams.zeroPoint = 0.0;

  if (((tensor->type == kTfLiteUInt8) || (tensor->type == kTfLiteInt8)) && (gParams.addDataConvertToNet))
  {
    TfLiteAffineQuantization* quantization = (TfLiteAffineQuantization*)tensor->quantization.params;
    if(quantization)
    {
        auto * scale = quantization->scale;
        auto *zero_point = quantization->zero_point;
        if (scale && zero_point)
        {
          layer.layerPCParams.dequantParams.scale     = scale->data[0];
          layer.layerPCParams.dequantParams.zeroPoint = zero_point->data[0];
        }
    }
  }
  return 0;
}


int32_t TIDL_tfliteRtMapDetectionOutParams(const TfLiteRegistration* registration, const TfLiteNode* node, TfLiteContext* context, sTIDL_LayerPC_t &layer,
                                            od_parameters * odUserParams)
{
  layer.layerType = TIDL_DetectionOutputLayer;
  layer.outData[0].elementType = tidl_getElementType(1);
  layer.numInBufs = 2;
  layer.numOutBufs = 1;
  layer.weightsElementSizeInBits = NUM_WHGT_BITS;

  sTIDL_DetectOutputParams_t * doParams = &layer.layerParams.detectOutParams;
  const flexbuffers::Map& m = flexbuffers::GetRoot(reinterpret_cast<const uint8_t*>(node->custom_initial_data), node->custom_initial_data_size).AsMap();
  int32_t max_detections = m["max_detections"].AsInt32();
  int32_t max_classes_per_detection = m["max_classes_per_detection"].AsInt32();
  int32_t detections_per_class;
  if (m["detections_per_class"].IsNull())
    detections_per_class = 100;
  else
    detections_per_class = m["detections_per_class"].AsInt32();
  if(detections_per_class == 0)
  {
    detections_per_class = 100;
  }
  bool use_regular_non_max_suppression;
  if (m["use_regular_nms"].IsNull())
    use_regular_non_max_suppression = false;
  else
    use_regular_non_max_suppression = m["use_regular_nms"].AsBool();

  float nms_score_threshold = m["nms_score_threshold"].AsFloat();
  float iou_threshold = m["nms_iou_threshold"].AsFloat();

  int32_t num_classes = m["num_classes"].AsInt32();
  float y_scale = m["y_scale"].AsFloat();
  float x_scale = m["x_scale"].AsFloat();
  float h_scale = m["h_scale"].AsFloat();
  float w_scale = m["w_scale"].AsFloat();

  if(odUserParams->confidence_threshold != -1) //Not default, provided by user
  {
    nms_score_threshold = odUserParams->confidence_threshold;
  }
  if(odUserParams->nms_threshold != -1)
  {
    iou_threshold = odUserParams->nms_threshold;
  }
  if(odUserParams->top_k != -1)
  {
    detections_per_class = odUserParams->top_k;
  }
  if(odUserParams->keep_top_k != -1)
  {
    if(odUserParams->keep_top_k > max_detections)
    {
      printf("WARNING : keep_top_k cannot be specified to be greater than 'max_detections' parameters in the tflite detection post processing layer. Setting it to max_detections - %d\n", max_detections);
    }
    else
    {
      max_detections = odUserParams->keep_top_k;
    }
  }

  doParams->numClasses        = num_classes;
  doParams->topK              = detections_per_class;
  doParams->keepTopK          = max_detections;
  doParams->nmsThreshold      = iou_threshold;
  doParams->confThreshold     = nms_score_threshold;

  const TfLiteTensor* input_box_encodings;
  input_box_encodings = &context->tensors[node->inputs->data[0]];
  const TfLiteTensor* input_class_predictions;
  input_class_predictions = &context->tensors[node->inputs->data[1]];

  const TfLiteTensor* output_tensor;
  // Save intermediate tensor name in outDataNames[0] and the OD post proc layers names in outDataNames[1,2,3,4]
  for(int j = 0; j < node->outputs->size; j++)
  {
    output_tensor = &context->tensors[node->outputs->data[j]]; 
    strcpy((char *)layer.outDataNames[j+1], output_tensor->name);
  }

  const int num_classes_with_background = input_class_predictions->dims->data[2];
  int label_offset = num_classes_with_background - num_classes;
  doParams->backgroundLabelId = (label_offset == 1) ? 0 : -1; 
  if(doParams->backgroundLabelId == 0)
  {
    doParams->numClasses = doParams->numClasses + 1;
  }
  doParams->codeType          = 2;
  doParams->varianceEncoded   = 0;
  doParams->eta               = 1.0;
  doParams->numKeypoints      = 0;
  doParams->shareLocation     = 1;

  doParams->imWidth  = gParams.inWidth[0];
  doParams->imHeight = gParams.inHeight[0];

  doParams->numHeads     = 6; // Can we hardcode this?
  // Derive scoreConverter from input layer of detection layer - sigmoid or softmax - checked in import_common, set to sigmoid by default
  doParams->scoreConverter = 1;
  doParams->metaArchType = TIDL_metaArchTFSSD;
  doParams->dataLayout = 0;
  
  int32_t numHeads = 6;
  int32_t numOutDataPerObject;
  
  layer.layerParams.detectOutParams.numKeypoints = 0;
  numOutDataPerObject = 7 + layer.layerParams.detectOutParams.numKeypoints * 2;

  layer.outData[0].numDim       = 1;
  layer.outData[0].dimValues[0] = 1;
  layer.outData[0].dimValues[1] = 1;
  layer.outData[0].dimValues[2] = 1;
  layer.outData[0].dimValues[3] = 4 + doParams->keepTopK *numOutDataPerObject;
  layer.outData[0].elementType = TIDL_SinglePrecFloat;

  layer.priorBox.ptr    = (void*)malloc(sizeof(sTIDL_AnchorBoxParams_t)*numHeads);
  layer.priorBox.bufSize = (sizeof(sTIDL_AnchorBoxParams_t)*numHeads)/sizeof(float);

  sTIDL_AnchorBoxParams_t *anchorBoxParams = (sTIDL_AnchorBoxParams_t*)layer.priorBox.ptr;

  const TfLiteTensor* anchor_boxes;
  anchor_boxes = &context->tensors[node->inputs->data[2]];
  
  auto * boxPtr = reinterpret_cast<float *>(anchor_boxes->data.raw);
  float * anchorBoxInput = (float *)malloc(anchor_boxes->bytes);
  memcpy(anchorBoxInput, boxPtr, anchor_boxes->bytes);
  anchorBoxParams->anchorInputs = anchorBoxInput;

  for (int32_t i = 0; i < numHeads; i++)
  {
    anchorBoxParams[i].boxScales[0] = 1.0/y_scale;
    anchorBoxParams[i].boxScales[1] = 1.0/x_scale;
    anchorBoxParams[i].boxScales[2] = 1.0/h_scale;
    anchorBoxParams[i].boxScales[3] = 1.0/w_scale;
  }
}

// Convert a tflite operator to a TIDL layer
static int32_t TIDL_tfliteRtMapNode(const TfLiteRegistration* registration, const TfLiteNode* node, TfLiteContext* context, sTIDL_LayerPC_t &layer, 
                                    od_parameters * odUserParams)
{
  int32_t status = 0;
  TfLiteTensor * tensor;
  /*** TODO : Add layer mapping using table as in tfliteImport ***/

  if(registration->builtin_code == kTfLiteBuiltinConv2d)
    status = TIDL_tfliteRtMapConvParams(registration, node, context, layer);
  else if(registration->builtin_code == kTfLiteBuiltinDepthwiseConv2d)
    status = TIDL_tfliteRtMapDWConvParams(registration, node, context, layer);
  else if(registration->builtin_code == kTfLiteBuiltinAveragePool2d)
    status = TIDL_tfliteRtMapAvgPoolParams(registration, node, context, layer);
  else if(registration->builtin_code == kTfLiteBuiltinMaxPool2d)
    status = TIDL_tfliteRtMapMaxPoolParams(registration, node, context, layer);
  else if(registration->builtin_code == kTfLiteBuiltinReshape)
    status = TIDL_tfliteRtMapReshapeParams(registration, node, context, layer);
  else if(registration->builtin_code == kTfLiteBuiltinSoftmax)
    status = TIDL_tfliteRtMapSoftmaxParams(registration, node, context, layer);
  else if(registration->builtin_code == kTfLiteBuiltinAdd)
    status = TIDL_tfliteRtMapAddParams(registration, node, context, layer);
  else if(registration->builtin_code == kTfLiteBuiltinMul)
    status = TIDL_tfliteRtMapMulParams(registration, node, context, layer);
  else if(registration->builtin_code == kTfLiteBuiltinDiv)
    status = TIDL_tfliteRtMapDivParams(registration, node, context, layer);
  else if(registration->builtin_code == kTfLiteBuiltinConcatenation)
    status = TIDL_tfliteRtMapConcatParams(registration, node, context, layer);
  else if(registration->builtin_code == kTfLiteBuiltinPad)
    status = TIDL_tfliteRtMapPadParams(registration, node, context, layer);
  else if(registration->builtin_code == kTfLiteBuiltinMean)
    status = TIDL_tfliteRtMapMeanParams(registration, node, context, layer);
  else if(registration->builtin_code == kTfLiteBuiltinFullyConnected)
    status = TIDL_tfliteRtMapFullyConnectedParams(registration, node, context, layer);
  else if(registration->builtin_code == kTfLiteBuiltinResizeBilinear)
    status = TIDL_tfliteRtMapBiInterPResizeParams(registration, node, context, layer);
  else if(registration->builtin_code == kTfLiteBuiltinResizeNearestNeighbor)
    status = TIDL_tfliteRtMapNNResizeParams(registration, node, context, layer);
  else if(registration->builtin_code == kTfLiteBuiltinArgMax)
    status = TIDL_tfliteRtMapArgmaxParams(registration, node, context, layer);
  else if(registration->builtin_code == kTfLiteBuiltinLogistic)
    status = TIDL_tfliteRtMapSigmoidParams(registration, node, context, layer);
  else if(registration->builtin_code == kTfLiteBuiltinBatchToSpaceNd)
    status = TIDL_tfliteRtMapBatchToSpaceParams(registration, node, context, layer);
  else if(registration->builtin_code == kTfLiteBuiltinSpaceToBatchNd)
    status = TIDL_tfliteRtMapSpaceToBatchParams(registration, node, context, layer);
  else if(registration->builtin_code == kTfLiteBuiltinSqueeze)
    status = TIDL_tfliteRtMapSqueezeParams(registration, node, context, layer);
  else if(registration->builtin_code == kTfLiteBuiltinPack)
    status = TIDL_tfliteRtMapPackParams(registration, node, context, layer);  
  else if(registration->builtin_code == kTfLiteBuiltinCustom)
    status = TIDL_tfliteRtMapDetectionOutParams(registration, node, context, layer, odUserParams);
  else if(registration->builtin_code == kTfLiteBuiltinDequantize)
    status = TIDL_tfliteRtMapDequantizeParams(registration, node, context, layer);    
  else if(registration->builtin_code == kTfLiteBuiltinCast)
    status = TIDL_tfliteRtMapCastParams(registration, node, context, layer);    

  else
  {
    //printf("Layer not supported \n");
    return -1;
  }
  return status;
}

int32_t TIDL_tfliteRtLayerUpdateConsumerCount(TfLiteContext* context, const TfLiteDelegateParams* params, int32_t layerIndex)
{
  int32_t i0, i1, i2;
  int32_t numCons = 0;
  TfLiteNode* node;
  TfLiteRegistration* registration;
  TfLiteTensor* tensor;

  for (i0 = 0; i0 < orgTIDLNetStructure.TIDLPCLayers[layerIndex].numOutBufs; i0++)
  {
    for (i1 = 0; i1 < params->nodes_to_replace->size; i1++) 
    {
      TF_LITE_ENSURE_STATUS(context->GetNodeAndRegistration(context, params->nodes_to_replace->data[i1], &node, &registration));
      for (i2 = 0; i2 < node->inputs->size; i2++)
      {
        tensor = &context->tensors[node->inputs->data[i2]];
        if(tensor->allocation_type == kTfLiteArenaRw)
        {
          if(registration->builtin_code == kTfLiteBuiltinPack)
          {
            if(node->inputs->size == 2)
            {
              if((strncmp((const char *)orgTIDLNetStructure.TIDLPCLayers[layerIndex].outDataNames[i0], tensor->name, TIDL_STRING_SIZE) == 0)
                 && (strcmp(context->tensors[node->inputs->data[0]].name, context->tensors[node->inputs->data[1]].name) == 0)) 
              {
                //pack layer has both inputs with same name, but actually only one edge connection in graph, so outConsumerCnt should be 1
                numCons = 1;
                break;
              } 
            }
          }
          else if (strncmp((const char *)orgTIDLNetStructure.TIDLPCLayers[layerIndex].outDataNames[i0], tensor->name, TIDL_STRING_SIZE) == 0)
          {
            numCons++;
          }
        }
      }
    }
    for (i2 = 0; i2 < numNetOutData; i2++)
    {
      if (strcmp((const char *)orgTIDLNetStructure.TIDLPCLayers[layerIndex].outDataNames[i0], outDataNames[i2]) == 0)
      {
        numCons++;
      }
    }
    orgTIDLNetStructure.TIDLPCLayers[layerIndex].outConsumerCnt[i0] = numCons;
  }
  return 0;
}

int32_t TIDL_tfliteRtLinkOutputTensors(int32_t layerIndex)
{
  int32_t i0, i1, i2;
  for (i0 = 0; i0 < orgTIDLNetStructure.TIDLPCLayers[layerIndex].numOutBufs; i0++)
  {
    for (i1 = layerIndex - 1; i1 >= 0; i1--)
    {
      for (i2 = 0; i2 < orgTIDLNetStructure.TIDLPCLayers[i1].numInBufs; i2++)
      {
        if (orgTIDLNetStructure.TIDLPCLayers[layerIndex].outConsumerLinked[i0] < orgTIDLNetStructure.TIDLPCLayers[layerIndex].outConsumerCnt[i0])
        {
          if (strcmp((const char *)orgTIDLNetStructure.TIDLPCLayers[layerIndex].outDataNames[i0], (const char *)orgTIDLNetStructure.TIDLPCLayers[i1].inDataNames[i2]) == 0)
          {
            orgTIDLNetStructure.TIDLPCLayers[i1].inData[i2].dataId = orgTIDLNetStructure.TIDLPCLayers[layerIndex].outData[i0].dataId;
            orgTIDLNetStructure.TIDLPCLayers[layerIndex].outConsumerLinked[i0]++;
          }
        }
      }
    }
  }
  return 0;
}

int32_t TIDL_tfliteRtLinkInputTensors(int32_t layerIndex)
{
  int32_t i0, i1, i2;
  for (i0 = 0; i0 < orgTIDLNetStructure.TIDLPCLayers[layerIndex].numInBufs; i0++)
  {
    for (i1 = layerIndex - 1; i1 >= 0; i1--)
    {
      for (i2 = 0; i2 < orgTIDLNetStructure.TIDLPCLayers[i1].numOutBufs; i2++)
      {
        if (orgTIDLNetStructure.TIDLPCLayers[i1].outConsumerLinked[i2] < orgTIDLNetStructure.TIDLPCLayers[i1].outConsumerCnt[i2])
        {
          if (strcmp((const char *)orgTIDLNetStructure.TIDLPCLayers[layerIndex].inDataNames[i0], (const char *)orgTIDLNetStructure.TIDLPCLayers[i1].outDataNames[i2]) == 0)
          {
            orgTIDLNetStructure.TIDLPCLayers[layerIndex].inData[i0].dataId = orgTIDLNetStructure.TIDLPCLayers[i1].outData[i2].dataId;
            orgTIDLNetStructure.TIDLPCLayers[i1].outConsumerLinked[i2]++;
          }
        }
      }
    }
  }
  return 0;
}

int32_t TIDL_tfliteRtLayerFillTensorNames(TfLiteContext* context, TfLiteNode* node, int32_t layerIndex)
{
  int32_t j, inBufIdx = 0;
  TfLiteTensor* outTensor = &context->tensors[node->outputs->data[0]];
  TfLiteTensor* inTensor;
  TfLiteTensor* tensor;

  strncpy((char*)orgTIDLNetStructure.TIDLPCLayers[layerIndex].name, outTensor->name, TIDL_STRING_SIZE);

  if (orgTIDLNetStructure.TIDLPCLayers[layerIndex].numInBufs > 0)
  {
    for (j = 0; j < node->inputs->size; j++)
    {
      inTensor = &context->tensors[node->inputs->data[j]];
      //if(orgTIDLNetStructure.TIDLPCLayers[layerIndex].layerType == TIDL_Deconv2DLayer)
      //{
      //  strcpy((char*)orgTIDLNetStructure.TIDLPCLayers[layerIndex].inDataNames[j], tensors->Get(op->inputs()->Get(j+2))->name()->c_str());
      //}
      if(inTensor->allocation_type == kTfLiteArenaRw)
      {
        strncpy((char*)orgTIDLNetStructure.TIDLPCLayers[layerIndex].inDataNames[inBufIdx], inTensor->name, TIDL_STRING_SIZE);
        orgTIDLNetStructure.TIDLPCLayers[layerIndex].inData[inBufIdx].dataId = -1;
        inBufIdx++;
      }
   }
  }
  if (orgTIDLNetStructure.TIDLPCLayers[layerIndex].numOutBufs > 0)
  {
    orgTIDLNetStructure.TIDLPCLayers[layerIndex].outConsumerLinked[0] = 0;
    for (j = 0; j < orgTIDLNetStructure.TIDLPCLayers[layerIndex].numOutBufs; j++)
    {
      tensor = &context->tensors[node->outputs->data[j]];

      strncpy((char *)orgTIDLNetStructure.TIDLPCLayers[layerIndex].outDataNames[j], tensor->name, TIDL_STRING_SIZE);
      // Save intermediate tensor name in outDataNames[0] and the OD post proc layers names in outDataNames[1,2,3,4]
      if(orgTIDLNetStructure.TIDLPCLayers[layerIndex].layerType == TIDL_DetectionOutputLayer)
      {
        strcpy((char *)orgTIDLNetStructure.TIDLPCLayers[layerIndex].outDataNames[j], "TfLite_Detection_Process_Intermediate");
      }
      /*if(orgTIDLNetStructure.TIDLPCLayers[layerIndex].actParams.actType == TIDL_RelU6)
      {
          orgTIDLNetStructure.TIDLPCLayers[layerIndex].actParams.actType = TIDL_Clip;
          orgTIDLNetStructure.TIDLPCLayers[layerIndex].actParams.clipMax = 6.0;
          orgTIDLNetStructure.TIDLPCLayers[layerIndex].actParams.clipMin = 0;
          orgTIDLNetStructure.TIDLPCLayers[layerIndex].outData[0].elementType = tidl_getElementType(0);

      }*/
      if ((tensor->type == kTfLiteUInt8) || (tensor->type == kTfLiteInt8))
      {
        TfLiteAffineQuantization* quantization = (TfLiteAffineQuantization*)tensor->quantization.params;
        auto * scale = quantization->scale;
        auto *zero_point = quantization->zero_point;
        if (scale && zero_point)
        {
          orgTIDLNetStructure.TIDLPCLayers[layerIndex].actParams.actType = TIDL_Clip;

          int32_t inMax = 127;
          int32_t inMin = -128;

          if (tensor->type == kTfLiteUInt8)
          {
            inMax = 255;
            inMin = 0;
          }

          orgTIDLNetStructure.TIDLPCLayers[layerIndex].actParams.clipMax = (inMax - zero_point->data[0]) * scale->data[0];
          orgTIDLNetStructure.TIDLPCLayers[layerIndex].actParams.clipMin = (inMin-zero_point->data[0]) * scale->data[0];
          if(orgTIDLNetStructure.TIDLPCLayers[layerIndex].actParams.clipMin >= 0)
          {
            orgTIDLNetStructure.TIDLPCLayers[layerIndex].outData[0].elementType = tidl_getElementType(0);
          }
          else
          {
            orgTIDLNetStructure.TIDLPCLayers[layerIndex].outData[0].elementType = tidl_getElementType(1);
          }
        }
      }
      orgTIDLNetStructure.TIDLPCLayers[layerIndex].outConsumerLinked[j] = 0;
    }
  }
  orgTIDLNetStructure.TIDLPCLayers[layerIndex].weightsElementSizeInBits = NUM_WHGT_BITS;

  return 0;
}

#ifdef __cplusplus
extern "C"
{
#endif

// Initialize global config for tflite import
int32_t TIDL_tfliteRtInit(int32_t tensor_bits, int32_t foldPreBnConv2D, int32_t addDataConvertToNet)
{
  /* Set global tensor data layout format */
  gloab_data_format = 0;  // 1 for NCHW, 0 for NHWC

  /* Set global import config parameters */
  setDefaultParams(&gParams);
  gParams.numParamBits = tensor_bits;
  gParams.numFeatureBits = tensor_bits;
  gParams.modelType = TIDL_IMPORT_MODEL_FORMAT_TFLITE_RT;   // 5
  gParams.addDataConvertToNet = addDataConvertToNet;

  char *tfliteRt_import_debug= getenv("TIDL_TFLITERT_IMPORT_DEBUG");
  if (tfliteRt_import_debug != nullptr)
  {
    tidl_tfliteRt_debuglevel = atoi(tfliteRt_import_debug);
    gParams.debugTraceLevel = std::min(tidl_tfliteRt_debuglevel, 3);
    gParams.writeTraceLevel = (tidl_tfliteRt_debuglevel > 3) ? 3 : 0;
  }
  gParams.foldPreBnConv2D = foldPreBnConv2D;
}

void TIDL_addLayerToDenyList(int32_t * denyList, int32_t * denyListSize, int32_t layerTfliteIdx)
{
  bool alreadyInDenyList = false;
  for(int i = 0; i < *denyListSize; i++)
  {
    if(denyList[i] ==  layerTfliteIdx)
    {
      alreadyInDenyList = true;
      break;
    }
  }
  if(! alreadyInDenyList)
  {
    denyList[*denyListSize] = layerTfliteIdx; 
    *denyListSize++;
  }
}

// Called from tfl_delegate to identify nodes that are supported by TIDL.
int32_t TIDL_tfliteAllowlistNode(const TfLiteRegistration* registration, const TfLiteNode* node, int32_t node_index, TfLiteContext* context, 
                                   bool isObjectDetectionNetwork, int32_t debugLevel, int32_t * denyList, int32_t denyListSize, 
                                   std::vector<int> odProcNodesComplement, bool hasDetectionPostprocLayer, od_parameters * odUserParams)
{
  sTIDL_LayerPC_t layer;
  memset(&layer, 0, sizeof(sTIDL_LayerPC_t));
  
  TfLiteTensor * tensor;
  tensor = &context->tensors[node->outputs->data[0]];

  //First check deny list
  int32_t isNodeInDenyList = 0;
  for(int i = 0; i < denyListSize; i++)
  {
    if(registration->builtin_code == denyList[i])
    {
      isNodeInDenyList = 1;
      break;
    }
  }
  if(isNodeInDenyList)
  {
    tflrt_printf(debugLevel, "Layer '%s', TIDL layer type -- %d added to unsupported nodes as specified in deny list \n", tensor->name, layer.layerType);
    diagsInfo.push_back("Node in deny list...delegated to ARM");
    return 0;
  }
  
  //OD related checks
  if((isObjectDetectionNetwork) && (! hasDetectionPostprocLayer))
  {
    if((odProcNodesComplement.size() == 0)) //means post processing on ARM mode, allowlist based on numDims != 4
    {
      //tflite detection postproc is handled separately, no neeed for this check which is especially to deny post proc layers
      TfLiteTensor * tensor;
      int32_t numDims;
      for(int i = 0; i < node->inputs->size; i++)
      {
        tensor = &context->tensors[node->inputs->data[i]];
        if(tensor->allocation_type == kTfLiteArenaRw) //inputs coming from previous layer
        {
          numDims = tensor->dims->size;
          tflrt_printf(debugLevel, "Layer %d -- layer name -- %s \n Input dims size = %d     dims --- ", node_index, tensor->name, numDims);
          for(int i = 0; i < numDims; i++)
          {
            tflrt_printf(debugLevel, "%d   ", tensor->dims->data[i]);
          }
          tflrt_printf(debugLevel, "\n");

          if(numDims != 4)
          {
            tflrt_printf(debugLevel, "Layer %d --- tflite layer type -  %d,   Number of input dims %d  != 4 .. not supported by TIDL \n", node_index, registration->builtin_code, numDims);
            diagsInfo.push_back("Number of input dims != 4 not supported for OD networks");
            return 0;
          }
        }
      }
    }
    else if(std::find(odProcNodesComplement.begin(), odProcNodesComplement.end(), node_index) == odProcNodesComplement.end()) //if node is not part of backbone network, mark it supported
    {
      //this corresponds to case where all post processing nodes are allowed to TIDL and converted to optimized post proc layer
      tflrt_printf(debugLevel, " Supported TIDL layer type --- %d Tflite layer type --- %d layer output name--- %15s \n", layer.layerType, registration->builtin_code, tensor->name);
      diagsInfo.push_back("");
      return 1;
    }
  }

  int32_t status = TIDL_tfliteRtMapNode(registration, node, context, layer, odUserParams);
  
  tensor = &context->tensors[node->outputs->data[0]];

  if(isObjectDetectionNetwork && hasDetectionPostprocLayer)
  {
    if((layer.layerType == TIDL_ReshapeLayer) || (layer.layerType == TIDL_DetectionOutputLayer) || (layer.layerType == TIDL_ConcatLayer)
      || (layer.layerType == TIDL_SigmoidLayer))
    {
      if(isNodeInDenyList)
      {
        /*
        printf("WARNING : Also adding kTfLiteBuiltinReshape, kTfLiteBuiltinCustom(detection post process layer), kTfLiteBuiltinConcatenation and kTfLiteBuiltinLogistic to deny list \n");
        printf("Performance of the network may deteriorate.. please remove '%d' from  deny list for better performance \n", registration->builtin_code);
        TIDL_addLayerToDenyList(denyList, denyListSize, kTfLiteBuiltinReshape);
        TIDL_addLayerToDenyList(denyList, denyListSize, kTfLiteBuiltinCustom);
        TIDL_addLayerToDenyList(denyList, denyListSize, kTfLiteBuiltinConcatenation);
        TIDL_addLayerToDenyList(denyList, denyListSize, kTfLiteBuiltinLogistic);
        */
      }
      else if((layer.layerType == TIDL_ReshapeLayer) && (context->tensors[node->inputs->data[0]].dims->size > 4)) //input tensor to reshape should not have more than 4 dimensions
      {
        tflrt_printf(debugLevel, "Layer '%s', TIDL layer type -- %d has more than 4 input dims -- not supported \n", tensor->name, layer.layerType);
        diagsInfo.push_back("Reshape layer has more than 4 input dims -- not supported");
        return 0;
      }
      else
      {
        tflrt_printf(debugLevel, " Supported TIDL layer type --- %d Tflite layer type --- %d layer output name--- %15s \n", layer.layerType, registration->builtin_code, tensor->name);
        diagsInfo.push_back("");
        return 1;
      }
    }
  }

  if (status != 0)
  {
    tflrt_printf(debugLevel, "Unsupported (import) TIDL layer type --- %d Tflite layer type --- %d  layer output name--- %15s \n", layer.layerType, registration->builtin_code, tensor->name);
    diagsInfo.push_back("Layer type not supported by TIDL");
    return 0;
  }
  
  // Run the model check on the layer to see if it's supported
  if (!tidlModelCheckOffload(gParams, layer))
  {
    tflrt_printf(debugLevel, "  Unsupported (TIDL check) TIDL layer type --- %d Tflite layer type --- %d layer output name--- %15s \n", layer.layerType, registration->builtin_code, tensor->name);
    return 0;
  }
  tflrt_printf(debugLevel, " Supported TIDL layer type --- %d Tflite layer type --- %d layer output name--- %15s \n", layer.layerType, registration->builtin_code, tensor->name);
  return 1;
}

int32_t TIDL_tfliteRtImportInit(TfLiteContext* context, const TfLiteDelegateParams* params, 
                                   int32_t subgraph_id, int32_t tensor_bits, int32_t foldPreBnConv2D, char * tidl_tools_path, int32_t debugLevel,
                                   std::string metaLayersNamesList, int32_t metaArchType, std::vector<std::string>outDataOd, bool isSubgraphOD, int32_t addDataConvertToNet)
{
  tflrt_printf(debugLevel, "In TIDL_tfliteRtImportInit subgraph_id=%d\n", subgraph_id);
  // Reset all the memories to to NULL, there could be multiple subgraphs
  memset(&orgTIDLNetStructure, 0, sizeof(sTIDL_OrgNetwork_t));
  memset(&tIDLNetStructure,    0, sizeof(sTIDL_Network_t));
  tfliteRt_import_state.layerIndex = 0;
  tfliteRt_import_state.dataIndex  = 0;
  
  // Initialize layer independent parameters of gParams
  TIDL_tfliteRtInit(tensor_bits, foldPreBnConv2D, addDataConvertToNet);
  int32_t status;

  sprintf((char *)gParams.metaLayersNamesList, metaLayersNamesList.c_str());
  int currInIdx = 0, currOutIdx = 0;
  void *ptr;
  
  if((outDataOd.size() == 0) || (! isSubgraphOD)) //Not an OD network / Subgaph does not contain OD post processing part
  {
    gParams.metaArchType = -1;
    for (auto tensor_index : TfLiteIntArrayView(params->output_tensors)) // output tensors of the subgraph
    {
      if (tensor_index == kTfLiteOptionalTensor) 
      {
        continue;
      }
      TfLiteTensor* tensor = &context->tensors[tensor_index];
      // Const tensors should be added as const nodes during graph construction.
      if(tensor->allocation_type == kTfLiteArenaRw)
      {
        int32_t layerIndex = TIDL_tfliteRtImportGetNewLayerIndex();
        sTIDL_LayerPC_t& layer = orgTIDLNetStructure.TIDLPCLayers[layerIndex];
        layer.layerType         = TIDL_DataLayer;
        layer.numInBufs         = 1;
        layer.numOutBufs        = -1;
        layer.outData[0].dataId = 0;
        strcpy((char*)layer.name, tensor->name);
        strcpy((char*)layer.inDataNames[0], tensor->name);
        strcpy((char*)layer.outDataNames[0], tensor->name);
        tflrt_printf(debugLevel, "Layer %d, subgraph input %d, name=%s\n", layerIndex, subgraph_id,
                        (char*)layer.outDataNames[0]);
        layer.outConsumerCnt[0] = 0;
        strcpy((char*)outDataNames[currOutIdx], tensor->name);
        if(gParams.addDataConvertToNet & ADD_DC_LAYER_AT_OUTPUT) 
        {      
          gParams.outLayout[currOutIdx] = TIDLRT_LT_NHWC;    
          TIDL_tfliteRtGetScaleAndZeroPoint(tensor, &gParams.outTensorScale[currOutIdx], &gParams.outZeroPoint[currOutIdx]);
          status =  TIDL_tfliteRtGetTypeAndPtr(tensor, &gParams.outElementType[currOutIdx], &ptr);
          if(status == -1)
          {
            return -1;
          }
        }
        currOutIdx++;
      }
    }
    numNetOutData = currOutIdx;
  }
  else
  {
    gParams.metaArchType = metaArchType;
    numNetOutData = outDataOd.size() ; //The OD heads are treated as outputs, since meta arch import assumes output data layers at heads
    for (int i = 0; i < numNetOutData; i++) 
    {
      int32_t layerIndex = TIDL_tfliteRtImportGetNewLayerIndex();
      sTIDL_LayerPC_t& layer = orgTIDLNetStructure.TIDLPCLayers[layerIndex];
      layer.layerType         = TIDL_DataLayer;
      layer.numInBufs         = 1;
      layer.numOutBufs        = -1;
      layer.outData[0].dataId = 0;
      strcpy((char*)layer.name, (char*)outDataOd[i].c_str());
      strcpy((char*)layer.inDataNames[0], (char*)outDataOd[i].c_str());
      strcpy((char*)layer.outDataNames[0], (char*)outDataOd[i].c_str());
      tflrt_printf(debugLevel, "Layer %d, subgraph input %s, name=%s\n", layerIndex, subgraph_id, (char*)layer.outDataNames[0]);
      layer.outConsumerCnt[0] = 0;
      //strcpy((char*)outDataNames[currOutIdx], tensor->name);
      if(gParams.addDataConvertToNet & ADD_DC_LAYER_AT_OUTPUT)
      {     
        gParams.outLayout[currOutIdx] = TIDLRT_LT_NHWC;
        gParams.outTensorScale[currOutIdx] = 1.0;
        gParams.outZeroPoint[currOutIdx] = 0;
        gParams.outElementType[currOutIdx] = TIDL_SinglePrecFloat;
      }
      currOutIdx++;
    }

  }

  for (auto tensor_index : TfLiteIntArrayView(params->input_tensors)) 
  {
    if (tensor_index == kTfLiteOptionalTensor) 
    {
      continue;
    }
    TfLiteTensor* tensor = &context->tensors[tensor_index];
    // Check if tensor is a network layer and not a parameter input
    if(tensor->allocation_type == kTfLiteArenaRw)
    {
      gParams.numBatches[currInIdx] = 1;
      gParams.inNumChannels[currInIdx] = tensor->dims->data[3];
      gParams.inHeight[currInIdx] = tensor->dims->data[1];
      gParams.inWidth[currInIdx] = tensor->dims->data[2];
      if(gParams.addDataConvertToNet & ADD_DC_LAYER_AT_INPUT)
      {
        gParams.rawDataInElementType[currInIdx]   = TIDL_SinglePrecFloat;
        gParams.inLayout[currInIdx] = TIDLRT_LT_NHWC;
        TIDL_tfliteRtGetScaleAndZeroPoint(tensor, &gParams.inQuantFactor[currInIdx], &gParams.inZeroPoint[currInIdx]);
        status =  TIDL_tfliteRtGetTypeAndPtr(tensor, &gParams.inElementType[currInIdx], &ptr);

        if(status == -1)
        {
          return -1;
        }
      }
      else
      {
        if(gParams.numFeatureBits == 32)
        {
          gParams.inElementType[currInIdx] = TIDL_SinglePrecFloat;
        }
        else if (tensor_bits > 8)
        {
          gParams.inElementType[currInIdx] = TIDL_SignedShort;
        }
        else
        {
          gParams.inElementType[currInIdx] = TIDL_SignedChar;
        }
      }


      /* Create one DataLayer for each input, enforce ordering */
      /* 1. tidl_makeDataIdLayerIdSame() only allows one outDataId per layer */
      /* 2. tidl_sortLayersInProcOrder() topo-sort layers in layer index order */
      /* 3. writeInfo() looks for input tensors in layer index order */
      /* Thus, input DataLayers ordering at creation is preserved in IOBufDesc */
      int32_t layerIndex = TIDL_tfliteRtImportGetNewLayerIndex();
      sTIDL_LayerPC_t& layer = orgTIDLNetStructure.TIDLPCLayers[layerIndex];
      layer.layerType         = TIDL_DataLayer;
      layer.numInBufs         = -1;
      layer.numOutBufs        = 1;
      layer.outData[0].dataId = TIDL_tfliteRtImportGetNewDataIndex();
      layer.outData[0].elementType  = gParams.inElementType[currInIdx];
      layer.outData[0].numDim       = 4;
      layer.outData[0].dimValues[0] = 1;
      layer.outData[0].dimValues[1] = gParams.inNumChannels[currInIdx];
      layer.outData[0].dimValues[2] = gParams.inHeight[currInIdx];
      layer.outData[0].dimValues[3] = gParams.inWidth[currInIdx];
      strcpy((char *)layer.name, tensor->name);
      strcpy((char *)layer.outDataNames[0], tensor->name);
      layer.outConsumerCnt[0] = 1;
      layer.outConsumerLinked[0] = 0;
      TIDL_tfliteRtLayerUpdateConsumerCount(context, params, layerIndex);
      TIDL_tfliteRtLinkOutputTensors(layerIndex);
      tflrt_printf(debugLevel, "Layer %d, subgraph input %d, name=%s\n", layerIndex, subgraph_id,
                      (char*)layer.outDataNames[0]);
      currInIdx++;
    }
  }

  tfliteRt_import_state.numInputDataLayers = currInIdx;
  
  // Initialize rest of the layers
  for (int i = tfliteRt_import_state.numInputDataLayers; i < TIDL_NUM_MAX_PC_LAYERS; i++)
  {
    sTIDL_LayerPC_t& layer_i = orgTIDLNetStructure.TIDLPCLayers[i];
    layer_i.actParams.actType  = TIDL_NoAct;
    layer_i.strideOffsetMethod = TIDL_StrideOffsetCenter;
  }

  gParams.inFileFormat = 1;  // raw data
  
  if(tidl_tools_path)
  {
    strcpy((char*)gParams.tidlStatsTool, tidl_tools_path);
    strcat((char*)gParams.tidlStatsTool,"/PC_dsp_test_dl_algo.out");
    strcpy((char*)gParams.perfSimTool, tidl_tools_path);
    strcat((char*)gParams.perfSimTool,"/ti_cnnperfsim.out");
    strcpy((char*)gParams.graphVizTool, tidl_tools_path);
    strcat((char*)gParams.graphVizTool,"/tidl_graphVisualiser.out");
    strcpy((char*)gParams.perfSimConfig, tidl_tools_path);
    strcat((char*)gParams.perfSimConfig,"/device_config.cfg");
  }
  else
  {
    printf("Please provide TIDL tools path \n");
    exit(-1);
  }

  if (tidlValidateImportParams(&gParams) == -1)
  {
    printf("Validation of TIDL tflite runtime import config parameters failed!\n");
    return -1;
  }

  if(gParams.metaArchType != -1)
  {
    tidl_metaArch_import(&gParams);
  }

  return 0;
}


int32_t TIDL_tfliteRtImportAndLinkNode(TfLiteRegistration* registration, TfLiteContext* context, const TfLiteDelegateParams* params, 
                                         TfLiteNode* node, int32_t debugLevel, od_parameters * odUserParams)
{
  int32_t status = 0;

  // Get new layerIndex, dataIndex
  int32_t layerIndex = TIDL_tfliteRtImportGetNewLayerIndex();
  int32_t dataIndex  = TIDL_tfliteRtImportGetNewDataIndex();
  sTIDL_LayerPC_t& layer = orgTIDLNetStructure.TIDLPCLayers[layerIndex];
  // set layer defaults
  layer.numInBufs         = 1;
  layer.numOutBufs        = 1;
  layer.outData[0].dataId = dataIndex;
  
  status = TIDL_tfliteRtMapNode(registration, node, context, layer, odUserParams);
  tflrt_printf(debugLevel, "In TIDL_tfliteRtImportNode  TIDL Layer type %d   Tflite builtin code type %d \n", layer.layerType, registration->builtin_code);

  TIDL_tfliteRtLayerFillTensorNames(context, node, layerIndex);
  TIDL_tfliteRtLayerUpdateConsumerCount(context, params, layerIndex);
  TIDL_tfliteRtLinkInputTensors(layerIndex);
  TIDL_tfliteRtLinkOutputTensors(layerIndex);

  return status;
}

int32_t TIDL_tfliteRtOptimizeNet(int32_t debugLevel)
{
  tflrt_printf(debugLevel, "In TIDL_tfliteRtOptimizeNet: LayerIndex = %d, dataIndex = %d \n", tfliteRt_import_state.layerIndex, tfliteRt_import_state.dataIndex);

  tidl_optimizeNet(orgTIDLNetStructure, tfliteRt_import_state.layerIndex,
                   tfliteRt_import_state.dataIndex);

  return 0;
}

int32_t tidl_setParamsForPostProcessNet(int32_t tensor_bits)
{
  gParams.numParamBits = tensor_bits;
  gParams.numFeatureBits = tensor_bits;
  
  for (int i = 0; i < TIDL_MAX_ALG_IN_BUFS; i++)
  {
    gParams.inElementType[i] = TIDL_SignedChar;  /** TODO : Set to signed or unsigned? */
    gParams.rawDataInElementType[i]   = TIDL_SinglePrecFloat;//gParams.inElementType[i];
  }
  if (gParams.numFeatureBits > 8)
  {
    for (int i = 0; i < TIDL_MAX_ALG_IN_BUFS; i++)
    {
      if(gParams.numFeatureBits == 32)
      {
        gParams.inElementType[i] = TIDL_SinglePrecFloat;
      }
      else if(gParams.inElementType[i] == TIDL_UnsignedChar)
      {
        gParams.inElementType[i] = TIDL_UnsignedShort;
      }
      else if(gParams.inElementType[i] == TIDL_SignedChar)
      {
        gParams.inElementType[i] = TIDL_SignedShort;
      }
    }
  }
  gParams.executeNetworkCompiler = 1;
}

int32_t TIDL_tfliteRtPostProcessNet(int32_t numFrames, int32_t numParamBits, int32_t calibrationOption, int32_t biasCalibrationIterations, 
                                    int32_t quantizationStyle, int32_t enableHighResOptimization, int32_t compileConstraintsFlag, 
                                    int32_t foldPreBnConv2D, void * subGraphPtr, float * inQuantFactor, char* artifacts_folder, 
                                    int32_t outTensorIdx, int32_t debugLevel, std::string outputFeature16bitNamesList, std::string params16bitNamesList)
{
  tflrt_printf(debugLevel, "In TIDL_tfliteRtPostProcessNet: \n");
  int32_t status = 0;
  memcpy(&orgTIDLNetStructure, subGraphPtr, sizeof(sTIDL_OrgNetwork_t));
  TIDL_allocAndCopyModelParams(&orgTIDLNetStructure, (sTIDL_OrgNetwork_t *)subGraphPtr, orgTIDLNetStructure.numLayers);
  
  memset(&tIDLNetStructure, 0, sizeof(sTIDL_Network_t));
  
  tidl_setParamsForPostProcessNet(numParamBits);
  gParams.numFrames = numFrames;
  gParams.calibrationOption = calibrationOption;
  gParams.biasCalibrationIterations = biasCalibrationIterations;
  gParams.enableHighResOptimization = enableHighResOptimization;
  gParams.quantizationStyle = quantizationStyle;
  gParams.compileConstraintsFlag = compileConstraintsFlag;
  gParams.foldPreBnConv2D = foldPreBnConv2D;

  strcpy((char *)&gParams.outputFeature16bitNamesList[0], const_cast<char *>(outputFeature16bitNamesList.c_str()));
  strcpy((char *)&gParams.params16bitNamesList[0], const_cast<char *>(params16bitNamesList.c_str()));

  if((gParams.addDataConvertToNet & ADD_DC_LAYER_AT_INPUT) == 0)
  {
    for(int i = 0; i < TIDL_MAX_ALG_IN_BUFS; i++)
    {
      gParams.inQuantFactor[i] = inQuantFactor[i];
      if(numParamBits == 32)
      {
        gParams.inQuantFactor[i] = 1.0;
      }
    }
  }
  int j = 0;
  for (int i = 0; i < orgTIDLNetStructure.numLayers; i++)
  {
    if((orgTIDLNetStructure.TIDLPCLayers[i].layerType == TIDL_DataLayer) && (orgTIDLNetStructure.TIDLPCLayers[i].numInBufs == -1))
    {
      orgTIDLNetStructure.TIDLPCLayers[i].outData[0].tensorScale = gParams.inQuantFactor[j++];
    }
  }

  tidl_updateWeightElemSize(&orgTIDLNetStructure, &gParams, orgTIDLNetStructure.numLayers);
  if(numParamBits == 32)
  {
    tidl_convertElementTypeToFloat(&orgTIDLNetStructure, orgTIDLNetStructure.numLayers);
  }
  snprintf((char *)inConfigFilename, FILE_NAME_SIZE, "%s/%d_tidl_io_", artifacts_folder, outTensorIdx);
  snprintf((char *)gParams.outputNetFile, FILE_NAME_SIZE, "%s/%d_tidl_net.bin", artifacts_folder, outTensorIdx);
  snprintf((char *)gParams.outputParamsFile, FILE_NAME_SIZE, "%s/%d_tidl_io_", artifacts_folder, outTensorIdx);
  sprintf((char *)gParams.inData, "%s/calib_raw_data_%d.bin", artifacts_folder, outTensorIdx);
  
  status = TIDL_import_backend(orgTIDLNetStructure.numLayers);
  tflrt_printf(debugLevel, "Done With TIDL_tfliteRtPostProcessNet: \n");

  return status;
}

void TIDL_saveTidlSubGraph(TfLiteContext* context, const TfLiteDelegateParams* params, void ** subGraphPtr)
{
  *subGraphPtr = (void *)malloc(sizeof(sTIDL_OrgNetwork_t));
  if ( subGraphPtr == NULL )
  {
    printf("Unable to allocate memory to save the subgraph \n");
    return;
  }
  memcpy(*subGraphPtr, &orgTIDLNetStructure, sizeof(sTIDL_OrgNetwork_t));
}

#ifdef __cplusplus
}
#endif

}  //namespace tfl_delegate
}  // namespace tflite

