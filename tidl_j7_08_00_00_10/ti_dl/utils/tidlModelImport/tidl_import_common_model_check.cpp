/*
*
* Copyright (c) {2015 - 2020} Texas Instruments Incorporated
*
* All rights reserved not granted herein.
*
* Limited License.
*
* Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
* license under copyrights and patents it now or hereafter owns or controls to make,
* have made, use, import, offer to sell and sell ("Utilize") this software subject to the
* terms herein.  With respect to the foregoing patent license, such license is granted
* solely to the extent that any such patent is necessary to Utilize the software alone.
* The patent license shall not apply to any combinations which include this software,
* other than combinations with devices manufactured by or for TI ("TI Devices").
* No hardware patent is licensed hereunder.
*
* Redistributions must preserve existing copyright notices and reproduce this license
* (including the above copyright notice and the disclaimer and (if applicable) source
* code license limitations below) in the documentation and/or other materials provided
* with the distribution
*
* Redistribution and use in binary form, without modification, are permitted provided
* that the following conditions are met:
*
* *       No reverse engineering, decompilation, or disassembly of this software is
* permitted with respect to any software provided in binary form.
*
* *       any redistribution and use are licensed by TI for use only with TI Devices.
*
* *       Nothing shall obligate TI to provide you with source code for the software
* licensed and provided to you in object code.
*
* If software source code is provided to you, modification and redistribution of the
* source code are permitted provided that the following conditions are met:
*
* *       any redistribution and use of the source code, including any resulting derivative
* works, are licensed by TI for use only with TI Devices.
*
* *       any redistribution and use of any object code compiled from the source code
* and any resulting derivative works, are licensed by TI for use only with TI Devices.
*
* Neither the name of Texas Instruments Incorporated nor the names of its suppliers
*
* may be used to endorse or promote products derived from this software without
* specific prior written permission.
*
* DISCLAIMER.
*
* THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
* OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
* OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
* IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
* BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
* OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
* OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
* OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/
#include <fcntl.h>
#include <stdint.h>
#include <stdio.h>
#include <math.h>
#include <float.h>
#include <cmath>
#include <stdlib.h>
#include <stdarg.h>
#include <string>
#include <list>
#include <vector>

#include "ti_dl.h"
#include "tidl_import_api.h"
#include "tidl_import_config.h"
#include "tidl_import_common.h"

#define TIDL_SIMD_WIDTH (64)
#define MODEL_CHECK_CONV_SUGGESTIONS 0

static bool is16bit = false;
extern "C"{
std::vector<std::string> diagsInfo;
}
// This class represents a warning or error diagnostic. It can apply to a single layer
// or the whole model.
class TIDL_ModelDiagnostic
{
public:
  enum Kind
  {
    DK_Supported,     // Fully supported
    DK_Info,          // Fully supported, informational message
    DK_NotPerformant, // Supported, but may not perform well
    DK_PCOnly,        // Supported only for PC host emulation
    DK_NotVerified,   // Supported, but not verified
    DK_NotSupported,  // Not supported (error)
  };

  // Construct a diagnostic, with printf-style formatting
  TIDL_ModelDiagnostic(Kind k, const char* fmt, ...);
  // Emit diagnostic to stdout
  void emit() const;
  // True if diagnostic represents a fatal error (model will not run on TIDL)
  bool isError() const;
  // True if node should part of subgraph offloaded to TIDL
  bool isOKToOffload() const;
  //Extract message from the diagnostic
  std::pair<const char *, std::string> extractDiagMsg();
private:
   const char* prefix() const;
   Kind kind;
   std::string msg;
};

using DiagList_t = std::list<TIDL_ModelDiagnostic>;

void tidlModelCheckLayer(const sTIDL_LayerPC_t &layerPC, DiagList_t &diags);

// Construct a diagnostic using printf-style formatting
TIDL_ModelDiagnostic::TIDL_ModelDiagnostic(TIDL_ModelDiagnostic::Kind k, const char* fmt, ...) : kind(k)
{
  va_list args;
  va_start(args, fmt);
  size_t len = vsnprintf(nullptr, 0, fmt, args);
  va_end(args);

  va_start(args, fmt);
  std::vector<char> buf(len+1);
  len = vsnprintf(buf.data(), buf.size(), fmt, args);
  va_end(args);
  msg = std::string(buf.data(), len);
}

// Emit diagnostic to stdout
void TIDL_ModelDiagnostic::emit() const
{
  if (kind == DK_Supported)
    return;
  printf("%s: ", prefix());
  printf("%s\n", msg.c_str());
}

const char* TIDL_ModelDiagnostic::prefix() const
{
  switch (kind)
  {
    default:               return "UNKNOWN ERROR";
    case DK_Supported:     return "";
    case DK_NotSupported:  return "ERROR";
    case DK_PCOnly:
    case DK_NotVerified:   return "WARNING";
    case DK_NotPerformant: return "SUGGESTION";
    case DK_Info:          return "INFORMATION";
  }
}

std::pair<const char *, std::string> TIDL_ModelDiagnostic::extractDiagMsg()
{
  return std::make_pair(prefix(), msg);
}

// Return true if this diagnostic represents a condition that will prevent the network
// from operating properly on TIDL.
bool TIDL_ModelDiagnostic::isError() const
{
  return kind == DK_NotSupported;
}

// Return true if this condition should not prevent offloading this node to TIDL as
// part of a subgraph.
bool TIDL_ModelDiagnostic::isOKToOffload() const
{
  switch (kind)
  {
    default:
    case DK_NotSupported:
    case DK_PCOnly:
    case DK_NotVerified:   return false;

    case DK_Supported:
    case DK_NotPerformant:
    case DK_Info:          return true;
  }
}

static void checkConvLayers(const sTIDL_LayerPC_t &layerPC, DiagList_t &diags)
{
  int32_t i2, i3, i4;

  bool notOptimal_input64align = false;
  bool notOptimal_output64align = false;
  bool validated = false;
  bool warning = false;
  bool notOptimal = false;
  bool fatalError = false;

  const sTIDL_ConvParams_t& params = layerPC.layerParams.convParams;

  /*
   * Validated combinations
   */
  int32_t validatedParams[][8] =
  {
    // -1 means dont care
    // kernelH kernelW strideH strideW dilationH dilationW padH padW
    {        1,      1,      1,      1,       -1,       -1,  -1,  -1},
    {        1,      1,      2,      2,       -1,       -1,  -1,  -1},
    {        2,      2,      1,      1,       -1,       -1,  -1,  -1},
    {        2,      2,      2,      2,        1,        1,  -1,  -1},
    {        3,      3,      1,      1,       -1,       -1,  -1,  -1},
    {        3,      3,      2,      2,        1,        1,  -1,  -1},
    {        3,      3,      3,      3,        1,        1,  -1,  -1},
    {        5,      5,      1,      1,       -1,       -1,  -1,  -1},
    {        5,      5,      2,      2,        1,        1,  -1,  -1},
    {        7,      7,      1,      1,       -1,       -1,  -1,  -1},
    {        7,      7,      2,      2,        1,        1,  -1,  -1},
    {        1,      3,      1,      1,       -1,       -1,  -1,  -1},
    {        3,      1,      1,      1,       -1,       -1,  -1,  -1},
    {        1,      5,      1,      1,       -1,       -1,  -1,  -1},
    {        5,      1,      1,      1,       -1,       -1,  -1,  -1},
    {        1,      7,      1,      1,       -1,       -1,  -1,  -1},
    {        7,      1,      1,      1,       -1,       -1,  -1,  -1},
  };

  for(i2 = 0; i2 < sizeof(validatedParams)/8/sizeof(int32_t); i2++)
  {
    if((validatedParams[i2][0] == -1 || params.kernelH == validatedParams[i2][0]) && (validatedParams[i2][1] == -1 || params.kernelW == validatedParams[i2][1]))
    {
      validated = true;
    }
    else
    {
      validated = false;
      continue;
    }

    if((validatedParams[i2][2] == -1 || params.strideH == validatedParams[i2][2]) && (validatedParams[i2][3] == -1 || params.strideW == validatedParams[i2][3]))
    {
      validated = true;
    }
    else
    {
      validated = false;
      continue;
    }

    if((validatedParams[i2][4] == -1 || params.dilationH == validatedParams[i2][4]) && (validatedParams[i2][5] == -1 || params.dilationW == validatedParams[i2][5]))
    {
      validated = true;
    }
    else
    {
      validated = false;
      continue;
    }

    if((validatedParams[i2][6] == -1 || params.padH == validatedParams[i2][6]) && (validatedParams[i2][7] == -1 || params.padW == validatedParams[i2][7]))
    {
      validated = true;
    }
    else
    {
      validated = false;
      continue;
    }

    if(validated)
      break;
  }

  /* Depthwise separable convolution */
  if (((params.numGroups == params.numInChannels) || (params.numInChannels ==1)) &&
      (params.numGroups == params.numOutChannels))
  {
    // kernelH kernelW strideH strideW dilationH dilationW padH padW
    #define DEPTHWISE_SUPPORTED_PARAMS (4)
    int32_t supportedConfigs[][DEPTHWISE_SUPPORTED_PARAMS] =
    {
      /*kernelW, kernelH, strideW, strideH*/
      {   3,          3,             1,           1},
      {   3,          3,             2,           2},
      {   5,          5,             1,           1},
      {   7,          7,             1,           1},
    };

    for(i2 = 0; i2 < sizeof(validatedParams)/(DEPTHWISE_SUPPORTED_PARAMS *sizeof(int32_t)); i2++)
    {
      if ( ( params.kernelW == supportedConfigs[i2][0] ) &&
	( params.kernelH == supportedConfigs[i2][1] ) &&
	( params.strideW == supportedConfigs[i2][2] ) &&
	( params.strideH == supportedConfigs[i2][3] ) )
      {
	/* Only these combinations are supported */
	validated = true;
	break;
      }
      else
      {
	validated = false;
      }
    }

    if ( validated!= true)
    {
      /* Any other combination is not supported */
      fatalError = true;
      diags.emplace_back(TIDL_ModelDiagnostic::DK_NotSupported, "[TIDL_ConvolutionLayer] "
         "%s Depthwise convolution layer is either not supported or doesn't have "
	 "optimized implementation available.", layerPC.name);
    }
  }

  /*
   * Not supported
   */
  if(params.kernelH == 11 && params.kernelW == 11 &&
     params.strideH == 4 && params.strideW == 4)
  {
    warning = true;
    diags.emplace_back(TIDL_ModelDiagnostic::DK_NotVerified, "[TIDL_ConvolutionLayer] "
       "%s kernel size 11x11 with stride 4 has gone through limited verification.",
       layerPC.name);
  }

  if(params.kernelH > 7  && params.kernelW > 7 &&
     params.strideH == 2 && params.strideW == 2)
  {
    fatalError = true;
    diags.emplace_back(TIDL_ModelDiagnostic::DK_NotSupported, "[TIDL_ConvolutionLayer] "
       "%s kernel size larger than 7 with stride 2 is not supported !!!",
       layerPC.name);
  }

  if(params.kernelH != 11 && params.kernelW != 11 &&
     params.strideH == 4 && params.strideW == 4)
  {
    fatalError = true;
    diags.emplace_back(TIDL_ModelDiagnostic::DK_NotSupported, "[TIDL_ConvolutionLayer] "
       "%s stride 4 only supports kernel size 11x11 !!!", layerPC.name);
  }

  /*
   * Performance Suggestion
   */
  if((params.kernelH * params.kernelW * layerPC.inData[0].dimValues[1] / params.numGroups + params.enableBias) % 64)
  {
    notOptimal = true;
    notOptimal_input64align = true;
  }
  if(layerPC.outData[0].dimValues[1] / params.numGroups % 64)
  {
    notOptimal = true;
    notOptimal_output64align = true;
  }

  /*
   * Conclusion
   */
  if(!validated && !fatalError)
  {
    diags.emplace_back(TIDL_ModelDiagnostic::DK_NotVerified, "[TIDL_ConvolutionLayer] "
       "%s Layer parameter combination has undergone limited validation and "
       "may have some issues. Following are the parameters:"
       "\n        Kernel %dx%d Stride %dx%d dilation %dx%d Pad %dx%d Bias %d",
       layerPC.name,
       params.kernelH,   params.kernelW,
       params.strideH,   params.strideW,
       params.dilationH, params.dilationW,
       params.padH,      params.padW,
       params.enableBias);
  }

#if MODEL_CHECK_CONV_SUGGESTIONS
  if(notOptimal_input64align)
    diags.emplace_back(TIDL_ModelDiagnostic::DK_NotPerformant, "[TIDL_ConvolutionLayer] "
       "%s params.kernelH * params.kernelW * layer.inData[0].dimValues[1] / params.numGroups "
       "is not 64 aligned, the performance is not efficient on MMA!",
       layerPC.name);

  if(notOptimal_output64align)
    diags.emplace_back(TIDL_ModelDiagnostic::DK_NotPerformant, "[TIDL_ConvolutionLayer] "
       "%s layer.outData[0].dimValues[1] / params.numGroups "
       "is not 64 aligned, the performance is not efficient on MMA!",
       layerPC.name);
#endif
}


static void checkPoolingLayers(const sTIDL_LayerPC_t &layerPC, DiagList_t &diags)
{
  const sTIDL_PoolingParams_t& params = layerPC.layerParams.poolParams;
  bool validated = false;
  bool warning = false;
  bool notOptimal = false;
  bool fatalError = false;

  /*
   * Validated combinations
   */
  if(params.kernelH == 0 && params.kernelW == 0)
  {
    validated = true; // Global pooling
  }

  if(params.kernelH == 2 && params.kernelW == 2 &&
     params.strideH == 2 && params.strideW == 2)
  {
    validated = true;
  }

  if(params.kernelH == 1 && params.kernelW == 1 &&
     ((params.strideH == 2 && params.strideW == 2) || (params.strideH == 1 && params.strideW == 1 )) &&
     params.poolingType == TIDL_MaxPooling)
  {
    validated = true;
  }

  if(params.kernelH == 3 && params.kernelW == 3 &&
     ((params.strideH == 1 && params.strideW == 1) || (params.strideH == 2 && params.strideW == 2)) &&
     params.padH    == 1 && params.padW    == 1)
  {
    validated = true;
  }


  if(params.kernelH == 3 && params.kernelW == 3 &&
     params.strideH == 2 && params.strideW == 2 &&
     params.padH    == 0 && params.padW    == 0)
  {
    validated = true;
  }

  if(is16bit && params.poolingType == TIDL_MaxPooling)
  {
    validated = true;
  }


  /*
   * Performance Suggestion
   */
  if(params.kernelH == 4 && params.kernelW == 4)
  {
    notOptimal = true;
    diags.emplace_back(TIDL_ModelDiagnostic::DK_NotPerformant, "[TIDL_PoolingLayer] "
       "%s 4x4 pooling is not efficient on MMA!", layerPC.name);
  }

  if (!validated)
    diags.emplace_back(TIDL_ModelDiagnostic::DK_NotSupported, "TIDL_PoolingLayer '%s': "
       "kernel size %dx%d with stride %dx%d not supported", 
       layerPC.name, params.kernelH, params.kernelW, params.strideH, params.strideW);
}


static void checkBatchNormLayers(const sTIDL_LayerPC_t &layerPC, DiagList_t &diags)
{
  const sTIDL_BatchNormParams_t& params = layerPC.layerParams.batchNormParams;
  bool notOptimal = false;

  /*
   * Performance Suggestion
   */
  if((is16bit) && (layerPC.actParams.actType != TIDL_Sigmoid))
  {
    notOptimal = true;
    diags.emplace_back(TIDL_ModelDiagnostic::DK_NotPerformant, "[TIDL_BatchNormLayer] "
       "%s 16 bits is not optimal in this release.", layerPC.name);
  }
}


static void checkInnerProductLayers(const sTIDL_LayerPC_t &layerPC, DiagList_t &diags)
{
  const sTIDL_InnerProductParams_t& params = layerPC.layerParams.innerProductParams;
  bool validated = false;
  bool warning = false;
  bool notOptimal = false;
  bool fatalError = false;

  /*
   * Not supported
   */
  if(/* layerPC.inData[0].dimValues[0] != 1 || */
     layerPC.inData[0].dimValues[1] != 1 ||
     layerPC.inData[0].dimValues[2] != 1)
  {
    fatalError = true;
    diags.emplace_back(TIDL_ModelDiagnostic::DK_NotSupported, "[TIDL_InnerProductLayer] "
        "%s input shape of inner product must be 1x1x1xN.", layerPC.name);
  }

  /*
   * Performance Suggestion
   */
  if(layerPC.inData[0].dimValues[3] * layerPC.outData[0].dimValues[3] > 2048 * 2048)
  {
    notOptimal = true;
    diags.emplace_back(TIDL_ModelDiagnostic::DK_NotPerformant, "[TIDL_InnerProductLayer] "
       "%s Size larger than 2048 * 2048 is not optimal.", layerPC.name);
  }
}


static void checkDeconvLayers(const sTIDL_LayerPC_t &layerPC, DiagList_t &diags)
{
  const sTIDL_ConvParams_t& params = layerPC.layerParams.convParams;
  bool validated = false;
  bool warning = false;
  bool notOptimal = false;
  bool fatalError = false;

  /*
   * Validated combinations
   */
  if(params.kernelH == 4 && params.kernelW == 4 &&
     params.strideH == 2 && params.strideW == 2 &&
     params.padW == 1 && params.padH == 1)
  {
    validated = true;
  }
  else if (params.kernelH == 2 && params.kernelW == 2 &&
     params.strideH == 2 && params.strideW == 2 &&
     params.padW == 0 && params.padH == 0)
  {
    validated = true;
  }
  else
  {
    fatalError = true;
    diags.emplace_back(TIDL_ModelDiagnostic::DK_NotSupported, "[TIDL_Deconv2DLayer] "
       "%s is not supported.", layerPC.name);
  }

  /*
   * Performance Suggestion
   */
  diags.emplace_back(TIDL_ModelDiagnostic::DK_NotPerformant, "[TIDL_Deconv2DLayer] "
      "%s Please change to Upsample/Resize if possible. Upsample/Resize will be more efficient.", layerPC.name);
}

static void checkDetectionOutLayers(const sTIDL_LayerPC_t &layerPC, DiagList_t &diags)
{
  const sTIDL_DetectOutputParams_t& params = layerPC.layerParams.detectOutParams;
  bool validated = false;
  bool warning = false;
  bool notOptimal = false;
  bool fatalError = false;

}


static void checkShuffleChannelLayers(const sTIDL_LayerPC_t &layerPC, DiagList_t &diags)
{
  const sTIDL_ShuffleLayerParams_t& params = layerPC.layerParams.shuffleLayerParams;
  bool validated = false;
  bool warning = false;
  bool notOptimal = false;
  bool fatalError = false;

  /*
   * Performance Suggestion
   */
  notOptimal = true;
  diags.emplace_back(TIDL_ModelDiagnostic::DK_NotPerformant, "[TIDL_ShuffleChannelLayer] "
      "%s ShuffleChannel layer is not optimal in this version.", layerPC.name);
}

static void checkDepthToSpaceLayers(const sTIDL_LayerPC_t &layerPC, DiagList_t &diags)
{
  const sTIDL_ShuffleLayerParams_t& params = layerPC.layerParams.shuffleLayerParams;
  bool validated = false;
  bool warning = false;
  bool notOptimal = false;
  bool fatalError = false;

  int32_t blockSize = layerPC.layerParams.depthToSpaceParams.blockSize;
  /*
   * Validated combinations
   */
  if(blockSize == 8 || blockSize == 4 || blockSize == 2)
  {
    validated = true;
  }
  else
  {
    fatalError = true;
    diags.emplace_back(TIDL_ModelDiagnostic::DK_NotSupported, "[TIDL_DepthToSpaceLayer] "
       "%s is not supported.", layerPC.name);
  }

  /*
   * Performance Suggestion
   */
  notOptimal = true;
  diags.emplace_back(TIDL_ModelDiagnostic::DK_NotPerformant, "[TIDL_DepthToSpaceLayer] "
      "%s Standalone DepthToSpace layer is not optimal in this version, it is optimal if it is next to 1x1 convolutions", layerPC.name);
}

static void checkBatchToSpaceLayers(const sTIDL_LayerPC_t &layerPC, DiagList_t &diags)
{
  bool validated = false;
  bool warning = false;
  bool notOptimal = false;
  bool fatalError = false;

  fatalError = true;
  if((gParams.modelType == 0) || (gParams.modelType == 1) || (gParams.modelType == 2) || (gParams.modelType == 3))
  diags.emplace_back(TIDL_ModelDiagnostic::DK_NotSupported, "[TIDL_BatchToSpaceLayer/TIDL_SpaceToBatchLayer] "
      "%s is PC layer, marked supported only for runtimes(to enable dispatch to TIDL), should be already merged for standalone TIDL import check", layerPC.name);
}

static void checkColorConversionLayers(const sTIDL_LayerPC_t &layerPC, DiagList_t &diags)
{
  return;
}

static void checkResizeLayers(const sTIDL_LayerPC_t &layerPC, DiagList_t &diags)
{
  const sTIDL_ResizeLayerParams_t& params = layerPC.layerParams.resizeParams;
  bool warning = false;
  bool notOptimal = false;
  bool fatalError = false;

  /*
   * Validated combinations
   */

  if ( params.resizeRatio[TIDL_DIM_WIDTH] == params.resizeRatio[TIDL_DIM_HEIGHT] )
  {
    if (( params.resizeRatio[TIDL_DIM_WIDTH] == 2.0 ) ||
      (params.resizeRatio[TIDL_DIM_WIDTH] == 4.0 ))
    {
      diags.emplace_back(TIDL_ModelDiagnostic::DK_Info, "[TIDL_ResizeLayer] "
         "%s Any resize ratio which is power of 2 and greater than 4 will be placed by combination of 4x4 resize layer and 2x2 resize layer. "
         "For example a 8x8 resize will be replaced by 4x4 resize followed by 2x2 resize.", layerPC.name);
    }
    else
    {
      warning = 1;
      diags.emplace_back(TIDL_ModelDiagnostic::DK_NotPerformant, "[TIDL_ResizeLayer] "
         "%s Resize kernel with non-power of 2 resize ratio is not optimal.", layerPC.name);
    }
  }
  else
  {
    warning = 1;
    diags.emplace_back(TIDL_ModelDiagnostic::DK_NotPerformant, "[TIDL_ResizeLayer] "
       "%s Resize Layer with non-symmetric resize ratio across width and height is not optimal.", layerPC.name);
  }
  if(params.mode != TIDL_ResizeNearest && params.mode != TIDL_ResizeBilinear)
  {
    diags.emplace_back(TIDL_ModelDiagnostic::DK_NotSupported, "[TIDL_ResizeLayer] "
       "%s Resize layer mode must be either nearest_neighbor or bilinear.", layerPC.name);
  }
}


static void checkPriorBoxLayers(const sTIDL_LayerPC_t &layerPC, DiagList_t &diags)
{
  diags.emplace_back(TIDL_ModelDiagnostic::DK_NotSupported, "[TIDL_PriorBoxLayer] "
     "%s should be removed in import process. Please make sure you have detection out layer in the model. "
     "If not, this model will not work!", layerPC.name);
}


static void checkPermuteLayers(const sTIDL_LayerPC_t &layerPC, DiagList_t &diags)
{
  diags.emplace_back(TIDL_ModelDiagnostic::DK_NotSupported, "[TIDL_PermuteLayer] "
     "%s should be removed in import process. Please make sure you have detection out layer in the model. "
     "If not, this model will not work!", layerPC.name);
}


static void checkEltwiseLayers(const sTIDL_LayerPC_t &layerPC, DiagList_t &diags)
{
  const sTIDL_EltWiseParams_t& params = layerPC.layerParams.eltWiseParams;
  bool validated = false;
  bool warning = false;
  bool notOptimal = false;
  bool fatalError = false;

#ifdef TIDL_EN_MULTI_TENSOR_ELT_WISE
  /* curently using (layer.inData[0].elementType >> 1)+1 instead of TIDL_getDatElementSize */
  if(((layerPC.numInBufs > 2) && (params.eltWiseType == TIDL_EltWiseProduct)) ||
     ((layerPC.numInBufs > ((TIDL_SIMD_WIDTH>>1)/((layerPC.inData[0].elementType >> 1) + 1)))
      && (params.eltWiseType == TIDL_EltWiseSum)))
  {
    fatalError = true;
    diags.emplace_back(TIDL_ModelDiagnostic::DK_NotSupported, "[TIDL_EltWiseLayer] "
       "%s Too many inputs for elementwise operator.", layerPC.name);
  }
#else
  /*
   * Not supported
   */
  if(layerPC.numInBufs > 2)
  {
    fatalError = true;
    diags.emplace_back(TIDL_ModelDiagnostic::DK_NotSupported, "[TIDL_EltWiseLayer] "
       "%s Only supports 2 inputs.", layerPC.name);
  }
#endif
}

static void checkFlattenLayers(const sTIDL_LayerPC_t &layerPC, DiagList_t &diags)
{
  bool validated = false;
  bool warning = false;
  bool notOptimal = false;
  bool fatalError = false;

  if(is16bit)
  {
    notOptimal = true;
    diags.emplace_back(TIDL_ModelDiagnostic::DK_NotPerformant, "[TIDL_FlattenLayer] "
       "%s Flatten with 16 bit is not optimal in this version.", layerPC.name);
  }
}

static void checkSliceLayers(const sTIDL_LayerPC_t &layerPC, DiagList_t &diags)
{
  const sTIDL_SliceLayerParams_t& params = layerPC.layerParams.sliceParams;
  bool validated = false;
  bool warning = false;
  bool notOptimal = false;
  bool fatalError = false;

  /*
   * Not supported
   */
  if((params.axis < 1) || (params.axis > 3) ||  (params.stride != 1))
  {
    fatalError = true;
    diags.emplace_back(TIDL_ModelDiagnostic::DK_NotSupported, "[TIDL_SliceLayer] "
       "%s Only supports axis = 1 to 3 and Stride 1.", layerPC.name);
  }
}



static void checkArgMaxLayers(const sTIDL_LayerPC_t &layerPC, DiagList_t &diags)
{
  const sTIDL_ArgMaxParams_t& params = layerPC.layerParams.argMaxParams;
  bool validated = false;
  bool warning = false;
  bool notOptimal = false;
  bool fatalError = false;

  /*
   * Not supported
   */
  if(layerPC.outData[0].dimValues[0] != 1 ||
     layerPC.outData[0].dimValues[1] != 1)
  {
    fatalError = true;
    diags.emplace_back(TIDL_ModelDiagnostic::DK_NotSupported, "[TIDL_ArgMaxLayer] "
       "%s Only supports axis == 1.", layerPC.name);
  }
}


static void checkDequantizeLayers(const sTIDL_LayerPC_t &layerPC, DiagList_t &diags)
{
  const sTIDL_DequantizeParams_t& params = layerPC.layerPCParams.dequantParams;
  bool validated = false;
  bool warning = false;
  bool notOptimal = false;
  bool fatalError = false;

  /*
   * Not supported
   */
  if((params.scale != 1.0 || params.zeroPoint != 0.0 ))
  {
    fatalError = true;
    diags.emplace_back(TIDL_ModelDiagnostic::DK_NotSupported, "[TIDL_DequantizeLayer] "
       "%s scale = 1.0 and zeroPoint= 0.0 are supported now", layerPC.name);
  }
}

static void checkCastLayers(const sTIDL_LayerPC_t &layerPC, DiagList_t &diags)
{
  bool validated = false;
  bool warning = false;
  bool notOptimal = false;
  bool fatalError = false;
  /*
   * Not supported
   */
  if(gParams.addDataConvertToNet == 0)
  {
    fatalError = true;
    diags.emplace_back(TIDL_ModelDiagnostic::DK_NotSupported, "[TIDL_CastLayer] "
       "%s is NOT supported now", layerPC.name);
  }
}


static void checkSoftmaxLayers(const sTIDL_LayerPC_t &layerPC, DiagList_t &diags)
{
  const sTIDL_SoftMaxParams_t& params = layerPC.layerParams.softMaxParams;
  bool validated = false;
  bool warning = false;
  bool notOptimal = false;
  bool fatalError = false;

  /*
   * Not supported
   */
  if(/* layerPC.inData[0].dimValues[0] != 1 || */
     layerPC.inData[0].dimValues[1] != 1 ||
     layerPC.inData[0].dimValues[2] != 1)
  {
    fatalError = true;
    diags.emplace_back(TIDL_ModelDiagnostic::DK_NotSupported, "[TIDL_SoftMaxLayer] "
       "%s input shape of softmax must be 1x1x1xN.", layerPC.name);
  }
}

static void checkConcatenateLayers(const sTIDL_LayerPC_t &layerPC, DiagList_t &diags)
{
  const sTIDL_ConcatParams_t &concatParams = layerPC.layerParams.concatParams;

  // Not supported, except concatenate on channel axis
  if (concatParams.axis != 1) // channel axis is 1 for NCHW
  {
    diags.emplace_back(TIDL_ModelDiagnostic::DK_NotSupported, "[TIDL_ConcatLayer] "
       "Concatenate axis must be channels.");
  }
}

static void checkClipLayers(const sTIDL_LayerPC_t &layerPC, DiagList_t &diags)
{
  const sTIDL_ActParams_t &actParams = layerPC.actParams;

  // Not supported, except min <= 0 and max > 0
  if(!((actParams.clipMin <= 0) && (actParams.clipMax > 0)))
  {
    diags.emplace_back(TIDL_ModelDiagnostic::DK_NotSupported, "[TIDL_ClipLayer] "
       "Clip must have min <= 0 and max > 0.");
  }
}

static void checkReshapeLayers(const sTIDL_LayerPC_t &layerPC, DiagList_t &diags)
{
  int32_t numDims = layerPC.weights.bufSize;

  int32_t isSupported = 1;
  bool fatalError = false;
  
  if(gParams.modelType == TIDL_IMPORT_MODEL_FORMAT_TFLITE_RT) 
  {
    const int32_t * weightPtr = (int32_t *)layerPC.weights.ptr;
    for(int32_t i = 0; i < numDims - 1; i++)
    {
      if((weightPtr[i] != 1))
      {
        isSupported = 0;
      }
    }
    if((numDims == 2) && ((weightPtr[0] == -1) || (weightPtr[1] == -1))) // hack for 2 dims, need a better method to check for more dims
    {
      isSupported = 1;
    }
  }
  else if(gParams.modelType == TIDL_IMPORT_MODEL_FORMAT_ONNX_RT)
  {
    const int64_t * weightPtr = (int64_t *)layerPC.weights.ptr;
    for(int32_t i = 0; i < numDims - 1; i++)
    {
      if((weightPtr[i] != 1))
      {
        isSupported = 0;
      }
    }
    if((numDims == 2) && ((weightPtr[0] == -1) || (weightPtr[1] == -1))) // hack for 2 dims, need a better method to check for more dims
    {
      isSupported = 1;
    }
  }

  if(numDims > 4)
  {
    isSupported = 0;
  }
  
  if(gParams.modelType == 0 || gParams.modelType == 1 || gParams.modelType == 2 || gParams.modelType == 3) //not supposed to be there in network if not open source runtime
  {
    isSupported = 0;
    diags.emplace_back(TIDL_ModelDiagnostic::DK_NotSupported, "[TIDL_ReshapeLayer] "
       "%s should be removed in import process. If not, this model will not work!", layerPC.name);
    return;
  }
  /*
   * Not supported
   */
  if(!isSupported)
  {
    fatalError = true;
    diags.emplace_back(TIDL_ModelDiagnostic::DK_NotSupported, "[TIDL_ReshapeLayer] "
       "%s Only N th dimension can be non-zero, first N-1 dimension must be 1, only flatten reshape layer supported.", layerPC.name);
  }
}

static void checkSqueezeLayers(const sTIDL_LayerPC_t &layerPC, DiagList_t &diags)
{
  if(gParams.modelType == 0 || gParams.modelType == 1 || gParams.modelType == 2 || gParams.modelType == 3) //not supposed to be there in network if not open source runtime
  {
    diags.emplace_back(TIDL_ModelDiagnostic::DK_NotSupported, "[TIDL_SqueezeLayer] "
       "%s should be removed in import process. If not, this model will not work!", layerPC.name);
    return;
  }
  else
  {
    //supported
  }
  
}
// This function is used to post an error for layers that should be removed by the import process.
static void checkFoldedLayers(const sTIDL_LayerPC_t &layerPC, DiagList_t &diags)
{
  diags.emplace_back(TIDL_ModelDiagnostic::DK_NotSupported, "[%s] "
     "%s should be removed in import process. If not, this model will not work!",
     TIDL_LayerString[layerPC.layerType], layerPC.name);
}

static void checkQuantStatsAvailable(sTIDL_Network_t * resultTIDLNetStructure, sTIDL_OrgNetwork_t * orgTIDLNetStructure, DiagList_t &diags)
{
  if(resultTIDLNetStructure->isQuantStatsAvailable == 0)
  {
    diags.emplace_back(TIDL_ModelDiagnostic::DK_NotSupported, "TIDL_E_QUANT_STATS_NOT_AVAILABLE] "
       "tidl_quant_stats_tool.out fails to collect dynamic range. Please look into quant stats log. This model will get fault on target.");
  }
}


static void checkDataflowInfoAvailable(sTIDL_Network_t * resultTIDLNetStructure, sTIDL_OrgNetwork_t * orgTIDLNetStructure, DiagList_t &diags)
{
  if(resultTIDLNetStructure->dataFlowInfo == 0)
  {
    diags.emplace_back(TIDL_ModelDiagnostic::DK_PCOnly, "[TIDL_E_DATAFLOW_INFO_NULL] "
       "ti_cnnperfsim.out fails to allocate memory in MSMC. Please look into perfsim log. This model can only be used on PC emulation, it will get fault on target.");
  }
}

int tidlModelCheck(tidl_import_config * params, sTIDL_OrgNetwork_t * orgTIDLNetStructure)
{
  sTIDL_Network_t * resultNetStructure;
  int32_t ret;

  FILE* fp = fopen((char*)params->outputNetFile, "rb");
  if(fp == NULL)
  {
    printf("ERROR  : TIDL Model Check cannot open the result network file %s.\n", params->outputNetFile);
    return 1;
  }

  resultNetStructure = (sTIDL_Network_t*)malloc(sizeof(sTIDL_Network_t));

  ret = fread(resultNetStructure, 1, sizeof(sTIDL_Network_t), fp);

  is16bit = params->numFeatureBits == 16;

  if(gParams.debugTraceLevel > 0)
  {
    printf("****************************************************\n");
    printf("**               TIDL Model Checker               **\n");
    printf("****************************************************\n");
  }

  // Check each layer for legality and accumulate diagnostics
  DiagList_t diags;
  int32_t errorCount = 0;
  int32_t warningCount = 0;

  for (int i = 0; i < orgTIDLNetStructure->numLayers; i++)
    tidlModelCheckLayer(orgTIDLNetStructure->TIDLPCLayers[i], diags);

  // Check global model properties
  checkQuantStatsAvailable(resultNetStructure, orgTIDLNetStructure, diags);
  checkDataflowInfoAvailable(resultNetStructure, orgTIDLNetStructure, diags);

  // Report any errors or warnings
  for (auto diag : diags)
  {
    diag.emit();
    if (diag.isError())
       ++errorCount;
    else
       ++warningCount;
  }

  if(diags.empty())
  {
    printf("****************************************************\n");
    printf("**                ALL MODEL CHECK PASSED          **\n");
    printf("****************************************************\n\n");
  }
  else
  {
    printf("****************************************************\n");
    printf("**        %3d WARNINGS        %3d ERRORS          **\n", warningCount, errorCount);
    printf("****************************************************\n");
  }

  return errorCount;
}


// Check a single layer for legality and enque any errors or warnings. This function is the basis
// of both the model check pass and the allowlist API used by external frameworks.
void tidlModelCheckLayer(const sTIDL_LayerPC_t &layerPC, DiagList_t &diags)
{
  switch (layerPC.layerType)
  {
    case TIDL_DataLayer:
      // fully supported
      break;
    case TIDL_ConvolutionLayer:
      checkConvLayers(layerPC, diags);
      break;
    case TIDL_PoolingLayer:
      if (gParams.modelType != TIDL_IMPORT_MODEL_FORMAT_ONNX_RT)
        checkPoolingLayers(layerPC, diags);
      break;
    case TIDL_ReLULayer:
      // should be merged or folded out
      checkFoldedLayers(layerPC, diags);
      break;
    case TIDL_PReLULayer:
      // should be merged or be folded out
      checkFoldedLayers(layerPC, diags);
      break;
    case TIDL_SigmoidLayer:
      // should be merged or be folded out
      // This layer has no specific check, and is converted to BN layer, so mark as supported for runtimes allowlisting, since it won't be folded during first allowlisting pass
      if ((gParams.modelType != TIDL_IMPORT_MODEL_FORMAT_ONNX_RT) && (gParams.modelType != TIDL_IMPORT_MODEL_FORMAT_TFLITE_RT))
        checkFoldedLayers(layerPC, diags);
      break;
    case TIDL_EltWiseLayer:
      checkEltwiseLayers(layerPC, diags);
      break;
    case TIDL_InnerProductLayer:
      checkInnerProductLayers(layerPC, diags);
      break;
    case TIDL_SoftMaxLayer:
      if (gParams.modelType != TIDL_IMPORT_MODEL_FORMAT_ONNX_RT)
        checkSoftmaxLayers(layerPC, diags);
      break;
    case TIDL_BatchNormLayer:
      checkBatchNormLayers(layerPC, diags);
      break;
    case TIDL_BiasLayer:
      // should be merged or be folded out
      checkFoldedLayers(layerPC, diags);
      break;
    case TIDL_ScaleLayer:
      checkFoldedLayers(layerPC, diags);
      break;
    case TIDL_Deconv2DLayer:
      checkDeconvLayers(layerPC, diags);
      break;
    case TIDL_ConcatLayer:
      checkConcatenateLayers(layerPC, diags);
      break;
    case TIDL_SplitLayer:
      // should be merged or be folded out
      checkFoldedLayers(layerPC, diags);
      break;
    case TIDL_SliceLayer:
      checkSliceLayers(layerPC, diags);
      break;
    case TIDL_CropLayer:
      // fully supported
      break;
    case TIDL_FlattenLayer:
      checkFlattenLayers(layerPC, diags);
      break;
    case TIDL_DropOutLayer:
      // should be folded out
      checkFoldedLayers(layerPC, diags);
      break;
    case TIDL_ArgMaxLayer:
      checkArgMaxLayers(layerPC, diags);
      break;
    case TIDL_DetectionOutputLayer:
      checkDetectionOutLayers(layerPC, diags);
      break;
    case TIDL_ShuffleChannelLayer:
      checkShuffleChannelLayers(layerPC, diags);
      break;
    case TIDL_ResizeLayer:
      checkResizeLayers(layerPC, diags);
      break;
    case TIDL_RoiPoolingLayer:
      // fully supported
      break;
    case TIDL_OdPostProcessingLayer:
      // fully supported
      break;
    case TIDL_DepthToSpaceLayer:
      checkDepthToSpaceLayers(layerPC, diags);
    case TIDL_ColorConversionLayer:
      checkColorConversionLayers(layerPC, diags);
    case TIDL_PadLayer:
      // fully supported
      break;
    case TIDL_OdOutputReformatLayer:
      //fully supported
      break;
    case TIDL_DataConvertLayer:
      //fully supported
      break;
    case TIDL_BatchToSpaceLayer:
      checkBatchToSpaceLayers(layerPC, diags);
      break;
    case TIDL_SpaceToBatchLayer:
      checkBatchToSpaceLayers(layerPC, diags);
      break;
    case TIDL_BatchReshapeLayer:
      // fully supported
      break;
    case TIDL_CustomLayer:
      // fully supported
      break;
    case TIDL_UnsupportedLayer:
      // already reported
      break;

    // These layerTypes are represented only in the PC imoprt data structure, not in the target model.
    // None of them are supported.
    case TIDL_ConstDataLayer:
      checkFoldedLayers(layerPC, diags);
      break;
    case TIDL_PriorBoxLayer:
      checkPriorBoxLayers(layerPC, diags);
      break;
    case TIDL_PermuteLayer:
      checkPermuteLayers(layerPC, diags);
      break;
    case TIDL_ReshapeLayer:
      //if (gParams.modelType != TIDL_IMPORT_MODEL_FORMAT_ONNX_RT)
        checkReshapeLayers(layerPC, diags);
      break;
    case TIDL_ShapeLayer:
      checkFoldedLayers(layerPC, diags);
      break;
    case TIDL_SqueezeLayer:
      checkSqueezeLayers(layerPC, diags);
      break;
    case TIDL_TransposeLayer:
      if (gParams.modelType != TIDL_IMPORT_MODEL_FORMAT_ONNX_RT)
        checkFoldedLayers(layerPC, diags);
      break;
    case TIDL_DequantizeLayer:
      checkDequantizeLayers(layerPC, diags);
      break;
    case TIDL_CastLayer:
      checkCastLayers(layerPC, diags);
      break;
    case TIDL_ClipLayer:
      checkFoldedLayers(layerPC, diags);
      break;
    case TIDL_MinimumLayer:
      checkFoldedLayers(layerPC, diags);
      break;
    case TIDL_LeakyReluLayer:
      if (gParams.modelType != TIDL_IMPORT_MODEL_FORMAT_ONNX_RT)
        checkFoldedLayers(layerPC, diags);
      break;

    default:
      diags.emplace_back(TIDL_ModelDiagnostic::DK_NotSupported,
        "unknown layer type: %d", layerPC.layerType);
  }
}

// This function is called by the allowlist interface to decide if a node can/should be supported
// as part of a subgraph offloaded to TIDL.
bool tidlModelCheckOffload(const tidl_import_config &params, const sTIDL_LayerPC_t &layerPC)
{
  DiagList_t diags;
  is16bit = params.numFeatureBits == 16;

  // Check layers which are folded out during import. They need to be checked separately here
  // because tidlModelCheckLayer() assumes folded layers are not present in the optimized graph.
  // These checking can be moved to tidlModelCheckLayer() in future if tidlModelCheckLayer() is
  // called before optimization which does layer folding.
  // Relay op "clip" is mapped to either TIDL_ReLULayer with Relu6 or TIDL_ClipLayer. Relu6 has 
  // no constraints but TIDL_ClipLayer has.
  if(layerPC.layerType == TIDL_ReLULayer)
    ; // this is needed because tidlModelCheckLayer() would flag this as error
  else if (layerPC.layerType == TIDL_ArgMaxLayer)
    ; // skip calling tidlModelCheckLayer which checks output dimension that is not available now
  else if (layerPC.layerType == TIDL_BiasLayer)
    ; // will be folded or converted to BatchNorm
  else if (layerPC.layerType == TIDL_ClipLayer)
    checkClipLayers(layerPC, diags);
  // Check layers which are not folded out during import
  else if (layerPC.layerType == TIDL_IdentityLayer)
    ; // PC only layer, skip checking, it will be merged in optimizeNet()
  else
    tidlModelCheckLayer(layerPC, diags);

  if(diags.size() == 0)
  {
    diagsInfo.push_back("");
  }

  for (auto diag : diags)
  {
    std::pair<const char *, std::string> pair = diag.extractDiagMsg();
    std::string infoStr = std::string(pair.first) + " -- " + pair.second;
    diagsInfo.push_back(infoStr);
    
    if (!diag.isOKToOffload())
    {
      if (params.debugTraceLevel > 0)  diag.emit();
      return false;
    }
  }

  return true;
}

int tidlInputTensorDimCheck(sTIDL_OrgNetwork_t &orgTIDLNetStructure)
{
  for (int i1 = 0; i1 < orgTIDLNetStructure.numLayers; i1++)
  {
    if(orgTIDLNetStructure.TIDLPCLayers[i1].layerType == TIDL_DataLayer)
    {
      for (int i2 = 0; i2 < orgTIDLNetStructure.TIDLPCLayers[i1].numOutBufs; i2++)
      {
        for (int i3 = 0; i3 < TIDL_DIM_MAX; i3++)
        {
          if(orgTIDLNetStructure.TIDLPCLayers[i1].outData[i2].dimValues[i3] <= 0)
          {
              printf("****************************************************\n");
              printf("**   All the Input Tensor Dimensions has to be greater then Zero \n");
              printf("**   DIM Error - For Tensor %d, Dim %d is %d\n", orgTIDLNetStructure.TIDLPCLayers[i1].outData[i2].dataId, i3, orgTIDLNetStructure.TIDLPCLayers[i1].outData[i2].dimValues[i3]);
              printf("****************************************************\n");
              return 0;
          }
        }
      }
    }
  }
  return 1;
}

int tidlModelTensorDimCheck(sTIDL_OrgNetwork_t &orgTIDLNetStructure)
{
  for (int i1 = 0; i1 < orgTIDLNetStructure.numLayers; i1++)
  {
    for (int i2 = 0; i2 < orgTIDLNetStructure.TIDLPCLayers[i1].numOutBufs; i2++)
    {
      for (int i3 = 0; i3 < TIDL_DIM_MAX; i3++)
      {
        if(orgTIDLNetStructure.TIDLPCLayers[i1].outData[i2].dimValues[i3] <= 0)
        {
            printf("****************************************************\n");
            printf("**   All the Tensor Dimensions has to be greater then Zero \n");
            printf("**   DIM Error - For Tensor %d, Dim %d is %d\n", orgTIDLNetStructure.TIDLPCLayers[i1].outData[i2].dataId, i3, orgTIDLNetStructure.TIDLPCLayers[i1].outData[i2].dimValues[i3]);
            printf("****************************************************\n");
            return 0;
        }
      }
    }
  }
  return 1;
}
