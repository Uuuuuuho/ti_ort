/*
*
* Copyright (c) {2015 - 2020} Texas Instruments Incorporated
*
* All rights reserved not granted herein.
*
* Limited License.
*
* Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
* license under copyrights and patents it now or hereafter owns or controls to make,
* have made, use, import, offer to sell and sell ("Utilize") this software subject to the
* terms herein.  With respect to the foregoing patent license, such license is granted
* solely to the extent that any such patent is necessary to Utilize the software alone.
* The patent license shall not apply to any combinations which include this software,
* other than combinations with devices manufactured by or for TI ("TI Devices").
* No hardware patent is licensed hereunder.
*
* Redistributions must preserve existing copyright notices and reproduce this license
* (including the above copyright notice and the disclaimer and (if applicable) source
* code license limitations below) in the documentation and/or other materials provided
* with the distribution
*
* Redistribution and use in binary form, without modification, are permitted provided
* that the following conditions are met:
*
* *       No reverse engineering, decompilation, or disassembly of this software is
* permitted with respect to any software provided in binary form.
*
* *       any redistribution and use are licensed by TI for use only with TI Devices.
*
* *       Nothing shall obligate TI to provide you with source code for the software
* licensed and provided to you in object code.
*
* If software source code is provided to you, modification and redistribution of the
* source code are permitted provided that the following conditions are met:
*
* *       any redistribution and use of the source code, including any resulting derivative
* works, are licensed by TI for use only with TI Devices.
*
* *       any redistribution and use of any object code compiled from the source code
* and any resulting derivative works, are licensed by TI for use only with TI Devices.
*
* Neither the name of Texas Instruments Incorporated nor the names of its suppliers
*
* may be used to endorse or promote products derived from this software without
* specific prior written permission.
*
* DISCLAIMER.
*
* THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
* OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
* OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
* IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
* BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
* OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
* OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
* OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

#include "tidl_runtimes_import_common.h"
#include <bits/stdc++.h>

extern "C"
{

/** Iterative implementation of DFS on graph */
void TIDL_nodeTraversal(std::vector<std::vector<int>> adjacencyList, int startIdx, std::vector<int> &odBackboneNodes, std::vector<bool> &visited)
{
  // Mark the current node as visited
  visited[startIdx] = true;
  odBackboneNodes.push_back(startIdx);

  // Create a stack for DFS
  std::stack<int> stack;
  // Push the current source node
  stack.push(startIdx);

  while (!stack.empty())
  {
    // Pop the topmost node from stack
    startIdx = stack.top();
    stack.pop();

    if (!visited[startIdx])
    {
      odBackboneNodes.push_back(startIdx);
      visited[startIdx] = true;
    }

    // Get all adjacent nodes of the popped node 
    // If an adjacent has not been visited, then push it
    // to the stack.
    for (int i = 0; i < adjacencyList[startIdx].size(); i++)
    {
      if (visited[adjacencyList[startIdx][i]] == false)
            stack.push(adjacencyList[startIdx][i]);
    }
  }
}

/** This function calls DFS with each of the OD heads as root node */
std::vector<int> TIDL_callNodeTraversal(std::vector<std::vector<int>> adjacencyList, std::vector<int> postProcInputIds, int graphSize)
{
  std::vector<bool> visited;
  visited.assign(graphSize, false);
  
  std::vector<int> odBackboneNodes = {};

  for(auto inIds : postProcInputIds)  //Trace back from each of the OD post proc inputs/ heads
  {
    TIDL_nodeTraversal(adjacencyList, inIds, odBackboneNodes, visited);
  }
  printf("Number of OD backbone nodes = %d \n", std::count(visited.begin(), visited.end(), true));
  return odBackboneNodes;
}

void TIDL_runGraphvizToolRuntimes(std::string tidlToolsPath, std::string artifactsFolderPath, int32_t debugLevel)
{
  std::string allowlistPath = artifactsFolderPath + "/allowedNode.txt";
  std::string graphvizInfoPath = artifactsFolderPath + "/tempDir/graphvizInfo.txt";
  std::string outputPath = artifactsFolderPath + "/tempDir/runtimes_visualization.svg";

  std::string sysCommandStr = tidlToolsPath + "/tidl_graphVisualiser_runtimes.out " + allowlistPath + " " + graphvizInfoPath + " " + outputPath;
  
  char * sysCommand = new char[sysCommandStr.length() + 1];
  strcpy(sysCommand, sysCommandStr.c_str()); 
  
  system(sysCommand);

  if(debugLevel > 0)
  {
    printf("Running runtimes graphviz - %s \n", sysCommand);
  }
  
  delete [] sysCommand;
}

} //extern "C"