/*
 * Copyright (c) 2018-2019, Texas Instruments Incorporated
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * *  Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *
 * *  Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * *  Neither the name of Texas Instruments Incorporated nor the names of
 *    its contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 * OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
 * OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
 * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

//
//File Name: launch_tidl.js
//Description:
//   Connects and loads binary of A72 and C7x and runs both and breaks at test_ti_dl_ivison
//
//Usage:
//
//From CCS Scripting console
//  1. loadJSFile "<tidl_repo>/ti_dl/utils/targetTest/launch_tidl.js"
//
//Note:
//  1. Search for "edit this" to look at changes that need to be edited
//     for your usage.
//

ip = "10.24.69.33";
var dots = ip.split('.');
var host_ip_address = dots[0]<<24 | dots[1]<<16 | dots[2]<<8 | dots[3];
var sd_card_fs = 1

//<!!!!!! EDIT THIS !!!!!>
function sleep(ms) {
    var unixtime_ms = new Date().getTime();
    while(new Date().getTime() < unixtime_ms + ms) {}
 }
//TIDL path. Edit this
tidlPath = "D:/work/vision/c7x-mma-tidl/";

//path to board config elf
if(sd_card_fs == 1)
{
    fastfileIO_a72_out_file = tidlPath+"/ti_dl/utils/targetTest/vx_app_rtos_fileio_mpu1.out"
}
else
{   
    fastfileIO_a72_out_file = tidlPath+"/ti_dl/utils/targetTest/cpsw_nimu_example_mpu1_0_release.xa72fg"
}
tidl_out_file = tidlPath+"/ti_dl/test/TI_DEVICE_dsp_test_dl_algo.out"
server_exe = tidlPath+"ti_dl/test/";


//<!!!!!! EDIT THIS !!!!!>

// Import the DSS packages into our namespace to save on typing
importPackage(Packages.com.ti.debug.engine.scripting)
importPackage(Packages.com.ti.ccstudio.scripting.environment)
importPackage(Packages.java.lang)
importPackage(java.io);
importPackage(java.lang);

function updateScriptVars()
{
    //Open a debug session
    dsCoretexA72_0 = debugServer.openSession( ".*CortexA72_0_0" );
    dsC71_0 = debugServer.openSession( ".*C71X_0" );
}

function printVars()
{
    updateScriptVars();
}

function connectTargets()
{
    updateScriptVars();
    print("Connecting to C71!");
    dsC71_0.target.connect();
    print("Connecting to A72!");
    dsCoretexA72_0.target.connect();

    print("Loading TIDL binary to C7x");
    dsC71_0.memory.loadProgram(tidl_out_file);
    print("Loading FastFile IO binary to A72");
    dsCoretexA72_0.memory.loadProgram(fastfileIO_a72_out_file);

    if(sd_card_fs != 1)
    {
        dsCoretexA72_0.memory.writeWord(0, 0x81050000, host_ip_address);
    }
    var tidlMultiInstanceTest = dsC71_0.symbol.getAddress("tidlMultiInstanceTest");
    dsC71_0.breakpoint.add(tidlMultiInstanceTest);
    
    dsC71_0.target.runAsynch();
    sleep(200)
    dsCoretexA72_0.target.runAsynch();
}

function disconnectTargets()
{
    updateScriptVars();
    // Disconnect targets
    dsDMSC_0.target.disconnect();
    // Reset the R5F to be in clean state.
    //dsMCU1_0.target.reset();
}

function doEverything()
{
    printVars();
    //java.lang.Runtime.getRuntime().exec(server_exe+"ti_file_io_server.exe", null, new File(server_exe));
    connectTargets();
    print("Okay TIDL you are good to go.. !");
    dsC71_0.target.waitForHalt()
    //java.lang.Runtime.getRuntime().exec("taskkill /IM ti_file_io_server.exe /T /F");
}

var ds;
var debugServer;
var script;

// Check to see if running from within CCSv4 Scripting Console
var withinCCS = (ds !== undefined);

// Create scripting environment and get debug server if running standalone
if (!withinCCS)
{
    // Import the DSS packages into our namespace to save on typing
    importPackage(Packages.com.ti.debug.engine.scripting);
    importPackage(Packages.com.ti.ccstudio.scripting.environment);
    importPackage(Packages.java.lang);

    // Create our scripting environment object - which is the main entry point into any script and
    // the factory for creating other Scriptable ervers and Sessions
    script = ScriptingEnvironment.instance();

    // Get the Debug Server and start a Debug Session
    debugServer = script.getServer("DebugServer.1");
}
else // otherwise leverage existing scripting environment and debug server
{
    debugServer = ds;
    script = env;
}

doEverything();
