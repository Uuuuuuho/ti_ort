#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include<sys/socket.h>
#include<arpa/inet.h>	//inet_addr
#include<unistd.h>

#define USE_HOST_FILE_IO

#include "ti_file_io.h"
#include "ti_file_io_msg.h"

int socket_desc , clientSock;

typedef struct ServerFileStruct
{
	int available;
	unsigned int fd;
} ServerFileStruct;


int sendData(int SocketId, char *dataBuf, unsigned int  dataSize)
{
  int actDataSize=0;

  while(dataSize > 0 ) {
    actDataSize = send(SocketId, dataBuf, dataSize, 0);
    if(actDataSize<=0)
    {
      break;
    }
    dataBuf += actDataSize;
    dataSize -= actDataSize;
  }

  return(actDataSize);
}


int recvData(int SocketId, char *dataBuf, unsigned int  dataSize)
{
  int actDataSize=0;

  while(dataSize > 0 ) {
    actDataSize = recv(SocketId, dataBuf, dataSize, 0);
    if(actDataSize<=0)
    {
      break;
    }
    dataBuf += actDataSize;
    dataSize -= actDataSize;
  }

  return(actDataSize);
}


int searchFileStruct(struct ServerFileStruct fileStruct[], unsigned int fd_arg)
{
    int i;
        for(i = 0; i < MAX_NUM_FILES; i++)
        {
            if( (fileStruct[i].available == 1) && (fileStruct[i].fd == fd_arg))
            {
                return i;
            }
        }
    return -1;
}


int getFreeIndex(struct ServerFileStruct fileStruct[])
{
    int i;
        for(i = 0; i < MAX_NUM_FILES; i++)
        {
            if( fileStruct[i].available == 0 )
            {
                return i;
            }
        }
    return -1;
}



int execFileIO()
{
    printf("Connected!\n");


    char * rdata; //stores received function from client
    char * buffer;
    int i, r;
    struct ServerFileStruct openFileInfo[MAX_NUM_FILES];

    rdata = (char *) malloc(sizeof(char) * sizeof(OpStruct_t));

    //initialize openFileInfo
    for(i = 0; i < MAX_NUM_FILES; i++)
    {
        openFileInfo[i].available = 0;
        openFileInfo[i].fd = -1;
    }

    for(;;)
    {

        //receive the function to execute
        r = recvData(clientSock, rdata, (sizeof(char) * sizeof(OpStruct_t)));

        if(r <= 0)
        {
          break;
        }
		if(r > 0)
		{
        OpStruct_t *opStruct = (OpStruct_t *) rdata;

        if (opStruct->opCode == TI_FILEIO_OPCODE_FOPEN)
        {
            //function is fopen
            printf("Request to open file %s in %s mode. ", opStruct->fileName, opStruct->mode);
            FILE * fp = fopen(opStruct->fileName, opStruct->mode);
            if (fp == NULL)
            {
                 fputs ("FOPEN : Cannot open file\n",stderr);
            }
            struct HOSTFILE *hostFile = (struct HOSTFILE*) malloc(sizeof( HOSTFILE));
            hostFile->id = fp;

            printf("File ID =  Ox%x, %d, %d\n", hostFile->id, hostFile->id, fp);

            sendData(clientSock, (char *) hostFile, (sizeof(char) * sizeof(HOSTFILE)));

            //set the details in ServerFileStruct openFileInfo
            if(fp)
            {
                i = getFreeIndex(openFileInfo);
                openFileInfo[i].available = 1;
                openFileInfo[i].fd = (unsigned int)hostFile->id;
            }

            free(hostFile);
        }


        else if (opStruct->opCode == TI_FILEIO_OPCODE_FCLOSE)
        {
            //function is fclose
            printf("Request to close file %d\n", opStruct->fid);
            r = fclose( (FILE *) (opStruct->fid));
            struct RETVALUE *retPtr = (struct RETVALUE*) malloc(sizeof( RETVALUE));
            retPtr->ret = r;
            if (r == 0)
            {
                i = searchFileStruct(openFileInfo, (unsigned int)opStruct->fid);
                openFileInfo[i].available = 0;
                openFileInfo[i].fd = -1;
            }
            sendData(clientSock, (char *) retPtr, (sizeof(char) * sizeof(RETVALUE)));

            free(retPtr);

        }


        else if (opStruct->opCode == TI_FILEIO_OPCODE_FREAD)
        {
            //function is fread
            int total = (opStruct->size) * (opStruct->count);
            printf("Request to read %d bytes from file %d \n",total, (unsigned int)opStruct->fid);
            buffer = (char*) malloc (sizeof(char)*total);
            if (buffer == NULL) {
                fputs ("FREAD : Cannot allocate memory\n",stderr);
            }


            //check if file is open
            i = searchFileStruct(openFileInfo, (unsigned int)opStruct->fid);
            if(i == -1)     //if file is not open
            {
                fputs ("FREAD : File not open\n", stderr);
            }


            int res = fread (buffer, opStruct->size, opStruct->count, (FILE*) opStruct->fid);

            sendData(clientSock, buffer, total);
            free(buffer);
        }

        else if (opStruct->opCode == TI_FILEIO_OPCODE_FWRITE)
        {
            //function is fwrite

            int total = (opStruct->size) * (opStruct->count);
            printf("Request to write %d bytes to file %d \n",total, opStruct->fid);
            buffer = (char*) malloc (sizeof(char)*total);
            if (buffer == NULL) {
                fputs ("FWRITE : Cannot allocate memory\n",stderr);
            }

            //check if file is open
            i = searchFileStruct(openFileInfo, (unsigned int)opStruct->fid);
            if(i == -1)     //if file is not open
            {
                fputs ("FWRITE : File not open\n", stderr);
            }

            recvData(clientSock, buffer, total);

            int res = fwrite (buffer, opStruct->size, opStruct->count, (FILE*) opStruct->fid);
            if (res != total)
            {
                fputs ("Fwrite error\n",stderr);
            }

            free(buffer);

        }

        else if (opStruct->opCode == TI_FILEIO_OPCODE_FSEEK)
        {
            //function is fseek

            //check if file is open
            i = searchFileStruct(openFileInfo, (unsigned int)opStruct->fid);
            if(i == -1)     //if file is not open
            {
                fputs ("FSEEK : File not open\n", stderr);
            }

            printf("Request to seek %d bytes in mode %d from file %d \n",opStruct->offset, opStruct->count, opStruct->fid);

            r = fseek ((FILE*) opStruct->fid, opStruct->offset, opStruct->count);

            struct RETVALUE *retPtr = (struct RETVALUE*) malloc(sizeof(struct RETVALUE));
            retPtr->ret = r;

            sendData(clientSock, (char *) retPtr, (sizeof(char) * sizeof(RETVALUE)));

            free(retPtr);
        }

        else if (opStruct->opCode == TI_FILEIO_OPCODE_FTELL)
        {
            //function is ftell

            //check if file is open
            i = searchFileStruct(openFileInfo, (unsigned int)opStruct->fid);
            if(i == -1)     //if file is not open
            {
                fputs ("FTELL : File not open\n", stderr);
            }

            printf("Request to ftell from file %d \n",opStruct->fid);

            long int res = ftell ((FILE*) opStruct->fid);

            struct RETVALUE *retPtr = (struct RETVALUE*) malloc(sizeof(struct RETVALUE));
            retPtr->ret = res;

            sendData(clientSock, (char *) retPtr, (sizeof(char) * sizeof(RETVALUE)));

            free(retPtr);
        }

        else if (opStruct->opCode == TI_FILEIO_OPCODE_FGETS)
        {
            //function is fgets

            //check if file is open
            i = searchFileStruct(openFileInfo, (unsigned int)opStruct->fid);
            if(i == -1)     //if file is not open
            {
                fputs ("FGETS : File not open\n", stderr);
            }


            buffer = (char*) malloc (sizeof(char) * ((opStruct->size)));
            buffer = fgets(buffer, opStruct->size, (FILE*) opStruct->fid);

            sendData(clientSock, buffer, (sizeof(char) * ((opStruct->size))));

            free(buffer);
        }
		}
    }

    free(rdata);

    return 0;
}

int main(int argc , char *argv[])
{
    int c;
	struct sockaddr_in server , client;
    char * message;
    char *client_ip;
    int ret;
	
	//Create socket
	socket_desc = socket(AF_INET , SOCK_STREAM , IPPROTO_TCP);
	if (socket_desc == -1)
	{
		printf("Could not create socket");
	}
	
	//Prepare the sockaddr_in structure
	server.sin_family = AF_INET;
	server.sin_addr.s_addr = INADDR_ANY;
	server.sin_port = htons(9999);
	
	//Bind
	if( bind(socket_desc,(struct sockaddr *)&server , sizeof(server)) < 0)
	{
		printf("bind failed\n");
	}
	printf("bind done\n");
	
	//Listen
	ret = listen(socket_desc , 128);
    printf("listen done.. %d\n", ret);
	
    for(;;)
    {
        //Accept and incoming connection
        printf("Waiting for incoming connections...\n");
        c = sizeof(struct sockaddr_in);
        clientSock = accept(socket_desc, (struct sockaddr *)&client, (socklen_t*)&c);
        if (clientSock<0)
        {
            perror("accept failed\n");
        }
        client_ip = inet_ntoa(client.sin_addr);
        printf("Connection accepted from %s \n", client_ip);
        
        execFileIO();
    }
    
    

  	message = "Hello Client , I have received your connection. But I have to go now, bye\n";
	write(clientSock , message , strlen(message));

	return 0;
}