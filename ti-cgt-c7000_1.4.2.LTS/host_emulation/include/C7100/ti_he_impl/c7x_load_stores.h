/*****************************************************************************/
/*  C7X_LOAD_STORES.H                                                     */
/*                                                                           */
/* Copyright (c) 2017 Texas Instruments Incorporated                         */
/* http://www.ti.com/                                                        */
/*                                                                           */
/*  Redistribution and  use in source  and binary forms, with  or without    */
/*  modification,  are permitted provided  that the  following conditions    */
/*  are met:                                                                 */
/*                                                                           */
/*     Redistributions  of source  code must  retain the  above copyright    */
/*     notice, this list of conditions and the following disclaimer.         */
/*                                                                           */
/*     Redistributions in binary form  must reproduce the above copyright    */
/*     notice, this  list of conditions  and the following  disclaimer in    */
/*     the  documentation  and/or   other  materials  provided  with  the    */
/*     distribution.                                                         */
/*                                                                           */
/*     Neither the  name of Texas Instruments Incorporated  nor the names    */
/*     of its  contributors may  be used to  endorse or  promote products    */
/*     derived  from   this  software  without   specific  prior  written    */
/*     permission.                                                           */
/*                                                                           */
/*  THIS SOFTWARE  IS PROVIDED BY THE COPYRIGHT  HOLDERS AND CONTRIBUTORS    */
/*  "AS IS"  AND ANY  EXPRESS OR IMPLIED  WARRANTIES, INCLUDING,  BUT NOT    */
/*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR    */
/*  A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT    */
/*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,    */
/*  SPECIAL,  EXEMPLARY,  OR CONSEQUENTIAL  DAMAGES  (INCLUDING, BUT  NOT    */
/*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,    */
/*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY    */
/*  THEORY OF  LIABILITY, WHETHER IN CONTRACT, STRICT  LIABILITY, OR TORT    */
/*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE    */
/*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.     */
/*                                                                           */
/*****************************************************************************/
#ifndef C7X_LDST_H
#define C7X_LDST_H

#include <c7x.h>

using namespace _c70_he_detail;

/*****************************************************************************
* VLOAD_DUP
*****************************************************************************/

/*****************************************************************************
* Enabled for vtypes and accessibles 
*****************************************************************************/
template<typename ELEM_T,
         size_t NELEM,
         size_t RES_NELEM = 64/sizeof(ELEM_T),
         typename RES_T = vtype<typename std::remove_cv<ELEM_T>::type,RES_NELEM> >
RES_T __vload_dup(vtype_ptr<vtype<ELEM_T, NELEM> > input)
{
    vtype_ptr<vtype<const ELEM_T, NELEM> > cinput = input;
    constexpr size_t num_duplicate = RES_NELEM / NELEM;
    RES_T ret;

    for(size_t i = 0; i < NELEM; i++)
        for(int j = 0; j < num_duplicate; j++)
            ret.s[num_duplicate*i+j] = (*cinput).s[i];
    return ret;
}

/*****************************************************************************
* Enabled if ELEM_T is an arithmetic type only (built-in scalar + floats + 
* doubles). Using decay here to ensure that at the lowest level,  ELEM_T is a 
* primitive type (example int& or const int).
* Note: using c++11 syntax here just in case, c++ 14 would allow 
* std::decay_t<ELEM_T> instead of typename std::decay<ELEM_T>::type
*****************************************************************************/
template<typename ELEM_T, 
         size_t RES_NELEM = 64/sizeof(ELEM_T),
         typename RES_T = vtype<typename std::remove_cv<ELEM_T>::type,RES_NELEM>,
         std::enable_if_t<std::is_arithmetic<ELEM_T>::value, int> = 0>
RES_T __vload_dup(ELEM_T* input)
{
    const ELEM_T *cinput = input;
    RES_T ret;
    
    for(size_t i = 0; i < RES_NELEM; i++)
        ret.s[i] = *cinput;
    return ret;
}

/*****************************************************************************
* VLOAD_DUP_VEC
*****************************************************************************/
template<typename ELEM_T,
         size_t NELEM,
         size_t RES_NELEM = 32,
         typename ELEM_T_NOCV = typename std::remove_cv<ELEM_T>::type,
         typename RES_T = vtype<ELEM_T_NOCV, RES_NELEM>,
         std::enable_if_t<   std::is_same<ELEM_T_NOCV, int16_t>::value
                          || std::is_same<ELEM_T_NOCV, uint16_t>::value, int> = 0>
RES_T __vload_dup_vec(vtype_ptr<vtype<ELEM_T, NELEM> > input)
{
    vtype_ptr<vtype<const ELEM_T, NELEM> > cinput = input;
    // Only defined for short and ushort vectors with 4 or 8 elements
    static_assert((NELEM == 4) || (NELEM == 8),
                  "Input vector may only have 4 or 8 elements");

    // Number of times the "group" (input vec) will be duplicated
    constexpr size_t num_group_duplicates = RES_NELEM / NELEM; 
    RES_T ret;

    for(size_t i = 0; i < num_group_duplicates; i++)
        for(size_t j = 0; j < NELEM; j++)
            ret.s[NELEM*i+j] = (*cinput).s[j];
    return ret;
}

/*****************************************************************************
* VLOAD_UNPACK_SHORT
*****************************************************************************/

/*****************************************************************************
* Requires enable_if as there is a difference between a char* and 
* a signed char* (aka int8_t*)
*****************************************************************************/
template<typename ELEM_T, 
         std::enable_if_t<std::is_signed<ELEM_T>::value &&
                          sizeof(ELEM_T) == sizeof(int8_t), int> = 0>
int16_t __vload_unpack_short(ELEM_T* input)
{
    const ELEM_T *cinput = input;
    return *cinput;
}

/*****************************************************************************
* There is no difference between a uint8_t* and an unsigned char*, 
* so no template is required here
****************************************************************************/
uint16_t __vload_unpack_short(const unsigned char* input);

__cshort __vload_unpack_short(__const_cchar_ptr input);

template<typename ELEM_T,
         size_t NELEM,
         typename ELEM_T_NOCV = typename std::remove_cv<ELEM_T>::type,
         typename RES_T = vtype<int16_t, NELEM>,
         std::enable_if_t<std::is_same<ELEM_T_NOCV, int8_t>::value, int> = 0>
RES_T __vload_unpack_short(vtype_ptr<vtype<ELEM_T, NELEM> > input)
{
    vtype_ptr<vtype<const int8_t, NELEM> > cinput = input;

    static_assert((NELEM <= 32), "Number of elements must be <= 32");

    RES_T ret;

    for(size_t i = 0; i < NELEM; i++)
        ret.s[i] = (*cinput).s[i];
    return ret;
}

template<typename ELEM_T,
         size_t NELEM,
         typename ELEM_T_NOCV = typename std::remove_cv<ELEM_T>::type,
         typename RES_T = vtype<uint16_t, NELEM>,
         std::enable_if_t<std::is_same<ELEM_T_NOCV, uint8_t>::value, int> = 0>
RES_T __vload_unpack_short(vtype_ptr<vtype<ELEM_T, NELEM> > input)
{
    vtype_ptr<vtype<const uint8_t, NELEM> > cinput = input;

    static_assert((NELEM <= 32), "Number of elements must be <= 32");

    RES_T ret;

    for(size_t i = 0; i < NELEM; i++)
        ret.s[i] = (*cinput).s[i];
    return ret;
}

template<size_t NELEM,
         typename RES_T = vtype<__cshort, NELEM> >
RES_T __vload_unpack_short(vtype_ptr<vtype<__cchar, NELEM> > input)
{
    vtype_ptr<vtype<__const_cchar, NELEM> > cinput = input;

    static_assert((NELEM <= 16), "Number of elements must be <= 16");

    RES_T ret;

    for(size_t i = 0; i < NELEM; i++)
        ret.s[i] = __convert_cshort((*cinput).s[i]);
    return ret;
}

/*****************************************************************************
* VLOAD_UNPACK_INT
*****************************************************************************/

/*****************************************************************************
* Only valid for vectors with signed char/short or char/short 
*****************************************************************************/
template<typename ELEM_T,
         size_t NELEM,
         typename RES_T = vtype<int32_t, NELEM>,
         std::enable_if_t<std::is_signed<ELEM_T>::value &&
                          (   sizeof(ELEM_T) == sizeof(int8_t) 
                           || sizeof(ELEM_T) == sizeof(int16_t)), int> = 0>
RES_T __vload_unpack_int(vtype_ptr<vtype<ELEM_T, NELEM> > input)
{
    vtype_ptr<vtype<const ELEM_T, NELEM> > cinput = input;

    static_assert((NELEM <= 16), "Number of elements must be <= 16");

    RES_T ret;

    for(size_t i = 0; i < NELEM; i++)
        ret.s[i] = (*cinput).s[i];
    return ret;
}

template<typename ELEM_T,
         size_t NELEM,
         typename RES_T = vtype<uint32_t, NELEM>,
         std::enable_if_t<std::is_unsigned<ELEM_T>::value &&
                          (   sizeof(ELEM_T) == sizeof(uint8_t) 
                           || sizeof(ELEM_T) == sizeof(uint16_t)), int> = 0>
RES_T __vload_unpack_int(vtype_ptr<vtype<ELEM_T, NELEM> > input)
{
    vtype_ptr<vtype<const ELEM_T, NELEM> > cinput = input;

    static_assert((NELEM <= 16), "Number of elements must be <= 16");

    RES_T ret;

    for(size_t i = 0; i < NELEM; i++)
        ret.s[i] = (*cinput).s[i];
    return ret;
}

template<typename ELEM_T,
         size_t NELEM,
         typename RES_T = vtype<__cint, NELEM>,
         std::enable_if_t<std::is_signed<ELEM_T>::value &&
                          (   sizeof(ELEM_T) == sizeof(int8_t) 
                           || sizeof(ELEM_T) == sizeof(int16_t)), int> = 0>
RES_T __vload_unpack_int(vtype_ptr<vtype<ctype<ELEM_T>, NELEM> > input)
{
    vtype_ptr<vtype<ctype<const ELEM_T>, NELEM> > cinput = input;
    static_assert((NELEM <= 8), "Number of elements must be <= 8");

    RES_T ret;

    for(size_t i = 0; i < NELEM; i++)
        ret.s[i] = __convert_cint((*cinput).s[i]);
    return ret;
}

template<typename ELEM_T,
         std::enable_if_t<std::is_signed<ELEM_T>::value &&
                          (   sizeof(ELEM_T) == sizeof(uint8_t) 
                           || sizeof(ELEM_T) == sizeof(uint16_t)), int> = 0>
int32_t __vload_unpack_int(ELEM_T* input)
{
    const ELEM_T *cinput = input;
    return *cinput;
}

template<typename ELEM_T,
         std::enable_if_t<std::is_unsigned<ELEM_T>::value &&
                          (   sizeof(ELEM_T) == sizeof(uint8_t) 
                           || sizeof(ELEM_T) == sizeof(uint16_t)), int> = 0>
uint32_t __vload_unpack_int(ELEM_T* input)
{
    const ELEM_T *cinput = input;
    return *cinput;
}

template<typename ELEM_T,
         std::enable_if_t<std::is_signed<ELEM_T>::value &&
                          (   sizeof(ELEM_T) == sizeof(uint8_t) 
                           || sizeof(ELEM_T) == sizeof(uint16_t)), int> = 0>
__cint __vload_unpack_int(ctype_ptr<ctype<ELEM_T> > input)
{
    ctype_ptr<ctype<const ELEM_T> > cinput = input;
    return __convert_cint(*cinput);
}

/*****************************************************************************
* VLOAD_UNPACK_LONG
*****************************************************************************/
template<typename ELEM_T,
         size_t NELEM,
         typename RES_T = vtype<int64_t, NELEM>,
         std::enable_if_t<!std::is_floating_point<ELEM_T>::value &&
                          std::is_signed<ELEM_T>::value &&
                          (   sizeof(ELEM_T) == sizeof(int8_t)
                           || sizeof(ELEM_T) == sizeof(int16_t) 
                           || sizeof(ELEM_T) == sizeof(int32_t)), int> = 0>
RES_T __vload_unpack_long(vtype_ptr<vtype<ELEM_T, NELEM> > input)
{
    vtype_ptr<vtype<const ELEM_T, NELEM> > cinput = input;

    static_assert((NELEM <= 8), "Number of elements must be <= 8");

    RES_T ret;

    for(size_t i = 0; i < NELEM; i++)
        ret.s[i] = (*cinput).s[i];
    return ret;
}

template<typename ELEM_T,
         size_t NELEM,
         typename RES_T = vtype<uint64_t, NELEM>,
         std::enable_if_t<std::is_unsigned<ELEM_T>::value &&
                          (   sizeof(ELEM_T) == sizeof(uint8_t) 
                           || sizeof(ELEM_T) == sizeof(uint16_t) 
                           || sizeof(ELEM_T) == sizeof(uint32_t)), int> = 0>
RES_T __vload_unpack_long(vtype_ptr<vtype<ELEM_T, NELEM> > input)
{
    vtype_ptr<vtype<const ELEM_T, NELEM> > cinput = input;

    static_assert((NELEM <= 8), "Number of elements must be <= 8");

    vtype<uint64_t, NELEM> ret = vtype<uint64_t, NELEM>(0);

    for(size_t i = 0; i < NELEM; i++)
        ret.s[i] = (*cinput).s[i];
    return ret;
}

template<typename ELEM_T,
         size_t NELEM,
         typename RES_T = vtype<__clong, NELEM>,
         std::enable_if_t<!std::is_floating_point<ELEM_T>::value &&
                          std::is_signed<ELEM_T>::value &&
                          (   sizeof(ELEM_T) == sizeof(int8_t)
                           || sizeof(ELEM_T) == sizeof(int16_t) 
                           || sizeof(ELEM_T) == sizeof(int32_t)), int> = 0>
RES_T __vload_unpack_long(vtype_ptr<vtype<ctype<ELEM_T>, NELEM> > input)
{
    vtype_ptr<vtype<ctype<const ELEM_T>, NELEM> > cinput = input;

    static_assert((NELEM <= 4), "Number of elements must be <= 4");

    RES_T ret;

    for(size_t i = 0; i < NELEM; i++)
        ret.s[i] = __convert_clong((*cinput).s[i]);
    return ret;
}

template<typename ELEM_T,
         std::enable_if_t<!std::is_floating_point<ELEM_T>::value &&
                          std::is_signed<ELEM_T>::value && 
                          (   sizeof(ELEM_T) == sizeof(uint8_t) 
                           || sizeof(ELEM_T) == sizeof(uint16_t) 
                           || sizeof(ELEM_T) == sizeof(uint32_t)), int> = 0>
int64_t __vload_unpack_long(ELEM_T* input)
{
    const ELEM_T *cinput = input;
    return *cinput;
}

template<typename ELEM_T,
         std::enable_if_t<std::is_unsigned<ELEM_T>::value &&
                          (   sizeof(ELEM_T) == sizeof(uint8_t)
                           || sizeof(ELEM_T) == sizeof(uint16_t) 
                           || sizeof(ELEM_T) == sizeof(uint32_t)), int> = 0>
uint64_t __vload_unpack_long(ELEM_T* input)
{
    const ELEM_T *cinput = input;

    return *cinput;
}

template<typename ELEM_T,
         std::enable_if_t<!std::is_floating_point<ELEM_T>::value &&
                          std::is_signed<ELEM_T>::value &&
                          (   sizeof(ELEM_T) == sizeof(uint8_t) 
                           || sizeof(ELEM_T) == sizeof(uint16_t) 
                           || sizeof(ELEM_T) == sizeof(uint32_t)), int> = 0>
__clong __vload_unpack_long(ctype_ptr<ctype<ELEM_T> > input)
{
    ctype_ptr<ctype<const ELEM_T> > cinput = input;

    return __convert_clong(*cinput);
}

/*****************************************************************************
* VLOAD_DEINTERLEAVE_INT
*****************************************************************************/

/*****************************************************************************
* Enable for vectors with 4 <= NELEM <= 32
*****************************************************************************/
template<typename ELEM_T,
         size_t NELEM,
         size_t RES_NELEM = NELEM/2,
         typename RES_T = vtype<int32_t, RES_NELEM>,
         std::enable_if_t<std::is_signed<ELEM_T>::value &&
                          (   sizeof(ELEM_T) == sizeof(int8_t) 
                           || sizeof(ELEM_T) == sizeof(int16_t)) &&
                          NELEM <= 32 &&
                          NELEM >= 4, int> = 0>
RES_T __vload_deinterleave_int(vtype_ptr<vtype<ELEM_T, NELEM> > input)
{
    vtype_ptr<vtype<const ELEM_T, NELEM> > cinput = input;

    RES_T ret;

    for(size_t i = 0; i < RES_NELEM; i++)
        ret.s[i] = (*cinput).s[i * 2];
    return ret;
}

template<typename ELEM_T,
         size_t NELEM,
         size_t RES_NELEM = NELEM/2,
         typename RES_T = vtype<uint32_t, RES_NELEM>,
         std::enable_if_t<std::is_unsigned<ELEM_T>::value &&
                          (   sizeof(ELEM_T) == sizeof(int8_t) 
                           || sizeof(ELEM_T) == sizeof(int16_t)) && 
                          NELEM <= 32 && 
                          NELEM >= 4, int> = 0>
RES_T __vload_deinterleave_int(vtype_ptr<vtype<ELEM_T, NELEM> > input)
{
    vtype_ptr<vtype<const ELEM_T, NELEM> > cinput = input;

    RES_T ret;;

    for(size_t i = 0; i < RES_NELEM; i++)
        ret.s[i] = (*cinput).s[i * 2];
    return ret;
}

template<typename ELEM_T,
         std::enable_if_t<std::is_signed<ELEM_T>::value && 
                          (   sizeof(ELEM_T) == sizeof(int8_t) 
                           || sizeof(ELEM_T) == sizeof(int16_t)), int> = 0>
int32_t __vload_deinterleave_int(vtype_ptr<vtype<ELEM_T, 2> > input)
{
    vtype_ptr<vtype<const ELEM_T, 2> > cinput = input;

    return (*cinput).s0;
}

template<typename ELEM_T,
         std::enable_if_t<std::is_unsigned<ELEM_T>::value &&
                          (   sizeof(ELEM_T) == sizeof(int8_t) 
                           || sizeof(ELEM_T) == sizeof(int16_t)), int> = 0>
uint32_t __vload_deinterleave_int(vtype_ptr<vtype<ELEM_T, 2> > input)
{
    vtype_ptr<vtype<const ELEM_T, 2> > cinput = input;

    return (*cinput).s0;
}

/*****************************************************************************
* VLOAD_DEINTERLEAVE_LONG
*****************************************************************************/

/*****************************************************************************
* Enable for vectors with 4 <= NELEM <= 16
*****************************************************************************/
template<typename ELEM_T,
         size_t NELEM,
         size_t RES_NELEM = NELEM/2,
         typename RES_T = vtype<int64_t, RES_NELEM>,
         std::enable_if_t<std::is_signed<ELEM_T>::value &&
                          (   sizeof(ELEM_T) == sizeof(int8_t) 
                           || sizeof(ELEM_T) == sizeof(int16_t)) &&
                          NELEM <= 16 &&
                          NELEM >= 4, int> = 0>
RES_T __vload_deinterleave_long(vtype_ptr<vtype<ELEM_T, NELEM> > input)
{
    vtype_ptr<vtype<ELEM_T, NELEM> > cinput = input;

    RES_T ret;

    for(size_t i = 0; i < RES_NELEM; i++)
        ret.s[i] = (*cinput).s[i * 2];
    return ret;
}

template<typename ELEM_T,
         size_t NELEM,
         size_t RES_NELEM = NELEM/2,
         typename RES_T = vtype<uint64_t, RES_NELEM>,
         std::enable_if_t<std::is_unsigned<ELEM_T>::value &&
                          (   sizeof(ELEM_T) == sizeof(int8_t) 
                           || sizeof(ELEM_T) == sizeof(int16_t)) && 
                           NELEM <= 16 && 
                           NELEM >= 4, int> = 0>
RES_T __vload_deinterleave_long(vtype_ptr<vtype<ELEM_T, NELEM> > input)
{
    vtype_ptr<vtype<const ELEM_T, NELEM> > cinput = input;

    RES_T ret;

    for(size_t i = 0; i < NELEM/2; i++)
        ret.s[i] = (*cinput).s[i * 2];
    return ret;
}

template<typename ELEM_T,
         std::enable_if_t<std::is_signed<ELEM_T>::value &&
                          (   sizeof(ELEM_T) == sizeof(int8_t)
                           || sizeof(ELEM_T) == sizeof(int16_t)), int> = 0>
int64_t __vload_deinterleave_long(vtype_ptr<vtype<ELEM_T, 2> > input)
{
    vtype_ptr<vtype<const ELEM_T, 2> > cinput = input;

    return (*cinput).s0;
}

template<typename ELEM_T,
         std::enable_if_t<std::is_unsigned<ELEM_T>::value &&
                          (   sizeof(ELEM_T) == sizeof(int8_t) 
                           || sizeof(ELEM_T) == sizeof(int16_t)), int> = 0>
uint64_t __vload_deinterleave_long(vtype_ptr<vtype<ELEM_T, 2> > input)
{
    vtype_ptr<vtype<const ELEM_T, 2> > cinput = input;

    return (*input).s0;
}

/*****************************************************************************
* VSTORE_INTERLEAVE
******************************************************************************
* Template only enabled for full vectors.
* Note: param1 does not have to be an accessible* because only  full (512-bit) 
* vectors are allowed here. There is no way to make a full vector from an 
* accessible.
*****************************************************************************/
template<typename ELEM_T,
         size_t NELEM,
         std::enable_if_t<!std::is_floating_point<ELEM_T>::value &&
                          (NELEM == 64/sizeof(ELEM_T)), int> = 0>
void __vstore_interleave(vtype_ptr<vtype<ELEM_T, NELEM> > result, 
                         vtype<ELEM_T, NELEM> input1, 
                         vtype<ELEM_T, NELEM> input2) 
{
    (*result).even = input1.even;
    (*result).odd  = input2.even;
}


/*****************************************************************************
* Store every fourth element. Enabled if result vector is 32 characters and 
* both inputs are 64 characters long. Also only enabled if the element type 
* is of size 8 bits sizeof(int8_t) -> applies to unsigned and signed char
****************************************************************************/
template<typename ELEM_T,
         size_t NELEM,
         std::enable_if_t<NELEM == 64/sizeof(int8_t) &&
                          sizeof(ELEM_T) == sizeof(int8_t), int> = 0>
void __vstore_interleave4(vtype_ptr<vtype<ELEM_T, NELEM/2> > result, 
                          vtype<ELEM_T, NELEM> input1, 
                          vtype<ELEM_T, NELEM> input2) 
{
    (*result).even = input1.even.even;
    (*result).odd  = input2.even.even;
}

/*****************************************************************************
* Helper function used to check if predicate is true at index 
*****************************************************************************/
bool check_pred_at_index(__vpred pred, uint64_t index, uint64_t element_size);

/*****************************************************************************
* VSTORE_PRED_INTERLEAVE
*****************************************************************************/
template<typename ELEM_T,
         size_t NELEM,
         std::enable_if_t<!std::is_floating_point<ELEM_T>::value &&
                          (NELEM == 64/sizeof(ELEM_T)), int> = 0>
void __vstore_pred_interleave(__vpred pred, 
                              vtype_ptr<vtype<ELEM_T, NELEM> > result, 
                              vtype<ELEM_T, NELEM> input1,   
                              vtype<ELEM_T, NELEM> input2) 
{
    // Even part
    for (size_t i = 0; i < NELEM; i+=2)
        if (check_pred_at_index(pred, i, sizeof(ELEM_T)))
            (*result).s[i] = input1.s[i];   
    
    // Odd part
    for (size_t i = 0; i < NELEM; i+=2)
        if (check_pred_at_index(pred, i+1, sizeof(ELEM_T)))
            (*result).s[i+1] = input2.s[i];
}

/*****************************************************************************
* Store every fourth element based on pred. Enabled if result vector is 32 
* characters and both inputs are 64 characters long. Also only enabled if the 
* element type is of size 8 bits sizeof(int8_t) -> applies to unsigned 
* and signed char
*****************************************************************************/
template<typename ELEM_T,
         size_t NELEM,
         std::enable_if_t<NELEM == 64/sizeof(int8_t) && 
                          sizeof(ELEM_T) == sizeof(int8_t), int> = 0>
void __vstore_pred_interleave4(__vpred pred, 
                               vtype_ptr<vtype<ELEM_T, NELEM/2> > result, 
                               vtype<ELEM_T, NELEM> input1, 
                               vtype<ELEM_T, NELEM> input2) 
{
    for (size_t i = 0; i < NELEM; i+=4)
        if (check_pred_at_index(pred, (i/2), sizeof(ELEM_T)))
            (*result).s[i/2] = input1.s[i];
        
    for (size_t i = 0; i < NELEM; i+=4)
        if (check_pred_at_index(pred, (i/2) + 1, sizeof(ELEM_T)))
            (*result).s[(i/2) + 1] = input2.s[i];
}

/*****************************************************************************
* VSTORE_PRED
*****************************************************************************/

/*****************************************************************************
* Accept accessible reference as second parameter. This allows the function to 
* accept accessibles as parameters. Must be references as you cannot create a 
* copy of an accessible bc it does not hold any storage (so a copy of it 
* doesn't make sense). When a vtype is the parameter, inheritance rules allow 
* for the function to be matched as all vtypes are accessibles.
* Note: input is not modified in this function and shouldn't be
*****************************************************************************/
template<typename ELEM_T,
         size_t NELEM,
         size_t DEPTH>
void __vstore_pred(__vpred pred, vtype_ptr<vtype<ELEM_T, NELEM> > result, 
                   accessible<ELEM_T, NELEM, DEPTH> &input)
{
    for(size_t i = 0; i < NELEM; i++)
        if (check_pred_at_index(pred, i, sizeof(ELEM_T))) 
            (*result).s[i] = input.s[i];
}

template<typename ELEM_T,
         size_t NELEM>
void __vstore_pred(__vpred pred, 
                   vtype_ptr<vtype<ELEM_T, NELEM> > result, 
                   vtype<ELEM_T, NELEM> input)
{
    for(size_t i = 0; i < NELEM; i++)
        if (check_pred_at_index(pred, i, sizeof(ELEM_T)))
            (*result).s[i] = input.s[i];
}

template<typename ELEM_T,
         std::enable_if_t<std::is_arithmetic<ELEM_T>::value, int> = 0>
void __vstore_pred(__vpred pred, ELEM_T *result, ELEM_T input)
{
    if (check_pred_at_index(pred, 0, sizeof(ELEM_T)))
        *result = input;
}

template<typename ELEM_T,
         size_t NELEM, 
         size_t DEPTH>
void __vstore_pred(__vpred pred, vtype_ptr<vtype<ctype<ELEM_T>, NELEM> > result, 
                   accessible<ctype<ELEM_T>, NELEM, DEPTH> &input)
{
    for(size_t i = 0; i < NELEM * 2; i++)
    {
        if (check_pred_at_index(pred, i, sizeof(ELEM_T)))
        { 
            if (i % 2 == 0)
                (*result).s[i/2].r = input.s[i/2].r;
            else
                (*result).s[i/2].i = input.s[i/2].i;
        }
    }
}

template<typename ELEM_T,
         size_t NELEM>
void __vstore_pred(__vpred pred, 
                   vtype_ptr<vtype<ctype<ELEM_T>, NELEM> > result, 
                   vtype<ctype<ELEM_T>, NELEM> input)
{
    for(size_t i = 0; i < NELEM * 2; i++)
    {
        if (check_pred_at_index(pred, i, sizeof(ELEM_T))) 
        {
            if (i % 2 == 0)
                (*result).s[i/2].r = input.s[i/2].r;
            else
                (*result).s[i/2].i = input.s[i/2].i;
        }
    }
}

template<typename ELEM_T>
void __vstore_pred(__vpred pred, 
                   ctype_ptr<ctype<ELEM_T> > result, 
                   ctype<ELEM_T> input)
{
    if (check_pred_at_index(pred, 0, sizeof(ELEM_T)))
        (*result).r = input.r;
    if (check_pred_at_index(pred, 1, sizeof(ELEM_T)))
        (*result).i = input.i;
}

/*****************************************************************************
* VSTORE_PRED_PACKL
*****************************************************************************/

/*****************************************************************************
* Limits:
* No floating, size of result element = half size of input element, 
* full vectors only, no floating points, element size of input at least short
*****************************************************************************/
template<typename ELEM_T,
         typename ELEM_T_RES,
         size_t NELEM,
         std::enable_if_t<!std::is_floating_point<ELEM_T>::value &&
                          !std::is_floating_point<ELEM_T_RES>::value &&
                          std::is_signed<ELEM_T>::value == std::is_signed<ELEM_T_RES>::value &&
                          std::is_unsigned<ELEM_T>::value == std::is_unsigned<ELEM_T_RES>::value &&
                          NELEM == 64/sizeof(ELEM_T) &&
                          sizeof(ELEM_T) >= sizeof(int16_t) &&
                          sizeof(ELEM_T) == sizeof(ELEM_T_RES)*2, int> = 0>
void __vstore_pred_packl(__vpred pred, 
                         vtype_ptr<vtype<ELEM_T_RES, NELEM> > result, 
                         vtype<ELEM_T, NELEM> input)
{
    for(size_t i = 0; i < NELEM; i++)
        if (check_pred_at_index(pred, i, sizeof(ELEM_T)))
            (*result).s[i] = (ELEM_T_RES)(input.s[i]); // Grabs low half
}

/*****************************************************************************
* VSTORE_PRED_PACKL_2SRC
*****************************************************************************/
template<typename ELEM_T,
         typename ELEM_T_RES,
         size_t NELEM,
         std::enable_if_t<!std::is_floating_point<ELEM_T>::value &&
                          !std::is_floating_point<ELEM_T_RES>::value && 
                          std::is_signed<ELEM_T>::value == std::is_signed<ELEM_T_RES>::value &&
                          std::is_unsigned<ELEM_T>::value == std::is_unsigned<ELEM_T_RES>::value &&
                          NELEM == 64/sizeof(ELEM_T) &&
                          sizeof(ELEM_T) >= sizeof(int16_t) && 
                          sizeof(ELEM_T) == sizeof(ELEM_T_RES)*2, int> = 0>
void __vstore_pred_packl_2src(__vpred pred, 
                         vtype_ptr<vtype<ELEM_T_RES, NELEM*2> > result, 
                         vtype<ELEM_T, NELEM> input1,
                         vtype<ELEM_T, NELEM> input2)
{
    // Loop num_input1/2 times
    for(size_t i = 0; i < NELEM; i++)
    {
        if (check_pred_at_index(pred, i, sizeof(ELEM_T_RES)))
            (*result).s[i] = (ELEM_T_RES)(input1.s[i]); // Grabs low half
        if (check_pred_at_index(pred, i + NELEM, sizeof(ELEM_T_RES)))
            (*result).s[i + NELEM] = (ELEM_T_RES)(input2.s[i]); // Grabs low half
    }
}


/*****************************************************************************
* VSTORE_PRED_PACKH
*****************************************************************************/
template<typename ELEM_T,
         typename ELEM_T_RES,
         size_t NELEM,
         std::enable_if_t<!std::is_floating_point<ELEM_T>::value &&
                          !std::is_floating_point<ELEM_T_RES>::value && 
                          std::is_signed<ELEM_T>::value == std::is_signed<ELEM_T_RES>::value && 
                          std::is_unsigned<ELEM_T>::value == std::is_unsigned<ELEM_T_RES>::value &&
                          NELEM == 64/sizeof(ELEM_T) &&
                          sizeof(ELEM_T) >= sizeof(int16_t) &&
                          sizeof(ELEM_T) == sizeof(ELEM_T_RES)*2, int> = 0>
void __vstore_pred_packh(__vpred pred, 
                         vtype_ptr<vtype<ELEM_T_RES, NELEM> > result,
                         vtype<ELEM_T, NELEM> input)
{
    for(size_t i = 0; i < NELEM; i++)
        if (check_pred_at_index(pred, i, sizeof(ELEM_T)))
            (*result).s[i] = (input.s[i]) >> (sizeof(ELEM_T_RES)*8); // Grabs high half
}

/*****************************************************************************
* VSTORE_PRED_PACKH_2SRC
*****************************************************************************/
template<typename ELEM_T,
         typename ELEM_T_RES,
         size_t NELEM,
         std::enable_if_t<!std::is_floating_point<ELEM_T>::value && 
                          !std::is_floating_point<ELEM_T_RES>::value &&
                          std::is_signed<ELEM_T>::value == std::is_signed<ELEM_T_RES>::value &&
                          std::is_unsigned<ELEM_T>::value == std::is_unsigned<ELEM_T_RES>::value &&
                          NELEM == 64/sizeof(ELEM_T) &&
                          sizeof(ELEM_T) >= sizeof(int16_t) && 
                          sizeof(ELEM_T) == sizeof(ELEM_T_RES)*2, int> = 0>
void __vstore_pred_packh_2src(__vpred pred, 
                         vtype_ptr<vtype<ELEM_T_RES, NELEM*2> > result,
                         vtype<ELEM_T, NELEM> input1,
                         vtype<ELEM_T, NELEM> input2)
{
    for(size_t i = 0; i < NELEM; i++)
    {
        if (check_pred_at_index(pred, i, sizeof(ELEM_T_RES)))
            (*result).s[i] = (input1.s[i]) >> (sizeof(ELEM_T_RES)*8); // Grabs high half
        if (check_pred_at_index(pred, i + NELEM, sizeof(ELEM_T_RES)))
            (*result).s[i + NELEM] = (input2.s[i]) >> (sizeof(ELEM_T_RES)*8); // Grabs high half
    }
}

/*****************************************************************************
* VSTORE_PRED_PACKHS1
*****************************************************************************/
template<typename ELEM_T,
         typename ELEM_T_RES,
         size_t NELEM,
         std::enable_if_t<!std::is_floating_point<ELEM_T>::value && 
                          !std::is_floating_point<ELEM_T_RES>::value &&
                          std::is_signed<ELEM_T>::value == std::is_signed<ELEM_T_RES>::value &&
                          std::is_unsigned<ELEM_T>::value == std::is_unsigned<ELEM_T_RES>::value &&
                          NELEM == 64/sizeof(ELEM_T) &&
                          sizeof(ELEM_T) >= sizeof(int16_t) &&
                          sizeof(ELEM_T) == sizeof(ELEM_T_RES)*2, int> = 0>
void __vstore_pred_packhs1(__vpred pred, 
                           vtype_ptr<vtype<ELEM_T_RES, NELEM> > result, 
                           vtype<ELEM_T, NELEM> input)
{
    for(size_t i = 0; i < NELEM; i++)
    {
        if (check_pred_at_index(pred, i, sizeof(ELEM_T)))
        {
            ELEM_T val = input.s[i] << (ELEM_T)1;
            (*result).s[i] = (val) >> (sizeof(ELEM_T_RES)*8); // Grabs high half
        }
    }
}

/*****************************************************************************
* VSTORE_PRED_PACKHS1_2SRC
*****************************************************************************/
template<typename ELEM_T, 
         typename ELEM_T_RES,
         size_t NELEM,
         std::enable_if_t<!std::is_floating_point<ELEM_T>::value &&
                          !std::is_floating_point<ELEM_T_RES>::value && 
                          std::is_signed<ELEM_T>::value == std::is_signed<ELEM_T_RES>::value &&
                          std::is_unsigned<ELEM_T>::value == std::is_unsigned<ELEM_T_RES>::value &&
                          NELEM == 64/sizeof(ELEM_T) &&
                          sizeof(ELEM_T) >= sizeof(int16_t) &&
                          sizeof(ELEM_T) == sizeof(ELEM_T_RES)*2, int> = 0>
void __vstore_pred_packhs1_2src(__vpred pred, 
                           vtype_ptr<vtype<ELEM_T_RES, NELEM*2> > result, 
                           vtype<ELEM_T, NELEM> input1,
                           vtype<ELEM_T, NELEM> input2)
{
    for(size_t i = 0; i < NELEM; i++)
    {
        if (check_pred_at_index(pred, i, sizeof(ELEM_T_RES)))
        {
            ELEM_T val = input1.s[i] << (ELEM_T)1;
            (*result).s[i] = (val) >> (sizeof(ELEM_T_RES)*8); // Grabs high half
        }
        if (check_pred_at_index(pred, i + NELEM, sizeof(ELEM_T_RES)))
        {
            ELEM_T val = input2.s[i] << (ELEM_T)1;
            (*result).s[i + NELEM] = (val) >> (sizeof(ELEM_T_RES)*8); // Grabs high half
        }
    }
}

/*****************************************************************************
* VSTORE_PRED_PACK_BYTE
*****************************************************************************/
void __vstore_pred_pack_byte(__vpred pred,
                             __char16_ptr result, 
                             __int16 input);

void __vstore_pred_pack_byte(__vpred pred, 
                             __uchar16_ptr result, 
                             __uint16 input);

/*****************************************************************************
* VSTORE_PRED_PACK_BYTE_2SRC
*****************************************************************************/
void __vstore_pred_pack_byte_2src(__vpred pred,
                                  __char32_ptr result, 
                                  __int16 input1,
                                  __int16 input2);

void __vstore_pred_pack_byte_2src(__vpred pred, 
                                  __uchar32_ptr result, 
                                  __uint16 input1,
                                  __uint16 input2);

/*****************************************************************************
* __VSTORE_PRED_PACKL_LONG 
*****************************************************************************/
void __vstore_pred_packl_long(__vpred pred, 
                               __long4_ptr result, 
                               __long8 input);

void __vstore_pred_packl_long(__vpred pred, 
                               __ulong4_ptr result,
                               __ulong8 input);

/*****************************************************************************
* __VSTORE_PRED_PACKL_LONG_2SRC 
*****************************************************************************/
void __vstore_pred_packl_long_2src(__vpred pred, 
                                    __long8_ptr result, 
                                    __long8 input1,
                                    __long8 input2);

void __vstore_pred_packl_long_2src(__vpred pred, 
                                    __ulong8_ptr result,
                                    __ulong8 input1,
                                    __ulong8 input2);

/*****************************************************************************
* __VSTORE_PRED_PACKH_LONG 
*****************************************************************************/
void __vstore_pred_packh_long(__vpred pred, 
                               __long4_ptr result, 
                               __long8 input);

void __vstore_pred_packh_long(__vpred pred,
                               __ulong4_ptr result,
                               __ulong8 input);

/*****************************************************************************
* __VSTORE_PRED_PACKH_LONG_2SRC 
*****************************************************************************/
void __vstore_pred_packh_long_2src(__vpred pred, 
                                    __long8_ptr result, 
                                    __long8 input1,
                                    __long8 input2);

void __vstore_pred_packh_long_2src(__vpred pred,
                                    __ulong8_ptr result,
                                    __ulong8 input1,
                                    __ulong8 input2);

/*****************************************************************************
* __VSTORE_PRED_PACKHS1_LONG 
*****************************************************************************/
void __vstore_pred_packhs1_long(__vpred pred, 
                                 __long4_ptr result, 
                                 __long8 input);

void __vstore_pred_packhs1_long(__vpred pred, 
                                 __ulong4_ptr result, 
                                 __ulong8 input);

/*****************************************************************************
* __VSTORE_PRED_PACKHS1_LONG_2SRC 
*****************************************************************************/
void __vstore_pred_packhs1_long_2src(__vpred pred, 
                                      __long8_ptr result, 
                                      __long8 input1,
                                      __long8 input2);

void __vstore_pred_packhs1_long_2src(__vpred pred, 
                                      __ulong8_ptr result, 
                                      __ulong8 input1,
                                      __ulong8 input2);

/*****************************************************************************
* Helper function used with bit reverse intrinsics 
*****************************************************************************/
uint32_t reverse_bits(uint32_t index, uint32_t num_of_bits);

/*****************************************************************************
* VSTORE_PRED_REVERSE_BIT
*****************************************************************************/

/*****************************************************************************
* Limits: At least short, full vector
*****************************************************************************/
template<typename ELEM_T,
         size_t NELEM,
         std::enable_if_t<NELEM == 64/(sizeof(ELEM_T) * 2) &&
                          sizeof(ELEM_T) >= sizeof(int16_t) && 
                          sizeof(ELEM_T) < sizeof(int64_t), int> = 0>
void __vstore_pred_reverse_bit(__vpred pred, 
                               vtype_ptr<vtype<ctype<ELEM_T>, NELEM> > result, 
                               vtype<ctype<ELEM_T>, NELEM> input)
{
    uint32_t num_of_bits = (uint32_t)std::log2(NELEM * 2);

    for (size_t i = 0; i < NELEM; i++)
    {
        size_t src_pos = reverse_bits((uint32_t)i*2, num_of_bits);
        if (check_pred_at_index(pred, i*2, sizeof(ELEM_T)))
            (*result).s[i].r = input.s[src_pos].r; 
        if (check_pred_at_index(pred, (i*2)+1, sizeof(ELEM_T)))
            (*result).s[i].i = input.s[src_pos].i; 
    }
}

/*****************************************************************************
* VSTORE_REVERSE_BIT
*****************************************************************************/

/*****************************************************************************
* Limits: At least short, full vector
*****************************************************************************/
template<typename ELEM_T, 
         size_t NELEM,
         std::enable_if_t<NELEM == 64/sizeof(ELEM_T) &&
                          sizeof(ELEM_T) >= sizeof(int16_t), int> = 0>
void __vstore_reverse_bit(vtype_ptr<vtype<ELEM_T, NELEM> > result,
                          vtype<ELEM_T, NELEM> input)
{
    uint32_t num_of_bits = (uint32_t)std::log2(NELEM);
  
    for (size_t i = 0; i < NELEM; i += 2)
    {
        size_t src_pos = reverse_bits((uint32_t)i, num_of_bits) * 2;
        (*result).s[i] =   input.s[src_pos]; 
        (*result).s[i+1] = input.s[src_pos+1]; 
    }
}

template<typename ELEM_T,
         size_t NELEM,
         std::enable_if_t<NELEM == 64/(sizeof(ELEM_T) * 2) &&
                          sizeof(ELEM_T) >= sizeof(int16_t) && 
                          sizeof(ELEM_T) < sizeof(int64_t), int> = 0>
void __vstore_reverse_bit(vtype_ptr<vtype<ctype<ELEM_T>, NELEM> > result, 
                               vtype<ctype<ELEM_T>, NELEM> input)
{
    uint32_t num_of_bits = (uint32_t)std::log2(NELEM * 2);

    for (size_t i = 0; i < NELEM; i++)
    {
        size_t src_pos = reverse_bits((uint32_t)i*2, num_of_bits);
        (*result).s[i] = input.s[src_pos]; 
    }
}

/*****************************************************************************
* VSTORE_CONST
*****************************************************************************/
void __vstore_const(uint32_t* result, uint32_t input);

/*****************************************************************************
* MULTI_WORD
*****************************************************************************/
template<size_t NELEM>
void __vstore_const_multi_word(vtype_ptr<vtype<uint32_t, NELEM> > result, 
                               uint32_t input)
{
    assert(((input >= 0) && (input <= 0x7fffffff)) && "Input constant out of range");
    *result = vtype<uint32_t, NELEM>(input);;
}

/*****************************************************************************
* UINT2
*****************************************************************************/
void __vstore_const_uint2(__uint2_ptr result, uint32_t input);

/*****************************************************************************
* UINT4
*****************************************************************************/
void __vstore_const_uint4(__uint4_ptr result, uint32_t input);

/*****************************************************************************
* UINT8
*****************************************************************************/
void __vstore_const_uint8(__uint8_ptr result, uint32_t input);

/*****************************************************************************
* UINT16
*****************************************************************************/
void __vstore_const_uint16(__uint16_ptr result, uint32_t input);

/*****************************************************************************
* VSTORE_PACKL
*****************************************************************************/

/*****************************************************************************
* Limits:
* No floating, size of result element = half size of input element, 
* full vectors only, no floating points, element size of input at least short
*****************************************************************************/
template<typename ELEM_T, 
         typename ELEM_T_RES, 
         size_t NELEM,
         std::enable_if_t<!std::is_floating_point<ELEM_T>::value && 
                          !std::is_floating_point<ELEM_T_RES>::value && 
                          std::is_signed<ELEM_T>::value == std::is_signed<ELEM_T_RES>::value && 
                          std::is_unsigned<ELEM_T>::value == std::is_unsigned<ELEM_T_RES>::value &&
                          NELEM == 64/sizeof(ELEM_T) &&
                          sizeof(ELEM_T) >= sizeof(int16_t) &&
                          sizeof(ELEM_T) == sizeof(ELEM_T_RES)*2, int> = 0>
void __vstore_packl(vtype_ptr<vtype<ELEM_T_RES, NELEM> > result, 
                    vtype<ELEM_T, NELEM> input)
{
    for(size_t i = 0; i < NELEM; i++)
        (*result).s[i] = (ELEM_T_RES)(input.s[i]); // Grabs low half
}

template<typename ELEM_T,
         size_t NELEM,
         std::enable_if_t<!std::is_floating_point<ELEM_T>::value &&
                          NELEM == 64/sizeof(ELEM_T) &&
                          sizeof(ELEM_T) == sizeof(int64_t), int> = 0> 
void __vstore_packl(vtype_ptr<vtype<ELEM_T, NELEM/2> > result, 
                    vtype<ELEM_T, NELEM> input)
{
    for(size_t i = 0; i < NELEM; i += 2)
        (*result).s[i/2] = (input.s[i]);
}


void __vstore_packl(__long4_ptr result, 
                    __clong4 input);

/*****************************************************************************
* VSTORE_PACKL_2SRC
*****************************************************************************/

/*****************************************************************************
* Limits:
* No floating, size of result element = half size of input element, 
* full vectors only, no floating points, element size of input at least short
*****************************************************************************/
template<typename ELEM_T,
         typename ELEM_T_RES,
         size_t NELEM,
         std::enable_if_t<!std::is_floating_point<ELEM_T>::value &&
                          !std::is_floating_point<ELEM_T_RES>::value &&
                          std::is_signed<ELEM_T>::value == std::is_signed<ELEM_T_RES>::value &&
                          std::is_unsigned<ELEM_T>::value == std::is_unsigned<ELEM_T_RES>::value &&
                          NELEM == 64/sizeof(ELEM_T) &&
                          sizeof(ELEM_T) >= sizeof(int16_t) &&
                          sizeof(ELEM_T) == sizeof(ELEM_T_RES)*2, int> = 0>
void __vstore_packl_2src(vtype_ptr<vtype<ELEM_T_RES, NELEM * 2> > result, 
                         vtype<ELEM_T, NELEM> input1,
                         vtype<ELEM_T, NELEM> input2)
{
    for(size_t i = 0; i < NELEM; i++)
    {
        (*result).s[i] = (ELEM_T_RES)(input1.s[i]); // Grabs low half
        (*result).s[i + NELEM] = (ELEM_T_RES)(input2.s[i]); // Grabs low half
    }
}

template<typename ELEM_T,
         size_t NELEM,
         std::enable_if_t<!std::is_floating_point<ELEM_T>::value &&
                          NELEM == 64/sizeof(ELEM_T) &&
                          sizeof(ELEM_T) == sizeof(int64_t), int> = 0> 
void __vstore_packl_2src(vtype_ptr<vtype<ELEM_T, NELEM> > result, 
                         vtype<ELEM_T, NELEM> input1,
                         vtype<ELEM_T, NELEM> input2)
{
    for (size_t i = 0; i < NELEM; i += 2)
    {
        (*result).s[i/2] = input1.s[i];
        (*result).s[(i + NELEM)/2] = input2.s[i]; 
    }
}


void __vstore_packl_2src(__long8_ptr result, 
                         __clong4 input1,
                         __clong4 input2);

/*****************************************************************************
* VSTORE_PACKH
*****************************************************************************/
template<typename ELEM_T,
         typename ELEM_T_RES,
         size_t NELEM,
         std::enable_if_t<!std::is_floating_point<ELEM_T>::value && 
                          !std::is_floating_point<ELEM_T_RES>::value &&
                          std::is_signed<ELEM_T>::value == std::is_signed<ELEM_T_RES>::value &&
                          std::is_unsigned<ELEM_T>::value == std::is_unsigned<ELEM_T_RES>::value &&
                          NELEM == 64/sizeof(ELEM_T) &&
                          sizeof(ELEM_T) >= sizeof(int16_t) &&
                          sizeof(ELEM_T) == sizeof(ELEM_T_RES)*2, int> = 0>
void __vstore_packh(vtype_ptr<vtype<ELEM_T_RES, NELEM> > result, 
                    vtype<ELEM_T, NELEM> input)
{
    for(size_t i = 0; i < NELEM; i++)
        (*result).s[i] = (input.s[i]) >> (sizeof(ELEM_T_RES)*8); // Grabs high half
}

template<typename ELEM_T,
         size_t NELEM,
         std::enable_if_t<!std::is_floating_point<ELEM_T>::value &&
                          NELEM == 64/sizeof(ELEM_T) &&
                          sizeof(ELEM_T) == sizeof(int64_t), int> = 0>
void __vstore_packh(vtype_ptr<vtype<ELEM_T, NELEM/2> > result, 
                    vtype<ELEM_T, NELEM> input)
{
    for(size_t i = 0; i < NELEM; i += 2)
        (*result).s[i/2] = (input.s[i+1]);
}


void __vstore_packh(__long4_ptr result, 
                    __clong4 input);

/*****************************************************************************
* VSTORE_PACKH_2SRC
*****************************************************************************/
template<typename ELEM_T,
         typename ELEM_T_RES,
         size_t NELEM,
         std::enable_if_t<!std::is_floating_point<ELEM_T>::value && 
                          !std::is_floating_point<ELEM_T_RES>::value &&
                          std::is_signed<ELEM_T>::value == std::is_signed<ELEM_T_RES>::value && 
                          std::is_unsigned<ELEM_T>::value == std::is_unsigned<ELEM_T_RES>::value &&
                          NELEM == 64/sizeof(ELEM_T) &&
                          sizeof(ELEM_T) >= sizeof(int16_t) && 
                          sizeof(ELEM_T) == sizeof(ELEM_T_RES)*2, int> = 0>
void __vstore_packh_2src(vtype_ptr<vtype<ELEM_T_RES, NELEM * 2> > result, 
                         vtype<ELEM_T, NELEM> input1,
                         vtype<ELEM_T, NELEM> input2)
{
    for(size_t i = 0; i < NELEM; i++)
    {
        (*result).s[i] = (input1.s[i]) >> (sizeof(ELEM_T_RES)*8); // Grabs high half
        (*result).s[i + NELEM] = (input2.s[i]) >> (sizeof(ELEM_T_RES)*8); // Grabs high half
    }
}

template<typename ELEM_T, 
         size_t NELEM,
         std::enable_if_t<!std::is_floating_point<ELEM_T>::value &&
                          NELEM == 64/sizeof(ELEM_T) &&
                          sizeof(ELEM_T) == sizeof(int64_t), int> = 0> 
void __vstore_packh_2src(vtype_ptr<vtype<ELEM_T, NELEM> > result, 
                         vtype<ELEM_T, NELEM> input1,
                         vtype<ELEM_T, NELEM> input2)
{
    for (size_t i = 0; i < NELEM; i += 2)
    {
        (*result).s[i/2] = input1.s[i+1];
        (*result).s[(i + NELEM)/2] = input2.s[i+1]; 
    }
}


void __vstore_packh_2src(__long8_ptr result, 
                         __clong4 input1,
                         __clong4 input2);

/*****************************************************************************
* VSTORE_PACKHS1
*****************************************************************************/
template<typename ELEM_T,
         typename ELEM_T_RES, 
         size_t NELEM,
         std::enable_if_t<!std::is_floating_point<ELEM_T>::value && 
                          !std::is_floating_point<ELEM_T_RES>::value &&
                          std::is_signed<ELEM_T>::value == std::is_signed<ELEM_T_RES>::value && 
                          std::is_unsigned<ELEM_T>::value == std::is_unsigned<ELEM_T_RES>::value &&
                          NELEM == 64/sizeof(ELEM_T) &&
                          sizeof(ELEM_T) >= sizeof(int16_t) &&
                          sizeof(ELEM_T) == sizeof(ELEM_T_RES)*2, int> = 0>
void __vstore_packhs1(vtype_ptr<vtype<ELEM_T_RES, NELEM> > result,
                      vtype<ELEM_T, NELEM> input)
{
    for(size_t i = 0; i < NELEM; i++)
    {
        ELEM_T val = input.s[i] << (ELEM_T)1;
        (*result).s[i] = (val) >> (sizeof(ELEM_T_RES)*8); // Grabs high half
    }
}

template<typename ELEM_T,
         size_t NELEM,
         std::enable_if_t<!std::is_floating_point<ELEM_T>::value &&
                          NELEM == 64/sizeof(ELEM_T) &&
                          sizeof(ELEM_T) == sizeof(int64_t), int> = 0> 
void __vstore_packhs1(vtype_ptr<vtype<ELEM_T, NELEM/2> > result, 
                      vtype<ELEM_T, NELEM> input)
{
    for(size_t i = 0; i < NELEM; i += 2)
    {
        (*result).s[i/2]  = ((input.s[i+1]) << 1);
        (*result).s[i/2] |= ((input.s[i] >> 63) & 0x1);
    }
}


void __vstore_packhs1(__long4_ptr result, 
                      __clong4 input);



/*****************************************************************************
* VSTORE_PACKHS1_2SRC
*****************************************************************************/
template<typename ELEM_T,
         typename ELEM_T_RES, 
         size_t NELEM,
         std::enable_if_t<!std::is_floating_point<ELEM_T>::value && 
                          !std::is_floating_point<ELEM_T_RES>::value &&
                          std::is_signed<ELEM_T>::value == std::is_signed<ELEM_T_RES>::value &&
                          std::is_unsigned<ELEM_T>::value == std::is_unsigned<ELEM_T_RES>::value &&
                          NELEM == 64/sizeof(ELEM_T) &&
                          sizeof(ELEM_T) >= sizeof(int16_t) && 
                          sizeof(ELEM_T) == sizeof(ELEM_T_RES)*2, int> = 0>
void __vstore_packhs1_2src(vtype_ptr<vtype<ELEM_T_RES, NELEM * 2> > result,
                           vtype<ELEM_T, NELEM> input1,
                           vtype<ELEM_T, NELEM> input2)
{
    for(size_t i = 0; i < NELEM; i++)
    {
        ELEM_T val = input1.s[i] << (ELEM_T)1;
        (*result).s[i] = (val) >> (sizeof(ELEM_T_RES)*8); // Grabs high half
        ELEM_T val2 = input2.s[i] << (ELEM_T)1;
        (*result).s[i + NELEM] = (val2) >> (sizeof(ELEM_T_RES)*8); // Grabs high half
    }
}

template<typename ELEM_T, 
         size_t NELEM,
         std::enable_if_t<!std::is_floating_point<ELEM_T>::value &&
                          NELEM == 64/sizeof(ELEM_T) &&
                          sizeof(ELEM_T) == sizeof(int64_t), int> = 0> 
void __vstore_packhs1_2src(vtype_ptr<vtype<ELEM_T, NELEM> > result, 
                           vtype<ELEM_T, NELEM> input1,
                           vtype<ELEM_T, NELEM> input2)
{
    for (size_t i = 0; i < NELEM; i += 2)
    {
        (*result).s[i/2]  = (input1.s[i+1] << 1);
        (*result).s[i/2] |= ((input1.s[i] >> 63) & 0x1);
        (*result).s[(i + NELEM)/2]  = (input2.s[i+1] << 1);
        (*result).s[(i + NELEM)/2] |= ((input2.s[i] >> 63) & 0x1); 
    }
}


void __vstore_packhs1_2src(__long8_ptr result, 
                           __clong4 input1,
                           __clong4 input2);

/*****************************************************************************
* VSTORE_PACK_BYTE
*****************************************************************************/
void __vstore_pack_byte(__char16_ptr result, __int16 input);

void __vstore_pack_byte(__uchar16_ptr result, __uint16 input);

/*****************************************************************************
* VSTORE_PACK_BYTE_2SRC
*****************************************************************************/
void __vstore_pack_byte_2src(__char32_ptr result, 
                             __int16 input1,
                             __int16 input2);

void __vstore_pack_byte_2src(__uchar32_ptr result, 
                              __uint16 input1,
                              __uint16 input2);

/*****************************************************************************
* STORE_PREDICATE_[TYPE]
*****************************************************************************/
void __store_predicate_char(uint64_t* result, __vpred pred);

void __store_predicate_short(uint32_t* result, __vpred pred);

void __store_predicate_int(uint16_t* result, __vpred pred);

void __store_predicate_long(uint8_t* result, __vpred pred);

/*****************************************************************************
* ATOMIC COMPARE SWAP
*****************************************************************************/
int32_t __atomic_compare_swap(int32_t* ptr, int32_t src1, int32_t src2);

int64_t __atomic_compare_swap(int64_t* ptr, int64_t src1, int64_t src2);

/*****************************************************************************
* ATOMIC SWAP
*****************************************************************************/
int32_t __atomic_swap(int32_t* ptr, int32_t src1, int32_t src2);

int64_t __atomic_swap(int64_t* ptr, int64_t src1, int64_t src2);

/*****************************************************************************
* PREFETCH
*****************************************************************************/
void __prefetch(void* ptr, unsigned input);

/*****************************************************************************
* MTAG
*****************************************************************************/
/* MTAG argument */
typedef enum
{
    __MTAG_COLOR0        = 0x0,
    __MTAG_COLOR1        = 0x1,
    __MTAG_SWITCH_COLORS = 0x1f
} __MTAG_COLOR;

void __mtag(__MTAG_COLOR input);


/*****************************************************************************
* MFENCE
*****************************************************************************/
/* MFENCE/MFENCEST argument */
typedef enum
{
    __MFENCE_COLOR0     = 0x0,
    __MFENCE_COLOR1     = 0x1,
    __MFENCE_ALL_COLORS = 0x1f
} __MFENCE_COLOR;

void __memory_fence(__MFENCE_COLOR input);

/*****************************************************************************
* MFENCEST
*****************************************************************************/
void __memory_fence_store(__MFENCE_COLOR input);

#endif /* C7X_LDST_H */
