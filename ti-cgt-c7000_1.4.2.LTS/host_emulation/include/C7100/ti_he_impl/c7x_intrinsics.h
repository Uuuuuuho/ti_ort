/*****************************************************************************/
/*  C7X_INTRINSICS.H                                                      */
/*                                                                           */
/* Copyright (c) 2017 Texas Instruments Incorporated                         */
/* http://www.ti.com/                                                        */
/*                                                                           */
/*  Redistribution and  use in source  and binary forms, with  or without    */
/*  modification,  are permitted provided  that the  following conditions    */
/*  are met:                                                                 */
/*                                                                           */
/*     Redistributions  of source  code must  retain the  above copyright    */
/*     notice, this list of conditions and the following disclaimer.         */
/*                                                                           */
/*     Redistributions in binary form  must reproduce the above copyright    */
/*     notice, this  list of conditions  and the following  disclaimer in    */
/*     the  documentation  and/or   other  materials  provided  with  the    */
/*     distribution.                                                         */
/*                                                                           */
/*     Neither the  name of Texas Instruments Incorporated  nor the names    */
/*     of its  contributors may  be used to  endorse or  promote products    */
/*     derived  from   this  software  without   specific  prior  written    */
/*     permission.                                                           */
/*                                                                           */
/*  THIS SOFTWARE  IS PROVIDED BY THE COPYRIGHT  HOLDERS AND CONTRIBUTORS    */
/*  "AS IS"  AND ANY  EXPRESS OR IMPLIED  WARRANTIES, INCLUDING,  BUT NOT    */
/*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR    */
/*  A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT    */
/*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,    */
/*  SPECIAL,  EXEMPLARY,  OR CONSEQUENTIAL  DAMAGES  (INCLUDING, BUT  NOT    */
/*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,    */
/*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY    */
/*  THEORY OF  LIABILITY, WHETHER IN CONTRACT, STRICT  LIABILITY, OR TORT    */
/*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE    */
/*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.     */
/*                                                                           */
/*****************************************************************************/
#ifndef C7X_INTR_H
#define C7X_INTR_H

#include <c7x.h>
#include <ti_he_impl/vector.h>
#include <ti_he_impl/vector_funcs.h>
#include <ti_he_impl/c7x_cr.h>

#ifdef _MSC_VER
#undef __max
#undef __min
/* __MINGW32__ is defined in both 32 bit and 64 bit MINGW */
#elif defined __MINGW32__
#undef __max
#undef __min
#endif

/*****************************************************************************/
/* NOTE OF CLARIFICATION: Functions that are declared with a reference to a  */
/* vector type are declared using the EQUIV_ACCESS_T<DEPTH> type alias, e.g.:*/
/*                                                                           */
/* void __max_circ_pred(char2 a,char2::EQUIV_ACCESS_T<DEPTH>& b, __vpred& c);*/
/*                                                                           */
/* This requires no special action on the part of the user, who from the     */
/* point of view of the source code ought to interpret this declaration as   */
/* being no different than the following:                                    */
/*                                                                           */
/*   void __max_circ_pred(char2 a, char2& b, __vpred& c);                    */
/*                                                                           */
/* This means that the user may pass a variable of the corresponding vector  */
/* type to the function, and a reference will be used to write data into the */
/* variable as output. The EQUIV_ACCESS_T type alias is an implementation    */
/* detail of the C7x host emulation framework to provide writable access     */
/* via the vector type reference argument.                                   */
/*****************************************************************************/

/*-----------------------------------------------------------------------------
* ID: __abs
*----------------------------------------------------------------------------*/

/* VABSB */
int8_t __abs(int8_t);
char2 __abs(char2);
/* UNSUPPORTED: char3 __abs(char3); */
char4 __abs(char4);
char8 __abs(char8);
char16 __abs(char16);
char32 __abs(char32);
char64 __abs(char64);
cchar __abs(cchar);
cchar2 __abs(cchar2);
cchar4 __abs(cchar4);
cchar8 __abs(cchar8);
cchar16 __abs(cchar16);
cchar32 __abs(cchar32);

/* VABSD */
int64_t __abs(int64_t);
long2 __abs(long2);
/* UNSUPPORTED: long3 __abs(long3); */
long4 __abs(long4);
long8 __abs(long8);
clong __abs(clong);
clong2 __abs(clong2);
clong4 __abs(clong4);

/* VABSDP */
double __abs(double);
double2 __abs(double2);
/* UNSUPPORTED: double3 __abs(double3); */
double4 __abs(double4);
double8 __abs(double8);
cdouble __abs(cdouble);
cdouble2 __abs(cdouble2);
cdouble4 __abs(cdouble4);

/* VABSH */
int16_t __abs(int16_t);
short2 __abs(short2);
/* UNSUPPORTED: short3 __abs(short3); */
short4 __abs(short4);
short8 __abs(short8);
short16 __abs(short16);
short32 __abs(short32);
cshort __abs(cshort);
cshort2 __abs(cshort2);
cshort4 __abs(cshort4);
cshort8 __abs(cshort8);
cshort16 __abs(cshort16);

/* VABSSP */
float __abs(float);
float2 __abs(float2);
/* UNSUPPORTED: float3 __abs(float3); */
float4 __abs(float4);
float8 __abs(float8);
float16 __abs(float16);
cfloat __abs(cfloat);
cfloat2 __abs(cfloat2);
cfloat4 __abs(cfloat4);
cfloat8 __abs(cfloat8);

/* VABSW */
int32_t __abs(int32_t);
int2 __abs(int2);
/* UNSUPPORTED: int3 __abs(int3); */
int4 __abs(int4);
int8 __abs(int8);
int16 __abs(int16);
cint __abs(cint);
cint2 __abs(cint2);
cint4 __abs(cint4);
cint8 __abs(cint8);




/*-----------------------------------------------------------------------------
* ID: __abs_diff
*----------------------------------------------------------------------------*/

/* VSUBABSB */
uchar __abs_diff(uchar, uchar);
uchar2 __abs_diff(uchar2, uchar2);
/* UNSUPPORTED: uchar3 __abs_diff(uchar3, uchar3); */
uchar4 __abs_diff(uchar4, uchar4);
uchar8 __abs_diff(uchar8, uchar8);
uchar16 __abs_diff(uchar16, uchar16);
uchar32 __abs_diff(uchar32, uchar32);
uchar64 __abs_diff(uchar64, uchar64);

/* VSUBABSD */
ulong __abs_diff(ulong, ulong);
ulong2 __abs_diff(ulong2, ulong2);
/* UNSUPPORTED: ulong3 __abs_diff(ulong3, ulong3); */
ulong4 __abs_diff(ulong4, ulong4);
ulong8 __abs_diff(ulong8, ulong8);

/* VSUBABSH */
ushort __abs_diff(ushort, ushort);
ushort2 __abs_diff(ushort2, ushort2);
/* UNSUPPORTED: ushort3 __abs_diff(ushort3, ushort3); */
ushort4 __abs_diff(ushort4, ushort4);
ushort8 __abs_diff(ushort8, ushort8);
ushort16 __abs_diff(ushort16, ushort16);
ushort32 __abs_diff(ushort32, ushort32);

/* VSUBABSW */
uint __abs_diff(uint, uint);
uint2 __abs_diff(uint2, uint2);
/* UNSUPPORTED: uint3 __abs_diff(uint3, uint3); */
uint4 __abs_diff(uint4, uint4);
uint8 __abs_diff(uint8, uint8);
uint16 __abs_diff(uint16, uint16);




/*-----------------------------------------------------------------------------
* ID: __add
*----------------------------------------------------------------------------*/

/* VADDCB */
int8_t __add(__vpred, int8_t, int8_t);
char2 __add(__vpred, char2, char2);
/* UNSUPPORTED: char3 __add(__vpred, char3, char3); */
char4 __add(__vpred, char4, char4);
char8 __add(__vpred, char8, char8);
char16 __add(__vpred, char16, char16);
char32 __add(__vpred, char32, char32);
char64 __add(__vpred, char64, char64);
uchar __add(__vpred, uchar, uchar);
uchar2 __add(__vpred, uchar2, uchar2);
/* UNSUPPORTED: uchar3 __add(__vpred, uchar3, uchar3); */
uchar4 __add(__vpred, uchar4, uchar4);
uchar8 __add(__vpred, uchar8, uchar8);
uchar16 __add(__vpred, uchar16, uchar16);
uchar32 __add(__vpred, uchar32, uchar32);
uchar64 __add(__vpred, uchar64, uchar64);

/* VADDCD */
int64_t __add(__vpred, int64_t, int64_t);
long2 __add(__vpred, long2, long2);
/* UNSUPPORTED: long3 __add(__vpred, long3, long3); */
long4 __add(__vpred, long4, long4);
long8 __add(__vpred, long8, long8);
ulong __add(__vpred, ulong, ulong);
ulong2 __add(__vpred, ulong2, ulong2);
/* UNSUPPORTED: ulong3 __add(__vpred, ulong3, ulong3); */
ulong4 __add(__vpred, ulong4, ulong4);
ulong8 __add(__vpred, ulong8, ulong8);

/* VADDCH */
int16_t __add(__vpred, int16_t, int16_t);
short2 __add(__vpred, short2, short2);
/* UNSUPPORTED: short3 __add(__vpred, short3, short3); */
short4 __add(__vpred, short4, short4);
short8 __add(__vpred, short8, short8);
short16 __add(__vpred, short16, short16);
short32 __add(__vpred, short32, short32);
ushort __add(__vpred, ushort, ushort);
ushort2 __add(__vpred, ushort2, ushort2);
/* UNSUPPORTED: ushort3 __add(__vpred, ushort3, ushort3); */
ushort4 __add(__vpred, ushort4, ushort4);
ushort8 __add(__vpred, ushort8, ushort8);
ushort16 __add(__vpred, ushort16, ushort16);
ushort32 __add(__vpred, ushort32, ushort32);

/* VADDCW */
int32_t __add(__vpred, int32_t, int32_t);
int2 __add(__vpred, int2, int2);
/* UNSUPPORTED: int3 __add(__vpred, int3, int3); */
int4 __add(__vpred, int4, int4);
int8 __add(__vpred, int8, int8);
int16 __add(__vpred, int16, int16);
uint __add(__vpred, uint, uint);
uint2 __add(__vpred, uint2, uint2);
/* UNSUPPORTED: uint3 __add(__vpred, uint3, uint3); */
uint4 __add(__vpred, uint4, uint4);
uint8 __add(__vpred, uint8, uint8);
uint16 __add(__vpred, uint16, uint16);




/*-----------------------------------------------------------------------------
* ID: __add_operator
*----------------------------------------------------------------------------*/
/*

ADDD
long = long + long;
long = long + (long)(k);
ulong = ulong + ulong;
ulong = ulong + (ulong)(k);

ADDDP
double = double + double;

ADDSP
float = float + float;

ADDW
int = int + int;
int = int + (int)(k);
uint = uint + uint;
uint = uint + (uint)(k);

VADDB
char = char + char;
char2 = char2 + char2;
char3 = char3 + char3;
char4 = char4 + char4;
char8 = char8 + char8;
char16 = char16 + char16;
char32 = char32 + char32;
char64 = char64 + char64;
char = char + (char)(k);
char2 = char2 + (char2)(k);
char3 = char3 + (char3)(k);
char4 = char4 + (char4)(k);
char8 = char8 + (char8)(k);
char16 = char16 + (char16)(k);
char32 = char32 + (char32)(k);
char64 = char64 + (char64)(k);
cchar = cchar + cchar;
cchar2 = cchar2 + cchar2;
cchar4 = cchar4 + cchar4;
cchar8 = cchar8 + cchar8;
cchar16 = cchar16 + cchar16;
cchar32 = cchar32 + cchar32;
uchar = uchar + uchar;
uchar2 = uchar2 + uchar2;
uchar3 = uchar3 + uchar3;
uchar4 = uchar4 + uchar4;
uchar8 = uchar8 + uchar8;
uchar16 = uchar16 + uchar16;
uchar32 = uchar32 + uchar32;
uchar64 = uchar64 + uchar64;
cchar = cchar + (cchar)(k);
cchar2 = cchar2 + (cchar2)(k);
cchar4 = cchar4 + (cchar4)(k);
cchar8 = cchar8 + (cchar8)(k);
cchar16 = cchar16 + (cchar16)(k);
cchar32 = cchar32 + (cchar32)(k);
uchar = uchar + (uchar)(k);
uchar2 = uchar2 + (uchar2)(k);
uchar3 = uchar3 + (uchar3)(k);
uchar4 = uchar4 + (uchar4)(k);
uchar8 = uchar8 + (uchar8)(k);
uchar16 = uchar16 + (uchar16)(k);
uchar32 = uchar32 + (uchar32)(k);
uchar64 = uchar64 + (uchar64)(k);

VADDD
long = long + long;
long2 = long2 + long2;
long3 = long3 + long3;
long4 = long4 + long4;
long8 = long8 + long8;
long = long + (long)(k);
long2 = long2 + (long2)(k);
long3 = long3 + (long3)(k);
long4 = long4 + (long4)(k);
long8 = long8 + (long8)(k);
clong = clong + clong;
clong2 = clong2 + clong2;
clong4 = clong4 + clong4;
ulong = ulong + ulong;
ulong2 = ulong2 + ulong2;
ulong3 = ulong3 + ulong3;
ulong4 = ulong4 + ulong4;
ulong8 = ulong8 + ulong8;
clong = clong + (clong)(k);
clong2 = clong2 + (clong2)(k);
clong4 = clong4 + (clong4)(k);
ulong = ulong + (ulong)(k);
ulong2 = ulong2 + (ulong2)(k);
ulong3 = ulong3 + (ulong3)(k);
ulong4 = ulong4 + (ulong4)(k);
ulong8 = ulong8 + (ulong8)(k);

VADDDP
double = double + double;
double2 = double2 + double2;
double3 = double3 + double3;
double4 = double4 + double4;
double8 = double8 + double8;
cdouble = cdouble + cdouble;
cdouble2 = cdouble2 + cdouble2;
cdouble4 = cdouble4 + cdouble4;

VADDH
short = short + short;
short2 = short2 + short2;
short3 = short3 + short3;
short4 = short4 + short4;
short8 = short8 + short8;
short16 = short16 + short16;
short32 = short32 + short32;
short = short + (short)(k);
short2 = short2 + (short2)(k);
short3 = short3 + (short3)(k);
short4 = short4 + (short4)(k);
short8 = short8 + (short8)(k);
short16 = short16 + (short16)(k);
short32 = short32 + (short32)(k);
cshort = cshort + cshort;
cshort2 = cshort2 + cshort2;
cshort4 = cshort4 + cshort4;
cshort8 = cshort8 + cshort8;
cshort16 = cshort16 + cshort16;
ushort = ushort + ushort;
ushort2 = ushort2 + ushort2;
ushort3 = ushort3 + ushort3;
ushort4 = ushort4 + ushort4;
ushort8 = ushort8 + ushort8;
ushort16 = ushort16 + ushort16;
ushort32 = ushort32 + ushort32;
cshort = cshort + (cshort)(k);
cshort2 = cshort2 + (cshort2)(k);
cshort4 = cshort4 + (cshort4)(k);
cshort8 = cshort8 + (cshort8)(k);
cshort16 = cshort16 + (cshort16)(k);
ushort = ushort + (ushort)(k);
ushort2 = ushort2 + (ushort2)(k);
ushort3 = ushort3 + (ushort3)(k);
ushort4 = ushort4 + (ushort4)(k);
ushort8 = ushort8 + (ushort8)(k);
ushort16 = ushort16 + (ushort16)(k);
ushort32 = ushort32 + (ushort32)(k);

VADDSP
float = float + float;
float2 = float2 + float2;
float3 = float3 + float3;
float4 = float4 + float4;
float8 = float8 + float8;
float16 = float16 + float16;
cfloat = cfloat + cfloat;
cfloat2 = cfloat2 + cfloat2;
cfloat4 = cfloat4 + cfloat4;
cfloat8 = cfloat8 + cfloat8;

VADDW
int = int + int;
int2 = int2 + int2;
int3 = int3 + int3;
int4 = int4 + int4;
int8 = int8 + int8;
int16 = int16 + int16;
int = int + (int)(k);
int2 = int2 + (int2)(k);
int3 = int3 + (int3)(k);
int4 = int4 + (int4)(k);
int8 = int8 + (int8)(k);
int16 = int16 + (int16)(k);
cint = cint + cint;
cint2 = cint2 + cint2;
cint4 = cint4 + cint4;
cint8 = cint8 + cint8;
uint = uint + uint;
uint2 = uint2 + uint2;
uint3 = uint3 + uint3;
uint4 = uint4 + uint4;
uint8 = uint8 + uint8;
uint16 = uint16 + uint16;
cint = cint + (cint)(k);
cint2 = cint2 + (cint2)(k);
cint4 = cint4 + (cint4)(k);
cint8 = cint8 + (cint8)(k);
uint = uint + (uint)(k);
uint2 = uint2 + (uint2)(k);
uint3 = uint3 + (uint3)(k);
uint4 = uint4 + (uint4)(k);
uint8 = uint8 + (uint8)(k);
uint16 = uint16 + (uint16)(k);

*/


/*-----------------------------------------------------------------------------
* ID: __add_sat
*----------------------------------------------------------------------------*/

/* VSADDB */
int8_t __add_sat(int8_t, int8_t);
char2 __add_sat(char2, char2);
/* UNSUPPORTED: char3 __add_sat(char3, char3); */
char4 __add_sat(char4, char4);
char8 __add_sat(char8, char8);
char16 __add_sat(char16, char16);
char32 __add_sat(char32, char32);
char64 __add_sat(char64, char64);
/* CONSTANT: int8_t __add_sat(int8_t, (int8_t)(k)); */
/* CONSTANT: char2 __add_sat(char2, (char2)(k)); */
/* UNSUPPORTED: char3 __add_sat(char3, (char3)(k)); */
/* CONSTANT: char4 __add_sat(char4, (char4)(k)); */
/* CONSTANT: char8 __add_sat(char8, (char8)(k)); */
/* CONSTANT: char16 __add_sat(char16, (char16)(k)); */
/* CONSTANT: char32 __add_sat(char32, (char32)(k)); */
/* CONSTANT: char64 __add_sat(char64, (char64)(k)); */
cchar __add_sat(cchar, cchar);
cchar2 __add_sat(cchar2, cchar2);
cchar4 __add_sat(cchar4, cchar4);
cchar8 __add_sat(cchar8, cchar8);
cchar16 __add_sat(cchar16, cchar16);
cchar32 __add_sat(cchar32, cchar32);
/* CONSTANT: cchar __add_sat(cchar, (cchar)(k)); */
/* CONSTANT: cchar2 __add_sat(cchar2, (cchar2)(k)); */
/* CONSTANT: cchar4 __add_sat(cchar4, (cchar4)(k)); */
/* CONSTANT: cchar8 __add_sat(cchar8, (cchar8)(k)); */
/* CONSTANT: cchar16 __add_sat(cchar16, (cchar16)(k)); */
/* CONSTANT: cchar32 __add_sat(cchar32, (cchar32)(k)); */

/* VSADDH */
int16_t __add_sat(int16_t, int16_t);
short2 __add_sat(short2, short2);
/* UNSUPPORTED: short3 __add_sat(short3, short3); */
short4 __add_sat(short4, short4);
short8 __add_sat(short8, short8);
short16 __add_sat(short16, short16);
short32 __add_sat(short32, short32);
/* CONSTANT: int16_t __add_sat(int16_t, (int16_t)(k)); */
/* CONSTANT: short2 __add_sat(short2, (short2)(k)); */
/* UNSUPPORTED: short3 __add_sat(short3, (short3)(k)); */
/* CONSTANT: short4 __add_sat(short4, (short4)(k)); */
/* CONSTANT: short8 __add_sat(short8, (short8)(k)); */
/* CONSTANT: short16 __add_sat(short16, (short16)(k)); */
/* CONSTANT: short32 __add_sat(short32, (short32)(k)); */
cshort __add_sat(cshort, cshort);
cshort2 __add_sat(cshort2, cshort2);
cshort4 __add_sat(cshort4, cshort4);
cshort8 __add_sat(cshort8, cshort8);
cshort16 __add_sat(cshort16, cshort16);
/* CONSTANT: cshort __add_sat(cshort, (cshort)(k)); */
/* CONSTANT: cshort2 __add_sat(cshort2, (cshort2)(k)); */
/* CONSTANT: cshort4 __add_sat(cshort4, (cshort4)(k)); */
/* CONSTANT: cshort8 __add_sat(cshort8, (cshort8)(k)); */
/* CONSTANT: cshort16 __add_sat(cshort16, (cshort16)(k)); */

/* VSADDUB */
uchar __add_sat(uchar, uchar);
uchar2 __add_sat(uchar2, uchar2);
/* UNSUPPORTED: uchar3 __add_sat(uchar3, uchar3); */
uchar4 __add_sat(uchar4, uchar4);
uchar8 __add_sat(uchar8, uchar8);
uchar16 __add_sat(uchar16, uchar16);
uchar32 __add_sat(uchar32, uchar32);
uchar64 __add_sat(uchar64, uchar64);

/* VSADDUH */
ushort __add_sat(ushort, ushort);
ushort2 __add_sat(ushort2, ushort2);
/* UNSUPPORTED: ushort3 __add_sat(ushort3, ushort3); */
ushort4 __add_sat(ushort4, ushort4);
ushort8 __add_sat(ushort8, ushort8);
ushort16 __add_sat(ushort16, ushort16);
ushort32 __add_sat(ushort32, ushort32);

/* VSADDUSB */
uchar __add_sat(uchar, int8_t);
uchar2 __add_sat(uchar2, char2);
/* UNSUPPORTED: uchar3 __add_sat(uchar3, char3); */
uchar4 __add_sat(uchar4, char4);
uchar8 __add_sat(uchar8, char8);
uchar16 __add_sat(uchar16, char16);
uchar32 __add_sat(uchar32, char32);
uchar64 __add_sat(uchar64, char64);

/* VSADDUSH */
ushort __add_sat(ushort, int16_t);
ushort2 __add_sat(ushort2, short2);
/* UNSUPPORTED: ushort3 __add_sat(ushort3, short3); */
ushort4 __add_sat(ushort4, short4);
ushort8 __add_sat(ushort8, short8);
ushort16 __add_sat(ushort16, short16);
ushort32 __add_sat(ushort32, short32);

/* VSADDUSW */
uint __add_sat(uint, int32_t);
uint2 __add_sat(uint2, int2);
/* UNSUPPORTED: uint3 __add_sat(uint3, int3); */
uint4 __add_sat(uint4, int4);
uint8 __add_sat(uint8, int8);
uint16 __add_sat(uint16, int16);

/* VSADDUW */
uint __add_sat(uint, uint);
uint2 __add_sat(uint2, uint2);
/* UNSUPPORTED: uint3 __add_sat(uint3, uint3); */
uint4 __add_sat(uint4, uint4);
uint8 __add_sat(uint8, uint8);
uint16 __add_sat(uint16, uint16);

/* VSADDW */
int32_t __add_sat(int32_t, int32_t);
int2 __add_sat(int2, int2);
/* UNSUPPORTED: int3 __add_sat(int3, int3); */
int4 __add_sat(int4, int4);
int8 __add_sat(int8, int8);
int16 __add_sat(int16, int16);
/* CONSTANT: int32_t __add_sat(int32_t, (int32_t)(k)); */
/* CONSTANT: int2 __add_sat(int2, (int2)(k)); */
/* UNSUPPORTED: int3 __add_sat(int3, (int3)(k)); */
/* CONSTANT: int4 __add_sat(int4, (int4)(k)); */
/* CONSTANT: int8 __add_sat(int8, (int8)(k)); */
/* CONSTANT: int16 __add_sat(int16, (int16)(k)); */
cint __add_sat(cint, cint);
cint2 __add_sat(cint2, cint2);
cint4 __add_sat(cint4, cint4);
cint8 __add_sat(cint8, cint8);
/* CONSTANT: cint __add_sat(cint, (cint)(k)); */
/* CONSTANT: cint2 __add_sat(cint2, (cint2)(k)); */
/* CONSTANT: cint4 __add_sat(cint4, (cint4)(k)); */
/* CONSTANT: cint8 __add_sat(cint8, (cint8)(k)); */




/*-----------------------------------------------------------------------------
* ID: __addd_ddd
*----------------------------------------------------------------------------*/

/* ADDD */
int64_t __addd_ddd(int64_t, int64_t);
ulong __addd_ddd(ulong, ulong);




/*-----------------------------------------------------------------------------
* ID: __addd_dkd
*----------------------------------------------------------------------------*/

/* ADDD */
int64_t __addd_dkd(int64_t, int64_t);
ulong __addd_dkd(ulong, ulong);




/*-----------------------------------------------------------------------------
* ID: __adddp_ddd
*----------------------------------------------------------------------------*/

/* ADDDP */
double __adddp_ddd(double, double);




/*-----------------------------------------------------------------------------
* ID: __addsp_rrr
*----------------------------------------------------------------------------*/

/* ADDSP */
float __addsp_rrr(float, float);




/*-----------------------------------------------------------------------------
* ID: __addw_rkr
*----------------------------------------------------------------------------*/

/* ADDW */
int32_t __addw_rkr(int32_t, int32_t);
uint __addw_rkr(uint, uint);




/*-----------------------------------------------------------------------------
* ID: __addw_rrr
*----------------------------------------------------------------------------*/

/* ADDW */
int32_t __addw_rrr(int32_t, int32_t);
uint __addw_rrr(uint, uint);




/*-----------------------------------------------------------------------------
* ID: __and
*----------------------------------------------------------------------------*/

/* AND */
__vpred __and(__vpred, __vpred);




/*-----------------------------------------------------------------------------
* ID: __and_ppp
*----------------------------------------------------------------------------*/

/* AND */
__vpred __and_ppp(__vpred, __vpred);




/*-----------------------------------------------------------------------------
* ID: __andb_operator
*----------------------------------------------------------------------------*/
/*

ANDD
char = char & char;
char2 = char2 & char2;
char3 = char3 & char3;
char4 = char4 & char4;
char8 = char8 & char8;
short = short & short;
short2 = short2 & short2;
short3 = short3 & short3;
short4 = short4 & short4;
int = int & int;
int2 = int2 & int2;
long = long & long;
long = long & (long)(k);
uchar = uchar & uchar;
uchar2 = uchar2 & uchar2;
uchar3 = uchar3 & uchar3;
uchar4 = uchar4 & uchar4;
uchar8 = uchar8 & uchar8;
ushort = ushort & ushort;
ushort2 = ushort2 & ushort2;
ushort3 = ushort3 & ushort3;
ushort4 = ushort4 & ushort4;
uint = uint & uint;
uint2 = uint2 & uint2;
ulong = ulong & ulong;
ulong = ulong & (ulong)(k);

ANDW
char = char & char;
char2 = char2 & char2;
char3 = char3 & char3;
char4 = char4 & char4;
short = short & short;
short2 = short2 & short2;
int = int & int;
char = char & (char)(k);
char2 = char2 & (char2)(k);
char3 = char3 & (char3)(k);
char4 = char4 & (char4)(k);
short = short & (short)(k);
short2 = short2 & (short2)(k);
int = int & (int)(k);
uchar = uchar & uchar;
uchar2 = uchar2 & uchar2;
uchar3 = uchar3 & uchar3;
uchar4 = uchar4 & uchar4;
ushort = ushort & ushort;
ushort2 = ushort2 & ushort2;
uint = uint & uint;
uchar = uchar & (uchar)(k);
uchar2 = uchar2 & (uchar2)(k);
uchar3 = uchar3 & (uchar3)(k);
uchar4 = uchar4 & (uchar4)(k);
ushort = ushort & (ushort)(k);
ushort2 = ushort2 & (ushort2)(k);
uint = uint & (uint)(k);

VANDW
char = char & char;
char2 = char2 & char2;
char3 = char3 & char3;
char4 = char4 & char4;
char8 = char8 & char8;
char16 = char16 & char16;
char32 = char32 & char32;
char64 = char64 & char64;
short = short & short;
short2 = short2 & short2;
short3 = short3 & short3;
short4 = short4 & short4;
short8 = short8 & short8;
short16 = short16 & short16;
short32 = short32 & short32;
int = int & int;
int2 = int2 & int2;
int3 = int3 & int3;
int4 = int4 & int4;
int8 = int8 & int8;
int16 = int16 & int16;
long = long & long;
long2 = long2 & long2;
long3 = long3 & long3;
long4 = long4 & long4;
long8 = long8 & long8;
int = int & (int)(k);
int2 = int2 & (int2)(k);
int3 = int3 & (int3)(k);
int4 = int4 & (int4)(k);
int8 = int8 & (int8)(k);
int16 = int16 & (int16)(k);
uchar = uchar & uchar;
uchar2 = uchar2 & uchar2;
uchar3 = uchar3 & uchar3;
uchar4 = uchar4 & uchar4;
uchar8 = uchar8 & uchar8;
uchar16 = uchar16 & uchar16;
uchar32 = uchar32 & uchar32;
uchar64 = uchar64 & uchar64;
ushort = ushort & ushort;
ushort2 = ushort2 & ushort2;
ushort3 = ushort3 & ushort3;
ushort4 = ushort4 & ushort4;
ushort8 = ushort8 & ushort8;
ushort16 = ushort16 & ushort16;
ushort32 = ushort32 & ushort32;
uint = uint & uint;
uint2 = uint2 & uint2;
uint3 = uint3 & uint3;
uint4 = uint4 & uint4;
uint8 = uint8 & uint8;
uint16 = uint16 & uint16;
ulong = ulong & ulong;
ulong2 = ulong2 & ulong2;
ulong3 = ulong3 & ulong3;
ulong4 = ulong4 & ulong4;
ulong8 = ulong8 & ulong8;
uint = uint & (uint)(k);
uint2 = uint2 & (uint2)(k);
uint3 = uint3 & (uint3)(k);
uint4 = uint4 & (uint4)(k);
uint8 = uint8 & (uint8)(k);
uint16 = uint16 & (uint16)(k);

*/


/*-----------------------------------------------------------------------------
* ID: __andd_ddd
*----------------------------------------------------------------------------*/

/* ANDD */
char8 __andd_ddd(char8, char8);
short4 __andd_ddd(short4, short4);
int2 __andd_ddd(int2, int2);
int64_t __andd_ddd(int64_t, int64_t);
uchar8 __andd_ddd(uchar8, uchar8);
ushort4 __andd_ddd(ushort4, ushort4);
uint2 __andd_ddd(uint2, uint2);
ulong __andd_ddd(ulong, ulong);




/*-----------------------------------------------------------------------------
* ID: __andd_dkd
*----------------------------------------------------------------------------*/

/* ANDD */
int64_t __andd_dkd(int64_t, int64_t);
ulong __andd_dkd(ulong, ulong);




/*-----------------------------------------------------------------------------
* ID: __andn
*----------------------------------------------------------------------------*/

/* ANDN */
__vpred __andn(__vpred, __vpred);

/* ANDND */
int8_t __andn(int8_t, int8_t);
char2 __andn(char2, char2);
/* UNSUPPORTED: char3 __andn(char3, char3); */
char4 __andn(char4, char4);
char8 __andn(char8, char8);
int16_t __andn(int16_t, int16_t);
short2 __andn(short2, short2);
/* UNSUPPORTED: short3 __andn(short3, short3); */
short4 __andn(short4, short4);
int32_t __andn(int32_t, int32_t);
int2 __andn(int2, int2);
int64_t __andn(int64_t, int64_t);
/* CONSTANT: int64_t __andn(int64_t, (int64_t)(k)); */
uchar __andn(uchar, uchar);
uchar2 __andn(uchar2, uchar2);
/* UNSUPPORTED: uchar3 __andn(uchar3, uchar3); */
uchar4 __andn(uchar4, uchar4);
uchar8 __andn(uchar8, uchar8);
ushort __andn(ushort, ushort);
ushort2 __andn(ushort2, ushort2);
/* UNSUPPORTED: ushort3 __andn(ushort3, ushort3); */
ushort4 __andn(ushort4, ushort4);
uint __andn(uint, uint);
uint2 __andn(uint2, uint2);
ulong __andn(ulong, ulong);
/* CONSTANT: ulong __andn(ulong, (ulong)(k)); */

/* ANDNW */
int8_t __andn(int8_t, int8_t);
char2 __andn(char2, char2);
/* UNSUPPORTED: char3 __andn(char3, char3); */
char4 __andn(char4, char4);
int16_t __andn(int16_t, int16_t);
short2 __andn(short2, short2);
int32_t __andn(int32_t, int32_t);
/* CONSTANT: int8_t __andn(int8_t, (int8_t)(k)); */
/* CONSTANT: char2 __andn(char2, (char2)(k)); */
/* UNSUPPORTED: char3 __andn(char3, (char3)(k)); */
/* CONSTANT: char4 __andn(char4, (char4)(k)); */
/* CONSTANT: int16_t __andn(int16_t, (int16_t)(k)); */
/* CONSTANT: short2 __andn(short2, (short2)(k)); */
/* CONSTANT: int32_t __andn(int32_t, (int32_t)(k)); */
uchar __andn(uchar, uchar);
uchar2 __andn(uchar2, uchar2);
/* UNSUPPORTED: uchar3 __andn(uchar3, uchar3); */
uchar4 __andn(uchar4, uchar4);
ushort __andn(ushort, ushort);
ushort2 __andn(ushort2, ushort2);
uint __andn(uint, uint);
/* CONSTANT: uchar __andn(uchar, (uchar)(k)); */
/* CONSTANT: uchar2 __andn(uchar2, (uchar2)(k)); */
/* UNSUPPORTED: uchar3 __andn(uchar3, (uchar3)(k)); */
/* CONSTANT: uchar4 __andn(uchar4, (uchar4)(k)); */
/* CONSTANT: ushort __andn(ushort, (ushort)(k)); */
/* CONSTANT: ushort2 __andn(ushort2, (ushort2)(k)); */
/* CONSTANT: uint __andn(uint, (uint)(k)); */

/* VANDNW */
int8_t __andn(int8_t, int8_t);
char2 __andn(char2, char2);
/* UNSUPPORTED: char3 __andn(char3, char3); */
char4 __andn(char4, char4);
char8 __andn(char8, char8);
char16 __andn(char16, char16);
char32 __andn(char32, char32);
char64 __andn(char64, char64);
int16_t __andn(int16_t, int16_t);
short2 __andn(short2, short2);
/* UNSUPPORTED: short3 __andn(short3, short3); */
short4 __andn(short4, short4);
short8 __andn(short8, short8);
short16 __andn(short16, short16);
short32 __andn(short32, short32);
int32_t __andn(int32_t, int32_t);
int2 __andn(int2, int2);
/* UNSUPPORTED: int3 __andn(int3, int3); */
int4 __andn(int4, int4);
int8 __andn(int8, int8);
int16 __andn(int16, int16);
int64_t __andn(int64_t, int64_t);
long2 __andn(long2, long2);
/* UNSUPPORTED: long3 __andn(long3, long3); */
long4 __andn(long4, long4);
long8 __andn(long8, long8);
/* CONSTANT: int32_t __andn(int32_t, (int32_t)(k)); */
/* CONSTANT: int2 __andn(int2, (int2)(k)); */
/* UNSUPPORTED: int3 __andn(int3, (int3)(k)); */
/* CONSTANT: int4 __andn(int4, (int4)(k)); */
/* CONSTANT: int8 __andn(int8, (int8)(k)); */
/* CONSTANT: int16 __andn(int16, (int16)(k)); */




/*-----------------------------------------------------------------------------
* ID: __andn_ppp
*----------------------------------------------------------------------------*/

/* ANDN */
__vpred __andn_ppp(__vpred, __vpred);




/*-----------------------------------------------------------------------------
* ID: __andnd_ddd
*----------------------------------------------------------------------------*/

/* ANDND */
char8 __andnd_ddd(char8, char8);
short4 __andnd_ddd(short4, short4);
int2 __andnd_ddd(int2, int2);
int64_t __andnd_ddd(int64_t, int64_t);
uchar8 __andnd_ddd(uchar8, uchar8);
ushort4 __andnd_ddd(ushort4, ushort4);
uint2 __andnd_ddd(uint2, uint2);
ulong __andnd_ddd(ulong, ulong);




/*-----------------------------------------------------------------------------
* ID: __andnd_dkd
*----------------------------------------------------------------------------*/

/* ANDND */
int64_t __andnd_dkd(int64_t, int64_t);
ulong __andnd_dkd(ulong, ulong);




/*-----------------------------------------------------------------------------
* ID: __andnw_rkr
*----------------------------------------------------------------------------*/

/* ANDNW */
char4 __andnw_rkr(char4, char4);
short2 __andnw_rkr(short2, short2);
int32_t __andnw_rkr(int32_t, int32_t);
uchar4 __andnw_rkr(uchar4, uchar4);
ushort2 __andnw_rkr(ushort2, ushort2);
uint __andnw_rkr(uint, uint);




/*-----------------------------------------------------------------------------
* ID: __andnw_rrr
*----------------------------------------------------------------------------*/

/* ANDNW */
char4 __andnw_rrr(char4, char4);
short2 __andnw_rrr(short2, short2);
int32_t __andnw_rrr(int32_t, int32_t);
uchar4 __andnw_rrr(uchar4, uchar4);
ushort2 __andnw_rrr(ushort2, ushort2);
uint __andnw_rrr(uint, uint);




/*-----------------------------------------------------------------------------
* ID: __andw_rkr
*----------------------------------------------------------------------------*/

/* ANDW */
char4 __andw_rkr(char4, char4);
short2 __andw_rkr(short2, short2);
int32_t __andw_rkr(int32_t, int32_t);
uchar4 __andw_rkr(uchar4, uchar4);
ushort2 __andw_rkr(ushort2, ushort2);
uint __andw_rkr(uint, uint);




/*-----------------------------------------------------------------------------
* ID: __andw_rrr
*----------------------------------------------------------------------------*/

/* ANDW */
char4 __andw_rrr(char4, char4);
short2 __andw_rrr(short2, short2);
int32_t __andw_rrr(int32_t, int32_t);
uchar4 __andw_rrr(uchar4, uchar4);
ushort2 __andw_rrr(ushort2, ushort2);
uint __andw_rrr(uint, uint);




/*-----------------------------------------------------------------------------
* ID: __apply_sign
*----------------------------------------------------------------------------*/

/* VAPYSB */
int8_t __apply_sign(int8_t, int8_t);
char2 __apply_sign(char2, char2);
/* UNSUPPORTED: char3 __apply_sign(char3, char3); */
char4 __apply_sign(char4, char4);
char8 __apply_sign(char8, char8);
char16 __apply_sign(char16, char16);
char32 __apply_sign(char32, char32);
char64 __apply_sign(char64, char64);

/* VAPYSD */
int64_t __apply_sign(int64_t, int64_t);
long2 __apply_sign(long2, long2);
/* UNSUPPORTED: long3 __apply_sign(long3, long3); */
long4 __apply_sign(long4, long4);
long8 __apply_sign(long8, long8);

/* VAPYSH */
int16_t __apply_sign(int16_t, int16_t);
short2 __apply_sign(short2, short2);
/* UNSUPPORTED: short3 __apply_sign(short3, short3); */
short4 __apply_sign(short4, short4);
short8 __apply_sign(short8, short8);
short16 __apply_sign(short16, short16);
short32 __apply_sign(short32, short32);

/* VAPYSW */
int32_t __apply_sign(int32_t, int32_t);
int2 __apply_sign(int2, int2);
/* UNSUPPORTED: int3 __apply_sign(int3, int3); */
int4 __apply_sign(int4, int4);
int8 __apply_sign(int8, int8);
int16 __apply_sign(int16, int16);




/*-----------------------------------------------------------------------------
* ID: __binary_log
*----------------------------------------------------------------------------*/

/* VBINLOGW */
uint __binary_log(uint);
uint2 __binary_log(uint2);
/* UNSUPPORTED: uint3 __binary_log(uint3); */
uint4 __binary_log(uint4);
uint8 __binary_log(uint8);
uint16 __binary_log(uint16);




/*-----------------------------------------------------------------------------
* ID: __expand_vpred
*----------------------------------------------------------------------------*/

/* BITXPND */
__vpred __expand_vpred(uint64_t a, uint8_t b);
__vpred __expand_vpred(__vpred a, uint8_t b);




/*-----------------------------------------------------------------------------
* ID: __pack_vpred
*----------------------------------------------------------------------------*/

/* BITPACK */
__vpred __pack_vpred(uint64_t a, uint8_t b);
__vpred __pack_vpred(__vpred a, uint8_t b);




/*-----------------------------------------------------------------------------
* ID: __bit_reverse
*----------------------------------------------------------------------------*/

/* BITR */
__vpred __bit_reverse(__vpred);

/* VBITRD */
int64_t __bit_reverse(int64_t);
long2 __bit_reverse(long2);
/* UNSUPPORTED: long3 __bit_reverse(long3); */
long4 __bit_reverse(long4);
long8 __bit_reverse(long8);
ulong __bit_reverse(ulong);
ulong2 __bit_reverse(ulong2);
/* UNSUPPORTED: ulong3 __bit_reverse(ulong3); */
ulong4 __bit_reverse(ulong4);
ulong8 __bit_reverse(ulong8);

/* VBITRW */
int32_t __bit_reverse(int32_t);
int2 __bit_reverse(int2);
/* UNSUPPORTED: int3 __bit_reverse(int3); */
int4 __bit_reverse(int4);
int8 __bit_reverse(int8);
int16 __bit_reverse(int16);
uint __bit_reverse(uint);
uint2 __bit_reverse(uint2);
/* UNSUPPORTED: uint3 __bit_reverse(uint3); */
uint4 __bit_reverse(uint4);
uint8 __bit_reverse(uint8);
uint16 __bit_reverse(uint16);




/*-----------------------------------------------------------------------------
* ID: __bit_transpose
*----------------------------------------------------------------------------*/

/* VBITTRAN8B */
uchar8 __bit_transpose(uchar8);
uchar16 __bit_transpose(uchar16);
uchar32 __bit_transpose(uchar32);
uchar64 __bit_transpose(uchar64);




/*-----------------------------------------------------------------------------
* ID: __bitpack_dkp
*----------------------------------------------------------------------------*/

/* BITPACK */
__vpred __bitpack_dkp(ulong, uchar);




/*-----------------------------------------------------------------------------
* ID: __bitpack_pkp
*----------------------------------------------------------------------------*/

/* BITPACK */
__vpred __bitpack_pkp(__vpred, uchar);




/*-----------------------------------------------------------------------------
* ID: __bitr_pp
*----------------------------------------------------------------------------*/

/* BITR */
__vpred __bitr_pp(__vpred);




/*-----------------------------------------------------------------------------
* ID: __bitxpnd_dkp
*----------------------------------------------------------------------------*/

/* BITXPND */
__vpred __bitxpnd_dkp(ulong, uchar);




/*-----------------------------------------------------------------------------
* ID: __bitxpnd_pkp
*----------------------------------------------------------------------------*/

/* BITXPND */
__vpred __bitxpnd_pkp(__vpred, uchar);




/*-----------------------------------------------------------------------------
* ID: __c6dcmpeqb_ddr
*----------------------------------------------------------------------------*/

/* C6DCMPEQB */
uchar __c6dcmpeqb_ddr(char8, char8);




/*-----------------------------------------------------------------------------
* ID: __c6dcmpeqh_ddr
*----------------------------------------------------------------------------*/

/* C6DCMPEQH */
uchar __c6dcmpeqh_ddr(short4, short4);




/*-----------------------------------------------------------------------------
* ID: __c6dcmpgth_ddr
*----------------------------------------------------------------------------*/

/* C6DCMPGTH */
uchar __c6dcmpgth_ddr(short4, short4);




/*-----------------------------------------------------------------------------
* ID: __c6dcmpgtub_ddr
*----------------------------------------------------------------------------*/

/* C6DCMPGTUB */
uchar __c6dcmpgtub_ddr(uchar8, uchar8);




/*-----------------------------------------------------------------------------
* ID: __c6ddotp2hhw_drd
*----------------------------------------------------------------------------*/

/* C6DDOTP2HHW */
int2 __c6ddotp2hhw_drd(short4, short2);




/*-----------------------------------------------------------------------------
* ID: __c6ddotp2hrhh_drr
*----------------------------------------------------------------------------*/

/* C6DDOTP2HRHH */
short2 __c6ddotp2hrhh_drr(short4, short2);




/*-----------------------------------------------------------------------------
* ID: __c6ddotp2lhw_drd
*----------------------------------------------------------------------------*/

/* C6DDOTP2LHW */
int2 __c6ddotp2lhw_drd(short4, short2);




/*-----------------------------------------------------------------------------
* ID: __c6ddotp2lrhh_drr
*----------------------------------------------------------------------------*/

/* C6DDOTP2LRHH */
short2 __c6ddotp2lrhh_drr(short4, short2);




/*-----------------------------------------------------------------------------
* ID: __c6dmpyhw_vvw
*----------------------------------------------------------------------------*/

/* C6DMPYHW */
void __c6dmpyhw_vvw(short32, short32, int16::EQUIV_ACCESS_T<0>&, int16::EQUIV_ACCESS_T<0>&);




/*-----------------------------------------------------------------------------
* ID: __c6dmpysubh_vvw
*----------------------------------------------------------------------------*/

/* C6DMPYSUBH */
void __c6dmpysubh_vvw(char32, uchar32, short16::EQUIV_ACCESS_T<0>&, short16::EQUIV_ACCESS_T<0>&);




/*-----------------------------------------------------------------------------
* ID: __c6dmpyubh_vvw
*----------------------------------------------------------------------------*/

/* C6DMPYUBH */
void __c6dmpyubh_vvw(uchar32, uchar32, ushort16::EQUIV_ACCESS_T<0>&, ushort16::EQUIV_ACCESS_T<0>&);




/*-----------------------------------------------------------------------------
* ID: __c6dmpyuhw_vvw
*----------------------------------------------------------------------------*/

/* C6DMPYUHW */
void __c6dmpyuhw_vvw(ushort16, ushort16, uint8::EQUIV_ACCESS_T<0>&, uint8::EQUIV_ACCESS_T<0>&);




/*-----------------------------------------------------------------------------
* ID: __c6dotp2rsuhw_rrr
*----------------------------------------------------------------------------*/

/* C6DOTP2RSUHW */
int32_t __c6dotp2rsuhw_rrr(short2, ushort2);




/*-----------------------------------------------------------------------------
* ID: __c6dotpn2hw_rrr
*----------------------------------------------------------------------------*/

/* C6DOTPN2HW */
int32_t __c6dotpn2hw_rrr(short2, short2);




/*-----------------------------------------------------------------------------
* ID: __c6dotpn2rsuhh_rrr
*----------------------------------------------------------------------------*/

/* C6DOTPN2RSUHH */
int32_t __c6dotpn2rsuhh_rrr(short2, ushort2);




/*-----------------------------------------------------------------------------
* ID: __c6dsmpyhw_vvw
*----------------------------------------------------------------------------*/

/* C6DSMPYHW */
void __c6dsmpyhw_vvw(short32, short32, int16::EQUIV_ACCESS_T<0>&, int16::EQUIV_ACCESS_T<0>&);




/*-----------------------------------------------------------------------------
* ID: __c6dspacku4_ddd
*----------------------------------------------------------------------------*/

/* C6DSPACKU4 */
uchar8 __c6dspacku4_ddd(short4, short4);




/*-----------------------------------------------------------------------------
* ID: __c6mpy2ir_rrd
*----------------------------------------------------------------------------*/

/* C6MPY2IR */
int2 __c6mpy2ir_rrd(short2, int32_t);




/*-----------------------------------------------------------------------------
* ID: __c6mpyhir_rrr
*----------------------------------------------------------------------------*/

/* C6MPYHIR */
int32_t __c6mpyhir_rrr(short2, int32_t);




/*-----------------------------------------------------------------------------
* ID: __c6mpylir_rrr
*----------------------------------------------------------------------------*/

/* C6MPYLIR */
int32_t __c6mpylir_rrr(short2, int32_t);




/*-----------------------------------------------------------------------------
* ID: __classify
*----------------------------------------------------------------------------*/

/* VCLASSDP */
int64_t __classify(double);
long2 __classify(double2);
/* UNSUPPORTED: long3 __classify(double3); */
long4 __classify(double4);
long8 __classify(double8);

/* VCLASSSP */
int32_t __classify(float);
int2 __classify(float2);
/* UNSUPPORTED: int3 __classify(float3); */
int4 __classify(float4);
int8 __classify(float8);
int16 __classify(float16);




/*-----------------------------------------------------------------------------
* ID: __clear
*----------------------------------------------------------------------------*/

/* CLR */
uint __clear(uint, uint);




/*-----------------------------------------------------------------------------
* ID: __clr_rrr
*----------------------------------------------------------------------------*/

/* CLR */
uint __clr_rrr(uint, uint);




/*-----------------------------------------------------------------------------
* ID: __cmatmpy_ext
*----------------------------------------------------------------------------*/

/* VCMATMPYHW */
cint2 __cmatmpy_ext(cshort2, cshort4);
cint4 __cmatmpy_ext(cshort4, cshort8);
cint8 __cmatmpy_ext(cshort8, cshort16);




/*-----------------------------------------------------------------------------
* ID: __cmatmpy_fx
*----------------------------------------------------------------------------*/

/* VCMATMPYR1HH */
cshort2 __cmatmpy_fx(cshort2, cshort4);
cshort4 __cmatmpy_fx(cshort4, cshort8);
cshort8 __cmatmpy_fx(cshort8, cshort16);




/*-----------------------------------------------------------------------------
* ID: __cmp_eq
*----------------------------------------------------------------------------*/

/* CMPEQD */
int32_t __cmp_eq(int64_t, int64_t);
int32_t __cmp_eq(int64_t, int32_t);

/* CMPEQDP */
int32_t __cmp_eq(double, double);

/* CMPEQSP */
int32_t __cmp_eq(float, float);

/* CMPEQW */
int32_t __cmp_eq(int32_t, int32_t);
/* CONSTANT: int32_t __cmp_eq(int32_t, (int32_t)(k)); */




/*-----------------------------------------------------------------------------
* ID: __cmp_eq_pred
*----------------------------------------------------------------------------*/

/* VCMPEQB */
__vpred __cmp_eq_pred(int8_t, int8_t);
__vpred __cmp_eq_pred(char2, char2);
/* UNSUPPORTED: __vpred __cmp_eq_pred(char3, char3); */
__vpred __cmp_eq_pred(char4, char4);
__vpred __cmp_eq_pred(char8, char8);
__vpred __cmp_eq_pred(char16, char16);
__vpred __cmp_eq_pred(char32, char32);
__vpred __cmp_eq_pred(char64, char64);
__vpred __cmp_eq_pred(uchar, uchar);
__vpred __cmp_eq_pred(uchar2, uchar2);
/* UNSUPPORTED: __vpred __cmp_eq_pred(uchar3, uchar3); */
__vpred __cmp_eq_pred(uchar4, uchar4);
__vpred __cmp_eq_pred(uchar8, uchar8);
__vpred __cmp_eq_pred(uchar16, uchar16);
__vpred __cmp_eq_pred(uchar32, uchar32);
__vpred __cmp_eq_pred(uchar64, uchar64);

/* VCMPEQD */
__vpred __cmp_eq_pred(int64_t, int64_t);
__vpred __cmp_eq_pred(long2, long2);
/* UNSUPPORTED: __vpred __cmp_eq_pred(long3, long3); */
__vpred __cmp_eq_pred(long4, long4);
__vpred __cmp_eq_pred(long8, long8);
__vpred __cmp_eq_pred(ulong, ulong);
__vpred __cmp_eq_pred(ulong2, ulong2);
/* UNSUPPORTED: __vpred __cmp_eq_pred(ulong3, ulong3); */
__vpred __cmp_eq_pred(ulong4, ulong4);
__vpred __cmp_eq_pred(ulong8, ulong8);

/* VCMPEQDP */
__vpred __cmp_eq_pred(double, double);
__vpred __cmp_eq_pred(double2, double2);
/* UNSUPPORTED: __vpred __cmp_eq_pred(double3, double3); */
__vpred __cmp_eq_pred(double4, double4);
__vpred __cmp_eq_pred(double8, double8);

/* VCMPEQH */
__vpred __cmp_eq_pred(int16_t, int16_t);
__vpred __cmp_eq_pred(short2, short2);
/* UNSUPPORTED: __vpred __cmp_eq_pred(short3, short3); */
__vpred __cmp_eq_pred(short4, short4);
__vpred __cmp_eq_pred(short8, short8);
__vpred __cmp_eq_pred(short16, short16);
__vpred __cmp_eq_pred(short32, short32);
__vpred __cmp_eq_pred(ushort, ushort);
__vpred __cmp_eq_pred(ushort2, ushort2);
/* UNSUPPORTED: __vpred __cmp_eq_pred(ushort3, ushort3); */
__vpred __cmp_eq_pred(ushort4, ushort4);
__vpred __cmp_eq_pred(ushort8, ushort8);
__vpred __cmp_eq_pred(ushort16, ushort16);
__vpred __cmp_eq_pred(ushort32, ushort32);

/* VCMPEQSP */
__vpred __cmp_eq_pred(float, float);
__vpred __cmp_eq_pred(float2, float2);
/* UNSUPPORTED: __vpred __cmp_eq_pred(float3, float3); */
__vpred __cmp_eq_pred(float4, float4);
__vpred __cmp_eq_pred(float8, float8);
__vpred __cmp_eq_pred(float16, float16);

/* VCMPEQW */
__vpred __cmp_eq_pred(int32_t, int32_t);
__vpred __cmp_eq_pred(int2, int2);
/* UNSUPPORTED: __vpred __cmp_eq_pred(int3, int3); */
__vpred __cmp_eq_pred(int4, int4);
__vpred __cmp_eq_pred(int8, int8);
__vpred __cmp_eq_pred(int16, int16);
__vpred __cmp_eq_pred(uint, uint);
__vpred __cmp_eq_pred(uint2, uint2);
/* UNSUPPORTED: __vpred __cmp_eq_pred(uint3, uint3); */
__vpred __cmp_eq_pred(uint4, uint4);
__vpred __cmp_eq_pred(uint8, uint8);
__vpred __cmp_eq_pred(uint16, uint16);




/*-----------------------------------------------------------------------------
* ID: __cmp_ge
*----------------------------------------------------------------------------*/

/* CMPGED */
int32_t __cmp_ge(int64_t, int64_t);
int32_t __cmp_ge(int64_t, int32_t);

/* CMPGEUD */
int32_t __cmp_ge(ulong, ulong);
int32_t __cmp_ge(ulong, uint);

/* CMPGEUW */
int32_t __cmp_ge(uint, uint);
/* CONSTANT: int32_t __cmp_ge(uint, (uint)(k)); */

/* CMPGEW */
int32_t __cmp_ge(int32_t, int32_t);
/* CONSTANT: int32_t __cmp_ge(int32_t, (int32_t)(k)); */




/*-----------------------------------------------------------------------------
* ID: __cmp_ge_pred
*----------------------------------------------------------------------------*/

/* VCMPGEB */
__vpred __cmp_ge_pred(int8_t, int8_t);
__vpred __cmp_ge_pred(char2, char2);
/* UNSUPPORTED: __vpred __cmp_ge_pred(char3, char3); */
__vpred __cmp_ge_pred(char4, char4);
__vpred __cmp_ge_pred(char8, char8);
__vpred __cmp_ge_pred(char16, char16);
__vpred __cmp_ge_pred(char32, char32);
__vpred __cmp_ge_pred(char64, char64);

/* VCMPGED */
__vpred __cmp_ge_pred(int64_t, int64_t);
__vpred __cmp_ge_pred(long2, long2);
/* UNSUPPORTED: __vpred __cmp_ge_pred(long3, long3); */
__vpred __cmp_ge_pred(long4, long4);
__vpred __cmp_ge_pred(long8, long8);

/* VCMPGEH */
__vpred __cmp_ge_pred(int16_t, int16_t);
__vpred __cmp_ge_pred(short2, short2);
/* UNSUPPORTED: __vpred __cmp_ge_pred(short3, short3); */
__vpred __cmp_ge_pred(short4, short4);
__vpred __cmp_ge_pred(short8, short8);
__vpred __cmp_ge_pred(short16, short16);
__vpred __cmp_ge_pred(short32, short32);

/* VCMPGEUB */
__vpred __cmp_ge_pred(uchar, uchar);
__vpred __cmp_ge_pred(uchar2, uchar2);
/* UNSUPPORTED: __vpred __cmp_ge_pred(uchar3, uchar3); */
__vpred __cmp_ge_pred(uchar4, uchar4);
__vpred __cmp_ge_pred(uchar8, uchar8);
__vpred __cmp_ge_pred(uchar16, uchar16);
__vpred __cmp_ge_pred(uchar32, uchar32);
__vpred __cmp_ge_pred(uchar64, uchar64);

/* VCMPGEUD */
__vpred __cmp_ge_pred(ulong, ulong);
__vpred __cmp_ge_pred(ulong2, ulong2);
/* UNSUPPORTED: __vpred __cmp_ge_pred(ulong3, ulong3); */
__vpred __cmp_ge_pred(ulong4, ulong4);
__vpred __cmp_ge_pred(ulong8, ulong8);

/* VCMPGEUH */
__vpred __cmp_ge_pred(ushort, ushort);
__vpred __cmp_ge_pred(ushort2, ushort2);
/* UNSUPPORTED: __vpred __cmp_ge_pred(ushort3, ushort3); */
__vpred __cmp_ge_pred(ushort4, ushort4);
__vpred __cmp_ge_pred(ushort8, ushort8);
__vpred __cmp_ge_pred(ushort16, ushort16);
__vpred __cmp_ge_pred(ushort32, ushort32);

/* VCMPGEUW */
__vpred __cmp_ge_pred(uint, uint);
__vpred __cmp_ge_pred(uint2, uint2);
/* UNSUPPORTED: __vpred __cmp_ge_pred(uint3, uint3); */
__vpred __cmp_ge_pred(uint4, uint4);
__vpred __cmp_ge_pred(uint8, uint8);
__vpred __cmp_ge_pred(uint16, uint16);

/* VCMPGEW */
__vpred __cmp_ge_pred(int32_t, int32_t);
__vpred __cmp_ge_pred(int2, int2);
/* UNSUPPORTED: __vpred __cmp_ge_pred(int3, int3); */
__vpred __cmp_ge_pred(int4, int4);
__vpred __cmp_ge_pred(int8, int8);
__vpred __cmp_ge_pred(int16, int16);




/*-----------------------------------------------------------------------------
* ID: __cmp_gt
*----------------------------------------------------------------------------*/

/* CMPGTD */
int32_t __cmp_gt(int64_t, int64_t);
int32_t __cmp_gt(int64_t, int32_t);

/* CMPGTUD */
int32_t __cmp_gt(ulong, ulong);
int32_t __cmp_gt(ulong, uint);

/* CMPGTUW */
int32_t __cmp_gt(uint, uint);
/* CONSTANT: int32_t __cmp_gt(uint, (uint)(k)); */

/* CMPGTW */
int32_t __cmp_gt(int32_t, int32_t);
/* CONSTANT: int32_t __cmp_gt(int32_t, (int32_t)(k)); */




/*-----------------------------------------------------------------------------
* ID: __cmp_gt_pred
*----------------------------------------------------------------------------*/

/* VCMPGTB */
__vpred __cmp_gt_pred(int8_t, int8_t);
__vpred __cmp_gt_pred(char2, char2);
/* UNSUPPORTED: __vpred __cmp_gt_pred(char3, char3); */
__vpred __cmp_gt_pred(char4, char4);
__vpred __cmp_gt_pred(char8, char8);
__vpred __cmp_gt_pred(char16, char16);
__vpred __cmp_gt_pred(char32, char32);
__vpred __cmp_gt_pred(char64, char64);

/* VCMPGTD */
__vpred __cmp_gt_pred(int64_t, int64_t);
__vpred __cmp_gt_pred(long2, long2);
/* UNSUPPORTED: __vpred __cmp_gt_pred(long3, long3); */
__vpred __cmp_gt_pred(long4, long4);
__vpred __cmp_gt_pred(long8, long8);

/* VCMPGTH */
__vpred __cmp_gt_pred(int16_t, int16_t);
__vpred __cmp_gt_pred(short2, short2);
/* UNSUPPORTED: __vpred __cmp_gt_pred(short3, short3); */
__vpred __cmp_gt_pred(short4, short4);
__vpred __cmp_gt_pred(short8, short8);
__vpred __cmp_gt_pred(short16, short16);
__vpred __cmp_gt_pred(short32, short32);

/* VCMPGTUB */
__vpred __cmp_gt_pred(uchar, uchar);
__vpred __cmp_gt_pred(uchar2, uchar2);
/* UNSUPPORTED: __vpred __cmp_gt_pred(uchar3, uchar3); */
__vpred __cmp_gt_pred(uchar4, uchar4);
__vpred __cmp_gt_pred(uchar8, uchar8);
__vpred __cmp_gt_pred(uchar16, uchar16);
__vpred __cmp_gt_pred(uchar32, uchar32);
__vpred __cmp_gt_pred(uchar64, uchar64);

/* VCMPGTUD */
__vpred __cmp_gt_pred(ulong, ulong);
__vpred __cmp_gt_pred(ulong2, ulong2);
/* UNSUPPORTED: __vpred __cmp_gt_pred(ulong3, ulong3); */
__vpred __cmp_gt_pred(ulong4, ulong4);
__vpred __cmp_gt_pred(ulong8, ulong8);

/* VCMPGTUH */
__vpred __cmp_gt_pred(ushort, ushort);
__vpred __cmp_gt_pred(ushort2, ushort2);
/* UNSUPPORTED: __vpred __cmp_gt_pred(ushort3, ushort3); */
__vpred __cmp_gt_pred(ushort4, ushort4);
__vpred __cmp_gt_pred(ushort8, ushort8);
__vpred __cmp_gt_pred(ushort16, ushort16);
__vpred __cmp_gt_pred(ushort32, ushort32);

/* VCMPGTUW */
__vpred __cmp_gt_pred(uint, uint);
__vpred __cmp_gt_pred(uint2, uint2);
/* UNSUPPORTED: __vpred __cmp_gt_pred(uint3, uint3); */
__vpred __cmp_gt_pred(uint4, uint4);
__vpred __cmp_gt_pred(uint8, uint8);
__vpred __cmp_gt_pred(uint16, uint16);

/* VCMPGTW */
__vpred __cmp_gt_pred(int32_t, int32_t);
__vpred __cmp_gt_pred(int2, int2);
/* UNSUPPORTED: __vpred __cmp_gt_pred(int3, int3); */
__vpred __cmp_gt_pred(int4, int4);
__vpred __cmp_gt_pred(int8, int8);
__vpred __cmp_gt_pred(int16, int16);




/*-----------------------------------------------------------------------------
* ID: __cmp_le
*----------------------------------------------------------------------------*/

/* CMPLEDP */
int32_t __cmp_le(double, double);

/* CMPLESP */
int32_t __cmp_le(float, float);




/*-----------------------------------------------------------------------------
* ID: __cmp_le_pred
*----------------------------------------------------------------------------*/

/* VCMPLEDP */
__vpred __cmp_le_pred(double, double);
__vpred __cmp_le_pred(double2, double2);
/* UNSUPPORTED: __vpred __cmp_le_pred(double3, double3); */
__vpred __cmp_le_pred(double4, double4);
__vpred __cmp_le_pred(double8, double8);

/* VCMPLESP */
__vpred __cmp_le_pred(float, float);
__vpred __cmp_le_pred(float2, float2);
/* UNSUPPORTED: __vpred __cmp_le_pred(float3, float3); */
__vpred __cmp_le_pred(float4, float4);
__vpred __cmp_le_pred(float8, float8);
__vpred __cmp_le_pred(float16, float16);




/*-----------------------------------------------------------------------------
* ID: __cmp_lt
*----------------------------------------------------------------------------*/

/* CMPLTDP */
int32_t __cmp_lt(double, double);

/* CMPLTSP */
int32_t __cmp_lt(float, float);




/*-----------------------------------------------------------------------------
* ID: __cmp_lt_pred
*----------------------------------------------------------------------------*/

/* VCMPLTDP */
__vpred __cmp_lt_pred(double, double);
__vpred __cmp_lt_pred(double2, double2);
/* UNSUPPORTED: __vpred __cmp_lt_pred(double3, double3); */
__vpred __cmp_lt_pred(double4, double4);
__vpred __cmp_lt_pred(double8, double8);

/* VCMPLTSP */
__vpred __cmp_lt_pred(float, float);
__vpred __cmp_lt_pred(float2, float2);
/* UNSUPPORTED: __vpred __cmp_lt_pred(float3, float3); */
__vpred __cmp_lt_pred(float4, float4);
__vpred __cmp_lt_pred(float8, float8);
__vpred __cmp_lt_pred(float16, float16);




/*-----------------------------------------------------------------------------
* ID: __cmpeqd_ddr
*----------------------------------------------------------------------------*/

/* CMPEQD */
int32_t __cmpeqd_ddr(int64_t, int64_t);




/*-----------------------------------------------------------------------------
* ID: __cmpeqd_dkr
*----------------------------------------------------------------------------*/

/* CMPEQD */
int32_t __cmpeqd_dkr(int64_t, int32_t);




/*-----------------------------------------------------------------------------
* ID: __cmpeqdp_ddr
*----------------------------------------------------------------------------*/

/* CMPEQDP */
int32_t __cmpeqdp_ddr(double, double);




/*-----------------------------------------------------------------------------
* ID: __cmpeqsp_rrr
*----------------------------------------------------------------------------*/

/* CMPEQSP */
int32_t __cmpeqsp_rrr(float, float);




/*-----------------------------------------------------------------------------
* ID: __cmpeqw_rkr
*----------------------------------------------------------------------------*/

/* CMPEQW */
int32_t __cmpeqw_rkr(int32_t, int32_t);




/*-----------------------------------------------------------------------------
* ID: __cmpeqw_rrr
*----------------------------------------------------------------------------*/

/* CMPEQW */
int32_t __cmpeqw_rrr(int32_t, int32_t);




/*-----------------------------------------------------------------------------
* ID: __cmpged_ddr
*----------------------------------------------------------------------------*/

/* CMPGED */
int32_t __cmpged_ddr(int64_t, int64_t);




/*-----------------------------------------------------------------------------
* ID: __cmpged_dkr
*----------------------------------------------------------------------------*/

/* CMPGED */
int32_t __cmpged_dkr(int64_t, int32_t);




/*-----------------------------------------------------------------------------
* ID: __cmpgeud_ddr
*----------------------------------------------------------------------------*/

/* CMPGEUD */
int32_t __cmpgeud_ddr(ulong, ulong);




/*-----------------------------------------------------------------------------
* ID: __cmpgeud_dkr
*----------------------------------------------------------------------------*/

/* CMPGEUD */
int32_t __cmpgeud_dkr(ulong, uint);




/*-----------------------------------------------------------------------------
* ID: __cmpgeuw_rkr
*----------------------------------------------------------------------------*/

/* CMPGEUW */
int32_t __cmpgeuw_rkr(uint, uint);




/*-----------------------------------------------------------------------------
* ID: __cmpgeuw_rrr
*----------------------------------------------------------------------------*/

/* CMPGEUW */
int32_t __cmpgeuw_rrr(uint, uint);




/*-----------------------------------------------------------------------------
* ID: __cmpgew_rkr
*----------------------------------------------------------------------------*/

/* CMPGEW */
int32_t __cmpgew_rkr(int32_t, int32_t);




/*-----------------------------------------------------------------------------
* ID: __cmpgew_rrr
*----------------------------------------------------------------------------*/

/* CMPGEW */
int32_t __cmpgew_rrr(int32_t, int32_t);




/*-----------------------------------------------------------------------------
* ID: __cmpgtd_ddr
*----------------------------------------------------------------------------*/

/* CMPGTD */
int32_t __cmpgtd_ddr(int64_t, int64_t);




/*-----------------------------------------------------------------------------
* ID: __cmpgtd_dkr
*----------------------------------------------------------------------------*/

/* CMPGTD */
int32_t __cmpgtd_dkr(int64_t, int32_t);




/*-----------------------------------------------------------------------------
* ID: __cmpgtud_ddr
*----------------------------------------------------------------------------*/

/* CMPGTUD */
int32_t __cmpgtud_ddr(ulong, ulong);




/*-----------------------------------------------------------------------------
* ID: __cmpgtud_dkr
*----------------------------------------------------------------------------*/

/* CMPGTUD */
int32_t __cmpgtud_dkr(ulong, uint);




/*-----------------------------------------------------------------------------
* ID: __cmpgtuw_rkr
*----------------------------------------------------------------------------*/

/* CMPGTUW */
int32_t __cmpgtuw_rkr(uint, uint);




/*-----------------------------------------------------------------------------
* ID: __cmpgtuw_rrr
*----------------------------------------------------------------------------*/

/* CMPGTUW */
int32_t __cmpgtuw_rrr(uint, uint);




/*-----------------------------------------------------------------------------
* ID: __cmpgtw_rkr
*----------------------------------------------------------------------------*/

/* CMPGTW */
int32_t __cmpgtw_rkr(int32_t, int32_t);




/*-----------------------------------------------------------------------------
* ID: __cmpgtw_rrr
*----------------------------------------------------------------------------*/

/* CMPGTW */
int32_t __cmpgtw_rrr(int32_t, int32_t);




/*-----------------------------------------------------------------------------
* ID: __cmpledp_ddr
*----------------------------------------------------------------------------*/

/* CMPLEDP */
int32_t __cmpledp_ddr(double, double);




/*-----------------------------------------------------------------------------
* ID: __cmplesp_rrr
*----------------------------------------------------------------------------*/

/* CMPLESP */
int32_t __cmplesp_rrr(float, float);




/*-----------------------------------------------------------------------------
* ID: __cmpltdp_ddr
*----------------------------------------------------------------------------*/

/* CMPLTDP */
int32_t __cmpltdp_ddr(double, double);




/*-----------------------------------------------------------------------------
* ID: __cmpltsp_rrr
*----------------------------------------------------------------------------*/

/* CMPLTSP */
int32_t __cmpltsp_rrr(float, float);




/*-----------------------------------------------------------------------------
* ID: __cmpy_conj_ext
*----------------------------------------------------------------------------*/

/* VCCMPYHW */
cint __cmpy_conj_ext(cshort, cshort);
cint2 __cmpy_conj_ext(cshort2, cshort2);
cint4 __cmpy_conj_ext(cshort4, cshort4);
cint8 __cmpy_conj_ext(cshort8, cshort8);




/*-----------------------------------------------------------------------------
* ID: __cmpy_conj_fx
*----------------------------------------------------------------------------*/

/* VCCMPYR1HH */
cshort __cmpy_conj_fx(cshort, cshort);
cshort2 __cmpy_conj_fx(cshort2, cshort2);
cshort4 __cmpy_conj_fx(cshort4, cshort4);
cshort8 __cmpy_conj_fx(cshort8, cshort8);
cshort16 __cmpy_conj_fx(cshort16, cshort16);

/* VCCMPYR1WW */
cint __cmpy_conj_fx(cint, cint);
cint2 __cmpy_conj_fx(cint2, cint2);
cint4 __cmpy_conj_fx(cint4, cint4);
cint8 __cmpy_conj_fx(cint8, cint8);




/*-----------------------------------------------------------------------------
* ID: __cmpy_ext
*----------------------------------------------------------------------------*/

/* VCMPYHW */
cint __cmpy_ext(cshort, cshort);
cint2 __cmpy_ext(cshort2, cshort2);
cint4 __cmpy_ext(cshort4, cshort4);
cint8 __cmpy_ext(cshort8, cshort8);

/* VCMPYSP */
float4 __cmpy_ext(cfloat, cfloat);
float8 __cmpy_ext(cfloat2, cfloat2);
float16 __cmpy_ext(cfloat4, cfloat4);




/*-----------------------------------------------------------------------------
* ID: __cmpy_fx
*----------------------------------------------------------------------------*/

/* VCMPYR1HH */
cshort __cmpy_fx(cshort, cshort);
cshort2 __cmpy_fx(cshort2, cshort2);
cshort4 __cmpy_fx(cshort4, cshort4);
cshort8 __cmpy_fx(cshort8, cshort8);
cshort16 __cmpy_fx(cshort16, cshort16);

/* VCMPYR1WW */
cint __cmpy_fx(cint, cint);
cint2 __cmpy_fx(cint2, cint2);
cint4 __cmpy_fx(cint4, cint4);
cint8 __cmpy_fx(cint8, cint8);




/*-----------------------------------------------------------------------------
* ID: __cmpyr_fx
*----------------------------------------------------------------------------*/

/* VCMPYRHH */
cshort __cmpyr_fx(cshort, cshort);
cshort2 __cmpyr_fx(cshort2, cshort2);
cshort4 __cmpyr_fx(cshort4, cshort4);
cshort8 __cmpyr_fx(cshort8, cshort8);
cshort16 __cmpyr_fx(cshort16, cshort16);




/*-----------------------------------------------------------------------------
* ID: __compress_set_bits_left
*----------------------------------------------------------------------------*/

/* COMPRESSL */
__vpred __compress_set_bits_left(__vpred);




/*-----------------------------------------------------------------------------
* ID: __compress_set_bits_right
*----------------------------------------------------------------------------*/

/* COMPRESSR */
__vpred __compress_set_bits_right(__vpred);




/*-----------------------------------------------------------------------------
* ID: __compressl_pp
*----------------------------------------------------------------------------*/

/* COMPRESSL */
__vpred __compressl_pp(__vpred);




/*-----------------------------------------------------------------------------
* ID: __compressr_pp
*----------------------------------------------------------------------------*/

/* COMPRESSR */
__vpred __compressr_pp(__vpred);




/*-----------------------------------------------------------------------------
* ID: __conj_cmatmpy_ext
*----------------------------------------------------------------------------*/

/* VCCMATMPYHW */
cint2 __conj_cmatmpy_ext(cshort2, cshort4);
cint4 __conj_cmatmpy_ext(cshort4, cshort8);
cint8 __conj_cmatmpy_ext(cshort8, cshort16);




/*-----------------------------------------------------------------------------
* ID: __conj_cmatmpy_fx
*----------------------------------------------------------------------------*/

/* VCCMATMPYR1HH */
cshort2 __conj_cmatmpy_fx(cshort2, cshort4);
cshort4 __conj_cmatmpy_fx(cshort4, cshort8);
cshort8 __conj_cmatmpy_fx(cshort8, cshort16);




/*-----------------------------------------------------------------------------
* ID: __crot270
*----------------------------------------------------------------------------*/

/* VCROT270H */
cshort __crot270(cshort);
cshort2 __crot270(cshort2);
cshort4 __crot270(cshort4);
cshort8 __crot270(cshort8);
cshort16 __crot270(cshort16);

/* VCROT270SP */
cfloat __crot270(cfloat);
cfloat2 __crot270(cfloat2);
cfloat4 __crot270(cfloat4);
cfloat8 __crot270(cfloat8);

/* VCROT270W */
cint __crot270(cint);
cint2 __crot270(cint2);
cint4 __crot270(cint4);
cint8 __crot270(cint8);




/*-----------------------------------------------------------------------------
* ID: __crot90
*----------------------------------------------------------------------------*/

/* VCROT90H */
cshort __crot90(cshort);
cshort2 __crot90(cshort2);
cshort4 __crot90(cshort4);
cshort8 __crot90(cshort8);
cshort16 __crot90(cshort16);

/* VCROT90SP */
cfloat __crot90(cfloat);
cfloat2 __crot90(cfloat2);
cfloat4 __crot90(cfloat4);
cfloat8 __crot90(cfloat8);

/* VCROT90W */
cint __crot90(cint);
cint2 __crot90(cint2);
cint4 __crot90(cint4);
cint8 __crot90(cint8);




/*-----------------------------------------------------------------------------
* ID: __deal_bit
*----------------------------------------------------------------------------*/

/* VBITDEALD */
int64_t __deal_bit(int64_t);
long2 __deal_bit(long2);
/* UNSUPPORTED: long3 __deal_bit(long3); */
long4 __deal_bit(long4);
long8 __deal_bit(long8);
ulong __deal_bit(ulong);
ulong2 __deal_bit(ulong2);
/* UNSUPPORTED: ulong3 __deal_bit(ulong3); */
ulong4 __deal_bit(ulong4);
ulong8 __deal_bit(ulong8);

/* VBITDEALW */
int32_t __deal_bit(int32_t);
int2 __deal_bit(int2);
/* UNSUPPORTED: int3 __deal_bit(int3); */
int4 __deal_bit(int4);
int8 __deal_bit(int8);
int16 __deal_bit(int16);
uint __deal_bit(uint);
uint2 __deal_bit(uint2);
/* UNSUPPORTED: uint3 __deal_bit(uint3); */
uint4 __deal_bit(uint4);
uint8 __deal_bit(uint8);
uint16 __deal_bit(uint16);




/*-----------------------------------------------------------------------------
* ID: __deal_stride2
*----------------------------------------------------------------------------*/

/* VDEAL2B */
char64 __deal_stride2(char64);
uchar64 __deal_stride2(uchar64);

/* VDEAL2H */
short32 __deal_stride2(short32);
ushort32 __deal_stride2(ushort32);
cchar32 __deal_stride2(cchar32);

/* VDEAL2W */
int16 __deal_stride2(int16);
float16 __deal_stride2(float16);
uint16 __deal_stride2(uint16);
cshort16 __deal_stride2(cshort16);




/*-----------------------------------------------------------------------------
* ID: __deal_stride4
*----------------------------------------------------------------------------*/

/* VDEAL4B */
char64 __deal_stride4(char64);
uchar64 __deal_stride4(uchar64);

/* VDEAL4H */
short32 __deal_stride4(short32);
ushort32 __deal_stride4(ushort32);
cchar32 __deal_stride4(cchar32);




/*-----------------------------------------------------------------------------
* ID: __decimate_char
*----------------------------------------------------------------------------*/

/* DECIMATEB */
__vpred __decimate_char(__vpred, __vpred);




/*-----------------------------------------------------------------------------
* ID: __decimate_int
*----------------------------------------------------------------------------*/

/* DECIMATEW */
__vpred __decimate_int(__vpred, __vpred);




/*-----------------------------------------------------------------------------
* ID: __decimate_long
*----------------------------------------------------------------------------*/

/* DECIMATED */
__vpred __decimate_long(__vpred, __vpred);




/*-----------------------------------------------------------------------------
* ID: __decimate_short
*----------------------------------------------------------------------------*/

/* DECIMATEH */
__vpred __decimate_short(__vpred, __vpred);




/*-----------------------------------------------------------------------------
* ID: __decimateb_ppp
*----------------------------------------------------------------------------*/

/* DECIMATEB */
__vpred __decimateb_ppp(__vpred, __vpred);




/*-----------------------------------------------------------------------------
* ID: __decimated_ppp
*----------------------------------------------------------------------------*/

/* DECIMATED */
__vpred __decimated_ppp(__vpred, __vpred);




/*-----------------------------------------------------------------------------
* ID: __decimateh_ppp
*----------------------------------------------------------------------------*/

/* DECIMATEH */
__vpred __decimateh_ppp(__vpred, __vpred);




/*-----------------------------------------------------------------------------
* ID: __decimatew_ppp
*----------------------------------------------------------------------------*/

/* DECIMATEW */
__vpred __decimatew_ppp(__vpred, __vpred);




/*-----------------------------------------------------------------------------
* ID: __div_operator
*----------------------------------------------------------------------------*/
/*

DIVDW
long = long / int;

DIVUDW
ulong = ulong / uint;

DIVUW
uint = uint / uint;

DIVW
int = int / int;

*/


/*-----------------------------------------------------------------------------
* ID: __divdw_drd
*----------------------------------------------------------------------------*/

/* DIVDW */
int64_t __divdw_drd(int64_t, int32_t);




/*-----------------------------------------------------------------------------
* ID: __divudw_drd
*----------------------------------------------------------------------------*/

/* DIVUDW */
ulong __divudw_drd(ulong, uint);




/*-----------------------------------------------------------------------------
* ID: __divuw_rrr
*----------------------------------------------------------------------------*/

/* DIVUW */
uint __divuw_rrr(uint, uint);




/*-----------------------------------------------------------------------------
* ID: __divw_rrr
*----------------------------------------------------------------------------*/

/* DIVW */
int32_t __divw_rrr(int32_t, int32_t);




/*-----------------------------------------------------------------------------
* ID: __dot_posneg128_mask
*----------------------------------------------------------------------------*/

/* VDOTPMPN16W8D */
long8 __dot_posneg128_mask(ushort32, ushort8, int16);




/*-----------------------------------------------------------------------------
* ID: __dot_posneg128_mask_ext
*----------------------------------------------------------------------------*/

/* VDOTPMPNU16W8D */
ulong8 __dot_posneg128_mask_ext(ushort32, ushort8, uint16);




/*-----------------------------------------------------------------------------
* ID: __dot_posneg256_mask
*----------------------------------------------------------------------------*/

/* VDOTPMPN16H16W */
int16 __dot_posneg256_mask(ushort32, ushort16, short16);

/* VDOTPMPN32H8W */
int8 __dot_posneg256_mask(uint8, uint8, short32);

/* VDOTPMPNU16H16W */
uint16 __dot_posneg256_mask(ushort32, ushort16, ushort16);

/* VDOTPMPNU32H8W */
uint8 __dot_posneg256_mask(uint8, uint8, ushort32);




/*-----------------------------------------------------------------------------
* ID: __dot_posneg512_mask
*----------------------------------------------------------------------------*/

/* VDOTPMPN16B32H */
short32 __dot_posneg512_mask(ushort32, ushort32, char16);

/* VDOTPMPN32B16H */
short16 __dot_posneg512_mask(uint16, uint16, char32);

/* VDOTPMPNU32B16H */
ushort16 __dot_posneg512_mask(uint16, uint16, uchar32);




/*-----------------------------------------------------------------------------
* ID: __dot_posneg512_mask_ext
*----------------------------------------------------------------------------*/

/* VDOTPMPN32B16W */
int16 __dot_posneg512_mask_ext(uint16, uint16, char32);

/* VDOTPMPNU32B16W */
uint16 __dot_posneg512_mask_ext(uint16, uint16, uchar32);




/*-----------------------------------------------------------------------------
* ID: __dotp2
*----------------------------------------------------------------------------*/

/* VCDOTP2HW */
cint __dotp2(cshort2, cshort2);
cint2 __dotp2(cshort4, cshort4);
cint4 __dotp2(cshort8, cshort8);
cint8 __dotp2(cshort16, cshort16);

/* VDOTP2HW */
int32_t __dotp2(short2, short2);
int2 __dotp2(short4, short4);
int4 __dotp2(short8, short8);
int8 __dotp2(short16, short16);
int16 __dotp2(short32, short32);

/* VDOTP2SUBH */
int16_t __dotp2(char2, uchar2);
short2 __dotp2(char4, uchar4);
short4 __dotp2(char8, uchar8);
short8 __dotp2(char16, uchar16);
short16 __dotp2(char32, uchar32);
short32 __dotp2(char64, uchar64);

/* VDOTP2SUHW */
int32_t __dotp2(short2, ushort2);
int2 __dotp2(short4, ushort4);
int4 __dotp2(short8, ushort8);
int8 __dotp2(short16, ushort16);
int16 __dotp2(short32, ushort32);

/* VDOTP2UBH */
ushort __dotp2(uchar2, uchar2);
ushort2 __dotp2(uchar4, uchar4);
ushort4 __dotp2(uchar8, uchar8);
ushort8 __dotp2(uchar16, uchar16);
ushort16 __dotp2(uchar32, uchar32);
ushort32 __dotp2(uchar64, uchar64);

/* VDOTP2UHW */
uint __dotp2(ushort2, ushort2);
uint2 __dotp2(ushort4, ushort4);
uint4 __dotp2(ushort8, ushort8);
uint8 __dotp2(ushort16, ushort16);
uint16 __dotp2(ushort32, ushort32);

/* VDOTP2WD */
int64_t __dotp2(int2, int2);
long2 __dotp2(int4, int4);
long4 __dotp2(int8, int8);
long8 __dotp2(int16, int16);




/*-----------------------------------------------------------------------------
* ID: __dotp2_conj
*----------------------------------------------------------------------------*/

/* VCCDOTP2HW */
cint __dotp2_conj(cshort2, cshort2);
cint2 __dotp2_conj(cshort4, cshort4);
cint4 __dotp2_conj(cshort8, cshort8);
cint8 __dotp2_conj(cshort16, cshort16);




/*-----------------------------------------------------------------------------
* ID: __dotp2_cross
*----------------------------------------------------------------------------*/

/* VDOTP2XWD */
int64_t __dotp2_cross(int2, int2);
long2 __dotp2_cross(int4, int4);
long4 __dotp2_cross(int8, int8);
long8 __dotp2_cross(int16, int16);




/*-----------------------------------------------------------------------------
* ID: __dotp2_cross_negate
*----------------------------------------------------------------------------*/

/* VDOTP2NXWD */
int64_t __dotp2_cross_negate(int2, int2);
long2 __dotp2_cross_negate(int4, int4);
long4 __dotp2_cross_negate(int8, int8);
long8 __dotp2_cross_negate(int16, int16);




/*-----------------------------------------------------------------------------
* ID: __dotp2_ext
*----------------------------------------------------------------------------*/

/* VDOTP2HD */
int64_t __dotp2_ext(short2, short2);
long2 __dotp2_ext(short4, short4);
long4 __dotp2_ext(short8, short8);
long8 __dotp2_ext(short16, short16);

/* VDOTP2SUHD */
int64_t __dotp2_ext(short2, ushort2);
long2 __dotp2_ext(short4, ushort4);
long4 __dotp2_ext(short8, ushort8);
long8 __dotp2_ext(short16, ushort16);

/* VDOTP2UHD */
ulong __dotp2_ext(ushort2, ushort2);
ulong2 __dotp2_ext(ushort4, ushort4);
ulong4 __dotp2_ext(ushort8, ushort8);
ulong8 __dotp2_ext(ushort16, ushort16);




/*-----------------------------------------------------------------------------
* ID: __dotp2_fx_ext
*----------------------------------------------------------------------------*/

/* C6DOTP2RSUHW */
int32_t __dotp2_fx_ext(short2, ushort2);




/*-----------------------------------------------------------------------------
* ID: __dotp2_negate
*----------------------------------------------------------------------------*/

/* VDOTP2NWD */
int64_t __dotp2_negate(int2, int2);
long2 __dotp2_negate(int4, int4);
long4 __dotp2_negate(int8, int8);
long8 __dotp2_negate(int16, int16);

/* C6DOTPN2HW */
int32_t __dotp2_negate(short2, short2);

/* C6DOTPN2RSUHH */
int32_t __dotp2_negate(short2, ushort2);




/*-----------------------------------------------------------------------------
* ID: __dotp4
*----------------------------------------------------------------------------*/

/* VDOTP4HW */
int32_t __dotp4(short4, short4);
int2 __dotp4(short8, short8);
int4 __dotp4(short16, short16);
int8 __dotp4(short32, short32);

/* VDOTP4SUHW */
int32_t __dotp4(short4, ushort4);
int2 __dotp4(short8, ushort8);
int4 __dotp4(short16, ushort16);
int8 __dotp4(short32, ushort32);




/*-----------------------------------------------------------------------------
* ID: __dotp4_ext
*----------------------------------------------------------------------------*/

/* VDOTP4HD */
int64_t __dotp4_ext(short4, short4);
long2 __dotp4_ext(short8, short8);
long4 __dotp4_ext(short16, short16);
long8 __dotp4_ext(short32, short32);

/* VDOTP4SUBW */
int32_t __dotp4_ext(char4, uchar4);
int2 __dotp4_ext(char8, uchar8);
int4 __dotp4_ext(char16, uchar16);
int8 __dotp4_ext(char32, uchar32);
int16 __dotp4_ext(char64, uchar64);

/* VDOTP4SUHD */
int64_t __dotp4_ext(short4, ushort4);
long2 __dotp4_ext(short8, ushort8);
long4 __dotp4_ext(short16, ushort16);
long8 __dotp4_ext(short32, ushort32);

/* VDOTP4UBW */
uint __dotp4_ext(uchar4, uchar4);
uint2 __dotp4_ext(uchar8, uchar8);
uint4 __dotp4_ext(uchar16, uchar16);
uint8 __dotp4_ext(uchar32, uchar32);
uint16 __dotp4_ext(uchar64, uchar64);

/* VDOTP4UHD */
ulong __dotp4_ext(ushort4, ushort4);
ulong2 __dotp4_ext(ushort8, ushort8);
ulong4 __dotp4_ext(ushort16, ushort16);
ulong8 __dotp4_ext(ushort32, ushort32);




/*-----------------------------------------------------------------------------
* ID: __dotp8_ext
*----------------------------------------------------------------------------*/

/* VDOTP8SUBW */
int2 __dotp8_ext(char8, uchar8);
int4 __dotp8_ext(char16, uchar16);
int8 __dotp8_ext(char32, uchar32);
int16 __dotp8_ext(char64, uchar64);




/*-----------------------------------------------------------------------------
* ID: __double_to_float
*----------------------------------------------------------------------------*/

/* VDPSP */
float2 __double_to_float(double);
float4 __double_to_float(double2);
float8 __double_to_float(double4);
float16 __double_to_float(double8);




/*-----------------------------------------------------------------------------
* ID: __double_to_int
*----------------------------------------------------------------------------*/

/* VDPINT */
int2 __double_to_int(double);
int4 __double_to_int(double2);
int8 __double_to_int(double4);
int16 __double_to_int(double8);




/*-----------------------------------------------------------------------------
* ID: __double_to_int_rtz
*----------------------------------------------------------------------------*/

/* VDPTRUNC */
int2 __double_to_int_rtz(double);
int4 __double_to_int_rtz(double2);
int8 __double_to_int_rtz(double4);
int16 __double_to_int_rtz(double8);




/*-----------------------------------------------------------------------------
* ID: __dual_horizontal_add_skip1
*----------------------------------------------------------------------------*/

/* VHADDEO8W4D */
long4 __dual_horizontal_add_skip1(int16, int16);
clong2 __dual_horizontal_add_skip1(cint8, cint8);

/* VHADDUEO8W4D */
ulong4 __dual_horizontal_add_skip1(uint16, uint16);




/*-----------------------------------------------------------------------------
* ID: __dual_sort_lo_asc_hi_asc
*----------------------------------------------------------------------------*/

/* VDSORTII16H */
short16 __dual_sort_lo_asc_hi_asc(short16);
short32 __dual_sort_lo_asc_hi_asc(short32);

/* VDSORTIIU16H */
ushort16 __dual_sort_lo_asc_hi_asc(ushort16);
ushort32 __dual_sort_lo_asc_hi_asc(ushort32);




/*-----------------------------------------------------------------------------
* ID: __dual_sort_lo_asc_hi_asc_perm
*----------------------------------------------------------------------------*/

/* VDSORTPII16H */
uchar32 __dual_sort_lo_asc_hi_asc_perm(short16);
uchar64 __dual_sort_lo_asc_hi_asc_perm(short32);

/* VDSORTPIIU16H */
uchar32 __dual_sort_lo_asc_hi_asc_perm(ushort16);
uchar64 __dual_sort_lo_asc_hi_asc_perm(ushort32);




/*-----------------------------------------------------------------------------
* ID: __dual_sort_lo_asc_hi_desc
*----------------------------------------------------------------------------*/

/* VDSORTID16H */
short32 __dual_sort_lo_asc_hi_desc(short32);

/* VDSORTIDU16H */
ushort32 __dual_sort_lo_asc_hi_desc(ushort32);




/*-----------------------------------------------------------------------------
* ID: __dual_sort_lo_asc_hi_desc_perm
*----------------------------------------------------------------------------*/

/* VDSORTPID16H */
uchar64 __dual_sort_lo_asc_hi_desc_perm(short32);

/* VDSORTPIDU16H */
uchar64 __dual_sort_lo_asc_hi_desc_perm(ushort32);




/*-----------------------------------------------------------------------------
* ID: __dual_sort_lo_desc_hi_asc
*----------------------------------------------------------------------------*/

/* VDSORTDI16H */
short32 __dual_sort_lo_desc_hi_asc(short32);

/* VDSORTDIU16H */
ushort32 __dual_sort_lo_desc_hi_asc(ushort32);




/*-----------------------------------------------------------------------------
* ID: __dual_sort_lo_desc_hi_asc_perm
*----------------------------------------------------------------------------*/

/* VDSORTPDI16H */
uchar64 __dual_sort_lo_desc_hi_asc_perm(short32);

/* VDSORTPDIU16H */
uchar64 __dual_sort_lo_desc_hi_asc_perm(ushort32);




/*-----------------------------------------------------------------------------
* ID: __dual_sort_lo_desc_hi_desc
*----------------------------------------------------------------------------*/

/* VDSORTDD16H */
short16 __dual_sort_lo_desc_hi_desc(short16);
short32 __dual_sort_lo_desc_hi_desc(short32);

/* VDSORTDDU16H */
ushort16 __dual_sort_lo_desc_hi_desc(ushort16);
ushort32 __dual_sort_lo_desc_hi_desc(ushort32);




/*-----------------------------------------------------------------------------
* ID: __dual_sort_lo_desc_hi_desc_perm
*----------------------------------------------------------------------------*/

/* VDSORTPDD16H */
uchar32 __dual_sort_lo_desc_hi_desc_perm(short16);
uchar64 __dual_sort_lo_desc_hi_desc_perm(short32);

/* VDSORTPDDU16H */
uchar32 __dual_sort_lo_desc_hi_desc_perm(ushort16);
uchar64 __dual_sort_lo_desc_hi_desc_perm(ushort32);




/*-----------------------------------------------------------------------------
* ID: __duplicate
*----------------------------------------------------------------------------*/

/* VDUPB */
char64 __duplicate(int8_t);
/* CONSTANT: char64 __duplicate((int8_t)(k)); */
uchar64 __duplicate(uchar);
/* CONSTANT: uchar64 __duplicate((uchar)(k)); */

/* VDUPD */
long8 __duplicate(int64_t);
double8 __duplicate(double);
cfloat8 __duplicate(cfloat);
ulong8 __duplicate(ulong);
cint8 __duplicate(cint);

/* VDUPH */
short32 __duplicate(int16_t);
/* CONSTANT: short32 __duplicate((int16_t)(k)); */
ushort32 __duplicate(ushort);
cchar32 __duplicate(cchar);
/* CONSTANT: ushort32 __duplicate((ushort)(k)); */
/* CONSTANT: cchar32 __duplicate((cchar)(k)); */

/* VDUPW */
int16 __duplicate(int32_t);
/* CONSTANT: int16 __duplicate((int32_t)(k)); */
float16 __duplicate(float);
uint16 __duplicate(uint);
cshort16 __duplicate(cshort);
/* CONSTANT: float16 __duplicate((float)(k)); */
/* CONSTANT: uint16 __duplicate((uint)(k)); */
/* CONSTANT: cshort16 __duplicate((cshort)(k)); */




/*-----------------------------------------------------------------------------
* ID: __duplicate16
*----------------------------------------------------------------------------*/

/* VDUP16B */
char16 __duplicate16(int8_t);
char32 __duplicate16(char2);
char64 __duplicate16(char4);
uchar16 __duplicate16(uchar);
uchar32 __duplicate16(uchar2);
uchar64 __duplicate16(uchar4);

/* VDUP16H */
short16 __duplicate16(int16_t);
short32 __duplicate16(short2);
ushort16 __duplicate16(ushort);
ushort32 __duplicate16(ushort2);
cchar16 __duplicate16(cchar);
cchar32 __duplicate16(cchar2);




/*-----------------------------------------------------------------------------
* ID: __duplicate2
*----------------------------------------------------------------------------*/

/* VDUP2B */
char2 __duplicate2(int8_t);
char4 __duplicate2(char2);
char8 __duplicate2(char4);
char16 __duplicate2(char8);
char32 __duplicate2(char16);
char64 __duplicate2(char32);
uchar2 __duplicate2(uchar);
uchar4 __duplicate2(uchar2);
uchar8 __duplicate2(uchar4);
uchar16 __duplicate2(uchar8);
uchar32 __duplicate2(uchar16);
uchar64 __duplicate2(uchar32);

/* VDUP2D */
long2 __duplicate2(int64_t);
long4 __duplicate2(long2);
long8 __duplicate2(long4);
double2 __duplicate2(double);
double4 __duplicate2(double2);
double8 __duplicate2(double4);
cfloat2 __duplicate2(cfloat);
cfloat4 __duplicate2(cfloat2);
cfloat8 __duplicate2(cfloat4);
ulong2 __duplicate2(ulong);
ulong4 __duplicate2(ulong2);
ulong8 __duplicate2(ulong4);
cint2 __duplicate2(cint);
cint4 __duplicate2(cint2);
cint8 __duplicate2(cint4);

/* VDUP2H */
short2 __duplicate2(int16_t);
short4 __duplicate2(short2);
short8 __duplicate2(short4);
short16 __duplicate2(short8);
short32 __duplicate2(short16);
ushort2 __duplicate2(ushort);
ushort4 __duplicate2(ushort2);
ushort8 __duplicate2(ushort4);
ushort16 __duplicate2(ushort8);
ushort32 __duplicate2(ushort16);
cchar2 __duplicate2(cchar);
cchar4 __duplicate2(cchar2);
cchar8 __duplicate2(cchar4);
cchar16 __duplicate2(cchar8);
cchar32 __duplicate2(cchar16);

/* VDUP2W */
int2 __duplicate2(int32_t);
int4 __duplicate2(int2);
int8 __duplicate2(int4);
int16 __duplicate2(int8);
float2 __duplicate2(float);
float4 __duplicate2(float2);
float8 __duplicate2(float4);
float16 __duplicate2(float8);
uint2 __duplicate2(uint);
uint4 __duplicate2(uint2);
uint8 __duplicate2(uint4);
uint16 __duplicate2(uint8);
cshort2 __duplicate2(cshort);
cshort4 __duplicate2(cshort2);
cshort8 __duplicate2(cshort4);
cshort16 __duplicate2(cshort8);




/*-----------------------------------------------------------------------------
* ID: __duplicate32
*----------------------------------------------------------------------------*/

/* VDUP32B */
char32 __duplicate32(int8_t);
char64 __duplicate32(char2);
uchar32 __duplicate32(uchar);
uchar64 __duplicate32(uchar2);




/*-----------------------------------------------------------------------------
* ID: __duplicate4
*----------------------------------------------------------------------------*/

/* VDUP4B */
char4 __duplicate4(int8_t);
char8 __duplicate4(char2);
char16 __duplicate4(char4);
char32 __duplicate4(char8);
char64 __duplicate4(char16);
uchar4 __duplicate4(uchar);
uchar8 __duplicate4(uchar2);
uchar16 __duplicate4(uchar4);
uchar32 __duplicate4(uchar8);
uchar64 __duplicate4(uchar16);

/* VDUP4D */
long4 __duplicate4(int64_t);
long8 __duplicate4(long2);
double4 __duplicate4(double);
double8 __duplicate4(double2);
cfloat4 __duplicate4(cfloat);
cfloat8 __duplicate4(cfloat2);
ulong4 __duplicate4(ulong);
ulong8 __duplicate4(ulong2);
cint4 __duplicate4(cint);
cint8 __duplicate4(cint2);

/* VDUP4H */
short4 __duplicate4(int16_t);
short8 __duplicate4(short2);
short16 __duplicate4(short4);
short32 __duplicate4(short8);
ushort4 __duplicate4(ushort);
ushort8 __duplicate4(ushort2);
ushort16 __duplicate4(ushort4);
ushort32 __duplicate4(ushort8);
cchar4 __duplicate4(cchar);
cchar8 __duplicate4(cchar2);
cchar16 __duplicate4(cchar4);
cchar32 __duplicate4(cchar8);

/* VDUP4W */
int4 __duplicate4(int32_t);
int8 __duplicate4(int2);
int16 __duplicate4(int4);
float4 __duplicate4(float);
float8 __duplicate4(float2);
float16 __duplicate4(float4);
uint4 __duplicate4(uint);
uint8 __duplicate4(uint2);
uint16 __duplicate4(uint4);
cshort4 __duplicate4(cshort);
cshort8 __duplicate4(cshort2);
cshort16 __duplicate4(cshort4);




/*-----------------------------------------------------------------------------
* ID: __duplicate8
*----------------------------------------------------------------------------*/

/* VDUP8B */
char8 __duplicate8(int8_t);
char16 __duplicate8(char2);
char32 __duplicate8(char4);
char64 __duplicate8(char8);
uchar8 __duplicate8(uchar);
uchar16 __duplicate8(uchar2);
uchar32 __duplicate8(uchar4);
uchar64 __duplicate8(uchar8);

/* VDUP8H */
short8 __duplicate8(int16_t);
short16 __duplicate8(short2);
short32 __duplicate8(short4);
ushort8 __duplicate8(ushort);
ushort16 __duplicate8(ushort2);
ushort32 __duplicate8(ushort4);
cchar8 __duplicate8(cchar);
cchar16 __duplicate8(cchar2);
cchar32 __duplicate8(cchar4);

/* VDUP8W */
int8 __duplicate8(int32_t);
int16 __duplicate8(int2);
float8 __duplicate8(float);
float16 __duplicate8(float2);
uint8 __duplicate8(uint);
uint16 __duplicate8(uint2);
cshort8 __duplicate8(cshort);
cshort16 __duplicate8(cshort2);




/*-----------------------------------------------------------------------------
* ID: __duplicate_pred_high_char
*----------------------------------------------------------------------------*/

/* PDUPH2B */
__vpred __duplicate_pred_high_char(__vpred);




/*-----------------------------------------------------------------------------
* ID: __duplicate_pred_high_int
*----------------------------------------------------------------------------*/

/* PDUPH2W */
__vpred __duplicate_pred_high_int(__vpred);




/*-----------------------------------------------------------------------------
* ID: __duplicate_pred_high_long
*----------------------------------------------------------------------------*/

/* PDUPH2D */
__vpred __duplicate_pred_high_long(__vpred);




/*-----------------------------------------------------------------------------
* ID: __duplicate_pred_high_short
*----------------------------------------------------------------------------*/

/* PDUPH2H */
__vpred __duplicate_pred_high_short(__vpred);




/*-----------------------------------------------------------------------------
* ID: __duplicate_pred_low_char
*----------------------------------------------------------------------------*/

/* PDUPL2B */
__vpred __duplicate_pred_low_char(__vpred);




/*-----------------------------------------------------------------------------
* ID: __duplicate_pred_low_int
*----------------------------------------------------------------------------*/

/* PDUPL2W */
__vpred __duplicate_pred_low_int(__vpred);




/*-----------------------------------------------------------------------------
* ID: __duplicate_pred_low_long
*----------------------------------------------------------------------------*/

/* PDUPL2D */
__vpred __duplicate_pred_low_long(__vpred);




/*-----------------------------------------------------------------------------
* ID: __duplicate_pred_low_short
*----------------------------------------------------------------------------*/

/* PDUPL2H */
__vpred __duplicate_pred_low_short(__vpred);




/*-----------------------------------------------------------------------------
* ID: __expand
*----------------------------------------------------------------------------*/

/* VPXPND */
char64 __expand(__vpred);




/*-----------------------------------------------------------------------------
* ID: __expand_lsb_pack_char
*----------------------------------------------------------------------------*/

/* XPND8B */
uchar8 __expand_lsb_pack_char(uchar);
char8 __expand_lsb_pack_char(int8_t);




/*-----------------------------------------------------------------------------
* ID: __expand_lsb_pack_short
*----------------------------------------------------------------------------*/

/* XPND4H */
ushort4 __expand_lsb_pack_short(uchar);
short4 __expand_lsb_pack_short(int8_t);




/*-----------------------------------------------------------------------------
* ID: __ext_dkkd
*----------------------------------------------------------------------------*/

/* EXT */
int64_t __ext_dkkd(char8, uchar, uchar);
int64_t __ext_dkkd(short4, uchar, uchar);
int64_t __ext_dkkd(int2, uchar, uchar);
int64_t __ext_dkkd(int64_t, uchar, uchar);




/*-----------------------------------------------------------------------------
* ID: __extu_dkkd
*----------------------------------------------------------------------------*/

/* EXTU */
ulong __extu_dkkd(uchar8, uchar, uchar);
ulong __extu_dkkd(ushort4, uchar, uchar);
ulong __extu_dkkd(uint2, uchar, uchar);
ulong __extu_dkkd(ulong, uchar, uchar);




/*-----------------------------------------------------------------------------
* ID: __extuv_vkkkd
*----------------------------------------------------------------------------*/

/* EXTUV */
ulong __extuv_vkkkd(ulong8, uchar, uchar, uchar);




/*-----------------------------------------------------------------------------
* ID: __extv_vkkkd
*----------------------------------------------------------------------------*/

/* EXTV */
int64_t __extv_vkkkd(long8, uchar, uchar, uchar);


/*----------------------------------------------------------------------------*/
/* __float_to_half_float                                                        */
/*----------------------------------------------------------------------------*/
/* VSPHP.vv */
uint __float_to_half_float(float);
uint2 __float_to_half_float(float2);
/* UNSUPPORTED: uint3 __float_to_half_float(float3);*/
uint4 __float_to_half_float(float4);
uint8 __float_to_half_float(float8);
uint16 __float_to_half_float(float16);


/*-----------------------------------------------------------------------------
* ID: __float_to_int
*----------------------------------------------------------------------------*/

/* VSPINT */
int32_t __float_to_int(float);
int2 __float_to_int(float2);
/* UNSUPPORTED: int3 __float_to_int(float3); */
int4 __float_to_int(float4);
int8 __float_to_int(float8);
int16 __float_to_int(float16);




/*-----------------------------------------------------------------------------
* ID: __float_to_int_rtz
*----------------------------------------------------------------------------*/

/* VSPTRUNC */
int32_t __float_to_int_rtz(float);
int2 __float_to_int_rtz(float2);
/* UNSUPPORTED: int3 __float_to_int_rtz(float3); */
int4 __float_to_int_rtz(float4);
int8 __float_to_int_rtz(float8);
int16 __float_to_int_rtz(float16);




/*-----------------------------------------------------------------------------
* ID: __float_to_short
*----------------------------------------------------------------------------*/

/* VSPINTH */
short2 __float_to_short(float);
short4 __float_to_short(float2);
short8 __float_to_short(float4);
short16 __float_to_short(float8);
short32 __float_to_short(float16);




/*-----------------------------------------------------------------------------
* ID: __gather_set_bits
*----------------------------------------------------------------------------*/

/* VGATHERB */
char64 __gather_set_bits(__vpred, char64);




/*-----------------------------------------------------------------------------
* ID: __gather_unset_bits
*----------------------------------------------------------------------------*/

/* VGATHERNB */
char64 __gather_unset_bits(__vpred, char64);




/*-----------------------------------------------------------------------------
* ID: __gmpy
*----------------------------------------------------------------------------*/

/* VGMPYB */
uchar __gmpy(uchar, uchar);
uchar2 __gmpy(uchar2, uchar2);
/* UNSUPPORTED: uchar3 __gmpy(uchar3, uchar3); */
uchar4 __gmpy(uchar4, uchar4);
uchar8 __gmpy(uchar8, uchar8);
uchar16 __gmpy(uchar16, uchar16);
uchar32 __gmpy(uchar32, uchar32);
uchar64 __gmpy(uchar64, uchar64);

/* VGMPYW */
uint __gmpy(uint, uint);
uint2 __gmpy(uint2, uint2);
/* UNSUPPORTED: uint3 __gmpy(uint3, uint3); */
uint4 __gmpy(uint4, uint4);
uint8 __gmpy(uint8, uint8);
uint16 __gmpy(uint16, uint16);




/*-----------------------------------------------------------------------------
* ID: __hadd
*----------------------------------------------------------------------------*/

/* VAVGNRB */
int8_t __hadd(int8_t, int8_t);
char2 __hadd(char2, char2);
/* UNSUPPORTED: char3 __hadd(char3, char3); */
char4 __hadd(char4, char4);
char8 __hadd(char8, char8);
char16 __hadd(char16, char16);
char32 __hadd(char32, char32);
char64 __hadd(char64, char64);

/* VAVGNRD */
int64_t __hadd(int64_t, int64_t);
long2 __hadd(long2, long2);
/* UNSUPPORTED: long3 __hadd(long3, long3); */
long4 __hadd(long4, long4);
long8 __hadd(long8, long8);

/* VAVGNRH */
int16_t __hadd(int16_t, int16_t);
short2 __hadd(short2, short2);
/* UNSUPPORTED: short3 __hadd(short3, short3); */
short4 __hadd(short4, short4);
short8 __hadd(short8, short8);
short16 __hadd(short16, short16);
short32 __hadd(short32, short32);

/* VAVGNRUB */
uchar __hadd(uchar, uchar);
uchar2 __hadd(uchar2, uchar2);
/* UNSUPPORTED: uchar3 __hadd(uchar3, uchar3); */
uchar4 __hadd(uchar4, uchar4);
uchar8 __hadd(uchar8, uchar8);
uchar16 __hadd(uchar16, uchar16);
uchar32 __hadd(uchar32, uchar32);
uchar64 __hadd(uchar64, uchar64);

/* VAVGNRUD */
ulong __hadd(ulong, ulong);
ulong2 __hadd(ulong2, ulong2);
/* UNSUPPORTED: ulong3 __hadd(ulong3, ulong3); */
ulong4 __hadd(ulong4, ulong4);
ulong8 __hadd(ulong8, ulong8);

/* VAVGNRUH */
ushort __hadd(ushort, ushort);
ushort2 __hadd(ushort2, ushort2);
/* UNSUPPORTED: ushort3 __hadd(ushort3, ushort3); */
ushort4 __hadd(ushort4, ushort4);
ushort8 __hadd(ushort8, ushort8);
ushort16 __hadd(ushort16, ushort16);
ushort32 __hadd(ushort32, ushort32);

/* VAVGNRUW */
uint __hadd(uint, uint);
uint2 __hadd(uint2, uint2);
/* UNSUPPORTED: uint3 __hadd(uint3, uint3); */
uint4 __hadd(uint4, uint4);
uint8 __hadd(uint8, uint8);
uint16 __hadd(uint16, uint16);

/* VAVGNRW */
int32_t __hadd(int32_t, int32_t);
int2 __hadd(int2, int2);
/* UNSUPPORTED: int3 __hadd(int3, int3); */
int4 __hadd(int4, int4);
int8 __hadd(int8, int8);
int16 __hadd(int16, int16);


/*----------------------------------------------------------------------------*/
/* __half_float_to_float                                                      */
/*----------------------------------------------------------------------------*/
/* VHPSP.vv */
float __half_float_to_float(uint);
float2 __half_float_to_float(uint2);
/* UNSUPPORTED: float3 __half_float_to_float(uint3);*/
float4 __half_float_to_float(uint4);
float8 __half_float_to_float(uint8);
float16 __half_float_to_float(uint16);

/*----------------------------------------------------------------------------*/
/* __half_float_to_int                                                        */
/*----------------------------------------------------------------------------*/
/* VHPSP.vv */
int __half_float_to_int(uint);
int2 __half_float_to_int(uint2);
/* UNSUPPORTED: int3 __half_float_to_int(uint3);*/
int4 __half_float_to_int(uint4);
int8 __half_float_to_int(uint8);
int16 __half_float_to_int(uint16);


/*-----------------------------------------------------------------------------
* ID: __high_char_to_short
*----------------------------------------------------------------------------*/

/* VBUNPKHH */
int16_t __high_char_to_short(char2);
short2 __high_char_to_short(char4);
short4 __high_char_to_short(char8);
short8 __high_char_to_short(char16);
short16 __high_char_to_short(char32);
short32 __high_char_to_short(char64);




/*-----------------------------------------------------------------------------
* ID: __high_float_to_double
*----------------------------------------------------------------------------*/

/* VSPDPH */
double __high_float_to_double(float2);
double2 __high_float_to_double(float4);
double4 __high_float_to_double(float8);
double8 __high_float_to_double(float16);




/*-----------------------------------------------------------------------------
* ID: __high_half_int_to_float
*----------------------------------------------------------------------------*/

/* VINTHSPH */
float2 __high_half_int_to_float(short4);
float4 __high_half_int_to_float(short8);
float8 __high_half_int_to_float(short16);
float16 __high_half_int_to_float(short32);

/* VINTHSPUH */
float2 __high_half_int_to_float(ushort4);
float4 __high_half_int_to_float(ushort8);
float8 __high_half_int_to_float(ushort16);
float16 __high_half_int_to_float(ushort32);




/*-----------------------------------------------------------------------------
* ID: __high_int_to_double
*----------------------------------------------------------------------------*/

/* VINTDPH */
double __high_int_to_double(int2);
double2 __high_int_to_double(int4);
double4 __high_int_to_double(int8);
double8 __high_int_to_double(int16);

/* VINTDPUH */
double __high_int_to_double(uint2);
double2 __high_int_to_double(uint4);
double4 __high_int_to_double(uint8);
double8 __high_int_to_double(uint16);




/*-----------------------------------------------------------------------------
* ID: __high_int_to_long
*----------------------------------------------------------------------------*/

/* VWUNPKDH */
int64_t __high_int_to_long(int2);
long2 __high_int_to_long(int4);
long4 __high_int_to_long(int8);
long8 __high_int_to_long(int16);




/*-----------------------------------------------------------------------------
* ID: __high_short_to_int
*----------------------------------------------------------------------------*/

/* VHUNPKWH */
int32_t __high_short_to_int(short2);
int2 __high_short_to_int(short4);
int4 __high_short_to_int(short8);
int8 __high_short_to_int(short16);
int16 __high_short_to_int(short32);




/*-----------------------------------------------------------------------------
* ID: __high_uchar_to_ushort
*----------------------------------------------------------------------------*/

/* VBUNPKUHH */
ushort __high_uchar_to_ushort(uchar2);
ushort2 __high_uchar_to_ushort(uchar4);
ushort4 __high_uchar_to_ushort(uchar8);
ushort8 __high_uchar_to_ushort(uchar16);
ushort16 __high_uchar_to_ushort(uchar32);
ushort32 __high_uchar_to_ushort(uchar64);




/*-----------------------------------------------------------------------------
* ID: __high_uint_to_ulong
*----------------------------------------------------------------------------*/

/* VWUNPKUDH */
ulong __high_uint_to_ulong(uint2);
ulong2 __high_uint_to_ulong(uint4);
ulong4 __high_uint_to_ulong(uint8);
ulong8 __high_uint_to_ulong(uint16);




/*-----------------------------------------------------------------------------
* ID: __high_ushort_to_uint
*----------------------------------------------------------------------------*/

/* VHUNPKUWH */
uint __high_ushort_to_uint(ushort2);
uint2 __high_ushort_to_uint(ushort4);
uint4 __high_ushort_to_uint(ushort8);
uint8 __high_ushort_to_uint(ushort16);
uint16 __high_ushort_to_uint(ushort32);




/*-----------------------------------------------------------------------------
* ID: __horizontal_add
*----------------------------------------------------------------------------*/

/* VHADD16W1D */
int64_t __horizontal_add(int16);

/* VHADD32H1D */
int64_t __horizontal_add(short32);

/* VHADD64B1D */
int64_t __horizontal_add(char64);

/* VHADD8D1D */
int64_t __horizontal_add(long8);

/* VHADDU16W1D */
ulong __horizontal_add(uint16);

/* VHADDU32H1D */
ulong __horizontal_add(ushort32);

/* VHADDU64B1D */
ulong __horizontal_add(uchar64);

/* VHADDU8D1D */
ulong __horizontal_add(ulong8);




/*-----------------------------------------------------------------------------
* ID: __horizontal_add_skip1
*----------------------------------------------------------------------------*/

/* VHADDEO16H2W */
int2 __horizontal_add_skip1(short32);
cint __horizontal_add_skip1(cshort16);

/* VHADDEO8W2D */
long2 __horizontal_add_skip1(int16);
clong __horizontal_add_skip1(cint8);

/* VHADDUEO16H2W */
uint2 __horizontal_add_skip1(ushort32);

/* VHADDUEO8W2D */
ulong2 __horizontal_add_skip1(uint16);




/*-----------------------------------------------------------------------------
* ID: __horizontal_xor
*----------------------------------------------------------------------------*/

/* VHXOR16W1W */
int32_t __horizontal_xor(int16);

/* VHXOR32H1H */
int16_t __horizontal_xor(short32);

/* VHXOR64B1B */
int16_t __horizontal_xor(char64);

/* VHXOR8D1D */
int64_t __horizontal_xor(long8);




/*-----------------------------------------------------------------------------
* ID: __int40_to_int_sat
*----------------------------------------------------------------------------*/

/* VSATLW */
int64_t __int40_to_int_sat(int64_t);
long2 __int40_to_int_sat(long2);
/* UNSUPPORTED: long3 __int40_to_int_sat(long3); */
long4 __int40_to_int_sat(long4);
long8 __int40_to_int_sat(long8);


/*----------------------------------------------------------------------------*/
/* __int_to_half_float                                                        */
/*----------------------------------------------------------------------------*/
/* VINTHP.vv */
uint __int_to_half_float(int);
uint2 __int_to_half_float(int2);
/* UNSUPPORTED: uint3 __int_to_half_float(int3); */
uint4 __int_to_half_float(int4);
uint8 __int_to_half_float(int8);
uint16 __int_to_half_float(int16);


/*-----------------------------------------------------------------------------
* ID: __int_to_float
*----------------------------------------------------------------------------*/

/* VINTSP */
float __int_to_float(int32_t);
float2 __int_to_float(int2);
/* UNSUPPORTED: float3 __int_to_float(int3); */
float4 __int_to_float(int4);
float8 __int_to_float(int8);
float16 __int_to_float(int16);

/* VINTSPU */
float __int_to_float(uint);
float2 __int_to_float(uint2);
/* UNSUPPORTED: float3 __int_to_float(uint3); */
float4 __int_to_float(uint4);
float8 __int_to_float(uint8);
float16 __int_to_float(uint16);




/*-----------------------------------------------------------------------------
* ID: __int_to_short_sat
*----------------------------------------------------------------------------*/

/* VSATWH */
int32_t __int_to_short_sat(int32_t);
int2 __int_to_short_sat(int2);
/* UNSUPPORTED: int3 __int_to_short_sat(int3); */
int4 __int_to_short_sat(int4);
int8 __int_to_short_sat(int8);
int16 __int_to_short_sat(int16);




/*-----------------------------------------------------------------------------
* ID: __landd_ddr
*----------------------------------------------------------------------------*/

/* LANDD */
int32_t __landd_ddr(int64_t, int64_t);




/*-----------------------------------------------------------------------------
* ID: __landnd_ddr
*----------------------------------------------------------------------------*/

/* LANDND */
int32_t __landnd_ddr(int64_t, int64_t);




/*-----------------------------------------------------------------------------
* ID: __leftmost_bit_detect
*----------------------------------------------------------------------------*/

/* VLMBDB */
uchar __leftmost_bit_detect(uchar, uchar);
uchar2 __leftmost_bit_detect(uchar2, uchar2);
/* UNSUPPORTED: uchar3 __leftmost_bit_detect(uchar3, uchar3); */
uchar4 __leftmost_bit_detect(uchar4, uchar4);
uchar8 __leftmost_bit_detect(uchar8, uchar8);
uchar16 __leftmost_bit_detect(uchar16, uchar16);
uchar32 __leftmost_bit_detect(uchar32, uchar32);
uchar64 __leftmost_bit_detect(uchar64, uchar64);

/* VLMBDD */
ulong __leftmost_bit_detect(ulong, ulong);
ulong2 __leftmost_bit_detect(ulong2, ulong2);
/* UNSUPPORTED: ulong3 __leftmost_bit_detect(ulong3, ulong3); */
ulong4 __leftmost_bit_detect(ulong4, ulong4);
ulong8 __leftmost_bit_detect(ulong8, ulong8);

/* VLMBDH */
ushort __leftmost_bit_detect(ushort, ushort);
ushort2 __leftmost_bit_detect(ushort2, ushort2);
/* UNSUPPORTED: ushort3 __leftmost_bit_detect(ushort3, ushort3); */
ushort4 __leftmost_bit_detect(ushort4, ushort4);
ushort8 __leftmost_bit_detect(ushort8, ushort8);
ushort16 __leftmost_bit_detect(ushort16, ushort16);
ushort32 __leftmost_bit_detect(ushort32, ushort32);

/* VLMBDW */
uint __leftmost_bit_detect(uint, uint);
uint2 __leftmost_bit_detect(uint2, uint2);
/* UNSUPPORTED: uint3 __leftmost_bit_detect(uint3, uint3); */
uint4 __leftmost_bit_detect(uint4, uint4);
uint8 __leftmost_bit_detect(uint8, uint8);
uint16 __leftmost_bit_detect(uint16, uint16);




/*-----------------------------------------------------------------------------
* ID: __leftmost_bit_detect_one
*----------------------------------------------------------------------------*/

/* VLMBD1B */
uchar __leftmost_bit_detect_one(uchar);
uchar2 __leftmost_bit_detect_one(uchar2);
/* UNSUPPORTED: uchar3 __leftmost_bit_detect_one(uchar3); */
uchar4 __leftmost_bit_detect_one(uchar4);
uchar8 __leftmost_bit_detect_one(uchar8);
uchar16 __leftmost_bit_detect_one(uchar16);
uchar32 __leftmost_bit_detect_one(uchar32);
uchar64 __leftmost_bit_detect_one(uchar64);

/* VLMBD1D */
ulong __leftmost_bit_detect_one(ulong);
ulong2 __leftmost_bit_detect_one(ulong2);
/* UNSUPPORTED: ulong3 __leftmost_bit_detect_one(ulong3); */
ulong4 __leftmost_bit_detect_one(ulong4);
ulong8 __leftmost_bit_detect_one(ulong8);

/* VLMBD1H */
ushort __leftmost_bit_detect_one(ushort);
ushort2 __leftmost_bit_detect_one(ushort2);
/* UNSUPPORTED: ushort3 __leftmost_bit_detect_one(ushort3); */
ushort4 __leftmost_bit_detect_one(ushort4);
ushort8 __leftmost_bit_detect_one(ushort8);
ushort16 __leftmost_bit_detect_one(ushort16);
ushort32 __leftmost_bit_detect_one(ushort32);

/* VLMBD1W */
uint __leftmost_bit_detect_one(uint);
uint2 __leftmost_bit_detect_one(uint2);
/* UNSUPPORTED: uint3 __leftmost_bit_detect_one(uint3); */
uint4 __leftmost_bit_detect_one(uint4);
uint8 __leftmost_bit_detect_one(uint8);
uint16 __leftmost_bit_detect_one(uint16);




/*-----------------------------------------------------------------------------
* ID: __leftmost_bit_detect_zero
*----------------------------------------------------------------------------*/

/* VLMBD0B */
uchar __leftmost_bit_detect_zero(uchar);
uchar2 __leftmost_bit_detect_zero(uchar2);
/* UNSUPPORTED: uchar3 __leftmost_bit_detect_zero(uchar3); */
uchar4 __leftmost_bit_detect_zero(uchar4);
uchar8 __leftmost_bit_detect_zero(uchar8);
uchar16 __leftmost_bit_detect_zero(uchar16);
uchar32 __leftmost_bit_detect_zero(uchar32);
uchar64 __leftmost_bit_detect_zero(uchar64);

/* VLMBD0D */
ulong __leftmost_bit_detect_zero(ulong);
ulong2 __leftmost_bit_detect_zero(ulong2);
/* UNSUPPORTED: ulong3 __leftmost_bit_detect_zero(ulong3); */
ulong4 __leftmost_bit_detect_zero(ulong4);
ulong8 __leftmost_bit_detect_zero(ulong8);

/* VLMBD0H */
ushort __leftmost_bit_detect_zero(ushort);
ushort2 __leftmost_bit_detect_zero(ushort2);
/* UNSUPPORTED: ushort3 __leftmost_bit_detect_zero(ushort3); */
ushort4 __leftmost_bit_detect_zero(ushort4);
ushort8 __leftmost_bit_detect_zero(ushort8);
ushort16 __leftmost_bit_detect_zero(ushort16);
ushort32 __leftmost_bit_detect_zero(ushort32);

/* VLMBD0W */
uint __leftmost_bit_detect_zero(uint);
uint2 __leftmost_bit_detect_zero(uint2);
/* UNSUPPORTED: uint3 __leftmost_bit_detect_zero(uint3); */
uint4 __leftmost_bit_detect_zero(uint4);
uint8 __leftmost_bit_detect_zero(uint8);
uint16 __leftmost_bit_detect_zero(uint16);




/*-----------------------------------------------------------------------------
* ID: __logical_and
*----------------------------------------------------------------------------*/

/* LANDD */
int32_t __logical_and(int64_t, int64_t);




/*-----------------------------------------------------------------------------
* ID: __logical_andn
*----------------------------------------------------------------------------*/

/* LANDND */
int32_t __logical_andn(int64_t, int64_t);




/*-----------------------------------------------------------------------------
* ID: __logical_or
*----------------------------------------------------------------------------*/

/* LORD */
int32_t __logical_or(int64_t, int64_t);




/*-----------------------------------------------------------------------------
* ID: __long_to_int40_sat
*----------------------------------------------------------------------------*/

/* VSATDL */
int64_t __long_to_int40_sat(int64_t);
long2 __long_to_int40_sat(long2);
/* UNSUPPORTED: long3 __long_to_int40_sat(long3); */
long4 __long_to_int40_sat(long4);
long8 __long_to_int40_sat(long8);




/*-----------------------------------------------------------------------------
* ID: __long_to_int_sat
*----------------------------------------------------------------------------*/

/* VSATDW */
int64_t __long_to_int_sat(int64_t);
long2 __long_to_int_sat(long2);
/* UNSUPPORTED: long3 __long_to_int_sat(long3); */
long4 __long_to_int_sat(long4);
long8 __long_to_int_sat(long8);




/*-----------------------------------------------------------------------------
* ID: __lord_ddr
*----------------------------------------------------------------------------*/

/* LORD */
int32_t __lord_ddr(int64_t, int64_t);




/*-----------------------------------------------------------------------------
* ID: __low_char_to_short
*----------------------------------------------------------------------------*/

/* VBUNPKHL */
int16_t __low_char_to_short(char2);
short2 __low_char_to_short(char4);
short4 __low_char_to_short(char8);
short8 __low_char_to_short(char16);
short16 __low_char_to_short(char32);
short32 __low_char_to_short(char64);




/*-----------------------------------------------------------------------------
* ID: __low_float_to_double
*----------------------------------------------------------------------------*/

/* VSPDPL */
double __low_float_to_double(float2);
double2 __low_float_to_double(float4);
double4 __low_float_to_double(float8);
double8 __low_float_to_double(float16);




/*-----------------------------------------------------------------------------
* ID: __low_half_int_to_float
*----------------------------------------------------------------------------*/

/* VINTHSPL */
float2 __low_half_int_to_float(short4);
float4 __low_half_int_to_float(short8);
float8 __low_half_int_to_float(short16);
float16 __low_half_int_to_float(short32);

/* VINTHSPUL */
float2 __low_half_int_to_float(ushort4);
float4 __low_half_int_to_float(ushort8);
float8 __low_half_int_to_float(ushort16);
float16 __low_half_int_to_float(ushort32);




/*-----------------------------------------------------------------------------
* ID: __low_int_to_double
*----------------------------------------------------------------------------*/

/* VINTDPL */
double __low_int_to_double(int2);
double2 __low_int_to_double(int4);
double4 __low_int_to_double(int8);
double8 __low_int_to_double(int16);

/* VINTDPUL */
double __low_int_to_double(uint2);
double2 __low_int_to_double(uint4);
double4 __low_int_to_double(uint8);
double8 __low_int_to_double(uint16);




/*-----------------------------------------------------------------------------
* ID: __low_int_to_long
*----------------------------------------------------------------------------*/

/* VWUNPKDL */
int64_t __low_int_to_long(int2);
long2 __low_int_to_long(int4);
long4 __low_int_to_long(int8);
long8 __low_int_to_long(int16);




/*-----------------------------------------------------------------------------
* ID: __low_short_to_int
*----------------------------------------------------------------------------*/

/* VHUNPKWL */
int32_t __low_short_to_int(short2);
int2 __low_short_to_int(short4);
int4 __low_short_to_int(short8);
int8 __low_short_to_int(short16);
int16 __low_short_to_int(short32);




/*-----------------------------------------------------------------------------
* ID: __low_uchar_to_ushort
*----------------------------------------------------------------------------*/

/* VBUNPKUHL */
ushort __low_uchar_to_ushort(uchar2);
ushort2 __low_uchar_to_ushort(uchar4);
ushort4 __low_uchar_to_ushort(uchar8);
ushort8 __low_uchar_to_ushort(uchar16);
ushort16 __low_uchar_to_ushort(uchar32);
ushort32 __low_uchar_to_ushort(uchar64);




/*-----------------------------------------------------------------------------
* ID: __low_uint_to_ulong
*----------------------------------------------------------------------------*/

/* VWUNPKUDL */
ulong __low_uint_to_ulong(uint2);
ulong2 __low_uint_to_ulong(uint4);
ulong4 __low_uint_to_ulong(uint8);
ulong8 __low_uint_to_ulong(uint16);




/*-----------------------------------------------------------------------------
* ID: __low_ushort_to_uint
*----------------------------------------------------------------------------*/

/* VHUNPKUWL */
uint __low_ushort_to_uint(ushort2);
uint2 __low_ushort_to_uint(ushort4);
uint4 __low_ushort_to_uint(ushort8);
uint8 __low_ushort_to_uint(ushort16);
uint16 __low_ushort_to_uint(ushort32);




/*-----------------------------------------------------------------------------
* ID: __mask_char
*----------------------------------------------------------------------------*/

/* MASKB */
__vpred __mask_char(uint);
__vpred __mask_char(uchar);




/*-----------------------------------------------------------------------------
* ID: __mask_int
*----------------------------------------------------------------------------*/

/* MASKW */
__vpred __mask_int(uint);
__vpred __mask_int(uchar);




/*-----------------------------------------------------------------------------
* ID: __mask_long
*----------------------------------------------------------------------------*/

/* MASKD */
__vpred __mask_long(uint);
__vpred __mask_long(uchar);




/*-----------------------------------------------------------------------------
* ID: __mask_short
*----------------------------------------------------------------------------*/

/* MASKH */
__vpred __mask_short(uint);
__vpred __mask_short(uchar);




/*-----------------------------------------------------------------------------
* ID: __maskb_kp
*----------------------------------------------------------------------------*/

/* MASKB */
__vpred __maskb_kp(uchar);




/*-----------------------------------------------------------------------------
* ID: __maskb_rp
*----------------------------------------------------------------------------*/

/* MASKB */
__vpred __maskb_rp(uint);




/*-----------------------------------------------------------------------------
* ID: __maskd_kp
*----------------------------------------------------------------------------*/

/* MASKD */
__vpred __maskd_kp(uchar);




/*-----------------------------------------------------------------------------
* ID: __maskd_rp
*----------------------------------------------------------------------------*/

/* MASKD */
__vpred __maskd_rp(uint);




/*-----------------------------------------------------------------------------
* ID: __maskh_kp
*----------------------------------------------------------------------------*/

/* MASKH */
__vpred __maskh_kp(uchar);




/*-----------------------------------------------------------------------------
* ID: __maskh_rp
*----------------------------------------------------------------------------*/

/* MASKH */
__vpred __maskh_rp(uint);




/*-----------------------------------------------------------------------------
* ID: __maskw_kp
*----------------------------------------------------------------------------*/

/* MASKW */
__vpred __maskw_kp(uchar);




/*-----------------------------------------------------------------------------
* ID: __maskw_rp
*----------------------------------------------------------------------------*/

/* MASKW */
__vpred __maskw_rp(uint);




/*-----------------------------------------------------------------------------
* ID: __matmpy
*----------------------------------------------------------------------------*/

/* VMATMPYHW */
int16 __matmpy(__SE_REG, __SE_REG);




/*-----------------------------------------------------------------------------
* ID: __matmpy_u2s
*----------------------------------------------------------------------------*/

/* VMATMPYUSHW */
int16 __matmpy_u2s(__SE_REG, __SE_REG);




/*-----------------------------------------------------------------------------
* ID: __max
*----------------------------------------------------------------------------*/

/* VMAXB */
int8_t __max(int8_t, int8_t);
char2 __max(char2, char2);
/* UNSUPPORTED: char3 __max(char3, char3); */
char4 __max(char4, char4);
char8 __max(char8, char8);
char16 __max(char16, char16);
char32 __max(char32, char32);
char64 __max(char64, char64);

/* VMAXD */
int64_t __max(int64_t, int64_t);
long2 __max(long2, long2);
/* UNSUPPORTED: long3 __max(long3, long3); */
long4 __max(long4, long4);
long8 __max(long8, long8);

/* VMAXDP */
double __max(double, double);
double2 __max(double2, double2);
/* UNSUPPORTED: double3 __max(double3, double3); */
double4 __max(double4, double4);
double8 __max(double8, double8);

/* VMAXH */
int16_t __max(int16_t, int16_t);
short2 __max(short2, short2);
/* UNSUPPORTED: short3 __max(short3, short3); */
short4 __max(short4, short4);
short8 __max(short8, short8);
short16 __max(short16, short16);
short32 __max(short32, short32);

/* VMAXSP */
float __max(float, float);
float2 __max(float2, float2);
/* UNSUPPORTED: float3 __max(float3, float3); */
float4 __max(float4, float4);
float8 __max(float8, float8);
float16 __max(float16, float16);

/* VMAXUB */
uchar __max(uchar, uchar);
uchar2 __max(uchar2, uchar2);
/* UNSUPPORTED: uchar3 __max(uchar3, uchar3); */
uchar4 __max(uchar4, uchar4);
uchar8 __max(uchar8, uchar8);
uchar16 __max(uchar16, uchar16);
uchar32 __max(uchar32, uchar32);
uchar64 __max(uchar64, uchar64);

/* VMAXUD */
ulong __max(ulong, ulong);
ulong2 __max(ulong2, ulong2);
/* UNSUPPORTED: ulong3 __max(ulong3, ulong3); */
ulong4 __max(ulong4, ulong4);
ulong8 __max(ulong8, ulong8);

/* VMAXUH */
ushort __max(ushort, ushort);
ushort2 __max(ushort2, ushort2);
/* UNSUPPORTED: ushort3 __max(ushort3, ushort3); */
ushort4 __max(ushort4, ushort4);
ushort8 __max(ushort8, ushort8);
ushort16 __max(ushort16, ushort16);
ushort32 __max(ushort32, ushort32);

/* VMAXUW */
uint __max(uint, uint);
uint2 __max(uint2, uint2);
/* UNSUPPORTED: uint3 __max(uint3, uint3); */
uint4 __max(uint4, uint4);
uint8 __max(uint8, uint8);
uint16 __max(uint16, uint16);

/* VMAXW */
int32_t __max(int32_t, int32_t);
int2 __max(int2, int2);
/* UNSUPPORTED: int3 __max(int3, int3); */
int4 __max(int4, int4);
int8 __max(int8, int8);
int16 __max(int16, int16);




/*-----------------------------------------------------------------------------
* ID: __max_circ
*----------------------------------------------------------------------------*/

/* VCMAXB */
int8_t __max_circ(int8_t, int8_t);
char2 __max_circ(char2, char2);
/* UNSUPPORTED: char3 __max_circ(char3, char3); */
char4 __max_circ(char4, char4);
char8 __max_circ(char8, char8);
char16 __max_circ(char16, char16);
char32 __max_circ(char32, char32);
char64 __max_circ(char64, char64);

/* VCMAXH */
int16_t __max_circ(int16_t, int16_t);
short2 __max_circ(short2, short2);
/* UNSUPPORTED: short3 __max_circ(short3, short3); */
short4 __max_circ(short4, short4);
short8 __max_circ(short8, short8);
short16 __max_circ(short16, short16);
short32 __max_circ(short32, short32);




/*-----------------------------------------------------------------------------
* ID: __max_circ_pred
*----------------------------------------------------------------------------*/

/* VCMAXPB */
void __max_circ_pred(int8_t, int8_t&, __vpred&);
void __max_circ_pred(char2, char2::EQUIV_ACCESS_T<0>&, __vpred&);
/* UNSUPPORTED: void __max_circ_pred(char3, char3::EQUIV_ACCESS_T<0>&, __vpred&); */
void __max_circ_pred(char4, char4::EQUIV_ACCESS_T<0>&, __vpred&);
void __max_circ_pred(char8, char8::EQUIV_ACCESS_T<0>&, __vpred&);
void __max_circ_pred(char16, char16::EQUIV_ACCESS_T<0>&, __vpred&);
void __max_circ_pred(char32, char32::EQUIV_ACCESS_T<0>&, __vpred&);
void __max_circ_pred(char64, char64::EQUIV_ACCESS_T<0>&, __vpred&);

/* VCMAXPH */
void __max_circ_pred(int16_t, int16_t&, __vpred&);
void __max_circ_pred(short2, short2::EQUIV_ACCESS_T<0>&, __vpred&);
/* UNSUPPORTED: void __max_circ_pred(short3, short3::EQUIV_ACCESS_T<0>&, __vpred&); */
void __max_circ_pred(short4, short4::EQUIV_ACCESS_T<0>&, __vpred&);
void __max_circ_pred(short8, short8::EQUIV_ACCESS_T<0>&, __vpred&);
void __max_circ_pred(short16, short16::EQUIV_ACCESS_T<0>&, __vpred&);
void __max_circ_pred(short32, short32::EQUIV_ACCESS_T<0>&, __vpred&);




/*-----------------------------------------------------------------------------
* ID: __max_index
*----------------------------------------------------------------------------*/

/* VMAXPB */
void __max_index(int8_t, int8_t&, __vpred&);
void __max_index(char2, char2::EQUIV_ACCESS_T<0>&, __vpred&);
/* UNSUPPORTED: void __max_index(char3, char3::EQUIV_ACCESS_T<0>&, __vpred&); */
void __max_index(char4, char4::EQUIV_ACCESS_T<0>&, __vpred&);
void __max_index(char8, char8::EQUIV_ACCESS_T<0>&, __vpred&);
void __max_index(char16, char16::EQUIV_ACCESS_T<0>&, __vpred&);
void __max_index(char32, char32::EQUIV_ACCESS_T<0>&, __vpred&);
void __max_index(char64, char64::EQUIV_ACCESS_T<0>&, __vpred&);

/* VMAXPD */
void __max_index(int64_t, int64_t&, __vpred&);
void __max_index(long2, long2::EQUIV_ACCESS_T<0>&, __vpred&);
/* UNSUPPORTED: void __max_index(long3, long3::EQUIV_ACCESS_T<0>&, __vpred&); */
void __max_index(long4, long4::EQUIV_ACCESS_T<0>&, __vpred&);
void __max_index(long8, long8::EQUIV_ACCESS_T<0>&, __vpred&);

/* VMAXPH */
void __max_index(int16_t, int16_t&, __vpred&);
void __max_index(short2, short2::EQUIV_ACCESS_T<0>&, __vpred&);
/* UNSUPPORTED: void __max_index(short3, short3::EQUIV_ACCESS_T<0>&, __vpred&); */
void __max_index(short4, short4::EQUIV_ACCESS_T<0>&, __vpred&);
void __max_index(short8, short8::EQUIV_ACCESS_T<0>&, __vpred&);
void __max_index(short16, short16::EQUIV_ACCESS_T<0>&, __vpred&);
void __max_index(short32, short32::EQUIV_ACCESS_T<0>&, __vpred&);

/* VMAXPW */
void __max_index(int32_t, int32_t&, __vpred&);
void __max_index(int2, int2::EQUIV_ACCESS_T<0>&, __vpred&);
/* UNSUPPORTED: void __max_index(int3, int3::EQUIV_ACCESS_T<0>&, __vpred&); */
void __max_index(int4, int4::EQUIV_ACCESS_T<0>&, __vpred&);
void __max_index(int8, int8::EQUIV_ACCESS_T<0>&, __vpred&);
void __max_index(int16, int16::EQUIV_ACCESS_T<0>&, __vpred&);

/* VMAXUPB */
void __max_index(uchar, uchar&, __vpred&);
void __max_index(uchar2, uchar2::EQUIV_ACCESS_T<0>&, __vpred&);
/* UNSUPPORTED: void __max_index(uchar3, uchar3::EQUIV_ACCESS_T<0>&, __vpred&); */
void __max_index(uchar4, uchar4::EQUIV_ACCESS_T<0>&, __vpred&);
void __max_index(uchar8, uchar8::EQUIV_ACCESS_T<0>&, __vpred&);
void __max_index(uchar16, uchar16::EQUIV_ACCESS_T<0>&, __vpred&);
void __max_index(uchar32, uchar32::EQUIV_ACCESS_T<0>&, __vpred&);
void __max_index(uchar64, uchar64::EQUIV_ACCESS_T<0>&, __vpred&);

/* VMAXUPD */
void __max_index(ulong, ulong&, __vpred&);
void __max_index(ulong2, ulong2::EQUIV_ACCESS_T<0>&, __vpred&);
/* UNSUPPORTED: void __max_index(ulong3, ulong3::EQUIV_ACCESS_T<0>&, __vpred&); */
void __max_index(ulong4, ulong4::EQUIV_ACCESS_T<0>&, __vpred&);
void __max_index(ulong8, ulong8::EQUIV_ACCESS_T<0>&, __vpred&);

/* VMAXUPH */
void __max_index(ushort, ushort&, __vpred&);
void __max_index(ushort2, ushort2::EQUIV_ACCESS_T<0>&, __vpred&);
/* UNSUPPORTED: void __max_index(ushort3, ushort3::EQUIV_ACCESS_T<0>&, __vpred&); */
void __max_index(ushort4, ushort4::EQUIV_ACCESS_T<0>&, __vpred&);
void __max_index(ushort8, ushort8::EQUIV_ACCESS_T<0>&, __vpred&);
void __max_index(ushort16, ushort16::EQUIV_ACCESS_T<0>&, __vpred&);
void __max_index(ushort32, ushort32::EQUIV_ACCESS_T<0>&, __vpred&);

/* VMAXUPW */
void __max_index(uint, uint&, __vpred&);
void __max_index(uint2, uint2::EQUIV_ACCESS_T<0>&, __vpred&);
/* UNSUPPORTED: void __max_index(uint3, uint3::EQUIV_ACCESS_T<0>&, __vpred&); */
void __max_index(uint4, uint4::EQUIV_ACCESS_T<0>&, __vpred&);
void __max_index(uint8, uint8::EQUIV_ACCESS_T<0>&, __vpred&);
void __max_index(uint16, uint16::EQUIV_ACCESS_T<0>&, __vpred&);




/*-----------------------------------------------------------------------------
* ID: __min
*----------------------------------------------------------------------------*/

/* VMINB */
int8_t __min(int8_t, int8_t);
char2 __min(char2, char2);
/* UNSUPPORTED: char3 __min(char3, char3); */
char4 __min(char4, char4);
char8 __min(char8, char8);
char16 __min(char16, char16);
char32 __min(char32, char32);
char64 __min(char64, char64);

/* VMIND */
int64_t __min(int64_t, int64_t);
long2 __min(long2, long2);
/* UNSUPPORTED: long3 __min(long3, long3); */
long4 __min(long4, long4);
long8 __min(long8, long8);

/* VMINDP */
double __min(double, double);
double2 __min(double2, double2);
/* UNSUPPORTED: double3 __min(double3, double3); */
double4 __min(double4, double4);
double8 __min(double8, double8);

/* VMINH */
int16_t __min(int16_t, int16_t);
short2 __min(short2, short2);
/* UNSUPPORTED: short3 __min(short3, short3); */
short4 __min(short4, short4);
short8 __min(short8, short8);
short16 __min(short16, short16);
short32 __min(short32, short32);

/* VMINSP */
float __min(float, float);
float2 __min(float2, float2);
/* UNSUPPORTED: float3 __min(float3, float3); */
float4 __min(float4, float4);
float8 __min(float8, float8);
float16 __min(float16, float16);

/* VMINUB */
uchar __min(uchar, uchar);
uchar2 __min(uchar2, uchar2);
/* UNSUPPORTED: uchar3 __min(uchar3, uchar3); */
uchar4 __min(uchar4, uchar4);
uchar8 __min(uchar8, uchar8);
uchar16 __min(uchar16, uchar16);
uchar32 __min(uchar32, uchar32);
uchar64 __min(uchar64, uchar64);

/* VMINUD */
ulong __min(ulong, ulong);
ulong2 __min(ulong2, ulong2);
/* UNSUPPORTED: ulong3 __min(ulong3, ulong3); */
ulong4 __min(ulong4, ulong4);
ulong8 __min(ulong8, ulong8);

/* VMINUH */
ushort __min(ushort, ushort);
ushort2 __min(ushort2, ushort2);
/* UNSUPPORTED: ushort3 __min(ushort3, ushort3); */
ushort4 __min(ushort4, ushort4);
ushort8 __min(ushort8, ushort8);
ushort16 __min(ushort16, ushort16);
ushort32 __min(ushort32, ushort32);

/* VMINUW */
uint __min(uint, uint);
uint2 __min(uint2, uint2);
/* UNSUPPORTED: uint3 __min(uint3, uint3); */
uint4 __min(uint4, uint4);
uint8 __min(uint8, uint8);
uint16 __min(uint16, uint16);

/* VMINW */
int32_t __min(int32_t, int32_t);
int2 __min(int2, int2);
/* UNSUPPORTED: int3 __min(int3, int3); */
int4 __min(int4, int4);
int8 __min(int8, int8);
int16 __min(int16, int16);




/*-----------------------------------------------------------------------------
* ID: __min_index
*----------------------------------------------------------------------------*/

/* VMINPB */
void __min_index(int8_t, int8_t&, __vpred&);
void __min_index(char2, char2::EQUIV_ACCESS_T<0>&, __vpred&);
/* UNSUPPORTED: void __min_index(char3, char3::EQUIV_ACCESS_T<0>&, __vpred&); */
void __min_index(char4, char4::EQUIV_ACCESS_T<0>&, __vpred&);
void __min_index(char8, char8::EQUIV_ACCESS_T<0>&, __vpred&);
void __min_index(char16, char16::EQUIV_ACCESS_T<0>&, __vpred&);
void __min_index(char32, char32::EQUIV_ACCESS_T<0>&, __vpred&);
void __min_index(char64, char64::EQUIV_ACCESS_T<0>&, __vpred&);

/* VMINPD */
void __min_index(int64_t, int64_t&, __vpred&);
void __min_index(long2, long2::EQUIV_ACCESS_T<0>&, __vpred&);
/* UNSUPPORTED: void __min_index(long3, long3::EQUIV_ACCESS_T<0>&, __vpred&); */
void __min_index(long4, long4::EQUIV_ACCESS_T<0>&, __vpred&);
void __min_index(long8, long8::EQUIV_ACCESS_T<0>&, __vpred&);

/* VMINPH */
void __min_index(int16_t, int16_t&, __vpred&);
void __min_index(short2, short2::EQUIV_ACCESS_T<0>&, __vpred&);
/* UNSUPPORTED: void __min_index(short3, short3::EQUIV_ACCESS_T<0>&, __vpred&); */
void __min_index(short4, short4::EQUIV_ACCESS_T<0>&, __vpred&);
void __min_index(short8, short8::EQUIV_ACCESS_T<0>&, __vpred&);
void __min_index(short16, short16::EQUIV_ACCESS_T<0>&, __vpred&);
void __min_index(short32, short32::EQUIV_ACCESS_T<0>&, __vpred&);

/* VMINPW */
void __min_index(int32_t, int32_t&, __vpred&);
void __min_index(int2, int2::EQUIV_ACCESS_T<0>&, __vpred&);
/* UNSUPPORTED: void __min_index(int3, int3::EQUIV_ACCESS_T<0>&, __vpred&); */
void __min_index(int4, int4::EQUIV_ACCESS_T<0>&, __vpred&);
void __min_index(int8, int8::EQUIV_ACCESS_T<0>&, __vpred&);
void __min_index(int16, int16::EQUIV_ACCESS_T<0>&, __vpred&);

/* VMINUPB */
void __min_index(uchar, uchar&, __vpred&);
void __min_index(uchar2, uchar2::EQUIV_ACCESS_T<0>&, __vpred&);
/* UNSUPPORTED: void __min_index(uchar3, uchar3::EQUIV_ACCESS_T<0>&, __vpred&); */
void __min_index(uchar4, uchar4::EQUIV_ACCESS_T<0>&, __vpred&);
void __min_index(uchar8, uchar8::EQUIV_ACCESS_T<0>&, __vpred&);
void __min_index(uchar16, uchar16::EQUIV_ACCESS_T<0>&, __vpred&);
void __min_index(uchar32, uchar32::EQUIV_ACCESS_T<0>&, __vpred&);
void __min_index(uchar64, uchar64::EQUIV_ACCESS_T<0>&, __vpred&);

/* VMINUPD */
void __min_index(ulong, ulong&, __vpred&);
void __min_index(ulong2, ulong2::EQUIV_ACCESS_T<0>&, __vpred&);
/* UNSUPPORTED: void __min_index(ulong3, ulong3::EQUIV_ACCESS_T<0>&, __vpred&); */
void __min_index(ulong4, ulong4::EQUIV_ACCESS_T<0>&, __vpred&);
void __min_index(ulong8, ulong8::EQUIV_ACCESS_T<0>&, __vpred&);

/* VMINUPH */
void __min_index(ushort, ushort&, __vpred&);
void __min_index(ushort2, ushort2::EQUIV_ACCESS_T<0>&, __vpred&);
/* UNSUPPORTED: void __min_index(ushort3, ushort3::EQUIV_ACCESS_T<0>&, __vpred&); */
void __min_index(ushort4, ushort4::EQUIV_ACCESS_T<0>&, __vpred&);
void __min_index(ushort8, ushort8::EQUIV_ACCESS_T<0>&, __vpred&);
void __min_index(ushort16, ushort16::EQUIV_ACCESS_T<0>&, __vpred&);
void __min_index(ushort32, ushort32::EQUIV_ACCESS_T<0>&, __vpred&);

/* VMINUPW */
void __min_index(uint, uint&, __vpred&);
void __min_index(uint2, uint2::EQUIV_ACCESS_T<0>&, __vpred&);
/* UNSUPPORTED: void __min_index(uint3, uint3::EQUIV_ACCESS_T<0>&, __vpred&); */
void __min_index(uint4, uint4::EQUIV_ACCESS_T<0>&, __vpred&);
void __min_index(uint8, uint8::EQUIV_ACCESS_T<0>&, __vpred&);
void __min_index(uint16, uint16::EQUIV_ACCESS_T<0>&, __vpred&);




/*-----------------------------------------------------------------------------
* ID: __mod_operator
*----------------------------------------------------------------------------*/
/*

MODDW
long = long % int;

MODUDW
ulong = ulong % uint;

MODUW
uint = uint % uint;

MODW
int = int % int;

*/


/*-----------------------------------------------------------------------------
* ID: __moddw_drd
*----------------------------------------------------------------------------*/

/* MODDW */
int64_t __moddw_drd(int64_t, int32_t);




/*-----------------------------------------------------------------------------
* ID: __modudw_drd
*----------------------------------------------------------------------------*/

/* MODUDW */
ulong __modudw_drd(ulong, uint);




/*-----------------------------------------------------------------------------
* ID: __moduw_rrr
*----------------------------------------------------------------------------*/

/* MODUW */
uint __moduw_rrr(uint, uint);




/*-----------------------------------------------------------------------------
* ID: __modw_rrr
*----------------------------------------------------------------------------*/

/* MODW */
int32_t __modw_rrr(int32_t, int32_t);




/*-----------------------------------------------------------------------------
* ID: __mpy_ext
*----------------------------------------------------------------------------*/

/* MPYHW */
int32_t __mpy_ext(int16_t, int16_t);

/* MPYSUHW */
int32_t __mpy_ext(int16_t, ushort);

/* MPYUHW */
uint __mpy_ext(ushort, ushort);

/* VMPYHW */
int32_t __mpy_ext(int16_t, int16_t);
int2 __mpy_ext(short2, short2);
/* UNSUPPORTED: int3 __mpy_ext(short3, short3); */
int4 __mpy_ext(short4, short4);
int8 __mpy_ext(short8, short8);
int16 __mpy_ext(short16, short16);

/* VMPYSP2DP */
double __mpy_ext(float, float);
double2 __mpy_ext(float2, float2);
/* UNSUPPORTED: double3 __mpy_ext(float3, float3); */
double4 __mpy_ext(float4, float4);
double8 __mpy_ext(float8, float8);

/* VMPYSUBH */
int16_t __mpy_ext(int8_t, uchar);
short2 __mpy_ext(char2, uchar2);
/* UNSUPPORTED: short3 __mpy_ext(char3, uchar3); */
short4 __mpy_ext(char4, uchar4);
short8 __mpy_ext(char8, uchar8);
short16 __mpy_ext(char16, uchar16);
short32 __mpy_ext(char32, uchar32);

/* VMPYSUHW */
int32_t __mpy_ext(int16_t, ushort);
int2 __mpy_ext(short2, ushort2);
/* UNSUPPORTED: int3 __mpy_ext(short3, ushort3); */
int4 __mpy_ext(short4, ushort4);
int8 __mpy_ext(short8, ushort8);
int16 __mpy_ext(short16, ushort16);

/* VMPYSUWD */
int64_t __mpy_ext(int32_t, uint);
long2 __mpy_ext(int2, uint2);
/* UNSUPPORTED: long3 __mpy_ext(int3, uint3); */
long4 __mpy_ext(int4, uint4);
long8 __mpy_ext(int8, uint8);

/* VMPYUBH */
ushort __mpy_ext(uchar, uchar);
ushort2 __mpy_ext(uchar2, uchar2);
/* UNSUPPORTED: ushort3 __mpy_ext(uchar3, uchar3); */
ushort4 __mpy_ext(uchar4, uchar4);
ushort8 __mpy_ext(uchar8, uchar8);
ushort16 __mpy_ext(uchar16, uchar16);
ushort32 __mpy_ext(uchar32, uchar32);

/* VMPYUDQ */
ulong2 __mpy_ext(ulong, ulong);
ulong4 __mpy_ext(ulong2, ulong2);
ulong8 __mpy_ext(ulong4, ulong4);

/* VMPYUHW */
uint __mpy_ext(ushort, ushort);
uint2 __mpy_ext(ushort2, ushort2);
/* UNSUPPORTED: uint3 __mpy_ext(ushort3, ushort3); */
uint4 __mpy_ext(ushort4, ushort4);
uint8 __mpy_ext(ushort8, ushort8);
uint16 __mpy_ext(ushort16, ushort16);

/* VMPYUWD */
ulong __mpy_ext(uint, uint);
ulong2 __mpy_ext(uint2, uint2);
/* UNSUPPORTED: ulong3 __mpy_ext(uint3, uint3); */
ulong4 __mpy_ext(uint4, uint4);
ulong8 __mpy_ext(uint8, uint8);

/* VMPYWD */
int64_t __mpy_ext(int32_t, int32_t);
long2 __mpy_ext(int2, int2);
/* UNSUPPORTED: long3 __mpy_ext(int3, int3); */
long4 __mpy_ext(int4, int4);
long8 __mpy_ext(int8, int8);




/*-----------------------------------------------------------------------------
* ID: __mpy_fx
*----------------------------------------------------------------------------*/

/* C6MPY2IR */
int2 __mpy_fx(short2, int32_t);




/*-----------------------------------------------------------------------------
* ID: __mpy_sat
*----------------------------------------------------------------------------*/

/* VSMPYWW */
int32_t __mpy_sat(int32_t, int32_t);
int2 __mpy_sat(int2, int2);
/* UNSUPPORTED: int3 __mpy_sat(int3, int3); */
int4 __mpy_sat(int4, int4);
int8 __mpy_sat(int8, int8);
int16 __mpy_sat(int16, int16);




/*-----------------------------------------------------------------------------
* ID: __mpy_sat_ext
*----------------------------------------------------------------------------*/

/* SMPYHW */
int32_t __mpy_sat_ext(int16_t, int16_t);

/* VSMPYHW */
int32_t __mpy_sat_ext(int16_t, int16_t);
int2 __mpy_sat_ext(short2, short2);
/* UNSUPPORTED: int3 __mpy_sat_ext(short3, short3); */
int4 __mpy_sat_ext(short4, short4);
int8 __mpy_sat_ext(short8, short8);
int16 __mpy_sat_ext(short16, short16);




/*-----------------------------------------------------------------------------
* ID: __mpy_sat_fxq15
*----------------------------------------------------------------------------*/

/* VSMPYR1HH */
int16_t __mpy_sat_fxq15(int16_t, int16_t);
short2 __mpy_sat_fxq15(short2, short2);
/* UNSUPPORTED: short3 __mpy_sat_fxq15(short3, short3); */
short4 __mpy_sat_fxq15(short4, short4);
short8 __mpy_sat_fxq15(short8, short8);
short16 __mpy_sat_fxq15(short16, short16);
short32 __mpy_sat_fxq15(short32, short32);

/* VSMPYRSUHH */
int16_t __mpy_sat_fxq15(int16_t, ushort);
short2 __mpy_sat_fxq15(short2, ushort2);
/* UNSUPPORTED: short3 __mpy_sat_fxq15(short3, ushort3); */
short4 __mpy_sat_fxq15(short4, ushort4);
short8 __mpy_sat_fxq15(short8, ushort8);
short16 __mpy_sat_fxq15(short16, ushort16);
short32 __mpy_sat_fxq15(short32, ushort32);




/*-----------------------------------------------------------------------------
* ID: __mpy_sat_fxq16
*----------------------------------------------------------------------------*/

/* VSMPYRUHH */
ushort __mpy_sat_fxq16(ushort, ushort);
ushort2 __mpy_sat_fxq16(ushort2, ushort2);
/* UNSUPPORTED: ushort3 __mpy_sat_fxq16(ushort3, ushort3); */
ushort4 __mpy_sat_fxq16(ushort4, ushort4);
ushort8 __mpy_sat_fxq16(ushort8, ushort8);
ushort16 __mpy_sat_fxq16(ushort16, ushort16);
ushort32 __mpy_sat_fxq16(ushort32, ushort32);




/*-----------------------------------------------------------------------------
* ID: __mpy_sat_fxq16_15
*----------------------------------------------------------------------------*/

/* VSMPYR17WW */
int32_t __mpy_sat_fxq16_15(int32_t, int32_t);
int2 __mpy_sat_fxq16_15(int2, int2);
/* UNSUPPORTED: int3 __mpy_sat_fxq16_15(int3, int3); */
int4 __mpy_sat_fxq16_15(int4, int4);
int8 __mpy_sat_fxq16_15(int8, int8);
int16 __mpy_sat_fxq16_15(int16, int16);




/*-----------------------------------------------------------------------------
* ID: __mpy_sat_fxq31
*----------------------------------------------------------------------------*/

/* VSMPYR1WW */
int32_t __mpy_sat_fxq31(int32_t, int32_t);
int2 __mpy_sat_fxq31(int2, int2);
/* UNSUPPORTED: int3 __mpy_sat_fxq31(int3, int3); */
int4 __mpy_sat_fxq31(int4, int4);
int8 __mpy_sat_fxq31(int8, int8);
int16 __mpy_sat_fxq31(int16, int16);




/*-----------------------------------------------------------------------------
* ID: __mpy_xor
*----------------------------------------------------------------------------*/

/* VXORMPYW */
uint __mpy_xor(uint, uint);
uint2 __mpy_xor(uint2, uint2);
/* UNSUPPORTED: uint3 __mpy_xor(uint3, uint3); */
uint4 __mpy_xor(uint4, uint4);
uint8 __mpy_xor(uint8, uint8);
uint16 __mpy_xor(uint16, uint16);




/*-----------------------------------------------------------------------------
* ID: __mpydd_ddd
*----------------------------------------------------------------------------*/

/* MPYDD */
int64_t __mpydd_ddd(int64_t, int64_t);
ulong __mpydd_ddd(ulong, ulong);




/*-----------------------------------------------------------------------------
* ID: __mpydp_ddd
*----------------------------------------------------------------------------*/

/* MPYDP */
double __mpydp_ddd(double, double);




/*-----------------------------------------------------------------------------
* ID: __mpyhw_rrr
*----------------------------------------------------------------------------*/

/* MPYHW */
int32_t __mpyhw_rrr(int16_t, int16_t);




/*-----------------------------------------------------------------------------
* ID: __mpysp_rrr
*----------------------------------------------------------------------------*/

/* MPYSP */
float __mpysp_rrr(float, float);




/*-----------------------------------------------------------------------------
* ID: __mpysuhw_rrr
*----------------------------------------------------------------------------*/

/* MPYSUHW */
int32_t __mpysuhw_rrr(int16_t, ushort);




/*-----------------------------------------------------------------------------
* ID: __mpyuhw_rrr
*----------------------------------------------------------------------------*/

/* MPYUHW */
uint __mpyuhw_rrr(ushort, ushort);




/*-----------------------------------------------------------------------------
* ID: __mpyww_rrr
*----------------------------------------------------------------------------*/

/* MPYWW */
int32_t __mpyww_rrr(int32_t, int32_t);
uint __mpyww_rrr(uint, uint);




/*-----------------------------------------------------------------------------
* ID: __mult_operator
*----------------------------------------------------------------------------*/
/*

MPYDD
long = long * long;
ulong = ulong * ulong;

MPYDP
double = double * double;

MPYSP
float = float * float;

MPYWW
int = int * int;
uint = uint * uint;

VMPYBB
char = char * char;
char2 = char2 * char2;
char3 = char3 * char3;
char4 = char4 * char4;
char8 = char8 * char8;
char16 = char16 * char16;
char32 = char32 * char32;
char64 = char64 * char64;
uchar = uchar * uchar;
uchar2 = uchar2 * uchar2;
uchar3 = uchar3 * uchar3;
uchar4 = uchar4 * uchar4;
uchar8 = uchar8 * uchar8;
uchar16 = uchar16 * uchar16;
uchar32 = uchar32 * uchar32;
uchar64 = uchar64 * uchar64;

VMPYDD
long = long * long;
long2 = long2 * long2;
long3 = long3 * long3;
long4 = long4 * long4;
long8 = long8 * long8;
ulong = ulong * ulong;
ulong2 = ulong2 * ulong2;
ulong3 = ulong3 * ulong3;
ulong4 = ulong4 * ulong4;
ulong8 = ulong8 * ulong8;

VMPYDP
double = double * double;
double2 = double2 * double2;
double3 = double3 * double3;
double4 = double4 * double4;
double8 = double8 * double8;

VMPYHH
short = short * short;
short2 = short2 * short2;
short3 = short3 * short3;
short4 = short4 * short4;
short8 = short8 * short8;
short16 = short16 * short16;
short32 = short32 * short32;
ushort = ushort * ushort;
ushort2 = ushort2 * ushort2;
ushort3 = ushort3 * ushort3;
ushort4 = ushort4 * ushort4;
ushort8 = ushort8 * ushort8;
ushort16 = ushort16 * ushort16;
ushort32 = ushort32 * ushort32;

VMPYSP
float = float * float;
float2 = float2 * float2;
float3 = float3 * float3;
float4 = float4 * float4;
float8 = float8 * float8;
float16 = float16 * float16;

VMPYWW
int = int * int;
int2 = int2 * int2;
int3 = int3 * int3;
int4 = int4 * int4;
int8 = int8 * int8;
int16 = int16 * int16;
uint = uint * uint;
uint2 = uint2 * uint2;
uint3 = uint3 * uint3;
uint4 = uint4 * uint4;
uint8 = uint8 * uint8;
uint16 = uint16 * uint16;

*/


/*-----------------------------------------------------------------------------
* ID: __nand
*----------------------------------------------------------------------------*/

/* NAND */
__vpred __nand(__vpred, __vpred);

/* NANDD */
int8_t __nand(int8_t, int8_t);
char2 __nand(char2, char2);
/* UNSUPPORTED: char3 __nand(char3, char3); */
char4 __nand(char4, char4);
char8 __nand(char8, char8);
int16_t __nand(int16_t, int16_t);
short2 __nand(short2, short2);
/* UNSUPPORTED: short3 __nand(short3, short3); */
short4 __nand(short4, short4);
int32_t __nand(int32_t, int32_t);
int2 __nand(int2, int2);
int64_t __nand(int64_t, int64_t);
/* CONSTANT: int64_t __nand(int64_t, (int64_t)(k)); */
uchar __nand(uchar, uchar);
uchar2 __nand(uchar2, uchar2);
/* UNSUPPORTED: uchar3 __nand(uchar3, uchar3); */
uchar4 __nand(uchar4, uchar4);
uchar8 __nand(uchar8, uchar8);
ushort __nand(ushort, ushort);
ushort2 __nand(ushort2, ushort2);
/* UNSUPPORTED: ushort3 __nand(ushort3, ushort3); */
ushort4 __nand(ushort4, ushort4);
uint __nand(uint, uint);
uint2 __nand(uint2, uint2);
ulong __nand(ulong, ulong);
/* CONSTANT: ulong __nand(ulong, (ulong)(k)); */

/* NANDW */
int8_t __nand(int8_t, int8_t);
char2 __nand(char2, char2);
/* UNSUPPORTED: char3 __nand(char3, char3); */
char4 __nand(char4, char4);
int16_t __nand(int16_t, int16_t);
short2 __nand(short2, short2);
int32_t __nand(int32_t, int32_t);
uchar __nand(uchar, uchar);
uchar2 __nand(uchar2, uchar2);
/* UNSUPPORTED: uchar3 __nand(uchar3, uchar3); */
uchar4 __nand(uchar4, uchar4);
ushort __nand(ushort, ushort);
ushort2 __nand(ushort2, ushort2);
uint __nand(uint, uint);

/* VNANDW */
int8_t __nand(int8_t, int8_t);
char2 __nand(char2, char2);
/* UNSUPPORTED: char3 __nand(char3, char3); */
char4 __nand(char4, char4);
char8 __nand(char8, char8);
char16 __nand(char16, char16);
char32 __nand(char32, char32);
char64 __nand(char64, char64);
int16_t __nand(int16_t, int16_t);
short2 __nand(short2, short2);
/* UNSUPPORTED: short3 __nand(short3, short3); */
short4 __nand(short4, short4);
short8 __nand(short8, short8);
short16 __nand(short16, short16);
short32 __nand(short32, short32);
int32_t __nand(int32_t, int32_t);
int2 __nand(int2, int2);
/* UNSUPPORTED: int3 __nand(int3, int3); */
int4 __nand(int4, int4);
int8 __nand(int8, int8);
int16 __nand(int16, int16);
int64_t __nand(int64_t, int64_t);
long2 __nand(long2, long2);
/* UNSUPPORTED: long3 __nand(long3, long3); */
long4 __nand(long4, long4);
long8 __nand(long8, long8);
uchar __nand(uchar, uchar);
uchar2 __nand(uchar2, uchar2);
/* UNSUPPORTED: uchar3 __nand(uchar3, uchar3); */
uchar4 __nand(uchar4, uchar4);
uchar8 __nand(uchar8, uchar8);
uchar16 __nand(uchar16, uchar16);
uchar32 __nand(uchar32, uchar32);
uchar64 __nand(uchar64, uchar64);
ushort __nand(ushort, ushort);
ushort2 __nand(ushort2, ushort2);
/* UNSUPPORTED: ushort3 __nand(ushort3, ushort3); */
ushort4 __nand(ushort4, ushort4);
ushort8 __nand(ushort8, ushort8);
ushort16 __nand(ushort16, ushort16);
ushort32 __nand(ushort32, ushort32);
uint __nand(uint, uint);
uint2 __nand(uint2, uint2);
/* UNSUPPORTED: uint3 __nand(uint3, uint3); */
uint4 __nand(uint4, uint4);
uint8 __nand(uint8, uint8);
uint16 __nand(uint16, uint16);
ulong __nand(ulong, ulong);
ulong2 __nand(ulong2, ulong2);
/* UNSUPPORTED: ulong3 __nand(ulong3, ulong3); */
ulong4 __nand(ulong4, ulong4);
ulong8 __nand(ulong8, ulong8);




/*-----------------------------------------------------------------------------
* ID: __nand_ppp
*----------------------------------------------------------------------------*/

/* NAND */
__vpred __nand_ppp(__vpred, __vpred);




/*-----------------------------------------------------------------------------
* ID: __nandd_ddd
*----------------------------------------------------------------------------*/

/* NANDD */
char8 __nandd_ddd(char8, char8);
short4 __nandd_ddd(short4, short4);
int2 __nandd_ddd(int2, int2);
int64_t __nandd_ddd(int64_t, int64_t);
uchar8 __nandd_ddd(uchar8, uchar8);
ushort4 __nandd_ddd(ushort4, ushort4);
uint2 __nandd_ddd(uint2, uint2);
ulong __nandd_ddd(ulong, ulong);




/*-----------------------------------------------------------------------------
* ID: __nandd_dkd
*----------------------------------------------------------------------------*/

/* NANDD */
int64_t __nandd_dkd(int64_t, int64_t);
ulong __nandd_dkd(ulong, ulong);




/*-----------------------------------------------------------------------------
* ID: __nandw_rrr
*----------------------------------------------------------------------------*/

/* NANDW */
char4 __nandw_rrr(char4, char4);
short2 __nandw_rrr(short2, short2);
int32_t __nandw_rrr(int32_t, int32_t);
uchar4 __nandw_rrr(uchar4, uchar4);
ushort2 __nandw_rrr(ushort2, ushort2);
uint __nandw_rrr(uint, uint);




/*-----------------------------------------------------------------------------
* ID: __negate
*----------------------------------------------------------------------------*/

/* NOT */
__vpred __negate(__vpred);




/*-----------------------------------------------------------------------------
* ID: __nor
*----------------------------------------------------------------------------*/

/* NOR */
__vpred __nor(__vpred, __vpred);

/* NORD */
int8_t __nor(int8_t, int8_t);
char2 __nor(char2, char2);
/* UNSUPPORTED: char3 __nor(char3, char3); */
char4 __nor(char4, char4);
char8 __nor(char8, char8);
int16_t __nor(int16_t, int16_t);
short2 __nor(short2, short2);
/* UNSUPPORTED: short3 __nor(short3, short3); */
short4 __nor(short4, short4);
int32_t __nor(int32_t, int32_t);
int2 __nor(int2, int2);
int64_t __nor(int64_t, int64_t);
/* CONSTANT: int64_t __nor(int64_t, (int64_t)(k)); */
uchar __nor(uchar, uchar);
uchar2 __nor(uchar2, uchar2);
/* UNSUPPORTED: uchar3 __nor(uchar3, uchar3); */
uchar4 __nor(uchar4, uchar4);
uchar8 __nor(uchar8, uchar8);
ushort __nor(ushort, ushort);
ushort2 __nor(ushort2, ushort2);
/* UNSUPPORTED: ushort3 __nor(ushort3, ushort3); */
ushort4 __nor(ushort4, ushort4);
uint __nor(uint, uint);
uint2 __nor(uint2, uint2);
ulong __nor(ulong, ulong);
/* CONSTANT: ulong __nor(ulong, (ulong)(k)); */

/* NORW */
int8_t __nor(int8_t, int8_t);
char2 __nor(char2, char2);
/* UNSUPPORTED: char3 __nor(char3, char3); */
char4 __nor(char4, char4);
int16_t __nor(int16_t, int16_t);
short2 __nor(short2, short2);
int32_t __nor(int32_t, int32_t);
uchar __nor(uchar, uchar);
uchar2 __nor(uchar2, uchar2);
/* UNSUPPORTED: uchar3 __nor(uchar3, uchar3); */
uchar4 __nor(uchar4, uchar4);
ushort __nor(ushort, ushort);
ushort2 __nor(ushort2, ushort2);
uint __nor(uint, uint);

/* VNORW */
int8_t __nor(int8_t, int8_t);
char2 __nor(char2, char2);
/* UNSUPPORTED: char3 __nor(char3, char3); */
char4 __nor(char4, char4);
char8 __nor(char8, char8);
char16 __nor(char16, char16);
char32 __nor(char32, char32);
char64 __nor(char64, char64);
int16_t __nor(int16_t, int16_t);
short2 __nor(short2, short2);
/* UNSUPPORTED: short3 __nor(short3, short3); */
short4 __nor(short4, short4);
short8 __nor(short8, short8);
short16 __nor(short16, short16);
short32 __nor(short32, short32);
int32_t __nor(int32_t, int32_t);
int2 __nor(int2, int2);
/* UNSUPPORTED: int3 __nor(int3, int3); */
int4 __nor(int4, int4);
int8 __nor(int8, int8);
int16 __nor(int16, int16);
int64_t __nor(int64_t, int64_t);
long2 __nor(long2, long2);
/* UNSUPPORTED: long3 __nor(long3, long3); */
long4 __nor(long4, long4);
long8 __nor(long8, long8);




/*-----------------------------------------------------------------------------
* ID: __nor_ppp
*----------------------------------------------------------------------------*/

/* NOR */
__vpred __nor_ppp(__vpred, __vpred);




/*-----------------------------------------------------------------------------
* ID: __nord_ddd
*----------------------------------------------------------------------------*/

/* NORD */
char8 __nord_ddd(char8, char8);
short4 __nord_ddd(short4, short4);
int2 __nord_ddd(int2, int2);
int64_t __nord_ddd(int64_t, int64_t);
uchar8 __nord_ddd(uchar8, uchar8);
ushort4 __nord_ddd(ushort4, ushort4);
uint2 __nord_ddd(uint2, uint2);
ulong __nord_ddd(ulong, ulong);




/*-----------------------------------------------------------------------------
* ID: __nord_dkd
*----------------------------------------------------------------------------*/

/* NORD */
int64_t __nord_dkd(int64_t, int64_t);
ulong __nord_dkd(ulong, ulong);




/*-----------------------------------------------------------------------------
* ID: __norm
*----------------------------------------------------------------------------*/

/* VNORMB */
int8_t __norm(int8_t);
char2 __norm(char2);
/* UNSUPPORTED: char3 __norm(char3); */
char4 __norm(char4);
char8 __norm(char8);
char16 __norm(char16);
char32 __norm(char32);
char64 __norm(char64);

/* VNORMD */
int64_t __norm(int64_t);
long2 __norm(long2);
/* UNSUPPORTED: long3 __norm(long3); */
long4 __norm(long4);
long8 __norm(long8);

/* VNORMH */
int16_t __norm(int16_t);
short2 __norm(short2);
/* UNSUPPORTED: short3 __norm(short3); */
short4 __norm(short4);
short8 __norm(short8);
short16 __norm(short16);
short32 __norm(short32);

/* VNORMW */
int32_t __norm(int32_t);
int2 __norm(int2);
/* UNSUPPORTED: int3 __norm(int3); */
int4 __norm(int4);
int8 __norm(int8);
int16 __norm(int16);




/*-----------------------------------------------------------------------------
* ID: __norm_and_shift_elem_pair
*----------------------------------------------------------------------------*/

/* VNORM2UH */
ushort2 __norm_and_shift_elem_pair(ushort2);
ushort4 __norm_and_shift_elem_pair(ushort4);
ushort8 __norm_and_shift_elem_pair(ushort8);
ushort16 __norm_and_shift_elem_pair(ushort16);
ushort32 __norm_and_shift_elem_pair(ushort32);

/* VNORM2UW */
uint2 __norm_and_shift_elem_pair(uint2);
uint4 __norm_and_shift_elem_pair(uint4);
uint8 __norm_and_shift_elem_pair(uint8);
uint16 __norm_and_shift_elem_pair(uint16);




/*-----------------------------------------------------------------------------
* ID: __norw_rrr
*----------------------------------------------------------------------------*/

/* NORW */
char4 __norw_rrr(char4, char4);
short2 __norw_rrr(short2, short2);
int32_t __norw_rrr(int32_t, int32_t);
uchar4 __norw_rrr(uchar4, uchar4);
ushort2 __norw_rrr(ushort2, ushort2);
uint __norw_rrr(uint, uint);




/*-----------------------------------------------------------------------------
* ID: __not_pp
*----------------------------------------------------------------------------*/

/* NOT */
__vpred __not_pp(__vpred);




/*-----------------------------------------------------------------------------
* ID: __or
*----------------------------------------------------------------------------*/

/* OR */
__vpred __or(__vpred, __vpred);




/*-----------------------------------------------------------------------------
* ID: __or_ppp
*----------------------------------------------------------------------------*/

/* OR */
__vpred __or_ppp(__vpred, __vpred);




/*-----------------------------------------------------------------------------
* ID: __orb_operator
*----------------------------------------------------------------------------*/
/*

ORD
char = char | char;
char2 = char2 | char2;
char3 = char3 | char3;
char4 = char4 | char4;
char8 = char8 | char8;
short = short | short;
short2 = short2 | short2;
short3 = short3 | short3;
short4 = short4 | short4;
int = int | int;
int2 = int2 | int2;
long = long | long;
long = long | (long)(k);
uchar = uchar | uchar;
uchar2 = uchar2 | uchar2;
uchar3 = uchar3 | uchar3;
uchar4 = uchar4 | uchar4;
uchar8 = uchar8 | uchar8;
ushort = ushort | ushort;
ushort2 = ushort2 | ushort2;
ushort3 = ushort3 | ushort3;
ushort4 = ushort4 | ushort4;
uint = uint | uint;
uint2 = uint2 | uint2;
ulong = ulong | ulong;
ulong = ulong | (ulong)(k);

ORW
char = char | char;
char2 = char2 | char2;
char3 = char3 | char3;
char4 = char4 | char4;
short = short | short;
short2 = short2 | short2;
int = int | int;
char = char | (char)(k);
char2 = char2 | (char2)(k);
char3 = char3 | (char3)(k);
char4 = char4 | (char4)(k);
short = short | (short)(k);
short2 = short2 | (short2)(k);
int = int | (int)(k);
uchar = uchar | uchar;
uchar2 = uchar2 | uchar2;
uchar3 = uchar3 | uchar3;
uchar4 = uchar4 | uchar4;
ushort = ushort | ushort;
ushort2 = ushort2 | ushort2;
uint = uint | uint;
uchar = uchar | (uchar)(k);
uchar2 = uchar2 | (uchar2)(k);
uchar3 = uchar3 | (uchar3)(k);
uchar4 = uchar4 | (uchar4)(k);
ushort = ushort | (ushort)(k);
ushort2 = ushort2 | (ushort2)(k);
uint = uint | (uint)(k);

VORW
char = char | char;
char2 = char2 | char2;
char3 = char3 | char3;
char4 = char4 | char4;
char8 = char8 | char8;
char16 = char16 | char16;
char32 = char32 | char32;
char64 = char64 | char64;
short = short | short;
short2 = short2 | short2;
short3 = short3 | short3;
short4 = short4 | short4;
short8 = short8 | short8;
short16 = short16 | short16;
short32 = short32 | short32;
int = int | int;
int2 = int2 | int2;
int3 = int3 | int3;
int4 = int4 | int4;
int8 = int8 | int8;
int16 = int16 | int16;
long = long | long;
long2 = long2 | long2;
long3 = long3 | long3;
long4 = long4 | long4;
long8 = long8 | long8;
int = int | (int)(k);
int2 = int2 | (int2)(k);
int3 = int3 | (int3)(k);
int4 = int4 | (int4)(k);
int8 = int8 | (int8)(k);
int16 = int16 | (int16)(k);
uchar = uchar | uchar;
uchar2 = uchar2 | uchar2;
uchar3 = uchar3 | uchar3;
uchar4 = uchar4 | uchar4;
uchar8 = uchar8 | uchar8;
uchar16 = uchar16 | uchar16;
uchar32 = uchar32 | uchar32;
uchar64 = uchar64 | uchar64;
ushort = ushort | ushort;
ushort2 = ushort2 | ushort2;
ushort3 = ushort3 | ushort3;
ushort4 = ushort4 | ushort4;
ushort8 = ushort8 | ushort8;
ushort16 = ushort16 | ushort16;
ushort32 = ushort32 | ushort32;
uint = uint | uint;
uint2 = uint2 | uint2;
uint3 = uint3 | uint3;
uint4 = uint4 | uint4;
uint8 = uint8 | uint8;
uint16 = uint16 | uint16;
ulong = ulong | ulong;
ulong2 = ulong2 | ulong2;
ulong3 = ulong3 | ulong3;
ulong4 = ulong4 | ulong4;
ulong8 = ulong8 | ulong8;
uint = uint | (uint)(k);
uint2 = uint2 | (uint2)(k);
uint3 = uint3 | (uint3)(k);
uint4 = uint4 | (uint4)(k);
uint8 = uint8 | (uint8)(k);
uint16 = uint16 | (uint16)(k);

*/


/*-----------------------------------------------------------------------------
* ID: __ord_ddd
*----------------------------------------------------------------------------*/

/* ORD */
char8 __ord_ddd(char8, char8);
short4 __ord_ddd(short4, short4);
int2 __ord_ddd(int2, int2);
int64_t __ord_ddd(int64_t, int64_t);
uchar8 __ord_ddd(uchar8, uchar8);
ushort4 __ord_ddd(ushort4, ushort4);
uint2 __ord_ddd(uint2, uint2);
ulong __ord_ddd(ulong, ulong);




/*-----------------------------------------------------------------------------
* ID: __ord_dkd
*----------------------------------------------------------------------------*/

/* ORD */
int64_t __ord_dkd(int64_t, int64_t);
ulong __ord_dkd(ulong, ulong);




/*-----------------------------------------------------------------------------
* ID: __orn
*----------------------------------------------------------------------------*/

/* ORN */
__vpred __orn(__vpred, __vpred);

/* ORND */
int8_t __orn(int8_t, int8_t);
char2 __orn(char2, char2);
/* UNSUPPORTED: char3 __orn(char3, char3); */
char4 __orn(char4, char4);
char8 __orn(char8, char8);
int16_t __orn(int16_t, int16_t);
short2 __orn(short2, short2);
/* UNSUPPORTED: short3 __orn(short3, short3); */
short4 __orn(short4, short4);
int32_t __orn(int32_t, int32_t);
int2 __orn(int2, int2);
int64_t __orn(int64_t, int64_t);
/* CONSTANT: int64_t __orn(int64_t, (int64_t)(k)); */
uchar __orn(uchar, uchar);
uchar2 __orn(uchar2, uchar2);
/* UNSUPPORTED: uchar3 __orn(uchar3, uchar3); */
uchar4 __orn(uchar4, uchar4);
uchar8 __orn(uchar8, uchar8);
ushort __orn(ushort, ushort);
ushort2 __orn(ushort2, ushort2);
/* UNSUPPORTED: ushort3 __orn(ushort3, ushort3); */
ushort4 __orn(ushort4, ushort4);
uint __orn(uint, uint);
uint2 __orn(uint2, uint2);
ulong __orn(ulong, ulong);
/* CONSTANT: ulong __orn(ulong, (ulong)(k)); */

/* ORNW */
int8_t __orn(int8_t, int8_t);
char2 __orn(char2, char2);
/* UNSUPPORTED: char3 __orn(char3, char3); */
char4 __orn(char4, char4);
int16_t __orn(int16_t, int16_t);
short2 __orn(short2, short2);
int32_t __orn(int32_t, int32_t);
/* CONSTANT: int8_t __orn(int8_t, (int8_t)(k)); */
/* CONSTANT: char2 __orn(char2, (char2)(k)); */
/* UNSUPPORTED: char3 __orn(char3, (char3)(k)); */
/* CONSTANT: char4 __orn(char4, (char4)(k)); */
/* CONSTANT: int16_t __orn(int16_t, (int16_t)(k)); */
/* CONSTANT: short2 __orn(short2, (short2)(k)); */
/* CONSTANT: int32_t __orn(int32_t, (int32_t)(k)); */
uchar __orn(uchar, uchar);
uchar2 __orn(uchar2, uchar2);
/* UNSUPPORTED: uchar3 __orn(uchar3, uchar3); */
uchar4 __orn(uchar4, uchar4);
ushort __orn(ushort, ushort);
ushort2 __orn(ushort2, ushort2);
uint __orn(uint, uint);
/* CONSTANT: uchar __orn(uchar, (uchar)(k)); */
/* CONSTANT: uchar2 __orn(uchar2, (uchar2)(k)); */
/* UNSUPPORTED: uchar3 __orn(uchar3, (uchar3)(k)); */
/* CONSTANT: uchar4 __orn(uchar4, (uchar4)(k)); */
/* CONSTANT: ushort __orn(ushort, (ushort)(k)); */
/* CONSTANT: ushort2 __orn(ushort2, (ushort2)(k)); */
/* CONSTANT: uint __orn(uint, (uint)(k)); */

/* VORNW */
int8_t __orn(int8_t, int8_t);
char2 __orn(char2, char2);
/* UNSUPPORTED: char3 __orn(char3, char3); */
char4 __orn(char4, char4);
char8 __orn(char8, char8);
char16 __orn(char16, char16);
char32 __orn(char32, char32);
char64 __orn(char64, char64);
int16_t __orn(int16_t, int16_t);
short2 __orn(short2, short2);
/* UNSUPPORTED: short3 __orn(short3, short3); */
short4 __orn(short4, short4);
short8 __orn(short8, short8);
short16 __orn(short16, short16);
short32 __orn(short32, short32);
int32_t __orn(int32_t, int32_t);
int2 __orn(int2, int2);
/* UNSUPPORTED: int3 __orn(int3, int3); */
int4 __orn(int4, int4);
int8 __orn(int8, int8);
int16 __orn(int16, int16);
int64_t __orn(int64_t, int64_t);
long2 __orn(long2, long2);
/* UNSUPPORTED: long3 __orn(long3, long3); */
long4 __orn(long4, long4);
long8 __orn(long8, long8);
/* CONSTANT: int32_t __orn(int32_t, (int32_t)(k)); */
/* CONSTANT: int2 __orn(int2, (int2)(k)); */
/* UNSUPPORTED: int3 __orn(int3, (int3)(k)); */
/* CONSTANT: int4 __orn(int4, (int4)(k)); */
/* CONSTANT: int8 __orn(int8, (int8)(k)); */
/* CONSTANT: int16 __orn(int16, (int16)(k)); */




/*-----------------------------------------------------------------------------
* ID: __orn_ppp
*----------------------------------------------------------------------------*/

/* ORN */
__vpred __orn_ppp(__vpred, __vpred);




/*-----------------------------------------------------------------------------
* ID: __ornd_ddd
*----------------------------------------------------------------------------*/

/* ORND */
char8 __ornd_ddd(char8, char8);
short4 __ornd_ddd(short4, short4);
int2 __ornd_ddd(int2, int2);
int64_t __ornd_ddd(int64_t, int64_t);
uchar8 __ornd_ddd(uchar8, uchar8);
ushort4 __ornd_ddd(ushort4, ushort4);
uint2 __ornd_ddd(uint2, uint2);
ulong __ornd_ddd(ulong, ulong);




/*-----------------------------------------------------------------------------
* ID: __ornd_dkd
*----------------------------------------------------------------------------*/

/* ORND */
int64_t __ornd_dkd(int64_t, int64_t);
ulong __ornd_dkd(ulong, ulong);




/*-----------------------------------------------------------------------------
* ID: __ornw_rkr
*----------------------------------------------------------------------------*/

/* ORNW */
char4 __ornw_rkr(char4, char4);
short2 __ornw_rkr(short2, short2);
int32_t __ornw_rkr(int32_t, int32_t);
uchar4 __ornw_rkr(uchar4, uchar4);
ushort2 __ornw_rkr(ushort2, ushort2);
uint __ornw_rkr(uint, uint);




/*-----------------------------------------------------------------------------
* ID: __ornw_rrr
*----------------------------------------------------------------------------*/

/* ORNW */
char4 __ornw_rrr(char4, char4);
short2 __ornw_rrr(short2, short2);
int32_t __ornw_rrr(int32_t, int32_t);
uchar4 __ornw_rrr(uchar4, uchar4);
ushort2 __ornw_rrr(ushort2, ushort2);
uint __ornw_rrr(uint, uint);




/*-----------------------------------------------------------------------------
* ID: __orw_rkr
*----------------------------------------------------------------------------*/

/* ORW */
char4 __orw_rkr(char4, char4);
short2 __orw_rkr(short2, short2);
int32_t __orw_rkr(int32_t, int32_t);
uchar4 __orw_rkr(uchar4, uchar4);
ushort2 __orw_rkr(ushort2, ushort2);
uint __orw_rkr(uint, uint);




/*-----------------------------------------------------------------------------
* ID: __orw_rrr
*----------------------------------------------------------------------------*/

/* ORW */
char4 __orw_rrr(char4, char4);
short2 __orw_rrr(short2, short2);
int32_t __orw_rrr(int32_t, int32_t);
uchar4 __orw_rrr(uchar4, uchar4);
ushort2 __orw_rrr(ushort2, ushort2);
uint __orw_rrr(uint, uint);




/*-----------------------------------------------------------------------------
* ID: __outer_product_matmpy
*----------------------------------------------------------------------------*/

/* VOPMATMPYSP */
float4 __outer_product_matmpy(float2, float2);
float8 __outer_product_matmpy(float4, float4);
float16 __outer_product_matmpy(float8, float8);




/*-----------------------------------------------------------------------------
* ID: __pack
*----------------------------------------------------------------------------*/

/* PACKW */
int2 __pack(int32_t, int32_t);
float2 __pack(float, float);
uint2 __pack(uint, uint);
cshort2 __pack(cshort, cshort);




/*-----------------------------------------------------------------------------
* ID: __pack_consec_high
*----------------------------------------------------------------------------*/

/* VBPACKH */
char64 __pack_consec_high(char64, char64);
uchar64 __pack_consec_high(uchar64, uchar64);

/* VDPACKH */
long8 __pack_consec_high(long8, long8);
double8 __pack_consec_high(double8, double8);
cfloat8 __pack_consec_high(cfloat8, cfloat8);
ulong8 __pack_consec_high(ulong8, ulong8);
cint8 __pack_consec_high(cint8, cint8);

/* VHPACKH */
short32 __pack_consec_high(short32, short32);
ushort32 __pack_consec_high(ushort32, ushort32);
cchar32 __pack_consec_high(cchar32, cchar32);

/* VWPACKH */
int16 __pack_consec_high(int16, int16);
float16 __pack_consec_high(float16, float16);
uint16 __pack_consec_high(uint16, uint16);
cshort16 __pack_consec_high(cshort16, cshort16);




/*-----------------------------------------------------------------------------
* ID: __pack_consec_low
*----------------------------------------------------------------------------*/

/* VBPACKL */
char64 __pack_consec_low(char64, char64);
uchar64 __pack_consec_low(uchar64, uchar64);

/* VDPACKL */
long8 __pack_consec_low(long8, long8);
double8 __pack_consec_low(double8, double8);
cfloat8 __pack_consec_low(cfloat8, cfloat8);
ulong8 __pack_consec_low(ulong8, ulong8);
cint8 __pack_consec_low(cint8, cint8);

/* VHPACKL */
short32 __pack_consec_low(short32, short32);
ushort32 __pack_consec_low(ushort32, ushort32);
cchar32 __pack_consec_low(cchar32, cchar32);

/* VWPACKL */
int16 __pack_consec_low(int16, int16);
float16 __pack_consec_low(float16, float16);
uint16 __pack_consec_low(uint16, uint16);
cshort16 __pack_consec_low(cshort16, cshort16);




/*-----------------------------------------------------------------------------
* ID: __pack_even
*----------------------------------------------------------------------------*/

/* VPACKP2 */
short4 __pack_even(short4, short4);
short8 __pack_even(short8, short8);
short16 __pack_even(short16, short16);
short32 __pack_even(short32, short32);
ushort4 __pack_even(ushort4, ushort4);
ushort8 __pack_even(ushort8, ushort8);
ushort16 __pack_even(ushort16, ushort16);
ushort32 __pack_even(ushort32, ushort32);
cchar4 __pack_even(cchar4, cchar4);
cchar8 __pack_even(cchar8, cchar8);
cchar16 __pack_even(cchar16, cchar16);
cchar32 __pack_even(cchar32, cchar32);




/*-----------------------------------------------------------------------------
* ID: __pack_even_cross
*----------------------------------------------------------------------------*/

/* PACKX2 */
short4 __pack_even_cross(short2, short2);
ushort4 __pack_even_cross(ushort2, ushort2);
cchar4 __pack_even_cross(cchar2, cchar2);

/* VPACKX2 */
short4 __pack_even_cross(short4, short4);
short8 __pack_even_cross(short8, short8);
short16 __pack_even_cross(short16, short16);
short32 __pack_even_cross(short32, short32);
ushort4 __pack_even_cross(ushort4, ushort4);
ushort8 __pack_even_cross(ushort8, ushort8);
ushort16 __pack_even_cross(ushort16, ushort16);
ushort32 __pack_even_cross(ushort32, ushort32);
cchar4 __pack_even_cross(cchar4, cchar4);
cchar8 __pack_even_cross(cchar8, cchar8);
cchar16 __pack_even_cross(cchar16, cchar16);
cchar32 __pack_even_cross(cchar32, cchar32);




/*-----------------------------------------------------------------------------
* ID: __pack_high
*----------------------------------------------------------------------------*/

/* VPACKH2 */
short2 __pack_high(short2, short2);
short4 __pack_high(short4, short4);
short8 __pack_high(short8, short8);
short16 __pack_high(short16, short16);
short32 __pack_high(short32, short32);
ushort2 __pack_high(ushort2, ushort2);
ushort4 __pack_high(ushort4, ushort4);
ushort8 __pack_high(ushort8, ushort8);
ushort16 __pack_high(ushort16, ushort16);
ushort32 __pack_high(ushort32, ushort32);
cchar2 __pack_high(cchar2, cchar2);
cchar4 __pack_high(cchar4, cchar4);
cchar8 __pack_high(cchar8, cchar8);
cchar16 __pack_high(cchar16, cchar16);
cchar32 __pack_high(cchar32, cchar32);

/* VPACKH4 */
char4 __pack_high(char4, char4);
char8 __pack_high(char8, char8);
char16 __pack_high(char16, char16);
char32 __pack_high(char32, char32);
char64 __pack_high(char64, char64);
uchar4 __pack_high(uchar4, uchar4);
uchar8 __pack_high(uchar8, uchar8);
uchar16 __pack_high(uchar16, uchar16);
uchar32 __pack_high(uchar32, uchar32);
uchar64 __pack_high(uchar64, uchar64);




/*-----------------------------------------------------------------------------
* ID: __pack_high_low
*----------------------------------------------------------------------------*/

/* VPACKHL2 */
short2 __pack_high_low(short2, short2);
short4 __pack_high_low(short4, short4);
short8 __pack_high_low(short8, short8);
short16 __pack_high_low(short16, short16);
short32 __pack_high_low(short32, short32);
ushort2 __pack_high_low(ushort2, ushort2);
ushort4 __pack_high_low(ushort4, ushort4);
ushort8 __pack_high_low(ushort8, ushort8);
ushort16 __pack_high_low(ushort16, ushort16);
ushort32 __pack_high_low(ushort32, ushort32);
cchar2 __pack_high_low(cchar2, cchar2);
cchar4 __pack_high_low(cchar4, cchar4);
cchar8 __pack_high_low(cchar8, cchar8);
cchar16 __pack_high_low(cchar16, cchar16);
cchar32 __pack_high_low(cchar32, cchar32);




/*-----------------------------------------------------------------------------
* ID: __pack_low
*----------------------------------------------------------------------------*/

/* VPACKL2 */
short2 __pack_low(short2, short2);
short4 __pack_low(short4, short4);
short8 __pack_low(short8, short8);
short16 __pack_low(short16, short16);
short32 __pack_low(short32, short32);
ushort2 __pack_low(ushort2, ushort2);
ushort4 __pack_low(ushort4, ushort4);
ushort8 __pack_low(ushort8, ushort8);
ushort16 __pack_low(ushort16, ushort16);
ushort32 __pack_low(ushort32, ushort32);
cchar2 __pack_low(cchar2, cchar2);
cchar4 __pack_low(cchar4, cchar4);
cchar8 __pack_low(cchar8, cchar8);
cchar16 __pack_low(cchar16, cchar16);
cchar32 __pack_low(cchar32, cchar32);

/* VPACKL4 */
char4 __pack_low(char4, char4);
char8 __pack_low(char8, char8);
char16 __pack_low(char16, char16);
char32 __pack_low(char32, char32);
char64 __pack_low(char64, char64);
uchar4 __pack_low(uchar4, uchar4);
uchar8 __pack_low(uchar8, uchar8);
uchar16 __pack_low(uchar16, uchar16);
uchar32 __pack_low(uchar32, uchar32);
uchar64 __pack_low(uchar64, uchar64);

/* VPACKW */
int2 __pack_low(int2, int2);
int4 __pack_low(int4, int4);
int8 __pack_low(int8, int8);
int16 __pack_low(int16, int16);
float2 __pack_low(float2, float2);
float4 __pack_low(float4, float4);
float8 __pack_low(float8, float8);
float16 __pack_low(float16, float16);
uint2 __pack_low(uint2, uint2);
uint4 __pack_low(uint4, uint4);
uint8 __pack_low(uint8, uint8);
uint16 __pack_low(uint16, uint16);
cshort2 __pack_low(cshort2, cshort2);
cshort4 __pack_low(cshort4, cshort4);
cshort8 __pack_low(cshort8, cshort8);
cshort16 __pack_low(cshort16, cshort16);




/*-----------------------------------------------------------------------------
* ID: __pack_low_high
*----------------------------------------------------------------------------*/

/* VPACKLH2 */
short2 __pack_low_high(short2, short2);
short4 __pack_low_high(short4, short4);
short8 __pack_low_high(short8, short8);
short16 __pack_low_high(short16, short16);
short32 __pack_low_high(short32, short32);
ushort2 __pack_low_high(ushort2, ushort2);
ushort4 __pack_low_high(ushort4, ushort4);
ushort8 __pack_low_high(ushort8, ushort8);
ushort16 __pack_low_high(ushort16, ushort16);
ushort32 __pack_low_high(ushort32, ushort32);
cchar2 __pack_low_high(cchar2, cchar2);
cchar4 __pack_low_high(cchar4, cchar4);
cchar8 __pack_low_high(cchar8, cchar8);
cchar16 __pack_low_high(cchar16, cchar16);
cchar32 __pack_low_high(cchar32, cchar32);

/* VPACKLH4 */
char8 __pack_low_high(char8, char8);
char16 __pack_low_high(char16, char16);
char32 __pack_low_high(char32, char32);
char64 __pack_low_high(char64, char64);
uchar8 __pack_low_high(uchar8, uchar8);
uchar16 __pack_low_high(uchar16, uchar16);
uchar32 __pack_low_high(uchar32, uchar32);
uchar64 __pack_low_high(uchar64, uchar64);




/*-----------------------------------------------------------------------------
* ID: __pack_sat
*----------------------------------------------------------------------------*/

/* VSPACKDW */
int2 __pack_sat(int64_t, int64_t);
int4 __pack_sat(long2, long2);
int8 __pack_sat(long4, long4);
int16 __pack_sat(long8, long8);

/* VSPACKHB */
char2 __pack_sat(int16_t, int16_t);
char4 __pack_sat(short2, short2);
char8 __pack_sat(short4, short4);
char16 __pack_sat(short8, short8);
char32 __pack_sat(short16, short16);
char64 __pack_sat(short32, short32);

/* VSPACKWH */
short2 __pack_sat(int32_t, int32_t);
short4 __pack_sat(int2, int2);
short8 __pack_sat(int4, int4);
short16 __pack_sat(int8, int8);
short32 __pack_sat(int16, int16);




/*-----------------------------------------------------------------------------
* ID: __pack_shift_sat
*----------------------------------------------------------------------------*/

/* VRPACKH */
short2 __pack_shift_sat(int32_t, int32_t);
short4 __pack_shift_sat(int2, int2);
short8 __pack_shift_sat(int4, int4);
short16 __pack_shift_sat(int8, int8);
short32 __pack_shift_sat(int16, int16);




/*-----------------------------------------------------------------------------
* ID: __pack_usat
*----------------------------------------------------------------------------*/

/* VSPACKUDW */
uint2 __pack_usat(long, long);
uint4 __pack_usat(long2, long2);
uint8 __pack_usat(long4, long4);
uint16 __pack_usat(long8, long8);

/* VSPACKUHB */
uchar2 __pack_usat(short, short);
uchar4 __pack_usat(short2, short2);
uchar8 __pack_usat(short4, short4);
uchar16 __pack_usat(short8, short8);
uchar32 __pack_usat(short16, short16);
uchar64 __pack_usat(short32, short32);

/* VSPACKUWH */
ushort2 __pack_usat(int, int);
ushort4 __pack_usat(int2, int2);
ushort8 __pack_usat(int4, int4);
ushort16 __pack_usat(int8, int8);
ushort32 __pack_usat(int16, int16);




/*-----------------------------------------------------------------------------
* ID: __packw_rrd
*----------------------------------------------------------------------------*/

/* PACKW */
int2 __packw_rrd(int32_t, int32_t);
float2 __packw_rrd(float, float);
uint2 __packw_rrd(uint, uint);
cshort2 __packw_rrd(cshort, cshort);




/*-----------------------------------------------------------------------------
* ID: __packwdly4_rrd
*----------------------------------------------------------------------------*/

/* PACKWDLY4 */
int64_t __packwdly4_rrd(int32_t, int32_t);
ulong __packwdly4_rrd(uint, uint);




/*-----------------------------------------------------------------------------
* ID: __packx2_rrd
*----------------------------------------------------------------------------*/

/* PACKX2 */
short4 __packx2_rrd(short2, short2);
ushort4 __packx2_rrd(ushort2, ushort2);
cchar4 __packx2_rrd(cchar2, cchar2);




/*-----------------------------------------------------------------------------
* ID: __parallel_pack_dup_16way
*----------------------------------------------------------------------------*/

/* VPPACKDUP16W */
uint16 __parallel_pack_dup_16way(__vpred);




/*-----------------------------------------------------------------------------
* ID: __parallel_pack_dup_8way
*----------------------------------------------------------------------------*/

/* VPPACKDUP8W */
uint16 __parallel_pack_dup_8way(__vpred);




/*-----------------------------------------------------------------------------
* ID: __pbitcntb_pr
*----------------------------------------------------------------------------*/

/* PBITCNTB */
uint __pbitcntb_pr(__vpred);




/*-----------------------------------------------------------------------------
* ID: __pbitcntd_pr
*----------------------------------------------------------------------------*/

/* PBITCNTD */
uint __pbitcntd_pr(__vpred);




/*-----------------------------------------------------------------------------
* ID: __pbitcnth_pr
*----------------------------------------------------------------------------*/

/* PBITCNTH */
uint __pbitcnth_pr(__vpred);




/*-----------------------------------------------------------------------------
* ID: __pbitcntw_pr
*----------------------------------------------------------------------------*/

/* PBITCNTW */
uint __pbitcntw_pr(__vpred);




/*-----------------------------------------------------------------------------
* ID: __pcntgatherb_prp
*----------------------------------------------------------------------------*/

/* PCNTGATHERB */
void __pcntgatherb_prp(__vpred, uint&, __vpred&);




/*-----------------------------------------------------------------------------
* ID: __pcntgatherd_prp
*----------------------------------------------------------------------------*/

/* PCNTGATHERD */
void __pcntgatherd_prp(__vpred, uint&, __vpred&);




/*-----------------------------------------------------------------------------
* ID: __pcntgatherh_prp
*----------------------------------------------------------------------------*/

/* PCNTGATHERH */
void __pcntgatherh_prp(__vpred, uint&, __vpred&);




/*-----------------------------------------------------------------------------
* ID: __pcntgatherw_prp
*----------------------------------------------------------------------------*/

/* PCNTGATHERW */
void __pcntgatherw_prp(__vpred, uint&, __vpred&);




/*-----------------------------------------------------------------------------
* ID: __pduph2b_pp
*----------------------------------------------------------------------------*/

/* PDUPH2B */
__vpred __pduph2b_pp(__vpred);




/*-----------------------------------------------------------------------------
* ID: __pduph2d_pp
*----------------------------------------------------------------------------*/

/* PDUPH2D */
__vpred __pduph2d_pp(__vpred);




/*-----------------------------------------------------------------------------
* ID: __pduph2h_pp
*----------------------------------------------------------------------------*/

/* PDUPH2H */
__vpred __pduph2h_pp(__vpred);




/*-----------------------------------------------------------------------------
* ID: __pduph2w_pp
*----------------------------------------------------------------------------*/

/* PDUPH2W */
__vpred __pduph2w_pp(__vpred);




/*-----------------------------------------------------------------------------
* ID: __pdupl2b_pp
*----------------------------------------------------------------------------*/

/* PDUPL2B */
__vpred __pdupl2b_pp(__vpred);




/*-----------------------------------------------------------------------------
* ID: __pdupl2d_pp
*----------------------------------------------------------------------------*/

/* PDUPL2D */
__vpred __pdupl2d_pp(__vpred);




/*-----------------------------------------------------------------------------
* ID: __pdupl2h_pp
*----------------------------------------------------------------------------*/

/* PDUPL2H */
__vpred __pdupl2h_pp(__vpred);




/*-----------------------------------------------------------------------------
* ID: __pdupl2w_pp
*----------------------------------------------------------------------------*/

/* PDUPL2W */
__vpred __pdupl2w_pp(__vpred);




/*-----------------------------------------------------------------------------
* ID: __permute
*----------------------------------------------------------------------------*/

/* VPERM */
uchar64 __permute(uchar64, uchar64);




/*-----------------------------------------------------------------------------
* ID: __permute_cntrl
*----------------------------------------------------------------------------*/

/* VPERM */
uchar64 __permute_cntrl(uchar64, uchar64);




/*-----------------------------------------------------------------------------
* ID: __permute_even_even_char
*----------------------------------------------------------------------------*/

/* VPERMEEB */
uchar64 __permute_even_even_char(uchar64, uchar64, uchar64);




/*-----------------------------------------------------------------------------
* ID: __permute_even_even_int
*----------------------------------------------------------------------------*/

/* VPERMEEW */
uchar64 __permute_even_even_int(uchar64, uchar64, uchar64);




/*-----------------------------------------------------------------------------
* ID: __permute_even_even_long
*----------------------------------------------------------------------------*/

/* VPERMEED */
uchar64 __permute_even_even_long(uchar64, uchar64, uchar64);




/*-----------------------------------------------------------------------------
* ID: __permute_even_even_quad
*----------------------------------------------------------------------------*/

/* VPERMEEQ */
uchar64 __permute_even_even_quad(uchar64, uchar64, uchar64);




/*-----------------------------------------------------------------------------
* ID: __permute_even_even_short
*----------------------------------------------------------------------------*/

/* VPERMEEH */
uchar64 __permute_even_even_short(uchar64, uchar64, uchar64);




/*-----------------------------------------------------------------------------
* ID: __permute_even_odd_char
*----------------------------------------------------------------------------*/

/* VPERMEOB */
uchar64 __permute_even_odd_char(uchar64, uchar64, uchar64);




/*-----------------------------------------------------------------------------
* ID: __permute_even_odd_int
*----------------------------------------------------------------------------*/

/* VPERMEOW */
uchar64 __permute_even_odd_int(uchar64, uchar64, uchar64);




/*-----------------------------------------------------------------------------
* ID: __permute_even_odd_long
*----------------------------------------------------------------------------*/

/* VPERMEOD */
uchar64 __permute_even_odd_long(uchar64, uchar64, uchar64);




/*-----------------------------------------------------------------------------
* ID: __permute_even_odd_quad
*----------------------------------------------------------------------------*/

/* VPERMEOQ */
uchar64 __permute_even_odd_quad(uchar64, uchar64, uchar64);




/*-----------------------------------------------------------------------------
* ID: __permute_even_odd_short
*----------------------------------------------------------------------------*/

/* VPERMEOH */
uchar64 __permute_even_odd_short(uchar64, uchar64, uchar64);




/*-----------------------------------------------------------------------------
* ID: __permute_high_high
*----------------------------------------------------------------------------*/

/* VPERMHH */
uchar64 __permute_high_high(uchar64, uchar64, uchar64);




/*-----------------------------------------------------------------------------
* ID: __permute_low_high
*----------------------------------------------------------------------------*/

/* VPERMLH */
uchar64 __permute_low_high(uchar64, uchar64, uchar64);




/*-----------------------------------------------------------------------------
* ID: __permute_low_low
*----------------------------------------------------------------------------*/

/* VPERMLL */
uchar64 __permute_low_low(uchar64, uchar64, uchar64);




/*-----------------------------------------------------------------------------
* ID: __permute_odd_odd_char
*----------------------------------------------------------------------------*/

/* VPERMOOB */
uchar64 __permute_odd_odd_char(uchar64, uchar64, uchar64);




/*-----------------------------------------------------------------------------
* ID: __permute_odd_odd_int
*----------------------------------------------------------------------------*/

/* VPERMOOW */
uchar64 __permute_odd_odd_int(uchar64, uchar64, uchar64);




/*-----------------------------------------------------------------------------
* ID: __permute_odd_odd_long
*----------------------------------------------------------------------------*/

/* VPERMOOD */
uchar64 __permute_odd_odd_long(uchar64, uchar64, uchar64);




/*-----------------------------------------------------------------------------
* ID: __permute_odd_odd_quad
*----------------------------------------------------------------------------*/

/* VPERMOOQ */
uchar64 __permute_odd_odd_quad(uchar64, uchar64, uchar64);




/*-----------------------------------------------------------------------------
* ID: __permute_odd_odd_short
*----------------------------------------------------------------------------*/

/* VPERMOOH */
uchar64 __permute_odd_odd_short(uchar64, uchar64, uchar64);




/*-----------------------------------------------------------------------------
* ID: __popcount
*----------------------------------------------------------------------------*/

/* VBITCNTB */
int8_t __popcount(int8_t);
char2 __popcount(char2);
/* UNSUPPORTED: char3 __popcount(char3); */
char4 __popcount(char4);
char8 __popcount(char8);
char16 __popcount(char16);
char32 __popcount(char32);
char64 __popcount(char64);
uchar __popcount(uchar);
uchar2 __popcount(uchar2);
/* UNSUPPORTED: uchar3 __popcount(uchar3); */
uchar4 __popcount(uchar4);
uchar8 __popcount(uchar8);
uchar16 __popcount(uchar16);
uchar32 __popcount(uchar32);
uchar64 __popcount(uchar64);

/* VBITCNTD */
int64_t __popcount(int64_t);
long2 __popcount(long2);
/* UNSUPPORTED: long3 __popcount(long3); */
long4 __popcount(long4);
long8 __popcount(long8);
ulong __popcount(ulong);
ulong2 __popcount(ulong2);
/* UNSUPPORTED: ulong3 __popcount(ulong3); */
ulong4 __popcount(ulong4);
ulong8 __popcount(ulong8);

/* VBITCNTH */
int16_t __popcount(int16_t);
short2 __popcount(short2);
/* UNSUPPORTED: short3 __popcount(short3); */
short4 __popcount(short4);
short8 __popcount(short8);
short16 __popcount(short16);
short32 __popcount(short32);
ushort __popcount(ushort);
ushort2 __popcount(ushort2);
/* UNSUPPORTED: ushort3 __popcount(ushort3); */
ushort4 __popcount(ushort4);
ushort8 __popcount(ushort8);
ushort16 __popcount(ushort16);
ushort32 __popcount(ushort32);

/* VBITCNTW */
int32_t __popcount(int32_t);
int2 __popcount(int2);
/* UNSUPPORTED: int3 __popcount(int3); */
int4 __popcount(int4);
int8 __popcount(int8);
int16 __popcount(int16);
uint __popcount(uint);
uint2 __popcount(uint2);
/* UNSUPPORTED: uint3 __popcount(uint3); */
uint4 __popcount(uint4);
uint8 __popcount(uint8);
uint16 __popcount(uint16);




/*-----------------------------------------------------------------------------
* ID: __popcount_char
*----------------------------------------------------------------------------*/

/* PBITCNTB */
uint __popcount_char(__vpred);




/*-----------------------------------------------------------------------------
* ID: __popcount_gather_char
*----------------------------------------------------------------------------*/

/* PCNTGATHERB */
void __popcount_gather_char(__vpred, uint&, __vpred&);




/*-----------------------------------------------------------------------------
* ID: __popcount_gather_int
*----------------------------------------------------------------------------*/

/* PCNTGATHERW */
void __popcount_gather_int(__vpred, uint&, __vpred&);




/*-----------------------------------------------------------------------------
* ID: __popcount_gather_long
*----------------------------------------------------------------------------*/

/* PCNTGATHERD */
void __popcount_gather_long(__vpred, uint&, __vpred&);




/*-----------------------------------------------------------------------------
* ID: __popcount_gather_short
*----------------------------------------------------------------------------*/

/* PCNTGATHERH */
void __popcount_gather_short(__vpred, uint&, __vpred&);




/*-----------------------------------------------------------------------------
* ID: __popcount_int
*----------------------------------------------------------------------------*/

/* PBITCNTW */
uint __popcount_int(__vpred);




/*-----------------------------------------------------------------------------
* ID: __popcount_long
*----------------------------------------------------------------------------*/

/* PBITCNTD */
uint __popcount_long(__vpred);




/*-----------------------------------------------------------------------------
* ID: __popcount_short
*----------------------------------------------------------------------------*/

/* PBITCNTH */
uint __popcount_short(__vpred);




/*-----------------------------------------------------------------------------
* ID: __prmbdb_pr
*----------------------------------------------------------------------------*/

/* PRMBDB */
uint __prmbdb_pr(__vpred);




/*-----------------------------------------------------------------------------
* ID: __prmbdd_pr
*----------------------------------------------------------------------------*/

/* PRMBDD */
uint __prmbdd_pr(__vpred);




/*-----------------------------------------------------------------------------
* ID: __prmbdh_pr
*----------------------------------------------------------------------------*/

/* PRMBDH */
uint __prmbdh_pr(__vpred);




/*-----------------------------------------------------------------------------
* ID: __prmbdw_pr
*----------------------------------------------------------------------------*/

/* PRMBDW */
uint __prmbdw_pr(__vpred);




/*-----------------------------------------------------------------------------
* ID: __recip
*----------------------------------------------------------------------------*/

/* VRCPDP */
double __recip(double);
double2 __recip(double2);
/* UNSUPPORTED: double3 __recip(double3); */
double4 __recip(double4);
double8 __recip(double8);

/* VRCPSP */
float __recip(float);
float2 __recip(float2);
/* UNSUPPORTED: float3 __recip(float3); */
float4 __recip(float4);
float8 __recip(float8);
float16 __recip(float16);




/*-----------------------------------------------------------------------------
* ID: __recip_sqrt
*----------------------------------------------------------------------------*/

/* VRSQRDP */
double __recip_sqrt(double);
double2 __recip_sqrt(double2);
/* UNSUPPORTED: double3 __recip_sqrt(double3); */
double4 __recip_sqrt(double4);
double8 __recip_sqrt(double8);

/* VRSQRSP */
float __recip_sqrt(float);
float2 __recip_sqrt(float2);
/* UNSUPPORTED: float3 __recip_sqrt(float3); */
float4 __recip_sqrt(float4);
float8 __recip_sqrt(float8);
float16 __recip_sqrt(float16);




/*-----------------------------------------------------------------------------
* ID: __replace
*----------------------------------------------------------------------------*/

/* REPLACE */
ulong __replace(ulong, uchar, uchar, ulong);
int64_t __replace(int64_t, int8_t, int8_t, int64_t);

/* VREPLACE */
ulong __replace(ulong, uchar, uchar, ulong);
ulong2 __replace(ulong2, uchar2, uchar2, ulong2);
/* UNSUPPORTED: ulong3 __replace(ulong3, uchar3, uchar3, ulong3); */
ulong4 __replace(ulong4, uchar4, uchar4, ulong4);
ulong8 __replace(ulong8, uchar8, uchar8, ulong8);
int64_t __replace(int64_t, int8_t, int8_t, int64_t);
long2 __replace(long2, char2, char2, long2);
/* UNSUPPORTED: long3 __replace(long3, char3, char3, long3); */
long4 __replace(long4, char4, char4, long4);
long8 __replace(long8, char8, char8, long8);




/*-----------------------------------------------------------------------------
* ID: __replace_dkkd
*----------------------------------------------------------------------------*/

/* REPLACE */
ulong __replace_dkkd(ulong, uchar, uchar, ulong);
int64_t __replace_dkkd(int64_t, int8_t, int8_t, int64_t);




/*-----------------------------------------------------------------------------
* ID: __replacev_dkkkv
*----------------------------------------------------------------------------*/

/* REPLACEV */
ulong8 __replacev_dkkkv(ulong, uchar, uchar, uchar, ulong8);
long8 __replacev_dkkkv(int64_t, int8_t, int8_t, int8_t, long8);




/*-----------------------------------------------------------------------------
* ID: __reverse
*----------------------------------------------------------------------------*/

/* VREVERSEB */
char64 __reverse(char64);
uchar64 __reverse(uchar64);

/* VREVERSED */
long8 __reverse(long8);
double8 __reverse(double8);
cfloat8 __reverse(cfloat8);
ulong8 __reverse(ulong8);
cint8 __reverse(cint8);

/* VREVERSEH */
short32 __reverse(short32);
ushort32 __reverse(ushort32);
cchar32 __reverse(cchar32);

/* VREVERSEW */
int16 __reverse(int16);
float16 __reverse(float16);
uint16 __reverse(uint16);
cshort16 __reverse(cshort16);




/*-----------------------------------------------------------------------------
* ID: __rhadd
*----------------------------------------------------------------------------*/

/* VAVGB */
int8_t __rhadd(int8_t, int8_t);
char2 __rhadd(char2, char2);
/* UNSUPPORTED: char3 __rhadd(char3, char3); */
char4 __rhadd(char4, char4);
char8 __rhadd(char8, char8);
char16 __rhadd(char16, char16);
char32 __rhadd(char32, char32);
char64 __rhadd(char64, char64);

/* VAVGD */
int64_t __rhadd(int64_t, int64_t);
long2 __rhadd(long2, long2);
/* UNSUPPORTED: long3 __rhadd(long3, long3); */
long4 __rhadd(long4, long4);
long8 __rhadd(long8, long8);

/* VAVGH */
int16_t __rhadd(int16_t, int16_t);
short2 __rhadd(short2, short2);
/* UNSUPPORTED: short3 __rhadd(short3, short3); */
short4 __rhadd(short4, short4);
short8 __rhadd(short8, short8);
short16 __rhadd(short16, short16);
short32 __rhadd(short32, short32);

/* VAVGUB */
uchar __rhadd(uchar, uchar);
uchar2 __rhadd(uchar2, uchar2);
/* UNSUPPORTED: uchar3 __rhadd(uchar3, uchar3); */
uchar4 __rhadd(uchar4, uchar4);
uchar8 __rhadd(uchar8, uchar8);
uchar16 __rhadd(uchar16, uchar16);
uchar32 __rhadd(uchar32, uchar32);
uchar64 __rhadd(uchar64, uchar64);

/* VAVGUD */
ulong __rhadd(ulong, ulong);
ulong2 __rhadd(ulong2, ulong2);
/* UNSUPPORTED: ulong3 __rhadd(ulong3, ulong3); */
ulong4 __rhadd(ulong4, ulong4);
ulong8 __rhadd(ulong8, ulong8);

/* VAVGUH */
ushort __rhadd(ushort, ushort);
ushort2 __rhadd(ushort2, ushort2);
/* UNSUPPORTED: ushort3 __rhadd(ushort3, ushort3); */
ushort4 __rhadd(ushort4, ushort4);
ushort8 __rhadd(ushort8, ushort8);
ushort16 __rhadd(ushort16, ushort16);
ushort32 __rhadd(ushort32, ushort32);

/* VAVGUW */
uint __rhadd(uint, uint);
uint2 __rhadd(uint2, uint2);
/* UNSUPPORTED: uint3 __rhadd(uint3, uint3); */
uint4 __rhadd(uint4, uint4);
uint8 __rhadd(uint8, uint8);
uint16 __rhadd(uint16, uint16);

/* VAVGW */
int32_t __rhadd(int32_t, int32_t);
int2 __rhadd(int2, int2);
/* UNSUPPORTED: int3 __rhadd(int3, int3); */
int4 __rhadd(int4, int4);
int8 __rhadd(int8, int8);
int16 __rhadd(int16, int16);




/*-----------------------------------------------------------------------------
* ID: __rightmost_bit_detect_char
*----------------------------------------------------------------------------*/

/* PRMBDB */
uint __rightmost_bit_detect_char(__vpred);




/*-----------------------------------------------------------------------------
* ID: __rightmost_bit_detect_int
*----------------------------------------------------------------------------*/

/* PRMBDW */
uint __rightmost_bit_detect_int(__vpred);




/*-----------------------------------------------------------------------------
* ID: __rightmost_bit_detect_long
*----------------------------------------------------------------------------*/

/* PRMBDD */
uint __rightmost_bit_detect_long(__vpred);




/*-----------------------------------------------------------------------------
* ID: __rightmost_bit_detect_short
*----------------------------------------------------------------------------*/

/* PRMBDH */
uint __rightmost_bit_detect_short(__vpred);




/*-----------------------------------------------------------------------------
* ID: __rotate_left
*----------------------------------------------------------------------------*/

/* VROTLB */
uchar4 __rotate_left(uchar4, uint);
uchar8 __rotate_left(uchar8, uint2);
uchar16 __rotate_left(uchar16, uint4);
uchar32 __rotate_left(uchar32, uint8);
uchar64 __rotate_left(uchar64, uint16);
uchar __rotate_left(uchar, uchar);
uchar2 __rotate_left(uchar2, uchar2);
/* UNSUPPORTED: uchar3 __rotate_left(uchar3, uchar3); */
uchar4 __rotate_left(uchar4, uchar4);
uchar8 __rotate_left(uchar8, uchar8);
uchar16 __rotate_left(uchar16, uchar16);
uchar32 __rotate_left(uchar32, uchar32);
uchar64 __rotate_left(uchar64, uchar64);

/* VROTLD */
ulong __rotate_left(ulong, ulong);
ulong2 __rotate_left(ulong2, ulong2);
/* UNSUPPORTED: ulong3 __rotate_left(ulong3, ulong3); */
ulong4 __rotate_left(ulong4, ulong4);
ulong8 __rotate_left(ulong8, ulong8);
ulong __rotate_left(ulong, uchar);
ulong2 __rotate_left(ulong2, uchar2);
/* UNSUPPORTED: ulong3 __rotate_left(ulong3, uchar3); */
ulong4 __rotate_left(ulong4, uchar4);
ulong8 __rotate_left(ulong8, uchar8);

/* VROTLH */
ushort2 __rotate_left(ushort2, uint);
ushort4 __rotate_left(ushort4, uint2);
ushort8 __rotate_left(ushort8, uint4);
ushort16 __rotate_left(ushort16, uint8);
ushort32 __rotate_left(ushort32, uint16);
ushort __rotate_left(ushort, ushort);
ushort2 __rotate_left(ushort2, ushort2);
/* UNSUPPORTED: ushort3 __rotate_left(ushort3, ushort3); */
ushort4 __rotate_left(ushort4, ushort4);
ushort8 __rotate_left(ushort8, ushort8);
ushort16 __rotate_left(ushort16, ushort16);
ushort32 __rotate_left(ushort32, ushort32);

/* VROTLW */
int32_t __rotate_left(int32_t, uint);
int2 __rotate_left(int2, uint2);
/* UNSUPPORTED: int3 __rotate_left(int3, uint3); */
int4 __rotate_left(int4, uint4);
int8 __rotate_left(int8, uint8);
int16 __rotate_left(int16, uint16);
int32_t __rotate_left(int32_t, uchar);
int2 __rotate_left(int2, uchar2);
/* UNSUPPORTED: int3 __rotate_left(int3, uchar3); */
int4 __rotate_left(int4, uchar4);
int8 __rotate_left(int8, uchar8);
int16 __rotate_left(int16, uchar16);




/*-----------------------------------------------------------------------------
* ID: __saturate
*----------------------------------------------------------------------------*/

/* VGSATD */
int64_t __saturate(int64_t, uchar);
long2 __saturate(long2, uchar2);
/* UNSUPPORTED: long3 __saturate(long3, uchar3); */
long4 __saturate(long4, uchar4);
long8 __saturate(long8, uchar8);

/* VGSATH */
int16_t __saturate(int16_t, uchar);
short2 __saturate(short2, uchar2);
/* UNSUPPORTED: short3 __saturate(short3, uchar3); */
short4 __saturate(short4, uchar4);
short8 __saturate(short8, uchar8);
short16 __saturate(short16, uchar16);
short32 __saturate(short32, uchar32);

/* VGSATUD */
ulong __saturate(ulong, uchar);
ulong2 __saturate(ulong2, uchar2);
/* UNSUPPORTED: ulong3 __saturate(ulong3, uchar3); */
ulong4 __saturate(ulong4, uchar4);
ulong8 __saturate(ulong8, uchar8);

/* VGSATUH */
ushort __saturate(ushort, uchar);
ushort2 __saturate(ushort2, uchar2);
/* UNSUPPORTED: ushort3 __saturate(ushort3, uchar3); */
ushort4 __saturate(ushort4, uchar4);
ushort8 __saturate(ushort8, uchar8);
ushort16 __saturate(ushort16, uchar16);
ushort32 __saturate(ushort32, uchar32);

/* VGSATUW */
uint __saturate(uint, uchar);
uint2 __saturate(uint2, uchar2);
/* UNSUPPORTED: uint3 __saturate(uint3, uchar3); */
uint4 __saturate(uint4, uchar4);
uint8 __saturate(uint8, uchar8);
uint16 __saturate(uint16, uchar16);

/* VGSATW */
int32_t __saturate(int32_t, uchar);
int2 __saturate(int2, uchar2);
/* UNSUPPORTED: int3 __saturate(int3, uchar3); */
int4 __saturate(int4, uchar4);
int8 __saturate(int8, uchar8);
int16 __saturate(int16, uchar16);




/*-----------------------------------------------------------------------------
* ID: __saturate_s2u
*----------------------------------------------------------------------------*/

/* VGSATSUD */
ulong __saturate_s2u(int64_t, uchar);
ulong2 __saturate_s2u(long2, uchar2);
/* UNSUPPORTED: ulong3 __saturate_s2u(long3, uchar3); */
ulong4 __saturate_s2u(long4, uchar4);
ulong8 __saturate_s2u(long8, uchar8);

/* VGSATSUH */
ushort __saturate_s2u(int16_t, uchar);
ushort2 __saturate_s2u(short2, uchar2);
/* UNSUPPORTED: ushort3 __saturate_s2u(short3, uchar3); */
ushort4 __saturate_s2u(short4, uchar4);
ushort8 __saturate_s2u(short8, uchar8);
ushort16 __saturate_s2u(short16, uchar16);
ushort32 __saturate_s2u(short32, uchar32);

/* VGSATSUW */
uint __saturate_s2u(int32_t, uchar);
uint2 __saturate_s2u(int2, uchar2);
/* UNSUPPORTED: uint3 __saturate_s2u(int3, uchar3); */
uint4 __saturate_s2u(int4, uchar4);
uint8 __saturate_s2u(int8, uchar8);
uint16 __saturate_s2u(int16, uchar16);




/*-----------------------------------------------------------------------------
* ID: __scale
*----------------------------------------------------------------------------*/

/* VSCALEDP */
double __scale(double, ulong);
double2 __scale(double2, ulong2);
/* UNSUPPORTED: double3 __scale(double3, ulong3); */
double4 __scale(double4, ulong4);
double8 __scale(double8, ulong8);

/* VSCALESP */
float __scale(float, uint);
float2 __scale(float2, uint2);
/* UNSUPPORTED: float3 __scale(float3, uint3); */
float4 __scale(float4, uint4);
float8 __scale(float8, uint8);
float16 __scale(float16, uint16);




/*-----------------------------------------------------------------------------
* ID: __scatter_set_bits
*----------------------------------------------------------------------------*/

/* VSCATTERB */
char64 __scatter_set_bits(__vpred, char64);




/*-----------------------------------------------------------------------------
* ID: __scatter_unset_bits
*----------------------------------------------------------------------------*/

/* VSCATTERNB */
char64 __scatter_unset_bits(__vpred, char64);




/*-----------------------------------------------------------------------------
* ID: __sconv_operator
*----------------------------------------------------------------------------*/

/* VBUNPKD */
int64_t __convert_long(int8_t);
long2 __convert_long2(char2);
/* UNSUPPORTED: long3 __convert_long3(char3); */
long4 __convert_long4(char4);
long8 __convert_long8(char8);
clong __convert_clong(cchar);
clong2 __convert_clong2(cchar2);
clong4 __convert_clong4(cchar4);

/* VBUNPKDU */
ulong __convert_ulong(uchar);
ulong2 __convert_ulong2(uchar2);
/* UNSUPPORTED: ulong3 __convert_ulong3(uchar3); */
ulong4 __convert_ulong4(uchar4);
ulong8 __convert_ulong8(uchar8);
int64_t __convert_long(uchar);
long2 __convert_long2(uchar2);
/* UNSUPPORTED: long3 __convert_long3(uchar3); */
long4 __convert_long4(uchar4);
long8 __convert_long8(uchar8);

/* VBUNPKH */
int16_t __convert_short(int8_t);
short2 __convert_short2(char2);
/* UNSUPPORTED: short3 __convert_short3(char3); */
short4 __convert_short4(char4);
short8 __convert_short8(char8);
short16 __convert_short16(char16);
short32 __convert_short32(char32);
cshort __convert_cshort(cchar);
cshort2 __convert_cshort2(cchar2);
cshort4 __convert_cshort4(cchar4);
cshort8 __convert_cshort8(cchar8);
cshort16 __convert_cshort16(cchar16);

/* VBUNPKHU */
ushort __convert_ushort(uchar);
ushort2 __convert_ushort2(uchar2);
/* UNSUPPORTED: ushort3 __convert_ushort3(uchar3); */
ushort4 __convert_ushort4(uchar4);
ushort8 __convert_ushort8(uchar8);
ushort16 __convert_ushort16(uchar16);
ushort32 __convert_ushort32(uchar32);
int16_t __convert_short(uchar);
short2 __convert_short2(uchar2);
/* UNSUPPORTED: short3 __convert_short3(uchar3); */
short4 __convert_short4(uchar4);
short8 __convert_short8(uchar8);
short16 __convert_short16(uchar16);
short32 __convert_short32(uchar32);

/* VBUNPKW */
int32_t __convert_int(int8_t);
int2 __convert_int2(char2);
/* UNSUPPORTED: int3 __convert_int3(char3); */
int4 __convert_int4(char4);
int8 __convert_int8(char8);
int16 __convert_int16(char16);
cint __convert_cint(cchar);
cint2 __convert_cint2(cchar2);
cint4 __convert_cint4(cchar4);
cint8 __convert_cint8(cchar8);

/* VBUNPKWU */
uint __convert_uint(uchar);
uint2 __convert_uint2(uchar2);
/* UNSUPPORTED: uint3 __convert_uint3(uchar3); */
uint4 __convert_uint4(uchar4);
uint8 __convert_uint8(uchar8);
uint16 __convert_uint16(uchar16);
int32_t __convert_int(uchar);
int2 __convert_int2(uchar2);
/* UNSUPPORTED: int3 __convert_int3(uchar3); */
int4 __convert_int4(uchar4);
int8 __convert_int8(uchar8);
int16 __convert_int16(uchar16);

/* VHUNPKD */
int64_t __convert_long(int16_t);
long2 __convert_long2(short2);
/* UNSUPPORTED: long3 __convert_long3(short3); */
long4 __convert_long4(short4);
long8 __convert_long8(short8);
clong __convert_clong(cshort);
clong2 __convert_clong2(cshort2);
clong4 __convert_clong4(cshort4);

/* VHUNPKDU */
ulong __convert_ulong(ushort);
ulong2 __convert_ulong2(ushort2);
/* UNSUPPORTED: ulong3 __convert_ulong3(ushort3); */
ulong4 __convert_ulong4(ushort4);
ulong8 __convert_ulong8(ushort8);
int64_t __convert_long(ushort);
long2 __convert_long2(ushort2);
/* UNSUPPORTED: long3 __convert_long3(ushort3); */
long4 __convert_long4(ushort4);
long8 __convert_long8(ushort8);

/* VHUNPKW */
int32_t __convert_int(int16_t);
int2 __convert_int2(short2);
/* UNSUPPORTED: int3 __convert_int3(short3); */
int4 __convert_int4(short4);
int8 __convert_int8(short8);
int16 __convert_int16(short16);
cint __convert_cint(cshort);
cint2 __convert_cint2(cshort2);
cint4 __convert_cint4(cshort4);
cint8 __convert_cint8(cshort8);

/* VHUNPKWU */
uint __convert_uint(ushort);
uint2 __convert_uint2(ushort2);
/* UNSUPPORTED: uint3 __convert_uint3(ushort3); */
uint4 __convert_uint4(ushort4);
uint8 __convert_uint8(ushort8);
uint16 __convert_uint16(ushort16);
int32_t __convert_int(ushort);
int2 __convert_int2(ushort2);
/* UNSUPPORTED: int3 __convert_int3(ushort3); */
int4 __convert_int4(ushort4);
int8 __convert_int8(ushort8);
int16 __convert_int16(ushort16);

/* VINTSP */
float __convert_float(int32_t);
float2 __convert_float2(int2);
/* UNSUPPORTED: float3 __convert_float3(int3); */
float4 __convert_float4(int4);
float8 __convert_float8(int8);
float16 __convert_float16(int16);

/* VINTSPU */
float __convert_float(uint);
float2 __convert_float2(uint2);
/* UNSUPPORTED: float3 __convert_float3(uint3); */
float4 __convert_float4(uint4);
float8 __convert_float8(uint8);
float16 __convert_float16(uint16);

/* VSPTRUNC */
int32_t __convert_int(float);
int2 __convert_int2(float2);
/* UNSUPPORTED: int3 __convert_int3(float3); */
int4 __convert_int4(float4);
int8 __convert_int8(float8);
int16 __convert_int16(float16);

/* VWUNPKD */
int64_t __convert_long(int32_t);
long2 __convert_long2(int2);
/* UNSUPPORTED: long3 __convert_long3(int3); */
long4 __convert_long4(int4);
long8 __convert_long8(int8);
clong __convert_clong(cint);
clong2 __convert_clong2(cint2);
clong4 __convert_clong4(cint4);

/* VWUNPKDU */
ulong __convert_ulong(uint);
ulong2 __convert_ulong2(uint2);
/* UNSUPPORTED: ulong3 __convert_ulong3(uint3); */
ulong4 __convert_ulong4(uint4);
ulong8 __convert_ulong8(uint8);
int64_t __convert_long(uint);
long2 __convert_long2(uint2);
/* UNSUPPORTED: long3 __convert_long3(uint3); */
long4 __convert_long4(uint4);
long8 __convert_long8(uint8);




/*-----------------------------------------------------------------------------
* ID: __select
*----------------------------------------------------------------------------*/

/* VSEL */
int8_t __select(__vpred, int8_t, int8_t);
char2 __select(__vpred, char2, char2);
/* UNSUPPORTED: char3 __select(__vpred, char3, char3); */
char4 __select(__vpred, char4, char4);
char8 __select(__vpred, char8, char8);
char16 __select(__vpred, char16, char16);
char32 __select(__vpred, char32, char32);
char64 __select(__vpred, char64, char64);
int16_t __select(__vpred, int16_t, int16_t);
short2 __select(__vpred, short2, short2);
/* UNSUPPORTED: short3 __select(__vpred, short3, short3); */
short4 __select(__vpred, short4, short4);
short8 __select(__vpred, short8, short8);
short16 __select(__vpred, short16, short16);
short32 __select(__vpred, short32, short32);
int32_t __select(__vpred, int32_t, int32_t);
int2 __select(__vpred, int2, int2);
/* UNSUPPORTED: int3 __select(__vpred, int3, int3); */
int4 __select(__vpred, int4, int4);
int8 __select(__vpred, int8, int8);
int16 __select(__vpred, int16, int16);
int64_t __select(__vpred, int64_t, int64_t);
long2 __select(__vpred, long2, long2);
/* UNSUPPORTED: long3 __select(__vpred, long3, long3); */
long4 __select(__vpred, long4, long4);
long8 __select(__vpred, long8, long8);
int32_t __select(__vpred, int32_t, uint);
int2 __select(__vpred, int2, uint2);
/* UNSUPPORTED: int3 __select(__vpred, int3, uint3); */
int4 __select(__vpred, int4, uint4);
int8 __select(__vpred, int8, uint8);
int16 __select(__vpred, int16, uint16);
cchar __select(__vpred, cchar, cchar);
cchar2 __select(__vpred, cchar2, cchar2);
cchar4 __select(__vpred, cchar4, cchar4);
cchar8 __select(__vpred, cchar8, cchar8);
cchar16 __select(__vpred, cchar16, cchar16);
cchar32 __select(__vpred, cchar32, cchar32);
uchar __select(__vpred, uchar, uchar);
uchar2 __select(__vpred, uchar2, uchar2);
/* UNSUPPORTED: uchar3 __select(__vpred, uchar3, uchar3); */
uchar4 __select(__vpred, uchar4, uchar4);
uchar8 __select(__vpred, uchar8, uchar8);
uchar16 __select(__vpred, uchar16, uchar16);
uchar32 __select(__vpred, uchar32, uchar32);
uchar64 __select(__vpred, uchar64, uchar64);
cshort __select(__vpred, cshort, cshort);
cshort2 __select(__vpred, cshort2, cshort2);
cshort4 __select(__vpred, cshort4, cshort4);
cshort8 __select(__vpred, cshort8, cshort8);
cshort16 __select(__vpred, cshort16, cshort16);
ushort __select(__vpred, ushort, ushort);
ushort2 __select(__vpred, ushort2, ushort2);
/* UNSUPPORTED: ushort3 __select(__vpred, ushort3, ushort3); */
ushort4 __select(__vpred, ushort4, ushort4);
ushort8 __select(__vpred, ushort8, ushort8);
ushort16 __select(__vpred, ushort16, ushort16);
ushort32 __select(__vpred, ushort32, ushort32);
float __select(__vpred, float, float);
float2 __select(__vpred, float2, float2);
/* UNSUPPORTED: float3 __select(__vpred, float3, float3); */
float4 __select(__vpred, float4, float4);
float8 __select(__vpred, float8, float8);
float16 __select(__vpred, float16, float16);
cfloat __select(__vpred, cfloat, cfloat);
cfloat2 __select(__vpred, cfloat2, cfloat2);
cfloat4 __select(__vpred, cfloat4, cfloat4);
cfloat8 __select(__vpred, cfloat8, cfloat8);
cint __select(__vpred, cint, cint);
cint2 __select(__vpred, cint2, cint2);
cint4 __select(__vpred, cint4, cint4);
cint8 __select(__vpred, cint8, cint8);
uint __select(__vpred, uint, uint);
uint2 __select(__vpred, uint2, uint2);
/* UNSUPPORTED: uint3 __select(__vpred, uint3, uint3); */
uint4 __select(__vpred, uint4, uint4);
uint8 __select(__vpred, uint8, uint8);
uint16 __select(__vpred, uint16, uint16);
double __select(__vpred, double, double);
double2 __select(__vpred, double2, double2);
/* UNSUPPORTED: double3 __select(__vpred, double3, double3); */
double4 __select(__vpred, double4, double4);
double8 __select(__vpred, double8, double8);
cdouble __select(__vpred, cdouble, cdouble);
cdouble2 __select(__vpred, cdouble2, cdouble2);
cdouble4 __select(__vpred, cdouble4, cdouble4);
clong __select(__vpred, clong, clong);
clong2 __select(__vpred, clong2, clong2);
clong4 __select(__vpred, clong4, clong4);
ulong __select(__vpred, ulong, ulong);
ulong2 __select(__vpred, ulong2, ulong2);
/* UNSUPPORTED: ulong3 __select(__vpred, ulong3, ulong3); */
ulong4 __select(__vpred, ulong4, ulong4);
ulong8 __select(__vpred, ulong8, ulong8);
float __select(__vpred, float, uint);
float2 __select(__vpred, float2, uint2);
/* UNSUPPORTED: float3 __select(__vpred, float3, uint3); */
float4 __select(__vpred, float4, uint4);
float8 __select(__vpred, float8, uint8);
float16 __select(__vpred, float16, uint16);
cfloat __select(__vpred, cfloat, uint2);
cfloat2 __select(__vpred, cfloat2, uint4);
cfloat4 __select(__vpred, cfloat4, uint8);
cfloat8 __select(__vpred, cfloat8, uint16);
cint __select(__vpred, cint, uint2);
cint2 __select(__vpred, cint2, uint4);
cint4 __select(__vpred, cint4, uint8);
cint8 __select(__vpred, cint8, uint16);
/* CONSTANT: uint __select(__vpred, uint, (uint)(k)); */
/* CONSTANT: uint2 __select(__vpred, uint2, (uint2)(k)); */
/* UNSUPPORTED: uint3 __select(__vpred, uint3, (uint3)(k)); */
/* CONSTANT: uint4 __select(__vpred, uint4, (uint4)(k)); */
/* CONSTANT: uint8 __select(__vpred, uint8, (uint8)(k)); */
/* CONSTANT: uint16 __select(__vpred, uint16, (uint16)(k)); */




/*-----------------------------------------------------------------------------
* ID: __set
*----------------------------------------------------------------------------*/

/* SET */
int32_t __set(int32_t, int32_t);




/*-----------------------------------------------------------------------------
* ID: __set_rrr
*----------------------------------------------------------------------------*/

/* SET */
int32_t __set_rrr(int32_t, int32_t);




/*-----------------------------------------------------------------------------
* ID: __shift_left
*----------------------------------------------------------------------------*/

/* SHLD */
int64_t __shift_left(int64_t, int32_t);
int64_t __shift_left(int64_t, uchar);
ulong __shift_left(ulong, uint);
ulong __shift_left(ulong, uchar);

/* SHLW */
int32_t __shift_left(int32_t, int32_t);
int32_t __shift_left(int32_t, uchar);
uint __shift_left(uint, uint);
uint __shift_left(uint, uchar);

/* VSHLB */
char4 __shift_left(char4, int32_t);
char8 __shift_left(char8, int2);
char16 __shift_left(char16, int4);
char32 __shift_left(char32, int8);
char64 __shift_left(char64, int16);
int8_t __shift_left(int8_t, int8_t);
char2 __shift_left(char2, char2);
/* UNSUPPORTED: char3 __shift_left(char3, char3); */
char4 __shift_left(char4, char4);
char8 __shift_left(char8, char8);
char16 __shift_left(char16, char16);
char32 __shift_left(char32, char32);
char64 __shift_left(char64, char64);
uchar4 __shift_left(uchar4, uint);
uchar8 __shift_left(uchar8, uint2);
uchar16 __shift_left(uchar16, uint4);
uchar32 __shift_left(uchar32, uint8);
uchar64 __shift_left(uchar64, uint16);
uchar __shift_left(uchar, uchar);
uchar2 __shift_left(uchar2, uchar2);
/* UNSUPPORTED: uchar3 __shift_left(uchar3, uchar3); */
uchar4 __shift_left(uchar4, uchar4);
uchar8 __shift_left(uchar8, uchar8);
uchar16 __shift_left(uchar16, uchar16);
uchar32 __shift_left(uchar32, uchar32);
uchar64 __shift_left(uchar64, uchar64);

/* VSHLD */
int64_t __shift_left(int64_t, int64_t);
long2 __shift_left(long2, long2);
/* UNSUPPORTED: long3 __shift_left(long3, long3); */
long4 __shift_left(long4, long4);
long8 __shift_left(long8, long8);
/* CONSTANT: int64_t __shift_left(int64_t, (int64_t)(k)); */
/* CONSTANT: long2 __shift_left(long2, (long2)(k)); */
/* UNSUPPORTED: long3 __shift_left(long3, (long3)(k)); */
/* CONSTANT: long4 __shift_left(long4, (long4)(k)); */
/* CONSTANT: long8 __shift_left(long8, (long8)(k)); */
ulong __shift_left(ulong, ulong);
ulong2 __shift_left(ulong2, ulong2);
/* UNSUPPORTED: ulong3 __shift_left(ulong3, ulong3); */
ulong4 __shift_left(ulong4, ulong4);
ulong8 __shift_left(ulong8, ulong8);
/* CONSTANT: ulong __shift_left(ulong, (ulong)(k)); */
/* CONSTANT: ulong2 __shift_left(ulong2, (ulong2)(k)); */
/* UNSUPPORTED: ulong3 __shift_left(ulong3, (ulong3)(k)); */
/* CONSTANT: ulong4 __shift_left(ulong4, (ulong4)(k)); */
/* CONSTANT: ulong8 __shift_left(ulong8, (ulong8)(k)); */

/* VSHLH */
short2 __shift_left(short2, int32_t);
short4 __shift_left(short4, int2);
short8 __shift_left(short8, int4);
short16 __shift_left(short16, int8);
short32 __shift_left(short32, int16);
int16_t __shift_left(int16_t, int16_t);
short2 __shift_left(short2, short2);
/* UNSUPPORTED: short3 __shift_left(short3, short3); */
short4 __shift_left(short4, short4);
short8 __shift_left(short8, short8);
short16 __shift_left(short16, short16);
short32 __shift_left(short32, short32);
ushort2 __shift_left(ushort2, uint);
ushort4 __shift_left(ushort4, uint2);
ushort8 __shift_left(ushort8, uint4);
ushort16 __shift_left(ushort16, uint8);
ushort32 __shift_left(ushort32, uint16);
ushort __shift_left(ushort, ushort);
ushort2 __shift_left(ushort2, ushort2);
/* UNSUPPORTED: ushort3 __shift_left(ushort3, ushort3); */
ushort4 __shift_left(ushort4, ushort4);
ushort8 __shift_left(ushort8, ushort8);
ushort16 __shift_left(ushort16, ushort16);
ushort32 __shift_left(ushort32, ushort32);

/* VSHLW */
int32_t __shift_left(int32_t, int32_t);
int2 __shift_left(int2, int2);
/* UNSUPPORTED: int3 __shift_left(int3, int3); */
int4 __shift_left(int4, int4);
int8 __shift_left(int8, int8);
int16 __shift_left(int16, int16);
/* CONSTANT: int32_t __shift_left(int32_t, (int32_t)(k)); */
/* CONSTANT: int2 __shift_left(int2, (int2)(k)); */
/* UNSUPPORTED: int3 __shift_left(int3, (int3)(k)); */
/* CONSTANT: int4 __shift_left(int4, (int4)(k)); */
/* CONSTANT: int8 __shift_left(int8, (int8)(k)); */
/* CONSTANT: int16 __shift_left(int16, (int16)(k)); */
uint __shift_left(uint, uint);
uint2 __shift_left(uint2, uint2);
/* UNSUPPORTED: uint3 __shift_left(uint3, uint3); */
uint4 __shift_left(uint4, uint4);
uint8 __shift_left(uint8, uint8);
uint16 __shift_left(uint16, uint16);
/* CONSTANT: uint __shift_left(uint, (uint)(k)); */
/* CONSTANT: uint2 __shift_left(uint2, (uint2)(k)); */
/* UNSUPPORTED: uint3 __shift_left(uint3, (uint3)(k)); */
/* CONSTANT: uint4 __shift_left(uint4, (uint4)(k)); */
/* CONSTANT: uint8 __shift_left(uint8, (uint8)(k)); */
/* CONSTANT: uint16 __shift_left(uint16, (uint16)(k)); */




/*-----------------------------------------------------------------------------
* ID: __shift_left_conditional
*----------------------------------------------------------------------------*/

/* VSHLCB */
int8_t __shift_left_conditional(__vpred, int8_t, int8_t);
char2 __shift_left_conditional(__vpred, char2, char2);
/* UNSUPPORTED: char3 __shift_left_conditional(__vpred, char3, char3); */
char4 __shift_left_conditional(__vpred, char4, char4);
char8 __shift_left_conditional(__vpred, char8, char8);
char16 __shift_left_conditional(__vpred, char16, char16);
char32 __shift_left_conditional(__vpred, char32, char32);
char64 __shift_left_conditional(__vpred, char64, char64);
uchar __shift_left_conditional(__vpred, uchar, uchar);
uchar2 __shift_left_conditional(__vpred, uchar2, uchar2);
/* UNSUPPORTED: uchar3 __shift_left_conditional(__vpred, uchar3, uchar3); */
uchar4 __shift_left_conditional(__vpred, uchar4, uchar4);
uchar8 __shift_left_conditional(__vpred, uchar8, uchar8);
uchar16 __shift_left_conditional(__vpred, uchar16, uchar16);
uchar32 __shift_left_conditional(__vpred, uchar32, uchar32);
uchar64 __shift_left_conditional(__vpred, uchar64, uchar64);

/* VSHLCH */
int16_t __shift_left_conditional(__vpred, int16_t, int16_t);
short2 __shift_left_conditional(__vpred, short2, short2);
/* UNSUPPORTED: short3 __shift_left_conditional(__vpred, short3, short3); */
short4 __shift_left_conditional(__vpred, short4, short4);
short8 __shift_left_conditional(__vpred, short8, short8);
short16 __shift_left_conditional(__vpred, short16, short16);
short32 __shift_left_conditional(__vpred, short32, short32);
ushort __shift_left_conditional(__vpred, ushort, ushort);
ushort2 __shift_left_conditional(__vpred, ushort2, ushort2);
/* UNSUPPORTED: ushort3 __shift_left_conditional(__vpred, ushort3, ushort3); */
ushort4 __shift_left_conditional(__vpred, ushort4, ushort4);
ushort8 __shift_left_conditional(__vpred, ushort8, ushort8);
ushort16 __shift_left_conditional(__vpred, ushort16, ushort16);
ushort32 __shift_left_conditional(__vpred, ushort32, ushort32);

/* VSHLCW */
int32_t __shift_left_conditional(__vpred, int32_t, int32_t);
int2 __shift_left_conditional(__vpred, int2, int2);
/* UNSUPPORTED: int3 __shift_left_conditional(__vpred, int3, int3); */
int4 __shift_left_conditional(__vpred, int4, int4);
int8 __shift_left_conditional(__vpred, int8, int8);
int16 __shift_left_conditional(__vpred, int16, int16);
uint __shift_left_conditional(__vpred, uint, uint);
uint2 __shift_left_conditional(__vpred, uint2, uint2);
/* UNSUPPORTED: uint3 __shift_left_conditional(__vpred, uint3, uint3); */
uint4 __shift_left_conditional(__vpred, uint4, uint4);
uint8 __shift_left_conditional(__vpred, uint8, uint8);
uint16 __shift_left_conditional(__vpred, uint16, uint16);




/*-----------------------------------------------------------------------------
* ID: __shift_left_full
*----------------------------------------------------------------------------*/

/* VSHL */
ulong8 __shift_left_full(ulong8, uchar);
ulong8 __shift_left_full(ulong8, ulong8);




/*-----------------------------------------------------------------------------
* ID: __shift_left_merge
*----------------------------------------------------------------------------*/

/* VSHLM16B */
char64 __shift_left_merge(char64, char16);

/* VSHLM1B */
char64 __shift_left_merge(char64, int8_t);

/* VSHLM2B */
char64 __shift_left_merge(char64, char2);

/* VSHLM4B */
char64 __shift_left_merge(char64, char4);

/* VSHLM8B */
char64 __shift_left_merge(char64, char8);

/* VSHLMB */
int32_t __shift_left_merge(int32_t, char4);
int2 __shift_left_merge(int2, char8);
int4 __shift_left_merge(int4, char16);
int8 __shift_left_merge(int8, char32);
int16 __shift_left_merge(int16, char64);




/*-----------------------------------------------------------------------------
* ID: __shift_left_round_sat
*----------------------------------------------------------------------------*/

/* VSSHLRDW */
int2 __shift_left_round_sat(int64_t, ulong);
int4 __shift_left_round_sat(long2, ulong2);
int8 __shift_left_round_sat(long4, ulong4);
int16 __shift_left_round_sat(long8, ulong8);
int2 __shift_left_round_sat(int64_t, uchar);
int4 __shift_left_round_sat(long2, uchar2);
int8 __shift_left_round_sat(long4, uchar4);
int16 __shift_left_round_sat(long8, uchar8);

/* VSSHLRHB */
char4 __shift_left_round_sat(short2, uint);
char8 __shift_left_round_sat(short4, uint2);
char16 __shift_left_round_sat(short8, uint4);
char32 __shift_left_round_sat(short16, uint8);
char64 __shift_left_round_sat(short32, uint16);
char4 __shift_left_round_sat(short2, uchar);
char8 __shift_left_round_sat(short4, uchar2);
char16 __shift_left_round_sat(short8, uchar4);
char32 __shift_left_round_sat(short16, uchar8);
char64 __shift_left_round_sat(short32, uchar16);

/* VSSHLRWH */
short2 __shift_left_round_sat(int32_t, uint);
short4 __shift_left_round_sat(int2, uint2);
short8 __shift_left_round_sat(int4, uint4);
short16 __shift_left_round_sat(int8, uint8);
short32 __shift_left_round_sat(int16, uint16);
short2 __shift_left_round_sat(int32_t, uchar);
short4 __shift_left_round_sat(int2, uchar2);
short8 __shift_left_round_sat(int4, uchar4);
short16 __shift_left_round_sat(int8, uchar8);
short32 __shift_left_round_sat(int16, uchar16);

/* VSSHLURDW */
uint2 __shift_left_round_sat(ulong, ulong);
uint4 __shift_left_round_sat(ulong2, ulong2);
uint8 __shift_left_round_sat(ulong4, ulong4);
uint16 __shift_left_round_sat(ulong8, ulong8);
uint2 __shift_left_round_sat(ulong, uchar);
uint4 __shift_left_round_sat(ulong2, uchar2);
uint8 __shift_left_round_sat(ulong4, uchar4);
uint16 __shift_left_round_sat(ulong8, uchar8);

/* VSSHLURHB */
uchar4 __shift_left_round_sat(ushort2, uint);
uchar8 __shift_left_round_sat(ushort4, uint2);
uchar16 __shift_left_round_sat(ushort8, uint4);
uchar32 __shift_left_round_sat(ushort16, uint8);
uchar64 __shift_left_round_sat(ushort32, uint16);
uchar4 __shift_left_round_sat(ushort2, uchar);
uchar8 __shift_left_round_sat(ushort4, uchar2);
uchar16 __shift_left_round_sat(ushort8, uchar4);
uchar32 __shift_left_round_sat(ushort16, uchar8);
uchar64 __shift_left_round_sat(ushort32, uchar16);

/* VSSHLURWH */
ushort2 __shift_left_round_sat(uint, uint);
ushort4 __shift_left_round_sat(uint2, uint2);
ushort8 __shift_left_round_sat(uint4, uint4);
ushort16 __shift_left_round_sat(uint8, uint8);
ushort32 __shift_left_round_sat(uint16, uint16);
ushort2 __shift_left_round_sat(uint, uchar);
ushort4 __shift_left_round_sat(uint2, uchar2);
ushort8 __shift_left_round_sat(uint4, uchar4);
ushort16 __shift_left_round_sat(uint8, uchar8);
ushort32 __shift_left_round_sat(uint16, uchar16);




/*-----------------------------------------------------------------------------
* ID: __shift_left_round_sat_to_unsigned
*----------------------------------------------------------------------------*/

/* VSSHLSURDW */
uint2 __shift_left_round_sat_to_unsigned(int64_t, ulong);
uint4 __shift_left_round_sat_to_unsigned(long2, ulong2);
uint8 __shift_left_round_sat_to_unsigned(long4, ulong4);
uint16 __shift_left_round_sat_to_unsigned(long8, ulong8);
uint2 __shift_left_round_sat_to_unsigned(int64_t, uchar);
uint4 __shift_left_round_sat_to_unsigned(long2, uchar2);
uint8 __shift_left_round_sat_to_unsigned(long4, uchar4);
uint16 __shift_left_round_sat_to_unsigned(long8, uchar8);

/* VSSHLSURHB */
uchar4 __shift_left_round_sat_to_unsigned(short2, uint);
uchar8 __shift_left_round_sat_to_unsigned(short4, uint2);
uchar16 __shift_left_round_sat_to_unsigned(short8, uint4);
uchar32 __shift_left_round_sat_to_unsigned(short16, uint8);
uchar64 __shift_left_round_sat_to_unsigned(short32, uint16);
uchar4 __shift_left_round_sat_to_unsigned(short2, uchar);
uchar8 __shift_left_round_sat_to_unsigned(short4, uchar2);
uchar16 __shift_left_round_sat_to_unsigned(short8, uchar4);
uchar32 __shift_left_round_sat_to_unsigned(short16, uchar8);
uchar64 __shift_left_round_sat_to_unsigned(short32, uchar16);

/* VSSHLSURWH */
ushort2 __shift_left_round_sat_to_unsigned(int32_t, uint);
ushort4 __shift_left_round_sat_to_unsigned(int2, uint2);
ushort8 __shift_left_round_sat_to_unsigned(int4, uint4);
ushort16 __shift_left_round_sat_to_unsigned(int8, uint8);
ushort32 __shift_left_round_sat_to_unsigned(int16, uint16);
ushort2 __shift_left_round_sat_to_unsigned(int32_t, uchar);
ushort4 __shift_left_round_sat_to_unsigned(int2, uchar2);
ushort8 __shift_left_round_sat_to_unsigned(int4, uchar4);
ushort16 __shift_left_round_sat_to_unsigned(int8, uchar8);
ushort32 __shift_left_round_sat_to_unsigned(int16, uchar16);




/*-----------------------------------------------------------------------------
* ID: __shift_left_sat
*----------------------------------------------------------------------------*/

/* VSSHLH */
short2 __shift_left_sat(short2, uint);
short4 __shift_left_sat(short4, uint2);
short8 __shift_left_sat(short8, uint4);
short16 __shift_left_sat(short16, uint8);
short32 __shift_left_sat(short32, uint16);
int16_t __shift_left_sat(int16_t, uchar);
short2 __shift_left_sat(short2, uchar2);
/* UNSUPPORTED: short3 __shift_left_sat(short3, uchar3); */
short4 __shift_left_sat(short4, uchar4);
short8 __shift_left_sat(short8, uchar8);
short16 __shift_left_sat(short16, uchar16);
short32 __shift_left_sat(short32, uchar32);

/* VSSHLUH */
ushort2 __shift_left_sat(ushort2, uint);
ushort4 __shift_left_sat(ushort4, uint2);
ushort8 __shift_left_sat(ushort8, uint4);
ushort16 __shift_left_sat(ushort16, uint8);
ushort32 __shift_left_sat(ushort32, uint16);
ushort __shift_left_sat(ushort, uchar);
ushort2 __shift_left_sat(ushort2, uchar2);
/* UNSUPPORTED: ushort3 __shift_left_sat(ushort3, uchar3); */
ushort4 __shift_left_sat(ushort4, uchar4);
ushort8 __shift_left_sat(ushort8, uchar8);
ushort16 __shift_left_sat(ushort16, uchar16);
ushort32 __shift_left_sat(ushort32, uchar32);

/* VSSHLUW */
uint __shift_left_sat(uint, uint);
uint2 __shift_left_sat(uint2, uint2);
/* UNSUPPORTED: uint3 __shift_left_sat(uint3, uint3); */
uint4 __shift_left_sat(uint4, uint4);
uint8 __shift_left_sat(uint8, uint8);
uint16 __shift_left_sat(uint16, uint16);
uint __shift_left_sat(uint, uchar);
uint2 __shift_left_sat(uint2, uchar2);
/* UNSUPPORTED: uint3 __shift_left_sat(uint3, uchar3); */
uint4 __shift_left_sat(uint4, uchar4);
uint8 __shift_left_sat(uint8, uchar8);
uint16 __shift_left_sat(uint16, uchar16);

/* VSSHLW */
int32_t __shift_left_sat(int32_t, uint);
int2 __shift_left_sat(int2, uint2);
/* UNSUPPORTED: int3 __shift_left_sat(int3, uint3); */
int4 __shift_left_sat(int4, uint4);
int8 __shift_left_sat(int8, uint8);
int16 __shift_left_sat(int16, uint16);
int32_t __shift_left_sat(int32_t, uchar);
int2 __shift_left_sat(int2, uchar2);
/* UNSUPPORTED: int3 __shift_left_sat(int3, uchar3); */
int4 __shift_left_sat(int4, uchar4);
int8 __shift_left_sat(int8, uchar8);
int16 __shift_left_sat(int16, uchar16);




/*-----------------------------------------------------------------------------
* ID: __shift_left_sat_to_unsigned
*----------------------------------------------------------------------------*/

/* VSSHLSUH */
ushort2 __shift_left_sat_to_unsigned(short2, uint);
ushort4 __shift_left_sat_to_unsigned(short4, uint2);
ushort8 __shift_left_sat_to_unsigned(short8, uint4);
ushort16 __shift_left_sat_to_unsigned(short16, uint8);
ushort32 __shift_left_sat_to_unsigned(short32, uint16);
ushort __shift_left_sat_to_unsigned(int16_t, uchar);
ushort2 __shift_left_sat_to_unsigned(short2, uchar2);
/* UNSUPPORTED: ushort3 __shift_left_sat_to_unsigned(short3, uchar3); */
ushort4 __shift_left_sat_to_unsigned(short4, uchar4);
ushort8 __shift_left_sat_to_unsigned(short8, uchar8);
ushort16 __shift_left_sat_to_unsigned(short16, uchar16);
ushort32 __shift_left_sat_to_unsigned(short32, uchar32);

/* VSSHLSUW */
uint __shift_left_sat_to_unsigned(int32_t, uint);
uint2 __shift_left_sat_to_unsigned(int2, uint2);
/* UNSUPPORTED: uint3 __shift_left_sat_to_unsigned(int3, uint3); */
uint4 __shift_left_sat_to_unsigned(int4, uint4);
uint8 __shift_left_sat_to_unsigned(int8, uint8);
uint16 __shift_left_sat_to_unsigned(int16, uint16);
uint __shift_left_sat_to_unsigned(int32_t, uchar);
uint2 __shift_left_sat_to_unsigned(int2, uchar2);
/* UNSUPPORTED: uint3 __shift_left_sat_to_unsigned(int3, uchar3); */
uint4 __shift_left_sat_to_unsigned(int4, uchar4);
uint8 __shift_left_sat_to_unsigned(int8, uchar8);
uint16 __shift_left_sat_to_unsigned(int16, uchar16);




/*-----------------------------------------------------------------------------
* ID: __shift_left_var
*----------------------------------------------------------------------------*/

/* VSHVLUW */
uint __shift_left_var(uint, int32_t);
uint2 __shift_left_var(uint2, int2);
/* UNSUPPORTED: uint3 __shift_left_var(uint3, int3); */
uint4 __shift_left_var(uint4, int4);
uint8 __shift_left_var(uint8, int8);
uint16 __shift_left_var(uint16, int16);

/* VSHVLW */
int32_t __shift_left_var(int32_t, int32_t);
int2 __shift_left_var(int2, int2);
/* UNSUPPORTED: int3 __shift_left_var(int3, int3); */
int4 __shift_left_var(int4, int4);
int8 __shift_left_var(int8, int8);
int16 __shift_left_var(int16, int16);




/*-----------------------------------------------------------------------------
* ID: __shift_left_var_sat
*----------------------------------------------------------------------------*/

/* VSSHVLW */
int32_t __shift_left_var_sat(int32_t, int32_t);
int2 __shift_left_var_sat(int2, int2);
/* UNSUPPORTED: int3 __shift_left_var_sat(int3, int3); */
int4 __shift_left_var_sat(int4, int4);
int8 __shift_left_var_sat(int8, int8);
int16 __shift_left_var_sat(int16, int16);




/*-----------------------------------------------------------------------------
* ID: __shift_right
*----------------------------------------------------------------------------*/

/* SHRD */
int64_t __shift_right(int64_t, int32_t);
int64_t __shift_right(int64_t, uchar);

/* SHRUD */
ulong __shift_right(ulong, uint);
ulong __shift_right(ulong, uchar);

/* SHRUW */
uint __shift_right(uint, uint);
uint __shift_right(uint, uchar);

/* SHRW */
int32_t __shift_right(int32_t, int32_t);
int32_t __shift_right(int32_t, uchar);

/* VSHRB */
char4 __shift_right(char4, int32_t);
char8 __shift_right(char8, int2);
char16 __shift_right(char16, int4);
char32 __shift_right(char32, int8);
char64 __shift_right(char64, int16);
int8_t __shift_right(int8_t, int8_t);
char2 __shift_right(char2, char2);
/* UNSUPPORTED: char3 __shift_right(char3, char3); */
char4 __shift_right(char4, char4);
char8 __shift_right(char8, char8);
char16 __shift_right(char16, char16);
char32 __shift_right(char32, char32);
char64 __shift_right(char64, char64);

/* VSHRD */
int64_t __shift_right(int64_t, int64_t);
long2 __shift_right(long2, long2);
/* UNSUPPORTED: long3 __shift_right(long3, long3); */
long4 __shift_right(long4, long4);
long8 __shift_right(long8, long8);
/* CONSTANT: int64_t __shift_right(int64_t, (int64_t)(k)); */
/* CONSTANT: long2 __shift_right(long2, (long2)(k)); */
/* UNSUPPORTED: long3 __shift_right(long3, (long3)(k)); */
/* CONSTANT: long4 __shift_right(long4, (long4)(k)); */
/* CONSTANT: long8 __shift_right(long8, (long8)(k)); */

/* VSHRH */
short2 __shift_right(short2, int32_t);
short4 __shift_right(short4, int2);
short8 __shift_right(short8, int4);
short16 __shift_right(short16, int8);
short32 __shift_right(short32, int16);
int16_t __shift_right(int16_t, int16_t);
short2 __shift_right(short2, short2);
/* UNSUPPORTED: short3 __shift_right(short3, short3); */
short4 __shift_right(short4, short4);
short8 __shift_right(short8, short8);
short16 __shift_right(short16, short16);
short32 __shift_right(short32, short32);

/* VSHRUB */
uchar4 __shift_right(uchar4, uint);
uchar8 __shift_right(uchar8, uint2);
uchar16 __shift_right(uchar16, uint4);
uchar32 __shift_right(uchar32, uint8);
uchar64 __shift_right(uchar64, uint16);
uchar __shift_right(uchar, uchar);
uchar2 __shift_right(uchar2, uchar2);
/* UNSUPPORTED: uchar3 __shift_right(uchar3, uchar3); */
uchar4 __shift_right(uchar4, uchar4);
uchar8 __shift_right(uchar8, uchar8);
uchar16 __shift_right(uchar16, uchar16);
uchar32 __shift_right(uchar32, uchar32);
uchar64 __shift_right(uchar64, uchar64);

/* VSHRUD */
ulong __shift_right(ulong, ulong);
ulong2 __shift_right(ulong2, ulong2);
/* UNSUPPORTED: ulong3 __shift_right(ulong3, ulong3); */
ulong4 __shift_right(ulong4, ulong4);
ulong8 __shift_right(ulong8, ulong8);
/* CONSTANT: ulong __shift_right(ulong, (ulong)(k)); */
/* CONSTANT: ulong2 __shift_right(ulong2, (ulong2)(k)); */
/* UNSUPPORTED: ulong3 __shift_right(ulong3, (ulong3)(k)); */
/* CONSTANT: ulong4 __shift_right(ulong4, (ulong4)(k)); */
/* CONSTANT: ulong8 __shift_right(ulong8, (ulong8)(k)); */

/* VSHRUH */
ushort2 __shift_right(ushort2, uint);
ushort4 __shift_right(ushort4, uint2);
ushort8 __shift_right(ushort8, uint4);
ushort16 __shift_right(ushort16, uint8);
ushort32 __shift_right(ushort32, uint16);
ushort __shift_right(ushort, ushort);
ushort2 __shift_right(ushort2, ushort2);
/* UNSUPPORTED: ushort3 __shift_right(ushort3, ushort3); */
ushort4 __shift_right(ushort4, ushort4);
ushort8 __shift_right(ushort8, ushort8);
ushort16 __shift_right(ushort16, ushort16);
ushort32 __shift_right(ushort32, ushort32);

/* VSHRUW */
uint __shift_right(uint, uint);
uint2 __shift_right(uint2, uint2);
/* UNSUPPORTED: uint3 __shift_right(uint3, uint3); */
uint4 __shift_right(uint4, uint4);
uint8 __shift_right(uint8, uint8);
uint16 __shift_right(uint16, uint16);
/* CONSTANT: uint __shift_right(uint, (uint)(k)); */
/* CONSTANT: uint2 __shift_right(uint2, (uint2)(k)); */
/* UNSUPPORTED: uint3 __shift_right(uint3, (uint3)(k)); */
/* CONSTANT: uint4 __shift_right(uint4, (uint4)(k)); */
/* CONSTANT: uint8 __shift_right(uint8, (uint8)(k)); */
/* CONSTANT: uint16 __shift_right(uint16, (uint16)(k)); */

/* VSHRW */
int32_t __shift_right(int32_t, int32_t);
int2 __shift_right(int2, int2);
/* UNSUPPORTED: int3 __shift_right(int3, int3); */
int4 __shift_right(int4, int4);
int8 __shift_right(int8, int8);
int16 __shift_right(int16, int16);
/* CONSTANT: int32_t __shift_right(int32_t, (int32_t)(k)); */
/* CONSTANT: int2 __shift_right(int2, (int2)(k)); */
/* UNSUPPORTED: int3 __shift_right(int3, (int3)(k)); */
/* CONSTANT: int4 __shift_right(int4, (int4)(k)); */
/* CONSTANT: int8 __shift_right(int8, (int8)(k)); */
/* CONSTANT: int16 __shift_right(int16, (int16)(k)); */




/*-----------------------------------------------------------------------------
* ID: __shift_right_full
*----------------------------------------------------------------------------*/

/* VSHR */
long8 __shift_right_full(long8, uchar);
long8 __shift_right_full(long8, ulong8);

/* VSHRU */
ulong8 __shift_right_full(ulong8, uchar);
ulong8 __shift_right_full(ulong8, ulong8);




/*-----------------------------------------------------------------------------
* ID: __shift_right_merge
*----------------------------------------------------------------------------*/

/* VSHRM16B */
char64 __shift_right_merge(char64, char16);

/* VSHRM1B */
char64 __shift_right_merge(char64, int8_t);

/* VSHRM2B */
char64 __shift_right_merge(char64, char2);

/* VSHRM4B */
char64 __shift_right_merge(char64, char4);

/* VSHRM8B */
char64 __shift_right_merge(char64, char8);

/* VSHRMB */
int32_t __shift_right_merge(int32_t, char4);
int2 __shift_right_merge(int2, char8);
int4 __shift_right_merge(int4, char16);
int8 __shift_right_merge(int8, char32);
int16 __shift_right_merge(int16, char64);




/*-----------------------------------------------------------------------------
* ID: __shift_right_round
*----------------------------------------------------------------------------*/

/* VSHRRB */
char4 __shift_right_round(char4, uint);
char8 __shift_right_round(char8, uint2);
char16 __shift_right_round(char16, uint4);
char32 __shift_right_round(char32, uint8);
char64 __shift_right_round(char64, uint16);
int8_t __shift_right_round(int8_t, uchar);
char2 __shift_right_round(char2, uchar2);
/* UNSUPPORTED: char3 __shift_right_round(char3, uchar3); */
char4 __shift_right_round(char4, uchar4);
char8 __shift_right_round(char8, uchar8);
char16 __shift_right_round(char16, uchar16);
char32 __shift_right_round(char32, uchar32);
char64 __shift_right_round(char64, uchar64);

/* VSHRRD */
int64_t __shift_right_round(int64_t, ulong);
long2 __shift_right_round(long2, ulong2);
/* UNSUPPORTED: long3 __shift_right_round(long3, ulong3); */
long4 __shift_right_round(long4, ulong4);
long8 __shift_right_round(long8, ulong8);
int64_t __shift_right_round(int64_t, uchar);
long2 __shift_right_round(long2, uchar2);
/* UNSUPPORTED: long3 __shift_right_round(long3, uchar3); */
long4 __shift_right_round(long4, uchar4);
long8 __shift_right_round(long8, uchar8);

/* VSHRRH */
short2 __shift_right_round(short2, uint);
short4 __shift_right_round(short4, uint2);
short8 __shift_right_round(short8, uint4);
short16 __shift_right_round(short16, uint8);
short32 __shift_right_round(short32, uint16);
int16_t __shift_right_round(int16_t, uchar);
short2 __shift_right_round(short2, uchar2);
/* UNSUPPORTED: short3 __shift_right_round(short3, uchar3); */
short4 __shift_right_round(short4, uchar4);
short8 __shift_right_round(short8, uchar8);
short16 __shift_right_round(short16, uchar16);
short32 __shift_right_round(short32, uchar32);

/* VSHRRW */
int32_t __shift_right_round(int32_t, uint);
int2 __shift_right_round(int2, uint2);
/* UNSUPPORTED: int3 __shift_right_round(int3, uint3); */
int4 __shift_right_round(int4, uint4);
int8 __shift_right_round(int8, uint8);
int16 __shift_right_round(int16, uint16);
int32_t __shift_right_round(int32_t, uchar);
int2 __shift_right_round(int2, uchar2);
/* UNSUPPORTED: int3 __shift_right_round(int3, uchar3); */
int4 __shift_right_round(int4, uchar4);
int8 __shift_right_round(int8, uchar8);
int16 __shift_right_round(int16, uchar16);

/* VSHRURB */
uchar4 __shift_right_round(uchar4, uint);
uchar8 __shift_right_round(uchar8, uint2);
uchar16 __shift_right_round(uchar16, uint4);
uchar32 __shift_right_round(uchar32, uint8);
uchar64 __shift_right_round(uchar64, uint16);
uchar __shift_right_round(uchar, uchar);
uchar2 __shift_right_round(uchar2, uchar2);
/* UNSUPPORTED: uchar3 __shift_right_round(uchar3, uchar3); */
uchar4 __shift_right_round(uchar4, uchar4);
uchar8 __shift_right_round(uchar8, uchar8);
uchar16 __shift_right_round(uchar16, uchar16);
uchar32 __shift_right_round(uchar32, uchar32);
uchar64 __shift_right_round(uchar64, uchar64);

/* VSHRURD */
ulong __shift_right_round(ulong, ulong);
ulong2 __shift_right_round(ulong2, ulong2);
/* UNSUPPORTED: ulong3 __shift_right_round(ulong3, ulong3); */
ulong4 __shift_right_round(ulong4, ulong4);
ulong8 __shift_right_round(ulong8, ulong8);
ulong __shift_right_round(ulong, uchar);
ulong2 __shift_right_round(ulong2, uchar2);
/* UNSUPPORTED: ulong3 __shift_right_round(ulong3, uchar3); */
ulong4 __shift_right_round(ulong4, uchar4);
ulong8 __shift_right_round(ulong8, uchar8);

/* VSHRURH */
ushort2 __shift_right_round(ushort2, uint);
ushort4 __shift_right_round(ushort4, uint2);
ushort8 __shift_right_round(ushort8, uint4);
ushort16 __shift_right_round(ushort16, uint8);
ushort32 __shift_right_round(ushort32, uint16);
ushort __shift_right_round(ushort, uchar);
ushort2 __shift_right_round(ushort2, uchar2);
/* UNSUPPORTED: ushort3 __shift_right_round(ushort3, uchar3); */
ushort4 __shift_right_round(ushort4, uchar4);
ushort8 __shift_right_round(ushort8, uchar8);
ushort16 __shift_right_round(ushort16, uchar16);
ushort32 __shift_right_round(ushort32, uchar32);

/* VSHRURW */
uint __shift_right_round(uint, uint);
uint2 __shift_right_round(uint2, uint2);
/* UNSUPPORTED: uint3 __shift_right_round(uint3, uint3); */
uint4 __shift_right_round(uint4, uint4);
uint8 __shift_right_round(uint8, uint8);
uint16 __shift_right_round(uint16, uint16);
uint __shift_right_round(uint, uchar);
uint2 __shift_right_round(uint2, uchar2);
/* UNSUPPORTED: uint3 __shift_right_round(uint3, uchar3); */
uint4 __shift_right_round(uint4, uchar4);
uint8 __shift_right_round(uint8, uchar8);
uint16 __shift_right_round(uint16, uchar16);




/*-----------------------------------------------------------------------------
* ID: __shift_right_var
*----------------------------------------------------------------------------*/

/* VSHVRUW */
uint __shift_right_var(uint, int32_t);
uint2 __shift_right_var(uint2, int2);
/* UNSUPPORTED: uint3 __shift_right_var(uint3, int3); */
uint4 __shift_right_var(uint4, int4);
uint8 __shift_right_var(uint8, int8);
uint16 __shift_right_var(uint16, int16);

/* VSHVRW */
int32_t __shift_right_var(int32_t, int32_t);
int2 __shift_right_var(int2, int2);
/* UNSUPPORTED: int3 __shift_right_var(int3, int3); */
int4 __shift_right_var(int4, int4);
int8 __shift_right_var(int8, int8);
int16 __shift_right_var(int16, int16);




/*-----------------------------------------------------------------------------
* ID: __shift_right_var_sat
*----------------------------------------------------------------------------*/

/* VSSHVRW */
int32_t __shift_right_var_sat(int32_t, int32_t);
int2 __shift_right_var_sat(int2, int2);
/* UNSUPPORTED: int3 __shift_right_var_sat(int3, int3); */
int4 __shift_right_var_sat(int4, int4);
int8 __shift_right_var_sat(int8, int8);
int16 __shift_right_var_sat(int16, int16);




/*-----------------------------------------------------------------------------
* ID: __shld_dkd
*----------------------------------------------------------------------------*/

/* SHLD */
int64_t __shld_dkd(int64_t, uchar);
ulong __shld_dkd(ulong, uchar);




/*-----------------------------------------------------------------------------
* ID: __shld_drd
*----------------------------------------------------------------------------*/

/* SHLD */
int64_t __shld_drd(int64_t, int32_t);
ulong __shld_drd(ulong, uint);




/*-----------------------------------------------------------------------------
* ID: __shlw_rkr
*----------------------------------------------------------------------------*/

/* SHLW */
int32_t __shlw_rkr(int32_t, uchar);
uint __shlw_rkr(uint, uchar);




/*-----------------------------------------------------------------------------
* ID: __shlw_rrr
*----------------------------------------------------------------------------*/

/* SHLW */
int32_t __shlw_rrr(int32_t, int32_t);
uint __shlw_rrr(uint, uint);




/*-----------------------------------------------------------------------------
* ID: __short_to_char_sat
*----------------------------------------------------------------------------*/

/* VSATHB */
int16_t __short_to_char_sat(int16_t);
short2 __short_to_char_sat(short2);
/* UNSUPPORTED: short3 __short_to_char_sat(short3); */
short4 __short_to_char_sat(short4);
short8 __short_to_char_sat(short8);
short16 __short_to_char_sat(short16);
short32 __short_to_char_sat(short32);




/*-----------------------------------------------------------------------------
* ID: __shrd_dkd
*----------------------------------------------------------------------------*/

/* SHRD */
int64_t __shrd_dkd(int64_t, uchar);




/*-----------------------------------------------------------------------------
* ID: __shrd_drd
*----------------------------------------------------------------------------*/

/* SHRD */
int64_t __shrd_drd(int64_t, int32_t);




/*-----------------------------------------------------------------------------
* ID: __shrud_dkd
*----------------------------------------------------------------------------*/

/* SHRUD */
ulong __shrud_dkd(ulong, uchar);




/*-----------------------------------------------------------------------------
* ID: __shrud_drd
*----------------------------------------------------------------------------*/

/* SHRUD */
ulong __shrud_drd(ulong, uint);




/*-----------------------------------------------------------------------------
* ID: __shruw_rkr
*----------------------------------------------------------------------------*/

/* SHRUW */
uint __shruw_rkr(uint, uchar);




/*-----------------------------------------------------------------------------
* ID: __shruw_rrr
*----------------------------------------------------------------------------*/

/* SHRUW */
uint __shruw_rrr(uint, uint);




/*-----------------------------------------------------------------------------
* ID: __shrw_rkr
*----------------------------------------------------------------------------*/

/* SHRW */
int32_t __shrw_rkr(int32_t, uchar);




/*-----------------------------------------------------------------------------
* ID: __shrw_rrr
*----------------------------------------------------------------------------*/

/* SHRW */
int32_t __shrw_rrr(int32_t, int32_t);




/*-----------------------------------------------------------------------------
* ID: __shuffle_3way
*----------------------------------------------------------------------------*/

/* VSHFL3 */
ulong __shuffle_3way(uint2, uint2);
ulong2 __shuffle_3way(uint4, uint4);
ulong4 __shuffle_3way(uint8, uint8);
ulong8 __shuffle_3way(uint16, uint16);
int64_t __shuffle_3way(int2, int2);
long2 __shuffle_3way(int4, int4);
long4 __shuffle_3way(int8, int8);
long8 __shuffle_3way(int16, int16);




/*-----------------------------------------------------------------------------
* ID: __shuffle_bit
*----------------------------------------------------------------------------*/

/* VBITSHFLD */
int64_t __shuffle_bit(int64_t);
long2 __shuffle_bit(long2);
/* UNSUPPORTED: long3 __shuffle_bit(long3); */
long4 __shuffle_bit(long4);
long8 __shuffle_bit(long8);
ulong __shuffle_bit(ulong);
ulong2 __shuffle_bit(ulong2);
/* UNSUPPORTED: ulong3 __shuffle_bit(ulong3); */
ulong4 __shuffle_bit(ulong4);
ulong8 __shuffle_bit(ulong8);

/* VBITSHFLW */
int32_t __shuffle_bit(int32_t);
int2 __shuffle_bit(int2);
/* UNSUPPORTED: int3 __shuffle_bit(int3); */
int4 __shuffle_bit(int4);
int8 __shuffle_bit(int8);
int16 __shuffle_bit(int16);
uint __shuffle_bit(uint);
uint2 __shuffle_bit(uint2);
/* UNSUPPORTED: uint3 __shuffle_bit(uint3); */
uint4 __shuffle_bit(uint4);
uint8 __shuffle_bit(uint8);
uint16 __shuffle_bit(uint16);




/*-----------------------------------------------------------------------------
* ID: __shuffle_stride2
*----------------------------------------------------------------------------*/

/* VSHFL2B */
char64 __shuffle_stride2(char64);
uchar64 __shuffle_stride2(uchar64);

/* VSHFL2D */
long8 __shuffle_stride2(long8);
double8 __shuffle_stride2(double8);
cfloat8 __shuffle_stride2(cfloat8);
ulong8 __shuffle_stride2(ulong8);
cint8 __shuffle_stride2(cint8);

/* VSHFL2H */
short32 __shuffle_stride2(short32);
ushort32 __shuffle_stride2(ushort32);
cchar32 __shuffle_stride2(cchar32);

/* VSHFL2W */
int16 __shuffle_stride2(int16);
float16 __shuffle_stride2(float16);
uint16 __shuffle_stride2(uint16);
cshort16 __shuffle_stride2(cshort16);




/*-----------------------------------------------------------------------------
* ID: __shuffle_stride2_even_even
*----------------------------------------------------------------------------*/

/* VSHFL2DEE */
long8 __shuffle_stride2_even_even(long8, long8);
double8 __shuffle_stride2_even_even(double8, double8);
cfloat8 __shuffle_stride2_even_even(cfloat8, cfloat8);
ulong8 __shuffle_stride2_even_even(ulong8, ulong8);
cint8 __shuffle_stride2_even_even(cint8, cint8);




/*-----------------------------------------------------------------------------
* ID: __shuffle_stride2_even_odd
*----------------------------------------------------------------------------*/

/* VSHFL2DEO */
long8 __shuffle_stride2_even_odd(long8, long8);
double8 __shuffle_stride2_even_odd(double8, double8);
cfloat8 __shuffle_stride2_even_odd(cfloat8, cfloat8);
ulong8 __shuffle_stride2_even_odd(ulong8, ulong8);
cint8 __shuffle_stride2_even_odd(cint8, cint8);




/*-----------------------------------------------------------------------------
* ID: __shuffle_stride2_high_high
*----------------------------------------------------------------------------*/

/* VSHFL2DHH */
long8 __shuffle_stride2_high_high(long8, long8);
double8 __shuffle_stride2_high_high(double8, double8);
cfloat8 __shuffle_stride2_high_high(cfloat8, cfloat8);
ulong8 __shuffle_stride2_high_high(ulong8, ulong8);
cint8 __shuffle_stride2_high_high(cint8, cint8);

/* VSHFL2HHH */
short32 __shuffle_stride2_high_high(short32, short32);
ushort32 __shuffle_stride2_high_high(ushort32, ushort32);
cchar32 __shuffle_stride2_high_high(cchar32, cchar32);

/* VSHFL2WHH */
int16 __shuffle_stride2_high_high(int16, int16);
float16 __shuffle_stride2_high_high(float16, float16);
uint16 __shuffle_stride2_high_high(uint16, uint16);
cshort16 __shuffle_stride2_high_high(cshort16, cshort16);




/*-----------------------------------------------------------------------------
* ID: __shuffle_stride2_low_high
*----------------------------------------------------------------------------*/

/* VSHFL2DLH */
long8 __shuffle_stride2_low_high(long8, long8);
double8 __shuffle_stride2_low_high(double8, double8);
cfloat8 __shuffle_stride2_low_high(cfloat8, cfloat8);
ulong8 __shuffle_stride2_low_high(ulong8, ulong8);
cint8 __shuffle_stride2_low_high(cint8, cint8);




/*-----------------------------------------------------------------------------
* ID: __shuffle_stride2_low_low
*----------------------------------------------------------------------------*/

/* VSHFL2DLL */
long8 __shuffle_stride2_low_low(long8, long8);
double8 __shuffle_stride2_low_low(double8, double8);
cfloat8 __shuffle_stride2_low_low(cfloat8, cfloat8);
ulong8 __shuffle_stride2_low_low(ulong8, ulong8);
cint8 __shuffle_stride2_low_low(cint8, cint8);

/* VSHFL2HLL */
short32 __shuffle_stride2_low_low(short32, short32);
ushort32 __shuffle_stride2_low_low(ushort32, ushort32);
cchar32 __shuffle_stride2_low_low(cchar32, cchar32);

/* VSHFL2WLL */
int16 __shuffle_stride2_low_low(int16, int16);
float16 __shuffle_stride2_low_low(float16, float16);
uint16 __shuffle_stride2_low_low(uint16, uint16);
cshort16 __shuffle_stride2_low_low(cshort16, cshort16);




/*-----------------------------------------------------------------------------
* ID: __shuffle (Convenience form of __shuffle_stride2_low_low)
*----------------------------------------------------------------------------*/

/* VSHFL2DLL */
long8 __shuffle(long4, long4);
double8 __shuffle(double4, double4);
cfloat8 __shuffle(cfloat4, cfloat4);
ulong8 __shuffle(ulong4, ulong4);
cint8 __shuffle(cint4, cint4);

/* VSHFL2HLL */
short32 __shuffle(short16, short16);
ushort32 __shuffle(ushort16, ushort16);
cchar32 __shuffle(cchar16, cchar16);

/* VSHFL2WLL */
int16 __shuffle(int8, int8);
float16 __shuffle(float8, float8);
uint16 __shuffle(uint8, uint8);
cshort16 __shuffle(cshort8, cshort8);




/*-----------------------------------------------------------------------------
* ID: __shuffle_stride2_odd_odd
*----------------------------------------------------------------------------*/

/* VSHFL2DOO */
long8 __shuffle_stride2_odd_odd(long8, long8);
double8 __shuffle_stride2_odd_odd(double8, double8);
cfloat8 __shuffle_stride2_odd_odd(cfloat8, cfloat8);
ulong8 __shuffle_stride2_odd_odd(ulong8, ulong8);
cint8 __shuffle_stride2_odd_odd(cint8, cint8);




/*-----------------------------------------------------------------------------
* ID: __shuffle_stride4
*----------------------------------------------------------------------------*/

/* VSHFL4B */
char64 __shuffle_stride4(char64);
uchar64 __shuffle_stride4(uchar64);

/* VSHFL4D */
long8 __shuffle_stride4(long8);
double8 __shuffle_stride4(double8);
cfloat8 __shuffle_stride4(cfloat8);
ulong8 __shuffle_stride4(ulong8);
cint8 __shuffle_stride4(cint8);

/* VSHFL4H */
short32 __shuffle_stride4(short32);
ushort32 __shuffle_stride4(ushort32);
cchar32 __shuffle_stride4(cchar32);

/* VSHFL4W */
int16 __shuffle_stride4(int16);
float16 __shuffle_stride4(float16);
uint16 __shuffle_stride4(uint16);
cshort16 __shuffle_stride4(cshort16);




/*-----------------------------------------------------------------------------
* ID: __sll_operator
*----------------------------------------------------------------------------*/
/*

SHLD
long = long << int;
long = long << (uchar)(k);
ulong = ulong << uint;
ulong = ulong << (uchar)(k);

SHLW
int = int << int;
int = int << (uchar)(k);
uint = uint << uint;
uint = uint << (uchar)(k);

VSHLD
long = long << long;
long2 = long2 << long2;
long3 = long3 << long3;
long4 = long4 << long4;
long8 = long8 << long8;
long = long << (long)(k);
long2 = long2 << (long2)(k);
long3 = long3 << (long3)(k);
long4 = long4 << (long4)(k);
long8 = long8 << (long8)(k);
ulong = ulong << ulong;
ulong2 = ulong2 << ulong2;
ulong3 = ulong3 << ulong3;
ulong4 = ulong4 << ulong4;
ulong8 = ulong8 << ulong8;
ulong = ulong << (ulong)(k);
ulong2 = ulong2 << (ulong2)(k);
ulong3 = ulong3 << (ulong3)(k);
ulong4 = ulong4 << (ulong4)(k);
ulong8 = ulong8 << (ulong8)(k);

VSHLW
int = int << int;
int2 = int2 << int2;
int3 = int3 << int3;
int4 = int4 << int4;
int8 = int8 << int8;
int16 = int16 << int16;
int = int << (int)(k);
int2 = int2 << (int2)(k);
int3 = int3 << (int3)(k);
int4 = int4 << (int4)(k);
int8 = int8 << (int8)(k);
int16 = int16 << (int16)(k);
uint = uint << uint;
uint2 = uint2 << uint2;
uint3 = uint3 << uint3;
uint4 = uint4 << uint4;
uint8 = uint8 << uint8;
uint16 = uint16 << uint16;
uint = uint << (uint)(k);
uint2 = uint2 << (uint2)(k);
uint3 = uint3 << (uint3)(k);
uint4 = uint4 << (uint4)(k);
uint8 = uint8 << (uint8)(k);
uint16 = uint16 << (uint16)(k);

*/


/*-----------------------------------------------------------------------------
* ID: __smpyhw_rrr
*----------------------------------------------------------------------------*/

/* SMPYHW */
int32_t __smpyhw_rrr(int16_t, int16_t);




/*-----------------------------------------------------------------------------
* ID: __sort_asc
*----------------------------------------------------------------------------*/

/* VSORTI16SP */
float16 __sort_asc(float16);

/* VSORTI16W */
int16 __sort_asc(int16);

/* VSORTIU16W */
uint16 __sort_asc(uint16);




/*-----------------------------------------------------------------------------
* ID: __sort_asc_perm
*----------------------------------------------------------------------------*/

/* VSORTPI16SP */
uchar64 __sort_asc_perm(float16);

/* VSORTPI16W */
uchar64 __sort_asc_perm(int16);

/* VSORTPIU16W */
uchar64 __sort_asc_perm(uint16);




/*-----------------------------------------------------------------------------
* ID: __sort_desc
*----------------------------------------------------------------------------*/

/* VSORTD16SP */
float16 __sort_desc(float16);

/* VSORTD16W */
int16 __sort_desc(int16);

/* VSORTDU16W */
uint16 __sort_desc(uint16);




/*-----------------------------------------------------------------------------
* ID: __sort_desc_perm
*----------------------------------------------------------------------------*/

/* VSORTPD16SP */
uchar64 __sort_desc_perm(float16);

/* VSORTPD16W */
uchar64 __sort_desc_perm(int16);

/* VSORTPDU16W */
uchar64 __sort_desc_perm(uint16);




/*-----------------------------------------------------------------------------
* ID: __srl_operator
*----------------------------------------------------------------------------*/
/*

SHRD
long = long >> int;
long = long >> (uchar)(k);

SHRUD
ulong = ulong >> uint;
ulong = ulong >> (uchar)(k);

SHRUW
uint = uint >> uint;
uint = uint >> (uchar)(k);

SHRW
int = int >> int;
int = int >> (uchar)(k);

VSHRD
long = long >> long;
long2 = long2 >> long2;
long3 = long3 >> long3;
long4 = long4 >> long4;
long8 = long8 >> long8;
long = long >> (long)(k);
long2 = long2 >> (long2)(k);
long3 = long3 >> (long3)(k);
long4 = long4 >> (long4)(k);
long8 = long8 >> (long8)(k);

VSHRUD
ulong = ulong >> ulong;
ulong2 = ulong2 >> ulong2;
ulong3 = ulong3 >> ulong3;
ulong4 = ulong4 >> ulong4;
ulong8 = ulong8 >> ulong8;
ulong = ulong >> (ulong)(k);
ulong2 = ulong2 >> (ulong2)(k);
ulong3 = ulong3 >> (ulong3)(k);
ulong4 = ulong4 >> (ulong4)(k);
ulong8 = ulong8 >> (ulong8)(k);

VSHRUW
uint = uint >> uint;
uint2 = uint2 >> uint2;
uint3 = uint3 >> uint3;
uint4 = uint4 >> uint4;
uint8 = uint8 >> uint8;
uint16 = uint16 >> uint16;
uint = uint >> (uint)(k);
uint2 = uint2 >> (uint2)(k);
uint3 = uint3 >> (uint3)(k);
uint4 = uint4 >> (uint4)(k);
uint8 = uint8 >> (uint8)(k);
uint16 = uint16 >> (uint16)(k);

VSHRW
int = int >> int;
int2 = int2 >> int2;
int3 = int3 >> int3;
int4 = int4 >> int4;
int8 = int8 >> int8;
int16 = int16 >> int16;
int = int >> (int)(k);
int2 = int2 >> (int2)(k);
int3 = int3 >> (int3)(k);
int4 = int4 >> (int4)(k);
int8 = int8 >> (int8)(k);
int16 = int16 >> (int16)(k);

*/


/*-----------------------------------------------------------------------------
* ID: __sub_cond
*----------------------------------------------------------------------------*/

/* VSUBCW */
uint __sub_cond(uint, uint);
uint2 __sub_cond(uint2, uint2);
/* UNSUPPORTED: uint3 __sub_cond(uint3, uint3); */
uint4 __sub_cond(uint4, uint4);
uint8 __sub_cond(uint8, uint8);
uint16 __sub_cond(uint16, uint16);




/*-----------------------------------------------------------------------------
* ID: __sub_operator
*----------------------------------------------------------------------------*/
/*

SUBD
long = long - long;
ulong = ulong - ulong;

SUBDP
double = double - double;

SUBSP
float = float - float;

SUBW
int = int - int;
uint = uint - uint;

VSUBB
char = char - char;
char2 = char2 - char2;
char3 = char3 - char3;
char4 = char4 - char4;
char8 = char8 - char8;
char16 = char16 - char16;
char32 = char32 - char32;
char64 = char64 - char64;
char = char - (char)(k);
char2 = char2 - (char2)(k);
char3 = char3 - (char3)(k);
char4 = char4 - (char4)(k);
char8 = char8 - (char8)(k);
char16 = char16 - (char16)(k);
char32 = char32 - (char32)(k);
char64 = char64 - (char64)(k);
cchar = cchar - cchar;
cchar2 = cchar2 - cchar2;
cchar4 = cchar4 - cchar4;
cchar8 = cchar8 - cchar8;
cchar16 = cchar16 - cchar16;
cchar32 = cchar32 - cchar32;
uchar = uchar - uchar;
uchar2 = uchar2 - uchar2;
uchar3 = uchar3 - uchar3;
uchar4 = uchar4 - uchar4;
uchar8 = uchar8 - uchar8;
uchar16 = uchar16 - uchar16;
uchar32 = uchar32 - uchar32;
uchar64 = uchar64 - uchar64;
cchar = cchar - (cchar)(k);
cchar2 = cchar2 - (cchar2)(k);
cchar4 = cchar4 - (cchar4)(k);
cchar8 = cchar8 - (cchar8)(k);
cchar16 = cchar16 - (cchar16)(k);
cchar32 = cchar32 - (cchar32)(k);
uchar = uchar - (uchar)(k);
uchar2 = uchar2 - (uchar2)(k);
uchar3 = uchar3 - (uchar3)(k);
uchar4 = uchar4 - (uchar4)(k);
uchar8 = uchar8 - (uchar8)(k);
uchar16 = uchar16 - (uchar16)(k);
uchar32 = uchar32 - (uchar32)(k);
uchar64 = uchar64 - (uchar64)(k);

VSUBD
long = long - long;
long2 = long2 - long2;
long3 = long3 - long3;
long4 = long4 - long4;
long8 = long8 - long8;
long = long - (long)(k);
long2 = long2 - (long2)(k);
long3 = long3 - (long3)(k);
long4 = long4 - (long4)(k);
long8 = long8 - (long8)(k);
clong = clong - clong;
clong2 = clong2 - clong2;
clong4 = clong4 - clong4;
ulong = ulong - ulong;
ulong2 = ulong2 - ulong2;
ulong3 = ulong3 - ulong3;
ulong4 = ulong4 - ulong4;
ulong8 = ulong8 - ulong8;
clong = clong - (clong)(k);
clong2 = clong2 - (clong2)(k);
clong4 = clong4 - (clong4)(k);
ulong = ulong - (ulong)(k);
ulong2 = ulong2 - (ulong2)(k);
ulong3 = ulong3 - (ulong3)(k);
ulong4 = ulong4 - (ulong4)(k);
ulong8 = ulong8 - (ulong8)(k);

VSUBDP
double = double - double;
double2 = double2 - double2;
double3 = double3 - double3;
double4 = double4 - double4;
double8 = double8 - double8;
cdouble = cdouble - cdouble;
cdouble2 = cdouble2 - cdouble2;
cdouble4 = cdouble4 - cdouble4;

VSUBH
short = short - short;
short2 = short2 - short2;
short3 = short3 - short3;
short4 = short4 - short4;
short8 = short8 - short8;
short16 = short16 - short16;
short32 = short32 - short32;
short = short - (short)(k);
short2 = short2 - (short2)(k);
short3 = short3 - (short3)(k);
short4 = short4 - (short4)(k);
short8 = short8 - (short8)(k);
short16 = short16 - (short16)(k);
short32 = short32 - (short32)(k);
cshort = cshort - cshort;
cshort2 = cshort2 - cshort2;
cshort4 = cshort4 - cshort4;
cshort8 = cshort8 - cshort8;
cshort16 = cshort16 - cshort16;
ushort = ushort - ushort;
ushort2 = ushort2 - ushort2;
ushort3 = ushort3 - ushort3;
ushort4 = ushort4 - ushort4;
ushort8 = ushort8 - ushort8;
ushort16 = ushort16 - ushort16;
ushort32 = ushort32 - ushort32;
cshort = cshort - (cshort)(k);
cshort2 = cshort2 - (cshort2)(k);
cshort4 = cshort4 - (cshort4)(k);
cshort8 = cshort8 - (cshort8)(k);
cshort16 = cshort16 - (cshort16)(k);
ushort = ushort - (ushort)(k);
ushort2 = ushort2 - (ushort2)(k);
ushort3 = ushort3 - (ushort3)(k);
ushort4 = ushort4 - (ushort4)(k);
ushort8 = ushort8 - (ushort8)(k);
ushort16 = ushort16 - (ushort16)(k);
ushort32 = ushort32 - (ushort32)(k);

VSUBSP
float = float - float;
float2 = float2 - float2;
float3 = float3 - float3;
float4 = float4 - float4;
float8 = float8 - float8;
float16 = float16 - float16;
cfloat = cfloat - cfloat;
cfloat2 = cfloat2 - cfloat2;
cfloat4 = cfloat4 - cfloat4;
cfloat8 = cfloat8 - cfloat8;

VSUBW
int = int - int;
int2 = int2 - int2;
int3 = int3 - int3;
int4 = int4 - int4;
int8 = int8 - int8;
int16 = int16 - int16;
int = int - (int)(k);
int2 = int2 - (int2)(k);
int3 = int3 - (int3)(k);
int4 = int4 - (int4)(k);
int8 = int8 - (int8)(k);
int16 = int16 - (int16)(k);
cint = cint - cint;
cint2 = cint2 - cint2;
cint4 = cint4 - cint4;
cint8 = cint8 - cint8;
uint = uint - uint;
uint2 = uint2 - uint2;
uint3 = uint3 - uint3;
uint4 = uint4 - uint4;
uint8 = uint8 - uint8;
uint16 = uint16 - uint16;
cint = cint - (cint)(k);
cint2 = cint2 - (cint2)(k);
cint4 = cint4 - (cint4)(k);
cint8 = cint8 - (cint8)(k);
uint = uint - (uint)(k);
uint2 = uint2 - (uint2)(k);
uint3 = uint3 - (uint3)(k);
uint4 = uint4 - (uint4)(k);
uint8 = uint8 - (uint8)(k);
uint16 = uint16 - (uint16)(k);

*/


/*-----------------------------------------------------------------------------
* ID: __sub_reverse
*----------------------------------------------------------------------------*/

/* SUBRD */
int64_t __sub_reverse(int64_t, int64_t);
/* CONSTANT: int64_t __sub_reverse(int64_t, (int64_t)(k)); */
ulong __sub_reverse(ulong, ulong);
/* CONSTANT: ulong __sub_reverse(ulong, (ulong)(k)); */

/* SUBRW */
int32_t __sub_reverse(int32_t, int32_t);
/* CONSTANT: int32_t __sub_reverse(int32_t, (int32_t)(k)); */
uint __sub_reverse(uint, uint);
/* CONSTANT: uint __sub_reverse(uint, (uint)(k)); */

/* VSUBRB */
int8_t __sub_reverse(int8_t, int8_t);
char2 __sub_reverse(char2, char2);
/* UNSUPPORTED: char3 __sub_reverse(char3, char3); */
char4 __sub_reverse(char4, char4);
char8 __sub_reverse(char8, char8);
char16 __sub_reverse(char16, char16);
char32 __sub_reverse(char32, char32);
char64 __sub_reverse(char64, char64);
/* CONSTANT: int8_t __sub_reverse(int8_t, (int8_t)(k)); */
/* CONSTANT: char2 __sub_reverse(char2, (char2)(k)); */
/* UNSUPPORTED: char3 __sub_reverse(char3, (char3)(k)); */
/* CONSTANT: char4 __sub_reverse(char4, (char4)(k)); */
/* CONSTANT: char8 __sub_reverse(char8, (char8)(k)); */
/* CONSTANT: char16 __sub_reverse(char16, (char16)(k)); */
/* CONSTANT: char32 __sub_reverse(char32, (char32)(k)); */
/* CONSTANT: char64 __sub_reverse(char64, (char64)(k)); */
cchar __sub_reverse(cchar, cchar);
cchar2 __sub_reverse(cchar2, cchar2);
cchar4 __sub_reverse(cchar4, cchar4);
cchar8 __sub_reverse(cchar8, cchar8);
cchar16 __sub_reverse(cchar16, cchar16);
cchar32 __sub_reverse(cchar32, cchar32);
uchar __sub_reverse(uchar, uchar);
uchar2 __sub_reverse(uchar2, uchar2);
/* UNSUPPORTED: uchar3 __sub_reverse(uchar3, uchar3); */
uchar4 __sub_reverse(uchar4, uchar4);
uchar8 __sub_reverse(uchar8, uchar8);
uchar16 __sub_reverse(uchar16, uchar16);
uchar32 __sub_reverse(uchar32, uchar32);
uchar64 __sub_reverse(uchar64, uchar64);
/* CONSTANT: cchar __sub_reverse(cchar, (cchar)(k)); */
/* CONSTANT: cchar2 __sub_reverse(cchar2, (cchar2)(k)); */
/* CONSTANT: cchar4 __sub_reverse(cchar4, (cchar4)(k)); */
/* CONSTANT: cchar8 __sub_reverse(cchar8, (cchar8)(k)); */
/* CONSTANT: cchar16 __sub_reverse(cchar16, (cchar16)(k)); */
/* CONSTANT: cchar32 __sub_reverse(cchar32, (cchar32)(k)); */
/* CONSTANT: uchar __sub_reverse(uchar, (uchar)(k)); */
/* CONSTANT: uchar2 __sub_reverse(uchar2, (uchar2)(k)); */
/* UNSUPPORTED: uchar3 __sub_reverse(uchar3, (uchar3)(k)); */
/* CONSTANT: uchar4 __sub_reverse(uchar4, (uchar4)(k)); */
/* CONSTANT: uchar8 __sub_reverse(uchar8, (uchar8)(k)); */
/* CONSTANT: uchar16 __sub_reverse(uchar16, (uchar16)(k)); */
/* CONSTANT: uchar32 __sub_reverse(uchar32, (uchar32)(k)); */
/* CONSTANT: uchar64 __sub_reverse(uchar64, (uchar64)(k)); */

/* VSUBRD */
int64_t __sub_reverse(int64_t, int64_t);
long2 __sub_reverse(long2, long2);
/* UNSUPPORTED: long3 __sub_reverse(long3, long3); */
long4 __sub_reverse(long4, long4);
long8 __sub_reverse(long8, long8);
/* CONSTANT: int64_t __sub_reverse(int64_t, (int64_t)(k)); */
/* CONSTANT: long2 __sub_reverse(long2, (long2)(k)); */
/* UNSUPPORTED: long3 __sub_reverse(long3, (long3)(k)); */
/* CONSTANT: long4 __sub_reverse(long4, (long4)(k)); */
/* CONSTANT: long8 __sub_reverse(long8, (long8)(k)); */
clong __sub_reverse(clong, clong);
clong2 __sub_reverse(clong2, clong2);
clong4 __sub_reverse(clong4, clong4);
ulong __sub_reverse(ulong, ulong);
ulong2 __sub_reverse(ulong2, ulong2);
/* UNSUPPORTED: ulong3 __sub_reverse(ulong3, ulong3); */
ulong4 __sub_reverse(ulong4, ulong4);
ulong8 __sub_reverse(ulong8, ulong8);
/* CONSTANT: clong __sub_reverse(clong, (clong)(k)); */
/* CONSTANT: clong2 __sub_reverse(clong2, (clong2)(k)); */
/* CONSTANT: clong4 __sub_reverse(clong4, (clong4)(k)); */
/* CONSTANT: ulong __sub_reverse(ulong, (ulong)(k)); */
/* CONSTANT: ulong2 __sub_reverse(ulong2, (ulong2)(k)); */
/* UNSUPPORTED: ulong3 __sub_reverse(ulong3, (ulong3)(k)); */
/* CONSTANT: ulong4 __sub_reverse(ulong4, (ulong4)(k)); */
/* CONSTANT: ulong8 __sub_reverse(ulong8, (ulong8)(k)); */

/* VSUBRH */
int16_t __sub_reverse(int16_t, int16_t);
short2 __sub_reverse(short2, short2);
/* UNSUPPORTED: short3 __sub_reverse(short3, short3); */
short4 __sub_reverse(short4, short4);
short8 __sub_reverse(short8, short8);
short16 __sub_reverse(short16, short16);
short32 __sub_reverse(short32, short32);
/* CONSTANT: int16_t __sub_reverse(int16_t, (int16_t)(k)); */
/* CONSTANT: short2 __sub_reverse(short2, (short2)(k)); */
/* UNSUPPORTED: short3 __sub_reverse(short3, (short3)(k)); */
/* CONSTANT: short4 __sub_reverse(short4, (short4)(k)); */
/* CONSTANT: short8 __sub_reverse(short8, (short8)(k)); */
/* CONSTANT: short16 __sub_reverse(short16, (short16)(k)); */
/* CONSTANT: short32 __sub_reverse(short32, (short32)(k)); */
cshort __sub_reverse(cshort, cshort);
cshort2 __sub_reverse(cshort2, cshort2);
cshort4 __sub_reverse(cshort4, cshort4);
cshort8 __sub_reverse(cshort8, cshort8);
cshort16 __sub_reverse(cshort16, cshort16);
ushort __sub_reverse(ushort, ushort);
ushort2 __sub_reverse(ushort2, ushort2);
/* UNSUPPORTED: ushort3 __sub_reverse(ushort3, ushort3); */
ushort4 __sub_reverse(ushort4, ushort4);
ushort8 __sub_reverse(ushort8, ushort8);
ushort16 __sub_reverse(ushort16, ushort16);
ushort32 __sub_reverse(ushort32, ushort32);
/* CONSTANT: cshort __sub_reverse(cshort, (cshort)(k)); */
/* CONSTANT: cshort2 __sub_reverse(cshort2, (cshort2)(k)); */
/* CONSTANT: cshort4 __sub_reverse(cshort4, (cshort4)(k)); */
/* CONSTANT: cshort8 __sub_reverse(cshort8, (cshort8)(k)); */
/* CONSTANT: cshort16 __sub_reverse(cshort16, (cshort16)(k)); */
/* CONSTANT: ushort __sub_reverse(ushort, (ushort)(k)); */
/* CONSTANT: ushort2 __sub_reverse(ushort2, (ushort2)(k)); */
/* UNSUPPORTED: ushort3 __sub_reverse(ushort3, (ushort3)(k)); */
/* CONSTANT: ushort4 __sub_reverse(ushort4, (ushort4)(k)); */
/* CONSTANT: ushort8 __sub_reverse(ushort8, (ushort8)(k)); */
/* CONSTANT: ushort16 __sub_reverse(ushort16, (ushort16)(k)); */
/* CONSTANT: ushort32 __sub_reverse(ushort32, (ushort32)(k)); */

/* VSUBRW */
int32_t __sub_reverse(int32_t, int32_t);
int2 __sub_reverse(int2, int2);
/* UNSUPPORTED: int3 __sub_reverse(int3, int3); */
int4 __sub_reverse(int4, int4);
int8 __sub_reverse(int8, int8);
int16 __sub_reverse(int16, int16);
/* CONSTANT: int32_t __sub_reverse(int32_t, (int32_t)(k)); */
/* CONSTANT: int2 __sub_reverse(int2, (int2)(k)); */
/* UNSUPPORTED: int3 __sub_reverse(int3, (int3)(k)); */
/* CONSTANT: int4 __sub_reverse(int4, (int4)(k)); */
/* CONSTANT: int8 __sub_reverse(int8, (int8)(k)); */
/* CONSTANT: int16 __sub_reverse(int16, (int16)(k)); */
cint __sub_reverse(cint, cint);
cint2 __sub_reverse(cint2, cint2);
cint4 __sub_reverse(cint4, cint4);
cint8 __sub_reverse(cint8, cint8);
uint __sub_reverse(uint, uint);
uint2 __sub_reverse(uint2, uint2);
/* UNSUPPORTED: uint3 __sub_reverse(uint3, uint3); */
uint4 __sub_reverse(uint4, uint4);
uint8 __sub_reverse(uint8, uint8);
uint16 __sub_reverse(uint16, uint16);
/* CONSTANT: cint __sub_reverse(cint, (cint)(k)); */
/* CONSTANT: cint2 __sub_reverse(cint2, (cint2)(k)); */
/* CONSTANT: cint4 __sub_reverse(cint4, (cint4)(k)); */
/* CONSTANT: cint8 __sub_reverse(cint8, (cint8)(k)); */
/* CONSTANT: uint __sub_reverse(uint, (uint)(k)); */
/* CONSTANT: uint2 __sub_reverse(uint2, (uint2)(k)); */
/* UNSUPPORTED: uint3 __sub_reverse(uint3, (uint3)(k)); */
/* CONSTANT: uint4 __sub_reverse(uint4, (uint4)(k)); */
/* CONSTANT: uint8 __sub_reverse(uint8, (uint8)(k)); */
/* CONSTANT: uint16 __sub_reverse(uint16, (uint16)(k)); */




/*-----------------------------------------------------------------------------
* ID: __sub_sat
*----------------------------------------------------------------------------*/

/* VSSUBB */
int8_t __sub_sat(int8_t, int8_t);
char2 __sub_sat(char2, char2);
/* UNSUPPORTED: char3 __sub_sat(char3, char3); */
char4 __sub_sat(char4, char4);
char8 __sub_sat(char8, char8);
char16 __sub_sat(char16, char16);
char32 __sub_sat(char32, char32);
char64 __sub_sat(char64, char64);
/* CONSTANT: int8_t __sub_sat(int8_t, (int8_t)(k)); */
/* CONSTANT: char2 __sub_sat(char2, (char2)(k)); */
/* UNSUPPORTED: char3 __sub_sat(char3, (char3)(k)); */
/* CONSTANT: char4 __sub_sat(char4, (char4)(k)); */
/* CONSTANT: char8 __sub_sat(char8, (char8)(k)); */
/* CONSTANT: char16 __sub_sat(char16, (char16)(k)); */
/* CONSTANT: char32 __sub_sat(char32, (char32)(k)); */
/* CONSTANT: char64 __sub_sat(char64, (char64)(k)); */
cchar __sub_sat(cchar, cchar);
cchar2 __sub_sat(cchar2, cchar2);
cchar4 __sub_sat(cchar4, cchar4);
cchar8 __sub_sat(cchar8, cchar8);
cchar16 __sub_sat(cchar16, cchar16);
cchar32 __sub_sat(cchar32, cchar32);
/* CONSTANT: cchar __sub_sat(cchar, (cchar)(k)); */
/* CONSTANT: cchar2 __sub_sat(cchar2, (cchar2)(k)); */
/* CONSTANT: cchar4 __sub_sat(cchar4, (cchar4)(k)); */
/* CONSTANT: cchar8 __sub_sat(cchar8, (cchar8)(k)); */
/* CONSTANT: cchar16 __sub_sat(cchar16, (cchar16)(k)); */
/* CONSTANT: cchar32 __sub_sat(cchar32, (cchar32)(k)); */

/* VSSUBH */
int16_t __sub_sat(int16_t, int16_t);
short2 __sub_sat(short2, short2);
/* UNSUPPORTED: short3 __sub_sat(short3, short3); */
short4 __sub_sat(short4, short4);
short8 __sub_sat(short8, short8);
short16 __sub_sat(short16, short16);
short32 __sub_sat(short32, short32);
/* CONSTANT: int16_t __sub_sat(int16_t, (int16_t)(k)); */
/* CONSTANT: short2 __sub_sat(short2, (short2)(k)); */
/* UNSUPPORTED: short3 __sub_sat(short3, (short3)(k)); */
/* CONSTANT: short4 __sub_sat(short4, (short4)(k)); */
/* CONSTANT: short8 __sub_sat(short8, (short8)(k)); */
/* CONSTANT: short16 __sub_sat(short16, (short16)(k)); */
/* CONSTANT: short32 __sub_sat(short32, (short32)(k)); */
cshort __sub_sat(cshort, cshort);
cshort2 __sub_sat(cshort2, cshort2);
cshort4 __sub_sat(cshort4, cshort4);
cshort8 __sub_sat(cshort8, cshort8);
cshort16 __sub_sat(cshort16, cshort16);
/* CONSTANT: cshort __sub_sat(cshort, (cshort)(k)); */
/* CONSTANT: cshort2 __sub_sat(cshort2, (cshort2)(k)); */
/* CONSTANT: cshort4 __sub_sat(cshort4, (cshort4)(k)); */
/* CONSTANT: cshort8 __sub_sat(cshort8, (cshort8)(k)); */
/* CONSTANT: cshort16 __sub_sat(cshort16, (cshort16)(k)); */

/* VSSUBW */
int32_t __sub_sat(int32_t, int32_t);
int2 __sub_sat(int2, int2);
/* UNSUPPORTED: int3 __sub_sat(int3, int3); */
int4 __sub_sat(int4, int4);
int8 __sub_sat(int8, int8);
int16 __sub_sat(int16, int16);
/* CONSTANT: int32_t __sub_sat(int32_t, (int32_t)(k)); */
/* CONSTANT: int2 __sub_sat(int2, (int2)(k)); */
/* UNSUPPORTED: int3 __sub_sat(int3, (int3)(k)); */
/* CONSTANT: int4 __sub_sat(int4, (int4)(k)); */
/* CONSTANT: int8 __sub_sat(int8, (int8)(k)); */
/* CONSTANT: int16 __sub_sat(int16, (int16)(k)); */
cint __sub_sat(cint, cint);
cint2 __sub_sat(cint2, cint2);
cint4 __sub_sat(cint4, cint4);
cint8 __sub_sat(cint8, cint8);
/* CONSTANT: cint __sub_sat(cint, (cint)(k)); */
/* CONSTANT: cint2 __sub_sat(cint2, (cint2)(k)); */
/* CONSTANT: cint4 __sub_sat(cint4, (cint4)(k)); */
/* CONSTANT: cint8 __sub_sat(cint8, (cint8)(k)); */




/*-----------------------------------------------------------------------------
* ID: __subd_ddd
*----------------------------------------------------------------------------*/

/* SUBD */
int64_t __subd_ddd(int64_t, int64_t);
ulong __subd_ddd(ulong, ulong);




/*-----------------------------------------------------------------------------
* ID: __subdp_ddd
*----------------------------------------------------------------------------*/

/* SUBDP */
double __subdp_ddd(double, double);




/*-----------------------------------------------------------------------------
* ID: __subrd_ddd
*----------------------------------------------------------------------------*/

/* SUBRD */
int64_t __subrd_ddd(int64_t, int64_t);
ulong __subrd_ddd(ulong, ulong);




/*-----------------------------------------------------------------------------
* ID: __subrd_dkd
*----------------------------------------------------------------------------*/

/* SUBRD */
int64_t __subrd_dkd(int64_t, int64_t);
ulong __subrd_dkd(ulong, ulong);




/*-----------------------------------------------------------------------------
* ID: __subrw_rkr
*----------------------------------------------------------------------------*/

/* SUBRW */
int32_t __subrw_rkr(int32_t, int32_t);
uint __subrw_rkr(uint, uint);




/*-----------------------------------------------------------------------------
* ID: __subrw_rrr
*----------------------------------------------------------------------------*/

/* SUBRW */
int32_t __subrw_rrr(int32_t, int32_t);
uint __subrw_rrr(uint, uint);




/*-----------------------------------------------------------------------------
* ID: __subsp_rrr
*----------------------------------------------------------------------------*/

/* SUBSP */
float __subsp_rrr(float, float);




/*-----------------------------------------------------------------------------
* ID: __subw_rrr
*----------------------------------------------------------------------------*/

/* SUBW */
int32_t __subw_rrr(int32_t, int32_t);
uint __subw_rrr(uint, uint);




/*-----------------------------------------------------------------------------
* ID: __swap
*----------------------------------------------------------------------------*/

/* VSWAPB */
char2 __swap(char2);
char4 __swap(char4);
char8 __swap(char8);
char16 __swap(char16);
char32 __swap(char32);
char64 __swap(char64);
uchar2 __swap(uchar2);
uchar4 __swap(uchar4);
uchar8 __swap(uchar8);
uchar16 __swap(uchar16);
uchar32 __swap(uchar32);
uchar64 __swap(uchar64);

/* VSWAPD */
long2 __swap(long2);
long4 __swap(long4);
long8 __swap(long8);
double2 __swap(double2);
double4 __swap(double4);
double8 __swap(double8);
cfloat2 __swap(cfloat2);
cfloat4 __swap(cfloat4);
cfloat8 __swap(cfloat8);
ulong2 __swap(ulong2);
ulong4 __swap(ulong4);
ulong8 __swap(ulong8);
cint2 __swap(cint2);
cint4 __swap(cint4);
cint8 __swap(cint8);

/* VSWAPH */
short2 __swap(short2);
short4 __swap(short4);
short8 __swap(short8);
short16 __swap(short16);
short32 __swap(short32);
ushort2 __swap(ushort2);
ushort4 __swap(ushort4);
ushort8 __swap(ushort8);
ushort16 __swap(ushort16);
ushort32 __swap(ushort32);
cchar2 __swap(cchar2);
cchar4 __swap(cchar4);
cchar8 __swap(cchar8);
cchar16 __swap(cchar16);
cchar32 __swap(cchar32);

/* VSWAPW */
int2 __swap(int2);
int4 __swap(int4);
int8 __swap(int8);
int16 __swap(int16);
float2 __swap(float2);
float4 __swap(float4);
float8 __swap(float8);
float16 __swap(float16);
uint2 __swap(uint2);
uint4 __swap(uint4);
uint8 __swap(uint8);
uint16 __swap(uint16);
cshort2 __swap(cshort2);
cshort4 __swap(cshort4);
cshort8 __swap(cshort8);
cshort16 __swap(cshort16);




/*-----------------------------------------------------------------------------
* ID: __unpack_high
*----------------------------------------------------------------------------*/

/* VUNPKHB */
short2 __unpack_high(char4);
short4 __unpack_high(char8);
short8 __unpack_high(char16);
short16 __unpack_high(char32);
short32 __unpack_high(char64);

/* VUNPKHH */
int2 __unpack_high(short4);
int4 __unpack_high(short8);
int8 __unpack_high(short16);
int16 __unpack_high(short32);

/* VUNPKHUB */
ushort2 __unpack_high(uchar4);
ushort4 __unpack_high(uchar8);
ushort8 __unpack_high(uchar16);
ushort16 __unpack_high(uchar32);
ushort32 __unpack_high(uchar64);

/* VUNPKHUH */
uint2 __unpack_high(ushort4);
uint4 __unpack_high(ushort8);
uint8 __unpack_high(ushort16);
uint16 __unpack_high(ushort32);




/*-----------------------------------------------------------------------------
* ID: __unpack_low
*----------------------------------------------------------------------------*/

/* VUNPKLB */
short2 __unpack_low(char4);
short4 __unpack_low(char8);
short8 __unpack_low(char16);
short16 __unpack_low(char32);
short32 __unpack_low(char64);

/* VUNPKLH */
int2 __unpack_low(short4);
int4 __unpack_low(short8);
int8 __unpack_low(short16);
int16 __unpack_low(short32);

/* VUNPKLUB */
ushort2 __unpack_low(uchar4);
ushort4 __unpack_low(uchar8);
ushort8 __unpack_low(uchar16);
ushort16 __unpack_low(uchar32);
ushort32 __unpack_low(uchar64);

/* VUNPKLUH */
uint2 __unpack_low(ushort4);
uint4 __unpack_low(ushort8);
uint8 __unpack_low(ushort16);
uint16 __unpack_low(ushort32);

/* VUNPKLUW */
ulong __unpack_low(uint2);
ulong2 __unpack_low(uint4);
ulong4 __unpack_low(uint8);
ulong8 __unpack_low(uint16);

/* VUNPKLW */
int64_t __unpack_low(int2);
long2 __unpack_low(int4);
long4 __unpack_low(int8);
long8 __unpack_low(int16);




/*-----------------------------------------------------------------------------
* ID: __vabsb_vv
*----------------------------------------------------------------------------*/

/* VABSB */
char64 __vabsb_vv(char64);




/*-----------------------------------------------------------------------------
* ID: __vabsd_vv
*----------------------------------------------------------------------------*/

/* VABSD */
long8 __vabsd_vv(long8);




/*-----------------------------------------------------------------------------
* ID: __vabsdp_vv
*----------------------------------------------------------------------------*/

/* VABSDP */
double8 __vabsdp_vv(double8);




/*-----------------------------------------------------------------------------
* ID: __vabsh_vv
*----------------------------------------------------------------------------*/

/* VABSH */
short32 __vabsh_vv(short32);




/*-----------------------------------------------------------------------------
* ID: __vabssp_vv
*----------------------------------------------------------------------------*/

/* VABSSP */
float16 __vabssp_vv(float16);




/*-----------------------------------------------------------------------------
* ID: __vabsw_vv
*----------------------------------------------------------------------------*/

/* VABSW */
int16 __vabsw_vv(int16);




/*-----------------------------------------------------------------------------
* ID: __vaddb_vkv
*----------------------------------------------------------------------------*/

/* VADDB */
char64 __vaddb_vkv(char64, char64);
uchar64 __vaddb_vkv(uchar64, uchar64);




/*-----------------------------------------------------------------------------
* ID: __vaddb_vvv
*----------------------------------------------------------------------------*/

/* VADDB */
char64 __vaddb_vvv(char64, char64);
uchar64 __vaddb_vvv(uchar64, uchar64);




/*-----------------------------------------------------------------------------
* ID: __vaddcb_pvv
*----------------------------------------------------------------------------*/

/* VADDCB */
char64 __vaddcb_pvv(__vpred, char64, char64);
uchar64 __vaddcb_pvv(__vpred, uchar64, uchar64);




/*-----------------------------------------------------------------------------
* ID: __vaddcd_pvv
*----------------------------------------------------------------------------*/

/* VADDCD */
long8 __vaddcd_pvv(__vpred, long8, long8);
ulong8 __vaddcd_pvv(__vpred, ulong8, ulong8);




/*-----------------------------------------------------------------------------
* ID: __vaddch_pvv
*----------------------------------------------------------------------------*/

/* VADDCH */
short32 __vaddch_pvv(__vpred, short32, short32);
ushort32 __vaddch_pvv(__vpred, ushort32, ushort32);




/*-----------------------------------------------------------------------------
* ID: __vaddcw_pvv
*----------------------------------------------------------------------------*/

/* VADDCW */
int16 __vaddcw_pvv(__vpred, int16, int16);
uint16 __vaddcw_pvv(__vpred, uint16, uint16);




/*-----------------------------------------------------------------------------
* ID: __vaddd_vkv
*----------------------------------------------------------------------------*/

/* VADDD */
long8 __vaddd_vkv(long8, int8);
ulong8 __vaddd_vkv(ulong8, uint8);




/*-----------------------------------------------------------------------------
* ID: __vaddd_vvv
*----------------------------------------------------------------------------*/

/* VADDD */
long8 __vaddd_vvv(long8, long8);
ulong8 __vaddd_vvv(ulong8, ulong8);




/*-----------------------------------------------------------------------------
* ID: __vadddp_vvv
*----------------------------------------------------------------------------*/

/* VADDDP */
double8 __vadddp_vvv(double8, double8);




/*-----------------------------------------------------------------------------
* ID: __vaddh_vkv
*----------------------------------------------------------------------------*/

/* VADDH */
short32 __vaddh_vkv(short32, short32);
ushort32 __vaddh_vkv(ushort32, ushort32);




/*-----------------------------------------------------------------------------
* ID: __vaddh_vvv
*----------------------------------------------------------------------------*/

/* VADDH */
short32 __vaddh_vvv(short32, short32);
ushort32 __vaddh_vvv(ushort32, ushort32);




/*-----------------------------------------------------------------------------
* ID: __vaddsp_vvv
*----------------------------------------------------------------------------*/

/* VADDSP */
float16 __vaddsp_vvv(float16, float16);




/*-----------------------------------------------------------------------------
* ID: __vaddw_vkv
*----------------------------------------------------------------------------*/

/* VADDW */
int16 __vaddw_vkv(int16, int16);
uint16 __vaddw_vkv(uint16, uint16);




/*-----------------------------------------------------------------------------
* ID: __vaddw_vvv
*----------------------------------------------------------------------------*/

/* VADDW */
int16 __vaddw_vvv(int16, int16);
uint16 __vaddw_vvv(uint16, uint16);




/*-----------------------------------------------------------------------------
* ID: __vandnw_vkv
*----------------------------------------------------------------------------*/

/* VANDNW */
int16 __vandnw_vkv(int16, int16);




/*-----------------------------------------------------------------------------
* ID: __vandnw_vvv
*----------------------------------------------------------------------------*/

/* VANDNW */
char64 __vandnw_vvv(char64, char64);
short32 __vandnw_vvv(short32, short32);
int16 __vandnw_vvv(int16, int16);
long8 __vandnw_vvv(long8, long8);




/*-----------------------------------------------------------------------------
* ID: __vandw_vkv
*----------------------------------------------------------------------------*/

/* VANDW */
int16 __vandw_vkv(int16, int16);
uint16 __vandw_vkv(uint16, uint16);




/*-----------------------------------------------------------------------------
* ID: __vandw_vvv
*----------------------------------------------------------------------------*/

/* VANDW */
char64 __vandw_vvv(char64, char64);
short32 __vandw_vvv(short32, short32);
int16 __vandw_vvv(int16, int16);
long8 __vandw_vvv(long8, long8);
uchar64 __vandw_vvv(uchar64, uchar64);
ushort32 __vandw_vvv(ushort32, ushort32);
uint16 __vandw_vvv(uint16, uint16);
ulong8 __vandw_vvv(ulong8, ulong8);




/*-----------------------------------------------------------------------------
* ID: __vapysb_vvv
*----------------------------------------------------------------------------*/

/* VAPYSB */
char64 __vapysb_vvv(char64, char64);




/*-----------------------------------------------------------------------------
* ID: __vapysd_vvv
*----------------------------------------------------------------------------*/

/* VAPYSD */
long8 __vapysd_vvv(long8, long8);




/*-----------------------------------------------------------------------------
* ID: __vapysh_vvv
*----------------------------------------------------------------------------*/

/* VAPYSH */
short32 __vapysh_vvv(short32, short32);




/*-----------------------------------------------------------------------------
* ID: __vapysw_vvv
*----------------------------------------------------------------------------*/

/* VAPYSW */
int16 __vapysw_vvv(int16, int16);




/*-----------------------------------------------------------------------------
* ID: __vavgb_vvv
*----------------------------------------------------------------------------*/

/* VAVGB */
char64 __vavgb_vvv(char64, char64);




/*-----------------------------------------------------------------------------
* ID: __vavgd_vvv
*----------------------------------------------------------------------------*/

/* VAVGD */
long8 __vavgd_vvv(long8, long8);




/*-----------------------------------------------------------------------------
* ID: __vavgh_vvv
*----------------------------------------------------------------------------*/

/* VAVGH */
short32 __vavgh_vvv(short32, short32);




/*-----------------------------------------------------------------------------
* ID: __vavgnrb_vvv
*----------------------------------------------------------------------------*/

/* VAVGNRB */
char64 __vavgnrb_vvv(char64, char64);




/*-----------------------------------------------------------------------------
* ID: __vavgnrd_vvv
*----------------------------------------------------------------------------*/

/* VAVGNRD */
long8 __vavgnrd_vvv(long8, long8);




/*-----------------------------------------------------------------------------
* ID: __vavgnrh_vvv
*----------------------------------------------------------------------------*/

/* VAVGNRH */
short32 __vavgnrh_vvv(short32, short32);




/*-----------------------------------------------------------------------------
* ID: __vavgnrub_vvv
*----------------------------------------------------------------------------*/

/* VAVGNRUB */
uchar64 __vavgnrub_vvv(uchar64, uchar64);




/*-----------------------------------------------------------------------------
* ID: __vavgnrud_vvv
*----------------------------------------------------------------------------*/

/* VAVGNRUD */
ulong8 __vavgnrud_vvv(ulong8, ulong8);




/*-----------------------------------------------------------------------------
* ID: __vavgnruh_vvv
*----------------------------------------------------------------------------*/

/* VAVGNRUH */
ushort32 __vavgnruh_vvv(ushort32, ushort32);




/*-----------------------------------------------------------------------------
* ID: __vavgnruw_vvv
*----------------------------------------------------------------------------*/

/* VAVGNRUW */
uint16 __vavgnruw_vvv(uint16, uint16);




/*-----------------------------------------------------------------------------
* ID: __vavgnrw_vvv
*----------------------------------------------------------------------------*/

/* VAVGNRW */
int16 __vavgnrw_vvv(int16, int16);




/*-----------------------------------------------------------------------------
* ID: __vavgub_vvv
*----------------------------------------------------------------------------*/

/* VAVGUB */
uchar64 __vavgub_vvv(uchar64, uchar64);




/*-----------------------------------------------------------------------------
* ID: __vavgud_vvv
*----------------------------------------------------------------------------*/

/* VAVGUD */
ulong8 __vavgud_vvv(ulong8, ulong8);




/*-----------------------------------------------------------------------------
* ID: __vavguh_vvv
*----------------------------------------------------------------------------*/

/* VAVGUH */
ushort32 __vavguh_vvv(ushort32, ushort32);




/*-----------------------------------------------------------------------------
* ID: __vavguw_vvv
*----------------------------------------------------------------------------*/

/* VAVGUW */
uint16 __vavguw_vvv(uint16, uint16);




/*-----------------------------------------------------------------------------
* ID: __vavgw_vvv
*----------------------------------------------------------------------------*/

/* VAVGW */
int16 __vavgw_vvv(int16, int16);




/*-----------------------------------------------------------------------------
* ID: __vbinlogw_vv
*----------------------------------------------------------------------------*/

/* VBINLOGW */
uint16 __vbinlogw_vv(uint16);




/*-----------------------------------------------------------------------------
* ID: __vbitcntb_vv
*----------------------------------------------------------------------------*/

/* VBITCNTB */
char64 __vbitcntb_vv(char64);
uchar64 __vbitcntb_vv(uchar64);




/*-----------------------------------------------------------------------------
* ID: __vbitcntd_vv
*----------------------------------------------------------------------------*/

/* VBITCNTD */
long8 __vbitcntd_vv(long8);
ulong8 __vbitcntd_vv(ulong8);




/*-----------------------------------------------------------------------------
* ID: __vbitcnth_vv
*----------------------------------------------------------------------------*/

/* VBITCNTH */
short32 __vbitcnth_vv(short32);
ushort32 __vbitcnth_vv(ushort32);




/*-----------------------------------------------------------------------------
* ID: __vbitcntw_vv
*----------------------------------------------------------------------------*/

/* VBITCNTW */
int16 __vbitcntw_vv(int16);
uint16 __vbitcntw_vv(uint16);




/*-----------------------------------------------------------------------------
* ID: __vbitdeald_vv
*----------------------------------------------------------------------------*/

/* VBITDEALD */
long8 __vbitdeald_vv(long8);
ulong8 __vbitdeald_vv(ulong8);




/*-----------------------------------------------------------------------------
* ID: __vbitdealw_vv
*----------------------------------------------------------------------------*/

/* VBITDEALW */
int16 __vbitdealw_vv(int16);
uint16 __vbitdealw_vv(uint16);




/*-----------------------------------------------------------------------------
* ID: __vbitrd_vv
*----------------------------------------------------------------------------*/

/* VBITRD */
long8 __vbitrd_vv(long8);
ulong8 __vbitrd_vv(ulong8);




/*-----------------------------------------------------------------------------
* ID: __vbitrw_vv
*----------------------------------------------------------------------------*/

/* VBITRW */
int16 __vbitrw_vv(int16);
uint16 __vbitrw_vv(uint16);




/*-----------------------------------------------------------------------------
* ID: __vbitshfld_vv
*----------------------------------------------------------------------------*/

/* VBITSHFLD */
long8 __vbitshfld_vv(long8);
ulong8 __vbitshfld_vv(ulong8);




/*-----------------------------------------------------------------------------
* ID: __vbitshflw_vv
*----------------------------------------------------------------------------*/

/* VBITSHFLW */
int16 __vbitshflw_vv(int16);
uint16 __vbitshflw_vv(uint16);




/*-----------------------------------------------------------------------------
* ID: __vbittran8b_vv
*----------------------------------------------------------------------------*/

/* VBITTRAN8B */
uchar64 __vbittran8b_vv(uchar64);




/*-----------------------------------------------------------------------------
* ID: __vbpackh_vvv
*----------------------------------------------------------------------------*/

/* VBPACKH */
char64 __vbpackh_vvv(char64, char64);
uchar64 __vbpackh_vvv(uchar64, uchar64);




/*-----------------------------------------------------------------------------
* ID: __vbpackl_vvv
*----------------------------------------------------------------------------*/

/* VBPACKL */
char64 __vbpackl_vvv(char64, char64);
uchar64 __vbpackl_vvv(uchar64, uchar64);




/*-----------------------------------------------------------------------------
* ID: __vbunpkd_vv
*----------------------------------------------------------------------------*/

/* VBUNPKD */
long8 __vbunpkd_vv(char8);




/*-----------------------------------------------------------------------------
* ID: __vbunpkdu_vv
*----------------------------------------------------------------------------*/

/* VBUNPKDU */
ulong8 __vbunpkdu_vv(uchar8);




/*-----------------------------------------------------------------------------
* ID: __vbunpkh_vv
*----------------------------------------------------------------------------*/

/* VBUNPKH */
short32 __vbunpkh_vv(char32);




/*-----------------------------------------------------------------------------
* ID: __vbunpkhh_vv
*----------------------------------------------------------------------------*/

/* VBUNPKHH */
short32 __vbunpkhh_vv(char64);




/*-----------------------------------------------------------------------------
* ID: __vbunpkhl_vv
*----------------------------------------------------------------------------*/

/* VBUNPKHL */
short32 __vbunpkhl_vv(char64);




/*-----------------------------------------------------------------------------
* ID: __vbunpkhu_vv
*----------------------------------------------------------------------------*/

/* VBUNPKHU */
ushort32 __vbunpkhu_vv(uchar32);




/*-----------------------------------------------------------------------------
* ID: __vbunpkuhh_vv
*----------------------------------------------------------------------------*/

/* VBUNPKUHH */
ushort32 __vbunpkuhh_vv(uchar64);




/*-----------------------------------------------------------------------------
* ID: __vbunpkuhl_vv
*----------------------------------------------------------------------------*/

/* VBUNPKUHL */
ushort32 __vbunpkuhl_vv(uchar64);




/*-----------------------------------------------------------------------------
* ID: __vbunpkw_vv
*----------------------------------------------------------------------------*/

/* VBUNPKW */
int16 __vbunpkw_vv(char16);




/*-----------------------------------------------------------------------------
* ID: __vbunpkwu_vv
*----------------------------------------------------------------------------*/

/* VBUNPKWU */
uint16 __vbunpkwu_vv(uchar16);




/*-----------------------------------------------------------------------------
* ID: __vccdotp2hw_vvv
*----------------------------------------------------------------------------*/

/* VCCDOTP2HW */
cint8 __vccdotp2hw_vvv(cshort16, cshort16);




/*-----------------------------------------------------------------------------
* ID: __vccmatmpyhw{_se}_vww
*----------------------------------------------------------------------------*/

/* VCCMATMPYHW */
void __vccmatmpyhw_vww(cshort16, cshort16, cshort16, cint8::EQUIV_ACCESS_T<0>&, cint8::EQUIV_ACCESS_T<0>&);
void __vccmatmpyhw_se_vww(cshort16, __SE_REG_PAIR, cint8::EQUIV_ACCESS_T<0>&, cint8::EQUIV_ACCESS_T<0>&);




/*-----------------------------------------------------------------------------
* ID: __vccmatmpyr1hh{_se}_vwv
*----------------------------------------------------------------------------*/

/* VCCMATMPYR1HH */
cshort16 __vccmatmpyr1hh_vwv(cshort16, cshort16, cshort16);
cshort16 __vccmatmpyr1hh_se_vwv(cshort16, __SE_REG_PAIR);




/*-----------------------------------------------------------------------------
* ID: __vccmpyhw_vvw
*----------------------------------------------------------------------------*/

/* VCCMPYHW */
void __vccmpyhw_vvw(cshort16, cshort16, cint8::EQUIV_ACCESS_T<0>&, cint8::EQUIV_ACCESS_T<0>&);




/*-----------------------------------------------------------------------------
* ID: __vccmpyr1hh_vvv
*----------------------------------------------------------------------------*/

/* VCCMPYR1HH */
cshort16 __vccmpyr1hh_vvv(cshort16, cshort16);




/*-----------------------------------------------------------------------------
* ID: __vccmpyr1ww_vvv
*----------------------------------------------------------------------------*/

/* VCCMPYR1WW */
cint8 __vccmpyr1ww_vvv(cint8, cint8);




/*-----------------------------------------------------------------------------
* ID: __vcdotp2hw_vvv
*----------------------------------------------------------------------------*/

/* VCDOTP2HW */
cint8 __vcdotp2hw_vvv(cshort16, cshort16);




/*-----------------------------------------------------------------------------
* ID: __vcdotpm2opn16b32h_yvvv
*----------------------------------------------------------------------------*/

/* VCDOTPM2OPN16B32H */
cshort16 __vcdotpm2opn16b32h_yvvv(ushort32, ulong, cchar32);




/*-----------------------------------------------------------------------------
* ID: __vcdotpm2opn1h32h_yvvv
*----------------------------------------------------------------------------*/

/* VCDOTPM2OPN1H32H */
cshort16 __vcdotpm2opn1h32h_yvvv(ushort32, uint, cshort16);




/*-----------------------------------------------------------------------------
* ID: __vcdotpm2opn8h16w_yvvv
*----------------------------------------------------------------------------*/

/* VCDOTPM2OPN8H16W */
cint8 __vcdotpm2opn8h16w_yvvv(ulong8, uint4, cshort8);




/*-----------------------------------------------------------------------------
* ID: __vcdotpm2opn8w16w_yvvv
*----------------------------------------------------------------------------*/

/* VCDOTPM2OPN8W16W */
cint8 __vcdotpm2opn8w16w_yvvv(ulong8, uint4, cint8);




/*-----------------------------------------------------------------------------
* ID: __vcdotpm32opn16b32h_yvvv
*----------------------------------------------------------------------------*/

/* VCDOTPM32OPN16B32H */
cshort16 __vcdotpm32opn16b32h_yvvv(ushort32, ulong4, cchar32);




/*-----------------------------------------------------------------------------
* ID: __vclassdp_vv
*----------------------------------------------------------------------------*/

/* VCLASSDP */
long8 __vclassdp_vv(double8);




/*-----------------------------------------------------------------------------
* ID: __vclasssp_vv
*----------------------------------------------------------------------------*/

/* VCLASSSP */
int16 __vclasssp_vv(float16);




/*-----------------------------------------------------------------------------
* ID: __vcmatmpyhw_vww
*----------------------------------------------------------------------------*/

/* VCMATMPYHW */
void __vcmatmpyhw_vww(cshort16, cshort16, cshort16, cint8::EQUIV_ACCESS_T<0>&, cint8::EQUIV_ACCESS_T<0>&);

void __vcmatmpyhw_se_vww(cshort16, __SE_REG_PAIR, cint8::EQUIV_ACCESS_T<0>&, cint8::EQUIV_ACCESS_T<0>&);




/*-----------------------------------------------------------------------------
* ID: __vcmatmpyr1hh_vwv
*----------------------------------------------------------------------------*/

/* VCMATMPYR1HH */
cshort16 __vcmatmpyr1hh_vwv(cshort16, cshort16, cshort16);

cshort16 __vcmatmpyr1hh_se_vwv(cshort16, __SE_REG_PAIR);




/*-----------------------------------------------------------------------------
* ID: __vcmaxb_vvv
*----------------------------------------------------------------------------*/

/* VCMAXB */
char64 __vcmaxb_vvv(char64, char64);




/*-----------------------------------------------------------------------------
* ID: __vcmaxh_vvv
*----------------------------------------------------------------------------*/

/* VCMAXH */
short32 __vcmaxh_vvv(short32, short32);




/*-----------------------------------------------------------------------------
* ID: __vcmaxpb_vvp
*----------------------------------------------------------------------------*/

/* VCMAXPB */
void __vcmaxpb_vvp(char64, char64::EQUIV_ACCESS_T<0>&, __vpred&);




/*-----------------------------------------------------------------------------
* ID: __vcmaxph_vvp
*----------------------------------------------------------------------------*/

/* VCMAXPH */
void __vcmaxph_vvp(short32, short32::EQUIV_ACCESS_T<0>&, __vpred&);




/*-----------------------------------------------------------------------------
* ID: __vcmpeqb_vvp
*----------------------------------------------------------------------------*/

/* VCMPEQB */
__vpred __vcmpeqb_vvp(char64, char64);
__vpred __vcmpeqb_vvp(uchar64, uchar64);




/*-----------------------------------------------------------------------------
* ID: __vcmpeqd_vvp
*----------------------------------------------------------------------------*/

/* VCMPEQD */
__vpred __vcmpeqd_vvp(long8, long8);
__vpred __vcmpeqd_vvp(ulong8, ulong8);




/*-----------------------------------------------------------------------------
* ID: __vcmpeqdp_vvp
*----------------------------------------------------------------------------*/

/* VCMPEQDP */
__vpred __vcmpeqdp_vvp(double8, double8);




/*-----------------------------------------------------------------------------
* ID: __vcmpeqh_vvp
*----------------------------------------------------------------------------*/

/* VCMPEQH */
__vpred __vcmpeqh_vvp(short32, short32);
__vpred __vcmpeqh_vvp(ushort32, ushort32);




/*-----------------------------------------------------------------------------
* ID: __vcmpeqsp_vvp
*----------------------------------------------------------------------------*/

/* VCMPEQSP */
__vpred __vcmpeqsp_vvp(float16, float16);




/*-----------------------------------------------------------------------------
* ID: __vcmpeqw_vvp
*----------------------------------------------------------------------------*/

/* VCMPEQW */
__vpred __vcmpeqw_vvp(int16, int16);
__vpred __vcmpeqw_vvp(uint16, uint16);




/*-----------------------------------------------------------------------------
* ID: __vcmpgeb_vvp
*----------------------------------------------------------------------------*/

/* VCMPGEB */
__vpred __vcmpgeb_vvp(char64, char64);




/*-----------------------------------------------------------------------------
* ID: __vcmpged_vvp
*----------------------------------------------------------------------------*/

/* VCMPGED */
__vpred __vcmpged_vvp(long8, long8);




/*-----------------------------------------------------------------------------
* ID: __vcmpgeh_vvp
*----------------------------------------------------------------------------*/

/* VCMPGEH */
__vpred __vcmpgeh_vvp(short32, short32);




/*-----------------------------------------------------------------------------
* ID: __vcmpgeub_vvp
*----------------------------------------------------------------------------*/

/* VCMPGEUB */
__vpred __vcmpgeub_vvp(uchar64, uchar64);




/*-----------------------------------------------------------------------------
* ID: __vcmpgeud_vvp
*----------------------------------------------------------------------------*/

/* VCMPGEUD */
__vpred __vcmpgeud_vvp(ulong8, ulong8);




/*-----------------------------------------------------------------------------
* ID: __vcmpgeuh_vvp
*----------------------------------------------------------------------------*/

/* VCMPGEUH */
__vpred __vcmpgeuh_vvp(ushort32, ushort32);




/*-----------------------------------------------------------------------------
* ID: __vcmpgeuw_vvp
*----------------------------------------------------------------------------*/

/* VCMPGEUW */
__vpred __vcmpgeuw_vvp(uint16, uint16);




/*-----------------------------------------------------------------------------
* ID: __vcmpgew_vvp
*----------------------------------------------------------------------------*/

/* VCMPGEW */
__vpred __vcmpgew_vvp(int16, int16);




/*-----------------------------------------------------------------------------
* ID: __vcmpgtb_vvp
*----------------------------------------------------------------------------*/

/* VCMPGTB */
__vpred __vcmpgtb_vvp(char64, char64);




/*-----------------------------------------------------------------------------
* ID: __vcmpgtd_vvp
*----------------------------------------------------------------------------*/

/* VCMPGTD */
__vpred __vcmpgtd_vvp(long8, long8);




/*-----------------------------------------------------------------------------
* ID: __vcmpgth_vvp
*----------------------------------------------------------------------------*/

/* VCMPGTH */
__vpred __vcmpgth_vvp(short32, short32);




/*-----------------------------------------------------------------------------
* ID: __vcmpgtub_vvp
*----------------------------------------------------------------------------*/

/* VCMPGTUB */
__vpred __vcmpgtub_vvp(uchar64, uchar64);




/*-----------------------------------------------------------------------------
* ID: __vcmpgtud_vvp
*----------------------------------------------------------------------------*/

/* VCMPGTUD */
__vpred __vcmpgtud_vvp(ulong8, ulong8);




/*-----------------------------------------------------------------------------
* ID: __vcmpgtuh_vvp
*----------------------------------------------------------------------------*/

/* VCMPGTUH */
__vpred __vcmpgtuh_vvp(ushort32, ushort32);




/*-----------------------------------------------------------------------------
* ID: __vcmpgtuw_vvp
*----------------------------------------------------------------------------*/

/* VCMPGTUW */
__vpred __vcmpgtuw_vvp(uint16, uint16);




/*-----------------------------------------------------------------------------
* ID: __vcmpgtw_vvp
*----------------------------------------------------------------------------*/

/* VCMPGTW */
__vpred __vcmpgtw_vvp(int16, int16);




/*-----------------------------------------------------------------------------
* ID: __vcmpledp_vvp
*----------------------------------------------------------------------------*/

/* VCMPLEDP */
__vpred __vcmpledp_vvp(double8, double8);




/*-----------------------------------------------------------------------------
* ID: __vcmplesp_vvp
*----------------------------------------------------------------------------*/

/* VCMPLESP */
__vpred __vcmplesp_vvp(float16, float16);




/*-----------------------------------------------------------------------------
* ID: __vcmpltdp_vvp
*----------------------------------------------------------------------------*/

/* VCMPLTDP */
__vpred __vcmpltdp_vvp(double8, double8);




/*-----------------------------------------------------------------------------
* ID: __vcmpltsp_vvp
*----------------------------------------------------------------------------*/

/* VCMPLTSP */
__vpred __vcmpltsp_vvp(float16, float16);




/*-----------------------------------------------------------------------------
* ID: __vcmpyhw_vvw
*----------------------------------------------------------------------------*/

/* VCMPYHW */
void __vcmpyhw_vvw(cshort16, cshort16, cint8::EQUIV_ACCESS_T<0>&, cint8::EQUIV_ACCESS_T<0>&);




/*-----------------------------------------------------------------------------
* ID: __vcmpyr1hh_vvv
*----------------------------------------------------------------------------*/

/* VCMPYR1HH */
cshort16 __vcmpyr1hh_vvv(cshort16, cshort16);




/*-----------------------------------------------------------------------------
* ID: __vcmpyr1ww_vvv
*----------------------------------------------------------------------------*/

/* VCMPYR1WW */
cint8 __vcmpyr1ww_vvv(cint8, cint8);




/*-----------------------------------------------------------------------------
* ID: __vcmpyrhh_vvv
*----------------------------------------------------------------------------*/

/* VCMPYRHH */
cshort16 __vcmpyrhh_vvv(cshort16, cshort16);




/*-----------------------------------------------------------------------------
* ID: __vcmpysp_vvw
*----------------------------------------------------------------------------*/

/* VCMPYSP */
void __vcmpysp_vvw(cfloat8, cfloat8, float16::EQUIV_ACCESS_T<0>&, float16::EQUIV_ACCESS_T<0>&);




/*-----------------------------------------------------------------------------
* ID: __vcrot270h_vv
*----------------------------------------------------------------------------*/

/* VCROT270H */
cshort16 __vcrot270h_vv(cshort16);




/*-----------------------------------------------------------------------------
* ID: __vcrot270sp_vv
*----------------------------------------------------------------------------*/

/* VCROT270SP */
cfloat8 __vcrot270sp_vv(cfloat8);




/*-----------------------------------------------------------------------------
* ID: __vcrot270w_vv
*----------------------------------------------------------------------------*/

/* VCROT270W */
cint8 __vcrot270w_vv(cint8);




/*-----------------------------------------------------------------------------
* ID: __vcrot90h_vv
*----------------------------------------------------------------------------*/

/* VCROT90H */
cshort16 __vcrot90h_vv(cshort16);




/*-----------------------------------------------------------------------------
* ID: __vcrot90sp_vv
*----------------------------------------------------------------------------*/

/* VCROT90SP */
cfloat8 __vcrot90sp_vv(cfloat8);




/*-----------------------------------------------------------------------------
* ID: __vcrot90w_vv
*----------------------------------------------------------------------------*/

/* VCROT90W */
cint8 __vcrot90w_vv(cint8);




/*-----------------------------------------------------------------------------
* ID: __vdeal2b_vv
*----------------------------------------------------------------------------*/

/* VDEAL2B */
char64 __vdeal2b_vv(char64);
uchar64 __vdeal2b_vv(uchar64);




/*-----------------------------------------------------------------------------
* ID: __vdeal2h_vv
*----------------------------------------------------------------------------*/

/* VDEAL2H */
short32 __vdeal2h_vv(short32);
ushort32 __vdeal2h_vv(ushort32);
cchar32 __vdeal2h_vv(cchar32);




/*-----------------------------------------------------------------------------
* ID: __vdeal2w_vv
*----------------------------------------------------------------------------*/

/* VDEAL2W */
int16 __vdeal2w_vv(int16);
float16 __vdeal2w_vv(float16);
uint16 __vdeal2w_vv(uint16);
cshort16 __vdeal2w_vv(cshort16);




/*-----------------------------------------------------------------------------
* ID: __vdeal4b_vv
*----------------------------------------------------------------------------*/

/* VDEAL4B */
char64 __vdeal4b_vv(char64);
uchar64 __vdeal4b_vv(uchar64);




/*-----------------------------------------------------------------------------
* ID: __vdeal4h_vv
*----------------------------------------------------------------------------*/

/* VDEAL4H */
short32 __vdeal4h_vv(short32);
ushort32 __vdeal4h_vv(ushort32);
cchar32 __vdeal4h_vv(cchar32);




/*-----------------------------------------------------------------------------
* ID: __vdotp2hd_vvw
*----------------------------------------------------------------------------*/

/* VDOTP2HD */
void __vdotp2hd_vvw(short32, short32, long8::EQUIV_ACCESS_T<0>&, long8::EQUIV_ACCESS_T<0>&);




/*-----------------------------------------------------------------------------
* ID: __vdotp2hw_vvv
*----------------------------------------------------------------------------*/

/* VDOTP2HW */
int16 __vdotp2hw_vvv(short32, short32);




/*-----------------------------------------------------------------------------
* ID: __vdotp2nwd_vvv
*----------------------------------------------------------------------------*/

/* VDOTP2NWD */
long8 __vdotp2nwd_vvv(int16, int16);




/*-----------------------------------------------------------------------------
* ID: __vdotp2nxwd_vvv
*----------------------------------------------------------------------------*/

/* VDOTP2NXWD */
long8 __vdotp2nxwd_vvv(int16, int16);




/*-----------------------------------------------------------------------------
* ID: __vdotp2subh_vvv
*----------------------------------------------------------------------------*/

/* VDOTP2SUBH */
short32 __vdotp2subh_vvv(char64, uchar64);




/*-----------------------------------------------------------------------------
* ID: __vdotp2suhd_vvw
*----------------------------------------------------------------------------*/

/* VDOTP2SUHD */
void __vdotp2suhd_vvw(short32, ushort32, long8::EQUIV_ACCESS_T<0>&, long8::EQUIV_ACCESS_T<0>&);




/*-----------------------------------------------------------------------------
* ID: __vdotp2suhw_vvv
*----------------------------------------------------------------------------*/

/* VDOTP2SUHW */
int16 __vdotp2suhw_vvv(short32, ushort32);




/*-----------------------------------------------------------------------------
* ID: __vdotp2ubh_vvv
*----------------------------------------------------------------------------*/

/* VDOTP2UBH */
ushort32 __vdotp2ubh_vvv(uchar64, uchar64);




/*-----------------------------------------------------------------------------
* ID: __vdotp2uhd_vvw
*----------------------------------------------------------------------------*/

/* VDOTP2UHD */
void __vdotp2uhd_vvw(ushort32, ushort32, ulong8::EQUIV_ACCESS_T<0>&, ulong8::EQUIV_ACCESS_T<0>&);




/*-----------------------------------------------------------------------------
* ID: __vdotp2uhw_vvv
*----------------------------------------------------------------------------*/

/* VDOTP2UHW */
uint16 __vdotp2uhw_vvv(ushort32, ushort32);




/*-----------------------------------------------------------------------------
* ID: __vdotp2wd_vvv
*----------------------------------------------------------------------------*/

/* VDOTP2WD */
long8 __vdotp2wd_vvv(int16, int16);




/*-----------------------------------------------------------------------------
* ID: __vdotp2xwd_vvv
*----------------------------------------------------------------------------*/

/* VDOTP2XWD */
long8 __vdotp2xwd_vvv(int16, int16);




/*-----------------------------------------------------------------------------
* ID: __vdotp4hd_vvv
*----------------------------------------------------------------------------*/

/* VDOTP4HD */
long8 __vdotp4hd_vvv(short32, short32);




/*-----------------------------------------------------------------------------
* ID: __vdotp4hw_wwv
*----------------------------------------------------------------------------*/

/* VDOTP4HW */
int16 __vdotp4hw_wwv(short32, short32, short32, short32);

int16 __vdotp4hw_se1_wwv(__SE_REG_PAIR, short32, short32);
int16 __vdotp4hw_se2_wwv(short32, short32, __SE_REG_PAIR);
int16 __vdotp4hw_se12_wwv(__SE_REG_PAIR, __SE_REG_PAIR);




/*-----------------------------------------------------------------------------
* ID: __vdotp4subw_vvv
*----------------------------------------------------------------------------*/

/* VDOTP4SUBW */
int16 __vdotp4subw_vvv(char64, uchar64);




/*-----------------------------------------------------------------------------
* ID: __vdotp4suhd_vvv
*----------------------------------------------------------------------------*/

/* VDOTP4SUHD */
long8 __vdotp4suhd_vvv(short32, ushort32);




/*-----------------------------------------------------------------------------
* ID: __vdotp4suhw_wwv
*----------------------------------------------------------------------------*/

/* VDOTP4SUHW */
int16 __vdotp4suhw_wwv(short32, short32, ushort32, ushort32);

int16 __vdotp4suhw_se1_wwv(__SE_REG_PAIR, ushort32, ushort32);
int16 __vdotp4suhw_se2_wwv(short32, short32, __SE_REG_PAIR);
int16 __vdotp4suhw_se12_wwv(__SE_REG_PAIR, __SE_REG_PAIR);




/*-----------------------------------------------------------------------------
* ID: __vdotp4ubw_vvv
*----------------------------------------------------------------------------*/

/* VDOTP4UBW */
uint16 __vdotp4ubw_vvv(uchar64, uchar64);




/*-----------------------------------------------------------------------------
* ID: __vdotp4uhd_vvv
*----------------------------------------------------------------------------*/

/* VDOTP4UHD */
ulong8 __vdotp4uhd_vvv(ushort32, ushort32);




/*-----------------------------------------------------------------------------
* ID: __vdotp8subw_vvv
*----------------------------------------------------------------------------*/

/* VDOTP8SUBW */
int16 __vdotp8subw_vvv(char64, uchar64);




/*-----------------------------------------------------------------------------
* ID: __vdotpmpn16b32h_yvvv
*----------------------------------------------------------------------------*/

/* VDOTPMPN16B32H */
short32 __vdotpmpn16b32h_yvvv(ushort32, ushort32, char16);




/*-----------------------------------------------------------------------------
* ID: __vdotpmpn16h16w_yvvv
*----------------------------------------------------------------------------*/

/* VDOTPMPN16H16W */
int16 __vdotpmpn16h16w_yvvv(ushort32, ushort16, short16);




/*-----------------------------------------------------------------------------
* ID: __vdotpmpn16w8d_yvvv
*----------------------------------------------------------------------------*/

/* VDOTPMPN16W8D */
long8 __vdotpmpn16w8d_yvvv(ushort32, ushort8, int16);




/*-----------------------------------------------------------------------------
* ID: __vdotpmpn32b16h_yvvv
*----------------------------------------------------------------------------*/

/* VDOTPMPN32B16H */
short16 __vdotpmpn32b16h_yvvv(uint16, uint16, char32);




/*-----------------------------------------------------------------------------
* ID: __vdotpmpn32b16w_yvvv
*----------------------------------------------------------------------------*/

/* VDOTPMPN32B16W */
int16 __vdotpmpn32b16w_yvvv(uint16, uint16, char32);




/*-----------------------------------------------------------------------------
* ID: __vdotpmpn32h8w_yvvv
*----------------------------------------------------------------------------*/

/* VDOTPMPN32H8W */
int8 __vdotpmpn32h8w_yvvv(uint8, uint8, short32);




/*-----------------------------------------------------------------------------
* ID: __vdotpmpnu16h16w_yvvv
*----------------------------------------------------------------------------*/

/* VDOTPMPNU16H16W */
uint16 __vdotpmpnu16h16w_yvvv(ushort32, ushort16, ushort16);




/*-----------------------------------------------------------------------------
* ID: __vdotpmpnu16w8d_yvvv
*----------------------------------------------------------------------------*/

/* VDOTPMPNU16W8D */
ulong8 __vdotpmpnu16w8d_yvvv(ushort32, ushort8, uint16);




/*-----------------------------------------------------------------------------
* ID: __vdotpmpnu32b16h_yvvv
*----------------------------------------------------------------------------*/

/* VDOTPMPNU32B16H */
ushort16 __vdotpmpnu32b16h_yvvv(uint16, uint16, uchar32);




/*-----------------------------------------------------------------------------
* ID: __vdotpmpnu32b16w_yvvv
*----------------------------------------------------------------------------*/

/* VDOTPMPNU32B16W */
uint16 __vdotpmpnu32b16w_yvvv(uint16, uint16, uchar32);




/*-----------------------------------------------------------------------------
* ID: __vdotpmpnu32h8w_yvvv
*----------------------------------------------------------------------------*/

/* VDOTPMPNU32H8W */
uint8 __vdotpmpnu32h8w_yvvv(uint8, uint8, ushort32);




/*-----------------------------------------------------------------------------
* ID: __vdpackh_vvv
*----------------------------------------------------------------------------*/

/* VDPACKH */
long8 __vdpackh_vvv(long8, long8);
double8 __vdpackh_vvv(double8, double8);
cfloat8 __vdpackh_vvv(cfloat8, cfloat8);
ulong8 __vdpackh_vvv(ulong8, ulong8);
cint8 __vdpackh_vvv(cint8, cint8);




/*-----------------------------------------------------------------------------
* ID: __vdpackl_vvv
*----------------------------------------------------------------------------*/

/* VDPACKL */
long8 __vdpackl_vvv(long8, long8);
double8 __vdpackl_vvv(double8, double8);
cfloat8 __vdpackl_vvv(cfloat8, cfloat8);
ulong8 __vdpackl_vvv(ulong8, ulong8);
cint8 __vdpackl_vvv(cint8, cint8);




/*-----------------------------------------------------------------------------
* ID: __vdpint_vv
*----------------------------------------------------------------------------*/

/* VDPINT */
int16 __vdpint_vv(double8);




/*-----------------------------------------------------------------------------
* ID: __vdpsp_vv
*----------------------------------------------------------------------------*/

/* VDPSP */
float16 __vdpsp_vv(double8);




/*-----------------------------------------------------------------------------
* ID: __vdptrunc_vv
*----------------------------------------------------------------------------*/

/* VDPTRUNC */
int16 __vdptrunc_vv(double8);




/*-----------------------------------------------------------------------------
* ID: __vdsad16ou8h8w_vvv
*----------------------------------------------------------------------------*/

/* VDSAD16OU8H8W */
uint16 __vdsad16ou8h8w_vvv(ushort32, ushort16);




/*-----------------------------------------------------------------------------
* ID: __vdsad8ou16b16h_vvv
*----------------------------------------------------------------------------*/

/* VDSAD8OU16B16H */
ushort32 __vdsad8ou16b16h_vvv(uchar64, uchar32);




/*-----------------------------------------------------------------------------
* ID: __vdsadm16ou8h8w_yvvv
*----------------------------------------------------------------------------*/

/* VDSADM16OU8H8W */
uint16 __vdsadm16ou8h8w_yvvv(uchar64, ushort32, ushort16);




/*-----------------------------------------------------------------------------
* ID: __vdsadm8ou16b16h_yvvv
*----------------------------------------------------------------------------*/

/* VDSADM8OU16B16H */
ushort32 __vdsadm8ou16b16h_yvvv(uchar64, uchar64, uchar32);




/*-----------------------------------------------------------------------------
* ID: __vdsortdd16h_vv
*----------------------------------------------------------------------------*/

/* VDSORTDD16H */
short32 __vdsortdd16h_vv(short32);




/*-----------------------------------------------------------------------------
* ID: __vdsortddu16h_vv
*----------------------------------------------------------------------------*/

/* VDSORTDDU16H */
ushort32 __vdsortddu16h_vv(ushort32);




/*-----------------------------------------------------------------------------
* ID: __vdsortdi16h_vv
*----------------------------------------------------------------------------*/

/* VDSORTDI16H */
short32 __vdsortdi16h_vv(short32);




/*-----------------------------------------------------------------------------
* ID: __vdsortdiu16h_vv
*----------------------------------------------------------------------------*/

/* VDSORTDIU16H */
ushort32 __vdsortdiu16h_vv(ushort32);




/*-----------------------------------------------------------------------------
* ID: __vdsortid16h_vv
*----------------------------------------------------------------------------*/

/* VDSORTID16H */
short32 __vdsortid16h_vv(short32);




/*-----------------------------------------------------------------------------
* ID: __vdsortidu16h_vv
*----------------------------------------------------------------------------*/

/* VDSORTIDU16H */
ushort32 __vdsortidu16h_vv(ushort32);




/*-----------------------------------------------------------------------------
* ID: __vdsortii16h_vv
*----------------------------------------------------------------------------*/

/* VDSORTII16H */
short32 __vdsortii16h_vv(short32);




/*-----------------------------------------------------------------------------
* ID: __vdsortiiu16h_vv
*----------------------------------------------------------------------------*/

/* VDSORTIIU16H */
ushort32 __vdsortiiu16h_vv(ushort32);




/*-----------------------------------------------------------------------------
* ID: __vdsortpdd16h_vv
*----------------------------------------------------------------------------*/

/* VDSORTPDD16H */
uchar64 __vdsortpdd16h_vv(short32);




/*-----------------------------------------------------------------------------
* ID: __vdsortpddu16h_vv
*----------------------------------------------------------------------------*/

/* VDSORTPDDU16H */
uchar64 __vdsortpddu16h_vv(ushort32);




/*-----------------------------------------------------------------------------
* ID: __vdsortpdi16h_vv
*----------------------------------------------------------------------------*/

/* VDSORTPDI16H */
uchar64 __vdsortpdi16h_vv(short32);




/*-----------------------------------------------------------------------------
* ID: __vdsortpdiu16h_vv
*----------------------------------------------------------------------------*/

/* VDSORTPDIU16H */
uchar64 __vdsortpdiu16h_vv(ushort32);




/*-----------------------------------------------------------------------------
* ID: __vdsortpid16h_vv
*----------------------------------------------------------------------------*/

/* VDSORTPID16H */
uchar64 __vdsortpid16h_vv(short32);




/*-----------------------------------------------------------------------------
* ID: __vdsortpidu16h_vv
*----------------------------------------------------------------------------*/

/* VDSORTPIDU16H */
uchar64 __vdsortpidu16h_vv(ushort32);




/*-----------------------------------------------------------------------------
* ID: __vdsortpii16h_vv
*----------------------------------------------------------------------------*/

/* VDSORTPII16H */
uchar64 __vdsortpii16h_vv(short32);




/*-----------------------------------------------------------------------------
* ID: __vdsortpiiu16h_vv
*----------------------------------------------------------------------------*/

/* VDSORTPIIU16H */
uchar64 __vdsortpiiu16h_vv(ushort32);




/*-----------------------------------------------------------------------------
* ID: __vdup16b_vv
*----------------------------------------------------------------------------*/

/* VDUP16B */
char64 __vdup16b_vv(char4);
uchar64 __vdup16b_vv(uchar4);




/*-----------------------------------------------------------------------------
* ID: __vdup16h_vv
*----------------------------------------------------------------------------*/

/* VDUP16H */
short32 __vdup16h_vv(short2);
ushort32 __vdup16h_vv(ushort2);
cchar32 __vdup16h_vv(cchar2);




/*-----------------------------------------------------------------------------
* ID: __vdup2b_vv
*----------------------------------------------------------------------------*/

/* VDUP2B */
char64 __vdup2b_vv(char32);
uchar64 __vdup2b_vv(uchar32);




/*-----------------------------------------------------------------------------
* ID: __vdup2d_vv
*----------------------------------------------------------------------------*/

/* VDUP2D */
long8 __vdup2d_vv(long4);
double8 __vdup2d_vv(double4);
cfloat8 __vdup2d_vv(cfloat4);
ulong8 __vdup2d_vv(ulong4);
cint8 __vdup2d_vv(cint4);




/*-----------------------------------------------------------------------------
* ID: __vdup2h_vv
*----------------------------------------------------------------------------*/

/* VDUP2H */
short32 __vdup2h_vv(short16);
ushort32 __vdup2h_vv(ushort16);
cchar32 __vdup2h_vv(cchar16);




/*-----------------------------------------------------------------------------
* ID: __vdup2w_vv
*----------------------------------------------------------------------------*/

/* VDUP2W */
int16 __vdup2w_vv(int8);
float16 __vdup2w_vv(float8);
uint16 __vdup2w_vv(uint8);
cshort16 __vdup2w_vv(cshort8);




/*-----------------------------------------------------------------------------
* ID: __vdup32b_vv
*----------------------------------------------------------------------------*/

/* VDUP32B */
char64 __vdup32b_vv(char2);
uchar64 __vdup32b_vv(uchar2);




/*-----------------------------------------------------------------------------
* ID: __vdup4b_vv
*----------------------------------------------------------------------------*/

/* VDUP4B */
char64 __vdup4b_vv(char16);
uchar64 __vdup4b_vv(uchar16);




/*-----------------------------------------------------------------------------
* ID: __vdup4d_vv
*----------------------------------------------------------------------------*/

/* VDUP4D */
long8 __vdup4d_vv(long2);
double8 __vdup4d_vv(double2);
cfloat8 __vdup4d_vv(cfloat2);
ulong8 __vdup4d_vv(ulong2);
cint8 __vdup4d_vv(cint2);




/*-----------------------------------------------------------------------------
* ID: __vdup4h_vv
*----------------------------------------------------------------------------*/

/* VDUP4H */
short32 __vdup4h_vv(short8);
ushort32 __vdup4h_vv(ushort8);
cchar32 __vdup4h_vv(cchar8);




/*-----------------------------------------------------------------------------
* ID: __vdup4w_vv
*----------------------------------------------------------------------------*/

/* VDUP4W */
int16 __vdup4w_vv(int4);
float16 __vdup4w_vv(float4);
uint16 __vdup4w_vv(uint4);
cshort16 __vdup4w_vv(cshort4);




/*-----------------------------------------------------------------------------
* ID: __vdup8b_vv
*----------------------------------------------------------------------------*/

/* VDUP8B */
char64 __vdup8b_vv(char8);
uchar64 __vdup8b_vv(uchar8);




/*-----------------------------------------------------------------------------
* ID: __vdup8h_vv
*----------------------------------------------------------------------------*/

/* VDUP8H */
short32 __vdup8h_vv(short4);
ushort32 __vdup8h_vv(ushort4);
cchar32 __vdup8h_vv(cchar4);




/*-----------------------------------------------------------------------------
* ID: __vdup8w_vv
*----------------------------------------------------------------------------*/

/* VDUP8W */
int16 __vdup8w_vv(int2);
float16 __vdup8w_vv(float2);
uint16 __vdup8w_vv(uint2);
cshort16 __vdup8w_vv(cshort2);




/*-----------------------------------------------------------------------------
* ID: __vdupb_kv
*----------------------------------------------------------------------------*/

/* VDUPB */
char64 __vdupb_kv(int8_t);
uchar64 __vdupb_kv(uchar);




/*-----------------------------------------------------------------------------
* ID: __vdupb_rv
*----------------------------------------------------------------------------*/

/* VDUPB */
char64 __vdupb_rv(int8_t);
uchar64 __vdupb_rv(uchar);




/*-----------------------------------------------------------------------------
* ID: __vdupd_dv
*----------------------------------------------------------------------------*/

/* VDUPD */
long8 __vdupd_dv(int64_t);
double8 __vdupd_dv(double);
cfloat8 __vdupd_dv(cfloat);
ulong8 __vdupd_dv(ulong);
cint8 __vdupd_dv(cint);




/*-----------------------------------------------------------------------------
* ID: __vduph_kv
*----------------------------------------------------------------------------*/

/* VDUPH */
short32 __vduph_kv(int16_t);
ushort32 __vduph_kv(ushort);
cchar32 __vduph_kv(cchar);




/*-----------------------------------------------------------------------------
* ID: __vduph_rv
*----------------------------------------------------------------------------*/

/* VDUPH */
short32 __vduph_rv(int16_t);
ushort32 __vduph_rv(ushort);
cchar32 __vduph_rv(cchar);




/*-----------------------------------------------------------------------------
* ID: __vdupw_kv
*----------------------------------------------------------------------------*/

/* VDUPW */
int16 __vdupw_kv(int32_t);
float16 __vdupw_kv(float);
uint16 __vdupw_kv(uint);
cshort16 __vdupw_kv(cshort);




/*-----------------------------------------------------------------------------
* ID: __vdupw_rv
*----------------------------------------------------------------------------*/

/* VDUPW */
int16 __vdupw_rv(int32_t);
float16 __vdupw_rv(float);
uint16 __vdupw_rv(uint);
cshort16 __vdupw_rv(cshort);




/*-----------------------------------------------------------------------------
* ID: __vfir4hw_vww
*----------------------------------------------------------------------------*/

/* VFIR4HW */
void __vfir4hw_vww(short32, __SE_REG_PAIR, int16::EQUIV_ACCESS_T<0>&, int16::EQUIV_ACCESS_T<0>&);




/*-----------------------------------------------------------------------------
* ID: __vfir4suhw_vww
*----------------------------------------------------------------------------*/

/* VFIR4SUHW */
void __vfir4suhw_vww(short32, __SE_REG_PAIR, int16::EQUIV_ACCESS_T<0>&, int16::EQUIV_ACCESS_T<0>&);




/*-----------------------------------------------------------------------------
* ID: __vfir4uhw_vww
*----------------------------------------------------------------------------*/

/* VFIR4UHW */
void __vfir4uhw_vww(ushort32, __SE_REG_PAIR, uint16::EQUIV_ACCESS_T<0>&, uint16::EQUIV_ACCESS_T<0>&);




/*-----------------------------------------------------------------------------
* ID: __vfir8hd_wvw
*----------------------------------------------------------------------------*/

/* VFIR8HD */
void __vfir8hd_wvw(short32, short32, __SE_REG, long8::EQUIV_ACCESS_T<0>&, long8::EQUIV_ACCESS_T<0>&);




/*-----------------------------------------------------------------------------
* ID: __vfir8hw_wvv
*----------------------------------------------------------------------------*/

/* VFIR8HW */
int16 __vfir8hw_wvv(short32, short32, __SE_REG);




/*-----------------------------------------------------------------------------
* ID: __vfir8suhd_wvw
*----------------------------------------------------------------------------*/

/* VFIR8SUHD */
void __vfir8suhd_wvw(short32, short32, __SE_REG, long8::EQUIV_ACCESS_T<0>&, long8::EQUIV_ACCESS_T<0>&);




/*-----------------------------------------------------------------------------
* ID: __vfir8suhw_wvv
*----------------------------------------------------------------------------*/

/* VFIR8SUHW */
int16 __vfir8suhw_wvv(short32, short32, __SE_REG);




/*-----------------------------------------------------------------------------
* ID: __vfir8uhd_wvw
*----------------------------------------------------------------------------*/

/* VFIR8UHD */
void __vfir8uhd_wvw(ushort32, ushort32, __SE_REG, ulong8::EQUIV_ACCESS_T<0>&, ulong8::EQUIV_ACCESS_T<0>&);




/*-----------------------------------------------------------------------------
* ID: __vfir8uhw_wvv
*----------------------------------------------------------------------------*/

/* VFIR8UHW */
uint16 __vfir8uhw_wvv(ushort32, ushort32, __SE_REG);




/*-----------------------------------------------------------------------------
* ID: __vgatherb_pvv
*----------------------------------------------------------------------------*/

/* VGATHERB */
char64 __vgatherb_pvv(__vpred, char64);




/*-----------------------------------------------------------------------------
* ID: __vgathernb_pvv
*----------------------------------------------------------------------------*/

/* VGATHERNB */
char64 __vgathernb_pvv(__vpred, char64);




/*-----------------------------------------------------------------------------
* ID: __vgetb_vkd
*----------------------------------------------------------------------------*/

/* VGETB */
int8_t __vgetb_vkd(char64, uchar);




/*-----------------------------------------------------------------------------
* ID: __vgetb_vrd
*----------------------------------------------------------------------------*/

/* VGETB */
int8_t __vgetb_vrd(char64, uint);




/*-----------------------------------------------------------------------------
* ID: __vgetd_vkd
*----------------------------------------------------------------------------*/

/* VGETD */
int64_t __vgetd_vkd(long8, uint);




/*-----------------------------------------------------------------------------
* ID: __vgetd_vrd
*----------------------------------------------------------------------------*/

/* VGETD */
int64_t __vgetd_vrd(long8, uint);




/*-----------------------------------------------------------------------------
* ID: __vgetdupb_vrv
*----------------------------------------------------------------------------*/

/* VGETDUPB */
uchar64 __vgetdupb_vrv(uchar64, uint);
char64 __vgetdupb_vrv(char64, int32_t);




/*-----------------------------------------------------------------------------
* ID: __vgetdupd_vrv
*----------------------------------------------------------------------------*/

/* VGETDUPD */
ulong8 __vgetdupd_vrv(ulong8, uint);
long8 __vgetdupd_vrv(long8, int32_t);




/*-----------------------------------------------------------------------------
* ID: __vgetduph_vrv
*----------------------------------------------------------------------------*/

/* VGETDUPH */
ushort32 __vgetduph_vrv(ushort32, uint);
short32 __vgetduph_vrv(short32, int32_t);




/*-----------------------------------------------------------------------------
* ID: __vgetdupw_vrv
*----------------------------------------------------------------------------*/

/* VGETDUPW */
uint16 __vgetdupw_vrv(uint16, uint);
int16 __vgetdupw_vrv(int16, int32_t);




/*-----------------------------------------------------------------------------
* ID: __vgeth_vkd
*----------------------------------------------------------------------------*/

/* VGETH */
int16_t __vgeth_vkd(short32, uint);




/*-----------------------------------------------------------------------------
* ID: __vgeth_vrd
*----------------------------------------------------------------------------*/

/* VGETH */
int16_t __vgeth_vrd(short32, uint);




/*-----------------------------------------------------------------------------
* ID: __vgetub_vkd
*----------------------------------------------------------------------------*/

/* VGETUB */
uchar __vgetub_vkd(uchar64, uint);




/*-----------------------------------------------------------------------------
* ID: __vgetub_vrd
*----------------------------------------------------------------------------*/

/* VGETUB */
uchar __vgetub_vrd(uchar64, uint);




/*-----------------------------------------------------------------------------
* ID: __vgetuh_vkd
*----------------------------------------------------------------------------*/

/* VGETUH */
ushort __vgetuh_vkd(ushort32, uint);




/*-----------------------------------------------------------------------------
* ID: __vgetuh_vrd
*----------------------------------------------------------------------------*/

/* VGETUH */
ushort __vgetuh_vrd(ushort32, uint);




/*-----------------------------------------------------------------------------
* ID: __vgetuw_vkd
*----------------------------------------------------------------------------*/

/* VGETUW */
uint __vgetuw_vkd(uint16, uint);




/*-----------------------------------------------------------------------------
* ID: __vgetuw_vrd
*----------------------------------------------------------------------------*/

/* VGETUW */
uint __vgetuw_vrd(uint16, uint);




/*-----------------------------------------------------------------------------
* ID: __vgetw_vkd
*----------------------------------------------------------------------------*/

/* VGETW */
int32_t __vgetw_vkd(int16, uchar);




/*-----------------------------------------------------------------------------
* ID: __vgetw_vrd
*----------------------------------------------------------------------------*/

/* VGETW */
int32_t __vgetw_vrd(int16, uint);




/*-----------------------------------------------------------------------------
* ID: __vgmpyb_vvv
*----------------------------------------------------------------------------*/

/* VGMPYB */
uchar64 __vgmpyb_vvv(uchar64, uchar64);




/*-----------------------------------------------------------------------------
* ID: __vgmpyw_vvv
*----------------------------------------------------------------------------*/

/* VGMPYW */
uint16 __vgmpyw_vvv(uint16, uint16);




/*-----------------------------------------------------------------------------
* ID: __vgsatd_vkv
*----------------------------------------------------------------------------*/

/* VGSATD */
long8 __vgsatd_vkv(long8, uchar8);




/*-----------------------------------------------------------------------------
* ID: __vgsath_vkv
*----------------------------------------------------------------------------*/

/* VGSATH */
short32 __vgsath_vkv(short32, uchar32);




/*-----------------------------------------------------------------------------
* ID: __vgsatsud_vkv
*----------------------------------------------------------------------------*/

/* VGSATSUD */
ulong8 __vgsatsud_vkv(long8, uchar8);




/*-----------------------------------------------------------------------------
* ID: __vgsatsuh_vkv
*----------------------------------------------------------------------------*/

/* VGSATSUH */
ushort32 __vgsatsuh_vkv(short32, uchar32);




/*-----------------------------------------------------------------------------
* ID: __vgsatsuw_vkv
*----------------------------------------------------------------------------*/

/* VGSATSUW */
uint16 __vgsatsuw_vkv(int16, uchar16);




/*-----------------------------------------------------------------------------
* ID: __vgsatud_vkv
*----------------------------------------------------------------------------*/

/* VGSATUD */
ulong8 __vgsatud_vkv(ulong8, uchar8);




/*-----------------------------------------------------------------------------
* ID: __vgsatuh_vkv
*----------------------------------------------------------------------------*/

/* VGSATUH */
ushort32 __vgsatuh_vkv(ushort32, uchar32);




/*-----------------------------------------------------------------------------
* ID: __vgsatuw_vkv
*----------------------------------------------------------------------------*/

/* VGSATUW */
uint16 __vgsatuw_vkv(uint16, uchar16);




/*-----------------------------------------------------------------------------
* ID: __vgsatw_vkv
*----------------------------------------------------------------------------*/

/* VGSATW */
int16 __vgsatw_vkv(int16, uchar16);




/*-----------------------------------------------------------------------------
* ID: __vhadd16w1d_vd
*----------------------------------------------------------------------------*/

/* VHADD16W1D */
int64_t __vhadd16w1d_vd(int16);




/*-----------------------------------------------------------------------------
* ID: __vhadd32h1d_vd
*----------------------------------------------------------------------------*/

/* VHADD32H1D */
int64_t __vhadd32h1d_vd(short32);




/*-----------------------------------------------------------------------------
* ID: __vhadd64b1d_vd
*----------------------------------------------------------------------------*/

/* VHADD64B1D */
int64_t __vhadd64b1d_vd(char64);




/*-----------------------------------------------------------------------------
* ID: __vhadd8d1d_vd
*----------------------------------------------------------------------------*/

/* VHADD8D1D */
int64_t __vhadd8d1d_vd(long8);




/*-----------------------------------------------------------------------------
* ID: __vhaddeo16h2w_vd
*----------------------------------------------------------------------------*/

/* VHADDEO16H2W */
int2 __vhaddeo16h2w_vd(short32);
cint __vhaddeo16h2w_vd(cshort16);




/*-----------------------------------------------------------------------------
* ID: __vhaddeo8w2d_vv
*----------------------------------------------------------------------------*/

/* VHADDEO8W2D */
long2 __vhaddeo8w2d_vv(int16);
clong __vhaddeo8w2d_vv(cint8);




/*-----------------------------------------------------------------------------
* ID: __vhaddeo8w4d_vvv
*----------------------------------------------------------------------------*/

/* VHADDEO8W4D */
long4 __vhaddeo8w4d_vvv(int16, int16);
clong2 __vhaddeo8w4d_vvv(cint8, cint8);




/*-----------------------------------------------------------------------------
* ID: __vhaddu16w1d_vd
*----------------------------------------------------------------------------*/

/* VHADDU16W1D */
ulong __vhaddu16w1d_vd(uint16);




/*-----------------------------------------------------------------------------
* ID: __vhaddu32h1d_vd
*----------------------------------------------------------------------------*/

/* VHADDU32H1D */
ulong __vhaddu32h1d_vd(ushort32);




/*-----------------------------------------------------------------------------
* ID: __vhaddu64b1d_vd
*----------------------------------------------------------------------------*/

/* VHADDU64B1D */
ulong __vhaddu64b1d_vd(uchar64);




/*-----------------------------------------------------------------------------
* ID: __vhaddu8d1d_vd
*----------------------------------------------------------------------------*/

/* VHADDU8D1D */
ulong __vhaddu8d1d_vd(ulong8);




/*-----------------------------------------------------------------------------
* ID: __vhaddueo16h2w_vd
*----------------------------------------------------------------------------*/

/* VHADDUEO16H2W */
uint2 __vhaddueo16h2w_vd(ushort32);




/*-----------------------------------------------------------------------------
* ID: __vhaddueo8w2d_vv
*----------------------------------------------------------------------------*/

/* VHADDUEO8W2D */
ulong2 __vhaddueo8w2d_vv(uint16);




/*-----------------------------------------------------------------------------
* ID: __vhaddueo8w4d_vvv
*----------------------------------------------------------------------------*/

/* VHADDUEO8W4D */
ulong4 __vhaddueo8w4d_vvv(uint16, uint16);




/*-----------------------------------------------------------------------------
* ID: __vhhmv_vvv
*----------------------------------------------------------------------------*/

/* VHHMV */
uchar64 __vhhmv_vvv(uchar64, uchar64);




/*-----------------------------------------------------------------------------
* ID: __vhlmv_vvv
*----------------------------------------------------------------------------*/

/* VHLMV */
uchar64 __vhlmv_vvv(uchar64, uchar64);




/*-----------------------------------------------------------------------------
* ID: __vhpackh_vvv
*----------------------------------------------------------------------------*/

/* VHPACKH */
short32 __vhpackh_vvv(short32, short32);
ushort32 __vhpackh_vvv(ushort32, ushort32);
cchar32 __vhpackh_vvv(cchar32, cchar32);




/*-----------------------------------------------------------------------------
* ID: __vhpackl_vvv
*----------------------------------------------------------------------------*/

/* VHPACKL */
short32 __vhpackl_vvv(short32, short32);
ushort32 __vhpackl_vvv(ushort32, ushort32);
cchar32 __vhpackl_vvv(cchar32, cchar32);


/*-----------------------------------------------------------------------------
* ID: __vhpint_vv
*----------------------------------------------------------------------------*/

/* VHPINT */
int16 __vhpint_vv(uint16);


/*-----------------------------------------------------------------------------
* ID: __vhpsp_vv
*----------------------------------------------------------------------------*/

/* VHPSP */
float16 __vhpsp_vv(uint16);


/*-----------------------------------------------------------------------------
* ID: __vhunpkd_vv
*----------------------------------------------------------------------------*/

/* VHUNPKD */
long8 __vhunpkd_vv(short8);




/*-----------------------------------------------------------------------------
* ID: __vhunpkdu_vv
*----------------------------------------------------------------------------*/

/* VHUNPKDU */
ulong8 __vhunpkdu_vv(ushort8);




/*-----------------------------------------------------------------------------
* ID: __vhunpkuwh_vv
*----------------------------------------------------------------------------*/

/* VHUNPKUWH */
uint16 __vhunpkuwh_vv(ushort32);




/*-----------------------------------------------------------------------------
* ID: __vhunpkuwl_vv
*----------------------------------------------------------------------------*/

/* VHUNPKUWL */
uint16 __vhunpkuwl_vv(ushort32);




/*-----------------------------------------------------------------------------
* ID: __vhunpkw_vv
*----------------------------------------------------------------------------*/

/* VHUNPKW */
int16 __vhunpkw_vv(short16);




/*-----------------------------------------------------------------------------
* ID: __vhunpkwh_vv
*----------------------------------------------------------------------------*/

/* VHUNPKWH */
int16 __vhunpkwh_vv(short32);




/*-----------------------------------------------------------------------------
* ID: __vhunpkwl_vv
*----------------------------------------------------------------------------*/

/* VHUNPKWL */
int16 __vhunpkwl_vv(short32);




/*-----------------------------------------------------------------------------
* ID: __vhunpkwu_vv
*----------------------------------------------------------------------------*/

/* VHUNPKWU */
uint16 __vhunpkwu_vv(ushort16);




/*-----------------------------------------------------------------------------
* ID: __vhxor16w1w_vr
*----------------------------------------------------------------------------*/

/* VHXOR16W1W */
int32_t __vhxor16w1w_vr(int16);




/*-----------------------------------------------------------------------------
* ID: __vhxor32h1h_vr
*----------------------------------------------------------------------------*/

/* VHXOR32H1H */
int16_t __vhxor32h1h_vr(short32);




/*-----------------------------------------------------------------------------
* ID: __vhxor64b1b_vr
*----------------------------------------------------------------------------*/

/* VHXOR64B1B */
int16_t __vhxor64b1b_vr(char64);




/*-----------------------------------------------------------------------------
* ID: __vhxor8d1d_vd
*----------------------------------------------------------------------------*/

/* VHXOR8D1D */
int64_t __vhxor8d1d_vd(long8);




/*-----------------------------------------------------------------------------
* ID: __vintdph_vv
*----------------------------------------------------------------------------*/

/* VINTDPH */
double8 __vintdph_vv(int16);




/*-----------------------------------------------------------------------------
* ID: __vintdpl_vv
*----------------------------------------------------------------------------*/

/* VINTDPL */
double8 __vintdpl_vv(int16);




/*-----------------------------------------------------------------------------
* ID: __vintdpuh_vv
*----------------------------------------------------------------------------*/

/* VINTDPUH */
double8 __vintdpuh_vv(uint16);




/*-----------------------------------------------------------------------------
* ID: __vintdpul_vv
*----------------------------------------------------------------------------*/

/* VINTDPUL */
double8 __vintdpul_vv(uint16);


/*-----------------------------------------------------------------------------
* ID: __vinthp_vv
*----------------------------------------------------------------------------*/

/* VINTHP */
uint16 __vinthp_vv(int16);



/*-----------------------------------------------------------------------------
* ID: __vinthsph_vv
*----------------------------------------------------------------------------*/

/* VINTHSPH */
float16 __vinthsph_vv(short32);


/*-----------------------------------------------------------------------------
* ID: __vinthspl_vv
*----------------------------------------------------------------------------*/

/* VINTHSPL */
float16 __vinthspl_vv(short32);




/*-----------------------------------------------------------------------------
* ID: __vinthspuh_vv
*----------------------------------------------------------------------------*/

/* VINTHSPUH */
float16 __vinthspuh_vv(ushort32);




/*-----------------------------------------------------------------------------
* ID: __vinthspul_vv
*----------------------------------------------------------------------------*/

/* VINTHSPUL */
float16 __vinthspul_vv(ushort32);




/*-----------------------------------------------------------------------------
* ID: __vintsp_vv
*----------------------------------------------------------------------------*/

/* VINTSP */
float16 __vintsp_vv(int16);




/*-----------------------------------------------------------------------------
* ID: __vintspu_vv
*----------------------------------------------------------------------------*/

/* VINTSPU */
float16 __vintspu_vv(uint16);




/*-----------------------------------------------------------------------------
* ID: __vlhmv_vvv
*----------------------------------------------------------------------------*/

/* VLHMV */
uchar64 __vlhmv_vvv(uchar64, uchar64);




/*-----------------------------------------------------------------------------
* ID: __vllmv_vvv
*----------------------------------------------------------------------------*/

/* VLLMV */
uchar64 __vllmv_vvv(uchar64, uchar64);




/*-----------------------------------------------------------------------------
* ID: __vlmbd0b_vv
*----------------------------------------------------------------------------*/

/* VLMBD0B */
uchar64 __vlmbd0b_vv(uchar64);




/*-----------------------------------------------------------------------------
* ID: __vlmbd0d_vv
*----------------------------------------------------------------------------*/

/* VLMBD0D */
ulong8 __vlmbd0d_vv(ulong8);




/*-----------------------------------------------------------------------------
* ID: __vlmbd0h_vv
*----------------------------------------------------------------------------*/

/* VLMBD0H */
ushort32 __vlmbd0h_vv(ushort32);




/*-----------------------------------------------------------------------------
* ID: __vlmbd0w_vv
*----------------------------------------------------------------------------*/

/* VLMBD0W */
uint16 __vlmbd0w_vv(uint16);




/*-----------------------------------------------------------------------------
* ID: __vlmbd1b_vv
*----------------------------------------------------------------------------*/

/* VLMBD1B */
uchar64 __vlmbd1b_vv(uchar64);




/*-----------------------------------------------------------------------------
* ID: __vlmbd1d_vv
*----------------------------------------------------------------------------*/

/* VLMBD1D */
ulong8 __vlmbd1d_vv(ulong8);




/*-----------------------------------------------------------------------------
* ID: __vlmbd1h_vv
*----------------------------------------------------------------------------*/

/* VLMBD1H */
ushort32 __vlmbd1h_vv(ushort32);




/*-----------------------------------------------------------------------------
* ID: __vlmbd1w_vv
*----------------------------------------------------------------------------*/

/* VLMBD1W */
uint16 __vlmbd1w_vv(uint16);




/*-----------------------------------------------------------------------------
* ID: __vlmbdb_vvv
*----------------------------------------------------------------------------*/

/* VLMBDB */
uchar64 __vlmbdb_vvv(uchar64, uchar64);




/*-----------------------------------------------------------------------------
* ID: __vlmbdd_vvv
*----------------------------------------------------------------------------*/

/* VLMBDD */
ulong8 __vlmbdd_vvv(ulong8, ulong8);




/*-----------------------------------------------------------------------------
* ID: __vlmbdh_vvv
*----------------------------------------------------------------------------*/

/* VLMBDH */
ushort32 __vlmbdh_vvv(ushort32, ushort32);




/*-----------------------------------------------------------------------------
* ID: __vlmbdw_vvv
*----------------------------------------------------------------------------*/

/* VLMBDW */
uint16 __vlmbdw_vvv(uint16, uint16);




/*-----------------------------------------------------------------------------
* ID: __vmatmpyhd_vvw
*----------------------------------------------------------------------------*/

/* VMATMPYHD */
void __vmatmpyhd_vvw(__SE_REG, __SE_REG, long8::EQUIV_ACCESS_T<0>&, long8::EQUIV_ACCESS_T<0>&);




/*-----------------------------------------------------------------------------
* ID: __vmatmpyhw_vvv
*----------------------------------------------------------------------------*/

/* VMATMPYHW */
int16 __vmatmpyhw_vvv(__SE_REG, __SE_REG);




/*-----------------------------------------------------------------------------
* ID: __vmatmpysp_vww
*----------------------------------------------------------------------------*/

/* VMATMPYSP */
void __vmatmpysp_vww(float16, float16, float16, float16::EQUIV_ACCESS_T<0>&, float16::EQUIV_ACCESS_T<0>&);

void __vmatmpysp_se_vww(float16, __SE_REG_PAIR, float16::EQUIV_ACCESS_T<0>&, float16::EQUIV_ACCESS_T<0>&);




/*-----------------------------------------------------------------------------
* ID: __vmatmpysuhd_vvw
*----------------------------------------------------------------------------*/

/* VMATMPYSUHD */
void __vmatmpysuhd_vvw(__SE_REG, __SE_REG, long8::EQUIV_ACCESS_T<0>&, long8::EQUIV_ACCESS_T<0>&);




/*-----------------------------------------------------------------------------
* ID: __vmatmpysuhw_vvv
*----------------------------------------------------------------------------*/

/* VMATMPYSUHW */
int16 __vmatmpysuhw_vvv(__SE_REG, __SE_REG);




/*-----------------------------------------------------------------------------
* ID: __vmatmpyuhd_vvw
*----------------------------------------------------------------------------*/

/* VMATMPYUHD */
void __vmatmpyuhd_vvw(__SE_REG, __SE_REG, ulong8::EQUIV_ACCESS_T<0>&, ulong8::EQUIV_ACCESS_T<0>&);




/*-----------------------------------------------------------------------------
* ID: __vmatmpyuhw_vvv
*----------------------------------------------------------------------------*/

/* VMATMPYUHW */
uint16 __vmatmpyuhw_vvv(__SE_REG, __SE_REG);




/*-----------------------------------------------------------------------------
* ID: __vmatmpyushd_vvw
*----------------------------------------------------------------------------*/

/* VMATMPYUSHD */
void __vmatmpyushd_vvw(__SE_REG, __SE_REG, long8::EQUIV_ACCESS_T<0>&, long8::EQUIV_ACCESS_T<0>&);




/*-----------------------------------------------------------------------------
* ID: __vmatmpyushw_vvv
*----------------------------------------------------------------------------*/

/* VMATMPYUSHW */
int16 __vmatmpyushw_vvv(__SE_REG, __SE_REG);




/*-----------------------------------------------------------------------------
* ID: __vmaxb_vvv
*----------------------------------------------------------------------------*/

/* VMAXB */
char64 __vmaxb_vvv(char64, char64);




/*-----------------------------------------------------------------------------
* ID: __vmaxd_vvv
*----------------------------------------------------------------------------*/

/* VMAXD */
long8 __vmaxd_vvv(long8, long8);




/*-----------------------------------------------------------------------------
* ID: __vmaxdp_vvv
*----------------------------------------------------------------------------*/

/* VMAXDP */
double8 __vmaxdp_vvv(double8, double8);




/*-----------------------------------------------------------------------------
* ID: __vmaxh_vvv
*----------------------------------------------------------------------------*/

/* VMAXH */
short32 __vmaxh_vvv(short32, short32);




/*-----------------------------------------------------------------------------
* ID: __vmaxpb_vvp
*----------------------------------------------------------------------------*/

/* VMAXPB */
void __vmaxpb_vvp(char64, char64::EQUIV_ACCESS_T<0>&, __vpred&);




/*-----------------------------------------------------------------------------
* ID: __vmaxpd_vvp
*----------------------------------------------------------------------------*/

/* VMAXPD */
void __vmaxpd_vvp(long8, long8::EQUIV_ACCESS_T<0>&, __vpred&);




/*-----------------------------------------------------------------------------
* ID: __vmaxph_vvp
*----------------------------------------------------------------------------*/

/* VMAXPH */
void __vmaxph_vvp(short32, short32::EQUIV_ACCESS_T<0>&, __vpred&);




/*-----------------------------------------------------------------------------
* ID: __vmaxpw_vvp
*----------------------------------------------------------------------------*/

/* VMAXPW */
void __vmaxpw_vvp(int16, int16::EQUIV_ACCESS_T<0>&, __vpred&);




/*-----------------------------------------------------------------------------
* ID: __vmaxsp_vvv
*----------------------------------------------------------------------------*/

/* VMAXSP */
float16 __vmaxsp_vvv(float16, float16);




/*-----------------------------------------------------------------------------
* ID: __vmaxub_vvv
*----------------------------------------------------------------------------*/

/* VMAXUB */
uchar64 __vmaxub_vvv(uchar64, uchar64);




/*-----------------------------------------------------------------------------
* ID: __vmaxud_vvv
*----------------------------------------------------------------------------*/

/* VMAXUD */
ulong8 __vmaxud_vvv(ulong8, ulong8);




/*-----------------------------------------------------------------------------
* ID: __vmaxuh_vvv
*----------------------------------------------------------------------------*/

/* VMAXUH */
ushort32 __vmaxuh_vvv(ushort32, ushort32);




/*-----------------------------------------------------------------------------
* ID: __vmaxupb_vvp
*----------------------------------------------------------------------------*/

/* VMAXUPB */
void __vmaxupb_vvp(uchar64, uchar64::EQUIV_ACCESS_T<0>&, __vpred&);




/*-----------------------------------------------------------------------------
* ID: __vmaxupd_vvp
*----------------------------------------------------------------------------*/

/* VMAXUPD */
void __vmaxupd_vvp(ulong8, ulong8::EQUIV_ACCESS_T<0>&, __vpred&);




/*-----------------------------------------------------------------------------
* ID: __vmaxuph_vvp
*----------------------------------------------------------------------------*/

/* VMAXUPH */
void __vmaxuph_vvp(ushort32, ushort32::EQUIV_ACCESS_T<0>&, __vpred&);




/*-----------------------------------------------------------------------------
* ID: __vmaxupw_vvp
*----------------------------------------------------------------------------*/

/* VMAXUPW */
void __vmaxupw_vvp(uint16, uint16::EQUIV_ACCESS_T<0>&, __vpred&);




/*-----------------------------------------------------------------------------
* ID: __vmaxuw_vvv
*----------------------------------------------------------------------------*/

/* VMAXUW */
uint16 __vmaxuw_vvv(uint16, uint16);




/*-----------------------------------------------------------------------------
* ID: __vmaxw_vvv
*----------------------------------------------------------------------------*/

/* VMAXW */
int16 __vmaxw_vvv(int16, int16);




/*-----------------------------------------------------------------------------
* ID: __vminb_vvv
*----------------------------------------------------------------------------*/

/* VMINB */
char64 __vminb_vvv(char64, char64);




/*-----------------------------------------------------------------------------
* ID: __vmind_vvv
*----------------------------------------------------------------------------*/

/* VMIND */
long8 __vmind_vvv(long8, long8);




/*-----------------------------------------------------------------------------
* ID: __vmindp_vvv
*----------------------------------------------------------------------------*/

/* VMINDP */
double8 __vmindp_vvv(double8, double8);




/*-----------------------------------------------------------------------------
* ID: __vminh_vvv
*----------------------------------------------------------------------------*/

/* VMINH */
short32 __vminh_vvv(short32, short32);




/*-----------------------------------------------------------------------------
* ID: __vminpb_vvp
*----------------------------------------------------------------------------*/

/* VMINPB */
void __vminpb_vvp(char64, char64::EQUIV_ACCESS_T<0>&, __vpred&);




/*-----------------------------------------------------------------------------
* ID: __vminpd_vvp
*----------------------------------------------------------------------------*/

/* VMINPD */
void __vminpd_vvp(long8, long8::EQUIV_ACCESS_T<0>&, __vpred&);




/*-----------------------------------------------------------------------------
* ID: __vminph_vvp
*----------------------------------------------------------------------------*/

/* VMINPH */
void __vminph_vvp(short32, short32::EQUIV_ACCESS_T<0>&, __vpred&);




/*-----------------------------------------------------------------------------
* ID: __vminpw_vvp
*----------------------------------------------------------------------------*/

/* VMINPW */
void __vminpw_vvp(int16, int16::EQUIV_ACCESS_T<0>&, __vpred&);




/*-----------------------------------------------------------------------------
* ID: __vminsp_vvv
*----------------------------------------------------------------------------*/

/* VMINSP */
float16 __vminsp_vvv(float16, float16);




/*-----------------------------------------------------------------------------
* ID: __vminub_vvv
*----------------------------------------------------------------------------*/

/* VMINUB */
uchar64 __vminub_vvv(uchar64, uchar64);




/*-----------------------------------------------------------------------------
* ID: __vminud_vvv
*----------------------------------------------------------------------------*/

/* VMINUD */
ulong8 __vminud_vvv(ulong8, ulong8);




/*-----------------------------------------------------------------------------
* ID: __vminuh_vvv
*----------------------------------------------------------------------------*/

/* VMINUH */
ushort32 __vminuh_vvv(ushort32, ushort32);




/*-----------------------------------------------------------------------------
* ID: __vminupb_vvp
*----------------------------------------------------------------------------*/

/* VMINUPB */
void __vminupb_vvp(uchar64, uchar64::EQUIV_ACCESS_T<0>&, __vpred&);




/*-----------------------------------------------------------------------------
* ID: __vminupd_vvp
*----------------------------------------------------------------------------*/

/* VMINUPD */
void __vminupd_vvp(ulong8, ulong8::EQUIV_ACCESS_T<0>&, __vpred&);




/*-----------------------------------------------------------------------------
* ID: __vminuph_vvp
*----------------------------------------------------------------------------*/

/* VMINUPH */
void __vminuph_vvp(ushort32, ushort32::EQUIV_ACCESS_T<0>&, __vpred&);




/*-----------------------------------------------------------------------------
* ID: __vminupw_vvp
*----------------------------------------------------------------------------*/

/* VMINUPW */
void __vminupw_vvp(uint16, uint16::EQUIV_ACCESS_T<0>&, __vpred&);




/*-----------------------------------------------------------------------------
* ID: __vminuw_vvv
*----------------------------------------------------------------------------*/

/* VMINUW */
uint16 __vminuw_vvv(uint16, uint16);




/*-----------------------------------------------------------------------------
* ID: __vminw_vvv
*----------------------------------------------------------------------------*/

/* VMINW */
int16 __vminw_vvv(int16, int16);




/*-----------------------------------------------------------------------------
* ID: __vmpybb_vvv
*----------------------------------------------------------------------------*/

/* VMPYBB */
char64 __vmpybb_vvv(char64, char64);
uchar64 __vmpybb_vvv(uchar64, uchar64);




/*-----------------------------------------------------------------------------
* ID: __vmpydd_vvv
*----------------------------------------------------------------------------*/

/* VMPYDD */
long8 __vmpydd_vvv(long8, long8);
ulong8 __vmpydd_vvv(ulong8, ulong8);




/*-----------------------------------------------------------------------------
* ID: __vmpydp_vvv
*----------------------------------------------------------------------------*/

/* VMPYDP */
double8 __vmpydp_vvv(double8, double8);




/*-----------------------------------------------------------------------------
* ID: __vmpyhh_vvv
*----------------------------------------------------------------------------*/

/* VMPYHH */
short32 __vmpyhh_vvv(short32, short32);
ushort32 __vmpyhh_vvv(ushort32, ushort32);




/*-----------------------------------------------------------------------------
* ID: __vmpyhw_vvw
*----------------------------------------------------------------------------*/

/* VMPYHW */
void __vmpyhw_vvw(short32, short32, int16::EQUIV_ACCESS_T<0>&, int16::EQUIV_ACCESS_T<0>&);




/*-----------------------------------------------------------------------------
* ID: __vmpysp2dp_vvw
*----------------------------------------------------------------------------*/

/* VMPYSP2DP */
void __vmpysp2dp_vvw(float16, float16, double8::EQUIV_ACCESS_T<0>&, double8::EQUIV_ACCESS_T<0>&);




/*-----------------------------------------------------------------------------
* ID: __vmpysp_vvv
*----------------------------------------------------------------------------*/

/* VMPYSP */
float16 __vmpysp_vvv(float16, float16);




/*-----------------------------------------------------------------------------
* ID: __vmpysubh_vvw
*----------------------------------------------------------------------------*/

/* VMPYSUBH */
void __vmpysubh_vvw(char64, uchar64, short32::EQUIV_ACCESS_T<0>&, short32::EQUIV_ACCESS_T<0>&);




/*-----------------------------------------------------------------------------
* ID: __vmpysuhw_vvw
*----------------------------------------------------------------------------*/

/* VMPYSUHW */
void __vmpysuhw_vvw(short32, ushort32, int16::EQUIV_ACCESS_T<0>&, int16::EQUIV_ACCESS_T<0>&);




/*-----------------------------------------------------------------------------
* ID: __vmpysuwd_vvw
*----------------------------------------------------------------------------*/

/* VMPYSUWD */
void __vmpysuwd_vvw(int16, uint16, long8::EQUIV_ACCESS_T<0>&, long8::EQUIV_ACCESS_T<0>&);




/*-----------------------------------------------------------------------------
* ID: __vmpyubh_vvw
*----------------------------------------------------------------------------*/

/* VMPYUBH */
void __vmpyubh_vvw(uchar64, uchar64, ushort32::EQUIV_ACCESS_T<0>&, ushort32::EQUIV_ACCESS_T<0>&);




/*-----------------------------------------------------------------------------
* ID: __vmpyudq_vvw
*----------------------------------------------------------------------------*/

/* VMPYUDQ */
void __vmpyudq_vvw(ulong8, ulong8, ulong8::EQUIV_ACCESS_T<0>&, ulong8::EQUIV_ACCESS_T<0>&);




/*-----------------------------------------------------------------------------
* ID: __vmpyuhw_vvw
*----------------------------------------------------------------------------*/

/* VMPYUHW */
void __vmpyuhw_vvw(ushort32, ushort32, uint16::EQUIV_ACCESS_T<0>&, uint16::EQUIV_ACCESS_T<0>&);




/*-----------------------------------------------------------------------------
* ID: __vmpyuwd_vvw
*----------------------------------------------------------------------------*/

/* VMPYUWD */
void __vmpyuwd_vvw(uint16, uint16, ulong8::EQUIV_ACCESS_T<0>&, ulong8::EQUIV_ACCESS_T<0>&);




/*-----------------------------------------------------------------------------
* ID: __vmpywd_vvw
*----------------------------------------------------------------------------*/

/* VMPYWD */
void __vmpywd_vvw(int16, int16, long8::EQUIV_ACCESS_T<0>&, long8::EQUIV_ACCESS_T<0>&);




/*-----------------------------------------------------------------------------
* ID: __vmpyww_vvv
*----------------------------------------------------------------------------*/

/* VMPYWW */
int16 __vmpyww_vvv(int16, int16);
uint16 __vmpyww_vvv(uint16, uint16);




/*-----------------------------------------------------------------------------
* ID: __vnandw_vvv
*----------------------------------------------------------------------------*/

/* VNANDW */
char64 __vnandw_vvv(char64, char64);
short32 __vnandw_vvv(short32, short32);
int16 __vnandw_vvv(int16, int16);
long8 __vnandw_vvv(long8, long8);
uchar64 __vnandw_vvv(uchar64, uchar64);
ushort32 __vnandw_vvv(ushort32, ushort32);
uint16 __vnandw_vvv(uint16, uint16);
ulong8 __vnandw_vvv(ulong8, ulong8);




/*-----------------------------------------------------------------------------
* ID: __vnorm2uh_vv
*----------------------------------------------------------------------------*/

/* VNORM2UH */
ushort32 __vnorm2uh_vv(ushort32);




/*-----------------------------------------------------------------------------
* ID: __vnorm2uw_vv
*----------------------------------------------------------------------------*/

/* VNORM2UW */
uint16 __vnorm2uw_vv(uint16);




/*-----------------------------------------------------------------------------
* ID: __vnormb_vv
*----------------------------------------------------------------------------*/

/* VNORMB */
char64 __vnormb_vv(char64);




/*-----------------------------------------------------------------------------
* ID: __vnormd_vv
*----------------------------------------------------------------------------*/

/* VNORMD */
long8 __vnormd_vv(long8);




/*-----------------------------------------------------------------------------
* ID: __vnormh_vv
*----------------------------------------------------------------------------*/

/* VNORMH */
short32 __vnormh_vv(short32);




/*-----------------------------------------------------------------------------
* ID: __vnormw_vv
*----------------------------------------------------------------------------*/

/* VNORMW */
int16 __vnormw_vv(int16);




/*-----------------------------------------------------------------------------
* ID: __vnorw_vvv
*----------------------------------------------------------------------------*/

/* VNORW */
char64 __vnorw_vvv(char64, char64);
short32 __vnorw_vvv(short32, short32);
int16 __vnorw_vvv(int16, int16);
long8 __vnorw_vvv(long8, long8);




/*-----------------------------------------------------------------------------
* ID: __vopmatmpysp_vvw
*----------------------------------------------------------------------------*/

/* VOPMATMPYSP */
void __vopmatmpysp_vvw(float16, float16, float16::EQUIV_ACCESS_T<0>&, float16::EQUIV_ACCESS_T<0>&);




/*-----------------------------------------------------------------------------
* ID: __vornw_vkv
*----------------------------------------------------------------------------*/

/* VORNW */
int16 __vornw_vkv(int16, int16);




/*-----------------------------------------------------------------------------
* ID: __vornw_vvv
*----------------------------------------------------------------------------*/

/* VORNW */
char64 __vornw_vvv(char64, char64);
short32 __vornw_vvv(short32, short32);
int16 __vornw_vvv(int16, int16);
long8 __vornw_vvv(long8, long8);




/*-----------------------------------------------------------------------------
* ID: __vorw_vkv
*----------------------------------------------------------------------------*/

/* VORW */
int16 __vorw_vkv(int16, int16);
uint16 __vorw_vkv(uint16, uint16);




/*-----------------------------------------------------------------------------
* ID: __vorw_vvv
*----------------------------------------------------------------------------*/

/* VORW */
char64 __vorw_vvv(char64, char64);
short32 __vorw_vvv(short32, short32);
int16 __vorw_vvv(int16, int16);
long8 __vorw_vvv(long8, long8);
uchar64 __vorw_vvv(uchar64, uchar64);
ushort32 __vorw_vvv(ushort32, ushort32);
uint16 __vorw_vvv(uint16, uint16);
ulong8 __vorw_vvv(ulong8, ulong8);




/*-----------------------------------------------------------------------------
* ID: __vpackh2_vvv
*----------------------------------------------------------------------------*/

/* VPACKH2 */
short32 __vpackh2_vvv(short32, short32);
ushort32 __vpackh2_vvv(ushort32, ushort32);
cchar32 __vpackh2_vvv(cchar32, cchar32);




/*-----------------------------------------------------------------------------
* ID: __vpackh4_vvv
*----------------------------------------------------------------------------*/

/* VPACKH4 */
char64 __vpackh4_vvv(char64, char64);
uchar64 __vpackh4_vvv(uchar64, uchar64);




/*-----------------------------------------------------------------------------
* ID: __vpackhl2_vvv
*----------------------------------------------------------------------------*/

/* VPACKHL2 */
short32 __vpackhl2_vvv(short32, short32);
ushort32 __vpackhl2_vvv(ushort32, ushort32);
cchar32 __vpackhl2_vvv(cchar32, cchar32);




/*-----------------------------------------------------------------------------
* ID: __vpackl2_vvv
*----------------------------------------------------------------------------*/

/* VPACKL2 */
short32 __vpackl2_vvv(short32, short32);
ushort32 __vpackl2_vvv(ushort32, ushort32);
cchar32 __vpackl2_vvv(cchar32, cchar32);




/*-----------------------------------------------------------------------------
* ID: __vpackl4_vvv
*----------------------------------------------------------------------------*/

/* VPACKL4 */
char64 __vpackl4_vvv(char64, char64);
uchar64 __vpackl4_vvv(uchar64, uchar64);




/*-----------------------------------------------------------------------------
* ID: __vpacklh2_vvv
*----------------------------------------------------------------------------*/

/* VPACKLH2 */
short32 __vpacklh2_vvv(short32, short32);
ushort32 __vpacklh2_vvv(ushort32, ushort32);
cchar32 __vpacklh2_vvv(cchar32, cchar32);




/*-----------------------------------------------------------------------------
* ID: __vpacklh4_vvv
*----------------------------------------------------------------------------*/

/* VPACKLH4 */
char64 __vpacklh4_vvv(char64, char64);
uchar64 __vpacklh4_vvv(uchar64, uchar64);




/*-----------------------------------------------------------------------------
* ID: __vpackp2_vvv
*----------------------------------------------------------------------------*/

/* VPACKP2 */
short32 __vpackp2_vvv(short32, short32);
ushort32 __vpackp2_vvv(ushort32, ushort32);
cchar32 __vpackp2_vvv(cchar32, cchar32);




/*-----------------------------------------------------------------------------
* ID: __vpackw_vvv
*----------------------------------------------------------------------------*/

/* VPACKW */
int16 __vpackw_vvv(int16, int16);
float16 __vpackw_vvv(float16, float16);
uint16 __vpackw_vvv(uint16, uint16);
cshort16 __vpackw_vvv(cshort16, cshort16);




/*-----------------------------------------------------------------------------
* ID: __vpackx2_vvv
*----------------------------------------------------------------------------*/

/* VPACKX2 */
short32 __vpackx2_vvv(short32, short32);
ushort32 __vpackx2_vvv(ushort32, ushort32);
cchar32 __vpackx2_vvv(cchar32, cchar32);




/*-----------------------------------------------------------------------------
* ID: __vperm_vvv
*----------------------------------------------------------------------------*/

/* VPERM */
uchar64 __vperm_vvv(uchar64, uchar64);




/*-----------------------------------------------------------------------------
* ID: __vperm_yvv
*----------------------------------------------------------------------------*/

/* VPERM */
uchar64 __vperm_yvv(uchar64, uchar64);




/*-----------------------------------------------------------------------------
* ID: __vpermeeb_yvvv
*----------------------------------------------------------------------------*/

/* VPERMEEB */
uchar64 __vpermeeb_yvvv(uchar64, uchar64, uchar64);




/*-----------------------------------------------------------------------------
* ID: __vpermeed_yvvv
*----------------------------------------------------------------------------*/

/* VPERMEED */
uchar64 __vpermeed_yvvv(uchar64, uchar64, uchar64);




/*-----------------------------------------------------------------------------
* ID: __vpermeeh_yvvv
*----------------------------------------------------------------------------*/

/* VPERMEEH */
uchar64 __vpermeeh_yvvv(uchar64, uchar64, uchar64);




/*-----------------------------------------------------------------------------
* ID: __vpermeeq_yvvv
*----------------------------------------------------------------------------*/

/* VPERMEEQ */
uchar64 __vpermeeq_yvvv(uchar64, uchar64, uchar64);




/*-----------------------------------------------------------------------------
* ID: __vpermeew_yvvv
*----------------------------------------------------------------------------*/

/* VPERMEEW */
uchar64 __vpermeew_yvvv(uchar64, uchar64, uchar64);




/*-----------------------------------------------------------------------------
* ID: __vpermeob_yvvv
*----------------------------------------------------------------------------*/

/* VPERMEOB */
uchar64 __vpermeob_yvvv(uchar64, uchar64, uchar64);




/*-----------------------------------------------------------------------------
* ID: __vpermeod_yvvv
*----------------------------------------------------------------------------*/

/* VPERMEOD */
uchar64 __vpermeod_yvvv(uchar64, uchar64, uchar64);




/*-----------------------------------------------------------------------------
* ID: __vpermeoh_yvvv
*----------------------------------------------------------------------------*/

/* VPERMEOH */
uchar64 __vpermeoh_yvvv(uchar64, uchar64, uchar64);




/*-----------------------------------------------------------------------------
* ID: __vpermeoq_yvvv
*----------------------------------------------------------------------------*/

/* VPERMEOQ */
uchar64 __vpermeoq_yvvv(uchar64, uchar64, uchar64);




/*-----------------------------------------------------------------------------
* ID: __vpermeow_yvvv
*----------------------------------------------------------------------------*/

/* VPERMEOW */
uchar64 __vpermeow_yvvv(uchar64, uchar64, uchar64);




/*-----------------------------------------------------------------------------
* ID: __vpermhh_yvvv
*----------------------------------------------------------------------------*/

/* VPERMHH */
uchar64 __vpermhh_yvvv(uchar64, uchar64, uchar64);




/*-----------------------------------------------------------------------------
* ID: __vpermlh_yvvv
*----------------------------------------------------------------------------*/

/* VPERMLH */
uchar64 __vpermlh_yvvv(uchar64, uchar64, uchar64);




/*-----------------------------------------------------------------------------
* ID: __vpermll_yvvv
*----------------------------------------------------------------------------*/

/* VPERMLL */
uchar64 __vpermll_yvvv(uchar64, uchar64, uchar64);




/*-----------------------------------------------------------------------------
* ID: __vpermoob_yvvv
*----------------------------------------------------------------------------*/

/* VPERMOOB */
uchar64 __vpermoob_yvvv(uchar64, uchar64, uchar64);




/*-----------------------------------------------------------------------------
* ID: __vpermood_yvvv
*----------------------------------------------------------------------------*/

/* VPERMOOD */
uchar64 __vpermood_yvvv(uchar64, uchar64, uchar64);




/*-----------------------------------------------------------------------------
* ID: __vpermooh_yvvv
*----------------------------------------------------------------------------*/

/* VPERMOOH */
uchar64 __vpermooh_yvvv(uchar64, uchar64, uchar64);




/*-----------------------------------------------------------------------------
* ID: __vpermooq_yvvv
*----------------------------------------------------------------------------*/

/* VPERMOOQ */
uchar64 __vpermooq_yvvv(uchar64, uchar64, uchar64);




/*-----------------------------------------------------------------------------
* ID: __vpermoow_yvvv
*----------------------------------------------------------------------------*/

/* VPERMOOW */
uchar64 __vpermoow_yvvv(uchar64, uchar64, uchar64);




/*-----------------------------------------------------------------------------
* ID: __vppackdup16w_pv
*----------------------------------------------------------------------------*/

/* VPPACKDUP16W */
uint16 __vppackdup16w_pv(__vpred);




/*-----------------------------------------------------------------------------
* ID: __vppackdup8w_pv
*----------------------------------------------------------------------------*/

/* VPPACKDUP8W */
uint16 __vppackdup8w_pv(__vpred);




/*-----------------------------------------------------------------------------
* ID: __vputb_rkv
*----------------------------------------------------------------------------*/

/* VPUTB */
uchar64 __vputb_rkv(uchar, uchar, uchar64);
char64 __vputb_rkv(int8_t, int8_t, char64);




/*-----------------------------------------------------------------------------
* ID: __vputd_dkv
*----------------------------------------------------------------------------*/

/* VPUTD */
ulong8 __vputd_dkv(ulong, uchar, ulong8);
long8 __vputd_dkv(int64_t, int8_t, long8);




/*-----------------------------------------------------------------------------
* ID: __vputh_rkv
*----------------------------------------------------------------------------*/

/* VPUTH */
ushort32 __vputh_rkv(ushort, uchar, ushort32);
short32 __vputh_rkv(int16_t, int8_t, short32);




/*-----------------------------------------------------------------------------
* ID: __vputw_rkv
*----------------------------------------------------------------------------*/

/* VPUTW */
uint16 __vputw_rkv(uint, uchar, uint16);
int16 __vputw_rkv(int32_t, int8_t, int16);




/*-----------------------------------------------------------------------------
* ID: __vpxpnd_pv
*----------------------------------------------------------------------------*/

/* VPXPND */
char64 __vpxpnd_pv(__vpred);




/*-----------------------------------------------------------------------------
* ID: __vrcpdp_vv
*----------------------------------------------------------------------------*/

/* VRCPDP */
double8 __vrcpdp_vv(double8);




/*-----------------------------------------------------------------------------
* ID: __vrcpsp_vv
*----------------------------------------------------------------------------*/

/* VRCPSP */
float16 __vrcpsp_vv(float16);




/*-----------------------------------------------------------------------------
* ID: __vreplace_vkkv
*----------------------------------------------------------------------------*/

/* VREPLACE */
ulong8 __vreplace_vkkv(ulong8, uchar8, uchar8, ulong8);
long8 __vreplace_vkkv(long8, char8, char8, long8);




/*-----------------------------------------------------------------------------
* ID: __vreverseb_vv
*----------------------------------------------------------------------------*/

/* VREVERSEB */
char64 __vreverseb_vv(char64);
uchar64 __vreverseb_vv(uchar64);




/*-----------------------------------------------------------------------------
* ID: __vreversed_vv
*----------------------------------------------------------------------------*/

/* VREVERSED */
long8 __vreversed_vv(long8);
double8 __vreversed_vv(double8);
cfloat8 __vreversed_vv(cfloat8);
ulong8 __vreversed_vv(ulong8);
cint8 __vreversed_vv(cint8);




/*-----------------------------------------------------------------------------
* ID: __vreverseh_vv
*----------------------------------------------------------------------------*/

/* VREVERSEH */
short32 __vreverseh_vv(short32);
ushort32 __vreverseh_vv(ushort32);
cchar32 __vreverseh_vv(cchar32);




/*-----------------------------------------------------------------------------
* ID: __vreversew_vv
*----------------------------------------------------------------------------*/

/* VREVERSEW */
int16 __vreversew_vv(int16);
float16 __vreversew_vv(float16);
uint16 __vreversew_vv(uint16);
cshort16 __vreversew_vv(cshort16);




/*-----------------------------------------------------------------------------
* ID: __vrotlb_vkv
*----------------------------------------------------------------------------*/

/* VROTLB */
uchar64 __vrotlb_vkv(uchar64, uchar64);




/*-----------------------------------------------------------------------------
* ID: __vrotlb_vvv
*----------------------------------------------------------------------------*/

/* VROTLB */
uchar64 __vrotlb_vvv(uchar64, uint16);




/*-----------------------------------------------------------------------------
* ID: __vrotld_vkv
*----------------------------------------------------------------------------*/

/* VROTLD */
ulong8 __vrotld_vkv(ulong8, uchar8);




/*-----------------------------------------------------------------------------
* ID: __vrotld_vvv
*----------------------------------------------------------------------------*/

/* VROTLD */
ulong8 __vrotld_vvv(ulong8, ulong8);




/*-----------------------------------------------------------------------------
* ID: __vrotlh_vkv
*----------------------------------------------------------------------------*/

/* VROTLH */
ushort32 __vrotlh_vkv(ushort32, ushort32);




/*-----------------------------------------------------------------------------
* ID: __vrotlh_vvv
*----------------------------------------------------------------------------*/

/* VROTLH */
ushort32 __vrotlh_vvv(ushort32, uint16);




/*-----------------------------------------------------------------------------
* ID: __vrotlw_vkv
*----------------------------------------------------------------------------*/

/* VROTLW */
int16 __vrotlw_vkv(int16, uchar16);




/*-----------------------------------------------------------------------------
* ID: __vrotlw_vvv
*----------------------------------------------------------------------------*/

/* VROTLW */
int16 __vrotlw_vvv(int16, uint16);




/*-----------------------------------------------------------------------------
* ID: __vrpackh_vvv
*----------------------------------------------------------------------------*/

/* VRPACKH */
short32 __vrpackh_vvv(int16, int16);




/*-----------------------------------------------------------------------------
* ID: __vrsqrdp_vv
*----------------------------------------------------------------------------*/

/* VRSQRDP */
double8 __vrsqrdp_vv(double8);




/*-----------------------------------------------------------------------------
* ID: __vrsqrsp_vv
*----------------------------------------------------------------------------*/

/* VRSQRSP */
float16 __vrsqrsp_vv(float16);




/*-----------------------------------------------------------------------------
* ID: __vsad16ou16h16w_vvv
*----------------------------------------------------------------------------*/

/* VSAD16OU16H16W */
uint16 __vsad16ou16h16w_vvv(ushort32, ushort16);




/*-----------------------------------------------------------------------------
* ID: __vsad16ou8h16w_vvv
*----------------------------------------------------------------------------*/

/* VSAD16OU8H16W */
uint16 __vsad16ou8h16w_vvv(ushort32, ushort8);




/*-----------------------------------------------------------------------------
* ID: __vsad8ou16b32h_vvv
*----------------------------------------------------------------------------*/

/* VSAD8OU16B32H */
ushort32 __vsad8ou16b32h_vvv(uchar64, uchar16);




/*-----------------------------------------------------------------------------
* ID: __vsaddb_vkv
*----------------------------------------------------------------------------*/

/* VSADDB */
char64 __vsaddb_vkv(char64, char64);




/*-----------------------------------------------------------------------------
* ID: __vsaddb_vvv
*----------------------------------------------------------------------------*/

/* VSADDB */
char64 __vsaddb_vvv(char64, char64);




/*-----------------------------------------------------------------------------
* ID: __vsaddh_vkv
*----------------------------------------------------------------------------*/

/* VSADDH */
short32 __vsaddh_vkv(short32, short32);




/*-----------------------------------------------------------------------------
* ID: __vsaddh_vvv
*----------------------------------------------------------------------------*/

/* VSADDH */
short32 __vsaddh_vvv(short32, short32);




/*-----------------------------------------------------------------------------
* ID: __vsaddub_vvv
*----------------------------------------------------------------------------*/

/* VSADDUB */
uchar64 __vsaddub_vvv(uchar64, uchar64);




/*-----------------------------------------------------------------------------
* ID: __vsadduh_vvv
*----------------------------------------------------------------------------*/

/* VSADDUH */
ushort32 __vsadduh_vvv(ushort32, ushort32);




/*-----------------------------------------------------------------------------
* ID: __vsaddusb_vvv
*----------------------------------------------------------------------------*/

/* VSADDUSB */
uchar64 __vsaddusb_vvv(uchar64, char64);




/*-----------------------------------------------------------------------------
* ID: __vsaddush_vvv
*----------------------------------------------------------------------------*/

/* VSADDUSH */
ushort32 __vsaddush_vvv(ushort32, short32);




/*-----------------------------------------------------------------------------
* ID: __vsaddusw_vvv
*----------------------------------------------------------------------------*/

/* VSADDUSW */
uint16 __vsaddusw_vvv(uint16, int16);




/*-----------------------------------------------------------------------------
* ID: __vsadduw_vvv
*----------------------------------------------------------------------------*/

/* VSADDUW */
uint16 __vsadduw_vvv(uint16, uint16);




/*-----------------------------------------------------------------------------
* ID: __vsaddw_vkv
*----------------------------------------------------------------------------*/

/* VSADDW */
int16 __vsaddw_vkv(int16, int16);




/*-----------------------------------------------------------------------------
* ID: __vsaddw_vvv
*----------------------------------------------------------------------------*/

/* VSADDW */
int16 __vsaddw_vvv(int16, int16);




/*-----------------------------------------------------------------------------
* ID: __vsadm16ou16h16w_yvvv
*----------------------------------------------------------------------------*/

/* VSADM16OU16H16W */
uint16 __vsadm16ou16h16w_yvvv(uchar64, ushort32, ushort16);




/*-----------------------------------------------------------------------------
* ID: __vsadm16ou8h16w_yvvv
*----------------------------------------------------------------------------*/

/* VSADM16OU8H16W */
uint16 __vsadm16ou8h16w_yvvv(uchar64, ushort32, ushort8);




/*-----------------------------------------------------------------------------
* ID: __vsadm8ou16b32h_yvvv
*----------------------------------------------------------------------------*/

/* VSADM8OU16B32H */
ushort32 __vsadm8ou16b32h_yvvv(uchar64, uchar64, uchar16);




/*-----------------------------------------------------------------------------
* ID: __vsatdl_vv
*----------------------------------------------------------------------------*/

/* VSATDL */
long8 __vsatdl_vv(long8);




/*-----------------------------------------------------------------------------
* ID: __vsatdw_vv
*----------------------------------------------------------------------------*/

/* VSATDW */
long8 __vsatdw_vv(long8);




/*-----------------------------------------------------------------------------
* ID: __vsathb_vv
*----------------------------------------------------------------------------*/

/* VSATHB */
short32 __vsathb_vv(short32);




/*-----------------------------------------------------------------------------
* ID: __vsatlw_vv
*----------------------------------------------------------------------------*/

/* VSATLW */
long8 __vsatlw_vv(long8);




/*-----------------------------------------------------------------------------
* ID: __vsatwh_vv
*----------------------------------------------------------------------------*/

/* VSATWH */
int16 __vsatwh_vv(int16);




/*-----------------------------------------------------------------------------
* ID: __vscaledp_vvv
*----------------------------------------------------------------------------*/

/* VSCALEDP */
double8 __vscaledp_vvv(double8, ulong8);




/*-----------------------------------------------------------------------------
* ID: __vscalesp_vvv
*----------------------------------------------------------------------------*/

/* VSCALESP */
float16 __vscalesp_vvv(float16, uint16);




/*-----------------------------------------------------------------------------
* ID: __vscatterb_pvv
*----------------------------------------------------------------------------*/

/* VSCATTERB */
char64 __vscatterb_pvv(__vpred, char64);




/*-----------------------------------------------------------------------------
* ID: __vscatternb_pvv
*----------------------------------------------------------------------------*/

/* VSCATTERNB */
char64 __vscatternb_pvv(__vpred, char64);




/*-----------------------------------------------------------------------------
* ID: __vsel_pvkv
*----------------------------------------------------------------------------*/

/* VSEL */
int16 __vsel_pvkv(__vpred, int16, int16);
float16 __vsel_pvkv(__vpred, float16, float16);
uint16 __vsel_pvkv(__vpred, uint16, uint16);




/*-----------------------------------------------------------------------------
* ID: __vsel_pvvv
*----------------------------------------------------------------------------*/

/* VSEL */
char64 __vsel_pvvv(__vpred, char64, char64);
short32 __vsel_pvvv(__vpred, short32, short32);
int16 __vsel_pvvv(__vpred, int16, int16);
long8 __vsel_pvvv(__vpred, long8, long8);
uchar64 __vsel_pvvv(__vpred, uchar64, uchar64);
ushort32 __vsel_pvvv(__vpred, ushort32, ushort32);
uint16 __vsel_pvvv(__vpred, uint16, uint16);
ulong8 __vsel_pvvv(__vpred, ulong8, ulong8);




/*-----------------------------------------------------------------------------
* ID: __vshfl2b_vv
*----------------------------------------------------------------------------*/

/* VSHFL2B */
char64 __vshfl2b_vv(char64);
uchar64 __vshfl2b_vv(uchar64);




/*-----------------------------------------------------------------------------
* ID: __vshfl2d_vv
*----------------------------------------------------------------------------*/

/* VSHFL2D */
long8 __vshfl2d_vv(long8);
double8 __vshfl2d_vv(double8);
cfloat8 __vshfl2d_vv(cfloat8);
ulong8 __vshfl2d_vv(ulong8);
cint8 __vshfl2d_vv(cint8);




/*-----------------------------------------------------------------------------
* ID: __vshfl2dee_vvv
*----------------------------------------------------------------------------*/

/* VSHFL2DEE */
long8 __vshfl2dee_vvv(long8, long8);
double8 __vshfl2dee_vvv(double8, double8);
cfloat8 __vshfl2dee_vvv(cfloat8, cfloat8);
ulong8 __vshfl2dee_vvv(ulong8, ulong8);
cint8 __vshfl2dee_vvv(cint8, cint8);




/*-----------------------------------------------------------------------------
* ID: __vshfl2deo_vvv
*----------------------------------------------------------------------------*/

/* VSHFL2DEO */
long8 __vshfl2deo_vvv(long8, long8);
double8 __vshfl2deo_vvv(double8, double8);
cfloat8 __vshfl2deo_vvv(cfloat8, cfloat8);
ulong8 __vshfl2deo_vvv(ulong8, ulong8);
cint8 __vshfl2deo_vvv(cint8, cint8);




/*-----------------------------------------------------------------------------
* ID: __vshfl2dhh_vvv
*----------------------------------------------------------------------------*/

/* VSHFL2DHH */
long8 __vshfl2dhh_vvv(long8, long8);
double8 __vshfl2dhh_vvv(double8, double8);
cfloat8 __vshfl2dhh_vvv(cfloat8, cfloat8);
ulong8 __vshfl2dhh_vvv(ulong8, ulong8);
cint8 __vshfl2dhh_vvv(cint8, cint8);




/*-----------------------------------------------------------------------------
* ID: __vshfl2dlh_vvv
*----------------------------------------------------------------------------*/

/* VSHFL2DLH */
long8 __vshfl2dlh_vvv(long8, long8);
double8 __vshfl2dlh_vvv(double8, double8);
cfloat8 __vshfl2dlh_vvv(cfloat8, cfloat8);
ulong8 __vshfl2dlh_vvv(ulong8, ulong8);
cint8 __vshfl2dlh_vvv(cint8, cint8);




/*-----------------------------------------------------------------------------
* ID: __vshfl2dll_vvv
*----------------------------------------------------------------------------*/

/* VSHFL2DLL */
long8 __vshfl2dll_vvv(long8, long8);
double8 __vshfl2dll_vvv(double8, double8);
cfloat8 __vshfl2dll_vvv(cfloat8, cfloat8);
ulong8 __vshfl2dll_vvv(ulong8, ulong8);
cint8 __vshfl2dll_vvv(cint8, cint8);




/*-----------------------------------------------------------------------------
* ID: __vshfl2doo_vvv
*----------------------------------------------------------------------------*/

/* VSHFL2DOO */
long8 __vshfl2doo_vvv(long8, long8);
double8 __vshfl2doo_vvv(double8, double8);
cfloat8 __vshfl2doo_vvv(cfloat8, cfloat8);
ulong8 __vshfl2doo_vvv(ulong8, ulong8);
cint8 __vshfl2doo_vvv(cint8, cint8);




/*-----------------------------------------------------------------------------
* ID: __vshfl2h_vv
*----------------------------------------------------------------------------*/

/* VSHFL2H */
short32 __vshfl2h_vv(short32);
ushort32 __vshfl2h_vv(ushort32);
cchar32 __vshfl2h_vv(cchar32);




/*-----------------------------------------------------------------------------
* ID: __vshfl2hhh_vvv
*----------------------------------------------------------------------------*/

/* VSHFL2HHH */
short32 __vshfl2hhh_vvv(short32, short32);
ushort32 __vshfl2hhh_vvv(ushort32, ushort32);
cchar32 __vshfl2hhh_vvv(cchar32, cchar32);




/*-----------------------------------------------------------------------------
* ID: __vshfl2hll_vvv
*----------------------------------------------------------------------------*/

/* VSHFL2HLL */
short32 __vshfl2hll_vvv(short32, short32);
ushort32 __vshfl2hll_vvv(ushort32, ushort32);
cchar32 __vshfl2hll_vvv(cchar32, cchar32);




/*-----------------------------------------------------------------------------
* ID: __vshfl2w_vv
*----------------------------------------------------------------------------*/

/* VSHFL2W */
int16 __vshfl2w_vv(int16);
float16 __vshfl2w_vv(float16);
uint16 __vshfl2w_vv(uint16);
cshort16 __vshfl2w_vv(cshort16);




/*-----------------------------------------------------------------------------
* ID: __vshfl2whh_vvv
*----------------------------------------------------------------------------*/

/* VSHFL2WHH */
int16 __vshfl2whh_vvv(int16, int16);
float16 __vshfl2whh_vvv(float16, float16);
uint16 __vshfl2whh_vvv(uint16, uint16);
cshort16 __vshfl2whh_vvv(cshort16, cshort16);




/*-----------------------------------------------------------------------------
* ID: __vshfl2wll_vvv
*----------------------------------------------------------------------------*/

/* VSHFL2WLL */
int16 __vshfl2wll_vvv(int16, int16);
float16 __vshfl2wll_vvv(float16, float16);
uint16 __vshfl2wll_vvv(uint16, uint16);
cshort16 __vshfl2wll_vvv(cshort16, cshort16);




/*-----------------------------------------------------------------------------
* ID: __vshfl3_vvv
*----------------------------------------------------------------------------*/

/* VSHFL3 */
ulong8 __vshfl3_vvv(uint16, uint16);
long8 __vshfl3_vvv(int16, int16);




/*-----------------------------------------------------------------------------
* ID: __vshfl4b_vv
*----------------------------------------------------------------------------*/

/* VSHFL4B */
char64 __vshfl4b_vv(char64);
uchar64 __vshfl4b_vv(uchar64);




/*-----------------------------------------------------------------------------
* ID: __vshfl4d_vv
*----------------------------------------------------------------------------*/

/* VSHFL4D */
long8 __vshfl4d_vv(long8);
double8 __vshfl4d_vv(double8);
cfloat8 __vshfl4d_vv(cfloat8);
ulong8 __vshfl4d_vv(ulong8);
cint8 __vshfl4d_vv(cint8);




/*-----------------------------------------------------------------------------
* ID: __vshfl4h_vv
*----------------------------------------------------------------------------*/

/* VSHFL4H */
short32 __vshfl4h_vv(short32);
ushort32 __vshfl4h_vv(ushort32);
cchar32 __vshfl4h_vv(cchar32);




/*-----------------------------------------------------------------------------
* ID: __vshfl4w_vv
*----------------------------------------------------------------------------*/

/* VSHFL4W */
int16 __vshfl4w_vv(int16);
float16 __vshfl4w_vv(float16);
uint16 __vshfl4w_vv(uint16);
cshort16 __vshfl4w_vv(cshort16);




/*-----------------------------------------------------------------------------
* ID: __vshl_vkv
*----------------------------------------------------------------------------*/

/* VSHL */
ulong8 __vshl_vkv(ulong8, uchar);




/*-----------------------------------------------------------------------------
* ID: __vshl_vvv
*----------------------------------------------------------------------------*/

/* VSHL */
ulong8 __vshl_vvv(ulong8, ulong8);




/*-----------------------------------------------------------------------------
* ID: __vshlb_vkv
*----------------------------------------------------------------------------*/

/* VSHLB */
char64 __vshlb_vkv(char64, char64);
uchar64 __vshlb_vkv(uchar64, uchar64);




/*-----------------------------------------------------------------------------
* ID: __vshlb_vvv
*----------------------------------------------------------------------------*/

/* VSHLB */
char64 __vshlb_vvv(char64, int16);
uchar64 __vshlb_vvv(uchar64, uint16);




/*-----------------------------------------------------------------------------
* ID: __vshlcb_pvv
*----------------------------------------------------------------------------*/

/* VSHLCB */
char64 __vshlcb_pvv(__vpred, char64, char64);
uchar64 __vshlcb_pvv(__vpred, uchar64, uchar64);




/*-----------------------------------------------------------------------------
* ID: __vshlch_pvv
*----------------------------------------------------------------------------*/

/* VSHLCH */
short32 __vshlch_pvv(__vpred, short32, short32);
ushort32 __vshlch_pvv(__vpred, ushort32, ushort32);




/*-----------------------------------------------------------------------------
* ID: __vshlcw_pvv
*----------------------------------------------------------------------------*/

/* VSHLCW */
int16 __vshlcw_pvv(__vpred, int16, int16);
uint16 __vshlcw_pvv(__vpred, uint16, uint16);




/*-----------------------------------------------------------------------------
* ID: __vshld_vkv
*----------------------------------------------------------------------------*/

/* VSHLD */
long8 __vshld_vkv(long8, long8);
ulong8 __vshld_vkv(ulong8, ulong8);




/*-----------------------------------------------------------------------------
* ID: __vshld_vvv
*----------------------------------------------------------------------------*/

/* VSHLD */
long8 __vshld_vvv(long8, long8);
ulong8 __vshld_vvv(ulong8, ulong8);




/*-----------------------------------------------------------------------------
* ID: __vshlh_vkv
*----------------------------------------------------------------------------*/

/* VSHLH */
short32 __vshlh_vkv(short32, short32);
ushort32 __vshlh_vkv(ushort32, ushort32);




/*-----------------------------------------------------------------------------
* ID: __vshlh_vvv
*----------------------------------------------------------------------------*/

/* VSHLH */
short32 __vshlh_vvv(short32, int16);
ushort32 __vshlh_vvv(ushort32, uint16);




/*-----------------------------------------------------------------------------
* ID: __vshlm16b_vvv
*----------------------------------------------------------------------------*/

/* VSHLM16B */
char64 __vshlm16b_vvv(char64, char16);




/*-----------------------------------------------------------------------------
* ID: __vshlm1b_vrv
*----------------------------------------------------------------------------*/

/* VSHLM1B */
char64 __vshlm1b_vrv(char64, int8_t);




/*-----------------------------------------------------------------------------
* ID: __vshlm2b_vrv
*----------------------------------------------------------------------------*/

/* VSHLM2B */
char64 __vshlm2b_vrv(char64, char2);




/*-----------------------------------------------------------------------------
* ID: __vshlm4b_vrv
*----------------------------------------------------------------------------*/

/* VSHLM4B */
char64 __vshlm4b_vrv(char64, char4);




/*-----------------------------------------------------------------------------
* ID: __vshlm8b_vdv
*----------------------------------------------------------------------------*/

/* VSHLM8B */
char64 __vshlm8b_vdv(char64, char8);




/*-----------------------------------------------------------------------------
* ID: __vshlmb_vvv
*----------------------------------------------------------------------------*/

/* VSHLMB */
int16 __vshlmb_vvv(int16, char64);




/*-----------------------------------------------------------------------------
* ID: __vshlw_vkv
*----------------------------------------------------------------------------*/

/* VSHLW */
int16 __vshlw_vkv(int16, int16);
uint16 __vshlw_vkv(uint16, uint16);




/*-----------------------------------------------------------------------------
* ID: __vshlw_vvv
*----------------------------------------------------------------------------*/

/* VSHLW */
int16 __vshlw_vvv(int16, int16);
uint16 __vshlw_vvv(uint16, uint16);




/*-----------------------------------------------------------------------------
* ID: __vshr_vkv
*----------------------------------------------------------------------------*/

/* VSHR */
long8 __vshr_vkv(long8, uchar);




/*-----------------------------------------------------------------------------
* ID: __vshr_vvv
*----------------------------------------------------------------------------*/

/* VSHR */
long8 __vshr_vvv(long8, ulong8);




/*-----------------------------------------------------------------------------
* ID: __vshrb_vkv
*----------------------------------------------------------------------------*/

/* VSHRB */
char64 __vshrb_vkv(char64, char64);




/*-----------------------------------------------------------------------------
* ID: __vshrb_vvv
*----------------------------------------------------------------------------*/

/* VSHRB */
char64 __vshrb_vvv(char64, int16);




/*-----------------------------------------------------------------------------
* ID: __vshrd_vkv
*----------------------------------------------------------------------------*/

/* VSHRD */
long8 __vshrd_vkv(long8, long8);




/*-----------------------------------------------------------------------------
* ID: __vshrd_vvv
*----------------------------------------------------------------------------*/

/* VSHRD */
long8 __vshrd_vvv(long8, long8);




/*-----------------------------------------------------------------------------
* ID: __vshrh_vkv
*----------------------------------------------------------------------------*/

/* VSHRH */
short32 __vshrh_vkv(short32, short32);




/*-----------------------------------------------------------------------------
* ID: __vshrh_vvv
*----------------------------------------------------------------------------*/

/* VSHRH */
short32 __vshrh_vvv(short32, int16);




/*-----------------------------------------------------------------------------
* ID: __vshrm16b_vvv
*----------------------------------------------------------------------------*/

/* VSHRM16B */
char64 __vshrm16b_vvv(char64, char16);




/*-----------------------------------------------------------------------------
* ID: __vshrm1b_vrv
*----------------------------------------------------------------------------*/

/* VSHRM1B */
char64 __vshrm1b_vrv(char64, int8_t);




/*-----------------------------------------------------------------------------
* ID: __vshrm2b_vrv
*----------------------------------------------------------------------------*/

/* VSHRM2B */
char64 __vshrm2b_vrv(char64, char2);




/*-----------------------------------------------------------------------------
* ID: __vshrm4b_vrv
*----------------------------------------------------------------------------*/

/* VSHRM4B */
char64 __vshrm4b_vrv(char64, char4);




/*-----------------------------------------------------------------------------
* ID: __vshrm8b_vdv
*----------------------------------------------------------------------------*/

/* VSHRM8B */
char64 __vshrm8b_vdv(char64, char8);




/*-----------------------------------------------------------------------------
* ID: __vshrmb_vvv
*----------------------------------------------------------------------------*/

/* VSHRMB */
int16 __vshrmb_vvv(int16, char64);




/*-----------------------------------------------------------------------------
* ID: __vshrrb_vkv
*----------------------------------------------------------------------------*/

/* VSHRRB */
char64 __vshrrb_vkv(char64, uchar64);




/*-----------------------------------------------------------------------------
* ID: __vshrrb_vvv
*----------------------------------------------------------------------------*/

/* VSHRRB */
char64 __vshrrb_vvv(char64, uint16);




/*-----------------------------------------------------------------------------
* ID: __vshrrd_vkv
*----------------------------------------------------------------------------*/

/* VSHRRD */
long8 __vshrrd_vkv(long8, uchar8);




/*-----------------------------------------------------------------------------
* ID: __vshrrd_vvv
*----------------------------------------------------------------------------*/

/* VSHRRD */
long8 __vshrrd_vvv(long8, ulong8);




/*-----------------------------------------------------------------------------
* ID: __vshrrh_vkv
*----------------------------------------------------------------------------*/

/* VSHRRH */
short32 __vshrrh_vkv(short32, uchar32);




/*-----------------------------------------------------------------------------
* ID: __vshrrh_vvv
*----------------------------------------------------------------------------*/

/* VSHRRH */
short32 __vshrrh_vvv(short32, uint16);




/*-----------------------------------------------------------------------------
* ID: __vshrrw_vkv
*----------------------------------------------------------------------------*/

/* VSHRRW */
int16 __vshrrw_vkv(int16, uchar16);




/*-----------------------------------------------------------------------------
* ID: __vshrrw_vvv
*----------------------------------------------------------------------------*/

/* VSHRRW */
int16 __vshrrw_vvv(int16, uint16);




/*-----------------------------------------------------------------------------
* ID: __vshru_vkv
*----------------------------------------------------------------------------*/

/* VSHRU */
ulong8 __vshru_vkv(ulong8, uchar);




/*-----------------------------------------------------------------------------
* ID: __vshru_vvv
*----------------------------------------------------------------------------*/

/* VSHRU */
ulong8 __vshru_vvv(ulong8, ulong8);




/*-----------------------------------------------------------------------------
* ID: __vshrub_vkv
*----------------------------------------------------------------------------*/

/* VSHRUB */
uchar64 __vshrub_vkv(uchar64, uchar64);




/*-----------------------------------------------------------------------------
* ID: __vshrub_vvv
*----------------------------------------------------------------------------*/

/* VSHRUB */
uchar64 __vshrub_vvv(uchar64, uint16);




/*-----------------------------------------------------------------------------
* ID: __vshrud_vkv
*----------------------------------------------------------------------------*/

/* VSHRUD */
ulong8 __vshrud_vkv(ulong8, ulong8);




/*-----------------------------------------------------------------------------
* ID: __vshrud_vvv
*----------------------------------------------------------------------------*/

/* VSHRUD */
ulong8 __vshrud_vvv(ulong8, ulong8);




/*-----------------------------------------------------------------------------
* ID: __vshruh_vkv
*----------------------------------------------------------------------------*/

/* VSHRUH */
ushort32 __vshruh_vkv(ushort32, ushort32);




/*-----------------------------------------------------------------------------
* ID: __vshruh_vvv
*----------------------------------------------------------------------------*/

/* VSHRUH */
ushort32 __vshruh_vvv(ushort32, uint16);




/*-----------------------------------------------------------------------------
* ID: __vshrurb_vkv
*----------------------------------------------------------------------------*/

/* VSHRURB */
uchar64 __vshrurb_vkv(uchar64, uchar64);




/*-----------------------------------------------------------------------------
* ID: __vshrurb_vvv
*----------------------------------------------------------------------------*/

/* VSHRURB */
uchar64 __vshrurb_vvv(uchar64, uint16);




/*-----------------------------------------------------------------------------
* ID: __vshrurd_vkv
*----------------------------------------------------------------------------*/

/* VSHRURD */
ulong8 __vshrurd_vkv(ulong8, uchar8);




/*-----------------------------------------------------------------------------
* ID: __vshrurd_vvv
*----------------------------------------------------------------------------*/

/* VSHRURD */
ulong8 __vshrurd_vvv(ulong8, ulong8);




/*-----------------------------------------------------------------------------
* ID: __vshrurh_vkv
*----------------------------------------------------------------------------*/

/* VSHRURH */
ushort32 __vshrurh_vkv(ushort32, uchar32);




/*-----------------------------------------------------------------------------
* ID: __vshrurh_vvv
*----------------------------------------------------------------------------*/

/* VSHRURH */
ushort32 __vshrurh_vvv(ushort32, uint16);




/*-----------------------------------------------------------------------------
* ID: __vshrurw_vkv
*----------------------------------------------------------------------------*/

/* VSHRURW */
uint16 __vshrurw_vkv(uint16, uchar16);




/*-----------------------------------------------------------------------------
* ID: __vshrurw_vvv
*----------------------------------------------------------------------------*/

/* VSHRURW */
uint16 __vshrurw_vvv(uint16, uint16);




/*-----------------------------------------------------------------------------
* ID: __vshruw_vkv
*----------------------------------------------------------------------------*/

/* VSHRUW */
uint16 __vshruw_vkv(uint16, uint16);




/*-----------------------------------------------------------------------------
* ID: __vshruw_vvv
*----------------------------------------------------------------------------*/

/* VSHRUW */
uint16 __vshruw_vvv(uint16, uint16);




/*-----------------------------------------------------------------------------
* ID: __vshrw_vkv
*----------------------------------------------------------------------------*/

/* VSHRW */
int16 __vshrw_vkv(int16, int16);




/*-----------------------------------------------------------------------------
* ID: __vshrw_vvv
*----------------------------------------------------------------------------*/

/* VSHRW */
int16 __vshrw_vvv(int16, int16);




/*-----------------------------------------------------------------------------
* ID: __vshvluw_vvv
*----------------------------------------------------------------------------*/

/* VSHVLUW */
uint16 __vshvluw_vvv(uint16, int16);




/*-----------------------------------------------------------------------------
* ID: __vshvlw_vvv
*----------------------------------------------------------------------------*/

/* VSHVLW */
int16 __vshvlw_vvv(int16, int16);




/*-----------------------------------------------------------------------------
* ID: __vshvruw_vvv
*----------------------------------------------------------------------------*/

/* VSHVRUW */
uint16 __vshvruw_vvv(uint16, int16);




/*-----------------------------------------------------------------------------
* ID: __vshvrw_vvv
*----------------------------------------------------------------------------*/

/* VSHVRW */
int16 __vshvrw_vvv(int16, int16);




/*-----------------------------------------------------------------------------
* ID: __vsmpyhw_vvw
*----------------------------------------------------------------------------*/

/* VSMPYHW */
void __vsmpyhw_vvw(short32, short32, int16::EQUIV_ACCESS_T<0>&, int16::EQUIV_ACCESS_T<0>&);




/*-----------------------------------------------------------------------------
* ID: __vsmpyr17ww_vvv
*----------------------------------------------------------------------------*/

/* VSMPYR17WW */
int16 __vsmpyr17ww_vvv(int16, int16);




/*-----------------------------------------------------------------------------
* ID: __vsmpyr1hh_vvv
*----------------------------------------------------------------------------*/

/* VSMPYR1HH */
short32 __vsmpyr1hh_vvv(short32, short32);




/*-----------------------------------------------------------------------------
* ID: __vsmpyr1ww_vvv
*----------------------------------------------------------------------------*/

/* VSMPYR1WW */
int16 __vsmpyr1ww_vvv(int16, int16);




/*-----------------------------------------------------------------------------
* ID: __vsmpyrsuhh_vvv
*----------------------------------------------------------------------------*/

/* VSMPYRSUHH */
short32 __vsmpyrsuhh_vvv(short32, ushort32);




/*-----------------------------------------------------------------------------
* ID: __vsmpyruhh_vvv
*----------------------------------------------------------------------------*/

/* VSMPYRUHH */
ushort32 __vsmpyruhh_vvv(ushort32, ushort32);




/*-----------------------------------------------------------------------------
* ID: __vsmpyww_vvv
*----------------------------------------------------------------------------*/

/* VSMPYWW */
int16 __vsmpyww_vvv(int16, int16);




/*-----------------------------------------------------------------------------
* ID: __vsortd16sp_vv
*----------------------------------------------------------------------------*/

/* VSORTD16SP */
float16 __vsortd16sp_vv(float16);




/*-----------------------------------------------------------------------------
* ID: __vsortd16w_vv
*----------------------------------------------------------------------------*/

/* VSORTD16W */
int16 __vsortd16w_vv(int16);




/*-----------------------------------------------------------------------------
* ID: __vsortdu16w_vv
*----------------------------------------------------------------------------*/

/* VSORTDU16W */
uint16 __vsortdu16w_vv(uint16);




/*-----------------------------------------------------------------------------
* ID: __vsorti16sp_vv
*----------------------------------------------------------------------------*/

/* VSORTI16SP */
float16 __vsorti16sp_vv(float16);




/*-----------------------------------------------------------------------------
* ID: __vsorti16w_vv
*----------------------------------------------------------------------------*/

/* VSORTI16W */
int16 __vsorti16w_vv(int16);




/*-----------------------------------------------------------------------------
* ID: __vsortiu16w_vv
*----------------------------------------------------------------------------*/

/* VSORTIU16W */
uint16 __vsortiu16w_vv(uint16);




/*-----------------------------------------------------------------------------
* ID: __vsortpd16sp_vv
*----------------------------------------------------------------------------*/

/* VSORTPD16SP */
uchar64 __vsortpd16sp_vv(float16);




/*-----------------------------------------------------------------------------
* ID: __vsortpd16w_vv
*----------------------------------------------------------------------------*/

/* VSORTPD16W */
uchar64 __vsortpd16w_vv(int16);




/*-----------------------------------------------------------------------------
* ID: __vsortpdu16w_vv
*----------------------------------------------------------------------------*/

/* VSORTPDU16W */
uchar64 __vsortpdu16w_vv(uint16);




/*-----------------------------------------------------------------------------
* ID: __vsortpi16sp_vv
*----------------------------------------------------------------------------*/

/* VSORTPI16SP */
uchar64 __vsortpi16sp_vv(float16);




/*-----------------------------------------------------------------------------
* ID: __vsortpi16w_vv
*----------------------------------------------------------------------------*/

/* VSORTPI16W */
uchar64 __vsortpi16w_vv(int16);




/*-----------------------------------------------------------------------------
* ID: __vsortpiu16w_vv
*----------------------------------------------------------------------------*/

/* VSORTPIU16W */
uchar64 __vsortpiu16w_vv(uint16);




/*-----------------------------------------------------------------------------
* ID: __vspackdw_vvv
*----------------------------------------------------------------------------*/

/* VSPACKDW */
int16 __vspackdw_vvv(long8, long8);




/*-----------------------------------------------------------------------------
* ID: __vspackhb_vvv
*----------------------------------------------------------------------------*/

/* VSPACKHB */
char64 __vspackhb_vvv(short32, short32);




/*-----------------------------------------------------------------------------
* ID: __vspackudw_vvv
*----------------------------------------------------------------------------*/

/* VSPACKUDW */
uint16 __vspackudw_vvv(long8, long8);




/*-----------------------------------------------------------------------------
* ID: __vspackuhb_vvv
*----------------------------------------------------------------------------*/

/* VSPACKUHB */
uchar64 __vspackuhb_vvv(short32, short32);




/*-----------------------------------------------------------------------------
* ID: __vspackuwh_vvv
*----------------------------------------------------------------------------*/

/* VSPACKUWH */
ushort32 __vspackuwh_vvv(int16, int16);




/*-----------------------------------------------------------------------------
* ID: __vspackwh_vvv
*----------------------------------------------------------------------------*/

/* VSPACKWH */
short32 __vspackwh_vvv(int16, int16);




/*-----------------------------------------------------------------------------
* ID: __vspdph_vv
*----------------------------------------------------------------------------*/

/* VSPDPH */
double8 __vspdph_vv(float16);




/*-----------------------------------------------------------------------------
* ID: __vspdpl_vv
*----------------------------------------------------------------------------*/

/* VSPDPL */
double8 __vspdpl_vv(float16);


/*-----------------------------------------------------------------------------
* ID: __vsphp_vv
*----------------------------------------------------------------------------*/

/* VSPHP */
uint16 __vsphp_vv(float16);


/*-----------------------------------------------------------------------------
* ID: __vspint_vv
*----------------------------------------------------------------------------*/

/* VSPINT */
int16 __vspint_vv(float16);




/*-----------------------------------------------------------------------------
* ID: __vspinth_vv
*----------------------------------------------------------------------------*/

/* VSPINTH */
short32 __vspinth_vv(float16);




/*-----------------------------------------------------------------------------
* ID: __vspinthpk_vv
*----------------------------------------------------------------------------*/

/* VSPINTHPK */
short32 __vspinthpk_vv(float16);




/*-----------------------------------------------------------------------------
* ID: __vsptrunc_vv
*----------------------------------------------------------------------------*/

/* VSPTRUNC */
int16 __vsptrunc_vv(float16);




/*-----------------------------------------------------------------------------
* ID: __vsshlh_vkv
*----------------------------------------------------------------------------*/

/* VSSHLH */
short32 __vsshlh_vkv(short32, uchar32);




/*-----------------------------------------------------------------------------
* ID: __vsshlh_vvv
*----------------------------------------------------------------------------*/

/* VSSHLH */
short32 __vsshlh_vvv(short32, uint16);




/*-----------------------------------------------------------------------------
* ID: __vsshlrdw_vkv
*----------------------------------------------------------------------------*/

/* VSSHLRDW */
int16 __vsshlrdw_vkv(long8, uchar8);




/*-----------------------------------------------------------------------------
* ID: __vsshlrdw_vvv
*----------------------------------------------------------------------------*/

/* VSSHLRDW */
int16 __vsshlrdw_vvv(long8, ulong8);




/*-----------------------------------------------------------------------------
* ID: __vsshlrhb_vkv
*----------------------------------------------------------------------------*/

/* VSSHLRHB */
char64 __vsshlrhb_vkv(short32, uchar16);




/*-----------------------------------------------------------------------------
* ID: __vsshlrhb_vvv
*----------------------------------------------------------------------------*/

/* VSSHLRHB */
char64 __vsshlrhb_vvv(short32, uint16);




/*-----------------------------------------------------------------------------
* ID: __vsshlrwh_vkv
*----------------------------------------------------------------------------*/

/* VSSHLRWH */
short32 __vsshlrwh_vkv(int16, uchar16);




/*-----------------------------------------------------------------------------
* ID: __vsshlrwh_vvv
*----------------------------------------------------------------------------*/

/* VSSHLRWH */
short32 __vsshlrwh_vvv(int16, uint16);




/*-----------------------------------------------------------------------------
* ID: __vsshlsuh_vkv
*----------------------------------------------------------------------------*/

/* VSSHLSUH */
ushort32 __vsshlsuh_vkv(short32, uchar32);




/*-----------------------------------------------------------------------------
* ID: __vsshlsuh_vvv
*----------------------------------------------------------------------------*/

/* VSSHLSUH */
ushort32 __vsshlsuh_vvv(short32, uint16);




/*-----------------------------------------------------------------------------
* ID: __vsshlsurdw_vkv
*----------------------------------------------------------------------------*/

/* VSSHLSURDW */
uint16 __vsshlsurdw_vkv(long8, uchar8);




/*-----------------------------------------------------------------------------
* ID: __vsshlsurdw_vvv
*----------------------------------------------------------------------------*/

/* VSSHLSURDW */
uint16 __vsshlsurdw_vvv(long8, ulong8);




/*-----------------------------------------------------------------------------
* ID: __vsshlsurhb_vkv
*----------------------------------------------------------------------------*/

/* VSSHLSURHB */
uchar64 __vsshlsurhb_vkv(short32, uchar16);




/*-----------------------------------------------------------------------------
* ID: __vsshlsurhb_vvv
*----------------------------------------------------------------------------*/

/* VSSHLSURHB */
uchar64 __vsshlsurhb_vvv(short32, uint16);




/*-----------------------------------------------------------------------------
* ID: __vsshlsurwh_vkv
*----------------------------------------------------------------------------*/

/* VSSHLSURWH */
ushort32 __vsshlsurwh_vkv(int16, uchar16);




/*-----------------------------------------------------------------------------
* ID: __vsshlsurwh_vvv
*----------------------------------------------------------------------------*/

/* VSSHLSURWH */
ushort32 __vsshlsurwh_vvv(int16, uint16);




/*-----------------------------------------------------------------------------
* ID: __vsshlsuw_vkv
*----------------------------------------------------------------------------*/

/* VSSHLSUW */
uint16 __vsshlsuw_vkv(int16, uchar16);




/*-----------------------------------------------------------------------------
* ID: __vsshlsuw_vvv
*----------------------------------------------------------------------------*/

/* VSSHLSUW */
uint16 __vsshlsuw_vvv(int16, uint16);




/*-----------------------------------------------------------------------------
* ID: __vsshluh_vkv
*----------------------------------------------------------------------------*/

/* VSSHLUH */
ushort32 __vsshluh_vkv(ushort32, uchar32);




/*-----------------------------------------------------------------------------
* ID: __vsshluh_vvv
*----------------------------------------------------------------------------*/

/* VSSHLUH */
ushort32 __vsshluh_vvv(ushort32, uint16);




/*-----------------------------------------------------------------------------
* ID: __vsshlurdw_vkv
*----------------------------------------------------------------------------*/

/* VSSHLURDW */
uint16 __vsshlurdw_vkv(ulong8, uchar8);




/*-----------------------------------------------------------------------------
* ID: __vsshlurdw_vvv
*----------------------------------------------------------------------------*/

/* VSSHLURDW */
uint16 __vsshlurdw_vvv(ulong8, ulong8);




/*-----------------------------------------------------------------------------
* ID: __vsshlurhb_vkv
*----------------------------------------------------------------------------*/

/* VSSHLURHB */
uchar64 __vsshlurhb_vkv(ushort32, uchar16);




/*-----------------------------------------------------------------------------
* ID: __vsshlurhb_vvv
*----------------------------------------------------------------------------*/

/* VSSHLURHB */
uchar64 __vsshlurhb_vvv(ushort32, uint16);




/*-----------------------------------------------------------------------------
* ID: __vsshlurwh_vkv
*----------------------------------------------------------------------------*/

/* VSSHLURWH */
ushort32 __vsshlurwh_vkv(uint16, uchar16);




/*-----------------------------------------------------------------------------
* ID: __vsshlurwh_vvv
*----------------------------------------------------------------------------*/

/* VSSHLURWH */
ushort32 __vsshlurwh_vvv(uint16, uint16);




/*-----------------------------------------------------------------------------
* ID: __vsshluw_vkv
*----------------------------------------------------------------------------*/

/* VSSHLUW */
uint16 __vsshluw_vkv(uint16, uchar16);




/*-----------------------------------------------------------------------------
* ID: __vsshluw_vvv
*----------------------------------------------------------------------------*/

/* VSSHLUW */
uint16 __vsshluw_vvv(uint16, uint16);




/*-----------------------------------------------------------------------------
* ID: __vsshlw_vkv
*----------------------------------------------------------------------------*/

/* VSSHLW */
int16 __vsshlw_vkv(int16, uchar16);




/*-----------------------------------------------------------------------------
* ID: __vsshlw_vvv
*----------------------------------------------------------------------------*/

/* VSSHLW */
int16 __vsshlw_vvv(int16, uint16);




/*-----------------------------------------------------------------------------
* ID: __vsshvlw_vvv
*----------------------------------------------------------------------------*/

/* VSSHVLW */
int16 __vsshvlw_vvv(int16, int16);




/*-----------------------------------------------------------------------------
* ID: __vsshvrw_vvv
*----------------------------------------------------------------------------*/

/* VSSHVRW */
int16 __vsshvrw_vvv(int16, int16);




/*-----------------------------------------------------------------------------
* ID: __vssubb_vkv
*----------------------------------------------------------------------------*/

/* VSSUBB */
char64 __vssubb_vkv(char64, char64);




/*-----------------------------------------------------------------------------
* ID: __vssubb_vvv
*----------------------------------------------------------------------------*/

/* VSSUBB */
char64 __vssubb_vvv(char64, char64);




/*-----------------------------------------------------------------------------
* ID: __vssubh_vkv
*----------------------------------------------------------------------------*/

/* VSSUBH */
short32 __vssubh_vkv(short32, short32);




/*-----------------------------------------------------------------------------
* ID: __vssubh_vvv
*----------------------------------------------------------------------------*/

/* VSSUBH */
short32 __vssubh_vvv(short32, short32);




/*-----------------------------------------------------------------------------
* ID: __vssubw_vkv
*----------------------------------------------------------------------------*/

/* VSSUBW */
int16 __vssubw_vkv(int16, int16);




/*-----------------------------------------------------------------------------
* ID: __vssubw_vvv
*----------------------------------------------------------------------------*/

/* VSSUBW */
int16 __vssubw_vvv(int16, int16);




/*-----------------------------------------------------------------------------
* ID: __vsubabsb_vvv
*----------------------------------------------------------------------------*/

/* VSUBABSB */
uchar64 __vsubabsb_vvv(uchar64, uchar64);




/*-----------------------------------------------------------------------------
* ID: __vsubabsd_vvv
*----------------------------------------------------------------------------*/

/* VSUBABSD */
ulong8 __vsubabsd_vvv(ulong8, ulong8);




/*-----------------------------------------------------------------------------
* ID: __vsubabsh_vvv
*----------------------------------------------------------------------------*/

/* VSUBABSH */
ushort32 __vsubabsh_vvv(ushort32, ushort32);




/*-----------------------------------------------------------------------------
* ID: __vsubabsw_vvv
*----------------------------------------------------------------------------*/

/* VSUBABSW */
uint16 __vsubabsw_vvv(uint16, uint16);




/*-----------------------------------------------------------------------------
* ID: __vsubb_vkv
*----------------------------------------------------------------------------*/

/* VSUBB */
char64 __vsubb_vkv(char64, char64);
uchar64 __vsubb_vkv(uchar64, uchar64);




/*-----------------------------------------------------------------------------
* ID: __vsubb_vvv
*----------------------------------------------------------------------------*/

/* VSUBB */
char64 __vsubb_vvv(char64, char64);
uchar64 __vsubb_vvv(uchar64, uchar64);




/*-----------------------------------------------------------------------------
* ID: __vsubcw_vvv
*----------------------------------------------------------------------------*/

/* VSUBCW */
uint16 __vsubcw_vvv(uint16, uint16);




/*-----------------------------------------------------------------------------
* ID: __vsubd_vkv
*----------------------------------------------------------------------------*/

/* VSUBD */
long8 __vsubd_vkv(long8, int8);
ulong8 __vsubd_vkv(ulong8, uint8);




/*-----------------------------------------------------------------------------
* ID: __vsubd_vvv
*----------------------------------------------------------------------------*/

/* VSUBD */
long8 __vsubd_vvv(long8, long8);
ulong8 __vsubd_vvv(ulong8, ulong8);




/*-----------------------------------------------------------------------------
* ID: __vsubdp_vvv
*----------------------------------------------------------------------------*/

/* VSUBDP */
double8 __vsubdp_vvv(double8, double8);




/*-----------------------------------------------------------------------------
* ID: __vsubh_vkv
*----------------------------------------------------------------------------*/

/* VSUBH */
short32 __vsubh_vkv(short32, short32);
ushort32 __vsubh_vkv(ushort32, ushort32);




/*-----------------------------------------------------------------------------
* ID: __vsubh_vvv
*----------------------------------------------------------------------------*/

/* VSUBH */
short32 __vsubh_vvv(short32, short32);
ushort32 __vsubh_vvv(ushort32, ushort32);




/*-----------------------------------------------------------------------------
* ID: __vsubrb_vkv
*----------------------------------------------------------------------------*/

/* VSUBRB */
char64 __vsubrb_vkv(char64, char64);
uchar64 __vsubrb_vkv(uchar64, uchar64);




/*-----------------------------------------------------------------------------
* ID: __vsubrb_vvv
*----------------------------------------------------------------------------*/

/* VSUBRB */
char64 __vsubrb_vvv(char64, char64);
uchar64 __vsubrb_vvv(uchar64, uchar64);




/*-----------------------------------------------------------------------------
* ID: __vsubrd_vkv
*----------------------------------------------------------------------------*/

/* VSUBRD */
long8 __vsubrd_vkv(long8, int8);
ulong8 __vsubrd_vkv(ulong8, uint8);




/*-----------------------------------------------------------------------------
* ID: __vsubrd_vvv
*----------------------------------------------------------------------------*/

/* VSUBRD */
long8 __vsubrd_vvv(long8, long8);
ulong8 __vsubrd_vvv(ulong8, ulong8);




/*-----------------------------------------------------------------------------
* ID: __vsubrh_vkv
*----------------------------------------------------------------------------*/

/* VSUBRH */
short32 __vsubrh_vkv(short32, short32);
ushort32 __vsubrh_vkv(ushort32, ushort32);




/*-----------------------------------------------------------------------------
* ID: __vsubrh_vvv
*----------------------------------------------------------------------------*/

/* VSUBRH */
short32 __vsubrh_vvv(short32, short32);
ushort32 __vsubrh_vvv(ushort32, ushort32);




/*-----------------------------------------------------------------------------
* ID: __vsubrw_vkv
*----------------------------------------------------------------------------*/

/* VSUBRW */
int16 __vsubrw_vkv(int16, int16);
uint16 __vsubrw_vkv(uint16, uint16);




/*-----------------------------------------------------------------------------
* ID: __vsubrw_vvv
*----------------------------------------------------------------------------*/

/* VSUBRW */
int16 __vsubrw_vvv(int16, int16);
uint16 __vsubrw_vvv(uint16, uint16);




/*-----------------------------------------------------------------------------
* ID: __vsubsp_vvv
*----------------------------------------------------------------------------*/

/* VSUBSP */
float16 __vsubsp_vvv(float16, float16);




/*-----------------------------------------------------------------------------
* ID: __vsubw_vkv
*----------------------------------------------------------------------------*/

/* VSUBW */
int16 __vsubw_vkv(int16, int16);
uint16 __vsubw_vkv(uint16, uint16);




/*-----------------------------------------------------------------------------
* ID: __vsubw_vvv
*----------------------------------------------------------------------------*/

/* VSUBW */
int16 __vsubw_vvv(int16, int16);
uint16 __vsubw_vvv(uint16, uint16);




/*-----------------------------------------------------------------------------
* ID: __vswapb_vv
*----------------------------------------------------------------------------*/

/* VSWAPB */
char64 __vswapb_vv(char64);
uchar64 __vswapb_vv(uchar64);




/*-----------------------------------------------------------------------------
* ID: __vswapd_vv
*----------------------------------------------------------------------------*/

/* VSWAPD */
long8 __vswapd_vv(long8);
double8 __vswapd_vv(double8);
cfloat8 __vswapd_vv(cfloat8);
ulong8 __vswapd_vv(ulong8);
cint8 __vswapd_vv(cint8);




/*-----------------------------------------------------------------------------
* ID: __vswaph_vv
*----------------------------------------------------------------------------*/

/* VSWAPH */
short32 __vswaph_vv(short32);
ushort32 __vswaph_vv(ushort32);
cchar32 __vswaph_vv(cchar32);




/*-----------------------------------------------------------------------------
* ID: __vswapw_vv
*----------------------------------------------------------------------------*/

/* VSWAPW */
int16 __vswapw_vv(int16);
float16 __vswapw_vv(float16);
uint16 __vswapw_vv(uint16);
cshort16 __vswapw_vv(cshort16);




/*-----------------------------------------------------------------------------
* ID: __vunpkhb_vv
*----------------------------------------------------------------------------*/

/* VUNPKHB */
short32 __vunpkhb_vv(char64);




/*-----------------------------------------------------------------------------
* ID: __vunpkhh_vv
*----------------------------------------------------------------------------*/

/* VUNPKHH */
int16 __vunpkhh_vv(short32);




/*-----------------------------------------------------------------------------
* ID: __vunpkhub_vv
*----------------------------------------------------------------------------*/

/* VUNPKHUB */
ushort32 __vunpkhub_vv(uchar64);




/*-----------------------------------------------------------------------------
* ID: __vunpkhuh_vv
*----------------------------------------------------------------------------*/

/* VUNPKHUH */
uint16 __vunpkhuh_vv(ushort32);




/*-----------------------------------------------------------------------------
* ID: __vunpklb_vv
*----------------------------------------------------------------------------*/

/* VUNPKLB */
short32 __vunpklb_vv(char64);




/*-----------------------------------------------------------------------------
* ID: __vunpklh_vv
*----------------------------------------------------------------------------*/

/* VUNPKLH */
int16 __vunpklh_vv(short32);




/*-----------------------------------------------------------------------------
* ID: __vunpklub_vv
*----------------------------------------------------------------------------*/

/* VUNPKLUB */
ushort32 __vunpklub_vv(uchar64);




/*-----------------------------------------------------------------------------
* ID: __vunpkluh_vv
*----------------------------------------------------------------------------*/

/* VUNPKLUH */
uint16 __vunpkluh_vv(ushort32);




/*-----------------------------------------------------------------------------
* ID: __vunpkluw_vv
*----------------------------------------------------------------------------*/

/* VUNPKLUW */
ulong8 __vunpkluw_vv(uint16);




/*-----------------------------------------------------------------------------
* ID: __vunpklw_vv
*----------------------------------------------------------------------------*/

/* VUNPKLW */
long8 __vunpklw_vv(int16);




/*-----------------------------------------------------------------------------
* ID: __vwpackh_vvv
*----------------------------------------------------------------------------*/

/* VWPACKH */
int16 __vwpackh_vvv(int16, int16);
float16 __vwpackh_vvv(float16, float16);
uint16 __vwpackh_vvv(uint16, uint16);
cshort16 __vwpackh_vvv(cshort16, cshort16);




/*-----------------------------------------------------------------------------
* ID: __vwpackl_vvv
*----------------------------------------------------------------------------*/

/* VWPACKL */
int16 __vwpackl_vvv(int16, int16);
float16 __vwpackl_vvv(float16, float16);
uint16 __vwpackl_vvv(uint16, uint16);
cshort16 __vwpackl_vvv(cshort16, cshort16);




/*-----------------------------------------------------------------------------
* ID: __vwunpkd_vv
*----------------------------------------------------------------------------*/

/* VWUNPKD */
long8 __vwunpkd_vv(int8);




/*-----------------------------------------------------------------------------
* ID: __vwunpkdh_vv
*----------------------------------------------------------------------------*/

/* VWUNPKDH */
long8 __vwunpkdh_vv(int16);




/*-----------------------------------------------------------------------------
* ID: __vwunpkdl_vv
*----------------------------------------------------------------------------*/

/* VWUNPKDL */
long8 __vwunpkdl_vv(int16);




/*-----------------------------------------------------------------------------
* ID: __vwunpkdu_vv
*----------------------------------------------------------------------------*/

/* VWUNPKDU */
ulong8 __vwunpkdu_vv(uint8);




/*-----------------------------------------------------------------------------
* ID: __vwunpkudh_vv
*----------------------------------------------------------------------------*/

/* VWUNPKUDH */
ulong8 __vwunpkudh_vv(uint16);




/*-----------------------------------------------------------------------------
* ID: __vwunpkudl_vv
*----------------------------------------------------------------------------*/

/* VWUNPKUDL */
ulong8 __vwunpkudl_vv(uint16);




/*-----------------------------------------------------------------------------
* ID: __vxnorw_vvv
*----------------------------------------------------------------------------*/

/* VXNORW */
char64 __vxnorw_vvv(char64, char64);
short32 __vxnorw_vvv(short32, short32);
int16 __vxnorw_vvv(int16, int16);
long8 __vxnorw_vvv(long8, long8);
uchar64 __vxnorw_vvv(uchar64, uchar64);
ushort32 __vxnorw_vvv(ushort32, ushort32);
uint16 __vxnorw_vvv(uint16, uint16);
ulong8 __vxnorw_vvv(ulong8, ulong8);




/*-----------------------------------------------------------------------------
* ID: __vxormpyw_vvv
*----------------------------------------------------------------------------*/

/* VXORMPYW */
uint16 __vxormpyw_vvv(uint16, uint16);




/*-----------------------------------------------------------------------------
* ID: __vxorw_vkv
*----------------------------------------------------------------------------*/

/* VXORW */
int16 __vxorw_vkv(int16, int16);
uint16 __vxorw_vkv(uint16, uint16);




/*-----------------------------------------------------------------------------
* ID: __vxorw_vvv
*----------------------------------------------------------------------------*/

/* VXORW */
char64 __vxorw_vvv(char64, char64);
short32 __vxorw_vvv(short32, short32);
int16 __vxorw_vvv(int16, int16);
long8 __vxorw_vvv(long8, long8);
uchar64 __vxorw_vvv(uchar64, uchar64);
ushort32 __vxorw_vvv(ushort32, ushort32);
uint16 __vxorw_vvv(uint16, uint16);
ulong8 __vxorw_vvv(ulong8, ulong8);




/*-----------------------------------------------------------------------------
* ID: __xnor
*----------------------------------------------------------------------------*/

/* VXNORW */
int8_t __xnor(int8_t, int8_t);
char2 __xnor(char2, char2);
/* UNSUPPORTED: char3 __xnor(char3, char3); */
char4 __xnor(char4, char4);
char8 __xnor(char8, char8);
char16 __xnor(char16, char16);
char32 __xnor(char32, char32);
char64 __xnor(char64, char64);
int16_t __xnor(int16_t, int16_t);
short2 __xnor(short2, short2);
/* UNSUPPORTED: short3 __xnor(short3, short3); */
short4 __xnor(short4, short4);
short8 __xnor(short8, short8);
short16 __xnor(short16, short16);
short32 __xnor(short32, short32);
int32_t __xnor(int32_t, int32_t);
int2 __xnor(int2, int2);
/* UNSUPPORTED: int3 __xnor(int3, int3); */
int4 __xnor(int4, int4);
int8 __xnor(int8, int8);
int16 __xnor(int16, int16);
int64_t __xnor(int64_t, int64_t);
long2 __xnor(long2, long2);
/* UNSUPPORTED: long3 __xnor(long3, long3); */
long4 __xnor(long4, long4);
long8 __xnor(long8, long8);
uchar __xnor(uchar, uchar);
uchar2 __xnor(uchar2, uchar2);
/* UNSUPPORTED: uchar3 __xnor(uchar3, uchar3); */
uchar4 __xnor(uchar4, uchar4);
uchar8 __xnor(uchar8, uchar8);
uchar16 __xnor(uchar16, uchar16);
uchar32 __xnor(uchar32, uchar32);
uchar64 __xnor(uchar64, uchar64);
ushort __xnor(ushort, ushort);
ushort2 __xnor(ushort2, ushort2);
/* UNSUPPORTED: ushort3 __xnor(ushort3, ushort3); */
ushort4 __xnor(ushort4, ushort4);
ushort8 __xnor(ushort8, ushort8);
ushort16 __xnor(ushort16, ushort16);
ushort32 __xnor(ushort32, ushort32);
uint __xnor(uint, uint);
uint2 __xnor(uint2, uint2);
/* UNSUPPORTED: uint3 __xnor(uint3, uint3); */
uint4 __xnor(uint4, uint4);
uint8 __xnor(uint8, uint8);
uint16 __xnor(uint16, uint16);
ulong __xnor(ulong, ulong);
ulong2 __xnor(ulong2, ulong2);
/* UNSUPPORTED: ulong3 __xnor(ulong3, ulong3); */
ulong4 __xnor(ulong4, ulong4);
ulong8 __xnor(ulong8, ulong8);

/* XNOR */
__vpred __xnor(__vpred, __vpred);

/* XNORD */
int8_t __xnor(int8_t, int8_t);
char2 __xnor(char2, char2);
/* UNSUPPORTED: char3 __xnor(char3, char3); */
char4 __xnor(char4, char4);
char8 __xnor(char8, char8);
int16_t __xnor(int16_t, int16_t);
short2 __xnor(short2, short2);
/* UNSUPPORTED: short3 __xnor(short3, short3); */
short4 __xnor(short4, short4);
int32_t __xnor(int32_t, int32_t);
int2 __xnor(int2, int2);
int64_t __xnor(int64_t, int64_t);
/* CONSTANT: int64_t __xnor(int64_t, (int64_t)(k)); */
uchar __xnor(uchar, uchar);
uchar2 __xnor(uchar2, uchar2);
/* UNSUPPORTED: uchar3 __xnor(uchar3, uchar3); */
uchar4 __xnor(uchar4, uchar4);
uchar8 __xnor(uchar8, uchar8);
ushort __xnor(ushort, ushort);
ushort2 __xnor(ushort2, ushort2);
/* UNSUPPORTED: ushort3 __xnor(ushort3, ushort3); */
ushort4 __xnor(ushort4, ushort4);
uint __xnor(uint, uint);
uint2 __xnor(uint2, uint2);
ulong __xnor(ulong, ulong);
/* CONSTANT: ulong __xnor(ulong, (ulong)(k)); */

/* XNORW */
int8_t __xnor(int8_t, int8_t);
char2 __xnor(char2, char2);
/* UNSUPPORTED: char3 __xnor(char3, char3); */
char4 __xnor(char4, char4);
int16_t __xnor(int16_t, int16_t);
short2 __xnor(short2, short2);
int32_t __xnor(int32_t, int32_t);
uchar __xnor(uchar, uchar);
uchar2 __xnor(uchar2, uchar2);
/* UNSUPPORTED: uchar3 __xnor(uchar3, uchar3); */
uchar4 __xnor(uchar4, uchar4);
ushort __xnor(ushort, ushort);
ushort2 __xnor(ushort2, ushort2);
uint __xnor(uint, uint);




/*-----------------------------------------------------------------------------
* ID: __xnor_ppp
*----------------------------------------------------------------------------*/

/* XNOR */
__vpred __xnor_ppp(__vpred, __vpred);




/*-----------------------------------------------------------------------------
* ID: __xnord_ddd
*----------------------------------------------------------------------------*/

/* XNORD */
char8 __xnord_ddd(char8, char8);
short4 __xnord_ddd(short4, short4);
int2 __xnord_ddd(int2, int2);
int64_t __xnord_ddd(int64_t, int64_t);
uchar8 __xnord_ddd(uchar8, uchar8);
ushort4 __xnord_ddd(ushort4, ushort4);
uint2 __xnord_ddd(uint2, uint2);
ulong __xnord_ddd(ulong, ulong);




/*-----------------------------------------------------------------------------
* ID: __xnord_dkd
*----------------------------------------------------------------------------*/

/* XNORD */
int64_t __xnord_dkd(int64_t, int64_t);
ulong __xnord_dkd(ulong, ulong);




/*-----------------------------------------------------------------------------
* ID: __xnorw_rrr
*----------------------------------------------------------------------------*/

/* XNORW */
char4 __xnorw_rrr(char4, char4);
short2 __xnorw_rrr(short2, short2);
int32_t __xnorw_rrr(int32_t, int32_t);
uchar4 __xnorw_rrr(uchar4, uchar4);
ushort2 __xnorw_rrr(ushort2, ushort2);
uint __xnorw_rrr(uint, uint);




/*-----------------------------------------------------------------------------
* ID: __xor
*----------------------------------------------------------------------------*/

/* XOR */
__vpred __xor(__vpred, __vpred);




/*-----------------------------------------------------------------------------
* ID: __xor_ppp
*----------------------------------------------------------------------------*/

/* XOR */
__vpred __xor_ppp(__vpred, __vpred);




/*-----------------------------------------------------------------------------
* ID: __xorb_operator
*----------------------------------------------------------------------------*/
/*

VXORW
char = char ^ char;
char2 = char2 ^ char2;
char3 = char3 ^ char3;
char4 = char4 ^ char4;
char8 = char8 ^ char8;
char16 = char16 ^ char16;
char32 = char32 ^ char32;
char64 = char64 ^ char64;
short = short ^ short;
short2 = short2 ^ short2;
short3 = short3 ^ short3;
short4 = short4 ^ short4;
short8 = short8 ^ short8;
short16 = short16 ^ short16;
short32 = short32 ^ short32;
int = int ^ int;
int2 = int2 ^ int2;
int3 = int3 ^ int3;
int4 = int4 ^ int4;
int8 = int8 ^ int8;
int16 = int16 ^ int16;
long = long ^ long;
long2 = long2 ^ long2;
long3 = long3 ^ long3;
long4 = long4 ^ long4;
long8 = long8 ^ long8;
int = int ^ (int)(k);
int2 = int2 ^ (int2)(k);
int3 = int3 ^ (int3)(k);
int4 = int4 ^ (int4)(k);
int8 = int8 ^ (int8)(k);
int16 = int16 ^ (int16)(k);
uchar = uchar ^ uchar;
uchar2 = uchar2 ^ uchar2;
uchar3 = uchar3 ^ uchar3;
uchar4 = uchar4 ^ uchar4;
uchar8 = uchar8 ^ uchar8;
uchar16 = uchar16 ^ uchar16;
uchar32 = uchar32 ^ uchar32;
uchar64 = uchar64 ^ uchar64;
ushort = ushort ^ ushort;
ushort2 = ushort2 ^ ushort2;
ushort3 = ushort3 ^ ushort3;
ushort4 = ushort4 ^ ushort4;
ushort8 = ushort8 ^ ushort8;
ushort16 = ushort16 ^ ushort16;
ushort32 = ushort32 ^ ushort32;
uint = uint ^ uint;
uint2 = uint2 ^ uint2;
uint3 = uint3 ^ uint3;
uint4 = uint4 ^ uint4;
uint8 = uint8 ^ uint8;
uint16 = uint16 ^ uint16;
ulong = ulong ^ ulong;
ulong2 = ulong2 ^ ulong2;
ulong3 = ulong3 ^ ulong3;
ulong4 = ulong4 ^ ulong4;
ulong8 = ulong8 ^ ulong8;
uint = uint ^ (uint)(k);
uint2 = uint2 ^ (uint2)(k);
uint3 = uint3 ^ (uint3)(k);
uint4 = uint4 ^ (uint4)(k);
uint8 = uint8 ^ (uint8)(k);
uint16 = uint16 ^ (uint16)(k);

XORD
char = char ^ char;
char2 = char2 ^ char2;
char3 = char3 ^ char3;
char4 = char4 ^ char4;
char8 = char8 ^ char8;
short = short ^ short;
short2 = short2 ^ short2;
short3 = short3 ^ short3;
short4 = short4 ^ short4;
int = int ^ int;
int2 = int2 ^ int2;
long = long ^ long;
long = long ^ (long)(k);
uchar = uchar ^ uchar;
uchar2 = uchar2 ^ uchar2;
uchar3 = uchar3 ^ uchar3;
uchar4 = uchar4 ^ uchar4;
uchar8 = uchar8 ^ uchar8;
ushort = ushort ^ ushort;
ushort2 = ushort2 ^ ushort2;
ushort3 = ushort3 ^ ushort3;
ushort4 = ushort4 ^ ushort4;
uint = uint ^ uint;
uint2 = uint2 ^ uint2;
ulong = ulong ^ ulong;
ulong = ulong ^ (ulong)(k);

XORW
char = char ^ char;
char2 = char2 ^ char2;
char3 = char3 ^ char3;
char4 = char4 ^ char4;
short = short ^ short;
short2 = short2 ^ short2;
int = int ^ int;
char = char ^ (char)(k);
char2 = char2 ^ (char2)(k);
char3 = char3 ^ (char3)(k);
char4 = char4 ^ (char4)(k);
short = short ^ (short)(k);
short2 = short2 ^ (short2)(k);
int = int ^ (int)(k);
uchar = uchar ^ uchar;
uchar2 = uchar2 ^ uchar2;
uchar3 = uchar3 ^ uchar3;
uchar4 = uchar4 ^ uchar4;
ushort = ushort ^ ushort;
ushort2 = ushort2 ^ ushort2;
uint = uint ^ uint;
uchar = uchar ^ (uchar)(k);
uchar2 = uchar2 ^ (uchar2)(k);
uchar3 = uchar3 ^ (uchar3)(k);
uchar4 = uchar4 ^ (uchar4)(k);
ushort = ushort ^ (ushort)(k);
ushort2 = ushort2 ^ (ushort2)(k);
uint = uint ^ (uint)(k);

*/


/*-----------------------------------------------------------------------------
* ID: __xord_ddd
*----------------------------------------------------------------------------*/

/* XORD */
char8 __xord_ddd(char8, char8);
short4 __xord_ddd(short4, short4);
int2 __xord_ddd(int2, int2);
int64_t __xord_ddd(int64_t, int64_t);
uchar8 __xord_ddd(uchar8, uchar8);
ushort4 __xord_ddd(ushort4, ushort4);
uint2 __xord_ddd(uint2, uint2);
ulong __xord_ddd(ulong, ulong);




/*-----------------------------------------------------------------------------
* ID: __xord_dkd
*----------------------------------------------------------------------------*/

/* XORD */
int64_t __xord_dkd(int64_t, int64_t);
ulong __xord_dkd(ulong, ulong);




/*-----------------------------------------------------------------------------
* ID: __xorw_rkr
*----------------------------------------------------------------------------*/

/* XORW */
char4 __xorw_rkr(char4, char4);
short2 __xorw_rkr(short2, short2);
int32_t __xorw_rkr(int32_t, int32_t);
uchar4 __xorw_rkr(uchar4, uchar4);
ushort2 __xorw_rkr(ushort2, ushort2);
uint __xorw_rkr(uint, uint);




/*-----------------------------------------------------------------------------
* ID: __xorw_rrr
*----------------------------------------------------------------------------*/

/* XORW */
char4 __xorw_rrr(char4, char4);
short2 __xorw_rrr(short2, short2);
int32_t __xorw_rrr(int32_t, int32_t);
uchar4 __xorw_rrr(uchar4, uchar4);
ushort2 __xorw_rrr(ushort2, ushort2);
uint __xorw_rrr(uint, uint);




/*-----------------------------------------------------------------------------
* ID: __xpnd4h_rd
*----------------------------------------------------------------------------*/

/* XPND4H */
ushort4 __xpnd4h_rd(uchar);
short4 __xpnd4h_rd(int8_t);




/*-----------------------------------------------------------------------------
* ID: __xpnd8b_rd
*----------------------------------------------------------------------------*/

/* XPND8B */
uchar8 __xpnd8b_rd(uchar);
char8 __xpnd8b_rd(int8_t);





/*****************************************************************************/
/* GTSC CONTROL REGISTER ACCESS                                              */
/*****************************************************************************/
int64_t __get_GTSC(uint32_t opt);

/*****************************************************************************/
/* INDEXED CONTROL REGISTER ACCESS                                           */
/*****************************************************************************/
int64_t __get_indexed(__INDEXED_CR src, uint32_t idx);
void    __set_indexed(__INDEXED_CR dst, uint32_t idx, int64_t src);

/*****************************************************************************/
/* INSTRUCTION CACHE FLUSH AND REFRESH                                       */
/*****************************************************************************/
typedef enum
{
    __IINVAL_MMUNONE = 0,
    __IINVAL_MMU0    = 1,
    __IINVAL_MMU1    = 2,
    __IINVAL_MMUALL  = 3
} __IINVAL_TYPE;
void __instr_cache_invalidate(__IINVAL_TYPE);

/*****************************************************************************/
/* VECTOR PREDICATE TO REGISTER INTERFACE                                    */
/*****************************************************************************/
vpred   _mvrp(int64_t);
int64_t _mvpb(vpred);
int64_t _mvph(vpred);
int64_t _mvpw(vpred);
int64_t _mvpd(vpred);

/*****************************************************************************/
/* Floating point VMATMPY pseudo-instruction: VMATMPYSP + VADDSP             */
/*****************************************************************************/
__float2 __float_matmpy(__float2, __float4);
__float4 __float_matmpy(__float4, __float8);
__float8 __float_matmpy(__float8, __float16);

/*****************************************************************************/
/* Complex VMPYSP pseudo-instruction: VCMPYSP + VADDSP */
/*****************************************************************************/
__cfloat   __complex_multiply(__cfloat,  __cfloat);
__cfloat2  __complex_multiply(__cfloat2, __cfloat2);
__cfloat4  __complex_multiply(__cfloat4, __cfloat4);
__cfloat8  __complex_multiply(__cfloat8, __cfloat8);

/*****************************************************************************/
/* Complex Conjugate VMPYSP pseudo-instruction: VCMPYSP + VSUBSP */
/*****************************************************************************/
__cfloat   __complex_conjugate_multiply(__cfloat,  __cfloat);
__cfloat2  __complex_conjugate_multiply(__cfloat2, __cfloat2);
__cfloat4  __complex_conjugate_multiply(__cfloat4, __cfloat4);
__cfloat8  __complex_conjugate_multiply(__cfloat8, __cfloat8);

#define __conj_cmpy(a,b) __complex_conjugate_multiply((a),(b));

bool2 __mask_bool2(uint32_t a);
bool2 __mask_bool2(uint8_t a);
bool4 __mask_bool4(uint32_t a);
bool4 __mask_bool4(uint8_t a);
bool8 __mask_bool8(uint32_t a);
bool8 __mask_bool8(uint8_t a);
bool16 __mask_bool16(uint32_t a);
bool16 __mask_bool16(uint8_t a);
bool32 __mask_bool32(uint32_t a);
bool32 __mask_bool32(uint8_t a);
bool64 __mask_bool64(uint32_t a);
bool64 __mask_bool64(uint8_t a);

bool2   __reduce_bool2(bool4);
bool2   __reduce_bool2(bool8);
bool2   __reduce_bool2(bool16);
bool2   __reduce_bool2(bool32);
bool2   __reduce_bool2(bool64);
bool2   __create_bool2(uint64_t);
bool4   __extend_bool4(bool2);
bool4   __reduce_bool4(bool8);
bool4   __reduce_bool4(bool16);
bool4   __reduce_bool4(bool32);
bool4   __reduce_bool4(bool64);
bool4   __create_bool4(uint64_t);
bool8   __extend_bool8(bool2);
bool8   __extend_bool8(bool4);
bool8   __reduce_bool8(bool16);
bool8   __reduce_bool8(bool32);
bool8   __reduce_bool8(bool64);
bool8   __create_bool8(uint64_t);
bool16   __extend_bool16(bool2);
bool16   __extend_bool16(bool4);
bool16   __extend_bool16(bool8);
bool16   __reduce_bool16(bool32);
bool16   __reduce_bool16(bool64);
bool16   __create_bool16(uint64_t);
bool32   __extend_bool32(bool2);
bool32   __extend_bool32(bool4);
bool32   __extend_bool32(bool8);
bool32   __extend_bool32(bool16);
bool32   __reduce_bool32(bool64);
bool32   __create_bool32(uint64_t);
bool64   __extend_bool64(bool2);
bool64   __extend_bool64(bool4);
bool64   __extend_bool64(bool8);
bool64   __extend_bool64(bool16);
bool64   __extend_bool64(bool32);
bool64   __create_bool64(uint64_t);

bool2  __create_bool2(__vpred);
bool4  __create_bool4(__vpred);
bool8  __create_bool8(__vpred);
bool16 __create_bool16(__vpred);
bool32 __create_bool32(__vpred);
bool64 __create_bool64(__vpred);

__vpred __create_vpred(bool2);
__vpred __create_vpred(bool4);
__vpred __create_vpred(bool8);
__vpred __create_vpred(bool16);
__vpred __create_vpred(bool32);
__vpred __create_vpred(bool64);
__vpred __create_vpred(uint64_t);

uint64_t  __create_scalar(bool2);
uint64_t  __create_scalar(bool4);
uint64_t  __create_scalar(bool8);
uint64_t  __create_scalar(bool16);
uint64_t  __create_scalar(bool32);
uint64_t  __create_scalar(bool64);
uint64_t  __create_scalar(__vpred);

void  __vstore_pred_interleave(bool64, __char64_ptr,  __char64,  __char64);
void  __vstore_pred_interleave(bool32, __short32_ptr, __short32, __short32);
void  __vstore_pred_interleave(bool16, __int16_ptr,   __int16,   __int16);
void  __vstore_pred_interleave(bool8, __long8_ptr,   __long8,   __long8);
void  __vstore_pred_interleave(bool64, __uchar64_ptr, __uchar64, __uchar64);
void  __vstore_pred_interleave(bool32, __ushort32_ptr,__ushort32,__ushort32);
void  __vstore_pred_interleave(bool16, __uint16_ptr,  __uint16,  __uint16);
void  __vstore_pred_interleave(bool8, __ulong8_ptr,  __ulong8,  __ulong8);
void  __vstore_pred_interleave4(bool32, __char32_ptr,  __char64,   __char64);
void  __vstore_pred_interleave4(bool32, __uchar32_ptr, __uchar64,  __uchar64);
void  __vstore_pred(bool2,  __char2_ptr,   __char2);
void  __vstore_pred(bool4,  __char4_ptr,   __char4);
void  __vstore_pred(bool8,  __char8_ptr,   __char8);
void  __vstore_pred(bool16, __char16_ptr,  __char16);
void  __vstore_pred(bool32, __char32_ptr,  __char32);
void  __vstore_pred(bool64, __char64_ptr,  __char64);
void  __vstore_pred(bool2,  __uchar2_ptr,  __uchar2);
void  __vstore_pred(bool4,  __uchar4_ptr,  __uchar4);
void  __vstore_pred(bool8,  __uchar8_ptr,  __uchar8);
void  __vstore_pred(bool16, __uchar16_ptr, __uchar16);
void  __vstore_pred(bool32, __uchar32_ptr, __uchar32);
void  __vstore_pred(bool64, __uchar64_ptr, __uchar64);
void  __vstore_pred(bool2,  __short2_ptr, __short2);
void  __vstore_pred(bool4,  __short4_ptr, __short4);
void  __vstore_pred(bool8,  __short8_ptr, __short8);
void  __vstore_pred(bool16, __short16_ptr, __short16);
void  __vstore_pred(bool32, __short32_ptr, __short32);
void  __vstore_pred(bool2,  __ushort2_ptr, __ushort2);
void  __vstore_pred(bool4,  __ushort4_ptr, __ushort4);
void  __vstore_pred(bool8,  __ushort8_ptr, __ushort8);
void  __vstore_pred(bool16, __ushort16_ptr, __ushort16);
void  __vstore_pred(bool32, __ushort32_ptr, __ushort32);
void  __vstore_pred(bool2,  __cchar2_ptr, __cchar2);
void  __vstore_pred(bool4,  __cchar4_ptr, __cchar4);
void  __vstore_pred(bool8,  __cchar8_ptr, __cchar8);
void  __vstore_pred(bool16, __cchar16_ptr, __cchar16);
void  __vstore_pred(bool32, __cchar32_ptr, __cchar32);
void  __vstore_pred(bool2,  __int2_ptr,     __int2);
void  __vstore_pred(bool4,  __int4_ptr,     __int4);
void  __vstore_pred(bool8,  __int8_ptr,     __int8);
void  __vstore_pred(bool16, __int16_ptr,    __int16);
void  __vstore_pred(bool2,  __uint2_ptr,    __uint2);
void  __vstore_pred(bool4,  __uint4_ptr,    __uint4);
void  __vstore_pred(bool8,  __uint8_ptr,    __uint8);
void  __vstore_pred(bool16, __uint16_ptr,   __uint16);
void  __vstore_pred(bool2,  __float2_ptr,   __float2);
void  __vstore_pred(bool4,  __float4_ptr,   __float4);
void  __vstore_pred(bool8,  __float8_ptr,   __float8);
void  __vstore_pred(bool16, __float16_ptr,  __float16);
void  __vstore_pred(bool2,  __cshort2_ptr,  __cshort2);
void  __vstore_pred(bool4,  __cshort4_ptr,  __cshort4);
void  __vstore_pred(bool8,  __cshort8_ptr,  __cshort8);
void  __vstore_pred(bool16, __cshort16_ptr, __cshort16);
void  __vstore_pred(bool2, __long2_ptr,   __long2);
void  __vstore_pred(bool4, __long4_ptr,   __long4);
void  __vstore_pred(bool8, __long8_ptr,   __long8);
void  __vstore_pred(bool2, __ulong2_ptr,  __ulong2);
void  __vstore_pred(bool4, __ulong4_ptr,  __ulong4);
void  __vstore_pred(bool8, __ulong8_ptr,  __ulong8);
void  __vstore_pred(bool2, __double2_ptr, __double2);
void  __vstore_pred(bool4, __double4_ptr, __double4);
void  __vstore_pred(bool8, __double8_ptr, __double8);
void  __vstore_pred(bool2, __cint2_ptr,   __cint2);
void  __vstore_pred(bool4, __cint4_ptr,   __cint4);
void  __vstore_pred(bool8, __cint8_ptr,   __cint8);
void  __vstore_pred(bool2, __cfloat2_ptr, __cfloat2);
void  __vstore_pred(bool4, __cfloat4_ptr, __cfloat4);
void  __vstore_pred(bool8, __cfloat8_ptr, __cfloat8);
void  __vstore_pred_packl(bool32,   __char32_ptr,  __short32);
void  __vstore_pred_packl(bool32,   __uchar32_ptr, __ushort32);
void  __vstore_pred_packh(bool32,   __char32_ptr,  __short32);
void  __vstore_pred_packh(bool32,   __uchar32_ptr, __ushort32);
void  __vstore_pred_packhs1(bool32, __char32_ptr,  __short32);
void  __vstore_pred_packhs1(bool32, __uchar32_ptr, __ushort32);
void  __vstore_pred_packl(bool16,   __short16_ptr,  __int16);
void  __vstore_pred_packl(bool16,   __ushort16_ptr, __uint16);
void  __vstore_pred_packh(bool16,   __short16_ptr,  __int16);
void  __vstore_pred_packh(bool16,   __ushort16_ptr, __uint16);
void  __vstore_pred_packhs1(bool16, __short16_ptr,  __int16);
void  __vstore_pred_packhs1(bool16, __ushort16_ptr, __uint16);
void  __vstore_pred_pack_byte(bool16, __char16_ptr,  __int16);
void  __vstore_pred_pack_byte(bool16, __uchar16_ptr, __uint16);
void  __vstore_pred_packl(bool8,   __int8_ptr,  __long8);
void  __vstore_pred_packl(bool8,   __uint8_ptr, __ulong8);
void  __vstore_pred_packh(bool8,   __int8_ptr,  __long8);
void  __vstore_pred_packh(bool8,   __uint8_ptr, __ulong8);
void  __vstore_pred_packhs1(bool8, __int8_ptr,  __long8);
void  __vstore_pred_packhs1(bool8, __uint8_ptr, __ulong8);
void  __vstore_pred_packl_2src(bool64,   __char64_ptr, __short32,  __short32);
void  __vstore_pred_packl_2src(bool64,   __uchar64_ptr, __ushort32, __ushort32);
void  __vstore_pred_packh_2src(bool64,   __char64_ptr, __short32,  __short32);
void  __vstore_pred_packh_2src(bool64,   __uchar64_ptr, __ushort32, __ushort32);
void  __vstore_pred_packhs1_2src(bool64, __char64_ptr, __short32,  __short32);
void  __vstore_pred_packhs1_2src(bool64, __uchar64_ptr, __ushort32, __ushort32);
void  __vstore_pred_packl_2src(bool32,   __short32_ptr, __int16,   __int16);
void  __vstore_pred_packl_2src(bool32,   __ushort32_ptr, __uint16, __uint16);
void  __vstore_pred_packh_2src(bool32,   __short32_ptr, __int16,   __int16);
void  __vstore_pred_packh_2src(bool32,   __ushort32_ptr, __uint16, __uint16);
void  __vstore_pred_packhs1_2src(bool32, __short32_ptr, __int16,   __int16);
void  __vstore_pred_packhs1_2src(bool32, __ushort32_ptr, __uint16, __uint16);
void  __vstore_pred_pack_byte_2src(bool32, __char32_ptr, __int16,  __int16);
void  __vstore_pred_pack_byte_2src(bool32, __uchar32_ptr, __uint16, __uint16);
void  __vstore_pred_packl_2src(bool16,   __int16_ptr, __long8,  __long8);
void  __vstore_pred_packl_2src(bool16,   __uint16_ptr, __ulong8, __ulong8);
void  __vstore_pred_packh_2src(bool16,   __int16_ptr, __long8,   __long8);
void  __vstore_pred_packh_2src(bool16,   __uint16_ptr, __ulong8, __ulong8);
void  __vstore_pred_packhs1_2src(bool16, __int16_ptr, __long8,   __long8);
void  __vstore_pred_packhs1_2src(bool16, __uint16_ptr, __ulong8, __ulong8);
void  __vstore_pred_packl_long_2src(bool8,   __long8_ptr, __long8, __long8);
void  __vstore_pred_packl_long_2src(bool8,   __ulong8_ptr, __ulong8,__ulong8);
void  __vstore_pred_packh_long_2src(bool8,   __long8_ptr, __long8, __long8);
void  __vstore_pred_packh_long_2src(bool8,   __ulong8_ptr, __ulong8,__ulong8);
void  __vstore_pred_packhs1_long_2src(bool8, __long8_ptr, __long8, __long8);
void  __vstore_pred_packhs1_long_2src(bool8, __ulong8_ptr, __ulong8,__ulong8);
void  __vstore_pred_reverse_bit(bool16, __cshort16_ptr, __cshort16);
void  __vstore_pred_reverse_bit(bool8, __cint8_ptr,   __cint8);
void  __vstore_pred_reverse_bit(bool8, __cfloat8_ptr, __cfloat8);

char2 __add(bool2 a, char2 b, char2 c);
char4 __add(bool4 a, char4 b, char4 c);
char8 __add(bool8 a, char8 b, char8 c);
char16 __add(bool16 a, char16 b, char16 c);
char32 __add(bool32 a, char32 b, char32 c);
char64 __add(bool64 a, char64 b, char64 c);
uchar2 __add(bool2 a, uchar2 b, uchar2 c);
uchar4 __add(bool4 a, uchar4 b, uchar4 c);
uchar8 __add(bool8 a, uchar8 b, uchar8 c);
uchar16 __add(bool16 a, uchar16 b, uchar16 c);
uchar32 __add(bool32 a, uchar32 b, uchar32 c);
uchar64 __add(bool64 a, uchar64 b, uchar64 c);
long2 __add(bool2 a, long2 b, long2 c);
long4 __add(bool4 a, long4 b, long4 c);
long8 __add(bool8 a, long8 b, long8 c);
ulong2 __add(bool2 a, ulong2 b, ulong2 c);
ulong4 __add(bool4 a, ulong4 b, ulong4 c);
ulong8 __add(bool8 a, ulong8 b, ulong8 c);
short2 __add(bool2 a, short2 b, short2 c);
short4 __add(bool4 a, short4 b, short4 c);
short8 __add(bool8 a, short8 b, short8 c);
short16 __add(bool16 a, short16 b, short16 c);
short32 __add(bool32 a, short32 b, short32 c);
ushort2 __add(bool2 a, ushort2 b, ushort2 c);
ushort4 __add(bool4 a, ushort4 b, ushort4 c);
ushort8 __add(bool8 a, ushort8 b, ushort8 c);
ushort16 __add(bool16 a, ushort16 b, ushort16 c);
ushort32 __add(bool32 a, ushort32 b, ushort32 c);
int2 __add(bool2 a, int2 b, int2 c);
int4 __add(bool4 a, int4 b, int4 c);
int8 __add(bool8 a, int8 b, int8 c);
int16 __add(bool16 a, int16 b, int16 c);
uint2 __add(bool2 a, uint2 b, uint2 c);
uint4 __add(bool4 a, uint4 b, uint4 c);
uint8 __add(bool8 a, uint8 b, uint8 c);
uint16 __add(bool16 a, uint16 b, uint16 c);

bool2 __and(bool2, bool2);
bool4 __and(bool4, bool4);
bool8 __and(bool8, bool8);
bool16 __and(bool16, bool16);
bool32 __and(bool32, bool32);
bool64 __and(bool64, bool64);
bool2 __andn(bool2, bool2);
bool4 __andn(bool4, bool4);
bool8 __andn(bool8, bool8);
bool16 __andn(bool16, bool16);
bool32 __andn(bool32, bool32);
bool64 __andn(bool64, bool64);
bool64 __bit_reverse(bool64);

bool2 __cmp_eq_bool(char2, char2);
bool4 __cmp_eq_bool(char4, char4);
bool8 __cmp_eq_bool(char8, char8);
bool16 __cmp_eq_bool(char16, char16);
bool32 __cmp_eq_bool(char32, char32);
bool64 __cmp_eq_bool(char64, char64);
bool2 __cmp_eq_bool(uchar2, uchar2);
bool4 __cmp_eq_bool(uchar4, uchar4);
bool8 __cmp_eq_bool(uchar8, uchar8);
bool16 __cmp_eq_bool(uchar16, uchar16);
bool32 __cmp_eq_bool(uchar32, uchar32);
bool64 __cmp_eq_bool(uchar64, uchar64);
bool2 __cmp_eq_bool(long2, long2);
bool4 __cmp_eq_bool(long4, long4);
bool8 __cmp_eq_bool(long8, long8);
bool2 __cmp_eq_bool(ulong2, ulong2);
bool4 __cmp_eq_bool(ulong4, ulong4);
bool8 __cmp_eq_bool(ulong8, ulong8);
bool2 __cmp_eq_bool(double2, double2);
bool4 __cmp_eq_bool(double4, double4);
bool8 __cmp_eq_bool(double8, double8);
bool2 __cmp_eq_bool(short2, short2);
bool4 __cmp_eq_bool(short4, short4);
bool8 __cmp_eq_bool(short8, short8);
bool16 __cmp_eq_bool(short16, short16);
bool32 __cmp_eq_bool(short32, short32);
bool2 __cmp_eq_bool(ushort2, ushort2);
bool4 __cmp_eq_bool(ushort4, ushort4);
bool8 __cmp_eq_bool(ushort8, ushort8);
bool16 __cmp_eq_bool(ushort16, ushort16);
bool32 __cmp_eq_bool(ushort32, ushort32);
bool2 __cmp_eq_bool(float2, float2);
bool4 __cmp_eq_bool(float4, float4);
bool8 __cmp_eq_bool(float8, float8);
bool16 __cmp_eq_bool(float16, float16);
bool2 __cmp_eq_bool(int2, int2);
bool4 __cmp_eq_bool(int4, int4);
bool8 __cmp_eq_bool(int8, int8);
bool16 __cmp_eq_bool(int16, int16);
bool2 __cmp_eq_bool(uint2, uint2);
bool4 __cmp_eq_bool(uint4, uint4);
bool8 __cmp_eq_bool(uint8, uint8);
bool16 __cmp_eq_bool(uint16, uint16);

bool2 __cmp_ge_bool(char2, char2);
bool4 __cmp_ge_bool(char4, char4);
bool8 __cmp_ge_bool(char8, char8);
bool16 __cmp_ge_bool(char16, char16);
bool32 __cmp_ge_bool(char32, char32);
bool64 __cmp_ge_bool(char64, char64);
bool2 __cmp_ge_bool(long2, long2);
bool4 __cmp_ge_bool(long4, long4);
bool8 __cmp_ge_bool(long8, long8);
bool2 __cmp_ge_bool(short2, short2);
bool4 __cmp_ge_bool(short4, short4);
bool8 __cmp_ge_bool(short8, short8);
bool16 __cmp_ge_bool(short16, short16);
bool32 __cmp_ge_bool(short32, short32);
bool2 __cmp_ge_bool(uchar2, uchar2);
bool4 __cmp_ge_bool(uchar4, uchar4);
bool8 __cmp_ge_bool(uchar8, uchar8);
bool16 __cmp_ge_bool(uchar16, uchar16);
bool32 __cmp_ge_bool(uchar32, uchar32);
bool64 __cmp_ge_bool(uchar64, uchar64);
bool2 __cmp_ge_bool(ulong2, ulong2);
bool4 __cmp_ge_bool(ulong4, ulong4);
bool8 __cmp_ge_bool(ulong8, ulong8);
bool2 __cmp_ge_bool(ushort2, ushort2);
bool4 __cmp_ge_bool(ushort4, ushort4);
bool8 __cmp_ge_bool(ushort8, ushort8);
bool16 __cmp_ge_bool(ushort16, ushort16);
bool32 __cmp_ge_bool(ushort32, ushort32);
bool2 __cmp_ge_bool(uint2, uint2);
bool4 __cmp_ge_bool(uint4, uint4);
bool8 __cmp_ge_bool(uint8, uint8);
bool16 __cmp_ge_bool(uint16, uint16);
bool2 __cmp_ge_bool(int2, int2);
bool4 __cmp_ge_bool(int4, int4);
bool8 __cmp_ge_bool(int8, int8);
bool16 __cmp_ge_bool(int16, int16);

bool2 __cmp_gt_bool(char2, char2);
bool4 __cmp_gt_bool(char4, char4);
bool8 __cmp_gt_bool(char8, char8);
bool16 __cmp_gt_bool(char16, char16);
bool32 __cmp_gt_bool(char32, char32);
bool64 __cmp_gt_bool(char64, char64);
bool2 __cmp_gt_bool(long2, long2);
bool4 __cmp_gt_bool(long4, long4);
bool8 __cmp_gt_bool(long8, long8);
bool2 __cmp_gt_bool(short2, short2);
bool4 __cmp_gt_bool(short4, short4);
bool8 __cmp_gt_bool(short8, short8);
bool16 __cmp_gt_bool(short16, short16);
bool32 __cmp_gt_bool(short32, short32);
bool2 __cmp_gt_bool(uchar2, uchar2);
bool4 __cmp_gt_bool(uchar4, uchar4);
bool8 __cmp_gt_bool(uchar8, uchar8);
bool16 __cmp_gt_bool(uchar16, uchar16);
bool32 __cmp_gt_bool(uchar32, uchar32);
bool64 __cmp_gt_bool(uchar64, uchar64);
bool2 __cmp_gt_bool(ulong2, ulong2);
bool4 __cmp_gt_bool(ulong4, ulong4);
bool8 __cmp_gt_bool(ulong8, ulong8);
bool2 __cmp_gt_bool(ushort2, ushort2);
bool4 __cmp_gt_bool(ushort4, ushort4);
bool8 __cmp_gt_bool(ushort8, ushort8);
bool16 __cmp_gt_bool(ushort16, ushort16);
bool32 __cmp_gt_bool(ushort32, ushort32);
bool2 __cmp_gt_bool(uint2, uint2);
bool4 __cmp_gt_bool(uint4, uint4);
bool8 __cmp_gt_bool(uint8, uint8);
bool16 __cmp_gt_bool(uint16, uint16);
bool2 __cmp_gt_bool(int2, int2);
bool4 __cmp_gt_bool(int4, int4);
bool8 __cmp_gt_bool(int8, int8);
bool16 __cmp_gt_bool(int16, int16);

bool2 __cmp_le_bool(double2, double2);
bool4 __cmp_le_bool(double4, double4);
bool8 __cmp_le_bool(double8, double8);
bool2 __cmp_le_bool(float2, float2);
bool4 __cmp_le_bool(float4, float4);
bool8 __cmp_le_bool(float8, float8);
bool16 __cmp_le_bool(float16, float16);

bool2 __cmp_lt_bool(double2, double2);
bool4 __cmp_lt_bool(double4, double4);
bool8 __cmp_lt_bool(double8, double8);
bool2 __cmp_lt_bool(float2, float2);
bool4 __cmp_lt_bool(float4, float4);
bool8 __cmp_lt_bool(float8, float8);
bool16 __cmp_lt_bool(float16, float16);

bool64 __compress_set_bits_left(bool64);
bool64 __compress_set_bits_right(bool64);
bool64 __decimate(bool64, bool64);
bool64 __duplicate_pred_high(bool64);
bool64 __duplicate_pred_low(bool64);
char2 __expand(bool2);
char4 __expand(bool4);
char8 __expand(bool8);
char16 __expand(bool16);
char32 __expand(bool32);
char64 __expand(bool64);
char2 __gather_set_bits(bool2, char2);
char4 __gather_set_bits(bool4, char4);
char8 __gather_set_bits(bool8, char8);
char16 __gather_set_bits(bool16, char16);
char32 __gather_set_bits(bool32, char32);
char64 __gather_set_bits(bool64, char64);
char2 __gather_unset_bits(bool2, char2);
char4 __gather_unset_bits(bool4, char4);
char8 __gather_unset_bits(bool8, char8);
char16 __gather_unset_bits(bool16, char16);
char32 __gather_unset_bits(bool32, char32);
char64 __gather_unset_bits(bool64, char64);

template <size_t DEPTH>
void __max_circ_bool(char2 a, char2::EQUIV_ACCESS_T<DEPTH>& b, bool2& c);
template <size_t DEPTH>
void __max_circ_bool(char4 a, char4::EQUIV_ACCESS_T<DEPTH>& b, bool4& c);
template <size_t DEPTH>
void __max_circ_bool(char8 a, char8::EQUIV_ACCESS_T<DEPTH>& b, bool8& c);
template <size_t DEPTH>
void __max_circ_bool(char16 a, char16::EQUIV_ACCESS_T<DEPTH>& b, bool16& c);
template <size_t DEPTH>
void __max_circ_bool(char32 a, char32::EQUIV_ACCESS_T<DEPTH>& b, bool32& c);
template <size_t DEPTH>
void __max_circ_bool(char64 a, char64::EQUIV_ACCESS_T<DEPTH>& b, bool64& c);
template <size_t DEPTH>
void __max_circ_bool(short2 a, short2::EQUIV_ACCESS_T<DEPTH>& b, bool2& c);
template <size_t DEPTH>
void __max_circ_bool(short4 a, short4::EQUIV_ACCESS_T<DEPTH>& b, bool4& c);
template <size_t DEPTH>
void __max_circ_bool(short8 a, short8::EQUIV_ACCESS_T<DEPTH>& b, bool8& c);
template <size_t DEPTH>
void __max_circ_bool(short16 a, short16::EQUIV_ACCESS_T<DEPTH>& b, bool16& c);
template <size_t DEPTH>
void __max_circ_bool(short32 a, short32::EQUIV_ACCESS_T<DEPTH>& b, bool32& c);

template <size_t DEPTH>
void __max_index_bool(char2 a, char2::EQUIV_ACCESS_T<DEPTH>& b, bool2& c);
template <size_t DEPTH>
void __max_index_bool(char4 a, char4::EQUIV_ACCESS_T<DEPTH>& b, bool4& c);
template <size_t DEPTH>
void __max_index_bool(char8 a, char8::EQUIV_ACCESS_T<DEPTH>& b, bool8& c);
template <size_t DEPTH>
void __max_index_bool(char16 a, char16::EQUIV_ACCESS_T<DEPTH>& b, bool16& c);
template <size_t DEPTH>
void __max_index_bool(char32 a, char32::EQUIV_ACCESS_T<DEPTH>& b, bool32& c);
template <size_t DEPTH>
void __max_index_bool(char64 a, char64::EQUIV_ACCESS_T<DEPTH>& b, bool64& c);
template <size_t DEPTH>
void __max_index_bool(long2 a, long2::EQUIV_ACCESS_T<DEPTH>& b, bool2& c);
template <size_t DEPTH>
void __max_index_bool(long4 a, long4::EQUIV_ACCESS_T<DEPTH>& b, bool4& c);
template <size_t DEPTH>
void __max_index_bool(long8 a, long8::EQUIV_ACCESS_T<DEPTH>& b, bool8& c);
template <size_t DEPTH>
void __max_index_bool(short2 a, short2::EQUIV_ACCESS_T<DEPTH>& b, bool2& c);
template <size_t DEPTH>
void __max_index_bool(short4 a, short4::EQUIV_ACCESS_T<DEPTH>& b, bool4& c);
template <size_t DEPTH>
void __max_index_bool(short8 a, short8::EQUIV_ACCESS_T<DEPTH>& b, bool8& c);
template <size_t DEPTH>
void __max_index_bool(short16 a, short16::EQUIV_ACCESS_T<DEPTH>& b, bool16& c);
template <size_t DEPTH>
void __max_index_bool(short32 a, short32::EQUIV_ACCESS_T<DEPTH>& b, bool32& c);
template <size_t DEPTH>
void __max_index_bool(int2 a, int2::EQUIV_ACCESS_T<DEPTH>& b, bool2& c);
template <size_t DEPTH>
void __max_index_bool(int4 a, int4::EQUIV_ACCESS_T<DEPTH>& b, bool4& c);
template <size_t DEPTH>
void __max_index_bool(int8 a, int8::EQUIV_ACCESS_T<DEPTH>& b, bool8& c);
template <size_t DEPTH>
void __max_index_bool(int16 a, int16::EQUIV_ACCESS_T<DEPTH>& b, bool16& c);
template <size_t DEPTH>
void __max_index_bool(uchar2 a, uchar2::EQUIV_ACCESS_T<DEPTH>& b, bool2& c);
template <size_t DEPTH>
void __max_index_bool(uchar4 a, uchar4::EQUIV_ACCESS_T<DEPTH>& b, bool4& c);
template <size_t DEPTH>
void __max_index_bool(uchar8 a, uchar8::EQUIV_ACCESS_T<DEPTH>& b, bool8& c);
template <size_t DEPTH>
void __max_index_bool(uchar16 a, uchar16::EQUIV_ACCESS_T<DEPTH>& b, bool16& c);
template <size_t DEPTH>
void __max_index_bool(uchar32 a, uchar32::EQUIV_ACCESS_T<DEPTH>& b, bool32& c);
template <size_t DEPTH>
void __max_index_bool(uchar64 a, uchar64::EQUIV_ACCESS_T<DEPTH>& b, bool64& c);
template <size_t DEPTH>
void __max_index_bool(ulong2 a, ulong2::EQUIV_ACCESS_T<DEPTH>& b, bool2& c);
template <size_t DEPTH>
void __max_index_bool(ulong4 a, ulong4::EQUIV_ACCESS_T<DEPTH>& b, bool4& c);
template <size_t DEPTH>
void __max_index_bool(ulong8 a, ulong8::EQUIV_ACCESS_T<DEPTH>& b, bool8& c);
template <size_t DEPTH>
void __max_index_bool(ushort2 a, ushort2::EQUIV_ACCESS_T<DEPTH>& b, bool2& c);
template <size_t DEPTH>
void __max_index_bool(ushort4 a, ushort4::EQUIV_ACCESS_T<DEPTH>& b, bool4& c);
template <size_t DEPTH>
void __max_index_bool(ushort8 a, ushort8::EQUIV_ACCESS_T<DEPTH>& b, bool8& c);
template <size_t DEPTH>
void __max_index_bool(ushort16 a, ushort16::EQUIV_ACCESS_T<DEPTH>& b, bool16& c);
template <size_t DEPTH>
void __max_index_bool(ushort32 a, ushort32::EQUIV_ACCESS_T<DEPTH>& b, bool32& c);
template <size_t DEPTH>
void __max_index_bool(uint2 a, uint2::EQUIV_ACCESS_T<DEPTH>& b, bool2& c);
template <size_t DEPTH>
void __max_index_bool(uint4 a, uint4::EQUIV_ACCESS_T<DEPTH>& b, bool4& c);
template <size_t DEPTH>
void __max_index_bool(uint8 a, uint8::EQUIV_ACCESS_T<DEPTH>& b, bool8& c);
template <size_t DEPTH>
void __max_index_bool(uint16 a, uint16::EQUIV_ACCESS_T<DEPTH>& b, bool16& c);

template <size_t DEPTH>
void __min_index_bool(char2 a, char2::EQUIV_ACCESS_T<DEPTH>& b, bool2& c);
template <size_t DEPTH>
void __min_index_bool(char4 a, char4::EQUIV_ACCESS_T<DEPTH>& b, bool4& c);
template <size_t DEPTH>
void __min_index_bool(char8 a, char8::EQUIV_ACCESS_T<DEPTH>& b, bool8& c);
template <size_t DEPTH>
void __min_index_bool(char16 a, char16::EQUIV_ACCESS_T<DEPTH>& b, bool16& c);
template <size_t DEPTH>
void __min_index_bool(char32 a, char32::EQUIV_ACCESS_T<DEPTH>& b, bool32& c);
template <size_t DEPTH>
void __min_index_bool(char64 a, char64::EQUIV_ACCESS_T<DEPTH>& b, bool64& c);
template <size_t DEPTH>
void __min_index_bool(long2 a, long2::EQUIV_ACCESS_T<DEPTH>& b, bool2& c);
template <size_t DEPTH>
void __min_index_bool(long4 a, long4::EQUIV_ACCESS_T<DEPTH>& b, bool4& c);
template <size_t DEPTH>
void __min_index_bool(long8 a, long8::EQUIV_ACCESS_T<DEPTH>& b, bool8& c);
template <size_t DEPTH>
void __min_index_bool(short2 a, short2::EQUIV_ACCESS_T<DEPTH>& b, bool2& c);
template <size_t DEPTH>
void __min_index_bool(short4 a, short4::EQUIV_ACCESS_T<DEPTH>& b, bool4& c);
template <size_t DEPTH>
void __min_index_bool(short8 a, short8::EQUIV_ACCESS_T<DEPTH>& b, bool8& c);
template <size_t DEPTH>
void __min_index_bool(short16 a, short16::EQUIV_ACCESS_T<DEPTH>& b, bool16& c);
template <size_t DEPTH>
void __min_index_bool(short32 a, short32::EQUIV_ACCESS_T<DEPTH>& b, bool32& c);
template <size_t DEPTH>
void __min_index_bool(int2 a, int2::EQUIV_ACCESS_T<DEPTH>& b, bool2& c);
template <size_t DEPTH>
void __min_index_bool(int4 a, int4::EQUIV_ACCESS_T<DEPTH>& b, bool4& c);
template <size_t DEPTH>
void __min_index_bool(int8 a, int8::EQUIV_ACCESS_T<DEPTH>& b, bool8& c);
template <size_t DEPTH>
void __min_index_bool(int16 a, int16::EQUIV_ACCESS_T<DEPTH>& b, bool16& c);
template <size_t DEPTH>
void __min_index_bool(uchar2 a, uchar2::EQUIV_ACCESS_T<DEPTH>& b, bool2& c);
template <size_t DEPTH>
void __min_index_bool(uchar4 a, uchar4::EQUIV_ACCESS_T<DEPTH>& b, bool4& c);
template <size_t DEPTH>
void __min_index_bool(uchar8 a, uchar8::EQUIV_ACCESS_T<DEPTH>& b, bool8& c);
template <size_t DEPTH>
void __min_index_bool(uchar16 a, uchar16::EQUIV_ACCESS_T<DEPTH>& b, bool16& c);
template <size_t DEPTH>
void __min_index_bool(uchar32 a, uchar32::EQUIV_ACCESS_T<DEPTH>& b, bool32& c);
template <size_t DEPTH>
void __min_index_bool(uchar64 a, uchar64::EQUIV_ACCESS_T<DEPTH>& b, bool64& c);
template <size_t DEPTH>
void __min_index_bool(ulong2 a, ulong2::EQUIV_ACCESS_T<DEPTH>& b, bool2& c);
template <size_t DEPTH>
void __min_index_bool(ulong4 a, ulong4::EQUIV_ACCESS_T<DEPTH>& b, bool4& c);
template <size_t DEPTH>
void __min_index_bool(ulong8 a, ulong8::EQUIV_ACCESS_T<DEPTH>& b, bool8& c);
template <size_t DEPTH>
void __min_index_bool(ushort2 a, ushort2::EQUIV_ACCESS_T<DEPTH>& b, bool2& c);
template <size_t DEPTH>
void __min_index_bool(ushort4 a, ushort4::EQUIV_ACCESS_T<DEPTH>& b, bool4& c);
template <size_t DEPTH>
void __min_index_bool(ushort8 a, ushort8::EQUIV_ACCESS_T<DEPTH>& b, bool8& c);
template <size_t DEPTH>
void __min_index_bool(ushort16 a, ushort16::EQUIV_ACCESS_T<DEPTH>& b, bool16& c);
template <size_t DEPTH>
void __min_index_bool(ushort32 a, ushort32::EQUIV_ACCESS_T<DEPTH>& b, bool32& c);
template <size_t DEPTH>
void __min_index_bool(uint2 a, uint2::EQUIV_ACCESS_T<DEPTH>& b, bool2& c);
template <size_t DEPTH>
void __min_index_bool(uint4 a, uint4::EQUIV_ACCESS_T<DEPTH>& b, bool4& c);
template <size_t DEPTH>
void __min_index_bool(uint8 a, uint8::EQUIV_ACCESS_T<DEPTH>& b, bool8& c);
template <size_t DEPTH>
void __min_index_bool(uint16 a, uint16::EQUIV_ACCESS_T<DEPTH>& b, bool16& c);

bool2 __nand(bool2, bool2);
bool4 __nand(bool4, bool4);
bool8 __nand(bool8, bool8);
bool16 __nand(bool16, bool16);
bool32 __nand(bool32, bool32);
bool64 __nand(bool64, bool64);
bool2 __negate(bool2);
bool4 __negate(bool4);
bool8 __negate(bool8);
bool16 __negate(bool16);
bool32 __negate(bool32);
bool64 __negate(bool64);
bool2 __nor(bool2, bool2);
bool4 __nor(bool4, bool4);
bool8 __nor(bool8, bool8);
bool16 __nor(bool16, bool16);
bool32 __nor(bool32, bool32);
bool64 __nor(bool64, bool64);
bool2 __or(bool2, bool2);
bool4 __or(bool4, bool4);
bool8 __or(bool8, bool8);
bool16 __or(bool16, bool16);
bool32 __or(bool32, bool32);
bool64 __or(bool64, bool64);
bool2 __orn(bool2, bool2);
bool4 __orn(bool4, bool4);
bool8 __orn(bool8, bool8);
bool16 __orn(bool16, bool16);
bool32 __orn(bool32, bool32);
bool64 __orn(bool64, bool64);
uint16 __parallel_pack_dup_16way(bool16);
uint16 __parallel_pack_dup_8way(bool8);
uint32_t __popcount(bool2);
uint32_t __popcount(bool4);
uint32_t __popcount(bool8);
uint32_t __popcount(bool16);
uint32_t __popcount(bool32);
uint32_t __popcount(bool64);
void __popcount_gather(bool2, uint32_t&, bool2&);
void __popcount_gather(bool4, uint32_t&, bool4&);
void __popcount_gather(bool8, uint32_t&, bool8&);
void __popcount_gather(bool16, uint32_t&, bool16&);
void __popcount_gather(bool32, uint32_t&, bool32&);
void __popcount_gather(bool64, uint32_t&, bool64&);
uint32_t __rightmost_bit_detect(bool2);
uint32_t __rightmost_bit_detect(bool4);
uint32_t __rightmost_bit_detect(bool8);
uint32_t __rightmost_bit_detect(bool16);
uint32_t __rightmost_bit_detect(bool32);
uint32_t __rightmost_bit_detect(bool64);
char2 __scatter_set_bits(bool2, char2);
char4 __scatter_set_bits(bool4, char4);
char8 __scatter_set_bits(bool8, char8);
char16 __scatter_set_bits(bool16, char16);
char32 __scatter_set_bits(bool32, char32);
char64 __scatter_set_bits(bool64, char64);
char2 __scatter_unset_bits(bool2, char2);
char4 __scatter_unset_bits(bool4, char4);
char8 __scatter_unset_bits(bool8, char8);
char16 __scatter_unset_bits(bool16, char16);
char32 __scatter_unset_bits(bool32, char32);
char64 __scatter_unset_bits(bool64, char64);
char2 __select(bool2, char2, char2);
char4 __select(bool4, char4, char4);
char8 __select(bool8, char8, char8);
char16 __select(bool16, char16, char16);
char32 __select(bool32, char32, char32);
char64 __select(bool64, char64, char64);
short2 __select(bool2, short2, short2);
short4 __select(bool4, short4, short4);
short8 __select(bool8, short8, short8);
short16 __select(bool16, short16, short16);
short32 __select(bool32, short32, short32);
int2 __select(bool2, int2, int2);
int4 __select(bool4, int4, int4);
int8 __select(bool8, int8, int8);
int16 __select(bool16, int16, int16);
long2 __select(bool2, long2, long2);
long4 __select(bool4, long4, long4);
long8 __select(bool8, long8, long8);
cchar2 __select(bool2, cchar2, cchar2);
cchar4 __select(bool4, cchar4, cchar4);
cchar8 __select(bool8, cchar8, cchar8);
cchar16 __select(bool16, cchar16, cchar16);
cchar32 __select(bool32, cchar32, cchar32);
uchar2 __select(bool2, uchar2, uchar2);
uchar4 __select(bool4, uchar4, uchar4);
uchar8 __select(bool8, uchar8, uchar8);
uchar16 __select(bool16, uchar16, uchar16);
uchar32 __select(bool32, uchar32, uchar32);
uchar64 __select(bool64, uchar64, uchar64);
cshort2 __select(bool2, cshort2, cshort2);
cshort4 __select(bool4, cshort4, cshort4);
cshort8 __select(bool8, cshort8, cshort8);
cshort16 __select(bool16, cshort16, cshort16);
ushort2 __select(bool2, ushort2, ushort2);
ushort4 __select(bool4, ushort4, ushort4);
ushort8 __select(bool8, ushort8, ushort8);
ushort16 __select(bool16, ushort16, ushort16);
ushort32 __select(bool32, ushort32, ushort32);
float2 __select(bool2, float2, float2);
float4 __select(bool4, float4, float4);
float8 __select(bool8, float8, float8);
float16 __select(bool16, float16, float16);
cfloat2 __select(bool2, cfloat2, cfloat2);
cfloat4 __select(bool4, cfloat4, cfloat4);
cfloat8 __select(bool8, cfloat8, cfloat8);
cint2 __select(bool2, cint2, cint2);
cint4 __select(bool4, cint4, cint4);
cint8 __select(bool8, cint8, cint8);
uint2 __select(bool2, uint2, uint2);
uint4 __select(bool4, uint4, uint4);
uint8 __select(bool8, uint8, uint8);
uint16 __select(bool16, uint16, uint16);
double2 __select(bool2, double2, double2);
double4 __select(bool4, double4, double4);
double8 __select(bool8, double8, double8);
cdouble2 __select(bool2, cdouble2, cdouble2);
cdouble4 __select(bool4, cdouble4, cdouble4);
clong2 __select(bool2, clong2, clong2);
clong4 __select(bool4, clong4, clong4);
ulong2 __select(bool2, ulong2, ulong2);
ulong4 __select(bool4, ulong4, ulong4);
ulong8 __select(bool8, ulong8, ulong8);
float2 __select(bool2 a, float2 b, uint2 c);
float4 __select(bool4 a, float4 b, uint4 c);
float8 __select(bool8 a, float8 b, uint8 c);
float16 __select(bool16 a, float16 b, uint16 c);
cfloat2 __select(bool2 a, cfloat2 b, uint4 c);
cfloat4 __select(bool4 a, cfloat4 b, uint8 c);
cfloat8 __select(bool8 a, cfloat8 b, uint16 c);
cint2 __select(bool2 a, cint2 b, uint4 c);
cint4 __select(bool4 a, cint4 b, uint8 c);
cint8 __select(bool8 a, cint8 b, uint16 c);
int2 __select(bool2 a, int2 b, uint2 c);
int4 __select(bool4 a, int4 b, uint4 c);
int8 __select(bool8 a, int8 b, uint8 c);
int16 __select(bool16 a, int16 b, uint16 c);

char2 __shift_left_conditional(bool2, char2, char2);
char4 __shift_left_conditional(bool4, char4, char4);
char8 __shift_left_conditional(bool8, char8, char8);
char16 __shift_left_conditional(bool16, char16, char16);
char32 __shift_left_conditional(bool32, char32, char32);
char64 __shift_left_conditional(bool64, char64, char64);
uchar2 __shift_left_conditional(bool2, uchar2, uchar2);
uchar4 __shift_left_conditional(bool4, uchar4, uchar4);
uchar8 __shift_left_conditional(bool8, uchar8, uchar8);
uchar16 __shift_left_conditional(bool16, uchar16, uchar16);
uchar32 __shift_left_conditional(bool32, uchar32, uchar32);
uchar64 __shift_left_conditional(bool64, uchar64, uchar64);
short2 __shift_left_conditional(bool2, short2, short2);
short4 __shift_left_conditional(bool4, short4, short4);
short8 __shift_left_conditional(bool8, short8, short8);
short16 __shift_left_conditional(bool16, short16, short16);
short32 __shift_left_conditional(bool32, short32, short32);
ushort2 __shift_left_conditional(bool2, ushort2, ushort2);
ushort4 __shift_left_conditional(bool4, ushort4, ushort4);
ushort8 __shift_left_conditional(bool8, ushort8, ushort8);
ushort16 __shift_left_conditional(bool16, ushort16, ushort16);
ushort32 __shift_left_conditional(bool32, ushort32, ushort32);
int2 __shift_left_conditional(bool2, int2, int2);
int4 __shift_left_conditional(bool4, int4, int4);
int8 __shift_left_conditional(bool8, int8, int8);
int16 __shift_left_conditional(bool16, int16, int16);
uint2 __shift_left_conditional(bool2, uint2, uint2);
uint4 __shift_left_conditional(bool4, uint4, uint4);
uint8 __shift_left_conditional(bool8, uint8, uint8);
uint16 __shift_left_conditional(bool16, uint16, uint16);
bool2 __xnor(bool2, bool2);
bool4 __xnor(bool4, bool4);
bool8 __xnor(bool8, bool8);
bool16 __xnor(bool16, bool16);
bool32 __xnor(bool32, bool32);
bool64 __xnor(bool64, bool64);
bool2 __xor(bool2, bool2);
bool4 __xor(bool4, bool4);
bool8 __xor(bool8, bool8);
bool16 __xor(bool16, bool16);
bool32 __xor(bool32, bool32);
bool64 __xor(bool64, bool64);

template<size_t DEPTH>
void __max_circ_pred(char2 a, char2::EQUIV_ACCESS_T<DEPTH>& b, __vpred& c)
{
	vreg_t b_ref = vreg_t(b);
	vpred_t c_ref = vpred_t(c);
	_vcmaxpb_vvp((vreg_t)a, b_ref, c_ref);
	b = char2(b_ref);
	uint8_t mask_bits = 2 * (sizeof(int8_t));
	vpred_t ret_pred_tmp;
	_maskb_kp(mask_bits, ret_pred_tmp);
	_and_ppp(ret_pred_tmp, c_ref, c_ref);
	c = __vpred(c_ref);
}
template<size_t DEPTH>
void __max_circ_pred(char4 a, char4::EQUIV_ACCESS_T<DEPTH>& b, __vpred& c)
{
	vreg_t b_ref = vreg_t(b);
	vpred_t c_ref = vpred_t(c);
	_vcmaxpb_vvp((vreg_t)a, b_ref, c_ref);
	b = char4(b_ref);
	uint8_t mask_bits = 4 * (sizeof(int8_t));
	vpred_t ret_pred_tmp;
	_maskb_kp(mask_bits, ret_pred_tmp);
	_and_ppp(ret_pred_tmp, c_ref, c_ref);
	c = __vpred(c_ref);
}
template<size_t DEPTH>
void __max_circ_pred(char8 a, char8::EQUIV_ACCESS_T<DEPTH>& b, __vpred& c)
{
	vreg_t b_ref = vreg_t(b);
	vpred_t c_ref = vpred_t(c);
	_vcmaxpb_vvp((vreg_t)a, b_ref, c_ref);
	b = char8(b_ref);
	uint8_t mask_bits = 8 * (sizeof(int8_t));
	vpred_t ret_pred_tmp;
	_maskb_kp(mask_bits, ret_pred_tmp);
	_and_ppp(ret_pred_tmp, c_ref, c_ref);
	c = __vpred(c_ref);
}
template<size_t DEPTH>
void __max_circ_pred(char16 a, char16::EQUIV_ACCESS_T<DEPTH>& b, __vpred& c)
{
	vreg_t b_ref = vreg_t(b);
	vpred_t c_ref = vpred_t(c);
	_vcmaxpb_vvp((vreg_t)a, b_ref, c_ref);
	b = char16(b_ref);
	uint8_t mask_bits = 16 * (sizeof(int8_t));
	vpred_t ret_pred_tmp;
	_maskb_kp(mask_bits, ret_pred_tmp);
	_and_ppp(ret_pred_tmp, c_ref, c_ref);
	c = __vpred(c_ref);
}
template<size_t DEPTH>
void __max_circ_pred(char32 a, char32::EQUIV_ACCESS_T<DEPTH>& b, __vpred& c)
{
	vreg_t b_ref = vreg_t(b);
	vpred_t c_ref = vpred_t(c);
	_vcmaxpb_vvp((vreg_t)a, b_ref, c_ref);
	b = char32(b_ref);
	uint8_t mask_bits = 32 * (sizeof(int8_t));
	vpred_t ret_pred_tmp;
	_maskb_kp(mask_bits, ret_pred_tmp);
	_and_ppp(ret_pred_tmp, c_ref, c_ref);
	c = __vpred(c_ref);
}
template<size_t DEPTH>
void __max_circ_pred(char64 a, char64::EQUIV_ACCESS_T<DEPTH>& b, __vpred& c)
{
	vreg_t b_ref = vreg_t(b);
	vpred_t c_ref = vpred_t(c);
	_vcmaxpb_vvp((vreg_t)a, b_ref, c_ref);
	b = char64(b_ref);
	uint8_t mask_bits = 64 * (sizeof(int8_t));
	vpred_t ret_pred_tmp;
	_maskb_kp(mask_bits, ret_pred_tmp);
	_and_ppp(ret_pred_tmp, c_ref, c_ref);
	c = __vpred(c_ref);
}

template<size_t DEPTH>
void __max_circ_pred(short2 a, short2::EQUIV_ACCESS_T<DEPTH>& b, __vpred& c)
{
	vreg_t b_ref = vreg_t(b);
	vpred_t c_ref = vpred_t(c);
	_vcmaxph_vvp((vreg_t)a, b_ref, c_ref);
	b = short2(b_ref);
	uint8_t mask_bits = 2 * (sizeof(int16_t));
	vpred_t ret_pred_tmp;
	_maskb_kp(mask_bits, ret_pred_tmp);
	_and_ppp(ret_pred_tmp, c_ref, c_ref);
	c = __vpred(c_ref);
}
template<size_t DEPTH>
void __max_circ_pred(short4 a, short4::EQUIV_ACCESS_T<DEPTH>& b, __vpred& c)
{
	vreg_t b_ref = vreg_t(b);
	vpred_t c_ref = vpred_t(c);
	_vcmaxph_vvp((vreg_t)a, b_ref, c_ref);
	b = short4(b_ref);
	uint8_t mask_bits = 4 * (sizeof(int16_t));
	vpred_t ret_pred_tmp;
	_maskb_kp(mask_bits, ret_pred_tmp);
	_and_ppp(ret_pred_tmp, c_ref, c_ref);
	c = __vpred(c_ref);
}
template<size_t DEPTH>
void __max_circ_pred(short8 a, short8::EQUIV_ACCESS_T<DEPTH>& b, __vpred& c)
{
	vreg_t b_ref = vreg_t(b);
	vpred_t c_ref = vpred_t(c);
	_vcmaxph_vvp((vreg_t)a, b_ref, c_ref);
	b = short8(b_ref);
	uint8_t mask_bits = 8 * (sizeof(int16_t));
	vpred_t ret_pred_tmp;
	_maskb_kp(mask_bits, ret_pred_tmp);
	_and_ppp(ret_pred_tmp, c_ref, c_ref);
	c = __vpred(c_ref);
}
template<size_t DEPTH>
void __max_circ_pred(short16 a, short16::EQUIV_ACCESS_T<DEPTH>& b, __vpred& c)
{
	vreg_t b_ref = vreg_t(b);
	vpred_t c_ref = vpred_t(c);
	_vcmaxph_vvp((vreg_t)a, b_ref, c_ref);
	b = short16(b_ref);
	uint8_t mask_bits = 16 * (sizeof(int16_t));
	vpred_t ret_pred_tmp;
	_maskb_kp(mask_bits, ret_pred_tmp);
	_and_ppp(ret_pred_tmp, c_ref, c_ref);
	c = __vpred(c_ref);
}
template<size_t DEPTH>
void __max_circ_pred(short32 a, short32::EQUIV_ACCESS_T<DEPTH>& b, __vpred& c)
{
	vreg_t b_ref = vreg_t(b);
	vpred_t c_ref = vpred_t(c);
	_vcmaxph_vvp((vreg_t)a, b_ref, c_ref);
	b = short32(b_ref);
	uint8_t mask_bits = 32 * (sizeof(int16_t));
	vpred_t ret_pred_tmp;
	_maskb_kp(mask_bits, ret_pred_tmp);
	_and_ppp(ret_pred_tmp, c_ref, c_ref);
	c = __vpred(c_ref);
}
template<size_t DEPTH>
void __max_index(char2 a, char2::EQUIV_ACCESS_T<DEPTH>& b, __vpred& c)
{
	vreg_t b_ref = vreg_t(b);
	vpred_t c_ref = vpred_t(c);
	_vmaxpb_vvp((vreg_t)a, b_ref, c_ref);
	b = char2(b_ref);
	uint8_t mask_bits = 2 * (sizeof(int8_t));
	vpred_t ret_pred_tmp;
	_maskb_kp(mask_bits, ret_pred_tmp);
	_and_ppp(ret_pred_tmp, c_ref, c_ref);
	c = __vpred(c_ref);
}
template<size_t DEPTH>
void __max_index(char4 a, char4::EQUIV_ACCESS_T<DEPTH>& b, __vpred& c)
{
	vreg_t b_ref = vreg_t(b);
	vpred_t c_ref = vpred_t(c);
	_vmaxpb_vvp((vreg_t)a, b_ref, c_ref);
	b = char4(b_ref);
	uint8_t mask_bits = 4 * (sizeof(int8_t));
	vpred_t ret_pred_tmp;
	_maskb_kp(mask_bits, ret_pred_tmp);
	_and_ppp(ret_pred_tmp, c_ref, c_ref);
	c = __vpred(c_ref);
}
template<size_t DEPTH>
void __max_index(char8 a, char8::EQUIV_ACCESS_T<DEPTH>& b, __vpred& c)
{
	vreg_t b_ref = vreg_t(b);
	vpred_t c_ref = vpred_t(c);
	_vmaxpb_vvp((vreg_t)a, b_ref, c_ref);
	b = char8(b_ref);
	uint8_t mask_bits = 8 * (sizeof(int8_t));
	vpred_t ret_pred_tmp;
	_maskb_kp(mask_bits, ret_pred_tmp);
	_and_ppp(ret_pred_tmp, c_ref, c_ref);
	c = __vpred(c_ref);
}
template<size_t DEPTH>
void __max_index(char16 a, char16::EQUIV_ACCESS_T<DEPTH>& b, __vpred& c)
{
	vreg_t b_ref = vreg_t(b);
	vpred_t c_ref = vpred_t(c);
	_vmaxpb_vvp((vreg_t)a, b_ref, c_ref);
	b = char16(b_ref);
	uint8_t mask_bits = 16 * (sizeof(int8_t));
	vpred_t ret_pred_tmp;
	_maskb_kp(mask_bits, ret_pred_tmp);
	_and_ppp(ret_pred_tmp, c_ref, c_ref);
	c = __vpred(c_ref);
}
template<size_t DEPTH>
void __max_index(char32 a, char32::EQUIV_ACCESS_T<DEPTH>& b, __vpred& c)
{
	vreg_t b_ref = vreg_t(b);
	vpred_t c_ref = vpred_t(c);
	_vmaxpb_vvp((vreg_t)a, b_ref, c_ref);
	b = char32(b_ref);
	uint8_t mask_bits = 32 * (sizeof(int8_t));
	vpred_t ret_pred_tmp;
	_maskb_kp(mask_bits, ret_pred_tmp);
	_and_ppp(ret_pred_tmp, c_ref, c_ref);
	c = __vpred(c_ref);
}
template<size_t DEPTH>
void __max_index(char64 a, char64::EQUIV_ACCESS_T<DEPTH>& b, __vpred& c)
{
	vreg_t b_ref = vreg_t(b);
	vpred_t c_ref = vpred_t(c);
	_vmaxpb_vvp((vreg_t)a, b_ref, c_ref);
	b = char64(b_ref);
	uint8_t mask_bits = 64 * (sizeof(int8_t));
	vpred_t ret_pred_tmp;
	_maskb_kp(mask_bits, ret_pred_tmp);
	_and_ppp(ret_pred_tmp, c_ref, c_ref);
	c = __vpred(c_ref);
}
template<size_t DEPTH>
void __max_index(long2 a, long2::EQUIV_ACCESS_T<DEPTH>& b, __vpred& c)
{
	vreg_t b_ref = vreg_t(b);
	vpred_t c_ref = vpred_t(c);
	_vmaxpd_vvp((vreg_t)a, b_ref, c_ref);
	b = long2(b_ref);
	uint8_t mask_bits = 2 * (sizeof(int64_t));
	vpred_t ret_pred_tmp;
	_maskb_kp(mask_bits, ret_pred_tmp);
	_and_ppp(ret_pred_tmp, c_ref, c_ref);
	c = __vpred(c_ref);
}
template<size_t DEPTH>
void __max_index(long4 a, long4::EQUIV_ACCESS_T<DEPTH>& b, __vpred& c)
{
	vreg_t b_ref = vreg_t(b);
	vpred_t c_ref = vpred_t(c);
	_vmaxpd_vvp((vreg_t)a, b_ref, c_ref);
	b = long4(b_ref);
	uint8_t mask_bits = 4 * (sizeof(int64_t));
	vpred_t ret_pred_tmp;
	_maskb_kp(mask_bits, ret_pred_tmp);
	_and_ppp(ret_pred_tmp, c_ref, c_ref);
	c = __vpred(c_ref);
}
template<size_t DEPTH>
void __max_index(long8 a, long8::EQUIV_ACCESS_T<DEPTH>& b, __vpred& c)
{
	vreg_t b_ref = vreg_t(b);
	vpred_t c_ref = vpred_t(c);
	_vmaxpd_vvp((vreg_t)a, b_ref, c_ref);
	b = long8(b_ref);
	uint8_t mask_bits = 8 * (sizeof(int64_t));
	vpred_t ret_pred_tmp;
	_maskb_kp(mask_bits, ret_pred_tmp);
	_and_ppp(ret_pred_tmp, c_ref, c_ref);
	c = __vpred(c_ref);
}

template<size_t DEPTH>
void __max_index(short2 a, short2::EQUIV_ACCESS_T<DEPTH>& b, __vpred& c)
{
	vreg_t b_ref = vreg_t(b);
	vpred_t c_ref = vpred_t(c);
	_vmaxph_vvp((vreg_t)a, b_ref, c_ref);
	b = short2(b_ref);
	uint8_t mask_bits = 2 * (sizeof(int16_t));
	vpred_t ret_pred_tmp;
	_maskb_kp(mask_bits, ret_pred_tmp);
	_and_ppp(ret_pred_tmp, c_ref, c_ref);
	c = __vpred(c_ref);
}
template<size_t DEPTH>
void __max_index(short4 a, short4::EQUIV_ACCESS_T<DEPTH>& b, __vpred& c)
{
	vreg_t b_ref = vreg_t(b);
	vpred_t c_ref = vpred_t(c);
	_vmaxph_vvp((vreg_t)a, b_ref, c_ref);
	b = short4(b_ref);
	uint8_t mask_bits = 4 * (sizeof(int16_t));
	vpred_t ret_pred_tmp;
	_maskb_kp(mask_bits, ret_pred_tmp);
	_and_ppp(ret_pred_tmp, c_ref, c_ref);
	c = __vpred(c_ref);
}
template<size_t DEPTH>
void __max_index(short8 a, short8::EQUIV_ACCESS_T<DEPTH>& b, __vpred& c)
{
	vreg_t b_ref = vreg_t(b);
	vpred_t c_ref = vpred_t(c);
	_vmaxph_vvp((vreg_t)a, b_ref, c_ref);
	b = short8(b_ref);
	uint8_t mask_bits = 8 * (sizeof(int16_t));
	vpred_t ret_pred_tmp;
	_maskb_kp(mask_bits, ret_pred_tmp);
	_and_ppp(ret_pred_tmp, c_ref, c_ref);
	c = __vpred(c_ref);
}
template<size_t DEPTH>
void __max_index(short16 a, short16::EQUIV_ACCESS_T<DEPTH>& b, __vpred& c)
{
	vreg_t b_ref = vreg_t(b);
	vpred_t c_ref = vpred_t(c);
	_vmaxph_vvp((vreg_t)a, b_ref, c_ref);
	b = short16(b_ref);
	uint8_t mask_bits = 16 * (sizeof(int16_t));
	vpred_t ret_pred_tmp;
	_maskb_kp(mask_bits, ret_pred_tmp);
	_and_ppp(ret_pred_tmp, c_ref, c_ref);
	c = __vpred(c_ref);
}
template<size_t DEPTH>
void __max_index(short32 a, short32::EQUIV_ACCESS_T<DEPTH>& b, __vpred& c)
{
	vreg_t b_ref = vreg_t(b);
	vpred_t c_ref = vpred_t(c);
	_vmaxph_vvp((vreg_t)a, b_ref, c_ref);
	b = short32(b_ref);
	uint8_t mask_bits = 32 * (sizeof(int16_t));
	vpred_t ret_pred_tmp;
	_maskb_kp(mask_bits, ret_pred_tmp);
	_and_ppp(ret_pred_tmp, c_ref, c_ref);
	c = __vpred(c_ref);
}
template<size_t DEPTH>
void __max_index(int2 a, int2::EQUIV_ACCESS_T<DEPTH>& b, __vpred& c)
{
	vreg_t b_ref = vreg_t(b);
	vpred_t c_ref = vpred_t(c);
	_vmaxpw_vvp((vreg_t)a, b_ref, c_ref);
	b = int2(b_ref);
	uint8_t mask_bits = 2 * (sizeof(int32_t));
	vpred_t ret_pred_tmp;
	_maskb_kp(mask_bits, ret_pred_tmp);
	_and_ppp(ret_pred_tmp, c_ref, c_ref);
	c = __vpred(c_ref);
}
template<size_t DEPTH>
void __max_index(int4 a, int4::EQUIV_ACCESS_T<DEPTH>& b, __vpred& c)
{
	vreg_t b_ref = vreg_t(b);
	vpred_t c_ref = vpred_t(c);
	_vmaxpw_vvp((vreg_t)a, b_ref, c_ref);
	b = int4(b_ref);
	uint8_t mask_bits = 4 * (sizeof(int32_t));
	vpred_t ret_pred_tmp;
	_maskb_kp(mask_bits, ret_pred_tmp);
	_and_ppp(ret_pred_tmp, c_ref, c_ref);
	c = __vpred(c_ref);
}
template<size_t DEPTH>
void __max_index(int8 a, int8::EQUIV_ACCESS_T<DEPTH>& b, __vpred& c)
{
	vreg_t b_ref = vreg_t(b);
	vpred_t c_ref = vpred_t(c);
	_vmaxpw_vvp((vreg_t)a, b_ref, c_ref);
	b = int8(b_ref);
	uint8_t mask_bits = 8 * (sizeof(int32_t));
	vpred_t ret_pred_tmp;
	_maskb_kp(mask_bits, ret_pred_tmp);
	_and_ppp(ret_pred_tmp, c_ref, c_ref);
	c = __vpred(c_ref);
}
template<size_t DEPTH>
void __max_index(int16 a, int16::EQUIV_ACCESS_T<DEPTH>& b, __vpred& c)
{
	vreg_t b_ref = vreg_t(b);
	vpred_t c_ref = vpred_t(c);
	_vmaxpw_vvp((vreg_t)a, b_ref, c_ref);
	b = int16(b_ref);
	uint8_t mask_bits = 16 * (sizeof(int32_t));
	vpred_t ret_pred_tmp;
	_maskb_kp(mask_bits, ret_pred_tmp);
	_and_ppp(ret_pred_tmp, c_ref, c_ref);
	c = __vpred(c_ref);
}
template<size_t DEPTH>
void __max_index(uchar2 a, uchar2::EQUIV_ACCESS_T<DEPTH>& b, __vpred& c)
{
	vreg_t b_ref = vreg_t(b);
	vpred_t c_ref = vpred_t(c);
	_vmaxupb_vvp((vreg_t)a, b_ref, c_ref);
	b = uchar2(b_ref);
	uint8_t mask_bits = 2 * (sizeof(uint8_t));
	vpred_t ret_pred_tmp;
	_maskb_kp(mask_bits, ret_pred_tmp);
	_and_ppp(ret_pred_tmp, c_ref, c_ref);
	c = __vpred(c_ref);
}
template<size_t DEPTH>
void __max_index(uchar4 a, uchar4::EQUIV_ACCESS_T<DEPTH>& b, __vpred& c)
{
	vreg_t b_ref = vreg_t(b);
	vpred_t c_ref = vpred_t(c);
	_vmaxupb_vvp((vreg_t)a, b_ref, c_ref);
	b = uchar4(b_ref);
	uint8_t mask_bits = 4 * (sizeof(uint8_t));
	vpred_t ret_pred_tmp;
	_maskb_kp(mask_bits, ret_pred_tmp);
	_and_ppp(ret_pred_tmp, c_ref, c_ref);
	c = __vpred(c_ref);
}
template<size_t DEPTH>
void __max_index(uchar8 a, uchar8::EQUIV_ACCESS_T<DEPTH>& b, __vpred& c)
{
	vreg_t b_ref = vreg_t(b);
	vpred_t c_ref = vpred_t(c);
	_vmaxupb_vvp((vreg_t)a, b_ref, c_ref);
	b = uchar8(b_ref);
	uint8_t mask_bits = 8 * (sizeof(uint8_t));
	vpred_t ret_pred_tmp;
	_maskb_kp(mask_bits, ret_pred_tmp);
	_and_ppp(ret_pred_tmp, c_ref, c_ref);
	c = __vpred(c_ref);
}
template<size_t DEPTH>
void __max_index(uchar16 a, uchar16::EQUIV_ACCESS_T<DEPTH>& b, __vpred& c)
{
	vreg_t b_ref = vreg_t(b);
	vpred_t c_ref = vpred_t(c);
	_vmaxupb_vvp((vreg_t)a, b_ref, c_ref);
	b = uchar16(b_ref);
	uint8_t mask_bits = 16 * (sizeof(uint8_t));
	vpred_t ret_pred_tmp;
	_maskb_kp(mask_bits, ret_pred_tmp);
	_and_ppp(ret_pred_tmp, c_ref, c_ref);
	c = __vpred(c_ref);
}
template<size_t DEPTH>
void __max_index(uchar32 a, uchar32::EQUIV_ACCESS_T<DEPTH>& b, __vpred& c)
{
	vreg_t b_ref = vreg_t(b);
	vpred_t c_ref = vpred_t(c);
	_vmaxupb_vvp((vreg_t)a, b_ref, c_ref);
	b = uchar32(b_ref);
	uint8_t mask_bits = 32 * (sizeof(uint8_t));
	vpred_t ret_pred_tmp;
	_maskb_kp(mask_bits, ret_pred_tmp);
	_and_ppp(ret_pred_tmp, c_ref, c_ref);
	c = __vpred(c_ref);
}
template<size_t DEPTH>
void __max_index(uchar64 a, uchar64::EQUIV_ACCESS_T<DEPTH>& b, __vpred& c)
{
	vreg_t b_ref = vreg_t(b);
	vpred_t c_ref = vpred_t(c);
	_vmaxupb_vvp((vreg_t)a, b_ref, c_ref);
	b = uchar64(b_ref);
	uint8_t mask_bits = 64 * (sizeof(uint8_t));
	vpred_t ret_pred_tmp;
	_maskb_kp(mask_bits, ret_pred_tmp);
	_and_ppp(ret_pred_tmp, c_ref, c_ref);
	c = __vpred(c_ref);
}
template<size_t DEPTH>
void __max_index(ulong2 a, ulong2::EQUIV_ACCESS_T<DEPTH>& b, __vpred& c)
{
	vreg_t b_ref = vreg_t(b);
	vpred_t c_ref = vpred_t(c);
	_vmaxupd_vvp((vreg_t)a, b_ref, c_ref);
	b = ulong2(b_ref);
	uint8_t mask_bits = 2 * (sizeof(uint64_t));
	vpred_t ret_pred_tmp;
	_maskb_kp(mask_bits, ret_pred_tmp);
	_and_ppp(ret_pred_tmp, c_ref, c_ref);
	c = __vpred(c_ref);
}
template<size_t DEPTH>
void __max_index(ulong4 a, ulong4::EQUIV_ACCESS_T<DEPTH>& b, __vpred& c)
{
	vreg_t b_ref = vreg_t(b);
	vpred_t c_ref = vpred_t(c);
	_vmaxupd_vvp((vreg_t)a, b_ref, c_ref);
	b = ulong4(b_ref);
	uint8_t mask_bits = 4 * (sizeof(uint64_t));
	vpred_t ret_pred_tmp;
	_maskb_kp(mask_bits, ret_pred_tmp);
	_and_ppp(ret_pred_tmp, c_ref, c_ref);
	c = __vpred(c_ref);
}
template<size_t DEPTH>
void __max_index(ulong8 a, ulong8::EQUIV_ACCESS_T<DEPTH>& b, __vpred& c)
{
	vreg_t b_ref = vreg_t(b);
	vpred_t c_ref = vpred_t(c);
	_vmaxupd_vvp((vreg_t)a, b_ref, c_ref);
	b = ulong8(b_ref);
	uint8_t mask_bits = 8 * (sizeof(uint64_t));
	vpred_t ret_pred_tmp;
	_maskb_kp(mask_bits, ret_pred_tmp);
	_and_ppp(ret_pred_tmp, c_ref, c_ref);
	c = __vpred(c_ref);
}
template<size_t DEPTH>
void __max_index(ushort2 a, ushort2::EQUIV_ACCESS_T<DEPTH>& b, __vpred& c)
{
	vreg_t b_ref = vreg_t(b);
	vpred_t c_ref = vpred_t(c);
	_vmaxuph_vvp((vreg_t)a, b_ref, c_ref);
	b = ushort2(b_ref);
	uint8_t mask_bits = 2 * (sizeof(uint16_t));
	vpred_t ret_pred_tmp;
	_maskb_kp(mask_bits, ret_pred_tmp);
	_and_ppp(ret_pred_tmp, c_ref, c_ref);
	c = __vpred(c_ref);
}
template<size_t DEPTH>
void __max_index(ushort4 a, ushort4::EQUIV_ACCESS_T<DEPTH>& b, __vpred& c)
{
	vreg_t b_ref = vreg_t(b);
	vpred_t c_ref = vpred_t(c);
	_vmaxuph_vvp((vreg_t)a, b_ref, c_ref);
	b = ushort4(b_ref);
	uint8_t mask_bits = 4 * (sizeof(uint16_t));
	vpred_t ret_pred_tmp;
	_maskb_kp(mask_bits, ret_pred_tmp);
	_and_ppp(ret_pred_tmp, c_ref, c_ref);
	c = __vpred(c_ref);
}
template<size_t DEPTH>
void __max_index(ushort8 a, ushort8::EQUIV_ACCESS_T<DEPTH>& b, __vpred& c)
{
	vreg_t b_ref = vreg_t(b);
	vpred_t c_ref = vpred_t(c);
	_vmaxuph_vvp((vreg_t)a, b_ref, c_ref);
	b = ushort8(b_ref);
	uint8_t mask_bits = 8 * (sizeof(uint16_t));
	vpred_t ret_pred_tmp;
	_maskb_kp(mask_bits, ret_pred_tmp);
	_and_ppp(ret_pred_tmp, c_ref, c_ref);
	c = __vpred(c_ref);
}
template<size_t DEPTH>
void __max_index(ushort16 a, ushort16::EQUIV_ACCESS_T<DEPTH>& b, __vpred& c)
{
	vreg_t b_ref = vreg_t(b);
	vpred_t c_ref = vpred_t(c);
	_vmaxuph_vvp((vreg_t)a, b_ref, c_ref);
	b = ushort16(b_ref);
	uint8_t mask_bits = 16 * (sizeof(uint16_t));
	vpred_t ret_pred_tmp;
	_maskb_kp(mask_bits, ret_pred_tmp);
	_and_ppp(ret_pred_tmp, c_ref, c_ref);
	c = __vpred(c_ref);
}
template<size_t DEPTH>
void __max_index(ushort32 a, ushort32::EQUIV_ACCESS_T<DEPTH>& b, __vpred& c)
{
	vreg_t b_ref = vreg_t(b);
	vpred_t c_ref = vpred_t(c);
	_vmaxuph_vvp((vreg_t)a, b_ref, c_ref);
	b = ushort32(b_ref);
	uint8_t mask_bits = 32 * (sizeof(uint16_t));
	vpred_t ret_pred_tmp;
	_maskb_kp(mask_bits, ret_pred_tmp);
	_and_ppp(ret_pred_tmp, c_ref, c_ref);
	c = __vpred(c_ref);
}
template<size_t DEPTH>
void __max_index(uint2 a, uint2::EQUIV_ACCESS_T<DEPTH>& b, __vpred& c)
{
	vreg_t b_ref = vreg_t(b);
	vpred_t c_ref = vpred_t(c);
	_vmaxupw_vvp((vreg_t)a, b_ref, c_ref);
	b = uint2(b_ref);
	uint8_t mask_bits = 2 * (sizeof(uint32_t));
	vpred_t ret_pred_tmp;
	_maskb_kp(mask_bits, ret_pred_tmp);
	_and_ppp(ret_pred_tmp, c_ref, c_ref);
	c = __vpred(c_ref);
}
template<size_t DEPTH>
void __max_index(uint4 a, uint4::EQUIV_ACCESS_T<DEPTH>& b, __vpred& c)
{
	vreg_t b_ref = vreg_t(b);
	vpred_t c_ref = vpred_t(c);
	_vmaxupw_vvp((vreg_t)a, b_ref, c_ref);
	b = uint4(b_ref);
	uint8_t mask_bits = 4 * (sizeof(uint32_t));
	vpred_t ret_pred_tmp;
	_maskb_kp(mask_bits, ret_pred_tmp);
	_and_ppp(ret_pred_tmp, c_ref, c_ref);
	c = __vpred(c_ref);
}
template<size_t DEPTH>
void __max_index(uint8 a, uint8::EQUIV_ACCESS_T<DEPTH>& b, __vpred& c)
{
	vreg_t b_ref = vreg_t(b);
	vpred_t c_ref = vpred_t(c);
	_vmaxupw_vvp((vreg_t)a, b_ref, c_ref);
	b = uint8(b_ref);
	uint8_t mask_bits = 8 * (sizeof(uint32_t));
	vpred_t ret_pred_tmp;
	_maskb_kp(mask_bits, ret_pred_tmp);
	_and_ppp(ret_pred_tmp, c_ref, c_ref);
	c = __vpred(c_ref);
}
template<size_t DEPTH>
void __max_index(uint16 a, uint16::EQUIV_ACCESS_T<DEPTH>& b, __vpred& c)
{
	vreg_t b_ref = vreg_t(b);
	vpred_t c_ref = vpred_t(c);
	_vmaxupw_vvp((vreg_t)a, b_ref, c_ref);
	b = uint16(b_ref);
	uint8_t mask_bits = 16 * (sizeof(uint32_t));
	vpred_t ret_pred_tmp;
	_maskb_kp(mask_bits, ret_pred_tmp);
	_and_ppp(ret_pred_tmp, c_ref, c_ref);
	c = __vpred(c_ref);
}
template<size_t DEPTH>
void __min_index(char2 a, char2::EQUIV_ACCESS_T<DEPTH>& b, __vpred& c)
{
	vreg_t b_ref = vreg_t(b);
	vpred_t c_ref = vpred_t(c);
	_vminpb_vvp((vreg_t)a, b_ref, c_ref);
	b = char2(b_ref);
	uint8_t mask_bits = 2 * (sizeof(int8_t));
	vpred_t ret_pred_tmp;
	_maskb_kp(mask_bits, ret_pred_tmp);
	_and_ppp(ret_pred_tmp, c_ref, c_ref);
	c = __vpred(c_ref);
}
template<size_t DEPTH>
void __min_index(char4 a, char4::EQUIV_ACCESS_T<DEPTH>& b, __vpred& c)
{
	vreg_t b_ref = vreg_t(b);
	vpred_t c_ref = vpred_t(c);
	_vminpb_vvp((vreg_t)a, b_ref, c_ref);
	b = char4(b_ref);
	uint8_t mask_bits = 4 * (sizeof(int8_t));
	vpred_t ret_pred_tmp;
	_maskb_kp(mask_bits, ret_pred_tmp);
	_and_ppp(ret_pred_tmp, c_ref, c_ref);
	c = __vpred(c_ref);
}
template<size_t DEPTH>
void __min_index(char8 a, char8::EQUIV_ACCESS_T<DEPTH>& b, __vpred& c)
{
	vreg_t b_ref = vreg_t(b);
	vpred_t c_ref = vpred_t(c);
	_vminpb_vvp((vreg_t)a, b_ref, c_ref);
	b = char8(b_ref);
	uint8_t mask_bits = 8 * (sizeof(int8_t));
	vpred_t ret_pred_tmp;
	_maskb_kp(mask_bits, ret_pred_tmp);
	_and_ppp(ret_pred_tmp, c_ref, c_ref);
	c = __vpred(c_ref);
}
template<size_t DEPTH>
void __min_index(char16 a, char16::EQUIV_ACCESS_T<DEPTH>& b, __vpred& c)
{
	vreg_t b_ref = vreg_t(b);
	vpred_t c_ref = vpred_t(c);
	_vminpb_vvp((vreg_t)a, b_ref, c_ref);
	b = char16(b_ref);
	uint8_t mask_bits = 16 * (sizeof(int8_t));
	vpred_t ret_pred_tmp;
	_maskb_kp(mask_bits, ret_pred_tmp);
	_and_ppp(ret_pred_tmp, c_ref, c_ref);
	c = __vpred(c_ref);
}
template<size_t DEPTH>
void __min_index(char32 a, char32::EQUIV_ACCESS_T<DEPTH>& b, __vpred& c)
{
	vreg_t b_ref = vreg_t(b);
	vpred_t c_ref = vpred_t(c);
	_vminpb_vvp((vreg_t)a, b_ref, c_ref);
	b = char32(b_ref);
	uint8_t mask_bits = 32 * (sizeof(int8_t));
	vpred_t ret_pred_tmp;
	_maskb_kp(mask_bits, ret_pred_tmp);
	_and_ppp(ret_pred_tmp, c_ref, c_ref);
	c = __vpred(c_ref);
}
template<size_t DEPTH>
void __min_index(char64 a, char64::EQUIV_ACCESS_T<DEPTH>& b, __vpred& c)
{
	vreg_t b_ref = vreg_t(b);
	vpred_t c_ref = vpred_t(c);
	_vminpb_vvp((vreg_t)a, b_ref, c_ref);
	b = char64(b_ref);
	uint8_t mask_bits = 64 * (sizeof(int8_t));
	vpred_t ret_pred_tmp;
	_maskb_kp(mask_bits, ret_pred_tmp);
	_and_ppp(ret_pred_tmp, c_ref, c_ref);
	c = __vpred(c_ref);
}
template<size_t DEPTH>
void __min_index(long2 a, long2::EQUIV_ACCESS_T<DEPTH>& b, __vpred& c)
{
	vreg_t b_ref = vreg_t(b);
	vpred_t c_ref = vpred_t(c);
	_vminpd_vvp((vreg_t)a, b_ref, c_ref);
	b = long2(b_ref);
	uint8_t mask_bits = 2 * (sizeof(int64_t));
	vpred_t ret_pred_tmp;
	_maskb_kp(mask_bits, ret_pred_tmp);
	_and_ppp(ret_pred_tmp, c_ref, c_ref);
	c = __vpred(c_ref);
}
template<size_t DEPTH>
void __min_index(long4 a, long4::EQUIV_ACCESS_T<DEPTH>& b, __vpred& c)
{
	vreg_t b_ref = vreg_t(b);
	vpred_t c_ref = vpred_t(c);
	_vminpd_vvp((vreg_t)a, b_ref, c_ref);
	b = long4(b_ref);
	uint8_t mask_bits = 4 * (sizeof(int64_t));
	vpred_t ret_pred_tmp;
	_maskb_kp(mask_bits, ret_pred_tmp);
	_and_ppp(ret_pred_tmp, c_ref, c_ref);
	c = __vpred(c_ref);
}
template<size_t DEPTH>
void __min_index(long8 a, long8::EQUIV_ACCESS_T<DEPTH>& b, __vpred& c)
{
	vreg_t b_ref = vreg_t(b);
	vpred_t c_ref = vpred_t(c);
	_vminpd_vvp((vreg_t)a, b_ref, c_ref);
	b = long8(b_ref);
	uint8_t mask_bits = 8 * (sizeof(int64_t));
	vpred_t ret_pred_tmp;
	_maskb_kp(mask_bits, ret_pred_tmp);
	_and_ppp(ret_pred_tmp, c_ref, c_ref);
	c = __vpred(c_ref);
}

template<size_t DEPTH>
void __min_index(short2 a, short2::EQUIV_ACCESS_T<DEPTH>& b, __vpred& c)
{
	vreg_t b_ref = vreg_t(b);
	vpred_t c_ref = vpred_t(c);
	_vminph_vvp((vreg_t)a, b_ref, c_ref);
	b = short2(b_ref);
	uint8_t mask_bits = 2 * (sizeof(int16_t));
	vpred_t ret_pred_tmp;
	_maskb_kp(mask_bits, ret_pred_tmp);
	_and_ppp(ret_pred_tmp, c_ref, c_ref);
	c = __vpred(c_ref);
}
template<size_t DEPTH>
void __min_index(short4 a, short4::EQUIV_ACCESS_T<DEPTH>& b, __vpred& c)
{
	vreg_t b_ref = vreg_t(b);
	vpred_t c_ref = vpred_t(c);
	_vminph_vvp((vreg_t)a, b_ref, c_ref);
	b = short4(b_ref);
	uint8_t mask_bits = 4 * (sizeof(int16_t));
	vpred_t ret_pred_tmp;
	_maskb_kp(mask_bits, ret_pred_tmp);
	_and_ppp(ret_pred_tmp, c_ref, c_ref);
	c = __vpred(c_ref);
}
template<size_t DEPTH>
void __min_index(short8 a, short8::EQUIV_ACCESS_T<DEPTH>& b, __vpred& c)
{
	vreg_t b_ref = vreg_t(b);
	vpred_t c_ref = vpred_t(c);
	_vminph_vvp((vreg_t)a, b_ref, c_ref);
	b = short8(b_ref);
	uint8_t mask_bits = 8 * (sizeof(int16_t));
	vpred_t ret_pred_tmp;
	_maskb_kp(mask_bits, ret_pred_tmp);
	_and_ppp(ret_pred_tmp, c_ref, c_ref);
	c = __vpred(c_ref);
}
template<size_t DEPTH>
void __min_index(short16 a, short16::EQUIV_ACCESS_T<DEPTH>& b, __vpred& c)
{
	vreg_t b_ref = vreg_t(b);
	vpred_t c_ref = vpred_t(c);
	_vminph_vvp((vreg_t)a, b_ref, c_ref);
	b = short16(b_ref);
	uint8_t mask_bits = 16 * (sizeof(int16_t));
	vpred_t ret_pred_tmp;
	_maskb_kp(mask_bits, ret_pred_tmp);
	_and_ppp(ret_pred_tmp, c_ref, c_ref);
	c = __vpred(c_ref);
}
template<size_t DEPTH>
void __min_index(short32 a, short32::EQUIV_ACCESS_T<DEPTH>& b, __vpred& c)
{
	vreg_t b_ref = vreg_t(b);
	vpred_t c_ref = vpred_t(c);
	_vminph_vvp((vreg_t)a, b_ref, c_ref);
	b = short32(b_ref);
	uint8_t mask_bits = 32 * (sizeof(int16_t));
	vpred_t ret_pred_tmp;
	_maskb_kp(mask_bits, ret_pred_tmp);
	_and_ppp(ret_pred_tmp, c_ref, c_ref);
	c = __vpred(c_ref);
}
template<size_t DEPTH>
void __min_index(int2 a, int2::EQUIV_ACCESS_T<DEPTH>& b, __vpred& c)
{
	vreg_t b_ref = vreg_t(b);
	vpred_t c_ref = vpred_t(c);
	_vminpw_vvp((vreg_t)a, b_ref, c_ref);
	b = int2(b_ref);
	uint8_t mask_bits = 2 * (sizeof(int32_t));
	vpred_t ret_pred_tmp;
	_maskb_kp(mask_bits, ret_pred_tmp);
	_and_ppp(ret_pred_tmp, c_ref, c_ref);
	c = __vpred(c_ref);
}
template<size_t DEPTH>
void __min_index(int4 a, int4::EQUIV_ACCESS_T<DEPTH>& b, __vpred& c)
{
	vreg_t b_ref = vreg_t(b);
	vpred_t c_ref = vpred_t(c);
	_vminpw_vvp((vreg_t)a, b_ref, c_ref);
	b = int4(b_ref);
	uint8_t mask_bits = 4 * (sizeof(int32_t));
	vpred_t ret_pred_tmp;
	_maskb_kp(mask_bits, ret_pred_tmp);
	_and_ppp(ret_pred_tmp, c_ref, c_ref);
	c = __vpred(c_ref);
}
template<size_t DEPTH>
void __min_index(int8 a, int8::EQUIV_ACCESS_T<DEPTH>& b, __vpred& c)
{
	vreg_t b_ref = vreg_t(b);
	vpred_t c_ref = vpred_t(c);
	_vminpw_vvp((vreg_t)a, b_ref, c_ref);
	b = int8(b_ref);
	uint8_t mask_bits = 8 * (sizeof(int32_t));
	vpred_t ret_pred_tmp;
	_maskb_kp(mask_bits, ret_pred_tmp);
	_and_ppp(ret_pred_tmp, c_ref, c_ref);
	c = __vpred(c_ref);
}
template<size_t DEPTH>
void __min_index(int16 a, int16::EQUIV_ACCESS_T<DEPTH>& b, __vpred& c)
{
	vreg_t b_ref = vreg_t(b);
	vpred_t c_ref = vpred_t(c);
	_vminpw_vvp((vreg_t)a, b_ref, c_ref);
	b = int16(b_ref);
	uint8_t mask_bits = 16 * (sizeof(int32_t));
	vpred_t ret_pred_tmp;
	_maskb_kp(mask_bits, ret_pred_tmp);
	_and_ppp(ret_pred_tmp, c_ref, c_ref);
	c = __vpred(c_ref);
}
template<size_t DEPTH>
void __min_index(uchar2 a, uchar2::EQUIV_ACCESS_T<DEPTH>& b, __vpred& c)
{
	vreg_t b_ref = vreg_t(b);
	vpred_t c_ref = vpred_t(c);
	_vminupb_vvp((vreg_t)a, b_ref, c_ref);
	b = uchar2(b_ref);
	uint8_t mask_bits = 2 * (sizeof(uint8_t));
	vpred_t ret_pred_tmp;
	_maskb_kp(mask_bits, ret_pred_tmp);
	_and_ppp(ret_pred_tmp, c_ref, c_ref);
	c = __vpred(c_ref);
}
template<size_t DEPTH>
void __min_index(uchar4 a, uchar4::EQUIV_ACCESS_T<DEPTH>& b, __vpred& c)
{
	vreg_t b_ref = vreg_t(b);
	vpred_t c_ref = vpred_t(c);
	_vminupb_vvp((vreg_t)a, b_ref, c_ref);
	b = uchar4(b_ref);
	uint8_t mask_bits = 4 * (sizeof(uint8_t));
	vpred_t ret_pred_tmp;
	_maskb_kp(mask_bits, ret_pred_tmp);
	_and_ppp(ret_pred_tmp, c_ref, c_ref);
	c = __vpred(c_ref);
}
template<size_t DEPTH>
void __min_index(uchar8 a, uchar8::EQUIV_ACCESS_T<DEPTH>& b, __vpred& c)
{
	vreg_t b_ref = vreg_t(b);
	vpred_t c_ref = vpred_t(c);
	_vminupb_vvp((vreg_t)a, b_ref, c_ref);
	b = uchar8(b_ref);
	uint8_t mask_bits = 8 * (sizeof(uint8_t));
	vpred_t ret_pred_tmp;
	_maskb_kp(mask_bits, ret_pred_tmp);
	_and_ppp(ret_pred_tmp, c_ref, c_ref);
	c = __vpred(c_ref);
}
template<size_t DEPTH>
void __min_index(uchar16 a, uchar16::EQUIV_ACCESS_T<DEPTH>& b, __vpred& c)
{
	vreg_t b_ref = vreg_t(b);
	vpred_t c_ref = vpred_t(c);
	_vminupb_vvp((vreg_t)a, b_ref, c_ref);
	b = uchar16(b_ref);
	uint8_t mask_bits = 16 * (sizeof(uint8_t));
	vpred_t ret_pred_tmp;
	_maskb_kp(mask_bits, ret_pred_tmp);
	_and_ppp(ret_pred_tmp, c_ref, c_ref);
	c = __vpred(c_ref);
}
template<size_t DEPTH>
void __min_index(uchar32 a, uchar32::EQUIV_ACCESS_T<DEPTH>& b, __vpred& c)
{
	vreg_t b_ref = vreg_t(b);
	vpred_t c_ref = vpred_t(c);
	_vminupb_vvp((vreg_t)a, b_ref, c_ref);
	b = uchar32(b_ref);
	uint8_t mask_bits = 32 * (sizeof(uint8_t));
	vpred_t ret_pred_tmp;
	_maskb_kp(mask_bits, ret_pred_tmp);
	_and_ppp(ret_pred_tmp, c_ref, c_ref);
	c = __vpred(c_ref);
}
template<size_t DEPTH>
void __min_index(uchar64 a, uchar64::EQUIV_ACCESS_T<DEPTH>& b, __vpred& c)
{
	vreg_t b_ref = vreg_t(b);
	vpred_t c_ref = vpred_t(c);
	_vminupb_vvp((vreg_t)a, b_ref, c_ref);
	b = uchar64(b_ref);
	uint8_t mask_bits = 64 * (sizeof(uint8_t));
	vpred_t ret_pred_tmp;
	_maskb_kp(mask_bits, ret_pred_tmp);
	_and_ppp(ret_pred_tmp, c_ref, c_ref);
	c = __vpred(c_ref);
}
template<size_t DEPTH>
void __min_index(ulong2 a, ulong2::EQUIV_ACCESS_T<DEPTH>& b, __vpred& c)
{
	vreg_t b_ref = vreg_t(b);
	vpred_t c_ref = vpred_t(c);
	_vminupd_vvp((vreg_t)a, b_ref, c_ref);
	b = ulong2(b_ref);
	uint8_t mask_bits = 2 * (sizeof(uint64_t));
	vpred_t ret_pred_tmp;
	_maskb_kp(mask_bits, ret_pred_tmp);
	_and_ppp(ret_pred_tmp, c_ref, c_ref);
	c = __vpred(c_ref);
}
template<size_t DEPTH>
void __min_index(ulong4 a, ulong4::EQUIV_ACCESS_T<DEPTH>& b, __vpred& c)
{
	vreg_t b_ref = vreg_t(b);
	vpred_t c_ref = vpred_t(c);
	_vminupd_vvp((vreg_t)a, b_ref, c_ref);
	b = ulong4(b_ref);
	uint8_t mask_bits = 4 * (sizeof(uint64_t));
	vpred_t ret_pred_tmp;
	_maskb_kp(mask_bits, ret_pred_tmp);
	_and_ppp(ret_pred_tmp, c_ref, c_ref);
	c = __vpred(c_ref);
}
template<size_t DEPTH>
void __min_index(ulong8 a, ulong8::EQUIV_ACCESS_T<DEPTH>& b, __vpred& c)
{
	vreg_t b_ref = vreg_t(b);
	vpred_t c_ref = vpred_t(c);
	_vminupd_vvp((vreg_t)a, b_ref, c_ref);
	b = ulong8(b_ref);
	uint8_t mask_bits = 8 * (sizeof(uint64_t));
	vpred_t ret_pred_tmp;
	_maskb_kp(mask_bits, ret_pred_tmp);
	_and_ppp(ret_pred_tmp, c_ref, c_ref);
	c = __vpred(c_ref);
}

template<size_t DEPTH>
void __min_index(ushort2 a, ushort2::EQUIV_ACCESS_T<DEPTH>& b, __vpred& c)
{
	vreg_t b_ref = vreg_t(b);
	vpred_t c_ref = vpred_t(c);
	_vminuph_vvp((vreg_t)a, b_ref, c_ref);
	b = ushort2(b_ref);
	uint8_t mask_bits = 2 * (sizeof(uint16_t));
	vpred_t ret_pred_tmp;
	_maskb_kp(mask_bits, ret_pred_tmp);
	_and_ppp(ret_pred_tmp, c_ref, c_ref);
	c = __vpred(c_ref);
}
template<size_t DEPTH>
void __min_index(ushort4 a, ushort4::EQUIV_ACCESS_T<DEPTH>& b, __vpred& c)
{
	vreg_t b_ref = vreg_t(b);
	vpred_t c_ref = vpred_t(c);
	_vminuph_vvp((vreg_t)a, b_ref, c_ref);
	b = ushort4(b_ref);
	uint8_t mask_bits = 4 * (sizeof(uint16_t));
	vpred_t ret_pred_tmp;
	_maskb_kp(mask_bits, ret_pred_tmp);
	_and_ppp(ret_pred_tmp, c_ref, c_ref);
	c = __vpred(c_ref);
}
template<size_t DEPTH>
void __min_index(ushort8 a, ushort8::EQUIV_ACCESS_T<DEPTH>& b, __vpred& c)
{
	vreg_t b_ref = vreg_t(b);
	vpred_t c_ref = vpred_t(c);
	_vminuph_vvp((vreg_t)a, b_ref, c_ref);
	b = ushort8(b_ref);
	uint8_t mask_bits = 8 * (sizeof(uint16_t));
	vpred_t ret_pred_tmp;
	_maskb_kp(mask_bits, ret_pred_tmp);
	_and_ppp(ret_pred_tmp, c_ref, c_ref);
	c = __vpred(c_ref);
}
template<size_t DEPTH>
void __min_index(ushort16 a, ushort16::EQUIV_ACCESS_T<DEPTH>& b, __vpred& c)
{
	vreg_t b_ref = vreg_t(b);
	vpred_t c_ref = vpred_t(c);
	_vminuph_vvp((vreg_t)a, b_ref, c_ref);
	b = ushort16(b_ref);
	uint8_t mask_bits = 16 * (sizeof(uint16_t));
	vpred_t ret_pred_tmp;
	_maskb_kp(mask_bits, ret_pred_tmp);
	_and_ppp(ret_pred_tmp, c_ref, c_ref);
	c = __vpred(c_ref);
}
template<size_t DEPTH>
void __min_index(ushort32 a, ushort32::EQUIV_ACCESS_T<DEPTH>& b, __vpred& c)
{
	vreg_t b_ref = vreg_t(b);
	vpred_t c_ref = vpred_t(c);
	_vminuph_vvp((vreg_t)a, b_ref, c_ref);
	b = ushort32(b_ref);
	uint8_t mask_bits = 32 * (sizeof(uint16_t));
	vpred_t ret_pred_tmp;
	_maskb_kp(mask_bits, ret_pred_tmp);
	_and_ppp(ret_pred_tmp, c_ref, c_ref);
	c = __vpred(c_ref);
}
template<size_t DEPTH>
void __min_index(uint2 a, uint2::EQUIV_ACCESS_T<DEPTH>& b, __vpred& c)
{
	vreg_t b_ref = vreg_t(b);
	vpred_t c_ref = vpred_t(c);
	_vminupw_vvp((vreg_t)a, b_ref, c_ref);
	b = uint2(b_ref);
	uint8_t mask_bits = 2 * (sizeof(uint32_t));
	vpred_t ret_pred_tmp;
	_maskb_kp(mask_bits, ret_pred_tmp);
	_and_ppp(ret_pred_tmp, c_ref, c_ref);
	c = __vpred(c_ref);
}
template<size_t DEPTH>
void __min_index(uint4 a, uint4::EQUIV_ACCESS_T<DEPTH>& b, __vpred& c)
{
	vreg_t b_ref = vreg_t(b);
	vpred_t c_ref = vpred_t(c);
	_vminupw_vvp((vreg_t)a, b_ref, c_ref);
	b = uint4(b_ref);
	uint8_t mask_bits = 4 * (sizeof(uint32_t));
	vpred_t ret_pred_tmp;
	_maskb_kp(mask_bits, ret_pred_tmp);
	_and_ppp(ret_pred_tmp, c_ref, c_ref);
	c = __vpred(c_ref);
}
template<size_t DEPTH>
void __min_index(uint8 a, uint8::EQUIV_ACCESS_T<DEPTH>& b, __vpred& c)
{
	vreg_t b_ref = vreg_t(b);
	vpred_t c_ref = vpred_t(c);
	_vminupw_vvp((vreg_t)a, b_ref, c_ref);
	b = uint8(b_ref);
	uint8_t mask_bits = 8 * (sizeof(uint32_t));
	vpred_t ret_pred_tmp;
	_maskb_kp(mask_bits, ret_pred_tmp);
	_and_ppp(ret_pred_tmp, c_ref, c_ref);
	c = __vpred(c_ref);
}
template<size_t DEPTH>
void __min_index(uint16 a, uint16::EQUIV_ACCESS_T<DEPTH>& b, __vpred& c)
{
	vreg_t b_ref = vreg_t(b);
	vpred_t c_ref = vpred_t(c);
	_vminupw_vvp((vreg_t)a, b_ref, c_ref);
	b = uint16(b_ref);
	uint8_t mask_bits = 16 * (sizeof(uint32_t));
	vpred_t ret_pred_tmp;
	_maskb_kp(mask_bits, ret_pred_tmp);
	_and_ppp(ret_pred_tmp, c_ref, c_ref);
	c = __vpred(c_ref);
}

#define __max_index_pred __max_index
#define __min_index_pred __min_index

#define FACTOR(type) (sizeof(type) == 16 ? 4 : \
                      sizeof(type) ==  8 ? 3 : \
                      sizeof(type) ==  4 ? 2 : \
                      sizeof(type) ==  2 ? 1 : 0)

#define BMINMAX(name,ftype,type,len) \
    template<size_t DEPTH> \
    void name##_bool(type##len a, type##len::EQUIV_ACCESS_T<DEPTH>& b, bool##len& c)\
    { vpred_t pred; _bitxpnd_pkp(c, FACTOR(ftype), pred);             \
      __vpred p_ref = __vpred(pred); name##_pred(a, b, p_ref);        \
      pred = vpred_t(p_ref); _bitpack_pkp(pred, FACTOR(ftype), pred); \
      c = bool##len(pred); \
      return; }

/*-----------------------------------------------------------------------------
* ID: __max_circ_bool()
*----------------------------------------------------------------------------*/
BMINMAX(__max_circ,uint8_t,char,2)
BMINMAX(__max_circ,uint8_t,char,4)
BMINMAX(__max_circ,uint8_t,char,8)
BMINMAX(__max_circ,uint8_t,char,16)
BMINMAX(__max_circ,uint8_t,char,32)
BMINMAX(__max_circ,uint8_t,char,64)

BMINMAX(__max_circ,uint16_t,short,2)
BMINMAX(__max_circ,uint16_t,short,4)
BMINMAX(__max_circ,uint16_t,short,8)
BMINMAX(__max_circ,uint16_t,short,16)
BMINMAX(__max_circ,uint16_t,short,32)

/*-----------------------------------------------------------------------------
* ID: __max_index_bool()
*----------------------------------------------------------------------------*/
BMINMAX(__max_index,uint8_t,char,2)
BMINMAX(__max_index,uint8_t,char,4)
BMINMAX(__max_index,uint8_t,char,8)
BMINMAX(__max_index,uint8_t,char,16)
BMINMAX(__max_index,uint8_t,char,32)
BMINMAX(__max_index,uint8_t,char,64)

BMINMAX(__max_index,uint8_t,uchar,2)
BMINMAX(__max_index,uint8_t,uchar,4)
BMINMAX(__max_index,uint8_t,uchar,8)
BMINMAX(__max_index,uint8_t,uchar,16)
BMINMAX(__max_index,uint8_t,uchar,32)
BMINMAX(__max_index,uint8_t,uchar,64)

BMINMAX(__max_index,uint16_t,short,2)
BMINMAX(__max_index,uint16_t,short,4)
BMINMAX(__max_index,uint16_t,short,8)
BMINMAX(__max_index,uint16_t,short,16)
BMINMAX(__max_index,uint16_t,short,32)

BMINMAX(__max_index,uint16_t,ushort,2)
BMINMAX(__max_index,uint16_t,ushort,4)
BMINMAX(__max_index,uint16_t,ushort,8)
BMINMAX(__max_index,uint16_t,ushort,16)
BMINMAX(__max_index,uint16_t,ushort,32)

BMINMAX(__max_index,uint32_t,int,2)
BMINMAX(__max_index,uint32_t,int,4)
BMINMAX(__max_index,uint32_t,int,8)
BMINMAX(__max_index,uint32_t,int,16)

BMINMAX(__max_index,uint32_t,uint,2)
BMINMAX(__max_index,uint32_t,uint,4)
BMINMAX(__max_index,uint32_t,uint,8)
BMINMAX(__max_index,uint32_t,uint,16)

BMINMAX(__max_index,uint64_t,long,2)
BMINMAX(__max_index,uint64_t,long,4)
BMINMAX(__max_index,uint64_t,long,8)

BMINMAX(__max_index,uint64_t,ulong,2)
BMINMAX(__max_index,uint64_t,ulong,4)
BMINMAX(__max_index,uint64_t,ulong,8)

/*-----------------------------------------------------------------------------
* ID: __min_index_bool()
*----------------------------------------------------------------------------*/
BMINMAX(__min_index,uint8_t,char,2)
BMINMAX(__min_index,uint8_t,char,4)
BMINMAX(__min_index,uint8_t,char,8)
BMINMAX(__min_index,uint8_t,char,16)
BMINMAX(__min_index,uint8_t,char,32)
BMINMAX(__min_index,uint8_t,char,64)

BMINMAX(__min_index,uint8_t,uchar,2)
BMINMAX(__min_index,uint8_t,uchar,4)
BMINMAX(__min_index,uint8_t,uchar,8)
BMINMAX(__min_index,uint8_t,uchar,16)
BMINMAX(__min_index,uint8_t,uchar,32)
BMINMAX(__min_index,uint8_t,uchar,64)

BMINMAX(__min_index,uint16_t,short,2)
BMINMAX(__min_index,uint16_t,short,4)
BMINMAX(__min_index,uint16_t,short,8)
BMINMAX(__min_index,uint16_t,short,16)
BMINMAX(__min_index,uint16_t,short,32)

BMINMAX(__min_index,uint16_t,ushort,2)
BMINMAX(__min_index,uint16_t,ushort,4)
BMINMAX(__min_index,uint16_t,ushort,8)
BMINMAX(__min_index,uint16_t,ushort,16)
BMINMAX(__min_index,uint16_t,ushort,32)

BMINMAX(__min_index,uint32_t,int,2)
BMINMAX(__min_index,uint32_t,int,4)
BMINMAX(__min_index,uint32_t,int,8)
BMINMAX(__min_index,uint32_t,int,16)

BMINMAX(__min_index,uint32_t,uint,2)
BMINMAX(__min_index,uint32_t,uint,4)
BMINMAX(__min_index,uint32_t,uint,8)
BMINMAX(__min_index,uint32_t,uint,16)

BMINMAX(__min_index,uint64_t,long,2)
BMINMAX(__min_index,uint64_t,long,4)
BMINMAX(__min_index,uint64_t,long,8)

BMINMAX(__min_index,uint64_t,ulong,2)
BMINMAX(__min_index,uint64_t,ulong,4)
BMINMAX(__min_index,uint64_t,ulong,8)

#undef __max_index_pred
#undef __min_index_pred
#undef BMINMAX

#endif /* C7X_INTR_H */
