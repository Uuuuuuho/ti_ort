/*****************************************************************************/
/*  VECTOR.H                                                                 */
/*                                                                           */
/* Copyright (c) 2017 Texas Instruments Incorporated                         */
/* http://www.ti.com/                                                        */
/*                                                                           */
/*  Redistribution and  use in source  and binary forms, with  or without    */
/*  modification,  are permitted provided  that the  following conditions    */
/*  are met:                                                                 */
/*                                                                           */
/*     Redistributions  of source  code must  retain the  above copyright    */
/*     notice, this list of conditions and the following disclaimer.         */
/*                                                                           */
/*     Redistributions in binary form  must reproduce the above copyright    */
/*     notice, this  list of conditions  and the following  disclaimer in    */
/*     the  documentation  and/or   other  materials  provided  with  the    */
/*     distribution.                                                         */
/*                                                                           */
/*     Neither the  name of Texas Instruments Incorporated  nor the names    */
/*     of its  contributors may  be used to  endorse or  promote products    */
/*     derived  from   this  software  without   specific  prior  written    */
/*     permission.                                                           */
/*                                                                           */
/*  THIS SOFTWARE  IS PROVIDED BY THE COPYRIGHT  HOLDERS AND CONTRIBUTORS    */
/*  "AS IS"  AND ANY  EXPRESS OR IMPLIED  WARRANTIES, INCLUDING,  BUT NOT    */
/*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR    */
/*  A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT    */
/*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,    */
/*  SPECIAL,  EXEMPLARY,  OR CONSEQUENTIAL  DAMAGES  (INCLUDING, BUT  NOT    */
/*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,    */
/*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY    */
/*  THEORY OF  LIABILITY, WHETHER IN CONTRACT, STRICT  LIABILITY, OR TORT    */
/*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE    */
/*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.     */
/*                                                                           */
/*****************************************************************************/
#ifndef VECTOR_H
#define VECTOR_H

#include <bitset>
#include <iostream>
#include <array>
#include <type_traits>
#include <functional>
#include <memory>

#include <cstdint>
#include <cstring>
#include <climits>
#include <cstddef>

#include <ti_he_impl/c7x_c_funcs.h>
#include <ti_he_impl/c7x_auto_c_funcs.h>

// Unless otherwise defined, we default to little endian support
#ifndef __little_endian__
#define __little_endian__ 1
#endif
#ifdef __big_endian__
#error Big endian is not supported in host emulation
#endif
#define __big_endian__ 0

/*-----------------------------------------------------------------------------
* Determines how many levels of nested accessors will be allowed.
*
* Currently set to 2 to minimize memory footprint. Compile-time is no longer
* affected by this value.
*-----------------------------------------------------------------------------*/
#define __TI_MAX_ACCESSOR_DEPTH 2

namespace _c70_he_detail
{

/******************************************************************************
*
* Forward declarations
*
******************************************************************************/
template <typename ELEM_T, size_t NELEM>
class vtype;

template<typename ELEM_T>
class ctype;

template<typename VTYPE>
class vtype_ptr;

template<typename ELEM_T>
class ctype_ptr;

template <typename ELEM_T, size_t NELEM, size_t ACCESS_DEPTH>
class accessible;


/******************************************************************************
*
* Utility templates
*
*******************************************************************************/
/******************************************************************************
* deconst -- removes const type with understanding of ctype and vtype
*******************************************************************************/
template <typename T>
struct deconst
{
    using type = typename std::remove_const<T>::type;
};
template <typename E>
struct deconst<ctype<E> >
{
    using type = ctype<typename std::remove_const<E>::type>;
};
template <typename E, int N>
struct deconst<vtype<E,N> >
{
    using type = vtype<typename deconst<E>::type, N>;
};

/******************************************************************************
* mkconst -- add const type with understanding of ctype and vtype
*******************************************************************************/
template <typename T>
struct mkconst
{
    using type = const typename deconst<T>::type;
};
template <typename E>
struct mkconst<ctype<E> >
{
    using type = ctype<const typename deconst<E>::type>;
};
template <typename E, int N>
struct mkconst<vtype<E,N> >
{
    using type = vtype<typename mkconst<E>::type, N>;
};

/******************************************************************************
* isconst -- check for const type with understanding of ctype and vtype
*******************************************************************************/
template <typename T>
struct isconst
{
    static constexpr bool value = !std::is_same<T, typename deconst<T>::type>::value;
};

/******************************************************************************
* enable_if_cmplx_t -- Helpful alias used to detect if a type is a complex type
*******************************************************************************/
template <typename TEST_T, typename RET_T>
using enable_if_cmplx_t = std::enable_if_t<(!std::is_arithmetic<TEST_T>::value),
                                            RET_T>;

/******************************************************************************
* sa_scale -- host emulation implementation of the sizeof(t)/__numelemof(t) for
*   use in __SA* macros.
*******************************************************************************/
template <typename T>
struct sa_scale
{
    static constexpr int value = sizeof(T);
};
// Complex specialization
template <typename CE>
struct sa_scale<ctype<CE> >
{
    static constexpr int value = sizeof(CE) * 2;
};
// Vector specialization
template <typename VE,
          size_t NELEM>
struct sa_scale<vtype<VE, NELEM> >
{
    static constexpr int value = sa_scale<VE>::value;
};

/******************************************************************************
* ptr_type -- Given a type, return an appropriate pointer type.  Used for
*   general access to vector pointers and scalar pointers.
*******************************************************************************/
template <typename T>
struct ptr_type
{
    using type = T *;
};
// Complex specialization
template <typename CE>
struct ptr_type<ctype<CE> >
{
    using type = ctype_ptr<ctype<CE> >;
};
// Vector specialization
template <typename VE,
          size_t NELEM>
struct ptr_type<vtype<VE, NELEM> >
{
    using type = vtype_ptr<vtype<VE, NELEM> >;
};

/******************************************************************************
*
* Helper functions to interface with the C functional description.
*
******************************************************************************/

/******************************************************************************
* init_from_vreg_t -- Converts a vreg_t to another class whose elements are
*   referenced via a reference wrapper array.
*******************************************************************************/
// Arithmetic variant
template <class ACCESS_T,
          typename ELEM_T,
          size_t NELEM,
          std::enable_if_t<std::is_arithmetic<ELEM_T>::value, int> = 0>
void init_from_vreg_t(const vreg_t &src, ACCESS_T &dst)
{
    for (uint32_t i = 0; i < NELEM; i++)
    {
        // Figure out which double-word the value is in. Multiply the index
        // by 2 to get the word index, which is the expected input for
        // get/put functions.
        uint32_t vindex = ((sizeof(ELEM_T) * i) / sizeof(uint64_t)) * 2;

        // Get the containing double-word, and then mask/shift to get the
        // bits we actually want
        uint64_t val = 0;
        get_unsigned_value(src, val, vindex);

        // Figure out the offset within the containing double-word
        uint64_t lane_bit_offset =
            ((sizeof(ELEM_T) * i) % sizeof(uint64_t)) * CHAR_BIT;

        if (lane_bit_offset != 0)
        {
            uint64_t mask = ((uint64_t)1 << lane_bit_offset) - 1;
            val = (val >> lane_bit_offset) & mask;
        }

        // memcpy avoids strict aliasing issues when type-punning
        memcpy(&dst.s[i], &val, sizeof(ELEM_T));
    }
}

// Complex variant
template <class ACCESS_T,
          typename ELEM_T,
          size_t NELEM,
          enable_if_cmplx_t<ELEM_T, int> = 0>
void init_from_vreg_t(const vreg_t &src, ACCESS_T &dst)
{
    uint32_t vec_count = 0;
    for (uint32_t i = 0; i < NELEM; i++)
    {
        // Figure out which double-word the value is in. Multiply the index
        // by 2 to get the word index, which is the expected input for
        // get/put functions.

        // Grab the index where each value is stored
        uint32_t vindex_imag = ((sizeof(typename ELEM_T::ELEM_TYPE)
                                   * vec_count) / sizeof(uint64_t)) * 2;
        uint32_t vindex_real = ((sizeof(typename ELEM_T::ELEM_TYPE)
                                   * (vec_count + 1)) / sizeof(uint64_t)) * 2;

        // Get the containing double-word, and then mask/shift to get the
        // bits we actually want. These indicies can be the same,
        // it just means that the values are in the same double word
        uint64_t val_imag = 0;
        uint64_t val_real = 0;
        get_unsigned_value(src, val_imag, vindex_imag);
        get_unsigned_value(src, val_real, vindex_real);

        // Figure out the offset within the containing double-word
        uint64_t lane_bit_offset_imag =
            ((sizeof(typename ELEM_T::ELEM_TYPE) * vec_count)
            % sizeof(uint64_t)) * CHAR_BIT;

        uint64_t lane_bit_offset_real =
            ((sizeof(typename ELEM_T::ELEM_TYPE) * (vec_count + 1))
            % sizeof(uint64_t)) * CHAR_BIT;

        // If multiple values are contained within the double word, use mask to
        // retrieve correct bits
        if (lane_bit_offset_imag != 0)
        {
            uint64_t mask_imag = ((uint64_t)1 << lane_bit_offset_imag) - 1;
            val_imag = (val_imag >> lane_bit_offset_imag) & mask_imag;
        }
        if (lane_bit_offset_real != 0)
        {
            uint64_t mask_real = ((uint64_t)1 << lane_bit_offset_real) - 1;
            val_real = (val_real >> lane_bit_offset_real) & mask_real;
        }

        typename ELEM_T::ELEM_TYPE val_i_cast;
        typename ELEM_T::ELEM_TYPE val_r_cast;

        // memcpy avoids strict aliasing issues when type-punning
        memcpy(&val_i_cast, &val_imag, sizeof(typename ELEM_T::ELEM_TYPE));
        memcpy(&val_r_cast, &val_real, sizeof(typename ELEM_T::ELEM_TYPE));

        dst.s[i] = ELEM_T(val_r_cast, val_i_cast);

        vec_count += 2;
    }
}

/******************************************************************************
* convert_to_vreg_t -- Converts an object whose elements are referenced via a
*   reference wrapper array into a vreg_t for use by the C functional
*   description.
*******************************************************************************/
// Arithmetic variant
template<class ACCESS_T,
         typename ELEM_T,
         size_t NELEM,
         std::enable_if_t<std::is_arithmetic<ELEM_T>::value, int> = 0>
vreg_t convert_to_vreg_t(const ACCESS_T &src)
{
    vreg_t res;

    for (uint32_t i = 0; i < NELEM; i++)
    {
        // Figure out which double-word the value is in. Multiply the index
        // by 2 to get the word index, which is the expected input for
        // get/put functions.
        uint32_t vindex = ((sizeof(ELEM_T) * i) / sizeof(uint64_t)) * 2;

        // Get the containing double-word, and then mask/shift to modify
        // the bits we actually want to touch
        uint64_t val;
        get_unsigned_value(res, val, vindex);

        // Figure out the offset within the containing double-word
        uint64_t lane_bit_offset =
            ((sizeof(ELEM_T) * i) % sizeof(uint64_t)) * CHAR_BIT;

        // Generate masks to allow us to reconstruct the value with the
        // modified lane dropped in
        uint64_t val_mask = 0;
        for (uint64_t j = 0; j < sizeof(uint64_t) * CHAR_BIT; j += CHAR_BIT)
        {
            if (j < lane_bit_offset ||
                j >= lane_bit_offset + sizeof(ELEM_T) * CHAR_BIT)
            {
                val_mask |= (((uint64_t)1 << CHAR_BIT) - 1) << j;
            }
        }
        uint64_t lane_mask = ~val_mask;

        val = (val & val_mask) |
              (((uint64_t)src.s[i] << lane_bit_offset) & lane_mask);

        put_unsigned_value(res, val, vindex);
    }

    return res;
}

// Complex variant
template<class ACCESS_T,
         typename ELEM_T,
         size_t NELEM,
         enable_if_cmplx_t<ELEM_T, int> = 0>
vreg_t convert_to_vreg_t(const ACCESS_T &src)
{
    vreg_t res;

    vtype<typename ELEM_T::ELEM_TYPE, NELEM * 2> vec_type =
        vtype<typename ELEM_T::ELEM_TYPE, NELEM * 2>(0);

    size_t vec_count = 0;
    for (uint32_t i = 0; i < NELEM ; i++)
    {
        // Grab ctype at index
        ELEM_T temp = src.s[i];
        vec_type.s[vec_count] = temp.i;
        vec_type.s[vec_count + 1] = temp.r;
        vec_count += 2;
    }

    res = convert_to_vreg_t<vtype<typename ELEM_T::ELEM_TYPE, NELEM * 2>,
                            typename ELEM_T::ELEM_TYPE, NELEM * 2>(vec_type);

    return res;
}

/******************************************************************************
* convert_ctype_to_vreg_t -- Converts a complex element object whose elements
*   are referenced via a reference wrapper array into a vreg_t for use by the C
*   functional description.
*******************************************************************************/
template<typename ELEM_T>
vreg_t convert_ctype_to_vreg_t(const ctype<ELEM_T> &src)
{
    vreg_t res;

    vtype<ELEM_T, 2> vec_type = vtype<ELEM_T, 2>(0);

    vec_type.s[0] = src.i;
    vec_type.s[1] = src.r;

    res = convert_to_vreg_t<vtype<ELEM_T, 2>, ELEM_T, 2>(vec_type);

    return res;
}

/******************************************************************************
* init_ctype_from_vreg_t -- Converts a vreg_t to a complex element whose
*   elements are referenced via a reference wrapper array.
*******************************************************************************/
template <typename ELEM_T>
void init_ctype_from_vreg_t(const vreg_t &src, ctype<ELEM_T> &dst)
{
    // Grab where each value is (which index)
    uint32_t vindex_imag = ((sizeof(ELEM_T) * 0) / sizeof(uint64_t)) * 2;
    uint32_t vindex_real = ((sizeof(ELEM_T) * 1) / sizeof(uint64_t)) * 2;

    uint64_t val_imag = 0;
    uint64_t val_real = 0;
    get_unsigned_value(src, val_imag, vindex_imag);
    get_unsigned_value(src, val_real, vindex_real);

    // Figure out the offset within the containing double-word
    uint64_t lane_bit_offset_imag =
        ((sizeof(ELEM_T) * 0) % sizeof(uint64_t)) * CHAR_BIT;

    uint64_t lane_bit_offset_real =
        ((sizeof(ELEM_T) * 1) % sizeof(uint64_t)) * CHAR_BIT;


    // If multiple values are contained within the double word,
    // use mask to retrieve correct bits
    if (lane_bit_offset_imag != 0)
    {
        uint64_t mask_imag = ((uint64_t)1 << lane_bit_offset_imag) - 1;
        val_imag = (val_imag >> lane_bit_offset_imag) & mask_imag;
    }
    if (lane_bit_offset_real != 0)
    {
        uint64_t mask_real = ((uint64_t)1 << lane_bit_offset_real) - 1;
        val_real = (val_real >> lane_bit_offset_real) & mask_real;
    }

    ELEM_T val_i_cast;
    ELEM_T val_r_cast;

    // memcpy avoids strict aliasing issues when type-punning
    memcpy(&val_i_cast, &val_imag, sizeof(ELEM_T));
    memcpy(&val_r_cast, &val_real, sizeof(ELEM_T));

    dst.i = val_i_cast;
    dst.r = val_r_cast;
}

/******************************************************************************
* ACCESS_TYPE -- ENUM for all valid access types
******************************************************************************/
enum class ACCESS_TYPE { NORMAL, LO, HI, EVEN, ODD, REAL, IMAG };

// Create struct here so that we don't have run-time
// overhead from passing access type as parameter
template<ACCESS_TYPE ATYPE>
struct access_types { };

/******************************************************************************
* ref_holder -- used to construct an array of reference wrappers to a specific
*   set of elements chosen based on ACCESS_TYPE. The accessible class derives
*   from the ref_holder class, meaning that all accessibles (and thus all
*   vtypes) will have an array of reference wrappers (refs_m). All index
*   calculations are handled during compile time, meaning that all references
*   are set up and ready to use without any run-time overhead.
*
*   The complex-type variant of ref_holder has to save off copies of ctype_ptr
*   pointers it allocates to a vector so that the objects can be freed when the
*   object is destroyed.  To do this, we use a base class (ref_holder_base) to
*   hold the vector that is only enabled when holding a complex vector.
*******************************************************************************/

template <typename ELEM_T,
          typename Enable=void>
class ref_holder_base;

// Complex specialization
template <typename ELEM_T>
class ref_holder_base<ELEM_T,
                      enable_if_cmplx_t<ELEM_T,void>>
{
public:
    std::vector<ctype_ptr<ctype<typename ELEM_T::ELEM_TYPE> > > cptrs;
};

// Arithmetic specialization
template <typename ELEM_T>
class ref_holder_base<ELEM_T,
                      std::enable_if_t<std::is_arithmetic<ELEM_T>::value>>
{};

template <typename ELEM_T,
          size_t NELEM>
class ref_holder : public ref_holder_base<ELEM_T>
{
/*-----------------------------------------------------------------------------
* Constructors
*-----------------------------------------------------------------------------*/
public:
    /*-------------------------------------------------------------------------
    * Constructor used during initialization of a vtype. Creates a reference
    * wrapper array with references to data found directly in vtype's "data"
    * member array.
    *-------------------------------------------------------------------------*/
    template<size_t... Is>
    ref_holder(std::array<ELEM_T, NELEM> &data,
               access_types<ACCESS_TYPE::NORMAL>,
               std::index_sequence<Is...>) :
        refs_m({(data[Is])...})
    {}

    /*-------------------------------------------------------------------------
    * Constructor used during initialization of a vtype. Creates a reference
    * wrapper array with references found in a higher level reference wrapper
    * array.
    *-------------------------------------------------------------------------*/
    template<size_t... Is>
    ref_holder(std::array<std::reference_wrapper<ELEM_T>, NELEM> &refs,
               access_types<ACCESS_TYPE::NORMAL>,
               std::index_sequence<Is...>) :
        refs_m({(refs[Is])...})
    {}

    /*-------------------------------------------------------------------------
    * Variation on above to retype references from non-const to const.
    *-------------------------------------------------------------------------*/
    template<size_t... Is,
             typename DE = ELEM_T,
             std::enable_if_t<isconst<DE>::value, int> = 0>
    ref_holder(std::array<std::reference_wrapper<typename deconst<ELEM_T>::type>, NELEM> &refs,
               access_types<ACCESS_TYPE::NORMAL>,
               std::index_sequence<Is...>) :
        refs_m({(std::reference_wrapper<ELEM_T>(refs[Is]))...})
    {}

    /*-------------------------------------------------------------------------
    * Constructor used during initialization of a LO accessible. Creates
    * a reference wrapper array with references found in a higher level
    * reference wrapper array.
    *-------------------------------------------------------------------------*/
    template<size_t... Is>
    ref_holder(std::array<std::reference_wrapper<ELEM_T>, NELEM*2> &refs,
               access_types<ACCESS_TYPE::LO>,
               std::index_sequence<Is...>) :
        refs_m({(refs[Is])...}) 
    {}

    /*-------------------------------------------------------------------------
    * Constructor used during initialization of a HI accessible
    *-------------------------------------------------------------------------*/
    template<size_t... Is>
    ref_holder(std::array<std::reference_wrapper<ELEM_T>, NELEM*2> &refs,
               access_types<ACCESS_TYPE::HI>,
               std::index_sequence<Is...>) :
        refs_m({(refs[Is + NELEM])...}) 
    {}

    /*-------------------------------------------------------------------------
    * Constructor used during initialization of an EVEN accessible
    *-------------------------------------------------------------------------*/
    template<size_t... Is>
    ref_holder(std::array<std::reference_wrapper<ELEM_T>, NELEM*2> &refs,
               access_types<ACCESS_TYPE::EVEN>,
               std::index_sequence<Is...>) :
        refs_m({(refs[2*Is])...}) 
    {}

    /*-------------------------------------------------------------------------
    * Constructor used during initialization of an ODD accessible
    *-------------------------------------------------------------------------*/
    template<size_t... Is>
    ref_holder(std::array<std::reference_wrapper<ELEM_T>, NELEM*2> &refs,
               access_types<ACCESS_TYPE::ODD>,
               std::index_sequence<Is...>) :
        refs_m({(refs[2*Is + 1])...})
    {}

    /*-------------------------------------------------------------------------
    * Constructor used during initialization of a REAL accessible
    *-------------------------------------------------------------------------*/
    template<size_t... Is>
    ref_holder(std::array<std::reference_wrapper<ctype<ELEM_T>>, NELEM> &refs,
               access_types<ACCESS_TYPE::REAL>,
               std::index_sequence<Is...>) :
        refs_m({(*(refs[Is].get().get_default_data_ptr()))...}) 
    {}

    /*-------------------------------------------------------------------------
    * Constructor used during initialization of an IMAG accessible
    *-------------------------------------------------------------------------*/
    template<size_t... Is>
    ref_holder(std::array<std::reference_wrapper<ctype<ELEM_T>>, NELEM> &refs,
               access_types<ACCESS_TYPE::IMAG>,
               std::index_sequence<Is...>) :
        refs_m({(*(refs[Is].get().get_default_data_ptr() + 1 ))...}) 
    {}

    /*-------------------------------------------------------------------------
    * Constructor used during initialization of a vtype whose data is located
    * as a consecutive set of elements in memory.  This constructor
    * initializes refs_m to contain reference wrappers to that data in memory
    * instead of the data found in vtype's "data" member array. This
    * constructor is used for vectors with scalar element types and complex
    * element types (when constructed ctype objects exist in memory).
    *-------------------------------------------------------------------------*/
    template<size_t... Is>
    ref_holder(ELEM_T* ptr,
               access_types<ACCESS_TYPE::NORMAL>,
               std::index_sequence<Is...>) :
        refs_m({init_from_vtype_ptr_ref(ptr, Is)...})
    {}

    /*-------------------------------------------------------------------------
    * Constructor used during initialization of a vtype whose data is located
    * as a consecutive set of complex components in memory. In this case, the
    * elements in memory are pairs of scalars that together represent a
    * complex element. A pointer is passed to this constructor and initializes
    * refs_m to contain reference wrappers to the data in memory instead of
    * the data found in vtype's "data" member array.
    *-------------------------------------------------------------------------*/
    template<size_t... Is,
             typename ELEM_DUMMY = ELEM_T,
             typename = enable_if_cmplx_t<ELEM_DUMMY, void>>
    ref_holder(typename ELEM_DUMMY::ELEM_TYPE* ptr,
               access_types<ACCESS_TYPE::NORMAL>,
               std::index_sequence<Is...>) :
        refs_m({init_from_vtype_ptr_ref_comp(ptr, Is)...})
    {}

/*-----------------------------------------------------------------------------
* Methods used to obtain ELEM_T references from data pointers.
*-----------------------------------------------------------------------------*/
private:
    /*-------------------------------------------------------------------------
    * init_from_vtype_ptr_ref --  Grab constructed ctypes or scalar element
    *   types from memory
    *-------------------------------------------------------------------------*/
    ELEM_T& init_from_vtype_ptr_ref(ELEM_T* ptr, size_t index)
    {
        return *(ptr + index);
    }

    /*-------------------------------------------------------------------------
    * init_from_vtype_ptr_ref_comp -- Construct a ctype from pairs of 
    *   component-type scalars that represent a complex element.
    *-------------------------------------------------------------------------*/
    template<typename ELEM_DUMMY = ELEM_T,
             typename = enable_if_cmplx_t<ELEM_DUMMY, void>>
    ELEM_T& init_from_vtype_ptr_ref_comp(typename ELEM_DUMMY::ELEM_TYPE* ptr, size_t index)
    {
        // ELEM_T is ctype<built in type>
        ctype_ptr<ctype<typename ELEM_DUMMY::ELEM_TYPE> > cptr(ptr + 2*index);

        // Keep active pointers to the constructed ctypes so they can be
        // freed during object destruction. The ctype_ptr in this function
        // scope will be destroyed, leaving ctype object ownership to the
        // saved copy.
        this->cptrs.push_back(cptr);

        return *cptr;
    }

/*-----------------------------------------------------------------------------
* Data
*-----------------------------------------------------------------------------*/
public:
    // Reference wrapper array member
    std::array<std::reference_wrapper<ELEM_T>, NELEM> refs_m;
};

/******************************************************************************
* cond_value -- A utility template to implement the ternary operator for values.
*******************************************************************************/
// True (default)
template<bool B, int T, int F>
struct cond_value { static constexpr int value = T; };
// False specialization
template<int T, int F>
struct cond_value<false, T, F> { static constexpr int value = F; };

/******************************************************************************
* comp_type -- A utility template to find fundamental datatype that a vector or
*   complex uses.
*******************************************************************************/
template <typename TYPE, typename Enable = void>
struct comp_type;
// Arithmetic specialization
template <typename TYPE>
struct comp_type<TYPE,
                 std::enable_if_t<std::is_arithmetic<TYPE>::value> >
{
    using type = TYPE;
};
// Complex specialization
template <typename TYPE>
struct comp_type<TYPE,
                 std::enable_if_t<!std::is_arithmetic<TYPE>::value> >
{
    using type = typename comp_type<typename TYPE::ELEM_TYPE>::type;
};

/******************************************************************************
* vtype -- the high-level abstraction for OpenCL vector types. It is
*   instantiated with an element type and a number of elements. The element
*   types should be those found in stdint so that bit widths are guaranteed.
*
*   e.g.: char64 = vtype<int8_t, 64>
*******************************************************************************/
template <typename ELEM_T,
          size_t NELEM>
class vtype : public accessible<ELEM_T, NELEM, 0>
{
/*-----------------------------------------------------------------------------
* Accessible information members
*-----------------------------------------------------------------------------*/
public:
    using ACCESS_T = accessible<ELEM_T, NELEM, 0>;

    template <size_t OTHER_DEPTH>
    using EQUIV_ACCESS_T = accessible<ELEM_T, NELEM, OTHER_DEPTH>;

/*-----------------------------------------------------------------------------
* General information members
*-----------------------------------------------------------------------------*/
public:
    using ELEM_TYPE = ELEM_T;
    using PTR_TYPE = vtype_ptr<vtype<ELEM_T,NELEM> >;
    /*-------------------------------------------------------------------------
    * The unqualified element type is the type with any nested const qualifiers
    * removed.  Therefore const scalar is scalar and ctype<const scalar> is
    * ctype<scalar>.
    *-------------------------------------------------------------------------*/
    using UQELEM_TYPE = typename deconst<ELEM_TYPE>::type;
    /*-------------------------------------------------------------------------
    * The data type is what is actually used to store the elements if not bound
    * to memory.  For scalar and ctype<scalar>, do nothing.  For const scalar,
    * get scalar (so you can modify it).  For ctype<const scalar>, do nothing
    * because the const only applies to the contained references and not the
    * data.
    *-------------------------------------------------------------------------*/
    using DATA_TYPE = typename std::remove_const<ELEM_T>::type;
    /*-------------------------------------------------------------------------
    * The arithmetic result type is the type that is returned from typical
    * arithmetic operations such as + and -.
    *-------------------------------------------------------------------------*/
    using ARITH_RES_TYPE = vtype<UQELEM_TYPE,NELEM>;
    /*-------------------------------------------------------------------------
    * The logical result type is the type that is returned from logical style
    * operations such as &&, ||, ==, etc.
    *-------------------------------------------------------------------------*/
    using LOG_RES_TYPE = vtype<typename std::conditional<sizeof(ELEM_TYPE) == 1, int8_t,
                               typename std::conditional<sizeof(ELEM_TYPE) == 2, int16_t,
                               typename std::conditional<sizeof(ELEM_TYPE) == 4, int32_t,
                               typename std::conditional<sizeof(ELEM_TYPE) == 8, int64_t, void>::type>::type>::type>::type,NELEM>;
    /*-------------------------------------------------------------------------
    * The component type is the underlying scalar type.  For example, vtype<int>
    * has a component type of int.  vtype<ctype<int> > also has a component type
    * of int.
    *-------------------------------------------------------------------------*/
    using COMP_TYPE = typename comp_type<ELEM_T>::type;

    static constexpr size_t NUM_ELEM = NELEM;
    static constexpr size_t NUM_COMPS = NELEM * cond_value<std::is_arithmetic<ELEM_T>::value,1,2>::value;

/*-----------------------------------------------------------------------------
* Constructors and construction related data types
*-----------------------------------------------------------------------------*/
public:
    /*-------------------------------------------------------------------------
    * mem_bound_cons_wrapper - A wrapper around another vector type that is used
    *   to differentiate between constructors.  For implementation use only.
    *-------------------------------------------------------------------------*/
    template<typename OELEM_T,
             int ONELEM>
    struct mem_bound_cons_wrapper
    {
        mem_bound_cons_wrapper<OELEM_T, ONELEM>(vtype<OELEM_T,ONELEM> &_v): v(_v) {}
        vtype<OELEM_T,ONELEM> &v;
    };

    /*-------------------------------------------------------------------------
    * OpenCL vector literal syntax constructor, see init_helper for more.
    * e.g.: int4(int2(1, 2), 3, 4)
    *-------------------------------------------------------------------------*/
    template<class ...INITS>
    explicit vtype(const INITS &...inits) :
        ACCESS_T(data, access_types<ACCESS_TYPE::NORMAL>()),
        references_scalar_array(false)
    {
        init_helper<0>(inits...);
    }

    /*-------------------------------------------------------------------------
    * Constructor to initialize vtype from an accessible (vtype.[lo|hi|even|odd])
    *-------------------------------------------------------------------------*/
    template<size_t ADEPTH>
    vtype(const accessible<ELEM_T, NELEM, ADEPTH> &a) :
        ACCESS_T(data, access_types<ACCESS_TYPE::NORMAL>()),
        references_scalar_array(false)
    {
        for(size_t i = 0 ; i < NELEM; i++)
            (*this).data[i] = a.s[i];
    }

    /*-------------------------------------------------------------------------
    * Constructor to initialize a vtype from another vtype
    *-------------------------------------------------------------------------*/
    vtype(const vtype &v) :
        ACCESS_T(data, access_types<ACCESS_TYPE::NORMAL>()),
        references_scalar_array(false)
    {
        for(size_t i = 0 ; i < NELEM; i++)
            (*this).data[i] = v.s[i];
    }

    /*-------------------------------------------------------------------------
    * Constructor to initialize vtype from a memory location
    * This is only done via an intrinsic and should be not be used externally.
    * In this case, we need to update the array of reference wrappers to
    * reference elements found in a memory location. When a vtype is
    * constructed from this, the "data" member array is not modified and
    * remains as an array of 0's, which is its default state.
    *-------------------------------------------------------------------------*/
    explicit vtype(ELEM_T* ptr) :
        ACCESS_T(ptr, access_types<ACCESS_TYPE::NORMAL>()),
        references_scalar_array(true)
    {}

    /*-------------------------------------------------------------------------
    * Constructor to generate a vtype whose references are bound exactly the
    * same as another vtype.  For implementation use only.
    *-------------------------------------------------------------------------*/
    // Arithmetic variant
    template<typename OELEM_T,
             int ONELEM,
             std::enable_if_t<std::is_arithmetic<OELEM_T>::value, int> = 0>
    vtype(const mem_bound_cons_wrapper<OELEM_T, ONELEM> &wrapper) :
        ACCESS_T(wrapper.v.s.refs_s, access_types<ACCESS_TYPE::NORMAL>()),
        references_scalar_array(wrapper.v.references_scalar_array)
    {}

    // Complex variant
    template<typename OELEM_T,
             int ONELEM,
             std::enable_if_t<!std::is_arithmetic<OELEM_T>::value, int> = 0>
    vtype(const mem_bound_cons_wrapper<OELEM_T, ONELEM> &wrapper) :
        ACCESS_T(data, access_types<ACCESS_TYPE::NORMAL>()),
        references_scalar_array(wrapper.v.references_scalar_array)
    {
        // Re-initialize our complex data.
        // For each complex member of the other vector, generate a const version
        // and store it to our data array.
        for (size_t i = 0; i < ONELEM; i++)
        {
            // The elements of the data array cannot be easily overwritten due
            // to the fact that operator= is defined in a custom way and that
            // references cannot be rebound anyway.  Our technique here is the
            // only possible one I know of: call the destructor of the element
            // but don't free the memory.  Then reinitialize that memory with
            // placement new.
            data[i].~ELEM_T();
            // Neglecting strict C++ syntax, essentially:
            // data[i] = ELEM_T(mem_bound_cons_wrapper(wrapper.v.s[i]))
            new (std::addressof(data[i])) ELEM_T(typename ELEM_T::template mem_bound_cons_wrapper<typename OELEM_T::ELEM_TYPE>(wrapper.v.s[i]));
        }
    }

    /*-------------------------------------------------------------------------
    * Constructor to initialize vtype from a complex component-type pointer.
    * Similar to constructing from a memory address but the data pointer is
    * passed directly to differentiate from an ELEM_T*
    *-------------------------------------------------------------------------*/
    template<typename ELEM_DUMMY = ELEM_T,
             typename = enable_if_cmplx_t<ELEM_DUMMY, void>>
    explicit vtype(typename ELEM_DUMMY::ELEM_TYPE* ptr) :
        ACCESS_T(ptr, access_types<ACCESS_TYPE::NORMAL>()),
        references_scalar_array(true)
    {}

    /*-------------------------------------------------------------------------
    * Default constructor
    *-------------------------------------------------------------------------*/
    vtype() :
        ACCESS_T(data, access_types<ACCESS_TYPE::NORMAL>()),
        references_scalar_array(false)
    {
        // Initialize differently based on what the element type is
        initialize_default<ELEM_T>();
    }

/*-----------------------------------------------------------------------------
* Recursive instantiations
*-----------------------------------------------------------------------------*/
private:
    /*-------------------------------------------------------------------------
    * The next initializer in the pack is a singular element
    * e.g.: (int4)(... 4, ...)
    * Add it to the current index and recurse.
    *-------------------------------------------------------------------------*/
    template<size_t curr_idx,
             class ...INITS>
    void init_helper(const ELEM_T &next, const INITS &...inits)
    {
        static_assert(curr_idx < NELEM, "Illegal vector init, too many");

        (*this).data[curr_idx] = next;

        init_helper<curr_idx + 1>(inits...);
    }

    /*-------------------------------------------------------------------------
    * The next two initializers in the pack represent a single complex element
    * e.g.: (cint4)(... 4, 5, ...)
    * Add it to the current index and recurse.
    *-------------------------------------------------------------------------*/
    template<size_t curr_idx,
             typename DUMMY = ELEM_T,
             class ...INITS,
             enable_if_cmplx_t<DUMMY, int> = 0>
    void init_helper(const typename DUMMY::ELEM_TYPE &next,
                     const typename DUMMY::ELEM_TYPE &next1,
                     const INITS &...inits)
    {
        static_assert(curr_idx < NELEM, "Illegal vector init, too many");

        ELEM_T temp = ELEM_T(next, next1);

        (*this).data[curr_idx] = temp;

        init_helper<curr_idx + 1>(inits...);
    }

    /*-------------------------------------------------------------------------
    * The next initializer is a vector
    * e.g.: (int8)(... int4(...), ...)
    * Add each element of the vector and recurse.
    *-------------------------------------------------------------------------*/
    template<size_t curr_idx,
             typename OELEM_T,
             size_t OTHER_NELEM, 
             size_t DEPTH, 
             class ...INITS>
    void init_helper(const accessible<OELEM_T, OTHER_NELEM, DEPTH> &next,
                     const INITS &...inits)
    {
        static_assert(curr_idx + OTHER_NELEM - 1 < NELEM,
                      "Illegal vector init, too many");

        // NOTE: when dealing with accessibles, use S to grab actual data
        for (size_t i = 0; i < OTHER_NELEM; i++)
            (*this).data[i + curr_idx] = next.s[i];

        init_helper<curr_idx + OTHER_NELEM>(inits...);
    }

    /*-------------------------------------------------------------------------
    * The only initializer is a vreg_t
    * Convert the vreg_t into its target vector representation.
    *-------------------------------------------------------------------------*/
    template <size_t curr_idx>
    void init_helper(const vreg_t &v)
    {
        static_assert(curr_idx == 0, "Accessor created illegal vector init.");
        init_from_vreg_t<vtype<ELEM_T, NELEM>, ELEM_T, NELEM>(v, *this);
    }

    /*-------------------------------------------------------------------------
    * Termination
    * When we've run out of things to process, ensure we've packed the full
    * vector, then we're done.
    *-------------------------------------------------------------------------*/
    template <size_t curr_idx>
    void init_helper()
    {
        static_assert(curr_idx == NELEM || curr_idx == 1,
                      "Illegal vector init, too few");

        // OpenCL vector literal duplication constructor
        // e.g.: int4(1)
        if (curr_idx == 1)
            for(size_t i = 0; i < NELEM; i++)
                (*this).data[i] = (*this).s[0];
    }

    /*-------------------------------------------------------------------------
    * initialize_default -- fill with 0s.
    *-------------------------------------------------------------------------*/
    // Complex variant
    template <typename E_TYPE,
              enable_if_cmplx_t<E_TYPE, int> = 0>
    void initialize_default()
    {
        // Calls default ctype constructor
        data.fill(E_TYPE());
    }

    // Arithmetic variant
    template <typename E_TYPE,
              std::enable_if_t<std::is_arithmetic<E_TYPE>::value, int> = 0>
    void initialize_default()
    {
        data.fill((E_TYPE)(0));
    }

/*-----------------------------------------------------------------------------
* Operators
*   Most operators will be implemented external of this class.
*-----------------------------------------------------------------------------*/
public:
    /*-------------------------------------------------------------------------
    * Assignment from vector (required as copy assignment operator)
    *-------------------------------------------------------------------------*/
    vtype<ELEM_TYPE, NELEM> &operator=(const vtype<ELEM_T, NELEM> &rhs)
    {
        for (size_t i = 0; i < NELEM; i++)
            (*this).s[i] = rhs.s[i];
        return *this;
    }

    /*-------------------------------------------------------------------------
    * Assignment from scalar
    *-------------------------------------------------------------------------*/
    vtype<UQELEM_TYPE, NELEM> &operator=(const ELEM_T &rhs)
    {
        for (size_t i = 0; i < NELEM; i++)
            (*this).s[i] = rhs;
        return *this;
    }

    /*-------------------------------------------------------------------------
    * Cast to vreg_t
    *-------------------------------------------------------------------------*/
    operator vreg_t() const
    {
        return convert_to_vreg_t<vtype<ELEM_T, NELEM>, ELEM_T, NELEM>(*this);
    }

    /*-------------------------------------------------------------------------
    * Cast to non-const
    *-------------------------------------------------------------------------*/
    operator vtype<UQELEM_TYPE, NUM_ELEM>() const
    {
        vtype<UQELEM_TYPE, NUM_ELEM> new_vec;
        for (size_t i = 0; i < NUM_ELEM; i++)
            new_vec.s[i] = (*this).s[i];
        return new_vec;
    }

    /*-------------------------------------------------------------------------
    * Address-of
    *-------------------------------------------------------------------------*/
    vtype_ptr<vtype<ELEM_T, NELEM> > operator&()
    {
        // Objects that inherit enable_shared_from_this<> must ensure that any
        // corresponding instantiated object of that type is owned by a shared
        // pointer.  In this case, it is the base class, accessible, that
        // inherits enable_shared_from_this<>.
        //
        // Problem: There are cases in which objects that inherit
        // enabled_shared_from_this<> are not dynamically-allocated and
        // therefore are not owned by a shared pointer. This is the case for
        // statically-allocated and automatically-allocated objects.  You can't
        // return a shared_ptr<> (actually a weak_ptr<>) to the current object
        // if it is not already owned by a shared_ptr<>. This is a known issue.
        //
        // Best Solution: Boost resolves a similar issue by creating a
        // temporary shared pointer with a custom null-deleter to manage a
        // shared_ptr to the object and avoid destroying the object when the
        // shared_ptr goes out of scope.  We will need to do something similar.
        // The temporary shared pointer owns the object if there are no other
        // shared pointers. The custom null-deleter ensures that the object
        // isn't destroyed when the temporary pointer goes out of function
        // scope. When the temporary pointer goes out of scope, the reference
        // count is properly decremented so that the object doesn't leak later.
        const auto wptr = std::shared_ptr<vtype<ELEM_T, NELEM>>(this,
                                                  [](vtype<ELEM_T, NELEM>*){});

        return vtype_ptr<vtype<ELEM_T, NELEM> >(this->shared_from_this());
    }

    // Delete const version of operator
    vtype_ptr<vtype<ELEM_T, NELEM> > operator&() const = delete;

/*-----------------------------------------------------------------------------
* Direct data access (for implementation use)
*-----------------------------------------------------------------------------*/
public:

    /*-------------------------------------------------------------------------
    * get_data_arr_ptr -- Return pointer to vector's underlying memory
    *
    *   Important Note: The object's "underlying memory" depends on how the
    *   object was constructed.  If this vector type object was constructed
    *   based on a pointer to memory, then effectively it always references that
    *   memory (via "struct s_impl<> s" in class accessible), and a pointer to
    *   that memory should be returned here.  On the other hand, if this
    *   object was not constructed based on a pointer to memory, then it
    *   always references its internal "data" array below.  Here we return the
    *   address to which "s" refers since that will always refer to the right
    *   location.
    *
    *   Previously, there was a defect where the location of the instance's
    *   underlying "data" array was always returned. This was not correct for
    *   cases in which the instance was constructed based on a pointer to
    *   memory. (See CODEGEN-6186)
    *-------------------------------------------------------------------------*/
    // Arithmetic variant (e.g. int* from int4)
    template <typename ED = ELEM_T,
              std::enable_if_t<std::is_arithmetic<ED>::value, int> = 0>
    ELEM_T* get_data_arr_ptr()
    {
        return &this->s[0];
    }

    // Complex variant (e.g. int* from cint4)
    template <typename ED = ELEM_T,
              typename = enable_if_cmplx_t<ED, void>>
    typename ED::ELEM_TYPE* get_data_arr_ptr()
    {
        if (!references_scalar_array)
            throw std::runtime_error("Cannot convert complex vector pointer"
                   " to scalar pointer: Vector was not previously initialized"
                   " using a scalar pointer");

        return (typename ED::ELEM_TYPE*)(&this->s[0]);
    }

    /*-------------------------------------------------------------------------
    * get_cdata_arr_ptr -- Return complex type element pointer
    *   (e.g. cint_ptr from cint4)
    *-------------------------------------------------------------------------*/
    template <typename ED = ELEM_T, 
              typename = enable_if_cmplx_t<ED, void>>
    ctype_ptr<ED> get_cdata_arr_ptr()
    {
        return &this->s[0];
    }

/*-----------------------------------------------------------------------------
* Private data
*-----------------------------------------------------------------------------*/
private:
    // Data array which represents lanes in the vector type.
    std::array<DATA_TYPE, NELEM> data;

/*-----------------------------------------------------------------------------
* Public data
*-----------------------------------------------------------------------------*/
public:
    const bool references_scalar_array;
};

/******************************************************************************
* ctype -- Class ctype is the high-level abstraction for complex element types.
*   It is instantiated with an element type. The element types should be those
*   found in stdint so that bit widths are guaranteed.
*
*   e.g.: cchar = ctype<int8_t>
*******************************************************************************/
template <typename ELEM_T>
class ctype : public std::enable_shared_from_this<ctype<ELEM_T>>
{
/*-----------------------------------------------------------------------------
* General information members
*-----------------------------------------------------------------------------*/
public:
    using ELEM_TYPE = ELEM_T;
    using UQELEM_TYPE = typename std::remove_const<ELEM_TYPE>::type;
    static constexpr size_t NUM_ELEM = 1;

    using COMP_TYPE = ELEM_T;
    static constexpr size_t NUM_COMPS = 2;

    using PTR_TYPE = ctype_ptr<ctype<ELEM_T> >;

    using SHARED_T = std::enable_shared_from_this<ctype<ELEM_T>>;

/*-----------------------------------------------------------------------------
* Constructors and construction related data types
*-----------------------------------------------------------------------------*/
public:
    /*-------------------------------------------------------------------------
    * mem_bound_cons_wrapper - A wrapper around another vector type that is used
    *   to differentiate between constructors.  For implementation use only.
    *-------------------------------------------------------------------------*/
    template<typename OELEM_T>
    struct mem_bound_cons_wrapper
    {
        mem_bound_cons_wrapper<OELEM_T>(ctype<OELEM_T> &_c) : c(_c) {}
        ctype<OELEM_T> &c;
    };

    /*-------------------------------------------------------------------------
    * Constructor to create a ctype from a real and imaginary component
    *-------------------------------------------------------------------------*/
    explicit ctype(const ELEM_T &real, const ELEM_T &imag) :
        r(data[0]),
        i(data[1]),
        references_scalar_array(false)
    {
        data[0] = real;
        data[1] = imag;
    }

    /*-------------------------------------------------------------------------
    * Copy constructor
    *-------------------------------------------------------------------------*/
    ctype(const ctype<ELEM_T> &cmplx) :
        SHARED_T(),
        r(data[0]),
        i(data[1]),
        references_scalar_array(false)
    {
        data[0] = cmplx.r;
        data[1] = cmplx.i;
    }

    /*-------------------------------------------------------------------------
    * Constructor to create a ctype from a pointer
    * This constructor should not be used externally
    *-------------------------------------------------------------------------*/
    ctype(ELEM_T* ptr):
        r(*(ptr)),
        i(*(ptr + 1)),
        references_scalar_array(true)
    { }

    /*-------------------------------------------------------------------------
    * Constructor to generate a ctype whose references are bound exactly the
    * same as another ctype.  For implementation use only.
    *-------------------------------------------------------------------------*/
    template<typename OELEM_T>
    ctype(const mem_bound_cons_wrapper<OELEM_T> &wrapper) :
        r(wrapper.c.r),
        i(wrapper.c.i),
        references_scalar_array(wrapper.c.references_scalar_array)
    {
    }

    /*-------------------------------------------------------------------------
    * Constructor to initialize a ctype from a vreg_t
    *-------------------------------------------------------------------------*/
    explicit ctype(const vreg_t &src) :
        r(data[0]),
        i(data[1]),
        references_scalar_array(false)
    {
        init_ctype_from_vreg_t(src, *this);
    }

    /*-------------------------------------------------------------------------
    * Default constructor
    *-------------------------------------------------------------------------*/
    ctype() :
        r(data[0]),
        i(data[1]),
        references_scalar_array(false)
    {
        data[0] = 0;
        data[1] = 0;
    }

/*-----------------------------------------------------------------------------
* Public member accessor methods
*-----------------------------------------------------------------------------*/
public:
    /*-------------------------------------------------------------------------
    * get_data_arr_ptr -- Return pointer to ctype's underlying memory
    * 
    *   Important Note: The object's "underlying memory" depends on how the
    *   object was constructed. If this complex type object was constructed
    *   based on a pointer to memory, then effectively it always references that
    *   memory, and a pointer to that memory should be
    *   returned here.  On the other hand, if this object was not constructed
    *   based on a pointer to memory, then it always references its internal
    *   "data" array below.  Here we return the address to which r
    *   refers since that will always refer to the right location.
    *  
    *   Previously, there was a defect where the location of the instance's
    *   underlying "data" array was always returned. This was not correct for
    *   cases in which the instance was constructed based on a pointer to
    *   memory. (See CODEGEN-6186)
    *-------------------------------------------------------------------------*/
    ELEM_TYPE* get_data_arr_ptr()
    {
        return &(*this).r;
    }

    /*-------------------------------------------------------------------------
    * get_default_data_ptr -- used during default construction of a ctype
    *-------------------------------------------------------------------------*/
    ELEM_TYPE* get_default_data_ptr()
    {
        return data.data();
    }

/*-----------------------------------------------------------------------------
* Private member data
*-----------------------------------------------------------------------------*/
private:
    std::array<UQELEM_TYPE, 2> data;

/*-----------------------------------------------------------------------------
* Public member data
*-----------------------------------------------------------------------------*/
public:
    ELEM_T &r, &i;

    const bool references_scalar_array;

/*-----------------------------------------------------------------------------
* Public debug operations -- Will not compile on the target!
*-----------------------------------------------------------------------------*/
public:
    /*-------------------------------------------------------------------------
    * print -- display the contents of the complex type
    *-------------------------------------------------------------------------*/
    void print() const
    {
        using conv_t =
            typename std::conditional<(std::is_integral<ELEM_T>::value &&
                                sizeof(ELEM_T) < sizeof(int32_t)),
                                int32_t, ELEM_T>::type;

        std::cout << "{R = " << (conv_t)r << ", I = " << (conv_t)i << "}";
    }

/*-----------------------------------------------------------------------------
* Operators
*-----------------------------------------------------------------------------*/
public:
    /*-------------------------------------------------------------------------
    * Address-of
    *-------------------------------------------------------------------------*/
    ctype_ptr<ctype<ELEM_T> > operator &()
    {
        // Objects that inherit enable_shared_from_this<> must ensure that any
        // corresponding instantiated object of that type is owned by a shared
        // pointer.
        //
        // Problem: There are cases in which objects that inherit
        // enabled_shared_from_this<> are not dynamically-allocated and
        // therefore are not owned by a shared pointer. This is the case for
        // statically-allocated and automatically-allocated objects.  You can't
        // return a shared_ptr<> (actually a weak_ptr<>) to the current object
        // if it is not already owned by a shared_ptr<>. This is a known issue.
        //
        // Best Solution: Boost resolves a similar issue by creating a
        // temporary shared pointer with a custom null-deleter to manage a
        // shared_ptr to the object and avoid destroying the object when the
        // shared_ptr goes out of scope.  We will need to do something similar.
        // The temporary shared pointer owns the object if there are no other
        // shared pointers. The custom null-deleter ensures that the object
        // isn't destroyed when the temporary pointer goes out of function
        // scope. When the temporary pointer goes out of scope, the reference
        // count is properly decremented so that the object doesn't leak later.
        const auto wptr = std::shared_ptr<ctype<ELEM_T>>(this,
                                                  [](ctype<ELEM_T>*){});

        return ctype_ptr<ctype<ELEM_T> >(this->shared_from_this());
    }

    // Delete const version of operator
    ctype_ptr<ctype<ELEM_T> > operator &() const = delete;

    /*-------------------------------------------------------------------------
    * Assignment from ctype
    *-------------------------------------------------------------------------*/
    ctype<ELEM_T> &operator=(const ctype<ELEM_T> &rhs)
    {
        (*this).r = rhs.r;
        (*this).i = rhs.i;
        return (*this);
    }

    /*-------------------------------------------------------------------------
    * Cast to non-const (Important for conversion to rvalue)
    *-------------------------------------------------------------------------*/
    operator ctype<UQELEM_TYPE>() const
    {
        ctype<UQELEM_TYPE> new_comp;
        new_comp.r = (*this).r;
        new_comp.i = (*this).i;
        return new_comp;
    }

    /*-------------------------------------------------------------------------
    * Cast to vreg_t
    *-------------------------------------------------------------------------*/
    operator vreg_t() const
    {
        return convert_ctype_to_vreg_t<ELEM_T>(*this);
    }

    /*-------------------------------------------------------------------------
    * Pre-increment
    *-------------------------------------------------------------------------*/
    ctype<UQELEM_TYPE> operator++()
    {
        static_assert(!std::is_same<UQELEM_TYPE, float>::value
                      && !std::is_same<UQELEM_TYPE, double>::value,
		      "Error, float and double vector pre-increment is not supported");
        r = r + 1;
	return *this;
    }

    /*-------------------------------------------------------------------------
    * Post-increment
    *-------------------------------------------------------------------------*/
    ctype<UQELEM_TYPE> operator++(int)
    {
        static_assert(!std::is_same<UQELEM_TYPE, float>::value
                      && !std::is_same<UQELEM_TYPE, double>::value,
                      "Error, float and double vector post-increment is not supported");
        ctype<UQELEM_TYPE> copy;
        copy.r = r;
        ++*this;
        return copy;
    }

    /*-------------------------------------------------------------------------
    * Pre-decrement
    *-------------------------------------------------------------------------*/
    ctype<UQELEM_TYPE> operator--()
    {
        static_assert(!std::is_same<UQELEM_TYPE, float>::value
                      && !std::is_same<UQELEM_TYPE, double>::value,
                      "Error, float and double vector pre-decrement is not supported");
        r = r - 1;
        return *this;
    }

    /*-------------------------------------------------------------------------
    * Post-decrement
    *-------------------------------------------------------------------------*/
    ctype<UQELEM_TYPE> operator--(int)
    {
        static_assert(!std::is_same<UQELEM_TYPE, float>::value
                      && !std::is_same<UQELEM_TYPE, double>::value,
                      "Error, float and double vector post-decrement is not supported");
        ctype<UQELEM_TYPE> copy;
        copy.r = r;
        --*this;
        return copy;
    }

    /*-------------------------------------------------------------------------
    * Plus
    *-------------------------------------------------------------------------*/
    ctype<UQELEM_TYPE> operator+(const ctype<UQELEM_TYPE> &rhs) const
    {
        ctype<UQELEM_TYPE> res;
        res.r = (*this).r + rhs.r;
        res.i = (*this).i + rhs.i;
        return res;
    }

    /*-------------------------------------------------------------------------
    * Unary plus (effectively a copy operation)
    *-------------------------------------------------------------------------*/
    ctype<UQELEM_TYPE> operator+() const
    {
        return *this; 
    }

    /*-------------------------------------------------------------------------
    * Minus
    *-------------------------------------------------------------------------*/
    ctype<UQELEM_TYPE> operator-(const ctype<UQELEM_TYPE> &rhs) const
    {
        ctype<UQELEM_TYPE> res;
        res.r = (*this).r - rhs.r;
        res.i = (*this).i - rhs.i;
        return res;
    }

    /*-------------------------------------------------------------------------
    * Unary minus
    *-------------------------------------------------------------------------*/
    ctype<UQELEM_TYPE> operator-() const
    {
        ctype<UQELEM_TYPE> res;
        res.r = -(*this).r;
        res.i = -(*this).i;
        return res;
    }

    /*-------------------------------------------------------------------------
    * Multiply
    *-------------------------------------------------------------------------*/
    ctype<UQELEM_TYPE> operator*(const ctype<UQELEM_TYPE> &rhs) const
    {
        ctype<UQELEM_TYPE> res;
        res.r = (*this).r * rhs.r - ((*this).i * rhs.i);
        res.i = (*this).r * rhs.i + ((*this).i * rhs.r);
        return res;
    }

    /*-------------------------------------------------------------------------
    * Divide
    *-------------------------------------------------------------------------*/
    ctype<UQELEM_TYPE> operator/(const ctype<UQELEM_TYPE> &rhs) const
    {
        ctype<UQELEM_TYPE> res;
        res.r = ((*this).r * rhs.r + ((*this).i * rhs.i)) /
                    (rhs.r * rhs.r + rhs.i * rhs.i);
        res.i = ((*this).i * rhs.r + ((*this).r * rhs.i)) /
                    (rhs.r * rhs.r + rhs.i * rhs.i);
        return res;
    }

    /*-------------------------------------------------------------------------
    * Plus Assign
    *-------------------------------------------------------------------------*/
    ctype<UQELEM_TYPE> operator+=(const ctype<UQELEM_TYPE>& rhs)
    {
        (*this).r += rhs.r;
        (*this).i += rhs.i;
        return *this;
    }

    /*-------------------------------------------------------------------------
    * Minus Assign
    *-------------------------------------------------------------------------*/
    ctype<UQELEM_TYPE> operator-=(const ctype<UQELEM_TYPE>& rhs)
    {
        (*this).r -= rhs.r;
        (*this).i -= rhs.i;
        return *this;
    }

    /*-------------------------------------------------------------------------
    * Multiply Assign
    *-------------------------------------------------------------------------*/
    ctype<UQELEM_TYPE> operator*=(const ctype<UQELEM_TYPE>& rhs)
    {
        *this = *this * rhs;
        return *this;
    }

    /*-------------------------------------------------------------------------
    * Divide Assign
    *-------------------------------------------------------------------------*/
    ctype<UQELEM_TYPE> operator/=(const ctype<UQELEM_TYPE>& rhs)
    {
        *this = *this / rhs;
        return *this;
    }
};

/******************************************************************************
* t_ptr -- is used to construct vtype_ptr<> and ctype_ptr<> in an abstract way.
*   Other than the constructors, most of the operations, including those for
*   pointer arithmetic, are abstracted.
******************************************************************************/
template<typename OTYPE>
class t_ptr
{
/*-----------------------------------------------------------------------------
* Information Members
*-----------------------------------------------------------------------------*/
public:
    using ETYPE = typename OTYPE::ELEM_TYPE;

    // Base scalar type (Useful to identify component type of a complex type)
    using BTYPE = typename OTYPE::COMP_TYPE;

    // Component lane # (Useful to identify # of lanes in a complex vector)
    static constexpr size_t NUM_COMPS = OTYPE::NUM_COMPS;

/*-----------------------------------------------------------------------------
* Constructors
*-----------------------------------------------------------------------------*/
public:
    /*-------------------------------------------------------------------------
    * Default Constructor
    *-------------------------------------------------------------------------*/
    t_ptr<OTYPE>() :
        sptr(std::shared_ptr<OTYPE>(new OTYPE()))
    {}

    /*-------------------------------------------------------------------------
    * Constructor (via shared pointer)
    *-------------------------------------------------------------------------*/
    t_ptr<OTYPE>(std::shared_ptr<OTYPE> sptr) :
        sptr(sptr)
    {}

    /*-------------------------------------------------------------------------
    * Constructor (via pointer)
    *-------------------------------------------------------------------------*/
    t_ptr<OTYPE>(OTYPE *ptr) :
        sptr(ptr)
    {}

    /*-------------------------------------------------------------------------
    * Constructor (via NULL pointer)
    *-------------------------------------------------------------------------*/
    t_ptr<OTYPE>(std::nullptr_t ptr) :
        sptr(ptr)
    {}

/*-----------------------------------------------------------------------------
* Public debug operations -- Will not compile on target!
*-----------------------------------------------------------------------------*/
public:
    /*-------------------------------------------------------------------------
    * use_count -- get the use count of the underlying shared pointer.
    *-------------------------------------------------------------------------*/
    long use_count()
    {
        return sptr.use_count();
    }

/*-----------------------------------------------------------------------------
* Operators
*-----------------------------------------------------------------------------*/
public:
    /*-------------------------------------------------------------------------
    * Dereference
    *-------------------------------------------------------------------------*/
    OTYPE& operator*() const
    {
        return *sptr;
    }

    /*-------------------------------------------------------------------------
    * Cast to base scalar type pointer (such as int*)
    *-------------------------------------------------------------------------*/
    operator BTYPE *() const
    {
        return sptr->get_data_arr_ptr();
    }

    /*-------------------------------------------------------------------------
    * Cast to const void*
    *-------------------------------------------------------------------------*/
    operator const void *() const
    {
        return sptr->get_data_arr_ptr();
    }

    /*-------------------------------------------------------------------------
    * Cast to truth value (boolean)
    *   For code such as "if (ptr) { ..."
    *-------------------------------------------------------------------------*/
    operator bool() const
    { 
        return sptr->get_data_arr_ptr() != 0;
    }

#define CHECK_MEM_OOR(v) { if (!v->references_scalar_array) \
        throw std::out_of_range("Memory out of allowable range"); }

    /*-------------------------------------------------------------------------
    * Pre-increment
    *-------------------------------------------------------------------------*/
    t_ptr<OTYPE>& operator++()
    {
        CHECK_MEM_OOR(sptr)
        BTYPE* data = ((BTYPE*)((*sptr).get_data_arr_ptr())+NUM_COMPS);
        sptr = std::shared_ptr<OTYPE>(new OTYPE(data));
        return *this;
    }

    /*-------------------------------------------------------------------------
    * Post-increment
    *-------------------------------------------------------------------------*/
    t_ptr<OTYPE> operator++(int)
    {
        t_ptr<OTYPE> old_view(*this);
        operator++();
        return old_view;
    }

    /*-------------------------------------------------------------------------
    * Pre-decrement
    *-------------------------------------------------------------------------*/
    t_ptr<OTYPE>& operator--()
    {
        CHECK_MEM_OOR(sptr)
        BTYPE* data = ((BTYPE*)((*sptr).get_data_arr_ptr())-NUM_COMPS);
        sptr = std::shared_ptr<OTYPE>(new OTYPE(data));
        return *this;
    }

    /*-------------------------------------------------------------------------
    * Post-decrement
    *-------------------------------------------------------------------------*/
    t_ptr<OTYPE> operator--(int)
    {
        t_ptr<OTYPE> old_view(*this);
        operator--();
        return old_view;
    }

    /*-------------------------------------------------------------------------
    * Plus integral offset
    *-------------------------------------------------------------------------*/
    template<typename ITYPE,
             std::enable_if_t<std::is_integral<ITYPE>::value,int> = 0>
    t_ptr<OTYPE> operator+(ITYPE n) const
    {
        CHECK_MEM_OOR(sptr)
        BTYPE* data = ((BTYPE*)((*sptr).get_data_arr_ptr())+(NUM_COMPS*n));
        t_ptr<OTYPE> new_view(new OTYPE(data));
        return new_view;
    }

    /*-------------------------------------------------------------------------
    * Unary plus (effectively a copy operation)
    *-------------------------------------------------------------------------*/
    t_ptr<OTYPE> operator+()
    {
        CHECK_MEM_OOR(sptr)
        return (*this);
    }

    /*-------------------------------------------------------------------------
    * Minus integral offset
    *-------------------------------------------------------------------------*/
    template<typename ITYPE,
             std::enable_if_t<std::is_integral<ITYPE>::value,int> = 0>
    t_ptr<OTYPE> operator-(ITYPE n)
    {
        CHECK_MEM_OOR(sptr)
        BTYPE* data = ((BTYPE*)((*sptr).get_data_arr_ptr())-(NUM_COMPS*n));
        t_ptr<OTYPE> new_view(new OTYPE(data));
        return new_view;
    }

    /*-------------------------------------------------------------------------
    * Pointer difference (integer type result)
    *-------------------------------------------------------------------------*/
    template <typename TPTR,
              std::enable_if_t<std::is_same<typename deconst<ETYPE>::type,
                                            typename deconst<typename TPTR::ETYPE>::type>::value,
                               int> = 0>
    ptrdiff_t operator-(TPTR other) const
    {
        CHECK_MEM_OOR(sptr)
        BTYPE* mydata = (BTYPE*)((*sptr).get_data_arr_ptr());
        BTYPE* otherdata = (BTYPE*)(other->get_data_arr_ptr());
        return (mydata - otherdata)/NUM_COMPS;
    }

    /*-------------------------------------------------------------------------
    * Plus assign integral offset
    *-------------------------------------------------------------------------*/
    template<typename ITYPE,
             std::enable_if_t<std::is_integral<ITYPE>::value,int> = 0>
    t_ptr<OTYPE>& operator+=(ITYPE n)
    {
        CHECK_MEM_OOR(sptr)
        BTYPE* data = ((BTYPE*)((*sptr).get_data_arr_ptr())+(NUM_COMPS*n));
        sptr = std::shared_ptr<OTYPE>(new OTYPE(data));
        return *this;
    }

    /*-------------------------------------------------------------------------
    * Minus assign integral offset
    *-------------------------------------------------------------------------*/
    template<typename ITYPE,
             std::enable_if_t<std::is_integral<ITYPE>::value,int> = 0>
    t_ptr<OTYPE>& operator-=(ITYPE n)
    {
        CHECK_MEM_OOR(sptr)
        BTYPE* data = ((BTYPE*)((*sptr).get_data_arr_ptr())-(NUM_COMPS*n));
        sptr = std::shared_ptr<OTYPE>(new OTYPE(data));
        return *this;
    }

    /*-------------------------------------------------------------------------
    * Array index
    *-------------------------------------------------------------------------*/
    template<typename ITYPE,
             std::enable_if_t<std::is_integral<ITYPE>::value,int> = 0>
    OTYPE &operator[](ITYPE offset) const
    {
        CHECK_MEM_OOR(sptr)
        return *((*this) + offset);
    }

    /*-------------------------------------------------------------------------
    * Compare equal
    *-------------------------------------------------------------------------*/
    bool operator==(const t_ptr<OTYPE> &other) const
    {
        return ((const void*)this) == ((const void*)other);
    }

    /*-------------------------------------------------------------------------
    * Compare not equal
    *-------------------------------------------------------------------------*/
    bool operator!=(const t_ptr<OTYPE> &other) const
    {
        return ((const void*)this) != ((const void*)other);
    }

    /*-------------------------------------------------------------------------
    * Compare less than
    *-------------------------------------------------------------------------*/
    bool operator<(const t_ptr<OTYPE> &other) const
    {
        return ((const void*)this) < ((const void*)other);
    }

    /*-------------------------------------------------------------------------
    * Compare greater than
    *-------------------------------------------------------------------------*/
    bool operator>(const t_ptr<OTYPE> &other) const
    {
        return ((const void*)this) > ((const void*)other);
    }

    /*-------------------------------------------------------------------------
    * Compare less than or equal
    *-------------------------------------------------------------------------*/
    bool operator<=(const t_ptr<OTYPE> &other) const
    {
        return ((const void*)this) <= ((const void*)other);
    }

    /*-------------------------------------------------------------------------
    * Compare greater than or equal
    *-------------------------------------------------------------------------*/
    bool operator>=(const t_ptr<OTYPE> &other) const
    {
        return ((const void*)this) >= ((const void*)other);
    }

#undef CHECK_MEM_OOR

/*-----------------------------------------------------------------------------
* Protected data
*-----------------------------------------------------------------------------*/
protected:

    // Internally managed shared_ptr<> to a vector or complex pointer
    std::shared_ptr<OTYPE> sptr;
};

/*-----------------------------------------------------------------------------
* integer plus t_ptr operator; commuted version of operator+ in t_ptr.
*-----------------------------------------------------------------------------*/
template <typename OTYPE>
t_ptr<OTYPE> operator+(const int n, t_ptr<OTYPE> tp)
{
    return tp + n;
}

/******************************************************************************
* vtype_ptr -- Implementation of a pointer to a vector type.
******************************************************************************/
template<typename VTYPE>
class vtype_ptr : public t_ptr<VTYPE>
{
/*-----------------------------------------------------------------------------
* Information members
*-----------------------------------------------------------------------------*/
public:
    using ETYPE = typename VTYPE::ELEM_TYPE;
    using UQETYPE = typename deconst<ETYPE>::type;
    static constexpr size_t N = VTYPE::NUM_ELEM;

/*-----------------------------------------------------------------------------
* Constructors
*-----------------------------------------------------------------------------*/
public:
    /*-------------------------------------------------------------------------
    * Default Constructor
    *-------------------------------------------------------------------------*/
    vtype_ptr<VTYPE>() :
        t_ptr<VTYPE>(std::shared_ptr<VTYPE>(new VTYPE()))
    {}

    /*-------------------------------------------------------------------------
    * Constructor (via shared pointer)
    *-------------------------------------------------------------------------*/
    vtype_ptr<VTYPE>(std::shared_ptr<VTYPE> sptr) :
        t_ptr<VTYPE>(sptr)
    {}

    /*-------------------------------------------------------------------------
    * Constructor via t_ptr.  Used for implicit conversion back to vtype_ptr
    *   from t_ptr returning operations.
    *-------------------------------------------------------------------------*/
    vtype_ptr<VTYPE>(t_ptr<VTYPE> t) :
        t_ptr<VTYPE>((VTYPE*)(typename VTYPE::COMP_TYPE*)t)
    {}

    /*-------------------------------------------------------------------------
    * Constructor (via vector pointer)
    *-------------------------------------------------------------------------*/
    vtype_ptr<VTYPE>(VTYPE *ptr) :
        t_ptr<VTYPE>(ptr)
    {}

    /*-------------------------------------------------------------------------
    * Constructor via another vtype_ptr.  Used for implicit conversion from a
    *   compatible vector pointer type.  For example, from an int8_ptr to a
    *   const_int8_ptr.
    * NOTE: The generated vtype MUST reference the same data as the input
    *   vtype_ptr's vtype!  In other words, these pointers must point to the
    *   exact same data.
    *-------------------------------------------------------------------------*/
    template <typename OETYPE,
              size_t ON,
              typename ED = ETYPE,
              std::enable_if_t<std::is_same<typename deconst<ED>::type,
                                            typename deconst<OETYPE>::type>::value &&
                               ON == N, int> = 0>
    vtype_ptr<VTYPE>(const vtype_ptr<vtype<OETYPE, ON> > &ptr) :
        t_ptr<VTYPE>(std::shared_ptr<VTYPE>(new VTYPE(typename VTYPE::template mem_bound_cons_wrapper<OETYPE,ON>(*ptr))))
    {}

    /*-------------------------------------------------------------------------
    * Constructor (via NULL pointer)
    *-------------------------------------------------------------------------*/
    vtype_ptr<VTYPE>(std::nullptr_t ptr) :
        t_ptr<VTYPE>(ptr)
    {}

    /*-------------------------------------------------------------------------
    * Constructor (via scalar pointer, to facilitate C-style casting)
    *-------------------------------------------------------------------------*/
    vtype_ptr<VTYPE>(ETYPE *input) :
        t_ptr<VTYPE>(std::shared_ptr<VTYPE>(new VTYPE(input)))
    {}

    // Special scalar case: void*.  Explicit to disallow conversions from other
    // pointers.
    explicit vtype_ptr<VTYPE>(void *input) :
        t_ptr<VTYPE>(std::shared_ptr<VTYPE>(new VTYPE((typename VTYPE::COMP_TYPE*)input)))
    {}

    /*-------------------------------------------------------------------------
    * Constructor (via complex pointer)
    *-------------------------------------------------------------------------*/
    template<typename ELEM_DUMMY = ETYPE,
             enable_if_cmplx_t<ELEM_DUMMY, int> = 0>
    vtype_ptr<VTYPE>(typename ELEM_DUMMY::ELEM_TYPE *input) :
        t_ptr<VTYPE>(std::shared_ptr<VTYPE>(new VTYPE(input)))
    {}

/*-----------------------------------------------------------------------------
* Operators
*-----------------------------------------------------------------------------*/
public:
    /*-------------------------------------------------------------------------
    * Cast to complex element pointer (from complex vector pointer)
    *-------------------------------------------------------------------------*/
    template <typename ED = ETYPE,
              enable_if_cmplx_t<ED, int> = 0>
    operator ctype_ptr<ED>()
    {
        return this->sptr->get_cdata_arr_ptr();
    }
};

/******************************************************************************
* ctype_ptr -- Implementation of complex pointers (but not complex vector
*   pointers)
******************************************************************************/
template<typename CTYPE>
class ctype_ptr : public t_ptr<CTYPE>
{
/*-----------------------------------------------------------------------------
* Information members
*-----------------------------------------------------------------------------*/
public:
    using ETYPE = typename CTYPE::ELEM_TYPE;
    using UQETYPE = typename deconst<ETYPE>::type;

/*-----------------------------------------------------------------------------
* Constructors
*-----------------------------------------------------------------------------*/
public:
    /*-------------------------------------------------------------------------
    * Default Constructor
    *-------------------------------------------------------------------------*/
    ctype_ptr<CTYPE>() :
        t_ptr<CTYPE>(std::shared_ptr<CTYPE>(new CTYPE()))
    {}

    /*-------------------------------------------------------------------------
    * Constructor (via shared pointer)
    *-------------------------------------------------------------------------*/
    ctype_ptr<CTYPE>(std::shared_ptr<CTYPE> sptr) :
        t_ptr<CTYPE>(sptr)
    {}

    /*-------------------------------------------------------------------------
    * Constructor via t_ptr.  Used for implicit conversion back to ctype_ptr
    *   from t_ptr returning operations.
    *-------------------------------------------------------------------------*/
    ctype_ptr<CTYPE>(t_ptr<CTYPE> t) :
        t_ptr<CTYPE>((CTYPE*)(typename CTYPE::COMP_TYPE*)t) {};

    /*-------------------------------------------------------------------------
    * Constructor via another ctype_ptr.  Used for implicit conversion from a
    *   compatible complex pointer type.  For example, from an cint_ptr to a
    *   const_cint_ptr.
    * NOTE: The generated ctype MUST reference the same data as the input
    *   ctype_ptr's ctype!  In other words, these pointers must point to the
    *   exact same data.
    *-------------------------------------------------------------------------*/
    template <typename OETYPE,
              typename ED = ETYPE,
              std::enable_if_t<std::is_same<typename deconst<ED>::type,
                                            typename deconst<OETYPE>::type>::value, int> = 0>
    ctype_ptr<CTYPE>(const ctype_ptr<ctype<OETYPE> > &ptr) :
        t_ptr<CTYPE>(std::shared_ptr<CTYPE>(new CTYPE(typename CTYPE::template mem_bound_cons_wrapper<OETYPE>(*ptr))))
    {}

    /*-------------------------------------------------------------------------
    * Constructor via a compatible vtype_ptr.  Used for implicit conversion from
    *   a compatible complex pointer type.  For example, from an cint8_ptr to a
    *   cint_ptr.
    * NOTE: The generated ctype MUST reference the same data as the input
    *   vtype_ptr's ctype!  In other words, these pointers must point to the
    *   exact same data.
    *-------------------------------------------------------------------------*/
    template <typename OBTYPE,
              size_t ONELEMS,
              typename ED = ETYPE,
              std::enable_if_t<std::is_same<typename deconst<ED>::type,
                                            typename deconst<OBTYPE>::type>::value, int> = 0>
    ctype_ptr<CTYPE>(const vtype_ptr<vtype<ctype<OBTYPE>, ONELEMS> > &ptr) :
        t_ptr<CTYPE>(std::shared_ptr<CTYPE>(new CTYPE(typename CTYPE::template mem_bound_cons_wrapper<OBTYPE>((*ptr).s0))))
    {}

    /*-------------------------------------------------------------------------
    * Constructor (via complex pointer)
    *-------------------------------------------------------------------------*/
    ctype_ptr<CTYPE>(CTYPE *ptr) :
        t_ptr<CTYPE>(ptr)
    {}

    /*-------------------------------------------------------------------------
    * Constructor (via NULL pointer)
    *-------------------------------------------------------------------------*/
    ctype_ptr<CTYPE>(std::nullptr_t ptr) :
        t_ptr<CTYPE>(ptr)
    {}

    /*-------------------------------------------------------------------------
    * Constructor (via scalar pointer, to facilitate C-style casting)
    *-------------------------------------------------------------------------*/
    ctype_ptr<CTYPE>(ETYPE *input) :
        t_ptr<CTYPE>(std::shared_ptr<CTYPE>(new CTYPE(input)))
    {}

    // Special scalar case: void*.  Explicit to disallow conversions from other
    // pointers.
    explicit ctype_ptr<CTYPE>(void *input) :
        t_ptr<CTYPE>(std::shared_ptr<CTYPE>(new CTYPE((ETYPE*)input)))
    {}
};

/******************************************************************************
* __vpred -- Implementation of the vector predicate type
******************************************************************************/
class __vpred
{
/*-----------------------------------------------------------------------------
* Constructors
*-----------------------------------------------------------------------------*/
public:
    /*-------------------------------------------------------------------------
    * Default constructor
    *-------------------------------------------------------------------------*/
    __vpred() :
        pred(0)
    {}

    /*-------------------------------------------------------------------------
    * Constructor to initialize a __vpred from a vpred_t. A vpred_t is
    * a uint64_t used to represent vector predicates within C semantic
    * function definitions. Constructor must be explicit. Otherwise,
    * can do vpred = int which is not allowed on C7000
    *-------------------------------------------------------------------------*/
    explicit __vpred(vpred_t input) :
        pred(input)
    {}

    /*-------------------------------------------------------------------------
    * Cast to vpred_t
    *-------------------------------------------------------------------------*/
    operator vpred_t() const
    {
        return pred;
    }

/*-----------------------------------------------------------------------------
* Public debug operations -- Will not compile on the target!
*-----------------------------------------------------------------------------*/
public:
    /*-------------------------------------------------------------------------
    * print -- Display the predicate as a binary value of 64 bits
    *-------------------------------------------------------------------------*/
    void print()
    {
        std::cout << std::bitset<64>(pred) << std::endl;
    }

/*-----------------------------------------------------------------------------
* Private data
*-----------------------------------------------------------------------------*/
private:
    uint64_t pred;
};

/******************************************************************************
* bvtype -- Implementation of boolean vector type
*******************************************************************************/
template <size_t NELEM>
class bvtype
{
/*-----------------------------------------------------------------------------
* Information members
*-----------------------------------------------------------------------------*/
public:
    static constexpr size_t NUM_ELEM = NELEM;

/*-----------------------------------------------------------------------------
* Constructors
*-----------------------------------------------------------------------------*/
public:
    /*-------------------------------------------------------------------------
    * Default constructor
    *-------------------------------------------------------------------------*/
    bvtype() :
        pred(0)
    {}

    /*-------------------------------------------------------------------------
    * Constructor to initialize a bvtype from a vpred_t. A vpred_t is
    * a uint64_t used to represent vector predicates within C semantic
    * function definitions. Constructor must be explicit. Otherwise,
    * can do bvtype = int which is not allowed on C7000
    *-------------------------------------------------------------------------*/
    explicit bvtype(vpred_t input) :
        pred(input)
    {}

/*-----------------------------------------------------------------------------
* Operators
*-----------------------------------------------------------------------------*/
public:
    /*-------------------------------------------------------------------------
    * Cast to vpred_t
    *-------------------------------------------------------------------------*/
    operator vpred_t() const
    {
        return pred;
    }

/*-----------------------------------------------------------------------------
* Public debug operations -- Will not compile on the target!
*-----------------------------------------------------------------------------*/
public:
    /*-------------------------------------------------------------------------
    * print -- Display the predicate as a binary value of NUM_ELEM bits
    *-------------------------------------------------------------------------*/
    void print()
    {
        std::cout << std::bitset<NUM_ELEM>(pred) << std::endl;
    }

/*-----------------------------------------------------------------------------
* Private data
*-----------------------------------------------------------------------------*/
private:
    uint64_t pred;
};


/******************************************************************************
*
* Implementation of the accessible interface.
*
* An accessible object defines the following accessors:
*   - .sk for k in the range [0-9, a-f, A-F] to access the first 16 elements
*   - .x, .y, .z, and .w to access elements 0, 1, 2, and 3
*   - .hi, .lo, .even, and .odd to access partitions of the vector
*
* Limitations:
*   - OpenCL "swizzle" accessors, which combine multiple accesses into a
*     single member are not supported. Examples: vector.xyz, vector.s3210
*
******************************************************************************/

/*-----------------------------------------------------------------------------
* scalar_accessors -- The scalar_accessors class is one base for the derived,
*   common accessible interface that represents the dot-accessor syntax allowed
*   by OpenCL. It does this by linking ELEM_T references to references found in
*   the parent's reference wrapper array. Index calculations are already handled
*   in the ref_holder class.
*
*   Each valid scalar_accessor inherits from the previous, all the way to
*   NELEM == 64.
*-----------------------------------------------------------------------------*/
template <typename ELEM_T,
          size_t NELEM>
class scalar_accessors { };

// 2 element specialization
template<typename ELEM_T>
class scalar_accessors<ELEM_T, 2>
{
public:
    ELEM_T &x;
    ELEM_T &y;
public:
    ELEM_T &s0;
    ELEM_T &s1;
public:
    template<size_t N>
    scalar_accessors(std::array<std::reference_wrapper<ELEM_T>, N> &refs) :
        x(refs[0]),
        y(refs[1]),
        s0(refs[0]),
        s1(refs[1])
    {}
};

// 4 element specialization
template<typename ELEM_T>
class scalar_accessors<ELEM_T, 4> :
    public scalar_accessors<ELEM_T, 2>
{
public:
    ELEM_T &z;
    ELEM_T &w;
public:
    ELEM_T &s2;
    ELEM_T &s3;
public:
    template<size_t N>
    scalar_accessors(std::array<std::reference_wrapper<ELEM_T>, N> &refs) :
        scalar_accessors<ELEM_T, 2>(refs),
        z(refs[2]),
        w(refs[3]),
        s2(refs[2]),
        s3(refs[3])
    {}
};

// 8 element specialization
template<typename ELEM_T>
class scalar_accessors<ELEM_T, 8> :
    public scalar_accessors<ELEM_T, 4>
{
public:
    ELEM_T &s4;
    ELEM_T &s5;
    ELEM_T &s6;
    ELEM_T &s7;
public:
    template<size_t N>
    scalar_accessors(std::array<std::reference_wrapper<ELEM_T>, N> &refs) :
        scalar_accessors<ELEM_T, 4>(refs),
        s4(refs[4]),
        s5(refs[5]),
        s6(refs[6]),
        s7(refs[7])
    {}
};

// 16 element specialization
template<typename ELEM_T>
class scalar_accessors<ELEM_T, 16> :
    public scalar_accessors<ELEM_T, 8>
{
public:
    ELEM_T &s8;
    ELEM_T &s9;
    ELEM_T &sa;
    ELEM_T &sA;
    ELEM_T &sb;
    ELEM_T &sB;
    ELEM_T &sc;
    ELEM_T &sC;
    ELEM_T &sd;
    ELEM_T &sD;
    ELEM_T &se;
    ELEM_T &sE;
    ELEM_T &sf;
    ELEM_T &sF;
public:
    template<size_t N>
    scalar_accessors(std::array<std::reference_wrapper<ELEM_T>, N> &refs) :
        scalar_accessors<ELEM_T, 8>(refs),
        s8(refs[8]), 
        s9(refs[9]), 
        sa(refs[10]), sA(refs[10]),
        sb(refs[11]), sB(refs[11]),
        sc(refs[12]), sC(refs[12]),
        sd(refs[13]), sD(refs[13]),
        se(refs[14]), sE(refs[14]),
        sf(refs[15]), sF(refs[15])
    {}
};

// 32 element specialization
template<typename ELEM_T>
class scalar_accessors<ELEM_T, 32> :
    public scalar_accessors<ELEM_T, 16>
{
public:
    template<size_t N>
    scalar_accessors(std::array<std::reference_wrapper<ELEM_T>, N> &refs) :
        scalar_accessors<ELEM_T, 16>(refs)
    {}
};

// 64 element specialization
template<typename ELEM_T>
class scalar_accessors<ELEM_T, 64> :
    public scalar_accessors<ELEM_T, 32>
{
public:
    template<size_t N>
    scalar_accessors(std::array<std::reference_wrapper<ELEM_T>, N> &refs) :
        scalar_accessors<ELEM_T, 32>(refs)
    {}
};

/*-----------------------------------------------------------------------------
* complex_accessors -- Helper class which is instantiated conditionally by an
*   instance of an accessible object. Contains subvector accessors real and
*   imag. Instantiated by accessibles with complex element types.
*
* Note: complex_accessors also need to have a notion of depth, so that
*   "cchar32.lo.lo.r" is still recognized as being "2-deep" and
*   "cchar32.lo.lo.r.lo" isn't supported when the depth is limited to 2.
*   Otherwise we end up with memory bloat for complex vectors.
*-----------------------------------------------------------------------------*/
template <typename ELEM_T,
          size_t NELEM,
          size_t DEPTH,
          typename ENABLED = void>
class complex_accessors;

// Arithmetic specialization -- Accessibles whose element types are not complex
// will have no complex accessors.
template <typename ELEM_T,
          size_t NELEM,
          size_t DEPTH>
class complex_accessors<ELEM_T,
                        NELEM,
                        DEPTH,
                        std::enable_if_t<std::is_arithmetic<ELEM_T>::value> >
{
public:
    complex_accessors(std::array<std::reference_wrapper<ELEM_T>, NELEM> &refs)
    {
        (void)refs;
    }
};

// Complex specialization -- Accessibles with complex element types have r and i
// members.  Note that single complex element r and i accessors are defined
// within the ctype class.
template <typename ELEM_T,
          size_t NELEM,
          size_t DEPTH>
class complex_accessors<ELEM_T,
                        NELEM,
                        DEPTH,
                        enable_if_cmplx_t<ELEM_T, void>>
{
public:
    accessible<typename ELEM_T::ELEM_TYPE, NELEM, DEPTH> r;
    accessible<typename ELEM_T::ELEM_TYPE, NELEM, DEPTH> i;
public:
    complex_accessors(std::array<std::reference_wrapper<ELEM_T>, NELEM> &refs) :
        r(refs, access_types<ACCESS_TYPE::REAL>()),
        i(refs, access_types<ACCESS_TYPE::IMAG>())
    {}
};

/*-----------------------------------------------------------------------------
* subvector_accessors -- Helper class which is instantiated conditionally by an
*   instance of an accessible object. Contains subvector accessors lo, hi, even,
*   and odd.
*-----------------------------------------------------------------------------*/
template <typename ELEM_T,
          size_t NELEM,
          size_t DEPTH,
          typename ENABLED = void>
class subvector_accessors
{
public:
    accessible<ELEM_T, NELEM / 2, DEPTH + 1>   lo;
    accessible<ELEM_T, NELEM / 2, DEPTH + 1>   hi;
    accessible<ELEM_T, NELEM / 2, DEPTH + 1> even;
    accessible<ELEM_T, NELEM / 2, DEPTH + 1>  odd;
public:
    subvector_accessors(std::array<std::reference_wrapper<ELEM_T>, NELEM> &refs) :
        lo(refs, access_types<ACCESS_TYPE::LO>()),
        hi(refs, access_types<ACCESS_TYPE::HI>()),
        even(refs, access_types<ACCESS_TYPE::EVEN>()),
        odd(refs, access_types<ACCESS_TYPE::ODD>())
    {}
};

// Two element specialization.  Acts as a termination condition: We've reached
// scalar elements.
template <typename ELEM_T,
          size_t DEPTH>
class subvector_accessors<ELEM_T, 2, DEPTH>
{
public:
    ELEM_T &lo;
    ELEM_T &hi;
    ELEM_T &even;
    ELEM_T &odd;
public:
    subvector_accessors(std::array<std::reference_wrapper<ELEM_T>, 2> &refs) :
        lo(refs[0]), hi(refs[1]), even(refs[0]), odd(refs[1])
    {}
};

// Max depth specialization.  Acts as a termination condition: We've reached the
// maximum configured depth.  Use enable_if to avoid ambiguity with the 2-element
// subvector->scalar termination condition above by disabling this condition for
// 2-element vectors.
template <typename ELEM_T,
          size_t NELEM>
class subvector_accessors<ELEM_T,
                          NELEM,
                          __TI_MAX_ACCESSOR_DEPTH,
                          std::enable_if_t<NELEM != 2> >
{
public:
    subvector_accessors(std::array<std::reference_wrapper<ELEM_T>, NELEM> &refs)
    {
        (void)refs;
    }
};

/******************************************************************************
* s_impl -- Struct used to allow for TI extension .s[k] syntax. All accessibles
*   will initialize an s_impl object.
******************************************************************************/
template<typename ELEM_T,
         size_t NELEM>
struct s_impl
{
    /*-------------------------------------------------------------------------
    * Array index operator -- used to retrieve data from a reference wrapper at
    * an index.
    *-------------------------------------------------------------------------*/
    ELEM_T& operator[](size_t index) const
    {
        return refs_s[index].get();
    }

    // Reference to array of references
    std::array<std::reference_wrapper<ELEM_T>, NELEM>& refs_s;

    /*-------------------------------------------------------------------------
    * Constructor -- Initialize reference wrapper array refs_s using passed in
    * array of reference wrappers.
    *-------------------------------------------------------------------------*/
    s_impl(std::array<std::reference_wrapper<ELEM_T>, NELEM> &refs) :
           refs_s(refs)
    {}

};

/*-----------------------------------------------------------------------------
* accessible
*
*   Inherits from:
*     - ref_holder
*         Responsible for creating reference wrapper array with references
*         to data based on ACCESS_TYPE
*     - subvector_accessors
*         Provides access to the lo, hi, even, and odd members, which are each
*         themselves accessibles or ELEM_T& members
*     - scalar_accessors
*         Provides the ELEM_T& members s0-sf
*     - complex_accessors
*         Provides the ELEM_T& members r,i
*     - enable_shared_from_this<>
*         Allows a shared_ptr of an object's instance to be returned.
*         Must be given the "most-derived-class" (i.e. "vtype<>")
*-----------------------------------------------------------------------------*/
template <typename ELEM_T,
          size_t NELEM,
          size_t DEPTH>
class accessible :
    public ref_holder<ELEM_T, NELEM>,
    public subvector_accessors<ELEM_T, NELEM, DEPTH>,
    public scalar_accessors<ELEM_T, NELEM>,
    public complex_accessors<ELEM_T, NELEM, DEPTH>,
    public std::enable_shared_from_this<vtype<ELEM_T, NELEM>>
{
/*-----------------------------------------------------------------------------
* Information members
*-----------------------------------------------------------------------------*/
private:
    using UQELEM_TYPE = typename deconst<ELEM_T>::type;
    using ACCESS_T = accessible<ELEM_T, NELEM, DEPTH>;

    template <size_t OTHER_DEPTH>
    using EQUIV_ACCESS_T = accessible<ELEM_T, NELEM, OTHER_DEPTH>;

    using SUBVECTOR_ACCESSOR_T = subvector_accessors<ELEM_T, NELEM, DEPTH>;

    using SCALAR_ACCESSOR_T = scalar_accessors<ELEM_T, NELEM>;

    using COMPLEX_ACCESSOR_T = complex_accessors<ELEM_T, NELEM, DEPTH>;

    using REF_HOLDER_T = ref_holder<ELEM_T, NELEM>;

    using ref_holder<ELEM_T, NELEM>::refs_m;

/*-----------------------------------------------------------------------------
* Constructors
*-----------------------------------------------------------------------------*/
public:
    /*-------------------------------------------------------------------------
    * Constructor via an array of elements.  (Used from vtype.)
    *-------------------------------------------------------------------------*/
    accessible(std::array<ELEM_T, NELEM> &data,
               access_types<ACCESS_TYPE::NORMAL>) :
        REF_HOLDER_T(data,
                     access_types<ACCESS_TYPE::NORMAL>(),
                     std::make_index_sequence<NELEM>()),
        SUBVECTOR_ACCESSOR_T(refs_m),
        SCALAR_ACCESSOR_T(refs_m),
        COMPLEX_ACCESSOR_T(refs_m),
        s(refs_m)
    {}

    /*-------------------------------------------------------------------------
    * Constructor via an array of reference wrappers.  (Used from vtype during
    * a subvector access.)
    *-------------------------------------------------------------------------*/
    accessible(std::array<std::reference_wrapper<ELEM_T>, NELEM> &refs,
               access_types<ACCESS_TYPE::NORMAL>) :
        REF_HOLDER_T(refs,
                     access_types<ACCESS_TYPE::NORMAL>(),
                     std::make_index_sequence<NELEM>()),
        SUBVECTOR_ACCESSOR_T(refs_m),
        SCALAR_ACCESSOR_T(refs_m),
        COMPLEX_ACCESSOR_T(refs_m),
        s(refs_m)
    {}

    /*-------------------------------------------------------------------------
    * Constructor via an array of non-const elements.  (Used from vtype to
    * create a vtype with duplicated references.)
    *-------------------------------------------------------------------------*/
    template<typename DE = ELEM_T,
             std::enable_if_t<!std::is_same<typename deconst<DE>::type, DE>::value, int> = 0>
    accessible(std::array<std::reference_wrapper<typename deconst<ELEM_T>::type>, NELEM> &refs,
               access_types<ACCESS_TYPE::NORMAL>) :
        REF_HOLDER_T(refs,
                     access_types<ACCESS_TYPE::NORMAL>(),
                     std::make_index_sequence<NELEM>()),
        SUBVECTOR_ACCESSOR_T(refs_m),
        SCALAR_ACCESSOR_T(refs_m),
        COMPLEX_ACCESSOR_T(refs_m),
        s(refs_m)
    {}

    /*-------------------------------------------------------------------------
    * Constructor is used to initialize a HI accessible
    *-------------------------------------------------------------------------*/
    accessible(std::array<std::reference_wrapper<ELEM_T>, NELEM*2> &refs,
               access_types<ACCESS_TYPE::HI>) :
        REF_HOLDER_T(refs,
                     access_types<ACCESS_TYPE::HI>(),
                     std::make_index_sequence<NELEM>()),
        SUBVECTOR_ACCESSOR_T(refs_m),
        SCALAR_ACCESSOR_T(refs_m),
        COMPLEX_ACCESSOR_T(refs_m),
        s(refs_m)
    {}

    /*-------------------------------------------------------------------------
    * Constructor is used to initialize a LO accessible
    *-------------------------------------------------------------------------*/
    accessible(std::array<std::reference_wrapper<ELEM_T>, NELEM*2> &refs,
               access_types<ACCESS_TYPE::LO>) :
        REF_HOLDER_T(refs,
                     access_types<ACCESS_TYPE::LO>(),
                     std::make_index_sequence<NELEM>()),
        SUBVECTOR_ACCESSOR_T(refs_m),
        SCALAR_ACCESSOR_T(refs_m),
        COMPLEX_ACCESSOR_T(refs_m),
        s(refs_m) 
    {}

    /*-------------------------------------------------------------------------
    * Constructor is used to initialize an EVEN accessible
    *-------------------------------------------------------------------------*/
    accessible(std::array<std::reference_wrapper<ELEM_T>, NELEM*2> &refs,
               access_types<ACCESS_TYPE::EVEN>) :
        REF_HOLDER_T(refs,
                     access_types<ACCESS_TYPE::EVEN>(),
                     std::make_index_sequence<NELEM>()),
        SUBVECTOR_ACCESSOR_T(refs_m),
        SCALAR_ACCESSOR_T(refs_m),
        COMPLEX_ACCESSOR_T(refs_m),
        s(refs_m)
    {}

    /*-------------------------------------------------------------------------
    * Constructor is used to initialize an ODD accessible
    *-------------------------------------------------------------------------*/
    accessible(std::array<std::reference_wrapper<ELEM_T>, NELEM*2> &refs,
               access_types<ACCESS_TYPE::ODD>) :
        REF_HOLDER_T(refs,
                     access_types<ACCESS_TYPE::ODD>(),
                     std::make_index_sequence<NELEM>()),
        SUBVECTOR_ACCESSOR_T(refs_m),
        SCALAR_ACCESSOR_T(refs_m),
        COMPLEX_ACCESSOR_T(refs_m),
        s(refs_m)
    {}

    /*-------------------------------------------------------------------------
    * Constructor is used to initialize a REAL accessible
    *-------------------------------------------------------------------------*/
    accessible(std::array<std::reference_wrapper<ctype<ELEM_T>>, NELEM> &refs,
               access_types<ACCESS_TYPE::REAL>) :
        REF_HOLDER_T(refs, 
                     access_types<ACCESS_TYPE::REAL>(),
                     std::make_index_sequence<NELEM>()),
        SUBVECTOR_ACCESSOR_T(refs_m),
        SCALAR_ACCESSOR_T(refs_m),
        COMPLEX_ACCESSOR_T(refs_m),
        s(refs_m)
    {}

    /*-------------------------------------------------------------------------
    * Constructor is used to initialize an IMAG accessible
    *-------------------------------------------------------------------------*/
    accessible(std::array<std::reference_wrapper<ctype<ELEM_T>>, NELEM> &refs,
               access_types<ACCESS_TYPE::IMAG>) :
        REF_HOLDER_T(refs,
                     access_types<ACCESS_TYPE::IMAG>(),
                     std::make_index_sequence<NELEM>()),
        SUBVECTOR_ACCESSOR_T(refs_m),
        SCALAR_ACCESSOR_T(refs_m),
        COMPLEX_ACCESSOR_T(refs_m),
        s(refs_m)
    {}

    /*-------------------------------------------------------------------------
    * Constructor via a pointer to an array
    *-------------------------------------------------------------------------*/
    accessible(ELEM_T *ptr,
               access_types<ACCESS_TYPE::NORMAL>) :
        REF_HOLDER_T(ptr,
                     access_types<ACCESS_TYPE::NORMAL>(),
                     std::make_index_sequence<NELEM>()),
        SUBVECTOR_ACCESSOR_T(refs_m),
        SCALAR_ACCESSOR_T(refs_m),
        COMPLEX_ACCESSOR_T(refs_m),
        s(refs_m)
    {}

    /*-------------------------------------------------------------------------
    * Constructor via a pointer to an array.  (Differs from above in that ELEM_T
    * is a complex type, not a scalar type.
    *-------------------------------------------------------------------------*/
    template<typename ELEM_DUMMY = ELEM_T,
             enable_if_cmplx_t<ELEM_DUMMY, int> = 0>
    accessible(typename ELEM_DUMMY::ELEM_TYPE* ptr,
               access_types<ACCESS_TYPE::NORMAL>) :
        REF_HOLDER_T(ptr,
                     access_types<ACCESS_TYPE::NORMAL>(),
                     std::make_index_sequence<NELEM>()),
        SUBVECTOR_ACCESSOR_T(refs_m),
        SCALAR_ACCESSOR_T(refs_m),
        COMPLEX_ACCESSOR_T(refs_m),
        s(refs_m)
    {}

    // You should not be able to construct an accessible on its own
    accessible(const accessible &a)  = delete;
    accessible(accessible &&a) = delete;

/*-----------------------------------------------------------------------------
* Public Members
*----------------------------------------------------------------------------*/
public:
    // Allows for TI syntax .s[k]
    struct s_impl<ELEM_T, NELEM> s;

/*-----------------------------------------------------------------------------
* Operators
*-----------------------------------------------------------------------------*/
public:
    /*-------------------------------------------------------------------------
    * Cast to vreg_t
    *-------------------------------------------------------------------------*/
    operator vreg_t() const
    {
        return convert_to_vreg_t<ACCESS_T, ELEM_T, NELEM>(*this);
    }

    /*-------------------------------------------------------------------------
    * Assign from compatible accessible at another depth
    *-------------------------------------------------------------------------*/
    template <size_t OTHER_DEPTH>
    ACCESS_T &operator=(const EQUIV_ACCESS_T<OTHER_DEPTH> &rhs)
    {
        for (size_t i = 0; i < NELEM; i++)
            s[i] = rhs.s[i];

        return *this;
    }

    /*-------------------------------------------------------------------------
    * Assign from compatible accessible. (required as copy assignment operator)
    *-------------------------------------------------------------------------*/
    ACCESS_T &operator=(const ACCESS_T &rhs)
    {
        for (size_t i = 0; i < NELEM; i++)
            s[i] = rhs.s[i];
        return *this;
    }

    /*-------------------------------------------------------------------------
    * Assign from scalar
    *-------------------------------------------------------------------------*/
    ACCESS_T &operator=(const ELEM_T &rhs)
    {
        for (size_t i = 0; i < NELEM; i++)
            s[i] = rhs;
        return *this;
    }

    /*-------------------------------------------------------------------------
    * Unary minus
    *-------------------------------------------------------------------------*/
    vtype<UQELEM_TYPE, NELEM> operator-()
    {
        vtype<UQELEM_TYPE,NELEM> res;
        for (int i = 0; i < NELEM; i++)
            res.s[i] = -((*this).s[i]);
        return res;
    }

    /*-------------------------------------------------------------------------
    * Unary plus
    *-------------------------------------------------------------------------*/
    vtype<UQELEM_TYPE, NELEM> operator+()
    {
        vtype<UQELEM_TYPE,NELEM> res;
        for (int i = 0; i < NELEM; i++)
            res.s[i] = +((*this).s[i]);
        return res;
    }

    /*-------------------------------------------------------------------------
    * Pre-increment
    *-------------------------------------------------------------------------*/
    ACCESS_T &operator++()
    {
        static_assert(!std::is_same<ELEM_T, float>::value
                      && !std::is_same<ELEM_T, double>::value,
		      "Error, float and double vector pre-increment is not supported");
	for (size_t i = 0; i < NELEM; i++)
			s[i] = s[i] + 1;
	return *this;
    }

    /*-------------------------------------------------------------------------
    * Post-increment
    *-------------------------------------------------------------------------*/
    vtype<UQELEM_TYPE, NELEM> operator++(int)
    {
        static_assert(!std::is_same<ELEM_T, float>::value
                      && !std::is_same<ELEM_T, double>::value,
                      "Error, float and double vector post-increment is not supported");
        vtype<ELEM_T, NELEM> copy;
        for (size_t i = 0; i < NELEM; i++)
                copy.s[i] = s[i];
        ++*this;
        return copy;
    }

    /*-------------------------------------------------------------------------
    * Pre-decrement
    *-------------------------------------------------------------------------*/
    ACCESS_T &operator--()
    {
        static_assert(!std::is_same<ELEM_T, float>::value
                      && !std::is_same<ELEM_T, double>::value,
                      "Error, float and double vector pre-decrement is not supported");
        for (size_t i = 0; i < NELEM; i++)
                s[i] = s[i] - 1;
        return *this;
    }

    /*-------------------------------------------------------------------------
    * Post-decrement
    *-------------------------------------------------------------------------*/
    vtype<UQELEM_TYPE, NELEM> operator--(int)
    {
        static_assert(!std::is_same<ELEM_T, float>::value
                      && !std::is_same<ELEM_T, double>::value,
                      "Error, float and double vector post-decrement is not supported");
        vtype<ELEM_T, NELEM> copy;
        for (size_t i = 0; i < NELEM; i++)
                copy.s[i] = s[i];
        --*this;
        return copy;
    }

    /*-------------------------------------------------------------------------
    * Bitwise not
    *-------------------------------------------------------------------------*/
    vtype<UQELEM_TYPE, NELEM> operator~()
    {
        static_assert(!std::is_same<ELEM_T, float>::value
                      && !std::is_same<ELEM_T, double>::value,
                      "Error, float and double vector bitwise ~ is not supported");
        vtype<UQELEM_TYPE, NELEM> res;
        for (size_t i = 0; i < NELEM; i++)
                res.s[i] = ~s[i];
        return res;
    }

    /*-------------------------------------------------------------------------
    * Plus assign with an accessible
    *-------------------------------------------------------------------------*/
    template <typename OELEM_T,
              size_t OTHER_DEPTH,
              typename ED = ELEM_T,
              std::enable_if_t<std::is_same<typename deconst<OELEM_T>::type,
                                            typename deconst<ED>::type>::value, int> = 0>
    ACCESS_T &operator+=(const accessible<OELEM_T,NELEM,OTHER_DEPTH> &rhs)
    {
        for (size_t i = 0; i < NELEM; i++)
                (*this).s[i] += rhs.s[i];
        return *this;
    }

    /*-------------------------------------------------------------------------
    * Plus assign with a scalar
    *-------------------------------------------------------------------------*/
    ACCESS_T &operator+=(const ELEM_T& rhs)
    {
        for (size_t i = 0; i < NELEM; i++)
                (*this).s[i] += rhs;
        return *this;
    }

    /*-------------------------------------------------------------------------
    * Minus assign with an accessible
    *-------------------------------------------------------------------------*/
    template <typename OELEM_T,
              size_t OTHER_DEPTH,
              typename ED = ELEM_T,
              std::enable_if_t<std::is_same<typename deconst<OELEM_T>::type,
                                            typename deconst<ED>::type>::value, int> = 0>
    ACCESS_T &operator-=(const accessible<OELEM_T,NELEM,OTHER_DEPTH> &rhs)
    {
        for (size_t i = 0; i < NELEM; i++)
                (*this).s[i] -= rhs.s[i];
        return *this;
    }

    /*-------------------------------------------------------------------------
    * Minus assign with a scalar
    *-------------------------------------------------------------------------*/
    ACCESS_T &operator-=(const ELEM_T& rhs)
    {
        for (size_t i = 0; i < NELEM; i++)
                (*this).s[i] -= rhs;
        return *this;
    }

    /*-------------------------------------------------------------------------
    * Multiply assign with an accessible
    *-------------------------------------------------------------------------*/
    template <typename OELEM_T,
              size_t OTHER_DEPTH,
              typename ED = ELEM_T,
              std::enable_if_t<std::is_same<typename deconst<OELEM_T>::type,
                                            typename deconst<ED>::type>::value, int> = 0>
    ACCESS_T &operator*=(const accessible<OELEM_T,NELEM,OTHER_DEPTH> &rhs)
    {
        for (size_t i = 0; i < NELEM; i++)
                (*this).s[i] *= rhs.s[i];
        return *this;
    }

    /*-------------------------------------------------------------------------
    * Multiply assign with a scalar
    *-------------------------------------------------------------------------*/
    ACCESS_T &operator*=(const ELEM_T& rhs)
    {
        for (size_t i = 0; i < NELEM; i++)
                (*this).s[i] *= rhs;
        return *this;
    }

    /*-------------------------------------------------------------------------
    * Divide assign with an accessible
    *-------------------------------------------------------------------------*/
    template <typename OELEM_T,
              size_t OTHER_DEPTH,
              typename ED = ELEM_T,
              std::enable_if_t<std::is_same<typename deconst<OELEM_T>::type,
                                            typename deconst<ED>::type>::value, int> = 0>
    ACCESS_T &operator/=(const accessible<OELEM_T,NELEM,OTHER_DEPTH> &rhs)
    {
        for (size_t i = 0; i < NELEM; i++)
                (*this).s[i] /= rhs.s[i];
        return *this;
    }

    /*-------------------------------------------------------------------------
    * Divide assign with a scalar
    *-------------------------------------------------------------------------*/
    ACCESS_T &operator/=(const ELEM_T& rhs)
    {
        for (size_t i = 0; i < NELEM; i++)
                (*this).s[i] /= rhs;
        return *this;
    }

    /*-------------------------------------------------------------------------
    * Modulus assign with an accessible
    *-------------------------------------------------------------------------*/
    template <typename OELEM_T,
              size_t OTHER_DEPTH,
              typename ED = ELEM_T,
              std::enable_if_t<std::is_same<typename deconst<OELEM_T>::type,
                                            typename deconst<ED>::type>::value, int> = 0>
    ACCESS_T &operator%=(const accessible<OELEM_T,NELEM,OTHER_DEPTH> &rhs)
    {
        static_assert(!std::is_same<ELEM_T, float>::value
                      && !std::is_same<ELEM_T, double>::value,
                      "Error, float and double vec to vec %= is not supported");

        for (size_t i = 0; i < NELEM; i++)
                (*this).s[i] %= rhs.s[i];
        return *this;
    }

    /*-------------------------------------------------------------------------
    * Modulus assign with a scalar
    *-------------------------------------------------------------------------*/
    ACCESS_T &operator%=(const ELEM_T& rhs)
    {

        static_assert(!std::is_same<ELEM_T, float>::value
                      && !std::is_same<ELEM_T, double>::value,
                      "Error, float and double vec to scalar %= is not supported");

        for (size_t i = 0; i < NELEM; i++)
                (*this).s[i] %= rhs;
        return *this;
    }

    /*-------------------------------------------------------------------------
    * Right shift assign with an accessible
    *-------------------------------------------------------------------------*/
    template <typename OELEM_T,
              size_t OTHER_DEPTH,
              typename ED = ELEM_T,
              std::enable_if_t<std::is_same<typename deconst<OELEM_T>::type,
                                            typename deconst<ED>::type>::value, int> = 0>
    ACCESS_T &operator>>=(const accessible<OELEM_T,NELEM,OTHER_DEPTH> &rhs)
    {
        static_assert(!std::is_same<ELEM_T, float>::value
                      && !std::is_same<ELEM_T, double>::value,
                      "Error, float and double vec to vec >>= is not supported");

        for (size_t i = 0; i < NELEM; i++)
                (*this).s[i] >>= rhs.s[i];
        return *this;
    }

    /*-------------------------------------------------------------------------
    * Right shift assign with a scalar
    *-------------------------------------------------------------------------*/
    ACCESS_T &operator>>=(const ELEM_T& rhs)
    {

        static_assert(!std::is_same<ELEM_T, float>::value
                      && !std::is_same<ELEM_T, double>::value,
                      "Error, float and double vec to scalar >>= is not supported");

        for (size_t i = 0; i < NELEM; i++)
                (*this).s[i] >>= rhs;
        return *this;
    }

    /*-------------------------------------------------------------------------
    * Left shift assign with an accessible
    *-------------------------------------------------------------------------*/
    template <typename OELEM_T,
              size_t OTHER_DEPTH,
              typename ED = ELEM_T,
              std::enable_if_t<std::is_same<typename deconst<OELEM_T>::type,
                                            typename deconst<ED>::type>::value, int> = 0>
    ACCESS_T &operator<<=(const accessible<OELEM_T,NELEM,OTHER_DEPTH> &rhs)
    {
        static_assert(!std::is_same<ELEM_T, float>::value
                      && !std::is_same<ELEM_T, double>::value,
                      "Error, float and double vec to vec <<= is not supported");

        for (size_t i = 0; i < NELEM; i++)
                (*this).s[i] <<= rhs.s[i];
        return *this;
    }

    /*-------------------------------------------------------------------------
    * Left shift assign with a scalar
    *-------------------------------------------------------------------------*/
    ACCESS_T &operator<<=(const ELEM_T& rhs)
    {

        static_assert(!std::is_same<ELEM_T, float>::value
                      && !std::is_same<ELEM_T, double>::value,
                      "Error, float and double vec to scalar <<= is not supported");

        for (size_t i = 0; i < NELEM; i++)
                (*this).s[i] <<= rhs;
        return *this;
    }

    /*-------------------------------------------------------------------------
    * Bitwise and assign with an accessible
    *-------------------------------------------------------------------------*/
    template <typename OELEM_T,
              size_t OTHER_DEPTH,
              typename ED = ELEM_T,
              std::enable_if_t<std::is_same<typename deconst<OELEM_T>::type,
                                            typename deconst<ED>::type>::value, int> = 0>
    ACCESS_T &operator&=(const accessible<OELEM_T,NELEM,OTHER_DEPTH> &rhs)
    {
        static_assert(!std::is_same<ELEM_T, float>::value
                      && !std::is_same<ELEM_T, double>::value,
                      "Error, float and double vec to vec &= is not supported");

        for (size_t i = 0; i < NELEM; i++)
                (*this).s[i] &= rhs.s[i];
        return *this;
    }

    /*-------------------------------------------------------------------------
    * Bitwise and assign with a scalar
    *-------------------------------------------------------------------------*/
    ACCESS_T &operator&=(const ELEM_T& rhs)
    {

        static_assert(!std::is_same<ELEM_T, float>::value
                      && !std::is_same<ELEM_T, double>::value,
                      "Error, float and double vec to scalar &= is not supported");

        for (size_t i = 0; i < NELEM; i++)
                (*this).s[i] &= rhs;
        return *this;
    }

    /*-------------------------------------------------------------------------
    * Bitwise or assign with an accessible
    *-------------------------------------------------------------------------*/
    template <typename OELEM_T,
              size_t OTHER_DEPTH,
              typename ED = ELEM_T,
              std::enable_if_t<std::is_same<typename deconst<OELEM_T>::type,
                                            typename deconst<ED>::type>::value, int> = 0>
    ACCESS_T &operator|=(const accessible<OELEM_T,NELEM,OTHER_DEPTH> &rhs)
    {
        static_assert(!std::is_same<ELEM_T, float>::value
                      && !std::is_same<ELEM_T, double>::value,
                      "Error, float and double vec to vec |= is not supported");

        for (size_t i = 0; i < NELEM; i++)
                (*this).s[i] |= rhs.s[i];
        return *this;
    }

    /*-------------------------------------------------------------------------
    * Bitwise or assign with a scalar
    *-------------------------------------------------------------------------*/
    ACCESS_T &operator|=(const ELEM_T& rhs)
    {

        static_assert(!std::is_same<ELEM_T, float>::value
                      && !std::is_same<ELEM_T, double>::value,
                      "Error, float and double vec to scalar |= is not supported");

        for (size_t i = 0; i < NELEM; i++)
                (*this).s[i] |= rhs;
        return *this;
    }

    /*-------------------------------------------------------------------------
    * Bitwise xor assign with an accessible
    *-------------------------------------------------------------------------*/
    template <typename OELEM_T,
              size_t OTHER_DEPTH,
              typename ED = ELEM_T,
              std::enable_if_t<std::is_same<typename deconst<OELEM_T>::type,
                                            typename deconst<ED>::type>::value, int> = 0>
    ACCESS_T &operator^=(const accessible<OELEM_T,NELEM,OTHER_DEPTH> &rhs)
    {
        static_assert(!std::is_same<ELEM_T, float>::value
                      && !std::is_same<ELEM_T, double>::value,
                      "Error, float and double vec to vec ^= is not supported");

        for (size_t i = 0; i < NELEM; i++)
                (*this).s[i] ^= rhs.s[i];
        return *this;
    }

    /*-------------------------------------------------------------------------
    * Bitwise xor assign with a scalar
    *-------------------------------------------------------------------------*/
    ACCESS_T &operator^=(const ELEM_T& rhs)
    {

        static_assert(!std::is_same<ELEM_T, float>::value
                      && !std::is_same<ELEM_T, double>::value,
                      "Error, float and double vec to scalar ^= is not supported");

        for (size_t i = 0; i < NELEM; i++)
                (*this).s[i] ^= rhs;
        return *this;
    }

/*-----------------------------------------------------------------------------
* Public debug operations -- Will not compile on the target!
*-----------------------------------------------------------------------------*/
public:
    /*-------------------------------------------------------------------------
    * print -- display the contents of the accessible
    *-------------------------------------------------------------------------*/
    void print() const
    {
        std::cout << "(";

        print_vec<ELEM_T>();
    }

/*-----------------------------------------------------------------------------
* Private helper methods
*-----------------------------------------------------------------------------*/
private:
    /*-------------------------------------------------------------------------
    * print_vec -- display the contents of an accessible.
    *-------------------------------------------------------------------------*/
    // Scalar variant
    template <typename E_TYPE,
              std::enable_if_t<std::is_arithmetic<E_TYPE>::value, int> = 0>
    void print_vec() const
    {
        // Pick a reasonable type for printing, i.e. promote short/char to int
        using conv_t =
            typename std::conditional<(std::is_integral<ELEM_T>::value &&
                                sizeof(ELEM_T) < sizeof(int32_t)),
                                int32_t, ELEM_T>::type;

        for (size_t i = 0; i < NELEM - 1; i++)
            std::cout << (conv_t)s[i] << ", ";
        std::cout << (conv_t)s[NELEM - 1] << ")" << std::endl;
    }

    // Complex variant
    template <typename E_TYPE,
              enable_if_cmplx_t<E_TYPE, int> = 0>
    void print_vec() const
    {
        for (size_t i = 0; i < NELEM - 1; i++)
        {
            s[i].print();
            std::cout << ", ";
        }
        s[NELEM-1].print();
        std::cout << ")" << std::endl;
   }
};


/******************************************************************************
*
* Conversion Operations
*
******************************************************************************/

// Functions to get source element size based on type (scalar or complex)
template <typename SRC_T,
          enable_if_cmplx_t<SRC_T, int> = 0>
constexpr size_t get_src_size()
{
    return sizeof(typename SRC_T::ELEM_TYPE) * 2;
}

template <typename SRC_T,
          std::enable_if_t<std::is_arithmetic<SRC_T>::value, int> = 0>
constexpr size_t get_src_size()
{
    return sizeof(SRC_T);
}

#include <ti_he_impl/vector_conv.h>

/******************************************************************************
*
* any/all Operations
*
*******************************************************************************/

/*-----------------------------------------------------------------------------
* __all - Takes an integral vector and returns 1 if the high bit is set in all
*   lanes.
*-----------------------------------------------------------------------------*/
// Scalar variant
template <typename ELEM_T,
          std::enable_if_t<std::is_integral<ELEM_T>::value, int> = 0>
int __all(const ELEM_T &input)
{
    typedef std::make_unsigned_t<ELEM_T> utype;
    constexpr int shift = (sizeof(ELEM_T) * CHAR_BIT) - 1;
    return (((utype)input) >> shift) & 1;
}

// Vector variant
template <typename ELEM_T,
          size_t NELEM,
          size_t DEPTH,
          std::enable_if_t<std::is_integral<ELEM_T>::value, int> = 0>
int __all(const accessible<ELEM_T, NELEM, DEPTH>& input)
{
    typedef std::make_unsigned_t<ELEM_T> utype;
    constexpr int shift = (sizeof(ELEM_T) * CHAR_BIT) - 1;
    ELEM_T result = 1;
    for (size_t i = 0; i < NELEM; i++)
    {
        result &= ((utype)input.s[i]) >> shift;
    }
    return result;
}

/*-----------------------------------------------------------------------------
* __any - Takes an integral vector and returns 1 if the high bit is set in any
*   lane.
*-----------------------------------------------------------------------------*/
// Scalar variant
template <typename ELEM_T,
          std::enable_if_t<std::is_integral<ELEM_T>::value, int> = 0>
int __any(const ELEM_T &input)
{
    typedef std::make_unsigned_t<ELEM_T> utype;
    constexpr int shift = (sizeof(ELEM_T) * CHAR_BIT) - 1;
    return (((utype)input) >> shift) & 1;
}

// Vector variant
template <typename ELEM_T,
          size_t NELEM,
          size_t DEPTH,
          std::enable_if_t<std::is_integral<ELEM_T>::value, int> = 0>
int __any(const accessible<ELEM_T, NELEM, DEPTH>& input)
{
    typedef std::make_unsigned_t<ELEM_T> utype;
    constexpr int shift = (sizeof(ELEM_T) * CHAR_BIT) - 1;
    ELEM_T result = 0;
    for (size_t i = 0; i < NELEM; i++)
    {
        result |= ((utype)input.s[i]) >> shift;
    }
    return result;
}


/******************************************************************************
*
* Vector Operations
*
******************************************************************************/
#define DEFINE_ARITH_OP_OVERLOAD(op) \
template <typename E1, typename E2, size_t NELEM, \
          size_t DEPTH_L, \
          size_t DEPTH_R, \
          std::enable_if_t<std::is_same<typename deconst<E1>::type, \
                                        typename deconst<E2>::type>::value, \
                           int> = 0> \
typename vtype<E1, NELEM>::ARITH_RES_TYPE operator op( \
    const accessible<E1, NELEM, DEPTH_L> &lhs, \
    const accessible<E2, NELEM, DEPTH_R> &rhs) \
{ \
    typename vtype<E1, NELEM>::ARITH_RES_TYPE res; \
    for (size_t i = 0; i < NELEM; i++) \
        res.s[i] = lhs.s[i] op rhs.s[i]; \
    return res; \
} \
template <typename E1, size_t NELEM, \
          size_t DEPTH_L> \
typename vtype<E1, NELEM>::ARITH_RES_TYPE operator op( \
    const accessible<E1, NELEM, DEPTH_L> &lhs, \
    typename mkconst<E1>::type immediate) \
{ \
    return lhs op typename vtype<E1, NELEM>::ARITH_RES_TYPE(immediate); \
} \
template <typename E2, size_t NELEM, \
          size_t DEPTH_R> \
typename vtype<E2, NELEM>::ARITH_RES_TYPE operator op( \
     typename mkconst<E2>::type immediate, \
     const accessible<E2, NELEM, DEPTH_R> &rhs) \
{ \
    return typename vtype<E2, NELEM>::ARITH_RES_TYPE(immediate) op rhs; \
}

DEFINE_ARITH_OP_OVERLOAD(+)
DEFINE_ARITH_OP_OVERLOAD(-)
DEFINE_ARITH_OP_OVERLOAD(*)
DEFINE_ARITH_OP_OVERLOAD(/)

#define DEFINE_INTEGRAL_ARITH_OP_OVERLOAD(op) \
template <typename E1, typename E2, size_t NELEM, \
          size_t DEPTH_L, \
          size_t DEPTH_R, \
          std::enable_if_t<std::is_integral<E1>::value, E1> = 0, \
          std::enable_if_t<std::is_same<typename deconst<E1>::type, \
                                        typename deconst<E2>::type>::value, \
                           int> = 0> \
typename vtype<E1, NELEM>::ARITH_RES_TYPE operator op( \
    const accessible<E1, NELEM, DEPTH_L> &lhs, \
    const accessible<E2, NELEM, DEPTH_R> &rhs) \
{ \
    typename vtype<E1, NELEM>::ARITH_RES_TYPE res; \
    for (size_t i = 0; i < NELEM; i++) \
        res.s[i] = lhs.s[i] op rhs.s[i]; \
    return res; \
} \
template <typename E1, size_t NELEM, \
          size_t DEPTH_L, \
          std::enable_if_t<std::is_integral<E1>::value, int> = 0> \
typename vtype<E1, NELEM>::ARITH_RES_TYPE operator op( \
    const accessible<E1, NELEM, DEPTH_L> &lhs, \
    typename mkconst<E1>::type immediate) \
{ \
    return lhs op typename vtype<E1, NELEM>::ARITH_RES_TYPE(immediate); \
} \
template <typename E2, size_t NELEM, \
          size_t DEPTH_R, \
          std::enable_if_t<std::is_integral<E2>::value, int> = 0> \
typename vtype<E2, NELEM>::ARITH_RES_TYPE operator op( \
     typename mkconst<E2>::type immediate, \
     const accessible<E2, NELEM, DEPTH_R> &rhs) \
{ \
    return typename vtype<E2, NELEM>::ARITH_RES_TYPE(immediate) op rhs; \
}

DEFINE_INTEGRAL_ARITH_OP_OVERLOAD(&)
DEFINE_INTEGRAL_ARITH_OP_OVERLOAD(|)
DEFINE_INTEGRAL_ARITH_OP_OVERLOAD(^)
DEFINE_INTEGRAL_ARITH_OP_OVERLOAD(%)
DEFINE_INTEGRAL_ARITH_OP_OVERLOAD(<<)
DEFINE_INTEGRAL_ARITH_OP_OVERLOAD(>>)

#define DEFINE_LOG_OP_OVERLOAD(op) \
template <typename E1, typename E2, size_t NELEM, \
          size_t DEPTH_L, \
          size_t DEPTH_R, \
          std::enable_if_t<std::is_same<typename deconst<E1>::type, \
                                        typename deconst<E2>::type>::value, \
                           int> = 0> \
typename vtype<E1, NELEM>::LOG_RES_TYPE operator op( \
    const accessible<E1, NELEM, DEPTH_L> &lhs, \
    const accessible<E2, NELEM, DEPTH_R> &rhs) \
{ \
    typename vtype<E1, NELEM>::LOG_RES_TYPE res; \
    for (size_t i = 0; i < NELEM; i++) \
		res.s[i] = lhs.s[i] op rhs.s[i] ? -1 : 0; \
	return res; \
} \
template <typename E1, size_t NELEM, \
          size_t DEPTH_L> \
typename vtype<E1, NELEM>::LOG_RES_TYPE operator op( \
    const accessible<E1, NELEM, DEPTH_L> &lhs, \
    typename mkconst<E1>::type immediate) \
{ \
	return lhs op vtype<typename deconst<E1>::type, NELEM>(immediate); \
} \
template <typename E2, size_t NELEM, \
          size_t DEPTH_R> \
vtype<int64_t, NELEM> operator op( \
     typename mkconst<E2>::type immediate, \
     const accessible<E2, NELEM, DEPTH_R> &rhs) \
{ \
    return vtype<typename deconst<E2>::type, NELEM>(immediate) op rhs; \
}

DEFINE_LOG_OP_OVERLOAD(>)
DEFINE_LOG_OP_OVERLOAD(>=)
DEFINE_LOG_OP_OVERLOAD(<)
DEFINE_LOG_OP_OVERLOAD(<=)
DEFINE_LOG_OP_OVERLOAD(==)
DEFINE_LOG_OP_OVERLOAD(!=)
DEFINE_LOG_OP_OVERLOAD(&&)
DEFINE_LOG_OP_OVERLOAD(||)

/*-----------------------------------------------------------------------------
* Logical Not ! Operator overloads
*-----------------------------------------------------------------------------*/
template <typename E1, size_t NELEM,
          size_t DEPTH_R>
typename vtype<E1, NELEM>::LOG_RES_TYPE operator!(
    const accessible<E1, NELEM, DEPTH_R> &rhs)
{
    typename vtype<E1, NELEM>::LOG_RES_TYPE res;
    for (size_t i = 0; i < NELEM; i++)
        res.s[i] = !rhs.s[i] ? -1 : 0;
    return res;
}

} /* namespace _c70_he_detail */


/******************************************************************************
*
* Definition of vector types in the global namespace.
*
******************************************************************************/
typedef _c70_he_detail::__vpred vpred;
using _c70_he_detail::__vpred;

/* Base complex types */
using __cchar   = _c70_he_detail::ctype<int8_t>;
using cchar = __cchar;
using __const_cchar = _c70_he_detail::ctype<const int8_t>;
using const_cchar = __const_cchar;
using __cshort  = _c70_he_detail::ctype<int16_t>;
using cshort = __cshort;
using __const_cshort = _c70_he_detail::ctype<const int16_t>;
using const_cshort = __const_cshort;
using __cint    = _c70_he_detail::ctype<int32_t>;
using cint = __cint;
using __const_cint = _c70_he_detail::ctype<const int32_t>;
using const_cint= __const_cint;
using __clong   = _c70_he_detail::ctype<int64_t>;
using clong = __clong;
using __const_clong = _c70_he_detail::ctype<const int64_t>;
using const_clong = __const_clong;
using __cfloat  = _c70_he_detail::ctype<float>;
using cfloat = __cfloat;
using __const_cfloat = _c70_he_detail::ctype<const float>;
using const_cfloat = __const_cfloat;
using __cdouble = _c70_he_detail::ctype<double>;
using cdouble = __cdouble;
using __const_cdouble = _c70_he_detail::ctype<const double>;
using const_cdouble = __const_cdouble;

/* Boolean vector types */
using __bool2  = _c70_he_detail::bvtype<2>;
using bool2 = __bool2;
using __bool4  = _c70_he_detail::bvtype<4>;
using bool4 = __bool4;
using __bool8  = _c70_he_detail::bvtype<8>;
using bool8 = __bool8;
using __bool16 = _c70_he_detail::bvtype<16>;
using bool16 = __bool16;
using __bool32 = _c70_he_detail::bvtype<32>;
using bool32 = __bool32;
using __bool64 = _c70_he_detail::bvtype<64>;
using bool64 = __bool64;

/* Arithmetic vector types and pointers */
#define DECLARE_VEC_TYPES(namebase,realbase,lanes) \
    using __ ## namebase ## lanes = _c70_he_detail::vtype<realbase,lanes>; \
    using namebase ## lanes = __ ## namebase ## lanes; \
    using __ ## namebase ## lanes ## _ptr = _c70_he_detail::vtype_ptr<namebase ## lanes>; \
    using namebase ## lanes ## _ptr = __ ## namebase ## lanes ## _ptr; \
    using __const_ ## namebase ## lanes = _c70_he_detail::vtype<const realbase,lanes>; \
    using const_ ## namebase ## lanes = __const_ ## namebase ## lanes; \
    using __const_ ## namebase ## lanes ## _ptr = _c70_he_detail::vtype_ptr<__const_ ## namebase ## lanes>; \
    using const_ ## namebase ## lanes ## _ptr = __const_ ## namebase ## lanes ## _ptr;

#define DECLARE_NONVEC_TYPES(namebase,realbase) \
    using __ ## namebase = realbase; \
    using namebase = __ ## namebase; \
    using __const_ ## namebase = const realbase; \
    using const_ ## namebase = __const_ ## namebase; \
    using __ ## namebase ## _ptr = realbase*; \
    using namebase ## _ptr = __ ## namebase ## _ptr; \
    using __const_ ## namebase ## _ptr = const realbase*; \
    using const_ ## namebase ## _ptr = __const_ ## namebase ## _ptr;

/* char types */
DECLARE_VEC_TYPES(char,int8_t,2)
DECLARE_VEC_TYPES(char,int8_t,4)
DECLARE_VEC_TYPES(char,int8_t,8)
DECLARE_VEC_TYPES(char,int8_t,16)
DECLARE_VEC_TYPES(char,int8_t,32)
DECLARE_VEC_TYPES(char,int8_t,64)

DECLARE_NONVEC_TYPES(uchar,uint8_t)
DECLARE_VEC_TYPES(uchar,uint8_t,2)
DECLARE_VEC_TYPES(uchar,uint8_t,4)
DECLARE_VEC_TYPES(uchar,uint8_t,8)
DECLARE_VEC_TYPES(uchar,uint8_t,16)
DECLARE_VEC_TYPES(uchar,uint8_t,32)
DECLARE_VEC_TYPES(uchar,uint8_t,64)

/* short types */
DECLARE_VEC_TYPES(short,int16_t,2)
DECLARE_VEC_TYPES(short,int16_t,4)
DECLARE_VEC_TYPES(short,int16_t,8)
DECLARE_VEC_TYPES(short,int16_t,16)
DECLARE_VEC_TYPES(short,int16_t,32)

DECLARE_NONVEC_TYPES(ushort,uint16_t)
DECLARE_VEC_TYPES(ushort,uint16_t,2)
DECLARE_VEC_TYPES(ushort,uint16_t,4)
DECLARE_VEC_TYPES(ushort,uint16_t,8)
DECLARE_VEC_TYPES(ushort,uint16_t,16)
DECLARE_VEC_TYPES(ushort,uint16_t,32)

/* int types */
DECLARE_VEC_TYPES(int,int32_t,2)
DECLARE_VEC_TYPES(int,int32_t,4)

/* Account for definition of __intn on Microsoft systems. */
/* __MINGW32__ is defined in both 32 bit and 64 bit MINGW */
#if defined(_MSC_VER) || defined(__MINGW32__)
#define __int8 int8
using int8 = _c70_he_detail::vtype<int32_t,8>;
using __int8_ptr = _c70_he_detail::vtype_ptr<int8>; \
using int8_ptr = __int8_ptr;
using __const_int8_ptr = _c70_he_detail::vtype_ptr<const int8>;
using const_int8_ptr = __const_int8_ptr;

#define __int16 int16
using int16 = _c70_he_detail::vtype<int32_t,16>;
using __int16_ptr = _c70_he_detail::vtype_ptr<int16>; \
using int16_ptr = __int16_ptr;
using __const_int16_ptr = _c70_he_detail::vtype_ptr<const int16>;
using const_int16_ptr = __const_int16_ptr;
#else
DECLARE_VEC_TYPES(int,int32_t,8)
DECLARE_VEC_TYPES(int,int32_t,16)
#endif

DECLARE_NONVEC_TYPES(uint,uint32_t)
DECLARE_VEC_TYPES(uint,uint32_t,2)
DECLARE_VEC_TYPES(uint,uint32_t,4)
DECLARE_VEC_TYPES(uint,uint32_t,8)
DECLARE_VEC_TYPES(uint,uint32_t,16)

/* float types */
DECLARE_VEC_TYPES(float,float,2)
DECLARE_VEC_TYPES(float,float,4)
DECLARE_VEC_TYPES(float,float,8)
DECLARE_VEC_TYPES(float,float,16)

/* long types */
DECLARE_VEC_TYPES(long,int64_t,2)
DECLARE_VEC_TYPES(long,int64_t,4)
DECLARE_VEC_TYPES(long,int64_t,8)
DECLARE_VEC_TYPES(long,int64_t,16)

DECLARE_NONVEC_TYPES(ulong,uint64_t)
DECLARE_VEC_TYPES(ulong,uint64_t,2)
DECLARE_VEC_TYPES(ulong,uint64_t,4)
DECLARE_VEC_TYPES(ulong,uint64_t,8)
DECLARE_VEC_TYPES(ulong,uint64_t,16)

/* double types */
DECLARE_VEC_TYPES(double,double,2)
DECLARE_VEC_TYPES(double,double,4)
DECLARE_VEC_TYPES(double,double,8)
DECLARE_VEC_TYPES(double,double,16)

#define DECLARE_CMPLX_VEC_TYPES(namebase,realbase,lanes) \
    using __ ## namebase ## lanes = _c70_he_detail::vtype<realbase,lanes>; \
    using namebase ## lanes = __ ## namebase ## lanes; \
    using __ ## namebase ## lanes ## _ptr = _c70_he_detail::vtype_ptr<namebase ## lanes>; \
    using namebase ## lanes ## _ptr = __ ## namebase ## lanes ## _ptr; \
    using __const_ ## namebase ## lanes = _c70_he_detail::vtype<__const_ ## realbase,lanes>; \
    using const_ ## namebase ## lanes = __const_ ## namebase ## lanes; \
    using __const_ ## namebase ## lanes ## _ptr = _c70_he_detail::vtype_ptr<__const_ ## namebase ## lanes>; \
    using const_ ## namebase ## lanes ## _ptr = __const_ ## namebase ## lanes ## _ptr;

/* cchar types */
using __cchar_ptr   = _c70_he_detail::ctype_ptr<__cchar>;
using cchar_ptr = __cchar_ptr;
using __const_cchar_ptr = _c70_he_detail::ctype_ptr<__const_cchar>;
using const_cchar_ptr = __const_cchar_ptr;
DECLARE_CMPLX_VEC_TYPES(cchar,cchar,2)
DECLARE_CMPLX_VEC_TYPES(cchar,cchar,4)
DECLARE_CMPLX_VEC_TYPES(cchar,cchar,8)
DECLARE_CMPLX_VEC_TYPES(cchar,cchar,16)
DECLARE_CMPLX_VEC_TYPES(cchar,cchar,32)

/* cshort types */
using __cshort_ptr   = _c70_he_detail::ctype_ptr<__cshort>;
using cshort_ptr = __cshort_ptr;
using __const_cshort_ptr = _c70_he_detail::ctype_ptr<__const_cshort>;
using const_cshort_ptr = __const_cshort_ptr;
DECLARE_CMPLX_VEC_TYPES(cshort,cshort,2)
DECLARE_CMPLX_VEC_TYPES(cshort,cshort,4)
DECLARE_CMPLX_VEC_TYPES(cshort,cshort,8)
DECLARE_CMPLX_VEC_TYPES(cshort,cshort,16)

/* cint types */
using __cint_ptr   = _c70_he_detail::ctype_ptr<__cint>;
using cint_ptr = __cint_ptr;
using __const_cint_ptr = _c70_he_detail::ctype_ptr<__const_cint>;
using const_cint_ptr = __const_cint_ptr;
DECLARE_CMPLX_VEC_TYPES(cint,cint,2)
DECLARE_CMPLX_VEC_TYPES(cint,cint,4)
DECLARE_CMPLX_VEC_TYPES(cint,cint,8)

/* cfloat types */
using __cfloat_ptr   = _c70_he_detail::ctype_ptr<__cfloat>;
using cfloat_ptr = __cfloat_ptr;
using __const_cfloat_ptr = _c70_he_detail::ctype_ptr<__const_cfloat>;
using const_cfloat_ptr = __const_cfloat_ptr;
DECLARE_CMPLX_VEC_TYPES(cfloat,cfloat,2)
DECLARE_CMPLX_VEC_TYPES(cfloat,cfloat,4)
DECLARE_CMPLX_VEC_TYPES(cfloat,cfloat,8)

/* clong types */
using __clong_ptr   = _c70_he_detail::ctype_ptr<__clong>;
using clong_ptr = __clong_ptr;
using __const_clong_ptr = _c70_he_detail::ctype_ptr<__const_clong>;
using const_clong_ptr = __const_clong_ptr;
DECLARE_CMPLX_VEC_TYPES(clong,clong,2)
DECLARE_CMPLX_VEC_TYPES(clong,clong,4)
DECLARE_CMPLX_VEC_TYPES(clong,clong,8)

/* cdouble types */
using __cdouble_ptr   = _c70_he_detail::ctype_ptr<__cdouble>;
using cdouble_ptr = __cdouble_ptr;
using __const_cdouble_ptr = _c70_he_detail::ctype_ptr<__const_cdouble>;
using const_cdouble_ptr = __const_cdouble_ptr;
DECLARE_CMPLX_VEC_TYPES(cdouble,cdouble,2)
DECLARE_CMPLX_VEC_TYPES(cdouble,cdouble,4)
DECLARE_CMPLX_VEC_TYPES(cdouble,cdouble,8)

#undef DECLARE_VEC_TYPES


/******************************************************************************
*
* Definition of vector utilities.
*
******************************************************************************/

/*-----------------------------------------------------------------------------
* Reinterpretation casts
*-----------------------------------------------------------------------------*/
using _c70_he_detail::__as_long16;
using _c70_he_detail::__as_long8;
using _c70_he_detail::__as_long4;
using _c70_he_detail::__as_long2;
using _c70_he_detail::__as_long;

using _c70_he_detail::__as_ulong16;
using _c70_he_detail::__as_ulong8;
using _c70_he_detail::__as_ulong4;
using _c70_he_detail::__as_ulong2;
using _c70_he_detail::__as_ulong;

using _c70_he_detail::__as_double16;
using _c70_he_detail::__as_double8;
using _c70_he_detail::__as_double4;
using _c70_he_detail::__as_double2;
using _c70_he_detail::__as_double;

using _c70_he_detail::__as_int16;
using _c70_he_detail::__as_int8;
using _c70_he_detail::__as_int4;
using _c70_he_detail::__as_int2;
using _c70_he_detail::__as_int;

using _c70_he_detail::__as_uint16;
using _c70_he_detail::__as_uint8;
using _c70_he_detail::__as_uint4;
using _c70_he_detail::__as_uint2;
using _c70_he_detail::__as_uint;

using _c70_he_detail::__as_float16;
using _c70_he_detail::__as_float8;
using _c70_he_detail::__as_float4;
using _c70_he_detail::__as_float2;
using _c70_he_detail::__as_float;

using _c70_he_detail::__as_short32;
using _c70_he_detail::__as_short16;
using _c70_he_detail::__as_short8;
using _c70_he_detail::__as_short4;
using _c70_he_detail::__as_short2;
using _c70_he_detail::__as_short;

using _c70_he_detail::__as_ushort32;
using _c70_he_detail::__as_ushort16;
using _c70_he_detail::__as_ushort8;
using _c70_he_detail::__as_ushort4;
using _c70_he_detail::__as_ushort2;
using _c70_he_detail::__as_ushort;

using _c70_he_detail::__as_char64;
using _c70_he_detail::__as_char32;
using _c70_he_detail::__as_char16;
using _c70_he_detail::__as_char8;
using _c70_he_detail::__as_char4;
using _c70_he_detail::__as_char2;
using _c70_he_detail::__as_char;

using _c70_he_detail::__as_uchar64;
using _c70_he_detail::__as_uchar32;
using _c70_he_detail::__as_uchar16;
using _c70_he_detail::__as_uchar8;
using _c70_he_detail::__as_uchar4;
using _c70_he_detail::__as_uchar2;
using _c70_he_detail::__as_uchar;

using _c70_he_detail::__as_clong8;
using _c70_he_detail::__as_clong4;
using _c70_he_detail::__as_clong2;
using _c70_he_detail::__as_clong;

using _c70_he_detail::__as_cdouble8;
using _c70_he_detail::__as_cdouble4;
using _c70_he_detail::__as_cdouble2;
using _c70_he_detail::__as_cdouble;

using _c70_he_detail::__as_cint8;
using _c70_he_detail::__as_cint4;
using _c70_he_detail::__as_cint2;
using _c70_he_detail::__as_cint;

using _c70_he_detail::__as_cfloat8;
using _c70_he_detail::__as_cfloat4;
using _c70_he_detail::__as_cfloat2;
using _c70_he_detail::__as_cfloat;

using _c70_he_detail::__as_cshort16;
using _c70_he_detail::__as_cshort8;
using _c70_he_detail::__as_cshort4;
using _c70_he_detail::__as_cshort2;
using _c70_he_detail::__as_cshort;

using _c70_he_detail::__as_cchar32;
using _c70_he_detail::__as_cchar16;
using _c70_he_detail::__as_cchar8;
using _c70_he_detail::__as_cchar4;
using _c70_he_detail::__as_cchar2;
using _c70_he_detail::__as_cchar;

/*-----------------------------------------------------------------------------
* Conversion casts
*-----------------------------------------------------------------------------*/
using _c70_he_detail::__convert_long16;
using _c70_he_detail::__convert_long8;
using _c70_he_detail::__convert_long4;
using _c70_he_detail::__convert_long2;
using _c70_he_detail::__convert_long;

using _c70_he_detail::__convert_ulong16;
using _c70_he_detail::__convert_ulong8;
using _c70_he_detail::__convert_ulong4;
using _c70_he_detail::__convert_ulong2;
using _c70_he_detail::__convert_ulong;

using _c70_he_detail::__convert_double16;
using _c70_he_detail::__convert_double8;
using _c70_he_detail::__convert_double4;
using _c70_he_detail::__convert_double2;
using _c70_he_detail::__convert_double;

using _c70_he_detail::__convert_int16;
using _c70_he_detail::__convert_int8;
using _c70_he_detail::__convert_int4;
using _c70_he_detail::__convert_int2;
using _c70_he_detail::__convert_int;

using _c70_he_detail::__convert_uint16;
using _c70_he_detail::__convert_uint8;
using _c70_he_detail::__convert_uint4;
using _c70_he_detail::__convert_uint2;
using _c70_he_detail::__convert_uint;

using _c70_he_detail::__convert_float16;
using _c70_he_detail::__convert_float8;
using _c70_he_detail::__convert_float4;
using _c70_he_detail::__convert_float2;
using _c70_he_detail::__convert_float;

using _c70_he_detail::__convert_short32;
using _c70_he_detail::__convert_short16;
using _c70_he_detail::__convert_short8;
using _c70_he_detail::__convert_short4;
using _c70_he_detail::__convert_short2;
using _c70_he_detail::__convert_short;

using _c70_he_detail::__convert_ushort32;
using _c70_he_detail::__convert_ushort16;
using _c70_he_detail::__convert_ushort8;
using _c70_he_detail::__convert_ushort4;
using _c70_he_detail::__convert_ushort2;
using _c70_he_detail::__convert_ushort;

using _c70_he_detail::__convert_char64;
using _c70_he_detail::__convert_char32;
using _c70_he_detail::__convert_char16;
using _c70_he_detail::__convert_char8;
using _c70_he_detail::__convert_char4;
using _c70_he_detail::__convert_char2;
using _c70_he_detail::__convert_char;

using _c70_he_detail::__convert_uchar64;
using _c70_he_detail::__convert_uchar32;
using _c70_he_detail::__convert_uchar16;
using _c70_he_detail::__convert_uchar8;
using _c70_he_detail::__convert_uchar4;
using _c70_he_detail::__convert_uchar2;
using _c70_he_detail::__convert_uchar;

using _c70_he_detail::__convert_clong8;
using _c70_he_detail::__convert_clong4;
using _c70_he_detail::__convert_clong2;
using _c70_he_detail::__convert_clong;

using _c70_he_detail::__convert_cdouble8;
using _c70_he_detail::__convert_cdouble4;
using _c70_he_detail::__convert_cdouble2;
using _c70_he_detail::__convert_cdouble;

using _c70_he_detail::__convert_cint8;
using _c70_he_detail::__convert_cint4;
using _c70_he_detail::__convert_cint2;
using _c70_he_detail::__convert_cint;

using _c70_he_detail::__convert_cfloat8;
using _c70_he_detail::__convert_cfloat4;
using _c70_he_detail::__convert_cfloat2;
using _c70_he_detail::__convert_cfloat;

using _c70_he_detail::__convert_cshort16;
using _c70_he_detail::__convert_cshort8;
using _c70_he_detail::__convert_cshort4;
using _c70_he_detail::__convert_cshort2;
using _c70_he_detail::__convert_cshort;

using _c70_he_detail::__convert_cchar32;
using _c70_he_detail::__convert_cchar16;
using _c70_he_detail::__convert_cchar8;
using _c70_he_detail::__convert_cchar4;
using _c70_he_detail::__convert_cchar2;
using _c70_he_detail::__convert_cchar;


/*-----------------------------------------------------------------------------
* Pointer Conversion Intrinsics (DEPRECATED IN FAVOR OF C-STYLE CASTS)
*-----------------------------------------------------------------------------*/

// Convert from vector pointer to an "element type" pointer
// Ex: int4_ptr  to int32_t*
// Ex: cint4_ptr to int32_t* (only allowed when previously init'd using int*)
// Ex: cint4_ptr to cint_ptr
// C-style cast: uint8_t *p = (uint8_t*)(pointer);
#define vtos_ptr(type, addr) ((type*)(addr))

// Convert from scalar pointer to vector pointer
// Ex: int32_t* to int4_ptr
// Ex: int32_t* to cint4_ptr
// Ex: cint* to cint4_ptr
// C-style cast: uchar64_ptr p = (uchar64_ptr)(pointer);
#define stov_ptr(type, addr) ((type##_ptr)(addr))

// Convert from complex element pointer to scalar pointer
// Ex: cint to int32_t*
// C-style cast: int32_t* p = (int32_t*)(pointer);
#define ctos_ptr(type, addr) ((type*)(addr))

// Convert from scalar pointer to complex element type pointer
// Ex: int32_t* to cint
// C-style cast: cint_ptr p = (cint_ptr)(pointer);
#define stoc_ptr(type, addr) ((type##_ptr)(addr))

// Convert from vector pointer to vector pointer (not supported for complex)
// Note: Use with care for where the original vector's memory is allocated.
// Ex: int4_ptr to char16_ptr
// C-style cast: char16_ptr p = (char16_ptr)(pointer);

#endif /* VECTOR_H */
