The following documents are included in this install package:

Available C7000 Compiler Tools Documents:

- SPRUIG8E_C7000_Compiler_Guide.pdf: C7000 C/C++ Optimizing Compiler User's Guide
- SPRUIV4A_C7000_Optimization_Guide.pdf: C7000 Optimization Guide
- SPRUIG4C_C7000_EABI_Technical_Reference.pdf: C7000 Embedded Application Binary Interface (EABI) Reference Guide
- SPRUIG5C_C6000_to_C7000_Migration.pdf: C6000-to-C7000 Migration User's Guide
- SPRUIG3C_C7000_VCOP_Kernel_C_Translation.pdf: VCOP Kernel-C to C7000 Migration Tool User's Guide
- SPRUIG6G_C7000_Host_Emulation_Users_Guide.pdf: C7000 Host Emulation User's Guide

Hardware Design Documents. Some of these hardware documents are not on the
ti.com website. They can be obtained from your Texas Instruments Field
Application Engineer.

- C71x DSP Corepac Technical Reference Manual
- C7x Instruction Guide
- C71x DSP CPU, Instruction Set, and Matrix Multiply Accelerator (SPRUIP0)
