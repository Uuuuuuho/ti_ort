var group__ETHFW__LIB__API =
[
    [ "EthFw_Version", "structEthFw__Version.html", [
      [ "major", "structEthFw__Version.html#a4233ceab3260e7324f080dbe1a7efb2a", null ],
      [ "minor", "structEthFw__Version.html#a144148bbdec72ea33933e42c47cc8078", null ],
      [ "rev", "structEthFw__Version.html#ae2118ee3f9773cb902b349bcc6fee51f", null ],
      [ "year", "structEthFw__Version.html#aa99ffde3ca735364c3622dd728147794", null ],
      [ "month", "structEthFw__Version.html#a0d2a097b7cdc11037f3778da91675ee8", null ],
      [ "date", "structEthFw__Version.html#a5dcbfe69f0d983c58aa0a2a6ef056ff6", null ],
      [ "hour", "structEthFw__Version.html#aef9560fa95498523fc7cb1a25922a8cd", null ],
      [ "min", "structEthFw__Version.html#a10a80052c90e114c9ad55df753f716fa", null ],
      [ "sec", "structEthFw__Version.html#aec783840d064708de8b1d7413a546a98", null ],
      [ "commitHash", "structEthFw__Version.html#a3f8782e9da31c45df2b03352079a99fd", null ]
    ] ],
    [ "EthFw_Port", "structEthFw__Port.html", [
      [ "portNum", "structEthFw__Port.html#ac344ef27a35b0578be45b48679f3669e", null ],
      [ "vlanCfg", "structEthFw__Port.html#a8e82b024a6fa5decc29827205c047c6e", null ]
    ] ],
    [ "EthFw_Config", "structEthFw__Config.html", [
      [ "cpswCfg", "structEthFw__Config.html#aa829b2e7a0f6a81a98a9c0224f6c37b3", null ],
      [ "ports", "structEthFw__Config.html#af020cdd38246a9a857ebcc64aefcf6b0", null ],
      [ "numPorts", "structEthFw__Config.html#a24b6294b5f9e4e8949b69f85e33a4713", null ]
    ] ],
    [ "ETHFW_VERSION_YEARLEN", "group__ETHFW__LIB__API.html#ga3a402186821929bbf255bec35e96f26f", null ],
    [ "ETHFW_VERSION_MONTHLEN", "group__ETHFW__LIB__API.html#gae4ffc9210752de9002c909c7273f1ed8", null ],
    [ "ETHFW_VERSION_DATELEN", "group__ETHFW__LIB__API.html#ga219fe0e4299bcf8ddf4ee760a124ebfe", null ],
    [ "ETHFW_VERSION_HOURLEN", "group__ETHFW__LIB__API.html#ga03b8adbe58d0cda84ceefb10936ecbcf", null ],
    [ "ETHFW_VERSION_MINLEN", "group__ETHFW__LIB__API.html#ga1dda8056746ed53847e424498065210f", null ],
    [ "ETHFW_VERSION_SECLEN", "group__ETHFW__LIB__API.html#ga7df6c51be4febcf02c87781ca2eaf0cd", null ],
    [ "ETHFW_VERSION_COMMITSHALEN", "group__ETHFW__LIB__API.html#ga3f7ee10d5b22570293d15f4f3f213d18", null ],
    [ "EthFw_Handle", "group__ETHFW__LIB__API.html#ga2508b2fe83c7ae9fcef3644d1417a764", null ],
    [ "EthFw_initConfigParams", "group__ETHFW__LIB__API.html#ga6d29cc1666d23b0ee68f46ba8d6c5b83", null ],
    [ "EthFw_init", "group__ETHFW__LIB__API.html#ga41135bde04b458fc86b70b2448c0e5a3", null ],
    [ "EthFw_deinit", "group__ETHFW__LIB__API.html#ga9721931532f3a84b0997fdec6477ac5b", null ],
    [ "EthFw_initRemoteConfig", "group__ETHFW__LIB__API.html#ga81cce97c7247778601ea0099e4f6336a", null ],
    [ "EthFw_lateAnnounce", "group__ETHFW__LIB__API.html#ga87aa11ec1a339c99823b77ad60d01fcd", null ],
    [ "EthFw_getVersion", "group__ETHFW__LIB__API.html#gac27807770cca62bc2cba2f359b606732", null ],
    [ "EthFw_initTimeSyncPtp", "group__ETHFW__LIB__API.html#gad18dd816893561b8525b20e0ca8897ec", null ]
];