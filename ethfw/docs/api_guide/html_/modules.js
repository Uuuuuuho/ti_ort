var modules =
[
    [ "Ethernet Firmware library APIs", "group__ETHFW__LIB__API.html", "group__ETHFW__LIB__API" ],
    [ "Ethernet Switch Proxy Client APIs", "group__CPSW__PROXY__API.html", "group__CPSW__PROXY__API" ],
    [ "Ethernet Switch Proxy Server APIs", "group__CPSW__PROXY__SERVER__API.html", "group__CPSW__PROXY__SERVER__API" ],
    [ "Ethernet Switch Remote Device", "group__ETHSWITCH__REMOTE__DEVICE__API.html", "group__ETHSWITCH__REMOTE__DEVICE__API" ]
];