var group__CPSW__PROXY__API =
[
    [ "CpswProxy_Config", "structCpswProxy__Config.html", [
      [ "rpmsgEndPointId", "structCpswProxy__Config.html#ad3741c68fafa7a2a22d572cd22333dac", null ],
      [ "masterCoreId", "structCpswProxy__Config.html#ac87d038cff775758e9bddfffaa1cd66b", null ],
      [ "device_name", "structCpswProxy__Config.html#ab1a022aed5ac3f5eb7981425f7b72ea8", null ],
      [ "deviceDataNotifyCb", "structCpswProxy__Config.html#ab5259410c82e9a2d0c967dd805a98de5", null ]
    ] ],
    [ "CpswProxy_deviceDataNotifyCbFxn", "group__CPSW__PROXY__API.html#gaa4968de98a34edee19d9c8412209d45d", null ],
    [ "CpswProxy_Handle", "group__CPSW__PROXY__API.html#ga25bd37511adb54a0b9ca7d0946afc6ea", null ],
    [ "CpswProxy_init", "group__CPSW__PROXY__API.html#ga4e2e19d860c31f8df2e7fb90b397991f", null ],
    [ "CpswProxy_deInit", "group__CPSW__PROXY__API.html#gaee800a1786444cdb370c3de6c7c70e2c", null ],
    [ "CpswProxy_start", "group__CPSW__PROXY__API.html#ga309c7327e2e16ddf545a86754d014c30", null ],
    [ "CpswProxy_attach", "group__CPSW__PROXY__API.html#gaaa3efe32d30bcaecc9b33adf068e40e5", null ],
    [ "CpswProxy_attachExtended", "group__CPSW__PROXY__API.html#gab486b9a23195427c86c4bd562b9df99e", null ],
    [ "CpswProxy_detach", "group__CPSW__PROXY__API.html#gaecf92f9b000114a9eefb03b306ff8678", null ],
    [ "CpswProxy_allocTxCh", "group__CPSW__PROXY__API.html#gab241b8b9316550ec9dbc680533ef1f28", null ],
    [ "CpswProxy_freeTxCh", "group__CPSW__PROXY__API.html#ga37786efbfc006ca5d2cb4f34231b9e9a", null ],
    [ "CpswProxy_allocRxFlow", "group__CPSW__PROXY__API.html#ga0fc18ebe90d862715ee8cc570e1e72e1", null ],
    [ "CpswProxy_freeRxFlow", "group__CPSW__PROXY__API.html#ga6e5b202b5ef169a70a2df42e52eff577", null ],
    [ "CpswProxy_allocMac", "group__CPSW__PROXY__API.html#ga60904468d126523fa58b809760261a48", null ],
    [ "CpswProxy_freeMac", "group__CPSW__PROXY__API.html#gae0ebc0fc2e2e17b0abaae7e9ffb0d420", null ],
    [ "CpswProxy_registerDstMacRxFlow", "group__CPSW__PROXY__API.html#ga872074e6523abc32beef1ff7348b3675", null ],
    [ "CpswProxy_unregisterDstMacRxFlow", "group__CPSW__PROXY__API.html#ga16d649f897f50816311fafac1eb872a0", null ],
    [ "CpswProxy_registerEthertypeRxFlow", "group__CPSW__PROXY__API.html#ga2542df595c04d3f7ec6c1f8a78900e58", null ],
    [ "CpswProxy_unregisterEthertypeRxFlow", "group__CPSW__PROXY__API.html#ga6dff5cf4abc3eae4269be38cb6794ec8", null ],
    [ "CpswProxy_registerDefaultRxFlow", "group__CPSW__PROXY__API.html#ga49450af3d299dc9de4dc60e78a3161e2", null ],
    [ "CpswProxy_unregisterDefaultRxFlow", "group__CPSW__PROXY__API.html#ga3a0c542b6ff8ff5646b2217b24d937bd", null ],
    [ "CpswProxy_registerIPV4Addr", "group__CPSW__PROXY__API.html#ga46aa432876eae5b3e3fc8f0229478dee", null ],
    [ "CpswProxy_unregisterIPV4Addr", "group__CPSW__PROXY__API.html#ga56fc32997cdecdb469568e6ba9a21ed0", null ],
    [ "CpswProxy_addHostPortEntry", "group__CPSW__PROXY__API.html#gabef748cf00d2ac0c92adbe8fe607fb9a", null ],
    [ "CpswProxy_delAddrEntry", "group__CPSW__PROXY__API.html#ga5178e9a52d808cd4618b8f11ec19fcf0", null ],
    [ "CpswProxy_ioctl", "group__CPSW__PROXY__API.html#ga8a010dba293a03e42c2951ce214b3638", null ],
    [ "CpswProxy_isPhyLinked", "group__CPSW__PROXY__API.html#gaa0ba6f4cb76ea00d8568430b4eaf8b8d", null ],
    [ "CpswProxy_sendNotify", "group__CPSW__PROXY__API.html#gaff5e7000c96fe83e7ac8367cd1a406bb", null ],
    [ "CpswProxy_registerRemoteTimer", "group__CPSW__PROXY__API.html#ga2fe76ec8b5249e7a3857d98422b36340", null ],
    [ "CpswProxy_unregisterRemoteTimer", "group__CPSW__PROXY__API.html#gab6f5ccb47b3bf8af1f26d64dafe5cac5", null ],
    [ "CpswProxy_registerHwPushNotifyCb", "group__CPSW__PROXY__API.html#ga6cf9ebdfb0cfbe814ad37205146e2850", null ],
    [ "CpswProxy_unregisterHwPushNotifyCb", "group__CPSW__PROXY__API.html#gaa41559fe6576b00fc9b407d5865932af", null ]
];