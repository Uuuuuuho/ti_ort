var indexSectionsWithContent =
{
  0: "_acdefghimnoprstuvy",
  1: "cer",
  2: "cer",
  3: "_ce",
  4: "acdefghimnoprstuvy",
  5: "ce",
  6: "r",
  7: "r",
  8: "e"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "enums",
  7: "enumvalues",
  8: "groups"
};

var indexSectionLabels =
{
  0: "All",
  1: "Data Structures",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Enumerations",
  7: "Enumerator",
  8: "Modules"
};

