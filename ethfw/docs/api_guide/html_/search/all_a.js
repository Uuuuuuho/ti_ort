var searchData=
[
  ['notify_5finfo',['notify_info',['../structrpmsg__kdrv__ethswitch__c2s__notify.html#a2e4f30507ff4262892acf8ee23aaa0f1',1,'rpmsg_kdrv_ethswitch_c2s_notify::notify_info()'],['../rpmsg-kdrv-transport-ethswitch_8h.html#ab510d0bdaa0d056a8b46aa39fd28aee7',1,'notify_info():&#160;rpmsg-kdrv-transport-ethswitch.h']]],
  ['notify_5finfo_5flen',['notify_info_len',['../structrpmsg__kdrv__ethswitch__c2s__notify.html#aa81e732783698f50d49d326b56acdc39',1,'rpmsg_kdrv_ethswitch_c2s_notify::notify_info_len()'],['../rpmsg-kdrv-transport-ethswitch_8h.html#a9f290074540d0e56371c54d41a1e4f73',1,'notify_info_len():&#160;rpmsg-kdrv-transport-ethswitch.h']]],
  ['notifycb',['notifyCb',['../structCpswProxyServer__Config__t.html#a9c7a975656f8e6cdc187d26dff7918f0',1,'CpswProxyServer_Config_t']]],
  ['notifyid',['notifyid',['../structrpmsg__kdrv__ethswitch__c2s__notify.html#a00f347dd17bc7111c3de362a329cf42e',1,'rpmsg_kdrv_ethswitch_c2s_notify::notifyid()'],['../rpmsg-kdrv-transport-ethswitch_8h.html#a842f1810c52d01aa4d396a35810cc2f3',1,'notifyid():&#160;rpmsg-kdrv-transport-ethswitch.h']]],
  ['notifyservicecpswtype',['notifyServiceCpswType',['../structCpswProxyServer__Config__t.html#a594b664149365ce505f038f67414a319',1,'CpswProxyServer_Config_t']]],
  ['notifyserviceremotecoreid',['notifyServiceRemoteCoreId',['../structCpswProxyServer__Config__t.html#a0e09094abe42c15a1a2c74d09ddd5952',1,'CpswProxyServer_Config_t']]],
  ['numports',['numPorts',['../structEthFw__Config.html#a24b6294b5f9e4e8949b69f85e33a4713',1,'EthFw_Config']]],
  ['numremotecores',['numRemoteCores',['../structCpswProxyServer__Config__t.html#a102222b4b274262e973aedba900101f1',1,'CpswProxyServer_Config_t']]]
];
