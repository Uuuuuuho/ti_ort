var group__CPSW__PROXY__SERVER__API =
[
    [ "CpswProxyServer_RemoteCoreConfig", "structCpswProxyServer__RemoteCoreConfig.html", [
      [ "remoteCoreId", "structCpswProxyServer__RemoteCoreConfig.html#a93542a9cb9ea01df5d44bcd3e9ebdb6b", null ],
      [ "serverName", "structCpswProxyServer__RemoteCoreConfig.html#a872dd25803265296169efc565e9a6d12", null ]
    ] ],
    [ "CpswProxyServer_Config_t", "structCpswProxyServer__Config__t.html", [
      [ "initEthfwDeviceDataCb", "structCpswProxyServer__Config__t.html#a914e27a567b88eb3fa5fdea27290c676", null ],
      [ "getMcmCmdIfCb", "structCpswProxyServer__Config__t.html#a48bbd0daf6f3b110796f9f78eba12240", null ],
      [ "notifyCb", "structCpswProxyServer__Config__t.html#a9c7a975656f8e6cdc187d26dff7918f0", null ],
      [ "rpmsgEndPointId", "structCpswProxyServer__Config__t.html#a168a53c886ad492c7e8953cce59821fc", null ],
      [ "autosarEthDeviceEndPointId", "structCpswProxyServer__Config__t.html#a6dd218deebfd35f85277e9b84b39d0eb", null ],
      [ "numRemoteCores", "structCpswProxyServer__Config__t.html#a102222b4b274262e973aedba900101f1", null ],
      [ "autosarEthDriverRemoteCoreId", "structCpswProxyServer__Config__t.html#a8f2fcbd0a2627def958988e08709f094", null ],
      [ "notifyServiceCpswType", "structCpswProxyServer__Config__t.html#a594b664149365ce505f038f67414a319", null ],
      [ "notifyServiceRemoteCoreId", "structCpswProxyServer__Config__t.html#a0e09094abe42c15a1a2c74d09ddd5952", null ],
      [ "remoteCoreCfg", "structCpswProxyServer__Config__t.html#afaa7d58fa6db083a0eead4ed38938814", null ]
    ] ],
    [ "CpswProxyServer_InitEthfwDeviceDataCb", "group__CPSW__PROXY__SERVER__API.html#gaefefd0f9bc7be70c52cfedca57868a17", null ],
    [ "CpswProxyServer_GetMcmCmdIfCb", "group__CPSW__PROXY__SERVER__API.html#gae5c85334e9f7c680857342e3b4bda6e7", null ],
    [ "CpswProxyServer_NotifyCb", "group__CPSW__PROXY__SERVER__API.html#ga941106a6d23f38e3751a4e772adf0ba6", null ],
    [ "CpswProxyServer_init", "group__CPSW__PROXY__SERVER__API.html#ga786431c107fade1f37e0f753aab075ca", null ],
    [ "CpswProxyServer_start", "group__CPSW__PROXY__SERVER__API.html#gadf25ca3833939cc293d85660343cb724", null ]
];