/*
@ @licstart  The following is the entire license notice for the
JavaScript code in this file.

Copyright (C) 1997-2017 by Dimitri van Heesch

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.

You should have received a copy of the GNU General Public License along
with this program; if not, write to the Free Software Foundation, Inc.,
51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

@licend  The above is the entire license notice
for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "Ethernet Firmware", "index.html", [
    [ "Introduction", "index.html", [
      [ "Integrated Switch", "index.html#ethfw_c_ug_switch", null ],
      [ "Ethernet Firmware Software Stack", "index.html#ethfw_c_ug_fw_architecture", null ],
      [ "Document Revision History", "index.html#ethfw_main_rev_history", null ]
    ] ],
    [ "User Guide", "ethfw_c_ug_top.html", [
      [ "EthFw Demos", "ethfw_c_ug_top.html#ethfw_c_ug_ethfw_demos", [
        [ "EthFw Switching & TCP/IP Apps Demo", "ethfw_c_ug_top.html#ethfw_switching_demo", null ],
        [ "Inter-VLAN Routing Demo", "ethfw_c_ug_top.html#ethfw_intervlan_demo", null ]
      ] ],
      [ "Supported Features", "ethfw_c_ug_top.html#ethfw_c_ug_features_list", null ],
      [ "Dependencies", "ethfw_c_ug_top.html#ethfw_instal_top", [
        [ "Hardware Dependencies", "ethfw_c_ug_top.html#ethfw_depend_hw", [
          [ "J721E/J7200 EVM", "ethfw_c_ug_top.html#ethfw_depend_evm_j721e", null ],
          [ "J721E GESI Expansion Board", "ethfw_c_ug_top.html#ethfw_depend_evm_gesi_j721e", null ],
          [ "J7200 Quad-Port Eth Expansion Board", "ethfw_c_ug_top.html#ethfw_depend_evm_quadport_j7200", null ]
        ] ],
        [ "Software Dependencies", "ethfw_c_ug_top.html#ethfw_depend_sw", [
          [ "PDK", "ethfw_c_ug_top.html#ethfw_depend_pdk", [
            [ "CSL", "ethfw_c_ug_top.html#ethfw_depend_pdk_csl", null ],
            [ "UDMA", "ethfw_c_ug_top.html#ethfw_depend_pdk_udma", null ],
            [ "Enet LLD", "ethfw_c_ug_top.html#ethfw_depend_pdk_enet", null ]
          ] ],
          [ "lwIP", "ethfw_c_ug_top.html#ethfw_depend_lwip", [
            [ "Ethernet Firmware Proxy ARP", "ethfw_c_ug_top.html#ethfw_depend_lwip_proxyarp", null ]
          ] ]
        ] ],
        [ "IDE (CCS)", "ethfw_c_ug_top.html#ethfw_instal_ccs", null ]
      ] ],
      [ "Installation Steps", "ethfw_c_ug_top.html#ethfw_instal_steps", null ],
      [ "Directory Structure", "ethfw_c_ug_top.html#ethfw_dir", [
        [ "Post Install Directory Structure", "ethfw_c_ug_top.html#ethfw_post_install_j721e", null ],
        [ "Utilities Directory Structure", "ethfw_c_ug_top.html#ethfw_dir_utils", null ],
        [ "Demo Application Sources Directory Structure", "ethfw_c_ug_top.html#ethfw_dir_demo", null ],
        [ "EthFw Demonstration Applications", "ethfw_c_ug_top.html#ethfw_dir_switch_demos", null ]
      ] ],
      [ "Build", "ethfw_c_ug_top.html#ethfw_build_top", [
        [ "Setup Environment", "ethfw_c_ug_top.html#ethfw_build_setup_env", null ],
        [ "Build", "ethfw_c_ug_top.html#ethfw_build", [
          [ "Build All", "ethfw_c_ug_top.html#ethfw_build_all", null ],
          [ "QNX Build", "ethfw_c_ug_top.html#ethfw_qnx_build_all", null ]
        ] ],
        [ "Clean", "ethfw_c_ug_top.html#ethfw_build_clean", [
          [ "Clean All", "ethfw_c_ug_top.html#ethfw_build_clean_all", null ],
          [ "Remove build output", "ethfw_c_ug_top.html#ethfw_build_clean_binaries", null ]
        ] ],
        [ "Profiles", "ethfw_c_ug_top.html#ethfw_build_profiles", null ],
        [ "Examples Linker File (Select memory location to hold example binary)", "ethfw_c_ug_top.html#ethfw_build_eg_linker", null ]
      ] ],
      [ "Running Examples", "ethfw_c_ug_top.html#ethfw_run_eg", [
        [ "Load Example Binaries", "ethfw_c_ug_top.html#ethfw_run_ccs_load_binary", null ]
      ] ],
      [ "Un Installation", "ethfw_c_ug_top.html#ethfw_uninstall", null ],
      [ "Known issues", "ethfw_c_ug_top.html#ethfw_known_issues", null ],
      [ "Compiler Flags used", "ethfw_c_ug_top.html#ethfw_cflag", [
        [ "Demo Application - Profile: Debug", "ethfw_c_ug_top.html#ethfw_cflag_debug", null ],
        [ "Demo Application - Profile: Release", "ethfw_c_ug_top.html#ethfw_cflag_release", null ]
      ] ],
      [ "Supported Device Families", "ethfw_c_ug_top.html#ethfw_supported_family", null ],
      [ "Document Revision History", "ethfw_c_ug_top.html#ethfw_rev_history", null ]
    ] ],
    [ "Validation", "val_notes_mainpage.html", [
      [ "Introduction", "val_notes_mainpage.html#val_intro", null ],
      [ "HIS Metric Report", "val_notes_mainpage.html#val_his_rep", null ],
      [ "KW Static Analysis Report", "val_notes_mainpage.html#val_kw_rep", null ],
      [ "EthFw Unit Test Reports", "val_notes_mainpage.html#val_unit_rep", null ],
      [ "Performance Measurements", "val_notes_mainpage.html#val_perform_measurements", null ]
    ] ],
    [ "CCS setup", "ccs_setup_top.html", null ],
    [ "EthFw Demo Applications", "demo_top.html", "demo_top" ],
    [ "TI Disclaimer", "TI_DISCLAIMER.html", null ]
  ] ]
];

var NAVTREEINDEX =
[
"TI_DISCLAIMER.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';