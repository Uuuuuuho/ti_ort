#!/bin/bash
#Deamon to crash kernel on a GPIO button
#Crashes kernel using sysrq
#https://www.kernel.org/doc/html/latest/admin-guide/sysrq.html

#Detect the board on which the script is running
BRD=`cat /proc/device-tree/model`

# Setup SW11(SYS_IRQz) => WAKEUP GPIO0_7 (CTRLMMR_WKUP_PADCONFIG51)
devmem2 0x4301c0cc w 0x44007

#Setup GPIO muxmode and input
echo 7 > /sys/class/gpio/export
echo in > /sys/class/gpio/gpio7/direction

# Setup SW10(SOC_EXTINTn) => MAIN GPIO0_7 (CTRLMMR_PADCONFIG0)
devmem2 0x11c000 w 0x44007

#Setup GPIO muxmode and input
echo 84 > /sys/class/gpio/export
echo in > /sys/class/gpio/gpio84/direction

#Wait till the button is pressed and crash the kernel
while true;
do
	val=`cat /sys/class/gpio/gpio7/value`
	if [ $val -eq "0" ]; then
		wall "SW11 button pressed, Crashing the root cell (Cluster VM)"
		msleep 100
		echo c > /proc/sysrq-trigger
	fi

	val=`cat /sys/class/gpio/gpio84/value`
	if [ $val -eq "0" ]; then
		wall "SW10 button pressed, Crashing the inmate cell (Infotainment VM)"
		msleep 100
		jailhouse cell destroy 1
	fi

	msleep 100
done &



