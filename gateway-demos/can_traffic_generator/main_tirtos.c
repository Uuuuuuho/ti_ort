/*
*
* Copyright (c) 2019-2020 Texas Instruments Incorporated
*
* All rights reserved not granted herein.
*
* Limited License.
*
* Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
* license under copyrights and patents it now or hereafter owns or controls to make,
* have made, use, import, offer to sell and sell ("Utilize") this software subject to the
* terms herein.  With respect to the foregoing patent license, such license is granted
* solely to the extent that any such patent is necessary to Utilize the software alone.
* The patent license shall not apply to any combinations which include this software,
* other than combinations with devices manufactured by or for TI ("TI Devices").
* No hardware patent is licensed hereunder.
*
* Redistributions must preserve existing copyright notices and reproduce this license
* (including the above copyright notice and the disclaimer and (if applicable) source
* code license limitations below) in the documentation and/or other materials provided
* with the distribution
*
* Redistribution and use in binary form, without modification, are permitted provided
* that the following conditions are met:
*
* *       No reverse engineering, decompilation, or disassembly of this software is
* permitted with respect to any software provided in binary form.
*
* *       any redistribution and use are licensed by TI for use only with TI Devices.
*
* *       Nothing shall obligate TI to provide you with source code for the software
* licensed and provided to you in object code.
*
* If software source code is provided to you, modification and redistribution of the
* source code are permitted provided that the following conditions are met:
*
* *       any redistribution and use of the source code, including any resulting derivative
* works, are licensed by TI for use only with TI Devices.
*
* *       any redistribution and use of any object code compiled from the source code
* and any resulting derivative works, are licensed by TI for use only with TI Devices.
*
* Neither the name of Texas Instruments Incorporated nor the names of its suppliers
*
* may be used to endorse or promote products derived from this software without
* specific prior written permission.
*
* DISCLAIMER.
*
* THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
* OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
* OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
* IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
* BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
* OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
* OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
* OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/**
 *  \file main_rtos.c
 *
 *  \brief Main file for RTOS build
 */

/* ========================================================================== */
/*                             Include Files                                  */
/* ========================================================================== */
/* RTOS Header files */

#if defined(SYSBIOS) && defined(LOW_LATENCY_DEMO)
#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Task.h>
#include <ti/sysbios/utils/Load.h>
#endif //SYSBIOS

#include <ti/osal/osal.h>
#include <ti/osal/TaskP.h>
#include <ti/osal/LoadP.h>

#include <ti/csl/arch/csl_arch.h>
#include <ti/csl/soc.h>
#include <ti/csl/cslr.h>

#include "CanApp_Startup.h"
#include "can_profile.h"
#include "CanIf.h"
#if (defined (BUILD_MCU1_0) && (defined (SOC_J721E) || defined (SOC_J7200)))
#include <ti/drv/sciclient/sciserver_tirtos.h>
#endif
/* ========================================================================== */
/*                           Macros & Typedefs                                */
/* ========================================================================== */

/* Test application stack size */
#define APP_TASK_STACK                  (10U * 1024U)
/* Stack size for Rx task */
#define RX_TASK_STACK                   (10U * 1024U)
/* Stack size for Rx task */
#define LOAD_MEASURE_TASK_STACK         (2U * 1024U)
/* Stack size for Rx task */
#define CAN_PROFILE_DEMO_TASK_NAME      ("CAN PROFILE")
/**< Task for sending CAN messages*/
#define CAN_PROFILE_LOAD_MEASURE_TASK   ("LOAD MEASUREMENT")
/**< Task for measuring CPU load */
#define CAN_PROFILE_DEMO_RX_TASK        ("CAN PROFILE_RX")
/**< Task to receive packets */

/* ========================================================================== */
/*                         Structure Declarations                             */
/* ========================================================================== */

/* None */

/* ========================================================================== */
/*                          Function Declarations                             */
/* ========================================================================== */

static void CanApp_TaskFxn(void* a0, void* a1);
#ifdef LOW_LATENCY_DEMO
static void CanApp_LoadMeasureFxn(void* a0, void* a1);
#endif //LOW_LATENCY_DEMO
static void CanApp_RxTask(void* a0, void* a1);
static void CanApp_Shutdown(void);
int32_t SetupSciServer(void);
/* ========================================================================== */
/*                            Global Variables                                */
/* ========================================================================== */
/* Application Stack */
static uint8_t CanApp_TaskStack[APP_TASK_STACK] __attribute__((aligned(32)));
/* Rx Stack */
static uint8_t CanApp_RxStack[RX_TASK_STACK] __attribute__((aligned(32)));
#ifdef LOW_LATENCY_DEMO
/* Load measurement Stack */
static uint8_t CanApp_LoadMeasureStack[LOAD_MEASURE_TASK_STACK] __attribute__((aligned(32)));
#endif //LOW_LATENCY_DEMO
/**< Stack for the task */
canAppTaskObj_t  CanApp_TestPrms;
/**< Test parameters */
static HwiP_Handle      CanApp_IsrHndls[CAN_MAX_CONTROLLER];
/**< Stores the ISR handles */
extern SemaphoreP_Handle CanIf_TxConfirmationSemaphore;
/**< TX Confirmation semaphore, would be posted when TX completes */
extern SemaphoreP_Handle CanIf_RxConfirmationSemaphore;
/**< Rx Confirmation semaphore, would be posted when TX completes */

#ifdef LOW_LATENCY_DEMO
uint32_t avgCpuLoad = 0;
/**GLobal variable to record average CPU load*/
#endif //LOW_LATENCY_DEMO

/* ========================================================================== */
/*                            External Variables                              */
/* ========================================================================== */
extern uint32 CanIf_DrvStatus;
/**< CAN IF Driver Status, defined in CanIf.c */

/* ========================================================================== */
/*                          Function Definitions                              */
/* ========================================================================== */

int main(void)
{
    TaskP_Handle task;
    TaskP_Params taskParams;
    int32_t ret = CSL_PASS;

#ifdef UART_ENABLED
    AppUtils_Init();
#endif

    /* Initialize SCI Client Server */
    ret = SetupSciServer();
    if(ret != CSL_PASS)
    {
        OS_stop();
    }
    CanApp_Startup();
    CanApp_PowerAndClkSrc();

    /* Initialize dummy CAN IF */
    CanIf_Init(NULL);

    if (CANIF_DRV_INITIALIZED == CanIf_DrvStatus)
    {
        /* Initialize the task params */
        TaskP_Params_init(&taskParams);
        taskParams.name = CAN_PROFILE_DEMO_TASK_NAME;
        /* Set the task priority higher than the default priority (1) */
        taskParams.priority     = 4;
        taskParams.stack        = CanApp_TaskStack;
        taskParams.stacksize    = sizeof (CanApp_TaskStack);

        task = TaskP_create(CanApp_TaskFxn, &taskParams);
        if(NULL == task)
        {
            OS_stop();
        }
        #ifdef LOW_LATENCY_DEMO
        /* Initialize the task params */
        TaskP_Params_init(&taskParams);
        taskParams.name = CAN_PROFILE_LOAD_MEASURE_TASK;
        /* Set the task priority higher than the default priority (1) */
        taskParams.priority     = 2;
        taskParams.stack        = CanApp_LoadMeasureStack;
        taskParams.stacksize    = sizeof (CanApp_LoadMeasureStack);

        task = TaskP_create(CanApp_LoadMeasureFxn, &taskParams);
        if(NULL == task)
        {
            OS_stop();
        }
        #endif //LOW_LATENCY_DEMO

        /* Initialize the task params */
        TaskP_Params_init(&taskParams);
        taskParams.name = CAN_PROFILE_DEMO_RX_TASK;
        /* Set the task priority higher than the default priority (1) */
        taskParams.priority     = 4;
        taskParams.stack        = CanApp_RxStack;
        taskParams.stacksize    = sizeof (CanApp_RxStack);

        task = TaskP_create(CanApp_RxTask, &taskParams);
        if(NULL == task)
        {
            OS_stop();
        }

        CanApp_TestPrms.runApp = TRUE;

        OS_start();    /* does not return */
    }
    else
    {
        AppUtils_Printf(MSG_NORMAL, MSG_APP_NAME
                " Error : CAN IF not initialized !!!\n");
    }

    return(0);
}
#ifdef LOW_LATENCY_DEMO
static void CanApp_LoadMeasureFxn(void* a0, void* a1)
{
    uint32_t cpuLoad;
    #if defined(FREERTOS)
    LoadP_reset();
    #endif
    while(CanApp_TestPrms.runApp)
    {
        TaskP_sleep(500);
        #if defined(FREERTOS)
        /*TODO : LoadP_getCPULoad is only implemented for FreeRTOS right now*/
        cpuLoad = LoadP_getCPULoad();
        avgCpuLoad = cpuLoad;
        #else
        cpuLoad = Load_getCPULoad();
        avgCpuLoad = (avgCpuLoad + cpuLoad) / 2;
        #endif //FREERTOS
    }
}
#endif //LOW_LATENCY_DEMO

static void CanApp_TaskFxn(void* a0, void* a1)
{
    CanApp_PlatformInit();

    TaskP_yield();

    CanApp_IPC_App_Init();

    CanApp_SetupCAN();

    while(CanApp_TestPrms.runApp)
    {
        TaskP_sleep(1);
    #ifndef LOW_LATENCY_DEMO
        /*Do we have to send packets*/
        if(CanApp_TestPrms.sendCANFrames)
        {
           send_CAN_frames();
        }

        if(CanApp_TestPrms.sendCANStats)
        {
           CanApp_SendStats();
        }
    #endif //LOW_LATENCY_DEMO         
    }
    CanApp_Shutdown();

    return;
}

static void CanApp_RxTask(void* a0, void* a1)
{
    /*This allows CAN Init code to execute before*/
    TaskP_sleep(1);

    /*There is an infinite loop inside*/
    CanApp_RxFunc();

    return;
}

/** \brief Application tear down functions */
static void CanApp_Shutdown(void)
{
    uint32 idx;

    for (idx = 0U; idx < CAN_MAX_CONTROLLER; idx++)
    {
        if (NULL != CanApp_IsrHndls[idx])
        {
            if (HwiP_OK != HwiP_delete(CanApp_IsrHndls[idx]))
            {
                AppUtils_Printf(MSG_NORMAL, MSG_APP_NAME
                                " Error!!! Could not De register"
                                " the ISR for instance %d!!!\n", idx);
                break;
            }
        }
    }

    if (NULL != CanIf_TxConfirmationSemaphore)
    {
        SemaphoreP_delete(CanIf_TxConfirmationSemaphore);
        if (NULL != CanIf_RxConfirmationSemaphore)
        {
            SemaphoreP_delete(CanIf_RxConfirmationSemaphore);
        }
    }
    return;
}

int32_t SetupSciServer(void)
{
    int32_t ret = CSL_PASS;
    Sciclient_ConfigPrms_t clientPrms;
    Sciserver_TirtosCfgPrms_t appPrms;

    /* Sciclient needs to be initialized before Sciserver. Sciserver depends on
     * Sciclient API to execute message forwarding */
    ret = Sciclient_configPrmsInit(&clientPrms);
    
    if (ret == CSL_PASS)
    {
        ret = Sciclient_init(&clientPrms);
    }

    if (ret == CSL_PASS)
    {
        ret = Sciserver_tirtosInitPrms_Init(&appPrms);
    }

    if (ret == CSL_PASS)
    {
        ret = Sciserver_tirtosInit(&appPrms);
    }

    if (ret == CSL_PASS)
    {
        AppUtils_Printf(MSG_NORMAL, "Starting Sciserver..... PASSED\n");
    }
    else
    {
        AppUtils_Printf(MSG_NORMAL, "Starting Sciserver..... FAILED\n");
    }

    return ret;

}

