/*
*
* Copyright (c) 2019-2020 Texas Instruments Incorporated
*
* All rights reserved not granted herein.
*
* Limited License.
*
* Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
* license under copyrights and patents it now or hereafter owns or controls to make,
* have made, use, import, offer to sell and sell ("Utilize") this software subject to the
* terms herein.  With respect to the foregoing patent license, such license is granted
* solely to the extent that any such patent is necessary to Utilize the software alone.
* The patent license shall not apply to any combinations which include this software,
* other than combinations with devices manufactured by or for TI ("TI Devices").
* No hardware patent is licensed hereunder.
*
* Redistributions must preserve existing copyright notices and reproduce this license
* (including the above copyright notice and the disclaimer and (if applicable) source
* code license limitations below) in the documentation and/or other materials provided
* with the distribution
*
* Redistribution and use in binary form, without modification, are permitted provided
* that the following conditions are met:
*
* *       No reverse engineering, decompilation, or disassembly of this software is
* permitted with respect to any software provided in binary form.
*
* *       any redistribution and use are licensed by TI for use only with TI Devices.
*
* *       Nothing shall obligate TI to provide you with source code for the software
* licensed and provided to you in object code.
*
* If software source code is provided to you, modification and redistribution of the
* source code are permitted provided that the following conditions are met:
*
* *       any redistribution and use of the source code, including any resulting derivative
* works, are licensed by TI for use only with TI Devices.
*
* *       any redistribution and use of any object code compiled from the source code
* and any resulting derivative works, are licensed by TI for use only with TI Devices.
*
* Neither the name of Texas Instruments Incorporated nor the names of its suppliers
*
* may be used to endorse or promote products derived from this software without
* specific prior written permission.
*
* DISCLAIMER.
*
* THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
* OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
* OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
* IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
* BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
* OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
* OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
* OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/**
 *  \file     can_profile.h
 *
 *  \brief    Common header file for this application
 *
 */
/**
 * \defgroup MCUSS_APP_CAN_PROFILE Can profiling application
 *
 *  This application perform CAN transmission on all the enabled instances.
 *  By default the CAN peripheral is configured to operate in loop-back mode,
 *  i.e. transmit and receive.
 *
 *  TI RTOS is used and CAN tx/rx is performed in a task, The CAN IF stub is
 *  is used to indicate completion of transmission / reception by posting a
 *  semaphore.
 * @{
 */
#ifndef CAN_PROFILE_H_
#define CAN_PROFILE_H_

/* ========================================================================== */
/*                             Include Files                                  */
/* ========================================================================== */
#include "string.h"
#include "stdlib.h"
#include "Std_Types.h"
#include "Det.h"
#include "Dem.h"
#include "Os.h"
#include "Can.h"
#include "CanIf_Cbk.h"
#include "EcuM_Cbk.h"
#include "Dio.h"

#include "CanApp_Startup.h"
#include "app_utils.h" /* MCAL Example utilities */
#include "utils_prf.h" /* Demo utilities */
#ifdef __cplusplus
extern "C"
{
#endif

/* ========================================================================== */
/*                       Application Configurations                           */
/* ========================================================================== */
#define APP_INSTANCE_1_INST_IN_CFG_ONLY     (STD_ON)
/**< Enables application to use 1st instance configured, useful when profiling
        has to be done for 1 instance only
        STD_OFF Configures to use all instances specified in the configuration
        STD_ON Configures to use only the first instance of MCAN */
#define CAN_TX_ONLY_MODE                    (STD_ON)
/**< Enables application to run in Transmit only mode, if set to STD_OFF then
     application works in Recieve only mode */
#define CAN_INITIAL_PRINT_DISABLE_BEFORE_CAN_RESPONSE (STD_OFF)
/**< Disables prints before sending/receiving of CAN messages, this is done so
     that the time for printing doesn't get accounted for in the CAN resonse
     calculation */
#define CAN_EXT_LOOPBACK                    (STD_ON)
/**< Enables external loopback in the application
     In MAIN domain external loopback from MCAN4 -> MCAN9 on the GESI card
     MCAN4 and MCAN9 should be connected CANH to CANH, CANL to CANL and GND to
     GND
     In MCU domain external loopback from MCAN0 -> MCAN1
     MCAN0 and MCAN1 should be connected CANH to CANH, CANL to CANL and GND to
     GND
     */
#define CAN_BATCH_PROCESSING_POLLING        (STD_OFF)

#define APP_MCU_MCAN0_IDX                   (0U)
#define APP_MCU_MCAN1_IDX                   (1U)
#define APP_MCAN0_IDX                       (0U)
#define APP_MCAN4_IDX                       (1U)
#define APP_MCAN8_IDX                       (2U)
#define APP_MCAN10_IDX                      (3U)

#ifdef LOW_LATENCY_DEMO
#define MCAN_NUM_PDU_IN_Q       (50U)
/**< Number of PDU's in the queue */
#define MCAN_MIN_PDU_SIZE       (8U)
/**< Max size of a PDU in bytes. From spec */
#define MCAN_MAX_PDU_SIZE       (64U)
/**< Max size of a PDU in bytes. From spec */
#define MCAN_MAX_Q_SIZE         (MCAN_NUM_PDU_IN_Q * MCAN_MAX_PDU_SIZE)
#endif //LOW_LATENCY_DEMO
/* ========================================================================== */
/*                           Macros & Typedefs                                */
/* ========================================================================== */
#define MSG_NORMAL              (APP_UTILS_PRINT_MSG_NORMAL)
/**< Message type */
#define MSG_STATUS              (APP_UTILS_PRINT_MSG_STATUS)
/**< Message type */
#define MSG_APP_NAME            "CAN Profile App:"

/* ========================================================================== */
/*                         Structures and Enums                               */
/* ========================================================================== */

typedef struct canAppTaskObj
{
    uint32 numTxPackets;
    /**< Number of CAN packets to be transmitted */
    uint32 actualPacketsSent;
    /**< Actual number of packets sent */
    uint32 numRxPackets;
    /**< Number of CAN packets to be received */
    bool  runApp;
    /**< setting this to False will result in application exit*/
    uint8 sendCANFrames;
    /**< If this is set then CAN frames are set*/
    uint8 sendCANStats;
    /**< If this is set then CAN stats are sent back*/
    uint8 testMode;
    /**< 0 : Unset, 1 : CAN 2 CAN mode, 2 : CAN 2 Eth 3, : Eth 2 CAN */
    uint32 max_can_2_can_bridge_delay;
    /**< Min time spent inside device for CAN 2 CAN as reported by gateway*/
    uint32 avg_can_2_can_bridge_delay;
    /**< Min time spent inside device for CAN 2 CAN as reported by gateway*/
    uint32 max_eth_2_can_bridge_delay;
    /**< Min time spent inside device for ETH 2 CAN as reported by gateway*/
    uint32 avg_eth_2_can_bridge_delay;
    /**< Min time spent inside device for ETH 2 CAN as reported by gateway*/
}canAppTaskObj_t;


#ifdef LOW_LATENCY_DEMO
/** \brief Ethernet Queue structure */
typedef struct CanApp_Queue_s
{
    uint8_t *currentRdPtr;
    /**< Consumer task fetches from this index */

    uint8_t *currentWrPtr;
    /**< Producer task writes to this index */

    uint8_t *topOfQueue;
    /**< Top of queue */

    uint8_t *bottomOfQueue;
    /**< Bottom of queue */

    uint64_t numPduEnqueued;
    /**< Number of bytes written to buffer by Producer */

    uint64_t numPduDequeued;
    /**< Number of bytes consumed from buffer by Consumer*/

    uint64_t numOverflowEvt;
    /**< Number of overflow events*/

    uint64_t numCanBusBusy;
    /**< Number of overflow events*/
    
} CanApp_Queue;

typedef struct Can_PduHdrType_s {
    PduIdType   swPduHandle;
    /**< private data for CanIf,just save and use for callback */
    uint8       length;
    /**< Length, max 8 bytes/64 bytes for CAN FD */
    uint8       srcPort;
    /**< Port on which CAN was received */
    Can_IdType  id;
    /**< the CAN ID, 29 or 11-bit */
    uint64      rxTS;
    /**< Time at which the packet was received */

} Can_PduHdrType;


/** \brief Ethernet Descriptor Queue structure */
typedef struct CanApp_DescQueue_s
{
    Can_PduHdrType  Can_Pdu[MCAN_NUM_PDU_IN_Q];
    /**<  Array of PDU headers which serves as descriptor*/

    uint8_t currentRdIndex;
    /**< Consumer task fetches from this index */

    uint8_t currentWrIndex;
    /**< Producer task writes to this index */
    
} CanApp_DescQ;

/** \brief Ethernet test state */
typedef struct CanApp_State_s
{
    CanApp_Queue canQueue;
    /**< Main queue structure */

    CanApp_DescQ canDescQueue;
    /**< Descriptor queue structure */

} CanApp_State;

#endif //LOW_LATENCY_DEMO

/* ========================================================================== */
/*                          Function Declarations                             */
/* ========================================================================== */

sint32 CanApp_SetupCAN(void);
void CanApp_RxFunc(void);
void CanApp_IPC_App_Init(void);
void CanApp_CanXIsr(uintptr_t CanPtr);
int32_t CanApp_receiveFrames(const PduInfoType* PduInfoPtr);
#ifdef LOW_LATENCY_DEMO
void send_CAN_frames(void);
#else
uint32 send_CAN_frames(void);
uint32 CanApp_SendStats(void);
#endif //LOW_LATENCY_DEMO
#ifdef __cplusplus
}
#endif

#endif  /* #ifndef CAN_PROFILE_H_ */
/* @} */
