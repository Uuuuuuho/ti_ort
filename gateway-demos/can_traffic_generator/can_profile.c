/*
*
* Copyright (c) 2019-2020 Texas Instruments Incorporated
*
* All rights reserved not granted herein.
*
* Limited License.
*
* Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
* license under copyrights and patents it now or hereafter owns or controls to make,
* have made, use, import, offer to sell and sell ("Utilize") this software subject to the
* terms herein.  With respect to the foregoing patent license, such license is granted
* solely to the extent that any such patent is necessary to Utilize the software alone.
* The patent license shall not apply to any combinations which include this software,
* other than combinations with devices manufactured by or for TI ("TI Devices").
* No hardware patent is licensed hereunder.
*
* Redistributions must preserve existing copyright notices and reproduce this license
* (including the above copyright notice and the disclaimer and (if applicable) source
* code license limitations below) in the documentation and/or other materials provided
* with the distribution
*
* Redistribution and use in binary form, without modification, are permitted provided
* that the following conditions are met:
*
* *       No reverse engineering, decompilation, or disassembly of this software is
* permitted with respect to any software provided in binary form.
*
* *       any redistribution and use are licensed by TI for use only with TI Devices.
*
* *       Nothing shall obligate TI to provide you with source code for the software
* licensed and provided to you in object code.
*
* If software source code is provided to you, modification and redistribution of the
* source code are permitted provided that the following conditions are met:
*
* *       any redistribution and use of the source code, including any resulting derivative
* works, are licensed by TI for use only with TI Devices.
*
* *       any redistribution and use of any object code compiled from the source code
* and any resulting derivative works, are licensed by TI for use only with TI Devices.
*
* Neither the name of Texas Instruments Incorporated nor the names of its suppliers
*
* may be used to endorse or promote products derived from this software without
* specific prior written permission.
*
* DISCLAIMER.
*
* THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
* OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
* OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
* IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
* BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
* OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
* OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
* OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/**
 *  \file     can_profile.c
 *
 *  \brief    This file implements CAN profiling application
 */

/* ========================================================================== */
/*                             Include Files                                  */
/* ========================================================================== */
#include "Std_Types.h"
#include "Det.h"
#include "Dem.h"
#include "Os.h"
#include "Can.h"
#include "CanIf_Cbk.h"
#include "EcuM_Cbk.h"
#include "Dio.h"

#include <ti/csl/arch/csl_arch.h>
#include <ti/csl/soc.h>
#include <ti/csl/cslr.h>

#include "can_profile.h"

/* ========================================================================== */
/*                           Macros & Typedefs                                */
/* ========================================================================== */
#define CAN_APP_E_DEM   (E_NOT_OK + 1U)
/**< Error code to indicate DEM */
#define CAN_APP_E_DET   (E_NOT_OK + 2U)
/**< Error code to indicate DEM */

#define BATCH_SIZE      (8U)
/**< Batch size for batch processing */
#define NUM_CAN_FRAMES_IN_SEC   (5300)
/**< Used to calculate, how many packets to send */
#define CAN_2_CAN_FRAME_ID                    (0xA0)
/**< Special identifier used to mark a CAN frame generated from CAN frame for the PC tool*/
#define ETH_2_CAN_FRAME_ID                    (0xB0)
/**< Special identifier used to mark an CAN frame routed from another Eth frame for the PC tool*/

#define GENERATOR_PKT_MARKER                  (0xA0)
/**< CAN ID for MCU MCAN1 port (CAN generator App Rx port)*/
#define GENERATOR_STAT_REQUEST_MARKER         (0xB0)
/**< This CAN ID tells CAN generator to send statistics*/
#define CAN_ID_FOR_MCAN9                      (0xE0)
/**< CAN ID for Main MCAN9*/
#define SPL_BYTE_FOR_STAT_FRAME               (0xAA)
/**< Spl byte to indicate to */

/* ========================================================================== */
/*              Internal Function Declarations                                */
/* ========================================================================== */
uint64_t CanProfileGetTimeSpent(uint64_t preTs, uint64_t postTs);

/* ========================================================================== */
/*                            Global Variables                                */
/* ========================================================================== */
uint32                      CanApp_ProfileStatus = E_OK;
/**< Variable used for Demo status */
Can_PduType                 CanApp_Pdu, CanApp_Pdu1;
Can_PduType                 CanApp_Pdu2, CanApp_Pdu3, CanApp_Pdu4, CanApp_Pdu5;
/**< Variables which contains Can PDU data used in Can transmission */
uint8                       CanApp_InputData[64U] = {0};
/*Frame used for sending CAN statistics back to Gateway*/
uint8  CanApp_Stats_Frame[64U] = {0};
/**< Variable which contains Can SDU data that is transported inside the PDU */
volatile PduInfoType                 *CanApp_RxPdu = NULL;
/**< Pointer received data */
uint32                      CanApp_DemStatus = E_OK;
/**< Variable used to track dem notifications */
uint32                      CanApp_DetStatus = E_OK;
/**< Variable used to track det notifications */
#if (CAN_TX_ONLY_MODE == STD_ON)
const Can_PduType   *CanApp_PduInfo;
/**< Pointer to Can PDU used in Can transmission  */
uint8               CanApp_Hth = 0U;
/**< Hardware Mailbox Number configured to trigger transmission */
uint32              CanApp_Mask = 0x00000000U;
/**< Mask is used for checking received message id with transmitted message */
#endif

#ifdef LOW_LATENCY_DEMO
uint32_t bridge_delay_can_2_can = 0;
/**< Bridge delay for last CAN 2 CAN route */

uint64_t avgLatency = 0;
uint64_t maxLatency = 0;

CanApp_State canAppState;
/**< Global structure used to track state variables */

#endif //LOW_LATENCY_DEMO

/* ========================================================================== */
/*                            External Variables                              */
/* ========================================================================== */
/* Global variables used in callback functions */
extern volatile P2VAR(Can_HwType, CANIF_VAR_CLEARED, CANIF_APPL_DATA)
        CanIf_Mailbox;

extern volatile VAR(uint32, CANIF_VAR_CLEARED) CanIf_TxConfirmationCount;
/**< TX Confirmation counter */
extern volatile VAR(uint32, CANIF_VAR_CLEARED) CanIf_RxConfirmationCount;
/**< RX Indication counter */
extern int32_t (*push_frame_to_app_queue)(const PduInfoType*);
/**< Function pointer used to process CAN data */

extern SemaphoreP_Handle CanIf_TxConfirmationSemaphore;
/**< TX Confirmation semaphore */
extern SemaphoreP_Handle CanIf_RxConfirmationSemaphore;
/**< Rx Confirmation semaphore */

extern volatile PduInfoType CanIf_PduInfo;

extern canAppTaskObj_t  CanApp_TestPrms;

#ifdef LOW_LATENCY_DEMO
extern uint32_t avgCpuLoad;
/**GLobal variable to record average CPU load*/
#endif //LOW_LATENCY_DEMO

/* ========================================================================== */
/*                        PMU setup code for reference                        */
/* ========================================================================== */
#ifdef PMU_PROFILE

#define PMU_EVENT_COUNTER_1 (CSL_ARM_R5_PMU_EVENT_TYPE_CYCLE_CNT)
#define PMU_EVENT_COUNTER_2 (CSL_ARM_R5_PMU_EVENT_TYPE_ICACHE_STALL)
#define PMU_EVENT_COUNTER_3 (CSL_ARM_R5_PMU_EVENT_TYPE_DCACHE_MISS)

/* To use PMU instrumentation, call the config_pmu() once during init time.
 *
 * Reset the counters using CSL_armR5PmuResetCntrs() API.
 * Then use CSL_armR5PmuReadCntr() with argument 0, 1 or 2 to capture value
 * of 1st, 2nd or 3rd PMU counter. 
 */

uint32_t config_pmu(void)
{

    CSL_armR5PmuCfg(0, 0 ,1);
    CSL_armR5PmuEnableAllCntrs(1);
    uint32_t num_cnt = CSL_armR5PmuGetNumCntrs();

    CSL_armR5PmuCfgCntr(0, PMU_EVENT_COUNTER_1);
    CSL_armR5PmuCfgCntr(1, PMU_EVENT_COUNTER_2);
    CSL_armR5PmuCfgCntr(2, PMU_EVENT_COUNTER_3);

    CSL_armR5PmuEnableCntrOverflowIntr(0, 0);
    CSL_armR5PmuEnableCntrOverflowIntr(1, 0);
    CSL_armR5PmuEnableCntrOverflowIntr(2, 0);
    CSL_armR5PmuResetCntrs();
    CSL_armR5PmuEnableCntr(0, 1);
    CSL_armR5PmuEnableCntr(1, 1);
    CSL_armR5PmuEnableCntr(2, 1);
    return 0;
}

#endif
/* ========================================================================== */
/*                          Function Definitions                              */
/* ========================================================================== */
sint32 CanApp_SetupCAN(void)
{
    Std_ReturnType      status;
    volatile uint32 cookie;

#if (APP_INSTANCE_1_INST_IN_CFG_ONLY == STD_OFF)
    const Can_ConfigType *Can_ConfigPtr;
#if (STD_ON == CAN_VARIANT_PRE_COMPILE)
    Can_ConfigPtr = &CAN_INIT_CONFIG_PC;
#else
    Can_ConfigPtr = &CanConfigSet;
#endif
#endif

    CanApp_RxPdu = &CanIf_PduInfo;
    /* Message for CAN-FD(MCU MCAN0 module) */
#if ((CAN_TX_ONLY_MODE == STD_ON) || (CAN_EXT_LOOPBACK == STD_ON))
    /* Setting Up CAN FD Extended Frame*/
    CanApp_Pdu.id          = 0xC0 | 0xC0000000U;
    CanApp_Pdu.length      = 64U;
    CanApp_Pdu.swPduHandle = 1U;
    CanApp_Pdu.sdu         = &CanApp_InputData[0U];

    /* Message for CAN-FD(MCU MCAN1 module) */
    /* Setting Up CAN FD Extended Frame*/
    CanApp_Pdu1.id          = 0xB0 | 0xC0000000U;
    CanApp_Pdu1.length      = 64U;
    CanApp_Pdu1.swPduHandle = 2U;
    CanApp_Pdu1.sdu         = &CanApp_InputData[0U];

    CanApp_DemStatus = E_OK;
    CanApp_DetStatus = E_OK;
#endif
    
    CanIf_TxConfirmationCount = 0;
    CanIf_RxConfirmationCount = 0;

    CanApp_ProfileStatus = E_OK;

    /* Set Can frame */
    CanApp_SetupCanFrame(0, &CanApp_Hth, &CanApp_Mask);

    /* Set Can frame */
    CanApp_SetupCanFrameExternalLoopback(&CanApp_Hth, &CanApp_Mask);
    /* Enable Tx and Rx controller for external loopback mode */
    status = CanApp_SetupControllersExternalLoopback(CAN_CS_STARTED);
    if (status != E_OK)
    {
        CanApp_ProfileStatus = E_NOT_OK;
        AppUtils_Printf(MSG_NORMAL, MSG_APP_NAME
            " Unable to setup CAN port !!!\n");
    }

    return (CanApp_ProfileStatus);
}
#ifdef LOW_LATENCY_DEMO
void send_CAN_frames()
{
    uint64_t         currentTS;
    uint32_t         canID = 0;
    Std_ReturnType   status[5];
    const Can_PduType   *PduInfo;
    Can_PduType     CanApp_Pdu_2;
    uint8            hth = 0U;
    uint16_t        index = 0;
    uint64_t        rxTS;

    if(canAppState.canDescQueue.currentRdIndex >= MCAN_NUM_PDU_IN_Q)
    {
        canAppState.canDescQueue.currentRdIndex = 0;
    }

    /*Check wraparound and reset Rd ptr*/
    if((canAppState.canQueue.currentRdPtr + canAppState.canDescQueue.Can_Pdu[canAppState.canDescQueue.currentRdIndex].length) > canAppState.canQueue.bottomOfQueue)
    {
        canAppState.canQueue.currentRdPtr = canAppState.canQueue.topOfQueue;
    }

    /*Get CAN ID and pointer to payload for embedding into frame*/
    index = canAppState.canDescQueue.currentRdIndex;
    canID = canAppState.canDescQueue.Can_Pdu[index].id;    
    /*Get rx timestamp*/
    rxTS = canAppState.canDescQueue.Can_Pdu[index].rxTS;

    CanApp_Pdu_2.length  = canAppState.canDescQueue.Can_Pdu[index].length;
    CanApp_Pdu_2.id = canID | 0xC0000000;

    /*Point to the packet data*/
    CanApp_Pdu_2.sdu = canAppState.canQueue.currentRdPtr;

    /*Send on MCU MCAN1 which is connected to Main MCAN9*/
    CanApp_Pdu_2.swPduHandle = 2U;
    hth = CAN_HTRH_2;
    PduInfo = &CanApp_Pdu_2;

    /*This tells the pcan automation tool that this delay is for CAN 2 CAN*/
    canAppState.canQueue.currentRdPtr[6] = CAN_2_CAN_FRAME_ID;
    /*Embed the bridge delay for last CAN 2 CAN routing in the payload*/
    memcpy((canAppState.canQueue.currentRdPtr + 7), &bridge_delay_can_2_can, 4);
    *(canAppState.canQueue.currentRdPtr + 11) = avgCpuLoad;

    status[0] = Can_Write(hth, PduInfo);
    currentTS = TimerP_getTimeInUsecs();
    Can_MainFunction_Write();       

    if (status[0] == E_OK)
    {        
        bridge_delay_can_2_can = currentTS - rxTS;

        if(bridge_delay_can_2_can > maxLatency)
        {
            maxLatency = bridge_delay_can_2_can;
        }

        avgLatency = (bridge_delay_can_2_can + avgLatency) / 2;
    }
    else
    {        
        canAppState.canQueue.numCanBusBusy++;      
    }    

    /*Update read pointer*/
    canAppState.canQueue.currentRdPtr += canAppState.canDescQueue.Can_Pdu[canAppState.canDescQueue.currentRdIndex].length;
    /*Update count*/
    canAppState.canQueue.numPduDequeued++;
    /*Check wraparound and reset for descriptor Q*/
    canAppState.canDescQueue.currentRdIndex++;
}

#else //LOW_LATENCY_DEMO
uint32 send_CAN_frames(void)
{
    Std_ReturnType      writeStatus[BATCH_SIZE];
    uint32              loopCnt;

    const Can_PduType   *PduInfo;
    Can_PduType     CanApp_Pdu_2;
    uint8            hth = 0U;

    /*Send message to Generator app over MCAN8/9*/
    CanApp_Pdu_2.length  = 64U;
    CanApp_Pdu_2.id = CAN_ID_FOR_MCAN9 | 0xC0000000;

    /*Point to the packet data*/
    CanApp_Pdu_2.sdu = CanApp_InputData;

    /*Send on MCU MCAN1 which is connected to Main MCAN9*/
    CanApp_Pdu_2.swPduHandle = 2U;
    hth = CAN_HTRH_2;
    PduInfo = &CanApp_Pdu_2;

    AppUtils_Printf(MSG_NORMAL, MSG_APP_NAME
            " Going to send %d messages now !!!\n", CanApp_TestPrms.numTxPackets);

    CanApp_ProfileStatus = E_OK;
    
    for (loopCnt = 0U; loopCnt < CanApp_TestPrms.numTxPackets; loopCnt+=5)
    {

        /* Do Can Write to send the CanApp_InputData on CAN bus */
        writeStatus[0] = Can_Write(hth, PduInfo);
        writeStatus[1] = Can_Write(hth, PduInfo);
        writeStatus[2] = Can_Write(hth, PduInfo);
        writeStatus[3] = Can_Write(hth, PduInfo);
        writeStatus[4] = Can_Write(hth, PduInfo);

        if ((writeStatus[0] == E_OK) && (writeStatus[1] == E_OK) &&
            (writeStatus[2] == E_OK) && (writeStatus[3] == E_OK) &&
            (writeStatus[4] == E_OK))
        {
            Can_MainFunction_Write();
            CanApp_TestPrms.actualPacketsSent+=5;
        }

        if (writeStatus[0] == E_OK)
        {
            Can_MainFunction_Write();
            TaskP_sleep(1);
        }
        else
        {
            CanApp_ProfileStatus = E_NOT_OK;
        }
    }

    /*Set to 0, so we don't keep sending frames*/
    CanApp_TestPrms.sendCANFrames = 0;

    AppUtils_Printf(MSG_NORMAL, MSG_APP_NAME
            " End of messages !!!\n");

    return CanApp_ProfileStatus;

}
#endif //LOW_LATENCY_DEMO

uint64_t CanProfileGetTimeSpent(uint64_t preTs, uint64_t postTs)
{
    uint64_t rtnTs;

    if (postTs >= preTs)
    {
        rtnTs = postTs - preTs;
    }
    else
    {
        rtnTs = postTs + (0xFFFFFFFFFFFFFFFF - preTs);
    }

    return (rtnTs);
}

#define CAN_START_SEC_ISR_CODE
#include "Can_MemMap.h"

CAN_ISR_TEXT_SECTION FUNC(void, CAN_CODE_FAST) CanApp_CanXIsr (
                                                            uintptr_t CanPtr)
{
    CanApp_IsrType canChIsr = (CanApp_IsrType)CanPtr;
    /* Associated CAN ISR */
    canChIsr();
}

void CanApp_RxFunc(void)
{
    /*Infinite loop to receive packets*/
    while(CanApp_TestPrms.runApp)
    {
        SemaphoreP_pend(CanIf_RxConfirmationSemaphore,
                                    SemaphoreP_WAIT_FOREVER);
    #ifdef LOW_LATENCY_DEMO
        send_CAN_frames();
    #else
        TaskP_sleep(1);
    #endif //LOW_LATENCY_DEMO

    }    
}

int32_t CanApp_receiveFrames(const PduInfoType* PduInfoPtr)
{
#ifdef LOW_LATENCY_DEMO
     uint8_t index;

    /*First check if there is enough space below write pointer till bottom of queue*/
    if((canAppState.canQueue.currentWrPtr + PduInfoPtr->SduLength) > canAppState.canQueue.bottomOfQueue)
    {
        /*If we wrapped around then would there be enough space*/
        if((canAppState.canQueue.currentRdPtr - canAppState.canQueue.topOfQueue) <  PduInfoPtr->SduLength)
        {
            canAppState.canQueue.numOverflowEvt++;  
            return -1; /*No space*/
        }
        else
        {
            /* We can wrap around so reset*/
            canAppState.canQueue.currentWrPtr = canAppState.canQueue.topOfQueue;
        }                
    }
    else 
    {
        /*We need to test for a special case where write pointer has wrapped around but read pointer is stuck*/
        if(canAppState.canQueue.currentWrPtr == canAppState.canQueue.currentRdPtr)
        {
            /*If they are same then check number of bytes pushed and pulled off the queue*/
            if(canAppState.canQueue.numPduEnqueued != canAppState.canQueue.numPduDequeued)
            {
                canAppState.canQueue.numOverflowEvt++;
                return -1; /*No space*/
            }
        }    
    }

    /*Get the timestamp*/
    index = canAppState.canDescQueue.currentWrIndex;
    canAppState.canDescQueue.Can_Pdu[index].rxTS = TimerP_getTimeInUsecs();

    /*Copy the packet data*/
    memcpy(canAppState.canQueue.currentWrPtr, PduInfoPtr->SduDataPtr, PduInfoPtr->SduLength);

    /*Increment write pointer and number of packets enqueued*/
    canAppState.canQueue.currentWrPtr += PduInfoPtr->SduLength;

    /*Copy descriptor data*/
    canAppState.canDescQueue.Can_Pdu[index].id = CanIf_Mailbox->CanId;
    canAppState.canDescQueue.Can_Pdu[index].length = PduInfoPtr->SduLength;
    canAppState.canDescQueue.Can_Pdu[index].srcPort = CanIf_Mailbox->ControllerId;

    canAppState.canDescQueue.currentWrIndex++;
    /*Check wraparound and reset*/
    if(canAppState.canDescQueue.currentWrIndex == MCAN_NUM_PDU_IN_Q)
    {
        canAppState.canDescQueue.currentWrIndex = 0;
    }   

    /*Increment number of CAN frames received*/
    canAppState.canQueue.numPduEnqueued++;
#else
    uint8_t num_seconds = 0;
    uint32_t bridge_delay;

    /*Check if it's a special packet, then enable transmission*/
    if(PduInfoPtr->SduDataPtr[0] == GENERATOR_PKT_MARKER)
    {
        /*Find out how many bytes to send*/
        num_seconds = PduInfoPtr->SduDataPtr[1];
        CanApp_TestPrms.numTxPackets = num_seconds * NUM_CAN_FRAMES_IN_SEC;
        /*Enable sending of CAN frames*/
        CanApp_TestPrms.sendCANFrames = 1;        
    }
    /*Check if we need to send statistics back to host*/
    else if (PduInfoPtr->SduDataPtr[0] == GENERATOR_STAT_REQUEST_MARKER)
    {
        CanApp_TestPrms.sendCANStats = 1;
    }
    else
    {
        CanApp_TestPrms.numRxPackets++;
        /*Compute the delay*/
        //Get the min, max and average bridge delay from payload
        memcpy(&bridge_delay, &(PduInfoPtr->SduDataPtr[7]), 4);

        /*Calculate for CAN 2 CAN*/
        if(PduInfoPtr->SduDataPtr[6] == CAN_2_CAN_FRAME_ID)
        {
            if(bridge_delay < CanApp_TestPrms.avg_can_2_can_bridge_delay) {
                CanApp_TestPrms.avg_can_2_can_bridge_delay = bridge_delay;
            }

            if(bridge_delay > CanApp_TestPrms.max_can_2_can_bridge_delay) {
                CanApp_TestPrms.max_can_2_can_bridge_delay = bridge_delay;
            }

            CanApp_TestPrms.avg_can_2_can_bridge_delay = (8 * CanApp_TestPrms.avg_can_2_can_bridge_delay + 2 * bridge_delay) / 10;

        }

        /*Calculate for ETH 2 CAN*/
        if(PduInfoPtr->SduDataPtr[6] == ETH_2_CAN_FRAME_ID)
        {
            if(bridge_delay < CanApp_TestPrms.avg_eth_2_can_bridge_delay) {
                CanApp_TestPrms.avg_eth_2_can_bridge_delay = bridge_delay;
            }

            if(bridge_delay > CanApp_TestPrms.max_eth_2_can_bridge_delay) {
                CanApp_TestPrms.max_eth_2_can_bridge_delay = bridge_delay;
            }

            CanApp_TestPrms.avg_eth_2_can_bridge_delay = (8 * CanApp_TestPrms.avg_eth_2_can_bridge_delay + 2 * bridge_delay) / 10;
        }        
    }
#endif //LOW_LATENCY_DEMO

    return 0;
}

uint32 CanApp_SendStats(void)
{
    Std_ReturnType      writeStatus[BATCH_SIZE];

    const Can_PduType   *PduInfo;
    Can_PduType     CanApp_Pdu_2;
    uint8            hth = 0U;

    /*Send message to Generator app over MCAN8/9*/
    CanApp_Pdu_2.length  = 64U;
    CanApp_Pdu_2.id = CAN_ID_FOR_MCAN9 | 0xC0000000;

    /*Point to the packet data*/
    CanApp_Pdu_2.sdu = CanApp_Stats_Frame;

    /*Use special marker to distinguish from regular traffic*/
    CanApp_Stats_Frame[0] = SPL_BYTE_FOR_STAT_FRAME;

    /*Send on MCU MCAN1 which is connected to Main MCAN9*/
    CanApp_Pdu_2.swPduHandle = 2U;
    hth = CAN_HTRH_2;
    PduInfo = &CanApp_Pdu_2;

    CanApp_ProfileStatus = E_OK;

    /*Write the statistics here*/

    /*First copy the number of packets sent and received*/
    memcpy((CanApp_Stats_Frame + 1), (uint8_t*)&(CanApp_TestPrms.numRxPackets), 4);
    memcpy((CanApp_Stats_Frame + 5), (uint8_t*)&(CanApp_TestPrms.actualPacketsSent), 4);
    /*Copy avg and max delay for CAN 2 CAN*/
    memcpy((CanApp_Stats_Frame + 9), (uint8_t*)&(CanApp_TestPrms.avg_can_2_can_bridge_delay), 4);
    memcpy((CanApp_Stats_Frame + 13), (uint8_t*)&(CanApp_TestPrms.max_can_2_can_bridge_delay), 4);

    /*Copy avg and max delay for ETH 2 CAN*/
    memcpy((CanApp_Stats_Frame + 17), (uint8_t*)&(CanApp_TestPrms.avg_eth_2_can_bridge_delay), 4);
    memcpy((CanApp_Stats_Frame + 21), (uint8_t*)&(CanApp_TestPrms.max_eth_2_can_bridge_delay), 4);

    /*Reset stats*/
    CanApp_TestPrms.numRxPackets = 0;
    CanApp_TestPrms.numTxPackets = 0;
    CanApp_TestPrms.actualPacketsSent = 0;

    CanApp_TestPrms.avg_can_2_can_bridge_delay = 0;
    CanApp_TestPrms.max_can_2_can_bridge_delay = 0;

    CanApp_TestPrms.avg_eth_2_can_bridge_delay = 0;
    CanApp_TestPrms.max_eth_2_can_bridge_delay = 0;
    
    /* Do Can Write to send the CanApp_InputData on CAN bus */
    writeStatus[0] = Can_Write(hth, PduInfo);

    if (writeStatus[0] == E_OK)
    {
        Can_MainFunction_Write();
        AppUtils_Printf(MSG_NORMAL, MSG_APP_NAME
            " Sent statistics to Gateway device !!!\n");
    }
    else
    {
        CanApp_ProfileStatus = E_NOT_OK;
    }


    /*Set to 0, so we don't keep sending messages*/
    CanApp_TestPrms.sendCANStats = 0;

    return CanApp_ProfileStatus;

}

void CanApp_IPC_App_Init(void)
{
    /*Initialize IPC with MCU 2_1 and other initializations*/

    /*Initialize to 0, so we don't start sending frames*/
    CanApp_TestPrms.sendCANFrames = 0;
    /*Initialize to 0, so we don't start sending CAN statistics*/
    CanApp_TestPrms.sendCANStats = 0;

    /*Set all state machine variables to 0*/

    CanApp_TestPrms.numTxPackets = 0;
    CanApp_TestPrms.actualPacketsSent = 0;
    CanApp_TestPrms.numRxPackets = 0;

    /*Assign callback function to handle packets*/
    push_frame_to_app_queue = &CanApp_receiveFrames;

#ifdef LOW_LATENCY_DEMO    

    /*Allocate memory for the CAN queues*/
    canAppState.canQueue.topOfQueue = (uint8_t*)malloc(sizeof(uint8_t) * MCAN_MAX_Q_SIZE);
    if(canAppState.canQueue.topOfQueue == NULL)
    {
        CanApp_ProfileStatus = E_NOT_OK;
    } 
    else
    {
        canAppState.canQueue.bottomOfQueue = canAppState.canQueue.topOfQueue + MCAN_MAX_Q_SIZE;
        canAppState.canQueue.currentRdPtr = canAppState.canQueue.topOfQueue;
        canAppState.canQueue.currentWrPtr = canAppState.canQueue.topOfQueue;
    }
#endif //LOW_LATENCY_DEMO

}

#define CAN_STOP_SEC_ISR_CODE
#include "Can_MemMap.h"

