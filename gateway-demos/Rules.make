
################################################################################
# Build Environment (Edit Below)
# Default build environment, Supported options (Windows_NT / linux)
################################################################################
export OS ?= linux

################################################################################
# GATEWAY DEMO Configurations (Edit Below)
################################################################################
# Possible options (tirtos/baremetal)
export BUILD_OS_TYPE ?= tirtos

# Utilities directory. This is required only if the build machine is Windows.
#   - specify the installation directory of utility which supports POSIX commands
#     (eg: Cygwin installation or MSYS installation).
# This could be in CCS install directory as in c:/ti/ccsv7/utils/cygwin or
# the XDC install bin folder $(XDC_INSTALL_PATH)/bin
ifeq ($(OS),Windows_NT)
  export utils_PATH ?= $(XDC_INSTALL_PATH)/bin
endif

ifeq ($(OS),Windows_NT)
  export SDK_INSTALL_PATH  ?= C:/ti/PSDKRA
else
  export SDK_INSTALL_PATH  ?= $(abspath ../)
endif

export ETHFW_PATH ?= $(SDK_INSTALL_PATH)/ethfw
export REMOTE_DEVICE_PATH ?= $(SDK_INSTALL_PATH)/remote_device
#Default AUTOSAR Version
export AUTOSAR_VERSION ?= 431

################################################################################
# GATEWAY Configurations Ends
################################################################################

export TOOLS_INSTALL_PATH ?= $(SDK_INSTALL_PATH)

BOARD_LIST_ALL = j721e_evm j7200_evm
CORE_LIST_ALL = mcu2_1
BUILD_PROFILE_LIST_ALL = release debug

# Default board
# Supported values are printed in "make -s help" option.
#
# default board and soc
export BOARD ?= j721e_evm

ifneq ($(BOARD), $(filter $(BOARD), $(BOARD_LIST_ALL)))
$(error BOARD environment variable = "$(BOARD)" not supported.  Valid options are $(BOARD_LIST_ALL))
endif

# Derived SOC and SOC_UC values for dependent components so user doesn't have to set them
ifeq ($(BOARD),j721e_evm)
export SOC     = j721e
export SOC_UC  = J721E
endif

ifeq ($(BOARD),j7200_evm)
export SOC     = j7200
export SOC_UC  = J7200
endif

ifeq ($(BUILD_OS_TYPE), tirtos)
export TIRTOS_BUILD_BOOL = yes
export FREERTOS_BUILD_BOOL = no
endif

ifeq ($(BUILD_OS_TYPE), freertos)
export TIRTOS_BUILD_BOOL = no
export FREERTOS_BUILD_BOOL = yes
endif

################################################################################
# Other user configurable variables
################################################################################
export CORE ?= mcu2_1

# Default Build Profile
# Supported Values: debug | release
export BUILD_PROFILE ?= release

# Treat compiler warning as error
# Supported Values: yes | no
export TREAT_WARNINGS_AS_ERROR ?= yes

################################################################################
# Configure toolchain paths
################################################################################
CGT_ARM_VERSION=20.2.0.LTS
BIOS_VERSION=6_83_02_07
XDC_VERSION=3_61_04_40_core
GCC_ARCH64_VERSION=9.2-2019.12

################################################################################
# Dependent toolchain paths variables
################################################################################
export GATEWAY_INSTALL_PATH     ?= $(SDK_INSTALL_PATH)/gateway-demos
export MCUSW_INSTALL_PATH       ?= $(SDK_INSTALL_PATH)/mcusw
export TOOLCHAIN_PATH_R5        ?= $(TOOLS_INSTALL_PATH)/ti-cgt-arm_$(CGT_ARM_VERSION)
export PDK_INSTALL_PATH         ?= $(SDK_INSTALL_PATH)/pdk_jacinto_08_00_00_37/packages
export BIOS_INSTALL_PATH        ?= $(SDK_INSTALL_PATH)/bios_$(BIOS_VERSION)
export XDC_INSTALL_PATH         ?= $(SDK_INSTALL_PATH)/xdctools_$(XDC_VERSION)

ifeq ($(OS),Windows_NT)
  #Paths for windows machine
  export TOOLCHAIN_PATH_GCC_ARCH64 ?= $(TOOLS_INSTALL_PATH)/gcc-linaro-$(GCC_ARCH64_VERSION)-i686-mingw32_aarch64-elf
else
  #Paths for linux machine
  export TOOLCHAIN_PATH_GCC_ARCH64 ?= $(TOOLS_INSTALL_PATH)/gcc-linaro-$(GCC_ARCH64_VERSION)-x86_64_aarch64-elf
endif
export TOOLCHAIN_PATH_A53        ?= $(TOOLCHAIN_PATH_GCC_ARCH64)
export TOOLCHAIN_PATH_A72        ?= $(TOOLCHAIN_PATH_GCC_ARCH64)

################################################################################
# GATEWAY Configurations (Edit Below)
################################################################################
# Enable GATEWAY to log messages in console (UART) or CCS console
export GATEWAY_UART_ENABLE ?= FALSE

################################################################################
# Other advanced configurable variables
################################################################################

#use <module>_PATH variable as makefile internally expects PATH variable this way for external component path
export pdk_PATH  := $(PDK_INSTALL_PATH)
export mcal_PATH := $(MCUSW_INSTALL_PATH)/mcal_drv/mcal
export bios_PATH := $(BIOS_INSTALL_PATH)
export xdc_PATH := $(XDC_INSTALL_PATH)
XDCPATH = $(bios_PATH)/packages;$(xdc_PATH)/packages;$(pdk_PATH);
export XDCPATH

export mcuss_demo_PATH := $(MCUSW_INSTALL_PATH)/mcuss_demos
export ROOTDIR := $(PDK_INSTALL_PATH)

#Autosar basic software include path provided by AUTOSAR stack
#TODO - should this be local?
export autosarConfigSrc_PATH     ?= $(GATEWAY_INSTALL_PATH)/mcuss_demos/mcal_config
export autosarBSWInc_PATH        ?= $(GATEWAY_INSTALL_PATH)/mcuss_demos/Bsw_Stubs
export autosarCompilerTypes_PATH ?= $(GATEWAY_INSTALL_PATH)/mcuss_demos/Bsw_Stubs/AsrGnrl/inc

# Set Core Build Profile depending on BUILD_PROFILE value
export BUILD_PROFILE_$(CORE) ?= $(BUILD_PROFILE)

# Disable recursive building of example dependencies
export DISABLE_RECURSE_DEPS ?= no

# Default C++ build flag, yes or no
export CPLUSPLUS_BUILD ?= no

#Default MCAL config - use base directory
MCAL_CONFIG ?= 0

# Get config path for each component
MCAL_COMP_DIR_LIST = Adc Can Dio Eth EthTrcv Gpt Spi Wdg Pwm CddIpc
define GET_MCAL_COMP_CONFIG_PATH
  $(1)_CONFIG_PATH += $(autosarConfigSrc_PATH)/$(1)_Demo_Cfg/output/generated/soc/$(SOC)/$(CORE)/include
endef
$(foreach MCAL_COMP,$(MCAL_COMP_DIR_LIST),$(eval $(call GET_MCAL_COMP_CONFIG_PATH,$(MCAL_COMP))))

#Autosar generated config include files
#Can be list of directories separated by space.
#Should include path to each modules pre compile header path
#Should include path to each modules <Mod>_Memmap.h
export autosarConfig_PATH = $(foreach MCAL_COMP,$(MCAL_COMP_DIR_LIST),$($(MCAL_COMP)_CONFIG_PATH))

# include other dependent files
ifeq ($(MAKERULEDIR), )
  #Makerule path not defined, define this and assume relative path from ROOTDIR
  export MAKERULEDIR := $(PDK_INSTALL_PATH)/ti/build/makerules
endif
include $(MAKERULEDIR)/build_config.mk
include $(MAKERULEDIR)/platform.mk
include $(GATEWAY_INSTALL_PATH)/build/env.mk
