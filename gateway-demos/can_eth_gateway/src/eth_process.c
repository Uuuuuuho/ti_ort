/*
*
* Copyright (c) 2019 Texas Instruments Incorporated
*
* All rights reserved not granted herein.
*
* Limited License.
*
* Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
* license under copyrights and patents it now or hereafter owns or controls to make,
* have made, use, import, offer to sell and sell ("Utilize") this software subject to the
* terms herein.  With respect to the foregoing patent license, such license is granted
* solely to the extent that any such patent is necessary to Utilize the software alone.
* The patent license shall not apply to any combinations which include this software,
* other than combinations with devices manufactured by or for TI ("TI Devices").
* No hardware patent is licensed hereunder.
*
* Redistributions must preserve existing copyright notices and reproduce this license
* (including the above copyright notice and the disclaimer and (if applicable) source
* code license limitations below) in the documentation and/or other materials provided
* with the distribution
*
* Redistribution and use in binary form, without modification, are permitted provided
* that the following conditions are met:
*
* *       No reverse engineering, decompilation, or disassembly of this software is
* permitted with respect to any software provided in binary form.
*
* *       any redistribution and use are licensed by TI for use only with TI Devices.
*
* *       Nothing shall obligate TI to provide you with source code for the software
* licensed and provided to you in object code.
*
* If software source code is provided to you, modification and redistribution of the
* source code are permitted provided that the following conditions are met:
*
* *       any redistribution and use of the source code, including any resulting derivative
* works, are licensed by TI for use only with TI Devices.
*
* *       any redistribution and use of any object code compiled from the source code
* and any resulting derivative works, are licensed by TI for use only with TI Devices.
*
* Neither the name of Texas Instruments Incorporated nor the names of its suppliers
*
* may be used to endorse or promote products derived from this software without
* specific prior written permission.
*
* DISCLAIMER.
*
* THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
* OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
* OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
* IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
* BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
* OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
* OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
* OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/**
 *  \file     eth_process.c
 *
 *  \brief    This file implements Eth-CAN gateway demo application
 */

/* ========================================================================== */
/*                             Include Files                                  */
/* ========================================================================== */
#include <stdint.h>
#include <assert.h>
#if defined(SYSBIOS)
#include <xdc/std.h>
#include <xdc/runtime/Error.h>
#include <xdc/runtime/System.h>
#include <xdc/runtime/Log.h>

#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/knl/Clock.h>
#include <ti/sysbios/knl/Task.h>
#include <ti/sysbios/knl/Semaphore.h>
#endif //SYSBIOS

#include <ti/drv/enet/enet.h>
#include <ti/drv/enet/include/core/enet_soc.h>
#include <ti/drv/enet/examples/utils/include/enet_appsoc.h>
#include "gateway_demo_cfg.h"
#include "eth_process.h"
#include "gateway_demo.h"
#include "Cdd_Ipc.h"
#include "Cdd_IpcIrq.h"

/* ========================================================================== */
/*                           Macros & Typedefs                                */
/* ========================================================================== */
#define REMOTE_DEVICE_ENDPT     (26)

#define ETHAPP_ETHMAIN_SCHEDULE_PERIOD_MS                                 (100U)

#define ETHAPP_PKTRX_TSK_PRI                                              (13U)
#define ETHAPP_PKTTX_TSK_PRI                                              (12U)
#define ETHAPP_RECLAIMTX_TSK_PRI                                          (11U)

#define ETHAPP_PKTRX_TSK_STACK_SIZE                                     (8 * 1024U)
#define ETHAPP_PKTTX_TSK_STACK_SIZE                                     (8 * 1024U)
#define ETHAPP_RECLAIMTX_TSK_STACK_SIZE                                 (8 * 1024U)


#define ETHAPP_PKTRX_TASK_NAME                                          ("EthAppPktRxTsk")
#define ETHAPP_PKTTX_TASK_NAME                                          ("EthAppPktTxTsk")
#define ETHAPP_RECLAIMTX_TASK_NAME                                      ("EthAppPktReclaimTxTsk")

#define CPSW_INTR_STATS_PEND0                                           (1U)
#define CPSW_INTR_MDIO_PEND                                             (2U)
#define CPSW_INTR_EVNT_PEND                                             (3U)

#define PIN_MODE(mode)                                                  ((mode) & 0xFU)

/* ========================================================================== */
/*              Internal Function Declarations                                */
/* ========================================================================== */

static void EthApp_interruptConfig(Enet_Type enetType);

void EthApp_setCpsw0Pinmux(void);

void EthApp_setConnectionType(Eth_MacConnectionType type);

static void EthApp_registerRxFlow(uint8_t *macAddress);

/* Main Eth / EthTrcv initialization/deinitialization functions */
static Std_ReturnType EthApp_trcvInit(EthTrcv_ConfigType *cfg);


/* Callbacks */

void EthIf_TxConfirmation(uint8 ctrlIdx,
                          Eth_BufIdxType bufIdx,
                          Std_ReturnType Result);

void EthIf_RxIndication(uint8 ctrlIdx,
                        Eth_FrameType FrameType,
                        boolean IsBroadcast,
                        uint8 *PhysAddrPtr,
                        Eth_DataType *DataPtr,
                        uint16 lenByte);

void EthIf_TrcvModeIndication(uint8 CtrlIdx,
                              EthTrcv_ModeType TrcvMode);

void Dem_ReportErrorStatus(Dem_EventIdType eventId,
                           Dem_EventStatusType eventStatus);

Std_ReturnType Det_ReportError(uint16 moduleId,
                               uint8 instanceId,
                               uint8 apiId,
                               uint8 errorId);

/* Test helper functions */
static int32_t EthApp_enqueueFrame(uint16_t pktLen,
                                     uint8_t *PhysAddrPtr);

int32_t EthApp_dequeueAndTx(uint8_t *payload, Eth_FrameHeader *ethHeader);

void EthApp_wbCache(uint8 *buf, uint16 len);
void EthApp_wbInvCache(uint8 *buf, uint16 len);

void EthApp_invCache(uint8 *buf, uint16 len);

static int32_t EthApp_registerIntrs(Enet_Type enetType);

static void EthApp_validateEthConfig(Eth_ConfigType *ethConfig);
static void EthApp_initPktProcessingTask(void);
static void EthApp_deInitPktProcessingTask(void);
static void EthApp_pktTxTask(void* arg0,void* arg1);
static void EthApp_queueAllFreeTxBuffer(EthApp_State *ethAppState);
static void EthAppMain_clockFxn (void* arg);
static void EthApp_createIpcSem(void);
static void EthApp_deleteIpcSem(void);

/* ========================================================================== */
/*                            Global Variables                                */
/* ========================================================================== */
uint8 BcastAddr[ETH_MAC_ADDR_LEN] =
{
    0xffU, 0xffU, 0xffU, 0xffU, 0xffU, 0xffU
};
/**< Broadcast MAC address */

EthApp_State gEthApp;
/**< ethernet control structure */

#if (STD_ON == GATEWAY_PROFILE_MEMCPY)
volatile uint64_t startTime = 0;
volatile uint64_t stopTime = 0;
volatile uint64_t avgTime = 0;
#endif //GATEWAY_PROFILE_MEMCPY

/* application stack */
static uint8_t EthApp_RxStack[ETHAPP_PKTRX_TSK_STACK_SIZE] __attribute__((aligned(32)));
static uint8_t EthApp_TxStack[ETHAPP_PKTTX_TSK_STACK_SIZE] __attribute__((aligned(32)));
static uint8_t EthApp_ReclaimStack[ETHAPP_RECLAIMTX_TSK_STACK_SIZE] __attribute__((aligned(32)));

static uint8_t EthApp_TxSendMbxBuf[sizeof(EthApp_TxSendPktInfo) * ETH_NUM_TX_BUFFERS] __attribute__ ((aligned(32)));
static uint8_t EthApp_TxFreeMbxBuf[sizeof(EthApp_TxFreePktInfo) * ETH_NUM_TX_BUFFERS] __attribute__ ((aligned(32)));
/* ========================================================================== */
/*                            External Variables                              */
/* ========================================================================== */
extern SemaphoreP_Handle gateway_semHandle;
/**< Semaphore handle used to trigger routing task */

SemaphoreP_Handle ipcNotify_semHandle;
/**< Semaphore handle used to indicate new msg received */

Bool ethAppProcessRx = FALSE;

uint8 gExpectedSid = 0;

/* ========================================================================== */
/*                          Function Definitions                              */
/* ========================================================================== */


void EthApp_Startup(void)
{
    Enet_Type enetType;

    /* Initialize the global app structure for Ethernet*/
    gEthApp.ctrlIdx = ETH_DRV_CONFIG_0->ctrlIdx;
    gEthApp.verbose = FALSE;
    gEthApp.veryVerbose = FALSE;
    memset(&gEthApp.stats, 0U, sizeof(EthApp_Stats));
    if (ETH_DRV_CONFIG_0->enetType == ETH_ENETTYPE_CPSW2G)
    {
        enetType = ENET_CPSW_2G;
    }
    else
    {
        enetType = ENET_CPSW_9G;
    }
    //CpswAppUtils_enableClocks(enetType,
    //                          MAC_CONN_TYPE_RGMII_FORCE_1000_FULL);

    /* Equivalent to EcuM_AL_SetProgrammableInterrupts */
    EthApp_interruptConfig(enetType);

    /* Initialize counters, that would be required for timed operations */
    AppUtils_ProfileInit(0);

    /* Initialize memory sections  */
    AppUtils_EthSectionInit();
}

Std_ReturnType EthApp_deinit(uint8 ctrlIdx)
{
    Std_ReturnType retVal;

    EthApp_deInitPktProcessingTask();
    /* Set controller to down mode */
    retVal = Eth_SetControllerMode(ctrlIdx, ETH_MODE_DOWN);

    if (E_OK != retVal)
    {
        AppUtils_Printf(ETHAPP_MSG_NORMAL, ETHAPP_MSG_APP_NAME"deinit: failed to set the controller to down mode\n");
    }

    EthApp_deleteIpcSem();
    /*Free memory*/
    free(gEthApp.ethQueue.topOfQueue);

    return retVal;
}

#define ETH_START_SEC_ISR_CODE
#include "Eth_MemMap.h"

ETH_ISR_TEXT_SECTION void Eth_MdioIrqHdlr_0_wrapper(uintptr_t arg)
{
#if (STD_ON == ETH_ENABLE_MII_API)
    Eth_MdioIrqHdlr_0();
#endif
}

ETH_ISR_TEXT_SECTION void Eth_TxIrqHdlr_0_wrapper(uintptr_t arg)
{
    Eth_TxIrqHdlr_0();
}

ETH_ISR_TEXT_SECTION void Eth_RxIrqHdlr_0_wrapper(uintptr_t arg)
{
    Eth_RxIrqHdlr_0();
}

#define ETH_STOP_SEC_ISR_CODE
#include "Eth_MemMap.h"

static void EthApp_interruptConfig(Enet_Type enetType)
{
#if ((STD_ON == ETH_ENABLE_TX_INTERRUPT) || (STD_ON == ETH_ENABLE_RX_INTERRUPT))
    HwiP_Params hwiParams;
#endif

    /* Enable MDIO Interrupt */
    EthApp_registerIntrs(enetType);

#if (STD_ON == ETH_ENABLE_TX_INTERRUPT)
    /* TX DMA completion interrupt */
    HwiP_Params_init(&hwiParams);
    hwiParams.arg = (uintptr_t)NULL;

    if(!HwiP_create(ETH_DRV_CONFIG_0->dmaTxChIntrNum,
                    Eth_TxIrqHdlr_0_wrapper, &hwiParams))
    {
        AppUtils_Printf(ETHAPP_MSG_NORMAL, ETHAPP_MSG_APP_NAME "Failed to create Interrupt Eth  Tx Interrupt\n");
    }

#endif /* (STD_ON == ETH_ENABLE_TX_INTERRUPT) */

#if (STD_ON == ETH_ENABLE_RX_INTERRUPT)
    /* RX DMA completion interrupt */
    HwiP_Params_init(&hwiParams);
    hwiParams.arg = (uintptr_t)NULL;

    if(!HwiP_create(ETH_DRV_CONFIG_0->dmaRxChIntrNum,
                    Eth_RxIrqHdlr_0_wrapper, &hwiParams))
    {
        AppUtils_Printf(ETHAPP_MSG_NORMAL, ETHAPP_MSG_APP_NAME "Failed to create Interrupt Eth Rx Interrupt\n");
    }

#endif /* (STD_ON == ETH_ENABLE_RX_INTERRUPT) */
}

void EthApp_setCpsw0Pinmux(void)
{
    CSL_wkup_ctrl_mmr_cfg0Regs *regs =
        (CSL_wkup_ctrl_mmr_cfg0Regs *)(uintptr_t)CSL_WKUP_CTRL_MMR0_CFG0_BASE;

    CSL_REG32_WR(&regs->LOCK2_KICK0, MMR_KICK0_UNLOCK_VAL);
    CSL_REG32_WR(&regs->LOCK2_KICK1, MMR_KICK1_UNLOCK_VAL);
    CSL_REG32_WR(&regs->LOCK7_KICK0, MMR_KICK0_UNLOCK_VAL);
    CSL_REG32_WR(&regs->LOCK7_KICK1, MMR_KICK1_UNLOCK_VAL);

 #if defined (SOC_J721E)
    /* MCU_CPSW -> MCU_RGMII1_TX_CTL -> N4 (DRA80X), B27 (DRA721E) */
    CSL_REG32_WR(&regs->PADCONFIG22, PIN_MODE(0U) | PIN_OUTPUT);

    /* MCU_CPSW -> MCU_RGMII1_RX_CTL -> N5 (DRA80X), C25 (DRA721E) */
    CSL_REG32_WR(&regs->PADCONFIG23, PIN_MODE(0U) | PIN_INPUT);

    /* MCU_CPSW -> MCU_RGMII1_TD3 -> M2 (DRA80X), A28 (DRA721E) */
    CSL_REG32_WR(&regs->PADCONFIG24, PIN_MODE(0U) | PIN_OUTPUT);

    /* MCU_CPSW -> MCU_RGMII1_TD2 -> M3 (DRA80X), A27 (DRA721E) */
    CSL_REG32_WR(&regs->PADCONFIG25, PIN_MODE(0U) | PIN_OUTPUT);

    /* MCU_CPSW -> MCU_RGMII1_TD1 -> M4 (DRA80X), A26 (DRA721E) */
    CSL_REG32_WR(&regs->PADCONFIG26, PIN_MODE(0U) | PIN_OUTPUT);

    /* MCU_CPSW -> MCU_RGMII1_TD0 -> M5 (DRA80X), B25 (DRA721E) */
    CSL_REG32_WR(&regs->PADCONFIG27, PIN_MODE(0U) | PIN_OUTPUT);

    /* MCU_CPSW -> MCU_RGMII1_TXC -> N1 (DRA80X), B26 (DRA721E) */
    CSL_REG32_WR(&regs->PADCONFIG28, PIN_MODE(0U) | PIN_OUTPUT);

    /* MCU_CPSW -> MCU_RGMII1_RXC -> M1 (DRA80X), C24 (DRA721E) */
    CSL_REG32_WR(&regs->PADCONFIG29, PIN_MODE(0U) | PIN_INPUT);

    /* MCU_CPSW -> MCU_RGMII1_RD3 -> L2 (DRA80X), A25 (DRA721E) */
    CSL_REG32_WR(&regs->PADCONFIG30, PIN_MODE(0U) | PIN_INPUT);

    /* MCU_CPSW -> MCU_RGMII1_RD2 -> L5 (DRA80X), D24 (DRA721E) */
    CSL_REG32_WR(&regs->PADCONFIG31, PIN_MODE(0U) | PIN_INPUT);

    /* MCU_CPSW -> MCU_RGMII1_RD1 -> M6 (DRA80X), A24 (DRA721E) */
    CSL_REG32_WR(&regs->PADCONFIG32, PIN_MODE(0U) | PIN_INPUT);

    /* MCU_CPSW -> MCU_RGMII1_RD0 -> L6 (DRA80X), B24 (DRA721E) */
    CSL_REG32_WR(&regs->PADCONFIG33, PIN_MODE(0U) | PIN_INPUT);

    /* MCU_MDIO0 -> MCU_MDIO0_MDIO -> L4 (DRA80X), E23 (DRA721E) */
    CSL_REG32_WR(&regs->PADCONFIG34, PIN_MODE(0U) | PIN_INPUT);

    /* MCU_MDIO0 -> MCU_MDIO0_MDC -> L1 (DRA80X), F23 (DRA721E) */
    CSL_REG32_WR(&regs->PADCONFIG35, PIN_MODE(0U) | PIN_OUTPUT);

    /* WKUP_GPIO0 -> WKUP_GPIO0_3 -> L1 (DRA80X), F27 (DRA721E) */
    CSL_REG32_WR(&regs->PADCONFIG47, PIN_MODE(7U) | PIN_INPUT);
#else /* J7200 */
    /* MCU_CPSW -> MCU_RGMII1_TX_CTL -> X (J7200) */
    CSL_REG32_WR(&regs->PADCONFIG26, PIN_MODE(0U) | PIN_OUTPUT);

    /* MCU_CPSW -> MCU_RGMII1_RX_CTL -> X (J7200) */
    CSL_REG32_WR(&regs->PADCONFIG27, PIN_MODE(0U) | PIN_INPUT);

    /* MCU_CPSW -> MCU_RGMII1_TD3 -> X (J7200) */
    CSL_REG32_WR(&regs->PADCONFIG28, PIN_MODE(0U) | PIN_OUTPUT);

    /* MCU_CPSW -> MCU_RGMII1_TD2 -> X (J7200) */
    CSL_REG32_WR(&regs->PADCONFIG29, PIN_MODE(0U) | PIN_OUTPUT);

    /* MCU_CPSW -> MCU_RGMII1_TD1 -> X (J7200) */
    CSL_REG32_WR(&regs->PADCONFIG30, PIN_MODE(0U) | PIN_OUTPUT);

    /* MCU_CPSW -> MCU_RGMII1_TD0 -> X (J7200) */
    CSL_REG32_WR(&regs->PADCONFIG31, PIN_MODE(0U) | PIN_OUTPUT);

    /* MCU_CPSW -> MCU_RGMII1_TXC -> X (J7200) */
    CSL_REG32_WR(&regs->PADCONFIG32, PIN_MODE(0U) | PIN_OUTPUT);

    /* MCU_CPSW -> MCU_RGMII1_RXC -> X (J7200) */
    CSL_REG32_WR(&regs->PADCONFIG33, PIN_MODE(0U) | PIN_INPUT);

    /* MCU_CPSW -> MCU_RGMII1_RD3 -> X (J7200) */
    CSL_REG32_WR(&regs->PADCONFIG34, PIN_MODE(0U) | PIN_INPUT);

    /* MCU_CPSW -> MCU_RGMII1_RD2 -> X (J7200) */
    CSL_REG32_WR(&regs->PADCONFIG35, PIN_MODE(0U) | PIN_INPUT);

    /* MCU_CPSW -> MCU_RGMII1_RD1 -> X (J7200) */
    CSL_REG32_WR(&regs->PADCONFIG36, PIN_MODE(0U) | PIN_INPUT);

    /* MCU_CPSW -> MCU_RGMII1_RD0 -> X (J7200) */
    CSL_REG32_WR(&regs->PADCONFIG37, PIN_MODE(0U) | PIN_INPUT);

    /* MCU_MDIO0 -> MCU_MDIO0_MDIO -> X (J7200) */
    CSL_REG32_WR(&regs->PADCONFIG38, PIN_MODE(0U) | PIN_INPUT);

    /* MCU_MDIO0 -> MCU_MDIO0_MDC -> X (J7200) */
    CSL_REG32_WR(&regs->PADCONFIG39, PIN_MODE(0U) | PIN_OUTPUT);

    /* WKUP_GPIO0 -> WKUP_GPIO0_3 -> X (J7200) */
    CSL_REG32_WR(&regs->PADCONFIG51, PIN_MODE(7U) | PIN_INPUT);
#endif

}

void EthApp_setConnectionType(Eth_MacConnectionType type)
{
    CSL_mcu_ctrl_mmr_cfg0Regs *regs =
        (CSL_mcu_ctrl_mmr_cfg0Regs *)(uintptr_t)CSL_MCU_CTRL_MMR0_CFG0_BASE;
    uint32 val;

    val = ENET_CTRL_RGMII_ID_NODELAY << ENET_CTRL_RGMII_ID_SHIFT;

    switch (type)
    {
    case ETH_MAC_CONN_TYPE_RMII_10:
    case ETH_MAC_CONN_TYPE_RMII_100:
        val |= ENET_CTRL_MODE_RMII;
        break;
    case ETH_MAC_CONN_TYPE_RGMII_FORCE_100_HALF:
    case ETH_MAC_CONN_TYPE_RGMII_FORCE_100_FULL:
    case ETH_MAC_CONN_TYPE_RGMII_FORCE_1000_FULL:
    case ETH_MAC_CONN_TYPE_RGMII_DETECT_INBAND:
        val |= ENET_CTRL_MODE_RGMII;
        break;
    default:
        break;
    }

    /* Set MAC port interface in MMR */
    CSL_REG32_WR(&regs->LOCK1_KICK0, MMR_KICK0_UNLOCK_VAL);
    CSL_REG32_WR(&regs->LOCK1_KICK1, MMR_KICK1_UNLOCK_VAL);
    CSL_REG32_WR(&regs->MCU_ENET_CTRL, val);
}

Std_ReturnType EthApp_initMainFunctionPoll(void)
{
    ClockP_Params clkParams;
    Std_ReturnType status = E_OK;

    ClockP_Params_init(&clkParams);

    clkParams.startMode = ClockP_StartMode_AUTO;
    clkParams.period    = ETHAPP_ETHMAIN_SCHEDULE_PERIOD_MS;
    clkParams.runMode   = ClockP_RunMode_CONTINUOUS;
    clkParams.arg       = (void *)&gEthApp;

    gEthApp.ethMainClock = ClockP_create((void*) &EthAppMain_clockFxn,
                                      &clkParams);

    if (gEthApp.ethMainClock == NULL)
    {
        status = E_NOT_OK;
    }

    return status;

}

Std_ReturnType EthApp_init(uint8 ctrlIdx)
{
    const Eth_ConfigType *ethCfg = NULL_PTR;
    EthTrcv_ConfigType *ethTrcvCfg = NULL_PTR;
    uint8 addr[ETH_MAC_ADDR_LEN];
    Std_ReturnType retVal;

    EthApp_createIpcSem();
    EthApp_validateEthConfig((Eth_ConfigType *) ETH_DRV_CONFIG_0);

    /* Eth driver initialization */
#if (STD_OFF == ETH_PRE_COMPILE_VARIANT)
    ethCfg = ETH_DRV_CONFIG_0;
#endif
    retVal = Eth_VirtMacRpcInit((Eth_ConfigType *) ETH_DRV_CONFIG_0);
    if (E_OK != retVal)
    {
        AppUtils_Printf(GATEWAYAPP_MSG_NORMAL, "init: failed to initialize Eth RPC \n");
    }

    /* Wait for ethFw registration */
    SemaphoreP_pend(ipcNotify_semHandle, SemaphoreP_WAIT_FOREVER);

    gExpectedSid = ETH_SID_DISPATCH_VIRTMAC_INIT;
    retVal = Eth_DispatchVirtmacInit(ctrlIdx);
    assert(retVal == E_OK);
    SemaphoreP_pend(ipcNotify_semHandle, SemaphoreP_WAIT_FOREVER);

    /* Eth controller initialization */
    Eth_Init(ethCfg);

    retVal = EthApp_initMainFunctionPoll();

    /* Set controller to active mode */
    if (E_OK == retVal)
    {
        retVal = Eth_SetControllerMode(ctrlIdx, ETH_MODE_ACTIVE);
        if (E_OK != retVal)
        {
            AppUtils_Printf(ETHAPP_MSG_NORMAL, ETHAPP_MSG_APP_NAME "init: failed to set the controller to active mode\n");
        }
    }

    if (E_OK == retVal)
    {
        Eth_ConfigType *ethConfig = (Eth_ConfigType *) ETH_DRV_CONFIG_0;
        uint8 macAddress[ETH_MAC_ADDR_LEN];

        assert(ethConfig->enableVirtualMac == true);

        Eth_GetPhysAddr(ctrlIdx, macAddress);
        EthApp_registerRxFlow(macAddress);

    }
    /* Print initial physical address */
    if (E_OK == retVal)
    {
        Eth_GetPhysAddr(ctrlIdx, addr);
        AppUtils_Printf(ETHAPP_MSG_NORMAL, ETHAPP_MSG_APP_NAME "MAC Port %d Address: %02x:%02x:%02x:%02x:%02x:%02x\n",
                        ctrlIdx + 1,
                        addr[0U], addr[1U], addr[2U],
                        addr[3U], addr[4U], addr[5U]);
    }

    /* Initialize the transceiver */
    if ((FALSE == ETH_DRV_CONFIG_0->loopback) && (E_OK == retVal) && (FALSE == ETH_DRV_CONFIG_0->enableVirtualMac))
    {
#if (STD_OFF == ETHTRCV_PRE_COMPILE_VARIANT)
        ethTrcvCfg = ETHTRCV_DRV_CONFIG_0;
#endif
        retVal = EthApp_trcvInit(ethTrcvCfg);
        if (E_OK != retVal)
        {
            AppUtils_Printf(ETHAPP_MSG_NORMAL, ETHAPP_MSG_APP_NAME "init: failed to initialize transceiver\n");
        }
    }

    /*Initialize structures*/
    memset(&gEthApp.ethQueue, 0x0, sizeof(EthApp_Queue));
    memset(&gEthApp.ethDescQueue, 0x0, sizeof(EthApp_DescQ));

    /*Do memory alloc for Eth queues*/
    gEthApp.ethQueue.topOfQueue = (uint8_t*)malloc(sizeof(uint8_t) * ETH_Q_SIZE);
    if(gEthApp.ethQueue.topOfQueue == NULL)
    {
        retVal = E_NOT_OK;
        AppUtils_Printf(ETHAPP_MSG_NORMAL, ETHAPP_MSG_APP_NAME "init: failed to allocate memory for Ethernet queues\n");
    }
    else
    {
        gEthApp.ethQueue.bottomOfQueue = gEthApp.ethQueue.topOfQueue + ETH_Q_SIZE;
        gEthApp.ethQueue.currentRdPtr = gEthApp.ethQueue.topOfQueue;
        gEthApp.ethQueue.currentWrPtr = gEthApp.ethQueue.topOfQueue;
    }

    EthApp_initPktProcessingTask();
    if (ethAppProcessRx)
    {
        EthApp_queueAllFreeTxBuffer(&gEthApp);
    }
    if(retVal == E_OK)
    {
        /*Set interface up*/
        gEthApp.linkUp[0] = TRUE;
    }

    return retVal;
}

static Std_ReturnType EthApp_trcvInit(EthTrcv_ConfigType *cfg)
{
    EthTrcv_ModeType trcvMode;
    EthTrcv_LinkStateType linkState;
    EthTrcv_BaudRateType baudRate;
    EthTrcv_DuplexModeType duplexMode;
    uint8 trcvIdx = ETHTRCV_DRV_CONFIG_0->trcvIdx;
    Std_ReturnType retVal = E_OK;

    /* Initialize the EthTrcv driver */
    EthTrcv_Init(cfg);

    /* Set the transceiver mode to ACITVE */
    if (E_OK == retVal)
    {
        retVal = EthTrcv_SetTransceiverMode(trcvIdx, ETHTRCV_MODE_ACTIVE);
        if (E_OK != retVal)
        {
            AppUtils_Printf(ETHAPP_MSG_NORMAL, ETHAPP_MSG_APP_NAME "Failed to set trcv mode: %d\n", retVal);
        }
    }

    /* Read the mode to make sure the change took effect */
    if (E_OK == retVal)
    {
        retVal = EthTrcv_GetTransceiverMode(trcvIdx, &trcvMode);
        if (E_OK == retVal)
        {
            AppUtils_Printf(ETHAPP_MSG_NORMAL, ETHAPP_MSG_APP_NAME "EthTrcv mode: %s\n",
                            (trcvMode == ETHTRCV_MODE_ACTIVE) ? "ACTIVE" : "DOWN");
        }
        else
        {
            AppUtils_Printf(ETHAPP_MSG_NORMAL, ETHAPP_MSG_APP_NAME "Failed to get trcv mode: %d\n", retVal);
        }
    }

    /* Get the link state */
    if (E_OK == retVal)
    {
        retVal = EthTrcv_GetLinkState(trcvIdx, &linkState);
        if (E_OK == retVal)
        {
            AppUtils_Printf(ETHAPP_MSG_NORMAL, ETHAPP_MSG_APP_NAME "EthTrcv link state: %s\n",
                            (linkState == ETHTRCV_LINK_STATE_ACTIVE) ? "Up" : "Down");
        }
        else
        {
            AppUtils_Printf(ETHAPP_MSG_NORMAL, ETHAPP_MSG_APP_NAME "Failed to get link state: %d\n", retVal);
        }
    }

    /* Get the baud rate */
    if (E_OK == retVal)
    {
        retVal = EthTrcv_GetBaudRate(trcvIdx, &baudRate);
        if (E_OK == retVal)
        {
            if (ETHTRCV_BAUD_RATE_10MBIT == baudRate)
            {
                AppUtils_Printf(ETHAPP_MSG_NORMAL, ETHAPP_MSG_APP_NAME "EthTrcv baud rate: 10Mbps\n");
            }
            else if (ETHTRCV_BAUD_RATE_100MBIT == baudRate)
            {
                AppUtils_Printf(ETHAPP_MSG_NORMAL, ETHAPP_MSG_APP_NAME "EthTrcv baud rate: 100Mbps\n");
            }
            else if (ETHTRCV_BAUD_RATE_1000MBIT == baudRate)
            {
                AppUtils_Printf(ETHAPP_MSG_NORMAL, ETHAPP_MSG_APP_NAME "EthTrcv baud rate: 1000Mbps\n");
            }
        }
        else
        {
            AppUtils_Printf(ETHAPP_MSG_NORMAL, ETHAPP_MSG_APP_NAME "Failed to get baud rate: %d\n", retVal);
        }
    }

    /* Get duplexity */
    if (E_OK == retVal)
    {
        retVal = EthTrcv_GetDuplexMode(trcvIdx, &duplexMode);
        if (E_OK == retVal)
        {
            AppUtils_Printf(ETHAPP_MSG_NORMAL, ETHAPP_MSG_APP_NAME "EthTrcv duplexity: %s\n",
                            (duplexMode == ETHTRCV_DUPLEX_MODE_FULL) ? "Full" : "Half");
        }
        else
        {
            AppUtils_Printf(ETHAPP_MSG_NORMAL, ETHAPP_MSG_APP_NAME "Failed to get duplexity: %d\n", retVal);
        }
    }

    EthTrcv_MainFunction();

    return retVal;
}

void EthIf_TxConfirmation(uint8 ctrlIdx,
                          Eth_BufIdxType bufIdx,
                          Std_ReturnType Result)
{
    gEthApp.stats.txConfPktCnt++;
}
#if ((STD_ON == ETH_ZERO_COPY_TX) && (STD_OFF == ETH_USE_Q_APIS))
void EthIf_TxConfirmationNoCpy(uint8 CtrlIdx,
                               uint8 *DataPtr,
                               Std_ReturnType Result)
{
    gEthApp.stats.txConfPktCnt++;
}
#endif

void EthIf_RxIndication(uint8 ctrlIdx,
                        Eth_FrameType FrameType,
                        boolean IsBroadcast,
                        uint8 *PhysAddrPtr,
                        Eth_DataType *DataPtr,
                        uint16 lenByte)
{
    uint8_t subType = 0;
    Bool postGatewayTask = FALSE;

    /*Variables for updating Hash table*/
    uint8_t canID = 0;
    uint16_t tempHalfWord = 0;
    uint16_t canMask = 0;
    uint16_t ethMask = 0;
    uint8_t count = 0;

    /*Only service 1722 frames*/
    if(FrameType != AVTP_ETH_TYPE)
    {
        return;
    }

    if (TRUE == gEthApp.veryVerbose)
    {
        AppUtils_Printf(ETHAPP_MSG_NORMAL, ETHAPP_MSG_APP_NAME "EthIf_RxIndication: len: %d, bcast: %s, EtherType: 0x%04x\n",
                        lenByte,
                        IsBroadcast ? "yes" : "no",
                        FrameType & 0xFFFFU);
    }

    /* Check that the correct controller index is being reported */
    if (ctrlIdx != gEthApp.ctrlIdx)
    {
        AppUtils_Printf(ETHAPP_MSG_NORMAL, ETHAPP_MSG_APP_NAME "EthIf_RxIndication: incorrect controller index "
                        "(got %d, exp %d)\n",
                        ctrlIdx, gEthApp.ctrlIdx);
        gEthApp.stats.rxCtrlIdxErr++;
    }

    /*Check and copy frame to queue*/
    subType = *(PhysAddrPtr + 8);
    if(NTSCF_SUBTYPE == subType)
    {
        EthApp_enqueueFrame(lenByte, PhysAddrPtr + 8);
        gEthApp.stats.rxPktIndCnt++;
    }

    /*Do we need to update Hash Table*/
    if(NTSCF_INSERT_HASH_ENTRY == subType)
    {

        /*Get the CAN ID from packet*/
        canID = (uint8)(*(uint8_t*)(PhysAddrPtr + 8 + 12 + 12));
        tempHalfWord = (uint16_t)(*(uint16_t*)(PhysAddrPtr + 8 + 12 + 12 + 4));
        canMask = ntohs(tempHalfWord);
        tempHalfWord = (uint16_t)(*(uint16_t*)(PhysAddrPtr + 8 + 12 + 12 + 4 + 2));
        ethMask = ntohs(tempHalfWord);
        gatewayApp_updateHashEntry(canID, canMask, ethMask);
        gEthApp.stats.rxPktIndCnt++;
        return;
    }

    /*Do we need to update MAC Table*/
    if(NTSCF_ADD_MAC_ENTRY == subType)
    {
        for(count = 0; count < NUM_ETH_INTERFACES; count++)
        {
            /*Get the MAC address from packet and insert into table*/
            gatewayApp_updateMacLUT(PhysAddrPtr + 8 + 12 + 12 + 4 + (count * 6), count);
        }
        gEthApp.stats.rxPktIndCnt++;
        return;
    }

    /*Tell gateway to signal can traffic generator*/
    if((NTSCF_SUBTYPE_TRIGGER_MCU1_0_MSG_SEND == subType) || (NTSCF_SUBTYPE_GET_MCU1_0_STATS == subType))
    {
        gatewayApp_sendMsgtoGenerator(PhysAddrPtr + 8 + 12 + 12 + 4, subType);
        gEthApp.stats.rxPktIndCnt++;
        return;
    }
    
#ifndef GATEWAY_POLL_MODE
    if ((ethAppProcessRx == FALSE) || (postGatewayTask == TRUE))
    {
        /*Trigger Gateway task*/
        SemaphoreP_post(gateway_semHandle);
    }
#endif //GATEWAY_POLL_MODE
}

#if ((STD_ON == ETH_ZERO_COPY_TX) && (STD_OFF == ETH_USE_Q_APIS))
void EthApp_sendNoCpy(uint8 ctrlIdx,
                      uint16_t etherType,
                      uint8_t *payload,
                      uint8_t *dstMac,
                      uint16 len)
{
    Std_ReturnType retVal;

    retVal = Eth_TransmitNoCpy(ctrlIdx,
                          payload,
                          etherType,
                          len,
                          dstMac);
    if (ETH_E_BUSY == retVal)
    {
        Eth_TxConfirmation(ctrlIdx);
        retVal = Eth_TransmitNoCpy(ctrlIdx,
                          payload,
                          etherType,
                          len,
                          dstMac);
    }

    if (E_OK != retVal)
    {
        AppUtils_Printf(ETHAPP_MSG_NORMAL, ETHAPP_MSG_APP_NAME "send: failed to send buffer\n");
    }
}
#endif

void EthApp_send(uint8 ctrlIdx,
                        Eth_Frame *frame,
                        uint16 len)
{
    Eth_FrameType frameType = frame->hdr.etherType;
    Eth_BufIdxType bufIdx;
    BufReq_ReturnType bufStatus;
    boolean txConfirmation = FALSE;
    uint8 *bufPtr;
    Std_ReturnType retVal;

    /* QoS is not supported in current MCAL Eth, so set to 0 */
    uint8 priority = 0U;
    do
    {
        bufStatus = Eth_ProvideTxBuffer(ctrlIdx,
                                        priority,
                                        &bufIdx,
                                        &bufPtr,
                                        &len);
        if (BUFREQ_OK == bufStatus)
        {
            break;
        }
        else
        {
            if (BUFREQ_E_BUSY == bufStatus)
            {
                Eth_TxConfirmation(ctrlIdx);
                gEthApp.stats.txBufUndrflw++;
            }
            else
            {
                gEthApp.stats.txProvideError++;
            }
        }
    }
    while (TRUE);

    memcpy(bufPtr, frame->payload, len);

    if (BUFREQ_OK == bufStatus)
    {
        retVal = Eth_Transmit(ctrlIdx,
                              bufIdx,
                              frameType,
                              txConfirmation,
                              len,
                              frame->hdr.dstMac);
        if (E_OK != retVal)
        {
            AppUtils_Printf(ETHAPP_MSG_NORMAL, ETHAPP_MSG_APP_NAME "send: failed to send buffer\n");
        }
    }
    else
    {
        AppUtils_Printf(ETHAPP_MSG_NORMAL, ETHAPP_MSG_APP_NAME "send: failed to get TX buffer: %d\n", (int)bufStatus);
    }

}

Std_ReturnType EthApp_getTxBuffer(uint8 ctrlIdx,
                                  Eth_BufIdxType *bufIdx,
                                  uint8 **bufPtr,
                                  uint16 *len)
{
    BufReq_ReturnType bufStatus;
    Std_ReturnType retVal;
    /* QoS is not supported in current MCAL Eth, so set to 0 */
    uint8 priority = 0U;

    do
    {
        #if (STD_OFF == ETH_ENABLE_TX_INTERRUPT)
             Eth_TxConfirmation(ctrlIdx);
        #endif
        bufStatus = Eth_ProvideTxBuffer(ctrlIdx,
                                        priority,
                                        bufIdx,
                                        bufPtr,
                                        len);
        if (BUFREQ_E_BUSY == bufStatus)
        {
            gEthApp.stats.txBufUndrflw++;
            TaskP_sleep(1);
        }
        else
        {
            break;
        }
    }
    while (TRUE);

    if (BUFREQ_OK == bufStatus)
    {
        retVal = E_OK;
    }
    else
    {
        AppUtils_Printf(ETHAPP_MSG_NORMAL, ETHAPP_MSG_APP_NAME "getTxBuffer: failed to get buffer\n");
        retVal = E_NOT_OK;
    }
    return retVal;
}

void EthIf_TrcvModeIndication(uint8 CtrlIdx,
                              EthTrcv_ModeType TrcvMode)
{
    AppUtils_Printf(ETHAPP_MSG_NORMAL, ETHAPP_MSG_APP_NAME "EthIf_TrcvModeIndication: %s\n",
                  (ETHTRCV_MODE_ACTIVE == TrcvMode) ? "Active" : "Down");
}

void EthApp_wbCache(uint8 *buf, uint16 len)
{
    CacheP_wb(buf, len);
}

void EthApp_wbInvCache(uint8 *buf, uint16 len)
{
    CacheP_wbInv(buf, len);
}

void EthApp_invCache(uint8 *buf, uint16 len)
{
    CacheP_Inv(buf, len);
}

static int32_t EthApp_enqueueFrame(uint16_t pktLen, uint8_t *PhysAddrPtr)
{
#if (STD_ON == GATEWAY_PROFILE_MEMCPY)
    uint32_t cookie;
#endif //GATEWAY_PROFILE_MEMCPY

    /*Check if there is space in buffer*/
    /*First check if there is enough space below write pointer till bottom of queue*/
    if((gEthApp.ethQueue.currentWrPtr + pktLen) > gEthApp.ethQueue.bottomOfQueue)
    {
        /*If we wrapped around then would there be enough space*/
        if((gEthApp.ethQueue.currentRdPtr - gEthApp.ethQueue.topOfQueue) < pktLen)
        {
            /*No space*/
            gEthApp.ethQueue.numOverflowEvt++;
            return 0;
        }
        else
        {
            /* We can wrap around so reset*/
            gEthApp.ethQueue.currentWrPtr = gEthApp.ethQueue.topOfQueue;
        }
    }
    else
    {
        /*We need to test for a special case where write pointer has wrapped around but read pointer is stuck*/
        if(gEthApp.ethQueue.currentWrPtr == gEthApp.ethQueue.currentRdPtr)
        {
            /*If they are same then check number of bytes pushed and pulled off the queue*/
            if(gEthApp.ethQueue.numPktEnqueued != gEthApp.ethQueue.numPktDequeued)
            {
                gEthApp.ethQueue.numOverflowEvt++;
                return 0; /*No space*/
            }
        }
    }

    /*Get the timestamp*/
    gEthApp.ethDescQueue.rxTS[gEthApp.ethDescQueue.currentWrIndex] = TimerP_getTimeInUsecs();
    /*Get the CAN ID*/
    gEthApp.ethDescQueue.canID[gEthApp.ethDescQueue.currentWrIndex] = (uint8_t)(*(uint8_t*)(PhysAddrPtr + 12 + 12));

#if (STD_ON == GATEWAY_PROFILE_MEMCPY)
    /*Copy the packet data*/
    EthApp_invCache(PhysAddrPtr, pktLen);
    EthApp_invCache(gEthApp.ethQueue.currentWrPtr, pktLen);
    cookie = Hwi_disable();
    startTime = Timestamp_get32();
#endif //GATEWAY_PROFILE_MEMCPY
    memcpy(gEthApp.ethQueue.currentWrPtr, PhysAddrPtr, pktLen);

#if (STD_ON == GATEWAY_PROFILE_MEMCPY)
    stopTime = Timestamp_get32();
    Hwi_restore(cookie);

    /*Get running average*/
    avgTime += (stopTime - startTime);
    avgTime = avgTime / 2;
#endif //GATEWAY_PROFILE_MEMCPY

    /*Increment write pointer and number of packets enqueued*/
    gEthApp.ethQueue.currentWrPtr += pktLen;
    gEthApp.ethQueue.numPktEnqueued++;

    /*Now update packet length in descriptor ethQueue and update write index*/
    gEthApp.ethDescQueue.pktLen[gEthApp.ethDescQueue.currentWrIndex] = pktLen;

    /*Check wraparound and reset*/
    gEthApp.ethDescQueue.currentWrIndex++;
    if(gEthApp.ethDescQueue.currentWrIndex == ETH_NUM_PKT_IN_Q)
    {
        gEthApp.ethDescQueue.currentWrIndex = 0;
    }

    return 0;
}


static int32_t EthApp_registerIntrs(Enet_Type enetType)
{
    int32_t status = CPSW_SOK;
    uint32_t mdioIntrNum;
    uint32_t instId = 0U;

    if (ETH_DRV_CONFIG_0->enableVirtualMac == FALSE)
    {
        mdioIntrNum = EnetSoc_getIntrNum(enetType, instId, CPSW_INTR_MDIO_PEND);

        status = EnetSoc_setupIntrCfg(enetType, instId, CPSW_INTR_MDIO_PEND);
        ENETTRACE_ERR_IF(status != ENET_SOK, "Failed to setup MDIO interrupt: %d\n", status);

        if (status == CPSW_SOK)
        {
            HwiP_Params hwiParams;

            HwiP_Params_init(&hwiParams);
            hwiParams.arg = (uintptr_t)NULL;

            if(!HwiP_create(mdioIntrNum,
                            Eth_MdioIrqHdlr_0_wrapper, &hwiParams))
            {
                AppUtils_Printf(ETHAPP_MSG_NORMAL, ETHAPP_MSG_APP_NAME "Failed to create Interrupt for MDIO\n");
                status = CPSW_EFAIL;
            }
        }
    }

    return status;
}

static void EthApp_registerRxFlow(uint8_t *macAddress)
{
    gExpectedSid = ETH_SID_DISPATCH_VIRTMAC_ADD_UNICAST_MACADDR;
    Eth_DispatchVirtmacAddUnicastAddr(gEthApp.ctrlIdx, macAddress, ETH_PORT_HOST_PORT,  0);
    SemaphoreP_pend(ipcNotify_semHandle, SemaphoreP_WAIT_FOREVER);
    gExpectedSid = ETH_SID_DISPATCH_VIRTMAC_SUBSCRIBE_DSTMAC;
    Eth_DispatchVirtmacSubscribeDstMac(gEthApp.ctrlIdx, macAddress);
    SemaphoreP_pend(ipcNotify_semHandle, SemaphoreP_WAIT_FOREVER);

    void EthApp_addStaticUnicastAddr();
    EthApp_addStaticUnicastAddr();
}

void EthApp_rpcCmdComplete (uint32 controllerIdx,
                            uint8 sid,
                            Std_ReturnType status)
{
    if ((status == E_OK) && (gExpectedSid == sid))
    {
        SemaphoreP_post(ipcNotify_semHandle);
    }
    else
    {
    #if defined(SYSBIOS)
        /*FIXME : Where is this coming from*/
        Log_error3("EthApp_rpcCmdComplete not complete. "
                   "Expected SID:%u, Received SID:%u,"
                   "Received status:%u",
                   gExpectedSid, sid, status);
    #else
        AppUtils_Printf(ETHAPP_MSG_NORMAL, ETHAPP_MSG_APP_NAME "EthApp_rpcCmdComplete not complete.\n");
    #endif //SYSBIOS
    }
}

void EthApp_rpcFwRegistered (uint32 controllerIdx)
{
    SemaphoreP_post(ipcNotify_semHandle);
}

void EthApp_recvMsgNotify(void)
{
    Eth_NotifyVirtmacMsgReceived(gEthApp.ctrlIdx);

}

static void EthApp_validateEthConfig(Eth_ConfigType *ethConfig)
{
#if defined (SOC_J721E)
    assert(ethConfig->enetType == ETH_ENETTYPE_CPSW9G);
#elif defined (SOC_J7200)
    assert(ethConfig->enetType == ETH_ENETTYPE_CPSW5G);
#endif
    assert(ethConfig->enableVirtualMac == TRUE);
    assert(ethConfig->virtualMacCfg.ethfwRpcComChId == ETHAPP_REMOTEETHDEVICE_COMCHID);
    assert(ethConfig->virtualMacCfg.pollRecvMsgInEthMain == FALSE);
    assert(ethConfig->virtualMacCfg.rpcCmdComplete == &EthApp_rpcCmdComplete);
    assert(ethConfig->virtualMacCfg.fwRegisteredCb == &EthApp_rpcFwRegistered);
}

static void EthApp_createIpcSem(void)
{
    SemaphoreP_Params semaphoreParams;

    SemaphoreP_Params_init(&semaphoreParams);
    semaphoreParams.mode = SemaphoreP_Mode_BINARY;
    ipcNotify_semHandle = SemaphoreP_create(0, &semaphoreParams);
    assert (NULL != ipcNotify_semHandle);
}

static void EthApp_deleteIpcSem(void)
{
    assert (NULL != ipcNotify_semHandle);
    SemaphoreP_delete(ipcNotify_semHandle);
}

void EthApp_freeEthResources(void)
{
    Std_ReturnType status;

    gExpectedSid = ETH_SID_DISPATCH_VIRTMAC_DEINIT;
    status = Eth_DispatchVirtmacDeinit(gEthApp.ctrlIdx);
    assert(status == E_OK);
    SemaphoreP_pend(ipcNotify_semHandle, SemaphoreP_WAIT_FOREVER);
}

void EthApp_addStaticUnicastAddr()
{
    Std_ReturnType status;
    uint8 macAddr[] = {0x00,0x11,0x01,0x00,0x00,0x01};
    Eth_PortType port = ETH_PORT_MAC_PORT_3;

    gExpectedSid = ETH_SID_DISPATCH_VIRTMAC_ADD_UNICAST_MACADDR;
    status = Eth_DispatchVirtmacAddUnicastAddr(gEthApp.ctrlIdx, macAddr, port, 0);
    assert(status == E_OK);
    SemaphoreP_pend(ipcNotify_semHandle, SemaphoreP_WAIT_FOREVER);
}

void EthApp_addUnicastAddr(uint8 *macAddr, Eth_PortType port)
{
    Std_ReturnType status;

    gExpectedSid = ETH_SID_DISPATCH_VIRTMAC_ADD_UNICAST_MACADDR;
    status = Eth_DispatchVirtmacAddUnicastAddr(gEthApp.ctrlIdx, macAddr, port, 0);
    assert(status == E_OK);
    SemaphoreP_pend(ipcNotify_semHandle, SemaphoreP_WAIT_FOREVER);
}


#if (STD_ON == GATEWAY_PKTRX_TASK_USE_CLOCK)
static void EthApp_clockFxn (void* arg)
{
#if (STD_OFF == ETH_ENABLE_RX_INTERRUPT)
    EthApp_State *ethAppState = (EthApp_State *)arg;
    SemaphoreP_post(ethAppState->semRxHandle);
#endif

}
#endif

static void EthAppMain_clockFxn (void* arg)
{
    Eth_MainFunction();
}

#if (STD_ON == GATEWAY_PKTRX_TASK_USE_CLOCK)
static int32_t EthApp_reconfigClk()
{
    ClockP_stop(gEthApp.ethRxTxClock);
    ClockP_start(gEthApp.ethRxTxClock);

    return CPSW_SOK;
}
#endif


static void EthApp_pktRxTask(void* arg0,void* arg1)
{
    EthApp_State *ethAppState = (EthApp_State *)arg0;
    Eth_RxStatusType rxStatusType;
    /* QoS is not supported in current MCAL Eth, so set to 0 */
    uint8 fifoIdx = 0U;

    while(ethAppState->exitPktRxTask != TRUE)
    {
        SemaphoreP_pend(ethAppState->semRxHandle,  SemaphoreP_WAIT_FOREVER);
        if (ethAppState->exitPktRxTask == TRUE)
        {
            break;
        }

        do {
            rxStatusType = ETH_NOT_RECEIVED;
            if ((ethAppProcessRx == FALSE) || (MailboxP_getNumPendingMsgs(gEthApp.pktTxFreeMbx) > 0))
            {
                Eth_Receive(ethAppState->ctrlIdx, fifoIdx, &rxStatusType);
            }
        } while (rxStatusType != ETH_NOT_RECEIVED);

        #if (STD_ON == GATEWAY_PKTRX_TASK_USE_CLOCK)
            EthApp_reconfigClk();
        #endif
    }

}

static void EthApp_pktTxTask(void* arg0,void* arg1)
{
    EthApp_State *ethAppState = (EthApp_State *)arg0;
    EthApp_TxSendPktInfo txPktInfo;
    Bool mbxStatus;

    while(TRUE)
    {
        mbxStatus = MailboxP_pend(ethAppState->pktTxSendMbx, &txPktInfo,  MailboxP_WAIT_FOREVER);
        assert(mbxStatus == TRUE);
        if (txPktInfo.exitTxTask == TRUE)
        {
            break;
        }
        else
        {
            Std_ReturnType retVal;

            retVal = Eth_Transmit(ethAppState->ctrlIdx,
                                  txPktInfo.bufIdx,
                                  txPktInfo.frameType,
                                  FALSE,
                                  txPktInfo.len,
                                  txPktInfo.destMac);
            if (E_OK != retVal)
            {
                AppUtils_Printf(ETHAPP_MSG_NORMAL, ETHAPP_MSG_APP_NAME "send: failed to send buffer\n");
            }
            else
            {
                SemaphoreP_post(ethAppState->semTxReclaimObj);
            }
        }
    }
}

static void EthApp_pktReclaimTxTask(void* arg0,void* arg1)
{
    EthApp_State *ethAppState = (EthApp_State *)arg0;

    while(ethAppState->exitPktTxReclaimTask != TRUE)
    {
        SemaphoreP_pend(ethAppState->semTxReclaimObj,  SemaphoreP_WAIT_FOREVER);
        if (ethAppState->exitPktTxReclaimTask == TRUE)
        {
            break;
        }
        EthApp_queueAllFreeTxBuffer(ethAppState);
    }
}

static void EthApp_initPktProcessingTask(void)
{
    SemaphoreP_Params semParams;
    TaskP_Params tskParams;
    MailboxP_Params mbxParams;

    SemaphoreP_Params_init(&semParams);

    semParams.mode = SemaphoreP_Mode_BINARY;
    gEthApp.semRxHandle = SemaphoreP_create(0, &semParams);

    SemaphoreP_Params_init(&semParams);

    semParams.mode = SemaphoreP_Mode_BINARY;
    gEthApp.semTxReclaimObj = SemaphoreP_create(0, &semParams);


#if (STD_ON == GATEWAY_PKTRX_TASK_USE_CLOCK)
    ClockP_Params clkParams;
    ClockP_Params_init(&clkParams);

    clkParams.arg = (void*) &gEthApp;
    /* Set clock expiry time in OS ticks */
    clkParams.period = ETHAPP_PKTTXRX_SCHEDULE_PERIOD_MS;
    /* Configure clock in one shot mode instead of periodic tick mode */
    clkParams.runMode = ClockP_RunMode_ONESHOT;
    clkParams.startMode = ClockP_StartMode_USER;

    gEthApp.ethRxTxClock = ClockP_create((void*) &EthApp_clockFxn, &clkParams);
    assert(gEthApp.ethRxTxClock != NULL);
    ClockP_start(gEthApp.ethRxTxClock);
#endif

    MailboxP_Params_init(&mbxParams);
    mbxParams.size = sizeof(EthApp_TxSendPktInfo);
    mbxParams.count = ETH_NUM_TX_BUFFERS;
    mbxParams.buf = (void *)EthApp_TxSendMbxBuf;
    mbxParams.bufsize = sizeof(EthApp_TxSendMbxBuf);
    gEthApp.pktTxSendMbx = MailboxP_create(&mbxParams);
    assert (gEthApp.pktTxSendMbx != NULL);

    MailboxP_Params_init(&mbxParams);
    mbxParams.size = sizeof(EthApp_TxFreePktInfo);
    mbxParams.count = ETH_NUM_TX_BUFFERS;
    mbxParams.buf = (void *)EthApp_TxFreeMbxBuf;
    mbxParams.bufsize = sizeof(EthApp_TxFreeMbxBuf);
    gEthApp.pktTxFreeMbx = MailboxP_create(&mbxParams);
    assert (gEthApp.pktTxFreeMbx != NULL);

    gEthApp.exitPktRxTask = FALSE;
    TaskP_Params_init(&tskParams);
    tskParams.arg0 = (void*)&gEthApp;
    tskParams.priority = ETHAPP_PKTRX_TSK_PRI;
    tskParams.stack = EthApp_RxStack;
    tskParams.stacksize = sizeof(EthApp_RxStack);
    tskParams.name = ETHAPP_PKTRX_TASK_NAME;

    gEthApp.taskRxPkt = TaskP_create(EthApp_pktRxTask, &tskParams);
    assert (gEthApp.taskRxPkt != NULL);

    TaskP_Params_init(&tskParams);
    tskParams.arg0 = (void*)&gEthApp;
    tskParams.priority = ETHAPP_PKTTX_TSK_PRI;
    tskParams.stack = EthApp_TxStack;
    tskParams.stacksize = sizeof(EthApp_TxStack);
    tskParams.name = ETHAPP_PKTTX_TASK_NAME;

    gEthApp.taskTxPkt = TaskP_create(EthApp_pktTxTask, &tskParams);
    assert (gEthApp.taskTxPkt != NULL);

    TaskP_Params_init(&tskParams);
    tskParams.arg0 = (void*)&gEthApp;
    tskParams.priority = ETHAPP_RECLAIMTX_TSK_PRI;
    tskParams.stack = EthApp_ReclaimStack;
    tskParams.stacksize = sizeof(EthApp_ReclaimStack);
    tskParams.name = ETHAPP_RECLAIMTX_TASK_NAME;

    gEthApp.taskTxReclaim = TaskP_create(EthApp_pktReclaimTxTask, &tskParams);
    assert (gEthApp.taskTxReclaim != NULL);

}

static void EthApp_deInitPktProcessingTask(void)
{
    Bool mbxStatus;
    EthApp_TxSendPktInfo txPktInfo;

    gEthApp.exitPktRxTask = TRUE;
    SemaphoreP_post(gEthApp.semRxHandle);
    while(!TaskP_isTerminated(gEthApp.taskRxPkt))
    {
        TaskP_sleep(1);
    }
    TaskP_delete(gEthApp.taskRxPkt);

    gEthApp.exitPktTxReclaimTask = TRUE;
    SemaphoreP_post(gEthApp.semTxReclaimObj);
    while(!TaskP_isTerminated(gEthApp.taskTxPkt))
    {
        TaskP_sleep(1);
    }
    TaskP_delete(gEthApp.taskTxReclaim);

#if (STD_ON == GATEWAY_PKTRX_TASK_USE_CLOCK)
    ClockP_stop(gEthApp.ethRxTxClock);
    ClockP_delete(gEthApp.ethRxTxClock);
#endif

    SemaphoreP_delete(gEthApp.semRxHandle);
    SemaphoreP_delete(gEthApp.semTxReclaimObj);

    txPktInfo.exitTxTask = TRUE;
    mbxStatus = MailboxP_post(gEthApp.pktTxSendMbx, &txPktInfo, MailboxP_NO_WAIT);
    assert(mbxStatus == TRUE);
    while(!TaskP_isTerminated(gEthApp.taskTxPkt))
    {
        TaskP_sleep(1);
    }
    TaskP_delete(gEthApp.taskTxPkt);
    MailboxP_delete(gEthApp.pktTxFreeMbx);
    MailboxP_delete(gEthApp.pktTxSendMbx);
}


void EthApp_reclaimBuffers(void)
{
    Eth_RxStatusType rxStatusType;
    BufReq_ReturnType bufStatus;
    uint16 len;
    Eth_BufIdxType bufIdx;
    uint8 *bufPtr;
    /* QoS is not supported in current MCAL Eth, so set to 0 */
    uint8 priority = 0U;
    /* QoS is not supported in current MCAL Eth, so set to 0 */
    uint8 fifoIdx = 0U;

    if (gEthApp.linkUp[0] == TRUE)
    {
        #if (STD_OFF == ETH_ENABLE_TX_INTERRUPT)
            Eth_TxConfirmation(gEthApp.ctrlIdx);
        #endif
        if (ethAppProcessRx == TRUE)
        {
            len = 64 - ETH_HEADER_LEN;
            bufStatus = Eth_ProvideTxBuffer(gEthApp.ctrlIdx,
                                            priority,
                                            &bufIdx,
                                            &bufPtr,
                                            &len);
            if ((BUFREQ_OK == bufStatus) ||
                (BUFREQ_E_OVFL == bufStatus))
            {
                EthApp_TxFreePktInfo txFreePktInfo;
                Bool mbxStatus;

                txFreePktInfo.bufIdx = bufIdx;
                txFreePktInfo.bufPtr = bufPtr;
                txFreePktInfo.len    = len;
                mbxStatus = MailboxP_post(gEthApp.pktTxFreeMbx, &txFreePktInfo, MailboxP_NO_WAIT);
                assert(mbxStatus == TRUE);
            }
        }
        Eth_Receive(gEthApp.ctrlIdx, fifoIdx, &rxStatusType);
        if (rxStatusType == ETH_RECEIVED_MORE_DATA_AVAILABLE)
        {
            SemaphoreP_post(gEthApp.semRxHandle);
        }
    }
}

static void EthApp_queueAllFreeTxBuffer(EthApp_State *ethAppState)
{
    BufReq_ReturnType bufStatus;
    uint16 len;
    Eth_BufIdxType bufIdx;
    uint8 *bufPtr;
    /* QoS is not supported in current MCAL Eth, so set to 0 */
    uint8 priority = 0U;

    do {
        #if (STD_OFF == ETH_ENABLE_TX_INTERRUPT)
            Eth_TxConfirmation(gEthApp.ctrlIdx);
        #endif

        len = 64 - ETH_HEADER_LEN;
        bufStatus = Eth_ProvideTxBuffer(ethAppState->ctrlIdx,
                                        priority,
                                        &bufIdx,
                                        &bufPtr,
                                        &len);
        if ((BUFREQ_OK == bufStatus) ||
            (BUFREQ_E_OVFL == bufStatus))
        {
            EthApp_TxFreePktInfo txFreePktInfo;
            Bool mbxStatus;

            txFreePktInfo.bufIdx = bufIdx;
            txFreePktInfo.bufPtr = bufPtr;
            txFreePktInfo.len    = len;
            mbxStatus = MailboxP_post(ethAppState->pktTxFreeMbx, &txFreePktInfo, MailboxP_NO_WAIT);
            assert(mbxStatus == TRUE);
        }
    } while (bufStatus  != BUFREQ_E_BUSY);
}

void EthApp_queueTx(uint8 ctrlIdx,
                    Eth_Frame *frame,
                    uint16 len)
{
    Eth_FrameType frameType = frame->hdr.etherType;
    Eth_BufIdxType bufIdx;
    uint8 *bufPtr;
    EthApp_TxFreePktInfo txFreePktInfo;
    Bool mbxStatus;
    EthApp_TxSendPktInfo txSendPktInfo;

    assert(Osal_getThreadType() == Osal_ThreadType_Task);

    mbxStatus = MailboxP_pend(gEthApp.pktTxFreeMbx, &txFreePktInfo,  MailboxP_WAIT_FOREVER);
    assert(mbxStatus == TRUE);

    bufIdx = txFreePktInfo.bufIdx;
    bufPtr = txFreePktInfo.bufPtr;
    assert(len >= txFreePktInfo.len);

    memcpy(bufPtr, frame->payload, len);

    txSendPktInfo.bufIdx = bufIdx;
    txSendPktInfo.exitTxTask = FALSE;
    txSendPktInfo.len = len;
    txSendPktInfo.frameType = frameType;
    memcpy(txSendPktInfo.destMac, frame->hdr.dstMac, sizeof(txSendPktInfo.destMac));

    mbxStatus = MailboxP_post(gEthApp.pktTxSendMbx, &txSendPktInfo, MailboxP_NO_WAIT);
    assert(mbxStatus == TRUE);
}


void EthApp_sendProfileInfo(appProfileAvgLoadInfo *avgProfileInfo)
{
    Std_ReturnType status;

    status = Eth_SendCustomNotify(gEthApp.ctrlIdx, avgProfileInfo, sizeof(*avgProfileInfo));
    assert(status == E_OK);
}

void EthIf_CtrlModeIndication(uint8 ControllerId,
                              Eth_ModeType ControllerMode)
{
    if (ETH_MODE_DOWN == ControllerMode)
    {
        gEthApp.stats.ctrlModeIndDownCnt++;
    }
    else if (ETH_MODE_ACTIVE == ControllerMode)
    {
        gEthApp.stats.ctrlModeIndActCnt++;
    }
    else
    {
        gEthApp.stats.ctrlModeIndErr++;
    }
}

