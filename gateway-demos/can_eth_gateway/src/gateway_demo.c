/*
*
* Copyright (c) 2019 Texas Instruments Incorporated
*
* All rights reserved not granted herein.
*
* Limited License.
*
* Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
* license under copyrights and patents it now or hereafter owns or controls to make,
* have made, use, import, offer to sell and sell ("Utilize") this software subject to the
* terms herein.  With respect to the foregoing patent license, such license is granted
* solely to the extent that any such patent is necessary to Utilize the software alone.
* The patent license shall not apply to any combinations which include this software,
* other than combinations with devices manufactured by or for TI ("TI Devices").
* No hardware patent is licensed hereunder.
*
* Redistributions must preserve existing copyright notices and reproduce this license
* (including the above copyright notice and the disclaimer and (if applicable) source
* code license limitations below) in the documentation and/or other materials provided
* with the distribution
*
* Redistribution and use in binary form, without modification, are permitted provided
* that the following conditions are met:
*
* *       No reverse engineering, decompilation, or disassembly of this software is
* permitted with respect to any software provided in binary form.
*
* *       any redistribution and use are licensed by TI for use only with TI Devices.
*
* *       Nothing shall obligate TI to provide you with source code for the software
* licensed and provided to you in object code.
*
* If software source code is provided to you, modification and redistribution of the
* source code are permitted provided that the following conditions are met:
*
* *       any redistribution and use of the source code, including any resulting derivative
* works, are licensed by TI for use only with TI Devices.
*
* *       any redistribution and use of any object code compiled from the source code
* and any resulting derivative works, are licensed by TI for use only with TI Devices.
*
* Neither the name of Texas Instruments Incorporated nor the names of its suppliers
*
* may be used to endorse or promote products derived from this software without
* specific prior written permission.
*
* DISCLAIMER.
*
* THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
* OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
* OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
* IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
* BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
* OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
* OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
* OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/**
 *  \file     gateway_demo.c
 *
 *  \brief    This file implements gateway functionalities of Eth-CAN gateway demo application
 */

/* ========================================================================== */
/*                             Include Files                                  */
/* ========================================================================== */
#include <assert.h>
#if defined(SYSBIOS)
#include <ti/sysbios/BIOS.h>
#include <ti/sysbios/utils/Load.h>
#endif //SYSBIOS
#include "gateway_demo_cfg.h"
#include "gateway_demo.h"
#include "can_process.h"
#include "eth_process.h"

/* ========================================================================== */
/*                           Macros & Typedefs                                */
/* ========================================================================== */
#define ETH_FRAME_MAX_SIZE                                           (1500U)
#define IEEE1722_FRAME_PAYLOAD_UPDATE_OFFSET                         (24U)

/* ========================================================================== */
/*                   Internal Function Declarations                           */
/* ========================================================================== */
static void gatewayApp_processEth(void);
static void gatewayApp_processCAN(void);
static void gatewayApp_getRoutingInfo(GatewayApp_RoutingInfo *routingInfo);
static void gatewayApp_updateEthQStatus(void);
static void gatewayApp_dequeueCAN(GatewayApp_RoutingInfo *routingInfo);
static void gatewayApp_sendStatFrame(uint32_t canID);
static void gatewayApp_prepare1722msg(Eth_Frame *frm1722);
static void gatewayApp_updateCANQueue(void);
static void gatewayApp_initRoutingTable();


/* ========================================================================== */
/*                            Global Variables                                */
/* ========================================================================== */
/**< Default structure for CAN parameters */
uint8_t sequence_id = 0;
/**< Sequence ID used in 1722 frames */

uint32_t bridge_delay_can_2_can = 0;
/**< Bridge delay for last CAN 2 CAN route */

uint32_t bridge_delay_can_2_eth = 0;
/**< Bridge delay for last CAN 2 Eth route */

uint32_t bridge_delay_eth_2_can[NUM_CAN_INTERFACES] = {0};
/**< Bridge delay for last Eth 2 CAN route */

uint32_t bridge_delay_eth_2_eth = 0;
/**< Bridge delay for last Eth 2 Eth route */

const uint8_t can_port_num[NUM_CAN_INTERFACES] = {1, 2};
/*CAN port numbers for Main MCAN4 and MCAN9*/

/*Pointer to Hash table*/
GatewayApp_HashBucket HashTable[NUM_BUCKETS_IN_HASH_TABLE];

/*LUT containing MAC addresses for all Ethernet ports*/
GatewayApp_EthMac_LUT ethMacLUT[NUM_ETH_INTERFACES];
/* ========================================================================== */
/*                            External Variables                              */
/* ========================================================================== */
extern GatewayApp_State gGatewayApp;
extern EthApp_State gEthApp;
extern CanApp_State canAppState;
Eth_Frame * frame_1722_ETHSrcIf = NULL;
Eth_Frame * frame_1722_CANSrcIf = NULL;
Eth_Frame * frame_1722_diagnostics = NULL;
Eth_Frame * frame_1722_stats = NULL;

extern uint8 BcastAddr[ETH_MAC_ADDR_LEN];

extern TimerP_Handle GatewayApp_timerHandle;

/**< Timer handle to trigger CAN polling events */
extern SemaphoreP_Handle gateway_semHandle;
/**< Semaphore handle used to trigger routing task */

extern SemaphoreP_Handle CanIf_TxConfirmationSemaphore;
/**< Semaphore used to wait for CAN Tx confirmation */
extern uint32_t busOff_error_cnt;
/**< Counter which increments during Bus off error */

/* ========================================================================== */
/*                          Function Definitions                              */
/* ========================================================================== */
int32_t GatewayApp_Init(void)
{
    frame_1722_CANSrcIf = (Eth_Frame *) malloc(sizeof(*frame_1722_CANSrcIf) + ETH_FRAME_MAX_SIZE);

    frame_1722_ETHSrcIf = (Eth_Frame *) malloc(sizeof(*frame_1722_CANSrcIf) + ETH_FRAME_MAX_SIZE);

    frame_1722_diagnostics = (Eth_Frame *) malloc(sizeof(*frame_1722_diagnostics) + ETH_FRAME_MAX_SIZE);

    frame_1722_stats = (Eth_Frame *) malloc(sizeof(*frame_1722_stats) + ETH_FRAME_MAX_SIZE);

    /*16-bit Unique ID used for stream ID*/
    gGatewayApp.stream[0] = 0xAA;
    gGatewayApp.stream[1] = 0xBB;

    /*Populate DA and SA MAC*/
    memcpy(frame_1722_CANSrcIf->hdr.dstMac, BcastAddr, 6);
    memcpy(frame_1722_CANSrcIf->hdr.srcMac, gEthApp.macAddr, 6);

    memcpy(frame_1722_diagnostics->hdr.dstMac, BcastAddr, 6);
    memcpy(frame_1722_diagnostics->hdr.srcMac, gEthApp.macAddr, 6);

    memcpy(frame_1722_stats->hdr.dstMac, BcastAddr, 6);
    memcpy(frame_1722_stats->hdr.srcMac, gEthApp.macAddr, 6);

    memcpy(frame_1722_ETHSrcIf->hdr.srcMac, gEthApp.macAddr, 6);
    /*Create ACF message header*/
    gatewayApp_prepare1722msg(frame_1722_CANSrcIf);
    gatewayApp_prepare1722msg(frame_1722_diagnostics);
    /*We modify byte 41 of packet to tell PC tool that this is a special frame*/ 
    frame_1722_diagnostics->payload[41 - 14] = 0xB0; //payload is 14B

    gatewayApp_prepare1722msg(frame_1722_ETHSrcIf);
    gatewayApp_prepare1722msg(frame_1722_stats);
    /*We modify byte 41 of packet to tell PC tool that this is a special frame*/ 
    frame_1722_stats->payload[41 - 14] = 0xE0; //payload is 14B
    /*Initialize routing table*/
    gatewayApp_initRoutingTable();

    /*Start the gateway timer*/
    TimerP_start(GatewayApp_timerHandle);

    return TRUE;
}

void gatewayApp_Router(void)
{
    /*Check semaphore posted by timer to see if either CAN or Ethernet frame arrived*/
    SemaphoreP_pend(gateway_semHandle, SemaphoreP_WAIT_FOREVER);  

    while ((gEthApp.ethQueue.numPktEnqueued != gEthApp.ethQueue.numPktDequeued)
           ||
           (canAppState.canQueue.numPduEnqueued != canAppState.canQueue.numPduDequeued))
    {
        if(gEthApp.ethQueue.numPktEnqueued != gEthApp.ethQueue.numPktDequeued)
        {
            gatewayApp_processEth();
        }

        if(canAppState.canQueue.numPduEnqueued != canAppState.canQueue.numPduDequeued)
        {
            gatewayApp_processCAN();
        }
    }
    return;
}

void gatewayApp_Shutdown(void)
{
    free(frame_1722_ETHSrcIf);
    free(frame_1722_CANSrcIf);
    free(frame_1722_diagnostics);
    free(frame_1722_stats);

    /*Stop the timer*/
    TimerP_stop(GatewayApp_timerHandle);

    return;
}

/*Service ethernet frame and send on CAN interface*/
static void gatewayApp_processEth(void)
{   
    const Can_PduType   *PduInfo;
    uint8               hth = 0U;
    uint8_t             can_if_count = 0, eth_if_count = 0;
    Std_ReturnType      status;
    GatewayApp_RoutingInfo  routingInfo; 
    Can_PduType         CanApp_Pdu_1;
    volatile uint64_t   rxTS = 0, currentTS = 0;
#if (STD_ON == GATEWAY_TEST_AUTOMATION)
    uint32_t            temp_bridge_delay = 0;
#endif //GATEWAY_TEST_AUTOMATION

    /*Clear the routing table information before lookup*/
    memset(&routingInfo, 0x0, sizeof(GatewayApp_RoutingInfo));    
    /*Call function to do actual routing and parse frames*/
    gatewayApp_getRoutingInfo(&routingInfo);
    /*Get rx timestamp to calculate bridge delay*/
    rxTS = gEthApp.ethDescQueue.rxTS[gEthApp.ethDescQueue.currentRdIndex];

    /*------------------ETH 2 ETH routing------------------*/
    for(eth_if_count = 0; eth_if_count < NUM_ETH_INTERFACES; eth_if_count++)
    {
        if(routingInfo.ethIfStatus[eth_if_count])
        {
            /*Copy destination MAC address from the LUT*/
            memcpy(frame_1722_ETHSrcIf->hdr.dstMac, ethMacLUT[eth_if_count].macAddr, 6);
            /*Copy payload from source to destination*/
            memcpy(frame_1722_ETHSrcIf->payload, gEthApp.ethQueue.currentRdPtr, gEthApp.ethDescQueue.pktLen[gEthApp.ethDescQueue.currentRdIndex]);

        #if (STD_ON == GATEWAY_TEST_AUTOMATION)
            /*Code for test automation*/
            frame_1722_ETHSrcIf->payload[41-14] = ETH_2_ETH_FRAME_ID; /*Embed the marker*/
            temp_bridge_delay = htonl(bridge_delay_eth_2_eth);
            memcpy(&(frame_1722_ETHSrcIf->payload[42-14]), &temp_bridge_delay, 4); /*Embed the bridge delay*/
        #endif //GATEWAY_TEST_AUTOMATION

        #if ((STD_ON == ETH_ZERO_COPY_TX) && (STD_OFF == ETH_USE_Q_APIS))
            EthApp_sendNoCpy(gEthApp.ctrlIdx, 
                      AVTP_ETH_TYPE,
                      (uint8_t *)frame_1722_ETHSrcIf,
                      &frame_1722_ETHSrcIf->hdr.dstMac[0U],
                       DEFAULT_1722_ETH_PKT_LEN - 8);
        #else
            EthApp_send(gEthApp.ctrlIdx, frame_1722_ETHSrcIf, DEFAULT_1722_ETH_PKT_LEN - 8);
        #endif
            gGatewayApp.eth_send_count[0]++;

        #if (STD_ON == GATEWAY_TEST_AUTOMATION)
            currentTS = TimerP_getTimeInUsecs();
            bridge_delay_eth_2_eth = currentTS - rxTS;
        #endif //GATEWAY_TEST_AUTOMATION
        }        
    }

    /*--------------------ETH 2 CAN routing-----------------------*/
    /*Check on all interfaces on which we have to send*/
    for(can_if_count = 0; can_if_count < NUM_CAN_INTERFACES; can_if_count++)
    {
        if(routingInfo.canIfStatus[can_if_count])
        {
            CanApp_Pdu_1.length = 64;
            CanApp_Pdu_1.id = gEthApp.ethDescQueue.canID[gEthApp.ethDescQueue.currentRdIndex] | 0xC0000000;
            CanApp_Pdu_1.sdu = gEthApp.ethQueue.currentRdPtr + 12 + 12 + 4;

            if(can_if_count == 0)
            {
            #if defined (BUILD_MCU2_1)
                /*Send on MCAN4*/
                CanApp_Pdu_1.swPduHandle = 4U;
                hth = CAN_HTRH_2;
            #endif //BUILD_MCU2_1
            }
            else
            {
            #if defined (BUILD_MCU2_1)
                /*Send on MCAN9*/
                CanApp_Pdu_1.swPduHandle = 5U;
                hth = CAN_HTRH_4;
            #endif //BUILD_MCU2_1
            }
            
            PduInfo = &CanApp_Pdu_1;
            /*Code for test automation, this tells the PCAN tool that it's a Eth 2 CAN delay*/
        #if (STD_ON == GATEWAY_TEST_AUTOMATION)
            CanApp_Pdu_1.sdu[6] = ETH_2_CAN_FRAME_ID;
            /*Test automation code, insert the Eth 2 CAN bridge delay in packet*/
            memcpy(&(CanApp_Pdu_1.sdu[7]), &(bridge_delay_eth_2_can[can_if_count]), 4);
        #endif //GATEWAY_TEST_AUTOMATION
            
            status = Can_Write(hth, PduInfo);

        #if (STD_ON == GATEWAY_TEST_AUTOMATION)
            currentTS = TimerP_getTimeInUsecs();
            bridge_delay_eth_2_can[can_if_count] = currentTS - rxTS;
        #endif //GATEWAY_TEST_AUTOMATION

        #if (CAN_TX_POLLING == STD_ON)
            Can_MainFunction_Write();
        #else
            SemaphoreP_pend(CanIf_TxConfirmationSemaphore,
                                    SemaphoreP_WAIT_FOREVER);
        #endif //CAN_TX_POLLING

            if (status != E_OK)
            {
                gGatewayApp.can_bus_busy++;
            }                                
        }
    }
    
    /*Once all CAN frames have been sent out update Q status*/
    gatewayApp_updateEthQStatus();
    
    return;
}

/*Parse 1722 frames and extract CAN message from them*/
static void gatewayApp_getRoutingInfo(GatewayApp_RoutingInfo *routingInfo)
{
    /*Point to start of first ACF message*/
    uint16_t pkt_length = 0;

    /*check if we need to wraparound eth descriptor queue*/
    if(gEthApp.ethDescQueue.currentRdIndex >= ETH_NUM_PKT_IN_Q)
    {
        gEthApp.ethDescQueue.currentRdIndex = 0;
    }  
    
    /*check if we need to wraparound eth queue*/
    pkt_length = gEthApp.ethDescQueue.pktLen[gEthApp.ethDescQueue.currentRdIndex];
    if(gEthApp.ethQueue.currentRdPtr + pkt_length > gEthApp.ethQueue.bottomOfQueue)
    {
        gEthApp.ethQueue.currentRdPtr = gEthApp.ethQueue.topOfQueue;
    }

    /*Do Lookup with the CAN ID received in packet*/
    gatewayApp_lookUpRoutingTable(gEthApp.ethDescQueue.canID[gEthApp.ethDescQueue.currentRdIndex], routingInfo);

    return;
}

static void gatewayApp_updateEthQStatus(void)
{
    /*CAN frames have been sent out so now update ethernet Q status*/
    gEthApp.ethQueue.currentRdPtr += gEthApp.ethDescQueue.pktLen[gEthApp.ethDescQueue.currentRdIndex];

    /*Update count*/
    gEthApp.ethQueue.numPktDequeued++;

    /*Check wraparound and reset for descriptor Q*/
    gEthApp.ethDescQueue.currentRdIndex++;
}

/*Service CAN frame and send on Eth interface*/
static void gatewayApp_processCAN()
{
    const Can_PduType   *PduInfo;
    uint8               hth = 0U;
    Std_ReturnType      status; 
    uint8_t             can_if_count = 0, eth_if_count = 0;
    GatewayApp_RoutingInfo routingInfo;
    Can_PduType     CanApp_Pdu_2;
    uint32_t        canID = 0, temp_word = 0;
    volatile uint64_t  rxTS = 0, currentTS = 0;
    uint16_t        index = 0;

#if (STD_ON == GATEWAY_TEST_AUTOMATION)
    uint32_t           temp_bridge_delay = 0;
#endif //GATEWAY_TEST_AUTOMATION

    /*Reset all routing info*/
    memset(&routingInfo, 0x0, sizeof(GatewayApp_RoutingInfo));

    /*Dequeue CAN frames, embed it in 1722 frame. Also checks routing*/
    gatewayApp_dequeueCAN(&routingInfo);

    /*Get CAN ID and pointer to payload for embedding into frame*/
    index = canAppState.canDescQueue.currentRdIndex;
    canID = canAppState.canDescQueue.Can_Pdu[index].id;    
    /*Get rx timestamp*/
    rxTS = canAppState.canDescQueue.Can_Pdu[index].rxTS;

    /*Dequeue CAN frames, do lookup and send on Eth interface*/
    for(eth_if_count = 0; eth_if_count < NUM_ETH_INTERFACES; eth_if_count++)
    {
        /*Is it a special packet sent by CAN generator ?*/
        if (*(canAppState.canQueue.currentRdPtr) == 0xAA)
        {
            gatewayApp_sendStatFrame(canID);
            break;
        }

        if(routingInfo.ethIfStatus[eth_if_count])
        {
            /*Copy the CAN ID into the frame*/
            temp_word = htonl(canID);
            memcpy(frame_1722_CANSrcIf->payload + 24, &temp_word, 4);
            /*Copy the data into the frame*/
            memcpy(frame_1722_CANSrcIf->payload + 28, canAppState.canQueue.currentRdPtr, canAppState.canDescQueue.Can_Pdu[index].length);

        #if (STD_ON == GATEWAY_TEST_AUTOMATION)
            /*Test automation code*/
            temp_bridge_delay = htonl(bridge_delay_can_2_eth);
            memcpy(&(frame_1722_CANSrcIf->payload[42 - 14]), &temp_bridge_delay, 4); /*Embed the delay*/
            frame_1722_CANSrcIf->payload[41 - 14] = CAN_2_ETH_FRAME_ID;  /*Embed the marker*/
        #endif //GATEWAY_TEST_AUTOMATION

        #if ((STD_ON == ETH_ZERO_COPY_TX) && (STD_OFF == ETH_USE_Q_APIS))
            EthApp_sendNoCpy(gEthApp.ctrlIdx, 
                      AVTP_ETH_TYPE,
                      (uint8_t *)frame_1722_CANSrcIf,
                      &frame_1722_CANSrcIf->hdr.dstMac[0U],
                       DEFAULT_1722_ETH_PKT_LEN - 8);
        #else
            EthApp_send(gEthApp.ctrlIdx, frame_1722_CANSrcIf, DEFAULT_1722_ETH_PKT_LEN - 8);
        #endif

        #if (STD_ON == GATEWAY_TEST_AUTOMATION)
            currentTS = TimerP_getTimeInUsecs();
            bridge_delay_can_2_eth = currentTS - rxTS;
        #endif //GATEWAY_TEST_AUTOMATION

            gGatewayApp.eth_send_count[0]++;
        }
    }

    /*If applicable send on CAN interface as well*/
    for(can_if_count = 0; can_if_count < NUM_CAN_INTERFACES; can_if_count++)
    {
        /*Is it a special packet sent by CAN generator ?*/
        if (*(canAppState.canQueue.currentRdPtr) == 0xAA)
        {
            break;
        }

        /*Don't echo back on the same port*/
        if(routingInfo.canIfStatus[can_if_count])
        {
            /*Update length and CAN ID*/
            CanApp_Pdu_2.length  = 64U;
            CanApp_Pdu_2.id = canID | 0xC0000000;

            /*Point to the packet data*/
            CanApp_Pdu_2.sdu = canAppState.canQueue.currentRdPtr;

        #if (STD_ON == GATEWAY_TEST_AUTOMATION)
            /*Add some code for test automation*/
            /*This tells the pcan automation tool that this delay is for CAN 2 CAN*/
            canAppState.canQueue.currentRdPtr[6] = CAN_2_CAN_FRAME_ID;
            /*Embed the bridge delay for last CAN 2 CAN routing in the payload*/
            memcpy((canAppState.canQueue.currentRdPtr + 7), &bridge_delay_can_2_can, 4);
        #endif //GATEWAY_TEST_AUTOMATION

            if(can_if_count == 0)
            {
            #if defined (BUILD_MCU2_1)
                /*Send on MCAN4*/
                CanApp_Pdu_2.swPduHandle = 4U;
                hth = CAN_HTRH_2;
            #endif //BUILD_MCU2_1
            }
            else
            {
            #if defined (BUILD_MCU2_1)
                /*Send on MCAN9*/
                CanApp_Pdu_2.swPduHandle = 5U;
                hth = CAN_HTRH_4;
            #endif //BUILD_MCU2_1
            }
        
            PduInfo = &CanApp_Pdu_2;

            status = Can_Write(hth, PduInfo);
        #if (STD_ON == GATEWAY_TEST_AUTOMATION)
            currentTS = TimerP_getTimeInUsecs();
            bridge_delay_can_2_can = currentTS - rxTS;
        #endif //GATEWAY_TEST_AUTOMATION
        #if (CAN_TX_POLLING == STD_ON)
            Can_MainFunction_Write();
        #else
            SemaphoreP_pend(CanIf_TxConfirmationSemaphore,
                                    SemaphoreP_WAIT_FOREVER);
        #endif //CAN_TX_POLLING
            
            if (status != E_OK)
            {
                gGatewayApp.can_bus_busy++;
            }
        }
    }    

    /*Update queue status*/
    gatewayApp_updateCANQueue();    

    return;
}

static void gatewayApp_dequeueCAN(GatewayApp_RoutingInfo *routingInfo)
{
    if(canAppState.canDescQueue.currentRdIndex >= MCAN_NUM_PDU_IN_Q)
    {
        canAppState.canDescQueue.currentRdIndex = 0;
    }

    /*Check wraparound and reset Rd ptr*/
    if((canAppState.canQueue.currentRdPtr + canAppState.canDescQueue.Can_Pdu[canAppState.canDescQueue.currentRdIndex].length) > canAppState.canQueue.bottomOfQueue)
    {
        canAppState.canQueue.currentRdPtr = canAppState.canQueue.topOfQueue;
    }
    /*Update routing information*/
    gatewayApp_lookUpRoutingTable((uint8_t)canAppState.canDescQueue.Can_Pdu[canAppState.canDescQueue.currentRdIndex].id, routingInfo);
    
    return;
}

static void gatewayApp_sendStatFrame(uint32_t canID)
{
    uint32_t temp_word;

    /*Copy the CAN ID into the frame*/
    temp_word = htonl(canID);
    memcpy(frame_1722_stats->payload + 24, &temp_word, 4);
    /*Copy the data into the frame*/
    memcpy(frame_1722_stats->payload + 28, canAppState.canQueue.currentRdPtr, canAppState.canDescQueue.Can_Pdu[canAppState.canDescQueue.currentRdIndex].length);

    #if ((STD_ON == ETH_ZERO_COPY_TX) && (STD_OFF == ETH_USE_Q_APIS))
        EthApp_sendNoCpy(gEthApp.ctrlIdx, 
                    AVTP_ETH_TYPE,
                    (uint8_t *)frame_1722_stats,
                    &frame_1722_stats->hdr.dstMac[0U],
                    DEFAULT_1722_ETH_PKT_LEN - 8);
    #else
        EthApp_send(gEthApp.ctrlIdx, frame_1722_stats, DEFAULT_1722_ETH_PKT_LEN - 8);
    #endif
}

static void gatewayApp_prepare1722msg(Eth_Frame *frm1722)
{
    uint8_t *frame = frm1722->payload;
    uint8_t temp_byte = 0;
    uint8_t curr_byte_offset = 0;
    uint16_t ntscf_data_length = DEFAULT_ACF_MSG_LENGTH_CAN * 4;
    uint8_t can_pad_val = 0;

    /*set eth type*/
    frm1722->hdr.etherType = AVTP_ETH_TYPE;

    /*Prepare 1722 message for a 64B CAN frame*/
    *(frame + curr_byte_offset++) = NTSCF_SUBTYPE;

    temp_byte = 0;
    temp_byte |= ((uint8_t)SV_BIT << 7) | ((uint8_t)NTSCF_VERSION << 4) | (uint8_t)(ntscf_data_length >> 8);

    /*Second byte is a composite value*/
    *(frame + curr_byte_offset++) = temp_byte;

    /*Write third byte which is lower 8 bits of payload length*/
    temp_byte = ntscf_data_length & 0xff;    
    *(frame + curr_byte_offset++) = temp_byte;

    /*fourth byte is sequence num*/
    *(frame + curr_byte_offset++) = sequence_id++;

    /*Next two bytes are stream ID*/
    //memcpy(frame + curr_byte_offset, gEthDrv.macAddr, 6);
    memcpy(frame + curr_byte_offset + 6, gGatewayApp.stream, 2);
    curr_byte_offset += 8;

    /*Now prepare the ACF message payload*/
    temp_byte = 0;
    temp_byte |= (ACF_MSG_TYPE_CAN << 1) | (uint8_t)((uint16_t)DEFAULT_ACF_MSG_LENGTH_CAN >> 8);
    *(frame + curr_byte_offset++) = temp_byte;

    temp_byte = DEFAULT_ACF_MSG_LENGTH_CAN & 0xff;
    *(frame + curr_byte_offset++) = temp_byte;

    temp_byte = ((can_pad_val & CAN_PAD_MASK) << CAN_SHIFT_VAL) | (MTV_BIT << MTV_SHIFT_VAL) \
                | (RTR_BIT << RTR_SHIFT_VAL) | (EFF_BIT << EFF_SHIFT_VAL) | (BRS_BIT << BRS_SHIFT_VAL) \
                | (FDF_BIT << FDF_SHIFT_VAL) | (ESI_BIT << ESI_SHIFT_VAL);
    *(frame + curr_byte_offset++) = temp_byte;

    temp_byte = 0;
    temp_byte |= (DEFAULT_CAN_BUS_ID | CAN_BUS_ID_MASK);
    *(frame + curr_byte_offset++) = temp_byte;

    /*message timestamp is zero*/
    memset(frame + curr_byte_offset, 0x0, 8);
    curr_byte_offset += 8;

    /*Make reserved byte and CAN ID as zero*/
    memset(frame + curr_byte_offset, 0x0, 4);
    curr_byte_offset += 4;

    /*Make CAN message payload zero*/
    memset(frame + curr_byte_offset, 0x0, 64);
    curr_byte_offset += 8;

}

static void gatewayApp_initRoutingTable()
{
    uint8_t count = 0;

    for(count = 0; count < NUM_BUCKETS_IN_HASH_TABLE; count++)
    {
        /*Make bucket size 0 and assign to NULL*/
        HashTable[count].numBuckets = 0;
        HashTable[count].entry = (GatewayApp_HashEntry*) malloc(sizeof(GatewayApp_HashEntry));
        assert(HashTable[count].entry != NULL);

        /*Zero out CAN mask*/
        HashTable[count].entry->canIfBitMask = 0;
        /*Zero out Eth mask*/
        HashTable[count].entry->ethIfBitMask = 0;
        
    }
}

void gatewayApp_updateHashEntry(uint8_t canID, uint16_t canMask, uint16_t ethMask)
{
    /*Current code does not handle collision*/
    HashTable[canID].entry->canIfBitMask = canMask;
    HashTable[canID].entry->ethIfBitMask = ethMask;
}

void gatewayApp_lookUpRoutingTable(uint8_t canID, GatewayApp_RoutingInfo* routingInfo)
{
    uint8_t count = 0;

    for(count = 0; count < NUM_CAN_INTERFACES; count++)
    {
       routingInfo->canIfStatus[count] = (HashTable[canID].entry->canIfBitMask >> count) & 1;
    } 

    for(count = 0; count < NUM_ETH_INTERFACES; count++)
    {
       routingInfo->ethIfStatus[count] = (HashTable[canID].entry->ethIfBitMask >> count) & 1;
    }
  
}

void gatewayApp_updateMacLUT(uint8_t *macID, uint8_t portNum)
{
    memcpy(ethMacLUT[portNum].macAddr, macID, 6);
}

void gatewayApp_sendMsgtoGenerator(uint8_t *pktBuf, uint8_t subType)
{
    uint8_t can_generator_payload[64] = {0};
    const Can_PduType   *PduInfo;
    Can_PduType     CanApp_Pdu_2;
    Std_ReturnType   status;
    uint8               hth = 0U;

    /*Send message to Generator app over MCAN8/9*/
    CanApp_Pdu_2.length  = 64U;
    CanApp_Pdu_2.id = CAN_ID_FOR_GENERATOR | 0xC0000000;

    /*Point to the packet data*/
    CanApp_Pdu_2.sdu = can_generator_payload;

    /*Embed data in CAN frame*/
    if (NTSCF_SUBTYPE_TRIGGER_MCU1_0_MSG_SEND == subType)
    {
        can_generator_payload[0] = GENERATOR_PKT_MARKER; /*Tells generator app to treat this as special*/
        can_generator_payload[1] = *(pktBuf + 1);        /*Encode the test duration*/
        can_generator_payload[2] = *pktBuf;              /*encode the test type. CAN 2 CAN, CAN 2 Eth, Eth 2 CAN etc*/
    }

    if (NTSCF_SUBTYPE_GET_MCU1_0_STATS == subType)
    {
        can_generator_payload[0] = GENERATOR_STAT_REQUEST_MARKER; /*Tells generator app to treat this as special*/
    }   

    /*Send on MCAN9 which is connected to MCU MCAN1*/
    CanApp_Pdu_2.swPduHandle = 5U;
    hth = CAN_HTRH_4;

    PduInfo = &CanApp_Pdu_2;

    status = Can_Write(hth, PduInfo);
    #if (CAN_TX_POLLING == STD_ON)
        Can_MainFunction_Write();
    #else
        SemaphoreP_pend(CanIf_TxConfirmationSemaphore,
                                SemaphoreP_WAIT_FOREVER);
    #endif //CAN_TX_POLLING
        
    if (status != E_OK)
    {
        gGatewayApp.can_bus_busy++;
    }
}

void gatewayApp_printParams(void)
{
    uint8_t cpu_load = 0U;
    /*Get total CPU load in percentage */
    #if defined(SYSBIOS)
    cpu_load = Load_getCPULoad();
    #endif

    /*Print stats for number of Ethernet and CAN frames 
    received and transmitted*/

    AppUtils_Printf(GATEWAYAPP_MSG_NORMAL, "\n---------------Stats---------------\r\n\n");

    AppUtils_Printf(GATEWAYAPP_MSG_NORMAL, 
                "Number of 1722 Frames Received : %d\r\n",
                gEthApp.ethQueue.numPktEnqueued);

    AppUtils_Printf(GATEWAYAPP_MSG_NORMAL, 
                "Number of 1722 Frames Dropped due to overflow: %d\r\n",
                gEthApp.ethQueue.numOverflowEvt);

    AppUtils_Printf(GATEWAYAPP_MSG_NORMAL, 
                "Number of 1722 Frames transmitted: %d\r\n",
                gGatewayApp.eth_send_count[0]);

    AppUtils_Printf(GATEWAYAPP_MSG_NORMAL, 
                "Number of CAN Frames Received : %d\r\n",
                canAppState.canQueue.numPduEnqueued);

    AppUtils_Printf(GATEWAYAPP_MSG_NORMAL, 
                "Number of CAN Frames Dropped due to overflow: %d\r\n",
                canAppState.canQueue.numOverflowEvt);

    AppUtils_Printf(GATEWAYAPP_MSG_NORMAL, 
                "Number of CAN Busy instances: %d\r\n",
                gGatewayApp.can_bus_busy);

    AppUtils_Printf(GATEWAYAPP_MSG_NORMAL, 
                "Number of CAN Bus Off errors: %d\r\n",
                busOff_error_cnt);

    AppUtils_Printf(GATEWAYAPP_MSG_NORMAL, 
                "Number of CAN Frames transmitted: %d\r\n",
                gGatewayApp.can_send_count[0] + \
                gGatewayApp.can_send_count[1]);

    /*Print Load numbers for application*/
    AppUtils_Printf(GATEWAYAPP_MSG_NORMAL, 
                "\nTotal CPU Load : %d%%\r\n",
                cpu_load);   

}

static void gatewayApp_updateCANQueue(void)
{
    uint8_t canPayload_length = \
    canAppState.canDescQueue.Can_Pdu[canAppState.canDescQueue.currentRdIndex].length;

    /*Update read pointer*/
    canAppState.canQueue.currentRdPtr += canPayload_length;
    /*Update count*/
    canAppState.canQueue.numPduDequeued++;
    /*Check wraparound and reset for descriptor Q*/
    canAppState.canDescQueue.currentRdIndex++;
}

void send_cpu_load(appProfileAvgLoadInfo *avgGatewayLoad)
{
    /*Encode the CPU load and send*/
    frame_1722_diagnostics->payload[42 - 14] = avgGatewayLoad->cpuLoad;
    frame_1722_diagnostics->payload[43 - 14] = avgGatewayLoad->isr;
    frame_1722_diagnostics->payload[44 - 14] = avgGatewayLoad->swi;

    #if ((STD_ON == ETH_ZERO_COPY_TX) && (STD_OFF == ETH_USE_Q_APIS))
        EthApp_sendNoCpy(gEthApp.ctrlIdx,
                  AVTP_ETH_TYPE,
                  (uint8_t *)frame_1722_diagnostics,
                  &frame_1722_diagnostics->hdr.dstMac[0U],
                   DEFAULT_1722_ETH_PKT_LEN - 8);
    #else
    EthApp_send(gEthApp.ctrlIdx, frame_1722_diagnostics, DEFAULT_1722_ETH_PKT_LEN - 8);
    #endif
}

