/*
*
* Copyright (c) 2019 Texas Instruments Incorporated
*
* All rights reserved not granted herein.
*
* Limited License.
*
* Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
* license under copyrights and patents it now or hereafter owns or controls to make,
* have made, use, import, offer to sell and sell ("Utilize") this software subject to the
* terms herein.  With respect to the foregoing patent license, such license is granted
* solely to the extent that any such patent is necessary to Utilize the software alone.
* The patent license shall not apply to any combinations which include this software,
* other than combinations with devices manufactured by or for TI ("TI Devices").
* No hardware patent is licensed hereunder.
*
* Redistributions must preserve existing copyright notices and reproduce this license
* (including the above copyright notice and the disclaimer and (if applicable) source
* code license limitations below) in the documentation and/or other materials provided
* with the distribution
*
* Redistribution and use in binary form, without modification, are permitted provided
* that the following conditions are met:
*
* *       No reverse engineering, decompilation, or disassembly of this software is
* permitted with respect to any software provided in binary form.
*
* *       any redistribution and use are licensed by TI for use only with TI Devices.
*
* *       Nothing shall obligate TI to provide you with source code for the software
* licensed and provided to you in object code.
*
* If software source code is provided to you, modification and redistribution of the
* source code are permitted provided that the following conditions are met:
*
* *       any redistribution and use of the source code, including any resulting derivative
* works, are licensed by TI for use only with TI Devices.
*
* *       any redistribution and use of any object code compiled from the source code
* and any resulting derivative works, are licensed by TI for use only with TI Devices.
*
* Neither the name of Texas Instruments Incorporated nor the names of its suppliers
*
* may be used to endorse or promote products derived from this software without
* specific prior written permission.
*
* DISCLAIMER.
*
* THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
* OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
* OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
* IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
* BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
* OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
* OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
* OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/**
 *  \file     eth_process.h
 *
 *  \brief    Header file for Ethernet Processing
 *
 */
/**
 * \defgroup MCUSS_APP_CAN_PROFILE Can profiling application

 * @{
 */
#ifndef ETH_PROCESS_H_
#define ETH_PROCESS_H_

/* ========================================================================== */
/*                             Include Files                                  */
/* ========================================================================== */
#if defined(SYSBIOS)
#include <xdc/std.h>
#include <xdc/runtime/System.h>
#include <xdc/runtime/Types.h>
#include <xdc/runtime/Timestamp.h>

#include <ti/sysbios/knl/Clock.h>
#include <ti/sysbios/knl/Task.h>
#include <ti/sysbios/knl/Semaphore.h>
#include <ti/sysbios/knl/Mailbox.h>
#include <ti/sysbios/hal/Hwi.h>
#endif //SYSBIOS

#include <ti/osal/osal.h>
#include <ti/osal/TaskP.h>
#include <ti/osal/EventP.h>
#include <ti/osal/TimerP.h>
#include <ti/osal/SemaphoreP.h>

#include <ti/csl/arch/csl_arch.h>
#include <ti/csl/soc.h>
#include <ti/csl/cslr.h>


#include "string.h"
#include "Std_Types.h"
#include "Det.h"
#include "Dem.h"
#include "Os.h"
#include "Can.h"
#include "CanIf_Cbk.h"
#include "EcuM_Cbk.h"
#include "Dio.h"

#include "EthUtils.h"
#include "Eth_GeneralTypes.h"
#include "Eth.h"
#include "Eth_Irq.h"
#include "EthTrcv.h"
#include "EthIf_Cbk.h"

#include "Cdd_Ipc.h"

#include "app_utils.h" /* MCAL Example utilities */
#include <utils/profile/include/app_profile.h>

#ifdef __cplusplus
extern "C"
{
#endif

/* ========================================================================== */
/*                           Macros & Typedefs                                */
/* ========================================================================== */
#define ETHAPP_MSG_NORMAL              (APP_UTILS_PRINT_MSG_NORMAL)
/**< Message type */
#define ETHAPP_MSG_STATUS              (APP_UTILS_PRINT_MSG_STATUS)
/**< Message type */
#define ETHAPP_MSG_APP_NAME            "Gateway App:"


#define NTSCF_SUBTYPE                          0x82
/**< Sub type for NTSCF frame*/

#define NTSCF_INSERT_HASH_ENTRY                0x83
/**< Special sub-type used to Add an entry to Hash table*/

#define NTSCF_ADD_MAC_ENTRY                    0x84
/**< Special sub-type used to add MAC entries*/

#define NTSCF_SUBTYPE_TRIGGER_MCU1_0_MSG_SEND  0x85
/**< Special sub-type used to trigger CAN messages from MCU 1_0*/

#define NTSCF_SUBTYPE_GET_MCU1_0_STATS         0x86
/**< Special sub-type used to get statistics from MCU 1_0*/

/** \brief Unlock value for lock kick 0 */
#define MMR_KICK0_UNLOCK_VAL            (0x68EF3490U)

/** \brief Unlock value for lock kick 1 */
#define MMR_KICK1_UNLOCK_VAL            (0xD172BC5AU)

/** \brief RGMII_ID_MODE shift value in CTRLMMR_MCU_ENET_CTRL register */
#define ENET_CTRL_RGMII_ID_SHIFT        (4U)

/** \brief No internal TX delay in RGMII interface (CTRLMMR_MCU_ENET_CTRL) */
#define ENET_CTRL_RGMII_ID_INTTXDLY     (0U)

/** \brief Internal TX delay in RGMII interface (CTRLMMR_MCU_ENET_CTRL) */
#define ENET_CTRL_RGMII_ID_NODELAY      (1U)

/** \brief RMII port interface (CTRLMMR_MCU_ENET_CTRL) */
#define ENET_CTRL_MODE_RMII             (1U)

/** \brief RGMII port interface (CTRLMMR_MCU_ENET_CTRL) */
#define ENET_CTRL_MODE_RGMII            (2U)

/** \brief Packet transmit retry in non-confirmation mode */
#define ETHAPP_TRANSMIT_RETRY_COUNT     (2U)

/** \brief Number of ethernet interfaces */
#define NUM_INTERFACES                  (1U)

#define PIN_PULL_DISABLE                (0x1U << 16U)
#define PIN_PULL_DIRECTION              (0x1U << 17U)
#define PIN_INPUT_ENABLE                (0x1U << 18U)
#define PIN_OUTPUT_DISABLE              (0x1U << 21U)
#define PIN_WAKEUP_ENABLE               (0x1U << 29U)
#define PIN_INPUT                       (PIN_PULL_DISABLE | PIN_INPUT_ENABLE)
#define PIN_OUTPUT                      (PIN_PULL_DISABLE)

/** \Number of Eth frames in the queue*/
#define ETH_NUM_PKT_IN_Q                (512U)

/** \Min bytes in a 1722 frame excluding CRC and Header*/
#define MIN_PKT_SIZE                    (28U)

/** \Max bytes in a 1722 frame excluding CRC and Header*/
#define MAX_PKT_SIZE                    (200U)

/** \Size of ethernet queue*/
#define ETH_Q_SIZE                      (ETH_NUM_PKT_IN_Q * MAX_PKT_SIZE)

/** \Eth type for AVTP (1722) */
#define AVTP_ETH_TYPE                   (0x22F0)

#define ETHAPP_REMOTEETHDEVICE_COMCHID  (CddIpcConf_IpcComChanId_Cdd_IpcMcu20_EthDevice)

#if defined (SOC_AM65XX)
#define PRAGMA(x) _Pragma(#x)
#ifdef __cplusplus
#define DATAPRAGMA(f,s) PRAGMA(DATA_SECTION(s))
#else
#define DATAPRAGMA(f,s) PRAGMA(DATA_SECTION(f, s))
#endif
#endif /* (SOC_AM65XX) */

/** \brief Ethernet test statistics (not to be confused with Ethernet statistics) */
typedef struct EthApp_Stats_s
{
    volatile uint32 txPktCnt;
    /**< Number of data transmitted packets (Eth_Transmit()) */

    volatile uint32 txConfPktCnt;
    /**< Number of confirmed transmit packets (Eth_TxConfirmation()) */

    volatile uint32 txBufUndrflw;
    volatile uint32 txProvideError;
    /**< Number of buffer requests with BUFREQ_E_BUSY error (Eth_ProvideTxBuffer()) */

    volatile uint32 txBufLenErr;
    /**< Number of buffer requests with BUFREQ_E_OVFL error (Eth_ProvideTxBuffer()) */

    volatile uint32 txBufProvErr;
    /**< Number of buffer requests with BUFREQ_E_NOT_OK error (Eth_ProvideTxBuffer()) */

    volatile uint32 txPktErr;
    /**< Number of data transmit errors (Eth_Transmit()) */

    volatile uint32 rxPktCnt;
    /**< Number of data received packets (Eth_Receive() or EthIf_RxIndication()) */

    volatile uint32 rxExp1PktCnt;
    /**< Number of data received packets with experimental 1 type (EthIf_RxIndication()) */

    volatile uint32 rxExp2PktCnt;
    /**< Number of data received packets with experimental 2 type (EthIf_RxIndication()) */

    volatile uint32 rxVlanPktCnt;
    /**< Number of data received packets with VLAN tag (EthIf_RxIndication()) */

    volatile uint32 rxPktErr;
    /**< Number of receive errors (Eth_Receive()) */

    volatile uint32 rxPktIndCnt;
    /**< Number of indicated receive packets (EthIf_RxIndication()) */

    volatile uint32 rxEtherTypeErr;
    /**< Number of packets with an unexpected EtherType field (EthIf_RxIndication()) */

    volatile uint32 rxPayloadErr;
    /**< Number of packets with unexpected payload (EthIf_RxIndication()) */

    volatile uint32 rxCtrlIdxErr;
    /**< Number of packets with unexpected controller index (EthIf_RxIndication()) */

    volatile uint32 rxBcastErr;
    /**< Number of packets with unexpected broadcast flag */

    volatile uint32 rxLenErr;
    /**< Number of packets with invalid buffer length */

    volatile uint32 ctrlModeActCnt;
    /**< Number of controller mode changes to ETH_MODE_ACTIVE (Eth_SetControllerMode()) */

    volatile uint32 ctrlModeDownCnt;
    /**< Number of controller mode changes to ETH_MODE_ACTIVE (Eth_SetControllerMode()) */

    volatile uint32 ctrlModeErr;
    /**< Number of controller mode changes with error (Eth_SetControllerMode()) */

    volatile uint32 ctrlModeIndActCnt;
    /**< Number of controller mode changes to ETH_MODE_ACTIVE (EthIf_CtrlModeIndication()) */

    volatile uint32 ctrlModeIndDownCnt;
    /**< Number of controller mode changes to ETH_MODE_DOWN (EthIf_CtrlModeIndication()) */

    volatile uint32 ctrlModeIndErr;
    /**< Invalid controller mode errors (EthIf_CtrlModeIndication()) */

    volatile uint32 filterUcastAddErr;
    /** Number of filter "add" action errors (Eth_UpdatePhysAddrFilter()) */

    volatile uint32 filterUcastRemErr;
    /** Number of filter "add" action (unicast) errors (Eth_UpdatePhysAddrFilter()) */

    volatile uint32 filterUcastRxErr;
    /** Number of errors while receiving unicast packets */

    volatile uint32 filterMcastAddErr;
    /** Number of filter "remove" action (multicast) errors (Eth_UpdatePhysAddrFilter()) */

    volatile uint32 filterMcastRemErr;
    /** Number of filter "remove" action (multicast) errors (Eth_UpdatePhysAddrFilter()) */

    volatile uint32 filterMcastRxErr;
    /** Number of errors while receiving multicast packets */

    volatile uint32 filterBcastErr;
    /** Number of filter "add" action (broadcast) errors (Eth_UpdatePhysAddrFilter()) */

    volatile uint32 filterBcastRxErr;
    /** Number of errors while receiving broadcast packets */

    volatile uint32 filterBcastTxErr;
    /** Number of errors while transmitting broadcast packets */

    volatile uint32 filterNullErr;
    /** Number of filter "add" action (null) errors (Eth_UpdatePhysAddrFilter()) */

    volatile uint32 filterNullRxErr;
    /** Number of errors while receiving unicast packets */

    volatile uint32 etherStatsErr;
    /**< Ethernet statistics errors (Eth_GetEtherStats()) */

    volatile uint32 etherDropCountErr;
    /**< Ethernet statistics errors (Eth_GetEtherStats()) */

    volatile uint32 demErr;
    /**< DEM errors (Dem_ReportErrorStatus()) */

    volatile uint32 detErr;
    /**< DET errors (Det_ReportError()) */
} EthApp_Stats;

/** \brief Ethernet Queue structure */
typedef struct EthApp_Queue_s
{
    uint8_t *currentRdPtr;
    /**< Consumer task fetches from this index */

    uint8_t *currentWrPtr;
    /**< Producer task writes to this index */

    uint8_t *topOfQueue;
    /**< Top of queue */

    uint8_t *bottomOfQueue;
    /**< Bottom of queue */

    uint64_t numPktEnqueued;
    /**< Number of bytes written to buffer by Producer */

    uint64_t numPktDequeued;
    /**< Number of bytes consumed from buffer by Consumer*/

    uint64_t numOverflowEvt;
    /**< Number of overflow events*/
    
} EthApp_Queue;

/** \brief Ethernet Descriptor Queue structure */
typedef struct EthApp_DescQueue_s
{
    uint16_t pktLen[ETH_NUM_PKT_IN_Q];
    /**< Descriptor only contains Packet length right now */

    uint64_t rxTS[ETH_NUM_PKT_IN_Q];
    /**< Time at which the packet was received */

    uint8_t canID[ETH_NUM_PKT_IN_Q];
    /**< Can ID embedded in the packet 
        This design is not suitable for multiple Can frames in one Eth*/

    uint8_t currentRdIndex;
    /**< Consumer task writes to this index */

    uint8_t currentWrIndex;
    /**< Producer task writes to this index */
    
} EthApp_DescQ;

/** \brief Ethernet test state */
typedef struct EthApp_State_s
{
    uint8 ctrlIdx;
    /**< Controller index */

    bool  linkUp[NUM_INTERFACES];
    /**< Information on Ethernet interfaces */

    EthApp_Queue ethQueue;
    /**< Main queue structure */

    EthApp_DescQ ethDescQueue;
    /**< Descriptor queue structure */

    EthApp_Stats stats;
    /**< Test statistics (not to be confused with Ethernet statistics) */

    boolean verbose;
    /**< Enable verbose */

    boolean veryVerbose;
    /**< Enable very verbose logs */

    volatile boolean runTest;
    /**< Used to terminate application */

    volatile boolean showStats;
    /**< Used to display statistics */

    Enet_Handle hCpsw;
    /**< Handle to Master Core Cpsw Handle */
    uint32_t coreKey;
    /**< Core key returned by ATTACH */
    uint8_t macAddr[CPSW_MAC_ADDR_LEN];
    /**< Mac address allocated to AUTOSAR core */
    ClockP_Handle ethRxTxClock;
    /**< Clock Object used for scheduling PkTTxRx if interrupts not used */
    ClockP_Handle ethMainClock;
    /**< Clock Object used to invoke  Eth_MainFunction() */
    SemaphoreP_Handle semRxHandle;
    /**< Semaphore Object used for scheduling PkTTxRx if interrupts not used */
    SemaphoreP_Handle semTxReclaimObj;
    /**< Semaphore Object used for scheduling PkTTxRx if interrupts not used */
    volatile Bool exitPktRxTask;
    /**< Flag indicating pkt processing task should be exited */
    volatile Bool exitPktTxReclaimTask;
    /**< Flag indicating pkt processing task should be exited */
    MailboxP_Handle pktTxSendMbx;
    /**< Mailbox into which Tx packets to be sent are queued */
    MailboxP_Handle pktTxFreeMbx;
    /**< Mailbox into which free Tx packets are queued */
    TaskP_Handle taskRxPkt;
    /**< Task Object used for scheduling PkTTxRx if interrupts not used */
    TaskP_Handle taskTxPkt;
    /**< Task Object used for scheduling packet trasmit if interrupts not used */
    TaskP_Handle taskTxReclaim;
    /**< Task Object used for scheduling reclaim of transmit complete buffers */
} EthApp_State;

/** \brief Ethernet Tx packet Info queued for transmission */
typedef struct EthApp_TxSendPktInfo_s
{
    Eth_BufIdxType bufIdx;
    /**< Buffer index of driver allocated buffer to be transmitted */
    Eth_FrameType  frameType;
    /**< EtherType of the frame */
    uint8_t destMac[ETH_MAC_ADDR_LEN];
    /**< Destination MAC address of the frame */
    uint16_t len;
    /**< Length of the frame */
    Bool exitTxTask;
    /**< Msg indicating Tx task should be exited */
} EthApp_TxSendPktInfo;

/** \brief Ethernet Free Tx packet Info */
typedef struct EthApp_TxFreePktInfo_s
{
    Eth_BufIdxType bufIdx;
    /**< Buffer index of driver free allocated buffer */
    uint8 *bufPtr;
    /**< Pointer to free buffer */
    uint16_t len;
    /**< Length of the frame */
} EthApp_TxFreePktInfo;

/* ========================================================================== */
/*                          Function Declarations                             */
/* ========================================================================== */
void EthApp_Startup(void);

/* Main Eth / EthTrcv initialization/deinitialization functions */
Std_ReturnType EthApp_init(uint8 ctrlIdx);

Std_ReturnType EthApp_deinit(uint8 ctrlIdx);
#if ((STD_ON == ETH_ZERO_COPY_TX) && (STD_OFF == ETH_USE_Q_APIS))
void EthApp_sendNoCpy(uint8 ctrlIdx,
                      uint16_t etherType,
                      uint8_t *payload,
                      uint8_t *dstMac,
                      uint16 len);
#endif
void EthApp_send(uint8 ctrlIdx,
                        Eth_Frame *frame,
                        uint16 len);

void EthApp_addUnicastAddr(uint8 *macAddr, Eth_PortType port);

void EthApp_sendProfileInfo(appProfileAvgLoadInfo *avgProfileInfo);

void EthApp_queueTx(uint8 ctrlIdx,
                    Eth_Frame *frame,
                    uint16 len);
void EthApp_recvMsgNotify();

extern Bool ethAppProcessRx;

void EthApp_rpcCmdComplete (uint32 controllerIdx,
                            uint8 sid,
                            Std_ReturnType status);

void EthApp_rpcFwRegistered (uint32 controllerIdx);

#ifdef __cplusplus
}
#endif

#endif /*ETH_PROCESS_H_*/
