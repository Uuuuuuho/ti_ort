/*
*
* Copyright (c) 2019 Texas Instruments Incorporated
*
* All rights reserved not granted herein.
*
* Limited License.
*
* Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
* license under copyrights and patents it now or hereafter owns or controls to make,
* have made, use, import, offer to sell and sell ("Utilize") this software subject to the
* terms herein.  With respect to the foregoing patent license, such license is granted
* solely to the extent that any such patent is necessary to Utilize the software alone.
* The patent license shall not apply to any combinations which include this software,
* other than combinations with devices manufactured by or for TI ("TI Devices").
* No hardware patent is licensed hereunder.
*
* Redistributions must preserve existing copyright notices and reproduce this license
* (including the above copyright notice and the disclaimer and (if applicable) source
* code license limitations below) in the documentation and/or other materials provided
* with the distribution
*
* Redistribution and use in binary form, without modification, are permitted provided
* that the following conditions are met:
*
* *       No reverse engineering, decompilation, or disassembly of this software is
* permitted with respect to any software provided in binary form.
*
* *       any redistribution and use are licensed by TI for use only with TI Devices.
*
* *       Nothing shall obligate TI to provide you with source code for the software
* licensed and provided to you in object code.
*
* If software source code is provided to you, modification and redistribution of the
* source code are permitted provided that the following conditions are met:
*
* *       any redistribution and use of the source code, including any resulting derivative
* works, are licensed by TI for use only with TI Devices.
*
* *       any redistribution and use of any object code compiled from the source code
* and any resulting derivative works, are licensed by TI for use only with TI Devices.
*
* Neither the name of Texas Instruments Incorporated nor the names of its suppliers
*
* may be used to endorse or promote products derived from this software without
* specific prior written permission.
*
* DISCLAIMER.
*
* THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
* OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
* OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
* IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
* BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
* OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
* OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
* OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/**
 *  \file main_tirtos.c
 *
 *  \brief Main file for TI-RTOS CAN-Ethernet gateway demo application
 */

/* ========================================================================== */
/*                             Include Files                                  */
/* ========================================================================== */
#include <assert.h>

#include <ti/osal/osal.h>
#include <ti/osal/TaskP.h>
#include <ti/osal/EventP.h>
#include <ti/osal/SemaphoreP.h>

#include "CanApp_Priv.h"
#include "gateway_demo.h"
#include <utils/profile/include/app_profile.h>
#include "CanIf.h"

#include <ti/drv/ipc/ipc.h>
#include <ti/drv/ipc/soc/ipc_soc.h>
#include <ti/csl/cslr.h>
#include <ti/csl/csl_mailbox.h>

#include "app_ipc_rsctable.h"

#include "Std_Types.h"
#include "Det.h"
#include "Dem.h"
#include "Os.h"
#include "Can.h"
#include "CanIf_Cbk.h"
#include "EcuM_Cbk.h"
#include "Dio.h"

#include "EthUtils.h"
#include "Eth_GeneralTypes.h"
#include "Eth.h"
#include "Eth_Irq.h"
#include "EthTrcv.h"
#include "EthIf_Cbk.h"
#include "EthIf_Cbk.h"
#include "Cdd_Ipc.h"
#include "Cdd_IpcIrq.h"
#include <ti/drv/enet/enet.h>
#include <ti/drv/enet/include/core/enet_soc.h>

/* ========================================================================== */
/*                           Macros & Typedefs                                */
/* ========================================================================== */

/* Test application stack size */
#define CAN_APP_TASK_STACK               (10U * 1024U)
/**< Stack required for the stack */
#define CAN_APP_DEMO_TASK_NAME           ("CAN RECEIVE")
/**< Task name */

/* Test application stack size */
#define ETH_APP_TASK_STACK               (10U * 1024U)
/**< Stack required for the stack */
#define ETH_APP_DEMO_TASK_NAME           ("ETH RECEIVE")
/**< Task name */

/* Test application stack size */
#define GATEWAY_APP_TASK_STACK           (10U * 1024U)
/**< Stack required for the stack */
#define GATEWAY_APP_DEMO_TASK_NAME       ("GATEWAY DEMO")
/**< Task name */

#define IPC_APP_TASK_NAME       ("IPC_INIT_TASK")
/**< Task name */
#if defined (SOC_J721E)
#define IPC_VRING_MEM_SIZE_GATEWAY (0x02000000u)
#elif defined (SOC_J7200)
#define IPC_VRING_MEM_SIZE_GATEWAY (0x00800000u)
#endif

#define RPMSG_VDEV_MONITOR_TASK_NAME  ("RPMSG_VDEV_MONITOR_TASK")

#define AUTOSAR_REMOTEETHDEVICE_COMCHID  (CddIpcConf_IpcComChanId_Cdd_IpcMcu20_EthDevice)
#define IPC_RPMESSAGE_OBJ_SIZE  (256)
#define VQ_TIMEOUT              (100)
#define VQ_BUF_SIZE             (2048)
#define RPMSG_DATA_SIZE         (256 * 512 + IPC_RPMESSAGE_OBJ_SIZE)

#define UTILS_ARRAYSIZE(x) sizeof(x)/sizeof (x[0U])
#define UTILS_ALIGN(x,align)  ((((x) + ((align) - 1))/(align)) * (align))

#define GATEWAYAPP_IPC_TASK_STACKSIZE   ((IPC_TASK_STACKSIZE) * 4U)

/* ========================================================================== */
/*                         Structure Declarations                             */
/* ========================================================================== */


/* ========================================================================== */
/*                          Function Declarations                             */
/* ========================================================================== */
static void GatewayApp_TaskFxn(void* a0, void* a1);
static void CanApp_TaskFxn(void* a0, void* a1);
static void EthApp_TaskFxn(void* a0, void* a1);

static void GatewayApp_TimerCb(void);

static void ipc_cdd_init(void* a0,
                         void* a1);
static void rpmsg_vdevMonitorFxn(void* arg0,
                                 void* arg1);
static void GatewayApp_IpcStartup(void);
void GatewayAppMsgFromMcu20Isr(uintptr_t notUsed);
static void GatewayApp_PrintVersion(void);

/* ========================================================================== */
/*                            Global Variables                                */
/* ========================================================================== */
GatewayApp_State gGatewayApp;
/**< Gateway application state */

volatile uint64_t DMTimerfreq;
/*Frequency of DM Timer used to do profiling*/

static uint8_t GatewayApp_TaskStack[GATEWAY_APP_TASK_STACK] __attribute__((aligned(32)));
/**< Stack for the task */

SemaphoreP_Handle gateway_semHandle;
/**< Semaphore handle used to trigger routing task */
TimerP_Handle GatewayApp_timerHandle;
/**< Timer handle to trigger CAN Polling events */
#if (STD_ON == GATEWAY_CAN_POLLING_TASK)
SemaphoreP_Handle canTask_semHandle;
/**< Semaphore handle used to trigger CAN polling task */
#endif //CAN_POLLING_TASK
/**************************CAN Global variables********************/
/* application stack */
static uint8_t CanApp_TaskStack[CAN_APP_TASK_STACK] __attribute__((aligned(32)));
/**< Stack for the task */
uint8_t can_init_success = 0;
/**< Used to check if CAN has initialized  */
/**************************Eth Global variables********************/
/* application stack */
static uint8_t EthApp_TaskStack[ETH_APP_TASK_STACK] __attribute__((aligned(32)));
/**< Stack for the task */
uint8_t eth_init_success = 0;
/**< Used to check if Eth has initialized  */

TaskP_Handle taskCAN;
/**< Handle for CAN task */
TaskP_Handle taskEth;
/**< Handle for Eth task */
TaskP_Handle taskGateway;
/**< Handle for Gateway task */
TaskP_Handle taskIPC;
/**< Handle for IPC task */

char Ipc_traceBuffer[IPC_TRACE_BUFFER_MAX_SIZE];

static uint8_t g_monitorStackBuf[GATEWAYAPP_IPC_TASK_STACKSIZE]
__attribute__ ((section(".bss:taskStackSection")))
__attribute__ ((aligned(8192)))
;

static uint8_t g_rdevStackBuf[GATEWAYAPP_IPC_TASK_STACKSIZE]
__attribute__ ((section(".bss:taskStackSection")))
__attribute__ ((aligned(8192)))
;

static uint8_t g_ipcStackBuf[GATEWAYAPP_IPC_TASK_STACKSIZE]
__attribute__ ((section(".bss:taskStackSection")))
__attribute__ ((aligned(8192)))
;

static uint8_t g_vdevMonStackBuf[GATEWAYAPP_IPC_TASK_STACKSIZE]
__attribute__ ((section(".bss:taskStackSection")))
__attribute__ ((aligned(8192)))
;

static uint8_t g_mainStackBuf[GATEWAYAPP_IPC_TASK_STACKSIZE]
__attribute__ ((section(".bss:taskStackSection")))
__attribute__ ((aligned(8192)))
;

static uint8_t ctrlTaskBuf[GATEWAYAPP_IPC_TASK_STACKSIZE]
__attribute__ ((section(".bss:taskStackSection")))
__attribute__ ((aligned(8192)))
;

static uint8_t g_messageTaskStack[GATEWAYAPP_IPC_TASK_STACKSIZE]
__attribute__ ((section(".bss:taskStackSection")))
__attribute__ ((aligned(8192)))
;

static uint8_t g_requestTaskStack[GATEWAYAPP_IPC_TASK_STACKSIZE]
__attribute__ ((section(".bss:taskStackSection")))
__attribute__ ((aligned(8192)))
;

static uint8_t sysVqBuf[VQ_BUF_SIZE]  __attribute__ ((section("ipc_data_buffer"), aligned(8)));
static uint8_t gCntrlBuf[RPMSG_DATA_SIZE] __attribute__ ((section("ipc_data_buffer"), aligned(8)));

static uint8_t g_vringMemBuf[IPC_VRING_MEM_SIZE_GATEWAY] __attribute__ ((section(".bss:ipc_vring_mem"), aligned(8192)));

static uint32_t selfProcId = IPC_MCU2_1;
static uint32_t gRemoteProc[] =
#if defined (SOC_J721E)
{
    IPC_MPU1_0, IPC_MCU1_0, IPC_MCU1_1, IPC_MCU2_0, IPC_MCU3_0, IPC_MCU3_1, IPC_C66X_1, IPC_C66X_2, IPC_C7X_1
};
#elif defined (SOC_J7200)
{
    IPC_MPU1_0, IPC_MCU1_0, IPC_MCU1_1, IPC_MCU2_0,
};
#endif
static uint32_t gNumRemoteProc = sizeof(gRemoteProc) / sizeof(uint32_t);

/**< Handle for Gateway task */
/* ========================================================================== */
/*                            External Variables                              */
/* ========================================================================== */
extern uint32 CanIf_DrvStatus;
/**< CAN IF Driver Status, defined in CanIf.c */
extern EthApp_State gEthApp;
/* ========================================================================== */
/*                          Function Definitions                              */
/* ========================================================================== */

int main(void)
{
    TaskP_Params taskParams;

    /* Run Startup for CAN and Ethernet and Gateway*/
    CanApp_Startup();

    EthApp_Startup();

    if ((CANIF_DRV_INITIALIZED == CanIf_DrvStatus))
    {
        TaskP_Params ipc_taskParams;

        TaskP_Params_init(&ipc_taskParams);
        ipc_taskParams.priority = 2;
        ipc_taskParams.stack = &g_ipcStackBuf[0];
        ipc_taskParams.stacksize = sizeof(g_ipcStackBuf);
        ipc_taskParams.name = IPC_APP_TASK_NAME;
        taskIPC = TaskP_create(ipc_cdd_init, &ipc_taskParams);
        if(NULL == taskIPC)
        {
            /*Failed to create task*/
            OS_stop();
        }

        /* Initialize the task params */
        TaskP_Params_init(&taskParams);
        taskParams.name = CAN_APP_DEMO_TASK_NAME;
        /* Set the task priority higher than the default priority (1) */
        taskParams.priority     = 10;
        taskParams.stack        = CanApp_TaskStack;
        taskParams.stacksize    = sizeof (CanApp_TaskStack);

        taskCAN = TaskP_create(CanApp_TaskFxn, &taskParams);
        if(NULL == taskCAN)
        {
            /*Failed to create task*/
            OS_stop();
        }

        /*Create Timer*/
        TimerP_Params timerParams;

        TimerP_Params_init(&timerParams);

        timerParams.startMode = TimerP_StartMode_USER;
        timerParams.periodType = TimerP_PeriodType_MICROSECS;
        timerParams.period = GATEWAY_TIMER_INTERVAL;/*usecs*/

        GatewayApp_timerHandle = TimerP_create(TimerP_ANY, (TimerP_Fxn)GatewayApp_TimerCb,
                                    &timerParams);
        if (NULL == GatewayApp_timerHandle)
        {
            /*Failed to create timer*/
            AppUtils_Printf(GATEWAYAPP_MSG_NORMAL, GATEWAYAPP_MSG_APP_NAME
                "Failed to create gateway router timer\n");
            OS_stop();
        }

        /*Create Semaphore*/
        SemaphoreP_Params semaphoreParams;
        SemaphoreP_Params_init(&semaphoreParams);
        semaphoreParams.mode = SemaphoreP_Mode_BINARY;

        gateway_semHandle = SemaphoreP_create(0, &semaphoreParams);

        if (NULL == gateway_semHandle)
        {
            /*Failed to create timer*/
            AppUtils_Printf(GATEWAYAPP_MSG_NORMAL, GATEWAYAPP_MSG_APP_NAME
                " Failed to create Semaphore\n");
            OS_stop();
        }

#if (STD_ON == GATEWAY_CAN_POLLING_TASK) && (CAN_RX_POLLING == STD_ON)
        SemaphoreP_Params_init(&semaphoreParams);
        semaphoreParams.mode = SemaphoreP_Mode_BINARY;

        canTask_semHandle = SemaphoreP_create(0, &semaphoreParams);

        if (NULL == canTask_semHandle)
        {
            /*Failed to create timer*/
            AppUtils_Printf(GATEWAYAPP_MSG_NORMAL, GATEWAYAPP_MSG_APP_NAME
                " Failed to create Semaphore\n");
            OS_stop();
        }
#endif //CAN_POLLING_TASK

        /*Enable the test*/
        gGatewayApp.runTest = 1;
        gEthApp.runTest = 1;
#if defined(SYSBIOS)
        Types_FreqHz bios_freq;
        /*Get DM Timer frequency*/
        Timestamp_getFreq(&bios_freq);

        DMTimerfreq = ((uint64_t) bios_freq.hi << 32) | bios_freq.lo; /* in units of HZ */
        DMTimerfreq = DMTimerfreq / 1000000u; /* in units of MHZ */
#endif

        OS_start();    /* does not return */
    }
    else
    {

        if (CANIF_DRV_INITIALIZED != CanIf_DrvStatus)
        {
            AppUtils_Printf(GATEWAYAPP_MSG_NORMAL, GATEWAYAPP_MSG_APP_NAME
                " Error : CAN IF not initialized !!!\n");
        }

    }

    return(0);
}

static void GatewayApp_TimerCb(void)
{
    static uint32_t timerTickCnt = 0U;

#if (STD_ON == GATEWAY_CAN_POLLING_TASK) && (CAN_RX_POLLING == STD_ON)
    static uint32_t routerInterval = ( GATEWAY_ROUTER_PERIOD/GATEWAY_TIMER_INTERVAL);
    if (timerTickCnt%routerInterval == 0U)
{
    /*Trigger Gateway task*/
    SemaphoreP_post(canTask_semHandle);
    }
#endif

#if (STD_OFF == ETH_ENABLE_RX_INTERRUPT)
    static uint32_t ethRxPktTaskInterval = ( GATEWAY_ETH_PKTRX_TASK_PERIOD/GATEWAY_TIMER_INTERVAL);
    if (timerTickCnt%ethRxPktTaskInterval == 0U)
    {
        SemaphoreP_post(gEthApp.semRxHandle);
    }
#endif

    timerTickCnt++;
    return;
}

static void CanApp_TaskFxn(void* a0, void* a1)
{
    /*Adding delay to synchronize with EthFW*/
    TaskP_sleep(2000);

    CanApp_PlatformInit();
#if defined (SYSBIOS)
    Utils_prfInit();

    Utils_prfLoadRegister (TaskP_self(), CAN_APP_DEMO_TASK_NAME);
#endif 

    TaskP_yield();

    AppUtils_Printf(GATEWAYAPP_MSG_NORMAL, GATEWAYAPP_MSG_APP_NAME
                " Starting CAN Rx\n");

    /*Initialize MCAN*/
    if(CanApp_Init() != E_OK)
    {
        gGatewayApp.runTest = 0;
        AppUtils_Printf(GATEWAYAPP_MSG_NORMAL, GATEWAYAPP_MSG_APP_NAME
                " Unable to start CAN Rx Interface\n");
    }
    else
    {
        AppUtils_Printf(GATEWAYAPP_MSG_NORMAL, GATEWAYAPP_MSG_APP_NAME
                " Starting CAN Rx Interface\n");
        can_init_success = 1;

    }

    /*Main loop which receives Ethernet frames*/
    while(gGatewayApp.runTest)
    {
        CanApp_RxTask();
    }

    /*De-initialize MCAN*/
    CanApp_DeInit();

    CanApp_Shutdown();
#if defined (SYSBIOS)
    Utils_prfLoadUnRegister (TaskP_self());
    Utils_prfDeInit();
#endif 

    return;
}
#if defined (SYSBIOS)
static uint32_t GatewayApp_getPacketProcessedCount(void)
{
    static uint32_t pktRxCount = 0;
    static uint32_t pktTxCount = 0;
    uint32_t rxProcessCount, txProcessCount;

    rxProcessCount = gEthApp.stats.rxPktIndCnt - pktRxCount;
    pktRxCount = gEthApp.stats.rxPktIndCnt;

    txProcessCount = gGatewayApp.eth_send_count[0] - pktTxCount;
    pktTxCount = gGatewayApp.eth_send_count[0];

    return (rxProcessCount + txProcessCount);
}
#endif 

static void EthApp_TaskFxn(void* a0, void* a1)
{
    Std_ReturnType retVal;

#if defined (SYSBIOS)
    uint32_t num_ticks = 0;
    Utils_prfInit();

    Utils_prfLoadRegister (TaskP_self(), ETH_APP_DEMO_TASK_NAME);
#endif

    TaskP_yield();

    /*Adding delay to synchronize with EthFW*/
    TaskP_sleep(2000);

    /* Initialize the Eth driver and controller */
    if(EthApp_init(gEthApp.ctrlIdx) != E_OK)
    {
        gGatewayApp.runTest = 0;
        AppUtils_Printf(GATEWAYAPP_MSG_NORMAL, GATEWAYAPP_MSG_APP_NAME
                " Unable to start Eth Rx Interface. Please check the link\n");
    }
    else
    {
        AppUtils_Printf(GATEWAYAPP_MSG_NORMAL, GATEWAYAPP_MSG_APP_NAME
                " Starting Eth Rx Interface\n");
        eth_init_success = 1;
    }
#if defined(SYSBIOS)
    appProfileConfig profileConfig;
    appProfileAvgLoadInfo avgLoadGateway;

    profileConfig.configPmu = true;
    profileConfig.enablePeriodicProfilePrint = false;
    profileConfig.profilePrintPeriodMs = 10000;
    profileConfig.printFxn = (AppProfilePrintFxn) &System_printf;

    profileConfig.getPacketCountFxn = &GatewayApp_getPacketProcessedCount;
    app_profileInit(&profileConfig);
    app_profileStart();
#endif 
    /*Main loop which receives Ethernet frames*/
    while(gGatewayApp.runTest)
    {
        /*Since packets are received are in interrupt context,
        nothing to do*/
        TaskP_sleep(10);
#if defined (SYSBIOS)
        /*Send it to PC Tools every 50ms*/
        if ((num_ticks % 200) == 0)
        {
            /*Send CPU load stats and other stats over ethernet*/
            app_profileGetAvgLoad((2000) , &avgLoadGateway);
            send_cpu_load(&avgLoadGateway);
        }

        num_ticks++;
#endif 
    }

    /* Deinitialize the Ethernet controller */
    retVal = EthApp_deinit(gEthApp.ctrlIdx);
    if (E_OK != retVal)
    {
        AppUtils_Printf(GATEWAYAPP_MSG_NORMAL, GATEWAYAPP_MSG_APP_NAME "Failed to deinitialize: %d\n", retVal);
    }
#if defined (SYSBIOS)
    Utils_prfLoadUnRegister (TaskP_self());
    Utils_prfDeInit();
#endif 
    return;
}

static void GatewayApp_TaskFxn(void* a0, void* a1)
{
    /*Adding delay to synchronize with EthFW*/
    TaskP_sleep(2000);
#if defined (SYSBIOS)
    Utils_prfInit();

    Utils_prfLoadRegister (TaskP_self(), GATEWAY_APP_DEMO_TASK_NAME);
#endif

    TaskP_yield();

    /*Don't start until both CAN and Eth are up*/
    while(!(can_init_success && eth_init_success))
    {
        TaskP_sleep(1);
    }

    if(GatewayApp_Init() != TRUE)
    {
        gGatewayApp.runTest = 0;
        AppUtils_Printf(GATEWAYAPP_MSG_NORMAL, GATEWAYAPP_MSG_APP_NAME
                " Unable to allocate memory for 1722 Frame\n");
    }
    else
    {
        AppUtils_Printf(GATEWAYAPP_MSG_NORMAL, GATEWAYAPP_MSG_APP_NAME
                " Starting Gateway Router\n");
    }

    while(gGatewayApp.runTest)
    {
        /*Allows remote termination of application*/
        gGatewayApp.runTest = gEthApp.runTest;
        gatewayApp_Router();
    }

    AppUtils_Printf(GATEWAYAPP_MSG_NORMAL, GATEWAYAPP_MSG_APP_NAME
                " Stopping Gateway Router\n");

    gatewayApp_Shutdown();
#if defined (SYSBIOS)
    Utils_prfLoadUnRegister (TaskP_self());
    Utils_prfDeInit();
#endif

    /*Gracefully exit for profiling*/
    TaskP_sleep(1000);

    AppUtils_Printf(GATEWAYAPP_MSG_NORMAL, GATEWAYAPP_MSG_APP_NAME
                " Shutting down application\n");

    return;
}

static void GatewayApp_initIpcCddCfg(struct Cdd_IpcConfigType_s *ipcCddCfg)
{
    uint32_t numProc = gNumRemoteProc;
    uint32_t i;

    ipcCddCfg->coreIds.numProcs = gNumRemoteProc;
    ipcCddCfg->coreIds.ownProcID = selfProcId;
    for (i = 0; i < UTILS_ARRAYSIZE(gRemoteProc); i++)
    {
        ipcCddCfg->coreIds.remoteProcID[i] = gRemoteProc[i];
    }
    ipcCddCfg->vertIoCfg.vertIoRingAddr = (void *)g_vringMemBuf;
    ipcCddCfg->vertIoCfg.vertIoRingSize = sizeof(g_vringMemBuf);
    ipcCddCfg->vertIoCfg.vertIoObjSize = numProc * Ipc_getVqObjMemoryRequiredPerCore();
}

static void ipc_cdd_init(void* a0,
                         void* a1)
{
    TaskP_Params params;
    struct Cdd_IpcConfigType_s *ipcCddCfg;

    /*Adding delay to synchronize with EthFW*/
    TaskP_sleep(2000);

    /* Initialize memory sections  */
    GatewayApp_IpcStartup();

    ipcCddCfg = (struct Cdd_IpcConfigType_s *)&CddIpcConfiguraions_PC;
    GatewayApp_initIpcCddCfg(ipcCddCfg);

    Cdd_IpcInit();

    Ipc_loadResourceTable(appGetIpcResourceTable());

    TaskP_Params_init(&params);
    params.priority = 3;
    params.stack = &g_vdevMonStackBuf[0];
    params.stacksize = sizeof(g_vdevMonStackBuf);
    params.name = RPMSG_VDEV_MONITOR_TASK_NAME;
    TaskP_create(rpmsg_vdevMonitorFxn, &params);

    /* Initialize the task params */
    TaskP_Params_init(&params);
    params.name = ETH_APP_DEMO_TASK_NAME;
    /* Set the task priority higher than the default priority (1) */
    params.priority     = 14;
    params.stack        = EthApp_TaskStack;
    params.stacksize    = sizeof (EthApp_TaskStack);

    taskEth = TaskP_create(EthApp_TaskFxn, &params);
    assert(NULL != taskEth);

    /* Initialize the task params */
    TaskP_Params_init(&params);
    params.name = GATEWAY_APP_DEMO_TASK_NAME;
    /* Set the task priority higher than the default priority (1) */
    params.priority     = 13;
    params.stack        = GatewayApp_TaskStack;
    params.stacksize    = sizeof (GatewayApp_TaskStack);

    taskGateway = TaskP_create(GatewayApp_TaskFxn, &params);
    assert(NULL != taskGateway);

}


static void rpmsg_vdevMonitorFxn(void* arg0,
                                 void* arg1)
{
    int32_t status;

    /* Wait for Linux VDev ready... */
    while (!Ipc_isRemoteReady(IPC_MPU1_0))
    {
        TaskP_sleep(10);
    }

    /* Create the VRing now ... */
    status = Ipc_lateVirtioCreate(IPC_MPU1_0);
    if (status != IPC_SOK)
    {
#if defined(SYSBIOS)
        System_printf("%s: Ipc_lateVirtioCreate failed\n", __func__);
#else
        AppUtils_Printf(GATEWAYAPP_MSG_NORMAL, "%s : Ipc_lateVirtioCreate failed\n", __func__);
#endif //SYSBIOS
        
    }

    if (status == IPC_SOK)
    {
        status = RPMessage_lateInit(IPC_MPU1_0);
        if (status != IPC_SOK)
        {
#if defined(SYSBIOS)
            System_printf("%s: RPMessage_lateInit failed\n", __func__);
#else
            AppUtils_Printf(GATEWAYAPP_MSG_NORMAL, "%s : RPMessage_lateInit failed\n", __func__);
#endif //SYSBIOS
        }
    }

    return;
}
#if defined (SYSBIOS)
void appLogPrintf(const char *format, ...)
{
    va_list args;

    va_start(args, format);
    System_vprintf(format, args);
    va_end(args);
}
#endif //SYSBIOS

/**
 * \brief CDD IPC New message available notification
 *
 *  Would be invoked by the driver on reception of message from remote core.
 *  Name of this function is configurable, please refer configurator.
 */
void Cdd_IpcNewMessageNotify(uint32 comId)
{
    if (CddIpcConf_IpcComChanId_Cdd_IpcMcu20_EthDevice == comId)
    {
        EthApp_recvMsgNotify();
    }

    return;
}

void Cdd_IpcNewCtrlMessageNotify(uint32 remoteProcId)
{

}


/** \brief prints of the version of this implementation */
static void GatewayApp_PrintVersion(void)
{
    Std_VersionInfoType versioninfo;

    Cdd_IpcGetVersionInfo(&versioninfo);
    AppUtils_Printf(GATEWAYAPP_MSG_NORMAL, " \n");
    AppUtils_Printf(GATEWAYAPP_MSG_NORMAL, GATEWAYAPP_MSG_APP_NAME " CDD IPC MCAL Version Info\n");
    AppUtils_Printf(GATEWAYAPP_MSG_NORMAL, GATEWAYAPP_MSG_APP_NAME "---------------------\n");
    AppUtils_Printf(GATEWAYAPP_MSG_NORMAL, GATEWAYAPP_MSG_APP_NAME " Vendor ID           : %d\n",
                                                versioninfo.vendorID);
    AppUtils_Printf(GATEWAYAPP_MSG_NORMAL, GATEWAYAPP_MSG_APP_NAME " Module ID           : %d\n",
                                                versioninfo.moduleID);
    AppUtils_Printf(GATEWAYAPP_MSG_NORMAL, GATEWAYAPP_MSG_APP_NAME " SW Major Version    : %d\n",
                                                MAJOR_VERSION_NUMBER);
    AppUtils_Printf(GATEWAYAPP_MSG_NORMAL, GATEWAYAPP_MSG_APP_NAME " SW Minor Version    : %d\n",
                                                MINOR_VERSION_NUMBER);
    AppUtils_Printf(GATEWAYAPP_MSG_NORMAL, GATEWAYAPP_MSG_APP_NAME " SW Patch Version    : %d\n",
                                                PATCH_VERSION_NUMBER);
    AppUtils_Printf(GATEWAYAPP_MSG_NORMAL, " \n");

}

/**
 *  \brief Clear interrupt and return the message.
 */
uint32_t GatewayApp_mailboxClear(uint32_t baseAddr, uint32_t queueId)
{
    uint32_t retVal = 0;
    uint32_t msg[4];

    retVal = MailboxGetMessage(baseAddr, queueId, msg);

    return retVal;
}

/* Indexed list of src ids */
const uint16_t gatewayapp_ipc_map_src_id[] =
{
    TISCI_DEV_NAVSS0_MAILBOX_0,
    TISCI_DEV_NAVSS0_MAILBOX_1,
    TISCI_DEV_NAVSS0_MAILBOX_2,
    TISCI_DEV_NAVSS0_MAILBOX_3,
    TISCI_DEV_NAVSS0_MAILBOX_4,
    TISCI_DEV_NAVSS0_MAILBOX_5,
    TISCI_DEV_NAVSS0_MAILBOX_6,
    TISCI_DEV_NAVSS0_MAILBOX_7,
    TISCI_DEV_NAVSS0_MAILBOX_8,
    TISCI_DEV_NAVSS0_MAILBOX_9,
    TISCI_DEV_NAVSS0_MAILBOX_10,
    TISCI_DEV_NAVSS0_MAILBOX_11,
};

/* Indexed list of host ids */
const uint16_t gatewayapp_ipc_map_host_id[] =
#if defined (SOC_J721E)
{
    TISCI_HOST_ID_A72_0,
    TISCI_HOST_ID_MCU_0_R5_0,
    TISCI_HOST_ID_MCU_0_R5_2,
    TISCI_HOST_ID_MAIN_0_R5_0,
    TISCI_HOST_ID_MAIN_0_R5_2,
    TISCI_HOST_ID_MAIN_1_R5_0,
    TISCI_HOST_ID_MAIN_1_R5_2,
    TISCI_HOST_ID_C6X_0_1,
    TISCI_HOST_ID_C6X_1_1,
    TISCI_HOST_ID_C7X_1
};
#elif defined (SOC_J7200)
{
    TISCI_HOST_ID_A72_0,
    TISCI_HOST_ID_MCU_0_R5_0,
    TISCI_HOST_ID_MCU_0_R5_2,
    TISCI_HOST_ID_MAIN_0_R5_0,
    TISCI_HOST_ID_MAIN_0_R5_2,
};
#endif

/* Indexed list of dst ids */
const uint8_t gatewayapp_ipc_map_dst_id[] =
#if defined (SOC_J721E)
{
    TISCI_DEV_COMPUTE_CLUSTER0_GIC500SS,
    TISCI_DEV_MCU_R5FSS0_CORE0,
    TISCI_DEV_MCU_R5FSS0_CORE1,
    TISCI_DEV_R5FSS0_CORE0,
    TISCI_DEV_R5FSS0_CORE1,
    TISCI_DEV_R5FSS1_CORE0,
    TISCI_DEV_R5FSS1_CORE1,
    TISCI_DEV_C66SS0_CORE0,
    TISCI_DEV_C66SS1_CORE0,
    TISCI_DEV_COMPUTE_CLUSTER0_CLEC
};
#elif defined (SOC_J7200)
{
    TISCI_DEV_COMPUTE_CLUSTER0_GIC500SS,
    TISCI_DEV_MCU_R5FSS0_CORE0,
    TISCI_DEV_MCU_R5FSS0_CORE1,
    TISCI_DEV_R5FSS0_CORE0,
    TISCI_DEV_R5FSS0_CORE1,
    TISCI_DEV_COMPUTE_CLUSTER0_CLEC
};
#endif


int32_t GatewayApp_sciclientIrqRelease(uint16_t coreId, uint32_t clusterId,
        uint32_t userId, uint32_t intNumber)
{
    int32_t                               retVal = IPC_SOK;
    struct tisci_msg_rm_irq_release_req   rmIrqRel;

    rmIrqRel.ia_id                  = 0U;
    rmIrqRel.vint                   = 0U;
    rmIrqRel.global_event           = 0U;
    rmIrqRel.vint_status_bit_index  = 0U;

    rmIrqRel.valid_params   = TISCI_MSG_VALUE_RM_DST_ID_VALID |
                              TISCI_MSG_VALUE_RM_DST_HOST_IRQ_VALID |
                              TISCI_MSG_VALUE_RM_SECONDARY_HOST_VALID;
    rmIrqRel.src_id         = gatewayapp_ipc_map_src_id[clusterId];
    rmIrqRel.src_index      = (uint16_t)userId;
    rmIrqRel.dst_id         = (uint16_t)gatewayapp_ipc_map_dst_id[coreId];
#if defined(BUILD_C7X_1)
    rmIrqRel.dst_host_irq   = (uint16_t)(intNumber +  IPC_C7X_COMPUTE_CLUSTER_OFFSET);
#else
    rmIrqRel.dst_host_irq   = (uint16_t)intNumber;
#endif
    rmIrqRel.secondary_host = (uint8_t)gatewayapp_ipc_map_host_id[coreId];

    retVal = Sciclient_rmIrqRelease(&rmIrqRel, IPC_SCICLIENT_TIMEOUT);

    return retVal;
}

int32_t GatewayApp_sciclientIrqSet(uint16_t coreId, uint32_t clusterId,
        uint32_t userId, uint32_t intNumber)
{
    int32_t                           retVal = IPC_SOK;
    struct tisci_msg_rm_irq_set_req   rmIrqReq;
    struct tisci_msg_rm_irq_set_resp  rmIrqResp;

    rmIrqReq.ia_id                  = 0U;
    rmIrqReq.vint                   = 0U;
    rmIrqReq.global_event           = 0U;
    rmIrqReq.vint_status_bit_index  = 0U;

    rmIrqReq.valid_params   = TISCI_MSG_VALUE_RM_DST_ID_VALID |
                              TISCI_MSG_VALUE_RM_DST_HOST_IRQ_VALID |
                              TISCI_MSG_VALUE_RM_SECONDARY_HOST_VALID;
    rmIrqReq.src_id         = gatewayapp_ipc_map_src_id[clusterId];
    rmIrqReq.src_index      = (uint16_t)userId;
    rmIrqReq.dst_id         = (uint16_t)gatewayapp_ipc_map_dst_id[coreId];
#if defined(BUILD_C7X_1)
    rmIrqReq.dst_host_irq   = (uint16_t)(intNumber +  IPC_C7X_COMPUTE_CLUSTER_OFFSET);
#else
    rmIrqReq.dst_host_irq   = (uint16_t)intNumber;
#endif
    rmIrqReq.secondary_host = (uint8_t)gatewayapp_ipc_map_host_id[coreId];

    /* Config event */
    retVal = Sciclient_rmIrqSet(&rmIrqReq, &rmIrqResp, IPC_SCICLIENT_TIMEOUT);

    return retVal;
}

/** \brief Register interrupt handler for new message notification from
 *          core MCU 20
 */

static void GatewayApp_MbIntRegForMcu20(void)
{
    OsalRegisterIntrParams_t    intrPrms;
    OsalInterruptRetCode_e      osalRetVal;
    Int32 retVal;
    HwiP_Handle hwiHandle;
    uint32_t selfId, remoteProcId;
    uint32_t clusterId;
    uint32_t userId;
    uint32_t queueId;
    Ipc_MbConfig cfg;
    uint32_t baseAddr;

    selfId = Ipc_getCoreId();
    remoteProcId = IPC_MCU2_0;
    Ipc_getMailboxInfoRx(selfId, remoteProcId, &clusterId, &userId, &queueId);
    baseAddr = Ipc_getMailboxBaseAddr(clusterId);

    /* Clear Mailbox cluster queue */
    GatewayApp_mailboxClear(baseAddr, queueId);
    MailboxClrNewMsgStatus(baseAddr, userId, queueId);

    /* Get the Interrupt Configuration */
    Ipc_getMailboxIntrRouterCfg(selfId, clusterId, userId, &cfg, 0);

    /* Release the resource first */
    retVal = GatewayApp_sciclientIrqRelease(selfId, clusterId, userId, cfg.eventId);

    uint32_t timeout_cnt = 10;
    do
    {
        retVal = GatewayApp_sciclientIrqSet(selfId, clusterId, userId, cfg.eventId);
        if(retVal != 0)
        {
            AppUtils_Printf(GATEWAYAPP_MSG_NORMAL, GATEWAYAPP_MSG_APP_NAME "Failed to register irq through sciclient...%x\n", retVal);
        }
        timeout_cnt--;
    }while((retVal != 0) && (timeout_cnt > 0));

    if(timeout_cnt == 0)
    {
        retVal = IPC_EFAIL;
    }

    /* Interrupt hookup */
    Osal_RegisterInterrupt_initParams(&intrPrms);
    intrPrms.corepacConfig.arg          = (uintptr_t)NULL;
    intrPrms.corepacConfig.isrRoutine   = &GatewayAppMsgFromMcu20Isr;
    intrPrms.corepacConfig.priority     = cfg.priority;
    intrPrms.corepacConfig.corepacEventNum = 0U;
    intrPrms.corepacConfig.intVecNum    = cfg.eventId;

    osalRetVal = Osal_RegisterInterrupt(&intrPrms, &hwiHandle);
    if(OSAL_INT_SUCCESS != osalRetVal)
    {
        AppUtils_Printf(GATEWAYAPP_MSG_NORMAL,
                        GATEWAYAPP_MSG_APP_NAME ": Error Could not register ISR to receive"
                        " from MCU 2 0 !!!\n");
    }
    return;
}


/** \brief Interrupt from mailbox for all cores registrations */
void GatewayApp_InterruptConfig(void)
{
    GatewayApp_MbIntRegForMcu20 ();

    return;
}

void GatewayAppMsgFromMcu20Isr(uintptr_t notUsed)
{
    /* Invoke MCU 20 Isr handler */
    Cdd_IpcIrqMbxFromMcu_20();
}

/** \brief Start up sequence : Program the interrupt muxes / priorities */
static void GatewayApp_IpcStartup(void)
{

    GatewayApp_PrintVersion();

    GatewayApp_InterruptConfig();

    /* Initialize memory sections  */
    AppUtils_CddIpcSectionInit();

}

/**< Sections defined in linker command file for ADC module */
extern uint32 __linker_cdd_ipc_text_start, __linker_cdd_ipc_text_end,
              __linker_cdd_ipc_const_start, __linker_cdd_ipc_const_end,
              __linker_cdd_ipc_init_start, __linker_cdd_ipc_init_end,
              __linker_cdd_ipc_no_init_start, __linker_cdd_ipc_no_init_end,
              __linker_cdd_ipc_no_init_align_8b_start,
              __linker_cdd_ipc_no_init_align_8b_end;
/**< Address list of sections defined in linker command file for ADC module */
static uint32 CddIpcApp_SecHoleAddrList[] =
{
    ((uint32) & __linker_cdd_ipc_text_start),
    ((uint32) & __linker_cdd_ipc_text_end) - APP_UTILS_LINKER_FILL_LENGTH,
    ((uint32) & __linker_cdd_ipc_const_start),
    ((uint32) & __linker_cdd_ipc_const_end) - APP_UTILS_LINKER_FILL_LENGTH,
    ((uint32) & __linker_cdd_ipc_init_start),
    ((uint32) & __linker_cdd_ipc_init_end) - APP_UTILS_LINKER_FILL_LENGTH,
    ((uint32) & __linker_cdd_ipc_no_init_start),
    ((uint32) & __linker_cdd_ipc_no_init_end) - APP_UTILS_LINKER_FILL_LENGTH,
    ((uint32) & __linker_cdd_ipc_no_init_align_8b_start),
    ((uint32) & __linker_cdd_ipc_no_init_align_8b_end) -
                                                APP_UTILS_LINKER_FILL_LENGTH,
};

void AppUtils_CddIpcSectionInit(void)
{
    /* Initialize memory sections  */
    AppUtils_SectionInit(&CddIpcApp_SecHoleAddrList[0U],
        MODULEAPP_NUM_SEC_HOLES(CddIpcApp_SecHoleAddrList));
}



