/*
*
* Copyright (c) 2019 Texas Instruments Incorporated
*
* All rights reserved not granted herein.
*
* Limited License.
*
* Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
* license under copyrights and patents it now or hereafter owns or controls to make,
* have made, use, import, offer to sell and sell ("Utilize") this software subject to the
* terms herein.  With respect to the foregoing patent license, such license is granted
* solely to the extent that any such patent is necessary to Utilize the software alone.
* The patent license shall not apply to any combinations which include this software,
* other than combinations with devices manufactured by or for TI ("TI Devices").
* No hardware patent is licensed hereunder.
*
* Redistributions must preserve existing copyright notices and reproduce this license
* (including the above copyright notice and the disclaimer and (if applicable) source
* code license limitations below) in the documentation and/or other materials provided
* with the distribution
*
* Redistribution and use in binary form, without modification, are permitted provided
* that the following conditions are met:
*
* *       No reverse engineering, decompilation, or disassembly of this software is
* permitted with respect to any software provided in binary form.
*
* *       any redistribution and use are licensed by TI for use only with TI Devices.
*
* *       Nothing shall obligate TI to provide you with source code for the software
* licensed and provided to you in object code.
*
* If software source code is provided to you, modification and redistribution of the
* source code are permitted provided that the following conditions are met:
*
* *       any redistribution and use of the source code, including any resulting derivative
* works, are licensed by TI for use only with TI Devices.
*
* *       any redistribution and use of any object code compiled from the source code
* and any resulting derivative works, are licensed by TI for use only with TI Devices.
*
* Neither the name of Texas Instruments Incorporated nor the names of its suppliers
*
* may be used to endorse or promote products derived from this software without
* specific prior written permission.
*
* DISCLAIMER.
*
* THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
* OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
* OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
* IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
* BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
* OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
* OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
* OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/**
 *  \file     gateway_demo.h
 *
 *  \brief    Common header file for gateway demo application
 *
 */
/**
 * \defgroup MCUSS_APP_GATEWAY_DEMO gateway demo application
 *
 *  This application perform CAN transmission on all the enabled instances.
 *  By default the CAN peripheral is configured to operate in loop-back mode,
 *  i.e. transmit and receive.
 *
 *  TI RTOS is used and CAN tx/rx is performed in a task, The CAN IF stub is
 *  is used to indicate completion of transmission / reception by posting a
 *  semaphore.
 * @{
 */
#ifndef GATEWAY_DEMO_H_
#define GATEWAY_DEMO_H_

/* ========================================================================== */
/*                             Include Files                                  */
/* ========================================================================== */
#include "gateway_demo_cfg.h"
#include "eth_process.h"
#include "can_process.h"
#include "utils_prf.h"

#include "string.h"
#include "Std_Types.h"
#include "Det.h"
#include "Dem.h"
#include "Os.h"
#include "Can.h"
#include "CanIf_Cbk.h"
#include "EcuM_Cbk.h"
#include "Dio.h"

#ifdef __cplusplus
extern "C"
{
#endif

/* ========================================================================== */
/*                       Application Configurations                           */
/* ========================================================================== */

/* ========================================================================== */
/*                           Macros & Typedefs                                */
/* ========================================================================== */
#define MAJOR_VERSION_NUMBER                    2U
/**< Major version number for application*/
#define MINOR_VERSION_NUMBER                    0U
/**< Minor version number for application*/
#define PATCH_VERSION_NUMBER                    0U
/**< Patch version number for application*/
#define BUILD_VERSION_NUMBER                    0U
/**< Build version number for application*/

#define NTSCF_VERSION                          0U
/**< Version number for NTSCF control header*/
#define SV_BIT                                 0U
/**< Bit which tells if Stream ID is valid or not*/
#define DEFAULT_NTSCF_BIT                      0U
/**< Bit which tells if Stream ID is valid or not*/

#define ACF_MSG_TYPE_FLEXRAY                   1U
/**< ACF message type for Flexray*/
#define ACF_MSG_TYPE_CAN                       1U
/**< ACF message type for CAN*/
#define ACF_MSG_TYPE_CAN_BRIEF                 2U
/**< ACF message type for CAN (abbreviated)*/
#define ACF_MSG_TYPE_LIN                       3U
/**< ACF message type for LIN*/
#define DEFAULT_ACF_MSG_LENGTH_CAN             20U
/**< ACF message length (number of 32 bit words) for CAN*/
#define CAN_PAD_MASK                           3U
/**< Mask for 2 pad bits*/
#define CAN_SHIFT_VAL                          6U
/**< Shift val for CAN pad bits*/
#define MTV_BIT                                0U
/**< Message timestamp valid bit*/
#define MTV_SHIFT_VAL                          5U
/**< Shift val for message timestamp bits*/
#define RTR_BIT                                0U
/**< Remote transmission request bit*/
#define RTR_SHIFT_VAL                          4U
/**< Shift val for RTR bits*/
#define EFF_BIT                                1U
/**< Extended frame format bit*/
#define EFF_SHIFT_VAL                          3U
/**< Shift val for EFF bits*/
#define BRS_BIT                                1U
/**< Bit rate switch bit*/
#define BRS_SHIFT_VAL                          2U
/**< Shift val for BRS bit*/
#define FDF_BIT                                1U
/**< Flexible data rate bit*/
#define FDF_SHIFT_VAL                          1U
/**< Shift val for FDF bit*/
#define ESI_BIT                                0U
/**< Error state indicator bit*/
#define ESI_SHIFT_VAL                          0U
/**< Shift val for ESI bit*/
#define RSV_SHIFT_VAL                          5U
/**< Shift val for 3 reserved bits*/
#define DEFAULT_CAN_BUS_ID                     0U            
/**< Default CAN Bus ID*/
#define CAN_BUS_ID_MASK                        0x1F
/**< Shift val for 3 reserved bits*/

#define MCAN0_PORT                            (0)
/**< Used for setting routing table decision to MCAN0*/
#define MCAN1_PORT                            (1)
/**< Used for setting routing table decision to MCAN1*/

#define NUM_ETH_INTERFACES                    (2U)
/**< Number of ethernet interfaces used for demo*/
#define NUM_CAN_INTERFACES                    (2U)
/**< Number of CAN interfaces used for demo */
#define NTSCF_SUBTYPE_OFFSET                   0U
/**< Offset for common control header subtype field*/
#define DEFAULT_1722_ETH_PKT_LEN              (100U)
/**< Length of 1722 frame by default. Assumes 1 CAN frame of 64B*/

#define WKUP_FRAME_ID                         (0xB0)
/**< Special identifier used to mark a wakeup frame for the PC tool*/

#define CAN_2_ETH_FRAME_ID                    (0xC0)
/**< Special identifier used to mark an Eth frame generated from CAN frame for the PC tool*/

#define ETH_2_ETH_FRAME_ID                    (0xD0)
/**< Special identifier used to mark an Eth frame routed from another Eth frame for the PC tool*/

#define CAN_2_CAN_FRAME_ID                    (0xA0)
/**< Special identifier used to mark a CAN frame generated from CAN frame for the PC tool*/

#define ETH_2_CAN_FRAME_ID                    (0xB0)
/**< Special identifier used to mark an CAN frame routed from another Eth frame for the PC tool*/

#define NUM_BUCKETS_IN_HASH_TABLE             (0xFF)
/**< Number of buckets in Hash Table*/

#define CAN_ID_FOR_GENERATOR                  (0xD0)
/**< CAN ID for MCU MCAN1 port (CAN generator App Rx port)*/

#define GENERATOR_PKT_MARKER                  (0xA0)
/**< CAN ID for MCU MCAN1 port (CAN generator App Rx port)*/

#define GENERATOR_STAT_REQUEST_MARKER         (0xB0)
/**< This CAN ID tells CAN generator to send statistics*/

typedef enum
{
    ETH_2_CAN = 0,
    CAN_2_ETH = 1,
    CAN_2_CAN = 2,
    ETH_2_ETH = 3

} routing_types;

typedef struct GatewayApp_Stats_s
{
    EthApp_Stats ethStats;
    /**< Struct for Ethernet stats */
} GatewayApp_Stats;

typedef struct GatewayApp_Routing_Info_s
{
    uint8_t canIfStatus[NUM_CAN_INTERFACES];
    /**< Array which tells which CAN port packet should go out on */
    uint8_t ethIfStatus[NUM_ETH_INTERFACES];
    /**< Array which tells which CAN port packet should go out on */
} GatewayApp_RoutingInfo;

typedef struct GatewayApp_Hash_Entry_s
{
    uint16_t ethIfBitMask;
    /**< Bit mask which tells which ethernet port packet should go out on
     * Only lower 9 bits are used */
    uint16_t canIfBitMask;
    /**< Bit mask which tells which CAN port packet should go out on */

} GatewayApp_HashEntry;

typedef struct GatewayApp_Hash_Bucket_s
{
    GatewayApp_HashEntry *entry;
    /**< Pointer to first entry */
    uint8_t  numBuckets;
    /**< Number of entries in the bucket */

} GatewayApp_HashBucket;

typedef struct GatewayApp_Eth_MAC_LUT_s
{
    uint8_t macAddr[6];
    /**< MacAddress */

} GatewayApp_EthMac_LUT;

typedef struct GatewayApp_State_s
{
    volatile bool runTest;
    /**< Whether test should be running or not */

    uint32_t can_bus_busy;
    /**< Statistic which tells if CAN Bus is busy*/
    uint32_t can_sem_failed;
    /**< Statistic which tells if CAN semaphore pend failed */
    uint32_t can_send_count[NUM_CAN_INTERFACES];
    /**< Statistic which tells how many CAN messages were sent on each interface */
    uint32_t eth_send_count[NUM_ETH_INTERFACES];
    /**< Statistic which tells how many Eth messages were sent out */
    uint8_t stream[2];
    /**< 16 bit unique value for stream ID */

} GatewayApp_State;

/* ========================================================================== */
/*                         Structures and Enums                               */
/* ========================================================================== */


/* ========================================================================== */
/*                          Function Declarations                             */
/* ========================================================================== */
int32_t GatewayApp_Init(void);
void gatewayApp_Router(void);
void gatewayApp_Shutdown(void);
void gatewayApp_printParams(void);
void send_cpu_load(appProfileAvgLoadInfo *);
void gatewayApp_lookUpRoutingTable(uint8_t dstID, GatewayApp_RoutingInfo* routingInfo);
void gatewayApp_updateHashEntry(uint8_t canID, uint16_t canMask, uint16_t ethMask);
void gatewayApp_updateMacLUT(uint8_t *macID, uint8_t portNum);
void gatewayApp_sendMsgtoGenerator(uint8_t *pktBuf, uint8_t subType);

#ifdef __cplusplus
}
#endif

#endif  /* #ifndef GATEWAY_DEMO_H_ */
/* @} */
