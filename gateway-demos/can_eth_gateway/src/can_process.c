/*
*
* Copyright (c) 2019 Texas Instruments Incorporated
*
* All rights reserved not granted herein.
*
* Limited License.
*
* Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
* license under copyrights and patents it now or hereafter owns or controls to make,
* have made, use, import, offer to sell and sell ("Utilize") this software subject to the
* terms herein.  With respect to the foregoing patent license, such license is granted
* solely to the extent that any such patent is necessary to Utilize the software alone.
* The patent license shall not apply to any combinations which include this software,
* other than combinations with devices manufactured by or for TI ("TI Devices").
* No hardware patent is licensed hereunder.
*
* Redistributions must preserve existing copyright notices and reproduce this license
* (including the above copyright notice and the disclaimer and (if applicable) source
* code license limitations below) in the documentation and/or other materials provided
* with the distribution
*
* Redistribution and use in binary form, without modification, are permitted provided
* that the following conditions are met:
*
* *       No reverse engineering, decompilation, or disassembly of this software is
* permitted with respect to any software provided in binary form.
*
* *       any redistribution and use are licensed by TI for use only with TI Devices.
*
* *       Nothing shall obligate TI to provide you with source code for the software
* licensed and provided to you in object code.
*
* If software source code is provided to you, modification and redistribution of the
* source code are permitted provided that the following conditions are met:
*
* *       any redistribution and use of the source code, including any resulting derivative
* works, are licensed by TI for use only with TI Devices.
*
* *       any redistribution and use of any object code compiled from the source code
* and any resulting derivative works, are licensed by TI for use only with TI Devices.
*
* Neither the name of Texas Instruments Incorporated nor the names of its suppliers
*
* may be used to endorse or promote products derived from this software without
* specific prior written permission.
*
* DISCLAIMER.
*
* THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
* OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
* OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
* IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
* BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
* OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
* OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
* OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/**
 *  \file     gateway_demo.c
 *
 *  \brief    This file implements Eth-CAN gateway demo application
 */

/* ========================================================================== */
/*                             Include Files                                  */
/* ========================================================================== */
#include "can_process.h"
#include "gateway_demo_cfg.h"

/* ========================================================================== */
/*                           Macros & Typedefs                                */
/* ========================================================================== */
#define CAN_APP_E_DEM   (E_NOT_OK + 1U)
/**< Error code to indicate DEM */
#define CAN_APP_E_DET   (E_NOT_OK + 2U)
/**< Error code to indicate DEM */
/* ========================================================================== */
/*              Internal Function Declarations                                */
/* ========================================================================== */
static void CanApp_BuildIntList(void);
static void CanApp_InterruptConfig(void);
static void CanApp_PowerAndClkSrc(void);
static void CanApp_EnableTransceivers(void);
#if defined (BUILD_MCU2_1)
static void SetupI2CTransfer(I2C_Handle handle,  uint32 slaveAddr,
                      uint8 *writeData, uint32 numWriteBytes,
                      uint8 *readData,  uint32 numReadBytes);
#endif //BUILD_MCU2_1
static int32_t CanApp_enqueueFrame(const PduInfoType* PduInfoPtr);

/* ========================================================================== */
/*                            Global Variables                                */
/* ========================================================================== */
static uint32           CanApp_IntNumbers[CAN_NUM_CONTROLLER];
/**< Stored the interrupt numbers for enabled all timers */
static CanApp_IsrType   CanApp_Isr[CAN_NUM_CONTROLLER];
/**< Associated ISR */
static HwiP_Handle      CanApp_IsrHndls[CAN_NUM_CONTROLLER];
/**< Stores the ISR handles */

uint32                      CanApp_ProfileStatus = E_OK;
/**< Variable used for Demo status */
/**< Variable which contains Can SDU data that is transported inside the PDU */
volatile PduInfoType                 *CanApp_RxPdu = NULL;
/**< Pointer received data */
uint32                      CanApp_DemStatus = E_OK;
/**< Variable used to track dem notifications */
uint32                      CanApp_DetStatus = E_OK;
/**< Variable used to track det notifications */

CanApp_State canAppState;
/**< Global structure used to track state variables */

extern SemaphoreP_Handle gateway_semHandle;
/**< Semaphore handle used to trigger routing task */
#if (STD_ON == GATEWAY_CAN_POLLING_TASK) && (CAN_RX_POLLING == STD_ON)
extern SemaphoreP_Handle canTask_semHandle;
/**< Semaphore handle used to trigger CAN polling task */
#endif //GATEWAY_CAN_POLLING_TASK
uint32_t num_can_frames_rcvd = 0;
/**< Local count for number of frames received */
/* ========================================================================== */
/*                            External Variables                              */
/* ========================================================================== */
/* Global variables used in callback functions */
extern volatile P2VAR(Can_HwType, CANIF_VAR_CLEARED, CANIF_APPL_DATA) CanIf_Mailbox;
        
extern volatile VAR(PduInfoType, CANIF_VAR_CLEARED) CanIf_PduInfo;
/**< CAN PDU structure instance */
extern SemaphoreP_Handle CanIf_TxConfirmationSemaphore;
/**< TX Confirmation semaphore, would be posted when TX completes */
extern SemaphoreP_Handle CanIf_RxConfirmationSemaphore;
/**< Rx Confirmation semaphore, would be posted when RX completes */
extern int32_t (*push_frame_to_app_queue)(const PduInfoType*);
/**< Function pointer used to copy data to application queue */

extern volatile VAR(uint32, CANIF_VAR_CLEARED) CanIf_RxConfirmationCount;
/**< Rx Indication count - number of packets that came in */
extern volatile VAR(uint32, CANIF_VAR_CLEARED) CanIf_TxConfirmationCount;
/**< Tx Confirmation count - number of packets that went out */
/* ========================================================================== */
/*                          Function Definitions                              */
/* ========================================================================== */
uint32 CanApp_Init(void)
{
    uint32              ctlr_cnt;
    Std_ReturnType      status;

    CanApp_RxPdu = &CanIf_PduInfo;

    CanApp_DemStatus = E_OK;
    CanApp_DetStatus = E_OK;   

    for (ctlr_cnt = 0U; ctlr_cnt < CAN_NUM_CONTROLLER; ctlr_cnt++)
    {
        CanApp_ProfileStatus = E_OK;

        /* Set Controller Mode to Start */
        status = Can_SetControllerMode(
        CanConfigSet_CanController_List_PC[ctlr_cnt]->ControllerId, CAN_CS_STARTED);
        if (status != E_OK)
        {
            CanApp_ProfileStatus = E_NOT_OK;
        }
    }

    /*Allocate memory for the CAN queues*/
    canAppState.canQueue.topOfQueue = (uint8_t*)malloc(sizeof(uint8_t) * MCAN_MAX_Q_SIZE);
    if(canAppState.canQueue.topOfQueue == NULL)
    {
        CanApp_ProfileStatus = E_NOT_OK;
    } 
    else
    {
        canAppState.canQueue.bottomOfQueue = canAppState.canQueue.topOfQueue + MCAN_MAX_Q_SIZE;
        canAppState.canQueue.currentRdPtr = canAppState.canQueue.topOfQueue;
        canAppState.canQueue.currentWrPtr = canAppState.canQueue.topOfQueue;
    }

    /*Assign to function pointer so it can be called by CAN IF callback*/
    push_frame_to_app_queue = &CanApp_enqueueFrame;

    /*----------------End of Initializations----------------------*/

    return CanApp_ProfileStatus;

}

void CanApp_RxTask(void)
{
#if (CAN_RX_POLLING == STD_ON)
    Can_MainFunction_Read();
#if (STD_ON == GATEWAY_CAN_POLLING_TASK) 
    /*Pend on semaphore posted by DM Timer*/
    SemaphoreP_pend(canTask_semHandle, SemaphoreP_WAIT_FOREVER);
#else
    TaskP_sleep(1);
#endif //GATEWAY_CAN_POLLING_TASK
#else //CAN_RX_POLLING
    SemaphoreP_pend(CanIf_RxConfirmationSemaphore,
                                SemaphoreP_WAIT_FOREVER);
#endif //CAN_RX_POLLING

#ifndef GATEWAY_POLL_MODE

    /*Trigger Gateway task*/
    if(num_can_frames_rcvd != CanIf_RxConfirmationCount)
    {
        SemaphoreP_post(gateway_semHandle);
        num_can_frames_rcvd = CanIf_RxConfirmationCount;
    }    
#endif //GATEWAY_POLL_MODE
}

void CanApp_DeInit(void)
{
    uint32              ctlr_cnt;
    Std_ReturnType      status;

    for (ctlr_cnt = 0U; ctlr_cnt < CAN_NUM_CONTROLLER; ctlr_cnt++)
    {
        /* Set Controller Mode to Stop*/
        status = Can_SetControllerMode(
            CanConfigSet_CanController_List_PC[ctlr_cnt]->ControllerId,
            CAN_CS_STOPPED);
        if (status != E_OK)
        {
            CanApp_ProfileStatus = E_NOT_OK;
        }
    } 
}

/** \brief Application tear down functions */
void CanApp_Shutdown(void)
{
    uint32 idx;

    for (idx = 0U; idx < CAN_NUM_CONTROLLER; idx++)
    {
        if (NULL != CanApp_IsrHndls[idx])
        {
            if (HwiP_OK != HwiP_delete(CanApp_IsrHndls[idx]))
            {
                AppUtils_Printf(GATEWAYAPP_MSG_NORMAL, GATEWAYAPP_MSG_APP_NAME
                                " Error!!! Could not De register"
                                " the ISR for instance %d!!!\n", idx);
                break;
            }
        }
    }

    if (NULL != CanIf_TxConfirmationSemaphore)
    {
        SemaphoreP_delete(CanIf_TxConfirmationSemaphore);
        if (NULL != CanIf_RxConfirmationSemaphore)
        {
            SemaphoreP_delete(CanIf_RxConfirmationSemaphore);
        }
    }

    /*Free memory*/
    free(canAppState.canQueue.topOfQueue);

    return;
}

/** \brief Start up sequence : Program the interrupt muxes / priorities */
void CanApp_Startup(void)
{
    /* Equivalent to EcuM_AL_SetProgrammableInterrupts */
    CanApp_BuildIntList();

    CanApp_InterruptConfig();

    /* Initialize counters, that would be required for timed operations */
    AppUtils_ProfileInit(0);

    CanApp_PowerAndClkSrc();

    /* Initialize dummy CAN IF */
    CanIf_Init(NULL);
}

void CanApp_BuildIntList(void)
{
    uint32 idx, flag, intNum;
    const Can_ConfigType *Can_ConfigPtr;
    CanApp_IsrType pIsrHandler = NULL;

    AppUtils_Printf(GATEWAYAPP_MSG_NORMAL, "\n");
    /*
     * 1. Determine the number of CAN used
     * 3. Build isr and interrupt number for enabled CAN instances
     */
    /* Do Can Init */
#if (STD_ON == CAN_VARIANT_PRE_COMPILE)
    Can_ConfigPtr = &CAN_INIT_CONFIG_PC;
    Can_Init((const Can_ConfigType *) NULL_PTR);
    AppUtils_Printf(GATEWAYAPP_MSG_NORMAL, GATEWAYAPP_MSG_APP_NAME
                    "Variant - Pre Compile being used !!!\n");
#else
    Can_ConfigPtr = &CanConfigSet;
    Can_Init(Can_ConfigPtr);
    AppUtils_Printf(GATEWAYAPP_MSG_NORMAL, GATEWAYAPP_MSG_APP_NAME
                    "Variant - Post Build being used !!!\n");
#endif

    for (idx = 0U; idx < Can_ConfigPtr->CanMaxControllerCount; idx++)
    {
        flag = 0U;

        if (CAN_CONTROLLER_INSTANCE_MCAN4 ==
            CanConfigSet_CanController_List_PC[idx]->CanControllerInst)
        {
            intNum = APP_MCAN_4_INT0;
            pIsrHandler = Can_6_Int0ISR;
            flag = 1U;
        }
    #if defined (SOC_J721E)    
        if (CAN_CONTROLLER_INSTANCE_MCAN9 ==
            CanConfigSet_CanController_List_PC[idx]->CanControllerInst)
        {
            intNum = APP_MCAN_9_INT0;
            pIsrHandler = Can_11_Int0ISR;
            flag = 1U;
        }
    #elif defined (SOC_J7200)
        if (CAN_CONTROLLER_INSTANCE_MCAN8 ==
            CanConfigSet_CanController_List_PC[idx]->CanControllerInst)
        {
            intNum = APP_MCAN_8_INT0;
            pIsrHandler = Can_10_Int0ISR;
            flag = 1U;
        }
    #endif

        if (0U != flag)
        {
            CanApp_IntNumbers[idx] = intNum;
            CanApp_Isr[idx] = pIsrHandler;
            flag = 0U;
        }
    }

    return;
}

static void CanApp_InterruptConfig(void)
{
    uint32 idx;
    OsalRegisterIntrParams_t    intrPrms;
    OsalInterruptRetCode_e      osalRetVal;
    HwiP_Handle hwiHandle;
    const Can_ConfigType *Can_ConfigPtr;

#if (STD_ON == CAN_VARIANT_PRE_COMPILE)
    Can_ConfigPtr = &CAN_INIT_CONFIG_PC;
#else
    Can_ConfigPtr = &CanConfigSet;
#endif

    for (idx = 0U; idx < Can_ConfigPtr->CanMaxControllerCount; idx++)
    {
        /* If the CAN instance is not in MCU domain, the interrupt router will
            have to be configured */
        /* Set the destination interrupt */
        Osal_RegisterInterrupt_initParams(&intrPrms);
        intrPrms.corepacConfig.arg          = (uintptr_t)CanApp_Isr[idx];
        intrPrms.corepacConfig.isrRoutine   = &CanApp_CanXIsr;
        intrPrms.corepacConfig.priority     = 1U;
        intrPrms.corepacConfig.corepacEventNum = 0U; /* NOT USED ? */
        intrPrms.corepacConfig.intVecNum        = CanApp_IntNumbers[idx];

        osalRetVal = Osal_RegisterInterrupt(&intrPrms, &hwiHandle);
        if(OSAL_INT_SUCCESS != osalRetVal)
        {
            AppUtils_Printf(GATEWAYAPP_MSG_NORMAL, GATEWAYAPP_MSG_APP_NAME
                    "Error - Unable to register interrupt !!!\n");
            break;
        }
    }
    return;
}

/** \brief None, SBL/GEL powers up the timers and clock sources */
static void CanApp_PowerAndClkSrc(void)
{
    /* Mcu module, when included will replace this operation */
    return;
}

void CanApp_PlatformInit()
{
    uint32 regVal = 0U;
    /* Unlock lock key registers for Partition 7: IO PAD
       configuration registers in MAIN_CTRL_MMR */
    /* write Partition 7 Lock Key 0 Register */
    CSL_REG32_WR(CSL_WKUP_CTRL_MMR0_CFG0_BASE + 0x1D008, 0x68EF3490);
    /* write Partition 7 Lock Key 1 Register */
    CSL_REG32_WR(CSL_WKUP_CTRL_MMR0_CFG0_BASE + 0x1D00C, 0xD172BC5A);
    /* Check for unlock */
    regVal = CSL_REG32_RD(CSL_WKUP_CTRL_MMR0_CFG0_BASE + 0x1D008);
    while ((regVal & 0x1) != 0x1U)
    {
        regVal = CSL_REG32_RD(CSL_WKUP_CTRL_MMR0_CFG0_BASE + 0x1D008);
    }

    /* Unlock lock key registers for Partition 7: IO PAD
       configuration registers in MAIN_CTRL_MMR */
    /* write Partition 7 Lock Key 0 Register */
    CSL_REG32_WR(CSL_CTRL_MMR0_CFG0_BASE + 0x1D008, 0x68EF3490);
    /* write Partition 7 Lock Key 1 Register */
    CSL_REG32_WR(CSL_CTRL_MMR0_CFG0_BASE + 0x1D00C, 0xD172BC5A);
    /* Check for unlock */
    regVal = CSL_REG32_RD(CSL_CTRL_MMR0_CFG0_BASE + 0x1D008);
    while ((regVal & 0x1) != 0x1U)
    {
        regVal = CSL_REG32_RD(CSL_CTRL_MMR0_CFG0_BASE + 0x1D008);
    }

    /* Unlocking done */
    /* Below code will be replaced by Port module in further releases */
  
    #if defined (SOC_J721E)    
    /* MAIN MCAN 4 Tx PAD configuration */
    regVal = 0x60006U;
    CSL_REG32_WR(CSL_CTRL_MMR0_CFG0_BASE + 0x1C020U, regVal);
    /* MAIN MCAN 4 Rx PAD configuration */
    regVal = 0x60006U;
    CSL_REG32_WR(CSL_CTRL_MMR0_CFG0_BASE + 0x1C024U, regVal);

    /* MAIN MCAN 9 Tx PAD configuration */
    regVal = 0x60006U;
    CSL_REG32_WR(CSL_CTRL_MMR0_CFG0_BASE + 0x1C0CCU, regVal);
    /* MAIN MCAN 9 Rx PAD configuration */
    regVal = 0x60006U;
    CSL_REG32_WR(CSL_CTRL_MMR0_CFG0_BASE + 0x1C0D0U, regVal);
    #elif defined (SOC_J7200)
    /* MAIN MCAN 4 Tx PAD configuration */ 
    regVal = 0x60006U; 
    CSL_REG32_WR(CSL_CTRL_MMR0_CFG0_BASE + 0x1C040U, regVal); 
    /* MAIN MCAN 4 Rx PAD configuration */ 
    regVal = 0x60006U; 
    CSL_REG32_WR(CSL_CTRL_MMR0_CFG0_BASE + 0x1C044U, regVal); 
    /* MAIN MCAN 8 Tx PAD configuration */ 
    regVal = 0x60006U; 
    CSL_REG32_WR(CSL_CTRL_MMR0_CFG0_BASE + 0x1C060U, regVal); 
    /* MAIN MCAN 8 Rx PAD configuration */ 
    regVal = 0x60006U; 
    CSL_REG32_WR(CSL_CTRL_MMR0_CFG0_BASE + 0x1C064U, regVal); 
    #endif

    /* Take MCAN transceivers out of STB mode i.e NORMAL Mode */
    CanApp_EnableTransceivers();
}

static void CanApp_EnableTransceivers(void)
{
#if defined (SOC_J721E)    
    uint32 regVal = 0U;
    Dio_LevelType dioPinLevel[2U];

    /* Configure SOC_PADCONFIG_143 set it to '1' to take
       MCAN transceiver out of STB mode */
    I2C_Params      i2cParams;
    I2C_Handle      handle = NULL;
    uint8           dataToSlave[4];

    /* Pin mux for CAN STB used in GESI board */
    regVal = 0x20007U;
    CSL_REG32_WR(CSL_CTRL_MMR0_CFG0_BASE + 0x1C0F4U, regVal);
    /* Take MCAN transceiver out of STB mode for MCAN0 */
    /* Set Pin direction to output */
    regVal = CSL_REG32_RD(CSL_GPIO0_BASE + 0x38U);
    regVal &= (~(1U << 0x1BU));
    CSL_REG32_WR(CSL_GPIO0_BASE + 0x38U, regVal);
    /* Drive Pin to Low */
    Dio_WriteChannel(CAN_TRCV_MAIN_DOMAIN_4_9_11, STD_LOW);
    /* Read Pin level */
    dioPinLevel[0] = Dio_ReadChannel(CAN_TRCV_MAIN_DOMAIN_4_9_11);
    /*Read back the pin levels to ensure transceiver is enabled*/
    if (STD_LOW != dioPinLevel[0U])
    {
        AppUtils_Printf(GATEWAYAPP_MSG_NORMAL,
        "CAN_APP: Error in Enabling CAN Transceiver Main Domain Inst 4 and 9!!!\n");
    }
    else
    {
        AppUtils_Printf(GATEWAYAPP_MSG_NORMAL,
        "CAN_APP: Successfully Enabled CAN Transceiver Main Domain Inst 4 and 9 !!!\n");
    }

    /*
     * Configuring TCA6424 IO Exp 2 with addr 0x22
     * This io expander is controlled by i2c0
     * For Main MCAN2 P13 and P14 should be set to 0, This should route the MCAN2 STB line to transciver.
     * For Main MCAN0 P06 and P07 should be set to 1.
     */
     /* I2c PinMux */
    /* I2C0_SDL */
    regVal = 0x40000U;
    CSL_REG32_WR(CSL_CTRL_MMR0_CFG0_BASE + 0x1C220U, regVal);
    /* I2C0_SDA */
    regVal = 0x40000U;
    CSL_REG32_WR(CSL_CTRL_MMR0_CFG0_BASE + 0x1C224U, regVal);
    /* I2C initialization */

    I2C_init();
    I2C_Params_init(&i2cParams);
    i2cParams.transferMode = I2C_MODE_BLOCKING;
    i2cParams.bitRate = I2C_400kHz;
    i2cParams.transferCallbackFxn = NULL;

    handle = I2C_open(0U, &i2cParams);

    dataToSlave[0] = TCA6424_REG_CONFIG0 | TCA6424_CMD_AUTO_INC;
    dataToSlave[1] = 0x0U;
    SetupI2CTransfer(handle, 0x22, &dataToSlave[0], 2, NULL, 0);

    dataToSlave[0] = TCA6424_REG_INPUT0 | TCA6424_CMD_AUTO_INC;
    dataToSlave[1] = 0x0U;
    dataToSlave[2] = 0x0U;
    dataToSlave[3] = 0x0U;
    SetupI2CTransfer(handle, 0x22, &dataToSlave[0], 1, &dataToSlave[1], 3);

    /* Set P06 and P07 to 1.
     * Set P13 and P14 to 0.
     */
    dataToSlave[0] = TCA6424_REG_OUTPUT0 | TCA6424_CMD_AUTO_INC;
    dataToSlave[1] |= 0xC0;
    dataToSlave[2] &= ~(0x18);
    SetupI2CTransfer(handle, 0x22, &dataToSlave[0], 1, &dataToSlave[1], 3);
#elif defined (SOC_J7200)
    uint32 regVal = 0U;

    /* Configure SOC_PADCONFIG_143 set it to '1' to take
       MCAN transceiver out of STB mode */
    I2C_Params      i2cParams;
    I2C_Handle      handle = NULL;
    uint8           dataToSlave[2];

    /*
     * Configuring TCA6408A IO Exp with addr 0x21
     * This io expander is controlled by i2c0
     * For Main MCAN4,5,6,7,8,10 P07 should be set to 1.
     */
     /* I2c PinMux */
    /* I2C0_SDL */
    regVal = 0x40000U;
    CSL_REG32_WR(CSL_CTRL_MMR0_CFG0_BASE + 0x1C0D4U, regVal);
    /* I2C0_SDA */
    regVal = 0x40000U;
    CSL_REG32_WR(CSL_CTRL_MMR0_CFG0_BASE + 0x1C0D8U, regVal);
    /* I2C initialization */
    I2C_init();
    I2C_Params_init(&i2cParams);
    i2cParams.transferMode = I2C_MODE_BLOCKING;
    i2cParams.bitRate = I2C_400kHz;
    i2cParams.transferCallbackFxn = NULL;

    handle = I2C_open(0U, &i2cParams);

    dataToSlave[0] = TCA6408A_REG_CONFIG0;
    dataToSlave[1] = 0x0U;
    SetupI2CTransfer(handle, 0x21, &dataToSlave[0], 2, NULL, 0);

    dataToSlave[0] = TCA6408A_REG_INPUT0;
    dataToSlave[1] = 0x0U;
    SetupI2CTransfer(handle, 0x21, &dataToSlave[0], 1, &dataToSlave[1], 1);

    dataToSlave[0] = TCA6408A_REG_OUTPUT0;
    dataToSlave[1] |= 0x05U;
    dataToSlave[1] &= ~(0x80U);
    SetupI2CTransfer(handle, 0x21, &dataToSlave[0], 2, NULL, 0);
#endif
    return;
}

#if defined (BUILD_MCU2_1)
static void SetupI2CTransfer(I2C_Handle handle,  uint32 slaveAddr,
                      uint8 *writeData, uint32 numWriteBytes,
                      uint8 *readData,  uint32 numReadBytes)
{
    bool status;
    I2C_Transaction i2cTransaction;

    I2C_transactionInit(&i2cTransaction);
    i2cTransaction.slaveAddress = slaveAddr;
    i2cTransaction.writeBuf = (uint8 *)&writeData[0];
    i2cTransaction.writeCount = numWriteBytes;
    i2cTransaction.readBuf = (uint8 *)&readData[0];
    i2cTransaction.readCount = numReadBytes;
    status = I2C_transfer(handle, &i2cTransaction);
    if(FALSE == status)
    {
        AppUtils_Printf(GATEWAYAPP_MSG_NORMAL, "\n Data Transfer failed. \n");
    }
}
#endif //BUILD_MCU2_1

static int32_t CanApp_enqueueFrame(const PduInfoType* PduInfoPtr)
{
    uint8_t index = 0;

    /*Check if there is space in buffer*/

    /*First check if there is enough space below write pointer till bottom of queue*/
    if((canAppState.canQueue.currentWrPtr + PduInfoPtr->SduLength) > canAppState.canQueue.bottomOfQueue)
    {
        /*If we wrapped around then would there be enough space*/
        if((canAppState.canQueue.currentRdPtr - canAppState.canQueue.topOfQueue) <  PduInfoPtr->SduLength)
        {
            canAppState.canQueue.numOverflowEvt++;  
            return -1; /*No space*/
        }
        else
        {
            /* We can wrap around so reset*/
            canAppState.canQueue.currentWrPtr = canAppState.canQueue.topOfQueue;
        }                
    }
    else 
    {
        /*We need to test for a special case where write pointer has wrapped around but read pointer is stuck*/
        if(canAppState.canQueue.currentWrPtr == canAppState.canQueue.currentRdPtr)
        {
            /*If they are same then check number of bytes pushed and pulled off the queue*/
            if(canAppState.canQueue.numPduEnqueued != canAppState.canQueue.numPduDequeued)
            {
                canAppState.canQueue.numOverflowEvt++;
                return -1; /*No space*/
            }
        }    
    }

    /*Get the timestamp*/
    index = canAppState.canDescQueue.currentWrIndex;
    canAppState.canDescQueue.Can_Pdu[index].rxTS = TimerP_getTimeInUsecs();

    /*Copy the packet data*/
    memcpy(canAppState.canQueue.currentWrPtr, PduInfoPtr->SduDataPtr, PduInfoPtr->SduLength);

    /*Increment write pointer and number of packets enqueued*/
    canAppState.canQueue.currentWrPtr += PduInfoPtr->SduLength;

    /*Copy descriptor data*/
    canAppState.canDescQueue.Can_Pdu[index].id = CanIf_Mailbox->CanId;
    canAppState.canDescQueue.Can_Pdu[index].length = PduInfoPtr->SduLength;
    canAppState.canDescQueue.Can_Pdu[index].srcPort = CanIf_Mailbox->ControllerId;

    canAppState.canDescQueue.currentWrIndex++;
    /*Check wraparound and reset*/
    if(canAppState.canDescQueue.currentWrIndex == MCAN_NUM_PDU_IN_Q)
    {
        canAppState.canDescQueue.currentWrIndex = 0;
    }   

    /*Increment number of CAN frames received*/
    canAppState.canQueue.numPduEnqueued++;

    return 0;
}

#define CAN_START_SEC_ISR_CODE
#include "Can_MemMap.h"

CAN_ISR_TEXT_SECTION FUNC(void, CAN_CODE_FAST) CanApp_CanXIsr (
                                                            uintptr_t CanPtr)
{
    CanApp_IsrType canChIsr = (CanApp_IsrType)CanPtr;
    /* Associated CAN ISR */
    canChIsr();
}

#define CAN_STOP_SEC_ISR_CODE
#include "Can_MemMap.h"

