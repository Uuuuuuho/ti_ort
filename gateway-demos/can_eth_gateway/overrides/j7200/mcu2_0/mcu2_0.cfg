/*
 *
 * Copyright (c) 2019 Texas Instruments Incorporated
 *
 * All rights reserved not granted herein.
 *
 * Limited License.
 *
 * Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
 * license under copyrights and patents it now or hereafter owns or controls to make,
 * have made, use, import, offer to sell and sell ("Utilize") this software subject to the
 * terms herein.  With respect to the foregoing patent license, such license is granted
 * solely to the extent that any such patent is necessary to Utilize the software alone.
 * The patent license shall not apply to any combinations which include this software,
 * other than combinations with devices manufactured by or for TI ("TI Devices").
 * No hardware patent is licensed hereunder.
 *
 * Redistributions must preserve existing copyright notices and reproduce this license
 * (including the above copyright notice and the disclaimer and (if applicable) source
 * code license limitations below) in the documentation and/or other materials provided
 * with the distribution
 *
 * Redistribution and use in binary form, without modification, are permitted provided
 * that the following conditions are met:
 *
 * *       No reverse engineering, decompilation, or disassembly of this software is
 * permitted with respect to any software provided in binary form.
 *
 * *       any redistribution and use are licensed by TI for use only with TI Devices.
 *
 * *       Nothing shall obligate TI to provide you with source code for the software
 * licensed and provided to you in object code.
 *
 * If software source code is provided to you, modification and redistribution of the
 * source code are permitted provided that the following conditions are met:
 *
 * *       any redistribution and use of the source code, including any resulting derivative
 * works, are licensed by TI for use only with TI Devices.
 *
 * *       any redistribution and use of any object code compiled from the source code
 * and any resulting derivative works, are licensed by TI for use only with TI Devices.
 *
 * Neither the name of Texas Instruments Incorporated nor the names of its suppliers
 *
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * DISCLAIMER.
 *
 * THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 * OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 * BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 * DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 * OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

var biosCommonCfg = xdc.loadCapsule("../bios_cfg/bios_common.cfg");
var biosCommonCfgArg =
{
    core:"mcu2_0",
    /* Two Timers per core:
     *  -- One for sysbios tick
     *  -- One for NimuCpsw Interrupt Pacing
     */
    timerAllocation:[
        {core: "mcu2_0", timer: ["DMTimer12", "DMTimer13"]},
        {core: "mcu2_1", timer: ["DMTimer14", "DMTimer15"]},
    ],
}
biosCommonCfg.init(biosCommonCfgArg)

var Core = xdc.useModule('ti.sysbios.family.arm.v7r.keystone3.Core');
Core.id = 0;


var Hwi = xdc.useModule('ti.sysbios.family.arm.v7r.keystone3.Hwi');
Hwi.vimBaseAddress = 0x0ff80000;

var Cache = xdc.useModule('ti.sysbios.family.arm.v7r.Cache');
Cache.enableCache = true;

/* DMTimer #x - in general, address is 0x024x0000 where x is timer # */
var Timer = xdc.useModule('ti.sysbios.timers.dmtimer.Timer');

var Clock = xdc.useModule('ti.sysbios.knl.Clock');
Clock.timerId = 0;

/* Create default heap and hook it into Memory */
var HeapMem = xdc.useModule('ti.sysbios.heaps.HeapMem');
var Memory = xdc.module('xdc.runtime.Memory')
var heapMemParams = new HeapMem.Params;
heapMemParams.size = 16384*28;
var heap0 = HeapMem.create(heapMemParams);
Memory.defaultHeapInstance = heap0;

/*
 * Initialize MPU and enable it
 *
 * Note: MPU must be enabled and properly configured for caching to work.
 */
xdc.loadCapsule("../bios_cfg/r5_mpu.xs");

var Reset = xdc.useModule("xdc.runtime.Reset");
Reset.fxns[Reset.fxns.length++] = "&utilsCopyVecs2ATcm";

/* load calculation related settings */
var Load                = xdc.useModule('ti.sysbios.utils.Load');
Load.swiEnabled = true;
Load.hwiEnabled = true;
Load.taskEnabled = true;
var Task = xdc.useModule('ti.sysbios.knl.Task');
if (false)
{
	Load.postUpdate   = '&app_loadUpdateCb';
	Load.updateInIdle = false;

	var APP_PROFILE_PRINT_DURATION = 30000;
	var APP_PROFILE_WINDOW_MS = 500;

	var profileTaskParams = new Task.Params;
	profileTaskParams.instance.name = "app_profile_task";
	profileTaskParams.stackSize = 4 * 1024;
	profileTaskParams.arg0 = APP_PROFILE_PRINT_DURATION;
	profileTaskParams.arg1 = APP_PROFILE_WINDOW_MS;
	profileTaskParams.priority = 15;
	var profileTaskHandle = Task.create('&app_profile', profileTaskParams);
}
Task.common$.namedInstance = true;
Task.common$.namedModule = true;

