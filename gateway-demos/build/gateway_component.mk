# File: gateway_component.mk
#       This file is component include make file of GATEWAY.
# List of variables set in this file and their purpose:
# <mod>_RELPATH        - This is the relative path of the module, typically from
#                        top-level directory of the package
# <mod>_PATH           - This is the absolute path of the module. It derives from
#                        absolute path of the top-level directory (set in env.mk)
#                        and relative path set above
# <mod>_INCLUDE        - This is the path that has interface header files of the
#                        module. This can be multiple directories (space separated)
# <mod>_PKG_LIST       - Names of the modules (and sub-modules) that are a part
#                        part of this module, including itself.
# <mod>_BOARD_DEPENDENCY    - "yes": means the code for this module depends on
#                             board and the compiled obj/lib has to be kept
#                             under <board> directory
#                             "no" or "" or if this variable is not defined: means
#                             this module has no board dependent code and hence
#                             the obj/libs are not kept under <board> dir.
# <mod>_CORE_DEPENDENCY     - "yes": means the code for this module depends on
#                             core and the compiled obj/lib has to be kept
#                             under <core> directory
#                             "no" or "" or if this variable is not defined: means
#                             this module has no core dependent code and hence
#                             the obj/libs are not kept under <core> dir.
# <mod>_APP_STAGE_FILES     - List of source files that belongs to the module
#                             <mod>, but that needs to be compiled at application
#                             build stage (in the context of the app). This is
#                             primarily for link time configurations or if the
#                             source file is dependent on options/defines that are
#                             application dependent. This can be left blank or
#                             not defined at all, in which case, it means there
#                             no source files in the module <mod> that are required
#                             to be compiled in the application build stage.
# <mod>_FP_DEPENDENCY       - "yes": means the code for this module depends on
#                             target toolchain floating point support.  Enabling
#                             this option will enable floating point print
#                             support for toolchains which link out floating
#                             point print support to save memory.
#
ifeq ($(gateway_component_make_include), )

DEFAULT_RTOS_LIST = tirtos

############################
# device gateway package
# List of components included under gateway lib
# The components included here are built and will be part of gateway lib
############################
gateway_LIB_LIST =
gateway_mcusw_LIB_LIST =
can_generator_mcusw_LIB_LIST =
gateway_mcusw_demo_LIB_LIST =
gateway_pdk_LIB_LIST =

############################
# device gateway application utility packages
# List of application utilities under gateway
# The components included here are built and will be part of gateway app_lib
############################
gateway_APP_LIB_LIST =

############################
# device gateway examples
# List of examples under gateway
# All the tests mentioned in list are built when test target is called
# List below all examples for allowed values
############################
gateway_EXAMPLE_LIST =

can_generator_EXAMPLE_LIST =

all_EXAMPLE_LIST =
############################
# Duplicate example list which should not be built with "all" (since this is
# duplicate -j option will build both and result in .obj correuption) but needs
# to be still exposed to top level makefile so that user can individually call
# them. Also these duplicate targets doesn't support package option and hence
# should not be included when packaging
############################
gateway_DUP_EXAMPLE_LIST =

can_generator_DUP_EXAMPLE_LIST =

# Components included

#include MCUSW module component makefile
-include $(MCUSW_INSTALL_PATH)/mcal_drv/mcusw_mcal_drv.mk
gateway_mcusw_LIB_LIST += can eth ethtrcv dio app_utils cdd_ipc
can_generator_mcusw_LIB_LIST += can dio app_utils
-include $(MCUSW_INSTALL_PATH)/mcuss_demos/mcusw_mcuss_demos.mk
gateway_mcusw_demo_LIB_LIST += demo_utils

#include PDK modules
-include $(PDK_INSTALL_PATH)/ti/build/comp_paths.mk
-include $(PDK_CSL_COMP_PATH)/csl_component.mk
-include $(PDK_UDMA_COMP_PATH)/udma_component.mk
-include $(PDK_OSAL_COMP_PATH)/osal_component.mk
-include $(PDK_FREERTOS_COMP_PATH)/freertos_component.mk
-include $(PDK_SCICLIENT_COMP_PATH)/sciclient_component.mk
-include $(PDK_IPC_COMP_PATH)/ipc_component.mk
-include $(PDK_SBL_COMP_PATH)/sbl_component.mk
-include $(PDK_I2C_COMP_PATH)/i2c_component.mk
-include $(PDK_BOARD_COMP_PATH)/board_component.mk
-include $(PDK_UART_COMP_PATH)/uart_component.mk
-include $(PDK_PM_COMP_PATH)/pm_component.mk
-include $(PDK_ENET_COMP_PATH)/enet_component.mk
gateway_pdk_LIB_LIST += csl csl_init
gateway_pdk_LIB_LIST += udma
ifneq ($(osal_LIB_LIST),)
  ifeq ($(BUILD_OS_TYPE),tirtos)
  gateway_pdk_LIB_LIST += osal_tirtos
  endif
  ifeq ($(BUILD_OS_TYPE),freertos)
  gateway_pdk_LIB_LIST += osal_freertos
  gateway_pdk_LIB_LIST += freertos
  endif
  ifeq ($(BUILD_OS_TYPE),baremetal)
  gateway_pdk_LIB_LIST += osal_nonos
  endif
endif
ifeq ($(CORE),mcu1_0)
ifneq ($(sciclient_LIB_LIST),)
  gateway_pdk_LIB_LIST += sciclient
  gateway_pdk_LIB_LIST += sciclient_direct
  gateway_pdk_LIB_LIST += rm_pm_hal
endif
ifeq ($(BUILD_OS_TYPE),tirtos)
    ifneq ($(sciclient_LIB_LIST),)
      gateway_pdk_LIB_LIST += sciserver_tirtos
    endif
else
    ifneq ($(sciclient_LIB_LIST),)
      gateway_pdk_LIB_LIST += sciserver_tirtos
    endif
endif
else
  ifneq ($(sciclient_LIB_LIST),)
     gateway_pdk_LIB_LIST += sciclient
  endif
endif
gateway_pdk_LIB_LIST += ipc
gateway_pdk_LIB_LIST += ipc_baremetal
gateway_pdk_LIB_LIST += i2c
#SBL does not support debug build as SBL does not fit in OCMC in debug mode.
#Bypass SBL in debug mode
ifneq ($(BUILD_PROFILE),debug)
gateway_pdk_LIB_LIST += sbl_lib_cust
endif
gateway_pdk_LIB_LIST += board
gateway_pdk_LIB_LIST += uart
gateway_pdk_LIB_LIST += pm_lib

ifeq ($(BUILD_OS_TYPE),tirtos)
gateway_pdk_LIB_LIST += enet_example_utils_full_tirtos
endif
ifeq ($(BUILD_OS_TYPE),freertos)
gateway_pdk_LIB_LIST += enet_example_utils_full_freertos
endif

# Library
# GATEWAY BSW_STUBS (TI RTOS)
gateway_bsw_stubs_rtos_COMP_LIST = gateway_bsw_stubs_rtos
gateway_bsw_stubs_rtos_RELPATH = mcuss_demos/Bsw_Stubs
gateway_bsw_stubs_rtos_PATH = $(GATEWAY_INSTALL_PATH)/mcuss_demos/Bsw_Stubs
gateway_bsw_stubs_rtos_MAKEFILE = -fmakefile
export gateway_bsw_stubs_rtos_MAKEFILE
gateway_bsw_stubs_rtos_BOARD_DEPENDENCY = no
gateway_bsw_stubs_rtos_CORE_DEPENDENCY = no
export gateway_bsw_stubs_rtos_COMP_LIST
export gateway_bsw_stubs_rtos_BOARD_DEPENDENCY
export gateway_bsw_stubs_rtos_CORE_DEPENDENCY
gateway_bsw_stubs_rtos_PKG_LIST = gateway_bsw_stubs_rtos
export gateway_bsw_stubs_rtos_PKG_LIST
gateway_bsw_stubs_rtos_INCLUDE = $(gateway_bsw_stubs_rtos_PATH)/Det/inc
gateway_bsw_stubs_rtos_INCLUDE += $(gateway_bsw_stubs_rtos_PATH)/MemMap/inc
gateway_bsw_stubs_rtos_INCLUDE += $(gateway_bsw_stubs_rtos_PATH)/Os/inc
gateway_bsw_stubs_rtos_INCLUDE += $(gateway_bsw_stubs_rtos_PATH)/Dem/inc
gateway_bsw_stubs_rtos_INCLUDE += $(gateway_bsw_stubs_rtos_PATH)/Rte/inc
gateway_bsw_stubs_rtos_INCLUDE += $(gateway_bsw_stubs_rtos_PATH)/EcuM/inc
gateway_bsw_stubs_rtos_INCLUDE += $(gateway_bsw_stubs_rtos_PATH)/EthIf/inc
gateway_bsw_stubs_rtos_INCLUDE += $(gateway_bsw_stubs_rtos_PATH)/AsrGnrl/inc
gateway_bsw_stubs_rtos_INCLUDE += $(gateway_bsw_stubs_rtos_PATH)/CanIf/inc
gateway_bsw_stubs_rtos_INCLUDE += $(gateway_bsw_stubs_rtos_PATH)/WdgIf/inc
export  gateway_bsw_stubs_rtos_SOCLIST = j721e j7200
export gateway_bsw_stubs_rtos_$(SOC)_CORELIST = mcu1_0 mcu2_0 mcu2_1
gateway_LIB_LIST += gateway_bsw_stubs_rtos

can_eth_gateway_app_COMP_LIST = can_eth_gateway_app
can_eth_gateway_app_RELPATH = can_eth_gateway
can_eth_gateway_app_PATH = $(GATEWAY_INSTALL_PATH)/can_eth_gateway
export can_eth_gateway_app_BOARD_DEPENDENCY = yes
export can_eth_gateway_app_CORE_DEPENDENCY = no
export can_eth_gateway_app_XDC_CONFIGURO = yes
can_eth_gateway_app_MAKEFILE = -fmakefile
can_eth_gateway_app_PKG_LIST = can_eth_gateway_app
can_eth_gateway_app_INCLUDE = $(can_eth_gateway_app_PATH)
export can_eth_gateway_app_BOARDLIST = j721e_evm j7200_evm
can_eth_gateway_app_$(SOC)_CORELIST = mcu2_0 mcu2_1
export can_eth_gateway_app_$(SOC)_CORELIST
export can_eth_gateway_app_SBL_APPIMAGEGEN = yes
gateway_EXAMPLE_LIST += can_eth_gateway_app
gateway_PKG_LIST_ALL = $(gateway_EXAMPLE_LIST) $(gateway_LIB_LIST) $(gateway_APP_LIB_LIST)

all_EXAMPLE_LIST += can_eth_gateway_app


can_traffic_generator_app_COMP_LIST = can_traffic_generator_app
can_traffic_generator_app_RELPATH = can_traffic_generator
can_traffic_generator_app_PATH = $(GATEWAY_INSTALL_PATH)/can_traffic_generator
export can_traffic_generator_app_BOARD_DEPENDENCY = yes
export can_traffic_generator_app_CORE_DEPENDENCY = no
export can_traffic_generator_app_XDC_CONFIGURO = yes
can_traffic_generator_app_MAKEFILE = -fmakefile
can_traffic_generator_app_PKG_LIST = can_traffic_generator_app
can_traffic_generator_app_INCLUDE = $(can_traffic_generator_app_PATH)
export can_traffic_generator_app_BOARDLIST = j721e_evm j7200_evm
can_traffic_generator_app_$(SOC)_CORELIST = mcu1_0
export can_traffic_generator_app_$(SOC)_CORELIST
export can_traffic_generator_app_SBL_APPIMAGEGEN = yes

can_generator_EXAMPLE_LIST += can_traffic_generator_app
can_generator_PKG_LIST_ALL = $(can_generator_EXAMPLE_LIST) $(gateway_APP_LIB_LIST)

all_EXAMPLE_LIST += can_traffic_generator_app

# Component specific CFLAGS
GATEWAY_CFLAGS =
GATEWAY_LNKFLAGS =

ifeq ($(CORE),mcu1_0)
  GATEWAY_CFLAGS += -DBUILD_MCU1_0 -DBUILD_MCU
  GATEWAY_LNKFLAGS += --define=BUILD_MCU1_0 --define=BUILD_MCU  --diag_suppress=10068
  LIB_ENDIAN_LIST = little
endif

ifeq ($(CORE),mcu1_1)
  GATEWAY_CFLAGS += -DBUILD_MCU1_1 -DBUILD_MCU
  GATEWAY_LNKFLAGS += --define=BUILD_MCU1_1 --define=BUILD_MCU  --diag_suppress=10068
  LIB_ENDIAN_LIST = little
endif

ifeq ($(CORE),mcu2_0)
  GATEWAY_CFLAGS += -DBUILD_MCU2_0 -DBUILD_MCU
  GATEWAY_LNKFLAGS += --define=BUILD_MCU2_0 --define=BUILD_MCU  --diag_suppress=10068
  LIB_ENDIAN_LIST = little
endif

ifeq ($(CORE),mcu2_1)
  GATEWAY_CFLAGS += -DBUILD_MCU2_1 -DBUILD_MCU
  GATEWAY_LNKFLAGS += --define=BUILD_MCU2_1 --define=BUILD_MCU  --diag_suppress=10068
  LIB_ENDIAN_LIST = little
endif

ifeq ($(CORE),mcu3_0)
  GATEWAY_CFLAGS += -DBUILD_MCU3_0 -DBUILD_MCU
  GATEWAY_LNKFLAGS += --define=BUILD_MCU3_0 --define=BUILD_MCU  --diag_suppress=10068
  LIB_ENDIAN_LIST = little
endif

ifeq ($(CORE),mcu3_1)
  GATEWAY_CFLAGS += -DBUILD_MCU3_1 -DBUILD_MCU
  GATEWAY_LNKFLAGS += --define=BUILD_MCU3_1 --define=BUILD_MCU  --diag_suppress=10068
  LIB_ENDIAN_LIST = little
endif

ifeq ($(CORE),mpu1_0)
  GATEWAY_CFLAGS += -DBUILD_MPU1_0 -DBUILD_MPU
#  GATEWAY_LNKFLAGS += --define=BUILD_MPU1_0 --define=BUILD_MPU
  LIB_ENDIAN_LIST = little
endif

GATEWAY_CFLAGS += -DAUTOSAR_431

export GATEWAY_CFLAGS
export GATEWAY_LNKFLAGS

gateway_component_make_include := 1
endif
