# File: env.mk. This file contains all the paths and other ENV variables

#
# Module paths
#

# Destination root directory.
#   - specify the directory where you want to place the object, archive/library,
#     binary and other generated files in a different location than source tree
#   - or leave it blank to place then in the same tree as the source
ifeq ($(MCAL_CONFIG),0)
  DEST_ROOT = $(GATEWAY_INSTALL_PATH)/binary
else
  DEST_ROOT = $(GATEWAY_INSTALL_PATH)/binary/config_$(MCAL_CONFIG)
endif

# Utilities directory. This is required only if the build machine is Windows.
#   - specify the installation directory of utility which supports POSIX commands
#     (eg: Cygwin installation or MSYS installation).
UTILS_INSTALL_DIR ?= $(utils_PATH)

# Set path separator, etc based on the OS
ifeq ($(OS),Windows_NT)
  PATH_SEPARATOR = ;
  UTILSPATH = $(UTILS_INSTALL_DIR)/
  export SHELL := $(UTILSPATH)sh.exe
else
  # else, assume it is linux
  PATH_SEPARATOR = :
  UTILSPATH = /bin/
endif

# Autosar framework include files
export autosarBSW_INCLUDE = $(autosarBSWInc_PATH)
#Autosar specific compiler files include path
export autosarCompiler_INCLUDE = $(autosarCompilerTypes_PATH)
#Autosar generated files include path
autosarConfig_INCLUDE = $(autosarConfig_PATH)

# pdk drivers
export pdk_INCLUDE = $(pdk_PATH)

# MCAL
export mcal_INCLUDE = $(mcal_PATH)

ifeq ($(BUILD_OS_TYPE),tirtos)
  # BIOS
  bios_INCLUDE = $(bios_PATH)/packages
  export bios_INCLUDE

  # XDC
  xdc_INCLUDE = $(xdc_PATH)/packages
  export xdc_INCLUDE
endif


include $(GATEWAY_INSTALL_PATH)/build/gateway_component.mk

# Commands commonly used within the make files
RM = $(UTILSPATH)rm -f
MV = $(UTILSPATH)mv
RMDIR = $(UTILSPATH)rm -rf
MKDIR = $(UTILSPATH)mkdir
ECHO = @$(UTILSPATH)echo

ifeq ($(OS),Windows_NT)
  MAKE = gmake
else
  MAKE = make
endif
EGREP = $(UTILSPATH)egrep
CP = $(UTILSPATH)cp
ifeq ($(OS),Windows_NT)
  CHMOD = $(UTILSPATH)echo
else
  CHMOD = $(UTILSPATH)chmod
endif

ifeq ($(BUILD_OS_TYPE),tirtos)
  ifeq ($(SOC),$(filter $(SOC), am65xx j721e j7200))
    ifeq ($(CONFIG_BLD_XDC_r5f),)
      CONFIG_BLD_XDC_r5f = $(pdk_PATH)/ti/build/$(SOC)/config_$(SOC)_r5f.bld
      CONFIG_BLD_LNK_r5f = $(MCUSW_INSTALL_PATH)/build/$(SOC)/$(CORE)/linker_r5_sysbios.lds
    endif
  endif
  ifeq ($(SOC),$(filter $(SOC), am65xx))
    ifeq ($(CONFIG_BLD_XDC_a53),)
        CONFIG_BLD_XDC_a53   = $(pdk_PATH)/ti/build/$(SOC)/config_$(SOC)_a53.bld
        CONFIG_BLD_LNK_a53   = $(MCUSW_INSTALL_PATH)/build/$(SOC)/$(CORE)/linker_a53.lds
    endif
  endif
  ifeq ($(SOC),$(filter $(SOC), j721e j7200))
    ifeq ($(CONFIG_BLD_XDC_a72),)
        CONFIG_BLD_XDC_a72   = $(pdk_PATH)/ti/build/$(SOC)/config_$(SOC)_a72.bld
        CONFIG_BLD_LNK_a72   = $(MCUSW_INSTALL_PATH)/build/$(SOC)/$(CORE)/linker_a72_mpu1_0.lds
    endif
  endif
  XDCROOT = $(xdc_PATH)
  XDCTOOLS = $(xdc_PATH)
  BIOSROOT = $(bios_PATH)
  export XDCROOT
  export XDCTOOLS
  export BIOSROOT
else
  ifeq ($(SOC),$(filter $(SOC), am65xx j721e j7200))
    ifeq ($(CONFIG_BLD_LNK_r5f),)
      CONFIG_BLD_LNK_r5f   = $(MCUSW_INSTALL_PATH)/build/$(SOC)/$(CORE)/linker_r5.lds
    endif
  endif
endif

CGTOOLS = $(TOOLCHAIN_PATH_R5)
export CGTOOLS
export CGTOOLS_A53 = $(TOOLCHAIN_PATH_A53)
export CGTOOLS_A72 = $(TOOLCHAIN_PATH_A72)

# Nothing beyond this point
