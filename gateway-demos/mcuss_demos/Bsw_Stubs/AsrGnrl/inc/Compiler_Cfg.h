/*
*
* Copyright (c) 2019 Texas Instruments Incorporated
*
* All rights reserved not granted herein.
*
* Limited License.
*
* Texas Instruments Incorporated grants a world-wide, royalty-free, non-exclusive
* license under copyrights and patents it now or hereafter owns or controls to make,
* have made, use, import, offer to sell and sell ("Utilize") this software subject to the
* terms herein.  With respect to the foregoing patent license, such license is granted
* solely to the extent that any such patent is necessary to Utilize the software alone.
* The patent license shall not apply to any combinations which include this software,
* other than combinations with devices manufactured by or for TI ("TI Devices").
* No hardware patent is licensed hereunder.
*
* Redistributions must preserve existing copyright notices and reproduce this license
* (including the above copyright notice and the disclaimer and (if applicable) source
* code license limitations below) in the documentation and/or other materials provided
* with the distribution
*
* Redistribution and use in binary form, without modification, are permitted provided
* that the following conditions are met:
*
* *       No reverse engineering, decompilation, or disassembly of this software is
* permitted with respect to any software provided in binary form.
*
* *       any redistribution and use are licensed by TI for use only with TI Devices.
*
* *       Nothing shall obligate TI to provide you with source code for the software
* licensed and provided to you in object code.
*
* If software source code is provided to you, modification and redistribution of the
* source code are permitted provided that the following conditions are met:
*
* *       any redistribution and use of the source code, including any resulting derivative
* works, are licensed by TI for use only with TI Devices.
*
* *       any redistribution and use of any object code compiled from the source code
* and any resulting derivative works, are licensed by TI for use only with TI Devices.
*
* Neither the name of Texas Instruments Incorporated nor the names of its suppliers
*
* may be used to endorse or promote products derived from this software without
* specific prior written permission.
*
* DISCLAIMER.
*
* THIS SOFTWARE IS PROVIDED BY TI AND TI'S LICENSORS "AS IS" AND ANY EXPRESS
* OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
* OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
* IN NO EVENT SHALL TI AND TI'S LICENSORS BE LIABLE FOR ANY DIRECT, INDIRECT,
* INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
* BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
* DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
* OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
* OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
* OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/

/**
 *  \file     Compiler_Cfg.h
 *
 *  \brief    This file defines compiler configuration
 *
 */

#ifndef COMPILER_CFG_H_
#define COMPILER_CFG_H_

#ifdef __cplusplus
extern "C"{
#endif

/** \brief Compiler Configuration Major Number */
#define COMPILER_SW_MAJOR_VERSION_CFG   (18U)
/** \brief Compiler Configuration Minor Number */
#define COMPILER_SW_MINOR_VERSION_CFG   (12U)
/** \brief Compiler Configuration Revision Number */
#define COMPILER_SW_PATCH_VERSION_CFG   (1U)

#if defined (AUTOSAR_421)
/** \brief AUTOSAR Version Major Number */
#define COMPILER_AR_RELEASE_MAJOR_VERSION_CFG       (4U)
/** \brief AUTOSAR Version Minor Number */
#define COMPILER_AR_RELEASE_MINOR_VERSION_CFG       (2U)
/** \brief AUTOSAR Version Revision Number */
#define COMPILER_AR_RELEASE_REVISION_VERSION_CFG    (1U)

/* End of 4.2.1 specifics */

#elif defined (AUTOSAR_431)

/** \brief AUTOSAR Version Major Number */
#define COMPILER_AR_RELEASE_MAJOR_VERSION_CFG       (4U)
/** \brief AUTOSAR Version Minor Number */
#define COMPILER_AR_RELEASE_MINOR_VERSION_CFG       (3U)
/** \brief AUTOSAR Version Revision Number */
#define COMPILER_AR_RELEASE_REVISION_VERSION_CFG    (1U)

/* End of 4.3.1 specifics */
#else
    #error UnSupported AUTOSAR Version
#endif

/* [SWS_BSW_00059] Published information elements. */
/** \brief Compiler Vendor ID */
#define COMPILER_VENDOR_ID  (44U)
/** \brief Module ID */
#define COMPILER_MODULE_ID  (198U)

/*****************************************************************************/
/* Mem class Types                                                           */
/*****************************************************************************/

/* Common for all modules. [SWS_COMPILER_00040] */
#define REGSPACE
/* Expected to be used for pointer to peripheral registers,
    static volatile CONSTP2VAR(uint32, PWM_CONST, REGSPACE)) */

/* Det SWS_COMPILER_00040 */
#define DET_CODE
#define DET_CODE_FAST
#define DET_CODE_SLOW
#define DET_CONST
#define DET_CALIB
#define DET_CONFIG_DATA
#define DET_APPL_DATA
#define DET_APPL_CONST
#define DET_VAR_NO_INIT
#define DET_VAR_CLEARED
#define DET_VAR_POWER_ON_CLEARED
#define DET_VAR_INIT
#define DET_VAR_POWER_ON_INIT
#define DET_VAR_FAST_NO_INIT
#define DET_VAR_FAST_CLEARED
#define DET_VAR_FAST_POWER_ON_CLEARED
#define DET_VAR_FAST_INIT
#define DET_VAR_FAST_POWER_ON_INIT
#define DET_VAR_SLOW_NO_INIT
#define DET_VAR_SLOW_CLEARED
#define DET_VAR_SLOW_POWER_ON_CLEARED
#define DET_VAR_SLOW_INIT
#define DET_VAR_SLOW_POWER_ON_INIT
#define DET_INTERNAL_VAR_NO_INIT
#define DET_INTERNAL_VAR_CLEARED
#define DET_INTERNAL_VAR_POWER_ON_CLEARED
#define DET_INTERNAL_VAR_INIT
#define DET_INTERNAL_VAR_POWER_ON_INIT

/* DEM driver SWS_COMPILER_00040 */
#define DEM_CODE
#define DEM_CODE_FAST
#define DEM_CODE_SLOW
#define DEM_CONST
#define DEM_CALIB
#define DEM_CONFIG_DATA
#define DEM_APPL_DATA
#define DEM_APPL_CONST
#define DEM_VAR_NO_INIT
#define DEM_VAR_CLEARED
#define DEM_VAR_POWER_ON_CLEARED
#define DEM_VAR_INIT
#define DEM_VAR_POWER_ON_INIT
#define DEM_VAR_FAST_NO_INIT
#define DEM_VAR_FAST_CLEARED
#define DEM_VAR_FAST_POWER_ON_CLEARED
#define DEM_VAR_FAST_INIT
#define DEM_VAR_FAST_POWER_ON_INIT
#define DEM_VAR_SLOW_NO_INIT
#define DEM_VAR_SLOW_CLEARED
#define DEM_VAR_SLOW_POWER_ON_CLEARED
#define DEM_VAR_SLOW_INIT
#define DEM_VAR_SLOW_POWER_ON_INIT
#define DEM_INTERNAL_VAR_NO_INIT
#define DEM_INTERNAL_VAR_CLEARED
#define DEM_INTERNAL_VAR_POWER_ON_CLEARED
#define DEM_INTERNAL_VAR_INIT
#define DEM_INTERNAL_VAR_POWER_ON_INIT

/* ECUM driver */
#define ECUM_CODE
#define ECUM_CODE_FAST
#define ECUM_CODE_SLOW
#define ECUM_CONST
#define ECUM_CALIB
#define ECUM_CONFIG_DATA
#define ECUM_APPL_DATA
#define ECUM_APPL_CONST
#define ECUM_VAR_NO_INIT
#define ECUM_VAR_CLEARED
#define ECUM_VAR_POWER_ON_CLEARED
#define ECUM_VAR_INIT
#define ECUM_VAR_POWER_ON_INIT
#define ECUM_VAR_FAST_NO_INIT
#define ECUM_VAR_FAST_CLEARED
#define ECUM_VAR_FAST_POWER_ON_CLEARED
#define ECUM_VAR_FAST_INIT
#define ECUM_VAR_FAST_POWER_ON_INIT
#define ECUM_VAR_SLOW_NO_INIT
#define ECUM_VAR_SLOW_CLEARED
#define ECUM_VAR_SLOW_POWER_ON_CLEARED
#define ECUM_VAR_SLOW_INIT
#define ECUM_VAR_SLOW_POWER_ON_INIT
#define ECUM_INTERNAL_VAR_NO_INIT
#define ECUM_INTERNAL_VAR_CLEARED
#define ECUM_INTERNAL_VAR_POWER_ON_CLEARED
#define ECUM_INTERNAL_VAR_INIT
#define ECUM_INTERNAL_VAR_POWER_ON_INIT


/* Can SWS_COMPILER_00040 */
#define CAN_CODE
#define CAN_CODE_FAST
#define CAN_CODE_SLOW
#define CAN_CONST
#define CAN_CALIB
#define CAN_CONFIG_DATA
#define CAN_APPL_DATA
#define CAN_APPL_CONST
#define CAN_VAR_NO_INIT
#define CAN_VAR_CLEARED
#define CAN_VAR_POWER_ON_CLEARED
#define CAN_VAR_INIT
#define CAN_VAR_POWER_ON_INIT
#define CAN_VAR_FAST_NO_INIT
#define CAN_VAR_FAST_CLEARED
#define CAN_VAR_FAST_POWER_ON_CLEARED
#define CAN_VAR_FAST_INIT
#define CAN_VAR_FAST_POWER_ON_INIT
#define CAN_VAR_SLOW_NO_INIT
#define CAN_VAR_SLOW_CLEARED
#define CAN_VAR_SLOW_POWER_ON_CLEARED
#define CAN_VAR_SLOW_INIT
#define CAN_VAR_SLOW_POWER_ON_INIT
#define CAN_INTERNAL_VAR_NO_INIT
#define CAN_INTERNAL_VAR_CLEARED
#define CAN_INTERNAL_VAR_POWER_ON_CLEARED
#define CAN_INTERNAL_VAR_INIT
#define CAN_INTERNAL_VAR_POWER_ON_INIT

/* CanIf SWS_COMPILER_00040 */
#define CANIF_CODE
#define CANIF_CODE_FAST
#define CANIF_CODE_SLOW
#define CANIF_CONST
#define CANIF_CALIB
#define CANIF_CONFIG_DATA
#define CANIF_APPL_DATA
#define CANIF_APPL_CONST
#define CANIF_VAR_NO_INIT
#define CANIF_VAR_CLEARED
#define CANIF_VAR_POWER_ON_CLEARED
#define CANIF_VAR_INIT
#define CANIF_VAR_POWER_ON_INIT
#define CANIF_VAR_FAST_NO_INIT
#define CANIF_VAR_FAST_CLEARED
#define CANIF_VAR_FAST_POWER_ON_CLEARED
#define CANIF_VAR_FAST_INIT
#define CANIF_VAR_FAST_POWER_ON_INIT
#define CANIF_VAR_SLOW_NO_INIT
#define CANIF_VAR_SLOW_CLEARED
#define CANIF_VAR_SLOW_POWER_ON_CLEARED
#define CANIF_VAR_SLOW_INIT
#define CANIF_VAR_SLOW_POWER_ON_INIT
#define CANIF_INTERNAL_VAR_NO_INIT
#define CANIF_INTERNAL_VAR_CLEARED
#define CANIF_INTERNAL_VAR_POWER_ON_CLEARED
#define CANIF_INTERNAL_VAR_INIT
#define CANIF_INTERNAL_VAR_POWER_ON_INIT

/* Dio SWS_COMPILER_00040 */
#define DIO_CODE
#define DIO_CODE_FAST
#define DIO_CODE_SLOW
#define DIO_CONST
#define DIO_CALIB
#define DIO_CONFIG_DATA
#define DIO_APPL_DATA
#define DIO_APPL_CONST
#define DIO_VAR_NO_INIT
#define DIO_VAR_CLEARED
#define DIO_VAR_POWER_ON_CLEARED
#define DIO_VAR_INIT
#define DIO_VAR_POWER_ON_INIT
#define DIO_VAR_FAST_NO_INIT
#define DIO_VAR_FAST_CLEARED
#define DIO_VAR_FAST_POWER_ON_CLEARED
#define DIO_VAR_FAST_INIT
#define DIO_VAR_FAST_POWER_ON_INIT
#define DIO_VAR_SLOW_NO_INIT
#define DIO_VAR_SLOW_CLEARED
#define DIO_VAR_SLOW_POWER_ON_CLEARED
#define DIO_VAR_SLOW_INIT
#define DIO_VAR_SLOW_POWER_ON_INIT
#define DIO_INTERNAL_VAR_NO_INIT
#define DIO_INTERNAL_VAR_CLEARED
#define DIO_INTERNAL_VAR_POWER_ON_CLEARED
#define DIO_INTERNAL_VAR_INIT
#define DIO_INTERNAL_VAR_POWER_ON_INIT

/* Dio SWS_COMPILER_00040 */
#define DIO_CODE
#define DIO_CODE_FAST
#define DIO_CODE_SLOW
#define DIO_CONST
#define DIO_CALIB
#define DIO_CONFIG_DATA
#define DIO_APPL_DATA
#define DIO_APPL_CONST
#define DIO_VAR_NO_INIT
#define DIO_VAR_CLEARED
#define DIO_VAR_POWER_ON_CLEARED
#define DIO_VAR_INIT
#define DIO_VAR_POWER_ON_INIT
#define DIO_VAR_FAST_NO_INIT
#define DIO_VAR_FAST_CLEARED
#define DIO_VAR_FAST_POWER_ON_CLEARED
#define DIO_VAR_FAST_INIT
#define DIO_VAR_FAST_POWER_ON_INIT
#define DIO_VAR_SLOW_NO_INIT
#define DIO_VAR_SLOW_CLEARED
#define DIO_VAR_SLOW_POWER_ON_CLEARED
#define DIO_VAR_SLOW_INIT
#define DIO_VAR_SLOW_POWER_ON_INIT
#define DIO_INTERNAL_VAR_NO_INIT
#define DIO_INTERNAL_VAR_CLEARED
#define DIO_INTERNAL_VAR_POWER_ON_CLEARED
#define DIO_INTERNAL_VAR_INIT
#define DIO_INTERNAL_VAR_POWER_ON_INIT

/* Eth SWS_COMPILER_00040 */
#define ETH_CODE
#define ETH_CODE_FAST
#define ETH_CODE_SLOW
#define ETH_CONST
#define ETH_CALIB
#define ETH_CONFIG_DATA
#define ETH_APPL_DATA
#define ETH_APPL_CONST
#define ETH_VAR_NO_INIT
#define ETH_VAR_CLEARED
#define ETH_VAR_POWER_ON_CLEARED
#define ETH_VAR_INIT
#define ETH_VAR_POWER_ON_INIT
#define ETH_VAR_FAST_NO_INIT
#define ETH_VAR_FAST_CLEARED
#define ETH_VAR_FAST_POWER_ON_CLEARED
#define ETH_VAR_FAST_INIT
#define ETH_VAR_FAST_POWER_ON_INIT
#define ETH_VAR_SLOW_NO_INIT
#define ETH_VAR_SLOW_CLEARED
#define ETH_VAR_SLOW_POWER_ON_CLEARED
#define ETH_VAR_SLOW_INIT
#define ETH_VAR_SLOW_POWER_ON_INIT
#define ETH_INTERNAL_VAR_NO_INIT
#define ETH_INTERNAL_VAR_CLEARED
#define ETH_INTERNAL_VAR_POWER_ON_CLEARED
#define ETH_INTERNAL_VAR_INIT
#define ETH_INTERNAL_VAR_POWER_ON_INIT
#define ETH_VAR_NO_INIT_UDMA

/* Eth Trcv SWS_COMPILER_00040 */
#define ETHTRCV_CODE
#define ETHTRCV_CODE_FAST
#define ETHTRCV_CODE_SLOW
#define ETHTRCV_CONST
#define ETHTRCV_CALIB
#define ETHTRCV_CONFIG_DATA
#define ETHTRCV_APPL_DATA
#define ETHTRCV_APPL_CONST
#define ETHTRCV_VAR_NO_INIT
#define ETHTRCV_VAR_CLEARED
#define ETHTRCV_VAR_POWER_ON_CLEARED
#define ETHTRCV_VAR_INIT
#define ETHTRCV_VAR_POWER_ON_INIT
#define ETHTRCV_VAR_FAST_NO_INIT
#define ETHTRCV_VAR_FAST_CLEARED
#define ETHTRCV_VAR_FAST_POWER_ON_CLEARED
#define ETHTRCV_VAR_FAST_INIT
#define ETHTRCV_VAR_FAST_POWER_ON_INIT
#define ETHTRCV_VAR_SLOW_NO_INIT
#define ETHTRCV_VAR_SLOW_CLEARED
#define ETHTRCV_VAR_SLOW_POWER_ON_CLEARED
#define ETHTRCV_VAR_SLOW_INIT
#define ETHTRCV_VAR_SLOW_POWER_ON_INIT
#define ETHTRCV_INTERNAL_VAR_NO_INIT
#define ETHTRCV_INTERNAL_VAR_CLEARED
#define ETHTRCV_INTERNAL_VAR_POWER_ON_CLEARED
#define ETHTRCV_INTERNAL_VAR_INIT
#define ETHTRCV_INTERNAL_VAR_POWER_ON_INIT

/* Gpt SWS_COMPILER_00040 */
#define GPT_CODE
#define GPT_CODE_FAST
#define GPT_CODE_SLOW
#define GPT_CONST
#define GPT_CALIB
#define GPT_CONFIG_DATA
#define GPT_APPL_DATA
#define GPT_APPL_CONST
#define GPT_VAR_NO_INIT
#define GPT_VAR_CLEARED
#define GPT_VAR_POWER_ON_CLEARED
#define GPT_VAR_INIT
#define GPT_VAR_POWER_ON_INIT
#define GPT_VAR_FAST_NO_INIT
#define GPT_VAR_FAST_CLEARED
#define GPT_VAR_FAST_POWER_ON_CLEARED
#define GPT_VAR_FAST_INIT
#define GPT_VAR_FAST_POWER_ON_INIT
#define GPT_VAR_SLOW_NO_INIT
#define GPT_VAR_SLOW_CLEARED
#define GPT_VAR_SLOW_POWER_ON_CLEARED
#define GPT_VAR_SLOW_INIT
#define GPT_VAR_SLOW_POWER_ON_INIT
#define GPT_INTERNAL_VAR_NO_INIT
#define GPT_INTERNAL_VAR_CLEARED
#define GPT_INTERNAL_VAR_POWER_ON_CLEARED
#define GPT_INTERNAL_VAR_INIT
#define GPT_INTERNAL_VAR_POWER_ON_INIT

/* Spi SWS_COMPILER_00040 */
#define SPI_CODE
#define SPI_CODE_FAST
#define SPI_CODE_SLOW
#define SPI_CONST
#define SPI_CALIB
#define SPI_CONFIG_DATA
#define SPI_APPL_DATA
#define SPI_APPL_CONST
#define SPI_VAR_NO_INIT
#define SPI_VAR_CLEARED
#define SPI_VAR_POWER_ON_CLEARED
#define SPI_VAR_INIT
#define SPI_VAR_POWER_ON_INIT
#define SPI_VAR_FAST_NO_INIT
#define SPI_VAR_FAST_CLEARED
#define SPI_VAR_FAST_POWER_ON_CLEARED
#define SPI_VAR_FAST_INIT
#define SPI_VAR_FAST_POWER_ON_INIT
#define SPI_VAR_SLOW_NO_INIT
#define SPI_VAR_SLOW_CLEARED
#define SPI_VAR_SLOW_POWER_ON_CLEARED
#define SPI_VAR_SLOW_INIT
#define SPI_VAR_SLOW_POWER_ON_INIT
#define SPI_INTERNAL_VAR_NO_INIT
#define SPI_INTERNAL_VAR_CLEARED
#define SPI_INTERNAL_VAR_POWER_ON_CLEARED
#define SPI_INTERNAL_VAR_INIT
#define SPI_INTERNAL_VAR_POWER_ON_INIT

/* Wdg SWS_COMPILER_00040 */
#define WDG_CODE
#define WDG_CODE_FAST
#define WDG_CODE_SLOW
#define WDG_CONST
#define WDG_CALIB
#define WDG_CONFIG_DATA
#define WDG_APPL_DATA
#define WDG_APPL_CONST
#define WDG_VAR_NO_INIT
#define WDG_VAR_CLEARED
#define WDG_VAR_POWER_ON_CLEARED
#define WDG_VAR_INIT
#define WDG_VAR_POWER_ON_INIT
#define WDG_VAR_FAST_NO_INIT
#define WDG_VAR_FAST_CLEARED
#define WDG_VAR_FAST_POWER_ON_CLEARED
#define WDG_VAR_FAST_INIT
#define WDG_VAR_FAST_POWER_ON_INIT
#define WDG_VAR_SLOW_NO_INIT
#define WDG_VAR_SLOW_CLEARED
#define WDG_VAR_SLOW_POWER_ON_CLEARED
#define WDG_VAR_SLOW_INIT
#define WDG_VAR_SLOW_POWER_ON_INIT
#define WDG_INTERNAL_VAR_NO_INIT
#define WDG_INTERNAL_VAR_CLEARED
#define WDG_INTERNAL_VAR_POWER_ON_CLEARED
#define WDG_INTERNAL_VAR_INIT
#define WDG_INTERNAL_VAR_POWER_ON_INIT

/* Pwm SWS_COMPILER_00040 */
#define PWM_CODE
#define PWM_CODE_FAST
#define PWM_CODE_SLOW
#define PWM_CONST
#define PWM_CALIB
#define PWM_CONFIG_DATA
#define PWM_APPL_DATA
#define PWM_APPL_CONST
#define PWM_VAR_NO_INIT
#define PWM_VAR_CLEARED
#define PWM_VAR_POWER_ON_CLEARED
#define PWM_VAR_INIT
#define PWM_VAR_POWER_ON_INIT
#define PWM_VAR_FAST_NO_INIT
#define PWM_VAR_FAST_CLEARED
#define PWM_VAR_FAST_POWER_ON_CLEARED
#define PWM_VAR_FAST_INIT
#define PWM_VAR_FAST_POWER_ON_INIT
#define PWM_VAR_SLOW_NO_INIT
#define PWM_VAR_SLOW_CLEARED
#define PWM_VAR_SLOW_POWER_ON_CLEARED
#define PWM_VAR_SLOW_INIT
#define PWM_VAR_SLOW_POWER_ON_INIT
#define PWM_INTERNAL_VAR_NO_INIT
#define PWM_INTERNAL_VAR_CLEARED
#define PWM_INTERNAL_VAR_POWER_ON_CLEARED
#define PWM_INTERNAL_VAR_INIT
#define PWM_INTERNAL_VAR_POWER_ON_INIT

/* Adc SWS_COMPILER_00040 */
#define ADC_CODE
#define ADC_CODE_FAST
#define ADC_CODE_SLOW
#define ADC_CONST
#define ADC_CALIB
#define ADC_CONFIG_DATA
#define ADC_APPL_DATA
#define ADC_APPL_CONST
#define ADC_VAR_NO_INIT
#define ADC_VAR_CLEARED
#define ADC_VAR_POWER_ON_CLEARED
#define ADC_VAR_INIT
#define ADC_VAR_POWER_ON_INIT
#define ADC_VAR_FAST_NO_INIT
#define ADC_VAR_FAST_CLEARED
#define ADC_VAR_FAST_POWER_ON_CLEARED
#define ADC_VAR_FAST_INIT
#define ADC_VAR_FAST_POWER_ON_INIT
#define ADC_VAR_SLOW_NO_INIT
#define ADC_VAR_SLOW_CLEARED
#define ADC_VAR_SLOW_POWER_ON_CLEARED
#define ADC_VAR_SLOW_INIT
#define ADC_VAR_SLOW_POWER_ON_INIT
#define ADC_INTERNAL_VAR_NO_INIT
#define ADC_INTERNAL_VAR_CLEARED
#define ADC_INTERNAL_VAR_POWER_ON_CLEARED
#define ADC_INTERNAL_VAR_INIT
#define ADC_INTERNAL_VAR_POWER_ON_INIT

/* CDD IPC SWS_COMPILER_00040 */
#define CDD_IPC_CODE
#define CDD_IPC_CODE_FAST
#define CDD_IPC_CODE_SLOW
#define CDD_IPC_CONST
#define CDD_IPC_CALIB
#define CDD_IPC_CONFIG_DATA
#define CDD_IPC_APPL_DATA
#define CDD_IPC_APPL_CONST
#define CDD_IPC_VAR_NO_INIT
#define CDD_IPC_VAR_CLEARED
#define CDD_IPC_VAR_POWER_ON_CLEARED
#define CDD_IPC_VAR_INIT
#define CDD_IPC_VAR_POWER_ON_INIT
#define CDD_IPC_VAR_FAST_NO_INIT
#define CDD_IPC_VAR_FAST_CLEARED
#define CDD_IPC_VAR_FAST_POWER_ON_CLEARED
#define CDD_IPC_VAR_FAST_INIT
#define CDD_IPC_VAR_FAST_POWER_ON_INIT
#define CDD_IPC_VAR_SLOW_NO_INIT
#define CDD_IPC_VAR_SLOW_CLEARED
#define CDD_IPC_VAR_SLOW_POWER_ON_CLEARED
#define CDD_IPC_VAR_SLOW_INIT
#define CDD_IPC_VAR_SLOW_POWER_ON_INIT
#define CDD_IPC_INTERNAL_VAR_NO_INIT
#define CDD_IPC_INTERNAL_VAR_CLEARED
#define CDD_IPC_INTERNAL_VAR_POWER_ON_CLEARED
#define CDD_IPC_INTERNAL_VAR_INIT
#define CDD_IPC_INTERNAL_VAR_POWER_ON_INIT


#ifdef __cplusplus
}
#endif

#endif /* COMPILER_CFG_H_ */
